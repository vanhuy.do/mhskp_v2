//
//  DbDefinesV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DbDefinesV2 : NSObject {
    
}

extern const NSString *DATABASE_NAME;
extern const NSString *DATABASE_NAME_DEMO;
extern const int DATABASE_VERSION;
extern const NSString *PANIC_RECEIVERS;
extern const NSString *USER;
extern const NSString *USER_LOGIN_LOGS;
extern const NSString *USER_CONFIGURATIONS;
extern const NSString *HOUSE_KEEPER_RECORDS;
extern const NSString *ROOM_ASSIGNMENT;
extern const NSString *SUPERVISOR_FILTER_DETAIL;
extern const NSString *ROOM;
extern const NSString *ROOM_BLOCKING;
extern const NSString *LOCATION;
extern const NSString *HOTELS;
extern const NSString *ROOM_STATUS;
extern const NSString *GUEST_INFOS;
extern const NSString *PROFILE_NOTE;
extern const NSString *PROFILE_NOTE_TYPE;
extern const NSString *CLEANING_STATUS;
extern const NSString *ROOM_RECORDS;
extern const NSString *ROOM_RECORD_HISTORY;
extern const NSString *ROOM_RECORDS_DETAIL;
extern const NSString *LAUNDRYS;
extern const NSString *LAUNDRY_ORDERS;
extern const NSString *LAUNDRY_TYPES;
extern const NSString *COUNT_CATEGORIES;
extern const NSString *COUNT_ITEMS;
extern const NSString *COUNT_ITEM_TYPES;
extern const NSString *COUNT_MORE_INFOR;
extern const NSString *COUNT_ORDERS;
extern const NSString *COUNT_ORDER_DETAILS;
extern const NSString *COUNT_SERVICE;
extern const NSString *LANGUAGE_REFERENCE;
extern const NSString *SETTINGS;
extern const NSString *GUIDELINES;
extern const NSString *GUIDE_ITEMS;
extern const NSString *GUIDE_ITEM_DETAIL;
extern const NSString *MESSAGE;
extern const NSString *CHECKLIST;
extern const NSString *CHECKLIST_ITEM;
extern const NSString *CHECKLIST_DETAILS;
extern const NSString *CHECKLIST_CATEGORIES;
extern const NSString *CHECKLIST_FORM;
extern const NSString *CHECKLIST_FORM_SCORE;
extern const NSString *CHECKLIST_ROOMTYPE;
extern const NSString *CHECKLIST_ITEM_CORE;
extern const NSString *engineering_case;
extern const NSString *engineering_case_detail;
extern const NSString *engineering_images;
extern const NSString *engineering_items;
extern const NSString *engineering_categories;
extern const NSString *LAF_CATIGORIES;
extern const NSString *ROOM_REMARK;
extern const NSString *LAF_ITEMS;
extern const NSString *LAF_IMAGES;
extern const NSString *LOSTANDFOUND;
extern const NSString *LOSTANDFOUND_DETAIL;
extern const NSString *ROOM_SERVICE_LATER;
extern const NSString *USER_LIST;
extern const NSString *USER_DETAILS;
extern const NSString *SUBJECT_MESSAGE_ITEMS;
extern const NSString *SUBJECT_MESSAGE_CATEGORIES;
extern const NSString *MESSAGE_TABLE;
extern const NSString *MESSAGE_PHOTOS;
extern const NSString *LAF_COLORS;
extern const NSString *LAUNDRY_CATEGORIES;
extern const NSString *LAUNDRY_ITEMS;
extern const NSString *LAUNDRY_ITEMS_PRICE;
extern const NSString *LAUNDRY_SERVICE;
extern const NSString *LAUNDRY_ORDER_DETAILS;
extern const NSString *LAUNDRY_ORDER;
extern const NSString *LAUNDRY_INSTRUCTIONS;
extern const NSString *LAUNDRY_INSTRUCTIONS_ORDER;
extern const NSString *MESSAGE_RECEIVER;
extern const NSString *FLOOR;
extern const NSString *ZONE;
extern const NSString *AMENITIES_CATEGORIES;
extern const NSString *AMENITIES_ITEM_PRICE;
extern const NSString *AMENITIES_ITEMS;
extern const NSString *AMENITIES_ORDER_DETAILS;
extern const NSString *AMENITIES_ORDERS;
extern const NSString *AMENITIES_SERVICE;
extern const NSString *ROOM_TYPE;
extern const NSString *AMENITIES_ITEMS_ORDER_TEMP;
extern const NSString *INSPECTION_STATUS;
extern const NSString *LAUNDRY_ITEMS_TEMP;
extern const NSString *UNASSIGN_ROOM;
extern const NSString *ASSIGN_ROOM;
extern const NSString *LANGUAGE_REFERENCES;
extern const NSString *REASSIGN_ROOM_ASSIGNMENT;

extern const NSString *ADDJOB_ITEM;
extern const NSString *ADDJOB_STATUS;
extern const NSString *ADDJOB_DETAIL;
extern const NSString *ADDJOB_DETAIL_RECORD;
extern const NSString *ADDJOB_FLOOR;
extern const NSString *ADDJOB_ROOM;
extern const NSString *ADDJOB_SEARCH;
extern const NSString *ADDJOB_CATEGORY;
extern const NSString *ADDJOB_ROOM_ITEM;
extern const NSString *ADDJOB_SEARCH_ROOM_CATEGORY;
extern const NSString *ADDJOB_GUEST_INFO;

//physical-history table
extern const NSString *PHYSICAL_HISTORY;
extern const NSString *COUNT_HISTORY;
extern const NSString *ENGINEERING_HISTORY;
extern const NSString *LOST_FOUND_HISTORY; 

extern const NSString *AMENITIESPOSTING;

extern const NSString *ROOMTYPE_INVENTORY;

extern const NSString *OTHER_ACTIVITY;
extern const NSString *OTHER_ACTIVITY_STATUS;
extern const NSString *OTHER_ACTIVITY_LOCATION;
extern const NSString *OTHER_ACTIVITY_ASSIGNMENT;
extern const NSString *OTHER_ACTIVITY_RECORD;

@end

@interface TableChecklistRoomType : NSObject {
    
}

extern const  NSString
*clrtUserId, *clrtRoomTypeID, *clrtChecklistID, *clrtLastModified;

@end

@interface TablePanicReceivers : NSObject {
    
}

extern const  NSString
*panic_receivers_owner_id,
*panic_receivers_receiver_id,
*panic_receivers_phone_number,
*panic_receivers_email,
*panic_receivers_receiver_name,
*panic_receivers_is_direct_call;

@end


@interface TableProfileNote : NSObject {
    
}

extern const  NSString
*profilenote_description,
*profilenote_last_modified,
*profilenote_user_id,
*profilenote_room_number,
*profilenote_guest_id;

@end

@interface TableProfileNoteType : NSObject {
    
}

extern const  NSString
*profilenote_type_code,
*profilenote_type_description,
*profilenote_type_id,
*profilenote_type_last_modified,
*profilenote_type_roomnumber,
*profilenote_type_user_id,
*profilenote_type_guest_id;

@end

@interface TableAddJobGuestInfo : NSObject {
    
}

extern const  NSString
*addjob_guestinfo_room_id,
*addjob_guestinfo_vip,
*addjob_guestinfo_user_id,
*addjob_guestinfo_guest_title,
*addjob_guestinfo_guest_name,
*addjob_guestinfo_lang_pref,
*addjob_guestinfo_check_in_date,
*addjob_guestinfo_check_out_date,
*addjob_guestinfo_pref_desc,
*addjob_guestinfo_last_modified,
*addjob_guestinfo_housekeeping_name,
*addjob_guestinfo_special_service,
*addjob_guestinfo_guest_number,
*addjob_guestinfo_photo,
*addjob_guestinfo_post_status;

@end

@interface TableAddJobDetailRecord : NSObject {
    
}

extern const  NSString
*addjob_detail_record_onday_addjob_id,
*addjob_detail_record_post_status,
*addjob_detail_record_user_id,
*addjob_detail_record_operation,
*addjob_detail_record_time,
*addjob_detail_record_last_modified;

@end

@interface TableAddJobSearchRoomCategory : NSObject {
    
}

extern const  NSString
*addjob_searchroomcategory_search_id,
*addjob_searchroomcategory_room_id,
*addjob_searchroomcategory_category_id,
*addjob_searchroomcategory_user_id,
*addjob_searchroomcategory_last_modified;

@end

@interface TableAddJobDetail : NSObject {
    
}

extern const  NSString  *addjob_detail_onday_addjob_id,
*addjob_detail_room_id,
*addjob_detail_expected_cleaning_time,
*addjob_detail_room_assign_id,
*addjob_detail_category_id,
*addjob_detail_status,
*addjob_detail_post_status,
*addjob_detail_user_id,
*addjob_detail_room_number,
*addjob_detail_total_time,
*addjob_detail_start_time,
*addjob_detail_stop_time,
*addjob_detail_last_modified,
*addjob_detail_remark,
*addjob_detail_assigned_date;

@end

@interface TableAddJobRoomItem : NSObject {
    
}

extern const  NSString *addjob_roomitem_onday_addjob_id,
*addjob_roomitem_item_id,
*addjob_roomitem_last_modified,
*addjob_roomitem_user_id;

@end

@interface TableAddJobCategory : NSObject {
    
}

extern const  NSString  *addjob_category_id,
*addjob_category_name,
*addjob_category_name_lang,
*addjob_category_name_lang2,
*addjob_category_last_modified,
*addjob_category_user_id;

@end

@interface TableAddJobRoom : NSObject {
    
}

extern const  NSString  *addjob_room_id,
*addjob_room_assign_id,
*addjob_room_number,
*addjob_room_status_id,
*addjob_room_floor_id,
*addjob_room_type_id,
*addjob_room_number_pending_job,
*addjob_room_search_id,
*addjob_room_is_due_out,
*addjob_room_remark,
*addjob_room_last_modified,
*addjob_room_user_id;

@end

@interface TableAddJobStatus : NSObject {
    
}

extern const  NSString  *addjob_status_id,
*addjob_status_name,
*addjob_status_lang,
*addjob_status_icon,
*addjob_status_last_modified;

@end

@interface TableAddJobItem : NSObject {
    
}

extern const  NSString  *addjob_item_id,
*addjob_item_user_id,
*addjob_item_onday_addjob_id,
*addjob_item_name,
*addjob_item_name_lang,
*addjob_item_name_lang2,
*addjob_item_last_modified;

@end

@interface TableAddJobFloor : NSObject {
    
}

extern const  NSString *addjob_floor_id;
extern const  NSString *addjob_floor_name;
extern const  NSString *addjob_floor_name_lang;
extern const  NSString *addjob_floor_search_id;
extern const  NSString *addjob_floor_last_modified;
extern const  NSString *addjob_floor_user_id;
extern const  NSString *addjob_floor_number_of_job;

@end


@interface TableAddJobSearch  : NSObject {
    
}

extern const  NSString *addjob_search_id;
extern const  NSString *addjob_search_user_id;
extern const  NSString *addjob_search_items_id;
extern const  NSString *addjob_search_last_modified;

@end

@interface TableMessageReceiver : NSObject {
    
}
extern const  NSString *message_receiver_owner_id,
*message_receiver_id,
*message_receiver_group_id,
*message_receiver_last_modified;

@end

@interface TableLostandFound_Color : NSObject {
    
}
extern const  NSString *laf_color_id;
extern const  NSString *laf_color_code;
extern const  NSString *laf_color_name;
extern const  NSString *laf_color_lang;
extern const  NSString *laf_color_lastModified;

@end

@interface TableMessagePhoto : NSObject {
    
}

extern const  NSString *message_photo_group_id,
*message_photo,
*message_photo_last_modified;

@end
@interface Table_Message : NSObject {
    
}

extern const  NSString *message_id,
*message_group_id,
*message_owner_id,
*message_kind,
*message_count_photo_attached,
*message_status,
*message_from_user_login_name,
*message_from_user_name,
*message_topic,
*message_content,
*message_last_modified;

@end
@interface TableSubject_Message_Categories : NSObject {
    
}
extern const  NSString *smc;
extern const  NSString *smc_name;
extern const  NSString *smc_name_lang;
extern const  NSString *smc_image;
@end

@interface TableSubject_Message_Items : NSObject {
    
}
extern const  NSString *smi_id;
extern const  NSString *smi_name;
extern const  NSString *smi_name_lang;
extern const  NSString *smi_category_id;
extern const  NSString *smi_image;
@end

@interface TableUser_List : NSObject {
    
}
extern const  NSString *usr_id;
extern const  NSString *usr_list_id;
extern const  NSString *usr_name;
extern const  NSString *usr_hotel_id;
extern const  NSString *usr_fullname;
extern const  NSString *user_fullname_lang;
extern const  NSString *usr_last_modified;
extern const  NSString *usr_supervisor;

@end

@interface TableUser : NSObject {
    
}
extern const  NSString *user_id;
extern const  NSString *user_name;
extern const  NSString *user_password;
extern const  NSString *user_role;
extern const  NSString *user_fullname;
extern const  NSString *user_fullnamelang;
extern const  NSString *user_language;
extern const  NSString *user_title;
extern const  NSString *user_hotel_id;
extern const  NSString *user_department;
extern const  NSString *user_department_id;
extern const  NSString *user_supervisor_id;
extern const  NSString *user_last_modified;
#define allow_block_room @"allow_block_room"
#define allow_reorder_room @"allow_reorder_room"
@end

@interface TableLogUser : NSObject {
    
}
extern const  NSString *log_user_id;
extern const  NSString *log_user_name;
extern const  NSString *log_user_last_login;
extern const  NSString *log_user_last_logout;
@end

@interface TableUserConfigurations : NSObject
{
    
}

extern const  NSString *row_id;
extern const  NSString *user_config_id;
extern const  NSString *user_config_key;
extern const  NSString *user_config_value;
extern const  NSString *last_modified;

@end

/*
@interface TableHouseKeeperRecords : NSObject {
    
}
extern const  NSString *hkp_user_id;
extern const  NSString *hkp_total_clean_time;
extern const  NSString *hkp_room_clean;
extern const  NSString *hkp_record_date;
extern const  NSString *hkp_posted;
@end */

@interface TableRoomAssignment : NSObject {
    
}
extern const  NSString *ra_id;
extern const  NSString *ra_room_id;
extern const  NSString *ra_user_id;
extern const  NSString *ra_assigned_date;
extern const  NSString *ra_prioprity;
extern const  NSString *ra_post_status;
extern const  NSString *ra_last_modified;
extern const  NSString *ra_priority_sort_order;
extern const NSString *ra_housekeeper_id;
extern const NSString *ra_guest_arrival_time;
extern const NSString *ra_guest_departure_time;
extern const NSString *ra_kind_of_room;
extern const NSString *ra_is_mock_room;
extern const NSString *ra_is_reassigned_room;
extern const NSString *ra_last_cleaning_date;
extern const NSString *ra_is_checked_ra_id;
extern const NSString *ra_zone_id;
#define ra_room_status_id   @"ra_room_status_id"
#define ra_room_cleaning_status_id  @"ra_room_cleaning_status_id"
#define ra_room_inspection_status_id    @"ra_room_inspection_status_id"
#define ra_room_expected_inspection_time @"ra_room_expected_inspection_time" 
#define ra_room_expected_clean_time  @"ra_room_expected_clean_time"
#define ra_guest_profile_id @"ra_guest_profile_id"
#define ra_first_name   @"ra_first_name"
#define ra_last_name    @"ra_last_name"
#define ra_delete       @"ra_delete"
#define ra_guest_first_name @"ra_guest_first_name"
#define ra_guest_last_name  @"ra_guest_last_name"
#define ra_inspected_score  @"ra_inspected_score"
#define ra_room_cleaning_credit @"ra_room_cleaning_credit"
#define ra_room_checklist_remark @"ra_room_checklist_remark"

@end

@interface TableSupervisorFilterDetail : NSObject {
    
}
extern const  NSString *filter_user_id;
extern const  NSString *filter_type;
extern const  NSString *filter_id;
extern const  NSString *filter_name;
extern const  NSString *filter_lang;
extern const  NSString *filter_hotel_id;
extern const  NSString *filter_room_number;
@end

@interface TableRoom : NSObject {
    
}
extern const  NSString *room_id;
extern const  NSString *room_hotel_id;
extern const  NSString *room_status_id ;
extern const  NSString *room_type_id;
extern const  NSString *room_lang;
extern const  NSString *room_building_name;

extern const  NSString *room_guest_name;

extern const  NSString *room_building_namelang ;
extern const  NSString *room_vip_status;
extern const  NSString *room_cleaning_status_id;
extern const  NSString *room_expected_clean_time ;
extern const  NSString *room_guest_reference;
extern const  NSString *room_additional_job;
extern const  NSString *room_post_status ;
extern const  NSString *room_last_modified ;
extern const  NSString *room_location_code ;

extern const NSString *room_expected_inspection_time ;
extern const NSString *room_is_re_cleaning;
extern const NSString *room_expected_status_id ;
extern const NSString *room_zone_id;
extern const NSString *room_inspection_status_id;
extern const NSString *room_floor_id;
#define room_building_id @"room_building_id"
@end

@interface TableRoomBlocking : NSObject
{
    
}

extern const NSString
*roomblocking_is_blocked,
*roomblocking_user_id,
*roomblocking_room_number,
*roomblocking_remark_physical_check,
*roomblocking_reason,
*roomblocking_oosdurations;

@end

@interface TableLocation : NSObject {
    
}
extern const  NSString *location_id;
extern const  NSString *location_hotel_id;
extern const  NSString *location_code;
extern const  NSString *location_desc;
extern const  NSString *location_lang;
extern const  NSString *location_roomNo;
extern const  NSString *location_type;
extern const  NSString *location_lastModified;

@end

@interface TableHotels : NSObject {
    
}
extern const  NSString *hotel_id ;
extern const  NSString *hotel_name_lang ;
extern const  NSString *hotel_logo ;
extern const  NSString *hotel_lang ;
extern const  NSString *hotel_last_modified ;
@end

@interface TableRoomStatus : NSObject {
    
}
extern const  NSString *rstat_id;
extern const  NSString *rstat_code;
extern const  NSString *rstat_name;
extern const  NSString *rstat_lang;
extern const  NSString *rstat_image;
extern const  NSString *rstat_last_modified;
extern const  NSString *rstat_physical_check;
extern const  NSString *rstat_find_status;
@end

//Definition Table Guest Info
@interface TableGuestInfos : NSObject {
    
}
extern const  NSString *guest_id ;
extern const  NSString *guest_name ;
extern const  NSString *guest_image;
extern const  NSString *guest_lang_pref ;
extern const  NSString *guest_room_id ;
extern const  NSString *guest_check_in_time ;
extern const  NSString *guest_check_out_time ;
extern const  NSString *guest_post_status;
extern const  NSString *guest_last_modified;
extern const NSString *guest_preference;
extern const NSString *vip_number;
extern const NSString *housekeeper_name;
extern const  NSString *guest_number;
extern const  NSString *guest_type;
//extern const  NSString *guest_pref_desc ;
extern const NSString *guest_title;
extern const  NSString *guest_special_service;
extern const  NSString *guest_preference_code;

@end

@interface TableCleaningStatus : NSObject {
    
}
extern const  NSString *cstat_id;
extern const  NSString *cstat_name ;
extern const  NSString *cstat_lang ;
extern const  NSString *cstat_image;
extern const  NSString *cstat_last_modified;
@end

@interface TableRoomRecords : NSObject {
    
}
extern const  NSString *rrec_id;
//extern const  NSString *rrec_room_id;
extern const  NSString *rrec_user_id;
extern const  NSString *rrec_cleaning_start_time;
extern const  NSString *rrec_cleaning_end_time;
extern const  NSString *rrec_cleaning_date;
extern const  NSString *rrec_pos_status;
extern const  NSString *rrec_inspection_start_time;
extern const  NSString *rrec_inspection_end_time;
#define rrec_roomAssignment_id @"rrec_room_assignment_id"
#define rrec_remark @"rrec_remark"

//Hao Tran - Remove
//extern const  NSString *rrec_cleaning_duration;
//extern const  NSString *rrec_cleaning_total_pause_time;
//#define rrec_inspected_total_pause_time @"rrec_inspected_total_pause_time"
//#define rrec_inspection_duration @"rrec_inspection_duration"

//Hao Tran - Add
extern const  NSString *rrec_last_cleaning_duration;
extern const  NSString *rrec_last_inspected_duration;
extern const  NSString *rrec_total_cleaning_time;
extern const  NSString *rrec_total_inspected_time;
extern const  NSString *rrec_room_status_id;
extern const  NSString *rrec_cleaning_status_id;
extern const  NSString *rrec_cleaning_pause_time;

@end



@interface TableRoomRecordDetail : NSObject {
    
}
extern const  NSString *recd_id;
extern const  NSString *recd_ra_id;
extern const  NSString *recd_user_id;
extern const  NSString *recd_start_date;
extern const  NSString *recd_stop_date;

@end

 

@interface TableEngineeringCase : NSObject {
    
}
extern const  NSString *ec_id;
extern const  NSString *ec_category_id ;
extern const  NSString *ec_item_id;
extern const  NSString *ec_detail_id;
extern const  NSString *ec_index;

@end

@interface TableEngineeringCaseDetail : NSObject {
    
}
extern const  NSString *ecd_id;
extern const  NSString *ecd_user_id ;
extern const  NSString *ecd_room_id;
extern const  NSString *ecd_pos_status;
extern const  NSString *ecd_date;
extern const  NSString *ecd_remark;
@end

@interface TableEngineeringImage : NSObject {
    
}
extern const  NSString *enim_id;
extern const  NSString *enim_image ;
extern const  NSString *enim_detail_id;
extern const  NSString *enim_case_id;
@end

@interface TableEngineeringItem : NSObject {
    
}
extern const  NSString *eni_id;
extern const  NSString *eni_name ;
extern const  NSString *eni_name_lang;
extern const  NSString *eni_category_id;
extern const  NSString *eni_last_modified;
extern const  NSString *eni_post_id;

@end

@interface TableEngineeringcategories : NSObject {
    
}
extern const  NSString *enca_id;
extern const  NSString *enca_name ;
extern const  NSString *enca_lang_name;
extern const  NSString *enca_image;
extern const  NSString *enca_last_modified;
@end


//Definition Table Laundry
@interface TableLaundryCategory : NSObject {
    
}
extern const  NSString *ldryca_id ;
extern const  NSString *ldryca_name ;
extern const  NSString *ldryca_name_lang ;
extern const  NSString *ldryca_image ;
extern const  NSString *ldryca_last_modified;

@end

@interface TableLaundryItem : NSObject {
    
}
extern const  NSString *ldry_Id ;
extern const  NSString *ldry_Name ;
extern const  NSString *ldry_Lang ;
extern const  NSString *ldry_item_last_modifier ;
extern const  NSString *ldry_image;
extern const  NSString *ldry_male_or_female;
@end
@interface TableLaundry : NSObject {
    
}
extern const  NSString *ldry_id ;
extern const  NSString *ldry_name ;
extern const  NSString *ldry_unit_price ;
extern const  NSString *ldry_type_id ;
extern const  NSString *ldry_lang ;
extern const NSString *ldry_last_modified;
@end

@interface TableLaundryOrders : NSObject {
    
}
extern const  NSString *sldry_id;
extern const  NSString *sldry_room_id;
extern const  NSString *sldry_user_id;
extern const  NSString *sldry_status;
extern const  NSString *sldry_remark;
extern const  NSString *sldry_collect_date;
extern const  NSString *sldry_item_id;
extern const  NSString *sldry_quantity;
extern const  NSString *sldry_post_status;
@end
@interface TableLaundryOrders1 : NSObject {
    
}
extern const  NSString *lo_id;
extern const  NSString *lo_room_id;
extern const  NSString *lo_user_id;
extern const  NSString *lo_status;
extern const  NSString *lo_remark;
extern const  NSString *lo_collect_date;

@end

@interface TableLaundryTypes : NSObject {
    
}
extern const  NSString *tldry_id;
extern const  NSString *tldry_name;
extern const  NSString *tldry_hotel_id;
extern const  NSString *tldry_last_modified;
extern const  NSString *tldry_lang;
extern const  NSString *ldry_service_id;
@end
@interface TableLaundItemPrice : NSObject {
    
}
extern const  NSString *lip_id;
extern const  NSString *lip_item_id;
extern const  NSString *lip_category_id;
extern const  NSString *lip_price;
extern const  NSString *lip_last_modified;
@end

@interface TableLaundryOrderDetails: NSObject {
    
}
extern const  NSString *lod_id;
extern const  NSString *lod_laundry_order_id;
extern const  NSString *lod_item_id;
extern const  NSString *lod_quantity;
extern const  NSString *lod_post_status;
extern const  NSString *lod_service_id;
extern const  NSString *lod_category_id;
extern const NSString *lod_price;
@end


@interface TableLaundryService : NSObject {
    
}
extern const  NSString *lservice_id;
extern const  NSString *lservice_name;
extern const  NSString *lservice_name_lang;
extern const  NSString *lservice_last_modifed;
@end

@interface TableLaundryInstructions : NSObject {
    
}
extern const  NSString *li_id;
extern const  NSString *li_name;
extern const  NSString *li_name_lang;
extern const  NSString *li_last_modified;
@end

@interface TableLaundryInstructionsOrder : NSObject {
    
}
extern const  NSString *lio_id;
extern const  NSString *lio_intruction_id;
extern const  NSString *lio_order_id;

@end

//Definition Table Count
@interface TableCountCategories : NSObject {
    
}
extern const NSString *cc_id;
extern const NSString *cc_name;
extern const NSString *cc_hotel_id;
extern const NSString *cc_name_lang;
extern const NSString *cc_image;
extern const NSString *c_service_id;
extern const NSString *cc_last_modified;
@end

@interface TableCountItems : NSObject {
    
}
extern const NSString *count_id;
extern const NSString *count_name;
extern const NSString *count_unit_price;
extern const NSString *count_type_id;
extern const NSString *count_lang;
extern const NSString *count_category_id;
extern const NSString *count_image;
extern const NSString *count_last_modified;
@end

@interface TableCountOrderDetails : NSObject {
    
}
extern const NSString *dcount_id;
extern const NSString *dcount_order_id;
extern const NSString *dcount_item_id;
extern const NSString *dcount_collected;
extern const NSString *dcount_new;
extern const NSString *dcount_quantity;
extern const NSString *dcount_pos_status;
extern const NSString *dcount_collected_date;
@end

@interface TableCountOrder : NSObject {
    
}
extern const NSString *scount_id;
extern const NSString *scount_room_id;
extern const NSString *scount_user_id;
extern const NSString *scount_status;
extern const NSString *scount_collect_date;
extern const NSString *scount_service_id;
extern const NSString *scount_room_number;
extern const NSString *scount_post_status;
extern const NSString *scount_order_ws_id;
@end

@interface TableCountService : NSObject {
    
}
extern const NSString *cs_id;
extern const NSString *cs_kind;
extern const NSString *cs_image;
extern const NSString *cs_name;
extern const NSString *cs_name_lang;
@end

@interface TableCountItemTypes : NSObject {
    
}
extern const NSString *tcount_id;
extern const NSString *tcount_name;
extern const NSString *tcount_chargeable;
extern const NSString *tcount_hotel_id;
extern const NSString *tcount_lang;
@end

@interface TableCountMoreInfor : NSObject {
    
}
extern const NSString *cmi_id;
extern const NSString *cmi_category_id;
extern const NSString *cmi_regular_or_express;
extern const NSString *cmi_male_or_female;
@end


@interface TableLanguageReference : NSObject {
}
extern const  NSString *langRe_id;
extern const  NSString *langRe_name;
extern const  NSString *langRe_pack;
extern const  NSString *langRe_active;
extern const  NSString *langRe_lastmodified;
extern const  NSString *langRe_currencysymbol;
extern const  NSString *langRe_currencynodigitafterdecimal;
@end

//Definition Table Setting
@interface TableSetting : NSObject {
    
}
extern const NSString *settings_lang;
extern const NSString *settings_sync;
extern const NSString *settings_last_sync;
extern const NSString *settings_notification_type;
extern const NSString *settings_photo;
extern const NSString *setting_log_enable;
extern const NSString *settings_log_amount;
extern const NSString *settings_ws_url;
extern const NSString *settings_room_validate_enable;
extern const NSString *settings_background_mode; // CFG [20160927/CRF-00001432]
extern const NSString *settings_heartbeat_server;  // CFG [20160927/CRF-00001432]
extern const NSString *settings_heartbeat_port;  // CFG [20160927/CRF-00001432]

@end

@interface TableGuidelines : NSObject {
    
}
extern const NSString *guide_id;
extern const NSString *guide_room_type_id;
extern const NSString *guide_room_section_id;
extern const NSString *guide_name;
extern const NSString *guide_name_lang;
extern const NSString *guide_contents;
extern const NSString *guide_content_lang;
extern const NSString *guide_image;
extern const NSString *guide_last_updated;
extern const NSString *guide_category_id;
@end

@interface TableMessage : NSObject {
    
}

extern const  NSString *msg_id;
extern const  NSString *msg_sender_id;
extern const  NSString *msg_receiver;
extern const  NSString *msg_topic;
extern const  NSString *msg_contents;
extern const  NSString *msg_read;
extern const  NSString *msg_user_id;
extern const  NSString *msg_last_modified;
extern const NSString *msg_sender_name;
extern const NSString *msg_sender_name_lang;
extern const NSString *msg_owner_id;
@end

//Definition Table CheckList
@interface TableChecklist : NSObject {
    
}
extern const  NSString *chklist_id;
extern const  NSString *chklist_user_id ;
extern const  NSString *chklist_room_id ;
extern const  NSString *chklist_inspect_date ;
extern const  NSString *chklist_status ;
extern const  NSString *chklist_post_status ;
extern const  NSString *chklist_core ;
@end

@interface TableChecklistCategories : NSObject {
    
}
extern const  NSString *chc_id ;
extern const  NSString *chc_form_id ;
extern const  NSString *chc_name;
extern const  NSString *chc_name_lang ;
extern const NSString *chc_last_modified;
@end

@interface TableChecklistForm : NSObject {
    
}
extern const  NSString *form_id ;
extern const  NSString *form_name ;
extern const  NSString *form_lang_name;
extern const  NSString *form_kind ;
extern const  NSString *form_hotel_id ;
extern const  NSString *form_possible_point ;
extern const NSString *form_passing_score;
extern const NSString *form_last_modified;
@end

@interface TableChecklistFormScore : NSObject {}

extern const NSString *df_id;	//Primary Key
extern const NSString *df_score;
extern const NSString *df_post_status;
extern const NSString *df_checklist_id;
extern const NSString *df_form_id;
extern const NSString *df_user_id;
extern const NSString *df_is_mandatory_pass;
@end

@interface TableChecklistItem : NSObject {
    
}
extern const  NSString *chkitem_id ;
extern const  NSString *chkitem_name ;
extern const  NSString *chkitem_room_type ;
extern const  NSString *chkitem_lang ;
extern const  NSString *chkitem_pass_score ;
extern const  NSString *chkitem_last_update ;
extern const NSString *chkitem_category_id;
extern const NSString *chkitem_form_id;
extern const NSString *chkitem_mandatory_pass;
@end


@interface TableChecklistItemCore : NSObject {
    
}
extern const  NSString *cic_id ;
extern const  NSString *cic_item_id;
extern const  NSString *cic_core ;
extern const NSString *cic_form_score;
extern const NSString *cic_post_status;
@end


@interface TableChecklistDetail : NSObject {
    
}
extern const  NSString *chkdetails_id ;
extern const  NSString *chkdetails_checklist_id ;
extern const  NSString *chkdetails_checklist_item ;
extern const  NSString *chkdetails_score ;
extern const  NSString *chkdetails_comment ;
extern const  NSString *chkdetails_post_status ;
@end

@interface TableLostandFoundCategories : NSObject {
}
extern const  NSString *lafc_id;
extern const  NSString *lafc_name;
extern const  NSString *lafc_lang_name;
extern const  NSString *lafc_image;
extern const  NSString *lafc_last_modified;
    
@end

@interface TableRoomRemark : NSObject {
}    
extern const  NSString *rm_id;
extern const  NSString *rm_time;
extern const  NSString *rm_record_id;
extern const  NSString *rm_pos_status;
extern const  NSString *rm_clean_pause_time;
extern const  NSString *rm_content;
extern const  NSString *rm_physical_content;
extern const  NSString *rm_roomassignment_roomId;
@end

@interface TableLostandFoundItems : NSObject {
}
extern const  NSString *lafi_id;
extern const  NSString *lafi_name;
extern const  NSString *lafi_name_lang;
extern const  NSString *lafi_image;
extern const  NSString *lafi_category_id;
extern const  NSString *lafi_last_modified;

@end

@interface TableLostandFoundImages : NSObject {
}
extern const  NSString *lafim_id ;
extern const  NSString *lafim_image ;
extern const  NSString *lafim_lafdetail_id;
@end

@interface TableLostandFound : NSObject {
}
extern const  NSString *laf_id ;
extern const  NSString *laf_category_id ;
extern const  NSString *laf_item_id;
extern const  NSString *laf_color ;
extern const  NSString *laf_lafdetail_id ;
extern const  NSString *laf_item_quantity;
@end

@interface TableLostandFoundDetail : NSObject {
}
extern const  NSString *lafd_id ;
extern const  NSString *lafd_user_id ;
extern const  NSString *lafd_room_id;
extern const  NSString *lafd_pos_status ;
extern const  NSString *lafd_date ;
extern const  NSString *lafd_remark ;
extern const  NSString *lafd_room_assignment_id;
@end

@interface TableRoomServiceLater : NSObject {
    
}    
extern const  NSString *rsl_id;
extern const  NSString *rsl_time;
extern const  NSString *rsl_record_id;
@end

@interface TableGuideItems : NSObject {
}
extern const NSString *gitem_id;
//extern const NSString *guide_id;
extern const NSString *gitem_name;
extern const NSString *gitem_name_lang;
extern const NSString *gitem_image;
extern const NSString *gitem_content;
extern const NSString *gitem_content_lang;
extern const NSString *gitem_section_id;
extern const NSString *gitem_last_modified;

@end

@interface tableFloor : NSObject {
}
extern const NSString *floor_id;
extern const NSString *floor_name;
extern const NSString *floor_name_lang;
extern const NSString *floor_building_id;
extern const NSString *floor_last_modified;
@end

@interface tableZone : NSObject {
}
extern const NSString *zone_id;
extern const NSString *zone_name;
extern const NSString *zone_name_lang;
extern const NSString *zone_floor_id;
extern const NSString *zone_last_modified;
@end

@interface tableUnassignRoom : NSObject {
}

#define  ur_id  @"ur_id"
#define  ur_floor_id  @"ur_floor_id"
#define  ur_zone_id  @"ur_zone_id"
#define  ur_hotel_id  @"ur_hotel_id"
#define  ur_room_status_id  @"ur_room_status_id"
#define  ur_cleaning_status_id  @"ur_cleaning_status_id"
#define  ur_guest_first_name  @"ur_guest_first_name"
#define  ur_guest_last_name  @"ur_guest_last_name"
#define  ur_room_kind  @"ur_room_kind"
#define  ur_room_id  @"ur_room_id"
#define  ur_last_modified  @"ur_last_modified"
@end

@interface tableAssignRoom : NSObject {    
}
#define ar_id @"ar_id"
#define ar_assign_id @"ar_assign_id"
#define ar_housekeeper_id @"ar_housekeeper_id"
#define ar_assign_date @"ar_assign_date"
#define ar_remark @"ar_remark"
#define ar_post_status @"ar_post_status"
#define ar_supervisor_id @"ar_supervisor_id"

@end

@interface TableLanguage: NSObject {
}
    extern const NSString *lang_id;
    extern const NSString *lang_name;
    extern const NSString *lang_pack;
    extern const NSString *lang_active;
    extern const NSString *lang_last_modified;
    extern const NSString *lang_currency;
    extern const NSString *lang_decimal_place;

@end

//Definition Table Amenities
@interface TableAmenitiesCategories : NSObject {
}

extern const NSString *amca_id;	//Primary Key
extern const NSString *amca_name;
extern const NSString *amca_hotel_id;
extern const NSString *amca_name_lang;
extern const NSString *amca_image;
extern const NSString *amca_last_modified;

@end

@interface TableAmenitiesItemPrice : NSObject {
}

extern const NSString *amip_id;	//Primary Key
extern const NSString *amip_item_id;
extern const NSString *amip_service_id;
extern const NSString *amip_price;

@end

@interface TableAmenitiesItems : NSObject {
}

extern const NSString *item_id;	//Primary Key
extern const NSString *item_name;
extern const NSString *item_lang;
extern const NSString *item_category_id;
extern const NSString *item_image;
extern const NSString *item_roomtype_id;
extern const NSString *item_last_modified;

@end

@interface TableAmenitiesOrderDetails : NSObject {
}

extern const NSString *amd_id;	//Primary Key
extern const NSString *amd_item_id;
extern const NSString *amd_new_quantity;
extern const NSString *amd_used_quantity;
extern const NSString *amd_category_id;
extern const NSString *amd_user_id;
extern const NSString *amd_room_assign_id;
extern const NSString *amd_room_number;
extern const NSString *amd_hotel_id;
extern const NSString *amd_transaction_date_time;

@end

@interface TableAmenitiesOrders : NSObject {
}

extern const NSString *amen_id;	//Primary Key
extern const NSString *amen_room_id;
extern const NSString *amen_user_id;
extern const NSString *amen_status;
extern const NSString *amen_collect_date;
extern const NSString *amen_service;

@end

@interface TableAmenitiesService : NSObject {
}

extern const NSString *ams_id;	//Primary Key
extern const NSString *ams_name;
extern const NSString *ams_name_lang;
extern const NSString *ams_last_modified;
extern const NSString *ams_image;

@end

@interface TableAmenitiesItemsOrderTemp : NSObject {}

extern const NSString *item_idtemp;	//Primary Key
extern const NSString *item_nametemp;
extern const NSString *item_langtemp;
extern const NSString *item_category_idtemp;	//Primary Key
extern const NSString *item_imagetemp;
extern const NSString *item_roomtype_idtemp;	//Primary Key
extern const NSString *item_last_modifiedtemp;
extern const NSString *item_quantitytemp;

@end


@interface TableInspectionStatus : NSObject {}

extern const NSString *istat_id;	//Primary Key
extern const NSString *istat_name;
extern const NSString *istat_image;
extern const NSString *istat_lang;	//Primary Key

@end

@interface FindAttendant : NSObject

#define atd_name                 @"atdName"
#define atd_currentstatus        @"atdCurrentStatus"
#define atd_roomno               @"atdRoomNo"
#define atd_roomassignID         @"atdRoomAssignID"
#define atd_currentscheduletime  @"atdCurrentScheduleTime"

@end




@interface TableReassignRoomAssignment : NSObject {}

extern const NSString *reassign_user_id;	//Primary Key
extern const NSString *reassign_room_id;
extern const NSString *reassign_datetime;
extern const NSString *reasign_expected_cleaning_time;
extern const NSString *reassign_room_status;
extern const NSString *reassign_cleaning_status;
extern const NSString *reassign_housekeeper_id;
extern const NSString *reassign_room_type;
extern const NSString *reassign_floor_id;
extern const NSString *reassign_guest_profile_id;
extern const NSString *reassign_hotel_id;
extern const NSString *reassign_ra_id;	//Primary Key
extern const NSString *reassign_room_remark;
@end


@interface TableAccessRight : NSObject{}

extern const NSString *accessRight_userId;
extern const NSString *accessRight_RoomAssignment_RoomAssignment;
extern const NSString *accessRight_RoomAssignment_CompletedRoom;
extern const NSString *accessRight_RoomAssignment_ManualUpdateRoomStatus;
extern const NSString *accessRight_RoomAssignment_RestrictionAssignment;
extern const NSString *accessRight_RoomAssignment_QRCodeScanner;
extern const NSString *accessRight_Posting_LostAndFound;
extern const NSString *accessRight_Posting_Engineering;
extern const NSString *accessRight_Posting_Laundry;
extern const NSString *accessRight_Posting_Minibar;
extern const NSString *accessRight_Posting_Linen;
extern const NSString *accessRight_Posting_Amenities;
extern const NSString *accessRight_Posting_PhysicalCheck;
extern const NSString *accessRight_Action_LostAndFound;
extern const NSString *accessRight_Action_Engineering;
extern const NSString *accessRight_Action_Laundry;
extern const NSString *accessRight_Action_Minibar;
extern const NSString *accessRight_Action_Linen;
extern const NSString *accessRight_Action_Amenities;
extern const NSString *accessRight_Action_GuideLine;
extern const NSString *accessRight_Action_CheckList;
extern const NSString *accessRight_Message;
extern const NSString *accessRight_FindAttendantPendingCompleted;
extern const NSString *accessRight_FindRoom;
extern const NSString *accessRight_FindInspection;
extern const NSString *accessRight_Setting;

//Update code
@end
@interface TableEngineeringHistory : NSObject
extern const NSString *EH_id;
extern const NSString *EH_user_id;
extern const NSString *EH_room_id;
extern const NSString *EH_item_id;
extern const NSString *EH_date;

@end

@interface TablePhysicalHistory : NSObject
extern const NSString *PH_floor_id;
extern const NSString *PH_room_id;
extern const NSString *PH_status;
extern const NSString *PH_dnd;
extern const NSString *PH_date;
extern const NSString *PH_user_id;
@end

@interface TableCountHistory : NSObject
extern const NSString *H_count_id;
extern const NSString *H_room_id;
extern const NSString *H_count_item_id;
extern const NSString *H_count_collected;
extern const NSString *H_count_new;
extern const NSString *H_count_service;
extern const NSString *H_count_date;
extern const NSString *H_count_user_id;
@end
@interface TableLAFHistory : NSObject
extern const NSString *LAFH_id;
extern const NSString *LAFH_room_id;
extern const NSString *LAFH_item_id;
extern const NSString *LAFH_color_id;
extern const NSString *LAFH_quantity;
extern const NSString *LAFH_user_id;
extern const NSString *LAFH_date;
@end

@interface TableAMPosting : NSObject
extern const NSString *AP_id;
extern const NSString *AP_order_id;
extern const NSString *AP_item_id;
extern const NSString *AP_quantity;
extern const NSString *AP_post_status;
extern const NSString *AP_date;
@end

@interface TableRoomTypeInventory : NSObject
extern const NSString *rti_item_id;
extern const NSString *rti_user_id;
extern const NSString *rti_category_id;
extern const NSString *rti_hotel_id;
extern const NSString *rti_roomtype_id;
extern const NSString *rti_building_id;
extern const NSString *rti_section_id;
extern const NSString *rti_service_item;
extern const NSString *rti_default_quantity; // CFG [20160921/CRF-00001275]
@end

@interface TableOtherActivity : NSObject
extern const NSString *oa_id;
extern const NSString *oa_name;
extern const NSString *oa_name_lang;
extern const NSString *oa_code;
extern const NSString *oa_desc;
extern const NSString *oa_desc_lang;
extern const NSString *oa_last_modified;
@end

@interface TableOtherActivityStatus : NSObject
extern const NSString *oas_id;
extern const NSString *oas_name;
extern const NSString *oas_lang;
extern const NSString *oas_image;
extern const NSString *oas_last_modified;
@end

@interface TableOtherActivityLocation : NSObject
extern const NSString *oal_id;
extern const NSString *oal_name;
extern const NSString *oal_name_lang;
extern const NSString *oal_code;
extern const NSString *oal_desc;
extern const NSString *oal_desc_lang;
extern const NSString *oal_hotel_id;
extern const NSString *oal_last_modified;
@end

@interface TableOtherActivityAssignment : NSObject
extern const NSString *oaa_id;
extern const NSString *oaa_activity_id;
extern const NSString *oaa_activities_id;
extern const NSString *oaa_location_id;
extern const NSString *oaa_status_id;
extern const NSString *oaa_remark;
extern const NSString *oaa_duration;
extern const NSString *oaa_reminder;
extern const NSString *oaa_remind;
extern const NSString *oaa_assign_date;
extern const NSString *oaa_start_time;
extern const NSString *oaa_end_time;
extern const NSString *oaa_user_start_time;
extern const NSString *oaa_user_end_time;
extern const NSString *oaa_user_stop_time;
extern const NSString *oaa_decline_service_time;
extern const NSString *oaa_duration_spent;
extern const NSString *oaa_local_duration;
extern const NSString *oaa_user_id;
extern const NSString *oaa_is_single;
extern const NSString *oaa_deleted;
extern const NSString *oaa_last_modified;
@end

@interface TableOtherActivityRecord : NSObject
extern const NSString *oar_id;
extern const NSString *oar_onday_activity_id;
extern const NSString *oar_user_id;
extern const NSString *oar_operation;
extern const NSString *oar_duration;
extern const NSString *oar_time;
extern const NSString *oar_remark;
extern const NSString *oar_post_status;
extern const NSString *oar_last_modified;
@end
