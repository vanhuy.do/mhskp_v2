//
//  DbDefinesV2.m
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DbDefinesV2.h"

@implementation DbDefinesV2

const NSString *DATABASE_NAME = @"mhousekeeping.db";
const NSString *DATABASE_NAME_DEMO = @"mhousekeeping_demo.db";
const int DATABASE_VERSION = 1;
const NSString *PANIC_RECEIVERS = @"panic_receivers";
const NSString *USER = @"user";
const NSString *USER_LOGIN_LOGS = @"user_login_log";
const NSString *USER_CONFIGURATIONS = @"users_configurations";
const NSString *HOUSE_KEEPER_RECORDS = @"house_keeper_record";
const NSString *ROOM_ASSIGNMENT = @"room_assignment";
const NSString *SUPERVISOR_FILTER_DETAIL = @"supervisor_filter_detail";
const NSString *ROOM = @"ROOM";
const NSString *ROOM_BLOCKING = @"room_blocking";
const NSString *LOCATION = @"location";
const NSString *HOTELS = @"HOTEL";
const NSString *ROOM_STATUS = @"room_status";
const NSString *GUEST_INFOS = @"guest_info";
const NSString *PROFILE_NOTE = @"profile_note";
const NSString *PROFILE_NOTE_TYPE = @"profile_note_type";
const NSString *CLEANING_STATUS = @"CLEANING_STATUS";
const NSString *ROOM_RECORDS = @"room_records";
const NSString *ROOM_RECORD_HISTORY = @"room_record_history";
const NSString *ROOM_RECORDS_DETAIL = @"room_record_detail";
const NSString *LAUNDRYS = @"laundrys";
const NSString *LAUNDRY_ORDERS = @"laundry_orders";
const NSString *LAUNDRY_TYPES = @"laundry_types";
const NSString *LAUNDRY_CATEGORIES=@"laundry_categories";
const NSString *LAUNDRY_ITEMS=@"laundry_items";
const NSString *COUNT_CATEGORIES = @"count_categories";
const NSString *COUNT_MORE_INFOR = @"count_more_infor";
const NSString *COUNT_SERVICE = @"count_service";
const NSString *COUNT_ITEMS = @"count_items";
const NSString *COUNT_ITEM_TYPES = @"count_item_types";
const NSString *COUNT_ORDERS = @"count_orders";
const NSString *COUNT_ORDER_DETAILS = @"count_order_details";
const NSString *LANGUAGE_REFERENCE = @"language_references";
const NSString *SETTINGS = @"SETTINGS";
const NSString *GUIDELINES = @"guides";
const NSString *GUIDE_ITEMS = @"guide_items";
const NSString *GUIDE_ITEM_DETAIL = @"guide_item_detail";
const NSString *MESSAGE = @"MESSAGES";
const NSString *CHECKLIST = @"checklist";
const NSString *CHECKLIST_ITEM = @"checklist_item";
const NSString *CHECKLIST_DETAILS = @"CHECKLIST_DETAILS";
const NSString *CHECKLIST_CATEGORIES = @"checklist_categories";
const NSString *CHECKLIST_FORM = @"checklist_form";
const NSString *CHECKLIST_ITEM_CORE = @"checklist_item_core";
const NSString *CHECKLIST_FORM_SCORE = @"checklist_form_score";
const NSString *CHECKLIST_ROOMTYPE = @"checklist_roomtype";
const NSString *engineering_case=@"engineering_case";
const NSString *engineering_case_detail=@"engineering_case_detail";
const NSString *engineering_images=@"engineering_images";
const NSString *engineering_items=@"engineering_items";
const NSString *engineering_categories=@"enginerring_categories";
const NSString *ROOM_REMARK = @"room_remark";
const NSString *LAF_CATIGORIES=@"laf_categories";
const NSString *LAF_ITEMS=@"laf_items";
const NSString *LAF_IMAGES=@"laf_images";
const NSString *LOSTANDFOUND=@"lost_and_found";
const NSString *LOSTANDFOUND_DETAIL=@"lost_and_found_detail";
const NSString *ROOM_SERVICE_LATER = @"room_service_later";
const NSString *USER_LIST=@"user_list";
const NSString *USER_DETAILS=@"user_details";
const NSString *SUBJECT_MESSAGE_ITEMS=@"subject_message_items";
const NSString *SUBJECT_MESSAGE_CATEGORIES=@"subject_message_categories";
const NSString *MESSAGE_TABLE=@"message";
const NSString *MESSAGE_PHOTOS=@"message_photos";
const NSString *LAF_COLORS=@"laf_color"; 
const NSString * LAUNDRY_ITEMS_PRICE=@"laundry_item_price";
const NSString * LAUNDRY_SERVICE=@"laundry_service";
const NSString * LAUNDRY_ORDER_DETAILS=@"laundry_order_details";
const NSString * LAUNDRY_ORDER=@"laundry_orders";
const NSString * LAUNDRY_INSTRUCTIONS=@"laundry_instructions";
const NSString * LAUNDRY_INSTRUCTIONS_ORDER=@"laundry_instructions_order";
const NSString * LAUNDRY_ITEMS_TEMP=@"laundry_order_details_temp";
const NSString * MESSAGE_RECEIVER=@"message_receiver";
const NSString *FLOOR = @"floor";
const NSString *ZONE= @"zone";
const NSString *AMENITIES_CATEGORIES = @"amenities_categories";
const NSString *AMENITIES_ITEM_PRICE = @"amenities_item_price";
const NSString *AMENITIES_ITEMS = @"amenities_items";
const NSString *AMENITIES_ORDER_DETAILS = @"amenities_order_details";
const NSString *AMENITIES_ORDERS = @"amenities_orders";
const NSString *AMENITIES_SERVICE = @"amenities_service";
const NSString *ROOM_TYPE = @"room_type";
const NSString *AMENITIES_ITEMS_ORDER_TEMP = @"amenities_items_order_temp";
const NSString *INSPECTION_STATUS = @"inspection_status";
const NSString *UNASSIGN_ROOM = @"unassign_room";
const NSString *ASSIGN_ROOM = @"assign_room";
const NSString *LANGUAGE_REFERENCES = @"language_references";
const NSString *REASSIGN_ROOM_ASSIGNMENT = @"reassign_room_assignment";

//ADDITIONAL JOB TABLE DEFINITIONS
const NSString *ADDJOB_ITEM = @"addjob_item";
const NSString *ADDJOB_STATUS = @"addjob_status";
const NSString *ADDJOB_DETAIL = @"addjob_detail";
const NSString *ADDJOB_DETAIL_RECORD = @"addjob_detail_record";
const NSString *ADDJOB_FLOOR = @"addjob_floor";
const NSString *ADDJOB_ROOM = @"addjob_room";
const NSString *ADDJOB_SEARCH = @"addjob_search";
const NSString *ADDJOB_CATEGORY = @"addjob_category";
const NSString *ADDJOB_ROOM_ITEM = @"addjob_room_item";
const NSString *ADDJOB_SEARCH_ROOM_CATEGORY = @"addjob_search_room_category";
const NSString *ADDJOB_GUEST_INFO = @"addjob_guest_info";

////update code---------history
const NSString *PHYSICAL_HISTORY = @"physical_history";
const NSString *COUNT_HISTORY = @"count_history";
const NSString *ENGINEERING_HISTORY = @"engineering_history";
const NSString *LOST_FOUND_HISTORY = @"laf_history";

const NSString *AMENITIESPOSTING = @"amenities_posting";

const NSString *ROOMTYPE_INVENTORY = @"roomtype_inventory";

const NSString *OTHER_ACTIVITY = @"other_activity";
const NSString *OTHER_ACTIVITY_STATUS = @"other_activity_status";
const NSString *OTHER_ACTIVITY_LOCATION = @"other_activity_location";
const NSString *OTHER_ACTIVITY_ASSIGNMENT = @"other_activity_assignment";
const NSString *OTHER_ACTIVITY_RECORD = @"other_activity_record";

@end



@implementation TableChecklistRoomType {
    
}

const  NSString
*clrtUserId = @"userId",
*clrtRoomTypeID = @"clrtRoomTypeID",
*clrtChecklistID = @"clrtChecklistID",
*clrtLastModified = @"lastModified";

@end

@implementation TablePanicReceivers {
    
}

const NSString *panic_receivers_owner_id = @"panic_receivers_owner_id",
*panic_receivers_receiver_id = @"panic_receivers_receiver_id",
*panic_receivers_phone_number = @"panic_receivers_phone_number",
*panic_receivers_email = @"panic_receivers_email",
*panic_receivers_receiver_name = @"panic_receivers_receiver_name",
*panic_receivers_is_direct_call = @"panic_receivers_is_direct_call";

@end

@implementation TableProfileNote {
    
}

const NSString *profilenote_description = @"profilenote_description",
*profilenote_last_modified = @"profilenote_last_modified",
*profilenote_user_id = @"profilenote_user_id",
*profilenote_room_number = @"profilenote_room_number",
*profilenote_guest_id = @"profilenote_guest_id";

@end

@implementation TableProfileNoteType {
    
}

const NSString *profilenote_type_code = @"profilenote_type_code",
*profilenote_type_description = @"profilenote_type_description",
*profilenote_type_id = @"profilenote_type_id",
*profilenote_type_last_modified = @"profilenote_type_last_modified",
*profilenote_type_roomnumber = @"profilenote_type_roomnumber",
*profilenote_type_user_id = @"profilenote_type_user_id",
*profilenote_type_guest_id = @"profilenote_type_guest_id";

@end

@implementation TableAddJobGuestInfo {
    
}

const NSString *addjob_guestinfo_room_id = @"addjob_guestinfo_room_id",
*addjob_guestinfo_vip = @"addjob_guestinfo_vip",
*addjob_guestinfo_user_id = @"addjob_guestinfo_user_id",
*addjob_guestinfo_guest_title = @"addjob_guestinfo_guest_title",
*addjob_guestinfo_guest_name = @"addjob_guestinfo_guest_name",
*addjob_guestinfo_lang_pref = @"addjob_guestinfo_lang_pref",
*addjob_guestinfo_check_in_date = @"addjob_guestinfo_check_in_date",
*addjob_guestinfo_check_out_date = @"addjob_guestinfo_check_out_date",
*addjob_guestinfo_pref_desc = @"addjob_guestinfo_pref_desc",
*addjob_guestinfo_last_modified = @"addjob_guestinfo_last_modified",
*addjob_guestinfo_housekeeping_name = @"addjob_guestinfo_housekeeping_name",
*addjob_guestinfo_special_service = @"addjob_guestinfo_special_service",
*addjob_guestinfo_guest_number = @"addjob_guestinfo_guest_number",
*addjob_guestinfo_photo = @"addjob_guestinfo_photo",
*addjob_guestinfo_post_status = @"addjob_guestinfo_post_status";

@end

@implementation TableAddJobDetailRecord {
    
}

const NSString *addjob_detail_record_onday_addjob_id = @"addjob_detail_record_onday_addjob_id",
*addjob_detail_record_post_status = @"addjob_detail_record_post_status",
*addjob_detail_record_user_id = @"addjob_detail_record_user_id",
*addjob_detail_record_operation = @"addjob_detail_record_operation",
*addjob_detail_record_time = @"addjob_detail_record_time",
*addjob_detail_record_last_modified = @"addjob_detail_record_last_modified";

@end


@implementation TableAddJobSearchRoomCategory {
    
}

const NSString *addjob_searchroomcategory_search_id = @"addjob_searchroomcategory_search_id",
*addjob_searchroomcategory_room_id = @"addjob_searchroomcategory_room_id",
*addjob_searchroomcategory_category_id = @"addjob_searchroomcategory_category_id",
*addjob_searchroomcategory_user_id = @"addjob_searchroomcategory_user_id",
*addjob_searchroomcategory_last_modified = @"addjob_searchroomcategory_last_modified";

@end


@implementation TableAddJobRoomItem {
    
}

const NSString  *addjob_roomitem_onday_addjob_id = @"addjob_roomitem_onday_addjob_id",
*addjob_roomitem_item_id = @"addjob_roomitem_item_id",
*addjob_roomitem_last_modified = @"addjob_roomitem_last_modified",
*addjob_roomitem_user_id = @"addjob_roomitem_user_id";

@end

@implementation TableAddJobCategory {
    
}

const NSString  *addjob_category_id = @"addjob_category_id",
*addjob_category_name = @"addjob_category_name",
*addjob_category_name_lang = @"addjob_category_name_lang",
*addjob_category_name_lang2 = @"addjob_category_name_lang2",
*addjob_category_last_modified = @"addjob_category_last_modified",
*addjob_category_user_id = @"addjob_category_user_id";

@end

@implementation TableAddJobDetail {
    
}

const NSString  *addjob_detail_onday_addjob_id = @"addjob_detail_onday_addjob_id",
*addjob_detail_room_id = @"addjob_detail_room_id",
*addjob_detail_expected_cleaning_time = @"addjob_detail_expected_cleaning_time",
*addjob_detail_room_assign_id = @"addjob_detail_room_assign_id",
*addjob_detail_category_id = @"addjob_detail_category_id",
*addjob_detail_status = @"addjob_detail_status",
*addjob_detail_post_status = @"addjob_detail_post_status",
*addjob_detail_user_id = @"addjob_detail_user_id",
*addjob_detail_room_number = @"addjob_detail_room_number",
*addjob_detail_total_time = @"addjob_detail_total_time",
*addjob_detail_start_time = @"addjob_detail_start_time",
*addjob_detail_stop_time = @"addjob_detail_stop_time",
*addjob_detail_last_modified = @"addjob_detail_last_modified",
*addjob_detail_remark = @"addjob_detail_remark",
*addjob_detail_assigned_date = @"addjob_detail_assigned_date";

@end

@implementation TableAddJobRoom  {
    
}

const NSString  *addjob_room_id = @"addjob_room_id",
*addjob_room_assign_id = @"addjob_room_assign_id",
*addjob_room_number = @"addjob_room_number",
*addjob_room_status_id = @"addjob_room_status_id",
*addjob_room_floor_id = @"addjob_room_floor_id",
*addjob_room_type_id = @"addjob_room_type_id",
*addjob_room_number_pending_job = @"addjob_room_number_pending_job",
*addjob_room_search_id = @"addjob_room_search_id",
*addjob_room_is_due_out = @"addjob_room_is_due_out",
*addjob_room_remark = @"addjob_room_remark",
*addjob_room_last_modified = @"addjob_room_last_modified",
*addjob_room_user_id = @"addjob_room_user_id";

@end

@implementation TableAddJobStatus {
    
}

const  NSString  *addjob_status_id = @"addjob_status_id",
*addjob_status_name = @"addjob_status_name",
*addjob_status_lang = @"addjob_status_lang",
*addjob_status_icon = @"addjob_status_icon",
*addjob_status_last_modified = @"addjob_status_last_modified";

@end

@implementation TableAddJobItem  {
    
}

const NSString  *addjob_item_id = @"addjob_item_id",
*addjob_item_user_id = @"addjob_item_user_id",
*addjob_item_onday_addjob_id = @"addjob_item_onday_addjob_id",
*addjob_item_name = @"addjob_item_name",
*addjob_item_name_lang = @"addjob_item_name_lang",
*addjob_item_name_lang2 = @"addjob_item_name_lang2",
*addjob_item_last_modified = @"addjob_item_last_modified";

@end

@implementation TableAddJobFloor  {
    
}

const  NSString *addjob_floor_id = @"addjob_floor_id";
const  NSString *addjob_floor_name = @"addjob_floor_name";
const  NSString *addjob_floor_name_lang = @"addjob_floor_name_lang";
const  NSString *addjob_floor_search_id = @"addjob_search_id";
const  NSString *addjob_floor_last_modified = @"addjob_floor_last_modified";
const  NSString *addjob_floor_user_id = @"addjob_floor_user_id";
const  NSString *addjob_floor_number_of_job = @"addjob_floor_number_of_job";

@end

@implementation TableAddJobSearch  {
    
}

const  NSString *addjob_search_id = @"addjob_search_id";
const  NSString *addjob_search_user_id = @"addjob_search_user_id";
const  NSString *addjob_search_items_id = @"addjob_search_tasks_id";
const  NSString *addjob_search_last_modified = @"addjob_search_last_modified";

@end

@implementation TableMessageReceiver
const  NSString *message_receiver_owner_id = @"message_receiver_owner_id",
*message_receiver_id= @"message_receiver_id",
*message_receiver_group_id= @"message_receiver_group_id",
*message_receiver_last_modified = @"message_receiver_last_modified";

@end

//Implement Table User
@implementation TableUser
const  NSString *user_id = @"usr_id";
const  NSString *user_name = @"usr_name";
const  NSString *user_password = @"usr_password";
const  NSString *user_role = @"usr_role";
const  NSString *user_fullname = @"usr_fullname";
const  NSString *user_fullnamelang = @"usr_fullnamelang";
const  NSString *user_language = @"usr_language";
const  NSString *user_title = @"usr_title";
const  NSString *user_hotel_id=@"usr_hotel_id";
const  NSString *user_department = @"usr_department";
const  NSString *user_department_id = @"usr_department_id";
const  NSString *user_supervisor_id = @"usr_supervisor_id";
const  NSString *user_last_modified = @"usr_last_modified";
@end

//Implement Table Log User
@implementation TableLogUser
const  NSString *log_user_id = @"usr_id";
const  NSString *log_user_name = @"usr_name";
const  NSString *log_user_last_login = @"usr_last_login_date";
const  NSString *log_user_last_logout = @"usr_last_logout_date";
@end

//Implement Table User Configuration
@implementation TableUserConfigurations
const  NSString *row_id = @"row_id";
const  NSString *user_config_id = @"user_id";
const  NSString *user_config_key = @"key";
const  NSString *user_config_value = @"value";
const  NSString *last_modified = @"last_modified";
@end

/*
@implementation TableHouseKeeperRecords 
const  NSString *hkp_user_id = @"hkp_user_id";
const  NSString *hkp_total_clean_time = @"hkp_total_clean_time";
const  NSString *hkp_room_clean = @"hkp_room_clean";
const  NSString *hkp_record_date = @"hkp_record_date";
const  NSString *hkp_posted = @"hkp_posted";
@end
*/

@implementation TableRoomAssignment    
const  NSString *ra_id = @"ra_id";
const  NSString *ra_room_id = @"ra_room_id";
const  NSString *ra_user_id = @"ra_user_id";
const  NSString *ra_assigned_date = @"ra_assigned_date";
const  NSString *ra_prioprity = @"ra_prioprity";
const  NSString *ra_post_status = @"ra_post_status";
const  NSString *ra_last_modified = @"ra_last_modified";
const  NSString *ra_priority_sort_order = @"ra_priority_sort_order";
const NSString *ra_housekeeper_id = @"ra_housekeeper_id";
const NSString *ra_guest_arrival_time = @"ra_guest_arrival_time";
const NSString *ra_guest_departure_time = @"ra_guest_departure_time";
const NSString *ra_kind_of_room = @"ra_kind_of_room";
const NSString *ra_is_mock_room = @"ra_is_mock_room";
const NSString *ra_is_reassigned_room = @"ra_is_reassigned_room";
const NSString *ra_last_cleaning_date = @"ra_last_cleaning_date";
const NSString *ra_is_checked_ra_id = @"ra_is_checked_ra_id";
const NSString *ra_zone_id = @"ra_zone_id";
@end

@implementation TableSupervisorFilterDetail : NSObject {
    
}
const  NSString *filter_user_id = @"filter_user_id";
const  NSString *filter_type = @"filter_type";
const  NSString *filter_id = @"filter_id";
const  NSString *filter_name = @"filter_name";
const  NSString *filter_lang = @"filter_lang";
const  NSString *filter_hotel_id = @"filter_hotel_id";
const  NSString *filter_room_number = @"filter_room_number";

@end


@implementation TableRoom 

const  NSString *room_id = @"room_id";
const  NSString *room_hotel_id = @"room_hotel_id";
const  NSString *room_status_id = @"room_status_id" ;
const  NSString *room_type_id = @"room_type_id";
const  NSString *room_lang = @"room_lang";
const  NSString *room_building_name=@"room_building_name";

const  NSString *room_guest_name = @"room_guest_name";
const  NSString *room_building_namelang = @"room_building_namelang";
const  NSString *room_vip_status = @"room_vip_status" ;
const  NSString *room_cleaning_status_id = @"room_cleaning_status_id";
const  NSString *room_expected_clean_time = @"room_expected_clean_time";
const  NSString *room_guest_reference = @"room_guest_reference";
const  NSString *room_additional_job = @"room_additional_job";
const  NSString *room_post_status = @"room_post_status";
const  NSString *room_last_modified = @"room_last_modified";

const NSString *room_expected_inspection_time = @"room_expected_inspection_time";
const NSString *room_is_re_cleaning = @"room_is_re_cleaning";
const NSString *room_expected_status_id = @"room_expected_status_id";
const NSString *room_zone_id = @"room_zone_id";
const NSString *room_inspection_status_id = @"room_inspection_status_id";

const  NSString *room_location_code = @"room_location_id";

const  NSString *room_floor_id = @"room_floor_id";
@end

@implementation TableRoomBlocking

const NSString
*roomblocking_is_blocked = @"roomblocking_is_blocked",
*roomblocking_user_id = @"roomblocking_user_id",
*roomblocking_room_number = @"roomblocking_room_number",
*roomblocking_remark_physical_check  = @"roomblocking_remark_physical_check",
*roomblocking_reason  = @"roomblocking_reason",
*roomblocking_oosdurations = @"roomblocking_oosdurations";
@end

@implementation TableLocation 

const  NSString *location_id = @"location_id";
const  NSString *location_hotel_id = @"location_hotel_id";
const  NSString *location_code = @"location_code";
const  NSString *location_desc = @"location_desc";
const  NSString *location_lang = @"location_lang";
const  NSString *location_roomNo = @"location_roomNo";
const  NSString *location_type = @"location_type";
const  NSString *location_lastModified = @"location_lastModified";
@end

@implementation TableHotels 

const  NSString *hotel_id = @"hotel_id";
const  NSString *hotel_lang = @"hotel_lang";
const  NSString *hotel_name_lang = @"hotel_name_lang";
const  NSString *hotel_logo = @"hotel_logo";
const  NSString *hotel_last_modified = @"hotel_last_modified";

@end

@implementation TableRoomStatus 

const  NSString *rstat_id = @"rstat_id";
const  NSString *rstat_name = @"rstat_name";
const  NSString *rstat_code = @"rstat_code";
const  NSString *rstat_image = @"rstat_image" ;
const  NSString *rstat_lang = @"rstat_lang";
const  NSString *rstat_last_modified = @"rstat_last_modified";
const  NSString *rstat_physical_check = @"rstat_physical_check";
const  NSString *rstat_find_status = @"rstat_find_status";

/*
 CREATE TABLE 'room_status' (
 'rstat_id' INT NULL DEFAULT NULL ,
 'rstat_name' TEXT NULL DEFAULT NULL ,
 'rstat_code' TEXT NULL DEFAULT NULL ,
 'rstat_lang' TEXT NULL DEFAULT NULL ,
 'rstat_last_modified' TEXT NULL DEFAULT NULL , "rstat_image" BLOB,
 PRIMARY KEY ('rstat_id', 'rstat_name') )
 */

@end

//Implement Table Guest Info
@implementation TableGuestInfos

const  NSString *guest_id =@"guest_id";
const  NSString *guest_name =@"guest_name";
const  NSString *guest_image =@"guest_image";
const  NSString *guest_lang_pref =@"guest_lang_pref";
const  NSString *guest_room_id =@"guest_room_id";
const  NSString *guest_check_in_time =@"guest_check_in_time";
const  NSString *guest_check_out_time =@"guest_check_out_time";
const  NSString *guest_post_status =@"guest_post_status";
const  NSString *guest_last_modified =@"guest_last_modified";
const  NSString *guest_preference=@"guest_preference";
const NSString *vip_number = @"vip_number";
const NSString *housekeeper_name = @"housekeeper_name";
const  NSString *guest_number = @"guest_number";
const  NSString *guest_type = @"guest_type";
//const  NSString *guest_pref_desc =@"guest_pref_desc";
const NSString *guest_title = @"guest_title";
const  NSString *guest_special_service = @"guest_special_service";
const  NSString *guest_preference_code = @"guest_preference_code";

@end

@implementation TableCleaningStatus

const  NSString *cstat_id = @"cstat_id";
const  NSString *cstat_name = @"cstat_name";
const  NSString *cstat_lang = @"cstat_lang";
const  NSString *cstat_image = @"cstat_image";
const  NSString *cstat_last_modified = @"cstat_last_modified";


@end

//Implement Table Laundry
@implementation TableLaundry

const  NSString *ldry_id =@"ldry_id";
const  NSString *ldry_name =@"ldry_name";
const  NSString *ldry_unit_price = @"ldry_unit_price";
const  NSString *ldry_type_id = @"ldry_type_id";
const  NSString *ldry_lang = @"ldry_lang";
const NSString *ldry_last_modified = @"ldry_last_modified";
//const NSString  *ldry_service_id=@"ldry_service_id";

@end

@implementation TableLaundryCategory

const  NSString *ldryca_id=@"ldryca_id" ;
const  NSString *ldryca_name=@"ldryca_name" ;
const  NSString *ldryca_name_lang=@"ldryca_name_lang" ;
const  NSString *ldryca_image=@"ldryca_image" ;
const  NSString *ldryca_last_modified=@"ldryca_last_modified";

@end 

@implementation TableLaundryItem

const  NSString *ldry_Id =@"ldry_id";
const  NSString *ldry_Name=@"ldry_name" ;
const  NSString  *ldry_Lang=@"ldry_lang" ;
const  NSString *ldry_image=@"ldry_image";
const  NSString *ldry_male_or_female=@"ldry_male_or_female";
const  NSString *ldry_item_last_modifier=@"ldry_last_modified";
@end

@implementation TableLaundryOrders1 
const  NSString *lo_id = @"lo_id";
const  NSString *lo_room_id = @"lo_room_id";
const  NSString *lo_user_id = @"lo_user_id";
const  NSString *lo_status = @"lo_status";
const  NSString *lo_remark = @"lo_remark";
const  NSString *lo_collect_date = @"lo_collect_date";

@end
@implementation TableLaundryOrders 
const  NSString *sldry_id = @"lo_id";
const  NSString *sldry_room_id = @"lo_room_id";
const  NSString *sldry_user_id = @"lo_user_id";
const  NSString *sldry_status = @"lo_status";
const  NSString *sldry_remark = @"lo_remark";
const  NSString *sldry_collect_date = @"lo_collect_date";
const  NSString *sldry_item_id=@"sldry_item_id";
const  NSString *sldry_quantity=@"sldry_quantity";
const  NSString *sldry_post_status=@"sldry_post_status";

@end

@implementation TableLaundryTypes
const  NSString *tldry_id = @"tldry_id";
const  NSString *tldry_name = @"tldry_name";
const  NSString *tldry_hotel_id = @"tldry_hotel_id";
const NSString *tldry_last_modified = @"tldry_last_modified";
const  NSString *tldry_lang = @"tldry_lang";
const NSString  *ldry_service_id = @"ldry_service_id";
@end

@implementation TableLaundryOrderDetails
const  NSString *lod_id=@"lod_id";
const  NSString *lod_laundry_order_id=@"lod_laundry_order_id";
const  NSString *lod_item_id=@"lod_item_id";
const  NSString *lod_quantity=@"lod_quantity";
const  NSString *lod_post_status=@"lod_post_status";
const  NSString *lod_service_id=@"lod_service_id";
const  NSString *lod_category_id=@"lod_category_id";
const NSString *lod_price = @"lod_price";

@end

@implementation TableLaundItemPrice 
const  NSString *lip_id=@"lip_id";
const  NSString *lip_item_id=@"lip_item_id";
const  NSString *lip_category_id=@"lip_category_id";
const  NSString *lip_price=@"lip_price";
const  NSString *lip_last_modified=@"lip_last_modified";

@end

@implementation TableLaundryService 
     
const  NSString *lservice_id=@"lservice_id";
const  NSString *lservice_name=@"lservice_name";
const  NSString *lservice_name_lang=@"lservice_name_lang";
const  NSString *lservice_last_modifed=@"lservice_last_modified";
@end

@implementation TableLaundryInstructions 

const  NSString *li_id=@"li_id";
const  NSString *li_name=@"li_name";
const  NSString *li_name_lang=@"li_name_lang";
const  NSString *li_last_modified =@"li_last_modified";
@end

@implementation TableLaundryInstructionsOrder 

const  NSString *lio_id=@"lio_id";
const  NSString *lio_intruction_id=@"lio_instruction_id";
const  NSString *lio_order_id=@"lio_order_id";

@end

//Implement Table Count
@implementation TableCountCategories 
const NSString *cc_id = @"cc_id";
const NSString *cc_name = @"cc_name";
const NSString *cc_hotel_id = @"cc_hotel_id";
const NSString *cc_name_lang = @"cc_name_lang";
const NSString *cc_image = @"cc_image";
const NSString *c_service_id = @"cc_service";
const NSString *cc_last_modified = @"cc_last_modified";
@end

@implementation TableCountItems 
const NSString *count_id = @"count_id";
const NSString *count_name = @"count_name";
const NSString *count_unit_price = @"count_unit_price";
const NSString *count_type_id = @"count_type_id";
const NSString *count_lang = @"count_lang";
const NSString *count_category_id = @"count_category_id";
const NSString *count_image = @"count_image";
const NSString *count_last_modified = @"count_last_modified";

@end

@implementation TableCountOrder
const NSString *scount_id = @"scount_id";
const NSString *scount_room_id = @"scount_room_id";
const NSString *scount_user_id = @"scount_user_id";
const NSString *scount_status = @"scount_status";
const NSString *scount_collect_date = @"scount_collect_date";
const NSString *scount_service_id = @"scount_service_id";
const NSString *scount_room_number = @"scount_room_number";
const NSString *scount_post_status = @"scount_post_status";
const NSString *scount_order_ws_id = @"scount_order_ws_id";
@end

@implementation TableCountOrderDetails 
const NSString *dcount_id = @"dcount_id";
const NSString *dcount_order_id = @"dcount_order_id";
const NSString *dcount_item_id = @"dcount_item_id";
const NSString *dcount_collected = @"dcount_collected";
const NSString *dcount_new = @"dcount_new";
const NSString *dcount_quantity = @"dcount_quantity";
const NSString *dcount_pos_status = @"dcount_pos_status";
const NSString *dcount_collected_date = @"dcount_collected_date";

@end

@implementation TableCountService 
const NSString *cs_id = @"cs_id";
const NSString *cs_kind = @"cs_kind";
const NSString *cs_image = @"cs_image";
const NSString *cs_name = @"cs_name";
const NSString *cs_name_lang = @"cs_name_lang";
@end

@implementation TableCountItemTypes
const NSString *tcount_id = @"tcount_id";
const NSString *tcount_name = @"tcount_name";
const NSString *tcount_chargeable = @"tcount_chargeable";
const NSString *tcount_hotel_id = @"tcount_hotel_id";
const NSString *tcount_lang = @"tcount_lang";

@end

@implementation TableCountMoreInfor
const NSString *cmi_id = @"cmi_id";
const NSString *cmi_category_id = @"cmi_category_id";
const NSString *cmi_regular_or_express = @"cmi_regular_or_express";
const NSString *cmi_male_or_female = @"cmi_male_or_female";

@end

@implementation TableLanguageReference 
const  NSString *langRe_id = @"lang_id";
const  NSString *langRe_name = @"lang_name";
const  NSString *langRe_pack = @"lang_pack";
const  NSString *langRe_active=@"lang_active";
const  NSString *langRe_lastmodified = @"lang_last_modified";
const  NSString *langRe_currencysymbol = @"lang_currency";
const  NSString *langRe_currencynodigitafterdecimal = @"lang_decimal_place";

@end

//Implement Table Setting
@implementation TableSetting 
const NSString *settings_lang=@"settings_lang";
const NSString *settings_sync=@"settings_sync";
const NSString *settings_last_sync=@"settings_last_sync";
const NSString *settings_notification_type=@"settings_notification_type";
const NSString *settings_photo=@"settings_photo";
const NSString *setting_log_enable = @"setting_log_enable";
const NSString *settings_log_amount = @"settings_log_amount";
const NSString *settings_ws_url = @"settings_ws_url";
const NSString *settings_room_validate_enable = @"settings_room_validate_enable";
const NSString *settings_background_mode = @"settings_background_mode"; // CFG [20160927/CRF-00001432]
const NSString *settings_heartbeat_server = @"settings_heartbeat_server"; // CFG [20160927/CRF-00001432]
const NSString *settings_heartbeat_port = @"settings_heartbeat_port"; // CFG [20160927/CRF-00001432]

@end

@implementation TableGuidelines 

const NSString *guide_id = @"guide_id";
const NSString *guide_room_type_id = @"guide_room_type_id";
const NSString *guide_room_section_id = @"guide_room_section";
const NSString *guide_name = @"guide_name";
const NSString *guide_name_lang = @"guide_name_lang";
const NSString *guide_contents = @"guide_contents";
const NSString *guide_content_lang = @"guide_contents_lang";
const NSString *guide_image = @"guide_image";
const NSString *guide_last_updated = @"guide_last_updated";
const NSString *guide_category_id = @"guide_category_id";
@end

@implementation TableMessage 
const  NSString *msg_id=@"msg_id";
const  NSString *msg_sender_id=@"msg_sender_id";
const  NSString *msg_receiver=@"msg_receiver";
const  NSString *msg_topic=@"msg_topic";
const  NSString *msg_contents=@"msg_contents";
const  NSString *msg_read=@"msg_read";
const NSString *msg_user_id=@"msg_user_id";
const NSString *msg_last_modified=@"msg_last_modified";
const NSString *msg_sender_name=@"msg_sender_name";
const NSString *msg_sender_name_lang=@"msg_sender_name_lang";
const NSString *msg_owner_id=@"msg_owner_id";

@end

//Implement Table CheckList
@implementation TableChecklist 

const  NSString *chklist_id = @"chklist_id";
const  NSString *chklist_user_id = @"chklist_user_id";
const  NSString *chklist_room_id = @"chklist_room_id";
const  NSString *chklist_inspect_date = @"chklist_inspect_date";
const  NSString *chklist_status = @"chklist_status";
const  NSString *chklist_post_status = @"chklist_post_status" ;
const  NSString *chklist_core = @"chklist_core";

@end

@implementation TableChecklistCategories

const  NSString *chc_id = @"chc_id";
const  NSString *chc_form_id = @"chc_form_id";
const  NSString *chc_name = @"chc_name";
const  NSString *chc_name_lang = @"chc_name_lang";
const NSString *chc_last_modified = @"chc_last_modified";

@end

@implementation TableChecklistForm 

const  NSString *form_id = @"form_id";
const  NSString *form_name = @"form_name";
const  NSString *form_lang_name = @"form_lang_name";
const  NSString *form_kind = @"form_kind";
const  NSString *form_hotel_id = @"form_hotel_id";
const  NSString *form_possible_point = @"form_possible_point";
const NSString *form_passing_score = @"form_passing_score";
const NSString *form_last_modified = @"form_last_modified";

@end

@implementation TableChecklistFormScore

const NSString *df_id = @"df_id";	//Primary Key
const NSString *df_score = @"df_score";
const NSString *df_post_status = @"df_post_status";
const NSString *df_checklist_id = @"df_checklist_id";
const NSString *df_form_id = @"df_form_id";
const NSString *df_user_id = @"df_user_id";
const NSString *df_is_mandatory_pass = @"df_is_mandatory_pass";

@end

@implementation TableChecklistItem 

const  NSString *chkitem_id = @"chkitem_id";
const  NSString *chkitem_name = @"chkitem_name";
const  NSString *chkitem_lang = @"chkitem_lang";
const  NSString *chkitem_room_type = @"chkitem_room_type";
const  NSString *chkitem_pass_score = @"chkitem_pass_score";
const  NSString *chkitem_last_update = @"chkitem_last_update";
const NSString *chkitem_category_id = @"chkitem_category_id";
const NSString *chkitem_form_id = @"chkitem_form_id";
const NSString *chkitem_mandatory_pass = @"chkitem_mandatory_pass";

@end

@implementation TableChecklistItemCore 

const NSString *cic_id = @"cic_id";	//Primary Key
const NSString *cic_item_id = @"cic_item_id";
const NSString *cic_core = @"cic_core";
const NSString *cic_form_score = @"cic_form_score";
const NSString *cic_post_status = @"cic_post_status";

@end



@implementation TableChecklistDetail

const  NSString *chkdetails_id =@"chkdetails_id";
const  NSString *chkdetails_checklist_id = @"chkdetails_checklist_id";
const  NSString *chkdetails_checklist_item = @"chkdetails_checklist_item";
const  NSString *chkdetails_score = @"chkdetails_score";
const  NSString *chkdetails_comment = @"chkdetails_comment";
const  NSString *chkdetails_post_status = @"chkdetails_post_status";

@end

@implementation TableEngineeringCase 


const  NSString *ec_id=@"ec_id";
const  NSString *ec_category_id =@"ec_category_id";
const  NSString *ec_item_id=@"ec_item_id";
const  NSString *ec_detail_id=@"ec_detail_id";
const  NSString *ec_index=@"ec_index";

@end

@implementation TableEngineeringCaseDetail 
const  NSString *ecd_id=@"ecd_id";
const  NSString *ecd_user_id=@"ecd_user_id" ;
const  NSString *ecd_room_id=@"ecd_room_id";
const  NSString *ecd_pos_status=@"ecd_pos_status";
const  NSString *ecd_date=@"ecd_date";
const  NSString *ecd_remark=@"ecd_remark";

@end

@implementation TableEngineeringImage 

const  NSString *enim_id=@"enim_id";
const  NSString *enim_image=@"enim_image" ;
const  NSString *enim_detail_id=@"enim_detail_id";
const  NSString *enim_case_id=@"enim_case_id";
@end

@implementation TableEngineeringItem 

const  NSString *eni_id=@"eni_id";
const  NSString *eni_name=@"eni_name" ;
const  NSString *eni_name_lang=@"eni_name_lang";
const  NSString *eni_category_id=@"eni_category_id";
const  NSString *eni_last_modified = @"eni_last_modified";
const  NSString *eni_post_id = @"eni_post_id";

@end

@implementation TableEngineeringcategories 

const  NSString *enca_id=@"enca_id";
const  NSString *enca_name=@"enca_name" ;
const  NSString *enca_lang_name=@"enca_lang_name";
const  NSString *enca_image=@"enca_image";
const  NSString *enca_last_modified = @"enca_last_modified";
@end




@implementation TableRoomRecords

const  NSString *rrec_id = @"rrec_id";
//const  NSString *rrec_room_id = @"rrec_room_id";
const  NSString *rrec_user_id = @"rrec_user_id";
const  NSString *rrec_cleaning_start_time = @"rrec_cleaning_start_time";
const  NSString *rrec_cleaning_end_time = @"rrec_cleaning_end_time";
const  NSString *rrec_inspection_start_time = @"rrec_inspection_start_time";
const  NSString *rrec_inspection_end_time = @"rrec_inspection_end_time";
const  NSString *rrec_cleaning_date = @"rrec_cleaning_date";
const  NSString *rrec_pos_status = @"rrec_pos_status";

//Hao Tran - Remove
//const  NSString *rrec_cleaning_duration = @"rrec_cleaning_duration";
//const  NSString *rrec_cleaning_total_pause_time = @"rrec_cleaning_total_pause_time";

//Hao Tran - Add
const  NSString *rrec_last_cleaning_duration = @"rrec_last_cleaning_duration";
const  NSString *rrec_last_inspected_duration = @"rrec_last_inspected_duration";
const  NSString *rrec_total_cleaning_time = @"rrec_total_cleaning_time";
const  NSString *rrec_total_inspected_time = @"rrec_total_inspected_time";
const  NSString *rrec_room_status_id = @"rrec_room_status_id";
const  NSString *rrec_cleaning_status_id = @"rrec_cleaning_status_id";
const  NSString *rrec_cleaning_pause_time = @"rrec_cleaning_pause_time";
@end

@implementation TableRoomRecordDetail

const  NSString *recd_id = @"recd_id";
const  NSString *recd_ra_id = @"recd_ra_id";
const  NSString *recd_user_id = @"recd_user_id";
const  NSString *recd_start_date = @"recd_start_date";
const  NSString *recd_stop_date = @"recd_stop_date";

@end

@implementation TableRoomRemark

const  NSString *rm_id = @"rm_id";
const  NSString *rm_time = @"rm_clean_pause_time";
const  NSString *rm_record_id = @"rm_record_id";
const  NSString *rm_pos_status = @"rm_pos_status";
const  NSString *rm_clean_pause_time = @"rm_clean_pause_time";
const  NSString *rm_content = @"rm_content";
const  NSString *rm_physical_content = @"rm_physical_content";
const  NSString *rm_roomassignment_roomId = @"rm_roomassignment_roomId";
@end

//implement the tableLostandFoundCategories
@implementation  TableLostandFoundCategories 

const  NSString *lafc_id=@"lafc_id";
const  NSString *lafc_name=@"lafc_name";
const  NSString *lafc_lang_name=@"lafc_lang_name";
const  NSString *lafc_image=@"lafc_image";
const  NSString *lafc_last_modified=@"lafc_last_modified";

@end

@implementation TableLostandFoundItems  

const  NSString *lafi_id=@"lafi_id" ;
const  NSString *lafi_name=@"lafi_name" ;
const  NSString *lafi_name_lang=@"lafi_name_lang";
const  NSString *lafi_image=@"lafi_image" ;
const  NSString *lafi_category_id=@"lafi_category_id" ;
const  NSString *lafi_last_modified=@"lafi_last_modified";

@end

@implementation TableLostandFoundImages 

const  NSString *lafim_id=@"lafim_id" ;
const  NSString *lafim_image=@"lafim_image" ;
const  NSString *lafim_lafdetail_id=@"lafim_lafdetail_id";
@end

@implementation TableLostandFound  
/* 'laf_id' INTEGER PRIMARY KEY autoincrement  NOT NULL ,
 'laf_category_id' INTEGER NULL ,
 'laf_item_id' INTEGER NULL ,
 'laf_color' VARCHAR(45) NULL ,
 'laf_lafdetail_id' INTEGER NULL  , "laf_item_quantity" INTEGER)*/
const  NSString *laf_id =@"laf_id" ;
const  NSString *laf_category_id=@"laf_category_id" ;
const  NSString *laf_item_id=@"laf_item_id";
const  NSString *laf_color=@"laf_color_id" ;
const  NSString *laf_lafdetail_id=@"laf_lafdetail_id" ;
const  NSString *laf_item_quantity=@"laf_item_quantity";
@end

@implementation TableLostandFoundDetail  

const  NSString *lafd_id=@"lafd_id" ;
const  NSString *lafd_user_id=@"lafd_user_id" ;
const  NSString *lafd_room_id=@"lafd_room_id";
const  NSString *lafd_pos_status=@"lafd_pos_status" ;
const  NSString *lafd_date=@"lafd_date" ;
const  NSString *lafd_remark=@"lafd_remark" ;
const  NSString *lafd_room_assignment_id=@"lafd_room_assignment_id" ;

@end

@implementation TableRoomServiceLater

const  NSString *rsl_id = @"rsl_id";
const  NSString *rsl_time = @"time";
const  NSString *rsl_record_id = @"record_id";

@end

@implementation TableGuideItems

const NSString *gitem_id = @"gitem_id";
//const NSString *guide_id = @"guide_id";
const NSString *gitem_name = @"gitem_name";
const NSString *gitem_name_lang = @"gitem_name_lang";
const NSString *gitem_image = @"gitem_images";
const NSString *gitem_content = @"gitem_content";
const NSString *gitem_content_lang = @"gitem_content_lang";
const NSString *gitem_section_id = @"gitem_section_id";
const NSString *gitem_last_modified = @"gitem_last_modified";

@end


@implementation TableLostandFound_Color  
const  NSString *laf_color_id=@"laf_color_id";
const  NSString *laf_color_code=@"laf_color_code";
const  NSString *laf_color_name=@"laf_color_name";
const  NSString *laf_color_lang=@"laf_color_lang";
const  NSString *laf_color_lastModified=@"laf_color_lastModified";

@end

@implementation TableMessagePhoto
const  NSString *message_photo_group_id = @"message_photo_group_id",
*message_photo = @"message_photo",
*message_photo_last_modified = @"message_photo_last_modified";
@end

@implementation Table_Message 

const  NSString *message_id = @"message_id",
*message_group_id = @"message_group_id",
*message_owner_id = @"message_owner_id",
*message_kind = @"message_kind",
*message_count_photo_attached = @"message_count_photo_attached",
*message_status = @"message_status",
*message_from_user_login_name = @"message_from_user_login_name",
*message_from_user_name = @"message_from_user_name",
*message_topic = @"message_topic",
*message_content = @"message_content",
*message_last_modified = @"message_last_modified";
@end

@implementation  TableSubject_Message_Categories 
const  NSString *smc=@"smc_id";
const  NSString *smc_name=@"smc_name";
const  NSString *smc_name_lang=@"smc_name_lang";
const  NSString *smc_image=@"smc_image";
@end
/*"smi_id" INTEGER PRIMARY KEY  NOT NULL , "smi_name" VARCHAR, "smc_name_lang" VARCHAR, "smi_category_id" INTEGER, "smi_image" BLOB*/
@implementation TableSubject_Message_Items 
const  NSString *smi_id=@"smi_id";
const  NSString *smi_name=@"smi_name";
const  NSString *smi_name_lang=@"smi_name_lang";
const  NSString *smi_category_id=@"smi_category_id";
const  NSString *smi_image=@"smi_image";
@end

@implementation TableUser_List 
const  NSString *usr_id=@"usr_id";
const  NSString *usr_list_id=@"usr_list_id";
const  NSString *usr_name=@"usr_name";
const  NSString *usr_hotel_id=@"usr_hotel_id";
const  NSString *usr_fullname=@"usr_fullname";
const  NSString *user_fullname_lang=@"user_fullnamelang";
const  NSString *usr_last_modified=@"usr_last_modified";
const  NSString *usr_supervisor = @"usr_supervisor";
@end


@implementation tableFloor
const NSString *floor_id=@"floor_id";
const NSString *floor_name=@"floor_name";
const NSString *floor_name_lang=@"floor_name_lang";
const NSString *floor_building_id=@"floor_building_id";
const NSString *floor_last_modified=@"floor_last_modified";
@end


@implementation tableZone
const NSString *zone_id = @"zone_id";
const NSString *zone_name = @"zone_name";
const NSString *zone_name_lang = @"zone_name_lang";
const NSString *zone_floor_id = @"zone_floor_id";
const NSString *zone_last_modified = @"zone_last_modified";
@end

@implementation tableUnassignRoom
@end

@implementation tableAssignRoom
@end


@implementation TableLanguage
const NSString *lang_id = @"lang_id";
const NSString *lang_name = @"lang_name";
const NSString *lang_pack = @"lang_pack";
const NSString *lang_active = @"lang_active";
const NSString *lang_last_modified = @"lang_last_modified";
const NSString *lang_currency = @"lang_currency";
const NSString *lang_decimal_place = @"lang_decimal_place";
@end

//Implement Amenities
@implementation TableAmenitiesCategories

const NSString *amca_id = @"amca_id";	//Primary Key
const NSString *amca_name = @"amca_name";
const NSString *amca_hotel_id = @"amca_hotel_id";
const NSString *amca_name_lang = @"amca_name_lang";
const NSString *amca_image = @"amca_image";
const NSString *amca_last_modified = @"amca_last_modified";

@end

@implementation TableAmenitiesItemPrice

const NSString *amip_id = @"amip_id";	//Primary Key
const NSString *amip_item_id = @"amip_item_id";
const NSString *amip_service_id = @"amip_service_id";
const NSString *amip_price = @"amip_price";

@end

@implementation TableAmenitiesItems

const NSString *item_id = @"item_id";	//Primary Key
const NSString *item_name = @"item_name";
const NSString *item_lang = @"item_lang";
const NSString *item_category_id = @"item_category_id";
const NSString *item_image = @"item_image";
const NSString *item_roomtype_id = @"item_roomtype_id";
const NSString *item_last_modified = @"item_last_modified";

@end

@implementation TableAmenitiesOrderDetails

const NSString *amd_id = @"amd_id";	//Primary Key
const NSString *amd_item_id = @"amd_item_id";
const NSString *amd_new_quantity = @"amd_new_quantity";
const NSString *amd_used_quantity = @"amd_used_quantity";
const NSString *amd_category_id = @"amd_category_id";
const NSString *amd_user_id = @"amd_user_id";
const NSString *amd_room_assign_id = @"amd_room_assign_id";
const NSString *amd_room_number = @"amd_room_number";
const NSString *amd_hotel_id = @"amd_hotel_id";
const NSString *amd_transaction_date_time = @"amd_transaction_date_time";

@end

@implementation TableAmenitiesOrders

const NSString *amen_id = @"amen_id";	//Primary Key
const NSString *amen_room_id = @"amen_room_id";
const NSString *amen_user_id = @"amen_user_id";
const NSString *amen_status = @"amen_status";
const NSString *amen_collect_date = @"amen_collect_date";
const NSString *amen_service = @"amen_service";

@end

@implementation TableAmenitiesService

const NSString *ams_id = @"ams_id";	//Primary Key
const NSString *ams_name = @"ams_name";
const NSString *ams_name_lang = @"ams_name_lang";
const NSString *ams_last_modified = @"ams_last_modified";
const NSString *ams_image = @"ams_image";
@end

@implementation TableAmenitiesItemsOrderTemp

const NSString *item_idtemp = @"item_id";	//Primary Key
const NSString *item_nametemp = @"item_name";
const NSString *item_langtemp = @"item_lang";
const NSString *item_category_idtemp = @"item_category_id";	//Primary Key
const NSString *item_imagetemp = @"item_image";
const NSString *item_roomtype_idtemp = @"item_roomtype_id";	//Primary Key
const NSString *item_last_modifiedtemp = @"item_last_modified";
const NSString *item_quantitytemp = @"item_quantity";

@end

@implementation TableInspectionStatus

const NSString *istat_id = @"istat_id";	//Primary Key
const NSString *istat_name = @"istat_name";
const NSString *istat_image = @"istat_image";
const NSString *istat_lang = @"istat_lang";	//Primary Key

@end

@implementation TableReassignRoomAssignment

const NSString *reassign_user_id = @"reassign_user_id";	//Primary Key
const NSString *reassign_room_id = @"reassign_room_id";
const NSString *reassign_datetime = @"reassign_datetime";
const NSString *reasign_expected_cleaning_time = @"reasign_expected_cleaning_time";
const NSString *reassign_room_status = @"reassign_room_status";
const NSString *reassign_cleaning_status = @"reassign_cleaning_status";
const NSString *reassign_housekeeper_id = @"reassign_housekeeper_id";
const NSString *reassign_room_type = @"reassign_room_type";
const NSString *reassign_floor_id = @"reassign_floor_id";
const NSString *reassign_guest_profile_id = @"reassign_guest_profile_id";
const NSString *reassign_hotel_id = @"reassign_hotel_id";
const NSString *reassign_ra_id = @"reassign_ra_id";	//Primary Key
const NSString *reassign_room_remark = @"reassign_room_remark";
@end

@implementation TableAccessRight

const NSString *accessRight_userId = @"userId";
const NSString *accessRight_RoomAssignment_RoomAssignment = @"roomAssignment_RoomAssignment";
const NSString *accessRight_RoomAssignment_CompletedRoom = @"roomAssignment_CompletedRoom";
const NSString *accessRight_RoomAssignment_ManualUpdateRoomStatus = @"roomAssignment_ManualUpdateRoomStatus";
const NSString *accessRight_RoomAssignment_RestrictionAssignment = @"roomAssignment_RestrictionAssignment";
const NSString *accessRight_RoomAssignment_QRCodeScanner = @"roomAssignment_QRCodeScanner";
const NSString *accessRight_Posting_LostAndFound = @"Posting_LostAndFound";
const NSString *accessRight_Posting_Engineering = @"Posting_Engineering";
const NSString *accessRight_Posting_Laundry = @"Posting_Laundry";
const NSString *accessRight_Posting_Minibar = @"Posting_MiniBar";
const NSString *accessRight_Posting_Linen = @"Posting_Linen";
const NSString *accessRight_Posting_Amenities = @"Posting_Amenities";
const NSString *accessRight_Posting_PhysicalCheck = @"Posting_PhysicalCheck";
const NSString *accessRight_Action_LostAndFound = @"Action_LostAndFound";
const NSString *accessRight_Action_Engineering = @"Action_Engineering";
const NSString *accessRight_Action_Laundry = @"Action_Laundry";
const NSString *accessRight_Action_Minibar = @"Action_MiniBar";
const NSString *accessRight_Action_Linen = @"Action_Linen";
const NSString *accessRight_Action_Amenities = @"Action_Amenities";
const NSString *accessRight_Action_GuideLine = @"Action_GuideLine";
const NSString *accessRight_Action_CheckList = @"Action_CheckList";
const NSString *accessRight_Message = @"Message_Message";
const NSString *accessRight_FindAttendantPendingCompleted = @"FindAttendantPendingCompleted";
const NSString *accessRight_FindRoom = @"FindRoom";
const NSString *accessRight_FindInspection = @"FindInspection";
const NSString *accessRight_Setting = @"Setting";

@end

@implementation TablePhysicalHistory
const NSString *PH_floor_id = @"ph_floor_id";
const NSString *PH_room_id = @"ph_room_id";
const NSString *PH_status = @"ph_status";
const NSString *PH_dnd = @"ph_dnd";
const NSString *PH_date = @"ph_date";
const NSString *PH_user_id = @"ph_user_id";

@end
@implementation TableCountHistory

const NSString *H_count_id = @"count_id";
const NSString *H_room_id = @"room_id";
const NSString *H_count_item_id = @"count_item_id";
const NSString *H_count_collected = @"count_collected";
const NSString *H_count_new = @"count_new";
const NSString *H_count_service = @"count_service";
const NSString *H_count_date = @"count_date";
const NSString *H_count_user_id = @"count_user_id";
@end

@implementation TableEngineeringHistory

const NSString *EH_id = @"eh_id";
const NSString *EH_user_id = @"eh_user_id";
const NSString *EH_room_id = @"eh_room_id";
const NSString *EH_item_id = @"eh_item_id";
const NSString *EH_date = @"eh_date";


@end
@implementation TableLAFHistory

const NSString *LAFH_id = @"lafh_id";
const NSString *LAFH_room_id = @"lafh_room_id";
const NSString *LAFH_item_id = @"lafh_item_id";
const NSString *LAFH_color_id = @"lafh_color_id";
const NSString *LAFH_quantity = @"lafh_quantity";
const NSString *LAFH_user_id = @"lafh_user_id";
const NSString *LAFH_date = @"lafh_date";
@end

@implementation TableAMPosting

const NSString *AP_id = @"ap_id";
const NSString *AP_order_id = @"ap_order_id";
const NSString *AP_item_id = @"ap_item_id";
const NSString *AP_quantity = @"ap_quantity";
const NSString *AP_post_status = @"ap_post_status";
const NSString *AP_date = @"ap_date";

@end

@implementation TableRoomTypeInventory

const NSString *rti_user_id = @"rti_user_id";
const NSString *rti_roomtype_id = @"rti_roomtype_id";
const NSString *rti_item_id = @"rti_item_id";
const NSString *rti_category_id = @"rti_category_id";
const NSString *rti_hotel_id = @"rti_hotel_id";
const NSString *rti_building_id = @"rti_building_id";
const NSString *rti_section_id = @"rti_section_id";
const NSString *rti_service_item = @"rti_service_item";
const NSString *rti_default_quantity = @"rti_default_quantity"; // CFG [20160921/CRF-00001275]

@end

@implementation TableOtherActivity

const NSString *oa_id = @"oa_id";
const NSString *oa_name = @"oa_name";
const NSString *oa_name_lang = @"oa_name_lang";
const NSString *oa_code = @"oa_code";
const NSString *oa_desc = @"oa_desc";
const NSString *oa_desc_lang = @"oa_desc_lang";
const NSString *oa_last_modified = @"oa_last_modified";

@end

@implementation TableOtherActivityStatus

const NSString *oas_id = @"oas_id";
const NSString *oas_name = @"oas_name";
const NSString *oas_lang = @"oas_lang";
const NSString *oas_image = @"oas_image";
const NSString *oas_last_modified = @"oas_last_modified";

@end

@implementation TableOtherActivityLocation

const NSString *oal_id = @"oal_id";
const NSString *oal_name = @"oal_name";
const NSString *oal_name_lang = @"oal_name_lang";
const NSString *oal_code = @"oal_code";
const NSString *oal_desc = @"oal_desc";
const NSString *oal_desc_lang = @"oal_desc_lang";
const NSString *oal_hotel_id = @"oal_hotel_id";
const NSString *oal_last_modified = @"oal_last_modified";

@end

@implementation TableOtherActivityAssignment

const NSString *oaa_id = @"oaa_id";
const NSString *oaa_activity_id = @"oaa_activity_id";
const NSString *oaa_activities_id = @"oaa_activities_id";
const NSString *oaa_location_id = @"oaa_location_id";
const NSString *oaa_status_id = @"oaa_status_id";
const NSString *oaa_remark = @"oaa_remark";
const NSString *oaa_duration = @"oaa_duration";
const NSString *oaa_reminder = @"oaa_reminder";
const NSString *oaa_remind = @"oaa_remind";
const NSString *oaa_assign_date = @"oaa_assign_date";
const NSString *oaa_start_time = @"oaa_start_time";
const NSString *oaa_end_time = @"oaa_end_time";
const NSString *oaa_user_start_time = @"oaa_user_start_time";
const NSString *oaa_user_end_time = @"oaa_user_end_time";
const NSString *oaa_user_stop_time = @"oaa_user_stop_time";
const NSString *oaa_decline_service_time = @"oaa_decline_service_time";
const NSString *oaa_duration_spent = @"oaa_duration_spent";
const NSString *oaa_local_duration = @"oaa_local_duration";
const NSString *oaa_user_id = @"oaa_user_id";
const NSString *oaa_is_single = @"oaa_is_single";
const NSString *oaa_deleted = @"oaa_deleted";
const NSString *oaa_last_modified = @"oaa_last_modified";

@end

@implementation TableOtherActivityRecord

const NSString *oar_id = @"oar_id";
const NSString *oar_onday_activity_id = @"oar_onday_activity_id";
const NSString *oar_user_id = @"oar_user_id";
const NSString *oar_operation = @"oar_operation";
const NSString *oar_duration = @"oar_duration";
const NSString *oar_time = @"oar_time";
const NSString *oar_remark = @"oar_remark";
const NSString *oar_post_status = @"oar_post_status";
const NSString *oar_last_modified = @"oar_last_modified";

@end
