//
//  ServiceDefines.h
//  eHouseKeeping
//
//  Created by KhanhNguyen on 6/9/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

//#define eHouseKeepingDefaultWebService @"http://220.231.89.163/Conrad-WS/Ehousekeepingservice.asmx"
//#define eHouseKeepingDefaultWebService @"http://220.231.89.163/Sheraton-WS/eHousekeepingService.asmx"
//#define eHouseKeepingDefaultWebService @"http://220.231.89.163/SheratonTower-WS/eHousekeepingService.asmx"
//#define eHouseKeepingDefaultWebService @"http://54.251.105.155/eHousekeeping/WebServices/eHousekeepingService.asmx" //Podium
//#define eHouseKeepingDefaultWebService @"http://120.72.81.77/eHousekeepingService-New/eHousekeepingService.asmx?WSDL"
//#define eHouseKeepingDefaultWebService @"http://120.72.81.77/New-WS/eHousekeepingService.asmx" //New-WS public
//#define eHouseKeepingDefaultWebService @"http://10.1.0.31/New-WS/eHousekeepingService.asmx" //New-WS local
//#define eHouseKeepingDefaultWebService @"http://mingaik.tan.fcssolution.com/eHousekeepingservice_v2.2/eHousekeepingService.asmx"
//#define eHouseKeepingDefaultWebService @"http://hitec.fcssolution.com/eHousekeepingService_v2.2/eHousekeepingService.asmx" //For deploy app store
//#define eHouseKeepingDefaultWebService @"http://120.72.81.77/MultipleProperty-WS/eHousekeepingService.asmx"
//#define eHouseKeepingDefaultWebService @"http://120.72.81.77/WebService_CRF807/eHousekeepingService.asmx"
//#define eHouseKeepingDefaultWebService @"http://120.72.81.77/Sheraton-WS/eHousekeepingService.asmx"
//#define eHouseKeepingDefaultWebService @"http://120.72.81.77/WebService_CRF807/eHousekeepingService.asmx"
//#define eHouseKeepingDefaultWebService @"https://120.72.81.77/WS-RC-20150112/eHousekeepingService.asmx" //for testing RC
//#define eHouseKeepingDefaultWebService @"http://61.8.247.157/fcses/ehservice/eHousekeepingService.asmx" //for testing Fullerton
//#define eHouseKeepingDefaultWebService @"https://10.193.17.27/EHService/eHousekeepingService.asmx" //for Deploy RC
//#define eHouseKeepingDefaultWebService @"http://192.168.1.102/fcses/ehservice/eHousekeepingService.asmx"
//#define eHouseKeepingDefaultWebService @"http://59.154.146.18:8080/FCSES/EHService/eHousekeepingService.asmx" //Testing Australia deployment
//#define eHouseKeepingDefaultWebService @"https://202.175.59.190/FCSES/EHService/eHousekeepingService.asmx" //Testing Australia deployment
//#define eHouseKeepingDefaultWebService @"https://120.72.81.77/WS-RC-20150112/eHousekeepingService.asmx"
//#define eHouseKeepingDefaultWebService @"http://120.72.81.77/EHService/eHousekeepingService.asmx"
//#define eHouseKeepingDefaultWebService @"http://120.72.81.77/eHPatch_v2.2-17.2.88_EHService/eHousekeepingService.asmx" //WS - CRF-1036
//#define eHouseKeepingDefaultWebService @"http://andrie.jumari.fcssolution.com/ehousekeepingservice/ehousekeepingservice.asmx"
//#define eHouseKeepingDefaultWebService @"http://192.168.38.254/FCSES/EHService/eHousekeepingService.asmx"
//#define eHouseKeepingDefaultWebService @"http://10.1.0.31/EHService/eHousekeepingService.asmx"
//#define eHouseKeepingDefaultWebService @"http://10.8.3.52/FCSNET/FCSES/v2.x/eHousekeepingService/eHousekeepingService.asmx"

// Default
//#define eHouseKeepingDefaultWebService @"http://localhost/FCSES/EHService/eHousekeepingService.asmx"

// Percipia
//#define eHouseKeepingDefaultWebService @"http://localhost/STAR/AttendantService/WebService.asmx"
//HK server
//#define eHouseKeepingDefaultWebService @"http://210.184.6.233/eHousekeepingService/eHousekeepingService.asmx"
//VN
//#define eHouseKeepingDefaultWebService @"http://14.161.36.97:1799/EHService/eHousekeepingService.asmx"
//#define eHouseKeepingDefaultWebService @"http://192.168.14.112/EHService2/eHousekeepingService.asmx"
//#define eHouseKeepingDefaultWebService @"http://14.161.36.97:81/EHService2/eHousekeepingService.asmx"

#define eHouseKeepingDefaultWebService @"http://PRBHEVHKSAP03.evt.wynnresorts.com/FCSES/EHService/eHousekeepingService.asmx"

//#define eHouseKeepingDefaultWebService @"http://172.16.11.89/FCSES/EHService/eHousekeepingService.asmx"

//#define eHouseKeepingDefaultWebService @"http://10.8.2.66/FCSES/EHService/eHousekeepingService.asmx"

// Testing
//#define eHouseKeepingDefaultWebService @"http://10.8.2.101/TFS/e-Housekeeping_v2/Trunk/eHousekeepingService/eHousekeepingService.asmx"
//#define eHouseKeepingDefaultWebService @"http://10.8.2.66/FCSES/EHService/eHousekeepingService.asmx"

#define DEFAULT_ENABLE_PTT 0 //Set PTT Module 1 = Show or 0 = Hide
#define DEFAULT_ENABLE_WS_LOG 0 //Set Enable WS 1 = on or 0 = OFF
#define DEFAULT_SYNC_SETTING 2 //Set Sync Setting 1 = Manual or 2 = Auto
#define DEFAULT_ROOM_VALIDATION YES //Set Room Validation YES = Enable or NO = Disable
#define DEFAULT_LOCAL_LOG NO //Set Local Log YES = Enable or NO = Disable
#define DEFAULT_RETRY_TIMES 2
#define EMAIL_SUPPORT @"fcsdevvn@gmail.com"
#define LOGIN_TIMEOUT_CHECK 2 // seconds

#define DEFAULT_ENABLE_3G  1 // 1 = on 0 = off
#define DEFAULT_ENABLE_WIFI  1 // 1 = on 0 = off

// CFG [20160927/CRF-00001432] - Moved to settings table in the database.
//#define BACKGROUND_MODE 1 // 0 = Disabled, 1 = Enabled
//#define HEARTBEAT_SERVER @"210.184.6.233" // Server IP
//#define HEARTBEAT_PORT 2002 // 0 = Disabled, >0 = Port number
// CFG [20160927/CRF-00001432] - End.

@interface ServiceDefines : NSObject {
    
}
extern int const RESPONSE_STATUS_NO_NETWORK;
extern int const RESPONSE_STATUS_OK;
extern int const RESPONSE_STATUS_NO_RERCORD_TO_DISPLAY;
extern int const RESPONSE_STATUS_ERROR;
extern int const RESPONSE_STATUS_ERROR_IN_UPDATING_RECORD;
extern int const RESPONSE_STATUS_ERROR_IN_INSERTING_RECORD;
extern int const RESPONSE_STATUS_NEW_RECORD_ADDED;
extern int const RESPONSE_STATUS_USER_SUCCESSFULLY_AUTHENTICATED;
extern int const RESPONSE_STATUS_USER_SUCCESSFULLY_AUTHENTICATED_NO_USER_DETAIL_RETURNED;
extern int const RESPONSE_STATUS_INVALID_USERNAME_OR_PASSWORD;
extern int const RESPONSE_STATUS_INVALID_USER_ID;
extern int const RESPONSE_STATUS_USER_ACCOUNT_IS_LOCKED;
extern int const RESPONSE_STATUS_INVALID_HOTEL_ID;
extern int const RESPONSE_STATUS_INVALID_ROOMASSIGNMENT_ID;
extern int const RESPONSE_STATUS_NOROOM_ASSIGNMENT_IS_FOUND;
extern int const RESPONSE_STATUS_INVALID_ROOM_STATUS;
extern int const RESPONSE_STATUS_INVALID_ROOM_CLEANING_STATUS;
extern int const RESPONSE_STATUS_INVALID_ROOM_CLEANING_START_TIME;
extern int const RESPONSE_STATUS_INVALID_ROOM_CLEANING_END_TIME;
extern int const RESPONSE_STATUS_INVALID_ROOM_PRIORITY;
extern int const RESPONSE_STATUS_INVALID_ROOM_CLEANING_STATUS_1;
extern int const RESPONSE_STATUS_INVALID_ROOM_TYPE;
extern int const RESPONSE_STATUS_INVALID_ROOM_SECTION;
extern int const RESPONSE_STATUS_INVALID_ROOM_INSPECTED_TIME;
extern int const RESPONSE_STATUS_INVALID_ROOM_INSPECTED_STATUS;
extern int const RESPONSE_STATUS_INVALID_CHECKLIST_ITEM;
extern int const RESPONSE_STATUS_INVALID_SCORE_FOR_CHECKLIST_ITEM;
extern int const RESPONSE_STATUS_INVALID_LAUNDRY_ITEM;
extern int const RESPONSE_STATUS_INVALID_LAUNDRY_SERVICE;
extern int const RESPONSE_STATUS_INVALID_LAUNDRY_TYPE_FOUND;
extern int const RESPONSE_STATUS_INVALID_LAUNDRY_ITEM_FOUND;
extern int const RESPONSE_STATUS_NO_LAUNDRY_LIST_MATCH;
extern int const RESPONSE_STATUS_INVALID_COUNT_ITEM;
extern int const RESPONSE_STATUS_1_RECORD_DELETED;
extern int const RESPONSE_STATUS_ROOM_HAVE_BEEN_DELETED;

//Response code for assign room error
extern int const RESPONSE_STATUS_ERROR_REASSIGN_DATE_IN_THE_PAST;
extern int const RESPONSE_STATUS_ERROR_REASSIGN_OUT_OF_STAFF_WORKSHIFT;
extern int const RESPONSE_STATUS_ERROR_REASSIGN_OUT_OF_REMAINED_TIME;
extern int const RESPONSE_STATUS_ERROR_ASSIGNMENT_NOT_EXIST;
extern int const RESPONSE_STATUS_ERROR_STAFF_NOT_ASSIGNED;
extern int const RESPONSE_STATUS_ERROR_NOT_CURRENT_DAY_ROOM_ASSIGNMENT;
extern int const RESPONSE_STATUS_ERROR_END_CLEANING_TIME_EXCEED_CURRENT_DAY_ASSIGNMENT;
@end
