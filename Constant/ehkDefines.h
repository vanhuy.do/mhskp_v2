//
//  ehkDefines.h
//  eHouseKeeping
//
//  Created by Khanh Nguyen on 15/06/2011.
//  Copyright 2011 TMA. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface ehkDefines : NSObject

extern const int keyTypeNotificationMessage;
extern const int intPopupMsg;
extern const int intVibrateMsg;
extern const int intSoundmsg;

enum TAG_TABBAR_BUTTON{
    tagOfHomeButton = 1,
    tagOfSyncNowButton = 2,
    tagOfPostingButton = 3,
    tagOfFindButton = 4,
    tagOfUnassignButton = 5,
    tagOfSettingButton = 6,
    tagOfLogOutButton = 7,
    tagOfHistoryButton = 8,
    tagOfGuestProfileButton = 9,
    tagOfJobButton = 11,
    tagOfSaveButton = 12,
    //const int tagOfCheckListButton = 4;
    tagOfAdditionalJob = 13,
    tagOfPanicButton = 14,
    tagOfLegendButton = 15,
    tagOfPTT                = 16,
    tagOfMessageButton = 123
};

////Hao Tran [20130517/Remove] -  Replace with Enum define to avoid unexpected circumstance in the future
//extern const int tagOfLogOutButton;
////extern const int tagOfCheckListButton;
//extern const int tagOfMessageButton;
//extern const int tagOfSaveButton;
//extern const int tagOfSyncNowButton;
//extern const int tagOfSettingButton;
//extern const int tagOfHomeButton;
//extern const int tagOfFindButton;
//extern const int tagOfUnassignButton;
//extern const int tagOfJobButton;
//extern const int tagOfPostingButton;
////add tagOfHistoryButton
//extern const int tagOfHistoryButton;
////Hao Tran [20130517/Remove] - END

extern const NSString *notificationCheckList;
extern const NSString *notificationSaveCheckList;
extern const NSString *notificationSelectedHome;
extern const NSString *notificationSelectedFind;
extern const NSString *notificationSaveGuestInfo;
extern const NSString *notificationSaveRoomDetail;
extern const NSString *notificationSetSelectedCurrentView;
extern const NSString *notificationSaveCountChareable;
extern const NSString *notificationSaveCountNonChareable;
extern const NSString *notificationSaveCleaningStatus;
extern const NSString *notificationSaveLaundry;
extern const NSString *notificationSaveHomeView; // save RoomAssignmentView
//notification reload data all view when sync done
extern const NSString *notificationReloadDataAllViewWhenSyncDone;
//notification add Engineering View
extern const NSString *notificationAddEngineeringView;
//notification add lost and found view
extern const NSString *notificationAddLostAndFoundView;
extern const NSString *notificationGuidelineView;
//notification Announce New Message
//extern const NSString *notificationAnnounceNewMessage;
//notification save setting client info.
extern const NSString *notificationSaveSetting;
//notification change language for tabbar
extern const NSString *notificationChangeLanguageTabbar;
//notification Update SyncPeriod Time at HomeView
extern const NSString *notificationUpdateSyncPeriodTime;

#pragma mark - Notification Hide Wifi View
extern const NSString *notificationHideWifiView;

#pragma mark - Notification Show Wifi View
extern const NSString *notificationShowWifiView;
extern const NSString *notificationClearMessageMainView ;

extern const int chkStatusPass;
extern const int chkStatusFail;
extern const int chkStatusNotCompleted;
extern const NSString *cleaningStatusNotification;
extern const NSString *editRoomInfoNotification;
extern const NSString *editCancelRoomInfoNotificaion;
extern const NSString *openPickerViewGuestInfoNotification;
extern const NSString *removePickerViewGuestInfoNotification;
extern const NSString *returnDateTimePickerNotification;

extern const NSString *updateCleaningStatusForRoomNotification;
extern const int room_Status_Id_VC;
extern const int room_Status_Id_VD;
extern const int room_Status_Id_OC;
extern const int room_Status_Id_OD;
extern const int room_Status_Id_OO;
extern const int room_Status_Id_VI;
extern const int room_Status_Id_OI;
extern const int room_Status_Id_VPU;
extern const int room_Status_Id_OCSO;
extern const int room_Status_Id_OOS;

extern const int cleaning_Status_Id_Pending;
extern const int cleaning_Status_Id_DND;
extern const int cleaning_Status_Id_DoubleLock;
extern const int cleaning_Status_Id_ServiceLater;
extern const int cleaning_Status_Id_DeclinedService;
extern const int cleaning_Status_Id_Completed;
extern const int cleaning_Status_Id_Reassign_Pending;
extern const int cleaning_Status_Id_Double_Lock;
extern const int cleaning_Status_Id_Stop;
extern const NSInteger POST_STATUS_UN_CHANGED;
extern const NSInteger POST_STATUS_SAVED_UNPOSTED;
extern const NSInteger POST_STATUS_POSTED;

extern const NSString *pause_icon ;
extern const NSString *done_icon  ;
extern const NSString *reject_icon ;
extern const NSString *start_icon ;
extern const NSString *service_later ;
extern const NSString *dnd_icon;

extern const NSString *OI_icon;
extern const NSString *OC_icon;
extern const NSString *OD_icon;
extern const NSString *VI_icon;
extern const NSString *OOO_icon;
extern const NSString *OOS_icon;
extern const NSString *VD_icon;
extern const NSString *VC_icon;
extern const NSString *VCB_icon;
extern const NSString *VPU_icon;


extern const NSString *ENGLISH_LANGUAGE;
extern const NSString *OTHER_LANGUAGE;
extern const NSString *SIMPLIFIED_LANGUAGE;
extern const NSString *TRADITIONAL_LANGUAGE;
extern const NSString *THAI_LANGUAGE;
extern const NSString *JAPAN_LANGUAGE;

extern const NSInteger NON_CHARGEABLE_ITEM;
extern const NSInteger CHAREABLE_ITEM;

//KEY for OBJECT LANGUAGE
extern const NSString *L_ENG_LANGUAGE_NAME;
#define L_SIMPLIFIED_LANGUAGE_NAME @"strings-chs.xml"
extern const NSString *L_DEFAULT_LANGUAGE_FILE;
extern const NSString *L_TYPE_LANGUAGE;

extern NSString *PHOTOIMAGE;

extern const bool ENABLE_QRCODESCANNER;
extern const bool ENABLE_CHECK_ACCESS_RIGHT_QRCODESCANNER;
extern const bool ENABLE_MESSAGE;
extern const bool ENABLE_CHECK_ACCESS_RIGHT_MESSAGE;
extern const bool ENABLE_POST_LINEN_WITH_ROOM_NUMBER;
extern const bool ENABLE_POST_LOSTANDFOUND_WITH_ROOM_NUMBER;
extern const bool ENABLE_ADDITIONAL_JOB;

#pragma mark - Define font
#define fontName @"Arial"
#define tagViewWifi 100 
#define tagTopbarView 1234

#pragma mark - Define Setting
#define sPhotoSolutionLow @"Low"
#define sPhotoSolutionMedium @"Medium"
#define sPhotoSolutionHeight @"Height"

#pragma mark - Topbar View
#define FRAME_BUTTON_TOPBAR CGRectMake(-5, 0, 215, 44)
#define FONT_BUTTON_TOPBAR [UIFont boldSystemFontOfSize:30]
#define FONT_SMALL_BUTTON_TOPBAR [UIFont boldSystemFontOfSize:22]
#define FONT_BUTTON_TOPBAR_SECOND [UIFont boldSystemFontOfSize:16]
#define COLOR_BUTTON_TOPBAR [UIColor colorWithRed:1 green:1 blue:1 alpha:1.0]
#define SHADOW_COLOR_BUTTON_TOPBAR [UIColor blackColor]
#define SHADOW_OFFSET_BUTTON_TOPBAR CGSizeMake(0, -1)
#define COLOR_CANCEL_SEARCH_BAR [UIColor colorWithRed:71.0/255 green:189.0/255 blue:194.0/255 alpha:1.0]

#pragma mark - Room status
#define ROOM_STATUS_VC @"VC"
#define ROOM_STATUS_VD @"VD"
#define ROOM_STATUS_OC @"OC"
#define ROOM_STATUS_OD @"OD"
#define ROOM_STATUS_VCI @"VI"
#define ROOM_STATUS_OCI @"OI"
#define ROOM_STATUS_VPU @"VPU"
#define ROOM_STATUS_OPU @"OPU"
#define ROOM_STATUS_VCB @"VCB"
#define ROOM_STATUS_OOO @"OOO"
#define ROOM_STATUS_OOS @"OOS"
#define ROOM_STATUS_OCS @"OC -S.O"

/*
 ENUM_ROOM_STATUS_VC = 1,
 ENUM_ROOM_STATUS_VD = 2,
 ENUM_ROOM_STATUS_OC = 3,
 ENUM_ROOM_STATUS_OD =4 ,
 ENUM_ROOM_STATUS_VCI = 5,
 ENUM_ROOM_STATUS_OCI = 6,
 ENUM_ROOM_STATUS_VPU = 7,
 ENUM_ROOM_STATUS_OPU = 8,
 ENUM_ROOM_STATUS_VCB = 9,
 ENUM_ROOM_STATUS_OOO = 10,
 ENUM_ROOM_STATUS_OOS = 11

 */

typedef enum {
    IN_ROOM = 1,
    LUNCH = 2,
    MOVING = 3
} ATTENDANT_CURRENT_STATUS;

typedef enum {
    START_SYNC_NOW = 1, // user want to start sync immediately
    NON_START_SYNC = 2, // sync function will be fire when sync period is out
} STATUS_START_SYNC_NOW;

typedef enum {
    CAN_START_RECORD_DETAIL = 1,
    CAN_NOT_START_RECORD_DETAIL = 2,
    CONTINUE_COUNT_INSPECTION = 3,
} STATUS_UPDATE_RECORD_DETAIL;

#pragma mark - SETTING DEFINES

#define MAX_MESSAGE_PERIOD 30 //Days
#define CONSOLE_LOG @"CONSOLE_LOG"
#define STACK_TRACE @"STACK_TRACE"
#define APP_VERSION @"APP_VERSION"
#define MAX_ROOM_RECORD_DETAIL 5
#define INTERVAL_START_STOP_RECORD_DETAIL 60
#define FONT_SIZE_ROOM_STATUS 17
#define REACCESS_MAX_TIMES  19 //one method only reaccess max 19 times
#define IS_LOAD_LANGUAGE @"IS_LOAD_LANGUAGE"
#define REMARK_CONSOLIDATE @"REMARK_CONSOLIDATE_"
/**********
 *
 * if any function is disable, assign 0 value for these constant
 * if any function is enable, assign 1 value for these constant
 *
 *********/
#define ENABLE_PHYSICAL     1
#define ENABLE_ENGINEER     1
#define ENABLE_LOSTFOUND    1
#define ENABLE_LAUNDRY      0
#define ENABLE_MINIBAR      1
//#warning modify for Conrad
//ENABLE_LINEN        1
#define ENABLE_LINEN        1
#define ENABLE_AMENITIES    1
typedef enum {
    DELETE_ROOM_ASSIGNMENT_FLAG = 1,
    UNDELETE_ROOM_ASSIGNMENT_FLAG = 0
} DELETE_FLAG_RA;


@end
