//
//  ehkDefines.m
//  eHouseKeeping
//
//  Created by Khanh Nguyen on 15/06/2011.
//  Copyright 2011 TMA. All rights reserved.
//

#import "ehkDefines.h"


@implementation ehkDefines
const NSString *keyURLService = @"keyURLService";
const int keyTypeNotificationMessage=1; // 1: popup , 2 : vibrate  ,3 : sound 

const int intPopupMsg=0;  // popup 
const int intVibrateMsg=1; //vibrate
const int intSoundmsg=2;  // loud sound.

//Hao Tran Remove - Replace with Enum define to avoid unexpected circumstance in the future
//const int tagOfHomeButton = 1;
////const int tagOfCheckListButton = 4;
//const int tagOfMessageButton = 123;
//const int tagOfSaveButton = 12;
//const int tagOfSyncNowButton = 2;
//const int tagOfJobButton = 11;
//const int tagOfSettingButton = 6;
//const int tagOfLogOutButton = 7;
//const int tagOfFindButton = 4;
//const int tagOfUnassignButton = 5;
//const int tagOfPostingButton = 3;
////ad tagOfHistoryButton
//const int tagOfHistoryButton = 8;
//Hao Tran Remove - END

const NSString *notificationCheckList= @"checkListAddBackNotification";

const NSString *notificationSaveCheckList = @"saveCheckListNotification";
const int chkStatusPass = 1;
const int chkStatusFail = -1;
const int chkStatusNotCompleted = 0;

const NSString *cleaningStatusNotification= @"cleaningStatusNotification";
const NSString *editCancelRoomInfoNotificaion = @"editCancelRoomInfoNotificaion";
const NSString *editRoomInfoNotification=   @"editRoomInfoNotification";
const NSString *notificationSelectedHome = @"selectedHomeNotification";
const NSString *notificationSelectedFind = @"selectedFindNotification";
const NSString *notificationSaveGuestInfo = @"saveGuestInfoNotification";
const NSString *notificationSaveRoomDetail = @"saveRoomDetailNotification";
const NSString *notificationSetSelectedCurrentView = @"setSelectedCurrentViewNotification";
const NSString *notificationSaveCountChareable = @"saveCountChareableNotification";
const NSString *notificationSaveCountNonChareable = @"saveCountNonChareableNotification";
const NSString *notificationSaveLaundry=@"SaveLaundryNotification";
const NSString *notificationSaveCleaningStatus = @"saveCleaningStatus";
const NSString *notificationSaveHomeView = @"saveHomeViewNotification"; // save RoomAssignmentView
//notification reload data all view when sync done
const NSString *notificationReloadDataAllViewWhenSyncDone = @"reloadDataAllViewWhenSyncDoneNotification";
//notification add Engineering View
const NSString *notificationAddEngineeringView = @"addEngineeringViewNotification";
//notification add lost and found view
const NSString *notificationAddLostAndFoundView = @"addLostAndFoundViewNotification";
const NSString *notificationGuidelineView = @"addGuidelineViewNotification";

//notification Announce New Message
//const NSString *notificationAnnounceNewMessage = @"announceNewMessageNotification";
//
const NSString *notificationSaveSetting=@"notificationSaveSetting";
//notification change language for tabbar
const NSString *notificationChangeLanguageTabbar = @"changeLanguageTabbarNotificaion";

// notification for get DateTimePicker.
const NSString *openPickerViewGuestInfoNotification=@"openPickerViewGuestInfo";
const NSString *removePickerViewGuestInfoNotification=@"removePickerViewGuestInfo";
const NSString *returnDateTimePickerNotification=@"returnDateTimePicker";

// notification for update cleaning status.
const NSString *updateCleaningStatusForRoomNotification=@"updateCleaningStatusForRoom";
//notification Update SyncPeriod Time at HomeView
const NSString *notificationUpdateSyncPeriodTime = @"updateSyncPeriodTimeNotification";
//
#pragma mark - Notification clear Message
const NSString *notificationClearMessageMainView = @"ClearMessageMainViewNotification";
#pragma mark - Notification Hide Wifi View
const NSString *notificationHideWifiView = @"HideWifiViewNotification";

#pragma mark - Notification Show Wifi View
const NSString *notificationShowWifiView = @"ShowWifiViewNotification";


const int room_Status_Id_VC=1;
const int room_Status_Id_VD=2;
const int room_Status_Id_OC=3;
const int room_Status_Id_OD=4;
const int room_Status_Id_OO=10;
const int room_Status_Id_VI=5;
const int room_Status_Id_OI=6;
const int room_Status_Id_VPU=7;
const int room_Status_Id_OCSO=13;
const int room_Status_Id_OOS=11;
const int cleaning_Status_Id_Reassign_Pending = 8;
const int cleaning_Status_Id_Double_Lock = 9;
const int cleaning_Status_Id_Pending = 7;
const int cleaning_Status_Id_DND = 1;
const int cleaning_Status_Id_DoubleLock = 9;
const int cleaning_Status_Id_ServiceLater = 2;
const int cleaning_Status_Id_DeclinedService = 3;
const int cleaning_Status_Id_Completed = 5;
const int cleaning_Status_Id_Stop = 6;
const NSInteger POST_STATUS_UN_CHANGED = -1;
const NSInteger POST_STATUS_SAVED_UNPOSTED = 0;
const NSInteger POST_STATUS_POSTED = 1;

const NSString *pause_icon =@"icon_pause.png";   //@status_pause.png
const NSString *done_icon  =@"Icon_completed.png";    //@status_complete.png
const NSString *reject_icon =@"Icon_Declined Service.png";  // status_declined.png
const NSString *start_icon  = @"Icon_Start.png";                 //@status_start.png
const NSString *service_later=@"Icon_Service later.png";                  // @status_service_later.png
const NSString *dnd_icon =  @"icon_dnd.png"; //@"Icon_DND.png";                      // status_dnd.png

const NSString *OI_icon =@"OI.png";
const NSString *OC_icon =@"OC.png";
const NSString *OD_icon =@"OD.png";
const NSString *VI_icon =@"VI.png";
const NSString *OOO_icon =@"OOO.png";
const NSString *OOS_icon =@"OOS.png";
const NSString *VD_icon =@"VD.png";
const NSString *VC_icon =@"VC.png";
const NSString *VCB_icon =@"VCB.png";
const NSString *VPU_icon =@"VPU.png";

const NSString *ENGLISH_LANGUAGE = @"eng";
const NSString *OTHER_LANGUAGE = @"Other";
const NSString *SIMPLIFIED_LANGUAGE = @"chs";
const NSString *TRADITIONAL_LANGUAGE = @"cht";
const NSString *THAI_LANGUAGE = @"tha";
const NSString *JAPAN_LANGUAGE = @"jpn";

const NSInteger NON_CHARGEABLE_ITEM = 0;
const NSInteger CHAREABLE_ITEM = 1;

//KEY for OBJECT LANGUAGE
const NSString *L_ENG_LANGUAGE_NAME = @"strings.xml";
const NSString *L_DEFAULT_LANGUAGE_FILE = @"default_strings.xml";//@"strings-chs.xml";
const NSString *L_TYPE_LANGUAGE = @"l_type_language";

NSString *PHOTOIMAGE = @"photo_icon.png";

const bool ENABLE_QRCODESCANNER = NO;
const bool ENABLE_CHECK_ACCESS_RIGHT_QRCODESCANNER = YES;
const bool ENABLE_MESSAGE = NO;
const bool ENABLE_CHECK_ACCESS_RIGHT_MESSAGE = YES;
const bool ENABLE_POST_LINEN_WITH_ROOM_NUMBER = YES;
const bool ENABLE_POST_LOSTANDFOUND_WITH_ROOM_NUMBER = YES;
const bool ENABLE_ADDITIONAL_JOB = YES;

@end
