//
//  ehkDefinesV2.h
//  mHouseKeeping
//
//  Created by TMS on 2/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum : NSInteger {
    GuestSearchType_InHouse = 0,
    GuestSearchType_DueIn = 1,
    GuestSearchType_DueOut = 2
} GuestSearchType;

typedef enum : NSInteger {
    GuestSearchTime_OnDay = 0,
    GuestSearchTime_Past1Week = 1,
    GuestSearchTime_Future1Week = 2,
    GuestSearchTime_Past1Month = 3,
    GuestSearchTime_Future1Month = 4
} GuestSearchTime;

typedef enum : NSInteger {
    ModuleHistory_Minibar = 1,
    ModuleHistory_Linen = 2,
    ModuleHistory_Amenities = 3,
    ModuleHistory_LostAndFound = 4,
    ModuleHistory_Engineering = 5,
    ModuleHistory_Physical = 6,
} ModuleHistory;

typedef enum : NSInteger {
    FilterOption_All = 0,
    FilterOption_RoomType = 1,
    FilterOption_RoomStatus = 2,
    FilterOption_Zone = 3,
    FilterOption_Floor = 4
} FilterOption;

typedef enum {
    RoomBlockingStatus_Normal = 0,
    RoomBlockingStatus_Blocked = 1,
    RoomBlockingStatus_Release = 2,
} RoomBlockingStatus;

typedef enum {
    GuestTypeCurrent = 0,
    GuestTypeArrival,
} GuestType;

typedef enum {
    RoomIndicatorGuestDueIn = 1,
    RoomIndicatorGuestDueOut,
    RoomIndicatorGuestBackToBack
}RoomIndicatorGuest;

typedef enum {
    sPopup = 1,
    sVibrate = 2,
    sRing = 3,
}sNotificationType;

typedef enum {
    sManual = 1,
    sAutomatic = 2,
}sSynch;

typedef enum {
    sLow = 1,
    sMedium = 2,
    sHeight = 3,
}sPhoto;

typedef enum : NSInteger {
    ServiceType_Linen = 1,
    ServiceType_Minibar = 3,
    ServiceType_Amenities = 4,
    ServiceType_Guideline = 5
} ServiceType;

enum MESSAGE_KIND {
    MESSAGE_KIND_DRAFT = 0,
    MESSAGE_KIND_SENT,
    MESSAGE_KIND_PENDING,
    MESSAGE_KIND_INBOX
};

typedef enum : NSInteger {
    MessageUpdateType_SenderOutbox = 0,
    MessageUpdateType_ReceiverInbox = 1
} MessageUpdateType;

enum MESSAGE_READ_STATUS{
    MESSAGE_UNREAD = 0,
    MESSAGE_READ = 1,
    MESSAGE_DELETE = 2
};

typedef enum {
    PHOTO_RESOLUTION_240_180 = 0,
    PHOTO_RESOLUTION_320_240,
    PHOTO_RESOLUTION_480_360,
} PHOTO_RESOLUTION;

enum PERMISSION{
    IS_ALLOWED_VIEW = (1UL << 2),
    IS_ALLOWED_EDIT = (1UL << 1),
    IS_ACTIVE = (1UL)
};

enum IS_PUSHED_FROM {
    IS_PUSHED_FROM_ROOM_ASSIGNMENT = 0,
    IS_PUSHED_FROM_FIND_BY_RA,
    IS_PUSHED_FROM_COMPLETE,
    IS_PUSHED_FROM_ADD_JOB_LIST,
    IS_PUSHED_FROM_ADD_JOB_DETAIL,
    IS_PUSHED_FROM_FIND_BY_ROOM,
    IS_PUSHED_FROM_MESSAGE_MAIN,
    IS_PUSHED_FROM_MESSAGE_INBOX,
    IS_PUSHED_FROM_HOME,
    IS_PUSHED_FROM_PASSED_INSPECTION,
    IS_PUSHED_FROM_FAILED_INSPECTION,
    IS_PUSHED_FROM_ROOM_STATUS,
    IS_PUSHED_FROM_PHYSICAL_CHECK
};

enum TIMER_STATUS {
    TIMER_STATUS_NOT_START = 0,
    TIMER_STATUS_START = 1,
    TIMER_STATUS_PAUSE = 2,
    TIMER_STATUS_FINISH = 3
};

enum POPUP_STATE {
    POPUP_STATE_SHOW = 1,
    POPUP_STATE_HIDE = 2,
    POPUP_STATE_HIDE_WITH_INVALID_ROOM_STATUS = 12010
};

enum RemarkType {
    RemarkType_RoomRemark = 1,
    RemarkType_PhyshicalCheck = 2, 
	RemarkType_ConsolidationRemark = 3
};

typedef NS_ENUM (NSInteger, MHChecklistCheckMark) {
    //0 = Original logic
    MHChecklistCheckMark_Normal = 0,
    //1 = Default full mark, all checkboxes are defaulted to unchecked, if check the box then will minus value from full mark amount.
    MHChecklistCheckMark_CheckToMinus = 1,
    //2 = Default full mark, all checkboxes are defaulted to checked, if uncheck the box, then will calculate value based on how many boxes checked.
    MHChecklistCheckMark_CheckToAdd = 2
};

typedef NS_ENUM (NSInteger, MHCheckMarkRate) {
    MHCheckMarkRate_Normal = 0,
    MHCheckMarkRate_FullMark = 1
};

@interface ehkDefinesV2 : NSObject

extern const NSString *L_LF_BACKGROUND_IMAGE;
extern const int TITLE_SIZE;
extern NSString *FONT_NAVIGATION_BAR;
extern const int MESSAGE_DURATION;
extern const int MESSAGE_INBOX_MAX;

extern const NSString *ROOM_ASSIGNMENT_ROOM_ASSIGNMENT;
extern const NSString *ROOM_ASSIGNMENT_MANUAL_CREDIT;
extern const NSString *ROOM_ASSIGNMENT_CHECKLIST_REMARKS;
extern const NSString *SYSTEM_ENABLE_ENCRYPTED_QRCODE;
extern const NSString *ROOM_ASSIGNMENT_COMPLETED_ROOM;
extern const NSString *ROOM_ASSIGNMENT_MANUAL_UPDATE_ROOM_STATUS;
extern const NSString *ROOM_ASSIGNMENT_RESTRICTION_ASSIGNMENT;
extern const NSString *ROOM_ASSIGNMENT_QR_CODE_SCANNER;
extern const NSString *POSTING_LOST_AND_FOUND;
extern const NSString *POSTING_ENGINEERING;
extern const NSString *POSTING_LAUNDRY;
extern const NSString *POSTING_MINIBAR;
extern const NSString *POSTING_LINEN;
extern const NSString *POSTING_AMENITIES;
extern const NSString *POSTING_PHYSICAL_CHECK;
extern const NSString *ACTION_LOST_AND_FOUND;
extern const NSString *ACTION_ENGINEERING;
extern const NSString *ACTION_LAUNDRY;
extern const NSString *ACTION_MINIBAR;
extern const NSString *ACTION_LINEN;
extern const NSString *ACTION_AMENITIES;
extern const NSString *ACTION_GUIDELINE;
extern const NSString *ACTION_CHECKLIST;
extern const NSString *MESSAGE_MESSAGE;
extern const NSString *FIND_ATTENDANT_PENDING_ROOM;
extern const NSString *FIND_ROOM;
extern const NSString *FIND_INSPECTION;
extern const NSString *SETTING_SETTING;
extern const NSString *ADDTIONAL_JOB;
extern const NSString *ROOM_DETAILS_ACTION_HISTORY;
extern const NSString *FIND_ACTION_HISTORY;
extern const NSString *FIND_ROOM_REMARKS_UPDATE;
extern const NSString *POSTING_FUNCTION_ACTION_HISTORY;
extern const NSString *POSTING_FUNCTION_MINIBAR_NEW;
extern const NSString *POSTING_FUNCTION_MINIBAR_USED;
extern const NSString *ROOM_DETAILS_ACTION_MINIBAR_NEW;
extern const NSString *ROOM_DETAILS_ACTION_MINIBAR_USED;

extern const NSString *POSTING_FUNCTION_AMENITIES_QR_CODE;
extern const NSString *POSTING_FUNCTION_ENGINEERING_QR_CODE;
extern const NSString *POSTING_FUNCTION_LINEN_QR_CODE;
extern const NSString *POSTING_FUNCTION_LOST_AND_FOUND_QR_CODE;
extern const NSString *POSTING_FUNCTION_MINIBAR_QR_CODE;
extern const NSString *POSTING_FUNCTION_PHYSICAL_CHECK_QR_CODE;
//MARK: USER CONFIGURATION KEYS - Add by getUserConfigurationsFromWS function
#define USER_CONFIG_ROOM_ASSIGNMENT_PAUSE_ONE_TIME @"ROOM_ASSIGNMENT_PAUSE_ONE_TIME"
#define USER_CONFIG_ROOM_ASSIGNMENT_UNASSIGNED_ROOMS_METHOD @"ROOM_ASSIGNMENT_UNASSIGNED_ROOMS_METHOD"
#define USER_CONFIG_ROOM_ASSIGNMENT_FIND_ROOMS_METHOD @"ROOM_ASSIGNMENT_FIND_ROOMS_METHOD"
#define USER_CONFIG_TIME_BACKGROUND_ADD_JOB_DETAIL @"USER_CONFIG_TIME_BACKGROUND_ADDITIONAL_JOB"
#define USER_CONFIG_ROOM_ASSIGNMENT_FIND_RA_METHOD @"ROOM_ASSIGNMENT_FIND_RA_METHOD"
#define USER_CONFIG_GUEST_INFO_ENABLE @"ENABLE_GUEST_INFO"
#define USER_CONFIG_CHECKLIST_DEFAULT_FULLMARK @"CHECKLIST_DEFAULT_FULLMARK"
#define USER_CONFIG_ENABLE_INSPECTION_MATRIX @"ENABLE_TODAY_INSPECTION"
#define USER_CONFIG_IS_VALIDATE_ROOM_NUMBER @"ENABLE_VALIDATE_ROOM_NUMBER"
#define USER_CONFIG_IS_LINEN_SEPERATE_NEW_AND_USED @"LINEN_SEPERATE_NEW_AND_USED"
#define USER_CONFIG_PHYSICAL_CHECK_SHOW_REMARKS @"PHYSICAL_CHECK_SHOW_REMARKS"
#define USER_CONFIG_PHYSICAL_CHECK_APPEND_TO_EXISTING_REMARKS @"PHYSICAL_CHECK_APPEND_TO_EXISTING_REMARKS"
#define USER_CONFIG_FIND_FUNCTION_DISPLAY_METHOD @"FIND_FUNCTION_DISPLAY_METHOD"
#define USER_CONFIG_RELEASE_ROOM_ENABLE @"RELEASE_ROOM_ENABLE"
#define USER_CONFIG_PANIC_BUTTON_ENABLE @"PANIC_BUTTON_ENABLE"
#define USER_CONFIG_MINIBAR_SYNCHRONIZE_USED_AND_NEW @"MINIBAR_SYNCHRONIZE_NEW_AND_USED"
#define USER_CONFIG_PHYSICALCHECK_ROOMSTATUS_FROM_ROOMTABLE @"PHYSICAL_CHECK_ROOM_STATUS_FROM_ROOM_TABLE"
#define USER_CONFIG_POSTING_POST_ZEROTH_VALUE @"POSTING_POST_ZEROTH_VALUE"
#define USER_CONFIG_REMARKS_TYPE @"REMARKS_TYPE"
#define USER_CONFIG_SUP_SYNC_INTERVAL @"SUP_SYNC_INTERVAL"
#define USER_CONFIG_RA_SYNC_INTERVAL @"RA_SYNC_INTERVAL"
#define USER_CONFIG_USER_CHECKING_LOGIN_INTERVAL @"USER_CHECKING_LOGIN_INTERVAL"
#define USER_CONFIG_MINIBAR_NEW_USED_DISPLAY @"MINIBAR_NEW_USED_DISPLAY"
#define USER_CONFIG_LINEN_NEW_USED_DISPLAY @"LINEN_NEW_USED_DISPLAY"
#define USER_CONFIG_POSTING_HISTORY_DAYS @"POSTING_HISTORY_DAYS"
#define USER_CONFIG_CHECKLIST_MANDATORYPASS @"CHECKLIST_MANDATORYPASS"
#define USER_CONFIG_SUP_HOME_FILTER_OPTION @"SUP_HOME_FILTER_OPTION"
#define USER_CONFIG_ENABLE_SUP_HOME_FILTER @"INSPECTION_ENABLE_SUP_HOME_FILTER"
#define USER_CONFIG_ENABLE_ONLINE_POSTING_HISTORY @"ENABLE_POSTING_HISTORY"
#define USER_CONFIG_CONTINUE_FROM_PAUSE_TIME @"CONTINUE_FROM_PAUSE_TIME"
#define USER_CONFIG_ENABLE_SEARCH_GUEST_PROFILE @"FIND_GUEST_STAY_HISTORY_ENABLE"
#define USER_CONFIG_USE_ADDITIONALJOB_STATUS_TABLE @"USE_ADDITIONALJOB_STATUS_TABLE"
#define USER_CONFIG_LOAD_AMENITIES_BY_ROOMTYPE_ENABLE @"LOAD_AMENITIES_BY_ROOMTYPE_ENABLE"
#define USER_CONFIG_LOAD_LINEN_BY_ROOMTYPE_ENABLE @"LOAD_LINEN_BY_ROOMTYPE_ENABLE"
#define USER_CONFIG_LOAD_MINIBAR_BY_ROOMTYPE_ENABLE @"LOAD_MINIBAR_BY_ROOMTYPE_ENABLE"
#define USER_CONFIG_ENABLE_OTHER_ACTIVITY @"ENABLE_OTHER_ACTIVITY"

//MARK: USER CONFIGURATION KEYS - Add by local
#define USER_CONFIG_PANIC_IS_DIRECT_CALL @"USER_CONFIG_PANIC_IS_DIRECT_CALL"
#define USER_CONFIG_PANIC_DIRECT_CALL_NUMBER @"USER_CONFIG_PANIC_DIRECT_CALL_NUMBER"
#define USER_CONFIG_PANIC_RA_PHONE @"USER_CONFIG_PANIC_RA_PHONE"
#define USER_CONFIG_PANIC_SUP_NAME @"USER_CONFIG_PANIC_SUP_NAME"
#define USER_CONFIG_PANIC_SUP_STAFF_ID @"USER_CONFIG_PANIC_SUP_STAFF_ID"
#define USER_CONFIG_PANIC_IS_ADHOC_MESSAGE @"USER_CONFIG_PANIC_IS_ADHOC_MESSAGE"
#define USER_CONFIG_PANIC_IS_SMS @"USER_CONFIG_PANIC_IS_SMS"
#define USER_CONFIG_PANIC_IS_EMAIL @"USER_CONFIG_PANIC_IS_EMAIL"
#define USER_CONFIG_PANIC_IS_ALARM_SOUND @"USER_CONFIG_PANIC_IS_ALARM_SOUND"
#define USER_CONFIG_PANIC_IS_ECONNECT_JOB @"USER_CONFIG_PANIC_IS_ECONNECT_JOB"
#define USER_CONFIG_PANIC_MESSAGE_SUBJECT @"USER_CONFIG_PANIC_MESSAGE_SUBJECT"
#define USER_CONFIG_PANIC_MESSAGE_CONTENT @"USER_CONFIG_PANIC_MESSAGE_CONTENT"

#define USER_CONFIG_IS_ALREADY_CONFIGED @"USER_CONFIG_IS_ALREADY_CONFIGED"
#define DEBUG_USER_COMFIG_PASSWORD @"DEBUG_USER_COMFIG_PASSWORD"
#define USER_CONFIG_IS_ADDJOB_ACTIVE @"USER_CONFIG_IS_ADDJOB_ACTIVE"
#define USER_CONFIG_IS_ADDJOB_VIEW @"USER_CONFIG_IS_ADDJOB_VIEW"
#define USER_CONFIG_DATE_DELETE_ADDJOB @"USER_CONFIG_DATE_DELETE_ADDJOB"

#define USER_CONFIG_MESSAGE_PERIOD @"USER_CONFIG_MESSAGE_PERIOD"
#define USER_CONFIG_MESSAGE_SENT @"USER_CONFIG_MESSAGE_SENT"
#define USER_CONFIG_MESSAGE_IS_SHOW_NOTIFICATION @"USER_CONFIG_MESSAGE_SHOW_NOTIFICATION"
#define USER_CONFIG_MESSAGE_IS_WAITING_DISMISS_ALERT @"USER_CONFIG_MESSAGE_IS_WAITING_DISMISS_ALERT"
#define USER_CONFIG_MESSAGE_READ_RECEIVED @"USER_CONFIG_MESSAGE_READ_RECEIVED"
#define USER_CONFIG_MESSAGE_UNREAD_RECEIVED @"USER_CONFIG_MESSAGE_UNREAD_RECEIVED"
#define USER_CONFIG_ROOM_INSPECTION_LASTMODIFIED @"USER_CONFIG_ROOM_INSPECTION_LASTMODIFIED"

#define USER_CONFIG_ROOM_DETAILS_ACTION_HISTORY_ACTIVE @"USER_CONFIG_ROOM_DETAILS_ACTION_HISTORY_ACTIVE"
#define USER_CONFIG_ROOM_DETAILS_ACTION_HISTORY_VIEW @"USER_CONFIG_ROOM_DETAILS_ACTION_HISTORY_VIEW"
#define USER_CONFIG_FIND_ACTION_HISTORY_ACTIVE @"USER_CONFIG_FIND_ACTION_HISTORY_ACTIVE"
#define USER_CONFIG_FIND_ACTION_HISTORY_VIEW @"USER_CONFIG_FIND_ACTION_HISTORY_VIEW"

#define USER_CONFIG_ROOM_ASSIGNMENT_SPECIAL_ASSIGNMENT_METHOD @"ROOM_ASSIGNMENT_SPECIAL_ASSIGNMENT_METHOD" // CFG [20160914/CRF-00001439]
#define USER_CONFIG_ROOM_ASSIGNMENT_MANDATORY_MINIBAR_POSTING @"ROOM_ASSIGNMENT_MANDATORY_MINIBAR_POSTING" // CFG [20160928/CRF-00001612]
#define USER_CONFIG_ROOM_ASSIGNMENT_MANDATORY_LINEN_POSTING @"ROOM_ASSIGNMENT_MANDATORY_LINEN_POSTING" // CFG [20160928/CRF-00001612]
#define USER_CONFIG_ROOM_ASSIGNMENT_MANDATORY_AMENITIES_POSTING @"ROOM_ASSIGNMENT_MANDATORY_AMENITIES_POSTING" // CFG [20160928/CRF-00001612]

#define USER_CONFIG_LINEN_POSTING_CONFIRMATION_MESSAGE_DETAILS_FORMAT @"LINEN_POSTING_CONFIRMATION_MESSAGE_DETAILS_FORMAT" // CFG [20161028/CRF-00001614]
#define USER_CONFIG_MINIBAR_POSTING_CONFIRMATION_MESSAGE_DETAILS_FORMAT @"MINIBAR_POSTING_CONFIRMATION_MESSAGE_DETAILS_FORMAT" // CFG [20161028/CRF-00001614]

#define USER_CONFIG_ROOM_ASSIGNMENT_DAY_OFFSET_MINUTES @"ROOM_ASSIGNMENT_DAY_OFFSET_MINUTES" // CFG [20161028/CRF-00001872]

#define USER_CONFIG_INSPECTION_CROSS_ZONE_METHOD @"INSPECTION_CROSS_ZONE_METHOD" // CFG [20161221/CRF-00001557]

//MARK: Others String contants
#define POSTECNJOB_SENDER @"FCS e-Housekeeping"
#define POSTECNJOB_ACTION @"Immediate" //Immediate, Scheduled, Repeated
#define POSTECNJOB_USERNAME @"fcs"
#define POSTECNJOB_PASSWORD @""
#define POSTECNJOB_PROPERTY_ID @"1"
#define POSTECNJOB_SERVICE_TYPE @"Guest"

//MARK: Tag contants
#define tagRoomNo                   1
#define tagRMStatus                 2
#define tagVIP                      3
#define tagFirstIconCleaningStatus  4
#define tagSecondIconCleaningStatus 5
#define tagTimeServiceLater         6
#define tagRushOrQueueRoom          7
#define tagDueOutRoom               8
#define tagArrivalTime              9
#define tagGuestName                10
#define tagRAName                   11
#define tagDueInRoom                12
#define tagDepartureTime            13
#define tagBlockingImage            17
#define tagBlockingReason           15
#define tagBlockingRemark           16
#define tagAddition                 18
#define tagPreviousDate             19
#define tagOOSDuration              20
#define tagRoomType                 21
#define tagProfileNotes             22 // CRF [20160929/CRF-00000827]
#define tagStayOver                 23 // CRF [20161106/CRF-00001293]

#define lblDueOutFrame              CGRectMake(5,33,45,21)
#define lblArrivalTimeFrameCase1    CGRectMake(50,33,50,21)
#define lblArrivalTimeFrameCase2    CGRectMake(5,33,50,21)
//#define lblGuestNameFrameInCase1    CGRectMake(100,33,100,21)
//#define lblGuestNameFrameInCase2    CGRectMake(55,33,100,21)
//#define lblGuestNameFrameInCase3    CGRectMake(50,33,100,21)
//#define lblGuestNameFrameInCase4    CGRectMake(5,33,100,21)
#define lblGuestNameFrameInCase1    CGRectMake(100,33,200,21)
#define lblGuestNameFrameInCase2    CGRectMake(55,33,200,21)
#define lblGuestNameFrameInCase3    CGRectMake(50,33,200,21)
#define lblGuestNameFrameInCase4    CGRectMake(5,40,200,21)
#define lblServiceLaterTimeFrame    CGRectMake(245,12,50,25)


//MARK: IMAGE DEFINES
#define imgPanic @"btn_Panic.png"
#define imgPanicPressed @"panic-button-pressed.png"
#define imgAmenities @"amenities_bt.png"

#define IMAGE_BLOCK_ON @"block_bt_299x58.png"
#define IMAGE_BLOCK_OFF @"block_bt_gray_299x58.png"
#define IMAGE_NO_IMAGE_AVAILABLE    @"no_image_2.png"
#define imgTopCart @"top_cart.png"
#define imgInstructionOff @"instruction_bt_off.png"
#define imginstructionOn @"instruction_bt_on.png"
#define imgBackground @"bg.png"
#define imgCheckMark @"Audit_Sheet_icon.png"
#define imgRating @"Inspection_Sheet.png"
#define imgBackgroundSection @"unassigned_room_zone_bt.png"
#define imgRightRow @"detail_59x60.png"
#define imgRightRowOpen @"detail_open_59x60.png"
#define imgChecked @"btn_checked.png"
#define imgUnCheck @"btn_uncheck.png"
#define imgRowGray @"gray_arrow_36x36.png"
#define imgBackgroundCategory @"big_bt_640x111.png"
#define optionBackgroundGray @"setting_bt_gray.png"
#define optionButLanguageImg @"setting_bt.png"
#define imgBtnSubmit            @"btn_submit.png"
#define imgBtnSubmitClicked     @"btn_submit (onpress).png"
#define imgBtnSubmitDisable     @"btn_submit (greyout).png"

#define iconDone            @"icon_done.png"
#define iconReject          @"icon_reject.png"
#define iconPause           @"icon_pause.png"
#define urlImgSupervisor    @"supervisor.png"
#define menuJob             @"menu_job.png"
//#define navigationBarImage  @"titleBar.png"
#define backgroundImage     @"bg.png"
#define logoutImage         @"but_titleBar.png"
#define imgBtnCompletedAndInspected                 @"btn_bg-completed & inspected.png"
#define imgBtnCompletedAndInspectedClicked          @"btn_bg-completed & inspected (onpress).png"
#define imgBtnCompletedAndInspectedDisable          @"btn_bg-completed & inspected (greyout).png"

#define imgActive           @"active.png"
#define imgYesBtn @"yes_bt.png"
#define imgNoBtn @"no_bt.png"
#define imgWrongRoom @"icon_wrong room.png"
#define imgMenuRight @"menuICON_right.png"
#define imgMenuLeft @"menuICON_left.png"
#define imgMenuTop @"menuICON_top.png"
#define imgMenuBottom @"menuICON_bottom.png"
#define imgBottom @"bottom.png"
#define imgGrayArrow @"gray_arrow_36x36.png"
#define imgRushRoom @"icon_rush room.png"
#define imgQueueRoom @"icon_queue room.png"
#define imgComplete @"icon_completed.png" //@"Icon_completed.png"
#define imgPending @"icon_pending.png"
#define imgDND @"icon_dnd.png"  //@"Icon_DND.png"
#define imgDeclinedService @"Icon_Declined Service.png"
#define imgServiceLater @"Icon_Service later.png"
#define imgStop @"icon_stop.png"
#define imgStart @"icon_start.png" //@"Icon_Start.png"
#define imgQRCode @"icon_QR_code_2.png"

#define markImage           @"!.png"
#define backgroundImage     @"bg.png"
#define imgStartBtn                             @"btn_start.png"
#define imgStartBtnClicked                      @"btn_start (onpress).png"
#define imgStartBtnDisable                      @"btn_start (greyout).png"
#define imgStopBtn                              @"btn_stop.png"
#define imgStopBtnClicked                       @"btn_stop (onpress).png"
#define imgStopBtnDisable                       @"btn_stop (disable).png"
#define imgCleaningStatusBtnAndActionBtn        @"btn_bg-cleaning status & action.png"
#define imgCleaningStatusBtnAndActionBtnClicked @"btn_bg-cleaning status & action (onpress).png"
#define imgCleaningStatusBtnAndActionBtnDisable @"btn_bg-cleaning status & action (greyout).png"
#define imgCompleteBtn                          @"btn_complete.png"
#define imgCompleteBtnClicked                   @"btn_complete (onpress).png"
#define imgCompleteBtnDisable                   @"btn_complete (greyout).png"
#define imgReassignBtn                          @"btn_Reassign.png"
#define imgPostingHistoryBtn                    @"btn_posting_history.png"
#define imgReassignBtnClicked                   @"btn_Reassign (onpress).png"
#define imgReassignBtnDisable                   @"btn_Reassign (grey out).png"
#define imgGuestInfoInactive                    @"top_bt_grey_240x58.png"
#define imgGuestInfoActive                      @"GuestInfo.png"
#define imgTimerNotStart                        @"timer_gray_274x58.png"
#define imgTimerPlay                            @"timer_play_274x58.png"
#define imgTimerPause                           @"timer_phuse_274x58.png"
#define imgTimerOver                            @"timer_red_274x58.png"
#define imgFailIcon                             @"fail_icon.png"
#define imgPassIcon                             @"pass_icon.png"
#define imgYesIcon                              @"yes_icon2.png"
#define imgArrowGrey                            @"icon_arrow (grey).png"

#define imgBtnSelectionPopup                    @"btn_popup selection.png"
#define imgBtnSelectionPopupClicked             @"btn_popup selection (onpress).png"
#define imgBtnCancelPopup                       @"btn_popup cancel.png"
#define imgBtnCancelPopupClicked                @"btn_popup cancel (onpress).png"
#define imgBgBigBtn                             @"big_bt_640x111.png"
#define imgRatingBox                            @"Ratingbox_bt.png"
#define imgButton                               @"button.png"
#define imgPhotoButton                          @"photo_bt.png"
#define imgChoosePhotoButton                    @"choose_bt.png"
#define imgCancelButton                         @"cancel_bt.png"
#define imgTopGray                              @"top_gray.png"
#define imgSelectAll                            @"select_all_bt.png"
#define imgDeactiveMessage                      @"massage_top_bt_320x77.png"
#define imgActiveMessage                        @"massage_top_bt_on_320x77.png"
#define imgTabCountActive                       @"tab_count_active.png"
#define imgTabCountNoActive                     @"tab_count_NOactive.png"
#define imgBehindSchedule                       @"icon_behind schedule.png"
#define imgAheadSchedule                        @"icon_ahead schedule.png"
#define imgBGAlert                              @"alert_big.png"
#define imgRoomStatusOOS                        @"oos.png"
#define imgBlockRoom                            @"block.png"
#define imgReleaseRoom                          @"icon_O.png"

//MARK: Flat Style iOS 7
#define imgBGAlertFlat                          @"alert_big_2.png"
#define imgLaundryCartButtonFlat                @"laundry_cart_bt_2.png"
#define imgTabCountActiveFlat                   @"tab_count_active_2.png"
#define imgTabCountNoActiveFlat                 @"tab_count_active_2.png"
#define imgDeactiveMessageFlat                  @"massage_top_bt_320x77_2.png"
#define imgActiveMessageFlat                    @"massage_top_bt_on_320x77_2.png"
#define imgYesBtnFlat                           @"yes_bt_2.png"
#define imgSelectAllFlat                        @"select_all_bt_2.png"
#define imgTopGrayFlat                          @"top_gray_2.png"
#define imgCancelButtonFlat                     @"cancel_bt_2.png"
#define imgChoosePhotoButtonFlat                @"choose_bt_2.png"
#define imgPhotoButtonFlat                      @"photo_bt_2.png"
#define imgNoBtnFlat                            @"no_bt_2.png"
#define imgButtonFlat                           @"button_2.png"
#define imgRatingBoxFlat                        @"Ratingbox_bt_2.png"
#define imgTopBtFlat                            @"top_bt_320x77_2.png"
#define imgRightRowFlat                         @"detail_59x60_2.png"
#define imgRightRowOpenFlat                     @"detail_open_59x60_2.png"
#define imgBackgroundSectionFlat                @"unassigned_room_zone_bt_2.png"
#define imgCheckedFlat                          @"btn_checked_2.png"
#define imgUnCheckFlat                          @"btn_uncheck_2.png"
#define imgBgBigBtnFlat                         @"big_bt_640x111_2.png"
#define imgMenuFindFlat                         @"menu2_find.png"
#define imgMenuHistoryFlat                      @"menu2_history.png"
#define imgMenuHomeFlat                         @"menu2_home.png"
#define imgMenuJobFlat                          @"menu2_job.png"
#define imgMenuLogoutFlat                       @"menu2_logout.png"
#define imgMenuMessageFlat                      @"menu2_message.png"
#define imgMenuPostingFlat                      @"menu2_posting.png"
#define imgMenuSettingFlat                      @"menu2_setting.png"
#define imgMenuSyncFlat                         @"menu2_syncnow.png"
#define imgMenuUnassignFlat                     @"menu2_unassign.png"
#define imgMenuLegendFlat                       @"menu2_legend.png"
#define imgMenuGuestInfo                        @"guest_info.png"
#define imgBtnCompletedAndInspectedFlat                 @"btn_bg-completed & inspected_2.png"
#define imgBtnCompletedAndInspectedClickedFlat          @"btn_bg-completed & inspected (onpress)_2.png"
#define imgBtnCompletedAndInspectedDisableFlat          @"btn_bg-completed & inspected (greyout)_2.png"
#define imgOptionSetting_Flat                   @"setting_bt_2.png"
#define imgOptionSettingGray_Flat               @"setting_bt_gray_2.png"
#define imgRushRoomFlat                         @"icon_rush room_2.png"
#define imgQueueRoomFlat                        @"icon_queue room_2.png"
#define imgCompleteFlat                         @"Icon_completed_2.png"
#define imgPendingFlat                          @"pending_icon_2.png"
#define imgDNDFlat                              @"Icon_DND_2.png"
#define imgDoubleLockFlat                       @"icon_Double_lock.png"
#define imgDeclinedServiceFlat                  @"Icon_Declined Service_2.png"
#define imgServiceLaterFlat                     @"Icon_Service later_2.png"
#define imgStopFlat                             @"Stop_46x45_2.png"
#define imgStartFlat                            @"Icon_Start_2.png"
#define ImageViewMessage_Flat                   @"adHoc_view_btn_2.png"
#define ImageSendMessage_Flat                   @"adHoc_send_btn_2.png"
#define imgActiveFlat                           @"active_2.png"

#define imgLoginButtonFlat @"btnlogin.png"

#define imgBgLoginFlat @"img_loginBG_2.png"
#define imgBtnSubmitFlat            @"btn_submit_2.png"
#define imgBtnSubmitClickedFlat     @"btn_submit (onpress)_2.png"
#define imgBtnSubmitDisableFlat     @"btn_submit (greyout)_2.png"
#define imgBtnViewLogFlat @"bt_299x58_2.png"
#define imgStartBtnFlat                             @"btn_start_2.png"
#define imgStartBtnClickedFlat                      @"btn_start (onpress)_2.png"
#define imgStartBtnDisableFlat                      @"btn_start (greyout)_2.png"
#define imgStopBtnFlat                              @"btn_stop_2.png"
#define imgStopBtnClickedFlat                       @"btn_stop (onpress)_2.png"
#define imgStopBtnDisableFlat                       @"btn_stop (disable)_2.png"
#define imgCleaningStatusBtnAndActionBtnFlat        @"btn_bg-cleaning status & action_2.png"
#define imgCleaningStatusBtnAndActionBtnClickedFlat @"btn_bg-cleaning status & action (onpress)_2.png"
#define imgCleaningStatusBtnAndActionBtnDisableFlat @"btn_bg-cleaning status & action (greyout)_2.png"
#define imgCompleteBtnFlat                          @"btn_complete_2.png"
#define imgCompleteBtnClickedFlat                   @"btn_complete (onpress)_2.png"
#define imgCompleteBtnDisableFlat                   @"btn_complete (greyout)_2.png"
#define imgReassignBtnFlat                          @"btn_Reassign_2.png"
#define imgReassignBtnClickedFlat                   @"btn_Reassign (onpress)_2.png"
#define imgReassignBtnDisableFlat                   @"btn_Reassign (grey out)_2.png"

#define imgStartBtnFlat                             @"btn_start_2.png"
#define imgStartBtnClickedFlat                      @"btn_start (onpress)_2.png"
#define imgStartBtnDisableFlat                      @"btn_start (greyout)_2.png"
#define imgStopBtnFlat                              @"btn_stop_2.png"
#define imgStopBtnClickedFlat                       @"btn_stop (onpress)_2.png"
#define imgCleaningStatusBtnAndActionBtnFlat        @"btn_bg-cleaning status & action_2.png"
#define imgCleaningStatusBtnAndActionBtnClickedFlat @"btn_bg-cleaning status & action (onpress)_2.png"
#define imgCleaningStatusBtnAndActionBtnDisableFlat @"btn_bg-cleaning status & action (greyout)_2.png"
#define imgCompleteBtnFlat                          @"btn_complete_2.png"
#define imgCompleteBtnClickedFlat                   @"btn_complete (onpress)_2.png"
#define imgCompleteBtnDisableFlat                   @"btn_complete (greyout)_2.png"
#define imgReassignBtnFlat                          @"btn_Reassign_2.png"
#define imgHistoryPostingBtnFlat                    @"btn_posting_history_2.png"
#define imgReassignBtnClickedFlat                   @"btn_Reassign (onpress)_2.png"
#define imgReassignBtnDisableFlat                   @"btn_Reassign (grey out)_2.png"
#define imgGuestInfoInactiveFlat                    @"top_bt_grey_240x58_2.png"
#define imgGuestInfoActiveFlat                      @"GuestInfo_2.png"
#define imgGuestInfoRoomAssignmentActiveFlat        @"GuestInfo2_2.png"
#define imgTimerNotStartFlat                        @"timer_gray_274x58_2.png"
#define imgTimerPlayFlat                            @"timer_play_274x58_2.png"
#define imgTimerPauseFlat                           @"timer_phuse_274x58_2.png"
#define imgTimerOverFlat                            @"timer_red_274x58_2.png"
#define imgFailIconFlat                             @"fail_icon_2.png"
#define imgPassIconFlat                             @"pass_icon_2.png"
#define imgYesIconFlat                              @"yes_icon2_2.png"

#define imgBtnSelectionPopupFlat                    @"btn_popup selection_2.png"
#define imgBtnSelectionPopupClickedFlat             @"btn_popup selection (onpress)_2.png"
#define imgBtnCancelPopupFlat                       @"btn_popup cancel_2.png"
#define imgBtnCancelPopupClickedFlat                @"btn_popup cancel (onpress)_2.png"
#define imgBgPopupBottomFlat                        @"bg_popup (bottom)_2.png"
#define imgBGPopupMidFlat                           @"bg_popup (mid)_2.png"
#define imgBGPopupTopFlat                           @"bg_popup (top)_2.png"

//MARK: Old Menu opaque
//#define imgMenuFind @"menu_find.png"
//#define imgMenuHistory @"menu_history.png"
//#define imgMenuHome @"menu_home.png"
//#define imgMenuJob @"menu_job.png"
//#define imgMenuLogout @"menu_logout.png"
//#define imgMenuMessage @"menu_message.png"
//#define imgMenuPosting @"menu_posting.png"
//#define imgMenuSetting @"menu_setting.png"
//#define imgMenuSync @"menu_syncnow.png"
//#define imgMenuUnassign @"menu_unassign.png"

//Transparent Menu for iOS 6 and older
#define imgMenuFind @"menu3_find.png"
#define imgMenuHistory @"menu3_history.png"
#define imgMenuHome @"menu3_home.png"
#define imgMenuJob @"menu3_job.png"
#define imgMenuLogout @"menu3_logout.png"
#define imgMenuMessage @"menu3_message.png"
#define imgMenuPTT @"menu_ptt"
#define imgMenuPosting @"menu3_posting.png"
#define imgMenuSetting @"menu3_setting.png"
#define imgMenuSync @"menu3_syncnow.png"
#define imgMenuUnassign @"menu3_unassign.png"
#define imgMenuLegend @"menu3_legend.png"

@end
