//
//  ehkDefinesV2.m
//  mHouseKeeping
//
//  Created by TMS on 2/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ehkDefinesV2.h"

@implementation ehkDefinesV2

const NSString *L_LF_BACKGROUND_IMAGE= @"bg.png";
const int TITLE_SIZE = 22;
NSString *FONT_NAVIGATION_BAR = @"Arial-BoldMT";
const int MESSAGE_DURATION = 60 * 5;
const int MESSAGE_INBOX_MAX = 99;

const NSString *ROOM_ASSIGNMENT_ROOM_ASSIGNMENT = @"ROOM_ASSIGNMENT_ROOM_ASSIGNMENT";
const NSString *ROOM_ASSIGNMENT_MANUAL_CREDIT = @"ROOM_ASSIGNMENT_RECLEANING_MANUAL_CREDIT";
const NSString *ROOM_ASSIGNMENT_CHECKLIST_REMARKS = @"ROOM_ASSIGNMENT_INSPECTION_CHECKLIST_REMARKS";
const NSString *SYSTEM_ENABLE_ENCRYPTED_QRCODE = @"SYSTEM_ENABLE_ENCRYPTED_QRCODE";
const NSString *ROOM_ASSIGNMENT_COMPLETED_ROOM = @"ROOM_ASSIGNMENT_COMPLETED_ROOM";
const NSString *ROOM_ASSIGNMENT_MANUAL_UPDATE_ROOM_STATUS = @"ROOM_ASSIGNMENT_MANUAL_UPDATE_ROOM_STATUS";
const NSString *ROOM_ASSIGNMENT_RESTRICTION_ASSIGNMENT = @"ROOM_ASSIGNMENT_RESTRICTION_ASSIGNMENT";
const NSString *ROOM_ASSIGNMENT_QR_CODE_SCANNER = @"ROOM_ASSIGNMENT_QR_CODE_SCAN";
const NSString *POSTING_LOST_AND_FOUND = @"POSTING_FUNCTION_LOST_AND_FOUND";
const NSString *POSTING_ENGINEERING = @"POSTING_FUNCTION_ENGINEERING";
const NSString *POSTING_LAUNDRY = @"POSTING_FUNCTION_LAUNDRY";
const NSString *POSTING_MINIBAR = @"POSTING_FUNCTION_MINIBAR";
const NSString *POSTING_LINEN = @"POSTING_FUNCTION_LINEN";
const NSString *POSTING_AMENITIES = @"POSTING_FUNCTION_AMENITIES";//Edit field name
const NSString *POSTING_PHYSICAL_CHECK = @"POSTING_FUNCTION_PHYSICAL_CHECK";
const NSString *ACTION_LOST_AND_FOUND = @"ACTION_LOSTFOUND";
const NSString *ACTION_ENGINEERING = @"ACTION_ENGINEERING";
const NSString *ACTION_LAUNDRY = @"ACTION_LAUNDRY";
const NSString *ACTION_MINIBAR = @"ACTION_ACTION_MINIBAR";
const NSString *ACTION_LINEN = @"ACTION_LINEN";
const NSString *ACTION_AMENITIES = @"ACTION_AMENITIES";
const NSString *ACTION_GUIDELINE = @"ACTION_GUIDELINE";
const NSString *ACTION_CHECKLIST = @"ACTION_CHECKLIST";
const NSString *MESSAGE_MESSAGE = @"MESSAGE_MESSAGE";
//Dung Phan - 2/3/2015
//Change field "FIND_ATTENDANT_PENDING_ROOM" to "FIND_ATTENDANT_PENDING_COMPLETED"
//const NSString *FIND_ATTENDANT_PENDING_ROOM = @"FIND_ATTENDANT_PENDING_ROOM";
const NSString *FIND_ATTENDANT_PENDING_ROOM = @"FIND_ATTENDANT_PENDING_COMPLETED";
const NSString *FIND_ROOM = @"FIND_ROOM";
const NSString *FIND_ROOM_REMARKS_UPDATE = @"FIND_ROOM_REMARKS_UPDATE";
const NSString *POSTING_FUNCTION_ACTION_HISTORY = @"POSTING_FUNCTION_ACTION_HISTORY";
const NSString *FIND_INSPECTION = @"FIND_INSPECTION";
const NSString *SETTING_SETTING = @"SETTING_SETTING";
const NSString *ADDTIONAL_JOB = @"ADDTIONAL_JOB";
const NSString *ROOM_DETAILS_ACTION_HISTORY = @"ROOM_DETAILS_ACTION_HISTORY";
const NSString *FIND_ACTION_HISTORY = @"FIND_ACTION_HISTORY";

const NSString *POSTING_FUNCTION_MINIBAR_NEW = @"POSTING_FUNCTION_MINIBAR_NEW";
const NSString *POSTING_FUNCTION_MINIBAR_USED = @"POSTING_FUNCTION_MINIBAR_USED";
const NSString *ROOM_DETAILS_ACTION_MINIBAR_NEW = @"ROOM_DETAILS_ACTION_MINIBAR_NEW";
const NSString *ROOM_DETAILS_ACTION_MINIBAR_USED = @"ROOM_DETAILS_ACTION_MINIBAR_USED";
const NSString *POSTING_FUNCTION_AMENITIES_QR_CODE = @"POSTING_FUNCTION_AMENITIES_QR_CODE";
const NSString *POSTING_FUNCTION_ENGINEERING_QR_CODE = @"POSTING_FUNCTION_ENGINEERING_QR_CODE";
const NSString *POSTING_FUNCTION_LINEN_QR_CODE = @"POSTING_FUNCTION_LINEN_QR_CODE";
const NSString *POSTING_FUNCTION_LOST_AND_FOUND_QR_CODE = @"POSTING_FUNCTION_LOST_AND_FOUND_QR_CODE";
const NSString *POSTING_FUNCTION_MINIBAR_QR_CODE = @"POSTING_FUNCTION_MINIBAR_QR_CODE";
const NSString *POSTING_FUNCTION_PHYSICAL_CHECK_QR_CODE = @"POSTING_FUNCTION_PHYSICAL_CHECK_QR_CODE";
@end
