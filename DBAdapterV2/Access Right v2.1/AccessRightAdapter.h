//
//  AccessRightAdapter.h
//  mHouseKeeping
//
//  Created by Khanh Phan on 2/25/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DatabaseAdapterV2.h"
#import "AccessRightModel.h"

@interface AccessRightAdapter : DatabaseAdapterV2
{
    
}

-(void) insertAccessRight:(AccessRightModel *) accessRightModel;
-(void) updateAccessRight:(AccessRightModel *) accessRightModel;
-(AccessRightModel *) loadAccessRightByUserId:(NSInteger) userId;
-(void) deleteAccessRightByUserId:(NSInteger) userId;

@end
