//
//  AccessRightAdapter.m
//  mHouseKeeping
//
//  Created by Khanh Phan on 2/25/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "AccessRightAdapter.h"
#import "DbDefinesV2.h"
#import "ehkDefines.h"
#import "LogFileManager.h"

@implementation AccessRightAdapter

-(void) insertAccessRight:(AccessRightModel *)accessRightModel
{
//    [LogFileManager logDebugMode:@"[Item 3] insertAccessRight Stack: %@",[NSThread callStackSymbols]];
    [LogFileManager logDebugMode:@"[Item 3] insertAccessRight: accessRightModel %@%d%@%d%@%d%@%d%@%d%@%d%@%d%@%d%@%d%@%d%@%d%@%d%@%d%@%d%@%d%@%d%@%d%@%d%@%d%@%d%@%d%@%d%@%d%@%d%@%d%@%d",
     @"\n userId: ",(int)accessRightModel.userId,
     @"\n roomAssignment: ",(int)accessRightModel.roomAssignment,
     @"\n completedRoom: ",(int)accessRightModel.completedRoom,
     @"\n manualUpdateRoomStatus: ",(int)accessRightModel.manualUpdateRoomStatus,
     @"\n restrictionAssignment: ",(int)accessRightModel.restrictionAssignment,
     @"\n QRCodeScanner: ",(int)accessRightModel.QRCodeScanner,
     @"\n postLostAndFound: ",(int)accessRightModel.postLostAndFound,
     @"\n postEngineering: ",(int)accessRightModel.postEngineering,
     @"\n postLaundry: ",(int)accessRightModel.postLaundry,
     @"\n postMinibar: ",(int)accessRightModel.postMinibar,
     @"\n postLinen: ",(int)accessRightModel.postLinen,
     @"\n postAmenities: ",(int)accessRightModel.postAmenities,
     @"\n postPhysicalCheck: ",(int)accessRightModel.postPhysicalCheck,
     @"\n actionLostAndFound: ",(int)accessRightModel.actionLostAndFound,
     @"\n actionEngineering: ",(int)accessRightModel.actionEngineering,
     @"\n actionLaundry: ",(int)accessRightModel.actionLaundry,
     @"\n actionMinibar: ",(int)accessRightModel.actionMinibar,
     @"\n actionLinen: ",(int)accessRightModel.actionLinen,
     @"\n actionAmenities: ",(int)accessRightModel.actionAmenities,
     @"\n actionGuideLine: ",(int)accessRightModel.actionGuideLine,
     @"\n actionCheckList: ",(int)accessRightModel.actionCheckList,
     @"\n message: ",(int)accessRightModel.message,
     @"\n findAttendantPendingCompleted: ",(int)accessRightModel.findAttendantPendingCompleted,
     @"\n findRoom: ",(int)accessRightModel.findRoom,
     @"\n findInspection: ",(int)accessRightModel.findInspection,
     @"\n setting: ",(int)accessRightModel.setting];

    NSString *sqlString = [[NSString alloc] initWithFormat:@"INSERT INTO %@ (%@, %@, %@, %@, %@ , %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@) %@",

                           @"access_right",

                           accessRight_userId,
                           accessRight_RoomAssignment_RoomAssignment,
                           accessRight_RoomAssignment_CompletedRoom,
                           accessRight_RoomAssignment_ManualUpdateRoomStatus,
                           accessRight_RoomAssignment_RestrictionAssignment,
                           accessRight_RoomAssignment_QRCodeScanner,
                           accessRight_Posting_LostAndFound,
                           accessRight_Posting_Engineering,
                           accessRight_Posting_Laundry,
                           accessRight_Posting_Minibar,
                           accessRight_Posting_Linen,
                           accessRight_Posting_Amenities,
                           accessRight_Posting_PhysicalCheck,
                           accessRight_Action_LostAndFound,
                           accessRight_Action_Engineering,
                           accessRight_Action_Laundry,
                           accessRight_Action_Minibar,
                           accessRight_Action_Linen,
                           accessRight_Action_Amenities,
                           accessRight_Action_GuideLine,
                           accessRight_Action_CheckList,
                           accessRight_Message,
                           accessRight_FindAttendantPendingCompleted,
                           accessRight_FindRoom,
                           accessRight_FindInspection,
                           accessRight_Setting,
                           
                           @"VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"];
    
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    reAccessCount ++;
}
    
    NSInteger  status;
    status = sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL);
    
    if(status != SQLITE_OK)
    {
        
    }
    
    sqlite3_bind_int (self.sqlStament, 1, (int)accessRightModel.userId);
    sqlite3_bind_int (self.sqlStament, 2, (int)accessRightModel.roomAssignment);
    sqlite3_bind_int (self.sqlStament, 3, (int)accessRightModel.completedRoom);
    sqlite3_bind_int (self.sqlStament, 4, (int)accessRightModel.manualUpdateRoomStatus);
    sqlite3_bind_int (self.sqlStament, 5, (int)accessRightModel.restrictionAssignment);
    sqlite3_bind_int (self.sqlStament, 6, (int)accessRightModel.QRCodeScanner);
    sqlite3_bind_int (self.sqlStament, 7, (int)accessRightModel.postLostAndFound);
    sqlite3_bind_int (self.sqlStament, 8, (int)accessRightModel.postEngineering);
    sqlite3_bind_int (self.sqlStament, 9, (int)accessRightModel.postLaundry);
    sqlite3_bind_int (self.sqlStament, 10, (int)accessRightModel.postMinibar);
    sqlite3_bind_int (self.sqlStament, 11, (int)accessRightModel.postLinen);
    sqlite3_bind_int (self.sqlStament, 12, (int)accessRightModel.postAmenities);
    sqlite3_bind_int (self.sqlStament, 13, (int)accessRightModel.postPhysicalCheck);
    sqlite3_bind_int (self.sqlStament, 14, (int)accessRightModel.actionLostAndFound);
    sqlite3_bind_int (self.sqlStament, 15, (int)accessRightModel.actionEngineering);
    sqlite3_bind_int (self.sqlStament, 16, (int)accessRightModel.actionLaundry);
    sqlite3_bind_int (self.sqlStament, 17, (int)accessRightModel.actionMinibar);
    sqlite3_bind_int (self.sqlStament, 18, (int)accessRightModel.actionLinen);
    sqlite3_bind_int (self.sqlStament, 19, (int)accessRightModel.actionAmenities);
    sqlite3_bind_int (self.sqlStament, 20,(int) accessRightModel.actionGuideLine);
    sqlite3_bind_int (self.sqlStament, 21, (int)accessRightModel.actionCheckList);
    sqlite3_bind_int (self.sqlStament, 22, (int)accessRightModel.message);
    sqlite3_bind_int (self.sqlStament, 23,(int) accessRightModel.findAttendantPendingCompleted);
    sqlite3_bind_int (self.sqlStament, 24, (int)accessRightModel.findRoom);
    sqlite3_bind_int (self.sqlStament, 25, (int)accessRightModel.findInspection);
    sqlite3_bind_int (self.sqlStament, 26, (int)accessRightModel.setting);
    
    status = sqlite3_step(self.sqlStament);
    
    if(SQLITE_DONE != status)
    {   
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return;
    }
    else
    {
        sqlite3_reset(self.sqlStament);
        return;
    }
    
}

-(void) updateAccessRight:(AccessRightModel *)accessRightModel
{
//    [LogFileManager logDebugMode:@"[Item 3] updateAccessRight Stack: %@",[NSThread callStackSymbols]];
    [LogFileManager logDebugMode:@"[Item 3] updateAccessRight: accessRightModel %@%d%@%d%@%d%@%d%@%d%@%d%@%d%@%d%@%d%@%d%@%d%@%d%@%d%@%d%@%d%@%d%@%d%@%d%@%d%@%d%@%d%@%d%@%d%@%d%@%d%@%d",
     @"\n userId: ",(int)accessRightModel.userId,
     @"\n roomAssignment: ",(int)accessRightModel.roomAssignment,
     @"\n completedRoom: ",(int)accessRightModel.completedRoom,
     @"\n manualUpdateRoomStatus: ",(int)accessRightModel.manualUpdateRoomStatus,
     @"\n restrictionAssignment: ",(int)accessRightModel.restrictionAssignment,
     @"\n QRCodeScanner: ",(int)accessRightModel.QRCodeScanner,
     @"\n postLostAndFound: ",(int)accessRightModel.postLostAndFound,
     @"\n postEngineering: ",(int)accessRightModel.postEngineering,
     @"\n postLaundry: ",(int)accessRightModel.postLaundry,
     @"\n postMinibar: ",(int)accessRightModel.postMinibar,
     @"\n postLinen: ",(int)accessRightModel.postLinen,
     @"\n postAmenities: ",(int)accessRightModel.postAmenities,
     @"\n postPhysicalCheck: ",(int)accessRightModel.postPhysicalCheck,
     @"\n actionLostAndFound: ",(int)accessRightModel.actionLostAndFound,
     @"\n actionEngineering: ",(int)accessRightModel.actionEngineering,
     @"\n actionLaundry: ",(int)accessRightModel.actionLaundry,
     @"\n actionMinibar: ",(int)accessRightModel.actionMinibar,
     @"\n actionLinen: ",(int)accessRightModel.actionLinen,
     @"\n actionAmenities: ",(int)accessRightModel.actionAmenities,
     @"\n actionGuideLine: ",(int)accessRightModel.actionGuideLine,
     @"\n actionCheckList: ",(int)accessRightModel.actionCheckList,
     @"\n message: ",(int)accessRightModel.message,
     @"\n findAttendantPendingCompleted: ",(int)accessRightModel.findAttendantPendingCompleted,
     @"\n findRoom: ",(int)accessRightModel.findRoom,
     @"\n findInspection: ",(int)accessRightModel.findInspection,
     @"\n setting: ",(int)accessRightModel.setting];

    NSString *sqlString = [[NSString alloc] initWithFormat:@"update %@ set %@ = ? ,%@ = ? , %@ = ? , %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ? where %@ = ? ",
                           @"access_right", 
                           
                           accessRight_RoomAssignment_RoomAssignment,
                           accessRight_RoomAssignment_CompletedRoom,
                           accessRight_RoomAssignment_ManualUpdateRoomStatus,
                           accessRight_RoomAssignment_RestrictionAssignment,
                           accessRight_RoomAssignment_QRCodeScanner,
                           accessRight_Posting_LostAndFound,
                           accessRight_Posting_Engineering,
                           accessRight_Posting_Laundry,
                           accessRight_Posting_Minibar,
                           accessRight_Posting_Linen,
                           accessRight_Posting_Amenities,
                           accessRight_Posting_PhysicalCheck,
                           accessRight_Action_LostAndFound,
                           accessRight_Action_Engineering,
                           accessRight_Action_Laundry,
                           accessRight_Action_Minibar,
                           accessRight_Action_Linen,
                           accessRight_Action_Amenities,
                           accessRight_Action_GuideLine,
                           accessRight_Action_CheckList,
                           accessRight_Message,
                           accessRight_FindAttendantPendingCompleted,
                           accessRight_FindRoom,
                           accessRight_FindInspection,
                           accessRight_Setting,
                           accessRight_userId];
    
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    reAccessCount ++;
}
    
    NSInteger status;
    status = sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL);
    
    if(status != SQLITE_OK)
    {
        
    }
    
    sqlite3_bind_int (self.sqlStament, 1, (int)accessRightModel.roomAssignment);
    sqlite3_bind_int (self.sqlStament, 2, (int)accessRightModel.completedRoom);
    sqlite3_bind_int (self.sqlStament, 3, (int)accessRightModel.manualUpdateRoomStatus);
    sqlite3_bind_int (self.sqlStament, 4, (int)accessRightModel.restrictionAssignment);
    sqlite3_bind_int (self.sqlStament, 5, (int)accessRightModel.QRCodeScanner);
    sqlite3_bind_int (self.sqlStament, 6, (int)accessRightModel.postLostAndFound);
    sqlite3_bind_int (self.sqlStament, 7, (int)accessRightModel.postEngineering);
    sqlite3_bind_int (self.sqlStament, 8, (int)accessRightModel.postLaundry);
    sqlite3_bind_int (self.sqlStament, 9, (int)accessRightModel.postMinibar);
    sqlite3_bind_int (self.sqlStament, 10, (int)accessRightModel.postLinen);
    sqlite3_bind_int (self.sqlStament, 11,(int) accessRightModel.postAmenities);
    sqlite3_bind_int (self.sqlStament, 12, (int)accessRightModel.postPhysicalCheck);
    sqlite3_bind_int (self.sqlStament, 13, (int)accessRightModel.actionLostAndFound);
    sqlite3_bind_int (self.sqlStament, 14, (int)accessRightModel.actionEngineering);
    sqlite3_bind_int (self.sqlStament, 15, (int)accessRightModel.actionLaundry);
    sqlite3_bind_int (self.sqlStament, 16, (int)accessRightModel.actionMinibar);
    sqlite3_bind_int (self.sqlStament, 17, (int)accessRightModel.actionLinen);
    sqlite3_bind_int (self.sqlStament, 18, (int)accessRightModel.actionAmenities);
    sqlite3_bind_int (self.sqlStament, 19, (int)accessRightModel.actionGuideLine);
    sqlite3_bind_int (self.sqlStament, 20, (int)accessRightModel.actionCheckList);
    sqlite3_bind_int (self.sqlStament, 21, (int)accessRightModel.message);
    sqlite3_bind_int (self.sqlStament, 22, (int)accessRightModel.findAttendantPendingCompleted);
    sqlite3_bind_int (self.sqlStament, 23, (int)accessRightModel.findRoom);
    sqlite3_bind_int (self.sqlStament, 24, (int)accessRightModel.findInspection);
    sqlite3_bind_int (self.sqlStament, 25, (int)accessRightModel.setting);
    sqlite3_bind_int (self.sqlStament, 26, (int)accessRightModel.userId);
    
    status =  sqlite3_step(self.sqlStament);
    
    if(SQLITE_DONE != status)
    {   
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return;
    }
    else
    {
        sqlite3_reset(self.sqlStament);
        return;
    }
    

}

-(AccessRightModel *) loadAccessRightByUserId:(NSInteger)userId
{
    NSString *sqlString = [[NSString alloc] initWithFormat:@"select %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ from %@ where %@ = %i",
                           
                           accessRight_userId,
                           accessRight_RoomAssignment_RoomAssignment,
                           accessRight_RoomAssignment_CompletedRoom,
                           accessRight_RoomAssignment_ManualUpdateRoomStatus,
                           accessRight_RoomAssignment_RestrictionAssignment,
                           accessRight_RoomAssignment_QRCodeScanner,
                           accessRight_Posting_LostAndFound,
                           accessRight_Posting_Engineering,
                           accessRight_Posting_Laundry,
                           accessRight_Posting_Minibar,
                           accessRight_Posting_Linen,
                           accessRight_Posting_Amenities,
                           accessRight_Posting_PhysicalCheck,
                           accessRight_Action_LostAndFound,
                           accessRight_Action_Engineering,
                           accessRight_Action_Laundry,
                           accessRight_Action_Minibar,
                           accessRight_Action_Linen,
                           accessRight_Action_Amenities,
                           accessRight_Action_GuideLine,
                           accessRight_Action_CheckList,
                           accessRight_Message,
                           accessRight_FindAttendantPendingCompleted,
                           accessRight_FindRoom,
                           accessRight_FindInspection,
                           accessRight_Setting,
                           
                           @"access_right",
                           accessRight_userId,
                           (int)userId
                           ];
    
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    reAccessCount ++;
}
    
    NSInteger status;
    status = sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL);
    
    if(status != SQLITE_OK)
    {
        
    }
    
    status =  sqlite3_step(self.sqlStament);
    
    AccessRightModel *accessRightModel = nil;
    
    while (status == SQLITE_ROW) 
    {
        accessRightModel = [[AccessRightModel alloc] init];
        accessRightModel.userId = sqlite3_column_int(self.sqlStament, 0);
        accessRightModel.roomAssignment = sqlite3_column_int(self.sqlStament, 1);
        accessRightModel.completedRoom = sqlite3_column_int(self.sqlStament, 2);
        accessRightModel.manualUpdateRoomStatus = sqlite3_column_int(self.sqlStament, 3);
        accessRightModel.restrictionAssignment = sqlite3_column_int(self.sqlStament, 4);
        accessRightModel.QRCodeScanner = sqlite3_column_int(self.sqlStament, 5);
        accessRightModel.postLostAndFound = sqlite3_column_int(self.sqlStament, 6);
        accessRightModel.postEngineering = sqlite3_column_int(self.sqlStament, 7);
        accessRightModel.postLaundry = sqlite3_column_int(self.sqlStament, 8);
        accessRightModel.postMinibar = sqlite3_column_int(self.sqlStament, 9);
        accessRightModel.postLinen = sqlite3_column_int(self.sqlStament, 10);
        accessRightModel.postAmenities = sqlite3_column_int(self.sqlStament, 11);
        accessRightModel.postPhysicalCheck = sqlite3_column_int(self.sqlStament, 12);
        accessRightModel.actionLostAndFound = sqlite3_column_int(self.sqlStament, 13);
        accessRightModel.actionEngineering = sqlite3_column_int(self.sqlStament, 14);
        accessRightModel.actionLaundry = sqlite3_column_int(self.sqlStament, 15);
        accessRightModel.actionMinibar = sqlite3_column_int(self.sqlStament, 16);
        accessRightModel.actionLinen = sqlite3_column_int(self.sqlStament, 17);
        accessRightModel.actionAmenities = sqlite3_column_int(self.sqlStament, 18);
        accessRightModel.actionGuideLine = sqlite3_column_int(self.sqlStament, 19);
        accessRightModel.actionCheckList = sqlite3_column_int(self.sqlStament, 20);
        accessRightModel.message = sqlite3_column_int(self.sqlStament, 21);
        accessRightModel.findAttendantPendingCompleted = sqlite3_column_int(self.sqlStament, 22);
        accessRightModel.findRoom = sqlite3_column_int(self.sqlStament, 23);
        accessRightModel.findInspection = sqlite3_column_int(self.sqlStament, 24);
        accessRightModel.setting = sqlite3_column_int(self.sqlStament, 25);
        
        status = sqlite3_step(self.sqlStament);
    }
    
    if (status== SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    sqlite3_reset(self.sqlStament);
//    [LogFileManager logDebugMode:@"[Item 3] loadAccessRightByUserId Stack: %@",[NSThread callStackSymbols]];
    [LogFileManager logDebugMode:@"[Item 3] loadAccessRightByUserId: accessRightModel ",
     @"\n userId: ",(int)accessRightModel.userId,
     @"\n roomAssignment: ",(int)accessRightModel.roomAssignment,
     @"\n completedRoom: ",(int)accessRightModel.completedRoom,
     @"\n manualUpdateRoomStatus: ",(int)accessRightModel.manualUpdateRoomStatus,
     @"\n restrictionAssignment: ",(int)accessRightModel.restrictionAssignment,
     @"\n QRCodeScanner: ",(int)accessRightModel.QRCodeScanner,
     @"\n postLostAndFound: ",(int)accessRightModel.postLostAndFound,
     @"\n postEngineering: ",(int)accessRightModel.postEngineering,
     @"\n postLaundry: ",(int)accessRightModel.postLaundry,
     @"\n postMinibar: ",(int)accessRightModel.postMinibar,
     @"\n postLinen: ",(int)accessRightModel.postLinen,
     @"\n postAmenities: ",(int)accessRightModel.postAmenities,
     @"\n postPhysicalCheck: ",(int)accessRightModel.postPhysicalCheck,
     @"\n actionLostAndFound: ",(int)accessRightModel.actionLostAndFound,
     @"\n actionEngineering: ",(int)accessRightModel.actionEngineering,
     @"\n actionLaundry: ",(int)accessRightModel.actionLaundry,
     @"\n actionMinibar: ",(int)accessRightModel.actionMinibar,
     @"\n actionLinen: ",(int)accessRightModel.actionLinen,
     @"\n actionAmenities: ",(int)accessRightModel.actionAmenities,
     @"\n actionGuideLine: ",(int)accessRightModel.actionGuideLine,
     @"\n actionCheckList: ",(int)accessRightModel.actionCheckList,
     @"\n message: ",(int)accessRightModel.message,
     @"\n findAttendantPendingCompleted: ",(int)accessRightModel.findAttendantPendingCompleted,
     @"\n findRoom: ",(int)accessRightModel.findRoom,
     @"\n findInspection: ",(int)accessRightModel.findInspection,
     @"\n setting: ",(int)accessRightModel.setting];

    return accessRightModel;
}

-(void) deleteAccessRightByUserId:(NSInteger)userId
{
    //NSInteger result=1;
    if(self.sqlStament == nil) {
        NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"delete from %@ where %@= ?",@"access_right",accessRight_userId];
		const char *sql=[sqlString UTF8String];
        
		if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
        {
            
        }
//			result=0;
	}
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    reAccessCount ++;
}
    
    sqlite3_bind_int(self.sqlStament, 1, (int)userId);
	
    NSInteger  status = sqlite3_step(self.sqlStament);
    
	if (SQLITE_DONE != status) {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }
}

@end
