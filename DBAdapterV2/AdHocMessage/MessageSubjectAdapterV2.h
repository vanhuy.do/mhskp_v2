//
//  MessageSubjectAdapterV2.h
//  mHouseKeeping
//
//  Created by TMS on 3/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DatabaseAdapterV2.h"
#import "MessageSubjectModelV2.h"
@interface MessageSubjectAdapterV2 : DatabaseAdapterV2
{}
-(int)insertMsgSubjectData:(MessageSubjectModelV2*) msgSubject ;
-(int)updateMsgSubjectData:(MessageSubjectModelV2*) msgSubject ;
-(int)deleteMsgSubjectData:(MessageSubjectModelV2*) msgSubject ;
-(MessageSubjectModelV2*)loadMsgSubjectData:(MessageSubjectModelV2*) msgSubject ;
-(NSMutableArray *) loadAllMsgSubject;
@end
