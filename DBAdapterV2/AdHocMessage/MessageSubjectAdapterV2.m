//
//  MessageSubjectAdapterV2.m
//  mHouseKeeping
//
//  Created by TMS on 3/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MessageSubjectAdapterV2.h"

@implementation MessageSubjectAdapterV2

-(int)insertMsgSubjectData:(MessageSubjectModelV2*) msgSubject
{
    NSString *sqlString = [[NSString alloc] initWithFormat:@"INSERT INTO %@ (%@, %@, %@, %@) %@",SUBJECT_MESSAGE_CATEGORIES, smc, smc_name, smc_name_lang,smc_image, @"VALUES(?,?,?,?)"];
   
    const char *sql=[sqlString UTF8String];
//    [sqlString release];
    if(sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
    {
        //NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(database));
    }
    
    sqlite3_bind_int(self.sqlStament, 1,(int)msgSubject.smc);
    sqlite3_bind_text(self.sqlStament, 2, [msgSubject.smcName UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 3, [msgSubject.smcNameLang UTF8String], -1, SQLITE_TRANSIENT);
    //
    int returnValue = -1;
    if(msgSubject.smcImage != nil) 
        returnValue = sqlite3_bind_blob(self.sqlStament, 4, [msgSubject.smcImage bytes], (int)[msgSubject.smcImage length], NULL);
    else
        returnValue = sqlite3_bind_blob(self.sqlStament, 4, nil, -1, NULL);
    
    NSInteger returnInt = 0;
    if(SQLITE_DONE != sqlite3_step(self.sqlStament))
    {   
        return (int)returnInt;
    }
    else
    {
       
        sqlite3_reset(self.sqlStament);
        returnInt = 1;
        return (int)returnInt;
    }
    

}
-(int)updateMsgSubjectData:(MessageSubjectModelV2*) msgSubject
{
    NSString *sqlString = [[NSString alloc] initWithFormat:@"UPDATE %@ SET  %@ = ?, %@ =?, %@ =? WHERE %@ = ?", SUBJECT_MESSAGE_CATEGORIES, smc_name, smc_name_lang, smc_image, smc];
    
    const char *sql = [sqlString UTF8String];
//    [sqlString release];
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
        
    }
    
    sqlite3_bind_text(self.sqlStament, 1, [msgSubject.smcName UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 2, [msgSubject.smcNameLang UTF8String], -1, SQLITE_TRANSIENT);
    //
    
    int returnValue = -1;
    
    if(msgSubject.smcImage != nil) 
        returnValue = sqlite3_bind_blob(self.sqlStament, 3, [msgSubject.smcImage bytes], (int)[msgSubject.smcImage length], NULL);
    else
        returnValue = sqlite3_bind_blob(self.sqlStament, 3, nil, -1, NULL);
    
    sqlite3_bind_int(self.sqlStament, 4,(int)msgSubject.smc);
    
    NSInteger returnInt = 0;
    
    if(SQLITE_DONE != sqlite3_step(self.sqlStament)) {        
        return (int)returnInt;
    } else { 
        returnInt = 1;
        
        return (int)returnInt;
    }

}
-(int)deleteMsgSubjectData:(MessageSubjectModelV2*) msgSubject 
{
    NSInteger result = 1;
    
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"DELETE FROM %@ WHERE %@ = ?", SUBJECT_MESSAGE_CATEGORIES, smc];
    
    const char *sql = [sqlString UTF8String];
//    [sqlString release];
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
        result = 0;
	
    sqlite3_bind_int(self.sqlStament, 1,(int)msgSubject.smc);
	
	if (SQLITE_DONE != sqlite3_step(self.sqlStament)) 
		result = 0;
    
    return (int)result;
}
-(MessageSubjectModelV2*)loadMsgSubjectData:(MessageSubjectModelV2*) msgSubject 
{ 
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@ FROM %@ WHERE %@ = ?", smc, smc_name, smc_name_lang, smc_image, SUBJECT_MESSAGE_CATEGORIES, smc];
    const char *sql =[sqlString UTF8String];
//    [sqlString release];
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {
          sqlite3_bind_int(self.sqlStament, 1,(int)msgSubject.smc);       
        while(sqlite3_step(sqlStament) == SQLITE_ROW) {
            msgSubject.smc = sqlite3_column_int(self.sqlStament, 0);
            if (sqlite3_column_text(self.sqlStament, 1)) {
                msgSubject.smcName=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
            }
            if (sqlite3_column_text(self.sqlStament, 2)) {
                msgSubject.smcNameLang=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
            }
          
            if (sqlite3_column_bytes(self.sqlStament, 3) > 0) {
                msgSubject.smcImage  = [NSData dataWithBytes:sqlite3_column_blob
                                            (self.sqlStament, 3) length:sqlite3_column_bytes(self.sqlStament, 3)];
            } 
            
        }
    }
    
    
    return msgSubject;
}
-(NSMutableArray *) loadAllMsgSubject
{
    NSMutableArray *msgSubjectArray=[[NSMutableArray alloc] init];
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@ FROM %@ ", smc, smc_name, smc_name_lang, smc_image, SUBJECT_MESSAGE_CATEGORIES];
    const char *sql =[sqlString UTF8String];
//    [sqlString release];
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {

        while(sqlite3_step(sqlStament) == SQLITE_ROW) {
            MessageSubjectModelV2 *msgSubject=[[MessageSubjectModelV2 alloc] init];
            msgSubject.smc = sqlite3_column_int(self.sqlStament, 0);
            if (sqlite3_column_text(self.sqlStament, 1)) {
                msgSubject.smcName=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
            }
            if (sqlite3_column_text(self.sqlStament, 2)) {
                msgSubject.smcNameLang=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
            }
            
            if (sqlite3_column_bytes(self.sqlStament, 3) > 0) {
                msgSubject.smcImage  = [NSData dataWithBytes:sqlite3_column_blob
                                        (self.sqlStament, 3) length:sqlite3_column_bytes(self.sqlStament, 3)];
            } 
            [msgSubjectArray addObject:msgSubject];
            
        }
    }
    
    
    return msgSubjectArray;

}

@end
