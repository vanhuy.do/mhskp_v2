//
//  MessageSubjectItemAdapterV2.h
//  mHouseKeeping
//
//  Created by TMS on 3/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DatabaseAdapterV2.h"
#import "MessageSubjectItemModelV2.h"
@interface MessageSubjectItemAdapterV2 : DatabaseAdapterV2
{}
-(int)insertMsgSubjectItemData:(MessageSubjectItemModelV2*) msgSubjectItem ;
-(int)updateMsgSubjectItemData:(MessageSubjectItemModelV2*) msgSubjectItem ;
-(int)deleteMsgSubjectItemData:(MessageSubjectItemModelV2*) msgSubjectItem ;
-(MessageSubjectItemModelV2*)loadMsgSubjectItemData:(MessageSubjectItemModelV2*) msgSubjectItem ;
-(NSMutableArray *) loadAllMsgSubjectItemByMsgSubject:(int) smicategory_id;

@end
