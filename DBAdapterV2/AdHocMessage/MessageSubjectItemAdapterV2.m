//
//  MessageSubjectItemAdapterV2.m
//  mHouseKeeping
//
//  Created by TMS on 3/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MessageSubjectItemAdapterV2.h"

@implementation MessageSubjectItemAdapterV2

-(int)insertMsgSubjectItemData:(MessageSubjectItemModelV2*) msgSubjectItem
{
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"INSERT INTO %@ (%@, %@, %@, %@, %@) %@",SUBJECT_MESSAGE_ITEMS, smi_id, smi_name, smi_name_lang,smi_category_id,smi_image, @"VALUES(?,?,?,?,?)"];
   
    const char *sql=[sqlString UTF8String];
//    [sqlString release];
    
    if(sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
    {
        //NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(database));
    }
    
    sqlite3_bind_int(self.sqlStament, 1,(int)msgSubjectItem.smiId);
    sqlite3_bind_text(self.sqlStament, 2, [msgSubjectItem.smiName UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 3, [msgSubjectItem.smiNameLang UTF8String], -1, SQLITE_TRANSIENT);
    //
    sqlite3_bind_int(self.sqlStament, 4,(int)msgSubjectItem.smiCategoryId);
    int returnValue = -1;
    
    if(msgSubjectItem.smiImage != nil) 
        returnValue = sqlite3_bind_blob(self.sqlStament, 5, [msgSubjectItem.smiImage bytes], (int)[msgSubjectItem.smiImage length], NULL);
    else
        returnValue = sqlite3_bind_blob(self.sqlStament, 5, nil, -1, NULL);
  
    NSInteger returnInt = 0;
    if(SQLITE_DONE != sqlite3_step(self.sqlStament))
    {   
        return (int)returnInt;
    }
    else
    {
        //  sqlite3_last_insert_rowid(self.database); 
        sqlite3_reset(self.sqlStament);
        returnInt = 1;
        return (int)returnInt;
    }
    

}
-(int)updateMsgSubjectItemData:(MessageSubjectItemModelV2*) msgSubjectItem
{
    NSString *sqlString = [[NSString alloc] initWithFormat:@"UPDATE %@ SET  %@ = ?, %@ = ?, %@ =?, %@ =? WHERE %@ = ?", SUBJECT_MESSAGE_ITEMS, smi_name, smi_name_lang, smi_category_id,smi_image, smi_id];
    
    const char *sql = [sqlString UTF8String];
//    [sqlString release];
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
        
    }
    
       sqlite3_bind_text(self.sqlStament, 1, [msgSubjectItem.smiName UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 2, [msgSubjectItem.smiNameLang UTF8String], -1, SQLITE_TRANSIENT);
  
    sqlite3_bind_int(self.sqlStament, 3,(int)msgSubjectItem.smiCategoryId);
    int returnValue = -1;
    
    if(msgSubjectItem.smiImage != nil) 
        returnValue = sqlite3_bind_blob(self.sqlStament, 4, [msgSubjectItem.smiImage bytes], (int)[msgSubjectItem.smiImage length], NULL);
    else
        returnValue = sqlite3_bind_blob(self.sqlStament, 4, nil, -1, NULL);
    
    sqlite3_bind_int(self.sqlStament, 5,(int)msgSubjectItem.smiId);

    NSInteger returnInt = 0;
    
    if(SQLITE_DONE != sqlite3_step(self.sqlStament)) {        
        return (int)returnInt;
    } else { 
        returnInt = 1;
        
        return (int)returnInt;
    }
    
}
-(int)deleteMsgSubjectItemData:(MessageSubjectItemModelV2*) msgSubjectItem
{
    NSInteger result = 1;
    
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"DELETE FROM %@ WHERE %@ = ?", SUBJECT_MESSAGE_ITEMS, smi_id];
    
    const char *sql = [sqlString UTF8String];
//    [sqlString release];
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
        result = 0;
	
    sqlite3_bind_int(self.sqlStament, 1,(int)msgSubjectItem.smiId);
	
	if (SQLITE_DONE != sqlite3_step(self.sqlStament)) 
		result = 0;
    
    return (int)result;

}
-(MessageSubjectItemModelV2*)loadMsgSubjectItemData:(MessageSubjectItemModelV2*) msgSubjectItem 
{ 
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ?", smi_id, smi_name, smi_name_lang, smi_category_id,smi_image, SUBJECT_MESSAGE_ITEMS, smi_id];
    const char *sql =[sqlString UTF8String];
//    [sqlString release];
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {
        sqlite3_bind_int(self.sqlStament, 1,(int)msgSubjectItem.smiId);        
        while(sqlite3_step(sqlStament) == SQLITE_ROW) {
            msgSubjectItem.smiId = sqlite3_column_int(self.sqlStament, 0);
            if (sqlite3_column_text(self.sqlStament, 1)) {
                msgSubjectItem.smiName=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
            }
            if (sqlite3_column_text(self.sqlStament, 2)) {
                msgSubjectItem.smiNameLang=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
            }
             msgSubjectItem.smiCategoryId = sqlite3_column_int(self.sqlStament, 3);
            if (sqlite3_column_bytes(self.sqlStament, 4) > 0) {
                msgSubjectItem.smiImage  = [NSData dataWithBytes:sqlite3_column_blob
                                       (self.sqlStament, 4) length:sqlite3_column_bytes(self.sqlStament, 4)];
            } 
        
        }
    }
    
    
    return msgSubjectItem;
}
-(NSMutableArray *) loadAllMsgSubjectItemByMsgSubject:(int) smicategory_id
{  

    NSMutableArray *subject_Item_Array=[[NSMutableArray alloc] init];
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ?", smi_id, smi_name, smi_name_lang, smi_category_id,smi_image, SUBJECT_MESSAGE_ITEMS, smi_category_id];
    
    const char *sql =[sqlString UTF8String];
//    [sqlString release];
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {
        sqlite3_bind_int(self.sqlStament, 1,smicategory_id);        
        while(sqlite3_step(sqlStament) == SQLITE_ROW) {
            MessageSubjectItemModelV2 *msgSubjectItem=[[MessageSubjectItemModelV2 alloc] init];
            msgSubjectItem.smiId = sqlite3_column_int(self.sqlStament, 0);
            if (sqlite3_column_text(self.sqlStament, 1)) {
                msgSubjectItem.smiName=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
            }
            if (sqlite3_column_text(self.sqlStament, 2)) {
                msgSubjectItem.smiNameLang=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
            }
            msgSubjectItem.smiCategoryId = sqlite3_column_int(self.sqlStament, 3);
            if (sqlite3_column_bytes(self.sqlStament, 4) > 0) {
                msgSubjectItem.smiImage  = [NSData dataWithBytes:sqlite3_column_blob
                                            (self.sqlStament, 4) length:sqlite3_column_bytes(self.sqlStament, 4)];
            } 
            
            [subject_Item_Array addObject:msgSubjectItem];
            
        }
    }
    
    
    return subject_Item_Array;
}
@end
