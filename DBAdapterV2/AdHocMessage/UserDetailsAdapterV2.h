//
//  UserDetailsAdapterV2.h
//  mHouseKeeping
//
//  Created by TMS on 3/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DatabaseAdapterV2.h"
#import "UserDetailsModelV2.h"
@interface UserDetailsAdapterV2 : DatabaseAdapterV2
{
    

}
-(int)insertUserDetailsData:(UserDetailsModelV2*) userDetailsData ;
-(int)updateUserDetailsData:(UserDetailsModelV2*) userDetailsData;
-(int)deleteUserDetailsData:(UserDetailsModelV2*) userDetailsData ;
// FM [20160330] - delete user Details in local database
-(int)deleteAllUserDetailsData ;

-(UserDetailsModelV2*)loadUserDetailsDataByUserId:(NSString *)userIdDetails;
-(BOOL)isExistUserDetailsData:(UserDetailsModelV2*) userDetailsData;

-(NSMutableArray *) loadAllUserDetailsDataByUserHotelId:(int) userHotelId;
-(NSMutableArray *) loadAllUserDetailsData;

-(NSMutableArray *) loadAllUserDetailsDataOfSupervisorId:(NSInteger) supervisorId;
-(UserDetailsModelV2 *) loadUserDetailsByUserName:(NSString *) userName;

@end
