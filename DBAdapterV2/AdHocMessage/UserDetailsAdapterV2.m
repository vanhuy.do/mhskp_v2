//
//  UserDetailsAdapterV2.m
//  mHouseKeeping
//
//  Created by TMS on 3/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UserDetailsAdapterV2.h"
#import "LogFileManager.h"
#import "DbDefinesV2.h"
@implementation UserDetailsAdapterV2

-(int)insertUserDetailsData:(UserDetailsModelV2*) userDetailsData 
{
    NSString *sqlString = [[NSString alloc] initWithFormat:@"INSERT INTO %@ (%@, %@, %@, %@, %@, %@, %@ ,%@, %@) %@",USER_DETAILS , usr_id, usr_list_id, usr_name, usr_hotel_id, usr_fullname,user_fullname_lang,usr_last_modified,usr_supervisor, user_supervisor_id, @"VALUES(?,?,?,?,?,?,?,?,?)"];
    const char *sql=[sqlString UTF8String];
    
    if(sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL) != SQLITE_OK){
        //NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(database));
    }
    
    sqlite3_bind_int(self.sqlStament, 1, (int)userDetailsData.userId);
     sqlite3_bind_text(self.sqlStament,2, [userDetailsData.userDetailsId UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament,3, [userDetailsData.userDetailsName UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_int(self.sqlStament, 4, (int)userDetailsData.userDetailsHotelId);
    sqlite3_bind_text(self.sqlStament,5, [userDetailsData.userDetailsFullName UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament,6, [userDetailsData.userDetailsFullNameLang UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament,7, [userDetailsData.userDetailsLastModified UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_int(self.sqlStament, 8, (int)userDetailsData.usrSupervisor);
    sqlite3_bind_int(sqlStament, 9,(int) userDetailsData.userSupervisorId);
    
    NSInteger returnInt = 0;
    if(SQLITE_DONE != sqlite3_step(self.sqlStament))
    {   
        return (int)returnInt;
    }
    else
    {
        sqlite3_reset(self.sqlStament);
        returnInt = 1;
        return (int)returnInt;
    }
    
    

}
-(int)updateUserDetailsData:(UserDetailsModelV2*) userDetailsData 
{
    NSString *sqlString = [[NSString alloc] initWithFormat:@"UPDATE %@ SET  %@ = ?,%@ = ?, %@ = ?, %@ = ?, %@ =?, %@ =?, %@ =?, %@ = ? WHERE %@ = '%@'", USER_DETAILS,usr_id, usr_name, usr_hotel_id,usr_fullname, user_fullname_lang,usr_last_modified,usr_supervisor,user_supervisor_id, usr_list_id,userDetailsData.userDetailsId];
    
    const char *sql = [sqlString UTF8String];
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
        
    }
    
    sqlite3_bind_int(sqlStament, 1, (int)userDetailsData.userId);
    sqlite3_bind_text(self.sqlStament,2, [userDetailsData.userDetailsName UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_int(self.sqlStament, 3, (int)userDetailsData.userDetailsHotelId);
    sqlite3_bind_text(self.sqlStament,4, [userDetailsData.userDetailsFullName UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament,5, [userDetailsData.userDetailsFullNameLang UTF8String], -1, SQLITE_TRANSIENT);
    
    sqlite3_bind_text(self.sqlStament,6, [userDetailsData.userDetailsLastModified UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_int(self.sqlStament, 7, (int)userDetailsData.usrSupervisor);
    sqlite3_bind_int(self.sqlStament, 8, (int)userDetailsData.userSupervisorId);
    sqlite3_bind_text(self.sqlStament,9, [userDetailsData.userDetailsId UTF8String], -1, SQLITE_TRANSIENT);
    
    NSInteger returnInt = 0;
    
    if(SQLITE_DONE != sqlite3_step(self.sqlStament)) {        
        return (int)returnInt;
    } else { 
        returnInt = 1;
        
        return (int)returnInt;
    }
    

}

-(int)deleteUserDetailsData:(UserDetailsModelV2*) userDetailsData 
{
    NSInteger result = 1;
    
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"DELETE FROM %@ WHERE %@ = ?", USER_DETAILS,usr_id ];
    
    const char *sql = [sqlString UTF8String];
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
        result = 0;
	
	
    sqlite3_bind_int(self.sqlStament, 1, 
                    (int) userDetailsData.userId);
    
	
	if (SQLITE_DONE != sqlite3_step(self.sqlStament)) 
		result = 0;
    
    return (int)result;

}

// FM [20160330] - delete user Details in local database
-(int)deleteAllUserDetailsData
{
    NSInteger result = 1;
    
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"DELETE FROM %@", USER_DETAILS ];
    
    const char *sql = [sqlString UTF8String];
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
        result = 0;
    
    
    //sqlite3_bind_text(self.sqlStament, -1);
    
    
    if (SQLITE_DONE != sqlite3_step(self.sqlStament))
        result = 0;
    
    return (int)result;
    
}

-(UserDetailsModelV2*)loadUserDetailsDataByUserId:(NSString *)userIdDetails
{
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@,%@,%@, %@, %@, %@, %@ ,%@, %@ FROM %@ WHERE %@ = '%@'", usr_id,usr_list_id, usr_name, usr_hotel_id,usr_fullname,user_fullname_lang, usr_last_modified,usr_supervisor, user_supervisor_id, USER_DETAILS, usr_list_id,userIdDetails];
    const char *sql =[sqlString UTF8String];
    
    UserDetailsModelV2 *userDetailsData = nil;
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {
        while(sqlite3_step(sqlStament) == SQLITE_ROW) {
            userDetailsData = [[UserDetailsModelV2 alloc] init];
            
            userDetailsData.userId = sqlite3_column_int(self.sqlStament, 0);
            if (sqlite3_column_text(self.sqlStament, 1)) {
                userDetailsData.userDetailsId=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
            }
            if (sqlite3_column_text(self.sqlStament, 2)) {
                userDetailsData.userDetailsName=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
            }
            userDetailsData.userDetailsHotelId=sqlite3_column_int(self.sqlStament, 3);
            
            if (sqlite3_column_text(self.sqlStament, 4)) {
                userDetailsData.userDetailsFullName=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
            }
            if (sqlite3_column_text(self.sqlStament, 5)) {
                userDetailsData.userDetailsFullNameLang=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
            }
            if (sqlite3_column_text(self.sqlStament, 6)) {
                userDetailsData.userDetailsLastModified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 6)];
            }
            
                       
            userDetailsData.usrSupervisor = sqlite3_column_int(self.sqlStament, 7);
            
            userDetailsData.userSupervisorId = sqlite3_column_int(sqlStament, 8);
        }
    }
    
    
    return userDetailsData;

}

-(BOOL)isExistUserDetailsData:(UserDetailsModelV2*) userDetailsData {
 NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT count(1) FROM %@ WHERE %@ = ? AND %@ = ?",
                        USER_DETAILS, usr_id,usr_supervisor];
    
    const char *sql =[sqlString UTF8String];
    BOOL isExist = NO;
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) == SQLITE_OK){
        sqlite3_bind_int(self.sqlStament, 1,  (int)userDetailsData.userId);
        sqlite3_bind_int(self.sqlStament, 2,  (int)userDetailsData.usrSupervisor);
        while(sqlite3_step(sqlStament) == SQLITE_ROW) {
            if(sqlite3_column_int(self.sqlStament, 0)>0){
                isExist = YES;
            }
        }
    }
    return isExist;
}

-(NSMutableArray *) loadAllUserDetailsDataByUserHotelId:(int) userHotelId
{
    if([LogFileManager isLogConsole]) {
        NSLog(@"hotelid = %d", userHotelId);
    }
    NSMutableArray *userDetailsArray=[[NSMutableArray alloc] init];

    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@,%@, %@, %@, %@, %@ ,%@, %@ FROM %@ WHERE %@ = ?", usr_id, usr_list_id, usr_name, usr_hotel_id,usr_fullname,user_fullname_lang, usr_last_modified,usr_supervisor, user_supervisor_id, USER_DETAILS, usr_hotel_id];
    const char *sql =[sqlString UTF8String];
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {
        
        sqlite3_bind_int(self.sqlStament, 1, userHotelId);
        while(sqlite3_step(sqlStament) == SQLITE_ROW) {
            UserDetailsModelV2 *userDetailsData=[[UserDetailsModelV2 alloc] init];
            userDetailsData.userId = sqlite3_column_int(self.sqlStament, 0);
            if (sqlite3_column_text(self.sqlStament, 1)) {
                userDetailsData.userDetailsId=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
            }
            if (sqlite3_column_text(self.sqlStament, 2)) {
                userDetailsData.userDetailsName=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
            }
            userDetailsData.userDetailsHotelId=sqlite3_column_int(self.sqlStament, 3);
            
            if (sqlite3_column_text(self.sqlStament, 4)) {
                userDetailsData.userDetailsFullName=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
            }
            if (sqlite3_column_text(self.sqlStament, 5)) {
                userDetailsData.userDetailsFullNameLang=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
            }
            if (sqlite3_column_text(self.sqlStament, 6)) {
                userDetailsData.userDetailsLastModified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 6)];
            }
            userDetailsData.usrSupervisor = sqlite3_column_int(self.sqlStament, 7);
            
            userDetailsData.userSupervisorId = sqlite3_column_int(sqlStament, 8);
            
            [userDetailsArray addObject:userDetailsData];
            
        }
    }
    
    
    return userDetailsArray;

}
-(NSMutableArray *) loadAllUserDetailsData
{
    NSMutableArray *userDetailsArray=[[NSMutableArray alloc] init];
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@ ,%@ ,%@, %@ FROM %@ ", usr_id, usr_list_id, usr_name, usr_hotel_id,usr_fullname,user_fullname_lang, usr_last_modified,usr_supervisor, user_supervisor_id, USER_DETAILS];
    const char *sql =[sqlString UTF8String];
    
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {
        
        while(sqlite3_step(sqlStament) == SQLITE_ROW) {
            UserDetailsModelV2 *userDetailsData=[[UserDetailsModelV2 alloc] init];
            userDetailsData.userId = sqlite3_column_int(self.sqlStament, 0);
            if (sqlite3_column_text(self.sqlStament, 1)) {
                userDetailsData.userDetailsId=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
            }
            if (sqlite3_column_text(self.sqlStament, 2)) {
                userDetailsData.userDetailsName=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
            }
            userDetailsData.userDetailsHotelId=sqlite3_column_int(self.sqlStament, 3);
            
            if (sqlite3_column_text(self.sqlStament, 4)) {
                userDetailsData.userDetailsFullName=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
            }
            if (sqlite3_column_text(self.sqlStament, 5)) {
                userDetailsData.userDetailsFullNameLang=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
            }
            if (sqlite3_column_text(self.sqlStament, 6)) {
                userDetailsData.userDetailsLastModified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 6)];
            }
            userDetailsData.usrSupervisor = sqlite3_column_int(self.sqlStament, 7);
            
            userDetailsData.userSupervisorId = sqlite3_column_int(sqlStament, 8);
            
            [userDetailsArray addObject:userDetailsData];
            
        }
    }
    
    return userDetailsArray;
    
}

-(NSMutableArray *)loadAllUserDetailsDataOfSupervisorId:(NSInteger)supervisorId {
    NSMutableArray *userDetailsArray = [NSMutableArray array];
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@ ,%@ , %@, %@ FROM %@ WHERE %@ = %d",
                           usr_id,
                           usr_list_id,
                           usr_name, 
                           usr_hotel_id, 
                           usr_fullname, 
                           user_fullname_lang, 
                           usr_last_modified, 
                           usr_supervisor, 
                           user_supervisor_id,
                           
                           USER_DETAILS, 
                           
                           user_supervisor_id, 
                           (int)supervisorId
                           ];
    
    const char *sql = [sqlString UTF8String];
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {
        
        while(sqlite3_step(sqlStament) == SQLITE_ROW) {
            UserDetailsModelV2 *userDetailsData=[[UserDetailsModelV2 alloc] init];
            
            userDetailsData.userId = sqlite3_column_int(self.sqlStament, 0);
            
            if (sqlite3_column_text(self.sqlStament, 1)) {
                userDetailsData.userDetailsId=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
            }

            if (sqlite3_column_text(self.sqlStament, 2)) {
                userDetailsData.userDetailsName=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
            }
            
            userDetailsData.userDetailsHotelId=sqlite3_column_int(self.sqlStament, 3);
            
            if (sqlite3_column_text(self.sqlStament, 4)) {
                userDetailsData.userDetailsFullName=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
            }
            
            if (sqlite3_column_text(self.sqlStament, 5)) {
                userDetailsData.userDetailsFullNameLang=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
            }
            
            if (sqlite3_column_text(self.sqlStament, 6)) {
                userDetailsData.userDetailsLastModified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 6)];
            }
            
            userDetailsData.usrSupervisor = sqlite3_column_int(self.sqlStament, 7);
            
            userDetailsData.userSupervisorId = sqlite3_column_int(sqlStament, 8);
            
            [userDetailsArray addObject:userDetailsData];
        }
    }
    
    return userDetailsArray;
}

-(UserDetailsModelV2 *)loadUserDetailsByUserName:(NSString *)userName {    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@ ,%@ , %@, %@ FROM %@ WHERE %@ = '%@' OR %@ = '%@'",
                           usr_id,
                           usr_list_id,
                           usr_name, 
                           usr_hotel_id, 
                           usr_fullname, 
                           user_fullname_lang, 
                           usr_last_modified, 
                           usr_supervisor, 
                           user_supervisor_id,
                           
                           USER_DETAILS, 
                           
                           usr_fullname, 
                           userName,
                           
                           user_fullname_lang,
                           userName
                           ];
    
    const char *sql =[sqlString UTF8String];
//    NSString *st = [sqlString string]
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {
        
        while(sqlite3_step(sqlStament) == SQLITE_ROW) {
            UserDetailsModelV2 *userDetailsData=[[UserDetailsModelV2 alloc] init];
            
            userDetailsData.userId = sqlite3_column_int(self.sqlStament, 0);
            
            if (sqlite3_column_text(self.sqlStament, 1)) {
                userDetailsData.userDetailsName=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
            }

            if (sqlite3_column_text(self.sqlStament, 2)) {
                userDetailsData.userDetailsName=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
            }
            
            userDetailsData.userDetailsHotelId=sqlite3_column_int(self.sqlStament, 3);
            
            if (sqlite3_column_text(self.sqlStament, 4)) {
                userDetailsData.userDetailsFullName=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
            }
            
            if (sqlite3_column_text(self.sqlStament, 5)) {
                userDetailsData.userDetailsFullNameLang=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
            }
            
            if (sqlite3_column_text(self.sqlStament, 6)) {
                userDetailsData.userDetailsLastModified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 56)];
            }
            
            userDetailsData.usrSupervisor = sqlite3_column_int(self.sqlStament, 7);
            
            userDetailsData.userSupervisorId = sqlite3_column_int(sqlStament, 8);
            
            return userDetailsData;
        }
    }
    
    return nil;
}

@end
