//
//  UserListAdapterV2.h
//  mHouseKeeping
//
//  Created by TMS on 3/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DatabaseAdapterV2.h"
#import "UserListModelV2.h"
@interface UserListAdapterV2 : DatabaseAdapterV2
{
    

}
-(int)insertUserListData:(UserListModelV2*) userListData ;
-(int)updateUserListData:(UserListModelV2*) userListData ;
-(int)deleteUserListData:(UserListModelV2*) userListData ;
// FM [20160330] - delete user list in local database
-(int)deleteAllUserListData ;
-(UserListModelV2*)loadUserListDataByUserId:(int)userId;
-(BOOL)isExistUserListData:(UserListModelV2*) userListData;

-(NSMutableArray *) loadAllUserListDataByUserHotelId:(int) userHotelId;
-(NSMutableArray *) loadAllUserListData;

-(NSMutableArray *) loadAllUserListDataOfSupervisorId:(NSInteger) supervisorId;
-(UserListModelV2 *) loadUserListByUserName:(NSString *) userName;

@end
