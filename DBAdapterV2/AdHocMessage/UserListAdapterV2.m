//
//  UserListAdapterV2.m
//  mHouseKeeping
//
//  Created by TMS on 3/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UserListAdapterV2.h"
#import "LogFileManager.h"

@implementation UserListAdapterV2

-(int)insertUserListData:(UserListModelV2*) userListData 
{
    NSString *sqlString = [[NSString alloc] initWithFormat:@"INSERT INTO %@ (%@, %@, %@, %@, %@, %@ ,%@, %@) %@",USER_LIST , usr_id, usr_name, usr_hotel_id, usr_fullname,user_fullname_lang,usr_last_modified,usr_supervisor, user_supervisor_id, @"VALUES(?,?,?,?,?,?,? , ?)"];
    const char *sql=[sqlString UTF8String];
    
    if(sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL) != SQLITE_OK){
        //NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(database));
    }
    
    sqlite3_bind_int(self.sqlStament, 1, 
                     (int)userListData.userListId);
    sqlite3_bind_text(self.sqlStament,2, [userListData.userListName UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_int(self.sqlStament, 3, (int)userListData.userListHotelId);
    sqlite3_bind_text(self.sqlStament,4, [userListData.userListFullName UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament,5, [userListData.userListFullNameLang UTF8String], -1, SQLITE_TRANSIENT);

    sqlite3_bind_text(self.sqlStament,6, [userListData.userListLastModified UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_int(self.sqlStament, 7, (int)userListData.usrSupervisor);
    sqlite3_bind_int(sqlStament, 8,(int) userListData.userSupervisorId);
    
    NSInteger returnInt = 0;
    if(SQLITE_DONE != sqlite3_step(self.sqlStament))
    {   
        return (int)returnInt;
    }
    else
    {
        sqlite3_reset(self.sqlStament);
        returnInt = 1;
        return (int)returnInt;
    }
    
    

}
-(int)updateUserListData:(UserListModelV2*) userListData 
{
    NSString *sqlString = [[NSString alloc] initWithFormat:@"UPDATE %@ SET  %@ = ?, %@ = ?, %@ = ?, %@ =?, %@ =?, %@ =?, %@ = ? WHERE %@ = ?", USER_LIST, usr_name, usr_hotel_id,usr_fullname, user_fullname_lang,usr_last_modified,usr_supervisor,user_supervisor_id, usr_id];
    
    const char *sql = [sqlString UTF8String];
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
        
    }
    
  
    sqlite3_bind_text(self.sqlStament,1, [userListData.userListName UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_int(self.sqlStament, 2, (int)userListData.userListHotelId);
    sqlite3_bind_text(self.sqlStament,3, [userListData.userListFullName UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament,4, [userListData.userListFullNameLang UTF8String], -1, SQLITE_TRANSIENT);
    
    sqlite3_bind_text(self.sqlStament,5, [userListData.userListLastModified UTF8String], -1, SQLITE_TRANSIENT); 
    sqlite3_bind_int(self.sqlStament, 6, (int)userListData.usrSupervisor);
    sqlite3_bind_int(self.sqlStament, 7, (int)userListData.userSupervisorId);
    sqlite3_bind_int(sqlStament, 8, (int)userListData.userListId);
    
    NSInteger returnInt = 0;
    
    if(SQLITE_DONE != sqlite3_step(self.sqlStament)) {        
        return (int)returnInt;
    } else { 
        returnInt = 1;
        
        return (int)returnInt;
    }
    

}

-(int)deleteUserListData:(UserListModelV2*) userListData 
{
    NSInteger result = 1;
    
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"DELETE FROM %@ WHERE %@ = ?", USER_LIST,usr_id ];
    
    const char *sql = [sqlString UTF8String];
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
        result = 0;
	
	
    sqlite3_bind_int(self.sqlStament, 1, 
                    (int) userListData.userListId);
    
	
	if (SQLITE_DONE != sqlite3_step(self.sqlStament)) 
		result = 0;
    
    return (int)result;

}

// FM [20160330] - delete user list in local database
-(int)deleteAllUserListData
{
    NSInteger result = 1;
    
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"DELETE FROM %@", USER_LIST ];
    
    const char *sql = [sqlString UTF8String];
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
        result = 0;
    
    
    //sqlite3_bind_text(self.sqlStament, -1);
    
    
    if (SQLITE_DONE != sqlite3_step(self.sqlStament))
        result = 0;
    
    return (int)result;
    
}

-(UserListModelV2*)loadUserListDataByUserId:(int)userId
{
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@,%@, %@, %@, %@, %@ ,%@, %@ FROM %@ WHERE %@ = ?", usr_id, usr_name, usr_hotel_id,usr_fullname,user_fullname_lang, usr_last_modified,usr_supervisor, user_supervisor_id, USER_LIST, usr_id];
    const char *sql =[sqlString UTF8String];
    
    UserListModelV2 *userListData = nil;
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {
        sqlite3_bind_int(self.sqlStament, 1,  userId);

        
        while(sqlite3_step(sqlStament) == SQLITE_ROW) {
            userListData = [[UserListModelV2 alloc] init];
            
            userListData.userListId = sqlite3_column_int(self.sqlStament, 0);
            if (sqlite3_column_text(self.sqlStament, 1)) {
                userListData.userListName=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
            }
            userListData.userListHotelId=sqlite3_column_int(self.sqlStament, 2);
            
            if (sqlite3_column_text(self.sqlStament, 3)) {
                userListData.userListFullName=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
            }
            if (sqlite3_column_text(self.sqlStament, 4)) {
                userListData.userListFullNameLang=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
            }
            if (sqlite3_column_text(self.sqlStament, 5)) {
                userListData.userListLastModified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
            }
            
                       
            userListData.usrSupervisor = sqlite3_column_int(self.sqlStament, 6);
            
            userListData.userSupervisorId = sqlite3_column_int(sqlStament, 7);
        }
    }
    
    
    return userListData;

}

-(BOOL)isExistUserListData:(UserListModelV2*) userListData {
 NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT count(1) FROM %@ WHERE %@ = ? AND %@ = ?",
                        USER_LIST, usr_id,usr_supervisor];
    
    const char *sql =[sqlString UTF8String];
    BOOL isExist = NO;
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) == SQLITE_OK){
        sqlite3_bind_int(self.sqlStament, 1,  (int)userListData.userListId);
        sqlite3_bind_int(self.sqlStament, 2,  (int)userListData.usrSupervisor);
        while(sqlite3_step(sqlStament) == SQLITE_ROW) {
            if(sqlite3_column_int(self.sqlStament, 0)>0){
                isExist = YES;
            }
        }
    }
    return isExist;
}

-(NSMutableArray *) loadAllUserListDataByUserHotelId:(int) userHotelId
{
    if([LogFileManager isLogConsole]) {
        NSLog(@"hotelid = %d", userHotelId);
    }
    NSMutableArray *userListArray=[[NSMutableArray alloc] init];

    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@ ,%@, %@ FROM %@ WHERE %@ = ?", usr_id, usr_name, usr_hotel_id,usr_fullname,user_fullname_lang, usr_last_modified,usr_supervisor, user_supervisor_id, USER_LIST, usr_hotel_id];
    const char *sql =[sqlString UTF8String];
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {
        
        sqlite3_bind_int(self.sqlStament, 1, userHotelId);
        while(sqlite3_step(sqlStament) == SQLITE_ROW) {
            UserListModelV2 *userListData=[[UserListModelV2 alloc] init];
            userListData.userListId = sqlite3_column_int(self.sqlStament, 0);
            if (sqlite3_column_text(self.sqlStament, 1)) {
                userListData.userListName=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
            }
            userListData.userListHotelId=sqlite3_column_int(self.sqlStament, 2);
            
            if (sqlite3_column_text(self.sqlStament, 3)) {
                userListData.userListFullName=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
            }
            if (sqlite3_column_text(self.sqlStament, 4)) {
                userListData.userListFullNameLang=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
            }
            if (sqlite3_column_text(self.sqlStament, 5)) {
                userListData.userListLastModified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
            }
            userListData.usrSupervisor = sqlite3_column_int(self.sqlStament, 6);
            
            userListData.userSupervisorId = sqlite3_column_int(sqlStament, 7);
            
            [userListArray addObject:userListData];
            
        }
    }
    
    return userListArray;

}
-(NSMutableArray *) loadAllUserListData
{
    NSMutableArray *userListArray=[[NSMutableArray alloc] init];
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@ ,%@ ,%@, %@ FROM %@ ORDER BY %@, %@", usr_id, usr_name, usr_hotel_id,usr_fullname,user_fullname_lang, usr_last_modified,usr_supervisor, user_supervisor_id, USER_LIST, usr_fullname, user_fullname_lang];
    const char *sql =[sqlString UTF8String];
    
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {
        
        while(sqlite3_step(sqlStament) == SQLITE_ROW) {
            UserListModelV2 *userListData=[[UserListModelV2 alloc] init];
            userListData.userListId = sqlite3_column_int(self.sqlStament, 0);
            if (sqlite3_column_text(self.sqlStament, 1)) {
                userListData.userListName=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
            }
            userListData.userListHotelId=sqlite3_column_int(self.sqlStament, 2);
            
            if (sqlite3_column_text(self.sqlStament, 3)) {
                userListData.userListFullName=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
            }
            if (sqlite3_column_text(self.sqlStament, 4)) {
                userListData.userListFullNameLang=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
            }
            if (sqlite3_column_text(self.sqlStament, 5)) {
                userListData.userListLastModified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
            }
            userListData.usrSupervisor = sqlite3_column_int(self.sqlStament, 6);
            
            userListData.userSupervisorId = sqlite3_column_int(sqlStament, 7);
            
            [userListArray addObject:userListData];
            
        }
    }
    
    return userListArray;
    
}

-(NSMutableArray *)loadAllUserListDataOfSupervisorId:(NSInteger)supervisorId {
    NSMutableArray *userListArray = [NSMutableArray array];
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@ ,%@ , %@, %@ FROM %@ WHERE %@ = %d", 
                           usr_id, 
                           usr_name, 
                           usr_hotel_id, 
                           usr_fullname, 
                           user_fullname_lang, 
                           usr_last_modified, 
                           usr_supervisor, 
                           user_supervisor_id,
                           
                           USER_LIST, 
                           
                           user_supervisor_id, 
                           (int)supervisorId
                           ];
    
    const char *sql = [sqlString UTF8String];
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {
        
        while(sqlite3_step(sqlStament) == SQLITE_ROW) {
            UserListModelV2 *userListData=[[UserListModelV2 alloc] init];
            
            userListData.userListId = sqlite3_column_int(self.sqlStament, 0);
            
            if (sqlite3_column_text(self.sqlStament, 1)) {
                userListData.userListName=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
            }
            
            userListData.userListHotelId=sqlite3_column_int(self.sqlStament, 2);
            
            if (sqlite3_column_text(self.sqlStament, 3)) {
                userListData.userListFullName=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
            }
            
            if (sqlite3_column_text(self.sqlStament, 4)) {
                userListData.userListFullNameLang=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
            }
            
            if (sqlite3_column_text(self.sqlStament, 5)) {
                userListData.userListLastModified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
            }
            
            userListData.usrSupervisor = sqlite3_column_int(self.sqlStament, 6);
            
            userListData.userSupervisorId = sqlite3_column_int(sqlStament, 7);
            
            [userListArray addObject:userListData];
        }
    }
    
    return userListArray;
}

-(UserListModelV2 *)loadUserListByUserName:(NSString *)userName {    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@ ,%@ , %@, %@ FROM %@ WHERE %@ = '%@' OR %@ = '%@'", 
                           usr_id, 
                           usr_name, 
                           usr_hotel_id, 
                           usr_fullname, 
                           user_fullname_lang, 
                           usr_last_modified, 
                           usr_supervisor, 
                           user_supervisor_id,
                           
                           USER_LIST, 
                           
                           usr_fullname, 
                           userName,
                           
                           user_fullname_lang,
                           userName
                           ];
    
    const char *sql =[sqlString UTF8String];
//    NSString *st = [sqlString string]
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {
        
        while(sqlite3_step(sqlStament) == SQLITE_ROW) {
            UserListModelV2 *userListData=[[UserListModelV2 alloc] init];
            
            userListData.userListId = sqlite3_column_int(self.sqlStament, 0);
            
            if (sqlite3_column_text(self.sqlStament, 1)) {
                userListData.userListName=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
            }
            
            userListData.userListHotelId=sqlite3_column_int(self.sqlStament, 2);
            
            if (sqlite3_column_text(self.sqlStament, 3)) {
                userListData.userListFullName=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
            }
            
            if (sqlite3_column_text(self.sqlStament, 4)) {
                userListData.userListFullNameLang=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
            }
            
            if (sqlite3_column_text(self.sqlStament, 5)) {
                userListData.userListLastModified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
            }
            
            userListData.usrSupervisor = sqlite3_column_int(self.sqlStament, 6);
            
            userListData.userSupervisorId = sqlite3_column_int(sqlStament, 7);
            
            return userListData;
        }
    }
    
    return nil;
}

@end
