//
//  CheckListAdapterV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 3/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CheckListModelV2.h"
#import "DatabaseAdapterV2.h"
#import "DbDefinesV2.h"
#import "UserManagerV2.h"

@interface CheckListAdapterV2 : DatabaseAdapterV2{
    
}
-(int)insertCheckListData:(CheckListModelV2*) checkList;
-(int)updateCheckListData:(CheckListModelV2*) checkList;
-(int)deleteCheckListData:(CheckListModelV2*) checkList;
-(CheckListModelV2 *)loadCheckListData:(CheckListModelV2*) checkList;
-(CheckListModelV2 *)loadCheckListModelByRoomIdAndUsrIDData:(CheckListModelV2 *)checkList;
-(NSMutableArray *)loadAllCheckListByCurrentUserData;

@end

