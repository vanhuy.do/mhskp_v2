//
//  CheckListAdapterV2.m
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 3/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CheckListAdapterV2.h"
#import "ehkDefines.h"

@implementation CheckListAdapterV2

#pragma mark - CheckListModel Adapter Methods
-(int)insertCheckListData:(CheckListModelV2*) checkList
{
    NSInteger returnInt = 0;
    if(self.sqlStament == nil) {
        NSString *sqlString = [[NSString alloc] initWithFormat:@"INSERT INTO %@ (%@, %@, %@, %@, %@, %@) %@",CHECKLIST,chklist_id, chklist_room_id,chklist_user_id,chklist_inspect_date,chklist_status, chklist_core,@"VALUES(?,?,?,?,?,?)"];
        
        const char *sql=[sqlString UTF8String];

        NSInteger reAccessCount = 0;
        RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
            reAccessCount ++;
        }
        
		if(sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL) != SQLITE_OK){
        }
        
        sqlite3_bind_int (self.sqlStament, 1, (int)checkList.chkListId);
        sqlite3_bind_text(self.sqlStament, 2, [checkList.chkListRoomId UTF8String], -1,SQLITE_TRANSIENT);
        sqlite3_bind_int (self.sqlStament, 3, (int)checkList.chkListUserId );
        sqlite3_bind_text(self.sqlStament, 4, [checkList.chkInspectDate UTF8String], -1,SQLITE_TRANSIENT);
        sqlite3_bind_int (self.sqlStament, 5, (int)checkList.chkListStatus);
        sqlite3_bind_int (self.sqlStament, 6, (int)checkList.chkCore);
        
        NSInteger temp = sqlite3_step(self.sqlStament);

        if(SQLITE_DONE != temp){
            if (SQLITE_BUSY == temp) {
                if (reAccessCount <= REACCESS_MAX_TIMES) {
                    goto RE_ACCESS_DB;
                }
            }
            return (int)returnInt;
        }
        else
        {
            sqlite3_last_insert_rowid(self.database); 
            
            returnInt = checkList.chkListId;
        }
    }
            
    return (int)returnInt;
}

-(int)updateCheckListData:(CheckListModelV2*) checkList
{
    NSInteger returnInt = 0;
    if(self.sqlStament == nil) {
        NSString *sqlString = [[NSString alloc] initWithFormat:@"UPDATE %@ SET  %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ? WHERE %@ = ? AND %@ = ?", CHECKLIST, chklist_user_id, chklist_room_id, chklist_inspect_date, chklist_status, chklist_post_status, chklist_core, chklist_id, chklist_user_id];
        
        const char *sql = [sqlString UTF8String];

        NSInteger reAccessCount = 0;
        RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
            reAccessCount ++;
        }

		if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
        {
        }
        
        sqlite3_bind_int (self.sqlStament, 1, (int)checkList.chkListUserId);
        sqlite3_bind_text(self.sqlStament, 2 , [checkList.chkListRoomId UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(self.sqlStament, 3, [checkList.chkInspectDate UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_int (self.sqlStament, 4, (int)checkList.chkListStatus);
        sqlite3_bind_int (self.sqlStament, 5, (int)checkList.chkPostStatus);
        sqlite3_bind_int (self.sqlStament, 6, (int)checkList.chkCore);
        sqlite3_bind_int (self.sqlStament, 7, (int)checkList.chkListId);
        sqlite3_bind_int (self.sqlStament, 8, (int)checkList.chkListUserId);
        
        NSInteger status = sqlite3_step(self.sqlStament);
        if(SQLITE_DONE != status)
        {
            if (SQLITE_BUSY == status) {
                if (reAccessCount <= REACCESS_MAX_TIMES) {
                    goto RE_ACCESS_DB;
                }
            }
            return (int)returnInt;
        }
        else
        { 
            returnInt = 1;
        }
    }
    
    return (int)returnInt;
}

-(int)deleteCheckListData:(CheckListModelV2*) checkList
{
    NSInteger result = 1;
    if(self.sqlStament == nil) {
        NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"DELETE FROM %@ WHERE %@ = ?",CHECKLIST,chklist_id];
		const char *sql = [sqlString UTF8String];

		if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
        {
            
        }
//			result = 0;
	}
    
	NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    sqlite3_bind_int(self.sqlStament, 1,(int)checkList.chkListId);
	
    NSInteger status = sqlite3_step(self.sqlStament);
	if (SQLITE_DONE != status) {
        if (SQLITE_BUSY == status) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        result = 0;
    }
    else{
        result = 1;
    }

    return (int)result;
}

-(CheckListModelV2 *)loadCheckListData:(CheckListModelV2*) checkList
{
    //temporary save chkListUserId
    NSInteger chkListUserId = checkList.chkListUserId;
    //reset ckListUserId
    checkList.chkListUserId = 0;
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ? AND %@ = ?", chklist_id, chklist_room_id,chklist_user_id, chklist_inspect_date, chklist_status,chklist_core, CHECKLIST,chklist_id, chklist_user_id];
    const char *sql =[sqlString UTF8String];

    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSInteger status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);     
    if(status == SQLITE_OK) {
        
        sqlite3_bind_int(self.sqlStament, 1, (int)checkList.chkListId);
        sqlite3_bind_int(self.sqlStament, 2, (int)chkListUserId);

        status = sqlite3_step(sqlStament);
        
        while(status == SQLITE_ROW)
        {
            checkList.chkListId=sqlite3_column_int(self.sqlStament, 0);
            if (sqlite3_column_text(self.sqlStament, 1)) {
                checkList.chkListRoomId = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
            }
            checkList.chkListUserId=sqlite3_column_int(self.sqlStament, 2);
            if (sqlite3_column_text(self.sqlStament, 3)) {
                checkList.chkInspectDate=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
            }
            checkList.chkListStatus=sqlite3_column_int(self.sqlStament, 4);
            checkList.chkCore=sqlite3_column_int(self.sqlStament, 5);
            
            status = sqlite3_step(sqlStament);
        }
        
        if (SQLITE_BUSY == status) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }
    if (SQLITE_BUSY == status) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    return checkList;
}

-(CheckListModelV2 *)loadCheckListModelByRoomIdAndUsrIDData:(CheckListModelV2 *)checkList{
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ? AND %@ = ?", 
                           chklist_id,
                           chklist_room_id,
                           chklist_user_id,
                           chklist_inspect_date,
                           chklist_status,
                           chklist_post_status,
                           chklist_core, 
                           CHECKLIST,
                           chklist_room_id,
                           chklist_user_id];
    const char *sql =[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSInteger status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);

    if(status == SQLITE_OK) {
        
        sqlite3_bind_text(self.sqlStament, 1, [checkList.chkListRoomId UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_int(self.sqlStament, 2, (int)checkList.chkListUserId);
        
        status = sqlite3_step(sqlStament);
        while(status == SQLITE_ROW)
        {
            checkList.chkListId=sqlite3_column_int(self.sqlStament, 0);
            if (sqlite3_column_text(self.sqlStament, 1)) {
                checkList.chkListRoomId = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
            }
            checkList.chkListUserId=sqlite3_column_int(self.sqlStament, 2);
            if (sqlite3_column_text(self.sqlStament, 3)) {
                checkList.chkInspectDate=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
            }
            checkList.chkListStatus=sqlite3_column_int(self.sqlStament, 4);
            checkList.chkPostStatus=sqlite3_column_int(self.sqlStament, 5);
            checkList.chkCore=sqlite3_column_int(self.sqlStament, 6);
           
            status = sqlite3_step(sqlStament);
        }
        
        if (SQLITE_BUSY == status) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }
    if (SQLITE_BUSY == status) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    return checkList;
}

-(NSMutableArray *)loadAllCheckListByCurrentUserData{
    NSMutableArray *array = [NSMutableArray array];
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ?", chklist_id, chklist_room_id,chklist_user_id, chklist_inspect_date, chklist_status, CHECKLIST,chklist_user_id];
    const char *sql =[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSInteger status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) ;
    
    if(status== SQLITE_OK) {
        
        sqlite3_bind_int(self.sqlStament, 1, [UserManagerV2 sharedUserManager].currentUser.userId);
        status = sqlite3_step(sqlStament);
        
        while(status == SQLITE_ROW)
        {
            CheckListModelV2 *model = [[CheckListModelV2 alloc] init];
            model.chkListId = sqlite3_column_int(self.sqlStament, 0);
            if (sqlite3_column_text(self.sqlStament, 1)) {
                model.chkListRoomId = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
            }
            model.chkListUserId = sqlite3_column_int(self.sqlStament, 2);
            if (sqlite3_column_text(self.sqlStament, 3)) {
                model.chkInspectDate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(self.sqlStament, 3)];
            }
            model.chkPostStatus = sqlite3_column_int(self.sqlStament, 4);
            [array addObject:model];
            
            status = sqlite3_step(sqlStament);
            
        }
        
        if (SQLITE_BUSY == status) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                [array removeAllObjects];
                goto RE_ACCESS_DB;
            }
        }
    }
    if (SQLITE_BUSY == status) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    return array;
}


@end
