//
//  CheckListCategoriesAdapterV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 3/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CheckListContentTypeModelV2.h"
#import "DatabaseAdapterV2.h"
#import "DbDefinesV2.h"

@interface CheckListCategoriesAdapterV2 : DatabaseAdapterV2{
    
}
-(int)insertCheckListCategoriesData:(CheckListContentTypeModelV2*) checkListCategories;
-(int)updateCheckListCategoriesData:(CheckListContentTypeModelV2*) checkListCategories;
-(int)deleteCheckListCategoriesData:(CheckListContentTypeModelV2*) checkListCategories;
-(CheckListContentTypeModelV2 *)loadCheckListCategoriesData:(CheckListContentTypeModelV2*) checkListCategories;
-(NSMutableArray *) loadCheckListCategoriesByChkFormIDData:(NSInteger) checkListFormID;
-(CheckListContentTypeModelV2 *) loadCheckListCategoriesLatest;
@end
