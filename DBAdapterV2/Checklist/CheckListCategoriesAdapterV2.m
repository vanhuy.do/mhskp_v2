//
//  CheckListCategoriesAdapterV2.m
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 3/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CheckListCategoriesAdapterV2.h"
#import "ehkDefines.h"

@implementation CheckListCategoriesAdapterV2


-(int)insertCheckListCategoriesData:(CheckListContentTypeModelV2*) checkListCategories
{
    NSInteger returnInt = 0;
    if(self.sqlStament == nil) {
        NSString *sqlString = [[NSString alloc] initWithFormat:@"INSERT INTO %@ (%@, %@, %@, %@, %@) %@",CHECKLIST_CATEGORIES,chc_id, chc_form_id,chc_name,chc_name_lang,chc_last_modified, @"VALUES(?,?,?,?, ?)"];
        
        const char *sql=[sqlString UTF8String];
        
        NSInteger reAccessCount = 0;
        RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
            reAccessCount ++;
        }

		if(sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL) != SQLITE_OK){
        }
        sqlite3_bind_int (self.sqlStament, 1, (int)checkListCategories.chkContentId);
        sqlite3_bind_int (self.sqlStament, 2, (int)checkListCategories.chkTypeId );
        sqlite3_bind_text(self.sqlStament, 3, [checkListCategories.chkContentName UTF8String], -1,SQLITE_TRANSIENT);
        sqlite3_bind_text(self.sqlStament, 4, [checkListCategories.chkContentNameLang UTF8String], -1,SQLITE_TRANSIENT);
        sqlite3_bind_text(self.sqlStament, 5, [checkListCategories.chkLastModified UTF8String], -1,SQLITE_TRANSIENT);
        
        NSInteger temp = sqlite3_step(self.sqlStament);
        if(SQLITE_DONE != temp){
            if (SQLITE_BUSY == temp) {
                if (reAccessCount <= REACCESS_MAX_TIMES) {
                    goto RE_ACCESS_DB;
                }
            }
            return (int)returnInt;
        }
        else{
            sqlite3_last_insert_rowid(self.database); 
            returnInt = checkListCategories.chkContentId;
        }
    }
    return (int)returnInt;
}

-(int)updateCheckListCategoriesData:(CheckListContentTypeModelV2*) checkListCategories
{
    NSInteger returnInt = 0;
    if(self.sqlStament == nil) {
        NSString *sqlString = [[NSString alloc] initWithFormat:@"UPDATE %@ SET %@ = ?, %@ = ?, %@ = ?, %@ = ? WHERE %@ = ?", CHECKLIST_CATEGORIES, chc_form_id, chc_name, chc_name_lang, chc_last_modified,chc_id];
        
        const char *sql = [sqlString UTF8String];
        
        NSInteger reAccessCount = 0;
        RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
            reAccessCount ++;
        }
        
		if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
        {
        }
        sqlite3_bind_int (self.sqlStament, 1, (int)checkListCategories.chkTypeId);
        sqlite3_bind_text(self.sqlStament, 2, [checkListCategories.chkContentName UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(self.sqlStament, 3, [checkListCategories.chkContentNameLang UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(self.sqlStament, 4, [checkListCategories.chkLastModified UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_int (self.sqlStament, 5, (int)checkListCategories.chkContentId);
        
        NSInteger status = sqlite3_step(self.sqlStament);
        if(SQLITE_DONE != status)
        {
            if (SQLITE_BUSY == status) {
                if (reAccessCount <= REACCESS_MAX_TIMES) {
                    goto RE_ACCESS_DB;
                }
            }
            return (int)returnInt;
        }
        else
        { 
            returnInt = 1;
        }
    }
    return (int)returnInt;
}

-(int)deleteCheckListCategoriesData:(CheckListContentTypeModelV2*) checkListCategories
{
    NSInteger result = 1;
    if(self.sqlStament == nil) {
        NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"DELETE FROM %@ WHERE %@ = ?",CHECKLIST_CATEGORIES,chc_id];
		const char *sql = [sqlString UTF8String];
        
		if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
			result = 0;
	}
	
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    sqlite3_bind_int(self.sqlStament, 1,(int)checkListCategories.chkContentId);
	
    NSInteger status = sqlite3_step(self.sqlStament);
	if (SQLITE_DONE != status) {
        if (SQLITE_BUSY == status) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
		result = 0;
    }
	
    return (int)result;
}
-(CheckListContentTypeModelV2 *)loadCheckListCategoriesData:(CheckListContentTypeModelV2*) checkListCategories
{
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ?", chc_id, chc_form_id,chc_name, chc_name_lang,chc_last_modified, CHECKLIST_CATEGORIES,chc_id];
    const char *sql =[sqlString UTF8String];

    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSInteger status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        
        sqlite3_bind_int(self.sqlStament, 1, (int)checkListCategories.chkContentId);
        status = sqlite3_step(self.sqlStament);
        while(status == SQLITE_ROW)
        {
            checkListCategories.chkContentId = sqlite3_column_int(self.sqlStament, 0);
            checkListCategories.chkTypeId = sqlite3_column_int(self.sqlStament, 1);
            
            if (sqlite3_column_text(self.sqlStament, 2)) {
                 checkListCategories.chkContentName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];    
            }
            if (sqlite3_column_text(self.sqlStament, 3)) {
                checkListCategories.chkContentNameLang = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
            } 
            if (sqlite3_column_text(self.sqlStament, 4)) {
                checkListCategories.chkLastModified = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
            }    
            
            status = sqlite3_step(self.sqlStament);
            
        }
        
        if (SQLITE_BUSY == status) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }
    if (SQLITE_BUSY == status) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    return checkListCategories;
}

-(NSMutableArray *)loadCheckListCategoriesByChkFormIDData:(NSInteger)checkListFormID
{
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ?", chc_id, chc_form_id,chc_name, chc_name_lang,chc_last_modified, CHECKLIST_CATEGORIES,chc_form_id];
    const char *sql =[sqlString UTF8String];
   
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }

    NSInteger status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        
        sqlite3_bind_int(self.sqlStament, 1, (int)checkListFormID);
        
        status = sqlite3_step(sqlStament);
        while(status == SQLITE_ROW)
        {
            CheckListContentTypeModelV2 *model = [[CheckListContentTypeModelV2 alloc] init];
            
            model.chkContentId = sqlite3_column_int(self.sqlStament, 0);
            model.chkTypeId = sqlite3_column_int(self.sqlStament, 1);
            
            if (sqlite3_column_text(self.sqlStament, 2)) {
                model.chkContentName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];    
            }
            if (sqlite3_column_text(self.sqlStament, 3)) {
                model.chkContentNameLang = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
            }
            if (sqlite3_column_text(self.sqlStament, 4)) {
                model.chkLastModified = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
            }
            
            [array addObject:model];
            
            status = sqlite3_step(sqlStament);
        }
        
        if (SQLITE_BUSY == status) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                [array removeAllObjects];
                goto RE_ACCESS_DB;
            }
        }
    }
    if (SQLITE_BUSY == status) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    return array;
}

-(CheckListContentTypeModelV2 *)loadCheckListCategoriesLatest
{
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@ FROM %@ ORDER BY %@ DESC", chc_last_modified, CHECKLIST_CATEGORIES,chc_last_modified];
    const char *sql =[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSInteger status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    
    if(status == SQLITE_OK) {
        
        status = sqlite3_step(sqlStament);
        while(status == SQLITE_ROW)
        {
            CheckListContentTypeModelV2 *model = [[CheckListContentTypeModelV2 alloc] init];
            
            if (sqlite3_column_text(self.sqlStament, 0)) {
                model.chkLastModified = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 0)];
            }
            
            [array addObject:model];
            
            status = sqlite3_step(sqlStament);
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                [array removeAllObjects];
                goto RE_ACCESS_DB;
            }
        }
    }
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    CheckListContentTypeModelV2 *chkContentType = nil;
    if (array.count != 0) {
        chkContentType = [array objectAtIndex:0];

    }
      
    return chkContentType;

}

@end
