//
//  CheckListFormAdapterV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 3/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CheckListTypeModelV2.h"
#import "CheckListModelV2.h"
#import "DatabaseAdapterV2.h"
#import "DbDefinesV2.h"

@interface CheckListFormAdapterV2 : DatabaseAdapterV2{
    
}
-(int)insertCheckListFormData:(CheckListTypeModelV2*) checkListForm;
-(int)updateCheckListFormData:(CheckListTypeModelV2*) checkListForm;
-(int)deleteCheckListFormData:(CheckListTypeModelV2*) checkListForm;
-(CheckListTypeModelV2 *)loadCheckListFormData:(CheckListTypeModelV2 *) checkListForm;
-(NSMutableArray *) loadCheckListFormByCheckListFormScoreData:(NSInteger)chkFormScoreFormId;
-(CheckListTypeModelV2 *) loadCheckListFormLatestWithHotelId:(NSInteger) hotelId;
-(NSMutableArray *)loadAllCheckListFormData;
-(NSMutableArray *)loadCheckListFormDataByHotelId:(int)hotelId;
- (NSMutableArray *)loadChecklistFormByUserId:(int) userId roomTypeId:(int) roomTypeId;

@end
