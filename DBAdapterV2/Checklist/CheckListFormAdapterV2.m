//
//  CheckListFormAdapterV2.m
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 3/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CheckListFormAdapterV2.h"
#import "ehkDefines.h"

@implementation CheckListFormAdapterV2

-(int)insertCheckListFormData:(CheckListTypeModelV2 *)checkListForm{
    NSInteger returnInt = 0;
    if(self.sqlStament == nil) {
        NSString *sqlString = [[NSString alloc] initWithFormat:@"INSERT INTO %@ (%@, %@, %@, %@, %@, %@, %@, %@) %@",CHECKLIST_FORM,form_id, form_name,form_lang_name,form_kind,form_hotel_id,form_possible_point,form_passing_score, form_last_modified, @"VALUES(?,?,?,?,?,?,?, ?)"];
        
        const char *sql=[sqlString UTF8String];

        NSInteger reAccessCount = 0;
        RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
            reAccessCount ++;
        }
        
		if(sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL) != SQLITE_OK){
        }
        sqlite3_bind_int (self.sqlStament, 1, (int)checkListForm.chkTypeId);
        sqlite3_bind_text(self.sqlStament, 2, [checkListForm.chkTypeName UTF8String], -1,SQLITE_TRANSIENT);
        sqlite3_bind_text(self.sqlStament, 3, [checkListForm.chkTypeLangName UTF8String], -1,SQLITE_TRANSIENT);
        sqlite3_bind_int (self.sqlStament, 4, (int)checkListForm.chkType);
        sqlite3_bind_int (self.sqlStament, 5, (int)checkListForm.chkTypeHotelId);
        sqlite3_bind_int (self.sqlStament, 6, checkListForm.chkTypePercentageOverall);
        sqlite3_bind_int (self.sqlStament, 7, (int)checkListForm.chkTypePassingScore);
        sqlite3_bind_text(self.sqlStament, 8, [checkListForm.chkTypeLastModified UTF8String], -1,SQLITE_TRANSIENT);
        
        NSInteger temp = sqlite3_step(self.sqlStament);
        
        if(SQLITE_DONE != temp){
            if (SQLITE_BUSY == temp) {
                if (reAccessCount <= REACCESS_MAX_TIMES) {
                    goto RE_ACCESS_DB;

                }
            }
            return (int)returnInt;
        }
        else
        {
            sqlite3_last_insert_rowid(self.database); 
            returnInt = checkListForm.chkTypeId;
        }
    }
    return (int)returnInt;
}

-(int)updateCheckListFormData:(CheckListTypeModelV2 *)checkListForm{
    NSInteger returnInt = 0;
    if(self.sqlStament == nil) {
        NSString *sqlString = [[NSString alloc] initWithFormat:@"UPDATE %@ SET %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ? WHERE %@ = ?", CHECKLIST_FORM,form_name,form_lang_name,form_kind,form_hotel_id,form_possible_point,form_passing_score, form_last_modified, form_id];
        
        const char *sql = [sqlString UTF8String];

        NSInteger reAccessCount = 0;
        RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
            reAccessCount ++;
        }
        
		if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
        {
        }
        
        sqlite3_bind_text(self.sqlStament, 1, [checkListForm.chkTypeName UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(self.sqlStament, 2, [checkListForm.chkTypeLangName UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_int (self.sqlStament, 3, (int)checkListForm.chkType);
        sqlite3_bind_int (self.sqlStament, 4, (int)checkListForm.chkTypeHotelId);
        sqlite3_bind_int (self.sqlStament, 5, checkListForm.chkTypePercentageOverall);
        sqlite3_bind_int (self.sqlStament, 6, (int)checkListForm.chkTypePassingScore);
        sqlite3_bind_text(self.sqlStament, 7, [checkListForm.chkTypeLastModified UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_int (self.sqlStament, 8, (int)checkListForm.chkTypeId);
        
        NSInteger status = sqlite3_step(self.sqlStament);
        if(SQLITE_DONE != status)
        {
            if (SQLITE_BUSY == status) {
                if (reAccessCount <= REACCESS_MAX_TIMES) {
                    goto RE_ACCESS_DB;
                }
            }
            return (int)returnInt;
        }
        else
        { 
            returnInt = 1;
        }
    }
    return (int)returnInt;
}

-(int)deleteCheckListFormData:(CheckListTypeModelV2 *)checkListForm{
    
    NSInteger result = 1;
    if(self.sqlStament == nil) {
        NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"DELETE FROM %@ WHERE %@ = ?",CHECKLIST_FORM,form_id];
		const char *sql = [sqlString UTF8String];

		if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
			result = 0;
	}
	
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    sqlite3_bind_int(self.sqlStament, 1,(int)checkListForm.chkTypeId);
	
    NSInteger status = sqlite3_step(self.sqlStament);
	if (SQLITE_DONE != status) {
        if (SQLITE_BUSY == status) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
		result = 0;
	}
    
    return (int)result;
}

-(CheckListTypeModelV2 *)loadCheckListFormData:(CheckListTypeModelV2 *)checkListForm{
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ?",form_id, form_name, form_lang_name,form_kind, form_hotel_id,form_possible_point, form_passing_score, form_last_modified, CHECKLIST_FORM,form_id];
    const char *sql =[sqlString UTF8String];

    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSInteger status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        
        sqlite3_bind_int(self.sqlStament, 1, (int)checkListForm.chkTypeId);
        status = sqlite3_step(self.sqlStament);
        while(status == SQLITE_ROW)
        {
            checkListForm.chkTypeId = sqlite3_column_int(self.sqlStament, 0);
            
            if (sqlite3_column_text(sqlStament, 1)) {
                checkListForm.chkTypeName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
            }
            
            if (sqlite3_column_text(sqlStament, 2)) {
                checkListForm.chkTypeLangName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
            }
            
            checkListForm.chkType = sqlite3_column_int(self.sqlStament, 3);
            checkListForm.chkTypeHotelId = sqlite3_column_int(self.sqlStament, 4);
            checkListForm.chkTypePercentageOverall = sqlite3_column_int(self.sqlStament, 5);
            checkListForm.chkTypePassingScore = sqlite3_column_int(self.sqlStament, 6);
            
            if (sqlite3_column_text(sqlStament, 7)) {
                checkListForm.chkTypeLastModified = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 7)];
            }
               
            return checkListForm;
        }
        
        if (SQLITE_BUSY == status) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }
    if (SQLITE_BUSY == status) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    return checkListForm;
}

-(NSMutableArray *)loadCheckListFormByCheckListFormScoreData:(NSInteger)chkFormScoreFormId{
    NSMutableArray *array = [NSMutableArray array];
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ? ORDER BY %@ ASC", 
                           form_id, 
                           form_name, 
                           form_lang_name, 
                           form_kind, 
                           form_hotel_id, 
                           form_possible_point, 
                           form_passing_score, 
                           form_last_modified,
                           CHECKLIST_FORM, 
                           form_id,
                           form_kind
                           ];
    const char *sql =[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSInteger status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    
    if(status == SQLITE_OK) {
        
        sqlite3_bind_int(self.sqlStament, 1, (int)chkFormScoreFormId);
        
        status = sqlite3_step(sqlStament) ;
        while(status == SQLITE_ROW)
        {
            CheckListTypeModelV2 *model = [[CheckListTypeModelV2 alloc] init];
            
            model.chkTypeId = sqlite3_column_int(self.sqlStament, 0);
            
            if (sqlite3_column_text(sqlStament, 1)) {
                model.chkTypeName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
            }
            if (sqlite3_column_text(sqlStament, 2)) {
                model.chkTypeLangName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];  
            }
            
            model.chkType = sqlite3_column_int(self.sqlStament, 3);
            
            model.chkTypeHotelId = sqlite3_column_int(self.sqlStament, 4);
            
            model.chkTypePercentageOverall = sqlite3_column_int(self.sqlStament, 5);
            
            model.chkTypePassingScore = sqlite3_column_int(self.sqlStament, 6);
            
            if (sqlite3_column_text(sqlStament, 7)) {
                model.chkTypeLastModified = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 7)];  
            }
            
            [array addObject:model];
            
            status = sqlite3_step(sqlStament) ;
        }
        
        if (SQLITE_BUSY == status) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                [array removeAllObjects];
                goto RE_ACCESS_DB;
            }
        }
    }
    if (SQLITE_BUSY == status) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    return array;

}

-(CheckListTypeModelV2 *)loadCheckListFormLatestWithHotelId:(NSInteger)hotelId
{
    NSMutableArray *array = [NSMutableArray array];
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ? ORDER BY %@ DESC", 
                           form_id, 
                           form_name, 
                           form_lang_name, 
                           form_kind, 
                           form_hotel_id, 
                           form_possible_point, 
                           form_passing_score, 
                           form_last_modified,
                           CHECKLIST_FORM, 
                           form_hotel_id,
                           form_last_modified
                           ];
    const char *sql =[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSInteger status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    
    if(status == SQLITE_OK) {
        
        sqlite3_bind_int(self.sqlStament, 1, (int)hotelId);
        
        status = sqlite3_step(sqlStament);
        while(status == SQLITE_ROW)
        {
            CheckListTypeModelV2 *model = [[CheckListTypeModelV2 alloc] init];
            
            model.chkTypeId = sqlite3_column_int(self.sqlStament, 0);
            
            if (sqlite3_column_text(sqlStament, 1)) {
                model.chkTypeName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
            }
            if (sqlite3_column_text(sqlStament, 2)) {
                model.chkTypeLangName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];  
            }
            
            model.chkType = sqlite3_column_int(self.sqlStament, 3);
            
            model.chkTypeHotelId = sqlite3_column_int(self.sqlStament, 4);
            
            model.chkTypePercentageOverall = sqlite3_column_int(self.sqlStament, 5);
            
            model.chkTypePassingScore = sqlite3_column_int(self.sqlStament, 6);
            
            if (sqlite3_column_text(sqlStament, 7)) {
                model.chkTypeLastModified = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 7)];  
            }
            
            [array addObject:model];
            
            status = sqlite3_step(sqlStament);
        }
        
        if (SQLITE_BUSY == status) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                [array removeAllObjects];
                goto RE_ACCESS_DB;
            }
        }
    }
    if (SQLITE_BUSY == status) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    CheckListTypeModelV2 *chkListType = nil;
    if (array.count != 0 ) {
        chkListType = [array objectAtIndex:0];
    }
    
    return chkListType;
}

-(NSMutableArray *)loadAllCheckListFormData{
    NSMutableArray *array = [NSMutableArray array];
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@, %@ FROM %@ ORDER BY %@ ASC, %@ ASC",form_id, form_name, form_lang_name,form_kind, form_hotel_id,form_possible_point, form_passing_score, form_last_modified, CHECKLIST_FORM, form_kind, form_id];
    const char *sql =[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    reAccessCount ++;
}
    
    NSInteger status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        
        status = sqlite3_step(self.sqlStament);
        while(status == SQLITE_ROW) {               
            CheckListTypeModelV2 *model = [[CheckListTypeModelV2 alloc] init];
            
            model.chkTypeId = sqlite3_column_int(self.sqlStament, 0);
            
            if (sqlite3_column_text(sqlStament, 1)) {
                model.chkTypeName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
            }
            if (sqlite3_column_text(sqlStament, 2)) {
                model.chkTypeLangName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
            }
            
            model.chkType = sqlite3_column_int(self.sqlStament, 3);
            
            model.chkTypeHotelId = sqlite3_column_int(self.sqlStament, 4);
            
            model.chkTypePercentageOverall = sqlite3_column_int(self.sqlStament, 5);
            
            model.chkTypePassingScore = sqlite3_column_int(self.sqlStament, 6);
            
            if (sqlite3_column_text(sqlStament, 7)) {
                model.chkTypeLastModified = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 7)];
            }
            
            [array addObject:model];
            
            status = sqlite3_step(sqlStament);
        }
        
        if (SQLITE_BUSY == status) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }
    if (SQLITE_BUSY == status) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    return array;
}

-(NSMutableArray *)loadCheckListFormDataByHotelId:(int)hotelId
{
    NSMutableArray *array = [NSMutableArray array];
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@, %@ FROM %@ where %@ = %d ORDER BY %@ ASC, %@ ASC",
                           form_id,
                           form_name,
                           form_lang_name,
                           form_kind,
                           form_hotel_id,
                           form_possible_point,
                           form_passing_score,
                           form_last_modified,
                           CHECKLIST_FORM,
                           
                           form_hotel_id,
                           hotelId,
                           
                           form_kind,
                           form_id];
    const char *sql =[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    reAccessCount ++;
}
    
    NSInteger status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        
        status = sqlite3_step(self.sqlStament);
        while(status == SQLITE_ROW) {
            CheckListTypeModelV2 *model = [[CheckListTypeModelV2 alloc] init];
            
            model.chkTypeId = sqlite3_column_int(self.sqlStament, 0);
            
            if (sqlite3_column_text(sqlStament, 1)) {
                model.chkTypeName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
            }
            if (sqlite3_column_text(sqlStament, 2)) {
                model.chkTypeLangName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
            }
            
            model.chkType = sqlite3_column_int(self.sqlStament, 3);
            
            model.chkTypeHotelId = sqlite3_column_int(self.sqlStament, 4);
            
            model.chkTypePercentageOverall = sqlite3_column_int(self.sqlStament, 5);
            
            model.chkTypePassingScore = sqlite3_column_int(self.sqlStament, 6);
            
            if (sqlite3_column_text(sqlStament, 7)) {
                model.chkTypeLastModified = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 7)];
            }
            
            [array addObject:model];
            
            status = sqlite3_step(sqlStament);
        }
        
        if (SQLITE_BUSY == status) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }
    if (SQLITE_BUSY == status) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    return array;
}

- (NSMutableArray *)loadChecklistFormByUserId:(int) userId roomTypeId:(int) roomTypeId {
     NSMutableArray *array = [NSMutableArray array];
    NSString *sqlString = [NSString stringWithFormat:@"SELECT %@.%@, %@.%@, %@.%@, %@.%@, %@.%@, %@.%@, %@.%@, %@.%@, %@.%@ FROM %@ INNER JOIN %@ ON %@.%@ = %@.%@ WHERE %@.%@ = %i AND %@.%@ = %i ORDER BY %@ ASC", CHECKLIST_FORM, form_id, CHECKLIST_FORM, form_name, CHECKLIST_FORM, form_lang_name, CHECKLIST_FORM, form_kind, CHECKLIST_FORM, form_hotel_id,CHECKLIST_FORM, form_possible_point, CHECKLIST_FORM, form_passing_score, CHECKLIST_FORM, form_last_modified, CHECKLIST_ROOMTYPE, clrtUserId,CHECKLIST_FORM, CHECKLIST_ROOMTYPE, CHECKLIST_ROOMTYPE,clrtChecklistID, CHECKLIST_FORM, form_id, CHECKLIST_ROOMTYPE, clrtRoomTypeID, roomTypeId, CHECKLIST_ROOMTYPE, clrtUserId, userId, form_kind];
    const char *sql =[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    reAccessCount ++;
}
    
    NSInteger status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        
        status = sqlite3_step(self.sqlStament);
        while(status == SQLITE_ROW) {
            CheckListTypeModelV2 *model = [[CheckListTypeModelV2 alloc] init];
            
            model.chkTypeId = sqlite3_column_int(self.sqlStament, 0);
            
            if (sqlite3_column_text(sqlStament, 1)) {
                model.chkTypeName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
            }
            if (sqlite3_column_text(sqlStament, 2)) {
                model.chkTypeLangName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
            }
            
            model.chkType = sqlite3_column_int(self.sqlStament, 3);
            
            model.chkTypeHotelId = sqlite3_column_int(self.sqlStament, 4);
            
            model.chkTypePercentageOverall = sqlite3_column_int(self.sqlStament, 5);
            
            model.chkTypePassingScore = sqlite3_column_int(self.sqlStament, 6);
            
            if (sqlite3_column_text(sqlStament, 7)) {
                model.chkTypeLastModified = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 7)];
            }
            [array addObject:model];
            
            status = sqlite3_step(sqlStament);
        }
        
        if (SQLITE_BUSY == status) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }
    if (SQLITE_BUSY == status) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    return array;
}

@end
