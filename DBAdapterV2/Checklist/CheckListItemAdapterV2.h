//
//  CheckListItemAdapterV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 3/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CheckListDetailContentModelV2.h"
#import "DatabaseAdapterV2.h"
#import "DbDefinesV2.h"

@interface CheckListItemAdapterV2 : DatabaseAdapterV2{
    
}
-(int)insertCheckListItemData:(CheckListDetailContentModelV2*) checkListItem;
-(int)updateCheckListItemData:(CheckListDetailContentModelV2*) checkListItem;
-(int)deleteCheckListItemData:(CheckListDetailContentModelV2*) checkListItem;
-(NSInteger)getChecklistItemPassScoreByCheckListItemID:(NSInteger) checklistItemId;
-(NSInteger)getChecklistItemMandatoryScoreByCheckListItemID:(NSInteger) checklistItemId;
-(CheckListDetailContentModelV2 *)loadCheckListItemData:(CheckListDetailContentModelV2*) checkListItem;
-(NSMutableArray*) loadCheckListItemByChkCategoriesIdData:(NSInteger) checkListCategoriesId formId:(NSInteger)formId;
-(NSMutableArray*) loadCheckListItemByChkFormIdData:(NSInteger) checkListFormId;
-(CheckListDetailContentModelV2 *) loadCheckListItemLatest;
@end

