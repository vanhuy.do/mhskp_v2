
//
//  CheckListItemAdapterV2.m
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 3/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CheckListItemAdapterV2.h"
#import "ehkDefines.h"

@implementation CheckListItemAdapterV2

-(int)insertCheckListItemData:(CheckListDetailContentModelV2 *)checkListItem
{
    NSInteger returnInt = 0;
    if(self.sqlStament == nil) {
        NSString *sqlString = [[NSString alloc] initWithFormat:@"INSERT INTO %@ (%@, %@, %@, %@, %@, %@, %@, %@, %@) %@",CHECKLIST_ITEM,chkitem_id, chkitem_name,chkitem_room_type,chkitem_lang,chkitem_pass_score,chkitem_last_update,chkitem_category_id, chkitem_form_id, chkitem_mandatory_pass, @"VALUES(?,?,?,?,?,?,?,?,?)"];
        
        const char *sql=[sqlString UTF8String];
        
        NSInteger reAccessCount = 0;
        RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
            reAccessCount ++;
        }

		if(sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL) != SQLITE_OK){
        }
        sqlite3_bind_int (self.sqlStament, 1, (int)checkListItem.chkDetailContentId);
        sqlite3_bind_text(self.sqlStament, 2, [checkListItem.chkDetailContentName UTF8String], -1,SQLITE_TRANSIENT);
         sqlite3_bind_int (self.sqlStament, 3, (int)checkListItem.chkDetailContentRoomType);
        sqlite3_bind_text(self.sqlStament, 4, [checkListItem.chkDetailContentLang UTF8String], -1,SQLITE_TRANSIENT);
        sqlite3_bind_int (self.sqlStament, 5, (int)checkListItem.chkDetailContentPointInpected);
        sqlite3_bind_text(self.sqlStament, 6, [checkListItem.chkDetailContentLastUpdate UTF8String], -1,SQLITE_TRANSIENT);
        sqlite3_bind_int (self.sqlStament, 7, (int)checkListItem.chkContentId);
        sqlite3_bind_int (self.sqlStament, 8, (int)checkListItem.chkTypeId);
        sqlite3_bind_int (self.sqlStament, 9, (int)checkListItem.chkMandatoryPass);
        
        NSInteger temp = sqlite3_step(self.sqlStament);
        
        if(SQLITE_DONE != temp){
            if (SQLITE_BUSY == temp) {
                if (reAccessCount <= REACCESS_MAX_TIMES) {
                    goto RE_ACCESS_DB;
                }
            }
            return (int)returnInt;
        }
        else
        {
            sqlite3_last_insert_rowid(self.database); 
            
            return (int)checkListItem.chkDetailContentId;
        }
    }
    return (int)returnInt;
}

-(int)updateCheckListItemData:(CheckListDetailContentModelV2 *)checkListItem
{
    NSInteger returnInt = 0;
    if(self.sqlStament == nil) {
        NSString *sqlString = [[NSString alloc] initWithFormat:@"UPDATE %@ SET %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ? WHERE %@ = ?", CHECKLIST_ITEM,chkitem_name,chkitem_room_type,chkitem_lang,chkitem_pass_score,chkitem_last_update, chkitem_category_id,chkitem_form_id, chkitem_mandatory_pass, chkitem_id];
        
        const char *sql = [sqlString UTF8String];
        
        NSInteger reAccessCount = 0;
        RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
            reAccessCount ++;
        }
        
		if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
        {
        }
        
        sqlite3_bind_text(self.sqlStament, 1, [checkListItem.chkDetailContentName UTF8String], -1,SQLITE_TRANSIENT);
        sqlite3_bind_int (self.sqlStament, 2, (int)checkListItem.chkDetailContentRoomType);
        sqlite3_bind_text(self.sqlStament, 3, [checkListItem.chkDetailContentLang UTF8String], -1,SQLITE_TRANSIENT);
        sqlite3_bind_int (self.sqlStament, 4, (int)checkListItem.chkDetailContentPointInpected);
        sqlite3_bind_text(self.sqlStament, 5, [checkListItem.chkDetailContentLastUpdate UTF8String], -1,SQLITE_TRANSIENT);
        sqlite3_bind_int (self.sqlStament, 6, (int)checkListItem.chkContentId);
        sqlite3_bind_int (self.sqlStament, 7, (int)checkListItem.chkTypeId);
        sqlite3_bind_int (self.sqlStament, 8, (int)checkListItem.chkMandatoryPass);
        sqlite3_bind_int (self.sqlStament, 9, (int)checkListItem.chkDetailContentId);
        
        NSInteger status = sqlite3_step(self.sqlStament);
        if(SQLITE_DONE != status)
        {
            if (SQLITE_BUSY == status) {
                if (reAccessCount <= REACCESS_MAX_TIMES) {
                    goto RE_ACCESS_DB;
                }
            }
            return (int)returnInt;
        }
        else
        { 
            returnInt = 1;
            return (int)returnInt;
        }
    }
    return (int)returnInt;
}

-(int)deleteCheckListItemData:(CheckListDetailContentModelV2 *)checkListItem
{
    NSInteger result = 1;
    if(self.sqlStament == nil) {
        NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"DELETE FROM %@ WHERE %@ = ?",CHECKLIST_ITEM,chkitem_id];
		const char *sql = [sqlString UTF8String];

		if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
			result = 0;
	}
	
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    sqlite3_bind_int(self.sqlStament, 1,(int)checkListItem.chkDetailContentId);
	
    NSInteger status = sqlite3_step(self.sqlStament);
	if (SQLITE_DONE != status) {
		if (SQLITE_BUSY == status) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        result = 0;
    }
    return (int)result;

}

-(NSInteger)getChecklistItemPassScoreByCheckListItemID:(NSInteger) checklistItemId
{
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@ FROM %@ WHERE %@ = ?", chkitem_pass_score, CHECKLIST_ITEM, chkitem_id];
    int result = 0;
    const char *sql =[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    reAccessCount ++;
}
    
    NSInteger status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    
    if(status == SQLITE_OK) {
        
        sqlite3_bind_int(self.sqlStament, 1, (int)checklistItemId);
        
        status =  sqlite3_step(self.sqlStament);
        while(status == SQLITE_ROW)
        {
            result = sqlite3_column_int(self.sqlStament, 0);
            break;
        }
        
        if (SQLITE_BUSY == status) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }
    if (SQLITE_BUSY == status) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    return result;
}

-(NSInteger)getChecklistItemMandatoryScoreByCheckListItemID:(NSInteger) checklistItemId
{
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@ FROM %@ WHERE %@ = ?", chkitem_mandatory_pass, CHECKLIST_ITEM, chkitem_id];
    int result = 0;
    const char *sql =[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    reAccessCount ++;
}
    
    NSInteger status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    
    if(status == SQLITE_OK) {
        
        sqlite3_bind_int(self.sqlStament, 1, (int)checklistItemId);
        
        status =  sqlite3_step(self.sqlStament);
        while(status == SQLITE_ROW)
        {
            result = sqlite3_column_int(self.sqlStament, 0);
            break;
        }
        
        if (SQLITE_BUSY == status) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }
    if (SQLITE_BUSY == status) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    return result;
}

-(CheckListDetailContentModelV2 *)loadCheckListItemData:(CheckListDetailContentModelV2 *)checkListItem
{
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ?", chkitem_name, chkitem_room_type,chkitem_lang, chkitem_pass_score,chkitem_last_update,chkitem_category_id, chkitem_form_id, chkitem_mandatory_pass, CHECKLIST_ITEM,chkitem_id];
    
    const char *sql =[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSInteger status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
      
    if(status == SQLITE_OK) {
        
        sqlite3_bind_int(self.sqlStament, 1,(int) checkListItem.chkDetailContentId);
        
        status =  sqlite3_step(self.sqlStament);
        while(status == SQLITE_ROW)
        {
            
            if (sqlite3_column_text(self.sqlStament, 0)) {
                checkListItem.chkDetailContentName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 0)];    
            }
             
            checkListItem.chkDetailContentRoomType = sqlite3_column_int(self.sqlStament, 1);
            
            if (sqlite3_column_text(self.sqlStament, 2)) {
                checkListItem.chkDetailContentLang = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];    
            }
                  
            checkListItem.chkDetailContentPointInpected = sqlite3_column_int(self.sqlStament, 3);
            
            if (sqlite3_column_text(self.sqlStament, 4)) {
                checkListItem.chkDetailContentLastUpdate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];    
            }
            checkListItem.chkContentId = sqlite3_column_int(self.sqlStament, 5);
            checkListItem.chkTypeId = sqlite3_column_int(self.sqlStament, 6);
            checkListItem.chkMandatoryPass = sqlite3_column_int(self.sqlStament, 7);
            
            status = sqlite3_step(self.sqlStament);
            
        }
        
        if (SQLITE_BUSY == status) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }
    if (SQLITE_BUSY == status) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    return checkListItem;

}

-(NSMutableArray *)loadCheckListItemByChkCategoriesIdData:(NSInteger)checkListCategoriesId formId:(NSInteger)formId
{
    NSMutableArray *array = [NSMutableArray array];
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ? AND %@ = ?",
                           chkitem_id, 
                           chkitem_name, 
                           chkitem_room_type, 
                           chkitem_lang, 
                           chkitem_pass_score, 
                           chkitem_last_update,
                           chkitem_category_id,
                           chkitem_form_id,
                           chkitem_mandatory_pass,
                           CHECKLIST_ITEM, 
                           chkitem_category_id,
                           chkitem_form_id
                           ];
    const char *sql =[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSInteger status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    
    if(status == SQLITE_OK) {
        
        sqlite3_bind_int(self.sqlStament, 1,(int) checkListCategoriesId);
        sqlite3_bind_int(self.sqlStament, 2, (int)formId);
        status = sqlite3_step(self.sqlStament);
        while(status == SQLITE_ROW)
        {
            CheckListDetailContentModelV2 *model = [[CheckListDetailContentModelV2 alloc] init];
            
            model.chkDetailContentId = sqlite3_column_int(self.sqlStament, 0);
            
            if (sqlite3_column_text(sqlStament, 1)) {
                model.chkDetailContentName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
            }
            model.chkDetailContentRoomType = sqlite3_column_int(self.sqlStament, 2);
            if (sqlite3_column_text(sqlStament, 3)) {
                model.chkDetailContentLang = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
            }
            model.chkDetailContentPointInpected = sqlite3_column_int(self.sqlStament, 4);
            if (sqlite3_column_text(sqlStament, 5)) {
                model.chkDetailContentLastUpdate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];  
            }
            
            model.chkContentId = sqlite3_column_int(self.sqlStament, 6);
            model.chkTypeId = sqlite3_column_int(self.sqlStament, 7);
            model.chkMandatoryPass = sqlite3_column_int(self.sqlStament, 8);
            
            [array addObject:model];
            
            status = sqlite3_step(self.sqlStament);
        }
        
        if (SQLITE_BUSY == status) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                [array removeAllObjects];
                goto RE_ACCESS_DB;
            }
        }
    }
    if (SQLITE_BUSY == status) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    return array;

}

-(NSMutableArray *)loadCheckListItemByChkFormIdData:(NSInteger)checkListFormId
{
    NSMutableArray *array = [NSMutableArray array];
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ? ORDER BY %@",
                           chkitem_id, 
                           chkitem_name, 
                           chkitem_room_type, 
                           chkitem_lang, 
                           chkitem_pass_score, 
                           chkitem_last_update,
                           chkitem_category_id,
                           chkitem_form_id,
                           chkitem_mandatory_pass,
                           CHECKLIST_ITEM, 
                           chkitem_form_id,
                           chkitem_category_id
                           ];
    const char *sql =[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    NSInteger status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        
        sqlite3_bind_int(self.sqlStament, 1, (int)checkListFormId);
        
        status = sqlite3_step(self.sqlStament);
        while(status == SQLITE_ROW)
        {
            CheckListDetailContentModelV2 *model = [[CheckListDetailContentModelV2 alloc] init];
            
            model.chkDetailContentId = sqlite3_column_int(self.sqlStament, 0);
            
            if (sqlite3_column_text(sqlStament, 1)) {
                model.chkDetailContentName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
            }
            model.chkDetailContentRoomType = sqlite3_column_int(self.sqlStament, 2);
            if (sqlite3_column_text(sqlStament, 3)) {
                model.chkDetailContentLang = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
            }
            model.chkDetailContentPointInpected = sqlite3_column_int(self.sqlStament, 4);
            if (sqlite3_column_text(sqlStament, 5)) {
                model.chkDetailContentLastUpdate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];  
            }
            
            model.chkContentId = sqlite3_column_int(self.sqlStament, 6);
            model.chkTypeId = sqlite3_column_int(self.sqlStament, 7);
            model.chkMandatoryPass = sqlite3_column_int(self.sqlStament, 8);
            
            [array addObject:model];
            
            status = sqlite3_step(self.sqlStament);
        }
        
        if (SQLITE_BUSY == status) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                [array removeAllObjects];
                goto RE_ACCESS_DB;
            }
        }
    }
    if (SQLITE_BUSY == status) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    return array;
}

-(CheckListDetailContentModelV2 *)loadCheckListItemLatest
{
    NSMutableArray *array = [NSMutableArray array];
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@ FROM %@ ORDER BY %@ DESC", 
                           chkitem_last_update, 
                           CHECKLIST_ITEM, 
                           chkitem_last_update
                           ];
    const char *sql =[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSInteger status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        
        status = sqlite3_step(self.sqlStament);
        while(status == SQLITE_ROW)
        {
            CheckListDetailContentModelV2 *model = [[CheckListDetailContentModelV2 alloc] init];
            
            if (sqlite3_column_text(sqlStament, 0)) {
                model.chkDetailContentLastUpdate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 0)];  
            }
            
            [array addObject:model];
            
            status = sqlite3_step(self.sqlStament);
        }
        
        if (SQLITE_BUSY == status) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }
    if (SQLITE_BUSY == status) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    CheckListDetailContentModelV2 *chkDetailContent = nil;
    if (array.count != 0) {
        chkDetailContent = [array objectAtIndex:0];
    }
    
    return chkDetailContent;

}

@end
