//
//  CheckListItemCoreAdapterV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 3/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CheckListItemCoreDBModelV2.h"
#import "DatabaseAdapterV2.h"
#import "DbDefinesV2.h"

@interface CheckListItemCoreAdapterV2 : DatabaseAdapterV2{
    
}
-(int)insertCheckListItemCoreData:(CheckListItemCoreDBModelV2*) checkListItemCore;
-(int)updateCheckListItemCoreData:(CheckListItemCoreDBModelV2*) checkListItemCore;
-(int)deleteCheckListItemCoreData:(CheckListItemCoreDBModelV2*) checkListItemCore;
-(CheckListItemCoreDBModelV2 *)loadCheckListItemCoreData:(CheckListItemCoreDBModelV2*) checkListItemCore;
-(NSMutableArray *) loadCheckListItemCoreByChkListFormScoreIdData:(NSInteger) checkListFormScoreID;
-(int)updateCheckListItemCoreByChkListItemIDData:(CheckListItemCoreDBModelV2*) checkListItemCore;
-(void) deleteCheckListItemScoreByItemId:(NSInteger) itemId AndFormScoreId:(NSInteger) formScoreId;
-(int)deleteSubmittedChecklistItemsByChecklistFormId:(NSInteger)checklistFormId;
-(int)deleteAllSubmittedChecklistItems;
-(CheckListItemCoreDBModelV2 *)loadCheckListItemScoreByItemId:(NSInteger) itemId AndFormScoreId:(NSInteger) formScoreId;
-(CheckListItemCoreDBModelV2 *)loadCheckListItemScoreByItemId:(NSInteger) itemId;
-(NSMutableArray *) loadCheckListItemCoreByChkListFormScoreIdData:(NSInteger) checkListFormScoreID AndStatusPost:(NSInteger) statusPost;

@end
