//
//  CheckListItemCoreAdapterV2.m
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 3/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CheckListItemCoreAdapterV2.h"
#import "DbDefinesV2.h"
#import "ehkDefines.h"

@implementation CheckListItemCoreAdapterV2

-(int)insertCheckListItemCoreData:(CheckListItemCoreDBModelV2 *)checkListItemCore
{
    NSInteger returnInt = 0;
    if(self.sqlStament == nil) {
        NSString *sqlString = [[NSString alloc] initWithFormat:@"INSERT INTO %@ (%@, %@, %@, %@) %@", CHECKLIST_ITEM_CORE, cic_item_id, cic_core, cic_form_score, cic_post_status,  @"VALUES(?,?,?,?)"];

        const char *sql=[sqlString UTF8String];
        
        NSInteger reAccessCount = 0;
        RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
            reAccessCount ++;
        }

		if(sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL) != SQLITE_OK){
        }
        sqlite3_bind_int (self.sqlStament, 1, (int)checkListItemCore.chkItemCoreDetailContentId);
        sqlite3_bind_int (self.sqlStament, 2, (int)checkListItemCore.chkItemCore);
        sqlite3_bind_int (self.sqlStament, 3, (int)checkListItemCore.chkFormScore);
        sqlite3_bind_int (self.sqlStament, 4, (int)checkListItemCore.chkPostStatus);
        
        NSInteger temp = sqlite3_step(self.sqlStament);
    
        if(SQLITE_DONE != temp){
            if (SQLITE_BUSY == temp) {
                if (reAccessCount <= REACCESS_MAX_TIMES) {
                    goto RE_ACCESS_DB;
                }
            }
            return (int)returnInt;
        }
        else
        {
            sqlite3_last_insert_rowid(self.database); 
            
            return (int)checkListItemCore.chkItemCoreId;
        }
    }
    return (int)returnInt;
}

-(int)updateCheckListItemCoreData:(CheckListItemCoreDBModelV2 *)checkListItemCore
{
    NSInteger returnInt = 0;
    if(self.sqlStament == nil) {
        NSString *sqlString = [[NSString alloc] initWithFormat:@"UPDATE %@ SET %@ = ?, %@ = ?, %@ = ?, %@ = ? WHERE %@ = ?", CHECKLIST_ITEM_CORE,
            cic_item_id,
            cic_core,
            cic_form_score, 
            cic_post_status,
            cic_id
            ];
        
        const char *sql = [sqlString UTF8String];
        
        NSInteger reAccessCount = 0;
        RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
            reAccessCount ++;
        }
        
		if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
        {
        }
        
        sqlite3_bind_int (self.sqlStament, 1, (int)checkListItemCore.chkItemCoreDetailContentId);
        sqlite3_bind_int (self.sqlStament, 2, (int)checkListItemCore.chkItemCore);
        sqlite3_bind_int (self.sqlStament, 3, (int)checkListItemCore.chkFormScore);
        sqlite3_bind_int (self.sqlStament, 4, (int)checkListItemCore.chkPostStatus);
        sqlite3_bind_int (self.sqlStament, 5, (int)checkListItemCore.chkItemCoreId);
        
        NSInteger status = sqlite3_step(self.sqlStament);
        if(SQLITE_DONE != status)
        {
            if (SQLITE_BUSY == status) {
                if (reAccessCount <= REACCESS_MAX_TIMES) {
                    goto RE_ACCESS_DB;
                }
            }
            return (int)returnInt;
        }
        else
        { 
            returnInt = 1;
            return (int)returnInt;
        }
    }
    return (int)returnInt;
}

-(int)deleteCheckListItemCoreData:(CheckListItemCoreDBModelV2 *)checkListItemCore
{
    NSInteger result = 1;
    if(self.sqlStament == nil) {
        NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"DELETE FROM %@ WHERE %@ = ?",CHECKLIST_ITEM_CORE,cic_id];
		const char *sql = [sqlString UTF8String];
		if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
			result = 0;
	}
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
	
    sqlite3_bind_int(self.sqlStament, 1,(int)checkListItemCore.chkItemCoreId);
	
    NSInteger status = sqlite3_step(self.sqlStament);
	if (SQLITE_DONE != status) {
        if (SQLITE_BUSY == status) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
		result = 0;
    }
	
    return (int)result;
}

-(int)deleteAllSubmittedChecklistItems
{
    NSInteger result = 1;
    if(self.sqlStament == nil) {
        NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"DELETE FROM %@ WHERE %@ = ?", CHECKLIST_ITEM_CORE, cic_post_status];
        const char *sql = [sqlString UTF8String];
        if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
            result = 0;
    }
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    reAccessCount ++;
}
    sqlite3_bind_int(self.sqlStament, 1,(int)POST_STATUS_POSTED);
    
    NSInteger status = sqlite3_step(self.sqlStament);
    if (SQLITE_DONE != status) {
        if (SQLITE_BUSY == status) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        result = 0;
    }
    
    return (int)result;
}

-(int)deleteSubmittedChecklistItemsByChecklistFormId:(NSInteger)checklistFormId
{
    NSInteger result = 1;
    if(self.sqlStament == nil) {
        NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"DELETE FROM %@ WHERE %@ = ? AND %@ = ?", CHECKLIST_ITEM_CORE, cic_form_score, cic_post_status];
        const char *sql = [sqlString UTF8String];
        if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
            result = 0;
    }
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    reAccessCount ++;
}
    
    sqlite3_bind_int(self.sqlStament, 1,(int)checklistFormId);
    sqlite3_bind_int(self.sqlStament, 2,(int)POST_STATUS_POSTED);
    
    NSInteger status = sqlite3_step(self.sqlStament);
    if (SQLITE_DONE != status) {
        if (SQLITE_BUSY == status) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        result = 0;
    }
    
    return (int)result;
}

-(CheckListItemCoreDBModelV2 *)loadCheckListItemCoreData:(CheckListItemCoreDBModelV2 *)checkListItemCore
{
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@ FROM %@ WHERE %@ = ?", cic_item_id, cic_core, cic_form_score, cic_post_status, CHECKLIST_ITEM_CORE,cic_id];
    const char *sql =[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }

    NSInteger status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        
        sqlite3_bind_int(self.sqlStament, 1, (int)checkListItemCore.chkItemCoreId);
        
        status = sqlite3_step(self.sqlStament);
        while(status == SQLITE_ROW)
        {
            checkListItemCore.chkItemCoreDetailContentId = sqlite3_column_int(self.sqlStament, 0);
            checkListItemCore.chkItemCore = sqlite3_column_int(self.sqlStament, 1);
            checkListItemCore.chkFormScore = sqlite3_column_int(self.sqlStament, 2);
            checkListItemCore.chkPostStatus = sqlite3_column_int(self.sqlStament, 3);
                   
            status = sqlite3_step(self.sqlStament);
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    return checkListItemCore;
}

-(NSMutableArray *)loadCheckListItemCoreByChkListFormScoreIdData:(NSInteger)checkListFormScoreID
{
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ?",cic_id, cic_item_id, cic_core, cic_form_score, cic_post_status, CHECKLIST_ITEM_CORE,cic_form_score];
    
    const char *sql =[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    NSInteger status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);

    if(status == SQLITE_OK) {
        
        sqlite3_bind_int(self.sqlStament, 1, (int)checkListFormScoreID);
        
        status = sqlite3_step(self.sqlStament);
        while(status == SQLITE_ROW)
        {
            CheckListItemCoreDBModelV2 *model = [[CheckListItemCoreDBModelV2 alloc] init];
            
            model.chkItemCoreId = sqlite3_column_int(self.sqlStament, 0);
            model.chkItemCoreDetailContentId = sqlite3_column_int(self.sqlStament, 1);
            model.chkItemCore = sqlite3_column_int(self.sqlStament, 2);
            model.chkFormScore = sqlite3_column_int(self.sqlStament, 3);
            model.chkPostStatus = sqlite3_column_int(self.sqlStament, 4);
            
            [array addObject:model];
            
            status = sqlite3_step(self.sqlStament);
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                [array removeAllObjects];
                goto RE_ACCESS_DB;
            }
        }
    }
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    return array;
}

-(int)updateCheckListItemCoreByChkListItemIDData:(CheckListItemCoreDBModelV2 *)checkListItemCore
{
    NSInteger returnInt = 0;
    if(self.sqlStament == nil) {
        NSString *sqlString = [[NSString alloc] initWithFormat:@"UPDATE %@ SET %@ = ?, %@ = ? WHERE %@ = ? AND %@ = ?", CHECKLIST_ITEM_CORE,
            cic_core, 
            cic_post_status,
            cic_item_id,
            cic_form_score                  
            ];
        
        const char *sql = [sqlString UTF8String];
        
        NSInteger reAccessCount = 0;
        RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
            reAccessCount ++;
        }
        
        if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
        {
        }
        
        sqlite3_bind_int (self.sqlStament, 1, (int)checkListItemCore.chkItemCore);
        sqlite3_bind_int (self.sqlStament, 2, (int)checkListItemCore.chkPostStatus);
        sqlite3_bind_int (self.sqlStament, 3, (int)checkListItemCore.chkItemCoreDetailContentId);
        sqlite3_bind_int (self.sqlStament, 4, (int)checkListItemCore.chkFormScore);
        
        NSInteger status = sqlite3_step(self.sqlStament);

        if(SQLITE_DONE != status)
        {
            if (status == SQLITE_BUSY) {
                if (reAccessCount <= REACCESS_MAX_TIMES) {
                    goto RE_ACCESS_DB;
                }
            }
            return (int)returnInt;
        }
        else
        { 
            sqlite3_last_insert_rowid(database);
            returnInt = 1;
            return (int)returnInt;
        }
    }
    
    return(int) returnInt;
}

-(void) deleteCheckListItemScoreByItemId:(NSInteger) itemId AndFormScoreId:(NSInteger) formScoreId
{
	NSString *sqlString = [NSString stringWithFormat:@"DELETE FROM %@ WHERE %@ = ? AND %@ = ?", CHECKLIST_ITEM_CORE, cic_item_id, cic_form_score];
	const char *sql = [sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
	if(sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL) != SQLITE_OK){
        
    };
    
    sqlite3_bind_int(sqlStament, 1, (int)itemId);
    sqlite3_bind_int(sqlStament, 2, (int)formScoreId);
    
    NSInteger status = sqlite3_step(sqlStament);
    
	if (SQLITE_DONE != status) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
}

-(CheckListItemCoreDBModelV2 *)loadCheckListItemScoreByItemId:(NSInteger)itemId AndFormScoreId:(NSInteger)formScoreId
{
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ? AND %@ = ?",cic_id, cic_item_id, cic_core, cic_form_score, cic_post_status, CHECKLIST_ITEM_CORE,cic_item_id, cic_form_score];
    const char *sql =[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    CheckListItemCoreDBModelV2 *model = [[CheckListItemCoreDBModelV2 alloc] init];
    
    NSInteger status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        
        sqlite3_bind_int(self.sqlStament, 1, (int)itemId);
        sqlite3_bind_int(self.sqlStament, 2, (int)formScoreId);
        
        status = sqlite3_step(self.sqlStament);
        while(status == SQLITE_ROW)
        {
            model.chkItemCoreId = sqlite3_column_int(self.sqlStament, 0);
            model.chkItemCoreDetailContentId = sqlite3_column_int(self.sqlStament, 1);
            model.chkItemCore = sqlite3_column_int(self.sqlStament, 2);
            model.chkFormScore = sqlite3_column_int(self.sqlStament, 3);
            model.chkPostStatus = sqlite3_column_int(self.sqlStament, 4);
            
            status = sqlite3_step(self.sqlStament);
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return model;
    }
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    return model;

}

-(CheckListItemCoreDBModelV2 *)loadCheckListItemScoreByItemId:(NSInteger)itemId{
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ?",cic_id, cic_item_id, cic_core, cic_form_score, cic_post_status, CHECKLIST_ITEM_CORE,cic_item_id];
    const char *sql =[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    CheckListItemCoreDBModelV2 *model = [[CheckListItemCoreDBModelV2 alloc] init];
    
    NSInteger status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        
        sqlite3_bind_int(self.sqlStament, 1, (int)itemId);
        
        status = sqlite3_step(self.sqlStament);
        while(status == SQLITE_ROW)
        {
            model.chkItemCoreId = sqlite3_column_int(self.sqlStament, 0);
            model.chkItemCoreDetailContentId = sqlite3_column_int(self.sqlStament, 1);
            model.chkItemCore = sqlite3_column_int(self.sqlStament, 2);
            model.chkFormScore = sqlite3_column_int(self.sqlStament, 3);
            model.chkPostStatus = sqlite3_column_int(self.sqlStament, 4);
            
            status = sqlite3_step(self.sqlStament);
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        
        return model;
    }
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    return model;

}

-(NSMutableArray *)loadCheckListItemCoreByChkListFormScoreIdData:(NSInteger)checkListFormScoreID AndStatusPost:(NSInteger)statusPost{
    
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ? AND %@ = ?",
                           cic_id, 
                           cic_item_id,
                           cic_core,
                           cic_form_score,
                           cic_post_status,
                           CHECKLIST_ITEM_CORE,
                           cic_form_score,
                           cic_post_status
                           ];
    
    const char *sql =[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSInteger status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        
        sqlite3_bind_int(self.sqlStament, 1, (int)checkListFormScoreID);
        sqlite3_bind_int(self.sqlStament, 2, (int)statusPost);
        
        status = sqlite3_step(self.sqlStament);
        while(status == SQLITE_ROW)
        {
            CheckListItemCoreDBModelV2 *model = [[CheckListItemCoreDBModelV2 alloc] init];
            
            model.chkItemCoreId = sqlite3_column_int(self.sqlStament, 0);
            model.chkItemCoreDetailContentId = sqlite3_column_int(self.sqlStament, 1);
            model.chkItemCore = sqlite3_column_int(self.sqlStament, 2);
            model.chkFormScore = sqlite3_column_int(self.sqlStament, 3);
            model.chkPostStatus = sqlite3_column_int(self.sqlStament, 4);
            
            [array addObject:model];
            
            status = sqlite3_step(self.sqlStament);
            
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                [array removeAllObjects];
                goto RE_ACCESS_DB;
            }
        }
    }
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    return array;
}
                                                                                
@end
