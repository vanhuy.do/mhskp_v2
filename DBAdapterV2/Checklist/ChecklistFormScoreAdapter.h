//
//  ChecklistFormScoreAdapter.h
//
//  Created by Thuong Nguyen.
//  Copyright 2012 __TMS__. All rights reserved.
//

#import "DatabaseAdapter.h"
#import "ChecklistFormScoreModel.h"
@interface ChecklistFormScoreAdapter : DatabaseAdapter

-(BOOL) insertChecklistFormScoreModel:(ChecklistFormScoreModel *)model;

-(BOOL) updateChecklistFormScoreModel:(ChecklistFormScoreModel *) model;

-(BOOL) deleteChecklistFormScoreModel:(ChecklistFormScoreModel *) model;

-(int)deletCheckListFormScoreByRoomAssignmentId:(NSInteger) roomAssignmentId;

-(ChecklistFormScoreModel *) loadChecklistFormScoreModelByPrimaryKey:(ChecklistFormScoreModel *) model;

-(NSMutableArray *) loadChecklistFormScoreByChkListId:(NSInteger) chkListId AndChkUserId:(NSInteger) chkUserId;

-(ChecklistFormScoreModel *) loadCheckListFormScoreByChkListId:(NSInteger) chkListId AndChkFormId:(NSInteger) chkListFormId AndChkUserId:(NSInteger) chkUserId;

-(ChecklistFormScoreModel *)loadCheckListFormScoreByChkListId:(NSInteger)chkListId AndChkFormId:(NSInteger)chkListFormId;
@end