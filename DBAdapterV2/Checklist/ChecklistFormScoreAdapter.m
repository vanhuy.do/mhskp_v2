//
//  ChecklistFormScoreAdapter.h
//
//  Created by Thuong Nguyen.
//  Copyright 2012 __TMS__. All rights reserved.
//

#import "ChecklistFormScoreAdapter.h"
#import "DbDefinesV2.h"
#import "ehkDefines.h"

@implementation ChecklistFormScoreAdapter

- (id)init
{
	self = [super init];
	if (self) {
		// Initialization code here.
	}
	return self;

}

-(BOOL)insertChecklistFormScoreModel:(ChecklistFormScoreModel *)model {
	NSString *sqlString = [NSString stringWithFormat:@"INSERT INTO CHECKLIST_FORM_SCORE ( %@, %@, %@, %@, %@, %@) VALUES ( ?, ?, ?, ?, ?, ?)" , df_score, df_post_status, df_checklist_id, df_form_id, df_user_id, df_is_mandatory_pass ];

	const char *sql = [sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }

	if(sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
		
	}
    
    sqlite3_bind_int(sqlStament, 1, (int)model.dfScore);
    sqlite3_bind_int(sqlStament, 2, (int)model.dfPostStatus);
    sqlite3_bind_int(sqlStament, 3, (int)model.dfChecklistId);
    sqlite3_bind_int(sqlStament, 4, (int)model.dfFormId);
    sqlite3_bind_int(sqlStament, 5, (int)model.dfUserId);
    sqlite3_bind_int(sqlStament, 6, (int)model.dfIsMandatoryPass);
    
	NSInteger status = sqlite3_step(sqlStament);
	
    if (status == SQLITE_DONE) {
		sqlite3_last_insert_rowid(database);
		sqlite3_reset(sqlStament);
		return YES;
	}else {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }

	return  NO;
}

-(BOOL)updateChecklistFormScoreModel:(ChecklistFormScoreModel *)model {
	NSString *sqlString = [NSString stringWithFormat:@"UPDATE %@ SET %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ? WHERE %@ = ? AND %@ = ? AND %@ = ?", CHECKLIST_FORM_SCORE, df_score, df_post_status, df_checklist_id, df_form_id, df_user_id, df_is_mandatory_pass, df_checklist_id, df_form_id, df_user_id];
	
    const char *sql = [sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSInteger status = sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL);
    
    if(status == SQLITE_OK) {
        sqlite3_bind_int(sqlStament, 1, (int)model.dfScore);
        sqlite3_bind_int(sqlStament, 2, (int)model.dfPostStatus);
        sqlite3_bind_int(sqlStament, 3, (int)model.dfChecklistId);
        sqlite3_bind_int(sqlStament, 4, (int)model.dfFormId);
        sqlite3_bind_int(sqlStament, 5, (int)model.dfUserId);
        sqlite3_bind_int(sqlStament, 6, (int)model.dfIsMandatoryPass);
        sqlite3_bind_int(sqlStament, 7, (int)model.dfChecklistId);
        sqlite3_bind_int(sqlStament, 8, (int)model.dfFormId);
        sqlite3_bind_int(sqlStament, 9, (int)model.dfUserId);
    }
    
    status = sqlite3_step(sqlStament);
    
    if (status == SQLITE_DONE) {
        return YES;
    } else {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }

	return  NO;
}

-(int)deletCheckListFormScoreByRoomAssignmentId:(NSInteger) roomAssignmentId
{
    NSInteger result = 1;
    if(self.sqlStament == nil) {
        NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"DELETE FROM %@ WHERE %@ = ?", CHECKLIST_FORM_SCORE, df_checklist_id];
        const char *sql = [sqlString UTF8String];
        if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
            result = 0;
    }
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    reAccessCount ++;
}
    
    sqlite3_bind_int(self.sqlStament, 1, (int)roomAssignmentId);
    
    NSInteger status = sqlite3_step(self.sqlStament);
    if (SQLITE_DONE != status) {
        if (SQLITE_BUSY == status) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        result = 0;
    }
    
    return (int)result;
}

-(BOOL)deleteChecklistFormScoreModel:(ChecklistFormScoreModel *)model {
	NSString *sqlString = [NSString stringWithFormat:@"DELETE FROM %@ WHERE %@ = ?", CHECKLIST_FORM_SCORE, df_id];
	const char *sql = [sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }

	if(sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
		
	}
    sqlite3_bind_int(sqlStament, 1,(int) model.dfId);
    
	NSInteger status = sqlite3_step(sqlStament);
	if (status == SQLITE_DONE) {
		return YES;
	}
    else{
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }
	return  NO;
}

-(ChecklistFormScoreModel *)loadChecklistFormScoreModelByPrimaryKey:(ChecklistFormScoreModel *)model {
	NSString *sqlString = [NSString stringWithFormat:@"SELECT %@, %@, %@, %@, %@, %@ FROM %@ WHERE  %@ = ?" , df_id, df_score, df_post_status, df_checklist_id, df_form_id, df_is_mandatory_pass, CHECKLIST_FORM_SCORE, df_id];
	const char *sql = [sqlString UTF8String];
	
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }

    NSInteger status = sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL);
	if(status == SQLITE_OK) {
        
		sqlite3_bind_int(sqlStament, 1, (int)model.dfId);
        status = sqlite3_step(self.sqlStament);
        
		while (status == SQLITE_ROW) {
			model.dfId = sqlite3_column_int(sqlStament, 0);
			model.dfScore = sqlite3_column_int(sqlStament, 1);
			model.dfPostStatus = sqlite3_column_int(sqlStament, 2);
			model.dfChecklistId = sqlite3_column_int(sqlStament, 3);
			model.dfFormId = sqlite3_column_int(sqlStament, 4);
            model.dfIsMandatoryPass = sqlite3_column_int(sqlStament, 5);
			return model;
		}
        if (SQLITE_BUSY == status) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
	}
    if (SQLITE_BUSY == status) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
	return model;
}

-(NSMutableArray *)loadChecklistFormScoreByChkListId:(NSInteger) chkListId AndChkUserId:(NSInteger)chkUserId
{
    NSMutableArray *array = [NSMutableArray array];
    NSString *sqlString = [NSString stringWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@ FROM %@ WHERE  %@ = ? AND %@ = ?" , df_id, df_score, df_post_status, df_checklist_id, df_form_id, df_user_id, df_is_mandatory_pass, CHECKLIST_FORM_SCORE, df_checklist_id, df_user_id];
	const char *sql = [sqlString UTF8String];
	
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }

    NSInteger status = sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL);
    
	if(status == SQLITE_OK) {
        
		sqlite3_bind_int(sqlStament, 1, (int)chkListId);
        sqlite3_bind_int(sqlStament, 2, (int)chkUserId);

        status = sqlite3_step(self.sqlStament);
        
		while (status == SQLITE_ROW) {
            
            ChecklistFormScoreModel *model = [[ChecklistFormScoreModel alloc] init];
			model.dfId = sqlite3_column_int(sqlStament, 0);
			model.dfScore = sqlite3_column_int(sqlStament, 1);
			model.dfPostStatus = sqlite3_column_int(sqlStament, 2);
			model.dfChecklistId = sqlite3_column_int(sqlStament, 3);
			model.dfFormId = sqlite3_column_int(sqlStament, 4);
            model.dfUserId = sqlite3_column_int(sqlStament, 5);
            model.dfIsMandatoryPass = sqlite3_column_int(sqlStament, 6);
            [array addObject:model];
            
            status = sqlite3_step(self.sqlStament);
		}
        
        if (SQLITE_BUSY == status) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                [array removeAllObjects];
                goto RE_ACCESS_DB;
            }
        }
	}
    if (SQLITE_BUSY == status) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    return array;
}

-(ChecklistFormScoreModel *)loadCheckListFormScoreByChkListId:(NSInteger)chkListId AndChkFormId:(NSInteger)chkListFormId AndChkUserId:(NSInteger)chkUserId
{
    NSString *sqlString = [NSString stringWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@ FROM %@ WHERE  %@ = ? AND %@ = ? AND %@ = ?" , df_id, df_score, df_post_status, df_checklist_id, df_form_id, df_user_id, df_is_mandatory_pass, CHECKLIST_FORM_SCORE, df_checklist_id, df_form_id, df_user_id];
	const char *sql = [sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
	
    ChecklistFormScoreModel *model = [[ChecklistFormScoreModel alloc] init];
    
    NSInteger status = sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL);
	if(status == SQLITE_OK) {
        
		sqlite3_bind_int(sqlStament, 1, (int)chkListId);
        sqlite3_bind_int(sqlStament, 2, (int)chkListFormId);
        sqlite3_bind_int(sqlStament, 3, (int)chkUserId);
        
        status = sqlite3_step(self.sqlStament);
		while (status == SQLITE_ROW) {
            
			model.dfId = sqlite3_column_int(sqlStament, 0);
			model.dfScore = sqlite3_column_int(sqlStament, 1);
			model.dfPostStatus = sqlite3_column_int(sqlStament, 2);
			model.dfChecklistId = sqlite3_column_int(sqlStament, 3);
			model.dfFormId = sqlite3_column_int(sqlStament, 4);
			model.dfUserId = sqlite3_column_int(sqlStament, 5);
            model.dfIsMandatoryPass = sqlite3_column_int(sqlStament, 6);
            
			return model;
		}
        
        if (SQLITE_BUSY == status) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
	}
    if (SQLITE_BUSY == status) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
	return model;

}

-(ChecklistFormScoreModel *)loadCheckListFormScoreByChkListId:(NSInteger)chkListId AndChkFormId:(NSInteger)chkListFormId
{
    NSString *sqlString = [NSString stringWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@ FROM %@ WHERE  %@ = ? AND %@ = ?" , df_id, df_score, df_post_status, df_checklist_id, df_form_id, df_user_id, df_is_mandatory_pass, CHECKLIST_FORM_SCORE, df_checklist_id, df_form_id];
	const char *sql = [sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    reAccessCount ++;
}
	
    ChecklistFormScoreModel *model = [[ChecklistFormScoreModel alloc] init];
    
    NSInteger status = sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL);
	if(status == SQLITE_OK) {
        
		sqlite3_bind_int(sqlStament, 1, (int)chkListId);
        sqlite3_bind_int(sqlStament, 2,(int) chkListFormId);
        
        status = sqlite3_step(self.sqlStament);
		while (status == SQLITE_ROW) {
            
			model.dfId = sqlite3_column_int(sqlStament, 0);
			model.dfScore = sqlite3_column_int(sqlStament, 1);
			model.dfPostStatus = sqlite3_column_int(sqlStament, 2);
			model.dfChecklistId = sqlite3_column_int(sqlStament, 3);
			model.dfFormId = sqlite3_column_int(sqlStament, 4);
			model.dfUserId = sqlite3_column_int(sqlStament, 5);
            model.dfIsMandatoryPass = sqlite3_column_int(sqlStament, 6);
            
			return model;
		}
        
        if (SQLITE_BUSY == status) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
	}
    if (SQLITE_BUSY == status) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
	return model;
    
}

@end