//
//  AmenitiesCategoriesAdapterV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 3/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "DatabaseAdapterV2.h"
#import "AmenitiesCategoriesModelV2.h"

@interface AmenitiesCategoriesAdapterV2 : DatabaseAdapterV2

-(BOOL) insertAmenitiesCategoriesModel:(AmenitiesCategoriesModelV2 *)model;

-(AmenitiesCategoriesModelV2 *) loadAmenitiesCategoriesModelByPrimaryKey:(AmenitiesCategoriesModelV2 *) model;

-(BOOL) updateAmenitiesCategoriesModel:(AmenitiesCategoriesModelV2 *) model;

-(BOOL) deleteAmenitiesCategoriesModel:(AmenitiesCategoriesModelV2 *) model;

-(NSMutableArray *) loadAllAmenitiesCategoriesByServiceId:(NSInteger) serviceId;

-(NSMutableArray *) loadAllAmenitiesCategories;

-(AmenitiesCategoriesModelV2 *) loadAmenitiesCategoryLatest;

- (NSMutableArray *)loadAllAmenitiesCategoriesByRoomTypeId:(NSInteger)roomTypeId;

@end