//
//  AmenitiesCategoriesAdapterV2.m
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 3/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AmenitiesCategoriesAdapterV2.h"
#import "DbDefinesV2.h"
#import "ehkDefines.h"

@implementation AmenitiesCategoriesAdapterV2

- (id)init
{
	self = [super init];
	if (self) {
		// Initialization code here.
	}
	return self;
    
}

-(BOOL)insertAmenitiesCategoriesModel:(AmenitiesCategoriesModelV2 *)model {
	NSString *sqlString = [NSString stringWithFormat:@"INSERT INTO AMENITIES_CATEGORIES ( %@, %@, %@, %@, %@, %@) VALUES ( ?, ?, ?, ?, ?, ?)" , amca_id, amca_name, amca_hotel_id, amca_name_lang, amca_image, amca_last_modified];
    
	const char *sql = [sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
	if(sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {
		sqlite3_bind_int(sqlStament, 1, (int)model.amcaId);
		sqlite3_bind_text(sqlStament, 2, [model.amcaName UTF8String], -1, SQLITE_TRANSIENT);
		sqlite3_bind_int(sqlStament, 3, (int)model.amcaHotelId);
		sqlite3_bind_text(sqlStament, 4, [model.amcaNameLang UTF8String], -1, SQLITE_TRANSIENT);
		sqlite3_bind_blob(sqlStament, 5, [model.amcaImage bytes], (int)[model.amcaImage length], NULL);
        sqlite3_bind_text(sqlStament, 6, [model.amcaLastModified UTF8String], -1, SQLITE_TRANSIENT);
	}
    
	NSInteger status = sqlite3_step(sqlStament);
	if (status == SQLITE_DONE) {
		sqlite3_last_insert_rowid(database);
		sqlite3_reset(sqlStament);
		return YES;
	}
    else{
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return NO;
    }
	return  NO;
}

-(AmenitiesCategoriesModelV2 *)loadAmenitiesCategoriesModelByPrimaryKey:(AmenitiesCategoriesModelV2 *)model {
	NSString *sqlString = [NSString stringWithFormat:@"SELECT %@, %@, %@, %@, %@, %@ FROM %@ WHERE  %@ = ?" , amca_id, amca_name, amca_hotel_id, amca_name_lang, amca_image,amca_last_modified, AMENITIES_CATEGORIES, amca_id];
	const char *sql = [sqlString UTF8String];
	
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSInteger status;
    status = sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL);
	if(status == SQLITE_OK) {
		sqlite3_bind_int(sqlStament, 1, (int)model.amcaId);
        
        status = sqlite3_step(sqlStament);
		while (status == SQLITE_ROW) {
			model.amcaId = sqlite3_column_int(sqlStament, 0);
			if (sqlite3_column_text(sqlStament, 1)) {
				model.amcaName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
			}
			model.amcaHotelId = sqlite3_column_int(sqlStament, 2);
			if (sqlite3_column_text(sqlStament, 3)) {
				model.amcaNameLang = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
			}
			if (sqlite3_column_bytes(sqlStament, 4) > 0) {
				model.amcaImage = [NSData dataWithBytes:sqlite3_column_blob(sqlStament, 4) length:sqlite3_column_bytes(sqlStament, 4)];
			}	
            
            model.amcaLastModified = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
            return model;
		}
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
	}
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
	return model;
}

-(BOOL)updateAmenitiesCategoriesModel:(AmenitiesCategoriesModelV2 *)model {
	NSString *sqlString = [NSString stringWithFormat:@"UPDATE %@ SET %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ? WHERE  %@ = ?", AMENITIES_CATEGORIES, amca_name, amca_hotel_id, amca_name_lang, amca_image,amca_last_modified, amca_id];
	const char *sql = [sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
	if(sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {
		sqlite3_bind_text(sqlStament, 1, [model.amcaName UTF8String], -1, SQLITE_TRANSIENT);
		sqlite3_bind_int(sqlStament, 2, (int)model.amcaHotelId);
		sqlite3_bind_text(sqlStament, 3, [model.amcaNameLang UTF8String], -1, SQLITE_TRANSIENT);
		sqlite3_bind_blob(sqlStament, 4, [model.amcaImage bytes], (int)[model.amcaImage length], NULL);
        sqlite3_bind_text(sqlStament, 5, [model.amcaLastModified UTF8String], -1, SQLITE_TRANSIENT);
		sqlite3_bind_int(sqlStament, 6, (int)model.amcaId);
	}
    
	NSInteger status = sqlite3_step(sqlStament);
	if (status == SQLITE_DONE) {
		return YES;
	}
    else{
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return NO;
    }
	return  NO;
}

-(BOOL)deleteAmenitiesCategoriesModel:(AmenitiesCategoriesModelV2 *)model {
	NSString *sqlString = [NSString stringWithFormat:@"DELETE FROM %@ WHERE %@ = ?", AMENITIES_CATEGORIES, amca_id];
	const char *sql = [sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
	if(sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {
		sqlite3_bind_int(sqlStament, 1, (int)model.amcaId);
	}
    
	NSInteger status = sqlite3_step(sqlStament);
	if (status == SQLITE_DONE) {
		return YES;
	}
    else{
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return  NO;
    }
	return  NO;
}

-(NSMutableArray *)loadAllAmenitiesCategoriesByServiceId:(NSInteger)serviceId
{
    NSMutableArray *array = [NSMutableArray array];
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ?",amca_id , amca_name, amca_hotel_id, amca_name_lang, amca_image,amca_last_modified, AMENITIES_CATEGORIES, amca_id];
    
    const char *sql =[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        
        sqlite3_bind_int(sqlStament, 1, (int)serviceId);

        status = sqlite3_step(sqlStament);
        while(status == SQLITE_ROW)
        {
            AmenitiesCategoriesModelV2 *model = [[AmenitiesCategoriesModelV2 alloc] init];
            
            model.amcaId = sqlite3_column_int(self.sqlStament, 0);
            
            if (sqlite3_column_text(self.sqlStament, 1)) {
                model.amcaName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(self.sqlStament, 1)];
            }
            
            model.amcaHotelId = sqlite3_column_int(self.sqlStament, 2);
            
            if (sqlite3_column_text(self.sqlStament, 3)) {
                model.amcaNameLang = [NSString stringWithUTF8String:(char *)sqlite3_column_text(self.sqlStament, 3)];
            }
            
            if (sqlite3_column_text(self.sqlStament, 4)) {
                model.amcaImage = [NSData dataWithBytes:sqlite3_column_blob(sqlStament, 4) length:sqlite3_column_bytes(sqlStament, 4)];
            }
            model.amcaLastModified = [NSString stringWithUTF8String:(char *)sqlite3_column_text(self.sqlStament, 5)];
            
            [array addObject:model];
            
            status = sqlite3_step(sqlStament);
        }
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                [array removeAllObjects];
                goto RE_ACCESS_DB;
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    
    return array;

}

-(NSMutableArray *)loadAllAmenitiesCategories
{
    NSMutableArray *array = [NSMutableArray array];
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@ FROM %@ ",amca_id , amca_name, amca_hotel_id, amca_name_lang, amca_image,amca_last_modified, AMENITIES_CATEGORIES];
    
    const char *sql =[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);

    if(status == SQLITE_OK) {
        
        status = sqlite3_step(sqlStament);
        while(status == SQLITE_ROW)
        {
            AmenitiesCategoriesModelV2 *model = [[AmenitiesCategoriesModelV2 alloc] init];
            
            model.amcaId = sqlite3_column_int(self.sqlStament, 0);
            
            if (sqlite3_column_text(self.sqlStament, 1)) {
                model.amcaName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(self.sqlStament, 1)];
            }
            
            model.amcaHotelId = sqlite3_column_int(self.sqlStament, 2);
            
            if (sqlite3_column_text(self.sqlStament, 3)) {
                model.amcaNameLang = [NSString stringWithUTF8String:(char *)sqlite3_column_text(self.sqlStament, 3)];
            }
            
            if (sqlite3_column_text(self.sqlStament, 4)) {
                model.amcaImage = [NSData dataWithBytes:sqlite3_column_blob(sqlStament, 4) length:sqlite3_column_bytes(sqlStament, 4)];
            }
            model.amcaLastModified = [NSString stringWithUTF8String:(char *)sqlite3_column_text(self.sqlStament, 5)];
            
            [array addObject:model];
            
            status = sqlite3_step(sqlStament);
        }
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                [array removeAllObjects];
                goto RE_ACCESS_DB;
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    
    return array;

}

-(AmenitiesCategoriesModelV2 *)loadAmenitiesCategoryLatest
{
    NSMutableArray *array = [NSMutableArray array];
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@ FROM %@ ORDER BY %@ DESC LIMIT 1",amca_last_modified, AMENITIES_CATEGORIES, amca_last_modified];
    
    const char *sql =[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        
        status = sqlite3_step(sqlStament);
        while(status == SQLITE_ROW)
        {
            AmenitiesCategoriesModelV2 *model = [[AmenitiesCategoriesModelV2 alloc] init];
            
            model.amcaLastModified = [NSString stringWithUTF8String:(char *)sqlite3_column_text(self.sqlStament, 0)];
            
            [array addObject:model];
            
            status = sqlite3_step(sqlStament);
        }
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                [array  removeAllObjects];
                goto  RE_ACCESS_DB;
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            [array  removeAllObjects];
            goto  RE_ACCESS_DB;
        }
    }
    
    AmenitiesCategoriesModelV2 *categoryLatest = nil;
    if (array.count != 0) {
        categoryLatest = [array objectAtIndex:0];
    }
    return categoryLatest;
}

- (NSMutableArray *)loadAllAmenitiesCategoriesByRoomTypeId:(NSInteger)roomTypeId {
    
    NSMutableArray *array = [NSMutableArray array];
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT DISTINCT %@, %@, %@, %@, %@, %@ FROM %@ a JOIN %@ b ON a.%@ = b.%@ WHERE b.%@ = ?",
                           amca_id,
                           amca_name,
                           amca_hotel_id,
                           amca_name_lang,
                           amca_image,
                           amca_last_modified,
                           AMENITIES_CATEGORIES,
                           ROOMTYPE_INVENTORY,
                           amca_id,
                           rti_category_id,
                           rti_roomtype_id
                           ];
    
    const char *sql =[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    reAccessCount ++;
}
    
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    
    if(status == SQLITE_OK) {
        
        sqlite3_bind_int (self.sqlStament, 1, (int)roomTypeId);
        
        status = sqlite3_step(sqlStament);
        while(status == SQLITE_ROW) {
            
            AmenitiesCategoriesModelV2 *model = [[AmenitiesCategoriesModelV2 alloc] init];
            
            model.amcaId = sqlite3_column_int(self.sqlStament, 0);
            
            if (sqlite3_column_text(self.sqlStament, 1)) {
                model.amcaName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(self.sqlStament, 1)];
            }
            
            model.amcaHotelId = sqlite3_column_int(self.sqlStament, 2);
            
            if (sqlite3_column_text(self.sqlStament, 3)) {
                model.amcaNameLang = [NSString stringWithUTF8String:(char *)sqlite3_column_text(self.sqlStament, 3)];
            }
            
            if (sqlite3_column_text(self.sqlStament, 4)) {
                model.amcaImage = [NSData dataWithBytes:sqlite3_column_blob(sqlStament, 4) length:sqlite3_column_bytes(sqlStament, 4)];
            }
            model.amcaLastModified = [NSString stringWithUTF8String:(char *)sqlite3_column_text(self.sqlStament, 5)];
            
            [array addObject:model];
            
            status = sqlite3_step(sqlStament);
        }
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                [array removeAllObjects];
                goto RE_ACCESS_DB;
            }
        }
    }
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    return array;
}

@end