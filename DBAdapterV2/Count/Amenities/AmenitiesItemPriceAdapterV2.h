//
//  AmenitiesItemPriceAdapterV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 3/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "DatabaseAdapterV2.h"
#import "AmenitiesItemPriceModelV2.h"

@interface AmenitiesItemPriceAdapterV2 : DatabaseAdapterV2

-(BOOL) insertAmenitiesItemPriceModel:(AmenitiesItemPriceModelV2 *)model;

-(AmenitiesItemPriceModelV2 *) loadAmenitiesItemPriceModelByPrimaryKey:(AmenitiesItemPriceModelV2 *) model;

-(BOOL) updateAmenitiesItemPriceModel:(AmenitiesItemPriceModelV2 *) model;

-(BOOL) deleteAmenitiesItemPriceModel:(AmenitiesItemPriceModelV2 *) model;
@end