//
//  AmenitiesItemPriceAdapterV2.m
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 3/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AmenitiesItemPriceAdapterV2.h"
#import "DbDefinesV2.h"
#import "ehkDefines.h"

@implementation AmenitiesItemPriceAdapterV2

- (id)init
{
	self = [super init];
	if (self) {
		// Initialization code here.
	}
	return self;
    
}

-(BOOL)insertAmenitiesItemPriceModel:(AmenitiesItemPriceModelV2 *)model {
	NSString *sqlString = [NSString stringWithFormat:@"INSERT INTO AMENITIES_ITEM_PRICE ( %@, %@, %@, %@) VALUES ( ?, ?, ?, ?)" , amip_id, amip_item_id, amip_service_id, amip_price ];
    
	const char *sql = [sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
	if(sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {
		sqlite3_bind_int(sqlStament, 1, (int)model.amipId);
		sqlite3_bind_int(sqlStament, 2, (int)model.amipItemId);
		sqlite3_bind_int(sqlStament, 3, (int)model.amipServiceId);
		sqlite3_bind_double(sqlStament, 4, model.amipPrice);
	}
    
	NSInteger status = sqlite3_step(sqlStament);
	if (status == SQLITE_DONE) {
		sqlite3_last_insert_rowid(database);
		sqlite3_reset(sqlStament);
		return YES;
	}
    else{
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return NO;
    }
	return  NO;
}

-(AmenitiesItemPriceModelV2 *)loadAmenitiesItemPriceModelByPrimaryKey:(AmenitiesItemPriceModelV2 *)model {
	NSString *sqlString = [NSString stringWithFormat:@"SELECT %@, %@, %@, %@ FROM %@ WHERE  %@ = ?" , amip_id, amip_item_id, amip_service_id, amip_price, AMENITIES_ITEM_PRICE, amip_id];
	const char *sql = [sqlString UTF8String];
	
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    NSInteger status;
    status = sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL);
	if(status == SQLITE_OK) {
		sqlite3_bind_int(sqlStament, 1, (int)model.amipId);
        
        status = sqlite3_step(sqlStament);
		while (status == SQLITE_ROW) {
			model.amipId = sqlite3_column_int(sqlStament, 0);
			model.amipItemId = sqlite3_column_int(sqlStament, 1);
			model.amipServiceId = sqlite3_column_int(sqlStament, 2);
			model.amipPrice = sqlite3_column_double(sqlStament, 3);
			return model;
		}
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
	}
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
	return model;
}

-(BOOL)updateAmenitiesItemPriceModel:(AmenitiesItemPriceModelV2 *)model {
	NSString *sqlString = [NSString stringWithFormat:@"UPDATE %@ SET %@ = ?, %@ = ?, %@ = ? WHERE  %@ = ?", AMENITIES_ITEM_PRICE, amip_item_id, amip_service_id, amip_price, amip_id];
	const char *sql = [sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
	if(sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {
		sqlite3_bind_int(sqlStament, 1, (int)model.amipItemId);
		sqlite3_bind_int(sqlStament, 2, (int)model.amipServiceId);
		sqlite3_bind_double(sqlStament, 3, (int)model.amipPrice);
		sqlite3_bind_int(sqlStament, 4, (int)model.amipId);
	}
    
	NSInteger status = sqlite3_step(sqlStament);
	if (status == SQLITE_DONE) {
		return YES;
	}
    else{
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return NO;
    }
	return  NO;
}

-(BOOL)deleteAmenitiesItemPriceModel:(AmenitiesItemPriceModelV2 *)model {
	NSString *sqlString = [NSString stringWithFormat:@"DELETE FROM %@ WHERE %@ = ?", AMENITIES_ITEM_PRICE, amip_id];
	const char *sql = [sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
	if(sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {
		sqlite3_bind_int(sqlStament, 1, (int)model.amipId);
	}
    
	NSInteger status = sqlite3_step(sqlStament);
	if (status == SQLITE_DONE) {
		return YES;
	}
    else{
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return NO;
    }
	return  NO;
}

@end