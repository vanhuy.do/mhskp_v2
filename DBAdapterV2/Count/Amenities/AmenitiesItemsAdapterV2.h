//
//  AmenitiesItemsAdapterV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 3/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "DatabaseAdapterV2.h"
#import "AmenitiesItemsModelV2.h"

@interface AmenitiesItemsAdapterV2 : DatabaseAdapterV2

-(BOOL) insertAmenitiesItemsModel:(AmenitiesItemsModelV2 *)model;

-(AmenitiesItemsModelV2 *) loadAmenitiesItemsModelByPrimaryKey:(AmenitiesItemsModelV2 *) model;

-(AmenitiesItemsModelV2 *)loadAmenitiesItemsModelByItemId:(NSInteger)itemId;

-(BOOL) updateAmenitiesItemsModel:(AmenitiesItemsModelV2 *) model;

-(BOOL) deleteAmenitiesItemsModel:(AmenitiesItemsModelV2 *) model;

-(NSMutableArray *) loadAllAmenitiesItemsByCategoriesId:(NSInteger) amenitiesCategoriesId AndRoomTypeId:(NSInteger) roomTypeId;

-(NSMutableArray *) loadAllAmenitiesItemsByRoomTypeId:(NSInteger) amenitiesRoomTypeId;

-(AmenitiesItemsModelV2 *) loadAmenitiesItemLatest;

- (NSMutableArray *) loadAmenitiesItemsModelByCateId:(NSInteger) cateId;

- (NSMutableArray*)loadAmenitiesItemsModelByCateId:(NSInteger)cateId andRoomTypeId:(NSInteger)roomTypeId;

@end
