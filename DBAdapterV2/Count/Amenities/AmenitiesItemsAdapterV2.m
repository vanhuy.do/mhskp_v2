//
//  AmenitiesItemsAdapterV2.m
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 3/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AmenitiesItemsAdapterV2.h"
#import "DbDefinesV2.h"
#import "ehkDefines.h"

@implementation AmenitiesItemsAdapterV2

- (id)init
{
	self = [super init];
	if (self) {
		// Initialization code here.
	}
	return self;
    
}

-(BOOL)insertAmenitiesItemsModel:(AmenitiesItemsModelV2 *)model {
	NSString *sqlString = [NSString stringWithFormat:@"INSERT INTO AMENITIES_ITEMS ( %@, %@, %@, %@, %@, %@, %@) VALUES ( ?, ?, ?, ?, ?, ?, ?)" , item_id, item_name, item_lang, item_category_id, item_image, item_roomtype_id, item_last_modified ];
    
	const char *sql = [sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
	if(sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {
		sqlite3_bind_int(sqlStament, 1, (int)model.itemId);
		sqlite3_bind_text(sqlStament, 2, [model.itemName UTF8String], -1, SQLITE_TRANSIENT);
		sqlite3_bind_text(sqlStament, 3, [model.itemLang UTF8String], -1, SQLITE_TRANSIENT);
		sqlite3_bind_int(sqlStament, 4, (int)model.itemCategoryId);
		sqlite3_bind_blob(sqlStament, 5, [model.itemImage bytes], (int)[model.itemImage length], NULL);
        sqlite3_bind_int(sqlStament, 6, (int)model.itemRoomType);
        sqlite3_bind_text(sqlStament, 7, [model.itemLastModified UTF8String], -1, SQLITE_TRANSIENT);

	}
    
	NSInteger status = sqlite3_step(sqlStament);
	if (status == SQLITE_DONE) {
		sqlite3_last_insert_rowid(database);
		sqlite3_reset(sqlStament);
		return YES;
	}
    else{
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return NO;
    }
	return  NO;
}


-(AmenitiesItemsModelV2 *)loadAmenitiesItemsModelByItemId:(NSInteger)itemId {
 
    AmenitiesItemsModelV2 *model = [[AmenitiesItemsModelV2 alloc] init];
    
    NSString *sqlString = [NSString stringWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ?" , item_id, item_name, item_lang, item_category_id, item_image, item_roomtype_id, item_last_modified, AMENITIES_ITEMS, item_id];
    const char *sql = [sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    reAccessCount ++;
}
    
    NSInteger status;
    status = sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        sqlite3_bind_int(sqlStament, 1, (int)itemId);
        
        status = sqlite3_step(sqlStament);
        while (status == SQLITE_ROW) {
            model.itemId = sqlite3_column_int(sqlStament, 0);
            if (sqlite3_column_text(sqlStament, 1)) {
                model.itemName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
            }
            if (sqlite3_column_text(sqlStament, 2)) {
                model.itemLang = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
            }
            model.itemCategoryId = sqlite3_column_int(sqlStament, 3);
            if (sqlite3_column_bytes(sqlStament, 4) > 0) {
                model.itemImage = [NSData dataWithBytes:sqlite3_column_blob(sqlStament, 4) length:sqlite3_column_bytes(sqlStament, 4)];
            }
            model.itemRoomType = sqlite3_column_int(sqlStament, 5);
            if (sqlite3_column_text(sqlStament, 6)) {
                model.itemLastModified = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 6)];
            }
            return model;
        }
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    return model;
}

-(AmenitiesItemsModelV2 *)loadAmenitiesItemsModelByPrimaryKey:(AmenitiesItemsModelV2 *)model {
	NSString *sqlString = [NSString stringWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@ FROM %@ WHERE  %@ = ? AND %@ = ? AND %@ = ?" , item_id, item_name, item_lang, item_category_id, item_image, item_roomtype_id, item_last_modified, AMENITIES_ITEMS, item_id, item_roomtype_id, item_category_id];
	const char *sql = [sqlString UTF8String];
	
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSInteger status;
    status = sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL);
	if(status == SQLITE_OK) {
		sqlite3_bind_int(sqlStament, 1, (int)model.itemId);
        sqlite3_bind_int(sqlStament, 2, (int)model.itemRoomType);
        sqlite3_bind_int(sqlStament, 3, (int)model.itemCategoryId);
        
        status = sqlite3_step(sqlStament);
		while (status == SQLITE_ROW) {
			model.itemId = sqlite3_column_int(sqlStament, 0);
			if (sqlite3_column_text(sqlStament, 1)) {
				model.itemName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
			}
			if (sqlite3_column_text(sqlStament, 2)) {
				model.itemLang = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
			}
			model.itemCategoryId = sqlite3_column_int(sqlStament, 3);
			if (sqlite3_column_bytes(sqlStament, 4) > 0) {
				model.itemImage = [NSData dataWithBytes:sqlite3_column_blob(sqlStament, 4) length:sqlite3_column_bytes(sqlStament, 4)];
			}
			model.itemRoomType = sqlite3_column_int(sqlStament, 5);
            if (sqlite3_column_text(sqlStament, 6)) {
				model.itemLastModified = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 6)];
			}
            return model;
		}
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
	}
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
	return model;
}

-(BOOL)updateAmenitiesItemsModel:(AmenitiesItemsModelV2 *)model {
	NSString *sqlString = [NSString stringWithFormat:@"UPDATE %@ SET %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ? WHERE  %@ = ?", AMENITIES_ITEMS, item_name, item_lang, item_category_id, item_image,item_roomtype_id, item_last_modified, item_id];
	const char *sql = [sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
	if(sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {
		sqlite3_bind_text(sqlStament, 1, [model.itemName UTF8String], -1, SQLITE_TRANSIENT);
		sqlite3_bind_text(sqlStament, 2, [model.itemLang UTF8String], -1, SQLITE_TRANSIENT);
		sqlite3_bind_int(sqlStament, 3, (int)model.itemCategoryId);
		sqlite3_bind_blob(sqlStament, 4, [model.itemImage bytes], (int)[model.itemImage length], NULL);
        sqlite3_bind_int(sqlStament, 5, (int)model.itemRoomType);
        sqlite3_bind_text(sqlStament, 6, [model.itemLastModified UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_int(sqlStament, 7, (int)model.itemId);
	}
    
	NSInteger status = sqlite3_step(sqlStament);
	if (status == SQLITE_DONE) {
		return YES;
	}
    else{
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return NO;
    }
	return  NO;
}

-(BOOL)deleteAmenitiesItemsModel:(AmenitiesItemsModelV2 *)model {
	NSString *sqlString = [NSString stringWithFormat:@"DELETE FROM %@ WHERE %@ = ? AND %@ = ? AND %@ = ?", AMENITIES_ITEMS, item_id, item_roomtype_id, item_category_id];
	const char *sql = [sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
	if(sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {
		sqlite3_bind_int(sqlStament, 1, (int)model.itemId);
        sqlite3_bind_int(sqlStament, 2, (int)model.itemRoomType);
        sqlite3_bind_int(sqlStament, 3, (int)model.itemCategoryId);
	}
    
	NSInteger status = sqlite3_step(sqlStament);
	if (status == SQLITE_DONE) {
		return YES;
	}
    else{
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return NO;
    }
	return  NO;
}

-(NSMutableArray *)loadAllAmenitiesItemsByCategoriesId:(NSInteger)amenitiesCategoriesId AndRoomTypeId:(NSInteger)roomTypeId
{
    NSMutableArray *array = [NSMutableArray array];
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ? AND %@ = ?",item_id , item_name, item_lang, item_category_id, item_image, item_roomtype_id, item_last_modified, AMENITIES_ITEMS, item_category_id, item_roomtype_id];
    
    const char *sql =[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        
        sqlite3_bind_int (self.sqlStament, 1, (int)amenitiesCategoriesId);
        sqlite3_bind_int (self.sqlStament, 2, (int)roomTypeId);
        
        status = sqlite3_step(sqlStament);
        while(status == SQLITE_ROW)
        {
            AmenitiesItemsModelV2 *model = [[AmenitiesItemsModelV2 alloc] init];
            
            model.itemId = sqlite3_column_int(self.sqlStament, 0);
            
            if (sqlite3_column_text(self.sqlStament, 1)) {
                model.itemName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(self.sqlStament, 1)];
            }
            
            if (sqlite3_column_text(self.sqlStament, 2)) {
                model.itemLang = [NSString stringWithUTF8String:(char *)sqlite3_column_text(self.sqlStament, 2)];
            }
            
            model.itemCategoryId = sqlite3_column_int(self.sqlStament, 3);
            
            if (sqlite3_column_text(self.sqlStament, 4)) {
                model.itemImage = [NSData dataWithBytes:sqlite3_column_blob(sqlStament, 4) length:sqlite3_column_bytes(sqlStament, 4)];
            }
            model.itemRoomType = sqlite3_column_int(self.sqlStament, 5);
            if (sqlite3_column_text(self.sqlStament, 6)) {
                model.itemLastModified = [NSString stringWithUTF8String:(char *)sqlite3_column_text(self.sqlStament, 6)];
            }
            [array addObject:model];
             
            status = sqlite3_step(sqlStament);
        }
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                [array removeAllObjects];
                goto RE_ACCESS_DB;
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    return array;

}

-(NSMutableArray *)loadAllAmenitiesItemsByRoomTypeId:(NSInteger)amenitiesRoomTypeId
{
    NSMutableArray *array = [NSMutableArray array];
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ?",item_id , item_name, item_lang, item_category_id, item_image, item_roomtype_id, item_last_modified, AMENITIES_ITEMS, item_roomtype_id];
    
    const char *sql =[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    
    if(status == SQLITE_OK) {
        
        sqlite3_bind_int (self.sqlStament, 1, (int)amenitiesRoomTypeId);
        
        status =  sqlite3_step(sqlStament);
        while(status == SQLITE_ROW)
        {
            AmenitiesItemsModelV2 *model = [[AmenitiesItemsModelV2 alloc] init];
            
            model.itemId = sqlite3_column_int(self.sqlStament, 0);
            
            if (sqlite3_column_text(self.sqlStament, 1)) {
                model.itemName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(self.sqlStament, 1)];
            }
            
            if (sqlite3_column_text(self.sqlStament, 2)) {
                model.itemLang = [NSString stringWithUTF8String:(char *)sqlite3_column_text(self.sqlStament, 2)];
            }
            
            model.itemCategoryId = sqlite3_column_int(self.sqlStament, 3);
            
            if (sqlite3_column_text(self.sqlStament, 4)) {
                model.itemImage = [NSData dataWithBytes:sqlite3_column_blob(sqlStament, 4) length:sqlite3_column_bytes(sqlStament, 4)];
            }
            model.itemRoomType = sqlite3_column_int(self.sqlStament, 5);
            if (sqlite3_column_text(self.sqlStament, 6)) {
                model.itemLastModified = [NSString stringWithUTF8String:(char *)sqlite3_column_text(self.sqlStament, 6)];
            }
            [array addObject:model];
            
            status = sqlite3_step(sqlStament);
        }
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                [array removeAllObjects];
                goto RE_ACCESS_DB;
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    return array;

}

-(AmenitiesItemsModelV2 *)loadAmenitiesItemLatest
{
    NSMutableArray *array = [NSMutableArray array];
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@ FROM %@ ORDER BY %@ DESC LIMIT 1", item_last_modified, AMENITIES_ITEMS, item_last_modified];
    
    const char *sql =[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) ;
    
    if(status == SQLITE_OK) {
        
        status = sqlite3_step(sqlStament);
        while(status == SQLITE_ROW)
        {
            AmenitiesItemsModelV2 *model = [[AmenitiesItemsModelV2 alloc] init];
            
            if (sqlite3_column_text(self.sqlStament, 0)) {
                model.itemLastModified = [NSString stringWithUTF8String:(char *)sqlite3_column_text(self.sqlStament, 0)];
            }
            [array addObject:model];
            
            status = sqlite3_step(sqlStament);
        }
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                [array removeAllObjects];
                goto RE_ACCESS_DB;
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    AmenitiesItemsModelV2 *itemLatest = nil;
    if (array.count != 0) {
        itemLatest = [array objectAtIndex:0];
    }
    return itemLatest;
}

- (NSMutableArray *) loadAmenitiesItemsModelByCateId:(NSInteger) cateId{
    
    NSMutableArray *array = [NSMutableArray array];
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ?",item_id , item_name, item_lang, item_category_id, item_image, item_roomtype_id, item_last_modified, AMENITIES_ITEMS, item_category_idtemp];
    
    const char *sql =[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    reAccessCount ++;
}
    
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    
    if(status == SQLITE_OK) {
        
        sqlite3_bind_int (self.sqlStament, 1, (int)cateId);
        
        status =  sqlite3_step(sqlStament);
        while(status == SQLITE_ROW)
        {
            AmenitiesItemsModelV2 *model = [[AmenitiesItemsModelV2 alloc] init];
            
            model.itemId = sqlite3_column_int(self.sqlStament, 0);
            
            if (sqlite3_column_text(self.sqlStament, 1)) {
                model.itemName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(self.sqlStament, 1)];
            }
            
            if (sqlite3_column_text(self.sqlStament, 2)) {
                model.itemLang = [NSString stringWithUTF8String:(char *)sqlite3_column_text(self.sqlStament, 2)];
            }
            
            model.itemCategoryId = sqlite3_column_int(self.sqlStament, 3);
            
            if (sqlite3_column_text(self.sqlStament, 4)) {
                model.itemImage = [NSData dataWithBytes:sqlite3_column_blob(sqlStament, 4) length:sqlite3_column_bytes(sqlStament, 4)];
            }
            model.itemRoomType = sqlite3_column_int(self.sqlStament, 5);
            if (sqlite3_column_text(self.sqlStament, 6)) {
                model.itemLastModified = [NSString stringWithUTF8String:(char *)sqlite3_column_text(self.sqlStament, 6)];
            }
            [array addObject:model];
            
            status = sqlite3_step(sqlStament);
        }
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                [array removeAllObjects];
                goto RE_ACCESS_DB;
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    return array;

}

- (NSMutableArray*)loadAmenitiesItemsModelByCateId:(NSInteger)cateId andRoomTypeId:(NSInteger)roomTypeId {
    
    NSMutableArray *array = [NSMutableArray array];
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@ FROM %@ a JOIN %@ b ON a.%@ = b.%@ WHERE a.%@ = ? AND b.%@ = ? ORDER BY %@ ASC",
                           item_id,
                           item_name,
                           item_lang,
                           item_category_id,
                           item_image,
                           item_roomtype_id,
                           item_last_modified,
                           
                           AMENITIES_ITEMS,
                           ROOMTYPE_INVENTORY,
                           
                           item_id,
                           rti_item_id,
                           
                           item_category_idtemp,
                           rti_roomtype_id,
                           item_id
                           ];
    
    const char *sql =[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    reAccessCount ++;
}
    
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    
    if(status == SQLITE_OK) {
        
        sqlite3_bind_int (self.sqlStament, 1, (int)cateId);
        sqlite3_bind_int (self.sqlStament, 2, (int)roomTypeId);
        
        status =  sqlite3_step(sqlStament);
        while(status == SQLITE_ROW) {
            
            AmenitiesItemsModelV2 *model = [[AmenitiesItemsModelV2 alloc] init];
            
            model.itemId = sqlite3_column_int(self.sqlStament, 0);
            
            if (sqlite3_column_text(self.sqlStament, 1)) {
                model.itemName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(self.sqlStament, 1)];
            }
            
            if (sqlite3_column_text(self.sqlStament, 2)) {
                model.itemLang = [NSString stringWithUTF8String:(char *)sqlite3_column_text(self.sqlStament, 2)];
            }
            
            model.itemCategoryId = sqlite3_column_int(self.sqlStament, 3);
            
            if (sqlite3_column_text(self.sqlStament, 4)) {
                model.itemImage = [NSData dataWithBytes:sqlite3_column_blob(sqlStament, 4) length:sqlite3_column_bytes(sqlStament, 4)];
            }
            model.itemRoomType = sqlite3_column_int(self.sqlStament, 5);
            if (sqlite3_column_text(self.sqlStament, 6)) {
                model.itemLastModified = [NSString stringWithUTF8String:(char *)sqlite3_column_text(self.sqlStament, 6)];
            }
            [array addObject:model];
            
            status = sqlite3_step(sqlStament);
        }
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                [array removeAllObjects];
                goto RE_ACCESS_DB;
            }
        }
    }
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    return array;
}

@end