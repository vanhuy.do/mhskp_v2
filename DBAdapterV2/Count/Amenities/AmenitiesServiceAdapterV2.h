//
//  AmenitiesServiceAdapterV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 3/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "DatabaseAdapterV2.h"
#import "AmenitiesServiceModelV2.h"

@interface AmenitiesServiceAdapterV2 : DatabaseAdapterV2

-(BOOL) insertAmenitiesServiceModel:(AmenitiesServiceModelV2 *)model;

-(AmenitiesServiceModelV2 *) loadAmenitiesServiceModelByPrimaryKey:(AmenitiesServiceModelV2 *) model;

-(BOOL) updateAmenitiesServiceModel:(AmenitiesServiceModelV2 *) model;

-(BOOL) deleteAmenitiesServiceModel:(AmenitiesServiceModelV2 *) model;

-(NSMutableArray *) loadAllAmenitiesServiceModel;

-(AmenitiesServiceModelV2 *) loadAmenitiesRoomTypeLatest;

@end
