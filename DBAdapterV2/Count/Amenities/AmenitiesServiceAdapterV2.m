//
//  AmenitiesServiceAdapterV2.m
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 3/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AmenitiesServiceAdapterV2.h"
#import "DbDefinesV2.h"
#import "ehkDefines.h"

@implementation AmenitiesServiceAdapterV2

- (id)init
{
	self = [super init];
	if (self) {
		// Initialization code here.
	}
	return self;
    
}

-(BOOL)insertAmenitiesServiceModel:(AmenitiesServiceModelV2 *)model {
	NSString *sqlString = [NSString stringWithFormat:@"INSERT INTO AMENITIES_SERVICE ( %@, %@, %@, %@, %@) VALUES ( ?, ?, ?, ?, ?)" , ams_id, ams_name, ams_name_lang, ams_last_modified,ams_image ];
    
	const char *sql = [sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
	if(sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {
		sqlite3_bind_int(sqlStament, 1, (int)model.amsId);
		sqlite3_bind_text(sqlStament, 2, [model.amsName UTF8String], -1, SQLITE_TRANSIENT);
		sqlite3_bind_text(sqlStament, 3, [model.amsNameLang UTF8String], -1, SQLITE_TRANSIENT);
		sqlite3_bind_text(sqlStament, 4, [model.amsLastModified UTF8String], -1, SQLITE_TRANSIENT);
        if(model.amsImage != nil)
            sqlite3_bind_blob(self.sqlStament, 5, [model.amsImage bytes], (int)[model.amsImage length], NULL);
        else
            sqlite3_bind_blob(self.sqlStament, 5, nil, -1, NULL);

	}
	NSInteger status = sqlite3_step(sqlStament);
	if (status == SQLITE_DONE) {
		sqlite3_last_insert_rowid(database);
		sqlite3_reset(sqlStament);
		return YES;
	}
    else{
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        } 
           
        return NO;
    }
	return  NO;
}

-(AmenitiesServiceModelV2 *)loadAmenitiesServiceModelByPrimaryKey:(AmenitiesServiceModelV2 *)model {
	NSString *sqlString = [NSString stringWithFormat:@"SELECT %@, %@, %@, %@, %@ FROM %@ WHERE  %@ = ?" , ams_id, ams_name, ams_name_lang, ams_last_modified,ams_image, AMENITIES_SERVICE, ams_id];
	const char *sql = [sqlString UTF8String];
	
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSInteger status;
    status = sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL);
	if(status == SQLITE_OK) {
		sqlite3_bind_int(sqlStament, 1, (int)model.amsId);
        
        NSInteger status = sqlite3_step(sqlStament);
		while (status == SQLITE_ROW) {
			model.amsId = sqlite3_column_int(sqlStament, 0);
			if (sqlite3_column_text(sqlStament, 1)) {
				model.amsName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
			}
			if (sqlite3_column_text(sqlStament, 2)) {
				model.amsNameLang = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
			}
			if (sqlite3_column_text(sqlStament, 3)) {
				model.amsLastModified = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
			}
            if (sqlite3_column_bytes(sqlStament, 4) > 0) {
                model.amsImage = [NSData dataWithBytes:sqlite3_column_blob(sqlStament, 4) length:sqlite3_column_bytes(sqlStament, 4)];
            }

			return model;
		}
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
	}
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
	return model;
}

-(BOOL)updateAmenitiesServiceModel:(AmenitiesServiceModelV2 *)model {
	NSString *sqlString = [NSString stringWithFormat:@"UPDATE %@ SET %@ = ?, %@ = ?, %@ = ?, %@ = ? WHERE  %@ = ?", AMENITIES_SERVICE, ams_name, ams_name_lang, ams_last_modified,ams_image, ams_id];
	const char *sql = [sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
	if(sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {
		sqlite3_bind_text(sqlStament, 1, [model.amsName UTF8String], -1, SQLITE_TRANSIENT);
		sqlite3_bind_text(sqlStament, 2, [model.amsNameLang UTF8String], -1, SQLITE_TRANSIENT);
		sqlite3_bind_text(sqlStament, 3, [model.amsLastModified UTF8String], -1, SQLITE_TRANSIENT);
        if(model.amsImage != nil)
            sqlite3_bind_blob(self.sqlStament, 4, [model.amsImage bytes], (int)[model.amsImage length], NULL);
        else
            sqlite3_bind_blob(self.sqlStament, 4, nil, -1, NULL);
		sqlite3_bind_int(sqlStament, 5, (int)model.amsId);
       
	}
    
	NSInteger status = sqlite3_step(sqlStament);
	if (status == SQLITE_DONE) {
		return YES;
	}
    else{
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return NO;
    }
	return  NO;
}

-(BOOL)deleteAmenitiesServiceModel:(AmenitiesServiceModelV2 *)model {
	NSString *sqlString = [NSString stringWithFormat:@"DELETE FROM %@ WHERE %@ = ?", AMENITIES_SERVICE, ams_id];
	const char *sql = [sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
        
	if(sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {
		sqlite3_bind_int(sqlStament, 1, (int)model.amsId);
	}
    
	NSInteger status = sqlite3_step(sqlStament);
	if (status == SQLITE_DONE) {
		return YES;
	}
    else{
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return NO;
    }
	return  NO;
}

-(NSMutableArray *)loadAllAmenitiesServiceModel
{
    NSMutableArray *array = [NSMutableArray array];
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@ FROM %@", ams_id, ams_name,ams_name_lang, ams_last_modified,ams_image, AMENITIES_SERVICE];
    const char *sql =[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    
    if(status == SQLITE_OK) {
        
        NSInteger status = sqlite3_step(sqlStament);
        while(status == SQLITE_ROW)
        {
            AmenitiesServiceModelV2 *model = [[AmenitiesServiceModelV2 alloc] init];
            
            model.amsId = sqlite3_column_int(self.sqlStament, 0);
            
            if (sqlite3_column_text(self.sqlStament, 1)) {
                model.amsName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(self.sqlStament, 1)];
            }

            if (sqlite3_column_text(self.sqlStament, 2)) {
                model.amsNameLang = [NSString stringWithUTF8String:(char *)sqlite3_column_text(self.sqlStament, 2)];
            }

            if (sqlite3_column_text(self.sqlStament, 3)) {
                model.amsLastModified = [NSString stringWithUTF8String:(char *)sqlite3_column_text(self.sqlStament, 3)];
            }
            if (sqlite3_column_bytes(sqlStament, 4) > 0) {
                model.amsImage = [NSData dataWithBytes:sqlite3_column_blob(sqlStament, 4) length:sqlite3_column_bytes(sqlStament, 4)];
            }

            [array addObject:model];
            
            status = sqlite3_step(sqlStament);
        }
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                [array removeAllObjects];
                goto RE_ACCESS_DB;
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    
    return array;

}

-(AmenitiesServiceModelV2 *)loadAmenitiesRoomTypeLatest
{
    NSMutableArray *array = [NSMutableArray array];
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@ FROM %@ ORDER BY %@ DESC",ams_last_modified, AMENITIES_SERVICE, ams_last_modified];
    const char *sql =[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        
        status = sqlite3_step(sqlStament);
        while(status == SQLITE_ROW)
        {
            AmenitiesServiceModelV2 *model = [[AmenitiesServiceModelV2 alloc] init];
            
            if (sqlite3_column_text(self.sqlStament, 0)) {
                model.amsLastModified = [NSString stringWithUTF8String:(char *)sqlite3_column_text(self.sqlStament, 0)];
            }
                       
            [array addObject:model];
            
            status = sqlite3_step(sqlStament);
        }
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                [array removeAllObjects];
                goto RE_ACCESS_DB;
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    AmenitiesServiceModelV2 *roomType = nil;
    if (array.count != 0) {
        roomType = [array objectAtIndex:0];
    }
    return roomType;
}

@end
