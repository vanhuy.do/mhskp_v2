//
//  CountCategoryAdapterV2.h
//  mHouseKeeping
//
//  Created by TMS on 3/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DatabaseAdapterV2.h"
#import "CountCategoryModelV2.h"

@interface CountCategoryAdapterV2 : DatabaseAdapterV2

-(BOOL)insertCountCategoryModel:(CountCategoryModelV2 *) model ;
-(BOOL) updateCountCategoryModel:(CountCategoryModelV2 *) model;
-(BOOL) deleteCountCategoryModel:(CountCategoryModelV2 *) model;
-(CountCategoryModelV2 *) loadCountCategoryModel:(CountCategoryModelV2 *) model;
-(NSMutableArray *) loadAllCountCategoryModelsByCountServiceId:(NSInteger) countServiceId;
-(CountCategoryModelV2 *) loadCountCategoryByCountServiceIdAndCategoryIdData:(CountCategoryModelV2 *) model;
-(CountCategoryModelV2 *) loadCountCategoryLatestWithHotelIdData:(NSInteger) hotelId AndCountServiceId:(NSInteger) serviceId;
- (NSMutableArray*)loadAllCountCategoryModelsByCountServiceId:(NSInteger)countServiceId andRoomTypeId:(NSInteger) roomTypeId;
@end
