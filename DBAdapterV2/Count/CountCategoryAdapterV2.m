//
//  CountCategoryAdapterV2.m
//  mHouseKeeping
//
//  Created by TMS on 3/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CountCategoryAdapterV2.h"
#import "ehkDefines.h"

@implementation CountCategoryAdapterV2

-(BOOL)insertCountCategoryModel:(CountCategoryModelV2 *)model {
    NSString *sqlString = [[NSString alloc] initWithFormat:@"INSERT INTO %@ (%@ ,%@ ,%@, %@, %@, %@, %@) VALUES(?, ?, ?, ?, ?, ?, ?)", 
                           COUNT_CATEGORIES,
                           cc_id,
                           c_service_id,
                           cc_name,
                           cc_hotel_id,
                           cc_name_lang,
                           cc_image,
                           cc_last_modified
                           ];
    
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK){
    }
    
    sqlite3_bind_int (sqlStament, 1, (int)model.countCategoryId);
    sqlite3_bind_int(sqlStament, 2,(int) model.countServiceId);
    sqlite3_bind_text(sqlStament, 3, [model.countCategoryName UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(sqlStament, 4, [model.countHotelId UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(sqlStament, 5, [model.countCategoryNameLang UTF8String], -1, SQLITE_TRANSIENT);
    if(model.countCategoryImage != nil)
        sqlite3_bind_blob(sqlStament, 6, [model.countCategoryImage bytes],(int) [model.countCategoryImage length], NULL);
    else
        sqlite3_bind_blob(self.sqlStament, 6, nil, -1, NULL);
    
     sqlite3_bind_text(sqlStament, 7, [model.countCategoryLastModified UTF8String], -1, SQLITE_TRANSIENT);
    
    NSInteger status = sqlite3_step(self.sqlStament);
    if(SQLITE_DONE != status)
    {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return NO;
    }
    else
    {
        sqlite3_last_insert_rowid(database);
        return YES;
    }
    return NO;
}

-(BOOL)updateCountCategoryModel:(CountCategoryModelV2 *)model {
    NSString *sqlString = [[NSString alloc] initWithFormat:@"UPDATE %@ SET %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ? WHERE %@ = ?", 
                           COUNT_CATEGORIES,
                           c_service_id,
                           cc_name,
                           cc_hotel_id,
                           cc_name_lang,
                           cc_image,
                           cc_id,
                           cc_last_modified
                           ];
    
    const char *sql=[sqlString UTF8String];
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK){
    }
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    sqlite3_bind_int(sqlStament, 1, (int)model.countServiceId);
    sqlite3_bind_text(sqlStament, 2, [model.countCategoryName UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(sqlStament, 3, [model.countHotelId UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(sqlStament, 4, [model.countCategoryNameLang UTF8String], -1, SQLITE_TRANSIENT);
    if(model.countCategoryImage != nil)
        sqlite3_bind_blob(sqlStament, 5, [model.countCategoryImage bytes], (int)[model.countCategoryImage length], NULL);
    else
        sqlite3_bind_blob(self.sqlStament, 5, nil, -1, NULL);
    sqlite3_bind_int (sqlStament, 6, (int)model.countCategoryId);
    
    sqlite3_bind_text(sqlStament, 7, [model.countCategoryLastModified UTF8String], -1, SQLITE_TRANSIENT);
    
    NSInteger status =  sqlite3_step(self.sqlStament);
    if(SQLITE_DONE != status)
    {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return NO;
    }
    else
    {
        sqlite3_last_insert_rowid(database);
        return YES;
    }
    return NO;
}

-(BOOL)deleteCountCategoryModel:(CountCategoryModelV2 *)model {
    NSString *sqlString = [[NSString alloc] initWithFormat:@"DELETE FROM %@ WHERE %@ = ?", 
                                  COUNT_CATEGORIES,
                                  cc_id
                                  ];
    
    const char *sql=[sqlString UTF8String];
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK){
    }
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    sqlite3_bind_int (self.sqlStament, 1, (int)model.countCategoryId);
    
    NSInteger status = sqlite3_step(self.sqlStament);
    if(SQLITE_DONE != status)
    {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return NO;
    }
    else
    {
        sqlite3_last_insert_rowid(database);
        return YES;
    }
    return NO;
}

-(CountCategoryModelV2 *)loadCountCategoryModel:(CountCategoryModelV2 *)model {
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@ ,%@, %@, %@, %@, %@ FROM %@ WHERE %@ = ?", 
                                  c_service_id,
                                  cc_name,
                                  cc_hotel_id,
                                  cc_name_lang,
                                  cc_image,
                                  cc_last_modified,
                                  COUNT_CATEGORIES,
                                  cc_id
                                  ];
    
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
        
    NSInteger status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        
        sqlite3_bind_int (self.sqlStament, 1, (int)model.countCategoryId);
        status = sqlite3_step(self.sqlStament);
        while(status == SQLITE_ROW) 
        {
            model.countServiceId = sqlite3_column_int(sqlStament, 0);
            if (sqlite3_column_text(sqlStament, 1)) {
                model.countCategoryName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
            }
            if (sqlite3_column_text(sqlStament, 2)) {
                model.countHotelId = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
            }
            if (sqlite3_column_text(sqlStament, 3)) {
                model.countCategoryNameLang = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
            }
            if (sqlite3_column_bytes(sqlStament, 4)) {
                model.countCategoryImage = [NSData dataWithBytes:sqlite3_column_blob(sqlStament, 4) length:sqlite3_column_bytes(sqlStament, 4)];
            }
            if (sqlite3_column_text(sqlStament, 5)) {
                model.countCategoryLastModified = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
            }

            return model;
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    return model;
}

-(NSMutableArray *)loadAllCountCategoryModelsByCountServiceId:(NSInteger)countServiceId {
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@ ,%@, %@, %@, %@ %@ FROM %@ WHERE %@ = ?", 
                                  cc_id,
                                  c_service_id,
                                  cc_name,
                                  cc_hotel_id,
                                  cc_name_lang,
                                  cc_image,
                                  cc_last_modified,
                                  COUNT_CATEGORIES,
                                  c_service_id
                                  ];
    
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSMutableArray *array = [NSMutableArray array];
    NSInteger status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        
        sqlite3_bind_int (self.sqlStament, 1, (int)countServiceId);
        
       status = sqlite3_step(self.sqlStament);
        while(status == SQLITE_ROW) 
        {
            CountCategoryModelV2 *model = [[CountCategoryModelV2 alloc] init];
            
            model.countCategoryId = sqlite3_column_int(sqlStament, 0);
            
            model.countServiceId = sqlite3_column_int(sqlStament, 1);
            
            if (sqlite3_column_text(sqlStament, 2)) {
                model.countCategoryName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
            }
            if (sqlite3_column_text(sqlStament, 3)) {
                model.countHotelId = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
            }
            if (sqlite3_column_text(sqlStament, 4)) {
                model.countCategoryNameLang = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
            }
            if (sqlite3_column_bytes(sqlStament, 5)) {
                model.countCategoryImage = [NSData dataWithBytes:sqlite3_column_blob(sqlStament, 5) length:sqlite3_column_bytes(sqlStament, 5)];
            }
            if (sqlite3_column_text(sqlStament, 6)) {
                model.countCategoryLastModified = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 6)];
            }
            [array addObject:model];
            
            status = sqlite3_step(self.sqlStament);
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                [array removeAllObjects];
                goto RE_ACCESS_DB;
            }
        }
    }
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    return array;
}

-(CountCategoryModelV2 *)loadCountCategoryByCountServiceIdAndCategoryIdData:(CountCategoryModelV2 *)model
{
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@ ,%@, %@, %@, %@ %@ FROM %@ WHERE %@ = ? AND %@ = ?", 
                           cc_id,
                           c_service_id,
                           cc_name,
                           cc_hotel_id,
                           cc_name_lang,
                           cc_image,
                           cc_last_modified,
                           COUNT_CATEGORIES,
                           c_service_id,
                           cc_id
                           ];
    
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
        
    NSInteger status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        
        sqlite3_bind_int (self.sqlStament, 1, (int)model.countServiceId);
        sqlite3_bind_int (self.sqlStament, 2, (int)model.countCategoryId);

        status = sqlite3_step(self.sqlStament);
        while(status == SQLITE_ROW) 
        {
            model.countCategoryId = sqlite3_column_int(sqlStament, 0);
            model.countServiceId = sqlite3_column_int(sqlStament, 1);
            if (sqlite3_column_text(sqlStament, 2)) {
                model.countCategoryName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
            }
            if (sqlite3_column_text(sqlStament, 3)) {
                model.countHotelId = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
            }
            if (sqlite3_column_text(sqlStament, 4)) {
                model.countCategoryNameLang = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
            }
            if (sqlite3_column_bytes(sqlStament, 5)) {
                model.countCategoryImage = [NSData dataWithBytes:sqlite3_column_blob(sqlStament, 4) length:sqlite3_column_bytes(sqlStament, 5)];
            }
            if (sqlite3_column_text(sqlStament, 6)) {
                model.countCategoryLastModified = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 6)];
            }
            return model;
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    return model;

}

-(CountCategoryModelV2 *)loadCountCategoryLatestWithHotelIdData:(NSInteger)hotelId AndCountServiceId:(NSInteger)serviceId
{
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@ ,%@, %@, %@, %@, %@ FROM %@ WHERE %@ = ? AND %@ = ? ORDER BY %@ DESC", 
                           cc_id,
                           c_service_id,
                           cc_name,
                           cc_hotel_id,
                           cc_name_lang,
                           cc_image,
                           cc_last_modified,
                           COUNT_CATEGORIES,
                           cc_hotel_id,
                           c_service_id,
                           cc_last_modified
                           ];
    
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSMutableArray *array = [NSMutableArray array];
    NSInteger status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        
        sqlite3_bind_int (self.sqlStament, 1, (int)hotelId);
        sqlite3_bind_int (self.sqlStament, 2, (int)serviceId);
        
        status = sqlite3_step(self.sqlStament);
        while(status == SQLITE_ROW) 
        {
            CountCategoryModelV2 *model = [[CountCategoryModelV2 alloc] init];
            
            model.countCategoryId = sqlite3_column_int(sqlStament, 0);
            
            model.countServiceId = sqlite3_column_int(sqlStament, 1);
            
            if (sqlite3_column_text(sqlStament, 2)) {
                model.countCategoryName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
            }
            if (sqlite3_column_text(sqlStament, 3)) {
                model.countHotelId = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
            }
            if (sqlite3_column_text(sqlStament, 4)) {
                model.countCategoryNameLang = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
            }
            if (sqlite3_column_bytes(sqlStament, 5)) {
                model.countCategoryImage = [NSData dataWithBytes:sqlite3_column_blob(sqlStament, 5) length:sqlite3_column_bytes(sqlStament, 5)];
            }
            if (sqlite3_column_text(sqlStament, 6)) {
                model.countCategoryLastModified = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 6)];
            }
            [array addObject:model];
            
            status = sqlite3_step(self.sqlStament);
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                [array removeAllObjects];
                goto RE_ACCESS_DB;
            }
        }
    }
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    CountCategoryModelV2 *countCategoryFirst = nil;
    if (array.count != 0) {
        countCategoryFirst = [array objectAtIndex:0];
    }
    return countCategoryFirst;
}

- (NSMutableArray *)loadAllCountCategoryModelsByCountServiceId:(NSInteger)countServiceId andRoomTypeId:(NSInteger) roomTypeId {
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT DISTINCT %@, %@, %@, %@, %@, %@, %@ FROM %@ a JOIN %@ b ON a.%@ = b.%@ WHERE a.%@ = ? and b.%@ = ?",
                           cc_id,
                           c_service_id,
                           cc_name,
                           cc_hotel_id,
                           cc_name_lang,
                           cc_image,
                           cc_last_modified,
                           
                           COUNT_CATEGORIES,
                           ROOMTYPE_INVENTORY,
                           
                           cc_id,
                           rti_category_id,
                           
                           c_service_id,
                           rti_roomtype_id
                           ];
    
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    reAccessCount ++;
}
    
    NSMutableArray *array = [NSMutableArray array];
    NSInteger status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        
        sqlite3_bind_int (self.sqlStament, 1,(int) countServiceId);
        sqlite3_bind_int (self.sqlStament, 2, (int)roomTypeId);
        
        status = sqlite3_step(self.sqlStament);
        while(status == SQLITE_ROW)
        {
            CountCategoryModelV2 *model = [[CountCategoryModelV2 alloc] init];
            
            model.countCategoryId = sqlite3_column_int(sqlStament, 0);
            
            model.countServiceId = sqlite3_column_int(sqlStament, 1);
            
            if (sqlite3_column_text(sqlStament, 2)) {
                model.countCategoryName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
            }
            if (sqlite3_column_text(sqlStament, 3)) {
                model.countHotelId = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
            }
            if (sqlite3_column_text(sqlStament, 4)) {
                model.countCategoryNameLang = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
            }
            if (sqlite3_column_bytes(sqlStament, 5)) {
                model.countCategoryImage = [NSData dataWithBytes:sqlite3_column_blob(sqlStament, 5) length:sqlite3_column_bytes(sqlStament, 5)];
            }
            if (sqlite3_column_text(sqlStament, 6)) {
                model.countCategoryLastModified = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 6)];
            }
            [array addObject:model];
            
            status = sqlite3_step(self.sqlStament);
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                [array removeAllObjects];
                goto RE_ACCESS_DB;
            }
        }
    }
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    return array;
}

@end
