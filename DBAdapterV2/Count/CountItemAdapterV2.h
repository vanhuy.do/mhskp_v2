//
//  CountItemAdapterV2.h
//  mHouseKeeping
//
//  Created by TMS on 3/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DatabaseAdapterV2.h"
#import "CountItemsModelV2.h"

@interface CountItemAdapterV2 : DatabaseAdapterV2

-(BOOL)insertCountItemsModel:(CountItemsModelV2 *) model;
-(BOOL) updateCountItemsModel:(CountItemsModelV2 *) model;
-(BOOL) deleteCountItemsModel:(CountItemsModelV2 *) model;
-(CountItemsModelV2 *) loadCountItemsModel:(CountItemsModelV2 *) model;
-(NSMutableArray *) loadAllCountItemModelByCountCategoryId:(NSInteger) countCategoryId;
-(CountItemsModelV2 *) loadCountItemsByCountCategoryIdAndCountItemIdData:(CountItemsModelV2 *) model;
-(CountItemsModelV2 *) loadCountItemsLatestWithHotelIdData:(NSInteger) hotelId AndServiceId:(NSInteger) serviceId;
- (NSMutableArray*)loadAllCountItemModelByCountCategoryId:(NSInteger) countCategoryId andRoomTypeId:(NSInteger) roomTypeId;
@end
