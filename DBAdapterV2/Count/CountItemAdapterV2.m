//
//  CountItemAdapterV2.m
//  mHouseKeeping
//
//  Created by TMS on 3/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CountItemAdapterV2.h"
#import "ehkDefines.h"

@implementation CountItemAdapterV2

-(BOOL)insertCountItemsModel:(CountItemsModelV2 *)model {
    NSString *sqlString = [[NSString alloc] initWithFormat:@"INSERT INTO %@ (%@ ,%@ ,%@, %@, %@, %@, %@, %@) VALUES(?, ?, ?, ?, ?, ?, ?, ?)", 
                           COUNT_ITEMS,
                           count_id,
                           count_name,
                           count_unit_price,
                           count_type_id,
                           count_lang,
                           count_category_id,
                           count_image,
                           count_last_modified
                           ];
    
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK){
    }
    
    sqlite3_bind_int (sqlStament, 1, (int)model.countItemId);
    sqlite3_bind_text(sqlStament, 2, [model.countItemName UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_double(sqlStament, 3, model.countUnitPrice);
    sqlite3_bind_int(sqlStament, 4, (int)model.countItemType);
    sqlite3_bind_text(sqlStament, 5, [model.countItemLang UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_int(sqlStament, 6, (int)model.countCategoryId);
    if(model.countItemImage != nil)
        sqlite3_bind_blob(sqlStament, 7, [model.countItemImage bytes], (int)[model.countItemImage length], NULL);
    else
        sqlite3_bind_blob(self.sqlStament, 7, nil, -1, NULL);
    
    sqlite3_bind_text(sqlStament, 8, [model.countItemLastModified UTF8String], -1, SQLITE_TRANSIENT);
    
    NSInteger status = sqlite3_step(self.sqlStament);
    if(SQLITE_DONE != status)
    {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return NO;
    }
    else
    {
        sqlite3_last_insert_rowid(database);
        return YES;
    }
    return NO;
}

-(BOOL)updateCountItemsModel:(CountItemsModelV2 *)model {
    NSString *sqlString = [[NSString alloc] initWithFormat:@"UPDATE %@ SET %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ? WHERE %@ = ?", 
                           COUNT_ITEMS,
                           count_name,
                           count_unit_price,
                           count_type_id,
                           count_lang,
                           count_category_id,
                           count_image,
                           count_last_modified,
                           count_id
                           ];
    
    const char *sql=[sqlString UTF8String];
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK){
    }
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    sqlite3_bind_text(sqlStament, 1, [model.countItemName UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_double(sqlStament, 2, model.countUnitPrice);
    sqlite3_bind_int(sqlStament, 3,(int) model.countItemType);
    sqlite3_bind_text(sqlStament, 4, [model.countItemLang UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_int(sqlStament, 5, (int)model.countCategoryId);
    if(model.countItemImage != nil)
        sqlite3_bind_blob(sqlStament, 6, [model.countItemImage bytes], (int)[model.countItemImage length], NULL);
    else
        sqlite3_bind_blob(self.sqlStament, 6, nil, -1, NULL);
    
    sqlite3_bind_text(sqlStament, 7, [model.countItemLastModified UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_int (sqlStament, 8, (int)model.countItemId);

    NSInteger status = sqlite3_step(self.sqlStament);
    if(SQLITE_DONE != status)
    {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return NO;
    }
    else
    {
        sqlite3_last_insert_rowid(database);
        return YES;
    }
    return NO;
}

-(BOOL)deleteCountItemsModel:(CountItemsModelV2 *)model {
    NSString *sqlString = [[NSString alloc] initWithFormat:@"DELETE FROM %@ WHERE %@ = ?", 
                                  COUNT_ITEMS,
                                  count_id
                                  ];
    
    const char *sql=[sqlString UTF8String];
    
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK){
    }
    
    sqlite3_bind_int (self.sqlStament, 1, (int)model.countItemId);
    
    NSInteger status = sqlite3_step(self.sqlStament);
    if(SQLITE_DONE != status)
    {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return NO;
    }
    else
    {
        sqlite3_last_insert_rowid(database);
        return YES;
    }
    return NO;
}

-(CountItemsModelV2 *)loadCountItemsModel:(CountItemsModelV2 *)model {
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@ ,%@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ?", 
                                  count_name,
                                  count_unit_price,
                                  count_type_id,
                                  count_lang,
                                  count_category_id,
                                  count_image,
                                  count_last_modified,
                                  COUNT_ITEMS,
                                  count_id
                                  ];
    
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSInteger status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    
    if(status == SQLITE_OK) {
        
        sqlite3_bind_int (self.sqlStament, 1, (int)model.countItemId);

        status = sqlite3_step(self.sqlStament);
        while(status == SQLITE_ROW) 
        {
            if (sqlite3_column_text(sqlStament, 0)) {
                model.countItemName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 0)];
            }
            model.countUnitPrice = sqlite3_column_double(sqlStament, 1);
            model.countItemType = sqlite3_column_int(sqlStament, 2);
            if (sqlite3_column_text(sqlStament, 3)) {
                model.countItemLang = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
            }
            model.countCategoryId = sqlite3_column_int(sqlStament, 4);
            if (sqlite3_column_bytes(sqlStament, 5) > 0 ) {
                model.countItemImage = [NSData dataWithBytes:sqlite3_column_blob(sqlStament, 5) length:sqlite3_column_bytes(sqlStament, 5)];
            }
            if (sqlite3_column_text(sqlStament, 6)) {
                model.countItemLastModified = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 6)];
            }

            return model;
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    return model;
}

-(NSMutableArray *)loadAllCountItemModelByCountCategoryId:(NSInteger)countCategoryId {
    // CFG [20160921/CRF-00001275] - Added rti_default_quantity
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@ ,%@, %@, %@, %@, %@, %@, 0 AS rti_default_quantity FROM %@ WHERE %@ = ? order by %@ asc",
                           count_id,
                           count_name,
                           count_unit_price,
                           count_type_id,
                           count_lang,
                           count_category_id,
                           count_image,
                           count_last_modified,
                           COUNT_ITEMS,
                           count_category_id,
                           count_id
                           ];
    // CFG [20160921/CRF-00001275] - End.
    
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSMutableArray *array = [NSMutableArray array];
    
    NSInteger status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        
        sqlite3_bind_int (self.sqlStament, 1, (int)countCategoryId);
        
        status = sqlite3_step(self.sqlStament);
        while(status == SQLITE_ROW) 
        {
            CountItemsModelV2 *model = [[CountItemsModelV2 alloc] init];
            model.countItemId = sqlite3_column_int(sqlStament, 0);
            if (sqlite3_column_text(sqlStament, 1)) {
                model.countItemName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
            }
            model.countUnitPrice = sqlite3_column_double(sqlStament, 2);
            model.countItemType = sqlite3_column_int(sqlStament, 3);
            if (sqlite3_column_text(sqlStament, 4)) {
                model.countItemLang = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
            }
            model.countCategoryId = sqlite3_column_int(sqlStament, 5);
            if (sqlite3_column_bytes(sqlStament, 6) > 0 ) {
                model.countItemImage = [NSData dataWithBytes:sqlite3_column_blob(sqlStament, 6) length:sqlite3_column_bytes(sqlStament, 6)];
            }
            if (sqlite3_column_text(sqlStament, 7)) {
                model.countItemLastModified = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 7)];
            }
            // CFG [20160921/CRF-00001275] - Added rti_default_quantity
            if (sqlite3_column_int(sqlStament, 8)) {
                model.countDefaultQuantity = sqlite3_column_int(sqlStament, 8);
            }
            // CFG [20160921/CRF-00001275] - End

            [array addObject:model];
            
            status = sqlite3_step(self.sqlStament);
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                [array removeAllObjects];
                goto RE_ACCESS_DB;
            }
        }
    }
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    return array;
}

-(CountItemsModelV2 *)loadCountItemsByCountCategoryIdAndCountItemIdData:(CountItemsModelV2 *)model
{
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@ ,%@, %@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ? AND %@ = ?", 
                           count_id,
                           count_name,
                           count_unit_price,
                           count_type_id,
                           count_lang,
                           count_category_id,
                           count_image,
                           count_last_modified,
                           COUNT_ITEMS,
                           count_category_id,
                           count_id
                           ];
    
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSInteger status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    
    if(status == SQLITE_OK) {
        
        sqlite3_bind_int (self.sqlStament, 1, (int)model.countCategoryId);
        sqlite3_bind_int (self.sqlStament, 2, (int)model.countItemId);
        
        status = sqlite3_step(self.sqlStament);
        while(status == SQLITE_ROW) 
        {
            model.countItemId = sqlite3_column_double(sqlStament, 0);

            if (sqlite3_column_text(sqlStament, 1)) {
                model.countItemName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
            }
            model.countUnitPrice = sqlite3_column_double(sqlStament, 2);
            model.countItemType = sqlite3_column_int(sqlStament, 3);
            if (sqlite3_column_text(sqlStament, 4)) {
                model.countItemLang = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
            }
            model.countCategoryId = sqlite3_column_int(sqlStament, 5);
            if (sqlite3_column_bytes(sqlStament, 6) > 0 ) {
                model.countItemImage = [NSData dataWithBytes:sqlite3_column_blob(sqlStament, 6) length:sqlite3_column_bytes(sqlStament, 6)];
            }
            if (sqlite3_column_text(sqlStament, 7)) {
                model.countItemLastModified = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 7)];
            }
            
            return model;
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    return model;

}

-(CountItemsModelV2 *)loadCountItemsLatestWithHotelIdData:(NSInteger)hotelId AndServiceId:(NSInteger)serviceId
{
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@ FROM %@ LEFT JOIN %@ WHERE %@ = ? AND %@ = ? AND %@ = %@ ORDER BY %@ DESC", 
                           
                           count_last_modified,
                           COUNT_ITEMS,
                           COUNT_CATEGORIES,
                           cc_hotel_id,
                           c_service_id,
                           cc_id,
                           count_category_id,
                           count_last_modified
                           ];
    
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSMutableArray *array = [NSMutableArray array];
    
    NSInteger status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        
        sqlite3_bind_int (self.sqlStament, 1, (int)hotelId);
        sqlite3_bind_int (self.sqlStament, 2, (int)serviceId);
        
        status = sqlite3_step(self.sqlStament);
        while(status == SQLITE_ROW) 
        {
            CountItemsModelV2 *model = [[CountItemsModelV2 alloc] init];
            
            if (sqlite3_column_text(sqlStament, 0)) {
                model.countItemLastModified = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 0)];
            }
            
            [array addObject:model];
            
            status = sqlite3_step(self.sqlStament);
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                [array removeAllObjects];
                goto RE_ACCESS_DB;
            }
        }
    }
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    CountItemsModelV2 *countItemFirst = nil;
    if (array.count != 0) {
        countItemFirst = [array objectAtIndex:0];

    }
    return countItemFirst;
}

- (NSMutableArray *)loadAllCountItemModelByCountCategoryId:(NSInteger)countCategoryId andRoomTypeId:(NSInteger) roomTypeId{
    // CFG [20160921/CRF-00001275] - Added rti_default_quantity
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@, %@, %@ FROM %@ a JOIN %@ b ON a.%@ = b.%@ WHERE a.%@ = ? AND b.%@ = ? ORDER BY %@ ASC",
                           count_id,
                           count_name,
                           count_unit_price,
                           count_type_id,
                           count_lang,
                           count_category_id,
                           count_image,
                           count_last_modified,
                           rti_default_quantity,
                           COUNT_ITEMS,
                           ROOMTYPE_INVENTORY,
                           
                           count_id,
                           rti_item_id,
                           
                           count_category_id,
                           rti_roomtype_id,
                           count_id
                           ];
    // CFG [20160921/CRF-00001275] - End.
    
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    reAccessCount ++;
}
    
    NSMutableArray *array = [NSMutableArray array];
    
    NSInteger status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        
        sqlite3_bind_int (self.sqlStament, 1, (int)countCategoryId);
        sqlite3_bind_int (self.sqlStament, 2, (int)roomTypeId);
        
        status = sqlite3_step(self.sqlStament);
        while(status == SQLITE_ROW)
        {
            CountItemsModelV2 *model = [[CountItemsModelV2 alloc] init];
            model.countItemId = sqlite3_column_int(sqlStament, 0);
            if (sqlite3_column_text(sqlStament, 1)) {
                model.countItemName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
            }
            model.countUnitPrice = sqlite3_column_double(sqlStament, 2);
            model.countItemType = sqlite3_column_int(sqlStament, 3);
            if (sqlite3_column_text(sqlStament, 4)) {
                model.countItemLang = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
            }
            model.countCategoryId = sqlite3_column_int(sqlStament, 5);
            if (sqlite3_column_bytes(sqlStament, 6) > 0 ) {
                model.countItemImage = [NSData dataWithBytes:sqlite3_column_blob(sqlStament, 6) length:sqlite3_column_bytes(sqlStament, 6)];
            }
            if (sqlite3_column_text(sqlStament, 7)) {
                model.countItemLastModified = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 7)];
            }
            // CFG [20160921/CRF-00001275] - Added rti_default_quantity
            if (sqlite3_column_int(sqlStament, 8)) {
                model.countDefaultQuantity = sqlite3_column_int(sqlStament, 8);
            }
            // CFG [20160921/CRF-00001275] - End
            
            [array addObject:model];
            
            status = sqlite3_step(self.sqlStament);
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                [array removeAllObjects];
                goto RE_ACCESS_DB;
            }
        }
    }
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    return array;
}

@end
