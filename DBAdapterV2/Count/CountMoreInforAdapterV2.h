//
//  CountMoreInforAdapterV2.h
//  mHouseKeeping
//
//  Created by TMS on 3/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DatabaseAdapterV2.h"
#import "CountMoreInforModelV2.h"

@interface CountMoreInforAdapterV2 : DatabaseAdapterV2

-(BOOL)insertCountMoreInforModel:(CountMoreInforModelV2 *) model ;
-(BOOL) updateCountMoreInforModel:(CountMoreInforModelV2 *) model;
-(BOOL) deleteCountMoreInforModel:(CountMoreInforModelV2 *) model;
-(CountMoreInforModelV2 *) loadCountMoreInforModel:(CountMoreInforModelV2 *) model;
-(NSMutableArray *) loadAllCountMoreInforByCountCategoryId:(NSInteger) countCategoryId;

@end
