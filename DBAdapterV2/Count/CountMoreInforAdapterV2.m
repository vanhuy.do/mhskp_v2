//
//  CountMoreInforAdapterV2.m
//  mHouseKeeping
//
//  Created by TMS on 3/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CountMoreInforAdapterV2.h"
#import "ehkDefines.h"

@implementation CountMoreInforAdapterV2

-(BOOL)insertCountMoreInforModel:(CountMoreInforModelV2 *)model {
    NSString *sqlString = [[NSString alloc] initWithFormat:@"INSERT INTO %@ (%@ ,%@ ,%@, %@) VALUES(?, ?, ?, ?)", 
                                  COUNT_MORE_INFOR,
                                  cmi_id,
                                  cmi_category_id,
                                  cmi_regular_or_express,
                                  cmi_male_or_female
                                  ];
    
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK){
    }
    
    sqlite3_bind_int (sqlStament, 1, (int)model.countMoreInforId);
    sqlite3_bind_int(sqlStament, 2, (int)model.countCategoryId);
    sqlite3_bind_int(sqlStament, 3, (int)model.countRegularOrExpress);
    sqlite3_bind_int(sqlStament, 4, (int)model.countMaleOrFemale);
    
    NSInteger status = sqlite3_step(self.sqlStament);
    if(SQLITE_DONE != status)
    {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return NO;
    }
    else
    {
        sqlite3_last_insert_rowid(database);
        return YES;
    }
    return NO;
}

-(BOOL)updateCountMoreInforModel:(CountMoreInforModelV2 *)model {
    NSString *sqlString = [[NSString alloc] initWithFormat:@"UPDATE %@ SET %@ = ?, %@ = ?, %@ = ? WHERE %@ = ?", 
                           COUNT_MORE_INFOR,
                           cmi_category_id,
                           cmi_regular_or_express,
                           cmi_male_or_female,
                           cmi_id
                           ];
    
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK){
    }
    
    sqlite3_bind_int(sqlStament, 1, (int)model.countCategoryId);
    sqlite3_bind_int(sqlStament, 2, (int)model.countRegularOrExpress);
    sqlite3_bind_int(sqlStament, 3, (int)model.countMaleOrFemale);
    sqlite3_bind_int (sqlStament, 4,(int) model.countMoreInforId);
    
    NSInteger status = sqlite3_step(self.sqlStament);
    if(SQLITE_DONE != status)
    {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return NO;
    }
    else
    {
        sqlite3_last_insert_rowid(database);
        return YES;
    }
    return NO;
}

-(BOOL)deleteCountMoreInforModel:(CountMoreInforModelV2 *)model {
    NSString *sqlString = [[NSString alloc] initWithFormat:@"DELETE FROM %@ WHERE %@ = ?", 
                                  COUNT_MORE_INFOR,
                                  cmi_id
                                  ];
    
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK){
    }
    
    sqlite3_bind_int (self.sqlStament, 1,(int) model.countMoreInforId);
    
    NSInteger status = sqlite3_step(self.sqlStament);
    if(SQLITE_DONE != status)
    {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return NO;
    }
    else
    {
        sqlite3_last_insert_rowid(database);
        return YES;
    }
    return NO;
}

-(CountMoreInforModelV2 *)loadCountMoreInforModel:(CountMoreInforModelV2 *)model {
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@ ,%@, %@ FROM %@ WHERE %@ = ?", 
                                  cmi_category_id,
                                  cmi_regular_or_express,
                                  cmi_male_or_female,
                                  COUNT_MORE_INFOR,
                                  cmi_id
                                  ];
    
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
        
    
    NSInteger status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) ;
    if(status == SQLITE_OK) {
        
        sqlite3_bind_int (self.sqlStament, 1, (int)model.countMoreInforId);
        status = sqlite3_step(self.sqlStament);
        while(status == SQLITE_ROW) 
        {
            model.countCategoryId = sqlite3_column_int(sqlStament, 0);
            model.countRegularOrExpress = sqlite3_column_int(sqlStament, 1);
            model.countMaleOrFemale = sqlite3_column_int(sqlStament, 2);
            return model;
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    return model;
}

-(NSMutableArray *)loadAllCountMoreInforByCountCategoryId:(NSInteger)countCategoryId {
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@ ,%@, %@ FROM %@ WHERE %@ = ?", 
                                  cmi_id,
                                  cmi_category_id,
                                  cmi_regular_or_express,
                                  cmi_male_or_female,
                                  COUNT_MORE_INFOR,
                                  cmi_category_id
                                  ];
    
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSMutableArray *array = [NSMutableArray array];
    NSInteger status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        
        sqlite3_bind_int (self.sqlStament, 1, (int)countCategoryId);
        status = sqlite3_step(self.sqlStament);
        while(status == SQLITE_ROW) 
        {
            CountMoreInforModelV2 *model = [[CountMoreInforModelV2 alloc] init];
            model.countMoreInforId = sqlite3_column_int(sqlStament, 0);
            model.countCategoryId = sqlite3_column_int(sqlStament, 1);
            model.countRegularOrExpress = sqlite3_column_int(sqlStament, 2);
            model.countMaleOrFemale = sqlite3_column_int(sqlStament, 3);
            [array addObject:model];
            
            status = sqlite3_step(self.sqlStament);
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                [array removeAllObjects];
                goto RE_ACCESS_DB;
            }
        }
    }
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    return array;
}

@end
