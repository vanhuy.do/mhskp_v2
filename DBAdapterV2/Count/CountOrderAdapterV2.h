//
//  CountOrderAdapterV2.h
//  mHouseKeeping
//
//  Created by TMS on 3/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DatabaseAdapterV2.h"
#import "CountOrdersModelV2.h"

@interface CountOrderAdapterV2 : DatabaseAdapterV2

-(BOOL)insertCountOrdersModel:(CountOrdersModelV2 *) model ;
-(BOOL) updateCountOrdersModel:(CountOrdersModelV2 *) model;
-(BOOL) deleteCountOrdersModelByOrderId:(int) orderId;
-(CountOrdersModelV2 *) loadCountOrdersModel:(CountOrdersModelV2 *) model;
-(NSMutableArray *) loadAllCountOrdersByRoomId:(NSInteger) countRoomId;
//-(CountOrdersModelV2 *) loadCountOrdersModelByRoomIdUserIdAndServiceId:(CountOrdersModelV2 *) model;
-(NSMutableArray *) loadAllCountOrdersByUserId:(NSInteger) userId AndServiceId:(NSInteger) serviceId;
-(NSMutableArray *) loadCountOrdersByUserId:(NSInteger) userId AndServiceId:(NSInteger) serviceId;
@end
