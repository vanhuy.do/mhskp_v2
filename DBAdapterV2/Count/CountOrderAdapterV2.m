//
//  CountOrderAdapterV2.m
//  mHouseKeeping
//
//  Created by TMS on 3/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CountOrderAdapterV2.h"
#import "ehkDefines.h"

@implementation CountOrderAdapterV2

-(BOOL)insertCountOrdersModel:(CountOrdersModelV2 *)model {
    
    if(model.countRoomNumber.length == 0){
        model.countRoomNumber = @"";
    }

    NSString *sqlString = [[NSString alloc] initWithFormat:@"INSERT INTO %@ (%@ ,%@ ,%@, %@, %@, %@, %@, %@) VALUES(?, ?, ?, ?, ?, ?, ?, ?)",
                           COUNT_ORDERS,
                           scount_room_id,
                           scount_user_id,
                           scount_status,
                           scount_collect_date,
                           scount_service_id,
                           scount_room_number,
                           scount_post_status,
                           scount_order_ws_id
                           ];
    
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK){
    }
    
    sqlite3_bind_int(sqlStament, 1, (int)model.countRoomAssignId);
    sqlite3_bind_int(sqlStament, 2, (int)model.countUserId);
    sqlite3_bind_int(sqlStament, 3, (int)model.countStatus);
    sqlite3_bind_text(sqlStament, 4, [model.countCollectDate UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_int(sqlStament, 5, (int)model.countServiceId);
    sqlite3_bind_text(sqlStament, 6, [model.countRoomNumber UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_int(sqlStament, 7, (int)model.countPostStatus);
    sqlite3_bind_int(sqlStament, 8, (int)model.countOrderWSId);

    NSInteger status = sqlite3_step(self.sqlStament);
    if(SQLITE_DONE != status)
    {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return NO;
    }
    else
    {
        sqlite3_last_insert_rowid(database);
        return YES;
    }
    return NO;
}

-(BOOL)updateCountOrdersModel:(CountOrdersModelV2 *)model {
    NSString *sqlString = [[NSString alloc] initWithFormat:@"UPDATE %@ SET %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ? WHERE %@ = ?",
                           COUNT_ORDERS,
                           scount_room_id,
                           scount_user_id,
                           scount_status,
                           scount_collect_date,
                           scount_service_id,
                           scount_room_number,
                           scount_post_status,
                           scount_order_ws_id,
                           scount_id
                           ];
    
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK){
    }
    
    sqlite3_bind_int(sqlStament, 1,(int) model.countRoomAssignId);
    sqlite3_bind_int(sqlStament, 2, (int)model.countUserId);
    sqlite3_bind_int(sqlStament, 3, (int)model.countStatus);
    sqlite3_bind_text(sqlStament, 4, [model.countCollectDate UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_int(sqlStament, 5, (int)model.countServiceId);
    sqlite3_bind_text(sqlStament, 6, [model.countRoomNumber UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_int(sqlStament, 7, (int)model.countPostStatus);
    sqlite3_bind_int(sqlStament, 8,(int) model.countOrderWSId);
    sqlite3_bind_int(sqlStament, 9, (int)model.countOrderId);
    
    NSInteger status = sqlite3_step(self.sqlStament);
    if(SQLITE_DONE != status)
    {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return NO;
    }
    else
    {
        sqlite3_last_insert_rowid(database);
        return YES;
    }
    return NO;
}

-(BOOL)deleteCountOrdersModelByOrderId:(int)orderId {
    NSString *sqlString = [[NSString alloc] initWithFormat:@"DELETE FROM %@ WHERE %@ = ?", 
                                  COUNT_ORDERS,
                                   scount_id
                                  ];
    
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK){
    }
    
    sqlite3_bind_int(sqlStament, 1, orderId);
    
    NSInteger status = sqlite3_step(self.sqlStament);
    if(SQLITE_DONE != status)
    {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return NO;
    }
    else
    {
        sqlite3_last_insert_rowid(database);
        return YES;
    }
    return NO;
}

-(CountOrdersModelV2 *)loadCountOrdersModel:(CountOrdersModelV2 *)model {
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@ ,%@, %@, %@ , %@, %@, %@, %@ FROM %@ WHERE %@ = ?",
                           scount_room_id,
                           scount_user_id,
                           scount_status,
                           scount_collect_date,
                           scount_service_id,
                           scount_room_number,
                           scount_post_status,
                           scount_order_ws_id,
                           COUNT_ORDERS,
                           scount_id
                           ];
    
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
            
    NSInteger status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        
        sqlite3_bind_int (self.sqlStament, 1, (int)model.countOrderId);
        
        status = sqlite3_step(self.sqlStament);
        while(status == SQLITE_ROW) 
        {
            model.countRoomAssignId = sqlite3_column_int(sqlStament, 0);
            model.countUserId = sqlite3_column_int(sqlStament, 1);
            model.countStatus = sqlite3_column_int(sqlStament, 2);
            if (sqlite3_column_text(sqlStament, 3)) {
                model.countCollectDate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
            }
            model.countServiceId = sqlite3_column_int(sqlStament, 4);
            if (sqlite3_column_text(sqlStament, 5)) {
                model.countRoomNumber = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
            }
            model.countPostStatus = sqlite3_column_int(sqlStament, 6);
            model.countOrderWSId = sqlite3_column_int(sqlStament, 7);
            
            return model;
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    return model;
}

-(NSMutableArray *)loadAllCountOrdersByRoomId:(NSInteger)countRoomId {
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@ ,%@, %@, %@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ?",
                           scount_id,
                           scount_room_id,
                           scount_user_id,
                           scount_status,
                           scount_collect_date,
                           scount_service_id,
                           scount_room_number,
                           scount_post_status,
                           scount_order_ws_id,
                           COUNT_ORDERS,
                           scount_room_id
                           ];
    
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSInteger status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    NSMutableArray *array = [NSMutableArray array];
    if(status == SQLITE_OK) {
        
        sqlite3_bind_int (self.sqlStament, 1, (int)countRoomId);
        status = sqlite3_step(self.sqlStament);
        while(status == SQLITE_ROW) 
        {
            CountOrdersModelV2 *model = [[CountOrdersModelV2 alloc] init];
            model.countOrderId = sqlite3_column_int(sqlStament, 0);
            model.countRoomAssignId = sqlite3_column_int(sqlStament, 1);
            model.countUserId = sqlite3_column_int(sqlStament, 2);
            model.countStatus = sqlite3_column_int(sqlStament, 3);
            if (sqlite3_column_text(sqlStament, 4)) {
                model.countCollectDate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
            }
            model.countServiceId = sqlite3_column_int(sqlStament, 5);
            if (sqlite3_column_text(sqlStament, 6)) {
                model.countRoomNumber = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 6)];
            }
            model.countServiceId = sqlite3_column_int(sqlStament, 7);
            model.countOrderWSId = sqlite3_column_int(sqlStament, 8);
            
            [array addObject:model];
            
            status = sqlite3_step(self.sqlStament);
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                [array removeAllObjects];
                goto RE_ACCESS_DB;
            }
        }
    }
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    return array;
}

//-(CountOrdersModelV2 *)loadCountOrdersModelByRoomIdUserIdAndServiceId:(CountOrdersModelV2 *)model
//{
// 
//    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@ ,%@, %@, %@ , %@, %@, %@, %@ FROM %@ WHERE %@ = ? AND %@ = ? AND %@ = ?",
//                           scount_id,
//                           scount_room_id,
//                           scount_user_id,
//                           scount_status,
//                           scount_collect_date,
//                           scount_service_id,
//                           scount_room_number,
//                           scount_post_status,
//                           scount_order_ws_id,
//                           
//                           COUNT_ORDERS,
//                           scount_room_id,
//                           scount_user_id,
//                           scount_service_id
//                           ];
//    
//    const char *sql=[sqlString UTF8String];
//    
//    NSInteger reAccessCount = 0;
//    RE_ACCESS_DB:{
//        if (reAccessCount > 0) {
//            [self close];
//            usleep(0.1);
//            [self openDatabase];
//        }
//        reAccessCount ++;
//    }
//    
//    NSInteger status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
//    if(status == SQLITE_OK) {
//        
//        sqlite3_bind_int (self.sqlStament, 1, model.countRoomAssignId);
//        sqlite3_bind_int (self.sqlStament, 2, model.countUserId);
//        sqlite3_bind_text(self.sqlStament, 3, [model.countCollectDate UTF8String], -1, SQLITE_TRANSIENT);
//        sqlite3_bind_int (self.sqlStament, 4, model.countServiceId);
//        sqlite3_bind_text(self.sqlStament, 5, [model.countRoomNumber UTF8String], -1, SQLITE_TRANSIENT);
//        sqlite3_bind_int (self.sqlStament, 6, model.countPostStatus);
//        status = sqlite3_step(self.sqlStament);
//        while(status == SQLITE_ROW) 
//        {
//            model.countOrderId = sqlite3_column_int(sqlStament, 0);
//            model.countRoomAssignId = sqlite3_column_int(sqlStament, 1);
//            model.countUserId = sqlite3_column_int(sqlStament, 2);
//            model.countStatus = sqlite3_column_int(sqlStament, 3);
//            if (sqlite3_column_text(sqlStament, 4)) {
//                model.countCollectDate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
//            }
//            model.countServiceId = sqlite3_column_int(sqlStament, 5);
//            return model;
//        }
//        if (status == SQLITE_BUSY) {
//            if (reAccessCount <= REACCESS_MAX_TIMES) {
//                goto RE_ACCESS_DB;
//            }
//        }
//    }
//    if (status == SQLITE_BUSY) {
//        if (reAccessCount <= REACCESS_MAX_TIMES) {
//            goto RE_ACCESS_DB;
//        }
//    }
//    return model;
//
//}

-(NSMutableArray *)loadAllCountOrdersByUserId:(NSInteger)userId AndServiceId:(NSInteger)serviceId{
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@ ,%@, %@, %@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ? AND %@ = ?",
                           scount_id,
                           scount_room_id,
                           scount_user_id,
                           scount_status,
                           scount_collect_date,
                           scount_service_id,
                           scount_room_number,
                           scount_post_status,
                           scount_order_ws_id,
                           COUNT_ORDERS,
                           scount_user_id,
                           scount_service_id
                           ];
    
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSMutableArray *array = [NSMutableArray array];
    NSInteger status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        
        sqlite3_bind_int (self.sqlStament, 1, (int)userId);
        sqlite3_bind_int (self.sqlStament, 2, (int)serviceId);
        
        status = sqlite3_step(self.sqlStament);
        while(status == SQLITE_ROW) 
        {
            CountOrdersModelV2 *model = [[CountOrdersModelV2 alloc] init];
            model.countOrderId = sqlite3_column_int(sqlStament, 0);
            model.countRoomAssignId = sqlite3_column_int(sqlStament, 1);
            model.countUserId = sqlite3_column_int(sqlStament, 2);
            model.countStatus = sqlite3_column_int(sqlStament, 3);
            if (sqlite3_column_text(sqlStament, 4)) {
                model.countCollectDate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
            }
            model.countServiceId = sqlite3_column_int(sqlStament, 5);
            if (sqlite3_column_text(sqlStament, 6)) {
                model.countRoomNumber = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 6)];
            }
            model.countPostStatus = sqlite3_column_int(sqlStament, 7);
            model.countOrderWSId = sqlite3_column_int(sqlStament, 8);
            
            [array addObject:model];
            
            status = sqlite3_step(self.sqlStament);
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount == REACCESS_MAX_TIMES) {
                [array removeAllObjects];
                goto RE_ACCESS_DB;
            }
        }
    }
    if (status == SQLITE_BUSY) {
        if (reAccessCount == REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    return array;
}

//------------------------update code------------------------------
-(NSMutableArray *) loadCountOrdersByUserId:(NSInteger) userId AndServiceId:(NSInteger) serviceId{
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT DISTINCT a.%@ ,a.%@, a.%@, a.%@, a.%@, a.%@, a.%@, a.%@, a.%@ FROM %@ a JOIN %@ b ON a.%@ = b.%@ WHERE a.%@ = ? AND a.%@ = ?",
                           scount_id,
                           scount_room_id,
                           scount_user_id,
                           scount_status,
                           scount_collect_date,
                           scount_service_id,
                           scount_room_number,
                           scount_post_status,
                           scount_order_ws_id,
                           
                           COUNT_ORDERS,
                           COUNT_ORDER_DETAILS,
                           
                           scount_id,
                           dcount_order_id,
                           
                           scount_user_id,
                           scount_service_id
                           ];
    
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    reAccessCount ++;
}
    
    NSMutableArray *array = [NSMutableArray array];
    NSInteger status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        
        sqlite3_bind_int (self.sqlStament, 1, (int)userId);
        sqlite3_bind_int (self.sqlStament, 2, (int)serviceId);
        
        status = sqlite3_step(self.sqlStament);
        while(status == SQLITE_ROW)
        {
            CountOrdersModelV2 *model = [[CountOrdersModelV2 alloc] init];
            model.countOrderId = sqlite3_column_int(sqlStament, 0);
            model.countRoomAssignId = sqlite3_column_int(sqlStament, 1);
            model.countUserId = sqlite3_column_int(sqlStament, 2);
            model.countStatus = sqlite3_column_int(sqlStament, 3);
            if (sqlite3_column_text(sqlStament, 4)) {
                model.countCollectDate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
            }
            model.countServiceId = sqlite3_column_int(sqlStament, 5);
            if (sqlite3_column_text(sqlStament, 6)) {
                model.countRoomNumber = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 6)];
            }
            model.countPostStatus = sqlite3_column_int(sqlStament, 7);
            model.countOrderWSId = sqlite3_column_int(sqlStament, 8);
            [array addObject:model];
            
            status = sqlite3_step(self.sqlStament);
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount == REACCESS_MAX_TIMES) {
                [array removeAllObjects];
                goto RE_ACCESS_DB;
            }
        }
    }
    if (status == SQLITE_BUSY) {
        if (reAccessCount == REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    return array;
}

@end
