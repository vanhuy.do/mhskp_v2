//
//  CountOrderDetailAdapterV2.h
//  mHouseKeeping
//
//  Created by TMS on 3/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DatabaseAdapterV2.h"
#import "CountOrderDetailsModelV2.h"

@interface CountOrderDetailAdapterV2 : DatabaseAdapterV2

-(BOOL)insertCountOrderDetailsModel:(CountOrderDetailsModelV2 *) model ;
-(BOOL) updateCountOrderDetailsModel:(CountOrderDetailsModelV2 *) model;
-(BOOL) deleteCountOrderDetailsModel:(CountOrderDetailsModelV2 *) model;
-(BOOL) deleteCountOrderDetailsModelById:(NSInteger) modelId;
-(CountOrderDetailsModelV2 *) loadCountOrderDetailsModel:(CountOrderDetailsModelV2 *) model;
-(NSMutableArray *) loadAllCountOrderDetailByOrderId:(NSInteger) countOrderId;
-(CountOrderDetailsModelV2 *) loadCountOrderDetailsByItemId:(NSInteger) countItemId;
-(CountOrderDetailsModelV2 *) loadCountOrderDetailsByItemId:(NSInteger) countItemId AndOrderId: (NSInteger) orderId;
@end
