//
//  CountOrderDetailAdapterV2.m
//  mHouseKeeping
//
//  Created by TMS on 3/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CountOrderDetailAdapterV2.h"
#import "ehkDefines.h"

@implementation CountOrderDetailAdapterV2

-(BOOL)insertCountOrderDetailsModel:(CountOrderDetailsModelV2 *)model {
    NSString *sqlString = [[NSString alloc] initWithFormat:@"INSERT INTO %@ (%@ ,%@ ,%@, %@, %@, %@, %@) VALUES(?, ?, ?, ?, ?, ?, ?)", 
                           COUNT_ORDER_DETAILS,
                           dcount_order_id,
                           dcount_item_id,
                           dcount_collected,
                           dcount_new,
                           dcount_quantity,
                           dcount_pos_status,
                           dcount_collected_date
                           ];
    
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK){
    }
    
    sqlite3_bind_int(sqlStament, 1, (int)model.countOrderId);
    sqlite3_bind_int(sqlStament, 2, (int)model.countItemId);
    sqlite3_bind_int(sqlStament, 3, (int)model.countCollected);
    sqlite3_bind_int(sqlStament, 4, (int)model.countNew);
    sqlite3_bind_int(sqlStament, 5, (int)model.countQuantity);
    sqlite3_bind_int(sqlStament, 6, (int)model.countPostStatus);
    sqlite3_bind_text(sqlStament, 7, [model.countCollectedDate UTF8String], -1, SQLITE_TRANSIENT);
        
    NSInteger status = sqlite3_step(self.sqlStament);
    if(SQLITE_DONE != status)
    {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return NO;
    }
    else
    {
        sqlite3_last_insert_rowid(database);
        return YES;
    }
    return NO;
}

-(BOOL)updateCountOrderDetailsModel:(CountOrderDetailsModelV2 *)model {
    NSString *sqlString = [[NSString alloc] initWithFormat:@"UPDATE %@ SET  %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ? WHERE %@ = ?", 
                           COUNT_ORDER_DETAILS,
                           dcount_order_id,
                           dcount_item_id,
                           dcount_collected,
                           dcount_new,
                           dcount_quantity,
                           dcount_pos_status,
                           dcount_collected_date,
                           dcount_id
                           ];
    
    
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
        
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK){
    }
    
    sqlite3_bind_int(sqlStament, 1, (int)model.countOrderId);
    sqlite3_bind_int(sqlStament, 2, (int)model.countItemId);
    sqlite3_bind_int(sqlStament, 3, (int)model.countCollected);
    sqlite3_bind_int(sqlStament, 4, (int)model.countNew);
    sqlite3_bind_int(sqlStament, 5, (int)model.countQuantity);
    sqlite3_bind_int(sqlStament, 6, (int)model.countPostStatus);
    sqlite3_bind_text(sqlStament, 7, [model.countCollectedDate UTF8String], -1, SQLITE_TRANSIENT);
    
    sqlite3_bind_int(sqlStament, 8, (int)model.countOrderDetailId);

    NSInteger status = sqlite3_step(self.sqlStament);
    if(SQLITE_DONE != status)
    {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return NO;
    }
    else
    {
        sqlite3_last_insert_rowid(database);
        return YES;
    }
    return NO;
}

-(BOOL)deleteCountOrderDetailsModel:(CountOrderDetailsModelV2 *)model {
    NSString *sqlString = [[NSString alloc] initWithFormat:@"DELETE FROM %@ WHERE %@ = ?", 
                           COUNT_ORDER_DETAILS,
                           dcount_id
                           ];
    
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK){
    }
    
    sqlite3_bind_int(sqlStament, 1, (int)model.countOrderDetailId);
    
    NSInteger status = sqlite3_step(self.sqlStament);
    if(SQLITE_DONE != status)
    {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return NO;
    }
    else
    {
        sqlite3_last_insert_rowid(database);
        return YES;
    }
    return NO;
}

-(BOOL) deleteCountOrderDetailsModelById:(NSInteger)modelId {
    NSString *sqlString = [[NSString alloc] initWithFormat:@"DELETE FROM %@ WHERE %@ = ?",
                           COUNT_ORDER_DETAILS,
                           dcount_id
                           ];
    
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
    reAccessCount ++;
    }
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK){
    }
    
    sqlite3_bind_int(sqlStament, 1, (int)modelId);
    
    NSInteger status = sqlite3_step(self.sqlStament);
    if(SQLITE_DONE != status)
    {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return NO;
    }
    else
    {
        sqlite3_last_insert_rowid(database);
        return YES;
    }
    return NO;
}

-(CountOrderDetailsModelV2 *)loadCountOrderDetailsModel:(CountOrderDetailsModelV2 *)model {
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@ ,%@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ?", 
                           dcount_order_id,
                           dcount_item_id,
                           dcount_collected,
                           dcount_new,
                           dcount_quantity,
                           dcount_pos_status,
                           dcount_collected_date,
                           COUNT_ORDER_DETAILS,
                           dcount_id
                           ];
    
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSInteger status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
        
    if(status == SQLITE_OK) {
        
        status = sqlite3_step(self.sqlStament);
        sqlite3_bind_int (self.sqlStament, 1, (int)model.countOrderDetailId);

        while(status == SQLITE_ROW) 
        {
            model.countOrderId = sqlite3_column_int(sqlStament, 0);
            model.countItemId = sqlite3_column_int(sqlStament, 1);
            model.countCollected = sqlite3_column_int(sqlStament, 2);
            model.countNew = sqlite3_column_int(sqlStament, 3);
            model.countQuantity = sqlite3_column_int(sqlStament, 4);
            model.countPostStatus = sqlite3_column_int(sqlStament, 5);
            if (sqlite3_column_text(sqlStament, 6)) {
                model.countCollectedDate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 6)];
            }
            return model;
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    return model;
}

-(NSMutableArray *)loadAllCountOrderDetailByOrderId:(NSInteger)countOrderId {
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@ ,%@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ?", 
                           dcount_id,
                           dcount_order_id,
                           dcount_item_id,
                           dcount_collected,
                           dcount_new,
                           dcount_quantity,
                           dcount_pos_status,
                           dcount_collected_date,
                           COUNT_ORDER_DETAILS,
                           dcount_order_id
                           ];
    
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSMutableArray *array = [NSMutableArray array];
    
    NSInteger status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        
        sqlite3_bind_int (self.sqlStament, 1, (int)countOrderId);
        
        status = sqlite3_step(self.sqlStament);
        while(status == SQLITE_ROW) 
        {
            CountOrderDetailsModelV2 *model = [[CountOrderDetailsModelV2 alloc] init];
            model.countOrderDetailId = sqlite3_column_int(sqlStament, 0);
            model.countOrderId = sqlite3_column_int(sqlStament, 1);
            model.countItemId = sqlite3_column_int(sqlStament, 2);
            model.countCollected = sqlite3_column_int(sqlStament, 3);
            model.countNew = sqlite3_column_int(sqlStament, 4);
            model.countQuantity = sqlite3_column_int(sqlStament, 5);
            model.countPostStatus = sqlite3_column_int(sqlStament, 6);
            if (sqlite3_column_text(sqlStament, 7)) {
                model.countCollectedDate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 7)];
            }
            [array addObject:model];
            
            status = sqlite3_step(self.sqlStament);
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                [array  removeAllObjects];
                goto  RE_ACCESS_DB;
            }
        }
    }
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            [array  removeAllObjects];
            goto  RE_ACCESS_DB;
        }
    }
    return array;
}

-(CountOrderDetailsModelV2 *)loadCountOrderDetailsByItemId:(NSInteger) countItemId
{
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@ ,%@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ?", 
                           dcount_order_id,
                           dcount_item_id,
                           dcount_collected,
                           dcount_new,
                           dcount_quantity,
                           dcount_pos_status,
                           dcount_collected_date,
                           COUNT_ORDER_DETAILS,
                           dcount_item_id
                           ];
    
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
        
    CountOrderDetailsModelV2 *model = [[CountOrderDetailsModelV2 alloc] init];
    NSInteger status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        
        sqlite3_bind_int (self.sqlStament, 1, (int)countItemId);
        
        status = sqlite3_step(self.sqlStament);
        while(status == SQLITE_ROW) 
        {
            model.countOrderId = sqlite3_column_int(sqlStament, 0);
            model.countItemId = sqlite3_column_int(sqlStament, 1);
            model.countCollected = sqlite3_column_int(sqlStament, 2);
            model.countNew = sqlite3_column_int(sqlStament, 3);
            model.countQuantity = sqlite3_column_int(sqlStament, 4);
            model.countPostStatus = sqlite3_column_int(sqlStament, 5);
            if (sqlite3_column_text(sqlStament, 6)) {
                model.countCollectedDate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 6)];
            }
            return model;
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    return model;

}

-(CountOrderDetailsModelV2 *)loadCountOrderDetailsByItemId:(NSInteger)countItemId AndOrderId:(NSInteger)orderId
{
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@ ,%@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ? AND %@ = ?", 
                           dcount_id,
                           dcount_order_id,
                           dcount_item_id,
                           dcount_collected,
                           dcount_new,
                           dcount_quantity,
                           dcount_pos_status,
                           dcount_collected_date,
                           COUNT_ORDER_DETAILS,
                           dcount_item_id,
                           dcount_order_id
                           ];
    
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
        
    CountOrderDetailsModelV2 *model = [[CountOrderDetailsModelV2 alloc] init];
    
    NSInteger status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        
        sqlite3_bind_int (self.sqlStament, 1, (int)countItemId);
        sqlite3_bind_int (self.sqlStament, 2, (int)orderId);
        
        status = sqlite3_step(self.sqlStament);
        while(status == SQLITE_ROW) 
        {
            model.countOrderDetailId = sqlite3_column_int(sqlStament, 0);
            model.countOrderId = sqlite3_column_int(sqlStament, 1);
            model.countItemId = sqlite3_column_int(sqlStament, 2);
            model.countCollected = sqlite3_column_int(sqlStament, 3);
            model.countNew = sqlite3_column_int(sqlStament, 4);
            model.countQuantity = sqlite3_column_int(sqlStament, 5);
            model.countPostStatus = sqlite3_column_int(sqlStament, 6);
            if (sqlite3_column_text(sqlStament, 7)) {
                model.countCollectedDate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 7)];
            }
            return model;
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    return model;
    
}


@end
