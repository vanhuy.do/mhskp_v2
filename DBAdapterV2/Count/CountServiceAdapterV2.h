//
//  CountServiceAdapterV2.h
//  mHouseKeeping
//
//  Created by TMS on 3/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DatabaseAdapterV2.h"
#import "CountServiceModelV2.h"

@interface CountServiceAdapterV2 : DatabaseAdapterV2 

-(BOOL)insertCountServiceModel:(CountServiceModelV2 *) model ;
-(BOOL) updateCountSericeModel:(CountServiceModelV2 *) model;
-(BOOL) deleteCountServiceModel:(CountServiceModelV2 *) model;
-(CountServiceModelV2 *) loadCountSericeModel:(CountServiceModelV2 *) model;
-(NSMutableArray *) loadAllCountSericeModel;

@end
