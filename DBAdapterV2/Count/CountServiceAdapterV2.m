//
//  CountServiceAdapterV2.m
//  mHouseKeeping
//
//  Created by TMS on 3/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CountServiceAdapterV2.h"
#import "ehkDefines.h"

@implementation CountServiceAdapterV2

-(BOOL)insertCountServiceModel:(CountServiceModelV2 *)model {
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"INSERT INTO %@ (%@, %@ ,%@, %@, %@) VALUES(?, ?, ?, ?, ?)", 
                                  COUNT_SERVICE,
                                  cs_id,
                                  cs_kind,
                                  cs_image,
                                  cs_name,
                                  cs_name_lang
                                  ];
    
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSInteger status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status != SQLITE_OK){
    }
    
    sqlite3_bind_int (self.sqlStament, 1, (int)model.countServiceId);
    sqlite3_bind_text(self.sqlStament, 2, [model.countServiceKind UTF8String], -1, SQLITE_TRANSIENT);
    
    if(model.countServiceImage != nil)
        sqlite3_bind_blob(self.sqlStament, 3, (int)[model.countServiceImage bytes], (int)[model.countServiceImage length], NULL);
    else
        sqlite3_bind_blob(self.sqlStament, 3, nil, -1, NULL);
    
    sqlite3_bind_text(sqlStament, 4, [model.countServiceName UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(sqlStament, 5, [model.countServiceNameLang UTF8String], -1, SQLITE_TRANSIENT);
    
    status = sqlite3_step(sqlStament);
    if(SQLITE_DONE != status)
    {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return NO;
    }
    else
    {
        sqlite3_last_insert_rowid(database);
        return YES;
    }
    return NO;
}

-(BOOL)updateCountSericeModel:(CountServiceModelV2 *)model {
    NSString *sqlString = [[NSString alloc] initWithFormat:@"UPDATE %@ SET %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ? WHERE %@ = ?", 
                                  COUNT_SERVICE,
                                  cs_id,
                                  cs_kind,
                                  cs_image,
                                  cs_name,
                                  cs_name_lang,
                                   cs_id
                                  ];
    
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK){
    }
    
    sqlite3_bind_text(self.sqlStament, 1, [model.countServiceKind UTF8String], -1, SQLITE_TRANSIENT);
    
    if(model.countServiceImage != nil)
        sqlite3_bind_blob(self.sqlStament, 2, [model.countServiceImage bytes], (int)[model.countServiceImage length], NULL);
    else
        sqlite3_bind_blob(self.sqlStament, 2, nil, -1, NULL);
    
    sqlite3_bind_text(sqlStament, 3, [model.countServiceName UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(sqlStament, 4, [model.countServiceNameLang UTF8String], -1, SQLITE_TRANSIENT);
    
    sqlite3_bind_int(self.sqlStament, 5, (int)model.countServiceId);
    
    NSInteger status = sqlite3_step(self.sqlStament);
    if(SQLITE_DONE != status)
    {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return NO;
    }
    else
    {
        sqlite3_last_insert_rowid(database);
        return YES;
    }
    return NO;
}

-(BOOL)deleteCountServiceModel:(CountServiceModelV2 *)model {
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"DELETE FROM %@ WHERE %@ = ?", 
                                  COUNT_SERVICE,
                                  cs_id
                                  ];
    
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK){
    }
    
    sqlite3_bind_int (self.sqlStament, 1, (int)model.countServiceId);
    
    NSInteger status = sqlite3_step(self.sqlStament);
    if(SQLITE_DONE != status)
    {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return NO;
    }
    else
    {
        sqlite3_last_insert_rowid(database);
        return YES;
    }
    return NO;
}

-(CountServiceModelV2 *)loadCountSericeModel:(CountServiceModelV2 *)model {
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"SELECT %@ ,%@, %@, %@ FROM %@ WHERE %@ = ?", 
                                  cs_kind,
                                  cs_image,
                                  cs_name,
                                  cs_name_lang,
                                  COUNT_SERVICE,
                                  cs_id
                                  ];
    
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSInteger status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        sqlite3_bind_int(sqlStament, 1, (int)model.countServiceId);
        
        status = sqlite3_step(sqlStament);
        while(status == SQLITE_ROW) 
        {
            if (sqlite3_column_text(sqlStament, 0)) {
                model.countServiceKind = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 0)];
            }
            if (sqlite3_column_bytes(sqlStament, 1) > 0) {
                model.countServiceImage = [NSData dataWithBytes:sqlite3_column_blob(sqlStament, 1) length:sqlite3_column_bytes(sqlStament, 1)];
            }
            if (sqlite3_column_text(sqlStament, 2)) {
                model.countServiceName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
            }
            if (sqlite3_column_text(sqlStament, 3)) {
                model.countServiceNameLang = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
            }
            return model;
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    } else {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    return model;
}

-(NSMutableArray *) loadAllCountSericeModel
{
    NSMutableArray *array = [[NSMutableArray alloc] init];
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"SELECT %@, %@ ,%@, %@, %@ FROM %@", 
                                  cs_kind,
                                  cs_image,
                                  cs_name,
                                  cs_name_lang,
                                  cs_id,
                                  COUNT_SERVICE
                                  ];
    
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSInteger status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {

        status = sqlite3_step(self.sqlStament);
        while(status == SQLITE_ROW) 
        {
            CountServiceModelV2 *model = [[CountServiceModelV2 alloc] init];
            
            if (sqlite3_column_text(sqlStament, 0)) {
                model.countServiceKind = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 0)];
            }
            if (sqlite3_column_bytes(sqlStament, 1) > 0) {
                model.countServiceImage = [NSData dataWithBytes:sqlite3_column_blob(sqlStament, 1) length:sqlite3_column_bytes(sqlStament, 1)];
            }
            if (sqlite3_column_text(sqlStament, 2)) {
                model.countServiceName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
            }
            if (sqlite3_column_text(sqlStament, 3)) {
                model.countServiceNameLang = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
            }
            model.countServiceId = sqlite3_column_int(sqlStament, 4);
            
            [array addObject:model];
            
            status = sqlite3_step(self.sqlStament);

        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                [array removeAllObjects];
                
                goto RE_ACCESS_DB;
            }
        }
    }
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }

    return array;
}

@end
