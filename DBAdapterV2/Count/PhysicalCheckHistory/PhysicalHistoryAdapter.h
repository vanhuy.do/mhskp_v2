//
//  PhysicalHistoryAdapter.h
//  mHouseKeeping
//
//  Created by Mac User on 26/03/2013.
//
//

#import <Foundation/Foundation.h>
#import "DatabaseAdapterV2.h"
#import "PhysicalHistoryModel.h"
#import "FloorModelV2.h"

@interface PhysicalHistoryAdapter : DatabaseAdapterV2

-(int) insertPhysicalHistoryData:(PhysicalHistoryModel *) physicalHistoryModel;
-(NSMutableArray *) loadAllPhysicalHistoryData;
-(NSMutableArray *) loadAllFloorContainsPhysicalHistoryByUserId:(NSInteger) userId;
-(NSMutableArray *)loadAllPhysicalHistoryByFloorId:(NSInteger)floorID AndUserId:(NSInteger)userId;
-(void) loadAllPhysicalHistory;
-(int) deleteDatabaseAfterSomeDays;
@end
