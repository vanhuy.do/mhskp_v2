//
//  PhysicalHistoryAdapter.m
//  mHouseKeeping
//
//  Created by Mac User on 26/03/2013.
//
//

#import "PhysicalHistoryAdapter.h"
#import "CountHistoryModel.h"
#import "DbDefinesV2.h"
#import "ehkDefines.h"
#import "NSDate-Utilities.h"

@implementation PhysicalHistoryAdapter

-(int) insertPhysicalHistoryData:(PhysicalHistoryModel *) physicalHistoryModel{
    NSString *sqlString = [[NSString alloc] initWithFormat:@"INSERT INTO %@ (%@, %@, %@, %@, %@, %@) %@", PHYSICAL_HISTORY, PH_floor_id,PH_room_id, PH_status, PH_dnd, PH_date,PH_user_id, @"VALUES(?, ?, ?, ?, ?, ?)"];
    
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {}
    
    sqlite3_bind_int(self.sqlStament, 1,(int)physicalHistoryModel.ph_floor_id );
    sqlite3_bind_text(self.sqlStament, 2 , [physicalHistoryModel.ph_room_id UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_int(self.sqlStament, 3,(int)physicalHistoryModel.ph_status );
    sqlite3_bind_text(self.sqlStament, 5, [physicalHistoryModel.ph_date UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_int(self.sqlStament, 4,(physicalHistoryModel.ph_dnd?1:0));
    sqlite3_bind_int(self.sqlStament, 6, (int)physicalHistoryModel.ph_user_id);

    NSInteger returnInt = 0;
    
    NSInteger status = sqlite3_step(self.sqlStament);
    
    if(SQLITE_DONE != status) {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return (int)returnInt;
    } else {
        sqlite3_reset(self.sqlStament);
        returnInt = 1;
        return (int)returnInt;
    }

}

-(NSMutableArray *) loadAllPhysicalHistoryData{
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@ FROM %@",
                           PH_floor_id,
                           PH_room_id,
                           PH_status,
                           PH_dnd,
                           PH_date,
                           PH_user_id,
                           PHYSICAL_HISTORY
                           ];
    
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    reAccessCount ++;
}
    NSMutableArray *array = [NSMutableArray array];
    NSInteger status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    
    if(status == SQLITE_OK) {
        
        status = sqlite3_step(self.sqlStament);
        while(status == SQLITE_ROW)
        {
            PhysicalHistoryModel *model = [[PhysicalHistoryModel alloc]init];
            model.ph_floor_id = sqlite3_column_int(sqlStament, 0);
            if (sqlite3_column_text(self.sqlStament, 1)) {
                model.ph_room_id = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
            }
            model.ph_status = sqlite3_column_int(sqlStament, 2);
            model.ph_dnd = sqlite3_column_int(sqlStament, 3) == 1;
            if (sqlite3_column_text(sqlStament, 4)) {
                model.ph_date = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
            }
            model.ph_user_id = sqlite3_column_int(sqlStament, 5);
            [array addObject:model];
            
            status = sqlite3_step(self.sqlStament);
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    return array;
}

-(NSMutableArray *) loadAllFloorContainsPhysicalHistoryByUserId:(NSInteger) userId{
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT DISTINCT b.%@, b.%@ FROM %@ a JOIN %@ b ON a.%@ = b.%@ WHERE %@ = ? ORDER BY b.%@ ASC",
                           floor_id,
                           floor_name,
                           PHYSICAL_HISTORY,
                           FLOOR,
                           
                           PH_floor_id,
                           floor_id,
                           PH_user_id,
                           floor_id
                           ];
    
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    reAccessCount ++;
}
    
    NSMutableArray *array = [NSMutableArray array];
    NSInteger status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        sqlite3_bind_int(self.sqlStament, 1, (int)userId);
        
        status = sqlite3_step(self.sqlStament);
        while(status == SQLITE_ROW)
        {

            FloorModelV2 *model = [[FloorModelV2 alloc]init];
            model.floor_id = sqlite3_column_int(sqlStament, 0);
     
            if (sqlite3_column_text(sqlStament, 1)) {
                model.floor_name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
            }
            [array addObject:model];
            
            status = sqlite3_step(self.sqlStament);
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount == REACCESS_MAX_TIMES) {
                [array removeAllObjects];
                goto RE_ACCESS_DB;
            }
        }
    }
    if (status == SQLITE_BUSY) {
        if (reAccessCount == REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    return array;

}

-(NSMutableArray *)loadAllPhysicalHistoryByFloorId:(NSInteger)floorID AndUserId:(NSInteger)userId{
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ? AND %@ = ? ORDER BY %@ ASC",
                           PH_floor_id,
                           PH_room_id,
                           PH_status,
                           PH_dnd,
                           PH_date,
                           PHYSICAL_HISTORY,
                           PH_floor_id,
                           PH_user_id,
                           PH_room_id
                           ];
    
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    reAccessCount ++;
}
    
    NSMutableArray *array = [NSMutableArray array];
    NSInteger status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        
        sqlite3_bind_int (self.sqlStament, 1, (int)floorID);
        sqlite3_bind_int(self.sqlStament, 2, (int)userId);
        
        status = sqlite3_step(self.sqlStament);
        while(status == SQLITE_ROW)
        {
            PhysicalHistoryModel *model = [[PhysicalHistoryModel alloc] init];
            
            model.ph_floor_id = sqlite3_column_int(sqlStament, 0);
            
            if (sqlite3_column_text(self.sqlStament, 1)) {
                model.ph_room_id = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
            }
            model.ph_status = sqlite3_column_int(sqlStament, 2);
            model.ph_dnd = sqlite3_column_int(sqlStament,3) == 1;
            
            if (sqlite3_column_text(sqlStament, 4)) {
                model.ph_date = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
            }
            [array addObject:model];
            
            status = sqlite3_step(self.sqlStament);
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                [array removeAllObjects];
                goto RE_ACCESS_DB;
            }
        }
    }
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    return array;

}

-(void) loadAllPhysicalHistory{
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@ FROM %@",
                           PH_floor_id,
                           PH_room_id,
                           PH_status,
                           PH_dnd,
                           PH_date,
                           PHYSICAL_HISTORY
                           ];
    
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    reAccessCount ++;
}
    
    NSInteger status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    
    if(status == SQLITE_OK) {
        
        status = sqlite3_step(self.sqlStament);
        while(status == SQLITE_ROW)
        {
            PhysicalHistoryModel *model = [[PhysicalHistoryModel alloc]init];
            model.ph_floor_id = sqlite3_column_int(sqlStament, 0);
            if (sqlite3_column_text(self.sqlStament, 1)) {
                model.ph_room_id = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
            }
            model.ph_status = sqlite3_column_int(sqlStament, 2);
            model.ph_dnd = sqlite3_column_int(sqlStament, 3) == 1;
            if (sqlite3_column_text(sqlStament, 4)) {
                model.ph_date = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
            }
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }

}

//delete data after 24h

-(int) deleteDatabaseAfterSomeDays{
    NSInteger result = 1;
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd 00:00:00"];
    int countDate = (int)[[UserManagerV2 sharedUserManager].currentUserAccessRight countPostingHistoryDays];
    NSDate *dateToDelete = [[NSDate date] dateBySubtractingDays:countDate - 1];
    NSString *beforeDays = [dateFormat stringFromDate:dateToDelete];
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"DELETE FROM %@ WHERE %@ < '%@'", PHYSICAL_HISTORY, PH_date, beforeDays];
    const char *sql = [sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
        result = 0;
	
    NSInteger status = sqlite3_step(sqlStament);
    
	if (SQLITE_DONE != status) {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
		result = 0;
    }
    
    return (int)result;
}
@end
