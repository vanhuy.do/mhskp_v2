//
//  RoomTypeInventoryAdapter.h
//  mHouseKeeping
//
//  Created by DungPhan on 02/10/2015.
//

#import "DatabaseAdapterV2.h"
#import "RoomTypeInventoryModel.h"

@interface RoomTypeInventoryAdapter : DatabaseAdapterV2

- (BOOL)insertRoomTypeInventoryModel:(RoomTypeInventoryModel *)rtiModel;
- (BOOL)deleteAllRoomTypeInventoryModel;

@end
