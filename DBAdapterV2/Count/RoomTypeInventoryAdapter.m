//
//  RoomTypeInventoryAdapter.m
//  mHouseKeeping
//
//  Created by DungPhan on 02/10/2015.
//

#import "RoomTypeInventoryAdapter.h"
#import "ehkDefines.h"

@implementation RoomTypeInventoryAdapter

-(BOOL)insertRoomTypeInventoryModel:(RoomTypeInventoryModel *)rtiModel {
    NSString *sqlString = [[NSString alloc] initWithFormat:@"INSERT INTO %@ (%@ ,%@ ,%@, %@, %@, %@, %@, %@, %@) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)",
                           ROOMTYPE_INVENTORY,
                           rti_roomtype_id,
                           rti_category_id,
                           rti_item_id,
                           rti_user_id,
                           rti_hotel_id,
                           rti_section_id,
                           rti_building_id,
                           rti_service_item,
                           rti_default_quantity
                           ];
    
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK){
    }
    
    sqlite3_bind_int(sqlStament, 1, (int)rtiModel.roomTypeId);
    sqlite3_bind_int(sqlStament, 2, (int)rtiModel.categoryId);
    sqlite3_bind_int(sqlStament, 3, (int)rtiModel.itemId);
    sqlite3_bind_int(sqlStament, 4, (int)rtiModel.userId);
    sqlite3_bind_int(sqlStament, 5, (int)rtiModel.hotelId);
    sqlite3_bind_int(sqlStament, 6, (int)rtiModel.sectionId);
    sqlite3_bind_int(sqlStament, 7, (int)rtiModel.buildingId);
    sqlite3_bind_int(sqlStament, 8, (int)rtiModel.serviceItem);
    sqlite3_bind_int(sqlStament, 9, (int)rtiModel.defaultQuantity);
    
    NSInteger status = sqlite3_step(self.sqlStament);
    if(SQLITE_DONE != status)
    {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return NO;
    }
    else
    {
        sqlite3_last_insert_rowid(database);
        return YES;
    }
    return NO;
}

-(BOOL)deleteAllRoomTypeInventoryModel {

    DataTable *tableDelete = [[DataTable alloc] initWithNameFormat:@"%@", ROOMTYPE_INVENTORY];
    return [tableDelete dropDBRows];
}

@end
