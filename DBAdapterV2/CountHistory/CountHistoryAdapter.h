//
//  CountHistoryAdapter.h
//  mHouseKeeping
//
//  Created by Mac User on 28/03/2013.
//
//

#import <Foundation/Foundation.h>
#import "CountHistoryModel.h"
#import "DatabaseAdapterV2.h"

@interface CountHistoryAdapter : DatabaseAdapterV2

-(int) insertCountHistoryData:(CountHistoryModel *) countHistoryModel;
//-(NSMutableArray *) loadAllItemsContainsCountHistoryByUserId:(NSInteger)userId;
-(NSMutableArray *) loadAllCountHistoryByServiceId:(NSInteger) serviceId AndUserId:(NSInteger)userId AndRoomId:(NSString*)roomNumber;
-(NSMutableArray *) loadRoomIdByUserId:(NSInteger)userId AndServiceId:(NSInteger)serviceId;
-(int) deleteDatabaseAfterSomeDays;
@end
