//
//  CountHistoryAdapter.m
//  mHouseKeeping
//
//  Created by Mac User on 28/03/2013.
//
//

#import "CountHistoryAdapter.h"
#import "NSDate-Utilities.h"

@implementation CountHistoryAdapter

-(int) insertCountHistoryData:(CountHistoryModel *) countHistoryModel{
    NSString *sqlString = [[NSString alloc] initWithFormat:@"INSERT INTO %@ (%@, %@, %@, %@, %@, %@, %@, %@) %@", COUNT_HISTORY, H_count_id,H_room_id, H_count_item_id, H_count_collected, H_count_new, H_count_service,H_count_date,H_count_user_id, @"VALUES(?, ?, ?, ?, ?, ?, ?, ?)"];
    
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {}
    
//    sqlite3_bind_int(self.sqlStament, 1,countHistoryModel.count_id);
    sqlite3_bind_text(self.sqlStament, 2 , [countHistoryModel.room_id UTF8String], -1,SQLITE_TRANSIENT);
    sqlite3_bind_int(self.sqlStament, 3, (int)countHistoryModel.count_item_id );
    sqlite3_bind_int(self.sqlStament, 4, (int)countHistoryModel.count_collected);
    sqlite3_bind_int(self.sqlStament, 5, (int)countHistoryModel.count_new);
    sqlite3_bind_int(self.sqlStament, 6, (int)countHistoryModel.count_service);
    sqlite3_bind_text(self.sqlStament, 7, [countHistoryModel.count_date UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_int(self.sqlStament, 8, (int)countHistoryModel.count_user_id);
    NSInteger returnInt = 0;
    
    NSInteger status = sqlite3_step(self.sqlStament);
    
    if(SQLITE_DONE != status) {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return (int)returnInt;
    } else {
        sqlite3_reset(self.sqlStament);
        returnInt = 1;
        return (int)returnInt;
    }

}

//-(NSMutableArray *) loadAllItemsContainsCountHistoryByUserId:(NSInteger)userId{
//    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT DISTINCT a.%@, a.%@ FROM %@ a JOIN %@ b ON a.%@ = b.%@ WHERE a.%@ = ?",
//                           count_id,
//                           count_name,
//                           COUNT_ITEMS,
//                           COUNT_HISTORY,
//                           count_id,
//                           H_count_item_id,
//                           H_count_user_id
//                           ];
//    
//    const char *sql=[sqlString UTF8String];
//    
//    NSInteger reAccessCount = 0;
//RE_ACCESS_DB:{
//    if (reAccessCount > 0) {
//        [self close];
//        usleep(0.1);
//        [self openDatabase];
//    }
//    reAccessCount ++;
//}
//    
//    NSMutableArray *array = [NSMutableArray array];
//    NSInteger status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
//    if(status == SQLITE_OK) {
//        
//        sqlite3_bind_int (self.sqlStament, 1, userId);
//        status = sqlite3_step(self.sqlStament);
//        while(status == SQLITE_ROW)
//        {
//            CountHistoryModel *model = [[CountHistoryModel alloc] init];
////            CountItemsModelV2 *model = [[CountItemsModelV2 alloc] init];
//            model.count_id = sqlite3_column_int(sqlStament, 0);
//            
//            if (sqlite3_column_text(sqlStament, 1)) {
//                model.count_item_name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
//            }
//
//            [array addObject:model];
//            
//            status = sqlite3_step(self.sqlStament);
//        }
//        
//        if (status == SQLITE_BUSY) {
//            if (reAccessCount <= REACCESS_MAX_TIMES) {
//                [array removeAllObjects];
//                goto RE_ACCESS_DB;
//            }
//        }
//    }
//    if (status == SQLITE_BUSY) {
//        if (reAccessCount <= REACCESS_MAX_TIMES) {
//            [array removeAllObjects];
//            goto RE_ACCESS_DB;
//        }
//    }
//    
//    CountCategoryModelV2 *countCategoryFirst = nil;
//    if (array.count != 0) {
//        countCategoryFirst = [array objectAtIndex:0];
//    }
//    return array;
//
//}

-(NSMutableArray *) loadAllCountHistoryByServiceId:(NSInteger) serviceId AndUserId:(NSInteger)userId AndRoomId:(NSString*)roomNumber{
    
    BOOL isEnglish = [[LanguageManagerV2 sharedLanguageManager] currentLanguageIsEqualTo:@"%@", ENGLISH_LANGUAGE];
    NSString *countName = nil;
    if(isEnglish) {
        countName = [NSString stringWithFormat:@"%@", count_name];
    } else {
        countName = [NSString stringWithFormat:@"%@", count_lang];
    }
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT a.%@, a.%@, a.%@, a.%@, a.%@, a.%@, a.%@, a.%@, b.%@ FROM %@ a JOIN %@ b ON a.%@ = b.%@  WHERE %@ = ? AND %@ = ? AND %@ = ? ORDER BY b.%@ ASC",
                           H_count_id,
                           H_room_id,
                           H_count_item_id,
                           H_count_collected,
                           H_count_new,
                           H_count_service,
                           H_count_date,
                           H_count_user_id,
                           countName,
                           COUNT_HISTORY,
                           COUNT_ITEMS,
                           H_count_item_id,
                           count_id,
                           H_count_service,
                           H_count_user_id,
                           H_room_id,
                           count_name
                           ];
    
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    reAccessCount ++;
}
    
    NSMutableArray *array = [NSMutableArray array];
    NSInteger status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        
        sqlite3_bind_int (self.sqlStament, 1, (int)serviceId);
        sqlite3_bind_int(self.sqlStament, 2, (int)userId);
        sqlite3_bind_text(self.sqlStament, 3 , [roomNumber UTF8String], -1,SQLITE_TRANSIENT);
        
        status = sqlite3_step(self.sqlStament);
        while(status == SQLITE_ROW)
        {
            CountHistoryModel *model = [[CountHistoryModel alloc] init];
            model.count_id = sqlite3_column_int(sqlStament, 0);
            if (sqlite3_column_text(self.sqlStament, 1)) {
                model.room_id = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
            }
            model.count_item_id = sqlite3_column_int(sqlStament, 2);
            model.count_collected = sqlite3_column_int(sqlStament, 3);
            model.count_new = sqlite3_column_int(sqlStament, 4);
            model.count_service = sqlite3_column_int(sqlStament, 5);
            
            if (sqlite3_column_text(sqlStament, 6)) {
                model.count_date = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 6)];
            }
            model.count_user_id = sqlite3_column_int(sqlStament, 7);
            if (sqlite3_column_text(sqlStament, 8)){
                model.count_item_name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 8)];
            }
            
            [array addObject:model];
            
            status = sqlite3_step(self.sqlStament);
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                [array removeAllObjects];
                goto RE_ACCESS_DB;
            }
        }
    }
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    return array;

}

-(NSMutableArray *) loadRoomIdByUserId:(NSInteger)userId AndServiceId:(NSInteger)serviceId{
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT DISTINCT %@ FROM %@ WHERE %@ = ? AND %@ = ? ORDER BY %@ ASC",
                           H_room_id,
                           COUNT_HISTORY,
                           H_count_user_id,
                           H_count_service,
                           H_room_id
                           ];
    
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    reAccessCount ++;
}
    
    NSMutableArray *array = [NSMutableArray array];
    NSInteger status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        
        sqlite3_bind_int(self.sqlStament, 1, (int)userId);
         sqlite3_bind_int (self.sqlStament, 2, (int)serviceId);
        status = sqlite3_step(self.sqlStament);
        while(status == SQLITE_ROW)
        {
            CountHistoryModel *model = [[CountHistoryModel alloc] init];
            if (sqlite3_column_text(self.sqlStament, 0)) {
                model.room_id = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 0)];
            }
            
            [array addObject:model];
            
            status = sqlite3_step(self.sqlStament);
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                [array removeAllObjects];
                goto RE_ACCESS_DB;
            }
        }
    }
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
//    CountCategoryModelV2 *countCategoryFirst = nil;
//    if (array.count != 0) {
//        countCategoryFirst = [array objectAtIndex:0];
//    }
    return array;

}

-(int) deleteDatabaseAfterSomeDays{
    NSInteger result = 1;
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd 00:00:00"];
    int countDate = (int)[[UserManagerV2 sharedUserManager].currentUserAccessRight countPostingHistoryDays];
    NSDate *dateToDelete = [[NSDate date] dateBySubtractingDays:countDate - 1];
    NSString *beforeDays = [dateFormat stringFromDate:dateToDelete];
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"DELETE FROM %@ WHERE %@ < '%@'", COUNT_HISTORY, H_count_date, beforeDays];
    const char *sql = [sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
        result = 0;
	
    NSInteger status = sqlite3_step(sqlStament);
    
	if (SQLITE_DONE != status) {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
		result = 0;
    }
    
    return (int)result;
}

@end
