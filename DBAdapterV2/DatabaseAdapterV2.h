//
//  DatabaseAdapterV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>


@interface DatabaseAdapterV2 : NSObject{
    sqlite3_stmt *sqlStament;
    sqlite3 *database;
//    NSInteger returnCode;
}

@property (nonatomic) sqlite3 *database;
@property (nonatomic) sqlite3_stmt *sqlStament;
//@property (nonatomic) NSInteger returnCode;
-(DatabaseAdapterV2*)openDatabase;
-(void)close;
-(void)resetSqlCommand;

//
- (void)setCacheSize:(NSUInteger)pages;
- (void)executeUpdateSQL:(NSString *) updateSQL;

 
@end
