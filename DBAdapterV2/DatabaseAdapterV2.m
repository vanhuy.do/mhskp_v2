//
//  DatabaseAdapterV2.m
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DatabaseAdapterV2.h"
#import "DbDefinesV2.h"
#import "CheckMemory.h"
#import "NSFileManager+DoNotBackup.h"

@implementation DatabaseAdapterV2
@synthesize database;
@synthesize sqlStament;
//@synthesize returnCode;

- (void)setCacheSize:(NSUInteger)pages
{
	NSString *updateSQL = [NSString stringWithFormat:@"PRAGMA cache_size=%d", (int)pages];
	[self executeUpdateSQL:updateSQL];
}
/*
 
 */
- (void)executeUpdateSQL:(NSString *) updateSQL
{
	char *errorMsg;
	if (sqlite3_exec([self database],[updateSQL UTF8String] , NULL, NULL, &errorMsg) != SQLITE_OK) {
		NSString *errorMessage = [NSString stringWithFormat:@"Failed to execute SQL '%@' with message '%s'.", updateSQL, errorMsg];
        NSLog(@"%@", errorMessage);
		//NSAssert(0, errorMessage);
		sqlite3_free(errorMsg);
	}
}
/*
 function for open database local.
 */
-(DatabaseAdapterV2*)openDatabase
{
    //NSLog(@"Memory before open = %@", [[CheckMemory sharedCheckMemory] getMemoryStatus]);
    const char *file = nil;
    if(isDemoMode)
    {
        file =[[NSString stringWithFormat:@"%@", [[NSBundle mainBundle] pathForResource:@"mhousekeeping_demo" ofType:@"db"]] UTF8String];
    }
    else{
        file =[[NSString stringWithFormat:@"%@", [[NSBundle mainBundle] pathForResource:@"mhousekeeping" ofType:@"db"]] UTF8String];
    }
    BOOL success;
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    //NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSArray *paths = NSSearchPathForDirectoriesInDomains(DataDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *databaseName = @"";
    if(isDemoMode)
    {
        databaseName = [NSString stringWithFormat:@"%@",DATABASE_NAME_DEMO];
    }
    else{
        databaseName = [NSString stringWithFormat:@"%@",DATABASE_NAME];
    }
    NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:databaseName];
    
    success = [fileManager fileExistsAtPath:writableDBPath];

    if (success) {
        file = [writableDBPath UTF8String];
    }
    
    if(sqlite3_open(file, &database)!=SQLITE_OK)
    {
//        self.returnCode=0;
        //[self executeUpdateSQL:@"PRAGMA auto_vacuum=1"];
        // [self setCacheSize:1];
    }
//    else self.returnCode=1;
    //NSLog(@"Memory after open = %@", [[CheckMemory sharedCheckMemory] getMemoryStatus]);
    return self;
    //if (sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK)
}
/*
 function for close database local
 */
-(void)close
{
    //NSLog(@"Memory before close = %@", [[CheckMemory sharedCheckMemory] getMemoryStatus]);
    if(sqlStament) {
        //        NSLog(@"finalize = %d", sqlite3_finalize(sqlStament));
        sqlite3_finalize(sqlStament);
    }
    if (self.database) {
        sqlite3_close(self.database);
        //NSLog(@"close db = %d", sqlite3_close(self.database));
    }
    //NSLog(@"Memory after close = %@", [[CheckMemory sharedCheckMemory] getMemoryStatus]);
    //self.sqlStament=nil;
    //sqlite3_shutdown();
}
/*function for reset sqlCommand.*/
-(void)resetSqlCommand
{
    self.sqlStament=nil;
}
//implement :
//-(void)dealloc
//{
//    self.database =nil;
//    self.sqlStament=nil;
//    //sqlite3_close(database);
//    // sqlite3_close(self.database);
//    //sqlite3_finalize(sqlStament);
//    //self.returnCode=nil;
//    [super dealloc];
//    
//    
//}

@end
