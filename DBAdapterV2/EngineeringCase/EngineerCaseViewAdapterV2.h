//
//  EngineerCaseViewAdpaterV2.h
//  mHouseKeeping
//
//  Created by ThuongNM on 3/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DatabaseAdapterV2.h"
#import "EngineerCaseViewModelV2.h"

@interface EngineerCaseViewAdapterV2 : DatabaseAdapterV2 {
    
}

-(int)insertEngineerCaseViewData:(EngineerCaseViewModelV2*) EngineerCaseView;
-(int)updateEngineerCaseViewData:(EngineerCaseViewModelV2*) EngineerCaseView;
-(int)deleteEngineerCaseViewData:(EngineerCaseViewModelV2*) EngineerCaseView;
-(int)deleteAllEngineerCaseViewData:(NSInteger) detail_Id;
-(NSMutableArray*)loadAllEngineerCaseViewModelV2:(NSInteger)detail_id;
-(EngineerCaseViewModelV2 *)loadEngineerCaseModelByCategogyItem:(EngineerCaseViewModelV2 *)model;    
@end
