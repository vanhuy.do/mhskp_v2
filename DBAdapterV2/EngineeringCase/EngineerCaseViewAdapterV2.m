//
//  EngineerCaseViewAdpaterV2.m
//  mHouseKeeping
//
//  Created by ThuongNM on 3/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "EngineerCaseViewAdapterV2.h"
#import "DbDefinesV2.h"
#import "ehkDefines.h"

@implementation EngineerCaseViewAdapterV2

-(int)insertEngineerCaseViewData:(EngineerCaseViewModelV2 *) EngineerCaseView {
    NSString *sqlString = [[NSString alloc] initWithFormat:@"INSERT INTO %@ (%@, %@, %@, %@) %@", engineering_case, ec_category_id, ec_item_id, ec_detail_id, ec_index, @"VALUES(?, ?, ?, ?)"];     
    
    const char *sql = [sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }

    reAccessCount++;
}

    NSInteger status1 = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status1 != SQLITE_OK) {           
    }

    sqlite3_bind_int(self.sqlStament, 1, (int)EngineerCaseView.ec_category_id);
    sqlite3_bind_int(self.sqlStament, 2, (int)EngineerCaseView.ec_item_id);
    sqlite3_bind_int(self.sqlStament, 3, (int)EngineerCaseView.ec_detail_id);
    sqlite3_bind_int(self.sqlStament, 4, (int)EngineerCaseView.ec_index);

    
    NSInteger returnInt = 0;
    
    NSInteger status = sqlite3_step(self.sqlStament);
    
    if(SQLITE_DONE != status) {  
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return (int)returnInt;
    } else {
        sqlite3_reset(self.sqlStament);
        returnInt = 1;
        return (int)returnInt;
    }
}

-(int)updateEngineerCaseViewData:(EngineerCaseViewModelV2*) EngineerCaseView {    

        NSString *sqlString = [[NSString alloc] initWithFormat:@"UPDATE %@ SET %@ = ?, %@ = ?, %@ = ?, %@ = ?,%@ = ? WHERE %@ = ?", engineering_case, ec_id, ec_category_id, ec_item_id, ec_detail_id, ec_index, ec_id];
        
        const char *sql = [sqlString UTF8String];
        NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }

        reAccessCount++;
    }

        NSInteger prepare = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
		
        if(prepare != SQLITE_OK) {
        }
        
        sqlite3_bind_int(self.sqlStament, 1, (int)EngineerCaseView.ec_id);
        sqlite3_bind_int(self.sqlStament, 2, (int)EngineerCaseView.ec_category_id);
        sqlite3_bind_int(self.sqlStament, 3, (int)EngineerCaseView.ec_item_id);
        sqlite3_bind_int(self.sqlStament, 4, (int)EngineerCaseView.ec_detail_id);
        sqlite3_bind_int(self.sqlStament, 5, (int)EngineerCaseView.ec_index);

        sqlite3_bind_int(self.sqlStament, 1, (int)EngineerCaseView.ec_id);
    
    
    NSInteger returnInt = 0;    
    NSInteger status = sqlite3_step(self.sqlStament);
    if(SQLITE_DONE != status) {   
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return (int)returnInt;
    } else { 
        returnInt = 1;        
        return (int)returnInt;
    }
}

-(int)deleteEngineerCaseViewData:(EngineerCaseViewModelV2*) EngineerCaseView {    
    NSInteger result = 1;
    

        NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"DELETE FROM %@ WHERE %@ = ?", engineering_case, ec_id];
        
        const char *sql = [sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }

    reAccessCount++;
}

        if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
			result = 0;
	

    sqlite3_bind_int(self.sqlStament, 1, (int)EngineerCaseView.ec_id);
    
	NSInteger status = sqlite3_step(sqlStament);
	if (SQLITE_DONE != status) {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
		result = 0;
    }
    
    return (int)result;
}

-(int)deleteAllEngineerCaseViewData:(NSInteger)detail_Id {    
    NSInteger result = 1;
    

        NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"DELETE FROM %@ WHERE %@ = ?", engineering_case, ec_detail_id];
        
        const char *sql = [sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }

    reAccessCount++;
}

        if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
			result = 0;
	
	
    sqlite3_bind_int(self.sqlStament, 1, (int)detail_Id);
	
    NSInteger status = sqlite3_step(sqlStament);
	if (SQLITE_DONE != status) {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
		result = 0;
    }
    
    return (int)result;

}

-(NSMutableArray*)loadAllEngineerCaseViewModelV2:(NSInteger)detail_id {
    NSMutableArray *array = [NSMutableArray array];
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ?", ec_id, ec_category_id, ec_item_id, ec_detail_id, ec_index, engineering_case, ec_detail_id];
    
    const char *sql = [sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }

    reAccessCount++;
}

    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {     
    }
    
    sqlite3_bind_int (self.sqlStament, 1, (int)detail_id);
    NSInteger status = sqlite3_step(sqlStament);
    
    while(status == SQLITE_ROW) {
        EngineerCaseViewModelV2 *model =[[EngineerCaseViewModelV2 alloc] init];
        model.ec_id = sqlite3_column_int(sqlStament, 0);
        model.ec_category_id = sqlite3_column_int(sqlStament, 1);
        model.ec_item_id = sqlite3_column_int(sqlStament, 2);
        model.ec_detail_id = sqlite3_column_int(sqlStament, 3);            
        model.ec_index = sqlite3_column_int(sqlStament, 4);            
        
        [array addObject:model]; 
        status = sqlite3_step(sqlStament);
    }
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }

    
    return  array;
}

-(EngineerCaseViewModelV2 *)loadEngineerCaseModelByCategogyItem:(EngineerCaseViewModelV2 *)model { 
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@ ,%@ ,%@ ,%@ FROM %@ WHERE %@ = ? AND %@ = ? AND %@ = ?", ec_id, ec_category_id, ec_item_id, ec_detail_id, ec_index, engineering_case, ec_category_id, ec_item_id, ec_detail_id];
    
    const char *sql = [sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }

    reAccessCount++;
}

    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {        
    }
    
    sqlite3_bind_int (self.sqlStament, 1, (int)model.ec_category_id);
    sqlite3_bind_int (self.sqlStament, 2, (int)model.ec_item_id);
    sqlite3_bind_int (self.sqlStament, 3, (int)model.ec_detail_id);
    
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW)  {
        model.ec_id = sqlite3_column_int(sqlStament, 0);
        model.ec_category_id = sqlite3_column_int(sqlStament, 1);
        model.ec_item_id = sqlite3_column_int(sqlStament, 2);
        model.ec_detail_id = sqlite3_column_int(sqlStament, 3);        
        model.ec_index = sqlite3_column_int(sqlStament, 4);        
        
        return model;
    }

    
    return  NULL;
}

@end
