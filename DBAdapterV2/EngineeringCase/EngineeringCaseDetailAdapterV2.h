//
//  EngineeringCaseDetailAdpaterV2.h
//  mHouseKeeping
//
//  Created by ThuongNM on 3/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DatabaseAdapterV2.h"
#import "EngineeringCaseDetailModelV2.h"

@interface EngineeringCaseDetailAdapterV2 : DatabaseAdapterV2 {
    
}

-(int)insertEngineeringCaseDetailData:(EngineeringCaseDetailModelV2*) 
EngineerCaseView;
-(int)updateEngineeringCaseDetailData:(EngineeringCaseDetailModelV2*) 
EngineerCaseView;
-(int)deleteEngineeringCaseDetailData:(EngineeringCaseDetailModelV2*) 
EngineerCaseView;
-(EngineeringCaseDetailModelV2 *)loadEngineerDetailModelByRoomIdUserIdAndServiceId:(EngineeringCaseDetailModelV2 *)model;
-(NSMutableArray *)loadAllEngineerDetailModelByRoomIdUserId:(NSInteger)userID;
-(NSInteger)numberOfEngineeringCaseItemMustSyn:(EngineeringCaseDetailModelV2 *)model;
-(NSMutableArray*)listEngineeringCaseItemId:(EngineeringCaseDetailModelV2 *)model ;
@end
