//
//  EngineeringCaseDetailAdpaterV2.m
//  mHouseKeeping
//
//  Created by ThuongNM on 3/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "EngineeringCaseDetailAdapterV2.h"
#import "DbDefinesV2.h"
#import "ehkDefines.h"

@implementation EngineeringCaseDetailAdapterV2

-(int)insertEngineeringCaseDetailData:(EngineeringCaseDetailModelV2*) EngineerCaseView {    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"INSERT INTO %@ (%@, %@, %@, %@,%@) %@", engineering_case_detail, ecd_user_id, ecd_room_id, ecd_pos_status, ecd_date, ecd_remark, @"VALUES(?, ?, ?, ?, ?)"];     
    
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }

    reAccessCount++;
}
    NSInteger status1=sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status1 != SQLITE_OK) {
     //   sqlite3_bind_int(self.sqlStament, 1,EngineerCaseView.ecd_id );
    }
    
    sqlite3_bind_int(self.sqlStament, 1, (int)EngineerCaseView.ecd_user_id );
    sqlite3_bind_text(self.sqlStament, 2 , [EngineerCaseView.ecd_room_id UTF8String], -1,SQLITE_TRANSIENT);
    sqlite3_bind_int(self.sqlStament, 3, (int)EngineerCaseView.ecd_pos_status);
    sqlite3_bind_text(self.sqlStament, 4, [EngineerCaseView.ecd_date UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 5, [EngineerCaseView.ecd_reMark UTF8String], -1, SQLITE_TRANSIENT);

    
    NSInteger returnInt = 0;
    
    NSInteger status = sqlite3_step(self.sqlStament);
    
    if(SQLITE_DONE != status) {  
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return (int)returnInt;
    } else {
        sqlite3_reset(self.sqlStament);
        returnInt = 1;
        return (int)returnInt;
    }

}

-(int)updateEngineeringCaseDetailData:(EngineeringCaseDetailModelV2*) EngineerCaseView {    

        NSString *sqlString = [[NSString alloc] initWithFormat:@"UPDATE %@ SET %@ = ?, %@ = ?,%@= ?,%@= ?,%@= ? WHERE %@ = ?", engineering_case_detail, ecd_user_id, ecd_room_id, ecd_pos_status, ecd_date, ecd_remark, ecd_id];
        const char *sql = [sqlString UTF8String];
        NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }

        reAccessCount++;
    }

        NSInteger prepare = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
		if(prepare != SQLITE_OK){
        }
        
        sqlite3_bind_int(self.sqlStament, 1, (int)EngineerCaseView.ecd_user_id );
        sqlite3_bind_text(self.sqlStament, 2 , [EngineerCaseView.ecd_room_id UTF8String], -1,SQLITE_TRANSIENT);
        sqlite3_bind_int(self.sqlStament, 3, (int)EngineerCaseView.ecd_pos_status);
        sqlite3_bind_text(self.sqlStament, 4, [EngineerCaseView.ecd_date UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(self.sqlStament, 5, [EngineerCaseView.ecd_reMark UTF8String], -1, SQLITE_TRANSIENT);
        
        sqlite3_bind_int(self.sqlStament, 6,(int)EngineerCaseView.ecd_id );
    
    NSInteger returnInt = 0;    
    NSInteger status = sqlite3_step(self.sqlStament);
    if(SQLITE_DONE != status) {   
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return (int)returnInt;
    } else { 
        returnInt = 1;        
        return (int)returnInt;
    }
}

-(int)deleteEngineeringCaseDetailData:(EngineeringCaseDetailModelV2*) EngineerCaseView {
    NSInteger result = 1;

        NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"DELETE FROM %@ WHERE %@ = ?", engineering_case_detail, ecd_id];
		const char *sql = [sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }

    reAccessCount++;
}

		if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
			result = 0;

	
    sqlite3_bind_int(self.sqlStament, 1,(int)EngineerCaseView.ecd_id );
	
    NSInteger status = sqlite3_step(sqlStament);
    
	if (SQLITE_DONE != status) {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
		result = 0;
    }
    
    return (int)result;

}

-(EngineeringCaseDetailModelV2 *)loadEngineerDetailModelByRoomIdUserIdAndServiceId:(EngineeringCaseDetailModelV2 *)model {    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@ ,%@,%@ ,%@, %@ FROM %@ WHERE %@ = ? AND %@ = ?", 
                           ecd_id,
                           ecd_room_id,
                           ecd_user_id,
                           ecd_pos_status,
                           ecd_date,
                           ecd_remark,
                           engineering_case_detail,
                           ecd_room_id,
                           ecd_user_id];
    
    const char *sql=[sqlString UTF8String];
   
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    
    sqlite3_bind_text(self.sqlStament, 1 , [model.ecd_room_id UTF8String], -1,SQLITE_TRANSIENT);
    sqlite3_bind_int (self.sqlStament, 2, (int)model.ecd_user_id);
    
    while(sqlite3_step(sqlStament) == SQLITE_ROW) {
        model.ecd_id = sqlite3_column_int(sqlStament, 0);
        if (sqlite3_column_text(self.sqlStament, 1)) {
            model.ecd_room_id = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
        }
        model.ecd_user_id = sqlite3_column_int(sqlStament, 2);
        model.ecd_pos_status = sqlite3_column_int(sqlStament, 3);
        
        if (sqlite3_column_text(sqlStament, 4)) {
            model.ecd_date = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
        }
        
        if(sqlite3_column_text(sqlStament, 5)) {               
            model.ecd_reMark=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
        }
        
        return model;
    }

    
    return model;
}

-(NSMutableArray *)loadAllEngineerDetailModelByRoomIdUserId:(NSInteger)userID {    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@ ,%@,%@ ,%@, %@ FROM %@ WHERE %@ = ?", 
                           ecd_id,
                           ecd_room_id,
                           ecd_user_id,
                           ecd_pos_status,
                           ecd_date,
                           ecd_remark,
                           engineering_case_detail,
                           ecd_user_id];
    
    const char *sql=[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }

    reAccessCount++;
}
    NSMutableArray *result = [NSMutableArray array];
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    
    sqlite3_bind_int (self.sqlStament, 1, (int)userID);
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW) {
        EngineeringCaseDetailModelV2 *model = [[EngineeringCaseDetailModelV2 alloc] init];
        
        model.ecd_id = sqlite3_column_int(sqlStament, 0);
        if (sqlite3_column_text(self.sqlStament, 1)) {
            model.ecd_room_id = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
        }
        model.ecd_user_id = sqlite3_column_int(sqlStament, 2);
        model.ecd_pos_status = sqlite3_column_int(sqlStament, 3);
        
        if (sqlite3_column_text(sqlStament, 4)) {
            model.ecd_date = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
        }
        
        if(sqlite3_column_text(sqlStament, 5)) {               
            model.ecd_reMark=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
        }
        
        [result addObject:model];
        status = sqlite3_step(sqlStament);
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            [result removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }

    
    return result;
}

-(NSInteger)numberOfEngineeringCaseItemMustSyn:(EngineeringCaseDetailModelV2 *)model {    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT COUNT(*) FROM %@ WHERE %@ = ? ", 
                           engineering_case_detail,
                           ecd_user_id];
    
    const char *sql=[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}

    NSInteger result = 0;
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {}
        
        sqlite3_bind_int (self.sqlStament, 1, (int)model.ecd_user_id);
        
        NSInteger status = sqlite3_step(sqlStament);
        while(sqlite3_step(sqlStament) == SQLITE_ROW) {
            
            result = sqlite3_column_int(sqlStament, 0);
            status = sqlite3_step(sqlStament);
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount < REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }

    
    
    return result;
}


-(NSMutableArray*)listEngineeringCaseItemId:(EngineeringCaseDetailModelV2 *)model {    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@  FROM %@ WHERE %@ = ?", 
                           ecd_id,
                           engineering_case_detail,
                           ecd_user_id];
    
    const char *sql=[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }

    reAccessCount++;
}
    
    NSMutableArray *result = [NSMutableArray array];
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {}
        
        sqlite3_bind_int (self.sqlStament, 1, (int)model.ecd_user_id);
         NSInteger status = sqlite3_step(sqlStament);
        
        while(status == SQLITE_ROW) {
            NSInteger _itemId = sqlite3_column_int(sqlStament, 0);
            [result addObject:[NSString stringWithFormat:@"%d",(int)_itemId]];
            status = sqlite3_step(sqlStament);
        }
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                [result removeAllObjects];
                goto RE_ACCESS_DB;
            }
        }

    
    
    return result;

}
@end
