//
//  EngineeringCatagoriesAdapter.h
//  mHouseKeeping
//
//  Created by ThuongNM on 3/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DatabaseAdapterV2.h"
#import "EngineeringCategoriesModeV2.h"

@interface EngineeringCatagoriesAdapterV2 : DatabaseAdapterV2{
    
}

-(int)insertEngineeringCatagoriesData:(EngineeringCategoriesModeV2*) EngineerCaseView;
-(int)updateEngineeringCatagoriesData:(EngineeringCategoriesModeV2*) EngineerCaseView;
-(int)deleteEngineeringCatagoriesData:(EngineeringCategoriesModeV2*) EngineerCaseView;
-(EngineeringCategoriesModeV2*)loadEngineeringCategoriesModeV2ById:(int)categoryId;
-(NSMutableArray*)loadAllEngineeringCatagoriesModelV2;
-(NSMutableArray*)loadAllEngineeringCatagoriesID:(int)ID;

//Get last date modified engineering category
-(NSString*)getEngineeringCategoryLastModifiedDate;

@end

