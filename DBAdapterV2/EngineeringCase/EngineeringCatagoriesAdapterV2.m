//
//  EngineeringCatagoriesAdapter.m
//  mHouseKeeping
//
//  Created by ThuongNM on 3/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "EngineeringCatagoriesAdapterV2.h"
#import "DbDefinesV2.h"
#import "ehkDefines.h"

@implementation EngineeringCatagoriesAdapterV2

-(int)insertEngineeringCatagoriesData:(EngineeringCategoriesModeV2*) EngineerCaseView {    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"INSERT INTO %@ (%@, %@, %@, %@, %@) %@", engineering_categories, enca_id,enca_name, enca_lang_name, enca_image, enca_last_modified, @"VALUES(?, ?, ?, ?, ?)"];     
    
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }

    reAccessCount++;
}

    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {}
        sqlite3_bind_int(self.sqlStament, 1,(int)EngineerCaseView.enca_id );
        sqlite3_bind_text(self.sqlStament, 2, [EngineerCaseView.enca_name UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(self.sqlStament, 3, [EngineerCaseView.enca_lang_name UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_blob(self.sqlStament, 4, [EngineerCaseView.enca_image bytes], (int)[EngineerCaseView.enca_image length], NULL);
        sqlite3_bind_text(self.sqlStament, 5, [EngineerCaseView.enca_last_modified UTF8String], -1, SQLITE_TRANSIENT);
    
    
    NSInteger returnInt = 0;
    
    NSInteger status = sqlite3_step(self.sqlStament);
    
    if(SQLITE_DONE != status) {  
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return (int)returnInt;
    } else {
        sqlite3_reset(self.sqlStament);
        returnInt = 1;
        return (int)returnInt;
    }

}

-(int)updateEngineeringCatagoriesData:(EngineeringCategoriesModeV2*) EngineerCaseView {    

        NSString *sqlString = [[NSString alloc] initWithFormat:@"UPDATE %@ SET %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ? WHERE %@ = ?", engineering_categories, enca_id,enca_name, enca_lang_name, enca_image,enca_id, enca_last_modified];
        
        const char *sql = [sqlString UTF8String];
        NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }

        reAccessCount++;
    }

        NSInteger prepare = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
		if(prepare != SQLITE_OK) {
        }
        
        sqlite3_bind_int(self.sqlStament, 1,(int)EngineerCaseView.enca_id);
        sqlite3_bind_text(self.sqlStament, 2, [EngineerCaseView.enca_name UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(self.sqlStament, 3, [EngineerCaseView.enca_lang_name UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_blob(self.sqlStament, 4, [EngineerCaseView.enca_image bytes], (int)[EngineerCaseView.enca_image length], NULL);
        sqlite3_bind_text(self.sqlStament, 5, [EngineerCaseView.enca_last_modified UTF8String], -1, SQLITE_TRANSIENT);
        
        sqlite3_bind_int(self.sqlStament, 6, (int)EngineerCaseView.enca_id);
    
    
    NSInteger returnInt = 0;    
    NSInteger status = sqlite3_step(self.sqlStament);
    if(SQLITE_DONE != status) {   
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return (int)returnInt;
    } else { 
        returnInt = 1;        
        return (int)returnInt;
    }
}

-(int)deleteEngineeringCatagoriesData:(EngineeringCategoriesModeV2*) EngineerCaseView {
    NSInteger result = 1;

        NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"DELETE FROM %@ WHERE %@ = ?", engineering_categories, enca_id];
		const char *sql = [sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }

    reAccessCount++;
}

    
		if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
			result = 0;

	
    sqlite3_bind_int(self.sqlStament, 1, (int)EngineerCaseView.enca_id );
	
    NSInteger status = sqlite3_step(sqlStament);
    
	if (SQLITE_DONE != status) {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
		result = 0;
    }
    
    return (int)result;
}

-(EngineeringCategoriesModeV2*)loadEngineeringCategoriesModeV2ById:(int)categoryId {
    EngineeringCategoriesModeV2 *model =[[EngineeringCategoriesModeV2 alloc] init];
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@ FROM %@ where %@ = %d", enca_id, enca_name, enca_lang_name, enca_image, enca_last_modified, engineering_categories, enca_id, categoryId];
    const char *sql=[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
        
    }
    
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW) {
        
        if(sqlite3_column_text(sqlStament, 0)){
            model.enca_id= sqlite3_column_int(sqlStament, 0);
        }
        if(sqlite3_column_text(sqlStament, 1)){
            model.enca_name=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
        }
        if(sqlite3_column_text(sqlStament, 2)){
            model.enca_lang_name=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
        }
        if(sqlite3_column_blob(sqlStament, 3)){
            NSData *image = [[NSData alloc] initWithBytes:sqlite3_column_blob(sqlStament, 3) length:sqlite3_column_bytes(sqlStament, 3)];
            [model setEnca_image:image];
        }
        if(sqlite3_column_text(sqlStament, 4)){
            model.enca_last_modified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
        }
        
        status = sqlite3_step(sqlStament);
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    return  model;
}

-(NSMutableArray*)loadAllEngineeringCatagoriesModelV2 {    
    NSMutableArray *array = [NSMutableArray array];
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@ FROM %@", enca_id, enca_name, enca_lang_name, enca_image, enca_last_modified, engineering_categories];
    const char *sql=[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }

    reAccessCount++;
}

    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {     
        
        }
    
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW) {
        
        EngineeringCategoriesModeV2 *model =[[EngineeringCategoriesModeV2 alloc] init];
        if(sqlite3_column_text(sqlStament, 0)){
            model.enca_id= sqlite3_column_int(sqlStament, 0);
        }
        if(sqlite3_column_text(sqlStament, 1)){
            model.enca_name=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];            
        }
        if(sqlite3_column_text(sqlStament, 2)){
            model.enca_lang_name=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
        }
        if(sqlite3_column_blob(sqlStament, 3)){
            NSData *image = [[NSData alloc] initWithBytes:sqlite3_column_blob(sqlStament, 3) length:sqlite3_column_bytes(sqlStament, 3)];
            [model setEnca_image:image];
        }
        if(sqlite3_column_text(sqlStament, 4)){
            model.enca_last_modified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];            
        }
        
        [array addObject:model];
        status = sqlite3_step(sqlStament);
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    

    
    return  array;    
}

-(NSMutableArray*)loadAllEngineeringCatagoriesID:(int)ID {    
    NSMutableArray * array=[NSMutableArray array];
    NSMutableString * sqlString=[[NSMutableString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@ FROM %@ Where %@ = %d ", enca_id, enca_name, enca_lang_name, enca_image, enca_last_modified, engineering_categories, enca_id, ID];
    const char *sql=[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }

    reAccessCount++;
}

    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {}
    
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW) {
        
        EngineeringCategoriesModeV2 *model =[[EngineeringCategoriesModeV2 alloc] init];
        if(sqlite3_column_text(sqlStament, 0)){
            model.enca_id= sqlite3_column_int(sqlStament, 0);
        }
        if(sqlite3_column_text(sqlStament, 1)){
            model.enca_name=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
        }
        if(sqlite3_column_text(sqlStament, 2)){
            model.enca_lang_name=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
        }
        if(sqlite3_column_text(sqlStament, 3)){
            model.enca_name=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];            
        }
        if(sqlite3_column_text(sqlStament, 4)){
            model.enca_last_modified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];            
        }
        
        [array addObject:model];
        status = sqlite3_step(sqlStament);
    }
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    return  array;    
}

-(NSString*)getEngineeringCategoryLastModifiedDate
{
    NSMutableString * sqlString=[[NSMutableString alloc] initWithFormat:@"SELECT MAX(%@) FROM %@ ORDER BY %@ ASC", enca_last_modified, engineering_categories, enca_last_modified];
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    reAccessCount ++;
}
    
    NSString *lastModified;
    
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        
        status = sqlite3_step(sqlStament);
        while(status == SQLITE_ROW) {
            
            if(sqlite3_column_text(sqlStament, 0)){
                lastModified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 0)];
            }
            return lastModified;
        }
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    return  lastModified;
}

@end
