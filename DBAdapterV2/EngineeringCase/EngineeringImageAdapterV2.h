//
//  EngineeringImageAdapterV2.h
//  mHouseKeeping
//
//  Created by ThuongNM on 3/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DatabaseAdapterV2.h"
#import "EngineerImageModelV2.h"

@interface EngineeringImageAdapterV2 : DatabaseAdapterV2 {
    
}

-(int)insertEngineerImageData:(EngineerImageModelV2*) EngineerCaseView;
-(int)updateEngineerImageData:(EngineerImageModelV2*) EngineerCaseView;
-(int)deleteEngineerImageData:(EngineerImageModelV2*) EngineerCaseView;
-(int)deleteAllEngineerImageData:(int)ID;
-(NSMutableArray *)loadAllEngineerImageDataByDetailId:(int)ID;
-(NSInteger)numberOfECAttachPhotoMustSyn:(NSMutableArray*)list_Enim_Detail_Id;
@end
