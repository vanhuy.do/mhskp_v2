//
//  EngineeringImageAdapterV2.m
//  mHouseKeeping
//
//  Created by ThuongNM on 3/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "EngineeringImageAdapterV2.h"
#import "DbDefinesV2.h"
#import "ehkDefines.h"

@implementation EngineeringImageAdapterV2

-(int)insertEngineerImageData:(EngineerImageModelV2*) EngineerCaseView {    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"INSERT INTO %@ (%@, %@, %@,%@) %@", engineering_images, enim_id, enim_image, enim_detail_id, enim_case_id, @"VALUES(?, ?, ?, ?)"];
    
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }

    reAccessCount++;
}
    NSInteger status1=sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status1 != SQLITE_OK) {
    }
    
    sqlite3_bind_int(self.sqlStament, 1,(int)EngineerCaseView.enim_id);
    sqlite3_bind_blob(self.sqlStament, 2, [EngineerCaseView.enim_image bytes], (int)[EngineerCaseView.enim_image length], NULL);
    sqlite3_bind_int(self.sqlStament, 3,(int)EngineerCaseView.enim_detail_id);
    sqlite3_bind_int(self.sqlStament, 4,(int)EngineerCaseView.enim_case_id);
    
    NSInteger returnInt = 0;
    
    NSInteger status = sqlite3_step(self.sqlStament);
    
    if(SQLITE_DONE != status) {  
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return (int)returnInt;
    } else {
        sqlite3_reset(self.sqlStament);
        returnInt = 1;
        return (int)returnInt;
    }

}

-(int)updateEngineerImageData:(EngineerImageModelV2*) EngineerCaseView {    

        NSString *sqlString = [[NSString alloc] initWithFormat:@"UPDATE %@ SET %@ = ?, %@ = ?,%@= ?, %@ = ? WHERE %@ = ?", engineering_images, enim_id, enim_image, enim_detail_id, enim_case_id, enim_id];
        const char *sql = [sqlString UTF8String];
        NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }

        reAccessCount++;
    }
    
    NSInteger prepare = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(prepare != SQLITE_OK){
    }
    
    sqlite3_bind_int(self.sqlStament, 1,(int)EngineerCaseView.enim_id);
    sqlite3_bind_blob(self.sqlStament, 2, [EngineerCaseView.enim_image bytes], (int)[EngineerCaseView.enim_image length], NULL);
    sqlite3_bind_int(self.sqlStament, 3, (int)EngineerCaseView.enim_detail_id);
    sqlite3_bind_int(self.sqlStament, 4, (int)EngineerCaseView.enim_case_id);
    sqlite3_bind_int(self.sqlStament, 5,(int)EngineerCaseView.enim_id);
    
    NSInteger returnInt = 0;
    NSInteger status = sqlite3_step(self.sqlStament);
    if(SQLITE_DONE != status) {   
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return (int)returnInt;
    } else { 
        returnInt = 1;        
        return (int)returnInt;
    }
}

-(int)deleteEngineerImageData:(EngineerImageModelV2*) EngineerCaseView {    
    NSInteger result = 1;

        NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"DELETE FROM %@ WHERE %@ = ?", engineering_images, enim_id];
		const char *sql = [sqlString UTF8String];
        NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }

        reAccessCount++;
    }

		if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
			result = 0;
	
	
    sqlite3_bind_int(self.sqlStament, 1, (int)EngineerCaseView.enim_id );
	
    NSInteger status = sqlite3_step(sqlStament);
    
	if (SQLITE_DONE != status) {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
		result = 0;
    }
    
    return (int)result;

}

-(int)deleteAllEngineerImageData:(int)ID {    
    NSInteger result = 1;

        NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"DELETE FROM %@ WHERE %@ = ?", engineering_images, enim_detail_id];
		const char *sql = [sqlString UTF8String];
        NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }

        reAccessCount++;
    }

		if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
			result = 0;
	
	
    sqlite3_bind_int(self.sqlStament, 1, ID);
	
    NSInteger status = sqlite3_step(sqlStament);
    
	if (SQLITE_DONE != status) {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
		result = 0;
    }
    
    return (int)result;
}


-(NSMutableArray *)loadAllEngineerImageDataByDetailId:(int)ID {    
    NSMutableArray * array = [NSMutableArray array];
    NSMutableString * sqlString = [[NSMutableString alloc] initWithFormat:@"SELECT %@, %@, %@ FROM %@ WHERE %@ = ? ORDER BY %@", enim_id, enim_image, enim_detail_id, engineering_images, enim_detail_id, enim_id];
    const char *sql = [sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }

    reAccessCount++;
}

    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {     
    }
    
    sqlite3_bind_int(self.sqlStament, 1, ID);
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW) {
        
        
        EngineerImageModelV2 *model = [[EngineerImageModelV2 alloc] init];
        
        if(sqlite3_column_text(sqlStament, 0)) {
            model.enim_id = sqlite3_column_int(sqlStament, 0);
        }
        
        if(sqlite3_column_blob(sqlStament, 1)) {
            NSData *image = [[NSData alloc] initWithBytes:sqlite3_column_blob(sqlStament, 1) length:sqlite3_column_bytes(sqlStament, 1)];
            [model setEnim_image:image];
        }
        
        if(sqlite3_column_text(sqlStament, 2)) {
            model.enim_detail_id=sqlite3_column_int(sqlStament, 2);
        }
        
        [array addObject:model];
        status = sqlite3_step(sqlStament);
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }

    
    return  array;
}

-(NSInteger)numberOfECAttachPhotoMustSyn:(NSMutableArray*)list_Enim_Detail_Id{
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT COUNT(*) FROM %@ WHERE %@ = ?", 
                           engineering_images,
                           enim_detail_id];
    
    const char *sql=[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }

    reAccessCount++;
}
    NSInteger result = 0;
    for (int index = 0; index < [list_Enim_Detail_Id count]; index ++) {
        NSInteger enim_Detail_Id = [[NSString stringWithFormat:@"%@",[list_Enim_Detail_Id objectAtIndex:index]] intValue];
           
        if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
        }
        
        sqlite3_bind_int (self.sqlStament, 1, (int)enim_Detail_Id);
        
        NSInteger status = sqlite3_step(sqlStament);
        while(status == SQLITE_ROW) {                
            result += sqlite3_column_int(sqlStament, 0);
            status = sqlite3_step(sqlStament);
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount < REACCESS_MAX_TIMES) {
                
                goto RE_ACCESS_DB;
            }
            
            if (status == SQLITE_BUSY) {
                if (reAccessCount <= REACCESS_MAX_TIMES) {
                   
                    goto RE_ACCESS_DB;
                }
            }

        }


    }
    
    return result;
}

@end
