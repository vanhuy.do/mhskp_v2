//
//  EngineeringItemAdapter.h
//  mHouseKeeping
//
//  Created by ThuongNM on 3/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DatabaseAdapterV2.h"
#import "EngineeringItemModelV2.h"

@interface EngineeringItemAdapterV2 : DatabaseAdapterV2 {
    
}

-(EngineeringItemModelV2 *) loadEngineeringItemModelV2:(NSInteger) itemId;
-(int)insertengineeringItemData:(EngineeringItemModelV2*) EngineerCaseView;
-(int)updatengineeringItemData:(EngineeringItemModelV2*) EngineerCaseView;
-(int)deletengineeringItemData:(EngineeringItemModelV2*) EngineerCaseView;
-(NSMutableArray*)loadAllEngineeringItemModelV2;
-(NSMutableArray*)loadAllEngineeringItembyItemCategogy:(NSInteger)categogy_id item_id:(NSInteger)item_id;
-(NSMutableArray*)loadAllEngineeringItemID:(int)ID;
@end
