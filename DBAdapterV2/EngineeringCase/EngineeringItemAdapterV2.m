//
//  EngineeringItemAdapter.m
//  mHouseKeeping
//
//  Created by ThuongNM on 3/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "EngineeringItemAdapterV2.h"
#import "DbDefinesV2.h"
#import "ehkDefines.h"
#import "LogFileManager.h"

@implementation EngineeringItemAdapterV2

-(int)insertengineeringItemData:(EngineeringItemModelV2*) EngineerCaseView {    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"INSERT INTO %@ (%@, %@, %@, %@, %@, %@) %@", engineering_items, eni_id, eni_name, eni_name_lang, eni_category_id, eni_last_modified, eni_post_id, @"VALUES(?, ?, ?, ?, ?, ?)"];
    
    if([LogFileManager isLogConsole])
    {
        NSLog(@"%@",sqlString);
    }
    
    const char *sql=[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }

    reAccessCount++;
}

    NSInteger status1=sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status1 != SQLITE_OK) {
    }
    
    sqlite3_bind_int(self.sqlStament, 1,(int)EngineerCaseView.eni_id);
    sqlite3_bind_text(self.sqlStament, 2, [EngineerCaseView.eni_name UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 3, [EngineerCaseView.eni_name_lang UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_int(self.sqlStament, 4, (int)EngineerCaseView.eni_category_id );
    sqlite3_bind_text(self.sqlStament, 5, [EngineerCaseView.eni_last_modified UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 6, [EngineerCaseView.eni_post_id UTF8String], -1, SQLITE_TRANSIENT);

    
    NSInteger returnInt = 0;
    
    NSInteger status = sqlite3_step(self.sqlStament);
    
    if(SQLITE_DONE != status) {  
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return (int)returnInt;
    } else {
        sqlite3_reset(self.sqlStament);
        returnInt = 1;
        return (int)returnInt;
    }
}

-(int)updatengineeringItemData:(EngineeringItemModelV2*) EngineerCaseView {

        NSString *sqlString = [[NSString alloc] initWithFormat:@"UPDATE %@ SET %@ = ?, %@ = ?,%@ = ?, %@ = ?, %@ = ?, %@ = ? WHERE %@ = ?", engineering_items, eni_id, eni_name, eni_name_lang, eni_category_id, eni_last_modified, eni_post_id, eni_id];
        const char *sql = [sqlString UTF8String];
        NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }

        reAccessCount++;
    }

    NSInteger prepare = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(prepare != SQLITE_OK) {
    }
    
    sqlite3_bind_int(self.sqlStament, 1,(int)EngineerCaseView.eni_id);
    sqlite3_bind_text(self.sqlStament, 2, [EngineerCaseView.eni_name UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 3, [EngineerCaseView.eni_name_lang UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_int(self.sqlStament, 4, (int)EngineerCaseView.eni_category_id);
    sqlite3_bind_text(self.sqlStament, 5, [EngineerCaseView.eni_last_modified UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 6, [EngineerCaseView.eni_post_id UTF8String], -1, SQLITE_TRANSIENT);
    
    sqlite3_bind_int(self.sqlStament, 7, (int)EngineerCaseView.eni_id);
    
    
    NSInteger returnInt = 0;
    NSInteger status = sqlite3_step(self.sqlStament);
    if(SQLITE_DONE != status) {   
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return (int)returnInt;
    } else { 
        returnInt = 1;        
        return (int)returnInt;
    }
}

-(int)deletengineeringItemData:(EngineeringItemModelV2*) EngineerCaseView {
    NSInteger result = 1;

        NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"DELETE FROM %@ WHERE %@ = ?", engineering_items, eni_id];
		const char *sql = [sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }

    reAccessCount++;
}

		if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
			result = 0;

	
    sqlite3_bind_int(self.sqlStament, 1,(int)EngineerCaseView.eni_id);
    
	NSInteger status = sqlite3_step(sqlStament);
	if (SQLITE_DONE != status) {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
		result = 0;
    }
    
    return (int)result;
}

-(NSMutableArray*)loadAllEngineeringItemModelV2 {
    NSMutableArray *array=[NSMutableArray array];
    NSMutableString *sqlString=[[NSMutableString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@ FROM %@ ", eni_id, eni_name, eni_name_lang, eni_category_id, eni_last_modified, eni_post_id, engineering_items];
    const char *sql=[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }

    reAccessCount++;
}

    NSInteger i=sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(i != SQLITE_OK) {     
    }
    
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW) {
        
        EngineeringItemModelV2 *model =[[EngineeringItemModelV2 alloc] init];
        
        model.eni_id= sqlite3_column_int(sqlStament, 0);
        
        if(sqlite3_column_text(sqlStament, 1)){
            model.eni_name=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
        }
        if(sqlite3_column_text(sqlStament, 2)){
            model.eni_name_lang=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
        }
        
        model.eni_category_id=sqlite3_column_int(sqlStament, 3);
        
        if(sqlite3_column_text(sqlStament, 4)){
            model.eni_last_modified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
        }
        
        if(sqlite3_column_text(sqlStament, 5)){
            model.eni_post_id=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
        }
        
        [array addObject:model];
        status = sqlite3_step(sqlStament);
    }
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    

    
    return  array;    
}

-(NSMutableArray*)loadAllEngineeringItembyItemCategogy:(NSInteger)categogy_id item_id:(NSInteger)item_id {    
    NSMutableArray * array=[NSMutableArray array];
    NSMutableString * sqlString=[[NSMutableString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@ FROM %@ Where %@= ? AND %@= ?",eni_id, eni_name, eni_name_lang, eni_category_id, eni_last_modified, eni_post_id, engineering_items, eni_id, eni_category_id];
    
    const char *sql=[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }

    reAccessCount++;
}

    NSInteger i=sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(i != SQLITE_OK) {     
    }
    sqlite3_bind_int (sqlStament, 1,(int)item_id);
    sqlite3_bind_int(sqlStament, 2,(int)categogy_id);
    NSInteger status = sqlite3_step(sqlStament);
    
    while(status == SQLITE_ROW) {
        
        EngineeringItemModelV2 *model =[[EngineeringItemModelV2 alloc] init];
        
        model.eni_id= sqlite3_column_int(sqlStament, 0);
        
        if(sqlite3_column_text(sqlStament, 1)){
            model.eni_name=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
        }
        if(sqlite3_column_text(sqlStament, 2)){
            model.eni_name_lang=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
        }
        
        model.eni_category_id=sqlite3_column_int(sqlStament, 3);
        
        if(sqlite3_column_text(sqlStament, 4)){
            model.eni_last_modified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
        }
        
        if(sqlite3_column_text(sqlStament, 5)){
            model.eni_post_id = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
        }
        
        [array addObject:model];
        status = sqlite3_step(sqlStament);
    }
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }

    
    return  array;
}

-(NSMutableArray*)loadAllEngineeringItemID:(int)ID {    
    NSMutableArray * array=[NSMutableArray array];
    NSMutableString * sqlString=[[NSMutableString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@ FROM %@ Where %@ = ?", eni_id, eni_name, eni_name_lang, eni_category_id,eni_last_modified, eni_post_id, engineering_items, eni_category_id];
    const char *sql=[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }

    reAccessCount++;
}

    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    
    sqlite3_bind_int (sqlStament, 1,ID);
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW) {
        EngineeringItemModelV2 *model =[[EngineeringItemModelV2 alloc] init];
        
        model.eni_id= sqlite3_column_int(sqlStament, 0);
        
        if(sqlite3_column_text(sqlStament, 1)){
            model.eni_name=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
        }
        if(sqlite3_column_text(sqlStament, 2)){
            model.eni_name_lang=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
        }
        
        model.eni_category_id=sqlite3_column_int(sqlStament, 3);
        
        if(sqlite3_column_text(sqlStament, 4)){
            model.eni_last_modified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
        }
        
        if(sqlite3_column_text(sqlStament, 5)){
            model.eni_post_id = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
        }
        
        [array addObject:model];
        status = sqlite3_step(sqlStament);
    }
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }

    
    return  array;    
}

-(EngineeringItemModelV2 *) loadEngineeringItemModelV2:(NSInteger)itemId {
    NSMutableString * sqlString=[[NSMutableString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@ FROM %@ Where %@ = ?", eni_id, eni_name, eni_name_lang, eni_category_id,eni_last_modified, eni_post_id, engineering_items, eni_id];
    const char *sql=[sqlString UTF8String];
    NSInteger reAccessCount = 0;

RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    
    sqlite3_bind_int (sqlStament, 1,(int)itemId);
    
    NSInteger status = sqlite3_step(sqlStament);
    EngineeringItemModelV2 *model = [[EngineeringItemModelV2 alloc] init];
    while(status == SQLITE_ROW) {
        model.eni_id= sqlite3_column_int(sqlStament, 0);
        
        if(sqlite3_column_text(sqlStament, 1)){
            model.eni_name=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
        }
        if(sqlite3_column_text(sqlStament, 2)){
            model.eni_name_lang=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
        }
        
        model.eni_category_id=sqlite3_column_int(sqlStament, 3);
        
        if(sqlite3_column_text(sqlStament, 4)){
            model.eni_last_modified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
        }
        
        if(sqlite3_column_text(sqlStament, 5)){
            model.eni_post_id = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
        }
        
        status = sqlite3_step(sqlStament);
    }
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            model = nil;
            goto RE_ACCESS_DB;
        }
    }
    
    return model;
}

@end
