//
//  EngineeringHistoryAdapter.h
//  mHouseKeeping
//
//  Created by Mac User on 02/04/2013.
//
//

#import <Foundation/Foundation.h>
#import "DatabaseAdapterV2.h"
#import "EngineeringHistoryModel.h"

@interface EngineeringHistoryAdapter : DatabaseAdapterV2

-(int) insertEngineeringHistory:(EngineeringHistoryModel *)model;
-(NSMutableArray *) loadRoomIdByUserId:(NSInteger)userId;
-(NSMutableArray *) loadAllEngineeringHistoryByUserId:(NSInteger)userId AndRoomId:(NSString*)roomNumber;
-(int) deleteDatabaseAfterSomeDays;

@end
