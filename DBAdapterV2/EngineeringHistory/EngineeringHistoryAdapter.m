//
//  EngineeringHistoryAdapter.m
//  mHouseKeeping
//
//  Created by Mac User on 02/04/2013.
//
//

#import "EngineeringHistoryAdapter.h"
#import "NSDate-Utilities.h"

@implementation EngineeringHistoryAdapter

-(int) insertEngineeringHistory:(EngineeringHistoryModel *)model{
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"INSERT INTO %@ (%@, %@, %@, %@, %@) %@", ENGINEERING_HISTORY, EH_id, EH_user_id, EH_room_id, EH_item_id,EH_date, @"VALUES(?, ?, ?, ?, ?)"];
    
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {}
    
    sqlite3_bind_int(self.sqlStament, 2, (int)model.eh_user_id);
    sqlite3_bind_text(self.sqlStament, 3 , [model.eh_room_id UTF8String], -1,SQLITE_TRANSIENT);
    sqlite3_bind_int(self.sqlStament, 4, (int)model.eh_item_id);
    sqlite3_bind_text(self.sqlStament, 5, [model.eh_date UTF8String], -1, SQLITE_TRANSIENT);
    NSInteger returnInt = 0;
    
    NSInteger status = sqlite3_step(self.sqlStament);
    
    if(SQLITE_DONE != status) {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return (int)returnInt;
    } else {
        sqlite3_reset(self.sqlStament);
        returnInt = 1;
        return (int)returnInt;
    }

}

-(NSMutableArray *) loadRoomIdByUserId:(NSInteger)userId{
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT DISTINCT %@ FROM %@ WHERE %@ = ? ORDER BY %@ ASC",
                           EH_room_id,
                           ENGINEERING_HISTORY,
                           EH_user_id,
                           EH_room_id
                           ];
    
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    reAccessCount ++;
}
    
    NSMutableArray *array = [NSMutableArray array];
    NSInteger status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        
        sqlite3_bind_int(self.sqlStament, 1, (int)userId);
        
        status = sqlite3_step(self.sqlStament);
        while(status == SQLITE_ROW)
        {
            EngineeringHistoryModel *model = [[EngineeringHistoryModel alloc] init];
            if (sqlite3_column_text(self.sqlStament, 0)) {
                model.eh_room_id = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 0)];
            }
            
            [array addObject:model];
            
            status = sqlite3_step(self.sqlStament);
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                [array removeAllObjects];
                goto RE_ACCESS_DB;
            }
        }
    }
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    return array;

}
-(NSMutableArray *) loadAllEngineeringHistoryByUserId:(NSInteger)userId AndRoomId:(NSString*)roomNumber{
    
    BOOL isEnglish = [[LanguageManagerV2 sharedLanguageManager] currentLanguageIsEqualTo:@"%@", ENGLISH_LANGUAGE];
    NSString *eni_Name = nil;
    if(isEnglish) {
        eni_Name = [NSString stringWithFormat:@"%@", eni_name];
    } else {
        eni_Name = [NSString stringWithFormat:@"%@", eni_name_lang];
    }
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT a.%@, a.%@, a.%@, a.%@, a.%@, b.%@ FROM %@ a JOIN %@ b ON a.%@ = b.%@  WHERE %@ = ? AND %@ = ? ORDER BY b.%@ ASC",
                           EH_id,
                           EH_user_id,
                           EH_room_id,
                           EH_item_id,
                           EH_date,
                           eni_Name,
                           ENGINEERING_HISTORY,
                           engineering_items,
                           EH_item_id,
                           eni_id,
                           EH_user_id,
                           EH_room_id,
                           eni_name
                           ];
    
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    reAccessCount ++;
}
    
    NSMutableArray *array = [NSMutableArray array];
    NSInteger status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        
        sqlite3_bind_int (self.sqlStament, 1, (int)userId);
        sqlite3_bind_text(self.sqlStament, 2 , [roomNumber UTF8String], -1,SQLITE_TRANSIENT);
        
        status = sqlite3_step(self.sqlStament);
        while(status == SQLITE_ROW)
        {
            EngineeringHistoryModel *model = [[EngineeringHistoryModel alloc] init];
            model.eh_id = sqlite3_column_int(sqlStament, 0);
            model.eh_user_id = sqlite3_column_int(sqlStament, 1);
            if (sqlite3_column_text(self.sqlStament, 2)) {
                model.eh_room_id = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
            }
            model.eh_item_id = sqlite3_column_int(sqlStament, 3);
            if (sqlite3_column_text(sqlStament, 4)) {
                model.eh_date = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
            }
            if (sqlite3_column_text(sqlStament, 5)) {
                model.eh_item_name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
            }
            
            [array addObject:model];
            
            status = sqlite3_step(self.sqlStament);
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                [array removeAllObjects];
                goto RE_ACCESS_DB;
            }
        }
    }
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    return array;
}
//delete database after 24h
-(int) deleteDatabaseAfterSomeDays{
    NSInteger result = 1;
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd 00:00:00"];
    int countDate = (int)[[UserManagerV2 sharedUserManager].currentUserAccessRight countPostingHistoryDays];
    NSDate *dateToDelete = [[NSDate date] dateBySubtractingDays:countDate - 1];
    NSString *beforeDays = [dateFormat stringFromDate:dateToDelete];
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"DELETE FROM %@ WHERE %@ < '%@'", ENGINEERING_HISTORY, EH_date, beforeDays];
    const char *sql = [sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
        result = 0;
	
    NSInteger status = sqlite3_step(sqlStament);
    
	if (SQLITE_DONE != status) {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
		result = 0;
    }
    
    return (int)result;
}
@end
