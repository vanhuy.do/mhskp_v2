//
//  GuideItemAdapterV2.h
//  mHouseKeeping
//
//  Created by TMS on 3/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DatabaseAdapterV2.h"
#import "DbDefinesV2.h"
#import "GuideItemsV2.h"
#import "GuideItemDetail.h"
#import "GuideAdapterV2Demo.h"

@interface GuideItemAdapterV2 : DatabaseAdapterV2 {
    
}

+ (GuideItemAdapterV2*) createGuideItemAdapter;

//MARK: Guide Item
-(int) insertGuideItem:(GuideItemsV2 *) image;
-(int) updateGuideItem:(GuideItemsV2 *) image;
-(int) deleteGuideItem:(GuideItemsV2 *) image;
-(GuideItemsV2 *) loadGuideItem:(GuideItemsV2 *) image;
-(NSMutableArray *) loadAllGuideItem;
-(NSString *) getLatestLastModifiedGuideItem;

//MARK: Guide ItemDetail
-(int) insertGuideItemDetail:(GuideItemDetail*) model;
-(int) deleteGuideItemDetailsByGuideItemId:(int)guideItemId;
-(NSMutableArray*) loadGuideItemDetailByGuideItemId:(int)guideItemId;

@end
