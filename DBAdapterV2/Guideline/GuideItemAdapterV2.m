//
//  GuideItemAdapterV2.m
//  mHouseKeeping
//
//  Created by TMS on 3/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GuideItemAdapterV2.h"
#import "LogFileManager.h"
#import "GuideItemAdapterV2Demo.h"

@implementation GuideItemAdapterV2

+ (GuideItemAdapterV2*) createGuideItemAdapter
{
    GuideItemAdapterV2* sharedAdapter;
    if(isDemoMode) {
        sharedAdapter = [[GuideItemAdapterV2Demo alloc] init];
    } else {
        sharedAdapter = [[GuideItemAdapterV2 alloc] init];
    }
    
    return sharedAdapter;
}

//MARK: Guide Item
-(int) insertGuideItem:(GuideItemsV2 *) image {
    NSString *sqlString = [[NSString alloc] initWithFormat:@"INSERT INTO %@ (%@, %@, %@, %@, %@, %@, %@, %@) %@", GUIDE_ITEMS, gitem_id, guide_category_id, gitem_name, gitem_name_lang, gitem_content, gitem_content_lang, gitem_image, gitem_last_modified, @"VALUES(?, ?, ?, ?, ?, ?, ?, ?)"];
    
    const char *sql=[sqlString UTF8String];
    
    
    if(sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
        
    }
    
    sqlite3_bind_int (self.sqlStament, 1, image.gitem_id);
    sqlite3_bind_int (self.sqlStament, 2, image.guide_CategoryId);
    sqlite3_bind_text(self.sqlStament, 3, [image.gitem_Name UTF8String], 
                      -1,SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 4, [image.gitem_NameLang UTF8String], 
                      -1,SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 5, [image.gitem_content UTF8String], 
                      -1,SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 6, [image.gitem_content_lang UTF8String], 
                      -1,SQLITE_TRANSIENT);
    sqlite3_bind_blob(self.sqlStament, 7, [image.gitem_Image bytes], (int)[image.gitem_Image length], NULL);
    
    sqlite3_bind_text(sqlStament, 8, [image.gitem_last_modified UTF8String], -1, SQLITE_TRANSIENT);
    
    NSInteger temp = sqlite3_step(self.sqlStament);
    
    NSInteger returnInt = 0;
    
    if(SQLITE_DONE != temp) {        
        return (int)returnInt;
    } else {
        sqlite3_last_insert_rowid(self.database); 
        sqlite3_reset(self.sqlStament);
        
        return image.gitem_id;
    }    
}

-(int) updateGuideItem:(GuideItemsV2 *) image {
    NSString *sqlString = [[NSString alloc] initWithFormat:@"UPDATE %@ SET %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ? WHERE %@ = ?", GUIDE_ITEMS, gitem_id, guide_category_id, gitem_name, gitem_name_lang, gitem_content, gitem_content_lang, gitem_image, gitem_last_modified, gitem_id];
    
    const char *sql = [sqlString UTF8String];
    
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
        
    }
    
    sqlite3_bind_int (self.sqlStament, 1, image.gitem_id);
    sqlite3_bind_int (self.sqlStament, 2, image.guide_CategoryId);
    sqlite3_bind_text(self.sqlStament, 3, [image.gitem_Name UTF8String], 
                      -1,SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 4, [image.gitem_NameLang UTF8String], 
                      -1,SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 5, [image.gitem_content UTF8String], 
                      -1,SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 6, [image.gitem_content_lang UTF8String], 
                      -1,SQLITE_TRANSIENT);
    sqlite3_bind_blob(self.sqlStament, 7, [image.gitem_Image bytes], (int)[image.gitem_Image length], NULL);
    sqlite3_bind_text(sqlStament, 8, [image.gitem_last_modified UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_int (self.sqlStament, 9, image.gitem_id);
    
    NSInteger returnInt = 0;
    
    if(SQLITE_DONE != sqlite3_step(self.sqlStament)) {        
        return (int)returnInt;
    } else { 
        returnInt = 1;
        
        return (int)returnInt;
    }
}

-(int) deleteGuideItem:(GuideItemsV2 *) image {
    NSInteger result = 1;
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"DELETE FROM %@ WHERE %@ = ?", GUIDE_ITEMS, gitem_id];
    
    const char *sql = [sqlString UTF8String];
    
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
        result = 0;
	
    sqlite3_bind_int(self.sqlStament, 1, image.gitem_id);
	
	if (SQLITE_DONE != sqlite3_step(self.sqlStament)) 
		result = 0;
    
    return (int)result;
}

-(GuideItemsV2 *) loadGuideItem:(GuideItemsV2 *) image {
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ?", gitem_id, guide_category_id, gitem_name, gitem_name_lang, gitem_content, gitem_content_lang, gitem_image, gitem_last_modified, GUIDE_ITEMS, gitem_id];
    
    const char *sql =[sqlString UTF8String];
    
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {        
        sqlite3_bind_int(self.sqlStament, 1, image.gitem_id);
        
        while(sqlite3_step(sqlStament) == SQLITE_ROW) {
            image.gitem_id = sqlite3_column_int(self.sqlStament, 0);
            image.guide_CategoryId = sqlite3_column_int(self.sqlStament, 1);
            if (sqlite3_column_text(self.sqlStament, 2)) {
                image.gitem_Name=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
            }
            if (sqlite3_column_text(self.sqlStament, 3)) {
                image.gitem_NameLang=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
            }if (sqlite3_column_text(self.sqlStament, 4)) {
                image.gitem_content=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
            }
            if (sqlite3_column_text(self.sqlStament, 5)) {
                image.gitem_content_lang=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
            }
            
            if (sqlite3_column_bytes(self.sqlStament, 6) > 0) {
                image.gitem_Image = [NSData dataWithBytes:sqlite3_column_blob
                                  (self.sqlStament, 6) length:sqlite3_column_bytes(self.sqlStament, 6)];
            }       
            if (sqlite3_column_text(sqlStament, 7)) {
                image.gitem_last_modified = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 7)];
            }
        }
    }
    
    return image;
}


-(NSMutableArray *) loadAllGuideItem {
    NSMutableArray *array = [NSMutableArray array];
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@, %@ FROM %@", gitem_id, guide_category_id, gitem_name, gitem_name_lang, gitem_content, gitem_content_lang, gitem_image, gitem_last_modified, GUIDE_ITEMS];
    
    const char *sql =[sqlString UTF8String];
    
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {
        
        while(sqlite3_step(sqlStament) == SQLITE_ROW) {
            GuideItemsV2 *image = [[GuideItemsV2 alloc] init];
            image.gitem_id = sqlite3_column_int(self.sqlStament, 0);
            image.guide_CategoryId = sqlite3_column_int(self.sqlStament, 1);
            if (sqlite3_column_text(self.sqlStament, 2)) {
                image.gitem_Name=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
            }
            if (sqlite3_column_text(self.sqlStament, 3)) {
                image.gitem_NameLang=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
            }
            if (sqlite3_column_text(self.sqlStament, 4)) {
                image.gitem_content=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
            }
            if (sqlite3_column_text(self.sqlStament, 5)) {
                image.gitem_content_lang=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
            }
            if (sqlite3_column_bytes(self.sqlStament, 6) > 0) {
                image.gitem_Image = [NSData dataWithBytes:sqlite3_column_blob
                                  (self.sqlStament, 6) length:sqlite3_column_bytes(self.sqlStament, 6)];
            } 
            if (sqlite3_column_text(sqlStament, 7)) {
                image.gitem_last_modified = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 7)];
            }
            
            [array addObject:image];
            
        }
    }    
    
    return array;
}

-(NSMutableArray *) loadAllGuideItemByGuideId:(int) guideId {
    NSMutableArray *array = [NSMutableArray array];
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ?", gitem_id, guide_category_id, gitem_name, gitem_name_lang, gitem_content, gitem_content_lang, gitem_image, gitem_last_modified, GUIDE_ITEMS, guide_category_id];
    
    const char *sql =[sqlString UTF8String];
    
    if([LogFileManager isLogConsole])
    {
        NSLog(@"sql string %s", sql);
    }
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {
        sqlite3_bind_int(self.sqlStament, 1, guideId);
        
        while(sqlite3_step(sqlStament) == SQLITE_ROW) {
            GuideItemsV2 *image = [[GuideItemsV2 alloc] init];
            image.gitem_id = sqlite3_column_int(self.sqlStament, 0);
            image.guide_CategoryId = sqlite3_column_int(self.sqlStament, 1);
            if (sqlite3_column_text(self.sqlStament, 2)) {
                image.gitem_Name=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
            }
            if (sqlite3_column_text(self.sqlStament, 3)) {
                image.gitem_NameLang=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
            }
            if (sqlite3_column_text(self.sqlStament, 4)) {
                image.gitem_content=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
            }
            if (sqlite3_column_text(self.sqlStament, 5)) {
                image.gitem_content_lang=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
            }
            if (sqlite3_column_bytes(self.sqlStament, 6) > 0) {
                image.gitem_Image = [NSData dataWithBytes:sqlite3_column_blob
                                  (self.sqlStament, 6) length:sqlite3_column_bytes(self.sqlStament, 6)];
            } 
            if (sqlite3_column_text(sqlStament, 7)) {
                image.gitem_last_modified = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 7)];
            }
            
            [array addObject:image];
            
        }
    }    
    
    return array;
}
/*
-(NSMutableArray *)loadAllGuideItemByRoomType:(int) roomType {
    NSMutableArray *array = [NSMutableArray array];
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ?", gitem_id, guide_id, gitem_name, gitem_name_lang, gitem_content, gitem_content_lang, gitem_image, GUIDE_ITEMS, guide_id];
    
    const char *sql =[sqlString UTF8String];
    [sqlString release];
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {
        sqlite3_bind_int(self.sqlStament, 1, guideId);
        
        while(sqlite3_step(sqlStament) == SQLITE_ROW) {
            GuideItemsV2 *image = [[GuideItemsV2 alloc] init];
            image.gitem_id = sqlite3_column_int(self.sqlStament, 0);
            image.guide_id = sqlite3_column_int(self.sqlStament, 1);
            if (sqlite3_column_text(self.sqlStament, 2)) {
                image.gitem_Name=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
            }
            if (sqlite3_column_text(self.sqlStament, 3)) {
                image.gitem_NameLang=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
            }
            if (sqlite3_column_text(self.sqlStament, 4)) {
                image.gitem_content=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
            }
            if (sqlite3_column_text(self.sqlStament, 5)) {
                image.gitem_content_lang=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
            }
            if (sqlite3_column_bytes(self.sqlStament, 6) > 0) {
                image.gitem_Image = [NSData dataWithBytes:sqlite3_column_blob
                                     (self.sqlStament, 6) length:sqlite3_column_bytes(self.sqlStament, 6)];
            } 
            
            [array addObject:image];
            [image release];
        }
    }    
    
    return array;
}*/

//get latest last modified of guide item table
-(NSString *)getLatestLastModifiedGuideItem {
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@ FROM %@ ORDER BY %@ DESC LIMIT 1", gitem_last_modified, GUIDE_ITEMS, gitem_last_modified];
    
    const char *sql =[sqlString UTF8String];
    
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {
        
        while(sqlite3_step(sqlStament) == SQLITE_ROW) {
            
            if (sqlite3_column_text(sqlStament, 0)) {
                return [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 0)];
            }
            
        }
    }    
    
    return @"";
}

//MARK: Guide Item Detail
-(int) insertGuideItemDetail:(GuideItemDetail*) model
{
    NSString *sqlString = [[NSString alloc] initWithFormat:@"INSERT INTO %@ (%@, %@) %@", GUIDE_ITEM_DETAIL, gitem_id, gitem_image, @"VALUES(?, ?)"];
    
    const char *sql=[sqlString UTF8String];
    
    
    if(sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
        
    }
    
    sqlite3_bind_int (self.sqlStament, 1, model.gitem_id);
    sqlite3_bind_blob(self.sqlStament, 2, [model.gitem_image bytes], (int)[model.gitem_image length], NULL);
    
    NSInteger temp = sqlite3_step(self.sqlStament);
    
    NSInteger returnInt = 0;
    
    if(SQLITE_DONE != temp) {
        return (int)returnInt;
    } else {
        sqlite3_last_insert_rowid(self.database);
        sqlite3_reset(self.sqlStament);
        
        return model.gitem_id;
    }
}

-(int) deleteGuideItemDetailsByGuideItemId:(int)guideItemId
{
    NSInteger result = 1;
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"DELETE FROM %@ WHERE %@ = ?", GUIDE_ITEM_DETAIL, gitem_id];
    const char *sql = [sqlString UTF8String];
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
        result = 0;
    sqlite3_bind_int(self.sqlStament, 1, guideItemId);
    if (SQLITE_DONE != sqlite3_step(self.sqlStament))
        result = 0;
    return (int)result;
}

-(NSMutableArray*) loadGuideItemDetailByGuideItemId:(int)guideItemId
{
    NSMutableArray *array = [NSMutableArray array];
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@ FROM %@ WHERE %@ = ?", gitem_id, gitem_image, GUIDE_ITEM_DETAIL, gitem_id];
    
    const char *sql =[sqlString UTF8String];
        if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {
        sqlite3_bind_int(self.sqlStament, 1, guideItemId);
        
        while(sqlite3_step(sqlStament) == SQLITE_ROW) {
            GuideItemDetail *image = [[GuideItemDetail alloc] init];
            image.gitem_id = sqlite3_column_int(self.sqlStament, 0);
            if (sqlite3_column_bytes(self.sqlStament, 1) > 0) {
                image.gitem_image = [NSData dataWithBytes:sqlite3_column_blob
                                     (self.sqlStament, 1) length:sqlite3_column_bytes(self.sqlStament, 1)];
            }
            [array addObject:image];
        }
    }
    
    return array;
}


@end
