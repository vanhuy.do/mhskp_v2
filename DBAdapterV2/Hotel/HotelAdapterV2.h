//
//  HotelAdapterV2.h
//  mHouseKeeping
//
//  Created by TMS TRIBE on 3/20/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DatabaseAdapterV2.h"
#import "HotelModelV2.h"
#import "DbDefinesV2.h"

@interface HotelAdapterV2 : DatabaseAdapterV2{
    
}

-(int) insertHotelData:(HotelModelV2 *) hotel;
-(int) updateHotelData:(HotelModelV2 *) hotel;
-(HotelModelV2 *) loadHotelData:(HotelModelV2 *) hotel;

@end
