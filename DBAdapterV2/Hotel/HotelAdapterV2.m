//
//  HotelAdapterV2.m
//  mHouseKeeping
//
//  Created by TMS TRIBE on 3/20/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "HotelAdapterV2.h"
#import "ehkDefines.h"

@implementation HotelAdapterV2

-(int) insertHotelData:(HotelModelV2 *) hotel{
        NSString *sqlString = [[NSString alloc] initWithFormat:@"INSERT INTO %@ (%@, %@, %@, %@, %@) %@",
                               HOTELS,
                               
                               hotel_id,
                               hotel_lang, 
                               hotel_name_lang, 
                               hotel_logo, 
                               hotel_last_modified, 
                               
                               @"VALUES(?,?,?,?,?)"];
        const char *sql=[sqlString UTF8String];
        NSInteger reAccessCount = 0;
        RE_ACCESS_DB:{
            if (reAccessCount > 0) {
                [self close];
                usleep(0.1);
                [self openDatabase];
            }

            reAccessCount++;
        }

		if(sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
        }
        
        sqlite3_bind_int (self.sqlStament, 1, (int)hotel.hotel_id);
        sqlite3_bind_text(self.sqlStament, 2, [hotel.hotel_lang UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(self.sqlStament, 3, [hotel.hotel_name_lang UTF8String], -1, SQLITE_TRANSIENT); 

        int returnValue = -1;
        if(hotel.hotel_logo != nil) 
            returnValue = sqlite3_bind_blob(self.sqlStament, 4, [hotel.hotel_logo bytes], (int)[hotel.hotel_logo length], NULL);
        else
            returnValue = sqlite3_bind_blob(self.sqlStament, 4, nil, -1, NULL);
       
        sqlite3_bind_text(self.sqlStament, 5, [hotel.hotel_last_modified UTF8String], -1, SQLITE_TRANSIENT);
    
    
    NSInteger returnInt = 0;
    
    NSInteger status = sqlite3_step(self.sqlStament);
    
    if(SQLITE_DONE != status) {  
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return (int)returnInt;
    } else {
        sqlite3_reset(self.sqlStament);
        returnInt = 1;
        return (int)returnInt;
    }


}

-(int) updateHotelData:(HotelModelV2 *) hotel{

        NSString *sqlString = [[NSString alloc] initWithFormat:@"UPDATE %@ SET %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ? WHERE %@ = %d",HOTELS, hotel_id,hotel_lang, hotel_name_lang, hotel_logo, hotel_last_modified, hotel_id, (int)hotel.hotel_id];
        const char *sql = [sqlString UTF8String];
        NSInteger reAccessCount = 0;
        RE_ACCESS_DB:{
            reAccessCount++;
        }

		if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
        {
        }
        
        sqlite3_bind_int(self.sqlStament, 1, (int)hotel.hotel_id);
        sqlite3_bind_text(self.sqlStament, 2, [hotel.hotel_lang UTF8String], -1, SQLITE_TRANSIENT);
        
        int returnValue = -1;
        if(hotel.hotel_logo != nil) 
            returnValue = sqlite3_bind_blob(self.sqlStament, 3, [hotel.hotel_logo bytes], (int)[hotel.hotel_logo length], NULL);
        else
            returnValue = sqlite3_bind_blob(self.sqlStament, 3, nil, -1, NULL);
        
        //sqlite3_bind_text(self.sqlStament, 3, [hotelModel.hotel_logo UTF8String], -1,SQLITE_TRANSIENT);
        sqlite3_bind_text(self.sqlStament, 4, [hotel.hotel_lang UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(self.sqlStament, 5, [hotel.hotel_last_modified UTF8String], -1, SQLITE_TRANSIENT);
    
    NSInteger returnInt = 0;    
    NSInteger status = sqlite3_step(self.sqlStament);
    if(SQLITE_DONE != status) {   
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return (int)returnInt;
    } else { 
        returnInt = 1;        
        return (int)returnInt;
    }
}



-(HotelModelV2 *) loadHotelData:(HotelModelV2 *) hotel{
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ?",
                           hotel_id, 
                           hotel_lang,
                           hotel_name_lang, 
                           hotel_logo, 
                           hotel_last_modified, 
                           
                           HOTELS, 
                           hotel_id];
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
        RE_ACCESS_DB:{
            if (reAccessCount > 0) {
                [self close];
                usleep(0.1);
                [self openDatabase];
            }

            reAccessCount++;
        }

    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {}
    
    sqlite3_bind_int(self.sqlStament, 1, (int)hotel.hotel_id);
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW)
    {
        if (sqlite3_column_text(sqlStament, 1)){
            hotel.hotel_lang = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
        }
        if (sqlite3_column_text(sqlStament, 2)){
            hotel.hotel_name_lang = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
        }
        
        
        if (sqlite3_column_bytes(sqlStament, 3)>0) {
            NSData* hotelLogo = [[NSData alloc] initWithBytes:sqlite3_column_blob(sqlStament, 3) length:sqlite3_column_bytes(sqlStament, 3)];
            
            hotel.hotel_logo = hotelLogo;
            
        }
        if (sqlite3_column_text(sqlStament, 4)) {
            hotel.hotel_last_modified = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
        }
        
        status = sqlite3_step(sqlStament);
    }
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }

    

    return hotel;

}

@end
