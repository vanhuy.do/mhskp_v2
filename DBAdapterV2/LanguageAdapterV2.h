//
//  LanguageAdapterV2.h
//  mHouseKeeping
//
//  Created by TMS on 3/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DatabaseAdapterV2.h"
#import "DbDefinesV2.h"
#import "LanguageModelV2.h"

@interface LanguageAdapterV2 : DatabaseAdapterV2 {
    
}
-(NSString*) insertLanguageModel:(LanguageModelV2 *)languageModel;
-(int) updatedLanguageModel:(LanguageModelV2 *)languageModel;
-(LanguageModelV2 *) loadLanguageModel:(LanguageModelV2 *)languageModel;
-(NSMutableArray *)loadAllLanguageModel;
-(void) loadLanguageModelByLanguageName:(LanguageModelV2 *) languageModel;

@end
