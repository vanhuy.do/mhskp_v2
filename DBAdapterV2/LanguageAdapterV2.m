//
//  LanguageAdapterV2.m
//  mHouseKeeping
//
//  Created by TMS on 3/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LanguageAdapterV2.h"
#import "ehkDefines.h"
#import "LogFileManager.h"

@implementation LanguageAdapterV2
-(NSString*) insertLanguageModel:(LanguageModelV2 *)languageModel
{
    NSString *sqlString = [[NSString alloc] initWithFormat:@"INSERT INTO %@ (%@, %@, %@, %@, %@, %@, %@) %@",LANGUAGE_REFERENCES,lang_id, lang_name, lang_pack, lang_active, lang_last_modified, lang_currency, lang_decimal_place,@"VALUES(?,?,?,?,?,?,?)"];
    const char *sql=[sqlString UTF8String];

    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount++;
    }
    
    if(sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
    {
        if([LogFileManager isLogConsole])
        {
            NSLog(@"Error while creating insert statement to langugage. '%s'", sqlite3_errmsg(database));
        }
    }

    sqlite3_bind_text(self.sqlStament, 1, [languageModel.lang_id UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 2, [languageModel.lang_name UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 3, [languageModel.lang_pack UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 4, [languageModel.lang_active UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 5, [languageModel.lang_last_modified UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 6, [languageModel.lang_currency UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 7, [languageModel.lang_decimal_place UTF8String], -1, SQLITE_TRANSIENT);
    
    NSString *returnInt = nil;
    NSInteger sqliteCode = sqlite3_step(sqlStament);
    if(sqliteCode != SQLITE_DONE){
        if (sqliteCode == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return returnInt;
    }
    else
    {
        sqlite3_last_insert_rowid(self.database);
        sqlite3_reset(self.sqlStament);
        return languageModel.lang_id;
    }
}

-(int) updatedLanguageModel:(LanguageModelV2 *)languageModel
{
    NSString *sqlString = [[NSString alloc] initWithFormat:@"UPDATE %@ SET %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ? WHERE %@ = \"%@\"", LANGUAGE_REFERENCES, lang_name, lang_pack, lang_active, lang_last_modified, lang_currency, lang_decimal_place, lang_id, languageModel.lang_id];
    const char *sql = [sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount++;
    }
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
    {
    }
    
    sqlite3_bind_text(self.sqlStament, 1, [languageModel.lang_name UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 2, [languageModel.lang_pack UTF8String], -1,SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 3, [languageModel.lang_active UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 4, [languageModel.lang_last_modified UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 5, [languageModel.lang_currency UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_int(sqlStament, 6, [languageModel.lang_decimal_place intValue]);
        
    
    NSInteger returnInt = 0;
    
    NSInteger result = sqlite3_step(sqlStament);
    if(SQLITE_DONE != result)
    {
        if (result == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        
        sqlite3_reset(self.sqlStament);
        
        return (int)returnInt;
    }
    else { 
        return 1;
    }
}

-(LanguageModelV2 *) loadLanguageModel:(LanguageModelV2 *)languageModel
{
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ?",lang_id, lang_name, lang_pack, lang_active, lang_last_modified, lang_currency, lang_decimal_place, LANGUAGE_REFERENCES, lang_id];
    const char *sql =[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount++;
    }
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {
        
        sqlite3_bind_text(self.sqlStament, 1, [languageModel.lang_id UTF8String], -1, SQLITE_TRANSIENT);
        
        NSInteger status = sqlite3_step(sqlStament);
        
        while(status == SQLITE_ROW)
        {
            languageModel.lang_name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
            if (sqlite3_column_text(sqlStament, 2)) {
                languageModel.lang_pack = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
            }
            if (sqlite3_column_text(sqlStament, 3)){
                languageModel.lang_active = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
            }
            if (sqlite3_column_text(self.sqlStament, 4)) {
                languageModel.lang_last_modified = [NSString stringWithUTF8String:(char *)sqlite3_column_text(self.sqlStament, 4)];
            }
            if (sqlite3_column_text(self.sqlStament, 5)) {
                languageModel.lang_currency = [NSString stringWithUTF8String:(char *) sqlite3_column_text(self.sqlStament, 5)];
            }
            if (sqlite3_column_text(self.sqlStament, 6)) {
                languageModel.lang_decimal_place = [NSString stringWithUTF8String:(char *)sqlite3_column_text(self.sqlStament, 6)];
            }
            
            status = sqlite3_step(sqlStament);
            
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }
    
    return languageModel;
    
}

-(void)loadLanguageModelByLanguageName:(LanguageModelV2 *)languageModel {
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ?",lang_id, lang_name, lang_pack, lang_active, lang_last_modified, lang_currency, lang_decimal_place, LANGUAGE_REFERENCES, lang_name];
    const char *sql =[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount++;
    }
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {
        
        sqlite3_bind_text(self.sqlStament, 1, [languageModel.lang_name UTF8String], -1, SQLITE_TRANSIENT);
        
        NSInteger status = sqlite3_step(sqlStament);
        while(status == SQLITE_ROW)
        {
            languageModel.lang_id = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sqlStament, 0)];
            
            
            if (sqlite3_column_text(sqlStament, 2)) {
                languageModel.lang_pack = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
            }
            if (sqlite3_column_text(sqlStament, 3)){
                languageModel.lang_active = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
            }
            if (sqlite3_column_text(self.sqlStament, 4)) {
                languageModel.lang_last_modified = [NSString stringWithUTF8String:(char *)sqlite3_column_text(self.sqlStament, 4)];
            }
            if (sqlite3_column_text(self.sqlStament, 5)) {
                languageModel.lang_currency = [NSString stringWithUTF8String:(char *) sqlite3_column_text(self.sqlStament, 5)];
            }
            if (sqlite3_column_text(self.sqlStament, 6)) {
                languageModel.lang_decimal_place = [NSString stringWithUTF8String:(char *)sqlite3_column_text(self.sqlStament, 6)];
            }
            
            status = sqlite3_step(sqlStament);
        }
        
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    }
}

-(NSMutableArray *)loadAllLanguageModel;
{
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT * FROM %@ WHERE %@ = %d",LANGUAGE_REFERENCES, lang_active, 1];
    //    NSLog(@"sql = %@", sqlString);
    const char *sql =[sqlString UTF8String];
    
    NSMutableArray *result = [NSMutableArray array];

    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount++;
    }
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {      
        NSInteger status = sqlite3_step(sqlStament);
        while(status == SQLITE_ROW)
        {
            LanguageModelV2* languageModel = [[LanguageModelV2 alloc] init];
            
            languageModel.lang_id   = [NSString stringWithUTF8String:(char*) sqlite3_column_text(sqlStament, 0)];
            languageModel.lang_name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
            if (sqlite3_column_text(sqlStament, 2)) {
                languageModel.lang_pack = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
            }
            if (sqlite3_column_text(sqlStament, 3)){
                languageModel.lang_active = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
            }
            if (sqlite3_column_text(sqlStament, 4)) {
                languageModel.lang_last_modified = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
            }
            if (sqlite3_column_text(self.sqlStament, 5)) {
                languageModel.lang_currency = [NSString stringWithUTF8String:(char *) sqlite3_column_text(self.sqlStament, 5)];
            }
            if (sqlite3_column_text(self.sqlStament, 6)) {
                languageModel.lang_decimal_place = [NSString stringWithUTF8String:(char *)sqlite3_column_text(self.sqlStament, 6)];
            }
            [result addObject:languageModel];   
            
            status = sqlite3_step(sqlStament);
           
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                [result removeAllObjects];
                goto RE_ACCESS_DB;
            }
        }
    }
    
    return result;
    
}
  
@end
