//
//  LocationAdapterV2.h
//  mHouseKeeping
//
//  Created by Nghia Truong on 3/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DatabaseAdapterV2.h"
#import "DbDefinesV2.h"
#import "LocationModelV2.h"

@interface LocationAdapterV2 : DatabaseAdapterV2 {
    
}

-(int) insertLocationData:(LocationModelV2 *) location;
-(int) updateLocationData:(LocationModelV2 *) location;
-(int) deleteLocationData:(LocationModelV2 *) location;
-(LocationModelV2 *) loadLocationData:(LocationModelV2 *) location;
-(LocationModelV2 *) loadLocationDataByHotelId:(LocationModelV2 *) location;
-(NSMutableArray *) loadAllLocationByHotelId:(int) hotelId;
-(NSMutableArray *) loadAllLocation;

@end
