//
//  LocationAdapterV2.m
//  mHouseKeeping
//
//  Created by Nghia Truong on 3/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LocationAdapterV2.h"

@implementation LocationAdapterV2

-(int)insertLocationData:(LocationModelV2 *)location {
    NSString *sqlString = [[NSString alloc] initWithFormat:@"INSERT INTO %@ (%@, %@, %@, %@, %@, %@, %@, %@) %@", LOCATION, location_id, location_hotel_id, location_code, location_desc, location_lang, location_roomNo, location_type, location_lastModified, @"VALUES(?, ?, ?, ?, ?, ?, ?, ?)"];
    
    const char *sql=[sqlString UTF8String];
//    [sqlString release];
    
    if(sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
        
    }
    
    sqlite3_bind_int (self.sqlStament, 1, location.location_Id);
    sqlite3_bind_int (self.sqlStament, 2, location.location_HotelId);
    sqlite3_bind_text(self.sqlStament, 3, [location.location_Code UTF8String], 
                      -1,SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 4, [location.location_Desc UTF8String], 
                      -1,SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 5, [location.location_Lang UTF8String], 
                      -1,SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 6, [location.location_RoomNo UTF8String], 
                      -1,SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 7, [location.location_Type UTF8String], 
                      -1,SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 8, [location.location_LastModified UTF8String], 
                      -1,SQLITE_TRANSIENT);    
    
    NSInteger temp = sqlite3_step(self.sqlStament);
    
    NSInteger returnInt = 0;
    
    if(SQLITE_DONE != temp) {        
        return (int)returnInt;
    } else {
        sqlite3_last_insert_rowid(self.database); 
        sqlite3_reset(self.sqlStament);
        
        return location.location_Id;
    }    
}

-(int)updateLocationData:(LocationModelV2 *)location {
    NSString *sqlString = [[NSString alloc] initWithFormat:@"UPDATE %@ SET %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ? WHERE %@ = ?", LOCATION, location_id, location_hotel_id, location_code, location_desc, location_lang, location_roomNo, location_type, location_lastModified, location_id];
    
    const char *sql = [sqlString UTF8String];
//    [sqlString release];
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
        
    }
    
    sqlite3_bind_int (self.sqlStament, 1, location.location_Id);
    sqlite3_bind_int (self.sqlStament, 2, location.location_HotelId);
    sqlite3_bind_text(self.sqlStament, 3, [location.location_Code UTF8String], 
                      -1,SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 4, [location.location_Desc UTF8String], 
                      -1,SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 5, [location.location_Lang UTF8String], 
                      -1,SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 6, [location.location_RoomNo UTF8String], 
                      -1,SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 7, [location.location_Type UTF8String], 
                      -1,SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 8, [location.location_LastModified UTF8String], 
                      -1,SQLITE_TRANSIENT); 
    sqlite3_bind_int (self.sqlStament, 9, location.location_Id);
    
    NSInteger returnInt = 0;
    
    if(SQLITE_DONE != sqlite3_step(self.sqlStament)) {        
        return (int)returnInt;
    } else { 
        returnInt = 1;
        
        return (int)returnInt;
    }
}

-(int)deleteLocationData:(LocationModelV2 *)location {
    NSInteger result = 1;
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"DELETE FROM %@ WHERE %@ = ?", LOCATION, location_id];
    
    const char *sql = [sqlString UTF8String];
//    [sqlString release];
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
        result = 0;
	
    sqlite3_bind_int(self.sqlStament, 1, location.location_Id);
	
	if (SQLITE_DONE != sqlite3_step(self.sqlStament)) 
		result = 0;
    
    return (int)result;
}

-(LocationModelV2 *)loadLocationData:(LocationModelV2 *)location {
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ?", location_id, location_hotel_id, location_code, location_desc, location_lang, location_roomNo, location_type, location_lastModified, LOCATION, location_id];
    
    const char *sql =[sqlString UTF8String];
//    [sqlString release];
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {        
        sqlite3_bind_int(self.sqlStament, 1, location.location_Id);
        
        while(sqlite3_step(sqlStament) == SQLITE_ROW) {
            location.location_Id = sqlite3_column_int(self.sqlStament, 0);
            location.location_HotelId = sqlite3_column_int(self.sqlStament, 1);
            if (sqlite3_column_text(self.sqlStament, 2)) {
                location.location_Code = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
            }
            if (sqlite3_column_text(self.sqlStament, 3)) {
                location.location_Desc = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
            }
            if (sqlite3_column_text(self.sqlStament, 4)) {
                location.location_Lang = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
            }
            if (sqlite3_column_text(self.sqlStament, 5)) {
                location.location_RoomNo = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
            }
            if (sqlite3_column_text(self.sqlStament, 6)) {
                location.location_Type = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 6)];
            }
            if (sqlite3_column_text(self.sqlStament, 7)) {
                location.location_LastModified = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 7)];
            }
        }
    }
    
    return location;
}

-(LocationModelV2 *)loadLocationDataByHotelId:(LocationModelV2 *)location {
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ?", location_id, location_hotel_id, location_code, location_desc, location_lang, location_roomNo, location_type, location_lastModified, LOCATION, location_hotel_id];
    
    const char *sql =[sqlString UTF8String];
//    [sqlString release];
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {        
        sqlite3_bind_int(self.sqlStament, 1, location.location_HotelId);
        
        while(sqlite3_step(sqlStament) == SQLITE_ROW) {
            location.location_Id = sqlite3_column_int(self.sqlStament, 0);
            location.location_HotelId = sqlite3_column_int(self.sqlStament, 1);
            if (sqlite3_column_text(self.sqlStament, 2)) {
                location.location_Code = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
            }
            if (sqlite3_column_text(self.sqlStament, 3)) {
                location.location_Desc = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
            }
            if (sqlite3_column_text(self.sqlStament, 4)) {
                location.location_Lang = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
            }
            if (sqlite3_column_text(self.sqlStament, 5)) {
                location.location_RoomNo = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
            }
            if (sqlite3_column_text(self.sqlStament, 6)) {
                location.location_Type = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 6)];
            }
            if (sqlite3_column_text(self.sqlStament, 7)) {
                location.location_LastModified = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 7)];
            }
        }
    }
    
    return location;
}

-(NSMutableArray *)loadAllLocation {
    NSMutableArray *array = [NSMutableArray array];
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@, %@ FROM %@", location_id, location_hotel_id, location_code, location_desc, location_lang, location_roomNo, location_type, location_lastModified, LOCATION];
    
    const char *sql =[sqlString UTF8String];
//    [sqlString release];
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {
        
        while(sqlite3_step(sqlStament) == SQLITE_ROW) {
            LocationModelV2 *location = [[LocationModelV2 alloc] init];
            location.location_Id = sqlite3_column_int(self.sqlStament, 0);
            location.location_HotelId = sqlite3_column_int(self.sqlStament, 1);
            if (sqlite3_column_text(self.sqlStament, 2)) {
                location.location_Code = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
            }
            if (sqlite3_column_text(self.sqlStament, 3)) {
                location.location_Desc = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
            }
            if (sqlite3_column_text(self.sqlStament, 4)) {
                location.location_Lang = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
            }
            if (sqlite3_column_text(self.sqlStament, 5)) {
                location.location_RoomNo = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
            }
            if (sqlite3_column_text(self.sqlStament, 6)) {
                location.location_Type = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 6)];
            }
            if (sqlite3_column_text(self.sqlStament, 7)) {
                location.location_LastModified = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 7)];
            }
            
            [array addObject:location];
//            [location release];
        }
    }    
    
    return array;
}

-(NSMutableArray *)loadAllLocationByHotelId:(int)hotelId {
    NSMutableArray *array = [NSMutableArray array];
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ?", location_id, location_hotel_id, location_code, location_desc, location_lang, location_roomNo, location_type, location_lastModified, LOCATION, location_hotel_id];
    
    const char *sql =[sqlString UTF8String];
//    [sqlString release];
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {
        sqlite3_bind_int(self.sqlStament, 1, hotelId);
        
        while(sqlite3_step(sqlStament) == SQLITE_ROW) {
            LocationModelV2 *location = [[LocationModelV2 alloc] init];
            location.location_Id = sqlite3_column_int(self.sqlStament, 0);
            location.location_HotelId = sqlite3_column_int(self.sqlStament, 1);
            if (sqlite3_column_text(self.sqlStament, 2)) {
                location.location_Code = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
            }
            if (sqlite3_column_text(self.sqlStament, 3)) {
                location.location_Desc = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
            }
            if (sqlite3_column_text(self.sqlStament, 4)) {
                location.location_Lang = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
            }
            if (sqlite3_column_text(self.sqlStament, 5)) {
                location.location_RoomNo = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
            }
            if (sqlite3_column_text(self.sqlStament, 6)) {
                location.location_Type = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 6)];
            }
            if (sqlite3_column_text(self.sqlStament, 7)) {
                location.location_LastModified = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 7)];
            }
            
            [array addObject:location];
//            [location release];
        }
    }    
    
    return array;
}

@end
