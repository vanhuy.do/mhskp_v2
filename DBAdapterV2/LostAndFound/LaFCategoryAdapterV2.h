//
//  LaFCategoryAdapterV2.h
//  mHouseKeeping
//
//  Created by TMS on 3/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LFCategoryModelV2.h"
#import "DatabaseAdapterV2.h"
#import "DbDefinesV2.h"

@interface LaFCategoryAdapterV2 : DatabaseAdapterV2{

}

-(int)insertCategoryData:(LFCategoryModelV2*) category;
-(int)updateCategoryData:(LFCategoryModelV2*) category;
-(int)deleteCategoryData:(LFCategoryModelV2*) category;
-(LFCategoryModelV2*)loadCategoryData:(LFCategoryModelV2*)categoryData;
-(NSMutableArray *)loadAllCategoryData;
@end
