//
//  LaFCategoryAdapterV2.m
//  mHouseKeeping
//
//  Created by TMS on 3/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LaFCategoryAdapterV2.h"
#import "ehkDefines.h"

@implementation LaFCategoryAdapterV2

-(int)insertCategoryData:(LFCategoryModelV2*) category {    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"INSERT INTO %@ (%@, %@, %@, %@, %@) %@", LAF_CATIGORIES, lafc_id, lafc_name, lafc_lang_name, lafc_image, lafc_last_modified, @"VALUES(?, ?, ?, ?, ?)"];
    const char *sql = [sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount++;
    }
    
    if(sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    
    sqlite3_bind_int(self.sqlStament, 1, (int)category.lfcID);
    sqlite3_bind_text(self.sqlStament, 2, [category.lfcName UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 3, [category.lfcLang UTF8String], -1, SQLITE_TRANSIENT);
        
    if(category.lfcImage != nil) 
        sqlite3_bind_blob(self.sqlStament, 4, [category.lfcImage bytes], (int)[category.lfcImage length], NULL);
    else
        sqlite3_bind_blob(self.sqlStament, 4, nil, -1, NULL);
    
    sqlite3_bind_text(self.sqlStament, 5, [category.lfcLastModified UTF8String], -1, SQLITE_TRANSIENT);
    
    NSInteger returnInt = 0;
    
    NSInteger status = sqlite3_step(self.sqlStament);
    
    if(SQLITE_DONE != status) {  
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return (int)returnInt;
    } else {
        sqlite3_reset(self.sqlStament);
        returnInt = 1;
        return (int)returnInt;
    }
}

-(int)updateCategoryData:(LFCategoryModelV2*) category {  
    NSString *sqlString = [[NSString alloc] initWithFormat:@"UPDATE %@ SET %@ = ?, %@ = ?, %@ = ?, %@ = ? WHERE %@ = ?", LAF_CATIGORIES, lafc_name, lafc_lang_name, lafc_image, lafc_last_modified, lafc_id];    
    const char *sql = [sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount++;
    }
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
        
    }    
   
    sqlite3_bind_text(self.sqlStament, 1, [category.lfcName UTF8String], 
                      -1,SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 2, [category.lfcLang UTF8String], 
                      -1,SQLITE_TRANSIENT);
    sqlite3_bind_blob(self.sqlStament, 3, [category.lfcImage bytes], (int)[category.lfcImage length], NULL);
    sqlite3_bind_text(self.sqlStament, 4, [category.lfcLastModified UTF8String], 
                      -1,SQLITE_TRANSIENT);
    sqlite3_bind_int (self.sqlStament, 5, (int)category.lfcID);

    NSInteger returnInt = 0;    
    NSInteger status = sqlite3_step(self.sqlStament);
    if(SQLITE_DONE != status) {   
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return (int)returnInt;
    } else { 
        returnInt = 1;        
        return (int)returnInt;
    }
}

-(int)deleteCategoryData:(LFCategoryModelV2*) category { 
    NSInteger result = 1;

    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"DELETE FROM %@ WHERE %@ = ?", LAF_CATIGORIES, lafc_id];        
    const char *sql = [sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount++;
    }
		
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
        result = 0;	
	
    sqlite3_bind_int(self.sqlStament, 1, (int)category.lfcID);
	
    NSInteger status = sqlite3_step(sqlStament);
    
	if (SQLITE_DONE != status) {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
		result = 0;
    }
    
    return (int)result;
}

-(LFCategoryModelV2*)loadCategoryData:(LFCategoryModelV2*)categoryData {    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ?", lafc_id, lafc_name, lafc_lang_name, lafc_image, lafc_last_modified, LAF_CATIGORIES, lafc_id];    
    const char *sql =[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount++;
    }
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        sqlite3_bind_int(self.sqlStament, 1, (int)categoryData.lfcID);
        
        status = sqlite3_step(sqlStament);
        
        while(status == SQLITE_ROW) {
            categoryData.lfcID = sqlite3_column_int(self.sqlStament, 0);

            if (sqlite3_column_text(self.sqlStament, 1)) {
                categoryData.lfcName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
            }
            
            if (sqlite3_column_text(self.sqlStament, 2)) {
                categoryData.lfcLang = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
            }
            
            if (sqlite3_column_bytes(self.sqlStament, 3) > 0) {
               categoryData.lfcImage = [NSData dataWithBytes:sqlite3_column_blob
                                      (self.sqlStament, 3) length:sqlite3_column_bytes(self.sqlStament, 3)];
            } 
            
            if (sqlite3_column_text(self.sqlStament, 4)) {
                categoryData.lfcLastModified = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
            }
            
            status = sqlite3_step(sqlStament);
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
        
    return categoryData;
}

-(NSMutableArray *)loadAllCategoryData {
    NSMutableArray *categoryDataArray = [[NSMutableArray alloc] init];
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@ FROM %@ ", lafc_id, lafc_name, lafc_lang_name, lafc_image, lafc_last_modified, LAF_CATIGORIES];    
    const char *sql =[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount++;
    }
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
               
        status = sqlite3_step(sqlStament);
        
        while(status == SQLITE_ROW) {
            LFCategoryModelV2 *categoryData = [[LFCategoryModelV2 alloc] init];

            categoryData.lfcID = sqlite3_column_int(self.sqlStament, 0);
            
            if (sqlite3_column_text(self.sqlStament, 1)) {
            categoryData.lfcName=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
            }
            
            if (sqlite3_column_text(self.sqlStament, 2)) {
                categoryData.lfcLang=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
            }
            
            if (sqlite3_column_bytes(self.sqlStament, 3) > 0) {
                categoryData.lfcImage  = [NSData dataWithBytes:sqlite3_column_blob
                                     (self.sqlStament, 3) length:sqlite3_column_bytes(self.sqlStament, 3)];
            } 

            if (sqlite3_column_text(self.sqlStament, 4)) {
                categoryData.lfcLastModified = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
            }
            
            [categoryDataArray addObject:categoryData];
            
            status = sqlite3_step(sqlStament);
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                [categoryDataArray removeAllObjects];
                goto RE_ACCESS_DB;
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            [categoryDataArray removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    return categoryDataArray;
}
@end
