//
//  LaFColorAdapterV2.h
//  mHouseKeeping
//
//  Created by TMS on 3/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DatabaseAdapterV2.h"
#import "LFColorModelV2.h"

@interface LaFColorAdapterV2 : DatabaseAdapterV2 {
    
}

-(int)insertColorData:(LFColorModelV2*) colorData;
-(int)updateColorData:(LFColorModelV2*) colorData;
-(int)deleteColorData:(LFColorModelV2*) colorData;
-(LFColorModelV2*)loadColorData:(LFColorModelV2*) colorData;
-(NSMutableArray *)loadAllColors;
@end
