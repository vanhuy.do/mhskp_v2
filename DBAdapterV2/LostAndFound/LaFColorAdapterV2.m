//
//  LaFColorAdapterV2.m
//  mHouseKeeping
//
//  Created by TMS on 3/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LaFColorAdapterV2.h"
#import "ehkDefines.h"

@implementation LaFColorAdapterV2
-(int)insertColorData:(LFColorModelV2*) colorData {    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"INSERT INTO %@ (%@, %@, %@, %@, %@) %@", LAF_COLORS, laf_color_id, laf_color_code, laf_color_name,laf_color_lang, laf_color_lastModified, @"VALUES(?, ?, ?, ?, ?)"];
    const char *sql = [sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
    reAccessCount++;
}
    
    if(sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    
    sqlite3_bind_int(self.sqlStament, 1, (int)colorData.lfcID);
    sqlite3_bind_text(self.sqlStament, 2, [colorData.htmlCode UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 3, [colorData.lfc_Name UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 4, [colorData.lfc_lang UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 5, [colorData.lfc_LastModifier UTF8String], -1, SQLITE_TRANSIENT);

    NSInteger returnInt = 0;
    
    NSInteger status = sqlite3_step(sqlStament);
    
    if(SQLITE_DONE != status) {   
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return (int)returnInt;
    } else {
        sqlite3_reset(self.sqlStament);
        returnInt = 1;
        return (int)returnInt;
    } 
}

-(int)updateColorData:(LFColorModelV2*) colorData {
    NSString *sqlString = [[NSString alloc] initWithFormat:@"UPDATE %@ SET %@ = ?, %@ = ?, %@ = ?, %@ =?  WHERE %@ = ?", LAF_COLORS, laf_color_code, laf_color_name,laf_color_lang, laf_color_lastModified, laf_color_id];
    const char *sql = [sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
    reAccessCount++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
        
    }
    
    sqlite3_bind_text(self.sqlStament, 1, [colorData.htmlCode UTF8String], 
                      -1,SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 2, [colorData.lfc_Name UTF8String], 
                      -1,SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 3, [colorData.lfc_lang UTF8String], 
                      -1,SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 4, [colorData.lfc_LastModifier UTF8String], 
                      -1,SQLITE_TRANSIENT);
    sqlite3_bind_int(self.sqlStament, 5, (int)colorData.lfcID);
    
    NSInteger returnInt = 0;
    
    NSInteger status = sqlite3_step(sqlStament);
    
    if(SQLITE_DONE != status) {        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        
        return (int)returnInt;
    } else { 
        returnInt = 1;
        return (int)returnInt;
    }
}

-(int)deleteColorData:(LFColorModelV2*) colorData {
    NSInteger result = 1;
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"DELETE FROM %@ WHERE %@ = ?", LAF_COLORS, laf_color_id];
    const char *sql = [sqlString UTF8String];    
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
    reAccessCount++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
        result = 0;
    
    sqlite3_bind_int(self.sqlStament, 1, (int)colorData.lfcID);
    
	NSInteger status = sqlite3_step(sqlStament);
    
	if (SQLITE_DONE != status) {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
		result = 0;
    }
    
    return (int)result;
}

-(LFColorModelV2*)loadColorData:(LFColorModelV2*)colorData {
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ?", laf_color_id, laf_color_code, laf_color_name,laf_color_lang, laf_color_lastModified, LAF_COLORS, laf_color_id];
    const char *sql =[sqlString UTF8String];

    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
    reAccessCount++;
}
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        sqlite3_bind_int(self.sqlStament, 1, (int)colorData.lfcID);
        
        status = sqlite3_step(sqlStament);
        
        while(status == SQLITE_ROW) {
            colorData.lfcID = sqlite3_column_int(self.sqlStament, 0);
           
            if (sqlite3_column_text(self.sqlStament, 1)) {
                 colorData.htmlCode = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
            }
            
            if (sqlite3_column_text(self.sqlStament, 2)) {
                colorData.lfc_Name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
            }
            
            if (sqlite3_column_text(self.sqlStament, 3)) {
                colorData.lfc_lang = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
            }
            
            if (sqlite3_column_text(self.sqlStament, 4)) {
                colorData.lfc_LastModifier = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
            }          
            
            status = sqlite3_step(sqlStament);
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }

    return colorData;
}

-(NSMutableArray *)loadAllColors {
    NSMutableArray *colorArray = [[NSMutableArray alloc] init];
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@ FROM %@", laf_color_id, laf_color_code, laf_color_name, laf_color_lang, laf_color_lastModified, LAF_COLORS];
    const char *sql =[sqlString UTF8String];

    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
    reAccessCount++;
}
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        
        status = sqlite3_step(sqlStament);
        
        while(status == SQLITE_ROW) {
            LFColorModelV2 *colorData = [[LFColorModelV2 alloc] init];
            colorData.lfcID = sqlite3_column_int(self.sqlStament, 0);
            
            if (sqlite3_column_text(self.sqlStament, 1)) {
                colorData.htmlCode = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
            }
            
            if (sqlite3_column_text(self.sqlStament, 2)) {
                colorData.lfc_Name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
            }
            
            if (sqlite3_column_text(self.sqlStament, 3)) {
                colorData.lfc_lang = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
            }
            
            if (sqlite3_column_text(self.sqlStament, 4)) {
                colorData.lfc_LastModifier = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
            }
            
            [colorArray addObject:colorData];      
            
            status = sqlite3_step(sqlStament);
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                [colorArray removeAllObjects];
                goto RE_ACCESS_DB;
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            [colorArray removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    return colorArray;
}
@end
