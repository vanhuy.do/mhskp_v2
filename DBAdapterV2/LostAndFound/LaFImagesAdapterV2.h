//
//  LaFImagesAdapterV2.h
//  mHouseKeeping
//
//  Created by TMS on 3/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DatabaseAdapterV2.h"
#import "DbDefinesV2.h"
#import "LFImageModelV2.h"

@interface LaFImagesAdapterV2 : DatabaseAdapterV2{
}

-(int)insertImageData:(LFImageModelV2*) imageData;
-(int)updateImageData:(LFImageModelV2*) imageData;
-(int)deleteImageData:(LFImageModelV2*) imageData;
-(int)deleteAllImageDataByDetailId:(int) lfDetailId;
-(LFImageModelV2*)loadImageData:(LFImageModelV2*) imageData;
-(NSMutableArray *)loadAllImageDataByLFDetailId:(int) lfDetailId;
-(NSInteger) numberOfLaFImagesMustPostRecordsByLaFDetailId:(NSInteger) lafDetailId;

@end
