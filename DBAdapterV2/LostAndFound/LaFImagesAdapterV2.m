//
//  LaFImagesAdapterV2.m
//  mHouseKeeping
//
//  Created by TMS on 3/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LaFImagesAdapterV2.h"
#import "ehkDefines.h"

@implementation LaFImagesAdapterV2

-(int)insertImageData:(LFImageModelV2*) imageData { 
    NSString *sqlString = [[NSString alloc] initWithFormat:@"INSERT INTO %@ (%@, %@) %@", LAF_IMAGES, lafim_image, lafim_lafdetail_id, @"VALUES(?, ?)"];
    const char *sql = [sqlString UTF8String];

    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
    reAccessCount++;
}
    
    if(sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
        
    }
    
    if(imageData.lafImgImage != nil) 
        sqlite3_bind_blob(self.sqlStament,1, [imageData.lafImgImage bytes], (int)[imageData.lafImgImage length], NULL);
    else
        sqlite3_bind_blob(self.sqlStament, 1, nil, -1, NULL);
    
    sqlite3_bind_int(self.sqlStament, 2, (int)imageData.lafDetailId);
    
    NSInteger returnInt = 0;
    
    NSInteger status = sqlite3_step(sqlStament);
    
    if(SQLITE_DONE != status) {   
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return (int)returnInt;
    } else {
        sqlite3_reset(self.sqlStament);
        returnInt = 1;
        return (int)returnInt;
    }    
}

-(int)updateImageData:(LFImageModelV2*) imageData {
    NSString *sqlString = [[NSString alloc] initWithFormat:@"UPDATE %@ SET %@ = ?, %@ = ? WHERE %@ = ?", LAF_IMAGES, lafim_image, lafim_lafdetail_id, lafim_id];        
    const char *sql = [sqlString UTF8String];

    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
    reAccessCount++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
        
    }
    
    sqlite3_bind_blob(self.sqlStament, 1, [imageData.lafImgImage bytes], (int)[imageData.lafImgImage length], NULL);
    sqlite3_bind_int (self.sqlStament, 2, (int)imageData.lafDetailId);
    
    sqlite3_bind_int (self.sqlStament, 3, (int)imageData.lafImgId);
    
    NSInteger returnInt = 0;
    
    NSInteger status = sqlite3_step(sqlStament);
    
    if(SQLITE_DONE != status) {    
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return (int)returnInt;
    } else { 
        returnInt = 1;        
        return (int)returnInt;
    }
}

-(int)deleteImageData:(LFImageModelV2 *)imageData {
    NSInteger result = 1;
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"DELETE FROM %@ WHERE %@ = ?", LAF_IMAGES, lafim_id];        
    const char *sql = [sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
    reAccessCount++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
        result = 0;	
	
    sqlite3_bind_int(self.sqlStament, 1, (int)imageData.lafImgId);
	
    NSInteger status = sqlite3_step(sqlStament);
    
	if (SQLITE_DONE != status) {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
		result = 0;
    }
    
    return (int)result;
}

-(int)deleteAllImageDataByDetailId:(int)lfDetailId {
    NSInteger result = 1;
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"DELETE FROM %@ WHERE %@ = ?", LAF_IMAGES, lafim_lafdetail_id];        
    const char *sql = [sqlString UTF8String];
	
	NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
    reAccessCount++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
        result = 0;	
	
    sqlite3_bind_int(self.sqlStament, 1, lfDetailId);
	
    NSInteger status = sqlite3_step(sqlStament);
    
	if (SQLITE_DONE != status) {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
		result = 0;
    }
    
    return (int)result;
}

-(LFImageModelV2*)loadImageData:(LFImageModelV2*)imageData {
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@ FROM %@ WHERE %@ = ?", lafim_id, lafim_image, lafim_lafdetail_id, LAF_IMAGES, lafim_id];
    const char *sql = [sqlString UTF8String];

    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
    reAccessCount++;
}
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        sqlite3_bind_int(self.sqlStament, 1, (int)imageData.lafImgId);
        
        NSInteger status = sqlite3_step(sqlStament);
        
        while(status == SQLITE_ROW) {
            imageData.lafImgId = sqlite3_column_int(self.sqlStament, 0);
            
            if (sqlite3_column_bytes(self.sqlStament, 1) > 0) {
                imageData.lafImgImage  = [NSData dataWithBytes:sqlite3_column_blob
                                    (self.sqlStament,1) length:sqlite3_column_bytes(self.sqlStament, 1)];
            } 
            
            imageData.lafDetailId = sqlite3_column_int(self.sqlStament, 2);
            
            status = sqlite3_step(sqlStament);
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }    
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    return imageData;
}

-(NSMutableArray *) loadAllImageDataByLFDetailId:(int) lfDetailId {
    NSMutableArray *imageArray = [[NSMutableArray alloc] init];
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@ FROM %@ WHERE %@ = ? ORDER BY %@", lafim_id, lafim_image, lafim_lafdetail_id, LAF_IMAGES, lafim_lafdetail_id, lafim_id];    
    const char *sql =[sqlString UTF8String];

    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
    reAccessCount++;
}
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        sqlite3_bind_int(self.sqlStament, 1, lfDetailId);
        
        status = sqlite3_step(sqlStament);
        
        while(status == SQLITE_ROW) {
            LFImageModelV2 *imageData = [[LFImageModelV2 alloc] init];
            imageData.lafImgId = sqlite3_column_int(self.sqlStament, 0);
            
            if (sqlite3_column_bytes(self.sqlStament, 1) > 0) {
                imageData.lafImgImage  = [NSData dataWithBytes:sqlite3_column_blob
                                    (self.sqlStament,1) length:sqlite3_column_bytes(self.sqlStament, 1)];
            } 
            
            imageData.lafDetailId=sqlite3_column_int(self.sqlStament, 2);
            
            [imageArray addObject:imageData];
            
            status = sqlite3_step(sqlStament);
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                [imageArray removeAllObjects];
                goto RE_ACCESS_DB;
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            [imageArray removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    return imageArray;
}

-(NSInteger)numberOfLaFImagesMustPostRecordsByLaFDetailId:(NSInteger)lafDetailId {
    NSString *sqlString = [NSString stringWithFormat:@"SELECT COUNT(*) FROM %@ WHERE %@ = %d", 
                           LAF_IMAGES,
                           
                           lafim_lafdetail_id,
                           (int)lafDetailId
                           ];
    const char *sql = [sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
    reAccessCount++;
}
    NSInteger status;
    status = sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL);
    if (status == SQLITE_OK) {
        
        status = sqlite3_step(sqlStament);
        
        while (status == SQLITE_ROW) {
            return sqlite3_column_int(sqlStament, 0);
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    return 0;
}

@end
