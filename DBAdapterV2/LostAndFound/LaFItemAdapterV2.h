//
//  LaFItemAdapterV2.h
//  mHouseKeeping
//
//  Created by TMS on 3/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LFItemModelV2.h"
#import "DatabaseAdapterV2.h"

@interface LaFItemAdapterV2 : DatabaseAdapterV2 {
    
}

-(int)insertItemData:(LFItemModelV2*) item ;
-(int)updateItemData:(LFItemModelV2*) item;
-(int)deleteItemData:(LFItemModelV2*) item;
-(LFItemModelV2*)loadItemData:(LFItemModelV2*)itemData;
-(NSMutableArray *)loadAllItemData;
-(NSMutableArray *)loadAllItemDataByCategoryId:(int) categoryId;
@end
