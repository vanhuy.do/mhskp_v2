//
//  LaFItemAdapterV2.m
//  mHouseKeeping
//
//  Created by TMS on 3/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LaFItemAdapterV2.h"

@implementation LaFItemAdapterV2

-(int)insertItemData:(LFItemModelV2*) item { 
    NSString *sqlString = [[NSString alloc] initWithFormat:@"INSERT INTO %@ (%@, %@, %@, %@, %@, %@) %@", LAF_ITEMS, lafi_id, lafi_name, lafi_name_lang, lafi_category_id, lafi_image, lafi_last_modified, @"VALUES(?, ?, ?, ?, ?, ?)"];
    const char *sql = [sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount++;
    }
    
    if(sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    
    sqlite3_bind_int(self.sqlStament, 1, (int)item.lfitID);
    sqlite3_bind_text(self.sqlStament, 2, [item.lfitName UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 3, [item.lfitLang UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_int(self.sqlStament, 4, (int)item.lafi_category_id);

    if(item.lfitImage != nil) 
        sqlite3_bind_blob(self.sqlStament, 5, [item.lfitImage bytes], (int)[item.lfitImage length], NULL);
    else
        sqlite3_bind_blob(self.sqlStament, 5, nil, -1, NULL);
    
     sqlite3_bind_text(self.sqlStament, 6, [item.lfitLastModified UTF8String], -1, SQLITE_TRANSIENT);
    
    NSInteger returnInt = 0;
    
    NSInteger status = sqlite3_step(sqlStament);
    
    if(SQLITE_DONE != status) {   
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        
        return (int)returnInt;
    } else {
        sqlite3_reset(self.sqlStament);
        returnInt = 1;
        return (int)returnInt;
    }
}

-(int)updateItemData:(LFItemModelV2*) item {   
    NSString *sqlString = [[NSString alloc] initWithFormat:@"UPDATE %@ SET %@ = ?, %@ = ?, %@ = ?, %@ =?, %@ =?  WHERE %@ = ?", LAF_ITEMS, lafi_name, lafi_name_lang, lafi_category_id, lafi_image, lafi_last_modified, lafi_id];        
    const char *sql = [sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount++;
    }
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
        
    }
        
    sqlite3_bind_text(self.sqlStament, 1, [item.lfitName UTF8String], 
                      -1,SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 2, [item.lfitLang UTF8String], 
                      -1,SQLITE_TRANSIENT);
    sqlite3_bind_int (self.sqlStament, 3, (int)item.lafi_category_id);
    sqlite3_bind_blob(self.sqlStament, 4, [item.lfitImage bytes], (int)[item.lfitImage length], NULL);
    sqlite3_bind_text(self.sqlStament, 5, [item.lfitLastModified UTF8String], 
                      -1,SQLITE_TRANSIENT);
    sqlite3_bind_int (self.sqlStament, 6, (int)item.lfitID);
    
    NSInteger returnInt = 0;
    
    NSInteger status = sqlite3_step(sqlStament);
    
    if(SQLITE_DONE != status) {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return (int)returnInt;
    } else { 
        returnInt = 1;        
        return (int)returnInt;
    }
}

-(int)deleteItemData:(LFItemModelV2*) item {    
    NSInteger result = 1;    
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"DELETE FROM %@ WHERE %@ = ?", LAF_ITEMS, lafi_id];
        
    const char *sql = [sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount++;
    }
		
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
        result = 0;
		
    sqlite3_bind_int(self.sqlStament, 1, (int)item.lfitID);
    
    NSInteger status = sqlite3_step(sqlStament);
	
	if (SQLITE_DONE != status) {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
		result = 0;
    }
    
    return (int)result;
}

-(LFItemModelV2*)loadItemData:(LFItemModelV2*)itemData {   
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ?", lafi_id, lafi_name, lafi_name_lang, lafi_category_id,lafi_image, lafi_last_modified, LAF_ITEMS, lafi_id];
    const char *sql =[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount++;
    }
    NSInteger status;
    
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        sqlite3_bind_int(self.sqlStament, 1, (int)itemData.lfitID);
        
        status = sqlite3_step(sqlStament);
        
        while(status == SQLITE_ROW) {
            itemData.lfitID = sqlite3_column_int(self.sqlStament, 0);
            
            if (sqlite3_column_text(self.sqlStament, 1)) {
                itemData.lfitName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
            }
            
            if (sqlite3_column_text(self.sqlStament, 2)) {
                itemData.lfitLang = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
            }
            
            itemData.lafi_category_id = sqlite3_column_int(self.sqlStament, 3);
            
            if (sqlite3_column_bytes(self.sqlStament, 4) > 0) {
                itemData.lfitImage  = [NSData dataWithBytes:sqlite3_column_blob
                                    (self.sqlStament, 4) length:sqlite3_column_bytes(self.sqlStament, 4)];
            } 
            
            if (sqlite3_column_text(self.sqlStament, 5)) {
                itemData.lfitLastModified = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
            }
            status = sqlite3_step(sqlStament);
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
        
    return itemData;
}

-(NSMutableArray *) loadAllItemData {
    NSMutableArray *itemDataArray = [[NSMutableArray alloc] init];
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@ FROM %@", lafi_id, lafi_name, lafi_name_lang, lafi_category_id, lafi_image, lafi_last_modified, LAF_ITEMS];
    const char *sql =[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount++;
    }
    
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        
        status = sqlite3_step(sqlStament);
        
        while(status == SQLITE_ROW) {
            LFItemModelV2 *itemData=[[LFItemModelV2 alloc] init];
            
            itemData.lfitID = sqlite3_column_int(self.sqlStament, 0);
            
            if (sqlite3_column_text(self.sqlStament, 1)) {
                itemData.lfitName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
            }
            
            if (sqlite3_column_text(self.sqlStament, 2)) {
                itemData.lfitLang = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
            }
            
            itemData.lafi_category_id = sqlite3_column_int(self.sqlStament, 3);
            
            if (sqlite3_column_bytes(self.sqlStament, 4) > 0) {
                itemData.lfitImage  = [NSData dataWithBytes:sqlite3_column_blob
                                       (self.sqlStament, 4) length:sqlite3_column_bytes(self.sqlStament, 4)];
            } 
            
            if (sqlite3_column_text(self.sqlStament, 5)) {
                itemData.lfitLastModified = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
            }
            
            [itemDataArray addObject:itemData];
            
            status = sqlite3_step(sqlStament);
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                [itemDataArray removeAllObjects];
                goto RE_ACCESS_DB;
            }
        }
    }  
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            [itemDataArray removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    return itemDataArray;
}

-(NSMutableArray *) loadAllItemDataByCategoryId:(int) categoryId {
    NSMutableArray *itemDataArray=[[NSMutableArray alloc] init];
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ?", lafi_id, lafi_name, lafi_name_lang, lafi_category_id,lafi_image, lafi_last_modified, LAF_ITEMS, lafi_category_id];
    const char *sql =[sqlString UTF8String];

    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount++;
    }
    
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        
        sqlite3_bind_int(self.sqlStament, 1, categoryId);
        
        status = sqlite3_step(sqlStament);
        
        while(status == SQLITE_ROW) {
            LFItemModelV2 *itemData=[[LFItemModelV2 alloc] init];
            itemData.lfitID = sqlite3_column_int(self.sqlStament, 0);
            
            if (sqlite3_column_text(self.sqlStament, 1)) {
                itemData.lfitName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
            }
            
            if (sqlite3_column_text(self.sqlStament, 2)) {
                itemData.lfitLang = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
            }
            
            itemData.lafi_category_id = sqlite3_column_int(self.sqlStament, 3);
            
            if (sqlite3_column_bytes(self.sqlStament, 4) > 0) {
                itemData.lfitImage  = [NSData dataWithBytes:sqlite3_column_blob
                                       (self.sqlStament, 4) length:sqlite3_column_bytes(self.sqlStament, 4)];
            } 
            
            if (sqlite3_column_text(self.sqlStament, 5)) {
                itemData.lfitLastModified = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
            }
           
            [itemDataArray addObject:itemData];
            
            status = sqlite3_step(sqlStament);
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                [itemDataArray removeAllObjects];
                goto RE_ACCESS_DB;
            }
        }
    }    
    
    return itemDataArray;
}
@end
