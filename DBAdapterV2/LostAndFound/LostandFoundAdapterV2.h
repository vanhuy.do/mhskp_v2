//
//  LostandFoundAdapterV2.h
//  mHouseKeeping
//
//  Created by TMS on 3/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DatabaseAdapterV2.h"
#import "LostandFoundModelV2.h"

@interface LostandFoundAdapterV2 : DatabaseAdapterV2 {    
}

-(int)insertLaFData:(LostandFoundModelV2*) lafData;
-(int)updateLaFData:(LostandFoundModelV2*) lafData;
-(int)deleteLaFData:(LostandFoundModelV2*) lafData;
-(int)deleteAllLaFData:(int) lfDetailId;
-(LostandFoundModelV2*)loadLaFData:(LostandFoundModelV2*) lafData;
-(NSMutableArray *)loadAllLaFDataByLaFDetailId:(int) lfDetailId;
-(NSInteger) numberOfLaFMustPostRecordsByLaFDetailId:(NSInteger) lafDetailId;

@end
