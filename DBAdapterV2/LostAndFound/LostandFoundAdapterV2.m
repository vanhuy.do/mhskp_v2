//
//  LostandFoundAdapterV2.m
//  mHouseKeeping
//
//  Created by TMS on 3/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LostandFoundAdapterV2.h"
#import "ehkDefines.h"

@implementation LostandFoundAdapterV2

-(int)insertLaFData:(LostandFoundModelV2*) lafData  {
    NSString *sqlString = [[NSString alloc] initWithFormat:@"INSERT INTO %@ (%@, %@, %@, %@, %@) %@", LOSTANDFOUND, laf_category_id, laf_item_id, laf_color,laf_lafdetail_id, laf_item_quantity, @"VALUES(?, ?, ?, ?, ?)"];
    const char *sql = [sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount++;
    }
    
    if(sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    
    sqlite3_bind_int(self.sqlStament, 1, (int)lafData.lafCategoryId);
    sqlite3_bind_int(self.sqlStament, 2, (int)lafData.lafItemId);
    sqlite3_bind_int(self.sqlStament, 3, (int)lafData.lafColorId);
    sqlite3_bind_int(self.sqlStament, 4, (int)lafData.lafLaFDetailId);
    sqlite3_bind_int(self.sqlStament, 5, (int)lafData.laf_item_quantity);

    NSInteger returnInt = 0;    
    
    NSInteger status = sqlite3_step(self.sqlStament);
    
    if(SQLITE_DONE != status) {
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        
        return (int)returnInt;
    } else {
        sqlite3_reset(self.sqlStament);
        returnInt = 1;
        return (int)returnInt;
    }
}

-(int)updateLaFData:(LostandFoundModelV2*) lafData {
    NSString *sqlString = [[NSString alloc] initWithFormat:@"UPDATE %@ SET  %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ? WHERE %@ = ?", LOSTANDFOUND, laf_category_id, laf_item_id, laf_color, laf_lafdetail_id, laf_item_quantity, laf_id];
    const char *sql = [sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount++;
    }
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    
    sqlite3_bind_int(self.sqlStament, 1, (int)lafData.lafCategoryId);
    sqlite3_bind_int(self.sqlStament, 2, (int)lafData.lafItemId);
    sqlite3_bind_int(self.sqlStament, 3, (int)lafData.lafColorId);
    sqlite3_bind_int(self.sqlStament, 4, (int)lafData.lafLaFDetailId);
    sqlite3_bind_int(self.sqlStament, 5, (int)lafData.laf_item_quantity);
    sqlite3_bind_int(self.sqlStament, 6, (int)lafData.lafId);

    NSInteger returnInt = 0;
    
    NSInteger status = sqlite3_step(self.sqlStament);
    
    if(SQLITE_DONE != status) {  
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return (int)returnInt;
    } else { 
        returnInt = 1;
        
        return (int)returnInt;
    }
}

-(int)deleteLaFData:(LostandFoundModelV2*) lafData {   
    NSInteger result = 1;    
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"DELETE FROM %@ WHERE %@ = ?", LOSTANDFOUND, laf_id];
    
    const char *sql = [sqlString UTF8String];
    
	NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount++;
    }
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
        result = 0;	
	}
    
    sqlite3_bind_int(self.sqlStament, 1, (int)lafData.lafId);
	
	NSInteger status = sqlite3_step(self.sqlStament);
    
    if(SQLITE_DONE != status) {	
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        
        result = 0;
    }
    
    return (int)result;
}

-(int)deleteAllLaFData:(int) lfDetailId {   
    NSInteger result = 1;    
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"DELETE FROM %@ WHERE %@ = ?", LOSTANDFOUND, laf_lafdetail_id];
    
    const char *sql = [sqlString UTF8String];
    
	NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount++;
//        usleep(2);
    }
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
        result = 0;	
	}
    
    sqlite3_bind_int(self.sqlStament, 1, lfDetailId);
	
	NSInteger status = sqlite3_step(self.sqlStament);
    
    if(SQLITE_DONE != status) {	
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        result = 0;
    }
    
    return (int)result;
}

-(LostandFoundModelV2*)loadLaFData:(LostandFoundModelV2*) lafData {
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ?", laf_id, laf_category_id, laf_item_id, laf_color,laf_lafdetail_id, laf_item_quantity, LOSTANDFOUND, laf_id];
    const char *sql = [sqlString UTF8String];

    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount++;
    }
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        sqlite3_bind_int(self.sqlStament, 1, (int)lafData.lafId);
        
        status = sqlite3_step(sqlStament);
        
        while(status == SQLITE_ROW) {
            lafData.lafId = sqlite3_column_int(self.sqlStament, 0);
            lafData.lafCategoryId = sqlite3_column_int(self.sqlStament, 1);
            lafData.lafItemId = sqlite3_column_int(self.sqlStament, 2);
            lafData.lafColorId = sqlite3_column_int(self.sqlStament, 3);
            lafData.lafLaFDetailId = sqlite3_column_int(self.sqlStament, 4);
            lafData.laf_item_quantity = sqlite3_column_int(self.sqlStament, 5);
            
            status = sqlite3_step(sqlStament);
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }    
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    return lafData;
}

-(NSMutableArray *) loadAllLaFDataByLaFDetailId:(int) lfDetailId {
    NSMutableArray *lafArray=[[NSMutableArray alloc] init];
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ?", laf_id, laf_category_id, laf_item_id, laf_color,laf_lafdetail_id, laf_item_quantity, LOSTANDFOUND, laf_lafdetail_id];
    const char *sql = [sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount++;
    }
    
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        sqlite3_bind_int(self.sqlStament, 1, lfDetailId);
        
        status = sqlite3_step(sqlStament);
        
        while(status == SQLITE_ROW) {
            LostandFoundModelV2 *lafData = [[LostandFoundModelV2 alloc] init];
            lafData.lafId = sqlite3_column_int(self.sqlStament, 0);
            lafData.lafCategoryId = sqlite3_column_int(self.sqlStament, 1);
            lafData.lafItemId = sqlite3_column_int(self.sqlStament, 2);
            lafData.lafColorId = sqlite3_column_int(self.sqlStament, 3);
            lafData.lafLaFDetailId = sqlite3_column_int(self.sqlStament, 4);
            lafData.laf_item_quantity=sqlite3_column_int(self.sqlStament, 5);
            
            [lafArray addObject:lafData];
            
            status = sqlite3_step(sqlStament);
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                [lafArray removeAllObjects];
                goto RE_ACCESS_DB;
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            [lafArray removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }

    return lafArray;
}

-(NSInteger)numberOfLaFMustPostRecordsByLaFDetailId:(NSInteger)lafDetailId {
    NSString *sqlString = [NSString stringWithFormat:@"SELECT COUNT(*) FROM %@ WHERE %@ = %d", 
                           LOSTANDFOUND,
                           
                           laf_lafdetail_id,
                           (int)lafDetailId
                           ];
    const char *sql = [sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount++;
    }
    NSInteger status;
    status = sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL);
    if (status == SQLITE_OK) {
        
        status = sqlite3_step(sqlStament);
        
        while (status == SQLITE_ROW) {
            return sqlite3_column_int(sqlStament, 0);
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    return 0;
}



@end
