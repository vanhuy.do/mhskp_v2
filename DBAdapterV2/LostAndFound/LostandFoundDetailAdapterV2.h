//
//  LostandFoundDetailAdapterV2.h
//  mHouseKeeping
//
//  Created by TMS on 3/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DatabaseAdapterV2.h"
#import "DbDefinesV2.h"
#import "LostandFoundDetailModelV2.h"

@interface LostandFoundDetailAdapterV2 : DatabaseAdapterV2{
}

-(int)insertLaFDetailData:(LostandFoundDetailModelV2*) lafDetailData;
-(int)updateLaFDetailData:(LostandFoundDetailModelV2*) lafDetailData;
-(int)deleteLaFDetailData:(LostandFoundDetailModelV2*) lafDetailData;
-(LostandFoundDetailModelV2*)loadLaFDetailData:(LostandFoundDetailModelV2*)lafDetailData;
-(LostandFoundDetailModelV2*)loadLaFDetailDataBy_UserIdand_RoomId:(LostandFoundDetailModelV2*)lafDetailData;
-(NSInteger) numberOfLaFDetailMustPostRecords;
-(NSMutableArray *) loadAllLostAndFoundDetailByUserId:(NSInteger) userId;

@end
