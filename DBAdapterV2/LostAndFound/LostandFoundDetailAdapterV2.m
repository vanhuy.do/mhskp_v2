//
//  LostandFoundDetailAdapterV2.m
//  mHouseKeeping
//
//  Created by TMS on 3/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LostandFoundDetailAdapterV2.h"
#import "ehkDefines.h"
#import "AccessRightModel.h"

@implementation LostandFoundDetailAdapterV2

-(int)insertLaFDetailData:(LostandFoundDetailModelV2*) lafDetailData  {
    NSString *sqlString = [[NSString alloc] initWithFormat:@"INSERT INTO %@ (%@, %@, %@, %@, %@, %@) %@", LOSTANDFOUND_DETAIL, lafd_user_id, lafd_room_id, lafd_pos_status, lafd_date, lafd_remark, lafd_room_assignment_id, @"VALUES(?, ?, ?, ?, ?, ?)"];
    if (isDemoMode) {
        sqlString = [[NSString alloc] initWithFormat:@"INSERT INTO %@ (%@, %@, %@, %@, %@) %@", LOSTANDFOUND_DETAIL, lafd_user_id, lafd_room_id, lafd_pos_status, lafd_date, lafd_remark, @"VALUES(?, ?, ?, ?, ?)"];
    }
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount++;
    }

    if(sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    
    sqlite3_bind_int(self.sqlStament, 1, (int)lafDetailData.lafDetailUserId);
    sqlite3_bind_text(self.sqlStament, 2 , [lafDetailData.lafDetailRoomId UTF8String], -1,SQLITE_TRANSIENT);
    if (!lafDetailData.lafDetailPosStatus) {
        lafDetailData.lafDetailPosStatus = 0;
    }
    sqlite3_bind_int(self.sqlStament, 3, (int)lafDetailData.lafDetailPosStatus);
    sqlite3_bind_text(self.sqlStament, 4, [lafDetailData.lafDetailDate UTF8String], -1, SQLITE_TRANSIENT);
    if (lafDetailData.lafDetailRemark == nil) {
        lafDetailData.lafDetailRemark = @"";
    }
    sqlite3_bind_text(self.sqlStament, 5, [lafDetailData.lafDetailRemark UTF8String], -1, SQLITE_TRANSIENT);
    if (!isDemoMode) {
        sqlite3_bind_int(self.sqlStament, 6, (int)lafDetailData.lafRoomAssignmentId);
    }
    
    
    NSInteger returnInt = 0;
    
    NSInteger status = sqlite3_step(sqlStament);
    
    if(SQLITE_DONE != status) {   
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return (int)returnInt;
    } else {
        sqlite3_reset(self.sqlStament);
        returnInt = 1;
        return (int)returnInt;
    }
}

-(int)updateLaFDetailData:(LostandFoundDetailModelV2*) lafDetailData {    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"UPDATE %@ SET %@ = ?, %@ = ?, %@ = ?, %@ =?, %@ =?, %@ = ?  WHERE %@ = ?", LOSTANDFOUND_DETAIL, lafd_user_id, lafd_room_id, lafd_pos_status, lafd_date, lafd_remark, lafd_room_assignment_id,lafd_id];
    const char *sql = [sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount++;
    }
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
        
    }
    
    sqlite3_bind_int(self.sqlStament, 1, (int)lafDetailData.lafDetailUserId);
    sqlite3_bind_text(self.sqlStament, 2 , [lafDetailData.lafDetailRoomId UTF8String], -1,SQLITE_TRANSIENT);
    sqlite3_bind_int(self.sqlStament, 3, (int)lafDetailData.lafDetailPosStatus);
    sqlite3_bind_text(self.sqlStament, 4, [lafDetailData.lafDetailDate UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 5, [lafDetailData.lafDetailRemark UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_int(self.sqlStament, 6, (int)lafDetailData.lafRoomAssignmentId);
    sqlite3_bind_int(self.sqlStament, 7, (int)lafDetailData.lafDetailId);

    NSInteger returnInt = 0;
    
    NSInteger status = sqlite3_step(sqlStament);
    
    if(SQLITE_DONE != status) {   
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return (int)returnInt;
    } else { 
        returnInt = 1;
        
        return (int)returnInt; 
    }
}

-(int)deleteLaFDetailData:(LostandFoundDetailModelV2*) lafDetailData {  
    NSInteger result = 1;
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"DELETE FROM %@ WHERE %@ = ?", LOSTANDFOUND_DETAIL, lafd_id];
    
    const char *sql = [sqlString UTF8String];

    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
    reAccessCount++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
        result = 0;
		
    sqlite3_bind_int(self.sqlStament, 1, (int)lafDetailData.lafDetailId);
	
    NSInteger status = sqlite3_step(sqlStament);
    
	if (SQLITE_DONE != status) { 
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
		result = 0;
    }
    
    return (int)result;
}

-(LostandFoundDetailModelV2*)loadLaFDetailData:(LostandFoundDetailModelV2*)lafDetailData {
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ?", lafd_id, lafd_user_id, lafd_room_id, lafd_pos_status,lafd_date, lafd_remark, lafd_room_assignment_id, LOSTANDFOUND_DETAIL, lafd_id];
    const char *sql =[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
    reAccessCount++;
}
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        sqlite3_bind_int(self.sqlStament, 1, (int)lafDetailData.lafDetailId);
        
        status = sqlite3_step(sqlStament);
        
        while(status == SQLITE_ROW) {
            lafDetailData.lafDetailId = sqlite3_column_int(self.sqlStament, 0);
            lafDetailData.lafDetailUserId = sqlite3_column_int(self.sqlStament, 1);
            if (sqlite3_column_text(self.sqlStament, 2)) {
                lafDetailData.lafDetailRoomId = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
            }
            lafDetailData.lafDetailPosStatus = sqlite3_column_int(self.sqlStament, 3);

            if (sqlite3_column_text(self.sqlStament, 4)) {
                lafDetailData.lafDetailDate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
            }
            if (sqlite3_column_text(self.sqlStament, 5)) {
                lafDetailData.lafDetailRemark = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
            }
            
            lafDetailData.lafRoomAssignmentId = sqlite3_column_int(self.sqlStament, 6);
            status = sqlite3_step(sqlStament);
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    return lafDetailData;
}

-(LostandFoundDetailModelV2*)loadLaFDetailDataBy_UserIdand_RoomId:(LostandFoundDetailModelV2*)lafDetailData {
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ? AND %@ =?", lafd_id, lafd_user_id, lafd_room_id, lafd_pos_status, lafd_date, lafd_remark, lafd_room_assignment_id, LOSTANDFOUND_DETAIL, lafd_user_id,lafd_room_id];
    const char *sql =[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
    reAccessCount++;
}
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {        
        sqlite3_bind_int(self.sqlStament, 1, (int)lafDetailData.lafDetailUserId);
        sqlite3_bind_text(self.sqlStament, 2 , [lafDetailData.lafDetailRoomId UTF8String], -1,SQLITE_TRANSIENT);
        
        status = sqlite3_step(sqlStament);
        
        while(status == SQLITE_ROW) {
            lafDetailData.lafDetailId = sqlite3_column_int(self.sqlStament, 0);
            lafDetailData.lafDetailUserId = sqlite3_column_int(self.sqlStament, 1);
            if (sqlite3_column_text(self.sqlStament, 2)) {
                lafDetailData.lafDetailRoomId = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
            }
            lafDetailData.lafDetailPosStatus = sqlite3_column_int(self.sqlStament, 3);
            
            if (sqlite3_column_text(self.sqlStament, 4)) {
                lafDetailData.lafDetailDate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
            }
            
            if (sqlite3_column_text(self.sqlStament, 5)) {
                lafDetailData.lafDetailRemark = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
            }
            
            lafDetailData.lafRoomAssignmentId = sqlite3_column_int(self.sqlStament, 6);
            
            status =sqlite3_step(sqlStament);
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }  
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    return lafDetailData;
}

-(NSInteger) numberOfLaFDetailMustPostRecords {
    NSString *sqlString = [NSString stringWithFormat:@"SELECT COUNT(*) FROM %@", LOSTANDFOUND_DETAIL];
    const char *sql = [sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
    reAccessCount++;
}
    NSInteger status;
    status = sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL);
    if (status == SQLITE_OK) {
        
        status = sqlite3_step(sqlStament);
        
        while (status == SQLITE_ROW) {
            return sqlite3_column_int(sqlStament, 0);
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    return 0;
}

-(NSMutableArray *)loadAllLostAndFoundDetailByUserId:(NSInteger)userId {
    NSString *sqlString = [NSString stringWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ = %d",
                           lafd_id, 
                           lafd_user_id, 
                           lafd_room_id, 
                           lafd_pos_status, 
                           lafd_date, 
                           lafd_remark,
                           lafd_room_assignment_id,
                           
                           LOSTANDFOUND_DETAIL, 
                           
                           lafd_user_id, 
                           (int)userId
                           ];
    
    const char *sql =[sqlString UTF8String];
    
    NSMutableArray *results = [NSMutableArray array];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
    reAccessCount++;
}
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {        
        
        status = sqlite3_step(sqlStament);
        
        while(status == SQLITE_ROW) {
            LostandFoundDetailModelV2 *lafDetailData = [[LostandFoundDetailModelV2 alloc] init];
            lafDetailData.lafDetailId = sqlite3_column_int(self.sqlStament, 0);
            lafDetailData.lafDetailUserId = sqlite3_column_int(self.sqlStament, 1);
            if (sqlite3_column_text(self.sqlStament, 2)) {
                lafDetailData.lafDetailRoomId = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
            }
            lafDetailData.lafDetailPosStatus = sqlite3_column_int(self.sqlStament, 3);
            
            if (sqlite3_column_text(self.sqlStament, 4)) {
                lafDetailData.lafDetailDate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
            }
            
            if (sqlite3_column_text(self.sqlStament, 5)) {
                lafDetailData.lafDetailRemark = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
            }
            
            lafDetailData.lafRoomAssignmentId = sqlite3_column_int(self.sqlStament, 6);
            
            [results addObject:lafDetailData];
            
            status = sqlite3_step(sqlStament);
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    return results;
}


@end
