//
//  LostandFoundHistoryAdapter.m
//  mHouseKeeping
//
//  Created by Quang Nguyen on 4/8/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "LostandFoundHistoryAdapter.h"
#import "NSDate-Utilities.h"
#import "DbDefinesV2.h"
#import "ehkDefines.h"
#import "LanguageManagerV2.h"
#import "UserManagerV2.h"

@implementation LostandFoundHistoryAdapter

-(int) insertLaFHistoryData:(LostandFoundHistoryModel *)lafhModel{
    NSString *sqlString = [[NSString alloc] initWithFormat:@"INSERT INTO %@ (%@, %@, %@, %@, %@, %@, %@) %@", LOST_FOUND_HISTORY, LAFH_id, LAFH_room_id, LAFH_item_id, LAFH_color_id, LAFH_quantity, LAFH_user_id, LAFH_date, @"VALUES(?, ?, ?, ?, ?, ?, ?)"];
    
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {}
    
    sqlite3_bind_text(self.sqlStament, 2 , [lafhModel.lafh_room_id UTF8String], -1,SQLITE_TRANSIENT);
    sqlite3_bind_int(self.sqlStament, 3, (int)lafhModel.lafh_item_id);
    sqlite3_bind_int(self.sqlStament, 4, (int)lafhModel.lafh_color_id);
    sqlite3_bind_int(self.sqlStament, 5, (int)lafhModel.lafh_quantity);
    sqlite3_bind_int(self.sqlStament, 6, (int)lafhModel.lafh_user_id);
    sqlite3_bind_text(self.sqlStament, 7, [lafhModel.lafh_date UTF8String], -1, SQLITE_TRANSIENT);
    NSInteger returnInt = 0;
    
    NSInteger status = sqlite3_step(self.sqlStament);
    
    if(SQLITE_DONE != status) {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return (int)returnInt;
    } else {
        sqlite3_reset(self.sqlStament);
        returnInt = 1;
        return (int)returnInt;
    }
}

-(NSMutableArray *) loadRoomIdByUserId:(NSInteger)userId{
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT DISTINCT %@ FROM %@ WHERE %@ = ? ORDER BY %@ ASC",
                           LAFH_room_id,
                           LOST_FOUND_HISTORY,
                           LAFH_user_id,
                           LAFH_room_id
                           ];
    
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    reAccessCount ++;
}
    
    NSMutableArray *array = [NSMutableArray array];
    NSInteger status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        
        sqlite3_bind_int(self.sqlStament, 1, (int)userId);
        
        status = sqlite3_step(self.sqlStament);
        while(status == SQLITE_ROW)
        {
            LostandFoundHistoryModel *model = [[LostandFoundHistoryModel alloc] init];
            if (sqlite3_column_text(self.sqlStament, 0)) {
                model.lafh_room_id = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 0)];
            }
            
            [array addObject:model];
            
            status = sqlite3_step(self.sqlStament);
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                [array removeAllObjects];
                goto RE_ACCESS_DB;
            }
        }
    }
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    return array;
}

-(NSMutableArray *) loadAllLaFHistoryByUserId:(NSInteger)userId AndRoomId:(NSString*)roomNumber{
    
    BOOL isEnglish = [[LanguageManagerV2 sharedLanguageManager] currentLanguageIsEqualTo:@"%@", ENGLISH_LANGUAGE];
    NSString *lafi_Name = nil;
    NSString *lafc_Name = nil;
    
    if(isEnglish) {
        lafi_Name = [NSString stringWithFormat:@"%@", lafi_name];
        lafc_Name = [NSString stringWithFormat:@"%@", lafc_name];
    } else {
        lafi_Name = [NSString stringWithFormat:@"%@", lafi_name_lang];
        lafc_Name = [NSString stringWithFormat:@"%@", lafc_lang_name];
    }
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT a.%@, a.%@, a.%@, a.%@, a.%@, a.%@, a.%@, b.%@, c.%@, d.%@ FROM %@ a JOIN %@ b ON a.%@ = b.%@  JOIN %@ c ON b.%@ = c.%@ LEFT JOIN %@ d ON a.%@ = d.%@ WHERE %@ = ? AND %@ = ? ORDER BY c.%@ ASC",
                           LAFH_id,
                           LAFH_room_id,
                           LAFH_item_id,
                           LAFH_color_id,
                           LAFH_quantity,
                           LAFH_user_id,
                           LAFH_date,
                           
                           lafi_Name,
                           lafc_Name,
                           laf_color_code,
                           
                           LOST_FOUND_HISTORY,
                           LAF_ITEMS,
                           LAFH_item_id,
                           lafi_id,
                           
                           LAF_CATIGORIES,
                           lafi_category_id,
                           lafc_id,
                           
                           LAF_COLORS,
                           LAFH_color_id,
                           laf_color_id,
                           
                           LAFH_user_id,
                           LAFH_room_id,
                           lafc_name
                           ];
    
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    reAccessCount ++;
}
    
    NSMutableArray *array = [NSMutableArray array];
    NSInteger status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        
        sqlite3_bind_int (self.sqlStament, 1, (int)userId);
        sqlite3_bind_text(self.sqlStament, 2 , [roomNumber UTF8String], -1,SQLITE_TRANSIENT);
        
        status = sqlite3_step(self.sqlStament);
        while(status == SQLITE_ROW)
        {
            LostandFoundHistoryModel *model = [[LostandFoundHistoryModel alloc] init];
            model.lafh_id = sqlite3_column_int(sqlStament, 0);
            if (sqlite3_column_text(self.sqlStament, 1)) {
                model.lafh_room_id = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
            }
            model.lafh_item_id = sqlite3_column_int(sqlStament, 2);
            model.lafh_color_id = sqlite3_column_int(sqlStament, 3);
            model.lafh_quantity = sqlite3_column_int(sqlStament, 4);
            model.lafh_user_id = sqlite3_column_int(sqlStament, 5);
            if (sqlite3_column_text(sqlStament, 6)) {
                model.lafh_date = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 6)];
            }
            
            if (sqlite3_column_text(sqlStament, 7)) {
                model.lafh_item_name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 7)];
            }
            
            if (sqlite3_column_text(sqlStament, 8)) {
                model.lafh_category_name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 8)];
            }
            
            if (sqlite3_column_text(sqlStament, 9)) {
                model.lafh_color_code = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 9)];
            }
            [array addObject:model];
            
            status = sqlite3_step(self.sqlStament);
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                [array removeAllObjects];
                goto RE_ACCESS_DB;
            }
        }
    }
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    return array;
}

//delete database after 24h
-(int) deleteDatabaseAfterSomeDays{
    NSInteger result = 1;
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd 00:00:00"];
    int countDate = (int)[[UserManagerV2 sharedUserManager].currentUserAccessRight countPostingHistoryDays];
    NSDate *dateToDelete = [[NSDate date] dateBySubtractingDays:countDate - 1];
    NSString *beforeDays = [dateFormat stringFromDate:dateToDelete];
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"DELETE FROM %@ WHERE %@ < '%@'", LOST_FOUND_HISTORY, LAFH_date, beforeDays];
    const char *sql = [sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
        result = 0;
	
    NSInteger status = sqlite3_step(sqlStament);
    
	if (SQLITE_DONE != status) {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
		result = 0;
    }
    
    return (int)result;
}
@end
