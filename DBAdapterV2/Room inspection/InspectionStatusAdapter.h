//
//  InspectionStatusAdapter.h
//
//  Created by Thuong Nguyen.
//  Copyright 2012 __TMS__. All rights reserved.
//

#import "DatabaseAdapterV2.h"
#import "InspectionStatusModel.h"
@interface InspectionStatusAdapter : DatabaseAdapter

-(BOOL) insertInspectionStatusModel:(InspectionStatusModel *)model;
-(InspectionStatusModel *)loadInspectionStatusModelByPrimaryKey:(int)inspectionStatusId;
-(BOOL) updateInspectionStatusModel:(InspectionStatusModel *) model;
-(BOOL) deleteInspectionStatusModel:(InspectionStatusModel *) model;
@end