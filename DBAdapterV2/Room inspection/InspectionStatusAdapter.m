//
//  InspectionStatusAdapter.h
//
//  Created by Thuong Nguyen.
//  Copyright 2012 __TMS__. All rights reserved.
//

#import "InspectionStatusAdapter.h"
#import "DbDefinesV2.h"
#import "ehkDefines.h"

@implementation InspectionStatusAdapter

- (id)init
{
	self = [super init];
	if (self) {
		// Initialization code here.
	}
	return self;
    
}

-(BOOL)insertInspectionStatusModel:(InspectionStatusModel *)model {
	NSString *sqlString = [NSString stringWithFormat:@"INSERT INTO INSPECTION_STATUS ( %@, %@, %@, %@) VALUES ( ?, ?, ?, ?)" , istat_id, istat_name, istat_image, istat_lang ];
    
	const char *sql = [sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    reAccessCount++;
}
    
	if(sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {
		sqlite3_bind_int(sqlStament, 1, (int)model.istatId);
		sqlite3_bind_text(sqlStament, 2, [model.istatName UTF8String], -1, SQLITE_TRANSIENT);
		sqlite3_bind_blob(sqlStament, 3, [model.istatImage bytes], (int)[model.istatImage length], NULL);
		sqlite3_bind_text(sqlStament, 4, [model.istatLang UTF8String], -1, SQLITE_TRANSIENT);
	}
    
    NSInteger returnInt = 0;
    NSInteger status = sqlite3_step(self.sqlStament);
    
    if(SQLITE_DONE != status) {
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        
        return returnInt;
    } else {
        sqlite3_reset(self.sqlStament);
        returnInt = 1;
        return returnInt;
    }
    
}

-(BOOL)updateInspectionStatusModel:(InspectionStatusModel *)model {
	NSString *sqlString = [NSString stringWithFormat:@"UPDATE %@ SET %@ = ?, %@ = ?, %@ = ? WHERE  %@ = ?", INSPECTION_STATUS, istat_name, istat_image, istat_lang, istat_id];
	const char *sql = [sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    reAccessCount++;
}
    
	if(sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {
		sqlite3_bind_text(sqlStament, 1, [model.istatName UTF8String], -1, SQLITE_TRANSIENT);
		sqlite3_bind_blob(sqlStament, 2, [model.istatImage bytes], (int)[model.istatImage length], NULL);
		sqlite3_bind_text(sqlStament, 3, [model.istatLang UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_int(sqlStament, 4, (int)model.istatId);
	}
    NSInteger returnInt = 0;
    
    NSInteger status = sqlite3_step(self.sqlStament);
    
    if(SQLITE_DONE != status) {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return (returnInt > 0);
    } else {
        returnInt = 1;
        
        return (returnInt > 0);
    }
    
}

-(BOOL)deleteInspectionStatusModel:(InspectionStatusModel *)model {
	NSString *sqlString = [NSString stringWithFormat:@"DELETE FROM %@ WHERE %@ = ? AND %@ = ?", INSPECTION_STATUS, istat_id, istat_lang];
	const char *sql = [sqlString UTF8String];
    
	if(sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {
		sqlite3_bind_int(sqlStament, 1, (int)model.istatId);
		sqlite3_bind_text(sqlStament, 2, [model.istatLang UTF8String], -1, SQLITE_TRANSIENT);
	}
	NSInteger status = sqlite3_step(sqlStament);
	if (status == SQLITE_DONE) {
		return YES;
	}
	return  NO;
}

-(InspectionStatusModel *)loadInspectionStatusModelByPrimaryKey:(int)inspectionStatusId {
	NSString *sqlString = [NSString stringWithFormat:@"SELECT %@, %@, %@, %@ FROM %@ WHERE  %@ = ? " , istat_id, istat_name, istat_image, istat_lang, INSPECTION_STATUS, istat_id];
	const char *sql = [sqlString UTF8String];
    InspectionStatusModel *model = nil;
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    reAccessCount++;
}
    
	if(sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {
		sqlite3_bind_int(sqlStament, 1, inspectionStatusId);
        
        NSInteger status = sqlite3_step(sqlStament);
		while (status == SQLITE_ROW) {
            model = [[InspectionStatusModel alloc] init];
			model.istatId = sqlite3_column_int(sqlStament, 0);
			
            if (sqlite3_column_text(sqlStament, 1)) {
				model.istatName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
			}
			
            if (sqlite3_column_bytes(sqlStament, 2) > 0) {
				model.istatImage = [NSData dataWithBytes:sqlite3_column_blob(sqlStament, 2) length:sqlite3_column_bytes(sqlStament, 2)];
			}
            
            if (sqlite3_column_text(sqlStament, 3)) {
				model.istatLang = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
			}
            
            status = sqlite3_step(sqlStament);
		}
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        
	}
    
    
	return model;
}



@end