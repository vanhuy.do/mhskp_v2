//
//  CleaningStatusAdapterV2.h
//  mHouseKeeping
//
//  Created by TMS TRIBE on 3/6/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DatabaseAdapter.h"
#import "DbDefinesV2.h"
@class CleaningStatusModelV2;

@interface CleaningStatusAdapterV2 : DatabaseAdapter{
    
}
-(int)insert:(CleaningStatusModelV2 *) cleaningStatus;
-(CleaningStatusModelV2*)loadCleaningStatus:(CleaningStatusModelV2*) cleaningStatus;
-(NSMutableArray*)loadAllCleaningStatus;
-(int) updateCleaningStatus:(CleaningStatusModelV2*)cleaningStatus;
-(BOOL) isExist:(CleaningStatusModelV2 *) cleaningStatusModel;
-(NSMutableArray *)loadAllCleaningStatusWithCleaningStatusID:(NSInteger)clsID;
-(NSMutableArray *)loadAllCleaningStatusForSupervisor;

//get last modified date
-(NSString*)getCleaningStatusLastModifiedDate;

@property (nonatomic,retain) NSMutableArray *result;

@end
