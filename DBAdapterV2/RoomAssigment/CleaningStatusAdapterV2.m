//
//  CleaningStatusAdapterV2.m
//  mHouseKeeping
//
//  Created by TMS TRIBE on 3/6/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "CleaningStatusAdapterV2.h"
#import "CleaningStatusModelV2.h"
#import "RoomManagerV2.h"
#import "LogFileManager.h"

@implementation CleaningStatusAdapterV2

@synthesize result;

-(int)insert: (CleaningStatusModelV2 *) mcleaningStatus
{
        NSString *sqlString = [[NSString alloc] initWithFormat:@"INSERT INTO %@ (%@, %@, %@ , %@, %@) %@",
                               CLEANING_STATUS, 
                               cstat_id, 
                               cstat_name, 
                               cstat_lang,
                               cstat_image,
                               cstat_last_modified,
                               @"VALUES(?,?,?,?, ?)"];
        
        const char *sql=[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }

    reAccessCount++;
}

//        [sqlString release];
		if(sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
        {
            //NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(database));
        }
        sqlite3_bind_int(self.sqlStament,  1, mcleaningStatus.cstat_Id);
        sqlite3_bind_text(self.sqlStament, 2, [mcleaningStatus.cstat_Name UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(self.sqlStament, 3, [mcleaningStatus.cstat_Language UTF8String], -1, SQLITE_TRANSIENT);
//        sqlite3_bind_text(self.sqlStament, 4 ,[mcleaningStatus.cstat_image UTF8String],-1,SQLITE_TRANSIENT);
        
        sqlite3_bind_blob(sqlStament, 4, [mcleaningStatus.cstat_image bytes], (int)[mcleaningStatus.cstat_image length], NULL);
        sqlite3_bind_text(self.sqlStament, 5, [mcleaningStatus.cstat_last_modified UTF8String], -1, SQLITE_TRANSIENT);
    
    NSInteger returnInt = 0;
    
    NSInteger status = sqlite3_step(self.sqlStament);
    
    if(SQLITE_DONE != status) {  
        if (status == SQLITE_BUSY) {
            if([LogFileManager isLogConsole])
            {
                NSLog(@"insert++ %d",(int)status);
            }
            
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return (int)returnInt;
    } else {
        sqlite3_reset(self.sqlStament);
        returnInt = 1;
        return (int)returnInt;
    }
}

-(CleaningStatusModelV2*)loadCleaningStatus:(CleaningStatusModelV2*) mcleaningStatus
{
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@ ,%@ FROM %@ WHERE %@ = ?",
                           cstat_id,
                           cstat_name,
                           cstat_lang,
                           cstat_image,
                           CLEANING_STATUS,
                           cstat_id
                           ];
    const char *sql =[sqlString UTF8String];
    //NSLog(@"sqlstring %@",sqlString);
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }

    reAccessCount++;
}

    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
 
    }
    
    sqlite3_bind_int(self.sqlStament, 1, mcleaningStatus.cstat_Id);
    NSInteger status = sqlite3_step(sqlStament);
    
    while(status == SQLITE_ROW)
    {
        mcleaningStatus.cstat_Id=sqlite3_column_int(self.sqlStament, 0);
        mcleaningStatus.cstat_Name=sqlite3_column_text(sqlStament, 1)==nil?@"": [NSString stringWithUTF8String:(char *) sqlite3_column_text(sqlStament, 1)];
        
        if(sqlite3_column_text(sqlStament, 2)!=nil)
            mcleaningStatus.cstat_Language=[NSString stringWithUTF8String:(char *) sqlite3_column_text(sqlStament, 2)];
        
        if (sqlite3_column_bytes(sqlStament, 3) > 0) {
            mcleaningStatus.cstat_image = [NSData dataWithBytes:sqlite3_column_blob(sqlStament, 3) 
                                                         length:sqlite3_column_bytes(sqlStament, 3)];
        }
        
        status = sqlite3_step(sqlStament);
    }
    
    if (status == SQLITE_BUSY) {
        if([LogFileManager isLogConsole])
        {
            NSLog(@"loadCleaningStatus++ %d",(int)status);
        }
        if (reAccessCount < REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }

    
    
    return mcleaningStatus;
    
}

-(NSMutableArray*)loadAllCleaningStatus {
    NSMutableArray *local_result= [NSMutableArray array];
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@,%@ FROM %@ ",
                           
                           cstat_id,
                           cstat_name,
                           cstat_lang,
                           cstat_image,
                           CLEANING_STATUS
                           ];
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }

    reAccessCount++;
}

    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
     }
    
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW)
    {
        CleaningStatusModelV2 *clsModel=[[CleaningStatusModelV2 alloc] init];
        clsModel.cstat_Id=sqlite3_column_int(self.sqlStament, 0);
        clsModel.cstat_Name=sqlite3_column_text(sqlStament, 1)==nil?@"": [NSString stringWithUTF8String:(char *) sqlite3_column_text(sqlStament, 1)];
        
        clsModel.cstat_Language=sqlite3_column_text(sqlStament, 2)==nil?@"": [NSString stringWithUTF8String:(char *) sqlite3_column_text(sqlStament, 2)];
        
        if (sqlite3_column_bytes(sqlStament, 3) > 0) {
            clsModel.cstat_image = [NSData dataWithBytes:sqlite3_column_blob(sqlStament, 3) length:sqlite3_column_bytes(sqlStament, 3)];
        }	
        
        [local_result addObject:clsModel];
        status = sqlite3_step(sqlStament);
        
    }
    if (status == SQLITE_BUSY) {
        
        if (reAccessCount < REACCESS_MAX_TIMES) {
            [local_result removeAllObjects];
            goto RE_ACCESS_DB;
        }
        if (status == SQLITE_BUSY) {
            
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                [local_result removeAllObjects];
                goto RE_ACCESS_DB;
            }
        }

    }
    

    
    return local_result;
}



-(NSMutableArray *)loadAllCleaningStatusWithCleaningStatusID:(NSInteger)clsID {
    NSMutableArray *local_result= [NSMutableArray array];
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@,%@ FROM %@ ",                           
                           cstat_id,
                           cstat_name,
                           cstat_lang,
                           cstat_image,
                           CLEANING_STATUS
                           ];
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }

    reAccessCount++;
}

    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW) {
        CleaningStatusModelV2 *clsModel = [[CleaningStatusModelV2 alloc] init];
        clsModel.cstat_Id = sqlite3_column_int(self.sqlStament, 0);
        clsModel.cstat_Name = sqlite3_column_text(sqlStament, 1) == nil ? @"": [NSString stringWithUTF8String:(char *) sqlite3_column_text(sqlStament, 1)];
        
        clsModel.cstat_Language = sqlite3_column_text(sqlStament, 2) == nil ? @"": [NSString stringWithUTF8String:(char *) sqlite3_column_text(sqlStament, 2)];
        
        if (sqlite3_column_bytes(sqlStament, 3) > 0) {
            clsModel.cstat_image = [NSData dataWithBytes:sqlite3_column_blob(sqlStament, 3) length:sqlite3_column_bytes(sqlStament, 3)];
        }	
        
        if(clsID == ENUM_CLEANING_STATUS_PENDING_FIRST_TIME ||
           clsID == ENUM_CLEANING_STATUS_PENDING_SECOND_TIME) {
            if(clsModel.cstat_Id == ENUM_CLEANING_STATUS_DECLINE_SERVICE||
               clsModel.cstat_Id == ENUM_CLEANING_STATUS_SERVICE_LATER || 
               clsModel.cstat_Id == ENUM_CLEANING_STATUS_DND||
               clsModel.cstat_Id == ENUM_CLEANING_STATUS_START) {
                [local_result addObject:clsModel];
            }
        }
        
        if(clsID == ENUM_CLEANING_STATUS_START) {
            if(clsModel.cstat_Id == ENUM_CLEANING_STATUS_DECLINE_SERVICE||
               clsModel.cstat_Id == ENUM_CLEANING_STATUS_SERVICE_LATER || 
               clsModel.cstat_Id == ENUM_CLEANING_STATUS_DND||
               clsModel.cstat_Id == ENUM_CLEANING_STATUS_COMPLETED||
               clsModel.cstat_Id == ENUM_CLEANING_STATUS_PAUSE) {
                [local_result addObject:clsModel];
            }
        }
        
        if(clsID == ENUM_CLEANING_STATUS_PAUSE) {
            if(clsModel.cstat_Id == ENUM_CLEANING_STATUS_DECLINE_SERVICE||
               clsModel.cstat_Id == ENUM_CLEANING_STATUS_SERVICE_LATER || 
               clsModel.cstat_Id == ENUM_CLEANING_STATUS_DND||
               clsModel.cstat_Id == ENUM_CLEANING_STATUS_COMPLETED||
               clsModel.cstat_Id == ENUM_CLEANING_STATUS_START) {
                [local_result addObject:clsModel];
            }
        }
        
        if(clsID == ENUM_CLEANING_STATUS_SERVICE_LATER){
            if(clsModel.cstat_Id == ENUM_CLEANING_STATUS_DECLINE_SERVICE||
               clsModel.cstat_Id == ENUM_CLEANING_STATUS_SERVICE_LATER || 
               clsModel.cstat_Id == ENUM_CLEANING_STATUS_DND||
               clsModel.cstat_Id == ENUM_CLEANING_STATUS_START){
                [local_result addObject:clsModel];
            }
        }
        
        status = sqlite3_step(sqlStament);        
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            [local_result removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }    

    return local_result;
}

-(NSMutableArray *)loadAllCleaningStatusForSupervisor {
    NSMutableArray *local_result= [NSMutableArray array];
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@ FROM %@ WHERE %@ IN (?, ?, ?)",                           
                           cstat_id,
                           cstat_name,
                           cstat_lang,
                           cstat_image,
                           CLEANING_STATUS,
                           cstat_id
                           ];
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    
    sqlite3_bind_int(self.sqlStament, 1, ENUM_CLEANING_STATUS_PENDING_FIRST_TIME);
    sqlite3_bind_int(self.sqlStament, 2, ENUM_CLEANING_STATUS_SERVICE_LATER);
    sqlite3_bind_int(self.sqlStament, 3, ENUM_CLEANING_STATUS_DECLINE_SERVICE);
    
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW) {
        CleaningStatusModelV2 *clsModel = [[CleaningStatusModelV2 alloc] init];
        clsModel.cstat_Id = sqlite3_column_int(self.sqlStament, 0);
        clsModel.cstat_Name = sqlite3_column_text(sqlStament, 1) == nil ? @"": [NSString stringWithUTF8String:(char *) sqlite3_column_text(sqlStament, 1)];
        
        clsModel.cstat_Language = sqlite3_column_text(sqlStament, 2) == nil ? @"": [NSString stringWithUTF8String:(char *) sqlite3_column_text(sqlStament, 2)];
        
        if (sqlite3_column_bytes(sqlStament, 3) > 0) {
            clsModel.cstat_image = [NSData dataWithBytes:sqlite3_column_blob(sqlStament, 3) length:sqlite3_column_bytes(sqlStament, 3)];
        }	
        
        [local_result addObject:clsModel];
        
        status = sqlite3_step(sqlStament);        
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            [local_result removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }    
    
    return local_result;
}

-(int)updateCleaningStatus:(CleaningStatusModelV2 *)cleaningStatus{

        NSString *sqlString = [[NSString alloc] initWithFormat:@"UPDATE %@ SET %@ = ?, %@ = ?, %@ = ?, %@ = ? WHERE %@ = ?", CLEANING_STATUS, cstat_id, cstat_name, cstat_image, cstat_lang, cstat_id];
        const char *sql = [sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }

    reAccessCount++;
}

//        [sqlString release];
		if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
        {
        }
    
    sqlite3_bind_int(self.sqlStament, 1, cleaningStatus.cstat_Id);
    sqlite3_bind_text(self.sqlStament, 2, [cleaningStatus.cstat_Name UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_blob(sqlStament, 3, [cleaningStatus.cstat_image bytes], (int)[cleaningStatus.cstat_image length], NULL);
    sqlite3_bind_text(self.sqlStament, 4, [cleaningStatus.cstat_Language UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_int(self.sqlStament, 5, cleaningStatus.cstat_Id);
    
    NSInteger returnInt = 0;    
    NSInteger status = sqlite3_step(self.sqlStament);
    if(SQLITE_DONE != status) {   
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return (int)returnInt;
    } else { 
        returnInt = 1;        
        return (int)returnInt;
    }
}

-(BOOL) isExist:(CleaningStatusModelV2 *)cleaningStatusModel
{
    BOOL isExist = false;
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@ FROM %@ WHERE %@ = ?",
                           @"rowid",
                           CLEANING_STATUS,
                           cstat_id
                           ];
    const char *sql =[sqlString UTF8String];
    //NSLog(@"sqlstring %@",sqlString);
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
        return isExist;
    }
    
    sqlite3_bind_int(self.sqlStament, 1, cleaningStatusModel.cstat_Id);
    NSInteger status = sqlite3_step(sqlStament);
    
    while(status == SQLITE_ROW)
    {
        isExist = (sqlite3_column_int(self.sqlStament, 0)!=0);
        
        status = sqlite3_step(sqlStament);
    }
    
    if (status == SQLITE_BUSY) {
        if([LogFileManager isLogConsole])
        {
            NSLog(@"loadCleaningStatus++ %d",(int)status);
        }
        if (reAccessCount < REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    return isExist;
}

-(NSString*)getCleaningStatusLastModifiedDate
{
    NSMutableString * sqlString=[[NSMutableString alloc] initWithFormat:@"SELECT MAX(%@) FROM %@", cstat_last_modified, CLEANING_STATUS];
    const char *sql = [sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    reAccessCount ++;
}
    
    NSString *lastModified;
    
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        
        status = sqlite3_step(sqlStament);
        while(status == SQLITE_ROW) {
            
            if(sqlite3_column_text(sqlStament, 0)){
                lastModified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 0)];
            }
            return lastModified;
        }
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    return  lastModified;
}

@end
