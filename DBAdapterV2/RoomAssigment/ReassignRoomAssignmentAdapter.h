//
//  ReassignRoomAssignmentAdapter.h
//
//  Created by Thuong Nguyen.
//  Copyright 2012 __TMS__. All rights reserved.
//

#import "DatabaseAdapter.h"
#import "ReassignRoomAssignmentModel.h"
@interface ReassignRoomAssignmentAdapter : DatabaseAdapter

-(BOOL) insertReassignRoomAssignmentModel:(ReassignRoomAssignmentModel *)model;

-(ReassignRoomAssignmentModel *) loadReassignRoomAssignmentModelByPrimaryKey:(ReassignRoomAssignmentModel *) model;

-(BOOL) updateReassignRoomAssignmentModel:(ReassignRoomAssignmentModel *) model;

-(BOOL) deleteReassignRoomAssignmentModel:(ReassignRoomAssignmentModel *) model;

-(NSInteger) numberOfMustPostReAssignRecordsWithUserId:(NSInteger) userId;

-(NSMutableArray *) loadAllReassignRoomAssignmentByUserId:(NSInteger) userId;
@end 