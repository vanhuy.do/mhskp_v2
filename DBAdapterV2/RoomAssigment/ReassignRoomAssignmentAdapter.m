//
//  ReassignRoomAssignmentAdapter.h
//
//  Created by Thuong Nguyen.
//  Copyright 2012 __TMS__. All rights reserved.
//

#import "ReassignRoomAssignmentAdapter.h"
#import "DbDefinesV2.h"
#import "ehkDefines.h"

@implementation ReassignRoomAssignmentAdapter

- (id)init
{
	self = [super init];
	if (self) {
		// Initialization code here.
	}
	return self;

}

-(BOOL)insertReassignRoomAssignmentModel:(ReassignRoomAssignmentModel *)model {
	NSString *sqlString = [NSString stringWithFormat:@"INSERT INTO REASSIGN_ROOM_ASSIGNMENT ( %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)" , reassign_user_id, reassign_room_id, reassign_datetime, reasign_expected_cleaning_time, reassign_room_status, reassign_cleaning_status, reassign_housekeeper_id, reassign_room_type, reassign_floor_id, reassign_guest_profile_id, reassign_hotel_id, reassign_ra_id, reassign_room_remark, ra_room_cleaning_credit];

	const char *sql = [sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }

    reAccessCount++;
}

	if(sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
	}
    
    sqlite3_bind_int(sqlStament, 1, (int)model.reassignUserId);
    sqlite3_bind_text(self.sqlStament, 2 , [model.reassignRoomId UTF8String], -1,SQLITE_TRANSIENT);
    sqlite3_bind_text(sqlStament, 3, [model.reassignDatetime UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_int(sqlStament, 4, (int)model.reasignExpectedCleaningTime);
    sqlite3_bind_int(sqlStament, 5, (int)model.reassignRoomStatus);
    sqlite3_bind_int(sqlStament, 6, (int)model.reassignCleaningStatus);
    sqlite3_bind_int(sqlStament, 7, (int)model.reassignHousekeeperId);
    sqlite3_bind_int(sqlStament, 8, (int)model.reassignRoomType);
    sqlite3_bind_int(sqlStament, 9, (int)model.reassignFloorId);
    sqlite3_bind_int(sqlStament, 10, (int)model.reassignGuestProfileId);
    sqlite3_bind_int(sqlStament, 11, (int)model.reassignHotelId);
    sqlite3_bind_int(sqlStament, 12, (int)model.reassignRaId);
    sqlite3_bind_text(self.sqlStament, 13 , [model.reassignRemark UTF8String], -1,SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 14 , [(model.cleaningCredit?:@"") UTF8String], -1,SQLITE_TRANSIENT);
    
    NSInteger returnInt = 0;
    
    NSInteger status = sqlite3_step(self.sqlStament);
    
    if(SQLITE_DONE != status) {  
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return returnInt;
    } else {
        sqlite3_reset(self.sqlStament);
        returnInt = 1;
        return returnInt;
    }

}

-(BOOL)updateReassignRoomAssignmentModel:(ReassignRoomAssignmentModel *)model {
	NSString *sqlString = [NSString stringWithFormat:@"UPDATE %@ SET %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = %@ WHERE  %@ = ? AND %@ = ?", REASSIGN_ROOM_ASSIGNMENT, reassign_room_id, reassign_datetime, reasign_expected_cleaning_time, reassign_room_status, reassign_cleaning_status, reassign_housekeeper_id, reassign_room_type, reassign_floor_id, reassign_guest_profile_id, reassign_hotel_id, reassign_room_remark, ra_room_cleaning_credit, model.cleaningCredit?:@"", reassign_user_id, reassign_ra_id];
    
	const char *sql = [sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }

    reAccessCount++;
}

	if(sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
	}
    
    sqlite3_bind_text(self.sqlStament, 1 , [model.reassignRoomId UTF8String], -1,SQLITE_TRANSIENT);
    sqlite3_bind_text(sqlStament, 2, [model.reassignDatetime UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_int(sqlStament, 3, (int)model.reasignExpectedCleaningTime);
    sqlite3_bind_int(sqlStament, 4, (int)model.reassignRoomStatus);
    sqlite3_bind_int(sqlStament, 5, (int)model.reassignCleaningStatus);
    sqlite3_bind_int(sqlStament, 6, (int)model.reassignHousekeeperId);
    sqlite3_bind_int(sqlStament, 7, (int)model.reassignRoomType);
    sqlite3_bind_int(sqlStament, 8, (int)model.reassignFloorId);
    sqlite3_bind_int(sqlStament, 9, (int)model.reassignGuestProfileId);
    sqlite3_bind_int(sqlStament, 10,(int) model.reassignHotelId);
    sqlite3_bind_text(sqlStament, 11, [model.reassignRemark UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_int(sqlStament, 12, (int)model.reassignUserId);
    sqlite3_bind_int(sqlStament, 13, (int)model.reassignRaId);
    
    
    NSInteger returnInt = 0;    
    NSInteger status = sqlite3_step(self.sqlStament);
    if(SQLITE_DONE != status) {   
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return returnInt;
    } else { 
        returnInt = 1;        
        return returnInt; 
    }

}

-(BOOL)deleteReassignRoomAssignmentModel:(ReassignRoomAssignmentModel *)model {
    NSInteger result = 1;
	NSString *sqlString = [NSString stringWithFormat:@"DELETE FROM %@ WHERE %@ = ? AND %@ = ?", REASSIGN_ROOM_ASSIGNMENT, reassign_user_id, reassign_ra_id];
	const char *sql = [sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }

    reAccessCount++;
}
    
	if(sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL) != SQLITE_OK) result = 0;
		sqlite3_bind_int(sqlStament, 1, (int)model.reassignUserId);
		sqlite3_bind_int(sqlStament, 2, (int)model.reassignRaId);
	
    
    NSInteger status = sqlite3_step(sqlStament);
    
	if (SQLITE_DONE != status) {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
		result = 0;
    }
    
    return result;
}

-(ReassignRoomAssignmentModel *)loadReassignRoomAssignmentModelByPrimaryKey:(ReassignRoomAssignmentModel *)model {
	NSString *sqlString = [NSString stringWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ FROM %@ WHERE  %@ = ? AND %@ = ?" , reassign_user_id, reassign_room_id, reassign_datetime, reasign_expected_cleaning_time, reassign_room_status, reassign_cleaning_status, reassign_housekeeper_id, reassign_room_type, reassign_floor_id, reassign_guest_profile_id, reassign_hotel_id, reassign_ra_id, reassign_room_remark, ra_room_cleaning_credit, REASSIGN_ROOM_ASSIGNMENT, reassign_user_id, reassign_ra_id];
	const char *sql = [sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}

	if(sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
	}
    
    sqlite3_bind_int(sqlStament, 1, (int)model.reassignUserId);
    sqlite3_bind_int(sqlStament, 2, (int)model.reassignRaId);
    NSInteger status = sqlite3_step(sqlStament);
    while (status == SQLITE_ROW) {
        model.reassignUserId = sqlite3_column_int(sqlStament, 0);
        if (sqlite3_column_text(self.sqlStament, 1)) {
            model.reassignRoomId = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
        }
        if (sqlite3_column_text(sqlStament, 2)) {
            model.reassignDatetime = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
        }
        model.reasignExpectedCleaningTime = sqlite3_column_int(sqlStament, 3);
        model.reassignRoomStatus = sqlite3_column_int(sqlStament, 4);
        model.reassignCleaningStatus = sqlite3_column_int(sqlStament, 5);
        model.reassignHousekeeperId = sqlite3_column_int(sqlStament, 6);
        model.reassignRoomType = sqlite3_column_int(sqlStament, 7);
        model.reassignFloorId = sqlite3_column_int(sqlStament, 8);
        model.reassignGuestProfileId = sqlite3_column_int(sqlStament, 9);
        model.reassignHotelId = sqlite3_column_int(sqlStament, 10);
        model.reassignRaId = sqlite3_column_int(sqlStament, 11);
        if (sqlite3_column_text(sqlStament, 12)) {
            model.reassignRemark = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 12)];
        }
        if (sqlite3_column_text(sqlStament, 13)) {
            model.cleaningCredit = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 13)];
        }
        return model;
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }

	return nil;
}

-(NSInteger)numberOfMustPostReAssignRecordsWithUserId:(NSInteger)userId {
    NSString *sqlString = [NSString stringWithFormat:@"SELECT COUNT(*) FROM %@ WHERE %@ = %d", 
                           REASSIGN_ROOM_ASSIGNMENT, 
                           
                           reassign_user_id,
                           (int)userId
                           ];
    const char *sql = [sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}

    if (sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    
    NSInteger status = sqlite3_step(sqlStament);
    while (status == SQLITE_ROW) {
        return sqlite3_column_int(sqlStament, 0);
    }
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
   
    }
    

    
    return 0;
}

-(NSMutableArray *)loadAllReassignRoomAssignmentByUserId:(NSInteger)userId {
    NSString *sqlString = [NSString stringWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ FROM %@ WHERE  %@ = %d" ,
                           reassign_user_id, 
                           reassign_room_id, 
                           reassign_datetime, 
                           reasign_expected_cleaning_time, 
                           reassign_room_status, 
                           reassign_cleaning_status, 
                           reassign_housekeeper_id, 
                           reassign_room_type, 
                           reassign_floor_id, 
                           reassign_guest_profile_id, 
                           reassign_hotel_id, 
                           reassign_ra_id,
                           reassign_room_remark,
                           ra_room_cleaning_credit,
                           REASSIGN_ROOM_ASSIGNMENT,
                           
                           reassign_user_id, 
                           (int)userId
                           ];
	const char *sql = [sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }

    reAccessCount++;
}

    
	NSMutableArray *results = [NSMutableArray array];
    
	if(sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
	}
    
    NSInteger status = sqlite3_step(sqlStament);
    while (status == SQLITE_ROW) {
        ReassignRoomAssignmentModel *model = [[ReassignRoomAssignmentModel alloc] init];
        
        model.reassignUserId = sqlite3_column_int(sqlStament, 0);
        if (sqlite3_column_text(self.sqlStament, 1)) {
            model.reassignRoomId = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
        }
        if (sqlite3_column_text(sqlStament, 2)) {
            model.reassignDatetime = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
        }
        model.reasignExpectedCleaningTime = sqlite3_column_int(sqlStament, 3);
        model.reassignRoomStatus = sqlite3_column_int(sqlStament, 4);
        model.reassignCleaningStatus = sqlite3_column_int(sqlStament, 5);
        model.reassignHousekeeperId = sqlite3_column_int(sqlStament, 6);
        model.reassignRoomType = sqlite3_column_int(sqlStament, 7);
        model.reassignFloorId = sqlite3_column_int(sqlStament, 8);
        model.reassignGuestProfileId = sqlite3_column_int(sqlStament, 9);
        model.reassignHotelId = sqlite3_column_int(sqlStament, 10);
        model.reassignRaId = sqlite3_column_int(sqlStament, 11);
        if (sqlite3_column_text(sqlStament, 12)) {
            model.reassignRemark = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 12)];
        }
        if (sqlite3_column_text(sqlStament, 13)) {
            model.cleaningCredit = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 13)];
        }
        
        [results addObject:model];
        status = sqlite3_step(sqlStament);
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            [results removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }

    
	return results;
}

@end
