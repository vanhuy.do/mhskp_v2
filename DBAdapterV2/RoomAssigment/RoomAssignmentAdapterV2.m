//
//  RoomAssignmentAdapter.m
//  eHouseKeeping
//
//  Created by KhanhNguyen on 6/3/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "RoomAssignmentAdapterV2.h"
#import "DbDefinesV2.h"
#import "UserManagerV2.h"
#import "ehkDefines.h"
#import "RoomAssignmentAdapterV2Demo.h"

@implementation RoomAssignmentAdapterV2
@synthesize result;

+ (RoomAssignmentAdapterV2*) createRoomAssignmentAdapter
{
    RoomAssignmentAdapterV2* sharedRoomAssignmentAdapterInstace;
    if(isDemoMode) {
        sharedRoomAssignmentAdapterInstace = [[RoomAssignmentAdapterV2Demo alloc] init];
    } else {
        sharedRoomAssignmentAdapterInstace = [[RoomAssignmentAdapterV2 alloc] init];
    }
    
    return sharedRoomAssignmentAdapterInstace;
}

-(int)insertRoomAssignmentData:(RoomAssignmentModelV2*) roomAssignment
{
        NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"%@ %@ ( %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@,%@) Values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?)",
                                      @"insert into",
                                      ROOM_ASSIGNMENT,
                                      ra_id,
                                      ra_room_id,
                                      ra_user_id,
                                      ra_assigned_date,
                                      ra_prioprity,
                                      ra_post_status,
                                      ra_last_modified,
                                      ra_priority_sort_order,
                                      ra_housekeeper_id,
                                      ra_room_status_id,
                                      ra_room_cleaning_status_id,
                                      ra_room_inspection_status_id,
                                      ra_room_expected_clean_time,
                                      ra_room_expected_inspection_time,
                                      ra_guest_profile_id,
                                      ra_first_name,
                                      ra_last_name,
                                      ra_guest_first_name,
                                      ra_guest_last_name,
                                      ra_guest_arrival_time,
                                      ra_guest_departure_time,
                                      ra_kind_of_room,
                                      ra_is_mock_room,
                                      ra_is_reassigned_room,
                                      ra_inspected_score,
                                      ra_last_cleaning_date,
                                      ra_zone_id,
                                      ra_room_cleaning_credit,
                                      ra_room_checklist_remark];
        
        const char *sql = [sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount++;
    }

    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
    {
    }
    
    sqlite3_bind_int (self.sqlStament, 1, roomAssignment.roomAssignment_Id);
    sqlite3_bind_text(self.sqlStament, 2 , [roomAssignment.roomAssignment_RoomId UTF8String], -1,SQLITE_TRANSIENT);
    sqlite3_bind_int (self.sqlStament, 3, roomAssignment.roomAssignment_UserId);
    sqlite3_bind_text(self.sqlStament, 4, [roomAssignment.roomAssignment_AssignedDate UTF8String], -1,SQLITE_TRANSIENT);
    sqlite3_bind_int (self.sqlStament, 5, roomAssignment.roomAssignment_Priority);
    sqlite3_bind_int (self.sqlStament, 6, roomAssignment.roomAssignment_PostStatus);
    sqlite3_bind_text(self.sqlStament, 7, [roomAssignment.roomAssignment_LastModified UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_int(self.sqlStament, 8, (int)roomAssignment.roomAssignment_Priority_Sort_Order);
    sqlite3_bind_int(sqlStament, 9, (int)roomAssignment.roomAssignmentHousekeeperId);
    
    sqlite3_bind_int(sqlStament, 10, (int)roomAssignment.roomAssignmentRoomStatusId);
    sqlite3_bind_int(sqlStament, 11, (int)roomAssignment.roomAssignmentRoomCleaningStatusId);
    sqlite3_bind_int(sqlStament, 12, (int)roomAssignment.roomAssignmentRoomInspectionStatusId);
    
    sqlite3_bind_int(sqlStament, 13, (int)roomAssignment.roomAssignmentRoomExpectedCleaningTime);
    
    sqlite3_bind_int(sqlStament, 14, (int)roomAssignment.roomAssignmentRoomExpectedInspectionTime);
    
    sqlite3_bind_int(sqlStament, 15, (int)roomAssignment.roomAssignmentGuestProfileId);
    
    if (roomAssignment.roomAssignmentFirstName) {
        sqlite3_bind_text(sqlStament, 16, [roomAssignment.roomAssignmentFirstName UTF8String], -1, SQLITE_TRANSIENT);
    } else {
        sqlite3_bind_text(sqlStament, 16, [@"" UTF8String], -1, SQLITE_TRANSIENT);
    }
    
    if (roomAssignment.roomAssignmentLastName) {
        sqlite3_bind_text(sqlStament, 17, [roomAssignment.roomAssignmentLastName UTF8String], -1, SQLITE_TRANSIENT);
    } else {
        sqlite3_bind_text(sqlStament, 17, [@"" UTF8String], -1, SQLITE_TRANSIENT);
    }
    
    if (roomAssignment.roomAssignmentGuestFirstName) {
        sqlite3_bind_text(sqlStament, 18, [roomAssignment.roomAssignmentGuestFirstName UTF8String], -1, SQLITE_TRANSIENT);
    } else {
        sqlite3_bind_text(sqlStament, 18, [@"" UTF8String], -1, SQLITE_TRANSIENT);
    }
    
    if (roomAssignment.roomAssignmentGuestLastName) {
        sqlite3_bind_text(sqlStament, 19, [roomAssignment.roomAssignmentGuestLastName UTF8String], -1, SQLITE_TRANSIENT);
    } else {
        sqlite3_bind_text(sqlStament, 19, [@"" UTF8String], -1, SQLITE_TRANSIENT);
    }
    
    if (roomAssignment.raGuestArrivalTime) {
        sqlite3_bind_text(sqlStament, 20, [roomAssignment.raGuestArrivalTime UTF8String], -1, SQLITE_TRANSIENT);
    }
    else {
        sqlite3_bind_text(sqlStament, 20, [@"" UTF8String], -1, SQLITE_TRANSIENT);
    }
    
    if (roomAssignment.raGuestDepartureTime) {
        sqlite3_bind_text(sqlStament, 21, [roomAssignment.raGuestDepartureTime UTF8String], -1, SQLITE_TRANSIENT);
    }
    else {
        sqlite3_bind_text(sqlStament, 21, [@"" UTF8String], -1, SQLITE_TRANSIENT);
    }
    
    sqlite3_bind_int(sqlStament, 22, (int)roomAssignment.raKindOfRoom);
    sqlite3_bind_int(sqlStament, 23, (int)roomAssignment.raIsMockRoom);
    
//    if (roomAssignment.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_PENDING_SECOND_TIME) {
//        roomAssignment.raIsReassignedRoom = 1;
//    }
//    else {
//        roomAssignment.raIsReassignedRoom = 0;
//    }
    
    sqlite3_bind_int(sqlStament, 24, (int)roomAssignment.raIsReassignedRoom);
    
    sqlite3_bind_int(sqlStament, 25, (int)roomAssignment.roomAssignmentInspectedScore);
//        sqlite3_reset(self.sqlStament);
    
    if(roomAssignment.roomAssignment_LastCleaningDate.length <= 0){
        roomAssignment.roomAssignment_LastCleaningDate = @"";
    }
    sqlite3_bind_text(self.sqlStament, 26 , [roomAssignment.roomAssignment_LastCleaningDate UTF8String], -1,SQLITE_TRANSIENT);
    if (roomAssignment.roomAssignmentZoneId) {
        sqlite3_bind_int(sqlStament, 27, (int)roomAssignment.roomAssignmentZoneId);
    }else{
        sqlite3_bind_int(sqlStament, 27, -1);
    }
    sqlite3_bind_text(sqlStament, 28, [roomAssignment.roomAssignmentCleaningCredit?:@"" UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(sqlStament, 29, [roomAssignment.roomAssignmentChecklistRemark?:@"" UTF8String], -1, SQLITE_TRANSIENT);
    NSInteger returnInt = 0;
    NSInteger status = sqlite3_step(sqlStament);
    
   
    
    if(SQLITE_DONE != status)
    { 
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return (int)returnInt;
    }
    else
    { 
        return 1;
    } 
}
//@ update RoomAssignment Data. - in para: Object RoomAssignment.
-(int)updateRoomAssignmentData:(RoomAssignmentModelV2*)roomAssignment
{
   
        NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"UPDATE %@ SET %@=?, %@=?, %@=?, %@=?, %@=?, %@=?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = %@, %@ = ? WHERE %@ = %d AND %@ = %d",
                                      ROOM_ASSIGNMENT,
                                      ra_room_id,
                                      ra_assigned_date,
                                      ra_prioprity,
                                      ra_post_status,
                                      ra_last_modified, 
                                      ra_priority_sort_order,
                                      ra_housekeeper_id,
                                      ra_room_status_id,
                                      ra_room_cleaning_status_id,
                                      ra_room_inspection_status_id,
                                      ra_room_expected_clean_time,
                                      ra_room_expected_inspection_time,
                                      ra_guest_profile_id,
                                      ra_first_name,
                                      ra_last_name,
                                      ra_guest_first_name,
                                      ra_guest_last_name,
                                      ra_guest_arrival_time,
                                      ra_guest_departure_time,
                                      ra_kind_of_room,
                                      ra_is_mock_room,
                                      ra_is_reassigned_room,
                                      ra_inspected_score,
                                      ra_last_cleaning_date,
                                      ra_zone_id,
                                      ra_room_cleaning_credit,
                                      roomAssignment.roomAssignmentCleaningCredit?:@"",
                                      ra_room_checklist_remark,
                                      ra_id,
                                      roomAssignment.roomAssignment_Id,
                                      ra_user_id,
                                      roomAssignment.roomAssignment_UserId
                                      ];
        
        const char *sql=[sqlString UTF8String];
        NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }

        reAccessCount++;
    }

    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
    {
    }
    
    sqlite3_bind_text(self.sqlStament, 1 , [roomAssignment.roomAssignment_RoomId UTF8String], -1,SQLITE_TRANSIENT);
    if (roomAssignment.roomAssignment_AssignedDate != nil) {
        sqlite3_bind_text(self.sqlStament, 2, [roomAssignment.roomAssignment_AssignedDate UTF8String], -1,SQLITE_TRANSIENT);
    } else {
        sqlite3_bind_text(self.sqlStament, 2, nil, -1, SQLITE_TRANSIENT);
    }
    
    sqlite3_bind_int (self.sqlStament, 3, roomAssignment.roomAssignment_Priority);
    sqlite3_bind_int (self.sqlStament, 4, roomAssignment.roomAssignment_PostStatus);
    sqlite3_bind_text(self.sqlStament, 5, [roomAssignment.roomAssignment_LastModified UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_int(self.sqlStament, 6, (int)roomAssignment.roomAssignment_Priority_Sort_Order);
    
    sqlite3_bind_int(sqlStament, 7, (int)roomAssignment.roomAssignmentHousekeeperId);
    sqlite3_bind_int(sqlStament, 8, (int)roomAssignment.roomAssignmentRoomStatusId);
    sqlite3_bind_int(sqlStament, 9, (int)roomAssignment.roomAssignmentRoomCleaningStatusId);
    sqlite3_bind_int(sqlStament, 10, (int)roomAssignment.roomAssignmentRoomInspectionStatusId);
    sqlite3_bind_int(sqlStament, 11, (int)roomAssignment.roomAssignmentRoomExpectedCleaningTime);
    sqlite3_bind_int(sqlStament, 12, (int)roomAssignment.roomAssignmentRoomExpectedInspectionTime);
    sqlite3_bind_int(sqlStament, 13, (int)roomAssignment.roomAssignmentGuestProfileId);
    sqlite3_bind_text(sqlStament, 14, [roomAssignment.roomAssignmentFirstName UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(sqlStament, 15, [roomAssignment.roomAssignmentLastName UTF8String], -1, SQLITE_TRANSIENT);
    if (roomAssignment.roomAssignmentGuestFirstName) {
        sqlite3_bind_text(sqlStament, 16, [roomAssignment.roomAssignmentGuestFirstName UTF8String], -1, SQLITE_TRANSIENT);
    } else {
        sqlite3_bind_text(sqlStament, 16, [@"" UTF8String], -1, SQLITE_TRANSIENT);
    }
    
    if (roomAssignment.roomAssignmentGuestLastName) {
        sqlite3_bind_text(sqlStament, 17, [roomAssignment.roomAssignmentGuestLastName UTF8String], -1, SQLITE_TRANSIENT);
    } else {
        sqlite3_bind_text(sqlStament, 17, [@"" UTF8String], -1, SQLITE_TRANSIENT);
    }
    
    if (roomAssignment.raGuestArrivalTime) {
        sqlite3_bind_text(sqlStament, 18, [roomAssignment.raGuestArrivalTime UTF8String], -1, SQLITE_TRANSIENT);
    } else {
        sqlite3_bind_text(sqlStament, 18, [@"" UTF8String], -1, SQLITE_TRANSIENT);
    }
    
    if (roomAssignment.raGuestDepartureTime) {
        sqlite3_bind_text(sqlStament, 19, [roomAssignment.raGuestDepartureTime UTF8String], -1, SQLITE_TRANSIENT);
    } else {
        sqlite3_bind_text(sqlStament, 19, [@"" UTF8String], -1, SQLITE_TRANSIENT);
    }
    
    sqlite3_bind_int(sqlStament, 20, (int)roomAssignment.raKindOfRoom);
    sqlite3_bind_int(sqlStament, 21, (int)roomAssignment.raIsMockRoom);
    sqlite3_bind_int(sqlStament, 22, (int)roomAssignment.raIsReassignedRoom);
    sqlite3_bind_int(sqlStament, 23, (int)roomAssignment.roomAssignmentInspectedScore);
    if(roomAssignment.roomAssignment_LastCleaningDate.length <= 0){
        roomAssignment.roomAssignment_LastCleaningDate = @"";
    }
    sqlite3_bind_text(self.sqlStament, 24 , [roomAssignment.roomAssignment_LastCleaningDate UTF8String], -1,SQLITE_TRANSIENT);
    if (roomAssignment.roomAssignmentZoneId) {
        sqlite3_bind_int(sqlStament, 25, (int)roomAssignment.roomAssignmentZoneId);
    } else {
        sqlite3_bind_int(sqlStament, 25, -1);
    }
    if (roomAssignment.roomAssignmentChecklistRemark) {
        sqlite3_bind_text(sqlStament, 26, [roomAssignment.roomAssignmentChecklistRemark UTF8String], -1, SQLITE_TRANSIENT);
    } else {
        sqlite3_bind_text(sqlStament, 26, [@"" UTF8String], -1, SQLITE_TRANSIENT);
    }
    NSInteger returnInt = 0;
    NSInteger status = sqlite3_step(self.sqlStament);
    if(SQLITE_DONE != status) {   
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return (int)returnInt;
    } else { 
        returnInt = 1;        
        return (int)returnInt;
    }
}

//An rework
-(int)updateClleaningStatusRoomAssignmentModel:(RoomAssignmentModelV2*) roomAssignment
{
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"UPDATE %@ SET %@=?, %@=?, %@=?, %@=?, %@=?, %@=? WHERE %@ = %d AND %@ = %d",
                                  ROOM_ASSIGNMENT,
                                  ra_id,
                                  ra_room_id,
                                  ra_prioprity,
                                  ra_user_id,
                                  ra_room_cleaning_status_id,
                                  ra_is_reassigned_room,
                                  
                                  ra_id,
                                  roomAssignment.roomAssignment_Id,
                                  ra_user_id,
                                  roomAssignment.roomAssignment_UserId
                                  ];
    
    
    const char *sql=[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
    {
    }
    
    sqlite3_bind_int (self.sqlStament, 1, roomAssignment.roomAssignment_Id );
    sqlite3_bind_text(self.sqlStament, 2 , [roomAssignment.roomAssignment_RoomId UTF8String], -1,SQLITE_TRANSIENT);
    sqlite3_bind_int (self.sqlStament, 3, roomAssignment.roomAssignment_Priority);
    sqlite3_bind_int(self.sqlStament, 4, roomAssignment.roomAssignment_UserId);
    sqlite3_bind_int(sqlStament, 5, (int)roomAssignment.roomAssignmentRoomCleaningStatusId);
    sqlite3_bind_int(sqlStament, 6, (roomAssignment.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_PENDING_SECOND_TIME)?1:0);
  
    NSInteger returnInt = 0;    
    NSInteger status = sqlite3_step(self.sqlStament);
    if(SQLITE_DONE != status) {   
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return (int)returnInt;
    } else { 
        returnInt = 1;        
        return (int)returnInt;
    }
 
}
//@ delete roomAssignment 
-(int)deleteRoomAssignmentData:(RoomAssignmentModelV2*) roomAssignment
{
    NSInteger result1=1;
   
        NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"delete from %@ where %@ = ?",ROOM_ASSIGNMENT,ra_id];
        
		const char *sql=[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }

    reAccessCount++;
}

		if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) result1=0;
	
	
    sqlite3_bind_int(self.sqlStament, 1,roomAssignment.roomAssignment_Id);
	
    NSInteger status = sqlite3_step(sqlStament);
    
	if (SQLITE_DONE != status) {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
		result = 0;
    }
    
    return (int)result1;
}

-(void)deleteOldRoomAssignmentsWithUserId:(NSInteger)userId {
    
    //[Hao Tran/20141013] - For fixing BUG TRACK ID 12984: delete everything in room assignment
    /*
    NSString *currentDate = nil;
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    [f setDateFormat:@"yyyy-MM-dd"];
    currentDate = [NSString stringWithFormat:@"%@ 00:00:00", [f stringFromDate:[NSDate date]]];
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"DELETE FROM %@ WHERE %@ < '%@' AND %@ = %d",ROOM_ASSIGNMENT, 
                                  ra_assigned_date, 
                                  currentDate,
                                  
                                  ra_user_id,
                                  userId
                                  ];
    */
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"DELETE FROM %@ WHERE %@ = %d", ROOM_ASSIGNMENT,
                                  
                                  ra_user_id,
                                  (int)userId
                                  ];
    //[Hao Tran/20141013] - END
    
    const char *sql=[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
    {
        return;
    }
	
    NSInteger status = sqlite3_step(sqlStament);
    
	if (SQLITE_DONE != status) {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }
}

-(NSMutableArray*)loadRoomAssignmentData:(BOOL)isComplete
{
    
    
    NSMutableString *sqlString;
    int userID=[[UserManagerV2 sharedUserManager] currentUser].userId;
    
   
    sqlString = [[NSMutableString alloc] initWithFormat:@"select  %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ from %@ where %@ = ? AND %@ = 0",
                 ra_id,
                 ra_room_id,
                 ra_user_id,
                 ra_assigned_date,
                 ra_prioprity,
                 ra_post_status,
                 ra_last_modified,
                 ra_priority_sort_order,
                 ra_housekeeper_id,
                 ra_room_status_id,
                 ra_room_cleaning_status_id,
                 ra_room_inspection_status_id,
                 ra_room_expected_clean_time,
                 ra_room_expected_inspection_time,
                 ra_guest_profile_id,
                 ra_first_name,
                 ra_last_name,
                 ra_guest_first_name,
                 ra_guest_last_name,
                 ra_guest_arrival_time,
                 ra_guest_departure_time,
                 ra_kind_of_room,
                 ra_is_mock_room,
                 ra_is_reassigned_room,
                 ra_last_cleaning_date,
                 
                 ROOM_ASSIGNMENT,
                 ra_user_id,
                 
                 ra_delete
                 ];  
        
    NSMutableArray* result1=[NSMutableArray array];
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }

    reAccessCount++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {

    }
    
    //sqlite3_bind_int(self.sqlStament, 1, roomAssignment.roomAssignment_Id);
    sqlite3_bind_int(self.sqlStament, 1, userID);
    //int loopStep=0;
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW) 
    {
        RoomAssignmentModelV2 *roomAssignmentModel=[[RoomAssignmentModelV2 alloc]  init];
        roomAssignmentModel.roomAssignment_Id = sqlite3_column_int(sqlStament, 0);
        if (sqlite3_column_text(self.sqlStament, 1)) {
            roomAssignmentModel.roomAssignment_RoomId = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
        }
        roomAssignmentModel.roomAssignment_UserId=sqlite3_column_int(sqlStament, 2);
        if (sqlite3_column_text(self.sqlStament, 3) != nil) {
            roomAssignmentModel.roomAssignment_AssignedDate=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
        }
        roomAssignmentModel.roomAssignment_Priority=sqlite3_column_int(sqlStament, 4);
        roomAssignmentModel.roomAssignment_PostStatus=sqlite3_column_int(sqlStament, 5);
        if (sqlite3_column_text(self.sqlStament, 6)) {
            roomAssignmentModel.roomAssignment_LastModified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 6)];
        }
        roomAssignmentModel.roomAssignment_Priority_Sort_Order = sqlite3_column_int(self.sqlStament, 7);
        
        roomAssignmentModel.roomAssignmentHousekeeperId = sqlite3_column_int(sqlStament, 8);
        
        roomAssignmentModel.roomAssignmentRoomStatusId = sqlite3_column_int(sqlStament, 9);
        
        roomAssignmentModel.roomAssignmentRoomCleaningStatusId = sqlite3_column_int(sqlStament, 10);
        
        roomAssignmentModel.roomAssignmentRoomInspectionStatusId = sqlite3_column_int(sqlStament, 11);
        
        roomAssignmentModel.roomAssignmentRoomExpectedCleaningTime = sqlite3_column_int(sqlStament, 12);
        
        roomAssignmentModel.roomAssignmentRoomExpectedInspectionTime = sqlite3_column_int(sqlStament, 13);
        
        roomAssignmentModel.roomAssignmentGuestProfileId = sqlite3_column_int(sqlStament, 14);
        
        if (sqlite3_column_text(sqlStament, 15)) {
            roomAssignmentModel.roomAssignmentFirstName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 15)];
        }
        
        if (sqlite3_column_text(sqlStament, 16)) {
            roomAssignmentModel.roomAssignmentLastName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 16)];
        }
        
        if (sqlite3_column_text(sqlStament, 17)) {
            roomAssignmentModel.roomAssignmentGuestFirstName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 17)];
        }
        
        if (sqlite3_column_text(sqlStament, 18)) {
            roomAssignmentModel.roomAssignmentGuestLastName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 18)];
        }
        
        if (sqlite3_column_text(sqlStament, 19)) {
            roomAssignmentModel.raGuestArrivalTime = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 19)];
        }
        
        if (sqlite3_column_text(sqlStament, 20)) {
            roomAssignmentModel.raGuestDepartureTime = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 20)];
        }
        
        roomAssignmentModel.raKindOfRoom = sqlite3_column_int(sqlStament, 21);
        roomAssignmentModel.raIsMockRoom = sqlite3_column_int(sqlStament, 22);
        roomAssignmentModel.raIsReassignedRoom = sqlite3_column_int(sqlStament, 23);
        if (sqlite3_column_text(self.sqlStament, 24)) {
            roomAssignmentModel.roomAssignment_LastCleaningDate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 24)];
        }
        
        [result1 addObject:roomAssignmentModel];
        status = sqlite3_step(sqlStament);
    }
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            [result1 removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }

    
    
    return result1;
}

-(RoomAssignmentModelV2 *)loadRoomAsignment:(RoomAssignmentModelV2 *)raModel{    
    NSMutableString *sqlString;
    sqlString = [[NSMutableString alloc] initWithFormat:@"select %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@,%@ from %@ where %@ = ? AND %@ = ?",
                 ra_id,
                 ra_room_id,
                 ra_user_id,
                 ra_assigned_date,
                 ra_prioprity,
                 ra_post_status,
                 ra_last_modified,
                 ra_priority_sort_order,
                 ra_housekeeper_id,
                 ra_room_status_id,
                 ra_room_cleaning_status_id,
                 ra_room_inspection_status_id,
                 ra_room_expected_clean_time,
                 ra_room_expected_inspection_time,
                 ra_guest_profile_id,
                 ra_first_name,
                 ra_last_name,
                 ra_guest_first_name,
                 ra_guest_last_name,
                 ra_guest_arrival_time,
                 ra_guest_departure_time,
                 ra_kind_of_room,
                 ra_is_mock_room,
                 ra_is_reassigned_room,
                 ra_inspected_score,
                 ra_last_cleaning_date,
                 ra_room_cleaning_credit,
                 ra_room_checklist_remark,
                 ROOM_ASSIGNMENT,
                 
                 ra_id,
                 ra_user_id
                 //ra_delete
                 ];
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }

    reAccessCount++;
}

    NSInteger status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status != SQLITE_OK) 
    {
    }
    
    sqlite3_bind_int64(self.sqlStament, 1, raModel.roomAssignment_Id);
    sqlite3_bind_int(self.sqlStament, 2, raModel.roomAssignment_UserId);
     status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW) 
    {
        raModel.roomAssignment_Id = sqlite3_column_int(sqlStament, 0);
        if (sqlite3_column_text(self.sqlStament, 1)) {
            raModel.roomAssignment_RoomId = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
        }
        raModel.roomAssignment_UserId=sqlite3_column_int(sqlStament, 2);
        if (sqlite3_column_text(self.sqlStament, 3) != nil) {
            raModel.roomAssignment_AssignedDate=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
        }
        raModel.roomAssignment_Priority=sqlite3_column_int(sqlStament, 4);
        raModel.roomAssignment_PostStatus=sqlite3_column_int(sqlStament, 5);
        if (sqlite3_column_text(self.sqlStament, 6)) {
            raModel.roomAssignment_LastModified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 6)];
        }
        raModel.roomAssignment_Priority_Sort_Order = sqlite3_column_int(self.sqlStament, 7);
        raModel.roomAssignmentHousekeeperId = sqlite3_column_int(sqlStament, 8);
        
        raModel.roomAssignmentRoomStatusId = sqlite3_column_int(sqlStament, 9);
        
        raModel.roomAssignmentRoomCleaningStatusId = sqlite3_column_int(sqlStament, 10);
        
        raModel.roomAssignmentRoomInspectionStatusId = sqlite3_column_int(sqlStament, 11);
        
        raModel.roomAssignmentRoomExpectedCleaningTime = sqlite3_column_int(sqlStament, 12);
        
        raModel.roomAssignmentRoomExpectedInspectionTime = sqlite3_column_int(sqlStament, 13);
        
        raModel.roomAssignmentGuestProfileId = sqlite3_column_int(sqlStament, 14);
        
        if (sqlite3_column_text(sqlStament, 15)) {
            raModel.roomAssignmentFirstName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 15)];
        }
        
        if (sqlite3_column_text(sqlStament, 16)) {
            raModel.roomAssignmentLastName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 16)];
        }
        
        if (sqlite3_column_text(sqlStament, 17)) {
            raModel.roomAssignmentGuestFirstName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 17)];
        }
        
        if (sqlite3_column_text(sqlStament, 18)) {
            raModel.roomAssignmentGuestLastName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 18)];
        }
        
        if (sqlite3_column_text(sqlStament, 19)) {
            raModel.raGuestArrivalTime = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 19)];
        }
        
        if (sqlite3_column_text(sqlStament, 20)) {
            raModel.raGuestDepartureTime = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 20)];
        }
        
        raModel.raKindOfRoom = sqlite3_column_int(sqlStament, 21);
        raModel.raIsMockRoom = sqlite3_column_int(sqlStament, 22);
        raModel.raIsReassignedRoom = sqlite3_column_int(sqlStament, 23);
        raModel.roomAssignmentInspectedScore = sqlite3_column_int(sqlStament, 24);
        if (sqlite3_column_text(self.sqlStament, 25)) {
            raModel.roomAssignment_LastCleaningDate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 25)];
        }
        if (sqlite3_column_text(self.sqlStament, 26)) {
            raModel.roomAssignmentCleaningCredit = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 26)];
        }
        if (sqlite3_column_text(self.sqlStament, 27)) {
            raModel.roomAssignmentChecklistRemark = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 27)];
        }
        status = sqlite3_step(sqlStament);
    }
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }

    
    return raModel;
}


-(RoomAssignmentModelV2 *)loadRoomAsignmentByRoomIdAndUserID:(RoomAssignmentModelV2 *)raModel{    
    NSMutableString *sqlString;
    sqlString = [[NSMutableString alloc] initWithFormat:@"select  %@, %@, %@, %@, %@, %@,  %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ from %@ where %@ = ? AND %@ = ? AND %@ = 0",
                 ra_id,
                 ra_room_id,
                 ra_user_id,
                 ra_assigned_date,
                 ra_prioprity,
                 ra_post_status,
                 ra_last_modified,
                 ra_priority_sort_order,
                 ra_housekeeper_id,
                 ra_room_status_id,
                 ra_room_cleaning_status_id,
                 ra_room_inspection_status_id,
                 ra_room_expected_clean_time,
                 ra_room_expected_inspection_time,
                 ra_guest_profile_id,
                 ra_first_name,
                 ra_last_name,
                 ra_guest_first_name,
                 ra_guest_last_name,
                 ra_guest_arrival_time,
                 ra_guest_departure_time,
                 ra_kind_of_room,
                 ra_is_mock_room,
                 ra_is_reassigned_room,
                 ra_inspected_score,
                 ra_last_cleaning_date,
                 
                 ROOM_ASSIGNMENT,
                 
                 ra_room_id,
                 ra_user_id,
                 ra_delete
                 ];  
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }

    reAccessCount++;
}
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) 
    {
    }
    
    sqlite3_bind_text(self.sqlStament, 1 , [raModel.roomAssignment_RoomId UTF8String], -1,SQLITE_TRANSIENT);
    sqlite3_bind_int(self.sqlStament, 2, raModel.roomAssignment_UserId);
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW) 
    {
        raModel.roomAssignment_Id = sqlite3_column_int(sqlStament, 0);
        if (sqlite3_column_text(self.sqlStament, 1)) {
            raModel.roomAssignment_RoomId = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
        }
        raModel.roomAssignment_UserId=sqlite3_column_int(sqlStament, 2);
        if (sqlite3_column_text(self.sqlStament, 3) != nil) {
            raModel.roomAssignment_AssignedDate=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
        }
        raModel.roomAssignment_Priority=sqlite3_column_int(sqlStament, 4);
        raModel.roomAssignment_PostStatus=sqlite3_column_int(sqlStament, 5);
        if (sqlite3_column_text(self.sqlStament, 6)) {
            raModel.roomAssignment_LastModified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 6)];
        }
        raModel.roomAssignment_Priority_Sort_Order = sqlite3_column_int(self.sqlStament, 7);
        raModel.roomAssignmentHousekeeperId = sqlite3_column_int(sqlStament, 8);
        
        raModel.roomAssignmentRoomStatusId = sqlite3_column_int(sqlStament, 9);
        
        raModel.roomAssignmentRoomCleaningStatusId = sqlite3_column_int(sqlStament, 10);
        
        raModel.roomAssignmentRoomInspectionStatusId = sqlite3_column_int(sqlStament, 11);
        
        raModel.roomAssignmentRoomExpectedCleaningTime = sqlite3_column_int(sqlStament, 12);
        
        raModel.roomAssignmentRoomExpectedInspectionTime = sqlite3_column_int(sqlStament, 13);
        
        raModel.roomAssignmentGuestProfileId = sqlite3_column_int(sqlStament, 14);
        
        if (sqlite3_column_text(sqlStament, 15)) {
            raModel.roomAssignmentFirstName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 15)];
        }
        
        if (sqlite3_column_text(sqlStament, 16)) {
            raModel.roomAssignmentLastName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 16)];
        }
        
        if (sqlite3_column_text(sqlStament, 17)) {
            raModel.roomAssignmentGuestFirstName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 17)];
        }
        
        if (sqlite3_column_text(sqlStament, 18)) {
            raModel.roomAssignmentGuestLastName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 18)];
        }
        
        if (sqlite3_column_text(sqlStament, 19)) {
            raModel.raGuestArrivalTime = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 19)];
        }
        
        if (sqlite3_column_text(sqlStament, 20)) {
            raModel.raGuestDepartureTime = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 20)];
        }
        
        raModel.raKindOfRoom = sqlite3_column_int(sqlStament, 21);
        raModel.raIsMockRoom = sqlite3_column_int(sqlStament, 22);
        raModel.raIsReassignedRoom = sqlite3_column_int(sqlStament, 23);
        raModel.roomAssignmentInspectedScore = sqlite3_column_int(sqlStament, 24);
        if (sqlite3_column_text(self.sqlStament, 25)) {
            raModel.roomAssignment_LastCleaningDate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 25)];
        }
        
        status = sqlite3_step(sqlStament);
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }

    
    return raModel;
}

-(NSString*)getRoomAssignmentLastModifiedDateByUserId:(int)userId
{
    NSMutableString * sqlString = [[NSMutableString alloc] initWithFormat:@"SELECT MAX(%@) FROM %@ WHERE %@ = %d", ra_last_modified, ROOM_ASSIGNMENT, ra_user_id, userId];
    const char *sql=[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    reAccessCount ++;
}
    
    NSString *lastModified;
    
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        
        status = sqlite3_step(sqlStament);
        while(status == SQLITE_ROW) {
            
            if(sqlite3_column_text(sqlStament, 0)){
                lastModified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 0)];
            }
            return lastModified;
        }
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    return  lastModified;
}

-(NSMutableArray *)loadRoomAssignmentsByUserId:(NSInteger)userId {
    NSMutableArray *resultRoomAssignment = [NSMutableArray array];
    NSString *sqlString = [NSString stringWithFormat:@"select  %@, %@, %@ , %@, %@,  %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ , %@, %@, %@ from %@ where %@ = %d AND %@ = 0",
                           ra_id,
                           ra_room_id,
                           ra_user_id,
                           ra_assigned_date,
                           ra_prioprity,
                           ra_post_status,
                           ra_last_modified,
                           ra_housekeeper_id,
                           ra_room_status_id,
                           ra_room_cleaning_status_id,
                           ra_room_inspection_status_id,
                           ra_room_expected_clean_time,
                           ra_room_expected_inspection_time,
                           ra_guest_profile_id,
                           ra_first_name,
                           ra_last_name,
                           ra_guest_first_name,
                           ra_guest_last_name,
                           ra_guest_arrival_time,
                           ra_guest_departure_time,
                           ra_kind_of_room,
                           ra_is_mock_room,
                           ra_is_reassigned_room,
                           ra_inspected_score,
                           ra_last_cleaning_date,
                           
                           ROOM_ASSIGNMENT,
                           
                           ra_user_id,
                           (int)userId,
                           ra_delete
                 ];  
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }

    reAccessCount++;
}

    
    RoomAssignmentModelV2 *ramModel = nil;
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW) 
    {
        
        ramModel = [[RoomAssignmentModelV2 alloc] init];
        ramModel.roomAssignment_Id = sqlite3_column_int(sqlStament, 0);
        if (sqlite3_column_text(self.sqlStament, 1)) {
            ramModel.roomAssignment_RoomId = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
        }
        ramModel.roomAssignment_UserId=sqlite3_column_int(sqlStament, 2);
        if (sqlite3_column_text(self.sqlStament, 3) != nil) {
            ramModel.roomAssignment_AssignedDate=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
        }
        ramModel.roomAssignment_Priority=sqlite3_column_int(sqlStament, 4);
        ramModel.roomAssignment_PostStatus=sqlite3_column_int(sqlStament, 5);
        if (sqlite3_column_text(self.sqlStament, 6)) {
            ramModel.roomAssignment_LastModified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 6)];
        }
        
        ramModel.roomAssignmentHousekeeperId = sqlite3_column_int(sqlStament, 7);
        
        ramModel.roomAssignmentRoomStatusId = sqlite3_column_int(sqlStament, 8);
        
        ramModel.roomAssignmentRoomCleaningStatusId = sqlite3_column_int(sqlStament, 9);
        
        ramModel.roomAssignmentRoomInspectionStatusId = sqlite3_column_int(sqlStament, 10);
        
        ramModel.roomAssignmentRoomExpectedCleaningTime = sqlite3_column_int(sqlStament, 11);
        
        ramModel.roomAssignmentRoomExpectedInspectionTime = sqlite3_column_int(sqlStament, 12);
        
        ramModel.roomAssignmentGuestProfileId = sqlite3_column_int(sqlStament, 13);
        
        if (sqlite3_column_text(sqlStament, 14)) {
            ramModel.roomAssignmentFirstName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 14)];
        }
        
        if (sqlite3_column_text(sqlStament, 15)) {
            ramModel.roomAssignmentLastName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 15)];
        }
        
        if (sqlite3_column_text(sqlStament, 16)) {
            ramModel.roomAssignmentGuestFirstName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 16)];
        }
        
        if (sqlite3_column_text(sqlStament, 17)) {
            ramModel.roomAssignmentGuestLastName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 17)];
        }
        
        if (sqlite3_column_text(sqlStament, 18)) {
            ramModel.raGuestArrivalTime = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 18)];
        }
        
        if (sqlite3_column_text(sqlStament, 19)) {
            ramModel.raGuestDepartureTime = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 19)];
        }
        
        ramModel.raKindOfRoom = sqlite3_column_int(sqlStament, 20);
        ramModel.raIsMockRoom = sqlite3_column_int(sqlStament, 21);
        ramModel.raIsReassignedRoom = sqlite3_column_int(sqlStament, 22);
        ramModel.roomAssignmentInspectedScore = sqlite3_column_int(sqlStament, 23);
        if (sqlite3_column_text(self.sqlStament, 24)) {
            ramModel.roomAssignment_LastCleaningDate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 24)];
        }
        [resultRoomAssignment addObject:ramModel];
        status = sqlite3_step(sqlStament);
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            [resultRoomAssignment removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }

    
    return resultRoomAssignment;
}

-(NSMutableArray *)loadRoomAssignmentsBySuperVisorId:(int)supervisorId houseKeeperId:(int)houseKeeperId {
    NSMutableArray *resultRoomAssignment = [NSMutableArray array];
    NSString *sqlString = [NSString stringWithFormat:@"select  %@, %@, %@ , %@, %@,  %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ , %@, %@, %@ from %@ where %@ = %d AND %@ = %d AND %@ = 0",
                           ra_id,
                           ra_room_id,
                           ra_user_id,
                           ra_assigned_date,
                           ra_prioprity,
                           ra_post_status,
                           ra_last_modified,
                           ra_housekeeper_id,
                           ra_room_status_id,
                           ra_room_cleaning_status_id,
                           ra_room_inspection_status_id,
                           ra_room_expected_clean_time,
                           ra_room_expected_inspection_time,
                           ra_guest_profile_id,
                           ra_first_name,
                           ra_last_name,
                           ra_guest_first_name,
                           ra_guest_last_name,
                           ra_guest_arrival_time,
                           ra_guest_departure_time,
                           ra_kind_of_room,
                           ra_is_mock_room,
                           ra_is_reassigned_room,
                           ra_inspected_score,
                           ra_last_cleaning_date,
                           
                           ROOM_ASSIGNMENT,
                           
                           ra_user_id,
                           supervisorId,
                           ra_housekeeper_id,
                           houseKeeperId,
                           
                           ra_delete
                           ];
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    
    RoomAssignmentModelV2 *ramModel = nil;
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW)
    {
        
        ramModel = [[RoomAssignmentModelV2 alloc] init];
        ramModel.roomAssignment_Id = sqlite3_column_int(sqlStament, 0);
        if (sqlite3_column_text(self.sqlStament, 1)) {
            ramModel.roomAssignment_RoomId = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
        }
        ramModel.roomAssignment_UserId=sqlite3_column_int(sqlStament, 2);
        if (sqlite3_column_text(self.sqlStament, 3) != nil) {
            ramModel.roomAssignment_AssignedDate=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
        }
        ramModel.roomAssignment_Priority=sqlite3_column_int(sqlStament, 4);
        ramModel.roomAssignment_PostStatus=sqlite3_column_int(sqlStament, 5);
        if (sqlite3_column_text(self.sqlStament, 6)) {
            ramModel.roomAssignment_LastModified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 6)];
        }
        
        ramModel.roomAssignmentHousekeeperId = sqlite3_column_int(sqlStament, 7);
        
        ramModel.roomAssignmentRoomStatusId = sqlite3_column_int(sqlStament, 8);
        
        ramModel.roomAssignmentRoomCleaningStatusId = sqlite3_column_int(sqlStament, 9);
        
        ramModel.roomAssignmentRoomInspectionStatusId = sqlite3_column_int(sqlStament, 10);
        
        ramModel.roomAssignmentRoomExpectedCleaningTime = sqlite3_column_int(sqlStament, 11);
        
        ramModel.roomAssignmentRoomExpectedInspectionTime = sqlite3_column_int(sqlStament, 12);
        
        ramModel.roomAssignmentGuestProfileId = sqlite3_column_int(sqlStament, 13);
        
        if (sqlite3_column_text(sqlStament, 14)) {
            ramModel.roomAssignmentFirstName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 14)];
        }
        
        if (sqlite3_column_text(sqlStament, 15)) {
            ramModel.roomAssignmentLastName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 15)];
        }
        
        if (sqlite3_column_text(sqlStament, 16)) {
            ramModel.roomAssignmentGuestFirstName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 16)];
        }
        
        if (sqlite3_column_text(sqlStament, 17)) {
            ramModel.roomAssignmentGuestLastName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 17)];
        }
        
        if (sqlite3_column_text(sqlStament, 18)) {
            ramModel.raGuestArrivalTime = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 18)];
        }
        
        if (sqlite3_column_text(sqlStament, 19)) {
            ramModel.raGuestDepartureTime = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 19)];
        }
        
        ramModel.raKindOfRoom = sqlite3_column_int(sqlStament, 20);
        ramModel.raIsMockRoom = sqlite3_column_int(sqlStament, 21);
        ramModel.raIsReassignedRoom = sqlite3_column_int(sqlStament, 22);
        ramModel.roomAssignmentInspectedScore = sqlite3_column_int(sqlStament, 23);
        if (sqlite3_column_text(self.sqlStament, 24)) {
            ramModel.roomAssignment_LastCleaningDate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 24)];
        }
        [resultRoomAssignment addObject:ramModel];
        status = sqlite3_step(sqlStament);
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            [resultRoomAssignment removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    
    return resultRoomAssignment;
}

-(NSMutableArray *) loadAllCurrentDateRoomAssignmentsWithUserId:(NSInteger) userId {
    NSMutableArray *resultRoomAssignment = [[NSMutableArray alloc] init] ;
    NSMutableString *sqlString;
    
    NSString *currentDate = nil;
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    [f setDateFormat:@"yyyy-MM-dd"];
    currentDate = [NSString stringWithFormat:@"%@ 00:00:00", [f stringFromDate:[NSDate date]]];
    
    sqlString = [[NSMutableString alloc] initWithFormat:@"select  %@, %@, %@ , %@, %@,  %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ from %@ where %@ = %d AND %@ > '%@' AND %@ = 0",
                 ra_id,
                 ra_room_id,
                 ra_user_id,
                 ra_assigned_date,
                 ra_prioprity,
                 ra_post_status,
                 ra_last_modified,
                 ra_housekeeper_id,
                 ra_room_status_id,
                 ra_room_cleaning_status_id,
                 ra_room_inspection_status_id,
                 ra_room_expected_clean_time,
                 ra_room_expected_inspection_time,
                 ra_guest_profile_id,
                 ra_first_name,
                 ra_last_name,
                 ra_guest_first_name,
                 ra_guest_last_name,
                 ra_guest_arrival_time,
                 ra_guest_departure_time,
                 ra_kind_of_room,
                 ra_is_mock_room,
                 ra_is_reassigned_room,
                 ra_inspected_score,
                 ra_last_cleaning_date,
                 
                 ROOM_ASSIGNMENT,
                 ra_user_id,
                 (int)userId,
                 
                 ra_assigned_date,
                 currentDate,
                 
                 ra_delete
                 ];  
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
        return resultRoomAssignment;
    }
    
    RoomAssignmentModelV2 *raModel = nil;
    
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW) 
    {
        raModel = [[RoomAssignmentModelV2 alloc] init];
        raModel.roomAssignment_Id = sqlite3_column_int(sqlStament, 0);
        if (sqlite3_column_text(self.sqlStament, 1)) {
            raModel.roomAssignment_RoomId = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
        }
        raModel.roomAssignment_UserId=sqlite3_column_int(sqlStament, 2);
        if (sqlite3_column_text(self.sqlStament, 3) != nil) {
            raModel.roomAssignment_AssignedDate=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
        }
        raModel.roomAssignment_Priority=sqlite3_column_int(sqlStament, 4);
        raModel.roomAssignment_PostStatus=sqlite3_column_int(sqlStament, 5);
        if (sqlite3_column_text(self.sqlStament, 6)) {
            raModel.roomAssignment_LastModified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 6)];
        }
        
        raModel.roomAssignmentHousekeeperId = sqlite3_column_int(sqlStament, 7);
        
        raModel.roomAssignmentRoomStatusId = sqlite3_column_int(sqlStament, 8);
        
        raModel.roomAssignmentRoomCleaningStatusId = sqlite3_column_int(sqlStament, 9);
        
        raModel.roomAssignmentRoomInspectionStatusId = sqlite3_column_int(sqlStament, 10);
        
        raModel.roomAssignmentRoomExpectedCleaningTime = sqlite3_column_int(sqlStament, 11);
        
        raModel.roomAssignmentRoomExpectedInspectionTime = sqlite3_column_int(sqlStament, 12);
        
        raModel.roomAssignmentGuestProfileId = sqlite3_column_int(sqlStament, 13);
        
        if (sqlite3_column_text(sqlStament, 14)) {
            raModel.roomAssignmentFirstName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 14)];
        }
        
        if (sqlite3_column_text(sqlStament, 15)) {
            raModel.roomAssignmentLastName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 15)];
        }
        
        if (sqlite3_column_text(sqlStament, 16)) {
            raModel.roomAssignmentGuestFirstName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 16)];
        }
        
        if (sqlite3_column_text(sqlStament, 17)) {
            raModel.roomAssignmentGuestLastName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 17)];
        }
        
        if (sqlite3_column_text(sqlStament, 18)) {
            raModel.raGuestArrivalTime = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 18)];
        }
        
        if (sqlite3_column_text(sqlStament, 19)) {
            raModel.raGuestDepartureTime = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 19)];
        }
        
        raModel.raKindOfRoom = sqlite3_column_int(sqlStament, 20);
        raModel.raIsMockRoom = sqlite3_column_int(sqlStament, 21);
        raModel.raIsReassignedRoom = sqlite3_column_int(sqlStament, 22);
        raModel.roomAssignmentInspectedScore = sqlite3_column_int(sqlStament, 23);
        if (sqlite3_column_text(self.sqlStament, 24)) {
            raModel.roomAssignment_LastCleaningDate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 24)];
        }
        
        [resultRoomAssignment addObject:raModel];
        status = sqlite3_step(sqlStament);
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            [resultRoomAssignment removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    return resultRoomAssignment;
}

-(NSMutableArray *)loadRoomAsignmentByUserID:(RoomAssignmentModelV2 *)raModel{   
    NSMutableArray *resultRoomAssignment=[NSMutableArray array] ;
    NSMutableString *sqlString;
    
    sqlString = [[NSMutableString alloc] initWithFormat:@"select  %@, %@, %@ , %@, %@,  %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ from %@ where %@ = ? AND %@ = 0",
                 ra_id,
                 ra_room_id,
                 ra_user_id,
                 ra_assigned_date,
                 ra_prioprity,
                 ra_post_status,
                 ra_last_modified,
                 ra_housekeeper_id,
                 ra_room_status_id,
                 ra_room_cleaning_status_id,
                 ra_room_inspection_status_id,
                 ra_room_expected_clean_time,
                 ra_room_expected_inspection_time,
                 ra_guest_profile_id,
                 ra_first_name,
                 ra_last_name,
                 ra_guest_first_name,
                 ra_guest_last_name,
                 ra_guest_arrival_time,
                 ra_guest_departure_time,
                 ra_kind_of_room,
                 ra_is_mock_room,
                 ra_is_reassigned_room,
                 ra_inspected_score,
                 ra_last_cleaning_date,
                
                 ROOM_ASSIGNMENT,
                 ra_user_id,
                 
                 ra_delete];  
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }

    reAccessCount++;
}

    //RoomAssignmentModelV2 *ramModel = nil;
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    
    sqlite3_bind_int(self.sqlStament, 1, raModel.roomAssignment_UserId);
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW) 
    {
        raModel = [[RoomAssignmentModelV2 alloc] init];
        raModel.roomAssignment_Id = sqlite3_column_int(sqlStament, 0);
        if (sqlite3_column_text(self.sqlStament, 1)) {
            raModel.roomAssignment_RoomId = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
        }
        raModel.roomAssignment_UserId=sqlite3_column_int(sqlStament, 2);
        if (sqlite3_column_text(self.sqlStament, 3) != nil) {
            raModel.roomAssignment_AssignedDate=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
        }
        raModel.roomAssignment_Priority=sqlite3_column_int(sqlStament, 4);
        raModel.roomAssignment_PostStatus=sqlite3_column_int(sqlStament, 5);
        if (sqlite3_column_text(self.sqlStament, 6)) {
            raModel.roomAssignment_LastModified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 6)];
        }
        
        raModel.roomAssignmentHousekeeperId = sqlite3_column_int(sqlStament, 7);
        
        raModel.roomAssignmentRoomStatusId = sqlite3_column_int(sqlStament, 8);
        
        raModel.roomAssignmentRoomCleaningStatusId = sqlite3_column_int(sqlStament, 9);
        
        raModel.roomAssignmentRoomInspectionStatusId = sqlite3_column_int(sqlStament, 10);
        
        raModel.roomAssignmentRoomExpectedCleaningTime = sqlite3_column_int(sqlStament, 11);
        
        raModel.roomAssignmentRoomExpectedInspectionTime = sqlite3_column_int(sqlStament, 12);
        
        raModel.roomAssignmentGuestProfileId = sqlite3_column_int(sqlStament, 13);
        
        if (sqlite3_column_text(sqlStament, 14)) {
            raModel.roomAssignmentFirstName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 14)];
        }
        
        if (sqlite3_column_text(sqlStament, 15)) {
            raModel.roomAssignmentLastName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 15)];
        }
        
        if (sqlite3_column_text(sqlStament, 16)) {
            raModel.roomAssignmentGuestFirstName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 16)];
        }
        
        if (sqlite3_column_text(sqlStament, 17)) {
            raModel.roomAssignmentGuestLastName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 17)];
        }
        
        if (sqlite3_column_text(sqlStament, 18)) {
            raModel.raGuestArrivalTime = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 18)];
        }
        
        if (sqlite3_column_text(sqlStament, 19)) {
            raModel.raGuestDepartureTime = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 19)];
        }
        
        raModel.raKindOfRoom = sqlite3_column_int(sqlStament, 20);
        raModel.raIsMockRoom = sqlite3_column_int(sqlStament, 21);
        raModel.raIsReassignedRoom = sqlite3_column_int(sqlStament, 22);
        raModel.roomAssignmentInspectedScore = sqlite3_column_int(sqlStament, 23);
        if (sqlite3_column_text(self.sqlStament, 24)) {
            raModel.roomAssignment_LastCleaningDate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 24)];
        }
        
        [resultRoomAssignment addObject:raModel];
        status = sqlite3_step(sqlStament);
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            [resultRoomAssignment removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }

    return resultRoomAssignment;
}

// Get roomAssignment from userId and RoomID

-(bool)getRoomAssignmentByUserIDAndRoomId:(RoomAssignmentModelV2*)raModel;
{
    NSMutableString *sqlString;
    sqlString = [[NSMutableString alloc] initWithFormat:@"select  %@, %@, %@, %@, %@, %@,  %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ from %@ where %@=? and %@=?",
                 ra_id,
                 ra_room_id,
                 ra_user_id,
                 ra_assigned_date,
                 ra_prioprity,
                 ra_post_status,
                 ra_last_modified,
                 ra_housekeeper_id,
                 ra_room_status_id,
                 ra_room_cleaning_status_id,
                 ra_room_inspection_status_id,
                 ra_room_expected_clean_time,
                 ra_room_expected_inspection_time,
                 ra_guest_profile_id,
                 ra_first_name,
                 ra_last_name,
                 ra_guest_first_name,
                 ra_guest_last_name,
                 ra_guest_arrival_time,
                 ra_guest_departure_time,
                 ra_kind_of_room,
                 ra_is_mock_room,
                 ra_is_reassigned_room,
                 ra_inspected_score,
                 ra_last_cleaning_date,
                 
                 ROOM_ASSIGNMENT,
                 ra_user_id,
                 ra_room_id];  
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }

    reAccessCount++;
}

    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    
    sqlite3_bind_int(sqlStament, 1, raModel.roomAssignment_UserId);
    sqlite3_bind_text(self.sqlStament, 2 , [raModel.roomAssignment_RoomId UTF8String], -1,SQLITE_TRANSIENT);
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW) 
    {
        raModel.roomAssignment_Id = sqlite3_column_int(sqlStament, 0);
        if (sqlite3_column_text(self.sqlStament, 1)) {
            raModel.roomAssignment_RoomId = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
        }
        raModel.roomAssignment_UserId=sqlite3_column_int(sqlStament, 2);
        if (sqlite3_column_text(self.sqlStament, 3) != nil) {
            raModel.roomAssignment_AssignedDate=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
        }
        raModel.roomAssignment_Priority=sqlite3_column_int(sqlStament, 4);
        raModel.roomAssignment_PostStatus=sqlite3_column_int(sqlStament, 5);
        if (sqlite3_column_text(self.sqlStament, 6) != nil) {
            raModel.roomAssignment_LastModified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 6)];
        }
//        raModel.roomAssignment_LastModified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 6)];
        
        raModel.roomAssignmentHousekeeperId = sqlite3_column_int(sqlStament, 7);
        
        raModel.roomAssignmentRoomStatusId = sqlite3_column_int(sqlStament, 8);
        
        raModel.roomAssignmentRoomStatusId = sqlite3_column_int(sqlStament, 9);
        
        raModel.roomAssignmentRoomInspectionStatusId = sqlite3_column_int(sqlStament, 10);
        
        raModel.roomAssignmentRoomExpectedCleaningTime = sqlite3_column_int(sqlStament, 11);
        
        raModel.roomAssignmentRoomExpectedInspectionTime = sqlite3_column_int(sqlStament, 12);
        
        raModel.roomAssignmentGuestProfileId = sqlite3_column_int(sqlStament, 13);
        
        if (sqlite3_column_text(sqlStament, 14)) {
            raModel.roomAssignmentFirstName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 14)];
        }
        
        if (sqlite3_column_text(sqlStament, 15)) {
            raModel.roomAssignmentLastName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 15)];
        }
        
        if (sqlite3_column_text(sqlStament, 16)) {
            raModel.roomAssignmentGuestFirstName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 16)];
        }
        
        if (sqlite3_column_text(sqlStament, 17)) {
            raModel.roomAssignmentGuestLastName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 17)];
        }
        
        if (sqlite3_column_text(sqlStament, 18)) {
            raModel.raGuestArrivalTime = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 18)];
        }
        
        if (sqlite3_column_text(sqlStament, 19)) {
            raModel.raGuestDepartureTime = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 19)];
        }
        
        raModel.raKindOfRoom = sqlite3_column_int(sqlStament, 20);
        raModel.raIsMockRoom = sqlite3_column_int(sqlStament, 21);
        raModel.raIsReassignedRoom = sqlite3_column_int(sqlStament, 22);
        raModel.roomAssignmentInspectedScore = sqlite3_column_int(sqlStament, 23);
        if (sqlite3_column_text(self.sqlStament, 24)) {
            raModel.roomAssignment_LastCleaningDate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 24)];
        }
        
        status = sqlite3_step(sqlStament);
    }
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {                
            goto RE_ACCESS_DB;
        }
    }
    
    if (raModel.roomAssignment_Id < 1) {            
        return false;
    }
    
    return true;
}

-(bool)getRoomIdByRoomAssignment:(RoomAssignmentModelV2*)raModel
{
    NSMutableString *sqlString;
    sqlString = [[NSMutableString alloc] initWithFormat:@"select  %@, %@, %@, %@, %@, %@,  %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ from %@ where %@=?",
                 ra_id,
                 ra_room_id,
                 ra_user_id,
                 ra_assigned_date,
                 ra_prioprity,
                 ra_post_status,
                 ra_last_modified,
                 ra_housekeeper_id,
                 ra_room_status_id,
                 ra_room_cleaning_status_id,
                 ra_room_inspection_status_id,
                 ra_room_expected_clean_time,
                 ra_room_expected_inspection_time,
                 ra_guest_profile_id,
                 ra_first_name,
                 ra_last_name,
                 ra_guest_first_name,
                 ra_guest_last_name,
                 ra_guest_arrival_time,
                 ra_guest_departure_time,
                 ra_kind_of_room,
                 ra_is_mock_room,
                 ra_is_reassigned_room,
                 ra_inspected_score,
                 ra_last_cleaning_date,
                 
                 ROOM_ASSIGNMENT,
                 ra_id];
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    
    sqlite3_bind_int(sqlStament, 1, raModel.roomAssignment_Id);
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW)
    {
        raModel.roomAssignment_Id = sqlite3_column_int(sqlStament, 0);
        if (sqlite3_column_text(self.sqlStament, 1)) {
            raModel.roomAssignment_RoomId = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
        }
        raModel.roomAssignment_UserId=sqlite3_column_int(sqlStament, 2);
        if (sqlite3_column_text(self.sqlStament, 3) != nil) {
            raModel.roomAssignment_AssignedDate=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
        }
        raModel.roomAssignment_Priority=sqlite3_column_int(sqlStament, 4);
        raModel.roomAssignment_PostStatus=sqlite3_column_int(sqlStament, 5);
        if (sqlite3_column_text(self.sqlStament, 6) != nil) {
            raModel.roomAssignment_LastModified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 6)];
        }
        //        raModel.roomAssignment_LastModified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 6)];
        
        raModel.roomAssignmentHousekeeperId = sqlite3_column_int(sqlStament, 7);
        
        raModel.roomAssignmentRoomStatusId = sqlite3_column_int(sqlStament, 8);
        
        raModel.roomAssignmentRoomStatusId = sqlite3_column_int(sqlStament, 9);
        
        raModel.roomAssignmentRoomInspectionStatusId = sqlite3_column_int(sqlStament, 10);
        
        raModel.roomAssignmentRoomExpectedCleaningTime = sqlite3_column_int(sqlStament, 11);
        
        raModel.roomAssignmentRoomExpectedInspectionTime = sqlite3_column_int(sqlStament, 12);
        
        raModel.roomAssignmentGuestProfileId = sqlite3_column_int(sqlStament, 13);
        
        if (sqlite3_column_text(sqlStament, 14)) {
            raModel.roomAssignmentFirstName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 14)];
        }
        
        if (sqlite3_column_text(sqlStament, 15)) {
            raModel.roomAssignmentLastName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 15)];
        }
        
        if (sqlite3_column_text(sqlStament, 16)) {
            raModel.roomAssignmentGuestFirstName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 16)];
        }
        
        if (sqlite3_column_text(sqlStament, 17)) {
            raModel.roomAssignmentGuestLastName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 17)];
        }
        
        if (sqlite3_column_text(sqlStament, 18)) {
            raModel.raGuestArrivalTime = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 18)];
        }
        
        if (sqlite3_column_text(sqlStament, 19)) {
            raModel.raGuestDepartureTime = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 19)];
        }
        
        raModel.raKindOfRoom = sqlite3_column_int(sqlStament, 20);
        raModel.raIsMockRoom = sqlite3_column_int(sqlStament, 21);
        raModel.raIsReassignedRoom = sqlite3_column_int(sqlStament, 22);
        raModel.roomAssignmentInspectedScore = sqlite3_column_int(sqlStament, 23);
        if (sqlite3_column_text(self.sqlStament, 24)) {
            raModel.roomAssignment_LastCleaningDate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 24)];
        }
        
        status = sqlite3_step(sqlStament);
    }
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    if (raModel.roomAssignment_Id < 1) {
        return false;
    }
    
    return true;
}

-(NSInteger)maxPrioprity{
    //SELECT MAX(OrderPrice) AS LargestOrderPrice FROM Orders
    
    NSMutableString *sqlString;
    sqlString = [[NSMutableString alloc] initWithFormat:@"select  MAX(%@) from %@",
                 
                 ra_priority_sort_order,
                 ROOM_ASSIGNMENT];  
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }

    reAccessCount++;
}

    NSInteger priority = 0;                                                                            
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW) 
    {
        priority = sqlite3_column_int(sqlStament, 0);
        status = sqlite3_step(sqlStament);
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }

    
    return priority;
}


-(NSInteger)minPrioprity
{
    NSMutableString *sqlString;
    sqlString = [[NSMutableString alloc] initWithFormat:@"select  MIN(%@) from %@",
                 
                 ra_prioprity,
                 ROOM_ASSIGNMENT];  
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    NSInteger priority = 0;                                                                            
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW) 
    {
        priority = sqlite3_column_int(sqlStament, 0);
        status = sqlite3_step(sqlStament);
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    
    return priority;
}

-(NSInteger)numberOfMustPostRoomAssignmentRecordsWithSupervisorId:(NSInteger)userId {
    NSString *sqlString = [NSString stringWithFormat:@"SELECT COUNT(*) FROM %@ WHERE %@ = %d AND %@ = %d", 
                           ROOM_ASSIGNMENT, 
                           
                           ra_post_status, 
                           (int)POST_STATUS_SAVED_UNPOSTED,
                           
                           ra_user_id, 
                           (int)userId
                           ];
    const char *sql = [sqlString UTF8String];
      NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}

    
    if (sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    
    NSInteger status = sqlite3_step(sqlStament);
    while (status == SQLITE_ROW) {
        return sqlite3_column_int(sqlStament, 0);
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }

    
    return 0;
}


-(NSInteger)numberOfMustPostRoomAssignmentRecordsWithUserId:(NSInteger)userId {
    NSString *sqlString = [NSString stringWithFormat:@"SELECT COUNT(*) FROM %@ WHERE %@ = %d AND %@ = %d", 
                           ROOM_ASSIGNMENT, 
                           
                           ra_post_status, 
                           (int)POST_STATUS_SAVED_UNPOSTED,
                           
                           ra_user_id, 
                           (int)userId
                           ];
    const char *sql = [sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}

    if (sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    NSInteger status = sqlite3_step(sqlStament);
    while (status == SQLITE_ROW) {
        return sqlite3_column_int(sqlStament, 0);
        status = sqlite3_step(sqlStament);
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }

    }
    

    
    return 0;
}

-(void)setDeleteKey:(NSInteger)deleteId forRoomAssignmentId:(NSInteger)raId andUserId:(NSInteger)userId{
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"UPDATE %@ SET %@ = %d WHERE %@ = %d AND %@ = %d",
                                  ROOM_ASSIGNMENT,
                                  ra_delete,
                                  (int)deleteId,
                                  
                                  ra_id,
                                  (int)raId,
                                  ra_user_id,
                                  (int)userId
                                  ];
    
    
    const char *sql=[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
    {
        
    }
    
    NSInteger status = sqlite3_step(self.sqlStament);
    if(SQLITE_DONE != status) {   
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }
}

- (RoomTypeModel*)getRoomTypeByRoomNoWithUserId:(NSInteger)userId andRoomNumber:(NSString*)roomNumber {
    
    RoomTypeModel *rtModel = [[RoomTypeModel alloc] init];
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"SELECT %@, %@, %@, %@ FROM %@ a JOIN %@ b ON a.%@ = b.%@ JOIN %@ c ON a.%@ = c.%@ WHERE b.%@ = ? AND b.%@ = ?",
                                  ams_id,
                                  ams_name,
                                  ams_name_lang,
                                  ams_last_modified,
                                  
                                  ROOM,
                                  
                                  ROOM_ASSIGNMENT,
                                  room_id,
                                  ra_room_id,
                                  
                                  ROOM_TYPE,
                                  room_type_id,
                                  ams_id,
                                  
                                  ra_user_id,
                                  ra_room_id];
    
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    if (sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    
    sqlite3_bind_int(sqlStament, 1, (int)userId);
    sqlite3_bind_text(self.sqlStament, 2 , [roomNumber UTF8String], -1,SQLITE_TRANSIENT);
    
    NSInteger status = sqlite3_step(sqlStament);
    while (status == SQLITE_ROW) {
        
        rtModel.amsId = sqlite3_column_int(sqlStament, 0);
        if (sqlite3_column_text(self.sqlStament, 1)) {
            rtModel.amsName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
        }
        if (sqlite3_column_text(self.sqlStament, 2) != nil) {
            rtModel.amsNameLang = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
        }
        if (sqlite3_column_text(self.sqlStament, 3)) {
            rtModel.amsLastModified = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
        }
        status = sqlite3_step(sqlStament);
    }
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    if (rtModel.amsId < 1) {
        return nil;
    }
    return rtModel;
}

@end
