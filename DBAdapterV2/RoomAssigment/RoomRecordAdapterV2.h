//
//  RoomRecordAdapterV2.h
//  mHouseKeeping
//
//  Created by Nghia Truong on 3/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DatabaseAdapterV2.h"
#import "RoomRecordModelV2.h"
#import "UserManagerV2.h"
#import "DbDefinesV2.h"

@interface RoomRecordAdapterV2 : DatabaseAdapterV2 {
    
}

-(int) insertRoomRecordData:(RoomRecordModelV2 *) record;
-(int) updateRoomRecordData:(RoomRecordModelV2 *) record;
-(int) deleteRoomRecordData:(RoomRecordModelV2 *) record;
-(RoomRecordModelV2 *) loadRoomRecordData:(RoomRecordModelV2 *) record;
-(RoomRecordModelV2 *) loadRoomRecordDataByRoomIdAndUserId:(RoomRecordModelV2 *) record;
-(NSMutableArray *) loadAllRoomRecordByCurrentUser;
-(NSMutableArray *) loadAllRoomRecord;
-(NSMutableArray *)loadAllRoomRecordDataByUserId:(RoomRecordModelV2 *)record;
@end
