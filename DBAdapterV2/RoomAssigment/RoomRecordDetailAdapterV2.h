//
//  RoomRecordDetailAdapterV2.h
//  mHouseKeeping
//
//  Created by TMS TRIBE on 6/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DatabaseAdapterV2.h"
#import "RoomRecordDetailModelV2.h"
#import "DbDefinesV2.h"
@interface RoomRecordDetailAdapterV2 : DatabaseAdapterV2{
    
}

-(int) insertRoomRecordDetailData:(RoomRecordDetailModelV2 *) record;
-(int) updateRoomRecordDetailData:(RoomRecordDetailModelV2 *) record;
-(int) deleteRoomRecordDetailData:(RoomRecordDetailModelV2 *) record;
-(RoomRecordDetailModelV2 *) loadRoomRecordDetailData:(RoomRecordDetailModelV2 *) record;
-(RoomRecordDetailModelV2 *) loadRoomRecordDataDetailByRoomAssignmentIdAndUserId:(RoomRecordDetailModelV2 *) record;
-(NSMutableArray *) loadAllRoomRecordDetailByCurrentUser:(NSInteger)userID;
-(NSInteger) numberOfRoomRecordDetails:(RoomRecordDetailModelV2 *) record;
//-(NSMutableArray *)loadAllRoomRecordDataDetailByUserId:(RoomRecordDetailModelV2 *)record;
-(NSMutableArray *) loadAllRoomRecordDetailsByRoomAssignmentId:(NSInteger) raId andUserId:(NSInteger) userId;
-(NSInteger) numberOfRoomRecordDetailMustSyn;
@end
