//
//  RoomRecordDetailAdapterV2.m
//  mHouseKeeping
//
//  Created by TMS TRIBE on 6/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "RoomRecordDetailAdapterV2.h"
#import "ehkDefines.h"

@implementation RoomRecordDetailAdapterV2

-(int) insertRoomRecordDetailData:(RoomRecordDetailModelV2 *) record{
    NSString *sqlString = [[NSString alloc] initWithFormat:@"INSERT INTO %@ (  %@, %@, %@, %@) %@", 
                           ROOM_RECORDS_DETAIL, 
                            
                           recd_ra_id,
                           recd_user_id, 
                           recd_start_date, 
                           recd_stop_date,
                           @"VALUES(?, ?, ?, ?)"];
    
    const char *sql=[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }

    reAccessCount++;
}

    if(sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
        
    }
    

    //sqlite3_bind_int (self.sqlStament, 1, record.recd_Id);
    sqlite3_bind_int (self.sqlStament, 1, (int)record.recd_Ra_Id);
    sqlite3_bind_int (self.sqlStament, 2, (int)record.recd_User_Id);
    
    sqlite3_bind_text(self.sqlStament, 3, [record.recd_Start_Date UTF8String], 
                      -1,SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 4, [record.recd_Stop_Date UTF8String], 
                      -1,SQLITE_TRANSIENT);
    
    NSInteger returnInt = 0;    
    NSInteger status = sqlite3_step(self.sqlStament);
    
    if(SQLITE_DONE != status) {  
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return (int)returnInt;
    } else {
        sqlite3_reset(self.sqlStament);
        returnInt = 1;
        return (int)returnInt;
    }
}

-(int) updateRoomRecordDetailData:(RoomRecordDetailModelV2 *) record{
    NSString *sqlString = [[NSString alloc] initWithFormat:@"UPDATE %@ SET %@ = ?,%@ = ?,%@ = ?, %@ = ?, %@ = ? WHERE %@ = ?", 
                           ROOM_RECORDS_DETAIL,
                           recd_id, 
                           recd_ra_id, 
                           recd_user_id, 
                           recd_start_date, 
                           recd_stop_date,
                           recd_id];
    
    const char *sql = [sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }

    reAccessCount++;
}

    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
        
    }
    
    sqlite3_bind_int (self.sqlStament, 1, (int)record.recd_Id);
    sqlite3_bind_int (self.sqlStament, 2, (int)record.recd_Ra_Id);
    sqlite3_bind_int (self.sqlStament, 3, (int)record.recd_User_Id);
   
    sqlite3_bind_text(self.sqlStament, 4, [record.recd_Start_Date UTF8String], 
                      -1,SQLITE_TRANSIENT);
    
    sqlite3_bind_text(sqlStament, 5, [record.recd_Stop_Date UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_int (self.sqlStament, 6, (int)record.recd_Id);
    
    NSInteger returnInt = 0;    
    NSInteger status = sqlite3_step(self.sqlStament);
    if(SQLITE_DONE != status) {   
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return (int)returnInt;
    } else { 
        returnInt = 1;        
        return (int)returnInt;
    }
}

-(int) deleteRoomRecordDetailData:(RoomRecordDetailModelV2 *) record{
    NSInteger result = 1;
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"DELETE FROM %@ WHERE %@ = ?", ROOM_RECORDS_DETAIL, recd_id];
    
    const char *sql = [sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }

    reAccessCount++;
}

    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
        result = 0;
	
    sqlite3_bind_int(self.sqlStament, 1, (int)record.recd_Id);
	
    NSInteger status = sqlite3_step(sqlStament);
    
	if (SQLITE_DONE != status) {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
		result = 0;
    }
    
    return (int)result;
}

-(RoomRecordDetailModelV2 *) loadRoomRecordDetailData:(RoomRecordDetailModelV2 *) record{
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@,%@,%@, %@, %@ FROM %@ WHERE %@ = ?", 
                           recd_id, 
                           recd_ra_id, 
                           rrec_user_id, 
                           recd_start_date, 
                           recd_stop_date,
                           
                           ROOM_RECORDS_DETAIL, 
                           recd_id];
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }

    reAccessCount++;
}

    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {        
    }
    
    sqlite3_bind_int(self.sqlStament, 1, (int)record.recd_Id);
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW) {
        record.recd_Id = sqlite3_column_int(self.sqlStament, 0);
        record.recd_Ra_Id = sqlite3_column_int(self.sqlStament, 1);
        record.recd_User_Id = sqlite3_column_int(self.sqlStament, 2);
        
        if (sqlite3_column_text(self.sqlStament, 3)) {
            record.recd_Start_Date=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
        }
        if (sqlite3_column_text(self.sqlStament, 4)) {
            record.recd_Stop_Date = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
        }
        
        status = sqlite3_step(sqlStament);          
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }

    
    return record;
}

-(RoomRecordDetailModelV2 *) loadRoomRecordDataDetailByRoomAssignmentIdAndUserId:(RoomRecordDetailModelV2 *) record{
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ? AND %@ = ?", 
                           recd_id, 
                           recd_ra_id, 
                           recd_user_id, 
                           recd_start_date, 
                           recd_stop_date,
                           
                           ROOM_RECORDS_DETAIL, 
                           recd_ra_id, 
                           recd_user_id];
    
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }

    reAccessCount++;
}
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
  
    sqlite3_bind_int(self.sqlStament, 1, (int)record.recd_Ra_Id);
    sqlite3_bind_int(self.sqlStament, 2, (int)record.recd_User_Id);
    NSInteger status = sqlite3_step(sqlStament);
    
    while(status == SQLITE_ROW) {
        record.recd_Id = sqlite3_column_int(self.sqlStament, 0);
        record.recd_Ra_Id = sqlite3_column_int(self.sqlStament, 1);
        record.recd_User_Id = sqlite3_column_int(self.sqlStament, 2);
        
        if (sqlite3_column_text(self.sqlStament, 3)) {
            record.recd_Start_Date=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
        }
        if (sqlite3_column_text(self.sqlStament, 4)) {
            record.recd_Stop_Date = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
        }
        status = sqlite3_step(sqlStament);   
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    

    
    return record;

}

-(NSInteger) numberOfRoomRecordDetails:(RoomRecordDetailModelV2 *) record{
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT COUNT(*) FROM %@ WHERE %@ = ? AND %@ = ?", 
                           ROOM_RECORDS_DETAIL, 
                           recd_ra_id, 
                           recd_user_id];
    
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }

    reAccessCount++;
}

    NSInteger result = 0;
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {

    }
    
    sqlite3_bind_int(self.sqlStament, 1, (int)record.recd_Ra_Id);
    sqlite3_bind_int(self.sqlStament, 2, (int)record.recd_User_Id);
    
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW) {
        result = sqlite3_column_int(self.sqlStament, 0);
        status = sqlite3_step(sqlStament);
        
    }
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }

    return result;
}


-(NSInteger) numberOfRoomRecordDetailMustSyn{
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT COUNT(*) FROM %@ WHERE %@ = ?", 
                           ROOM_RECORDS_DETAIL, 
                           recd_user_id];
    
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }

    reAccessCount++;
}

    NSInteger result = 0;
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    
    sqlite3_bind_int(self.sqlStament, 1, [UserManagerV2 sharedUserManager].currentUser.userId);
    
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW) {
        result = sqlite3_column_int(self.sqlStament, 0);
        status = sqlite3_step(sqlStament);
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }

    }
    


    return result;
}



-(NSMutableArray *) loadAllRoomRecordDetailByCurrentUser:(NSInteger)userID{
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ? ", 
                           recd_id, 
                           recd_ra_id, 
                           recd_user_id, 
                           recd_start_date, 
                           recd_stop_date,
                           
                           ROOM_RECORDS_DETAIL, 
                           recd_user_id];
    
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }

    reAccessCount++;
}

    NSMutableArray *result = [NSMutableArray array];
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {

    }
    sqlite3_bind_int(self.sqlStament, 1,(int) userID);
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW) {
        RoomRecordDetailModelV2 *record = [[RoomRecordDetailModelV2 alloc] init];
        record.recd_Id = sqlite3_column_int(self.sqlStament, 0);
        record.recd_Ra_Id = sqlite3_column_int(self.sqlStament, 1);
        record.recd_User_Id = sqlite3_column_int(self.sqlStament, 2);
        
        if (sqlite3_column_text(self.sqlStament, 3)) {
            record.recd_Start_Date=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
        }
        if (sqlite3_column_text(self.sqlStament, 4)) {
            record.recd_Stop_Date = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
        }
        
        [result addObject:record];
        status = sqlite3_step(sqlStament);
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }

    
    return result;
}

-(NSMutableArray *)loadAllRoomRecordDetailsByRoomAssignmentId:(NSInteger)raId andUserId:(NSInteger)userId {
    NSString *sqlString = [NSString stringWithFormat:@"SELECT %@, %@, %@, %@, %@ FROM %@ WHERE %@ = %d AND %@ = %d",
                           recd_id, 
                           recd_ra_id, 
                           recd_user_id, 
                           recd_start_date, 
                           recd_stop_date,
                           
                           ROOM_RECORDS_DETAIL, 
                           recd_user_id,
                           (int)userId,
                           recd_ra_id,
                           (int)raId
                           ];
    
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }

    reAccessCount++;
}

    NSMutableArray *result = [NSMutableArray array];
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {

    }
    
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW) {
        RoomRecordDetailModelV2 *record = [[RoomRecordDetailModelV2 alloc] init];
        record.recd_Id = sqlite3_column_int(self.sqlStament, 0);
        record.recd_Ra_Id = sqlite3_column_int(self.sqlStament, 1);
        record.recd_User_Id = sqlite3_column_int(self.sqlStament, 2);
        
        if (sqlite3_column_text(self.sqlStament, 3)) {
            record.recd_Start_Date=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
        }
        if (sqlite3_column_text(self.sqlStament, 4)) {
            record.recd_Stop_Date = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
        }
        
        [result addObject:record];
        status = sqlite3_step(sqlStament);
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            [result removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }

    
    return result;
}


@end
