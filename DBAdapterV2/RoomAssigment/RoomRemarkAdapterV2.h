//
//  RoomRemarkAdapterV2.h
//  mHouseKeeping
//
//  Created by Nghia Truong on 3/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DatabaseAdapterV2.h"
#import "RoomRemarkModelV2.h"
#import "DbDefinesV2.h"
 
@interface RoomRemarkAdapterV2 : DatabaseAdapterV2 {
    
}

-(int) insertRoomRemarkData:(RoomRemarkModelV2 *) remark;
-(int) updateRoomRemarkData:(RoomRemarkModelV2 *) remark;
-(int) deleteRoomRemarkData:(RoomRemarkModelV2 *) remark;
-(RoomRemarkModelV2 *) loadRoomRemarkData:(RoomRemarkModelV2 *) remark;
-(RoomRemarkModelV2 *) loadRoomRemarkDataByRecordId:(RoomRemarkModelV2 *) remark;
-(NSMutableArray *) loadAllRoomRemark;
-(NSMutableArray *) loadAllRoomRemarkByRecordId:(int) recordId;
-(NSInteger)countRecordsByRecordId:(int) recordId;
- (NSInteger)countRecordUnPost;
-(NSMutableArray *)loadAllRoomRemarkUnPost;
@end
