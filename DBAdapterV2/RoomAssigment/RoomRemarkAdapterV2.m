//
//  RoomRemarkAdapterV2.m
//  mHouseKeeping
//
//  Created by Nghia Truong on 3/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "RoomRemarkAdapterV2.h"
#import "ehkDefines.h"

@implementation RoomRemarkAdapterV2

-(int)insertRoomRemarkData:(RoomRemarkModelV2 *)remark {
    NSString *sqlString = [[NSString alloc] initWithFormat:@"INSERT INTO %@ (%@, %@, %@, %@, %@, %@) %@", ROOM_REMARK,  rm_time, rm_record_id, rm_pos_status, rm_content, rm_physical_content, rm_roomassignment_roomId,
                           @"VALUES(?, ?, ?, ?, ?, ?)"];
    
    const char *sql=[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    if(sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
        
    }
    
    //    sqlite3_bind_int (self.sqlStament, 1, remark.rm_Id);
    sqlite3_bind_text(self.sqlStament, 1, [remark.rm_clean_pause_time UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_int (self.sqlStament, 2, remark.rm_RecordId);
    sqlite3_bind_int (self.sqlStament, 3, remark.rm_PostStatus);
    sqlite3_bind_text(self.sqlStament, 4, [remark.rm_content UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 5, [remark.rm_physical_content UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 6, [remark.rm_roomassignment_roomId UTF8String], -1, SQLITE_TRANSIENT);
    NSInteger returnInt = 0;
    
    NSInteger status = sqlite3_step(self.sqlStament);
    
    if(SQLITE_DONE != status) {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return (int)returnInt;
    } else {
        sqlite3_reset(self.sqlStament);
        returnInt = 1;
        return (int)returnInt;
    }
    
}

-(int)updateRoomRemarkData:(RoomRemarkModelV2 *)remark {
    NSString *sqlString = [[NSString alloc] initWithFormat:@"UPDATE %@ SET %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ? WHERE %@ = ?", ROOM_REMARK, rm_clean_pause_time, rm_record_id, rm_pos_status, rm_content, rm_physical_content, rm_id];
    
    const char *sql = [sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
        
    }
    
    sqlite3_bind_text(self.sqlStament, 1, [remark.rm_clean_pause_time UTF8String],
                      -1,SQLITE_TRANSIENT);
    sqlite3_bind_int (self.sqlStament, 2, remark.rm_RecordId);
    sqlite3_bind_int (self.sqlStament, 3, remark.rm_PostStatus);
    sqlite3_bind_text(self.sqlStament, 4, [remark.rm_content UTF8String],
                      -1,SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 5, [remark.rm_physical_content UTF8String],
                      -1,SQLITE_TRANSIENT);
    sqlite3_bind_int (self.sqlStament, 6, (int)remark.rm_Id);
    
    
    NSInteger returnInt = 0;
    NSInteger status = sqlite3_step(self.sqlStament);
    if(SQLITE_DONE != status) {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return (int)returnInt;
    } else {
        returnInt = 1;
        return (int)returnInt;
    }
}

-(int)deleteRoomRemarkData:(RoomRemarkModelV2 *)remark {
    NSInteger result = 1;
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"DELETE FROM %@ WHERE %@ = ?", ROOM_REMARK, rm_id];
    
    const char *sql = [sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
        result = 0;
    
    sqlite3_bind_int(self.sqlStament, 1, (int)remark.rm_Id);
    
    NSInteger status = sqlite3_step(sqlStament);
    
    if (SQLITE_DONE != status) {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        result = 0;
    }
    
    return (int)result;
}

-(RoomRemarkModelV2 *)loadRoomRemarkData:(RoomRemarkModelV2 *)remark {
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ?", rm_id, rm_clean_pause_time, rm_record_id, rm_pos_status, rm_content, rm_physical_content, rm_roomassignment_roomId, ROOM_REMARK, rm_id];
    
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    
    sqlite3_bind_int(self.sqlStament, 1, (int)remark.rm_Id);
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW) {
        remark.rm_Id = sqlite3_column_int(self.sqlStament, 0);
        if (sqlite3_column_text(self.sqlStament, 1)) {
            remark.rm_clean_pause_time=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
        }
        remark.rm_RecordId = sqlite3_column_int(self.sqlStament, 2);
        remark.rm_PostStatus = sqlite3_column_int(self.sqlStament, 3);
        if (sqlite3_column_text(self.sqlStament, 4)) {
            remark.rm_content =[NSString stringWithUTF8String:
                                (char *)sqlite3_column_text(sqlStament, 4)];
        }
        
        if (sqlite3_column_text(self.sqlStament, 5)) {
            remark.rm_physical_content =[NSString stringWithUTF8String:
                                         (char *)sqlite3_column_text(sqlStament, 5)];
        }
        if (sqlite3_column_text(self.sqlStament, 6)) {
            remark.rm_roomassignment_roomId =[NSString stringWithUTF8String:
                                         (char *)sqlite3_column_text(sqlStament, 6)];
        }
        
        status = sqlite3_step(sqlStament);
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    
    return remark;
}

-(RoomRemarkModelV2 *)loadRoomRemarkDataByRecordId:(RoomRemarkModelV2 *)remark {
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@ ,%@, %@, %@ FROM %@ WHERE %@ = ?", rm_id, rm_clean_pause_time, rm_record_id, rm_pos_status,rm_content, rm_physical_content, rm_roomassignment_roomId, ROOM_REMARK, rm_record_id];
    
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    
    sqlite3_bind_int(self.sqlStament, 1, remark.rm_RecordId);
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW) {
        remark.rm_Id = sqlite3_column_int(self.sqlStament, 0);
        if (sqlite3_column_text(self.sqlStament, 1)) {
            remark.rm_clean_pause_time =[NSString stringWithUTF8String:
                                         (char *)sqlite3_column_text(sqlStament, 1)];
        }
        remark.rm_RecordId = sqlite3_column_int(self.sqlStament, 2);
        remark.rm_PostStatus = sqlite3_column_int(self.sqlStament, 3);
        
        if (sqlite3_column_text(self.sqlStament, 4)) {
            remark.rm_content =[NSString stringWithUTF8String:
                                (char *)sqlite3_column_text(sqlStament, 4)];
        }
        
        if (sqlite3_column_text(self.sqlStament, 5)) {
            remark.rm_physical_content =[NSString stringWithUTF8String:
                                (char *)sqlite3_column_text(sqlStament, 5)];
        }
        if (sqlite3_column_text(self.sqlStament, 6)) {
            remark.rm_roomassignment_roomId =[NSString stringWithUTF8String:
                                         (char *)sqlite3_column_text(sqlStament, 6)];
        }
        
        status = sqlite3_step(sqlStament);
        
        
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    
    return remark;
}

-(NSMutableArray *)loadAllRoomRemark {
    NSMutableArray *array = [NSMutableArray array];
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@ FROM %@", rm_id, rm_time, rm_record_id, rm_pos_status, rm_content, rm_physical_content, rm_roomassignment_roomId, ROOM_REMARK];
    
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    
    
    NSInteger status = sqlite3_step(sqlStament);
    
    while(status == SQLITE_ROW) {
        RoomRemarkModelV2 *remark = [[RoomRemarkModelV2 alloc] init];
        remark.rm_Id = sqlite3_column_int(self.sqlStament, 0);
        if (sqlite3_column_text(self.sqlStament, 1)) {
            remark.rm_Time=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
        } else {
            remark.rm_Time = @"0";
        }
        remark.rm_RecordId = sqlite3_column_int(self.sqlStament, 2);
        remark.rm_PostStatus = sqlite3_column_int(self.sqlStament, 3);
        if (sqlite3_column_text(self.sqlStament, 4)) {
            remark.rm_content =[NSString stringWithUTF8String:
                                (char *)sqlite3_column_text(sqlStament, 4)];
        }
        
        if (sqlite3_column_text(self.sqlStament, 5)) {
            remark.rm_physical_content =[NSString stringWithUTF8String:
                                         (char *)sqlite3_column_text(sqlStament, 5)];
        }
        if (sqlite3_column_text(self.sqlStament, 6)) {
            remark.rm_roomassignment_roomId =[NSString stringWithUTF8String:
                                         (char *)sqlite3_column_text(sqlStament, 6)];
        }
        
        [array addObject:remark];
        
        status = sqlite3_step(sqlStament);
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    return array;
}

-(NSMutableArray *)loadAllRoomRemarkByRecordId:(int) recordId {
    NSMutableArray *array = [NSMutableArray array];
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ?",
                           rm_id,
                           rm_time,
                           rm_record_id,
                           rm_pos_status,
                           rm_content,
                           rm_physical_content,
                           rm_roomassignment_roomId,
                           ROOM_REMARK,
                           rm_record_id];
    
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    
    sqlite3_bind_int(self.sqlStament, 1, recordId);
    
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW) {
        RoomRemarkModelV2 *remark = [[RoomRemarkModelV2 alloc] init];
        remark.rm_Id = sqlite3_column_int(self.sqlStament, 0);
        if (sqlite3_column_text(self.sqlStament, 1)) {
            remark.rm_clean_pause_time=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
        }
        remark.rm_RecordId = sqlite3_column_int(self.sqlStament, 2);
        remark.rm_PostStatus = sqlite3_column_int(self.sqlStament, 3);
        
        if (sqlite3_column_text(self.sqlStament, 4)) {
            remark.rm_content=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
        }
        
        if (sqlite3_column_text(self.sqlStament, 5)) {
            remark.rm_physical_content =[NSString stringWithUTF8String:
                                         (char *)sqlite3_column_text(sqlStament, 5)];
        }
        if (sqlite3_column_text(self.sqlStament, 6)) {
            remark.rm_roomassignment_roomId =[NSString stringWithUTF8String:
                                         (char *)sqlite3_column_text(sqlStament, 6)];
        }
        
        [array addObject:remark];
        status = sqlite3_step(sqlStament);
    }
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    
    return array;
}


-(NSInteger)countRecordsByRecordId:(int) recordId {
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT COUNT(*) FROM %@ WHERE %@ = ?",ROOM_REMARK, rm_record_id];
    
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    NSInteger result=0;
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    
    sqlite3_bind_int(self.sqlStament, 1, recordId);
    
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW) {
        result =  sqlite3_column_int(self.sqlStament, 0);
        status = sqlite3_step(sqlStament);
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    
    return result;
}

- (NSInteger)countRecordUnPost{
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT COUNT(*) FROM %@ WHERE %@ = ?",ROOM_REMARK, rm_pos_status];
    
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    NSInteger result=0;
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    
    sqlite3_bind_int(self.sqlStament, 1, (int)POST_STATUS_SAVED_UNPOSTED);
    
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW) {
        result =  sqlite3_column_int(self.sqlStament, 0);
        status = sqlite3_step(sqlStament);
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    
    return result;
}

-(NSMutableArray *)loadAllRoomRemarkUnPost {
    NSMutableArray *array = [NSMutableArray array];
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ?", rm_id, rm_time, rm_record_id, rm_pos_status, rm_content, rm_physical_content, rm_roomassignment_roomId, ROOM_REMARK, rm_pos_status];
    
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    
    sqlite3_bind_int(self.sqlStament, 1, (int)POST_STATUS_SAVED_UNPOSTED);
    NSInteger status = sqlite3_step(sqlStament);
    
    while(status == SQLITE_ROW) {
        RoomRemarkModelV2 *remark = [[RoomRemarkModelV2 alloc] init];
        remark.rm_Id = sqlite3_column_int(self.sqlStament, 0);
        if (sqlite3_column_text(self.sqlStament, 1)) {
            remark.rm_Time=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
        } else {
            remark.rm_Time = @"0";
        }
        remark.rm_RecordId = sqlite3_column_int(self.sqlStament, 2);
        remark.rm_PostStatus = sqlite3_column_int(self.sqlStament, 3);
        if (sqlite3_column_text(self.sqlStament, 4)) {
            remark.rm_content =[NSString stringWithUTF8String:
                                (char *)sqlite3_column_text(sqlStament, 4)];
        }
        
        if (sqlite3_column_text(self.sqlStament, 5)) {
            remark.rm_physical_content =[NSString stringWithUTF8String:
                                         (char *)sqlite3_column_text(sqlStament, 5)];
        }
        if (sqlite3_column_text(self.sqlStament, 6)) {
            remark.rm_roomassignment_roomId =[NSString stringWithUTF8String:
                                         (char *)sqlite3_column_text(sqlStament, 6)];
        }
        
        [array addObject:remark];
        
        status = sqlite3_step(sqlStament);
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    return array;
}
@end
