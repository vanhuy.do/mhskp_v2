//
//  RoomServiceLaterAdapterV2.h
//  mHouseKeeping
//
//  Created by Nghia Truong on 3/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DatabaseAdapterV2.h"
#import "DbDefinesV2.h"
#import "RoomServiceLaterModelV2.h"

@interface RoomServiceLaterAdapterV2 : DatabaseAdapterV2 {
    
}

-(int) insertRoomServiceLaterData:(RoomServiceLaterModelV2 *) service;
-(int) updateRoomServiceLaterData:(RoomServiceLaterModelV2 *) service;
-(int) deleteRoomServiceLaterData:(RoomServiceLaterModelV2 *) service;
-(RoomServiceLaterModelV2 *) loadRoomServiceLaterData:(RoomServiceLaterModelV2 *) service;
-(RoomServiceLaterModelV2 *) loadRoomServiceLaterDataByRecordId:(RoomServiceLaterModelV2 *) service;
-(NSMutableArray *) loadAllRoomServiceLaterByRecordId:(int) recordId;
-(NSMutableArray *) loadAllRoomServiceLater;
-(NSInteger)numberOfRoomServiceLaterMustPost:(NSMutableArray*)listRoomRecordID;
@end
