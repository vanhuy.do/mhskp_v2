//
//  RoomServiceLaterAdapterV2.m
//  mHouseKeeping
//
//  Created by Nghia Truong on 3/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "RoomServiceLaterAdapterV2.h"
#import "ehkDefines.h"

@implementation RoomServiceLaterAdapterV2

-(int)insertRoomServiceLaterData:(RoomServiceLaterModelV2 *)service {
    NSString *sqlString = [[NSString alloc] initWithFormat:@"INSERT INTO %@ ( %@, %@) %@", ROOM_SERVICE_LATER, rsl_time, rsl_record_id, @"VALUES(?, ?)"];
    
    const char *sql=[sqlString UTF8String];

    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }

    reAccessCount++;
}

    if(sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
        
    }
    
//    sqlite3_bind_int (self.sqlStament, 1, service.rsl_Id);
    sqlite3_bind_text(self.sqlStament, 1, [service.rsl_Time UTF8String], 
                      -1,SQLITE_TRANSIENT);
    sqlite3_bind_int (self.sqlStament, 2, service.rsl_RecordId);
    
    
    NSInteger returnInt = 0;
    
    NSInteger status = sqlite3_step(self.sqlStament);
    
    if(SQLITE_DONE != status) {  
        if (status == SQLITE_BUSY) {
            if (reAccessCount < REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return (int)returnInt;
    } else {
        sqlite3_reset(self.sqlStament);
        returnInt = 1;
        return (int)returnInt;
    }
}

-(int)updateRoomServiceLaterData:(RoomServiceLaterModelV2 *)service {
    NSString *sqlString = [[NSString alloc] initWithFormat:@"UPDATE %@ SET %@ = ?, %@ = ?, %@ = ? WHERE %@ = ?", ROOM_SERVICE_LATER, rsl_id, rsl_time, rsl_record_id, rsl_id];
    
    const char *sql = [sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }

    reAccessCount++;
}

    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
        
    }
    
    sqlite3_bind_int (self.sqlStament, 1, service.rsl_Id);
    sqlite3_bind_text(self.sqlStament, 2, [service.rsl_Time UTF8String], 
                      -1,SQLITE_TRANSIENT);
    sqlite3_bind_int (self.sqlStament, 3, service.rsl_RecordId);

    sqlite3_bind_int (self.sqlStament, 4, service.rsl_Id);
    
    NSInteger returnInt = 0;    
    NSInteger status = sqlite3_step(self.sqlStament);
    if(SQLITE_DONE != status) {   
        if (status == SQLITE_BUSY) {
            if (reAccessCount < REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return (int)returnInt;
    } else { 
        returnInt = 1;        
        return (int)returnInt;
    }
}

-(int)deleteRoomServiceLaterData:(RoomServiceLaterModelV2 *)service {
    NSInteger result = 1;
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"DELETE FROM %@ WHERE %@ = ?", ROOM_SERVICE_LATER, rsl_id];
    
    const char *sql = [sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }

    reAccessCount++;
}

    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
        result = 0;
	
    sqlite3_bind_int(self.sqlStament, 1, service.rsl_Id);
	
    NSInteger status = sqlite3_step(sqlStament);
    
	if (SQLITE_DONE != status) {
        if (status == SQLITE_BUSY) {
            if (reAccessCount < REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
		result = 0;
    }
    
    return (int)result;

}

-(RoomServiceLaterModelV2 *)loadRoomServiceLaterData:(RoomServiceLaterModelV2 *)service {
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@ FROM %@ WHERE %@ = ?", rsl_id, rsl_time, rsl_record_id, ROOM_SERVICE_LATER, rsl_id];
    
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }

    reAccessCount++;
}

    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {        

    }
    
    sqlite3_bind_int(self.sqlStament, 1, service.rsl_Id);
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW) {
        service.rsl_Id = sqlite3_column_int(self.sqlStament, 0);            
        if (sqlite3_column_text(self.sqlStament, 1)) {
            service.rsl_Time = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
        }
        service.rsl_RecordId = sqlite3_column_int(self.sqlStament, 2);
        
        status = sqlite3_step(sqlStament);
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }

    
    return service;
}

-(RoomServiceLaterModelV2 *)loadRoomServiceLaterDataByRecordId:(RoomServiceLaterModelV2 *)service {
     NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@ FROM %@ WHERE %@ = ?", rsl_id, rsl_time, rsl_record_id, ROOM_SERVICE_LATER, rsl_record_id];
    
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }

    reAccessCount++;
}

    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {

    }
    
    sqlite3_bind_int(self.sqlStament, 1, service.rsl_RecordId);
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW) {
        service.rsl_Id = sqlite3_column_int(self.sqlStament, 0);            
        if (sqlite3_column_text(self.sqlStament, 1)) {
            service.rsl_Time = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
        }
        service.rsl_RecordId = sqlite3_column_int(self.sqlStament, 2);
        status = sqlite3_step(sqlStament);
    }
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }

    
    return service;
}

-(NSMutableArray *)loadAllRoomServiceLater {
    NSMutableArray *array = [NSMutableArray array];
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@ FROM %@", rsl_id, rsl_time, rsl_record_id, ROOM_SERVICE_LATER];
    
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }

    reAccessCount++;
}

    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }    
    
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW) {
        RoomServiceLaterModelV2 *service = [[RoomServiceLaterModelV2 alloc] init];
        service.rsl_Id = sqlite3_column_int(self.sqlStament, 0);            
        if (sqlite3_column_text(self.sqlStament, 1)) {
            service.rsl_Time = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
        }
        service.rsl_RecordId = sqlite3_column_int(self.sqlStament, 2);
        
        [array addObject:service];
        status = sqlite3_step(sqlStament);
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    

    
    return array;
}

-(NSMutableArray *)loadAllRoomServiceLaterByRecordId:(int)recordId {
    NSMutableArray *array = [NSMutableArray array];
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@ FROM %@ WHERE %@ = ?", rsl_id, rsl_time, rsl_record_id, ROOM_SERVICE_LATER, rsl_record_id];
    
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }

    reAccessCount++;
}

    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {

    }    
    
    sqlite3_bind_int(self.sqlStament, 1, recordId);
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW) {
        RoomServiceLaterModelV2 *service = [[RoomServiceLaterModelV2 alloc] init];
        service.rsl_Id = sqlite3_column_int(self.sqlStament, 0);            
        if (sqlite3_column_text(self.sqlStament, 1)) {
            service.rsl_Time = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
        }
        service.rsl_RecordId = sqlite3_column_int(self.sqlStament, 2);
        
        [array addObject:service];
        status = sqlite3_step(sqlStament);
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }

    return array;
}

-(NSInteger)numberOfRoomServiceLaterMustPost:(NSMutableArray*)listRoomRecordID{
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT COUNT(*) FROM %@ WHERE %@ = ?", 
                           ROOM_SERVICE_LATER,
                           rsl_record_id];
    
    const char *sql=[sqlString UTF8String];
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        
        reAccessCount++;
    }

    NSInteger result = 0;
    for (int index = 0; index < [listRoomRecordID count]; index ++) {
        NSInteger roomRecordID = [[NSString stringWithFormat:@"%@",[listRoomRecordID objectAtIndex:index]] intValue];
        
        if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {}
        
        sqlite3_bind_int (self.sqlStament, 1, (int)roomRecordID);
        
        NSInteger status = sqlite3_step(sqlStament);
        while(status == SQLITE_ROW) {
            
            result += sqlite3_column_int(sqlStament, 0);
            
            status = sqlite3_step(sqlStament);
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount < REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
                
    }
    
    return result;

}
@end
