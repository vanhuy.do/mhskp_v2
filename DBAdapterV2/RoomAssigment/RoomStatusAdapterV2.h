//
//  RoomStatusAdapterV2.h
//  mHouseKeeping
//
//  Created by Nghia Truong on 3/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DatabaseAdapterV2.h"
#import "RoomStatusModelV2.h"
#import "DbDefinesV2.h"

@interface RoomStatusAdapterV2 : DatabaseAdapterV2 {
    
}

-(int) insertRoomStatusData:(RoomStatusModelV2 *) status;
-(int) updateRoomStatusData:(RoomStatusModelV2 *) status;
-(int) deleteRoomStatusData:(RoomStatusModelV2 *) status;
-(int)deleteAllRoomStatus;
-(RoomStatusModelV2 *) loadRoomStatusData:(RoomStatusModelV2 *) status;
-(NSMutableArray *) loadAllRoomStatus;
-(RoomStatusModelV2 *) loadRoomStatusByStatusNameData:(RoomStatusModelV2 *) status;
-(NSMutableArray *)loadAllMaidRoomStatusByIsOcupied:(BOOL)isOcupied;
-(NSMutableArray *) loadAllSuperVisorRoomStatusByIsOcupied:(BOOL)isOcupied;
-(NSMutableArray *) loadAllSuperVisorRoomStatusByIsOcupied:(BOOL)isOcupied andChekList:(NSInteger)tchk;
-(NSMutableArray *) loadAllSuperVisorBlockRoomStatusByIsOcupied:(BOOL)isOcupied;

//get last modified room status
-(NSString*)getRoomStatusLastModifiedDate;

//conrad
-(NSMutableArray *)loadAllMaidRoomStatus;
@end
