//
//  RoomStatusAdapterV2.m
//  mHouseKeeping
//
//  Created by Nghia Truong on 3/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "RoomStatusAdapterV2.h"
#import "CheckListModelV2.h"
#import "ehkDefines.h"

@implementation RoomStatusAdapterV2

-(int)insertRoomStatusData:(RoomStatusModelV2 *)status {
    NSString *sqlString = [[NSString alloc] initWithFormat:@"INSERT INTO %@ (%@, %@, %@, %@, %@, %@, %@, %@) %@", ROOM_STATUS, rstat_id, rstat_code, rstat_name, rstat_lang, rstat_image, rstat_last_modified, rstat_physical_check, rstat_find_status, @"VALUES(?, ?, ?, ?, ?, ?, ?, ?)"];
        
    const char *sql=[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }

    reAccessCount++;
}

    if(sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
            
    }
        
    sqlite3_bind_int (self.sqlStament, 1, status.rstat_Id);
    sqlite3_bind_text(self.sqlStament, 2, [status.rstat_Code UTF8String], 
                        -1,SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 3, [status.rstat_Name UTF8String], 
                        -1,SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 4, [status.rstat_Lang UTF8String], 
                        -1,SQLITE_TRANSIENT);
    sqlite3_bind_blob(self.sqlStament, 5, [status.rstat_Image bytes], (int)[status.rstat_Image length], NULL);
    sqlite3_bind_text(self.sqlStament, 6, [status.rstat_LastModified UTF8String], 
                        -1,SQLITE_TRANSIENT);
    sqlite3_bind_int (self.sqlStament, 7, status.rstat_PhysicalCheck);
    sqlite3_bind_int (self.sqlStament, 8, status.rstat_FindStatus);
    
    NSInteger returnInt = 0;
    
    NSInteger status_ = sqlite3_step(self.sqlStament);
    
    if(SQLITE_DONE != status_) {  
        if (status_ == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return (int)returnInt;
    } else {
        sqlite3_reset(self.sqlStament);
        returnInt = 1;
        return (int)returnInt;
    }
}

-(int)updateRoomStatusData:(RoomStatusModelV2 *)status {
    NSString *sqlString = [[NSString alloc] initWithFormat:@"UPDATE %@ SET %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ? WHERE %@ = ?", ROOM_STATUS, rstat_id, rstat_code, rstat_name, rstat_lang, rstat_image, rstat_last_modified, rstat_physical_check, rstat_find_status, rstat_id];
        
    const char *sql = [sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }

    reAccessCount++;
}

        
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
            
    }
        
    sqlite3_bind_int (self.sqlStament, 1, status.rstat_Id);
    sqlite3_bind_text(self.sqlStament, 2, [status.rstat_Code UTF8String], 
                        -1,SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 3, [status.rstat_Name UTF8String], 
                        -1,SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 4, [status.rstat_Lang UTF8String], 
                        -1,SQLITE_TRANSIENT);
    sqlite3_bind_blob(self.sqlStament, 5, [status.rstat_Image bytes], (int)[status.rstat_Image length], NULL);
    sqlite3_bind_text(self.sqlStament, 6, [status.rstat_LastModified UTF8String], 
                        -1,SQLITE_TRANSIENT);
    sqlite3_bind_int (self.sqlStament, 7, status.rstat_PhysicalCheck);
    sqlite3_bind_int (self.sqlStament, 8, status.rstat_FindStatus);
    sqlite3_bind_int (self.sqlStament, 9, status.rstat_Id);

    NSInteger returnInt = 0;    
    NSInteger status_ = sqlite3_step(self.sqlStament);
    if(SQLITE_DONE != status_) {   
        if (status_ == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return (int)returnInt;
    } else { 
        returnInt = 1;        
        return (int)returnInt;
    }
}

-(int)deleteRoomStatusData:(RoomStatusModelV2 *)status {
    NSInteger result = 1;
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"DELETE FROM %@ WHERE %@ = ?", ROOM_STATUS, rstat_id];
        
    const char *sql = [sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }

    reAccessCount++;
}

		
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
        result = 0;
	
    sqlite3_bind_int(self.sqlStament, 1, status.rstat_Id);
    NSInteger status_ = sqlite3_step(sqlStament);
    
	if (SQLITE_DONE != status_) {
        if (status_ == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
		result = 0;
    }
    
    return (int)result;
}

-(int)deleteAllRoomStatus
{
    DataTable *tableDelete = [[DataTable alloc] initWithNameFormat:@"%@", ROOM_STATUS];
    int result = (int)[tableDelete excuteQueryNonSelect:[NSString stringWithFormat:@"Delete from %@", ROOM_STATUS]];
    if(result > 0) {
        [NSString stringWithFormat: @"REINDEX %@.%@",DATABASE_NAME, ROOM_STATUS];
    }
    
    return result;
}

-(RoomStatusModelV2 *)loadRoomStatusData:(RoomStatusModelV2 *)status {
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ?", rstat_id, rstat_code, rstat_name, rstat_lang, rstat_image, rstat_last_modified, rstat_physical_check, rstat_find_status, ROOM_STATUS, rstat_id];
    
    const char *sql =[sqlString UTF8String];
    //NSLog(@"sqlString %@",sqlString);

    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }

    reAccessCount++;
}

    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    
    sqlite3_bind_int(self.sqlStament, 1, status.rstat_Id);
    NSInteger status_ = sqlite3_step(sqlStament);
    while(status_ == SQLITE_ROW) {
        status.rstat_Id = sqlite3_column_int(self.sqlStament, 0);
        
        if (sqlite3_column_text(self.sqlStament, 1)) {
            status.rstat_Code=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
        }
        
        if (sqlite3_column_text(self.sqlStament, 2)) {
            status.rstat_Name=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
        }
        
        if (sqlite3_column_text(self.sqlStament, 3)) {
            status.rstat_Lang=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
        }
        
        if (sqlite3_column_bytes(self.sqlStament, 4) > 0) {
            status.rstat_Image = [NSData dataWithBytes:sqlite3_column_blob
                                  (self.sqlStament, 4) length:sqlite3_column_bytes(self.sqlStament, 4)];
        }            
        
        if (sqlite3_column_text(self.sqlStament, 5)) {
            status.rstat_LastModified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
        }
        
        if (sqlite3_column_int(self.sqlStament, 6)) {
            status.rstat_PhysicalCheck=sqlite3_column_int(self.sqlStament, 6);
        }
        
        if (sqlite3_column_int(self.sqlStament, 7)) {
            status.rstat_PhysicalCheck=sqlite3_column_int(self.sqlStament, 7);
        }
        
        status_ = sqlite3_step(sqlStament);
    }
    
    if (status_ == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }

    
    return status;
}

-(NSMutableArray *)loadAllRoomStatus {
    NSMutableArray *array = [NSMutableArray array];
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@, %@ FROM %@", rstat_id, rstat_code, rstat_name, rstat_lang, rstat_image, rstat_last_modified, rstat_physical_check, rstat_find_status, ROOM_STATUS];
    
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }

    reAccessCount++;
}

    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }    
    
    NSInteger status_ = sqlite3_step(sqlStament);
    while(status_ == SQLITE_ROW) {
        RoomStatusModelV2 *status = [[RoomStatusModelV2 alloc] init];
        status.rstat_Id = sqlite3_column_int(self.sqlStament, 0);
        if (sqlite3_column_text(self.sqlStament, 1)) {
            status.rstat_Code=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
        }
        if (sqlite3_column_text(self.sqlStament, 2)) {
            status.rstat_Name=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
        }
        if (sqlite3_column_text(self.sqlStament, 3)) {
            status.rstat_Lang=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
        }
        if (sqlite3_column_bytes(self.sqlStament, 4) > 0) {
            status.rstat_Image = [NSData dataWithBytes:sqlite3_column_blob
                                  (self.sqlStament, 4) length:sqlite3_column_bytes(self.sqlStament, 4)];
        }            
        if (sqlite3_column_text(self.sqlStament, 5)) {
            status.rstat_LastModified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
        }
        if (sqlite3_column_int(self.sqlStament, 6)) {
            status.rstat_PhysicalCheck=sqlite3_column_int(self.sqlStament, 6);
        }
        if (sqlite3_column_int(self.sqlStament, 7)) {
            status.rstat_FindStatus=sqlite3_column_int(self.sqlStament, 7);
        }
        
        [array addObject:status];
        status_ = sqlite3_step(sqlStament);
    }
    
    
    if (status_ == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }

    
    return array;
}

-(NSMutableArray *)loadAllMaidRoomStatusByIsOcupied:(BOOL)isOcupied{
    NSMutableArray *array = [NSMutableArray array];
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@, %@ FROM %@", rstat_id, rstat_code, rstat_name, rstat_lang, rstat_image, rstat_last_modified, rstat_physical_check, rstat_find_status, ROOM_STATUS];
    
    const char *sql =[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}

    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }    
    
    NSInteger status_ = sqlite3_step(sqlStament);
    while(status_ == SQLITE_ROW) {
        RoomStatusModelV2 *status = [[RoomStatusModelV2 alloc] init];
        status.rstat_Id = sqlite3_column_int(self.sqlStament, 0);
        if (sqlite3_column_text(self.sqlStament, 1)) {
            status.rstat_Code=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
        }
        if (sqlite3_column_text(self.sqlStament, 2)) {
            status.rstat_Name=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
        }
        if (sqlite3_column_text(self.sqlStament, 3)) {
            status.rstat_Lang=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
        }
        if (sqlite3_column_bytes(self.sqlStament, 4) > 0) {
            status.rstat_Image = [NSData dataWithBytes:sqlite3_column_blob
                                  (self.sqlStament, 4) length:sqlite3_column_bytes(self.sqlStament, 4)];
        }            
        if (sqlite3_column_text(self.sqlStament, 5)) {
            status.rstat_LastModified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
        }
        if (sqlite3_column_int(self.sqlStament, 6)) {
            status.rstat_PhysicalCheck=sqlite3_column_int(self.sqlStament, 6);
        }
        if (sqlite3_column_int(self.sqlStament, 7)) {
            status.rstat_PhysicalCheck=sqlite3_column_int(self.sqlStament, 7);
        }
        if(isOcupied){
            if(status.rstat_Id == ENUM_ROOM_STATUS_OC || status.rstat_Id == ENUM_ROOM_STATUS_OC_SO){
                [array addObject:status];
            }
            
            //                if([UserManagerV2 sharedUserManager].currentUser.userAllowBlockRoom){
            //                    if(status.rstat_Id == ENUM_ROOM_STATUS_OOO || status.rstat_Id == ENUM_ROOM_STATUS_OOS){
            //                        [array addObject:status];
            //                    }
            //                }
            
        } else{
            if(status.rstat_Id == ENUM_ROOM_STATUS_VC){
                [array addObject:status];
            }
            
            if([UserManagerV2 sharedUserManager].currentUser.userAllowBlockRoom){
                if(status.rstat_Id == ENUM_ROOM_STATUS_OOO || status.rstat_Id == ENUM_ROOM_STATUS_OOS){
                    [array addObject:status];
                }
            }
        }
        status_ = sqlite3_step(sqlStament);  
    }
    
    if (status_ == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }

    return array;
    
}

-(NSMutableArray *)loadAllMaidRoomStatus
{
    NSMutableArray *array = [NSMutableArray array];
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@, %@ FROM %@", rstat_id, rstat_code, rstat_name, rstat_lang, rstat_image, rstat_last_modified, rstat_physical_check, rstat_find_status, ROOM_STATUS];
    
    const char *sql =[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }    
    
    NSInteger status_ = sqlite3_step(sqlStament);
    while(status_ == SQLITE_ROW) {
        RoomStatusModelV2 *status = [[RoomStatusModelV2 alloc] init];
        status.rstat_Id = sqlite3_column_int(self.sqlStament, 0);
        if (sqlite3_column_text(self.sqlStament, 1)) {
            status.rstat_Code=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
        }
        if (sqlite3_column_text(self.sqlStament, 2)) {
            status.rstat_Name=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
        }
        if (sqlite3_column_text(self.sqlStament, 3)) {
            status.rstat_Lang=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
        }
        if (sqlite3_column_bytes(self.sqlStament, 4) > 0) {
            status.rstat_Image = [NSData dataWithBytes:sqlite3_column_blob
                                  (self.sqlStament, 4) length:sqlite3_column_bytes(self.sqlStament, 4)];
        }            
        if (sqlite3_column_text(self.sqlStament, 5)) {
            status.rstat_LastModified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
        }
        if (sqlite3_column_int(self.sqlStament, 6)) {
            status.rstat_PhysicalCheck=sqlite3_column_int(self.sqlStament, 6);
        }
        if (sqlite3_column_int(self.sqlStament, 7)) {
            status.rstat_PhysicalCheck=sqlite3_column_int(self.sqlStament, 7);
        }
        if(status.rstat_Id == ENUM_ROOM_STATUS_VC || status.rstat_Id == ENUM_ROOM_STATUS_VD || status.rstat_Id == ENUM_ROOM_STATUS_OD
           || status.rstat_Id == ENUM_ROOM_STATUS_OC || status.rstat_Id == ENUM_ROOM_STATUS_VI || status.rstat_Id == ENUM_ROOM_STATUS_OI){
            [array addObject:status];
        }
        
        status_ = sqlite3_step(sqlStament);  
    }
    
    if (status_ == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    return array;
}

-(NSMutableArray *) loadAllSuperVisorRoomStatusByIsOcupied:(BOOL)isOcupied {
    NSMutableArray *array = [NSMutableArray array];
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@, %@ FROM %@", rstat_id, rstat_code, rstat_name, rstat_lang, rstat_image, rstat_last_modified, rstat_physical_check, rstat_find_status, ROOM_STATUS];
    
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}

    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }    
    
    NSInteger status_ = sqlite3_step(sqlStament);
    while(status_ == SQLITE_ROW) {
        RoomStatusModelV2 *status = [[RoomStatusModelV2 alloc] init];
        status.rstat_Id = sqlite3_column_int(self.sqlStament, 0);
        if (sqlite3_column_text(self.sqlStament, 1)) {
            status.rstat_Code=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
        }
        if (sqlite3_column_text(self.sqlStament, 2)) {
            status.rstat_Name=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
        }
        if (sqlite3_column_text(self.sqlStament, 3)) {
            status.rstat_Lang=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
        }
        if (sqlite3_column_bytes(self.sqlStament, 4) > 0) {
            status.rstat_Image = [NSData dataWithBytes:sqlite3_column_blob
                                  (self.sqlStament, 4) length:sqlite3_column_bytes(self.sqlStament, 4)];
        }            
        if (sqlite3_column_text(self.sqlStament, 5)) {
            status.rstat_LastModified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
        }
        if (sqlite3_column_int(self.sqlStament, 6)) {
            status.rstat_PhysicalCheck=sqlite3_column_int(self.sqlStament, 6);
        }
        if (sqlite3_column_int(self.sqlStament, 7)) {
            status.rstat_FindStatus=sqlite3_column_int(self.sqlStament, 7);
        }
        
        if(isOcupied){
            if(status.rstat_Id == ENUM_ROOM_STATUS_OI){
                [array addObject:status];
            }
            if (status.rstat_Id == ENUM_ROOM_STATUS_OD) {
                [array addObject:status];
            }
        } else{
            if(status.rstat_Id == ENUM_ROOM_STATUS_VI){
                [array addObject:status];
            }
            if (status.rstat_Id == ENUM_ROOM_STATUS_VD) {
                [array addObject:status];
            }
            
            //Add by CR 5.0, always show block statuses
            if (status.rstat_Id == ENUM_ROOM_STATUS_OOO || status.rstat_Id == ENUM_ROOM_STATUS_OOS) {
                [array addObject:status];
            }
        }
     
        status_ = sqlite3_step(sqlStament);
    }
    
    if (status_ == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }

    
    return array;
    
}

-(NSMutableArray *) loadAllSuperVisorRoomStatusByIsOcupied:(BOOL)isOcupied andChekList:(NSInteger)tchk{
    NSMutableArray *array = [NSMutableArray array];
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@, %@ FROM %@", rstat_id, rstat_code, rstat_name, rstat_lang, rstat_image, rstat_last_modified, rstat_physical_check, rstat_find_status, ROOM_STATUS];
    
    const char *sql =[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}

    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }    
    
    NSInteger status_ = sqlite3_step(sqlStament);
    while(status_ == SQLITE_ROW) {
        RoomStatusModelV2 *status = [[RoomStatusModelV2 alloc] init];
        status.rstat_Id = sqlite3_column_int(self.sqlStament, 0);
        if (sqlite3_column_text(self.sqlStament, 1)) {
            status.rstat_Code=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
        }
        if (sqlite3_column_text(self.sqlStament, 2)) {
            status.rstat_Name=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
        }
        if (sqlite3_column_text(self.sqlStament, 3)) {
            status.rstat_Lang=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
        }
        if (sqlite3_column_bytes(self.sqlStament, 4) > 0) {
            status.rstat_Image = [NSData dataWithBytes:sqlite3_column_blob
                                  (self.sqlStament, 4) length:sqlite3_column_bytes(self.sqlStament, 4)];
        }            
        if (sqlite3_column_text(self.sqlStament, 5)) {
            status.rstat_LastModified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
        }
        if (sqlite3_column_int(self.sqlStament, 6)) {
            status.rstat_PhysicalCheck=sqlite3_column_int(self.sqlStament, 6);
        }
        if (sqlite3_column_int(self.sqlStament, 7)) {
            status.rstat_FindStatus=sqlite3_column_int(self.sqlStament, 7);
        }
        //            if(tchk == tChkPass){
        if(isOcupied){
            if(status.rstat_Id == ENUM_ROOM_STATUS_OI){
                [array addObject:status];
            }
        } else{
            if(status.rstat_Id == ENUM_ROOM_STATUS_VI){
                [array addObject:status];
            }
        }
        
        //            } else {
        if(isOcupied){
            if(status.rstat_Id == ENUM_ROOM_STATUS_OD){
                [array addObject:status];
            }
        } else{
            if(status.rstat_Id == ENUM_ROOM_STATUS_VD){
                [array addObject:status];
            }
        }
        
        //            }
        
        if (isOcupied == NO) {
            //Add by CR 5.0, always show block statuses
            if (status.rstat_Id == ENUM_ROOM_STATUS_OOO || status.rstat_Id == ENUM_ROOM_STATUS_OOS) {
                [array addObject:status];
            }
        }
     
        status_ = sqlite3_step(sqlStament);
    }
    
    if (status_ == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }

    
    return array;
    
}


-(NSMutableArray *) loadAllSuperVisorBlockRoomStatusByIsOcupied:(BOOL)isOcupied {
    NSMutableArray *array = [NSMutableArray array];
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@, %@ FROM %@", rstat_id, rstat_code, rstat_name, rstat_lang, rstat_image, rstat_last_modified, rstat_physical_check, rstat_find_status, ROOM_STATUS];
    
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}

    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }    
    
    NSInteger status_ = sqlite3_step(sqlStament);
    while(status_ == SQLITE_ROW) {
        RoomStatusModelV2 *status = [[RoomStatusModelV2 alloc] init];
        status.rstat_Id = sqlite3_column_int(self.sqlStament, 0);
        if (sqlite3_column_text(self.sqlStament, 1)) {
            status.rstat_Code=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
        }
        if (sqlite3_column_text(self.sqlStament, 2)) {
            status.rstat_Name=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
        }
        if (sqlite3_column_text(self.sqlStament, 3)) {
            status.rstat_Lang=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
        }
        if (sqlite3_column_bytes(self.sqlStament, 4) > 0) {
            status.rstat_Image = [NSData dataWithBytes:sqlite3_column_blob
                                  (self.sqlStament, 4) length:sqlite3_column_bytes(self.sqlStament, 4)];
        }            
        if (sqlite3_column_text(self.sqlStament, 5)) {
            status.rstat_LastModified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
        }
        if (sqlite3_column_int(self.sqlStament, 6)) {
            status.rstat_PhysicalCheck=sqlite3_column_int(self.sqlStament, 6);
        }
        if (sqlite3_column_int(self.sqlStament, 7)) {
            status.rstat_FindStatus=sqlite3_column_int(self.sqlStament, 7);
        }
        
        if(!isOcupied){
            if(status.rstat_Id == ENUM_ROOM_STATUS_OOO||status.rstat_Id == ENUM_ROOM_STATUS_OOS){
                [array addObject:status];
            }
        }
        
        status_ = sqlite3_step(sqlStament);
    }
    
    if (status_ == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }

    
    return array;
    
}

-(NSString*)getRoomStatusLastModifiedDate
{
    NSMutableString * sqlString=[[NSMutableString alloc] initWithFormat:@"SELECT MAX(%@) FROM %@ ORDER BY %@ ASC", rstat_last_modified, ROOM_STATUS, rstat_last_modified];
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    reAccessCount ++;
}
    
    NSString *lastModified;
    
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        
        status = sqlite3_step(sqlStament);
        while(status == SQLITE_ROW) {
            
            if(sqlite3_column_text(sqlStament, 0)){
                lastModified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 0)];
            }
            return lastModified;
        }
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    return  lastModified;
}

-(RoomStatusModelV2 *)loadRoomStatusByStatusNameData:(RoomStatusModelV2 *)status
{
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ?", rstat_id, rstat_code, rstat_name, rstat_lang, rstat_image, rstat_last_modified, rstat_physical_check, rstat_find_status, ROOM_STATUS, rstat_name];
    
    const char *sql =[sqlString UTF8String];
    //NSLog(@"sqlString %@",sqlString);

    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}

    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) { 
    }
    
    sqlite3_bind_text(sqlStament, 1, [status.rstat_Name UTF8String], -1, SQLITE_TRANSIENT);
    NSInteger status_ = sqlite3_step(sqlStament);
    while(status_ == SQLITE_ROW) {
        status.rstat_Id = sqlite3_column_int(self.sqlStament, 0);
        
        if (sqlite3_column_text(self.sqlStament, 1)) {
            status.rstat_Code=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
        }
        
        if (sqlite3_column_text(self.sqlStament, 2)) {
            status.rstat_Name=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
        }
        
        if (sqlite3_column_text(self.sqlStament, 3)) {
            status.rstat_Lang=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
        }
        
        if (sqlite3_column_bytes(self.sqlStament, 4) > 0) {
            status.rstat_Image = [NSData dataWithBytes:sqlite3_column_blob
                                  (self.sqlStament, 4) length:sqlite3_column_bytes(self.sqlStament, 4)];
        }            
        
        if (sqlite3_column_text(self.sqlStament, 5)) {
            status.rstat_LastModified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
        }
        if (sqlite3_column_int(self.sqlStament, 6)) {
            status.rstat_PhysicalCheck=sqlite3_column_int(self.sqlStament, 6);
        }
        if (sqlite3_column_int(self.sqlStament, 7)) {
            status.rstat_FindStatus=sqlite3_column_int(self.sqlStament, 7);
        }
        
        status_ = sqlite3_step(sqlStament);
    }
    
    if (status_ == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }

    
    return status;

}

@end
