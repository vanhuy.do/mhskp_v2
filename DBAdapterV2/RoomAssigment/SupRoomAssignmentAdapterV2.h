//
//  SupRoomAssignmentAdapter.h
//  eHouseKeeping
//
//  Created by chinh bx on 6/15/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DbDefinesV2.h"
#import "DatabaseAdapter.h"
#define kRoomNo              @"RoomNo"    //@ RoomID in room table
#define kRoomStatusId         @"roomStatusId"
#define kRoomInspectedStatusId  @"roomInspectedStatusId"
#define kRMStatus            @"RMStatus"    // 
#define kCleaningStatus      @"CleaningStatus"
#define kCleaningStatusID    @"Cleaning Status ID"
#define kVIP                 @"VIP"
#define kGuestName           @"GuestName"
#define kRoomAssignmentID    @"RoomAssignmentID"
#define kDurationUsed        @"DurationUsed"
#define kTotalTimeUsed       @"TotalTimeUsed"
#define kRaPrioritySortOrder @"RaPrioritySortOrder"
#define kRoom_Type_Id        @"RoomTypeID"          
#define kExpected_Clean_time @"Expected_Clean_time"
#define KInspection_Status_id @"KInspection_Status_id"
#define kDurationInspectedTime @"DurationInspectedTime"
#define kRAFirstName            @"room assignment first name"
#define kRALastName             @"room assignment last name"
#define kGuestFirstName         @"guest first name"
#define kGuestLastName          @"guest last name"
#define kRaGuestArrivalTime     @"raGuestArrivalTime"
#define kRaGuestDepartureTime   @"raGuestDepartureTime"
#define kRaKindOfRoom           @"raKindOfRoom"
#define kRaIsMockRoom           @"raIsMockRoom"
#define kRaCleaningCredit           @"raCleaningCredit"
#define kRoomStatusImage        @"roomStatusImage"
#define kRoomAssignmentTime     @"roomAssignmentTime"
#define kRaIsReassignedRoom     @"raIsReassignedRoom"
#define kRaUserId               @"raUserId"
#define kRoomType               @"RoomType"

@interface SupRoomAssignmentAdapterV2 : DatabaseAdapter {
    
}
+ (SupRoomAssignmentAdapterV2*) createSupRoomAssignmentAdapter;
-(NSMutableArray*)getRoomAssigmentSupByFilterType:(int)filterType filterDetailId:(int)filterDetailId;
-(NSMutableArray*)getRoomAssigmentSup;
-(NSMutableArray *) getAllRoomAssignmentDatas:(NSInteger) userId;
-(NSMutableArray*)getRoomAssigmentSupComlete;
-(NSMutableArray*)getTotalRoomComlete;
-(NSMutableArray*)getRoomInspectionComlete;
-(NSMutableArray*)getRoomInspectionSup;
-(NSMutableArray*)getRoomTypesCurrentUser;
-(NSMutableArray*)getRoomInspectedPassed;
-(NSMutableArray*)getRoomInspectedFailed;
- (NSMutableArray*)getInspectedRoom;
-(NSMutableArray *) getAllRoomInspectionWithSupervisorId:(NSInteger) userId;
-(NSMutableArray *) getAllRoomInspectionWithSupervisorId:(NSInteger) userId andRoomStatusID:(NSInteger) roomID;
-(NSDictionary *) getRoomAssignmentDataByRoomAssignmentId:(NSInteger)roomAssignmentId andRaUserId:(NSInteger) raUserId;
-(NSDictionary *) getRoomAssignmentDataByRoomAssignmentId:(NSInteger)roomAssignmentId andRaUserId:(NSInteger) raUserId isFilterCurrentDate:(BOOL)isFilterCurrentDate;
-(NSMutableArray *) getAllRoomByFloorId:(NSInteger)floorId AndUserId:(NSInteger)userId AndRoomStatus:(NSInteger)roomStatusId;
-(NSMutableArray *) getRoomDetailsListByFloorId:(NSInteger)floorId userId:(NSInteger)userId filterType:(NSInteger)filterType generalFilter:(NSString*)generalFilter;
-(NSMutableArray *) getZoneRoomDetailsListByFloorId:(NSInteger)floorId userId:(NSInteger)userId filterType:(NSInteger)filterType generalFilter:(NSString*)generalFilter;
-(NSMutableArray *) getFloorRoomDetailsListByFloorId:(NSString*)floorId userId:(NSInteger)userId filterType:(NSInteger)filterType generalFilter:(NSString*)generalFilter;

-(NSMutableArray *) getAllRoomByFloorId:(NSInteger)floorId userId:(NSInteger)userId filterType:(NSInteger)filterType statusId:(NSInteger)statusId;
-(NSMutableArray *) getAllRoomByFloorId:(NSInteger)floorId userId:(NSInteger)userId filterType:(NSInteger)filterType generalFilter:(NSString*)generalFilter;
-(NSMutableArray *) getUnassignRoomsByFloorId:(NSInteger)floorId AndUserId:(NSInteger)userId;
-(NSMutableArray *) getUnassignRoomsByUserId:(NSInteger)userId;
-(NSMutableArray *) getAllRoomByFloorId:(NSInteger)floorId AndUserId:(NSInteger)userId;
-(NSMutableArray *) getAllOfRoomByFloorId:(NSInteger)floorId AndUserId:(NSInteger)userId andFilter:(int)kindOfRoom;
-(NSMutableArray *) getAllOfRoomByFloorId:(NSInteger)floorId AndUserId:(NSInteger)userId;
-(NSMutableArray *) getAllRoomByFloorId:(NSInteger)floorId AndUserId:(NSInteger)userId AndCleaningStatus:(NSInteger)roomStatusId;
-(NSMutableArray *) getAllRoomByFloorId:(NSInteger)floorId AndUserId:(NSInteger)userId AndKindOfRoom:(NSInteger)kindOfRoom;
-(NSMutableArray *) getAllRoomByUserId:(NSInteger)userId AndKindOfRoom:(NSInteger)kindOfRoom;
-(int)updateIsCheckedRa: (NSInteger)userID houseKeeperId:(NSString*)houseKeeperId;
-(int)updateIsCheckedFind: (NSInteger)raID;
@end
