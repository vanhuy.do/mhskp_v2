//
//  SupRoomAssignmentAdapter.m
//  eHouseKeeping
//
//  Created by chinh bx on 6/15/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "SupRoomAssignmentAdapterV2.h"
#import "SupRoomAssignmentAdapterV2Demo.h"
#import "RoomAssignmentModelV2.h"
//#import "RoomStatusModel.h"
#import "UserManagerV2.h"
#import "DateTimeUtility.h"
#import "RoomRecordModelV2.h"
#import "RoomManagerV2.h"
#import "UserManagerV2.h"
//#import "CleaningStatusModel.m"
#import "CommonVariable.h"
#import "LogFileManager.h"
#import "LanguageManagerV2.h"
#import "FindByRoomFloorViewV2.h"
#import "FindManagerV2.h"
#import "ehkDefinesV2.h"


@interface SupRoomAssignmentAdapterV2 (PrivateMethods)

-(NSInteger) getTimeDifferenceBetweenStartDate:(NSString *) startDate andEndDate:(NSString *) endDate;

@end

@implementation SupRoomAssignmentAdapterV2

+ (SupRoomAssignmentAdapterV2*) createSupRoomAssignmentAdapter
{
    SupRoomAssignmentAdapterV2 *roomAssignmentAdapterInstance;
    if(isDemoMode) {
        roomAssignmentAdapterInstance = [[SupRoomAssignmentAdapterV2Demo alloc] init];
        
    } else {
        
            roomAssignmentAdapterInstance = [[SupRoomAssignmentAdapterV2 alloc] init];
        
    }
    
    return roomAssignmentAdapterInstance;
}

-(NSMutableArray*)getRoomAssigmentSupByFilterType:(int)filterType filterDetailId:(int)filterDetailId
{
    [CommonVariable sharedCommonVariable].currentPauseRoom = noPauseRoom;
    int currentUserID =[[UserManagerV2 sharedUserManager] currentUser].userId;
    
    NSString *currentDate = nil;
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    [f setDateFormat:@"yyyy-MM-dd"];
    currentDate = [NSString stringWithFormat:@"%@ 00:00:00", [f stringFromDate:[NSDate date]]];
    
    NSString *roomStatusName = [NSString stringWithFormat:@"%@", rstat_name];
    NSString *roomTypeName = [NSString stringWithFormat:@"%@",ams_name];
    if(![[LanguageManagerV2 sharedLanguageManager] currentLanguageIsEqualTo:@"%@",ENGLISH_LANGUAGE ]){
        roomStatusName = [NSString stringWithFormat:@"%@",rstat_lang];
        roomTypeName = [NSString stringWithFormat:@"%@", ams_name_lang];
    }
    
    //Filter date by user setting for supervisor
    NSString *assignDateFilter = @"";
    if([[UserManagerV2 sharedUserManager].currentUserAccessRight isEnableRoomMatrix]) {
        assignDateFilter = [NSString stringWithFormat:@" AND %@ >= '%@'",ra_assigned_date, currentDate];
    }
    
    NSMutableString *sqlString = nil;
    if (filterType == FilterOption_Floor) {
        sqlString = [[NSMutableString alloc] initWithFormat:@"%@  %@, %@, %@, %@, %@ ,%@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ from %@ a left join %@ b on b.%@=a.%@ left join %@ c on a.%@=c.%@  left join %@ d on a.%@=d.%@ left join %@ e on b.%@ = e.%@ where a.%@=%d %@ AND b.%@ = %d AND %@ = 0 order by a.%@",
                     @"select  ",
                     ra_room_id,
                     ra_id,
                     
                     //uncomment this for get room status code
                     //rstat_code,
                     
                     //get room status name
                     roomStatusName,
                     
                     cstat_image,
                     room_vip_status,
                     room_guest_name,
                     ra_priority_sort_order,
                     ra_room_inspection_status_id,
                     cstat_id,
                     ra_first_name,
                     ra_last_name,
                     rstat_id,
                     ra_guest_first_name,
                     ra_guest_last_name,
                     ra_guest_arrival_time,
                     ra_guest_departure_time,
                     ra_kind_of_room,
                     ra_is_mock_room,
                     rstat_image,
                     ra_assigned_date,
                     ra_is_reassigned_room,
                     ra_last_cleaning_date,
                     
                     //Add for fixing MHKP-223
                     ra_housekeeper_id,
                     ra_is_checked_ra_id,
                     roomTypeName,
                     
                     ROOM_ASSIGNMENT,
                     ROOM,
                     room_id,
                     ra_room_id,
                     
                     ROOM_STATUS,
                     ra_room_status_id,
                     rstat_id,
                     
                     CLEANING_STATUS,
                     ra_room_cleaning_status_id,
                     cstat_id,
                     
                     ROOM_TYPE,
                     room_type_id,
                     ams_id, //this is room type id in table room_type
                     
                     ra_user_id,
                     currentUserID,
                     assignDateFilter,
                     
                     room_floor_id,
                     filterDetailId,
                     
                     ra_delete,
                     ra_assigned_date
                     
                     ];
    } else if (filterType == FilterOption_RoomStatus) {
        sqlString = [[NSMutableString alloc] initWithFormat:@"%@  %@, %@, %@, %@, %@ ,%@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ from %@ a left join %@ b on b.%@=a.%@ left join %@ c on a.%@=c.%@  left join %@ d on a.%@=d.%@ left join %@ e on b.%@ = e.%@ where a.%@=%d %@ AND a.%@ = %d AND %@ = 0 order by a.%@",
                     @"select  ",
                     ra_room_id,
                     ra_id,
                     
                     //uncomment this for get room status code
                     //rstat_code,
                     
                     //get room status name
                     roomStatusName,
                     
                     cstat_image,
                     room_vip_status,
                     room_guest_name,
                     ra_priority_sort_order,
                     ra_room_inspection_status_id,
                     cstat_id,
                     ra_first_name,
                     ra_last_name,
                     rstat_id,
                     ra_guest_first_name,
                     ra_guest_last_name,
                     ra_guest_arrival_time,
                     ra_guest_departure_time,
                     ra_kind_of_room,
                     ra_is_mock_room,
                     rstat_image,
                     ra_assigned_date,
                     ra_is_reassigned_room,
                     ra_last_cleaning_date,
                     
                     //Add for fixing MHKP-223
                     ra_housekeeper_id,
                     ra_is_checked_ra_id,
                     roomTypeName,
                     
                     ROOM_ASSIGNMENT,
                     ROOM,
                     room_id,
                     ra_room_id,
                     
                     ROOM_STATUS,
                     ra_room_status_id,
                     rstat_id,
                     
                     CLEANING_STATUS,
                     ra_room_cleaning_status_id,
                     cstat_id,
                     
                     ROOM_TYPE,
                     room_type_id,
                     ams_id, //this is room type id in table room_type
                     
                     ra_user_id,
                     currentUserID,
                     assignDateFilter,
                     
                     ra_room_status_id,
                     filterDetailId,
                     
                     ra_delete,
                     ra_assigned_date
                     
                     ];
    } else if (filterType == FilterOption_RoomType) {
        sqlString = [[NSMutableString alloc] initWithFormat:@"%@  %@, %@, %@, %@, %@ ,%@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ from %@ a left join %@ b on b.%@=a.%@ left join %@ c on a.%@=c.%@  left join %@ d on a.%@=d.%@ left join %@ e on b.%@ = e.%@ where a.%@=%d %@ AND e.%@ = %d AND %@ = 0 order by a.%@",
                     @"select  ",
                     ra_room_id,
                     ra_id,
                     
                     //uncomment this for get room status code
                     //rstat_code,
                     
                     //get room status name
                     roomStatusName,
                     
                     cstat_image,
                     room_vip_status,
                     room_guest_name,
                     ra_priority_sort_order,
                     ra_room_inspection_status_id,
                     cstat_id,
                     ra_first_name,
                     ra_last_name,
                     rstat_id,
                     ra_guest_first_name,
                     ra_guest_last_name,
                     ra_guest_arrival_time,
                     ra_guest_departure_time,
                     ra_kind_of_room,
                     ra_is_mock_room,
                     rstat_image,
                     ra_assigned_date,
                     ra_is_reassigned_room,
                     ra_last_cleaning_date,
                     
                     //Add for fixing MHKP-223
                     ra_housekeeper_id,
                     ra_is_checked_ra_id,
                     roomTypeName,
                     
                     ROOM_ASSIGNMENT,
                     ROOM,
                     room_id,
                     ra_room_id,
                     
                     ROOM_STATUS,
                     ra_room_status_id,
                     rstat_id,
                     
                     CLEANING_STATUS,
                     ra_room_cleaning_status_id,
                     cstat_id,
                     
                     ROOM_TYPE,
                     room_type_id,
                     ams_id, //this is room type id in table room_type
                     
                     ra_user_id,
                     currentUserID,
                     assignDateFilter,
                     
                     ams_id,
                     filterDetailId,
                     
                     ra_delete,
                     ra_assigned_date
                     ];
        
    } else if (filterType == FilterOption_Zone) {
        sqlString = [[NSMutableString alloc] initWithFormat:@"%@  %@, %@, %@, %@, %@ ,%@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ from %@ a left join %@ b on b.%@=a.%@ left join %@ c on a.%@=c.%@  left join %@ d on a.%@=d.%@ left join %@ e on b.%@ = e.%@ left join %@ w on w.%@ = a.%@ where a.%@=%d %@ AND w.%@ = %d AND w.%@ = %d AND %@ = 0 order by a.%@",
                     @"select  distinct",
                     ra_room_id,
                     ra_id,
                     
                     //uncomment this for get room status code
                     //rstat_code,
                     
                     //get room status name
                     roomStatusName,
                     
                     cstat_image,
                     room_vip_status,
                     room_guest_name,
                     ra_priority_sort_order,
                     ra_room_inspection_status_id,
                     cstat_id,
                     ra_first_name,
                     ra_last_name,
                     rstat_id,
                     ra_guest_first_name,
                     ra_guest_last_name,
                     ra_guest_arrival_time,
                     ra_guest_departure_time,
                     ra_kind_of_room,
                     ra_is_mock_room,
                     rstat_image,
                     ra_assigned_date,
                     ra_is_reassigned_room,
                     ra_last_cleaning_date,
                     
                     //Add for fixing MHKP-223
                     ra_housekeeper_id,
                     ra_is_checked_ra_id,
                     roomTypeName,
                     
                     ROOM_ASSIGNMENT,
                     ROOM,
                     room_id,
                     ra_room_id,
                     
                     ROOM_STATUS,
                     ra_room_status_id,
                     rstat_id,
                     
                     CLEANING_STATUS,
                     ra_room_cleaning_status_id,
                     cstat_id,
                     
                     ROOM_TYPE,
                     room_type_id,
                     ams_id, //this is room type id in table room_type
                     
                     SUPERVISOR_FILTER_DETAIL,
                     filter_room_number,
                     ra_room_id,
                     
                     ra_user_id,
                     currentUserID,
                     assignDateFilter,
                     
                     filter_type,
                     filterType,
                     
                     filter_id,
                     filterDetailId,
                     
                     ra_delete,
                     ra_assigned_date
                     ];
    } else { //All
        
        sqlString = [[NSMutableString alloc] initWithFormat:@"%@  %@, %@, %@, %@, %@ ,%@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ from %@ a left join %@ b on b.%@=a.%@ left join %@ c on a.%@=c.%@  left join %@ d on a.%@=d.%@ left join %@ e on b.%@ = e.%@ where a.%@=%d %@ AND %@ = 0 order by a.%@",
                     @"select  ",
                     ra_room_id,
                     ra_id,
                     
                     //uncomment this for get room status code
                     //rstat_code,
                     
                     //get room status name
                     roomStatusName,
                     
                     cstat_image,
                     room_vip_status,
                     room_guest_name,
                     ra_priority_sort_order,
                     ra_room_inspection_status_id,
                     cstat_id,
                     ra_first_name,
                     ra_last_name,
                     rstat_id,
                     ra_guest_first_name,
                     ra_guest_last_name,
                     ra_guest_arrival_time,
                     ra_guest_departure_time,
                     ra_kind_of_room,
                     ra_is_mock_room,
                     rstat_image,
                     ra_assigned_date,
                     ra_is_reassigned_room,
                     ra_last_cleaning_date,
                     
                     //Add for fixing MHKP-223
                     ra_housekeeper_id,
                     ra_is_checked_ra_id,
                     roomTypeName,
                     
                     ROOM_ASSIGNMENT,
                     ROOM,
                     room_id,
                     ra_room_id,
                     
                     ROOM_STATUS,
                     ra_room_status_id,
                     rstat_id,
                     
                     CLEANING_STATUS,
                     ra_room_cleaning_status_id,
                     cstat_id,
                     
                     ROOM_TYPE,
                     room_type_id,
                     ams_id, //this is room type id in table room_type
                     
                     ra_user_id,
                     currentUserID,
                     assignDateFilter,
                     
                     ra_delete,
                     
                     //additionalCondition,
                     
                     ra_assigned_date
                     
                     ];
    }
    
    NSMutableArray *rushList = [NSMutableArray array]; // CFG [20160810/CRF-00001440]
    NSMutableArray *tmpList = [NSMutableArray array]; // CFG [20160810/CRF-00001440]
    NSMutableArray *dndList = [NSMutableArray array];
    //    NSMutableArray *serviceLater = [NSMutableArray array];
    NSMutableArray *declinedServiceList = [NSMutableArray array];
    NSMutableArray *result=[NSMutableArray array];
    const char *sql =[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    
    NSInteger status = sqlite3_step(sqlStament);
    
    //Reset counting room started before load data
    [CommonVariable sharedCommonVariable].currentStartedRoom = 0;
    
    while(status == SQLITE_ROW)
    {
        NSString *roomStatusCode = sqlite3_column_text(sqlStament, 2) == nil ? @"": [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
        
        //NSInteger roomInspectionStatus = sqlite3_column_int(sqlStament, 7);
        NSInteger roomStatusId = sqlite3_column_int(self.sqlStament, 11);
        NSInteger cleaningStatusId = sqlite3_column_int(sqlStament, 8);
        
        if ([UserManagerV2 isSupervisor]) {
            
            
            if(roomStatusId == ENUM_ROOM_STATUS_VD ||
               roomStatusId == ENUM_ROOM_STATUS_OD ||
               roomStatusId == ENUM_ROOM_STATUS_OI||
               roomStatusId == ENUM_ROOM_STATUS_VI||
               roomStatusId == ENUM_ROOM_STATUS_OOO ||
               roomStatusId == ENUM_ROOM_STATUS_OOS){
                status = sqlite3_step(sqlStament);
                
                continue;
                
            }
            
            
        }
        else {
            
            if(roomStatusId == ENUM_ROOM_STATUS_OC
               || roomStatusId == ENUM_ROOM_STATUS_VC
               || roomStatusId == ENUM_ROOM_STATUS_OI
               || roomStatusId == ENUM_ROOM_STATUS_VI
               || roomStatusId == ENUM_ROOM_STATUS_OOO
               || roomStatusId == ENUM_ROOM_STATUS_OPU
               || roomStatusId == ENUM_ROOM_STATUS_VPU
               || roomStatusId == ENUM_ROOM_STATUS_OC_SO
               || cleaningStatusId == ENUM_CLEANING_STATUS_COMPLETED){
                
                status = sqlite3_step(sqlStament);
                continue;
                
            }
        }
        
        NSString *roomId = nil;
        if (sqlite3_column_text(self.sqlStament, 0)) {
            roomId = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 0)];
        }
        
        NSString *roomAssignmentID=[[NSString alloc] initWithFormat:@"%d",sqlite3_column_int(self.sqlStament,1)] ;
        
        NSData *image_cleaningStatus = nil;
        if (sqlite3_column_bytes(sqlStament, 3) > 0) {
            image_cleaningStatus = [NSData dataWithBytes:sqlite3_column_blob(sqlStament, 3) length:sqlite3_column_bytes(sqlStament, 3)];
        }
        
        NSString *roomVipStatus=sqlite3_column_text(sqlStament, 4)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
        
        NSString *guestName=sqlite3_column_text(sqlStament, 5)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
        
        NSString *raPrioritySortOrder = @"";
        if(sqlite3_column_int(self.sqlStament, 6)){
            raPrioritySortOrder =  [NSString stringWithFormat:@"%d", sqlite3_column_int(self.sqlStament, 6)];
        }
        
        
        NSString *cleaningStatusIdString = @"";
        if(cleaningStatusId){
            cleaningStatusIdString = [NSString stringWithFormat:@"%d", (int)cleaningStatusId];
        }
        
        NSString *raFirstName = @"";
        if (sqlite3_column_text(sqlStament, 9)) {
            raFirstName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 9)];
        }
        
        NSString *raLastName = @"";
        if (sqlite3_column_text(sqlStament, 10)) {
            raLastName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 10)];
        }
        
        NSString *raGuestFirstName = @"";
        if (sqlite3_column_text(sqlStament, 12)) {
            raGuestFirstName = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 12)];
        }
        
        NSString *raGuestLastName = @"";
        if (sqlite3_column_text(sqlStament, 13)) {
            raGuestLastName = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 13)];
        }
        
        NSString *raGuestArrivalTime = @"";
        if (sqlite3_column_text(sqlStament, 14)) {
            raGuestArrivalTime = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 14)];
        }
        if([LogFileManager isLogConsole])
        {
            NSLog(@"%@",raGuestArrivalTime);
        }
        
        NSString *raGuestDepartureTime = @"";
        if (sqlite3_column_text(sqlStament, 15)) {
            raGuestDepartureTime = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 15)];
        }
        
        NSInteger raKindOfRoom = sqlite3_column_int(sqlStament, 16);
        NSInteger raIsMockRoom = sqlite3_column_int(sqlStament, 17);
        
        NSData *image_roomStatus = nil;
        if (sqlite3_column_bytes(sqlStament, 18) > 0) {
            image_roomStatus = [NSData dataWithBytes:sqlite3_column_blob(sqlStament, 18) length:sqlite3_column_bytes(sqlStament, 18)];
        }
        
        NSString *roomAssignmentTime = nil;
        if (sqlite3_column_bytes(sqlStament, 19)) {
            roomAssignmentTime = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 19)];
        }
        
        NSInteger raIsReassignedRoom = sqlite3_column_int(sqlStament, 20);
        NSString *lastCleaningDate = nil;
        if (sqlite3_column_bytes(sqlStament, 21)) {
            lastCleaningDate = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 21)];
        }
        
        NSString *houseKeeperID = [[NSString alloc] initWithFormat:@"%d",sqlite3_column_int(self.sqlStament,22)] ;
        NSString *isCheckedWithRaId = [[NSString alloc] initWithFormat:@"%d",sqlite3_column_int(self.sqlStament,23)] ;
        
        NSString *roomTypeNameValue = @"";
        if (sqlite3_column_text(sqlStament, 24)) {
            roomTypeNameValue = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 24)];
        }
        
        if (cleaningStatusId == ENUM_CLEANING_STATUS_PAUSE) {
            if ([CommonVariable sharedCommonVariable].currentPauseRoom == noPauseRoom) {
                [CommonVariable sharedCommonVariable].currentPauseRoom = [roomAssignmentID integerValue];
            }
            else if ([CommonVariable sharedCommonVariable].currentPauseRoom != [roomAssignmentID integerValue]
                     && [CommonVariable sharedCommonVariable].currentPauseRoom == greaterThanOnePauseRoom) {
                [CommonVariable sharedCommonVariable].currentPauseRoom = greaterThanOnePauseRoom;
            }
        } else if(cleaningStatusId == ENUM_CLEANING_STATUS_START){
            [CommonVariable sharedCommonVariable].currentStartedRoom ++;
        }
        
        NSMutableDictionary *row = [[NSMutableDictionary alloc] init];
        [row setValue:[NSString stringWithFormat:@"%i",currentUserID] forKey:kRaUserId];
        [row setValue:roomId forKey:kRoomNo];
        [row setValue:roomStatusCode forKey:kRMStatus];
        [row setValue:cleaningStatusIdString forKey:kCleaningStatusID];
        [row setValue:guestName forKey:kGuestName];
        [row setValue:roomAssignmentID forKey:kRoomAssignmentID];
        [row setValue:raPrioritySortOrder forKey:kRaPrioritySortOrder];
        [row setValue:raFirstName forKey:kRAFirstName];
        [row setValue:raLastName forKey:kRALastName];
        [row setValue:image_cleaningStatus forKey:kCleaningStatus];
        [row setValue:raGuestFirstName forKey:kGuestFirstName];
        [row setValue:raGuestLastName forKey:kGuestLastName];
        [row setValue:raGuestArrivalTime forKey:kRaGuestArrivalTime];
        [row setValue:raGuestDepartureTime forKey:kRaGuestDepartureTime];
        [row setValue:[NSString stringWithFormat:@"%i",(int)raKindOfRoom] forKey:kRaKindOfRoom];
        [row setValue:[NSString stringWithFormat:@"%i",(int)raIsMockRoom] forKey:kRaIsMockRoom];
        [row setValue:image_roomStatus forKey:kRoomStatusImage];
        [row setValue:roomAssignmentTime forKey:kRoomAssignmentTime];
        [row setValue:[NSString stringWithFormat:@"%i",(int)raIsReassignedRoom] forKey:kRaIsReassignedRoom];
        [row setValue:roomVipStatus forKey:kVIP];
        [row setValue:[NSString stringWithFormat:@"%i",(int)roomStatusId] forKey:kRoomStatusId];
        [row setValue:houseKeeperID forKey:kraHousekeeperID];
        [row setValue:isCheckedWithRaId forKey:kraCheckedRa];
        [row setValue:lastCleaningDate forKey:kLastCleaningDate];
        [row setValue:roomTypeNameValue forKey:kRoomType];
        
        if (sqlite3_column_int(sqlStament, 8) == ENUM_CLEANING_STATUS_DND) {
            [dndList addObject:row];
        }
        else if (sqlite3_column_int(sqlStament, 8) == ENUM_CLEANING_STATUS_DECLINE_SERVICE) {
            [declinedServiceList addObject:row];
        }
        // CFG [20160810/CRF-00001440] - Rush room sorting.
        else if ((sqlite3_column_int(sqlStament, 16) & ENUM_KIND_OF_ROOM_RUSH) == ENUM_KIND_OF_ROOM_RUSH) { // CFG [20160913/CRF-00001439]
            [rushList addObject:row];
        }
        // CFG [20160810/CRF-00001440] - End.
        else {
            [tmpList addObject:row]; // CFG [20160810/CRF-00001440]
        }
        status = sqlite3_step(sqlStament);
    }
    
    [result addObjectsFromArray:rushList]; // CFG [20160810/CRF-00001440]
    [result addObjectsFromArray:tmpList]; // CFG [20160810/CRF-00001440]
    [result addObjectsFromArray:dndList];
    [result addObjectsFromArray:declinedServiceList];
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            [result removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    return result;
}

-(NSMutableArray*)getRoomAssigmentSup
{
    [CommonVariable sharedCommonVariable].currentPauseRoom = noPauseRoom;
    int currentUserID =[[UserManagerV2 sharedUserManager] currentUser].userId;
    
    // CFG [20161028/CRF-00001872] - Get offset.
    NSInteger nRoomAssignmentDayOffsetMinutes = 0;
    @try {
        nRoomAssignmentDayOffsetMinutes = [[[UserManagerV2 sharedUserManager].currentUserAccessRight getConfigByKey:USER_CONFIG_ROOM_ASSIGNMENT_DAY_OFFSET_MINUTES] integerValue];
    } @catch (NSException *exception) {
        nRoomAssignmentDayOffsetMinutes = 0;
    }
    // CFG [20161028/CRF-00001872] - End.
    
    NSString *currentDate = nil;
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    [f setDateFormat:@"yyyy-MM-dd"];
    currentDate = [NSString stringWithFormat:@"%@ 00:00:00", [f stringFromDate:[[NSDate date] dateByAddingTimeInterval:(-nRoomAssignmentDayOffsetMinutes * 60)]]];
    
    //#warning fix for conrad
    //    NSString *additionalCondition = @"AND a.ra_room_cleaning_status_id != 3";
    //#warning end
    
    NSString *roomStatusName = [NSString stringWithFormat:@"%@", rstat_name];
    NSString *roomTypeName = [NSString stringWithFormat:@"%@",ams_name];
    if(![[LanguageManagerV2 sharedLanguageManager] currentLanguageIsEqualTo:@"%@",ENGLISH_LANGUAGE ]){
        roomStatusName = [NSString stringWithFormat:@"%@",rstat_lang];
        roomTypeName = [NSString stringWithFormat:@"%@", ams_name_lang];
    }
    
    //Filter date by user setting for supervisor
    NSString *assignDateFilter = @"";
    if([[UserManagerV2 sharedUserManager].currentUserAccessRight isEnableRoomMatrix]) {
        assignDateFilter = [NSString stringWithFormat:@" AND %@ >= '%@'",ra_assigned_date, currentDate];
    }
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"%@  %@, %@, %@, %@, %@ ,%@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ from %@ a left join %@ b on b.%@=a.%@ left join %@ c on a.%@=c.%@  left join %@ d on a.%@=d.%@ left join %@ e on b.%@ = e.%@ where a.%@=%d %@ AND %@ = 0 order by a.%@",
                                  @"select  ",
                                  ra_room_id,
                                  ra_id,
                                  
                                  //uncomment this for get room status code
                                  //rstat_code,
                                  
                                  //get room status name
                                  roomStatusName,
                                  
                                  cstat_image,
                                  room_vip_status,
                                  room_guest_name,
                                  ra_priority_sort_order,
                                  ra_room_inspection_status_id,
                                  cstat_id,
                                  ra_first_name,
                                  ra_last_name,
                                  rstat_id,
                                  ra_guest_first_name,
                                  ra_guest_last_name,
                                  ra_guest_arrival_time,
                                  ra_guest_departure_time,
                                  ra_kind_of_room,
                                  ra_is_mock_room,
                                  rstat_image,
                                  ra_assigned_date,
                                  ra_is_reassigned_room,
                                  ra_last_cleaning_date,
                                  
                                  //Add for fixing MHKP-223
                                  ra_housekeeper_id,
                                  ra_is_checked_ra_id,
                                  roomTypeName,
                                  
                                  ROOM_ASSIGNMENT,
                                  ROOM,
                                  room_id,
                                  ra_room_id,
                                  
                                  ROOM_STATUS,
                                  ra_room_status_id,
                                  rstat_id,
                                  
                                  CLEANING_STATUS,
                                  ra_room_cleaning_status_id,
                                  cstat_id,
                                  
                                  ROOM_TYPE,
                                  room_type_id,
                                  ams_id, //this is room type id in table room_type
                                  
                                  ra_user_id,
                                  currentUserID,
                                  assignDateFilter,
                                  
                                  ra_delete,
                                  
                                  //additionalCondition,
                                  
                                  ra_assigned_date
                                  
                                  ];
    
    NSMutableArray *rushList = [NSMutableArray array]; // CFG [20160810/CRF-00001440]
    NSMutableArray *tmpList = [NSMutableArray array]; // CFG [20160810/CRF-00001440]
    NSMutableArray *dndList = [NSMutableArray array];
    //    NSMutableArray *serviceLater = [NSMutableArray array];
    NSMutableArray *declinedServiceList = [NSMutableArray array];
    NSMutableArray *result=[NSMutableArray array];
    const char *sql =[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    
    NSInteger status = sqlite3_step(sqlStament);
    
    //Reset counting room started before load data
    [CommonVariable sharedCommonVariable].currentStartedRoom = 0;
    //Reset previous started room before load data
    [CommonVariable sharedCommonVariable].prevStartedRoomID = 0;
    [CommonVariable sharedCommonVariable].prevStartedRoomNo = @"";
    
    while(status == SQLITE_ROW)
    {
        NSString *roomStatusCode = sqlite3_column_text(sqlStament, 2) == nil ? @"": [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
        
        //NSInteger roomInspectionStatus = sqlite3_column_int(sqlStament, 7);
        NSInteger roomStatusId = sqlite3_column_int(self.sqlStament, 11);
        NSInteger cleaningStatusId = sqlite3_column_int(sqlStament, 8);
        
        if ([UserManagerV2 isSupervisor]) {
            //            if ([roomStatusCode isEqualToString:ROOM_STATUS_VD] || [roomStatusCode isEqualToString:ROOM_STATUS_OD] || [roomStatusCode isEqualToString:ROOM_STATUS_OCI]||[roomStatusCode isEqualToString:ROOM_STATUS_VCI] ||
            //                [roomStatusCode isEqualToString:ROOM_STATUS_OOO] || [roomStatusCode isEqualToString:ROOM_STATUS_OOS] || [roomStatusCode isEqualToString:ROOM_STATUS_OCS]){
            //                status = sqlite3_step(sqlStament);
            //                continue;
            //
            //            }
            
            
            if(roomStatusId == ENUM_ROOM_STATUS_VD ||
               roomStatusId == ENUM_ROOM_STATUS_OD ||
               roomStatusId == ENUM_ROOM_STATUS_OI||
               roomStatusId == ENUM_ROOM_STATUS_VI||
               roomStatusId == ENUM_ROOM_STATUS_OOO ||
               roomStatusId == ENUM_ROOM_STATUS_OOS){
                status = sqlite3_step(sqlStament);
                
                continue;
                
            }
            
            
        }
        else {
            //            if (roomInspectionStatus == ENUM_INSPECTION_COMPLETED_FAIL) {
            //                status = sqlite3_step(sqlStament);
            //                continue;
            //            }
            //            if (roomInspectionStatus == ENUM_INSPECTION_COMPLETED_PASS) {
            //                status = sqlite3_step(sqlStament);
            //                continue;
            //            }
            
            //            if ([roomStatusCode isEqualToString:ROOM_STATUS_OC] || [roomStatusCode isEqualToString:ROOM_STATUS_VC]
            //                ||
            //                [roomStatusCode isEqualToString:ROOM_STATUS_OCI]||[roomStatusCode isEqualToString:ROOM_STATUS_VCI] || [roomStatusCode isEqualToString:ROOM_STATUS_OOO] || [roomStatusCode isEqualToString:ROOM_STATUS_OOS] || [roomStatusCode isEqualToString:ROOM_STATUS_OPU] || [roomStatusCode isEqualToString:ROOM_STATUS_VPU]  ||[roomStatusCode isEqualToString:ROOM_STATUS_OCS]  )
            //            {
            //                status = sqlite3_step(sqlStament);
            //                continue;
            //            }
            
            if(roomStatusId == ENUM_ROOM_STATUS_OC
               || roomStatusId == ENUM_ROOM_STATUS_VC
               || roomStatusId == ENUM_ROOM_STATUS_OI
               || roomStatusId == ENUM_ROOM_STATUS_VI
               || roomStatusId == ENUM_ROOM_STATUS_OOO
               || roomStatusId == ENUM_ROOM_STATUS_OPU
               || roomStatusId == ENUM_ROOM_STATUS_VPU
               || roomStatusId == ENUM_ROOM_STATUS_OC_SO
               || cleaningStatusId == ENUM_CLEANING_STATUS_COMPLETED){
                
                status = sqlite3_step(sqlStament);
                continue;
                
            }
        }
        
        //Hao Tran - remove because change from int to string
        //NSString *roomId = [[NSString alloc] initWithFormat:@"%d",sqlite3_column_int(self.sqlStament,0)] ;
        
        NSString *roomId = nil;
        if (sqlite3_column_text(self.sqlStament, 0)) {
            roomId = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 0)];
        }
        
        NSString *roomAssignmentID=[[NSString alloc] initWithFormat:@"%d",sqlite3_column_int(self.sqlStament,1)] ;
        
        NSData *image_cleaningStatus = nil;
        if (sqlite3_column_bytes(sqlStament, 3) > 0) {
            image_cleaningStatus = [NSData dataWithBytes:sqlite3_column_blob(sqlStament, 3) length:sqlite3_column_bytes(sqlStament, 3)];
        }
        
        NSString *roomVipStatus=sqlite3_column_text(sqlStament, 4)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
        
        NSString *guestName=sqlite3_column_text(sqlStament, 5)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
        
        NSString *raPrioritySortOrder = @"";
        if(sqlite3_column_int(self.sqlStament, 6)){
            raPrioritySortOrder =  [NSString stringWithFormat:@"%d", sqlite3_column_int(self.sqlStament, 6)];
        }
        
        
        NSString *cleaningStatusIdString = @"";
        if(cleaningStatusId){
            cleaningStatusIdString = [NSString stringWithFormat:@"%d", (int)cleaningStatusId];
        }
        
        NSString *raFirstName = @"";
        if (sqlite3_column_text(sqlStament, 9)) {
            raFirstName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 9)];
        }
        
        NSString *raLastName = @"";
        if (sqlite3_column_text(sqlStament, 10)) {
            raLastName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 10)];
        }
        
        NSString *raGuestFirstName = @"";
        if (sqlite3_column_text(sqlStament, 12)) {
            raGuestFirstName = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 12)];
        }
        
        NSString *raGuestLastName = @"";
        if (sqlite3_column_text(sqlStament, 13)) {
            raGuestLastName = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 13)];
        }
        
        NSString *raGuestArrivalTime = @"";
        if (sqlite3_column_text(sqlStament, 14)) {
            raGuestArrivalTime = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 14)];
        }
        if([LogFileManager isLogConsole])
        {
            NSLog(@"%@",raGuestArrivalTime);
        }
        
        NSString *raGuestDepartureTime = @"";
        if (sqlite3_column_text(sqlStament, 15)) {
            raGuestDepartureTime = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 15)];
        }
        
        if([LogFileManager isLogConsole])
        {
            NSLog(@"%@",raGuestDepartureTime);
        }
        
        NSInteger raKindOfRoom = sqlite3_column_int(sqlStament, 16);
        NSInteger raIsMockRoom = sqlite3_column_int(sqlStament, 17);
        
        NSData *image_roomStatus = nil;
        if (sqlite3_column_bytes(sqlStament, 18) > 0) {
            image_roomStatus = [NSData dataWithBytes:sqlite3_column_blob(sqlStament, 18) length:sqlite3_column_bytes(sqlStament, 18)];
        }
        
        NSString *roomAssignmentTime = nil;
        if (sqlite3_column_bytes(sqlStament, 19)) {
            roomAssignmentTime = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 19)];
        }
        
        NSInteger raIsReassignedRoom = sqlite3_column_int(sqlStament, 20);
        NSString *lastCleaningDate = nil;
        if (sqlite3_column_bytes(sqlStament, 21)) {
            lastCleaningDate = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 21)];
        }
        
        NSString *houseKeeperID = [[NSString alloc] initWithFormat:@"%d",sqlite3_column_int(self.sqlStament,22)] ;
        NSString *isCheckedWithRaId = [[NSString alloc] initWithFormat:@"%d",sqlite3_column_int(self.sqlStament,23)] ;
        
        NSString *roomTypeNameValue = @"";
        if (sqlite3_column_text(sqlStament, 24)) {
            roomTypeNameValue = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 24)];
        }
        
        //        NSDictionary *returnRow =[[NSDictionary alloc] initWithObjectsAndKeys:roomId,kRoomNo,
        //                                  roomStatusCode,kRMStatus,
        //                                  roomVipStatus,kVIP,
        //                                  cleaningStatusIdString,kCleaningStatusID,
        //                                  guestName,kGuestName,
        //                                  roomAssignmentID, kRoomAssignmentID,
        //                                  raPrioritySortOrder, kRaPrioritySortOrder,
        //                                  raFirstName, kRAFirstName,
        //                                  raLastName, kRALastName,
        //                                  image_cleaningStatus, kCleaningStatus,
        //                                  raGuestFirstName, kGuestFirstName,
        //                                  raGuestLastName, kGuestLastName,
        //                                  raGuestArrivalTime,kRaGuestArrivalTime,
        //                                  raGuestDepartureTime,kRaGuestDepartureTime,
        //                                  [NSString stringWithFormat:@"%i",raKindOfRoom],kRaKindOfRoom,
        //                                  [NSString stringWithFormat:@"%i",raIsMockRoom],kRaIsMockRoom,
        //                                  nil];
        
        if (cleaningStatusId == ENUM_CLEANING_STATUS_PAUSE) {
            if ([CommonVariable sharedCommonVariable].currentPauseRoom == noPauseRoom) {
                [CommonVariable sharedCommonVariable].currentPauseRoom = [roomAssignmentID integerValue];
            }
            else if ([CommonVariable sharedCommonVariable].currentPauseRoom != [roomAssignmentID integerValue]
                     && [CommonVariable sharedCommonVariable].currentPauseRoom == greaterThanOnePauseRoom) {
                [CommonVariable sharedCommonVariable].currentPauseRoom = greaterThanOnePauseRoom;
            }
        } else if(cleaningStatusId == ENUM_CLEANING_STATUS_START){
            [CommonVariable sharedCommonVariable].currentStartedRoom ++;
            [CommonVariable sharedCommonVariable].prevStartedRoomID = [roomAssignmentID intValue];
            [CommonVariable sharedCommonVariable].prevStartedRoomNo = roomId;
        }
        
        NSMutableDictionary *row = [[NSMutableDictionary alloc] init];
        [row setValue:[NSString stringWithFormat:@"%i",currentUserID] forKey:kRaUserId];
        [row setValue:roomId forKey:kRoomNo];
        [row setValue:roomStatusCode forKey:kRMStatus];
        [row setValue:cleaningStatusIdString forKey:kCleaningStatusID];
        [row setValue:guestName forKey:kGuestName];
        [row setValue:roomAssignmentID forKey:kRoomAssignmentID];
        [row setValue:raPrioritySortOrder forKey:kRaPrioritySortOrder];
        [row setValue:raFirstName forKey:kRAFirstName];
        [row setValue:raLastName forKey:kRALastName];
        [row setValue:image_cleaningStatus forKey:kCleaningStatus];
        [row setValue:raGuestFirstName forKey:kGuestFirstName];
        [row setValue:raGuestLastName forKey:kGuestLastName];
        [row setValue:raGuestArrivalTime forKey:kRaGuestArrivalTime];
        [row setValue:raGuestDepartureTime forKey:kRaGuestDepartureTime];
        [row setValue:[NSString stringWithFormat:@"%i",(int)raKindOfRoom] forKey:kRaKindOfRoom];
        [row setValue:[NSString stringWithFormat:@"%i",(int)raIsMockRoom] forKey:kRaIsMockRoom];
        [row setValue:image_roomStatus forKey:kRoomStatusImage];
        [row setValue:roomAssignmentTime forKey:kRoomAssignmentTime];
        [row setValue:[NSString stringWithFormat:@"%i",(int)raIsReassignedRoom] forKey:kRaIsReassignedRoom];
        [row setValue:roomVipStatus forKey:kVIP];
        [row setValue:[NSString stringWithFormat:@"%i",(int)roomStatusId] forKey:kRoomStatusId];
        [row setValue:houseKeeperID forKey:kraHousekeeperID];
        [row setValue:isCheckedWithRaId forKey:kraCheckedRa];
        [row setValue:lastCleaningDate forKey:kLastCleaningDate];
        [row setValue:roomTypeNameValue forKey:kRoomType];
        
        if (sqlite3_column_int(sqlStament, 8) == ENUM_CLEANING_STATUS_DND) {
            [dndList addObject:row];
        }
        else if (sqlite3_column_int(sqlStament, 8) == ENUM_CLEANING_STATUS_DECLINE_SERVICE) {
            [declinedServiceList addObject:row];
        }
        //        else if (sqlite3_column_int(sqlStament, 8) == ENUM_CLEANING_STATUS_SERVICE_LATER) {
        //            [serviceLater addObject:row];
        //        }
        // CFG [20160810/CRF-00001440] - Rush room sorting.
        else if ((sqlite3_column_int(sqlStament, 16) & ENUM_KIND_OF_ROOM_RUSH) == ENUM_KIND_OF_ROOM_RUSH) { // CFG [20160913/CRF-00001439]
            [rushList addObject:row];
        }
        // CFG [20160810/CRF-00001440] - End.
        else {
            [tmpList addObject:row];
        }
        status = sqlite3_step(sqlStament);
        
    }
    
    //    [result addObjectsFromArray:serviceLater];
    [result addObjectsFromArray:rushList]; // CFG [20160810/CRF-00001440]
    [result addObjectsFromArray:tmpList]; // CFG [20160810/CRF-00001440]
    [result addObjectsFromArray:dndList];
    [result addObjectsFromArray:declinedServiceList];
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            [result removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    return result;
}

-(NSMutableArray *) getAllRoomAssignmentDatas:(NSInteger) userId
{
    NSString *currentDate = nil;
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    [f setDateFormat:@"yyyy-MM-dd"];
    currentDate = [NSString stringWithFormat:@"%@ 00:00:00", [f stringFromDate:[NSDate date]]];
    NSString *roomStatus = nil;
    if([[LanguageManagerV2 sharedLanguageManager] currentLanguageIsEqualTo:@"%@", ENGLISH_LANGUAGE]) {
        roomStatus = [NSString stringWithFormat:@"%@",rstat_name];
    } else {
        roomStatus = [NSString stringWithFormat:@"%@",rstat_lang];
    }
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"%@  %@, %@, %@, %@, %@ ,%@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ from %@ a left join %@ b on b.%@=a.%@ left join %@ c on a.%@=c.%@  left join %@ d on a.%@=d.%@  where a.%@=%d AND %@ >= '%@' AND %@ = 0 order by a.%@",
                                  @"select  ",
                                  ra_room_id,
                                  ra_id,
                                  
                                  //uncomment this for get roomstatus code
                                  //rstat_code,
                                  
                                  //get with room status name
                                  roomStatus,
                                  cstat_image,
                                  room_vip_status,
                                  room_guest_name,
                                  ra_priority_sort_order,
                                  ra_room_inspection_status_id,
                                  cstat_id,
                                  ra_first_name,
                                  ra_last_name,
                                  rstat_id,
                                  ra_guest_first_name,
                                  ra_guest_last_name,
                                  ra_guest_arrival_time,
                                  ra_guest_departure_time,
                                  ra_kind_of_room,
                                  ra_is_mock_room,
                                  rstat_image,
                                  ra_assigned_date,
                                  ra_is_reassigned_room,
                                  
                                  ROOM_ASSIGNMENT,
                                  ROOM,
                                  room_id,
                                  ra_room_id,
                                  
                                  ROOM_STATUS,
                                  ra_room_status_id,
                                  rstat_id,
                                  
                                  CLEANING_STATUS,
                                  ra_room_cleaning_status_id,
                                  cstat_id,
                                  
                                  
                                  ra_user_id,
                                  (int)userId,
                                  ra_assigned_date,
                                  currentDate,
                                  
                                  ra_delete,
                                  
                                  //additionalCondition,
                                  
                                  ra_assigned_date
                                  
                                  ];
    
    NSMutableArray *result = [NSMutableArray array];
    const char *sql =[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW)
    {
        NSString *roomStatusCode = sqlite3_column_text(sqlStament, 2) == nil ? @"": [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
        
        NSInteger roomInspectionStatus = sqlite3_column_int(sqlStament, 7);
        NSInteger roomStatusId = sqlite3_column_int(self.sqlStament, 11);
        
        NSString *roomId = nil;
        if (sqlite3_column_text(self.sqlStament, 0)) {
            roomId = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 0)];
        }
        NSString *roomAssignmentID=[[NSString alloc] initWithFormat:@"%d",sqlite3_column_int(self.sqlStament,1)] ;
        
        NSData *image_cleaningStatus = nil;
        if (sqlite3_column_bytes(sqlStament, 3) > 0) {
            image_cleaningStatus = [NSData dataWithBytes:sqlite3_column_blob(sqlStament, 3) length:sqlite3_column_bytes(sqlStament, 3)];
        }
        
        //NSString *roomVipStatus=[[NSString alloc] initWithFormat:@"%d",sqlite3_column_int(self.sqlStament,4)] ;
        
        NSString *guestName=sqlite3_column_text(sqlStament, 5)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
        
        NSString *raPrioritySortOrder = @"";
        if(sqlite3_column_int(self.sqlStament, 6)){
            raPrioritySortOrder =  [NSString stringWithFormat:@"%d", sqlite3_column_int(self.sqlStament, 6)];
        }
        
        
        NSInteger cleaningStatusId = sqlite3_column_int(sqlStament, 8);
        NSString *cleaningStatusIdString =@"";
        if(cleaningStatusId){
            cleaningStatusIdString = [NSString stringWithFormat:@"%d", (int)cleaningStatusId];
        }
        
        NSString *raFirstName = @"";
        if (sqlite3_column_text(sqlStament, 9)) {
            raFirstName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 9)];
        }
        
        NSString *raLastName = @"";
        if (sqlite3_column_text(sqlStament, 10)) {
            raLastName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 10)];
        }
        
        NSString *raGuestFirstName = @"";
        if (sqlite3_column_text(sqlStament, 12)) {
            raGuestFirstName = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 12)];
        }
        
        NSString *raGuestLastName = @"";
        if (sqlite3_column_text(sqlStament, 13)) {
            raGuestLastName = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 13)];
        }
        
        NSString *raGuestArrivalTime = @"";
        if (sqlite3_column_text(sqlStament, 14)) {
            raGuestArrivalTime = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 14)];
        }
        if([LogFileManager isLogConsole])
        {
            NSLog(@"%@",raGuestArrivalTime);
        }
        
        NSString *raGuestDepartureTime = @"";
        if (sqlite3_column_text(sqlStament, 15)) {
            raGuestDepartureTime = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 15)];
        }
        if([LogFileManager isLogConsole])
        {
            NSLog(@"%@",raGuestDepartureTime);
        }
        
        NSInteger raKindOfRoom = sqlite3_column_int(sqlStament, 16);
        NSInteger raIsMockRoom = sqlite3_column_int(sqlStament, 17);
        
        NSData *image_roomStatus = nil;
        if (sqlite3_column_bytes(sqlStament, 18) > 0) {
            image_roomStatus = [NSData dataWithBytes:sqlite3_column_blob(sqlStament, 18) length:sqlite3_column_bytes(sqlStament, 18)];
        }
        
        NSString *roomAssignmentTime = nil;
        if (sqlite3_column_bytes(sqlStament, 19)) {
            roomAssignmentTime = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 19)];
        }
        
        NSInteger raIsReassignedRoom = sqlite3_column_int(sqlStament, 20);
        
        NSMutableDictionary *row = [[NSMutableDictionary alloc] init];
        [row setValue:roomId forKey:kRoomNo];
        [row setValue:[NSString stringWithFormat:@"%i",(int)roomStatusId] forKey:kRoomStatusId];
        [row setValue:[NSString stringWithFormat:@"%i",(int)roomInspectionStatus] forKey:kRoomInspectedStatusId];
        [row setValue:roomStatusCode forKey:kRMStatus];
        [row setValue:cleaningStatusIdString forKey:kCleaningStatusID];
        [row setValue:guestName forKey:kGuestName];
        [row setValue:roomAssignmentID forKey:kRoomAssignmentID];
        [row setValue:raPrioritySortOrder forKey:kRaPrioritySortOrder];
        [row setValue:raFirstName forKey:kRAFirstName];
        [row setValue:raLastName forKey:kRALastName];
        [row setValue:image_cleaningStatus forKey:kCleaningStatus];
        [row setValue:raGuestFirstName forKey:kGuestFirstName];
        [row setValue:raGuestLastName forKey:kGuestLastName];
        [row setValue:raGuestArrivalTime forKey:kRaGuestArrivalTime];
        [row setValue:raGuestDepartureTime forKey:kRaGuestDepartureTime];
        [row setValue:[NSString stringWithFormat:@"%i",(int)raKindOfRoom] forKey:kRaKindOfRoom];
        [row setValue:[NSString stringWithFormat:@"%i",(int)raIsMockRoom] forKey:kRaIsMockRoom];
        [row setValue:image_roomStatus forKey:kRoomStatusImage];
        [row setValue:roomAssignmentTime forKey:kRoomAssignmentTime];
        [row setValue:[NSString stringWithFormat:@"%i",(int)raIsReassignedRoom] forKey:kRaIsReassignedRoom];
        
        [result addObject:row];
        
        status = sqlite3_step(sqlStament);
        
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            [result removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    return result;
}

-(NSMutableArray*)getRoomInspectionSup
{
    
    int currentUserID =[[UserManagerV2 sharedUserManager] currentUser].userId;
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"%@ %@, %@, %@, %@, %@ ,%@, %@, %@, %@, %@, %@  from %@ a left join %@ b on b.%@=a.%@ left join %@ c on a.%@=c.%@  left join %@ d on a.%@=d.%@  left join %@ e on b.%@=e.%@ where a.%@=%d AND %@ = 0 order by a.%@, a.%@ ",
                                  @"select  ",
                                  room_id,
                                  ra_id,
                                  rstat_code,
                                  cstat_image,
                                  room_vip_status,
                                  guest_name,
                                  ra_priority_sort_order,
                                  ra_first_name,
                                  ra_last_name,
                                  ra_guest_first_name,
                                  ra_guest_last_name,
                                  
                                  ROOM_ASSIGNMENT,
                                  ROOM,
                                  room_id,
                                  ra_room_id,
                                  
                                  ROOM_STATUS,
                                  ra_room_status_id,
                                  rstat_id,
                                  
                                  CLEANING_STATUS,
                                  ra_room_cleaning_status_id,
                                  cstat_id,
                                  
                                  GUEST_INFOS,
                                  room_id,
                                  guest_room_id,
                                  
                                  ra_user_id,
                                  currentUserID,
                                  
                                  ra_delete,
                                  
                                  ra_assigned_date,
                                  ra_prioprity
                                  ];
    if([LogFileManager isLogConsole])
    {
        NSLog(@"sqlString %@",sqlString);
    }
    NSMutableArray *result=[NSMutableArray array];
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    
    //sqlite3_bind_int(self.sqlStament, 1, roomAssignment.roomAssignment_Id);
    //  NSInteger stepLoop=0;
    NSInteger status = sqlite3_step(sqlStament);
    
    while(status == SQLITE_ROW)
    {
        
        NSString *roomId = nil;
        if (sqlite3_column_text(self.sqlStament, 0)) {
            roomId = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 0)];
        }
        NSString *roomAssignmentID=[[NSString alloc] initWithFormat:@"%d",sqlite3_column_int(self.sqlStament,1)] ;
        
        NSString *roomStatusCode=sqlite3_column_text(sqlStament, 2)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
        
        NSString *image_cleaningStatus=sqlite3_column_text(sqlStament, 3)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
        
        NSString *roomVipStatus=sqlite3_column_text(sqlStament, 4)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
        
        NSString *guestName=sqlite3_column_text(sqlStament, 5)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
        
        NSString *raPrioritySortOrder = [NSString stringWithFormat:@"%d", sqlite3_column_int(self.sqlStament, 6)];
        
        NSString *raFirstName = @"";
        if (sqlite3_column_text(sqlStament, 7)) {
            raFirstName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 7)];
        }
        
        NSString *raLastName = @"";
        if (sqlite3_column_text(sqlStament, 8)) {
            raLastName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 8)];
        }
        
        NSString *raGuestFirstName = @"";
        if (sqlite3_column_text(sqlStament, 9)) {
            raGuestFirstName = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 9)];
        }
        
        NSString *raGuestLastName = @"";
        if (sqlite3_column_text(sqlStament, 10)) {
            raGuestLastName = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 10)];
        }
        
        NSDictionary *returnRow =[[NSDictionary alloc] initWithObjectsAndKeys:
                                  roomId, kRoomNo,
                                  roomStatusCode, kRMStatus,
                                  roomVipStatus, kVIP,
                                  guestName, kGuestName,
                                  roomAssignmentID, kRoomAssignmentID,
                                  raPrioritySortOrder, kRaPrioritySortOrder,
                                  raFirstName, kRAFirstName,
                                  raLastName, kRALastName,
                                  image_cleaningStatus,kCleaningStatus, nil];
        
        [result addObject:returnRow];
        status = sqlite3_step(sqlStament);
        
    }
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            [result removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    return result;
    
}

-(NSMutableArray*)getRoomAssigmentSupComlete
{
    int currentUserID = [[UserManagerV2 sharedUserManager] currentUser].userId;

    NSString *roomTypeName = [NSString stringWithFormat:@"%@",ams_name];
    if(![[LanguageManagerV2 sharedLanguageManager] currentLanguageIsEqualTo:@"%@",ENGLISH_LANGUAGE ]) {
        roomTypeName = [NSString stringWithFormat:@"%@", ams_name_lang];
    }
    
    NSString *currentDate = nil;
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    [f setDateFormat:@"yyyy-MM-dd"];
    currentDate = [NSString stringWithFormat:@"%@ 00:00:00", [f stringFromDate:[NSDate date]]];
    
    NSString *completed_id = [NSString stringWithFormat:@"(%d)", ENUM_CLEANING_STATUS_COMPLETED];
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"%@ %@, %@, %@, %@, %@ ,%@, %@, %@, %@, %@, %@  from %@ a left join %@ b on b.%@=a.%@ left join %@ c on a.%@=c.%@  left join %@ d on a.%@=d.%@  left join %@ e on b.%@=e.%@ left join %@ f on f.%@ = b.%@ where ((a.%@=%@) and a.%@=%d) AND %@ > '%@' AND %@ = 0 order by a.%@ ",
                                  @"select  ",
                                  room_id,
                                  ra_id,
                                  rstat_code,
                                  cstat_name,
                                  room_vip_status,
                                  room_guest_name,
                                  ra_room_expected_clean_time,
                                  ra_room_inspection_status_id,
                                  ra_first_name,
                                  ra_last_name,
                                  roomTypeName,
                                  
                                  ROOM_ASSIGNMENT,
                                  ROOM,
                                  room_id,
                                  ra_room_id,
                                  
                                  ROOM_STATUS,
                                  ra_room_status_id,
                                  rstat_id,
                                  
                                  CLEANING_STATUS,
                                  ra_room_cleaning_status_id,
                                  cstat_id,
                                  
                                  GUEST_INFOS,
                                  room_id,
                                  guest_room_id,
                                  
                                  ROOM_TYPE,
                                  ams_id,
                                  room_type_id,
                                  
                                  //Where
                                  ra_room_cleaning_status_id,
                                  completed_id,
                                  
                                  ra_user_id,
                                  currentUserID,
                                  ra_assigned_date,
                                  currentDate,
                                  
                                  ra_delete,
                                  
                                  ra_priority_sort_order
                                  ];
    
    NSMutableArray *result=[[NSMutableArray alloc] init] ;
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW)
    {
        
        NSString *roomId = nil;
        if (sqlite3_column_text(self.sqlStament, 0)) {
            roomId = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 0)];
        }
        
        NSString *roomAssignmentID=[[NSString alloc] initWithFormat:@"%d",sqlite3_column_int(self.sqlStament,1)] ;
        
        NSString *roomStatusCode=sqlite3_column_text(sqlStament, 2)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
        
        NSString *cleaningStatus=sqlite3_column_text(sqlStament, 3)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
        
        CleaningStatusModelV2 *csmodel = [[CleaningStatusModelV2 alloc] init];
        csmodel.cstat_Id = ENUM_CLEANING_STATUS_COMPLETED;
        [[RoomManagerV2 sharedRoomManager] loadCleaningStatus:csmodel];
        
//        NSString *inspection_Status = [[NSString alloc] initWithFormat:@"%d",sqlite3_column_int(self.sqlStament,7)];
        if (![csmodel.cstat_Name isEqualToString:cleaningStatus]) {
            status = sqlite3_step(sqlStament);
            continue;
        }
        
//        if([inspection_Status intValue] == ENUM_INSPECTION_COMPLETED_FAIL || [inspection_Status intValue] == ENUM_INSPECTION_COMPLETED_PASS){
//            status = sqlite3_step(sqlStament);
//            continue;
//        }
        
        //CRF-1199: allow OOS show as completed room
        //if(([roomStatusCode isEqualToString:ROOM_STATUS_OOS] ) || ([roomStatusCode isEqualToString:ROOM_STATUS_OOO])){
        if([roomStatusCode isEqualToString:ROOM_STATUS_OOO]){
            status = sqlite3_step(sqlStament);
            continue;
        }
        
        NSString *roomVipStatus=sqlite3_column_text(sqlStament, 4)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
        
        NSString *guestName=sqlite3_column_text(sqlStament, 5)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
        NSString *roomExpectedCleanTime = sqlite3_column_text(sqlStament, 6)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 6)];
        int roomInspectedStatusId = sqlite3_column_int(sqlStament, 7);
        
        // get room record for this room :
        RoomRecordModelV2 *roomRecordModel = [[RoomRecordModelV2 alloc] init];
        roomRecordModel.rrec_room_assignment_id=sqlite3_column_int(self.sqlStament,1);
        roomRecordModel.rrec_User_Id=[[UserManagerV2 sharedUserManager] currentUser].userId;
        [[RoomManagerV2 sharedRoomManager] loadRoomRecordDataByRoomIdAndUserId:roomRecordModel];
        
        // get time clean
        //        int SecondOfDurationTime= round([DateTimeUtility HoursByTime:roomRecordModel.rrec_Cleaning_Start_Time :roomRecordModel.rrec_Cleaning_End_Time]);
        
        //Hao Tran - Changed
        //int SecondOfDurationTime = [roomRecordModel.rrec_Cleaning_Duration intValue];
        int SecondOfDurationTime = roomRecordModel.rrec_Total_Cleaning_Time;
        
        // Get room remark to get clean pause time
        
        //Hao Tran - Remove for change properties
        /*
         NSMutableArray* allRoomRemark =[[RoomManagerV2  sharedRoomManager] loadAllRoomRemarkByRecordId:roomRecordModel.rrec_Id];
         int pauseTime = 0;
         if (allRoomRemark && [allRoomRemark count] > 0) {
         for (RoomRemarkModelV2* aRoomRemark in allRoomRemark) {
         if ([aRoomRemark.rm_Time intValue]) {
         pauseTime += [aRoomRemark.rm_Time intValue];
         }
         }
         }
         SecondOfDurationTime -= pauseTime;
         */
        
        NSString* durationUsed = [NSString stringWithFormat:@"%d", SecondOfDurationTime];
        
        NSString *raFirstName = @"";
        if (sqlite3_column_text(sqlStament, 8)) {
            raFirstName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 8)];
        }
        
        NSString *raLastName = @"";
        if (sqlite3_column_text(sqlStament, 9)) {
            raLastName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 9)];
        }
        
        NSString *roomTypeValue = @"";
        if (sqlite3_column_text(sqlStament, 10)) {
            roomTypeValue = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 10)];
        }
        
        NSDictionary *returnRow =[[NSDictionary alloc] initWithObjectsAndKeys:roomId,kRoomNo,roomStatusCode,kRMStatus,roomVipStatus, kVIP, guestName,kGuestName,roomAssignmentID,kRoomAssignmentID, durationUsed,kDurationUsed, roomExpectedCleanTime, kExpected_Clean_time,[NSString stringWithFormat:@"%d",roomInspectedStatusId], KInspection_Status_id,
                                  raFirstName, kRAFirstName,
                                  raLastName, kRALastName,
                                  roomTypeValue, kRoomType,
                                  nil];
        [result addObject:returnRow];
        status = sqlite3_step(sqlStament);
        
    }
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            [result removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    return result;
    
}

-(NSMutableArray*)getTotalRoomComlete
{
    int currentUserID = [[UserManagerV2 sharedUserManager] currentUser].userId;
    
    NSString *currentDate = nil;
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    [f setDateFormat:@"yyyy-MM-dd"];
    currentDate = [NSString stringWithFormat:@"%@ 00:00:00", [f stringFromDate:[NSDate date]]];
    
    NSString *completed_ids = [NSString stringWithFormat:@"(%d,%d, %d, %d, %d, %d, %d, %d, %d, %d, %d)", ENUM_ROOM_STATUS_OC_SO,ENUM_ROOM_STATUS_OC, ENUM_ROOM_STATUS_VC,ENUM_ROOM_STATUS_OOO, ENUM_ROOM_STATUS_OOS, ENUM_ROOM_STATUS_OPU, ENUM_ROOM_STATUS_VPU, ENUM_ROOM_STATUS_VI, ENUM_ROOM_STATUS_OI, ENUM_ROOM_STATUS_OD, ENUM_ROOM_STATUS_VD];
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"%@ %@, %@, %@, %@, %@ ,%@, %@, %@, %@, %@  from %@ a left join %@ b on b.%@=a.%@ left join %@ c on a.%@=c.%@  left join %@ d on a.%@=d.%@  left join %@ e on b.%@=e.%@ where ((%@ = %d) and a.%@=%d) and (%@ in %@) AND %@ > '%@' AND %@ = 0 order by a.%@ ",
                                  @"select  ",
                                  room_id,
                                  ra_id,
                                  rstat_code,
                                  cstat_name,
                                  room_vip_status,
                                  guest_name,
                                  ra_room_expected_clean_time,
                                  ra_room_inspection_status_id,
                                  ra_first_name,
                                  ra_last_name,
                                  
                                  ROOM_ASSIGNMENT,
                                  ROOM,
                                  room_id,
                                  ra_room_id,
                                  
                                  ROOM_STATUS,
                                  ra_room_status_id,
                                  rstat_id,
                                  
                                  CLEANING_STATUS,
                                  ra_room_cleaning_status_id,
                                  cstat_id,
                                  
                                  GUEST_INFOS,
                                  room_id,
                                  guest_room_id,
                                  ra_room_cleaning_status_id,
                                  ENUM_CLEANING_STATUS_COMPLETED,
                                  
                                  ra_user_id,
                                  currentUserID,
                                  ra_room_status_id,
                                  completed_ids,
                                  ra_assigned_date,
                                  currentDate,
                                  
                                  ra_delete,
                                  
                                  ra_priority_sort_order
                                  ];
    
    NSMutableArray *result=[[NSMutableArray alloc] init] ;
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW)
    {
        
        NSString *roomId = nil;
        if (sqlite3_column_text(self.sqlStament, 0)) {
            roomId = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 0)];
        }
        
        NSString *roomAssignmentID=[[NSString alloc] initWithFormat:@"%d",sqlite3_column_int(self.sqlStament,1)] ;
        
        NSString *roomStatusCode=sqlite3_column_text(sqlStament, 2)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
        
        NSString *cleaningStatus=sqlite3_column_text(sqlStament, 3)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
        
        CleaningStatusModelV2 *csmodel = [[CleaningStatusModelV2 alloc] init];
        csmodel.cstat_Id = ENUM_CLEANING_STATUS_COMPLETED;
        [[RoomManagerV2 sharedRoomManager] loadCleaningStatus:csmodel];
        
        if (![csmodel.cstat_Name isEqualToString:cleaningStatus]) {
            status = sqlite3_step(sqlStament);
            continue;
        }
        
        //CRF-1199: Allow OOS as a complete room
        //if(([roomStatusCode isEqualToString:ROOM_STATUS_OOS] ) || ([roomStatusCode isEqualToString:ROOM_STATUS_OOO])){
        if([roomStatusCode isEqualToString:ROOM_STATUS_OOO]){
            status = sqlite3_step(sqlStament);
            continue;
        }
        
        NSString *roomVipStatus=sqlite3_column_text(sqlStament, 4)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
        
        NSString *guestName=sqlite3_column_text(sqlStament, 5)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
        NSString *roomExpectedCleanTime = sqlite3_column_text(sqlStament, 6)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 6)];
        int roomInspectedStatusId = sqlite3_column_int(sqlStament, 7);
        
        // get room record for this room :
        RoomRecordModelV2 *roomRecordModel = [[RoomRecordModelV2 alloc] init];
        roomRecordModel.rrec_room_assignment_id=sqlite3_column_int(self.sqlStament,1);
        roomRecordModel.rrec_User_Id=[[UserManagerV2 sharedUserManager] currentUser].userId;
        [[RoomManagerV2 sharedRoomManager] loadRoomRecordDataByRoomIdAndUserId:roomRecordModel];
        
        int SecondOfDurationTime = roomRecordModel.rrec_Total_Cleaning_Time;
        
        NSString* durationUsed = [NSString stringWithFormat:@"%d", SecondOfDurationTime];
        
        NSString *raFirstName = @"";
        if (sqlite3_column_text(sqlStament, 9)) {
            raFirstName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 9)];
        }
        
        NSString *raLastName = @"";
        if (sqlite3_column_text(sqlStament, 10)) {
            raLastName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 10)];
        }
        
        NSDictionary *returnRow =[[NSDictionary alloc] initWithObjectsAndKeys:roomId,kRoomNo,roomStatusCode,kRMStatus,roomVipStatus, kVIP, guestName,kGuestName,roomAssignmentID,kRoomAssignmentID, durationUsed,kDurationUsed, roomExpectedCleanTime, kExpected_Clean_time,[NSString stringWithFormat:@"%d",roomInspectedStatusId], KInspection_Status_id,
                                  raFirstName, kRAFirstName,
                                  raLastName, kRALastName,
                                  
                                  nil];
        [result addObject:returnRow];
        status = sqlite3_step(sqlStament);
        
    }
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            [result removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    return result;
    
}

-(NSMutableArray*)getRoomInspectionComlete
{
    int currentUserID = [[UserManagerV2 sharedUserManager] currentUser].userId;
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"%@ %@, %@, %@, %@, %@ , %@, %@, %@  from %@ a left join %@ b on b.%@=a.%@ left join %@ c on a.%@=c.%@  left join %@ d on a.%@=d.%@  left join %@ e on b.%@=e.%@ where ((%@=%d or %@=%d or %@=%d) and a.%@=%d AND %@ = 0) order by a.%@ ",
                                  @"select  ",
                                  room_id,
                                  ra_id,
                                  rstat_code,
                                  cstat_image,
                                  room_vip_status,
                                  guest_name,
                                  ra_first_name,
                                  ra_last_name,
                                  
                                  ROOM_ASSIGNMENT,
                                  ROOM,
                                  room_id,
                                  ra_room_id,
                                  
                                  ROOM_STATUS,
                                  ra_room_status_id,
                                  rstat_id,
                                  
                                  CLEANING_STATUS,
                                  ra_room_cleaning_status_id,
                                  cstat_id,
                                  
                                  GUEST_INFOS,
                                  room_id,
                                  guest_room_id,
                                  //room_cleaning_status_id,
                                  ra_room_status_id,
                                  1,
                                  ra_room_status_id,
                                  3,
                                  ra_room_status_id,
                                  5,
                                  
                                  ra_user_id,
                                  currentUserID,
                                  
                                  ra_delete,
                                  
                                  ra_prioprity
                                  ];
    
    NSMutableArray *result=[[NSMutableArray alloc] init] ;
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
        
        
    }
    
    float floatTotalTimeCleanUsed=0;
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW)
    {
        NSString *roomId = nil;
        if (sqlite3_column_text(self.sqlStament, 0)) {
            roomId = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 0)];
        }
        NSString *roomAssignmentID=[[NSString alloc] initWithFormat:@"%d",sqlite3_column_int(self.sqlStament,1)] ;
        
        NSString *roomStatusCode=sqlite3_column_text(sqlStament, 2)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
        
        NSString *cleaningStatus=sqlite3_column_text(sqlStament, 3)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
        
        NSString *roomVipStatus=sqlite3_column_text(sqlStament, 4)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
        
        NSString *guestName=sqlite3_column_text(sqlStament, 5)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
        
        // get room record for this room :
        RoomRecordModelV2 *roomRecordModel=[[RoomRecordModelV2  alloc] init];
        roomRecordModel.rrec_room_assignment_id=sqlite3_column_int(self.sqlStament,1);
        roomRecordModel.rrec_User_Id=[[UserManagerV2 sharedUserManager] currentUser].userId;
        [[RoomManagerV2 sharedRoomManager] loadRoomRecordData:roomRecordModel];
        //[roomRecordModel load];
        
        //Hao Tran - Remove for fix bug
        float SecondOfDurationTime = ([DateTimeUtility HoursByTime:roomRecordModel.rrec_Cleaning_Start_Time endTime:roomRecordModel.rrec_Cleaning_End_Time]);
        floatTotalTimeCleanUsed+=SecondOfDurationTime/3600; //save time on each room
        
        NSInteger hours =round( SecondOfDurationTime/3600);
        NSInteger minutes = round((SecondOfDurationTime - hours*3600)/60);
        NSInteger seconds = round((SecondOfDurationTime - hours*3600 - minutes*60));
        
        NSString *raFirstName = @"";
        if (sqlite3_column_text(sqlStament, 6)) {
            raFirstName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 6)];
        }
        
        NSString *raLastName = @"";
        if (sqlite3_column_text(sqlStament, 7)) {
            raLastName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 7)];
        }
        
        NSDictionary *returnRow =[[NSDictionary alloc] initWithObjectsAndKeys:roomId,kRoomNo,roomStatusCode,kRMStatus,roomVipStatus,kVIP,cleaningStatus,kCleaningStatus,guestName,kGuestName,roomAssignmentID,kRoomAssignmentID,[NSString stringWithFormat:@"%.2d:%.2d:%.2d", (int)hours, (int)minutes, (int)seconds],kDurationUsed,[NSString stringWithFormat:@"%0.2f",floatTotalTimeCleanUsed],kTotalTimeUsed,
                                  raFirstName, kRAFirstName,
                                  raLastName, kRALastName,
                                  nil];
        
        [result addObject:returnRow];
        status = sqlite3_step(sqlStament);
    }
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            [result removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    return result;
    
}
// Get inspected room list
- (NSMutableArray*)getInspectedRoom
{
    int currentUserID = [[UserManagerV2 sharedUserManager] currentUser].userId;
    //    NSString *inspected_ids = [NSString stringWithFormat:@"(%d)", ENUM_INSPECTION_COMPLETED_PASS];
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@ ,%@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@  from %@ a left join %@ b on b.%@=a.%@ left join %@ c on a.%@=c.%@  left join %@ d on a.%@=d.%@  left join %@ e on b.%@=e.%@ where ((%@ = %d OR %@ = %d) and a.%@=%d AND %@ = 0) order by a.%@ ",
                                  room_id,
                                  ra_id,
                                  rstat_code,
                                  cstat_image,
                                  room_vip_status,
                                  guest_name,
                                  ra_room_expected_inspection_time,
                                  ra_room_inspection_status_id,
                                  rstat_id,
                                  ra_first_name,
                                  ra_last_name,
                                  cstat_id,
                                  cstat_image,
                                  ra_guest_first_name,
                                  ra_guest_last_name,
                                  ra_guest_arrival_time,
                                  ra_guest_departure_time,
                                  ra_kind_of_room,
                                  ra_is_mock_room,
                                  rstat_image,
                                  ra_assigned_date,
                                  ra_is_reassigned_room,
                                  
                                  ROOM_ASSIGNMENT,
                                  ROOM,
                                  room_id,
                                  ra_room_id,
                                  
                                  ROOM_STATUS,
                                  ra_room_status_id,
                                  rstat_id,
                                  
                                  CLEANING_STATUS,
                                  ra_room_cleaning_status_id,
                                  cstat_id,
                                  
                                  GUEST_INFOS,
                                  room_id,
                                  guest_room_id,
                                  
                                  ra_room_inspection_status_id,
                                  ENUM_INSPECTION_COMPLETED_PASS,
                                  ra_room_inspection_status_id,
                                  ENUM_INSPECTION_COMPLETED_FAIL,
                                  ra_user_id,
                                  currentUserID,
                                  
                                  ra_delete,
                                  
                                  ra_prioprity
                                  ];
    
    NSMutableArray *result=[[NSMutableArray alloc] init] ;
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW)
    {
        //when room status is list below, it means the room assignment was not inspected
        NSInteger roomStatusId = sqlite3_column_int(sqlStament, 8);
        switch (roomStatusId) {
            case ENUM_ROOM_STATUS_OC_SO:
            case ENUM_ROOM_STATUS_OC:
            case ENUM_ROOM_STATUS_OOO:
            case ENUM_ROOM_STATUS_OOS:
            case ENUM_ROOM_STATUS_OPU:
            case ENUM_ROOM_STATUS_VC:
            case ENUM_ROOM_STATUS_VCB:
            case ENUM_ROOM_STATUS_VPU:
            {
                status = sqlite3_step(sqlStament);
                continue;
            }
                break;
                
            default:
                break;
        }
        
        NSString *roomId = nil;
        if (sqlite3_column_text(self.sqlStament, 0)) {
            roomId = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 0)];
        }
        
        NSString *roomAssignmentID=[[NSString alloc] initWithFormat:@"%d",sqlite3_column_int(self.sqlStament,1)] ;
        
        NSString *roomStatusCode=sqlite3_column_text(sqlStament, 2)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
        
        //            NSString *cleaningStatus=sqlite3_column_text(sqlStament, 3)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
        NSString *roomVipStatus=sqlite3_column_text(sqlStament, 4)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
        
        NSString *guestName=sqlite3_column_text(sqlStament, 5)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
        
        NSString *roomExpectedCleanTime = sqlite3_column_text(sqlStament, 6)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 6)];
        
        int roomInspectedStatusId = sqlite3_column_int(sqlStament, 7);
        
        
        // get room record for this room :
        RoomRecordModelV2 *roomRecordModel=[[RoomRecordModelV2 alloc] init];
        roomRecordModel.rrec_room_assignment_id = sqlite3_column_int(self.sqlStament,1);
        roomRecordModel.rrec_User_Id = [[UserManagerV2 sharedUserManager] currentUser].userId;
        
        [[RoomManagerV2 sharedRoomManager] loadRoomRecordDataByRoomIdAndUserId:roomRecordModel];
        
        //Hao Tran - Changed
        //NSString* durationUsed = roomRecordModel.rrec_Cleaning_Duration;
        //NSString *inspectedDurationUsed = roomRecordModel.rrec_Inspection_Duration;
        
        NSString* durationUsed = [NSString stringWithFormat:@"%d", roomRecordModel.rrec_Total_Cleaning_Time ];
        NSString *inspectedDurationUsed = [NSString stringWithFormat:@"%d", roomRecordModel.rrec_Total_Inspected_Time];
        NSString *raFirstName = @"";
        
        if (sqlite3_column_text(sqlStament, 9)) {
            raFirstName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 9)];
        }
        
        NSString *raLastName = @"";
        if (sqlite3_column_text(sqlStament, 10)) {
            raLastName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 10)];
        }
        
        NSInteger cleaningStatusId = sqlite3_column_int(sqlStament, 11);
        NSString *cleaningStatusIdString =@"";
        if(cleaningStatusId){
            cleaningStatusIdString = [NSString stringWithFormat:@"%d", (int)cleaningStatusId];
        }
        
        NSData *image_cleaningStatus = nil;
        if (sqlite3_column_bytes(sqlStament, 12) > 0) {
            image_cleaningStatus = [NSData dataWithBytes:sqlite3_column_blob(sqlStament, 12) length:sqlite3_column_bytes(sqlStament, 12)];
        }
        
        NSString *raGuestFirstName = @"";
        if (sqlite3_column_text(sqlStament, 13)) {
            raGuestFirstName = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 13)];
        }
        
        NSString *raGuestLastName = @"";
        if (sqlite3_column_text(sqlStament, 14)) {
            raGuestLastName = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 14)];
        }
        
        NSString *raGuestArrivalTime = @"";
        if (sqlite3_column_text(sqlStament, 15)) {
            raGuestArrivalTime = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 15)];
        }
        if([LogFileManager isLogConsole])
        {
            NSLog(@"%@",raGuestArrivalTime);
        }
        
        NSString *raGuestDepartureTime = @"";
        if (sqlite3_column_text(sqlStament, 16)) {
            raGuestDepartureTime = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 16)];
        }
        
        if([LogFileManager isLogConsole])
        {
            NSLog(@"%@",raGuestDepartureTime);
        }
        
        NSInteger raKindOfRoom = sqlite3_column_int(sqlStament, 17);
        NSInteger raIsMockRoom = sqlite3_column_int(sqlStament, 18);
        
        NSData *image_roomStatus = nil;
        if (sqlite3_column_bytes(sqlStament, 19) > 0) {
            image_roomStatus = [NSData dataWithBytes:sqlite3_column_blob(sqlStament, 19) length:sqlite3_column_bytes(sqlStament, 19)];
        }
        
        NSString *roomAssignmentTime = nil;
        if (sqlite3_column_bytes(sqlStament, 20)) {
            roomAssignmentTime = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 20)];
        }
        
        NSInteger raIsReassignedRoom = sqlite3_column_int(sqlStament, 21);
        
        //        NSDictionary *returnRow =[[NSDictionary alloc] initWithObjectsAndKeys:roomId,kRoomNo,roomStatusCode,kRMStatus,roomVipStatus, kVIP, guestName,kGuestName,roomAssignmentID,kRoomAssignmentID, durationUsed,kDurationUsed, roomExpectedCleanTime, kExpected_Clean_time,[NSString stringWithFormat:@"%d",roomInspectedStatusId], KInspection_Status_id,inspectedDurationUsed,kDurationInspectedTime,
        //                                  raFirstName, kRAFirstName,
        //                                  raLastName, kRALastName,
        //
        //                                  nil];
        
        NSMutableDictionary *row = [[NSMutableDictionary alloc] init];
        [row setValue:[NSString stringWithFormat:@"%i",currentUserID] forKey:kRaUserId];
        [row setValue:roomId forKey:kRoomNo];
        [row setValue:roomStatusCode forKey:kRMStatus];
        [row setValue:cleaningStatusIdString forKey:kCleaningStatusID];
        [row setValue:guestName forKey:kGuestName];
        [row setValue:roomAssignmentID forKey:kRoomAssignmentID];
        [row setValue:raFirstName forKey:kRAFirstName];
        [row setValue:raLastName forKey:kRALastName];
        [row setValue:image_cleaningStatus forKey:kCleaningStatus];
        [row setValue:raGuestFirstName forKey:kGuestFirstName];
        [row setValue:raGuestLastName forKey:kGuestLastName];
        [row setValue:raGuestArrivalTime forKey:kRaGuestArrivalTime];
        [row setValue:raGuestDepartureTime forKey:kRaGuestDepartureTime];
        [row setValue:[NSString stringWithFormat:@"%i",(int)raKindOfRoom] forKey:kRaKindOfRoom];
        [row setValue:[NSString stringWithFormat:@"%i",(int)raIsMockRoom] forKey:kRaIsMockRoom];
        [row setValue:image_roomStatus forKey:kRoomStatusImage];
        [row setValue:roomAssignmentTime forKey:kRoomAssignmentTime];
        [row setValue:roomVipStatus forKey:kVIP];
        [row setValue:guestName forKey:kGuestName];
        [row setValue:durationUsed forKey:kDurationUsed];
        [row setValue:roomExpectedCleanTime forKey:kExpected_Clean_time];
        [row setValue:[NSString stringWithFormat:@"%d",roomInspectedStatusId] forKey:KInspection_Status_id];
        [row setValue:inspectedDurationUsed forKey:kDurationInspectedTime];
        [row setValue:[NSString stringWithFormat:@"%i",(int)raIsReassignedRoom] forKey:kRaIsReassignedRoom];
        
        [result addObject:row];
        status = sqlite3_step(sqlStament);
        
    }
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            [result removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    
    return result;
}
// Get room inspected passed
- (NSMutableArray*)getRoomInspectedPassed
{
    int currentUserID = [[UserManagerV2 sharedUserManager] currentUser].userId;
    
    NSString *currentDate = nil;
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    [f setDateFormat:@"yyyy-MM-dd"];
    currentDate = [NSString stringWithFormat:@"%@ 00:00:00", [f stringFromDate:[NSDate date]]];
    
    NSString *roomTypeName = [NSString stringWithFormat:@"%@",ams_name];
    if(![[LanguageManagerV2 sharedLanguageManager] currentLanguageIsEqualTo:@"%@",ENGLISH_LANGUAGE ]){
        roomTypeName = [NSString stringWithFormat:@"%@", ams_name_lang];
    }
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"%@ %@, %@, %@, %@, %@ ,%@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@  from %@ a left join %@ b on b.%@=a.%@ left join %@ c on a.%@=c.%@  left join %@ d on a.%@=d.%@  left join %@ e on b.%@=e.%@ left join %@ f on f.%@ = b.%@ where ((%@ = %d OR a.%@ = %d OR a.%@ = %d) and a.%@ = %d) AND %@ > '%@' AND %@ = 0 order by a.%@ ",
                                  @"select  ",
                                  room_id,
                                  ra_id,
                                  rstat_code,
                                  cstat_image,
                                  room_vip_status,
                                  guest_name,
                                  ra_room_expected_inspection_time,
                                  ra_room_inspection_status_id,
                                  rstat_id,
                                  ra_first_name,
                                  ra_last_name,
                                  cstat_id,
                                  cstat_image,
                                  ra_guest_first_name,
                                  ra_guest_last_name,
                                  ra_guest_arrival_time,
                                  ra_guest_departure_time,
                                  ra_kind_of_room,
                                  ra_is_mock_room,
                                  rstat_image,
                                  ra_assigned_date,
                                  ra_is_reassigned_room,
                                  roomTypeName,
                                  
                                  ROOM_ASSIGNMENT,
                                  ROOM,
                                  room_id,
                                  ra_room_id,
                                  
                                  ROOM_STATUS,
                                  ra_room_status_id,
                                  rstat_id,
                                  
                                  CLEANING_STATUS,
                                  ra_room_cleaning_status_id,
                                  cstat_id,
                                  
                                  GUEST_INFOS,
                                  room_id,
                                  guest_room_id,
                                  
                                  ROOM_TYPE,
                                  ams_id, //this is room type id in table room_type
                                  room_type_id,
                                  
                                  //WHERE
                                  ra_room_inspection_status_id,
                                  ENUM_INSPECTION_COMPLETED_PASS,
                                  
                                  ra_room_status_id,
                                  ENUM_ROOM_STATUS_OI,
                                  
                                  ra_room_status_id,
                                  ENUM_ROOM_STATUS_VI,
                                  
                                  ra_user_id,
                                  currentUserID,
                                  ra_assigned_date,
                                  currentDate,
                                  
                                  ra_delete,
                                  
                                  ra_prioprity
                                  ];
    
    NSMutableArray *result=[[NSMutableArray alloc] init] ;
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {
    }
    
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW)
    {
        //when room status is list below, it means the room assignment was not inspected pass
        NSInteger roomStatusId = sqlite3_column_int(sqlStament, 8);
        switch (roomStatusId) {
            case ENUM_ROOM_STATUS_OC_SO:
            case ENUM_ROOM_STATUS_OC:
            case ENUM_ROOM_STATUS_OPU:
            case ENUM_ROOM_STATUS_VC:
            case ENUM_ROOM_STATUS_VCB:
            case ENUM_ROOM_STATUS_VPU:
            case ENUM_ROOM_STATUS_VD:
            case ENUM_ROOM_STATUS_OD:
                
            case ENUM_ROOM_STATUS_OOO:
            case ENUM_ROOM_STATUS_OOS:
            {
                status = sqlite3_step(sqlStament);
                continue;
            }
                break;
                
            default:
                break;
        }
        
        NSString *roomId = nil;
        if (sqlite3_column_text(self.sqlStament, 0)) {
            roomId = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 0)];
        }
        
        NSString *roomAssignmentID=[[NSString alloc] initWithFormat:@"%d",sqlite3_column_int(self.sqlStament,1)] ;
        
        NSString *roomStatusCode=sqlite3_column_text(sqlStament, 2)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
        
        //            NSString *cleaningStatus=sqlite3_column_text(sqlStament, 3)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
        NSString *roomVipStatus=sqlite3_column_text(sqlStament, 4)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
        
        NSString *guestName=sqlite3_column_text(sqlStament, 5)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
        NSString *roomExpectedCleanTime = sqlite3_column_text(sqlStament, 6)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 6)];
        
        int roomInspectedStatusId = sqlite3_column_int(sqlStament, 7);
        
        
        // get room record for this room :
        RoomRecordModelV2 *roomRecordModel=[[RoomRecordModelV2 alloc] init];
        roomRecordModel.rrec_room_assignment_id=sqlite3_column_int(self.sqlStament,1);
        roomRecordModel.rrec_User_Id=[[UserManagerV2 sharedUserManager] currentUser].userId;
        
        [[RoomManagerV2 sharedRoomManager] loadRoomRecordDataByRoomIdAndUserId:roomRecordModel];
        
        //            // get time clean
        //            int SecondOfDurationTime= round([DateTimeUtility HoursByTime:roomRecordModel.rrec_Cleaning_Start_Time :roomRecordModel.rrec_Cleaning_End_Time]);
        //            // Get room remark to get clean pause time
        //            NSMutableArray* allRoomRemark =[[RoomManagerV2  sharedRoomManager] loadAllRoomRemarkByRecordId:roomRecordModel.rrec_Id];
        //            int pauseTime = 0;
        //            if (allRoomRemark && [allRoomRemark count]>0) {
        //                for (RoomRemarkModelV2* aRoomRemark in allRoomRemark) {
        //                    if ([aRoomRemark.rm_Time intValue]) {
        //                        pauseTime+=[aRoomRemark.rm_Time intValue];
        //                    }
        //                }
        //            }
        //
        //            SecondOfDurationTime -= pauseTime;
        //            NSString* durationUsed = [NSString stringWithFormat:@"%d",SecondOfDurationTime];
        
        //Hao Tran - changed
        //NSString* durationUsed = roomRecordModel.rrec_Cleaning_Duration;
        NSString* durationUsed = [NSString stringWithFormat:@"%d", roomRecordModel.rrec_Total_Cleaning_Time];
        
        NSString *raFirstName = @"";
        if (sqlite3_column_text(sqlStament, 9)) {
            raFirstName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 9)];
        }
        
        NSString *raLastName = @"";
        if (sqlite3_column_text(sqlStament, 10)) {
            raLastName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 10)];
        }
        
        NSInteger cleaningStatusId = sqlite3_column_int(sqlStament, 11);
        NSString *cleaningStatusIdString =@"";
        if(cleaningStatusId){
            cleaningStatusIdString = [NSString stringWithFormat:@"%d", (int)cleaningStatusId];
        }
        
        NSData *image_cleaningStatus = nil;
        if (sqlite3_column_bytes(sqlStament, 12) > 0) {
            image_cleaningStatus = [NSData dataWithBytes:sqlite3_column_blob(sqlStament, 12) length:sqlite3_column_bytes(sqlStament, 12)];
        }
        
        NSString *raGuestFirstName = @"";
        if (sqlite3_column_text(sqlStament, 13)) {
            raGuestFirstName = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 13)];
        }
        
        NSString *raGuestLastName = @"";
        if (sqlite3_column_text(sqlStament, 14)) {
            raGuestLastName = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 14)];
        }
        
        NSString *raGuestArrivalTime = @"";
        if (sqlite3_column_text(sqlStament, 15)) {
            raGuestArrivalTime = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 15)];
        }
        
        if([LogFileManager isLogConsole])
        {
            NSLog(@"%@",raGuestArrivalTime);
        }
        
        NSString *raGuestDepartureTime = @"";
        if (sqlite3_column_text(sqlStament, 16)) {
            raGuestDepartureTime = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 16)];
        }
        
        if([LogFileManager isLogConsole])
        {
            NSLog(@"%@",raGuestDepartureTime);
        }
        
        NSInteger raKindOfRoom = sqlite3_column_int(sqlStament, 17);
        NSInteger raIsMockRoom = sqlite3_column_int(sqlStament, 18);
        
        NSData *image_roomStatus = nil;
        if (sqlite3_column_bytes(sqlStament, 19) > 0) {
            image_roomStatus = [NSData dataWithBytes:sqlite3_column_blob(sqlStament, 19) length:sqlite3_column_bytes(sqlStament, 19)];
        }
        
        NSString *roomAssignmentTime = nil;
        if (sqlite3_column_bytes(sqlStament, 20)) {
            roomAssignmentTime = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 20)];
        }
        
        NSInteger raIsReassignedRoom = sqlite3_column_int(sqlStament, 21);
        
        //        NSDictionary *returnRow =[[NSDictionary alloc] initWithObjectsAndKeys:roomId,kRoomNo,roomStatusCode,kRMStatus,roomVipStatus, kVIP, guestName,kGuestName,roomAssignmentID,kRoomAssignmentID, durationUsed,kDurationUsed, roomExpectedCleanTime, kExpected_Clean_time,[NSString stringWithFormat:@"%d",roomInspectedStatusId], KInspection_Status_id,
        //                                  raFirstName, kRAFirstName,
        //                                  raLastName, kRALastName,
        //
        //                                  nil];
        
        NSString *roomTypeValue = @"";
        if (sqlite3_column_bytes(sqlStament, 22)) {
            roomTypeValue = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 22)];
        }
        
        NSMutableDictionary *row = [[NSMutableDictionary alloc] init];
        [row setValue:[NSString stringWithFormat:@"%i",currentUserID] forKey:kRaUserId];
        [row setValue:roomId forKey:kRoomNo];
        [row setValue:roomStatusCode forKey:kRMStatus];
        [row setValue:cleaningStatusIdString forKey:kCleaningStatusID];
        [row setValue:guestName forKey:kGuestName];
        [row setValue:roomAssignmentID forKey:kRoomAssignmentID];
        [row setValue:raFirstName forKey:kRAFirstName];
        [row setValue:raLastName forKey:kRALastName];
        [row setValue:image_cleaningStatus forKey:kCleaningStatus];
        [row setValue:raGuestFirstName forKey:kGuestFirstName];
        [row setValue:raGuestLastName forKey:kGuestLastName];
        [row setValue:raGuestArrivalTime forKey:kRaGuestArrivalTime];
        [row setValue:raGuestDepartureTime forKey:kRaGuestDepartureTime];
        [row setValue:[NSString stringWithFormat:@"%i",(int)raKindOfRoom] forKey:kRaKindOfRoom];
        [row setValue:[NSString stringWithFormat:@"%i",(int)raIsMockRoom] forKey:kRaIsMockRoom];
        [row setValue:image_roomStatus forKey:kRoomStatusImage];
        [row setValue:roomAssignmentTime forKey:kRoomAssignmentTime];
        [row setValue:roomVipStatus forKey:kVIP];
        [row setValue:guestName forKey:kGuestName];
        [row setValue:durationUsed forKey:kDurationUsed];
        [row setValue:roomExpectedCleanTime forKey:kExpected_Clean_time];
        [row setValue:[NSString stringWithFormat:@"%d",roomInspectedStatusId] forKey:KInspection_Status_id];
        [row setValue:[NSString stringWithFormat:@"%i",(int)raIsReassignedRoom] forKey:kRaIsReassignedRoom];
        [row setValue:roomTypeValue forKey:kRoomType];
        
        [result addObject:row];
        status = sqlite3_step(sqlStament);
        // stepLoop++;
        
        //[strHourOfDurationTime release];
        
    }
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            [result removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    return result;
    
}

// Get room inspected failed
- (NSMutableArray*)getRoomInspectedFailed
{
    int currentUserID = [[UserManagerV2 sharedUserManager] currentUser].userId;
    
    NSString *currentDate = nil;
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    [f setDateFormat:@"yyyy-MM-dd"];
    currentDate = [NSString stringWithFormat:@"%@ 00:00:00", [f stringFromDate:[NSDate date]]];
    
    NSString *roomTypeName = [NSString stringWithFormat:@"%@", ams_name];
    if(![[LanguageManagerV2 sharedLanguageManager] currentLanguageIsEqualTo:@"%@",ENGLISH_LANGUAGE ]){
        roomTypeName = [NSString stringWithFormat:@"%@", ams_name_lang];
    }
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"%@ %@, %@, %@, %@, %@ ,%@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@  from %@ a left join %@ b on b.%@=a.%@ left join %@ c on a.%@=c.%@  left join %@ d on a.%@=d.%@  left join %@ e on b.%@=e.%@ left join %@ f on f.%@ = b.%@ where ((%@ = %d) AND (%@ = %d) AND (a.%@ == %d OR a.%@ == %d) and a.%@=%d) AND %@ > '%@' AND %@ = 0 order by a.%@ ",
                                  @"select  ",
                                  room_id,
                                  ra_id,
                                  rstat_code,
                                  cstat_image,
                                  room_vip_status,
                                  guest_name,
                                  ra_room_expected_inspection_time,
                                  ra_room_inspection_status_id,
                                  rstat_id,
                                  ra_first_name,
                                  ra_last_name,
                                  cstat_id,
                                  cstat_image,
                                  ra_guest_first_name,
                                  ra_guest_last_name,
                                  ra_guest_arrival_time,
                                  ra_guest_departure_time,
                                  ra_kind_of_room,
                                  ra_is_mock_room,
                                  rstat_image,
                                  ra_assigned_date,
                                  ra_is_reassigned_room,
                                  roomTypeName,
                                  
                                  ROOM_ASSIGNMENT,
                                  ROOM,
                                  room_id,
                                  ra_room_id,
                                  
                                  ROOM_STATUS,
                                  ra_room_status_id,
                                  rstat_id,
                                  
                                  CLEANING_STATUS,
                                  ra_room_cleaning_status_id,
                                  cstat_id,
                                  
                                  GUEST_INFOS,
                                  room_id,
                                  guest_room_id,
                                  
                                  ROOM_TYPE,
                                  ams_id, //this is room type id in table room_type
                                  room_type_id,
                                  
                                  ra_room_inspection_status_id,
                                  ENUM_INSPECTION_COMPLETED_FAIL,
                                  
                                  ra_room_cleaning_status_id,
                                  ENUM_CLEANING_STATUS_COMPLETED,
                                  
                                  ra_room_status_id,
                                  ENUM_ROOM_STATUS_VD,
                                  
                                  ra_room_status_id,
                                  ENUM_ROOM_STATUS_OD,
                                  
                                  ra_user_id,
                                  currentUserID,
                                  ra_assigned_date,
                                  currentDate,
                                  
                                  ra_delete,
                                  
                                  ra_prioprity
                                  ];
    
    NSMutableArray *result=[[NSMutableArray alloc] init] ;
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
        
    }
    
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW)
    {
        //when room status is list below, it means the room assignment was not inspected fail
        NSInteger roomStatusId = sqlite3_column_int(sqlStament, 8);
        switch (roomStatusId) {
            case ENUM_ROOM_STATUS_OC_SO:
            case ENUM_ROOM_STATUS_OC:
            case ENUM_ROOM_STATUS_OPU:
            case ENUM_ROOM_STATUS_VC:
            case ENUM_ROOM_STATUS_VCB:
            case ENUM_ROOM_STATUS_VPU:
                
            case ENUM_ROOM_STATUS_OOO:
            case ENUM_ROOM_STATUS_OOS:
            case ENUM_ROOM_STATUS_OI:
            case ENUM_ROOM_STATUS_VI:
            {
                status = sqlite3_step(sqlStament);
                continue;
            }
                break;
                
            default:
                break;
        }
        
        
        NSString *roomId = nil;
        if (sqlite3_column_text(self.sqlStament, 0)) {
            roomId = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 0)];
        }
        
        NSString *roomAssignmentID=[[NSString alloc] initWithFormat:@"%d",sqlite3_column_int(self.sqlStament,1)] ;
        
        NSString *roomStatusCode=sqlite3_column_text(sqlStament, 2)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
        
        //            NSString *cleaningStatus=sqlite3_column_text(sqlStament, 3)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
        NSString *roomVipStatus=sqlite3_column_text(sqlStament, 4)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
        
        NSString *guestName=sqlite3_column_text(sqlStament, 5)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
        NSString *roomExpectedCleanTime = sqlite3_column_text(sqlStament, 6)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 6)];
        
        int roomInspectedStatusId = sqlite3_column_int(sqlStament, 7);
        
        // get room record for this room :
        RoomRecordModelV2 *roomRecordModel=[[RoomRecordModelV2 alloc] init];
        roomRecordModel.rrec_room_assignment_id=sqlite3_column_int(self.sqlStament,1);
        roomRecordModel.rrec_User_Id=[[UserManagerV2 sharedUserManager] currentUser].userId;
        
        [[RoomManagerV2 sharedRoomManager] loadRoomRecordDataByRoomIdAndUserId:roomRecordModel];
        
        //            // get time clean
        //            int SecondOfDurationTime= round([DateTimeUtility HoursByTime:roomRecordModel.rrec_Cleaning_Start_Time :roomRecordModel.rrec_Cleaning_End_Time]);
        //            // Get room remark to get clean pause time
        //            NSMutableArray* allRoomRemark =[[RoomManagerV2  sharedRoomManager] loadAllRoomRemarkByRecordId:roomRecordModel.rrec_Id];
        //            int pauseTime = 0;
        //            if (allRoomRemark && [allRoomRemark count]>0) {
        //                for (RoomRemarkModelV2* aRoomRemark in allRoomRemark) {
        //                    if ([aRoomRemark.rm_Time intValue]) {
        //                        pauseTime+=[aRoomRemark.rm_Time intValue];
        //                    }
        //                }
        //            }
        //
        //            SecondOfDurationTime -= pauseTime;
        //            NSString* durationUsed = [NSString stringWithFormat:@"%d",SecondOfDurationTime];
        
        //Hao Tran - changed
        //NSString* durationUsed = roomRecordModel.rrec_Cleaning_Duration;
        NSString* durationUsed = [NSString stringWithFormat:@"%d", roomRecordModel.rrec_Total_Cleaning_Time];
        
        NSString *raFirstName = @"";
        if (sqlite3_column_text(sqlStament, 9)) {
            raFirstName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 9)];
        }
        
        NSString *raLastName = @"";
        if (sqlite3_column_text(sqlStament, 10)) {
            raLastName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 10)];
        }
        
        NSInteger cleaningStatusId = sqlite3_column_int(sqlStament, 11);
        NSString *cleaningStatusIdString =@"";
        if(cleaningStatusId){
            cleaningStatusIdString = [NSString stringWithFormat:@"%d", (int)cleaningStatusId];
        }
        
        NSData *image_cleaningStatus = nil;
        if (sqlite3_column_bytes(sqlStament, 12) > 0) {
            image_cleaningStatus = [NSData dataWithBytes:sqlite3_column_blob(sqlStament, 12) length:sqlite3_column_bytes(sqlStament, 12)];
        }
        
        NSString *raGuestFirstName = @"";
        if (sqlite3_column_text(sqlStament, 13)) {
            raGuestFirstName = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 13)];
        }
        
        NSString *raGuestLastName = @"";
        if (sqlite3_column_text(sqlStament, 14)) {
            raGuestLastName = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 14)];
        }
        
        NSString *raGuestArrivalTime = @"";
        if (sqlite3_column_text(sqlStament, 15)) {
            raGuestArrivalTime = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 15)];
        }
        
        if([LogFileManager isLogConsole])
        {
            NSLog(@"%@",raGuestArrivalTime);
        }
        
        NSString *raGuestDepartureTime = @"";
        if (sqlite3_column_text(sqlStament, 16)) {
            raGuestDepartureTime = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 16)];
        }
        
        if([LogFileManager isLogConsole])
        {
            NSLog(@"%@",raGuestDepartureTime);
        }
        
        NSInteger raKindOfRoom = sqlite3_column_int(sqlStament, 17);
        NSInteger raIsMockRoom = sqlite3_column_int(sqlStament, 18);
        
        NSData *image_roomStatus = nil;
        if (sqlite3_column_bytes(sqlStament, 19) > 0) {
            image_roomStatus = [NSData dataWithBytes:sqlite3_column_blob(sqlStament, 19) length:sqlite3_column_bytes(sqlStament, 19)];
        }
        
        NSString *roomAssignmentTime = nil;
        if (sqlite3_column_bytes(sqlStament, 20)) {
            roomAssignmentTime = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 20)];
        }
        
        NSInteger raIsReassignedRoom = sqlite3_column_int(sqlStament, 21);
        //        NSDictionary *returnRow =[[NSDictionary alloc] initWithObjectsAndKeys:roomId,kRoomNo,roomStatusCode,kRMStatus,roomVipStatus, kVIP, guestName,kGuestName,roomAssignmentID,kRoomAssignmentID, durationUsed,kDurationUsed, roomExpectedCleanTime, kExpected_Clean_time,[NSString stringWithFormat:@"%d",roomInspectedStatusId], KInspection_Status_id,
        //                                  raFirstName, kRAFirstName,
        //                                  raLastName, kRALastName,
        //
        //                                  nil];
        
        NSString *roomTypeValue = @"";
        if (sqlite3_column_bytes(sqlStament, 22) > 0) {
            roomTypeValue = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 22)];
        }
        
        NSMutableDictionary *row = [[NSMutableDictionary alloc] init];
        [row setValue:[NSString stringWithFormat:@"%i",currentUserID] forKey:kRaUserId];
        [row setValue:roomId forKey:kRoomNo];
        [row setValue:roomStatusCode forKey:kRMStatus];
        [row setValue:cleaningStatusIdString forKey:kCleaningStatusID];
        [row setValue:guestName forKey:kGuestName];
        [row setValue:roomAssignmentID forKey:kRoomAssignmentID];
        [row setValue:raFirstName forKey:kRAFirstName];
        [row setValue:raLastName forKey:kRALastName];
        [row setValue:image_cleaningStatus forKey:kCleaningStatus];
        [row setValue:raGuestFirstName forKey:kGuestFirstName];
        [row setValue:raGuestLastName forKey:kGuestLastName];
        [row setValue:raGuestArrivalTime forKey:kRaGuestArrivalTime];
        [row setValue:raGuestDepartureTime forKey:kRaGuestDepartureTime];
        [row setValue:[NSString stringWithFormat:@"%i",(int)raKindOfRoom] forKey:kRaKindOfRoom];
        [row setValue:[NSString stringWithFormat:@"%i",(int)raIsMockRoom] forKey:kRaIsMockRoom];
        [row setValue:image_roomStatus forKey:kRoomStatusImage];
        [row setValue:roomAssignmentTime forKey:kRoomAssignmentTime];
        [row setValue:roomVipStatus forKey:kVIP];
        [row setValue:guestName forKey:kGuestName];
        [row setValue:durationUsed forKey:kDurationUsed];
        [row setValue:roomExpectedCleanTime forKey:kExpected_Clean_time];
        [row setValue:[NSString stringWithFormat:@"%d",roomInspectedStatusId] forKey:KInspection_Status_id];
        [row setValue:[NSString stringWithFormat:@"%i",(int)raIsReassignedRoom] forKey:kRaIsReassignedRoom];
        [row setValue:roomTypeValue forKey:kRoomType];
        
        [result addObject:row];
        
        status = sqlite3_step(sqlStament);
        // stepLoop++;
        
        //[strHourOfDurationTime release];
        
    }
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            [result removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    return result;
    
}

-(NSMutableArray*)getRoomTypesCurrentUser
{
    
    int currentUserID =[[UserManagerV2 sharedUserManager] currentUser].userId;
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"%@ DISTINCT %@  from %@ a left join %@ b on b.%@=a.%@ left join %@ c on a.%@=c.%@  left join %@ d on a.%@=d.%@  left join %@ e on b.%@=e.%@ where a.%@=%d order by a.%@, a.%@ ",
                                  @"select  ",
                                  room_type_id,
                                  
                                  ROOM_ASSIGNMENT,
                                  ROOM,
                                  room_id,
                                  ra_room_id,
                                  
                                  ROOM_STATUS,
                                  ra_room_status_id,
                                  rstat_id,
                                  
                                  CLEANING_STATUS,
                                  ra_room_cleaning_status_id,
                                  cstat_id,
                                  
                                  GUEST_INFOS,
                                  room_id,
                                  guest_room_id,
                                  
                                  ra_user_id,
                                  currentUserID,
                                  
                                  ra_priority_sort_order,
                                  ra_prioprity
                                  ];
    NSMutableArray *result=[NSMutableArray array] ;
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    
    //sqlite3_bind_int(self.sqlStament, 1, roomAssignment.roomAssignment_Id);
    //  NSInteger stepLoop=0;
    NSInteger status = sqlite3_step(sqlStament);
    while(sqlite3_step(sqlStament) == SQLITE_ROW)
    {
        
        NSString *rs_Room_Type_Id=[[NSString alloc] initWithFormat:@"%d",sqlite3_column_int(self.sqlStament,0)] ;
        //            NSLog(@"room typed id -----   %@",rs_Room_Type_Id);
        //@ add roomtype to Array
        NSDictionary *returnRow =[[NSDictionary alloc] initWithObjectsAndKeys: rs_Room_Type_Id, kRoom_Type_Id, nil];
        [result addObject:returnRow];
        status = sqlite3_step(sqlStament);
        // stepLoop++;
        
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            [result removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    
    //     sqlite3_reset(self.sqlStament);
    //     sqlite3_finalize(self.sqlStament);
    return result;
    
}

-(NSMutableArray *) getAllRoomInspectionWithSupervisorId:(NSInteger) userId {
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"%@  %@, %@, %@, %@ ,%@, %@, %@, %@, %@, %@, %@  from %@ a left join %@ b on b.%@=a.%@ left join %@ c on a.%@=c.%@  left join %@ d on a.%@=d.%@ left join %@ e on e.%@=a.%@ where a.%@=%d AND %@ = 0 order by e.%@",
                                  @"select  ",
                                  room_id,
                                  ra_id,
                                  rstat_code,
                                  cstat_image,
                                  room_vip_status,
                                  room_guest_name,
                                  ra_priority_sort_order,
                                  ra_room_inspection_status_id,
                                  cstat_id,
                                  ra_first_name,
                                  ra_last_name,
                                  
                                  ROOM_ASSIGNMENT,
                                  ROOM,
                                  room_id,
                                  ra_room_id,
                                  
                                  ROOM_STATUS,
                                  ra_room_status_id,
                                  rstat_id,
                                  
                                  CLEANING_STATUS,
                                  ra_room_cleaning_status_id,
                                  cstat_id,
                                  
                                  ROOM_RECORDS,
                                  rrec_roomAssignment_id,
                                  ra_id,
                                  
                                  ra_user_id,
                                  (int)userId,
                                  
                                  ra_delete,
                                  
                                  rrec_cleaning_date
                                  ];
    NSMutableArray *result = [NSMutableArray array];
    const char *sql = [sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW)
    {
        NSString *roomStatusCode=sqlite3_column_text(sqlStament, 2) == nil ? @"" : [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
        
        //            if ([UserManagerV2 isSupervisor]) {
        //                if ([roomStatusCode isEqualToString:ROOM_STATUS_VD] || [roomStatusCode isEqualToString:ROOM_STATUS_OD] || [roomStatusCode isEqualToString:ROOM_STATUS_OCI]||[roomStatusCode isEqualToString:ROOM_STATUS_VCI] ||
        //                    [roomStatusCode isEqualToString:ROOM_STATUS_OOO] || [roomStatusCode isEqualToString:ROOM_STATUS_OOS]) continue;
        //            }
        
        NSString *roomId = nil;
        if (sqlite3_column_text(self.sqlStament, 0)) {
            roomId = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 0)];
        }
        NSString *roomAssignmentID=[[NSString alloc] initWithFormat:@"%d",sqlite3_column_int(self.sqlStament,1)] ;
        
        NSData *image_cleaningStatus = nil;
        if (sqlite3_column_bytes(sqlStament, 3) > 0) {
            image_cleaningStatus = [NSData dataWithBytes:sqlite3_column_blob(sqlStament, 3) length:sqlite3_column_bytes(sqlStament, 3)];
        }
        
        NSString *roomVipStatus=sqlite3_column_text(sqlStament, 4)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
        
        NSString *guestName=sqlite3_column_text(sqlStament, 5)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
        
        NSString *raPrioritySortOrder = [NSString stringWithFormat:@"%d", sqlite3_column_int(self.sqlStament, 6)];
        
        NSString *inspectionStatusId = [NSString stringWithFormat:@"%d", sqlite3_column_int(sqlStament, 7)];
        
        NSString *cleaningStatusId = [NSString stringWithFormat:@"%d", sqlite3_column_int(sqlStament, 8)];
        
        NSString *raFirstName = @"";
        if (sqlite3_column_text(sqlStament, 9)) {
            raFirstName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 9)];
        }
        
        NSString *raLastName = @"";
        if (sqlite3_column_text(sqlStament, 10)) {
            raLastName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 10)];
        }
        
        
        NSDictionary *returnRow =[[NSDictionary alloc] initWithObjectsAndKeys:
                                  roomId, kRoomNo,
                                  roomStatusCode, kRMStatus,
                                  roomVipStatus, kVIP,
                                  image_cleaningStatus, kCleaningStatus,
                                  guestName, kGuestName,
                                  roomAssignmentID, kRoomAssignmentID,
                                  raPrioritySortOrder, kRaPrioritySortOrder,
                                  cleaningStatusId, kCleaningStatusID,
                                  inspectionStatusId, KInspection_Status_id,
                                  raFirstName, kRAFirstName,
                                  raLastName, kRALastName,
                                  nil];
        [result addObject:returnRow];
        status = sqlite3_step(sqlStament);
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            [result removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    return result;
}
-(int)updateIsCheckedRa: (NSInteger)userID houseKeeperId:(NSString*)houseKeeperId{
    NSString *sqlString = [[NSString alloc] initWithFormat:@"UPDATE %@ SET %@ = ? WHERE %@ = ? and %@ = ?",
                           ROOM_ASSIGNMENT,
                           ra_is_checked_ra_id,
                           ra_user_id,
                           ra_housekeeper_id];
    const char *sql = [sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    sqlite3_bind_int(self.sqlStament, 1, 1);
    sqlite3_bind_int(self.sqlStament, 2, (int)userID);
    sqlite3_bind_text(self.sqlStament,3, [houseKeeperId UTF8String], -1, SQLITE_TRANSIENT);
    NSInteger returnInt = 0;
    NSInteger status = sqlite3_step(self.sqlStament);
    if(SQLITE_DONE != status) {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return (int)returnInt;
    }
    else
    {
        returnInt = 1;
        return (int)returnInt;
    }
}
-(int)updateIsCheckedFind: (NSInteger)raID {
    NSString *sqlString = [[NSString alloc] initWithFormat:@"UPDATE %@ SET %@ = ? WHERE %@ = ?",
                           ROOM_ASSIGNMENT,
                           ra_is_checked_ra_id,
                           ra_id];
    const char *sql = [sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    sqlite3_bind_int(self.sqlStament, 1, 1);
    sqlite3_bind_int(self.sqlStament, 2, (int)raID);
    NSInteger returnInt = 0;
    NSInteger status = sqlite3_step(self.sqlStament);
    if(SQLITE_DONE != status) {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return (int)returnInt;
    }
    else
    {
        returnInt = 1;
        return (int)returnInt;
    }
}

-(NSMutableArray *) getAllRoomByFloorId:(NSInteger)floorId AndUserId:(NSInteger)userId AndRoomStatus:(NSInteger)roomStatusId
{
    NSString *roomStatus = nil;
    if([[LanguageManagerV2 sharedLanguageManager] currentLanguageIsEqualTo:@"%@",ENGLISH_LANGUAGE]){
        roomStatus = [NSString stringWithFormat:@"%@",rstat_name];
    } else {
        roomStatus = [NSString stringWithFormat:@"%@",rstat_lang];
    }
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:
                                  @"select %@, %@, %@, %@ , %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ from %@ a join %@ b on a.%@ = b.%@ join %@ c on a.%@ = c.%@ join %@ d on a.%@ = d.%@ where b.%@ = %d and a.%@ = %d and a.%@ = 0 and a.%@ = %d order by a.%@ asc",
                                  room_id,
                                  ra_id,
                                  //uncoment this for get room status code
                                  //rstat_code,
                                  roomStatus,
                                  cstat_image,
                                  room_vip_status,
                                  room_guest_name,
                                  ra_priority_sort_order,
                                  ra_room_inspection_status_id,
                                  cstat_id,
                                  ra_first_name,
                                  ra_last_name,
                                  ra_guest_first_name,
                                  ra_guest_last_name,
                                  ra_guest_arrival_time,
                                  ra_guest_departure_time,
                                  ra_kind_of_room,
                                  ra_is_mock_room,
                                  rstat_image,
                                  ra_assigned_date,
                                  ra_is_reassigned_room,
                                  ra_housekeeper_id,
                                  ra_is_checked_ra_id,
                                  
                                  ROOM_ASSIGNMENT,
                                  ROOM,
                                  
                                  ra_room_id,
                                  room_id,
                                  
                                  ROOM_STATUS,
                                  ra_room_status_id,
                                  rstat_id,
                                  
                                  CLEANING_STATUS,
                                  ra_room_cleaning_status_id,
                                  cstat_id,
                                  
                                  room_floor_id,
                                  (int)floorId,
                                  
                                  ra_user_id,
                                  (int)userId,
                                  
                                  ra_delete,
                                  
                                  ra_room_status_id,
                                  (int)roomStatusId,
                                  
                                  ra_room_id
                                  ];
    
    NSMutableArray *result = [NSMutableArray array];
    const char *sql = [sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW)
    {
        NSString *roomStatusCode=sqlite3_column_text(sqlStament, 2) == nil ? @"" : [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
        NSString *roomId = nil;
        if (sqlite3_column_text(self.sqlStament, 0)) {
            roomId = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 0)];
        }
        
        NSString *roomAssignmentID=[[NSString alloc] initWithFormat:@"%d",sqlite3_column_int(self.sqlStament,1)] ;
        
        NSData *image_cleaningStatus = nil;
        if (sqlite3_column_bytes(sqlStament, 3) > 0) {
            image_cleaningStatus = [NSData dataWithBytes:sqlite3_column_blob(sqlStament, 3) length:sqlite3_column_bytes(sqlStament, 3)];
        }
        
        NSString *roomVipStatus=sqlite3_column_text(sqlStament, 4)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
        
        NSString *guestName=sqlite3_column_text(sqlStament, 5)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
        
        NSString *raPrioritySortOrder = [NSString stringWithFormat:@"%d", sqlite3_column_int(self.sqlStament, 6)];
        
        //NSString *inspectionStatusId = [NSString stringWithFormat:@"%d", sqlite3_column_int(sqlStament, 7)];
        
        NSString *cleaningStatusId = [NSString stringWithFormat:@"%d", sqlite3_column_int(sqlStament, 8)];
        
        NSString *raFirstName = @"";
        if (sqlite3_column_text(sqlStament, 9)) {
            raFirstName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 9)];
        }
        
        NSString *raLastName = @"";
        if (sqlite3_column_text(sqlStament, 10)) {
            raLastName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 10)];
        }
        
        NSString *raGuestFirstName = @"";
        if (sqlite3_column_text(sqlStament, 11)) {
            raGuestFirstName = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 11)];
        }
        
        NSString *raGuestLastName = @"";
        if (sqlite3_column_text(sqlStament, 12)) {
            raGuestLastName = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 12)];
        }
        
        NSString *raGuestArrivalTime = @"";
        if (sqlite3_column_text(sqlStament, 13)) {
            raGuestArrivalTime = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 13)];
        }
        
        NSString *raGuestDepartureTime = @"";
        if (sqlite3_column_text(sqlStament, 14)) {
            raGuestDepartureTime = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 14)];
        }
        
        NSInteger raKindOfRoom = sqlite3_column_int(sqlStament, 15);
        NSInteger raIsMockRoom = sqlite3_column_int(sqlStament, 16);
        
        NSData *image_roomStatus = nil;
        if (sqlite3_column_bytes(sqlStament, 17) > 0) {
            image_roomStatus = [[NSData alloc] initWithBytes:sqlite3_column_blob(sqlStament, 17) length:sqlite3_column_bytes(sqlStament, 17)];
        }
        
        NSString *roomAssignmentTime = nil;
        if (sqlite3_column_bytes(sqlStament, 18)) {
            roomAssignmentTime = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 18)];
        }
        
        NSInteger raIsReassignedRoom = sqlite3_column_int(sqlStament, 19);
        NSString *houseKeeperID = [[NSString alloc] initWithFormat:@"%d",sqlite3_column_int(self.sqlStament,20)] ;
        NSString *flagChecked = [[NSString alloc] initWithFormat:@"%d",sqlite3_column_int(self.sqlStament,21)] ;
        
        NSMutableDictionary *row = [[NSMutableDictionary alloc] init];
        [row setValue:[NSString stringWithFormat:@"%i",(int)userId] forKey:kRaUserId];
        [row setValue:roomId forKey:kRoomNo];
        [row setValue:roomStatusCode forKey:kRMStatus];
        [row setValue:cleaningStatusId forKey:kCleaningStatusID];
        [row setValue:guestName forKey:kGuestName];
        [row setValue:roomAssignmentID forKey:kRoomAssignmentID];
        [row setValue:raPrioritySortOrder forKey:kRaPrioritySortOrder];
        [row setValue:raFirstName forKey:kRAFirstName];
        [row setValue:raLastName forKey:kRALastName];
        [row setValue:image_cleaningStatus forKey:kCleaningStatus];
        [row setValue:raGuestFirstName forKey:kGuestFirstName];
        [row setValue:raGuestLastName forKey:kGuestLastName];
        [row setValue:raGuestArrivalTime forKey:kRaGuestArrivalTime];
        [row setValue:raGuestDepartureTime forKey:kRaGuestDepartureTime];
        [row setValue:[[NSString alloc] initWithFormat:@"%i",(int)raKindOfRoom] forKey:kRaKindOfRoom];
        [row setValue:[[NSString alloc] initWithFormat:@"%i",(int)raIsMockRoom] forKey:kRaIsMockRoom];
        [row setValue:image_roomStatus forKey:kRoomStatusImage];
        [row setValue:roomAssignmentTime forKey:kRoomAssignmentTime];
        [row setValue:[[NSString alloc] initWithFormat:@"%i",(int)raIsReassignedRoom] forKey:kRaIsReassignedRoom];
        [row setValue:roomVipStatus forKey:kVIP];
        [row setValue:[[NSString alloc] initWithFormat:@"%i",(int)roomStatusId] forKey:kRoomStatusId];
        [row setValue:houseKeeperID forKey:kraHousekeeperID];
        [row setValue:flagChecked forKey:kraCheckedRa];
        
        [result addObject:row];
        status = sqlite3_step(sqlStament);
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            [result removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    return result;
}

-(NSMutableArray *) getAllRoomByFloorId:(NSInteger)floorId userId:(NSInteger)userId filterType:(NSInteger)filterType statusId:(NSInteger)statusId
{
    NSMutableString *sqlString = nil;
    
    NSString *roomStatus = nil;
    NSString *roomTypeName = nil;
    if([[LanguageManagerV2 sharedLanguageManager] currentLanguageIsEqualTo:@"%@",ENGLISH_LANGUAGE]){
        roomStatus = [NSString stringWithFormat:@"%@",rstat_name];
        roomTypeName = [NSString stringWithFormat:@"%@",ams_name];
    } else {
        roomStatus = [NSString stringWithFormat:@"%@",rstat_lang];
        roomTypeName = [NSString stringWithFormat:@"%@",ams_name_lang];
    }
    
    if(filterType == roomOccupiedType) {
        if(statusId == ENUM_ROOM_TYPE_OCCUPIED_OD_DO) {
            
            NSString *currentDate = nil;
            NSDateFormatter *f = [[NSDateFormatter alloc] init];
            [f setDateFormat:@"yyyy-MM-dd"];
            currentDate = [NSString stringWithFormat:@"%@ 00:00:00", [f stringFromDate:[NSDate date]]];
            
            sqlString = [[NSMutableString alloc] initWithFormat:
                         @"select %@, %@, %@, %@ , %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ from %@ a join %@ b on a.%@ = b.%@ join %@ c on a.%@ = c.%@ join %@ d on a.%@ = d.%@ left join %@ f on f.%@ = b.%@ where b.%@ = %d and a.%@ = %d and a.%@ = 0 and a.%@ = %d and a.%@ >= '%@' order by a.%@ asc",
                         room_id,
                         ra_id,
                         //uncoment this for get room status code
                         //rstat_code,
                         roomStatus,
                         cstat_image,
                         room_vip_status,
                         room_guest_name,
                         ra_priority_sort_order,
                         ra_room_inspection_status_id,
                         cstat_id,
                         ra_first_name,
                         ra_last_name,
                         ra_guest_first_name,
                         ra_guest_last_name,
                         ra_guest_arrival_time,
                         ra_guest_departure_time,
                         ra_kind_of_room,
                         ra_is_mock_room,
                         rstat_image,
                         ra_assigned_date,
                         ra_is_reassigned_room,
                         ra_room_status_id,
                         roomTypeName,
                         
                         ROOM_ASSIGNMENT,
                         ROOM,
                         
                         ra_room_id,
                         room_id,
                         
                         ROOM_STATUS,
                         ra_room_status_id,
                         rstat_id,
                         
                         CLEANING_STATUS,
                         ra_room_cleaning_status_id,
                         cstat_id,
                         
                         ROOM_TYPE,
                         ams_id,
                         room_type_id,
                         
                         room_floor_id,
                         (int)floorId,
                         
                         ra_user_id,
                         (int)userId,
                         
                         ra_delete,
                         
                         ra_room_status_id,
                         ENUM_ROOM_STATUS_OD,
                         
                         ra_guest_departure_time,
                         currentDate,
                         
                         ra_room_id
                         ];
        } else if (statusId == ENUM_ROOM_TYPE_ROOM_OCCUPIED_DUEOUT){
            
            NSString *currentDate = nil;
            NSDateFormatter *f = [[NSDateFormatter alloc] init];
            [f setDateFormat:@"yyyy-MM-dd"];
            currentDate = [NSString stringWithFormat:@"%@ 00:00:00", [f stringFromDate:[NSDate date]]];
            sqlString = [[NSMutableString alloc] initWithFormat:
                         @"select %@, %@, %@, %@ , %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ from %@ a join %@ b on a.%@ = b.%@ join %@ c on a.%@ = c.%@ join %@ d on a.%@ = d.%@ left join %@ f on f.%@ = b.%@ where b.%@ = %d and a.%@ = %d and a.%@ = 0 and a.%@ IN (%d,%d,%d,%d,%d) and a.%@ >= '%@' order by a.%@ asc",
                         room_id,
                         ra_id,
                         //uncomment this for get room status code
                         //rstat_code,
                         roomStatus,
                         cstat_image,
                         room_vip_status,
                         room_guest_name,
                         ra_priority_sort_order,
                         ra_room_inspection_status_id,
                         cstat_id,
                         ra_first_name,
                         ra_last_name,
                         ra_guest_first_name,
                         ra_guest_last_name,
                         ra_guest_arrival_time,
                         ra_guest_departure_time,
                         ra_kind_of_room,
                         ra_is_mock_room,
                         rstat_image,
                         ra_assigned_date,
                         ra_is_reassigned_room,
                         ra_room_status_id,
                         roomTypeName,
                         
                         ROOM_ASSIGNMENT,
                         ROOM,
                         
                         ra_room_id,
                         room_id,
                         
                         ROOM_STATUS,
                         ra_room_status_id,
                         rstat_id,
                         
                         CLEANING_STATUS,
                         ra_room_cleaning_status_id,
                         cstat_id,
                         
                         ROOM_TYPE,
                         ams_id,
                         room_type_id,
                         
                         room_floor_id,
                         (int)floorId,
                         
                         ra_user_id,
                         (int)userId,
                         
                         ra_delete,
                         
                         ra_room_status_id,
                         ENUM_ROOM_STATUS_OC,
                         ENUM_ROOM_STATUS_OD,
                         ENUM_ROOM_STATUS_OI,
                         ENUM_ROOM_STATUS_OPU,
                         ENUM_ROOM_STATUS_OC_SO,
                         
                         ra_guest_departure_time,
                         currentDate,
                         
                         ra_room_id
                         ];
        }
    }
    
    NSMutableArray *result = [NSMutableArray array];
    const char *sql = [sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW)
    {
        NSString *roomStatusCode=sqlite3_column_text(sqlStament, 2) == nil ? @"" : [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
        
        //            if ([UserManagerV2 isSupervisor]) {
        //                if ([roomStatusCode isEqualToString:ROOM_STATUS_VD] || [roomStatusCode isEqualToString:ROOM_STATUS_OD] || [roomStatusCode isEqualToString:ROOM_STATUS_OCI]||[roomStatusCode isEqualToString:ROOM_STATUS_VCI] ||
        //                    [roomStatusCode isEqualToString:ROOM_STATUS_OOO] || [roomStatusCode isEqualToString:ROOM_STATUS_OOS]) continue;
        //            }
        
        NSString *roomId = nil;
        if (sqlite3_column_text(self.sqlStament, 0)) {
            roomId = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 0)];
        }
        
        NSString *roomAssignmentID=[[NSString alloc] initWithFormat:@"%d",sqlite3_column_int(self.sqlStament,1)] ;
        
        NSData *image_cleaningStatus = nil;
        if (sqlite3_column_bytes(sqlStament, 3) > 0) {
            image_cleaningStatus = [NSData dataWithBytes:sqlite3_column_blob(sqlStament, 3) length:sqlite3_column_bytes(sqlStament, 3)];
        }
        
        NSString *roomVipStatus=sqlite3_column_text(sqlStament, 4)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
        
        NSString *guestName=sqlite3_column_text(sqlStament, 5)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
        
        NSString *raPrioritySortOrder = [NSString stringWithFormat:@"%d", sqlite3_column_int(self.sqlStament, 6)];
        
        //NSString *inspectionStatusId = [NSString stringWithFormat:@"%d", sqlite3_column_int(sqlStament, 7)];
        
        NSString *cleaningStatusId = [NSString stringWithFormat:@"%d", sqlite3_column_int(sqlStament, 8)];
        
        NSString *raFirstName = @"";
        if (sqlite3_column_text(sqlStament, 9)) {
            raFirstName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 9)];
        }
        
        NSString *raLastName = @"";
        if (sqlite3_column_text(sqlStament, 10)) {
            raLastName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 10)];
        }
        
        NSString *raGuestFirstName = @"";
        if (sqlite3_column_text(sqlStament, 11)) {
            raGuestFirstName = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 11)];
        }
        
        NSString *raGuestLastName = @"";
        if (sqlite3_column_text(sqlStament, 12)) {
            raGuestLastName = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 12)];
        }
        
        NSString *raGuestArrivalTime = @"";
        if (sqlite3_column_text(sqlStament, 13)) {
            raGuestArrivalTime = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 13)];
        }
        
        NSString *raGuestDepartureTime = @"";
        if (sqlite3_column_text(sqlStament, 14)) {
            raGuestDepartureTime = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 14)];
        }
        
        NSInteger raKindOfRoom = sqlite3_column_int(sqlStament, 15);
        NSInteger raIsMockRoom = sqlite3_column_int(sqlStament, 16);
        
        NSData *image_roomStatus = nil;
        if (sqlite3_column_bytes(sqlStament, 17) > 0) {
            image_roomStatus = [[NSData alloc] initWithBytes:sqlite3_column_blob(sqlStament, 17) length:sqlite3_column_bytes(sqlStament, 17)];
        }
        
        NSString *roomAssignmentTime = nil;
        if (sqlite3_column_bytes(sqlStament, 18)) {
            roomAssignmentTime = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 18)];
        }
        
        NSInteger raIsReassignedRoom = sqlite3_column_int(sqlStament, 19);
        NSInteger roomStatusId = sqlite3_column_int(sqlStament, 20);
        
        NSString *roomTypeValue = nil;
        if (sqlite3_column_bytes(sqlStament, 21)) {
            roomTypeValue = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 21)];
        }
        
        NSMutableDictionary *row = [[NSMutableDictionary alloc] init];
        [row setValue:[NSString stringWithFormat:@"%i",(int)userId] forKey:kRaUserId];
        [row setValue:roomId forKey:kRoomNo];
        [row setValue:roomStatusCode forKey:kRMStatus];
        [row setValue:cleaningStatusId forKey:kCleaningStatusID];
        [row setValue:guestName forKey:kGuestName];
        [row setValue:roomAssignmentID forKey:kRoomAssignmentID];
        [row setValue:raPrioritySortOrder forKey:kRaPrioritySortOrder];
        [row setValue:raFirstName forKey:kRAFirstName];
        [row setValue:raLastName forKey:kRALastName];
        [row setValue:image_cleaningStatus forKey:kCleaningStatus];
        [row setValue:raGuestFirstName forKey:kGuestFirstName];
        [row setValue:raGuestLastName forKey:kGuestLastName];
        [row setValue:raGuestArrivalTime forKey:kRaGuestArrivalTime];
        [row setValue:raGuestDepartureTime forKey:kRaGuestDepartureTime];
        [row setValue:[[NSString alloc] initWithFormat:@"%i",(int)raKindOfRoom] forKey:kRaKindOfRoom];
        [row setValue:[[NSString alloc] initWithFormat:@"%i",(int)raIsMockRoom] forKey:kRaIsMockRoom];
        [row setValue:image_roomStatus forKey:kRoomStatusImage];
        [row setValue:roomAssignmentTime forKey:kRoomAssignmentTime];
        [row setValue:[[NSString alloc] initWithFormat:@"%i",(int)raIsReassignedRoom] forKey:kRaIsReassignedRoom];
        [row setValue:roomVipStatus forKey:kVIP];
        [row setValue:[[NSString alloc] initWithFormat:@"%i",(int)roomStatusId] forKey:kRoomStatusId];
        [row setValue:roomTypeValue forKey:kRoomType];
        
        [result addObject:row];
        status = sqlite3_step(sqlStament);
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            [result removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    return result;
}
-(NSMutableArray *) getAllRoomByFloorId:(NSInteger)floorId userId:(NSInteger)userId filterType:(NSInteger)filterType generalFilter:(NSString*)generalFilter
{
    NSMutableString *sqlString = nil;
    
    NSString *roomStatus = nil;
    NSString *roomTypeName = nil;
    if([[LanguageManagerV2 sharedLanguageManager] currentLanguageIsEqualTo:@"%@",ENGLISH_LANGUAGE]){
        roomStatus = [NSString stringWithFormat:@"%@",rstat_name];
        roomTypeName = [NSString stringWithFormat:@"%@",ams_name];
    } else {
        roomStatus = [NSString stringWithFormat:@"%@",rstat_lang];
        roomTypeName = [NSString stringWithFormat:@"%@",ams_name_lang];
    }
    
    NSString *currentDate = nil;
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    [f setDateFormat:@"yyyy-MM-dd"];
    currentDate = [NSString stringWithFormat:@"%@ 00:00:00", [f stringFromDate:[NSDate date]]];
    
    sqlString = [[NSMutableString alloc] initWithFormat:
                 @"select %@, %@, %@, %@ , %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ from %@ a join %@ b on a.%@ = b.%@ join %@ c on a.%@ = c.%@ join %@ d on a.%@ = d.%@ left join %@ f on f.%@ = b.%@ where b.%@ = %d and a.%@ = %d and a.%@ = 0 and a.%@ = '%@' order by a.%@ asc",
                 room_id,
                 ra_id,
                 //uncoment this for get room status code
                 //rstat_code,
                 roomStatus,
                 cstat_image,
                 room_vip_status,
                 room_guest_name,
                 ra_priority_sort_order,
                 ra_room_inspection_status_id,
                 cstat_id,
                 ra_first_name,
                 ra_last_name,
                 ra_guest_first_name,
                 ra_guest_last_name,
                 ra_guest_arrival_time,
                 ra_guest_departure_time,
                 ra_kind_of_room,
                 ra_is_mock_room,
                 rstat_image,
                 ra_assigned_date,
                 ra_is_reassigned_room,
                 ra_room_status_id,
                 ra_last_cleaning_date,
                 roomTypeName,
                 ra_room_cleaning_credit,
                 ROOM_ASSIGNMENT,
                 ROOM,
                 
                 ra_room_id,
                 room_id,
                 
                 ROOM_STATUS,
                 ra_room_status_id,
                 rstat_id,
                 
                 CLEANING_STATUS,
                 ra_room_cleaning_status_id,
                 cstat_id,
                 
                 ROOM_TYPE,
                 ams_id,
                 room_type_id,
                 
                 room_floor_id,
                 (int)floorId,
                 
                 ra_user_id,
                 (int)userId,
                 
                 ra_delete,
                 
                 ra_room_id,
                 generalFilter,
                
                 
                 ra_room_id
                 ];
    

    
    NSMutableArray *result = [NSMutableArray array];
    const char *sql = [sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW)
    {
        NSString *roomStatusCode=sqlite3_column_text(sqlStament, 2) == nil ? @"" : [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
        
        NSString *roomId = nil;
        if (sqlite3_column_text(self.sqlStament, 0)) {
            roomId = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 0)];
        }
        
        NSString *roomAssignmentID=[[NSString alloc] initWithFormat:@"%d",sqlite3_column_int(self.sqlStament,1)] ;
        
        NSData *image_cleaningStatus = nil;
        if (sqlite3_column_bytes(sqlStament, 3) > 0) {
            image_cleaningStatus = [NSData dataWithBytes:sqlite3_column_blob(sqlStament, 3) length:sqlite3_column_bytes(sqlStament, 3)];
        }
        
        NSString *roomVipStatus=sqlite3_column_text(sqlStament, 4)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
        
        NSString *guestName=sqlite3_column_text(sqlStament, 5)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
        
        NSString *raPrioritySortOrder = [NSString stringWithFormat:@"%d", sqlite3_column_int(self.sqlStament, 6)];
        
        //NSString *inspectionStatusId = [NSString stringWithFormat:@"%d", sqlite3_column_int(sqlStament, 7)];
        
        NSString *cleaningStatusId = [NSString stringWithFormat:@"%d", sqlite3_column_int(sqlStament, 8)];
        
        NSString *raFirstName = @"";
        if (sqlite3_column_text(sqlStament, 9)) {
            raFirstName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 9)];
        }
        
        NSString *raLastName = @"";
        if (sqlite3_column_text(sqlStament, 10)) {
            raLastName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 10)];
        }
        
        NSString *raGuestFirstName = @"";
        if (sqlite3_column_text(sqlStament, 11)) {
            raGuestFirstName = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 11)];
        }
        
        NSString *raGuestLastName = @"";
        if (sqlite3_column_text(sqlStament, 12)) {
            raGuestLastName = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 12)];
        }
        
        NSString *raGuestArrivalTime = @"";
        if (sqlite3_column_text(sqlStament, 13)) {
            raGuestArrivalTime = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 13)];
        }
        
        NSString *raGuestDepartureTime = @"";
        if (sqlite3_column_text(sqlStament, 14)) {
            raGuestDepartureTime = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 14)];
        }
        
        NSInteger raKindOfRoom = sqlite3_column_int(sqlStament, 15);
        NSInteger raIsMockRoom = sqlite3_column_int(sqlStament, 16);
        
        NSData *image_roomStatus = nil;
        if (sqlite3_column_bytes(sqlStament, 17) > 0) {
            image_roomStatus = [[NSData alloc] initWithBytes:sqlite3_column_blob(sqlStament, 17) length:sqlite3_column_bytes(sqlStament, 17)];
        }
        
        NSString *roomAssignmentTime = nil;
        if (sqlite3_column_bytes(sqlStament, 18)) {
            roomAssignmentTime = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 18)];
        }
        
        NSInteger raIsReassignedRoom = sqlite3_column_int(sqlStament, 19);
        NSInteger roomStatusId = sqlite3_column_int(sqlStament, 20);
        
        NSString *lastCleaningDate = nil;
        if (sqlite3_column_bytes(sqlStament, 21)) {
            lastCleaningDate = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 21)];
        }
        
        NSString *roomTypeValue = nil;
        if (sqlite3_column_bytes(sqlStament, 22)) {
            roomTypeValue = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 22)];
        }
        NSString *roomCleaningCredit = @"";
        if (sqlite3_column_bytes(sqlStament, 23)) {
            roomCleaningCredit = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 23)];
        }
        
        NSMutableDictionary *row = [[NSMutableDictionary alloc] init];
        [row setValue:[NSString stringWithFormat:@"%i",(int)userId] forKey:kRaUserId];
        [row setValue:roomId forKey:kRoomNo];
        [row setValue:roomStatusCode forKey:kRMStatus];
        [row setValue:cleaningStatusId forKey:kCleaningStatusID];
        [row setValue:guestName forKey:kGuestName];
        [row setValue:roomAssignmentID forKey:kRoomAssignmentID];
        [row setValue:raPrioritySortOrder forKey:kRaPrioritySortOrder];
        [row setValue:raFirstName forKey:kRAFirstName];
        [row setValue:raLastName forKey:kRALastName];
        [row setValue:image_cleaningStatus forKey:kCleaningStatus];
        [row setValue:raGuestFirstName forKey:kGuestFirstName];
        [row setValue:raGuestLastName forKey:kGuestLastName];
        [row setValue:raGuestArrivalTime forKey:kRaGuestArrivalTime];
        [row setValue:raGuestDepartureTime forKey:kRaGuestDepartureTime];
        [row setValue:[[NSString alloc] initWithFormat:@"%i",(int)raKindOfRoom] forKey:kRaKindOfRoom];
        [row setValue:[[NSString alloc] initWithFormat:@"%i",(int)raIsMockRoom] forKey:kRaIsMockRoom];
        [row setValue:image_roomStatus forKey:kRoomStatusImage];
        [row setValue:roomAssignmentTime forKey:kRoomAssignmentTime];
        [row setValue:[[NSString alloc] initWithFormat:@"%i",(int)raIsReassignedRoom] forKey:kRaIsReassignedRoom];
        [row setValue:roomVipStatus forKey:kVIP];
        [row setValue:[[NSString alloc]initWithFormat:@"%i",(int)roomStatusId] forKey:kRoomStatusId];
        [row setValue:lastCleaningDate forKey:kLastCleaningDate];
        [row setValue:roomTypeValue forKey:kRoomType];
        [row setValue:roomCleaningCredit forKey:kRaCleaningCredit];
        [result addObject:row];
        status = sqlite3_step(sqlStament);
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            [result removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    return result;
}

-(NSMutableArray *) getZoneRoomDetailsListByFloorId:(NSInteger)floorId userId:(NSInteger)userId filterType:(NSInteger)filterType generalFilter:(NSString*)generalFilter
{
    NSMutableString *sqlString = nil;
    
    NSString *roomStatus = nil;
    NSString *roomTypeName = nil;
    if([[LanguageManagerV2 sharedLanguageManager] currentLanguageIsEqualTo:@"%@",ENGLISH_LANGUAGE]){
        roomStatus = [NSString stringWithFormat:@"%@",rstat_name];
        roomTypeName = [NSString stringWithFormat:@"%@",ams_name];
    } else {
        roomStatus = [NSString stringWithFormat:@"%@",rstat_lang];
        roomTypeName = [NSString stringWithFormat:@"%@",ams_name_lang];
    }
    int iGneralFilter = [generalFilter intValue];
    NSString *currentDate = nil;
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    [f setDateFormat:@"yyyy-MM-dd"];
    currentDate = [NSString stringWithFormat:@"%@ 00:00:00", [f stringFromDate:[NSDate date]]];
    
    sqlString = [[NSMutableString alloc] initWithFormat:
                 @"select %@, %@, %@, %@ , %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ from %@ a join %@ b on a.%@ = b.%@ join %@ c on a.%@ = c.%@ join %@ d on a.%@ = d.%@ left join %@ f on f.%@ = b.%@ where a.%@ = %d and a.%@ = 0 and a.%@ = %d order by a.%@ asc",
                 room_id,
                 ra_id,
                 //uncoment this for get room status code
                 //rstat_code,
                 roomStatus,
                 cstat_image,
                 room_vip_status,
                 room_guest_name,
                 ra_priority_sort_order,
                 ra_room_inspection_status_id,
                 cstat_id,
                 ra_first_name,
                 ra_last_name,
                 ra_guest_first_name,
                 ra_guest_last_name,
                 ra_guest_arrival_time,
                 ra_guest_departure_time,
                 ra_kind_of_room,
                 ra_is_mock_room,
                 rstat_image,
                 ra_assigned_date,
                 ra_is_reassigned_room,
                 ra_room_status_id,
                 ra_housekeeper_id,
                 ra_last_cleaning_date,
                 roomTypeName,
                 
                 ROOM_ASSIGNMENT,
                 ROOM,
                 
                 ra_room_id,
                 room_id,
                 
                 ROOM_STATUS,
                 ra_room_status_id,
                 rstat_id,
                 
                 CLEANING_STATUS,
                 ra_room_cleaning_status_id,
                 cstat_id,
                 
                 ROOM_TYPE,
                 ams_id,
                 room_type_id,
                 
                 
                 ra_user_id,
                 (int)userId,
                 
                 ra_delete,
                 
                 ra_zone_id,
                 iGneralFilter,
                 ra_room_id
                 ];
    
    
    NSMutableArray *result = [NSMutableArray array];
    const char *sql = [sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW)
    {
        NSString *roomStatusCode=sqlite3_column_text(sqlStament, 2) == nil ? @"" : [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
        
        NSString *roomId = nil;
        if (sqlite3_column_text(self.sqlStament, 0)) {
            roomId = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 0)];
        }
        
        NSString *roomAssignmentID=[[NSString alloc] initWithFormat:@"%d",sqlite3_column_int(self.sqlStament,1)] ;
        
        NSData *image_cleaningStatus = nil;
        if (sqlite3_column_bytes(sqlStament, 3) > 0) {
            image_cleaningStatus = [NSData dataWithBytes:sqlite3_column_blob(sqlStament, 3) length:sqlite3_column_bytes(sqlStament, 3)];
        }
        
        NSString *roomVipStatus=sqlite3_column_text(sqlStament, 4)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
        
        NSString *guestName=sqlite3_column_text(sqlStament, 5)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
        
        NSString *raPrioritySortOrder = [NSString stringWithFormat:@"%d", sqlite3_column_int(self.sqlStament, 6)];
        
        //NSString *inspectionStatusId = [NSString stringWithFormat:@"%d", sqlite3_column_int(sqlStament, 7)];
        
        NSString *cleaningStatusId = [NSString stringWithFormat:@"%d", sqlite3_column_int(sqlStament, 8)];
        
        NSString *raFirstName = @"";
        if (sqlite3_column_text(sqlStament, 9)) {
            raFirstName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 9)];
        }
        
        NSString *raLastName = @"";
        if (sqlite3_column_text(sqlStament, 10)) {
            raLastName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 10)];
        }
        
        NSString *raGuestFirstName = @"";
        if (sqlite3_column_text(sqlStament, 11)) {
            raGuestFirstName = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 11)];
        }
        
        NSString *raGuestLastName = @"";
        if (sqlite3_column_text(sqlStament, 12)) {
            raGuestLastName = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 12)];
        }
        
        NSString *raGuestArrivalTime = @"";
        if (sqlite3_column_text(sqlStament, 13)) {
            raGuestArrivalTime = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 13)];
        }
        
        NSString *raGuestDepartureTime = @"";
        if (sqlite3_column_text(sqlStament, 14)) {
            raGuestDepartureTime = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 14)];
        }
        
        NSInteger raKindOfRoom = sqlite3_column_int(sqlStament, 15);
        NSInteger raIsMockRoom = sqlite3_column_int(sqlStament, 16);
        
        NSData *image_roomStatus = nil;
        if (sqlite3_column_bytes(sqlStament, 17) > 0) {
            image_roomStatus = [[NSData alloc] initWithBytes:sqlite3_column_blob(sqlStament, 17) length:sqlite3_column_bytes(sqlStament, 17)];
        }
        
        NSString *roomAssignmentTime = nil;
        if (sqlite3_column_bytes(sqlStament, 18)) {
            roomAssignmentTime = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 18)];
        }
        
        NSInteger raIsReassignedRoom = sqlite3_column_int(sqlStament, 19);
        NSInteger roomStatusId = sqlite3_column_int(sqlStament, 20);
        NSInteger houseKeeperId = sqlite3_column_int(sqlStament, 21);
        
        NSString *lastCleaningDate = nil;
        if (sqlite3_column_bytes(sqlStament, 22)) {
            lastCleaningDate = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 22)];
        }
        
        NSString *roomTypeValue = nil;
        if (sqlite3_column_bytes(sqlStament, 23)) {
            roomTypeValue = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 23)];
        }
        
        NSMutableDictionary *row = [[NSMutableDictionary alloc] init];
        [row setValue:[NSString stringWithFormat:@"%i",(int)userId] forKey:kRaUserId];
        [row setValue:roomId forKey:kRoomNo];
        [row setValue:roomStatusCode forKey:kRMStatus];
        [row setValue:cleaningStatusId forKey:kCleaningStatusID];
        [row setValue:guestName forKey:kGuestName];
        [row setValue:roomAssignmentID forKey:kRoomAssignmentID];
        [row setValue:raPrioritySortOrder forKey:kRaPrioritySortOrder];
        [row setValue:raFirstName forKey:kRAFirstName];
        [row setValue:raLastName forKey:kRALastName];
        [row setValue:image_cleaningStatus forKey:kCleaningStatus];
        [row setValue:raGuestFirstName forKey:kGuestFirstName];
        [row setValue:raGuestLastName forKey:kGuestLastName];
        [row setValue:raGuestArrivalTime forKey:kRaGuestArrivalTime];
        [row setValue:raGuestDepartureTime forKey:kRaGuestDepartureTime];
        [row setValue:[[NSString alloc] initWithFormat:@"%i",(int)raKindOfRoom] forKey:kRaKindOfRoom];
        [row setValue:[[NSString alloc] initWithFormat:@"%i",(int)raIsMockRoom] forKey:kRaIsMockRoom];
        [row setValue:image_roomStatus forKey:kRoomStatusImage];
        [row setValue:roomAssignmentTime forKey:kRoomAssignmentTime];
        [row setValue:[[NSString alloc] initWithFormat:@"%i",(int)raIsReassignedRoom] forKey:kRaIsReassignedRoom];
        [row setValue:roomVipStatus forKey:kVIP];
        [row setValue:[[NSString alloc] initWithFormat:@"%i",(int)roomStatusId] forKey:kRoomStatusId];
        [row setValue:[[NSString alloc] initWithFormat:@"%i",(int)houseKeeperId] forKey:kraHousekeeperID];
        [row setValue:lastCleaningDate forKey:kLastCleaningDate];
        [row setValue:roomTypeValue forKey:kRoomType];
        
        [result addObject:row];
        status = sqlite3_step(sqlStament);
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            [result removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    return result;
}
//[Huy]
-(NSMutableArray *) getFloorRoomDetailsListByFloorId:(NSString*)floorId userId:(NSInteger)userId filterType:(NSInteger)filterType generalFilter:(NSString*)generalFilter
{
    NSMutableString *sqlString = nil;
    
    NSString *roomStatus = nil;
    NSString *roomTypeName = nil;
    if([[LanguageManagerV2 sharedLanguageManager] currentLanguageIsEqualTo:@"%@",ENGLISH_LANGUAGE]){
        roomStatus = [NSString stringWithFormat:@"%@",rstat_name];
        roomTypeName = [NSString stringWithFormat:@"%@",ams_name];
    } else {
        roomStatus = [NSString stringWithFormat:@"%@",rstat_lang];
        roomTypeName = [NSString stringWithFormat:@"%@",ams_name_lang];
    }
    int iGneralFilter = [generalFilter intValue];
    NSString *currentDate = nil;
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    [f setDateFormat:@"yyyy-MM-dd"];
    currentDate = [NSString stringWithFormat:@"%@ 00:00:00", [f stringFromDate:[NSDate date]]];
    
    sqlString = [[NSMutableString alloc] initWithFormat:
                 @"select %@, %@, %@, %@ , %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ from %@ a join %@ b on a.%@ = b.%@ join %@ c on a.%@ = c.%@ join %@ d on a.%@ = d.%@ left join %@ f on f.%@ = b.%@ where a.%@ = %d and a.%@ = 0 and b.%@ in (%@) order by a.%@ asc",
                 room_id,
                 ra_id,
                 //uncoment this for get room status code
                 //rstat_code,
                 roomStatus,
                 cstat_image,
                 room_vip_status,
                 room_guest_name,
                 ra_priority_sort_order,
                 ra_room_inspection_status_id,
                 cstat_id,
                 ra_first_name,
                 ra_last_name,
                 ra_guest_first_name,
                 ra_guest_last_name,
                 ra_guest_arrival_time,
                 ra_guest_departure_time,
                 ra_kind_of_room,
                 ra_is_mock_room,
                 rstat_image,
                 ra_assigned_date,
                 ra_is_reassigned_room,
                 ra_room_status_id,
                 ra_housekeeper_id,
                 ra_last_cleaning_date,
                 roomTypeName,
                 
                 ROOM_ASSIGNMENT,
                 ROOM,
                 
                 ra_room_id,
                 room_id,
                 
                 ROOM_STATUS,
                 ra_room_status_id,
                 rstat_id,
                 
                 CLEANING_STATUS,
                 ra_room_cleaning_status_id,
                 cstat_id,
                 
                 ROOM_TYPE,
                 ams_id,
                 room_type_id,
                 
                 
                 ra_user_id,
                 (int)userId,
                 
                 ra_delete,
                 
                 room_floor_id,
                 floorId,
                 ra_room_id
                 ];
    
    
    NSMutableArray *result = [NSMutableArray array];
    const char *sql = [sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW)
    {
        NSString *roomStatusCode=sqlite3_column_text(sqlStament, 2) == nil ? @"" : [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
        
        NSString *roomId = nil;
        if (sqlite3_column_text(self.sqlStament, 0)) {
            roomId = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 0)];
        }
        
        NSString *roomAssignmentID=[[NSString alloc] initWithFormat:@"%d",sqlite3_column_int(self.sqlStament,1)] ;
        
        NSData *image_cleaningStatus = nil;
        if (sqlite3_column_bytes(sqlStament, 3) > 0) {
            image_cleaningStatus = [NSData dataWithBytes:sqlite3_column_blob(sqlStament, 3) length:sqlite3_column_bytes(sqlStament, 3)];
        }
        
        NSString *roomVipStatus=sqlite3_column_text(sqlStament, 4)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
        
        NSString *guestName=sqlite3_column_text(sqlStament, 5)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
        
        NSString *raPrioritySortOrder = [NSString stringWithFormat:@"%d", sqlite3_column_int(self.sqlStament, 6)];
        
        //NSString *inspectionStatusId = [NSString stringWithFormat:@"%d", sqlite3_column_int(sqlStament, 7)];
        
        NSString *cleaningStatusId = [NSString stringWithFormat:@"%d", sqlite3_column_int(sqlStament, 8)];
        
        NSString *raFirstName = @"";
        if (sqlite3_column_text(sqlStament, 9)) {
            raFirstName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 9)];
        }
        
        NSString *raLastName = @"";
        if (sqlite3_column_text(sqlStament, 10)) {
            raLastName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 10)];
        }
        
        NSString *raGuestFirstName = @"";
        if (sqlite3_column_text(sqlStament, 11)) {
            raGuestFirstName = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 11)];
        }
        
        NSString *raGuestLastName = @"";
        if (sqlite3_column_text(sqlStament, 12)) {
            raGuestLastName = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 12)];
        }
        
        NSString *raGuestArrivalTime = @"";
        if (sqlite3_column_text(sqlStament, 13)) {
            raGuestArrivalTime = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 13)];
        }
        
        NSString *raGuestDepartureTime = @"";
        if (sqlite3_column_text(sqlStament, 14)) {
            raGuestDepartureTime = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 14)];
        }
        
        NSInteger raKindOfRoom = sqlite3_column_int(sqlStament, 15);
        NSInteger raIsMockRoom = sqlite3_column_int(sqlStament, 16);
        
        NSData *image_roomStatus = nil;
        if (sqlite3_column_bytes(sqlStament, 17) > 0) {
            image_roomStatus = [[NSData alloc] initWithBytes:sqlite3_column_blob(sqlStament, 17) length:sqlite3_column_bytes(sqlStament, 17)];
        }
        
        NSString *roomAssignmentTime = nil;
        if (sqlite3_column_bytes(sqlStament, 18)) {
            roomAssignmentTime = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 18)];
        }
        
        NSInteger raIsReassignedRoom = sqlite3_column_int(sqlStament, 19);
        NSInteger roomStatusId = sqlite3_column_int(sqlStament, 20);
        NSInteger houseKeeperId = sqlite3_column_int(sqlStament, 21);
        
        NSString *lastCleaningDate = nil;
        if (sqlite3_column_bytes(sqlStament, 22)) {
            lastCleaningDate = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 22)];
        }
        
        NSString *roomTypeValue = nil;
        if (sqlite3_column_bytes(sqlStament, 23)) {
            roomTypeValue = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 23)];
        }
        
        NSMutableDictionary *row = [[NSMutableDictionary alloc] init];
        [row setValue:[NSString stringWithFormat:@"%i",(int)userId] forKey:kRaUserId];
        [row setValue:roomId forKey:kRoomNo];
        [row setValue:roomStatusCode forKey:kRMStatus];
        [row setValue:cleaningStatusId forKey:kCleaningStatusID];
        [row setValue:guestName forKey:kGuestName];
        [row setValue:roomAssignmentID forKey:kRoomAssignmentID];
        [row setValue:raPrioritySortOrder forKey:kRaPrioritySortOrder];
        [row setValue:raFirstName forKey:kRAFirstName];
        [row setValue:raLastName forKey:kRALastName];
        [row setValue:image_cleaningStatus forKey:kCleaningStatus];
        [row setValue:raGuestFirstName forKey:kGuestFirstName];
        [row setValue:raGuestLastName forKey:kGuestLastName];
        [row setValue:raGuestArrivalTime forKey:kRaGuestArrivalTime];
        [row setValue:raGuestDepartureTime forKey:kRaGuestDepartureTime];
        [row setValue:[[NSString alloc] initWithFormat:@"%i",(int)raKindOfRoom] forKey:kRaKindOfRoom];
        [row setValue:[[NSString alloc] initWithFormat:@"%i",(int)raIsMockRoom] forKey:kRaIsMockRoom];
        [row setValue:image_roomStatus forKey:kRoomStatusImage];
        [row setValue:roomAssignmentTime forKey:kRoomAssignmentTime];
        [row setValue:[[NSString alloc] initWithFormat:@"%i",(int)raIsReassignedRoom] forKey:kRaIsReassignedRoom];
        [row setValue:roomVipStatus forKey:kVIP];
        [row setValue:[[NSString alloc] initWithFormat:@"%i",(int)roomStatusId] forKey:kRoomStatusId];
        [row setValue:[[NSString alloc] initWithFormat:@"%i",(int)houseKeeperId] forKey:kraHousekeeperID];
        [row setValue:lastCleaningDate forKey:kLastCleaningDate];
        [row setValue:roomTypeValue forKey:kRoomType];
        
        [result addObject:row];
        status = sqlite3_step(sqlStament);
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            [result removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    return result;
}
-(NSMutableArray *) getRoomDetailsListByFloorId:(NSInteger)floorId userId:(NSInteger)userId filterType:(NSInteger)filterType generalFilter:(NSString*)generalFilter
{
    NSMutableString *sqlString = nil;
    
    NSString *roomStatus = nil;
    NSString *roomTypeName = nil;
    if
        ([[LanguageManagerV2 sharedLanguageManager] currentLanguageIsEqualTo:@"%@",ENGLISH_LANGUAGE]){
        roomStatus = [NSString stringWithFormat:@"%@",rstat_name];
        roomTypeName = [NSString stringWithFormat:@"%@",ams_name];
    } else {
        roomStatus = [NSString stringWithFormat:@"%@",rstat_lang];
        roomTypeName = [NSString stringWithFormat:@"%@",ams_name_lang];
    }
    int iGneralFilter = [generalFilter intValue];
    NSString *currentDate = nil;
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    [f setDateFormat:@"yyyy-MM-dd"];
    currentDate = [NSString stringWithFormat:@"%@ 00:00:00", [f stringFromDate:[NSDate date]]];
    
    if (iGneralFilter == ENUM_ROOM_STATUS_OOS) {
        //[Hao Tran / 2015-05-11] - Discussed with FM/Chris, Find OOS will be included blocked room as well. Find OOS result will contain any rooms status with blocked room
        sqlString = [[NSMutableString alloc] initWithFormat:
                     @"select DISTINCT %@, %@, %@, %@ , %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ from %@ a, %@ e join %@ b on a.%@ = b.%@ join %@ c on a.%@ = c.%@ join %@ d on a.%@ = d.%@ left join %@ f on f.%@ = b.%@ where b.%@ = %d and a.%@ = %d and a.%@ = 0 and (a.%@ = %d or e.%@ = a.%@) order by a.%@ asc",
                     room_id,
                     ra_id,
                     //uncoment this for get room status code
                     //rstat_code,
                     roomStatus,
                     cstat_image,
                     room_vip_status,
                     room_guest_name,
                     ra_priority_sort_order,
                     ra_room_inspection_status_id,
                     cstat_id,
                     ra_first_name,
                     ra_last_name,
                     ra_guest_first_name,
                     ra_guest_last_name,
                     ra_guest_arrival_time,
                     ra_guest_departure_time,
                     ra_kind_of_room,
                     ra_is_mock_room,
                     rstat_image,
                     ra_assigned_date,
                     ra_is_reassigned_room,
                     ra_room_status_id,
                     ra_housekeeper_id,
                     ra_last_cleaning_date,
                     roomTypeName,
                     
                     ROOM_ASSIGNMENT,
                     ROOM_BLOCKING,
                     ROOM,
                     
                     ra_room_id,
                     room_id,
                     
                     ROOM_STATUS,
                     ra_room_status_id,
                     rstat_id,
                     
                     CLEANING_STATUS,
                     ra_room_cleaning_status_id,
                     cstat_id,
                     
                     ROOM_TYPE,
                     ams_id,
                     room_type_id,
                     
                     room_floor_id,
                     (int)floorId,
                     
                     ra_user_id,
                     (int)userId,
                     
                     ra_delete,
                     
                     ra_room_status_id,
                     iGneralFilter,
                     
                     roomblocking_room_number,
                     ra_room_id,
                     
                     ra_room_id
                     ];
    } else {
        sqlString = [[NSMutableString alloc] initWithFormat:
                     @"select %@, %@, %@, %@ , %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ from %@ a join %@ b on a.%@ = b.%@ join %@ c on a.%@ = c.%@ join %@ d on a.%@ = d.%@ left join %@ f on f.%@ = b.%@ where b.%@ = %d and a.%@ = %d and a.%@ = 0 and a.%@ = %d order by a.%@ asc",
                     room_id,
                     ra_id,
                     //uncoment this for get room status code
                     //rstat_code,
                     roomStatus,
                     cstat_image,
                     room_vip_status,
                     room_guest_name,
                     ra_priority_sort_order,
                     ra_room_inspection_status_id,
                     cstat_id,
                     ra_first_name,
                     ra_last_name,
                     ra_guest_first_name,
                     ra_guest_last_name,
                     ra_guest_arrival_time,
                     ra_guest_departure_time,
                     ra_kind_of_room,
                     ra_is_mock_room,
                     rstat_image,
                     ra_assigned_date,
                     ra_is_reassigned_room,
                     ra_room_status_id,
                     ra_housekeeper_id,
                     ra_last_cleaning_date,
                     roomTypeName,
                     
                     ROOM_ASSIGNMENT,
                     ROOM,
                     
                     ra_room_id,
                     room_id,
                     
                     ROOM_STATUS,
                     ra_room_status_id,
                     rstat_id,
                     
                     CLEANING_STATUS,
                     ra_room_cleaning_status_id,
                     cstat_id,
                     
                     ROOM_TYPE,
                     ams_id,
                     room_type_id,
                     
                     room_floor_id,
                     (int)floorId,
                     
                     ra_user_id,
                     (int)userId,
                     
                     ra_delete,
                     
                     ra_room_status_id,
                     iGneralFilter,
                     
                     
                     ra_room_id
                     ];
    }
    
    
    NSMutableArray *result = [NSMutableArray array];
    const char *sql = [sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
    
        reAccessCount++;
    }
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW)
    {
        NSString *roomStatusCode=sqlite3_column_text(sqlStament, 2) == nil ? @"" : [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
        
        NSString *roomId = nil;
        if (sqlite3_column_text(self.sqlStament, 0)) {
            roomId = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 0)];
        }
        
        NSString *roomAssignmentID=[[NSString alloc] initWithFormat:@"%d",sqlite3_column_int(self.sqlStament,1)] ;
        
        NSData *image_cleaningStatus = nil;
        if (sqlite3_column_bytes(sqlStament, 3) > 0) {
            image_cleaningStatus = [NSData dataWithBytes:sqlite3_column_blob(sqlStament, 3) length:sqlite3_column_bytes(sqlStament, 3)];
        }
        
        NSString *roomVipStatus=sqlite3_column_text(sqlStament, 4)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
        
        NSString *guestName=sqlite3_column_text(sqlStament, 5)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
        
        NSString *raPrioritySortOrder = [NSString stringWithFormat:@"%d", sqlite3_column_int(self.sqlStament, 6)];
        
        //NSString *inspectionStatusId = [NSString stringWithFormat:@"%d", sqlite3_column_int(sqlStament, 7)];
        
        NSString *cleaningStatusId = [NSString stringWithFormat:@"%d", sqlite3_column_int(sqlStament, 8)];
        
        NSString *raFirstName = @"";
        if (sqlite3_column_text(sqlStament, 9)) {
            raFirstName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 9)];
        }
        
        NSString *raLastName = @"";
        if (sqlite3_column_text(sqlStament, 10)) {
            raLastName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 10)];
        }
        
        NSString *raGuestFirstName = @"";
        if (sqlite3_column_text(sqlStament, 11)) {
            raGuestFirstName = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 11)];
        }
        
        NSString *raGuestLastName = @"";
        if (sqlite3_column_text(sqlStament, 12)) {
            raGuestLastName = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 12)];
        }
        
        NSString *raGuestArrivalTime = @"";
        if (sqlite3_column_text(sqlStament, 13)) {
            raGuestArrivalTime = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 13)];
        }
        
        NSString *raGuestDepartureTime = @"";
        if (sqlite3_column_text(sqlStament, 14)) {
            raGuestDepartureTime = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 14)];
        }
        
        NSInteger raKindOfRoom = sqlite3_column_int(sqlStament, 15);
        NSInteger raIsMockRoom = sqlite3_column_int(sqlStament, 16);
        
        NSData *image_roomStatus = nil;
        if (sqlite3_column_bytes(sqlStament, 17) > 0) {
            image_roomStatus = [[NSData alloc] initWithBytes:sqlite3_column_blob(sqlStament, 17) length:sqlite3_column_bytes(sqlStament, 17)];
        }
        
        NSString *roomAssignmentTime = nil;
        if (sqlite3_column_bytes(sqlStament, 18)) {
            roomAssignmentTime = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 18)];
        }
        
        NSInteger raIsReassignedRoom = sqlite3_column_int(sqlStament, 19);
        NSInteger roomStatusId = sqlite3_column_int(sqlStament, 20);
        NSInteger houseKeeperId = sqlite3_column_int(sqlStament, 21);
        
        NSString *lastCleaningDate = nil;
        if (sqlite3_column_bytes(sqlStament, 22)) {
            lastCleaningDate = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 22)];
        }
        
        NSString *roomTypeValue = nil;
        if (sqlite3_column_bytes(sqlStament, 23)) {
            roomTypeValue = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 23)];
        }
        
        NSMutableDictionary *row = [[NSMutableDictionary alloc] init];
        [row setValue:[NSString stringWithFormat:@"%i",(int)userId] forKey:kRaUserId];
        [row setValue:roomId forKey:kRoomNo];
        [row setValue:roomStatusCode forKey:kRMStatus];
        [row setValue:cleaningStatusId forKey:kCleaningStatusID];
        [row setValue:guestName forKey:kGuestName];
        [row setValue:roomAssignmentID forKey:kRoomAssignmentID];
        [row setValue:raPrioritySortOrder forKey:kRaPrioritySortOrder];
        [row setValue:raFirstName forKey:kRAFirstName];
        [row setValue:raLastName forKey:kRALastName];
        [row setValue:image_cleaningStatus forKey:kCleaningStatus];
        [row setValue:raGuestFirstName forKey:kGuestFirstName];
        [row setValue:raGuestLastName forKey:kGuestLastName];
        [row setValue:raGuestArrivalTime forKey:kRaGuestArrivalTime];
        [row setValue:raGuestDepartureTime forKey:kRaGuestDepartureTime];
        [row setValue:[[NSString alloc] initWithFormat:@"%i",(int)raKindOfRoom] forKey:kRaKindOfRoom];
        [row setValue:[[NSString alloc] initWithFormat:@"%i",(int)raIsMockRoom] forKey:kRaIsMockRoom];
        [row setValue:image_roomStatus forKey:kRoomStatusImage];
        [row setValue:roomAssignmentTime forKey:kRoomAssignmentTime];
        [row setValue:[[NSString alloc] initWithFormat:@"%i",(int)raIsReassignedRoom] forKey:kRaIsReassignedRoom];
        [row setValue:roomVipStatus forKey:kVIP];
        [row setValue:[[NSString alloc] initWithFormat:@"%i",(int)roomStatusId] forKey:kRoomStatusId];
        [row setValue:[[NSString alloc] initWithFormat:@"%i",(int)houseKeeperId] forKey:kraHousekeeperID];
        [row setValue:lastCleaningDate forKey:kLastCleaningDate];
        [row setValue:roomTypeValue forKey:kRoomType];
        
        [result addObject:row];
        status = sqlite3_step(sqlStament);
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            [result removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    return result;
}

-(NSMutableArray *) getUnassignRoomsByFloorId:(NSInteger)floorId AndUserId:(NSInteger)userId
{
    NSString *roomStatus = nil;
    NSString *roomTypeName = nil;
    
    if([[LanguageManagerV2 sharedLanguageManager] currentLanguageIsEqualTo:@"%@",ENGLISH_LANGUAGE]){
        roomStatus = [NSString stringWithFormat:@"%@",rstat_name];
        roomTypeName = [NSString stringWithFormat:@"%@",ams_name];
    } else {
        roomStatus = [NSString stringWithFormat:@"%@",rstat_lang];
        roomTypeName = [NSString stringWithFormat:@"%@",ams_name_lang];
    }
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:
                                  @"select %@, %@, %@, %@ , %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ from %@ a join %@ b on a.%@ = b.%@ join %@ c on a.%@ = c.%@ join %@ d on a.%@ = d.%@ left join %@ f on f.%@ = b.%@ where b.%@ = %d and a.%@ = %d and a.%@ = 0 and a.%@ = 0 order by a.%@ asc",
                                  room_id,
                                  ra_id,
                                  //uncomment this for get room status code
                                  //rstat_code,
                                  roomStatus,
                                  cstat_image,
                                  room_vip_status,
                                  room_guest_name,
                                  ra_priority_sort_order,
                                  ra_room_inspection_status_id,
                                  cstat_id,
                                  ra_first_name,
                                  ra_last_name,
                                  ra_guest_first_name,
                                  ra_guest_last_name,
                                  ra_guest_arrival_time,
                                  ra_guest_departure_time,
                                  ra_kind_of_room,
                                  ra_is_mock_room,
                                  rstat_image,
                                  ra_assigned_date,
                                  ra_is_reassigned_room,
                                  ra_room_status_id,
                                  roomTypeName,
                                  
                                  ROOM_ASSIGNMENT,
                                  ROOM,
                                  
                                  ra_room_id,
                                  room_id,
                                  
                                  ROOM_STATUS,
                                  ra_room_status_id,
                                  rstat_id,
                                  
                                  CLEANING_STATUS,
                                  ra_room_cleaning_status_id,
                                  cstat_id,
                                  
                                  ROOM_TYPE,
                                  ams_id,
                                  room_type_id,
                                  
                                  room_floor_id,
                                  (int)floorId,
                                  
                                  ra_user_id,
                                  (int)userId,
                                  
                                  ra_delete,
                                  
                                  //House keeper id = 0 for unassign rooms
                                  ra_housekeeper_id,
                                  
                                  ra_room_id
                                  ];
    
    NSMutableArray *result = [NSMutableArray array];
    const char *sql = [sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW)
    {
        NSString *roomStatusCode=sqlite3_column_text(sqlStament, 2) == nil ? @"" : [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
        
        //            if ([UserManagerV2 isSupervisor]) {
        //                if ([roomStatusCode isEqualToString:ROOM_STATUS_VD] || [roomStatusCode isEqualToString:ROOM_STATUS_OD] || [roomStatusCode isEqualToString:ROOM_STATUS_OCI]||[roomStatusCode isEqualToString:ROOM_STATUS_VCI] ||
        //                    [roomStatusCode isEqualToString:ROOM_STATUS_OOO] || [roomStatusCode isEqualToString:ROOM_STATUS_OOS]) continue;
        //            }
        
        NSString *roomId = nil;
        if (sqlite3_column_text(self.sqlStament, 0)) {
            roomId = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 0)];
        }
        NSString *roomAssignmentID=[[NSString alloc] initWithFormat:@"%d",sqlite3_column_int(self.sqlStament,1)] ;
        
        NSData *image_cleaningStatus = nil;
        if (sqlite3_column_bytes(sqlStament, 3) > 0) {
            image_cleaningStatus = [NSData dataWithBytes:sqlite3_column_blob(sqlStament, 3) length:sqlite3_column_bytes(sqlStament, 3)];
        }
        
        NSString *roomVipStatus=sqlite3_column_text(sqlStament, 4)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
        
        NSString *guestName=sqlite3_column_text(sqlStament, 5)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
        
        NSString *raPrioritySortOrder = [NSString stringWithFormat:@"%d", sqlite3_column_int(self.sqlStament, 6)];
        
        //NSString *inspectionStatusId = [NSString stringWithFormat:@"%d", sqlite3_column_int(sqlStament, 7)];
        
        NSString *cleaningStatusId = [NSString stringWithFormat:@"%d", sqlite3_column_int(sqlStament, 8)];
        
        NSString *raFirstName = @"";
        if (sqlite3_column_text(sqlStament, 9)) {
            raFirstName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 9)];
        }
        
        NSString *raLastName = @"";
        if (sqlite3_column_text(sqlStament, 10)) {
            raLastName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 10)];
        }
        
        NSString *raGuestFirstName = @"";
        if (sqlite3_column_text(sqlStament, 11)) {
            raGuestFirstName = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 11)];
        }
        
        NSString *raGuestLastName = @"";
        if (sqlite3_column_text(sqlStament, 12)) {
            raGuestLastName = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 12)];
        }
        
        NSString *raGuestArrivalTime = @"";
        if (sqlite3_column_text(sqlStament, 13)) {
            raGuestArrivalTime = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 13)];
        }
        
        NSString *raGuestDepartureTime = @"";
        if (sqlite3_column_text(sqlStament, 14)) {
            raGuestDepartureTime = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 14)];
        }
        
        NSInteger raKindOfRoom = sqlite3_column_int(sqlStament, 15);
        NSInteger raIsMockRoom = sqlite3_column_int(sqlStament, 16);
        
        NSData *image_roomStatus = nil;
        if (sqlite3_column_bytes(sqlStament, 17) > 0) {
            image_roomStatus = [[NSData alloc] initWithBytes:sqlite3_column_blob(sqlStament, 17) length:sqlite3_column_bytes(sqlStament, 17)];
        }
        
        NSString *roomAssignmentTime = nil;
        if (sqlite3_column_bytes(sqlStament, 18)) {
            roomAssignmentTime = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 18)];
        }
        
        NSInteger raIsReassignedRoom = sqlite3_column_int(sqlStament, 19);
        
        NSInteger roomStatusId = sqlite3_column_int(sqlStament, 20);
        
        NSString *roomTypeValue = nil;
        if (sqlite3_column_bytes(sqlStament, 21)) {
            roomTypeValue = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 21)];
        }
        
        NSMutableDictionary *row = [[NSMutableDictionary alloc] init];
        [row setValue:[NSString stringWithFormat:@"%i",(int)userId] forKey:kRaUserId];
        [row setValue:roomId forKey:kRoomNo];
        [row setValue:roomStatusCode forKey:kRMStatus];
        [row setValue:cleaningStatusId forKey:kCleaningStatusID];
        [row setValue:guestName forKey:kGuestName];
        [row setValue:roomAssignmentID forKey:kRoomAssignmentID];
        [row setValue:raPrioritySortOrder forKey:kRaPrioritySortOrder];
        [row setValue:raFirstName forKey:kRAFirstName];
        [row setValue:raLastName forKey:kRALastName];
        [row setValue:image_cleaningStatus forKey:kCleaningStatus];
        [row setValue:raGuestFirstName forKey:kGuestFirstName];
        [row setValue:raGuestLastName forKey:kGuestLastName];
        [row setValue:raGuestArrivalTime forKey:kRaGuestArrivalTime];
        [row setValue:raGuestDepartureTime forKey:kRaGuestDepartureTime];
        [row setValue:[[NSString alloc] initWithFormat:@"%i",(int)raKindOfRoom] forKey:kRaKindOfRoom];
        [row setValue:[NSString stringWithFormat:@"%i",(int)raIsMockRoom] forKey:kRaIsMockRoom];
        [row setValue:image_roomStatus forKey:kRoomStatusImage];
        [row setValue:roomAssignmentTime forKey:kRoomAssignmentTime];
        [row setValue:[[NSString alloc] initWithFormat:@"%i",(int)raIsReassignedRoom] forKey:kRaIsReassignedRoom];
        [row setValue:roomVipStatus forKey:kVIP];
        [row setValue:[[NSString alloc] initWithFormat:@"%i",(int)roomStatusId] forKey:kRoomStatusId];
        [row setValue:roomTypeValue forKey:kRoomType];
        
        [result addObject:row];
        status = sqlite3_step(sqlStament);
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            [result removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    return result;
}

-(NSMutableArray *) getUnassignRoomsByUserId:(NSInteger)userId
{
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:
                                  @"select %@, %@, %@, %@ , %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ from %@ a join %@ b on a.%@ = b.%@ join %@ c on a.%@ = c.%@ join %@ d on a.%@ = d.%@ where a.%@ = %d and a.%@ = 0 and a.%@ = 0 order by a.%@ asc",
                                  room_id,
                                  ra_id,
                                  rstat_code,
                                  cstat_image,
                                  room_vip_status,
                                  room_guest_name,
                                  ra_priority_sort_order,
                                  ra_room_inspection_status_id,
                                  cstat_id,
                                  ra_first_name,
                                  ra_last_name,
                                  ra_guest_first_name,
                                  ra_guest_last_name,
                                  ra_guest_arrival_time,
                                  ra_guest_departure_time,
                                  ra_kind_of_room,
                                  ra_is_mock_room,
                                  rstat_image,
                                  ra_assigned_date,
                                  ra_is_reassigned_room,
                                  ra_room_status_id,
                                  
                                  ROOM_ASSIGNMENT,
                                  ROOM,
                                  
                                  ra_room_id,
                                  room_id,
                                  
                                  ROOM_STATUS,
                                  ra_room_status_id,
                                  rstat_id,
                                  
                                  CLEANING_STATUS,
                                  ra_room_cleaning_status_id,
                                  cstat_id,
                                  
                                  ra_user_id,
                                  (int)userId,
                                  
                                  ra_delete,
                                  
                                  //House keeper id = 0 for unassign rooms
                                  ra_housekeeper_id,
                                  
                                  ra_room_id
                                  ];
    
    NSMutableArray *result = [NSMutableArray array];
    const char *sql = [sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW)
    {
        NSString *roomStatusCode=sqlite3_column_text(sqlStament, 2) == nil ? @"" : [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
        
        //            if ([UserManagerV2 isSupervisor]) {
        //                if ([roomStatusCode isEqualToString:ROOM_STATUS_VD] || [roomStatusCode isEqualToString:ROOM_STATUS_OD] || [roomStatusCode isEqualToString:ROOM_STATUS_OCI]||[roomStatusCode isEqualToString:ROOM_STATUS_VCI] ||
        //                    [roomStatusCode isEqualToString:ROOM_STATUS_OOO] || [roomStatusCode isEqualToString:ROOM_STATUS_OOS]) continue;
        //            }
        
        NSString *roomId = nil;
        if (sqlite3_column_text(self.sqlStament, 0)) {
            roomId = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 0)];
        }
        NSString *roomAssignmentID=[[NSString alloc] initWithFormat:@"%d",sqlite3_column_int(self.sqlStament,1)] ;
        
        NSData *image_cleaningStatus = nil;
        if (sqlite3_column_bytes(sqlStament, 3) > 0) {
            image_cleaningStatus = [NSData dataWithBytes:sqlite3_column_blob(sqlStament, 3) length:sqlite3_column_bytes(sqlStament, 3)];
        }
        
        NSString *roomVipStatus=sqlite3_column_text(sqlStament, 4)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
        
        NSString *guestName=sqlite3_column_text(sqlStament, 5)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
        
        NSString *raPrioritySortOrder = [NSString stringWithFormat:@"%d", sqlite3_column_int(self.sqlStament, 6)];
        
        //NSString *inspectionStatusId = [NSString stringWithFormat:@"%d", sqlite3_column_int(sqlStament, 7)];
        
        NSString *cleaningStatusId = [NSString stringWithFormat:@"%d", sqlite3_column_int(sqlStament, 8)];
        
        NSString *raFirstName = @"";
        if (sqlite3_column_text(sqlStament, 9)) {
            raFirstName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 9)];
        }
        
        NSString *raLastName = @"";
        if (sqlite3_column_text(sqlStament, 10)) {
            raLastName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 10)];
        }
        
        NSString *raGuestFirstName = @"";
        if (sqlite3_column_text(sqlStament, 11)) {
            raGuestFirstName = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 11)];
        }
        
        NSString *raGuestLastName = @"";
        if (sqlite3_column_text(sqlStament, 12)) {
            raGuestLastName = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 12)];
        }
        
        NSString *raGuestArrivalTime = @"";
        if (sqlite3_column_text(sqlStament, 13)) {
            raGuestArrivalTime = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 13)];
        }
        
        NSString *raGuestDepartureTime = @"";
        if (sqlite3_column_text(sqlStament, 14)) {
            raGuestDepartureTime = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 14)];
        }
        
        NSInteger raKindOfRoom = sqlite3_column_int(sqlStament, 15);
        NSInteger raIsMockRoom = sqlite3_column_int(sqlStament, 16);
        
        NSData *image_roomStatus = nil;
        if (sqlite3_column_bytes(sqlStament, 17) > 0) {
            image_roomStatus = [[NSData alloc] initWithBytes:sqlite3_column_blob(sqlStament, 17) length:sqlite3_column_bytes(sqlStament, 17)];
        }
        
        NSString *roomAssignmentTime = nil;
        if (sqlite3_column_bytes(sqlStament, 18)) {
            roomAssignmentTime = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 18)];
        }
        
        NSInteger raIsReassignedRoom = sqlite3_column_int(sqlStament, 19);
        
        NSInteger roomStatusId = sqlite3_column_int(sqlStament, 20);
        
        NSMutableDictionary *row = [[NSMutableDictionary alloc] init];
        [row setValue:[NSString stringWithFormat:@"%i",(int)userId] forKey:kRaUserId];
        [row setValue:roomId forKey:kRoomNo];
        [row setValue:roomStatusCode forKey:kRMStatus];
        [row setValue:cleaningStatusId forKey:kCleaningStatusID];
        [row setValue:guestName forKey:kGuestName];
        [row setValue:roomAssignmentID forKey:kRoomAssignmentID];
        [row setValue:raPrioritySortOrder forKey:kRaPrioritySortOrder];
        [row setValue:raFirstName forKey:kRAFirstName];
        [row setValue:raLastName forKey:kRALastName];
        [row setValue:image_cleaningStatus forKey:kCleaningStatus];
        [row setValue:raGuestFirstName forKey:kGuestFirstName];
        [row setValue:raGuestLastName forKey:kGuestLastName];
        [row setValue:raGuestArrivalTime forKey:kRaGuestArrivalTime];
        [row setValue:raGuestDepartureTime forKey:kRaGuestDepartureTime];
        [row setValue:[[NSString alloc] initWithFormat:@"%i",(int)raKindOfRoom] forKey:kRaKindOfRoom];
        [row setValue:[[NSString alloc] initWithFormat:@"%i",(int)raIsMockRoom] forKey:kRaIsMockRoom];
        [row setValue:image_roomStatus forKey:kRoomStatusImage];
        [row setValue:roomAssignmentTime forKey:kRoomAssignmentTime];
        [row setValue:[[NSString alloc] initWithFormat:@"%i",(int)raIsReassignedRoom] forKey:kRaIsReassignedRoom];
        [row setValue:roomVipStatus forKey:kVIP];
        [row setValue:[[NSString alloc] initWithFormat:@"%i",(int)roomStatusId] forKey:kRoomStatusId];
        
        [result addObject:row];
        status = sqlite3_step(sqlStament);
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            [result removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    return result;
}

-(NSMutableArray *) getAllRoomByFloorId:(NSInteger)floorId AndUserId:(NSInteger)userId AndCleaningStatus:(NSInteger)cleaningStatusId
{
    NSString *roomStatus = nil;
    NSString *roomTypeName = nil;
    if([[LanguageManagerV2 sharedLanguageManager] currentLanguageIsEqualTo:@"%@",ENGLISH_LANGUAGE]){
        roomStatus = [NSString stringWithFormat:@"%@",rstat_name];
        roomTypeName = [NSString stringWithFormat:@"%@",ams_name];
    } else {
        roomStatus = [NSString stringWithFormat:@"%@",rstat_lang];
        roomTypeName = [NSString stringWithFormat:@"%@",ams_name_lang];
    }
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:
                                  @"select %@, %@, %@, %@ , %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ from %@ a join %@ b on a.%@ = b.%@ join %@ c on a.%@ = c.%@ join %@ d on a.%@ = d.%@ left join %@ f on f.%@ = b.%@ where b.%@ = %d and a.%@ = %d and a.%@ = 0 and a.%@ = %d order by a.%@ asc",
                                  room_id,
                                  ra_id,
                                  //uncomment this for get room status code
                                  //rstat_code,
                                  roomStatus,
                                  cstat_image,
                                  room_vip_status,
                                  room_guest_name,
                                  ra_priority_sort_order,
                                  ra_room_inspection_status_id,
                                  cstat_id,
                                  ra_first_name,
                                  ra_last_name,
                                  ra_guest_first_name,
                                  ra_guest_last_name,
                                  ra_guest_arrival_time,
                                  ra_guest_departure_time,
                                  ra_kind_of_room,
                                  ra_is_mock_room,
                                  rstat_image,
                                  ra_assigned_date,
                                  ra_is_reassigned_room,
                                  ra_room_status_id,
                                  roomTypeName,
                                  
                                  ROOM_ASSIGNMENT,
                                  ROOM,
                                  
                                  ra_room_id,
                                  room_id,
                                  
                                  ROOM_STATUS,
                                  ra_room_status_id,
                                  rstat_id,
                                  
                                  CLEANING_STATUS,
                                  ra_room_cleaning_status_id,
                                  cstat_id,
                                  
                                  ROOM_TYPE,
                                  ams_id,
                                  room_type_id,
                                  
                                  room_floor_id,
                                  (int)floorId,
                                  
                                  ra_user_id,
                                  (int)userId,
                                  
                                  ra_delete,
                                  
                                  ra_room_cleaning_status_id,
                                  (int)cleaningStatusId,
                                  
                                  ra_room_id
                                  ];
    
    NSMutableArray *result = [NSMutableArray array];
    const char *sql = [sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW)
    {
        NSString *roomStatusCode=sqlite3_column_text(sqlStament, 2) == nil ? @"" : [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
        
        //            if ([UserManagerV2 isSupervisor]) {
        //                if ([roomStatusCode isEqualToString:ROOM_STATUS_VD] || [roomStatusCode isEqualToString:ROOM_STATUS_OD] || [roomStatusCode isEqualToString:ROOM_STATUS_OCI]||[roomStatusCode isEqualToString:ROOM_STATUS_VCI] ||
        //                    [roomStatusCode isEqualToString:ROOM_STATUS_OOO] || [roomStatusCode isEqualToString:ROOM_STATUS_OOS]) continue;
        //            }
        
        NSString *roomId = nil;
        if (sqlite3_column_text(self.sqlStament, 0)) {
            roomId = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 0)];
        }
        NSString *roomAssignmentID=[[NSString alloc] initWithFormat:@"%d",sqlite3_column_int(self.sqlStament,1)] ;
        
        NSData *image_cleaningStatus = nil;
        if (sqlite3_column_bytes(sqlStament, 3) > 0) {
            image_cleaningStatus = [[NSData alloc] initWithBytes:sqlite3_column_blob(sqlStament, 3) length:sqlite3_column_bytes(sqlStament, 3)];
        }
        
        NSString *roomVipStatus=sqlite3_column_text(sqlStament, 4)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
        
        NSString *guestName=sqlite3_column_text(sqlStament, 5)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
        
        NSString *raPrioritySortOrder = [NSString stringWithFormat:@"%d", sqlite3_column_int(self.sqlStament, 6)];
        
        //NSString *inspectionStatusId = [NSString stringWithFormat:@"%d", sqlite3_column_int(sqlStament, 7)];
        
        NSString *cleaningStatusId = [NSString stringWithFormat:@"%d", sqlite3_column_int(sqlStament, 8)];
        
        NSString *raFirstName = @"";
        if (sqlite3_column_text(sqlStament, 9)) {
            raFirstName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 9)];
        }
        
        NSString *raLastName = @"";
        if (sqlite3_column_text(sqlStament, 10)) {
            raLastName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 10)];
        }
        
        NSString *raGuestFirstName = @"";
        if (sqlite3_column_text(sqlStament, 11)) {
            raGuestFirstName = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 11)];
        }
        
        NSString *raGuestLastName = @"";
        if (sqlite3_column_text(sqlStament, 12)) {
            raGuestLastName = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 12)];
        }
        
        NSString *raGuestArrivalTime = @"";
        if (sqlite3_column_text(sqlStament, 13)) {
            raGuestArrivalTime = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 13)];
        }
        
        NSString *raGuestDepartureTime = @"";
        if (sqlite3_column_text(sqlStament, 14)) {
            raGuestDepartureTime = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 14)];
        }
        
        NSInteger raKindOfRoom = sqlite3_column_int(sqlStament, 15);
        NSInteger raIsMockRoom = sqlite3_column_int(sqlStament, 16);
        
        NSData *image_roomStatus = nil;
        if (sqlite3_column_bytes(sqlStament, 17) > 0) {
            image_roomStatus = [[NSData alloc] initWithBytes:sqlite3_column_blob(sqlStament, 17) length:sqlite3_column_bytes(sqlStament, 17)];
        }
        
        NSString *roomAssignmentTime = nil;
        if (sqlite3_column_bytes(sqlStament, 18)) {
            roomAssignmentTime = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 18)];
        }
        
        NSInteger raIsReassignedRoom = sqlite3_column_int(sqlStament, 19);
        
        NSInteger raRoomStatusId = sqlite3_column_int(sqlStament, 20);
        
        NSString *roomTypeValue = @"";
        if (sqlite3_column_text(sqlStament, 21)) {
            roomTypeValue = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 21)];
        }
        
        NSMutableDictionary *row = [[NSMutableDictionary alloc] init];
        [row setValue:[NSString stringWithFormat:@"%i",(int)userId] forKey:kRaUserId];
        [row setValue:roomId forKey:kRoomNo];
        [row setValue:roomStatusCode forKey:kRMStatus];
        [row setValue:cleaningStatusId forKey:kCleaningStatusID];
        [row setValue:guestName forKey:kGuestName];
        [row setValue:roomAssignmentID forKey:kRoomAssignmentID];
        [row setValue:raPrioritySortOrder forKey:kRaPrioritySortOrder];
        [row setValue:raFirstName forKey:kRAFirstName];
        [row setValue:raLastName forKey:kRALastName];
        [row setValue:image_cleaningStatus forKey:kCleaningStatus];
        [row setValue:raGuestFirstName forKey:kGuestFirstName];
        [row setValue:raGuestLastName forKey:kGuestLastName];
        [row setValue:raGuestArrivalTime forKey:kRaGuestArrivalTime];
        [row setValue:raGuestDepartureTime forKey:kRaGuestDepartureTime];
        [row setValue:[[NSString alloc] initWithFormat:@"%d",(int)raKindOfRoom] forKey:kRaKindOfRoom];
        [row setValue:[[NSString alloc] initWithFormat:@"%d",(int)raIsMockRoom] forKey:kRaIsMockRoom];
        [row setValue:image_roomStatus forKey:kRoomStatusImage];
        [row setValue:roomAssignmentTime forKey:kRoomAssignmentTime];
        [row setValue:[[NSString alloc] initWithFormat:@"%d",(int)raIsReassignedRoom] forKey:kRaIsReassignedRoom];
        [row setValue:roomVipStatus forKey:kVIP];
        [row setValue:[[NSString alloc] initWithFormat:@"%d",(int)raRoomStatusId] forKey:kRoomStatusId];
        [row setValue:roomTypeValue forKey:kRoomType];
        
        [result addObject:row];
        status = sqlite3_step(sqlStament);
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            [result removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    return result;
}

-(NSMutableArray *) getAllRoomByFloorId:(NSInteger)floorId AndUserId:(NSInteger)userId AndKindOfRoom:(NSInteger)kindOfRoom
{
    NSString *roomStatus = nil;
    NSString *roomTypeName = nil;
    if([[LanguageManagerV2 sharedLanguageManager] currentLanguageIsEqualTo:@"%@",ENGLISH_LANGUAGE]){
        roomStatus = [NSString stringWithFormat:@"%@",rstat_name];
        roomTypeName = [NSString stringWithFormat:@"%@",ams_name];
    } else {
        roomStatus = [NSString stringWithFormat:@"%@",rstat_lang];
        roomTypeName = [NSString stringWithFormat:@"%@",ams_name_lang];
    }
    
    NSMutableString *sqlString = nil;
    
    if ((kindOfRoom & ENUM_KIND_OF_ROOM_OCCUPIED_DUEOUT) == ENUM_KIND_OF_ROOM_OCCUPIED_DUEOUT) { // CFG [20160913/CRF-00001439]
       
        NSString *currentDate = nil;
        NSDateFormatter *f = [[NSDateFormatter alloc] init];
        [f setDateFormat:@"yyyy-MM-dd"];
        currentDate = [NSString stringWithFormat:@"%@ 00:00:00", [f stringFromDate:[NSDate date]]];
        sqlString = [[NSMutableString alloc] initWithFormat:
                     @"select %@, %@, %@, %@ , %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ from %@ a join %@ b on a.%@ = b.%@ join %@ c on a.%@ = c.%@ join %@ d on a.%@ = d.%@ left join %@ f on f.%@ = b.%@ where b.%@ = %d and a.%@ = %d and a.%@ = 0 and a.%@ = %d and a.%@ >= '%@' order by a.%@ asc",
                     room_id,
                     ra_id,
                     //uncomment this for get room status code
                     //rstat_code,
                     roomStatus,
                     cstat_image,
                     room_vip_status,
                     room_guest_name,
                     ra_priority_sort_order,
                     ra_room_inspection_status_id,
                     cstat_id,
                     ra_first_name,
                     ra_last_name,
                     ra_guest_first_name,
                     ra_guest_last_name,
                     ra_guest_arrival_time,
                     ra_guest_departure_time,
                     ra_kind_of_room,
                     ra_is_mock_room,
                     rstat_image,
                     ra_assigned_date,
                     ra_is_reassigned_room,
                     ra_room_status_id,
                     roomTypeName,
                     
                     ROOM_ASSIGNMENT,
                     ROOM,
                     
                     ra_room_id,
                     room_id,
                     
                     ROOM_STATUS,
                     ra_room_status_id,
                     rstat_id,
                     
                     CLEANING_STATUS,
                     ra_room_cleaning_status_id,
                     cstat_id,
                     
                     ROOM_TYPE,
                     ams_id,
                     room_type_id,
                     
                     room_floor_id,
                     (int)floorId,
                     
                     ra_user_id,
                     (int)userId,
                     
                     ra_delete,
                     
                     ra_kind_of_room,
                     (int)kindOfRoom,
                     
                     ra_guest_departure_time,
                     currentDate,
                     
                     ra_room_id
                     ];
    } else {
        sqlString = [[NSMutableString alloc] initWithFormat:
                     @"select %@, %@, %@, %@ , %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ from %@ a join %@ b on a.%@ = b.%@ join %@ c on a.%@ = c.%@ join %@ d on a.%@ = d.%@ left join %@ f on f.%@ = b.%@ where b.%@ = %d and a.%@ = %d and a.%@ = 0 and a.%@ = %d order by a.%@ asc",
                     room_id,
                     ra_id,
                     //uncomment this for get room status code
                     //rstat_code,
                     roomStatus,
                     cstat_image,
                     room_vip_status,
                     room_guest_name,
                     ra_priority_sort_order,
                     ra_room_inspection_status_id,
                     cstat_id,
                     ra_first_name,
                     ra_last_name,
                     ra_guest_first_name,
                     ra_guest_last_name,
                     ra_guest_arrival_time,
                     ra_guest_departure_time,
                     ra_kind_of_room,
                     ra_is_mock_room,
                     rstat_image,
                     ra_assigned_date,
                     ra_is_reassigned_room,
                     ra_room_status_id,
                     roomTypeName,
                     
                     ROOM_ASSIGNMENT,
                     ROOM,
                     
                     ra_room_id,
                     room_id,
                     
                     ROOM_STATUS,
                     ra_room_status_id,
                     rstat_id,
                     
                     CLEANING_STATUS,
                     ra_room_cleaning_status_id,
                     cstat_id,
                     
                     ROOM_TYPE,
                     ams_id,
                     room_type_id,
                     
                     room_floor_id,
                     (int)floorId,
                     
                     ra_user_id,
                     (int)userId,
                     
                     ra_delete,
                     
                     ra_kind_of_room,
                     (int)kindOfRoom,
                     
                     ra_room_id
                     ];
    }
    NSMutableArray *result = [NSMutableArray array];
    const char *sql = [sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW)
    {
        NSString *roomStatusCode=sqlite3_column_text(sqlStament, 2) == nil ? @"" : [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
        
        //            if ([UserManagerV2 isSupervisor]) {
        //                if ([roomStatusCode isEqualToString:ROOM_STATUS_VD] || [roomStatusCode isEqualToString:ROOM_STATUS_OD] || [roomStatusCode isEqualToString:ROOM_STATUS_OCI]||[roomStatusCode isEqualToString:ROOM_STATUS_VCI] ||
        //                    [roomStatusCode isEqualToString:ROOM_STATUS_OOO] || [roomStatusCode isEqualToString:ROOM_STATUS_OOS]) continue;
        //            }
        
        NSString *roomId = nil;
        if (sqlite3_column_text(self.sqlStament, 0)) {
            roomId = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 0)];
        }
        NSString *roomAssignmentID=[[NSString alloc] initWithFormat:@"%d",sqlite3_column_int(self.sqlStament,1)] ;
        
        NSData *image_cleaningStatus = nil;
        if (sqlite3_column_bytes(sqlStament, 3) > 0) {
            image_cleaningStatus = [NSData dataWithBytes:sqlite3_column_blob(sqlStament, 3) length:sqlite3_column_bytes(sqlStament, 3)];
        }
        
        NSString *roomVipStatus=sqlite3_column_text(sqlStament, 4)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
        
        NSString *guestName=sqlite3_column_text(sqlStament, 5)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
        
        NSString *raPrioritySortOrder = [NSString stringWithFormat:@"%d", sqlite3_column_int(self.sqlStament, 6)];
        
        //NSString *inspectionStatusId = [NSString stringWithFormat:@"%d", sqlite3_column_int(sqlStament, 7)];
        
        NSString *cleaningStatusId = [NSString stringWithFormat:@"%d", sqlite3_column_int(sqlStament, 8)];
        
        NSString *raFirstName = @"";
        if (sqlite3_column_text(sqlStament, 9)) {
            raFirstName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 9)];
        }
        
        NSString *raLastName = @"";
        if (sqlite3_column_text(sqlStament, 10)) {
            raLastName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 10)];
        }
        
        NSString *raGuestFirstName = @"";
        if (sqlite3_column_text(sqlStament, 11)) {
            raGuestFirstName = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 11)];
        }
        
        NSString *raGuestLastName = @"";
        if (sqlite3_column_text(sqlStament, 12)) {
            raGuestLastName = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 12)];
        }
        
        NSString *raGuestArrivalTime = @"";
        if (sqlite3_column_text(sqlStament, 13)) {
            raGuestArrivalTime = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 13)];
        }
        
        NSString *raGuestDepartureTime = @"";
        if (sqlite3_column_text(sqlStament, 14)) {
            raGuestDepartureTime = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 14)];
        }
        
        NSInteger raKindOfRoom = sqlite3_column_int(sqlStament, 15);
        NSInteger raIsMockRoom = sqlite3_column_int(sqlStament, 16);
        
        NSData *image_roomStatus = nil;
        if (sqlite3_column_bytes(sqlStament, 17) > 0) {
            image_roomStatus = [[NSData alloc] initWithBytes:sqlite3_column_blob(sqlStament, 17) length:sqlite3_column_bytes(sqlStament, 17)];
        }
        
        NSString *roomAssignmentTime = nil;
        if (sqlite3_column_bytes(sqlStament, 18)) {
            roomAssignmentTime = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 18)];
        }
        
        NSInteger raIsReassignedRoom = sqlite3_column_int(sqlStament, 19);
        
        NSInteger raRoomStatusId = sqlite3_column_int(sqlStament, 20);
        
        NSString *roomTypeValue = @"";
        if (sqlite3_column_bytes(sqlStament, 21)) {
            roomTypeValue = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 21)];
        }
        
        NSMutableDictionary *row = [[NSMutableDictionary alloc] init];
        [row setValue:[NSString stringWithFormat:@"%i",(int)userId] forKey:kRaUserId];
        [row setValue:roomId forKey:kRoomNo];
        [row setValue:roomStatusCode forKey:kRMStatus];
        [row setValue:cleaningStatusId forKey:kCleaningStatusID];
        [row setValue:guestName forKey:kGuestName];
        [row setValue:roomAssignmentID forKey:kRoomAssignmentID];
        [row setValue:raPrioritySortOrder forKey:kRaPrioritySortOrder];
        [row setValue:raFirstName forKey:kRAFirstName];
        [row setValue:raLastName forKey:kRALastName];
        [row setValue:image_cleaningStatus forKey:kCleaningStatus];
        [row setValue:raGuestFirstName forKey:kGuestFirstName];
        [row setValue:raGuestLastName forKey:kGuestLastName];
        [row setValue:raGuestArrivalTime forKey:kRaGuestArrivalTime];
        [row setValue:raGuestDepartureTime forKey:kRaGuestDepartureTime];
        [row setValue:[[NSString alloc] initWithFormat:@"%i",(int)raKindOfRoom] forKey:kRaKindOfRoom];
        [row setValue:[[NSString alloc] initWithFormat:@"%i",(int)raIsMockRoom] forKey:kRaIsMockRoom];
        [row setValue:image_roomStatus forKey:kRoomStatusImage];
        [row setValue:roomAssignmentTime forKey:kRoomAssignmentTime];
        [row setValue:[[NSString alloc] initWithFormat:@"%i",(int)raIsReassignedRoom] forKey:kRaIsReassignedRoom];
        [row setValue:roomVipStatus forKey:kVIP];
        [row setValue:[[NSString alloc] initWithFormat:@"%i",(int)raRoomStatusId] forKey:kRoomStatusId];
        [row setValue:roomTypeValue forKey:kRoomType];
        
        [result addObject:row];
        status = sqlite3_step(sqlStament);
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            [result removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    return result;
}

-(NSMutableArray *) getAllRoomByUserId:(NSInteger)userId AndKindOfRoom:(NSInteger)kindOfRoom
{
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:
                                  @"select %@, %@, %@, %@ , %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ from %@ a join %@ b on a.%@ = b.%@ join %@ c on a.%@ = c.%@ join %@ d on a.%@ = d.%@ where a.%@ = %d and a.%@ = 0 and a.%@ = %d order by a.%@ asc",
                                  room_id,
                                  ra_id,
                                  rstat_code,
                                  cstat_image,
                                  room_vip_status,
                                  room_guest_name,
                                  ra_priority_sort_order,
                                  ra_room_inspection_status_id,
                                  cstat_id,
                                  ra_first_name,
                                  ra_last_name,
                                  ra_guest_first_name,
                                  ra_guest_last_name,
                                  ra_guest_arrival_time,
                                  ra_guest_departure_time,
                                  ra_kind_of_room,
                                  ra_is_mock_room,
                                  rstat_image,
                                  ra_assigned_date,
                                  ra_is_reassigned_room,
                                  ra_room_status_id,
                                  
                                  ROOM_ASSIGNMENT,
                                  ROOM,
                                  
                                  ra_room_id,
                                  room_id,
                                  
                                  ROOM_STATUS,
                                  ra_room_status_id,
                                  rstat_id,
                                  
                                  CLEANING_STATUS,
                                  ra_room_cleaning_status_id,
                                  cstat_id,
                                  
                                  ra_user_id,
                                  (int)userId,
                                  
                                  ra_delete,
                                  
                                  ra_kind_of_room,
                                  (int)kindOfRoom,
                                  
                                  ra_room_id
                                  ];
    
    NSMutableArray *result = [NSMutableArray array];
    const char *sql = [sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW)
    {
        NSString *roomStatusCode=sqlite3_column_text(sqlStament, 2) == nil ? @"" : [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
        
        //            if ([UserManagerV2 isSupervisor]) {
        //                if ([roomStatusCode isEqualToString:ROOM_STATUS_VD] || [roomStatusCode isEqualToString:ROOM_STATUS_OD] || [roomStatusCode isEqualToString:ROOM_STATUS_OCI]||[roomStatusCode isEqualToString:ROOM_STATUS_VCI] ||
        //                    [roomStatusCode isEqualToString:ROOM_STATUS_OOO] || [roomStatusCode isEqualToString:ROOM_STATUS_OOS]) continue;
        //            }
        
        NSString *roomId = nil;
        if (sqlite3_column_text(self.sqlStament, 0)) {
            roomId = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 0)];
        }
        NSString *roomAssignmentID=[[NSString alloc] initWithFormat:@"%d",sqlite3_column_int(self.sqlStament,1)] ;
        
        NSData *image_cleaningStatus = nil;
        if (sqlite3_column_bytes(sqlStament, 3) > 0) {
            image_cleaningStatus = [NSData dataWithBytes:sqlite3_column_blob(sqlStament, 3) length:sqlite3_column_bytes(sqlStament, 3)];
        }
        
        NSString *roomVipStatus=sqlite3_column_text(sqlStament, 4)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
        
        NSString *guestName=sqlite3_column_text(sqlStament, 5)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
        
        NSString *raPrioritySortOrder = [NSString stringWithFormat:@"%d", sqlite3_column_int(self.sqlStament, 6)];
        
        //NSString *inspectionStatusId = [NSString stringWithFormat:@"%d", sqlite3_column_int(sqlStament, 7)];
        
        NSString *cleaningStatusId = [NSString stringWithFormat:@"%d", sqlite3_column_int(sqlStament, 8)];
        
        NSString *raFirstName = @"";
        if (sqlite3_column_text(sqlStament, 9)) {
            raFirstName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 9)];
        }
        
        NSString *raLastName = @"";
        if (sqlite3_column_text(sqlStament, 10)) {
            raLastName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 10)];
        }
        
        NSString *raGuestFirstName = @"";
        if (sqlite3_column_text(sqlStament, 11)) {
            raGuestFirstName = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 11)];
        }
        
        NSString *raGuestLastName = @"";
        if (sqlite3_column_text(sqlStament, 12)) {
            raGuestLastName = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 12)];
        }
        
        NSString *raGuestArrivalTime = @"";
        if (sqlite3_column_text(sqlStament, 13)) {
            raGuestArrivalTime = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 13)];
        }
        
        NSString *raGuestDepartureTime = @"";
        if (sqlite3_column_text(sqlStament, 14)) {
            raGuestDepartureTime = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 14)];
        }
        
        NSInteger raKindOfRoom = sqlite3_column_int(sqlStament, 15);
        NSInteger raIsMockRoom = sqlite3_column_int(sqlStament, 16);
        
        NSData *image_roomStatus = nil;
        if (sqlite3_column_bytes(sqlStament, 17) > 0) {
            image_roomStatus = [[NSData alloc] initWithBytes:sqlite3_column_blob(sqlStament, 17) length:sqlite3_column_bytes(sqlStament, 17)];
        }
        
        NSString *roomAssignmentTime = nil;
        if (sqlite3_column_bytes(sqlStament, 18)) {
            roomAssignmentTime = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 18)];
        }
        
        NSInteger raIsReassignedRoom = sqlite3_column_int(sqlStament, 19);
        
        NSInteger raRoomStatusId = sqlite3_column_int(sqlStament, 20);
        
        NSMutableDictionary *row = [[NSMutableDictionary alloc] init];
        [row setValue:[NSString stringWithFormat:@"%i",(int)userId] forKey:kRaUserId];
        [row setValue:roomId forKey:kRoomNo];
        [row setValue:roomStatusCode forKey:kRMStatus];
        [row setValue:cleaningStatusId forKey:kCleaningStatusID];
        [row setValue:guestName forKey:kGuestName];
        [row setValue:roomAssignmentID forKey:kRoomAssignmentID];
        [row setValue:raPrioritySortOrder forKey:kRaPrioritySortOrder];
        [row setValue:raFirstName forKey:kRAFirstName];
        [row setValue:raLastName forKey:kRALastName];
        [row setValue:image_cleaningStatus forKey:kCleaningStatus];
        [row setValue:raGuestFirstName forKey:kGuestFirstName];
        [row setValue:raGuestLastName forKey:kGuestLastName];
        [row setValue:raGuestArrivalTime forKey:kRaGuestArrivalTime];
        [row setValue:raGuestDepartureTime forKey:kRaGuestDepartureTime];
        [row setValue:[[NSString alloc] initWithFormat:@"%i",(int)raKindOfRoom] forKey:kRaKindOfRoom];
        [row setValue:[[NSString alloc] initWithFormat:@"%i",(int)raIsMockRoom] forKey:kRaIsMockRoom];
        [row setValue:image_roomStatus forKey:kRoomStatusImage];
        [row setValue:roomAssignmentTime forKey:kRoomAssignmentTime];
        [row setValue:[[NSString alloc] initWithFormat:@"%i",(int)raIsReassignedRoom] forKey:kRaIsReassignedRoom];
        [row setValue:roomVipStatus forKey:kVIP];
        [row setValue:[[NSString alloc] initWithFormat:@"%i",(int)raRoomStatusId] forKey:kRoomStatusId];
        
        [result addObject:row];
        status = sqlite3_step(sqlStament);
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            [result removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    return result;
}

-(NSMutableArray *) getAllRoomByFloorId:(NSInteger)floorId AndUserId:(NSInteger)userId
{
    NSString *roomStatus = nil;
    NSString *roomTypeName = nil;
    if([[LanguageManagerV2 sharedLanguageManager] currentLanguageIsEqualTo:@"%@",ENGLISH_LANGUAGE]){
        roomStatus = [NSString stringWithFormat:@"%@",rstat_name];
        roomTypeName = [NSString stringWithFormat:@"%@",ams_name];
    } else {
        roomStatus = [NSString stringWithFormat:@"%@",rstat_lang];
        roomTypeName = [NSString stringWithFormat:@"%@", ams_name_lang];
    }
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:
                                  @"select %@, %@, %@, %@ , %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ from %@ a join %@ b on a.%@ = b.%@ join %@ c on a.%@ = c.%@ join %@ d on a.%@ = d.%@ left join %@ e on e.%@ = b.%@ where b.%@ = %d and a.%@ = %d and a.%@ = 0 and (a.%@ = %d or a.%@ = %d) order by a.%@ asc",
                                  room_id,
                                  ra_id,
                                  
                                  //uncomment this for get room status code
                                  //rstat_code,
                                  roomStatus,
                                  
                                  cstat_image,
                                  room_vip_status,
                                  room_guest_name,
                                  ra_priority_sort_order,
                                  ra_room_inspection_status_id,
                                  cstat_id,
                                  ra_first_name,
                                  ra_last_name,
                                  ra_guest_first_name,
                                  ra_guest_last_name,
                                  ra_guest_arrival_time,
                                  ra_guest_departure_time,
                                  ra_kind_of_room,
                                  ra_is_mock_room,
                                  rstat_image,
                                  ra_assigned_date,
                                  ra_is_reassigned_room,
                                  ra_room_status_id,
                                  roomTypeName,
                                  
                                  ROOM_ASSIGNMENT,
                                  ROOM,
                                  
                                  ra_room_id,
                                  room_id,
                                  
                                  ROOM_STATUS,
                                  ra_room_status_id,
                                  rstat_id,
                                  
                                  CLEANING_STATUS,
                                  ra_room_cleaning_status_id,
                                  cstat_id,
                                  
                                  ROOM_TYPE,
                                  ams_id,
                                  room_type_id,
                                  
                                  //WHERE
                                  room_floor_id,
                                  (int)floorId,
                                  
                                  ra_user_id,
                                  (int)userId,
                                  
                                  ra_delete,
                                  
                                  ra_room_status_id,
                                  5,
                                  ra_room_status_id,
                                  6,
                                  
                                  ra_room_id];
    
    NSMutableArray *result = [NSMutableArray array];
    const char *sql = [sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW)
    {
        NSString *roomStatusCode=sqlite3_column_text(sqlStament, 2) == nil ? @"" : [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
        
        //            if ([UserManagerV2 isSupervisor]) {
        //                if ([roomStatusCode isEqualToString:ROOM_STATUS_VD] || [roomStatusCode isEqualToString:ROOM_STATUS_OD] || [roomStatusCode isEqualToString:ROOM_STATUS_OCI]||[roomStatusCode isEqualToString:ROOM_STATUS_VCI] ||
        //                    [roomStatusCode isEqualToString:ROOM_STATUS_OOO] || [roomStatusCode isEqualToString:ROOM_STATUS_OOS]) continue;
        //            }
        
        NSString *roomId = nil;
        if (sqlite3_column_text(self.sqlStament, 0)) {
            roomId = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 0)];
        }
        NSString *roomAssignmentID=[[NSString alloc] initWithFormat:@"%d",sqlite3_column_int(self.sqlStament,1)] ;
        
        NSData *image_cleaningStatus = nil;
        if (sqlite3_column_bytes(sqlStament, 3) > 0) {
            image_cleaningStatus = [NSData dataWithBytes:sqlite3_column_blob(sqlStament, 3) length:sqlite3_column_bytes(sqlStament, 3)];
        }
        
        NSString *roomVipStatus=sqlite3_column_text(sqlStament, 4)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
        
        NSString *guestName=sqlite3_column_text(sqlStament, 5)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
        
        NSString *raPrioritySortOrder = [NSString stringWithFormat:@"%d", sqlite3_column_int(self.sqlStament, 6)];
        
        //NSString *inspectionStatusId = [NSString stringWithFormat:@"%d", sqlite3_column_int(sqlStament, 7)];
        
        NSString *cleaningStatusId = [NSString stringWithFormat:@"%d", sqlite3_column_int(sqlStament, 8)];
        
        NSString *raFirstName = @"";
        if (sqlite3_column_text(sqlStament, 9)) {
            raFirstName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 9)];
        }
        
        NSString *raLastName = @"";
        if (sqlite3_column_text(sqlStament, 10)) {
            raLastName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 10)];
        }
        
        NSString *raGuestFirstName = @"";
        if (sqlite3_column_text(sqlStament, 11)) {
            raGuestFirstName = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 11)];
        }
        
        NSString *raGuestLastName = @"";
        if (sqlite3_column_text(sqlStament, 12)) {
            raGuestLastName = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 12)];
        }
        
        NSString *raGuestArrivalTime = @"";
        if (sqlite3_column_text(sqlStament, 13)) {
            raGuestArrivalTime = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 13)];
        }
        
        NSString *raGuestDepartureTime = @"";
        if (sqlite3_column_text(sqlStament, 14)) {
            raGuestDepartureTime = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 14)];
        }
        
        NSInteger raKindOfRoom = sqlite3_column_int(sqlStament, 15);
        NSInteger raIsMockRoom = sqlite3_column_int(sqlStament, 16);
        
        NSData *image_roomStatus = nil;
        if (sqlite3_column_bytes(sqlStament, 17) > 0) {
            image_roomStatus = [[NSData alloc] initWithBytes:sqlite3_column_blob(sqlStament, 17) length:sqlite3_column_bytes(sqlStament, 18)];
        }
        
        NSString *roomAssignmentTime = nil;
        if (sqlite3_column_bytes(sqlStament, 18)) {
            roomAssignmentTime = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 18)];
        }
        
        NSInteger raIsReassignedRoom = sqlite3_column_int(sqlStament, 19);
        
        NSInteger raRoomStatusId = sqlite3_column_int(sqlStament, 20);
        
        NSString *roomTypeValue = @"";
        if (sqlite3_column_bytes(sqlStament, 21)) {
            roomTypeValue = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 21)];
        }
        
        NSMutableDictionary *row = [[NSMutableDictionary alloc] init];
        [row setValue:[[NSString alloc]initWithFormat:@"%i",(int)userId] forKey:kRaUserId];
        [row setValue:roomId forKey:kRoomNo];
        [row setValue:roomStatusCode forKey:kRMStatus];
        [row setValue:cleaningStatusId forKey:kCleaningStatusID];
        [row setValue:guestName forKey:kGuestName];
        [row setValue:roomAssignmentID forKey:kRoomAssignmentID];
        [row setValue:raPrioritySortOrder forKey:kRaPrioritySortOrder];
        [row setValue:raFirstName forKey:kRAFirstName];
        [row setValue:raLastName forKey:kRALastName];
        [row setValue:image_cleaningStatus forKey:kCleaningStatus];
        [row setValue:raGuestFirstName forKey:kGuestFirstName];
        [row setValue:raGuestLastName forKey:kGuestLastName];
        [row setValue:raGuestArrivalTime forKey:kRaGuestArrivalTime];
        [row setValue:raGuestDepartureTime forKey:kRaGuestDepartureTime];
        [row setValue:[[NSString alloc]initWithFormat:@"%i",(int)raKindOfRoom] forKey:kRaKindOfRoom];
        [row setValue:[[NSString alloc]initWithFormat:@"%i",(int)raIsMockRoom] forKey:kRaIsMockRoom];
        [row setValue:image_roomStatus forKey:kRoomStatusImage];
        [row setValue:roomAssignmentTime forKey:kRoomAssignmentTime];
        [row setValue:[[NSString alloc]initWithFormat:@"%i",(int)raIsReassignedRoom] forKey:kRaIsReassignedRoom];
        [row setValue:roomVipStatus forKey:kVIP];
        [row setValue:[[NSString alloc]initWithFormat:@"%i",(int)raRoomStatusId] forKey:kRoomStatusId];
        [row setValue:roomTypeValue forKey:kRoomType];
        
        [result addObject:row];
        status = sqlite3_step(sqlStament);
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            [result removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    return result;
}

-(NSMutableArray *) getAllOfRoomByFloorId:(NSInteger)floorId AndUserId:(NSInteger)userId andFilter:(int)kindOfRoom
{
    NSString *roomStatus = nil;
    NSString *roomTypeName = nil;
    if([[LanguageManagerV2 sharedLanguageManager] currentLanguageIsEqualTo:@"%@",ENGLISH_LANGUAGE]){
        roomStatus = [NSString stringWithFormat:@"%@",rstat_name];
        roomTypeName = [NSString stringWithFormat:@"%@",ams_name];
    } else {
        roomStatus = [NSString stringWithFormat:@"%@",rstat_lang];
        roomTypeName = [NSString stringWithFormat:@"%@", ams_name_lang];
    }
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:
                                  @"select %@, %@, %@, %@ , %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ from %@ a join %@ b on a.%@ = b.%@ join %@ c on a.%@ = c.%@ join %@ d on a.%@ = d.%@ left join %@ e on e.%@ = b.%@ where b.%@ = %d and a.%@ = %d and a.%@ = 0 and a.%@ = %d order by a.%@ asc",
                                  room_id,
                                  ra_id,
                                  
                                  //uncomment this for get room status code
                                  //rstat_code,
                                  roomStatus,
                                  
                                  cstat_image,
                                  room_vip_status,
                                  room_guest_name,
                                  ra_priority_sort_order,
                                  ra_room_inspection_status_id,
                                  cstat_id,
                                  ra_first_name,
                                  ra_last_name,
                                  ra_guest_first_name,
                                  ra_guest_last_name,
                                  ra_guest_arrival_time,
                                  ra_guest_departure_time,
                                  ra_kind_of_room,
                                  ra_is_mock_room,
                                  rstat_image,
                                  ra_assigned_date,
                                  ra_is_reassigned_room,
                                  ra_room_status_id,
                                  ra_housekeeper_id,
                                  ra_last_cleaning_date,
                                  roomTypeName,
                                  
                                  ROOM_ASSIGNMENT,
                                  ROOM,
                                  
                                  ra_room_id,
                                  room_id,
                                  
                                  ROOM_STATUS,
                                  ra_room_status_id,
                                  rstat_id,
                                  
                                  CLEANING_STATUS,
                                  ra_room_cleaning_status_id,
                                  cstat_id,
                                  
                                  ROOM_TYPE,
                                  ams_id,
                                  room_type_id,
                                  
                                  //WHERE
                                  room_floor_id,
                                  (int)floorId,
                                  
                                  ra_user_id,
                                  (int)userId,
                                  
                                  ra_delete,
                        
                                  ra_kind_of_room,
                                  kindOfRoom,
                                  ra_room_id];
    
    NSMutableArray *result = [NSMutableArray array];
    const char *sql = [sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW)
    {
        NSString *roomStatusCode=sqlite3_column_text(sqlStament, 2) == nil ? @"" : [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
        
        //            if ([UserManagerV2 isSupervisor]) {
        //                if ([roomStatusCode isEqualToString:ROOM_STATUS_VD] || [roomStatusCode isEqualToString:ROOM_STATUS_OD] || [roomStatusCode isEqualToString:ROOM_STATUS_OCI]||[roomStatusCode isEqualToString:ROOM_STATUS_VCI] ||
        //                    [roomStatusCode isEqualToString:ROOM_STATUS_OOO] || [roomStatusCode isEqualToString:ROOM_STATUS_OOS]) continue;
        //            }
        
        NSString *roomId = nil;
        if (sqlite3_column_text(self.sqlStament, 0)) {
            roomId = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 0)];
        }
        NSString *roomAssignmentID=[[NSString alloc] initWithFormat:@"%d",sqlite3_column_int(self.sqlStament,1)] ;
        
        NSData *image_cleaningStatus = nil;
        if (sqlite3_column_bytes(sqlStament, 3) > 0) {
            image_cleaningStatus = [NSData dataWithBytes:sqlite3_column_blob(sqlStament, 3) length:sqlite3_column_bytes(sqlStament, 3)];
        }
        
        NSString *roomVipStatus=sqlite3_column_text(sqlStament, 4)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
        
        NSString *guestName=sqlite3_column_text(sqlStament, 5)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
        
        NSString *raPrioritySortOrder = [NSString stringWithFormat:@"%d", sqlite3_column_int(self.sqlStament, 6)];
        
        //NSString *inspectionStatusId = [NSString stringWithFormat:@"%d", sqlite3_column_int(sqlStament, 7)];
        
        NSString *cleaningStatusId = [NSString stringWithFormat:@"%d", sqlite3_column_int(sqlStament, 8)];
        
        NSString *raFirstName = @"";
        if (sqlite3_column_text(sqlStament, 9)) {
            raFirstName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 9)];
        }
        
        NSString *raLastName = @"";
        if (sqlite3_column_text(sqlStament, 10)) {
            raLastName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 10)];
        }
        
        NSString *raGuestFirstName = @"";
        if (sqlite3_column_text(sqlStament, 11)) {
            raGuestFirstName = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 11)];
        }
        
        NSString *raGuestLastName = @"";
        if (sqlite3_column_text(sqlStament, 12)) {
            raGuestLastName = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 12)];
        }
        
        NSString *raGuestArrivalTime = @"";
        if (sqlite3_column_text(sqlStament, 13)) {
            raGuestArrivalTime = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 13)];
        }
        
        NSString *raGuestDepartureTime = @"";
        if (sqlite3_column_text(sqlStament, 14)) {
            raGuestDepartureTime = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 14)];
        }
        
        NSInteger raKindOfRoom = sqlite3_column_int(sqlStament, 15);
        NSInteger raIsMockRoom = sqlite3_column_int(sqlStament, 16);
        
        NSData *image_roomStatus = nil;
        if (sqlite3_column_bytes(sqlStament, 17) > 0) {
            image_roomStatus = [[NSData alloc] initWithBytes:sqlite3_column_blob(sqlStament, 17) length:sqlite3_column_bytes(sqlStament, 18)];
        }
        
        NSString *roomAssignmentTime = nil;
        if (sqlite3_column_bytes(sqlStament, 18)) {
            roomAssignmentTime = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 18)];
        }
        
        NSInteger raIsReassignedRoom = sqlite3_column_int(sqlStament, 19);
        
        NSInteger raRoomStatusId = sqlite3_column_int(sqlStament, 20);
        
        NSString *roomTypeValue = @"";
        if (sqlite3_column_bytes(sqlStament, 21)) {
            roomTypeValue = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 21)];
        }
        
        NSMutableDictionary *row = [[NSMutableDictionary alloc] init];
        [row setValue:[[NSString alloc]initWithFormat:@"%i",(int)userId] forKey:kRaUserId];
        [row setValue:roomId forKey:kRoomNo];
        [row setValue:roomStatusCode forKey:kRMStatus];
        [row setValue:cleaningStatusId forKey:kCleaningStatusID];
        [row setValue:guestName forKey:kGuestName];
        [row setValue:roomAssignmentID forKey:kRoomAssignmentID];
        [row setValue:raPrioritySortOrder forKey:kRaPrioritySortOrder];
        [row setValue:raFirstName forKey:kRAFirstName];
        [row setValue:raLastName forKey:kRALastName];
        [row setValue:image_cleaningStatus forKey:kCleaningStatus];
        [row setValue:raGuestFirstName forKey:kGuestFirstName];
        [row setValue:raGuestLastName forKey:kGuestLastName];
        [row setValue:raGuestArrivalTime forKey:kRaGuestArrivalTime];
        [row setValue:raGuestDepartureTime forKey:kRaGuestDepartureTime];
        [row setValue:[[NSString alloc]initWithFormat:@"%i",(int)raKindOfRoom] forKey:kRaKindOfRoom];
        [row setValue:[[NSString alloc]initWithFormat:@"%i",(int)raIsMockRoom] forKey:kRaIsMockRoom];
        [row setValue:image_roomStatus forKey:kRoomStatusImage];
        [row setValue:roomAssignmentTime forKey:kRoomAssignmentTime];
        [row setValue:[[NSString alloc]initWithFormat:@"%i",(int)raIsReassignedRoom] forKey:kRaIsReassignedRoom];
        [row setValue:roomVipStatus forKey:kVIP];
        [row setValue:[[NSString alloc]initWithFormat:@"%i",(int)raRoomStatusId] forKey:kRoomStatusId];
        [row setValue:roomTypeValue forKey:kRoomType];
        
        [result addObject:row];
        status = sqlite3_step(sqlStament);
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            [result removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    return result;
}

-(NSMutableArray *) getAllOfRoomByFloorId:(NSInteger)floorId AndUserId:(NSInteger)userId
{
    NSString *roomStatus = nil;
    NSString *roomTypeName = nil;
    if([[LanguageManagerV2 sharedLanguageManager] currentLanguageIsEqualTo:@"%@",ENGLISH_LANGUAGE]){
        roomStatus = [NSString stringWithFormat:@"%@",rstat_name];
        roomTypeName = [NSString stringWithFormat:@"%@",ams_name];
    } else {
        roomStatus = [NSString stringWithFormat:@"%@",rstat_lang];
        roomTypeName = [NSString stringWithFormat:@"%@", ams_name_lang];
    }
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:
                                  @"select %@, %@, %@, %@ , %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ from %@ a join %@ b on a.%@ = b.%@ join %@ c on a.%@ = c.%@ join %@ d on a.%@ = d.%@ left join %@ e on e.%@ = b.%@ where b.%@ = %d and a.%@ = %d and a.%@ = 0 order by a.%@ asc",
                                  room_id,
                                  ra_id,
                                  
                                  //uncomment this for get room status code
                                  //rstat_code,
                                  roomStatus,
                                  
                                  cstat_image,
                                  room_vip_status,
                                  room_guest_name,
                                  ra_priority_sort_order,
                                  ra_room_inspection_status_id,
                                  cstat_id,
                                  ra_first_name,
                                  ra_last_name,
                                  ra_guest_first_name,
                                  ra_guest_last_name,
                                  ra_guest_arrival_time,
                                  ra_guest_departure_time,
                                  ra_kind_of_room,
                                  ra_is_mock_room,
                                  rstat_image,
                                  ra_assigned_date,
                                  ra_is_reassigned_room,
                                  ra_room_status_id,
                                  ra_housekeeper_id,
                                  ra_last_cleaning_date,
                                  roomTypeName,
                                  
                                  ROOM_ASSIGNMENT,
                                  ROOM,
                                  
                                  ra_room_id,
                                  room_id,
                                  
                                  ROOM_STATUS,
                                  ra_room_status_id,
                                  rstat_id,
                                  
                                  CLEANING_STATUS,
                                  ra_room_cleaning_status_id,
                                  cstat_id,
                                  
                                  ROOM_TYPE,
                                  ams_id,
                                  room_type_id,
                                  
                                  //WHERE
                                  room_floor_id,
                                  (int)floorId,
                                  
                                  ra_user_id,
                                  (int)userId,
                                  
                                  ra_delete,
                                  
                                  ra_room_id];
    
    NSMutableArray *result = [NSMutableArray array];
    const char *sql = [sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW)
    {
        NSString *roomStatusCode=sqlite3_column_text(sqlStament, 2) == nil ? @"" : [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
        
        //            if ([UserManagerV2 isSupervisor]) {
        //                if ([roomStatusCode isEqualToString:ROOM_STATUS_VD] || [roomStatusCode isEqualToString:ROOM_STATUS_OD] || [roomStatusCode isEqualToString:ROOM_STATUS_OCI]||[roomStatusCode isEqualToString:ROOM_STATUS_VCI] ||
        //                    [roomStatusCode isEqualToString:ROOM_STATUS_OOO] || [roomStatusCode isEqualToString:ROOM_STATUS_OOS]) continue;
        //            }
        
        NSString *roomId = nil;
        if (sqlite3_column_text(self.sqlStament, 0)) {
            roomId = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 0)];
        }
        NSString *roomAssignmentID=[[NSString alloc] initWithFormat:@"%d",sqlite3_column_int(self.sqlStament,1)] ;
        
        NSData *image_cleaningStatus = nil;
        if (sqlite3_column_bytes(sqlStament, 3) > 0) {
            image_cleaningStatus = [NSData dataWithBytes:sqlite3_column_blob(sqlStament, 3) length:sqlite3_column_bytes(sqlStament, 3)];
        }
        
        NSString *roomVipStatus=sqlite3_column_text(sqlStament, 4)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
        
        NSString *guestName=sqlite3_column_text(sqlStament, 5)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
        
        NSString *raPrioritySortOrder = [NSString stringWithFormat:@"%d", sqlite3_column_int(self.sqlStament, 6)];
        
        //NSString *inspectionStatusId = [NSString stringWithFormat:@"%d", sqlite3_column_int(sqlStament, 7)];
        
        NSString *cleaningStatusId = [NSString stringWithFormat:@"%d", sqlite3_column_int(sqlStament, 8)];
        
        NSString *raFirstName = @"";
        if (sqlite3_column_text(sqlStament, 9)) {
            raFirstName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 9)];
        }
        
        NSString *raLastName = @"";
        if (sqlite3_column_text(sqlStament, 10)) {
            raLastName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 10)];
        }
        
        NSString *raGuestFirstName = @"";
        if (sqlite3_column_text(sqlStament, 11)) {
            raGuestFirstName = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 11)];
        }
        
        NSString *raGuestLastName = @"";
        if (sqlite3_column_text(sqlStament, 12)) {
            raGuestLastName = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 12)];
        }
        
        NSString *raGuestArrivalTime = @"";
        if (sqlite3_column_text(sqlStament, 13)) {
            raGuestArrivalTime = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 13)];
        }
        
        NSString *raGuestDepartureTime = @"";
        if (sqlite3_column_text(sqlStament, 14)) {
            raGuestDepartureTime = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 14)];
        }
        
        NSInteger raKindOfRoom = sqlite3_column_int(sqlStament, 15);
        NSInteger raIsMockRoom = sqlite3_column_int(sqlStament, 16);
        
        NSData *image_roomStatus = nil;
        if (sqlite3_column_bytes(sqlStament, 17) > 0) {
            image_roomStatus = [[NSData alloc] initWithBytes:sqlite3_column_blob(sqlStament, 17) length:sqlite3_column_bytes(sqlStament, 18)];
        }
        
        NSString *roomAssignmentTime = nil;
        if (sqlite3_column_bytes(sqlStament, 18)) {
            roomAssignmentTime = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 18)];
        }
        
        NSInteger raIsReassignedRoom = sqlite3_column_int(sqlStament, 19);
        
        NSInteger raRoomStatusId = sqlite3_column_int(sqlStament, 20);
        
        NSString *roomTypeValue = @"";
        if (sqlite3_column_bytes(sqlStament, 21)) {
            roomTypeValue = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 21)];
        }
        
        NSMutableDictionary *row = [[NSMutableDictionary alloc] init];
        [row setValue:[[NSString alloc]initWithFormat:@"%i",(int)userId] forKey:kRaUserId];
        [row setValue:roomId forKey:kRoomNo];
        [row setValue:roomStatusCode forKey:kRMStatus];
        [row setValue:cleaningStatusId forKey:kCleaningStatusID];
        [row setValue:guestName forKey:kGuestName];
        [row setValue:roomAssignmentID forKey:kRoomAssignmentID];
        [row setValue:raPrioritySortOrder forKey:kRaPrioritySortOrder];
        [row setValue:raFirstName forKey:kRAFirstName];
        [row setValue:raLastName forKey:kRALastName];
        [row setValue:image_cleaningStatus forKey:kCleaningStatus];
        [row setValue:raGuestFirstName forKey:kGuestFirstName];
        [row setValue:raGuestLastName forKey:kGuestLastName];
        [row setValue:raGuestArrivalTime forKey:kRaGuestArrivalTime];
        [row setValue:raGuestDepartureTime forKey:kRaGuestDepartureTime];
        [row setValue:[[NSString alloc]initWithFormat:@"%i",(int)raKindOfRoom] forKey:kRaKindOfRoom];
        [row setValue:[[NSString alloc]initWithFormat:@"%i",(int)raIsMockRoom] forKey:kRaIsMockRoom];
        [row setValue:image_roomStatus forKey:kRoomStatusImage];
        [row setValue:roomAssignmentTime forKey:kRoomAssignmentTime];
        [row setValue:[[NSString alloc]initWithFormat:@"%i",(int)raIsReassignedRoom] forKey:kRaIsReassignedRoom];
        [row setValue:roomVipStatus forKey:kVIP];
        [row setValue:[[NSString alloc]initWithFormat:@"%i",(int)raRoomStatusId] forKey:kRoomStatusId];
        [row setValue:roomTypeValue forKey:kRoomType];
        
        [result addObject:row];
        status = sqlite3_step(sqlStament);
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            [result removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    return result;
}

-(NSMutableArray *) getAllRoomInspectionWithSupervisorId:(NSInteger) userId andRoomStatusID:(NSInteger) roomStatusId{
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"%@  %@, %@, %@, %@ ,%@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, ams_name from %@ a left join %@ b on b.%@=a.%@ left join %@ c on a.%@=c.%@  left join %@ d on a.%@=d.%@ left join %@ e on e.%@=a.%@ left join room_type f on f.ams_id = b.room_type_id where a.%@=%d AND a.%@ = %d AND %@ = 0 order by e.%@",
                                  @"select  ",
                                  room_id,
                                  ra_id,
                                  rstat_code,
                                  cstat_image,
                                  room_vip_status,
                                  room_guest_name,
                                  ra_priority_sort_order,
                                  ra_room_inspection_status_id,
                                  cstat_id,
                                  ra_first_name,
                                  ra_last_name,
                                  ra_guest_first_name,
                                  ra_guest_last_name,
                                  ra_guest_arrival_time,
                                  ra_guest_departure_time,
                                  ra_kind_of_room,
                                  ra_is_mock_room,
                                  rstat_image,
                                  ra_assigned_date,
                                  ra_is_reassigned_room,
                                  
                                  ROOM_ASSIGNMENT,
                                  ROOM,
                                  room_id,
                                  ra_room_id,
                                  
                                  ROOM_STATUS,
                                  ra_room_status_id,
                                  rstat_id,
                                  
                                  CLEANING_STATUS,
                                  ra_room_cleaning_status_id,
                                  cstat_id,
                                  
                                  ROOM_RECORDS,
                                  rrec_roomAssignment_id,
                                  ra_id,
                                  
                                  ra_user_id,
                                  (int)userId,
                                  
                                  ra_room_status_id,
                                  (int)roomStatusId,
                                  
                                  ra_delete,
                                  
                                  rrec_cleaning_date
                                  ];
    NSMutableArray *result = [NSMutableArray array];
    const char *sql = [sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW)
    {
        NSString *roomStatusCode=sqlite3_column_text(sqlStament, 2) == nil ? @"" : [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
        
        //            if ([UserManagerV2 isSupervisor]) {
        //                if ([roomStatusCode isEqualToString:ROOM_STATUS_VD] || [roomStatusCode isEqualToString:ROOM_STATUS_OD] || [roomStatusCode isEqualToString:ROOM_STATUS_OCI]||[roomStatusCode isEqualToString:ROOM_STATUS_VCI] ||
        //                    [roomStatusCode isEqualToString:ROOM_STATUS_OOO] || [roomStatusCode isEqualToString:ROOM_STATUS_OOS]) continue;
        //            }
        
        NSString *roomId = nil;
        if (sqlite3_column_text(self.sqlStament, 0)) {
            roomId = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 0)];
        }
        NSString *roomAssignmentID=[[NSString alloc] initWithFormat:@"%d",sqlite3_column_int(self.sqlStament,1)] ;
        
        NSData *image_cleaningStatus = nil;
        if (sqlite3_column_bytes(sqlStament, 3) > 0) {
            image_cleaningStatus = [NSData dataWithBytes:sqlite3_column_blob(sqlStament, 3) length:sqlite3_column_bytes(sqlStament, 3)];
        }
        
        NSString *roomVipStatus=sqlite3_column_text(sqlStament, 4)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
        
        NSString *guestName=sqlite3_column_text(sqlStament, 5)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
        
        NSString *raPrioritySortOrder = [NSString stringWithFormat:@"%d", sqlite3_column_int(self.sqlStament, 6)];
        
        //NSString *inspectionStatusId = [NSString stringWithFormat:@"%d", sqlite3_column_int(sqlStament, 7)];
        
        NSString *cleaningStatusId = [NSString stringWithFormat:@"%d", sqlite3_column_int(sqlStament, 8)];
        
        NSString *raFirstName = @"";
        if (sqlite3_column_text(sqlStament, 9)) {
            raFirstName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 9)];
        }
        
        NSString *raLastName = @"";
        if (sqlite3_column_text(sqlStament, 10)) {
            raLastName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 10)];
        }
        
        NSString *raGuestFirstName = @"";
        if (sqlite3_column_text(sqlStament, 11)) {
            raGuestFirstName = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 11)];
        }
        
        NSString *raGuestLastName = @"";
        if (sqlite3_column_text(sqlStament, 12)) {
            raGuestLastName = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 12)];
        }
        
        NSString *raGuestArrivalTime = @"";
        if (sqlite3_column_text(sqlStament, 13)) {
            raGuestArrivalTime = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 13)];
        }
        
        NSString *raGuestDepartureTime = @"";
        if (sqlite3_column_text(sqlStament, 14)) {
            raGuestDepartureTime = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 14)];
        }
        
        NSInteger raKindOfRoom = sqlite3_column_int(sqlStament, 15);
        NSInteger raIsMockRoom = sqlite3_column_int(sqlStament, 16);
        
        NSData *image_roomStatus = nil;
        if (sqlite3_column_bytes(sqlStament, 17) > 0) {
            image_roomStatus = [[NSData alloc] initWithBytes:sqlite3_column_blob(sqlStament, 17) length:sqlite3_column_bytes(sqlStament, 18)];
        }
        
        NSString *roomAssignmentTime = nil;
        if (sqlite3_column_bytes(sqlStament, 18)) {
            roomAssignmentTime = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 18)];
        }
        
        NSInteger raIsReassignedRoom = sqlite3_column_int(sqlStament, 19);
        NSString *roomType = @"";
        if (sqlite3_column_text(sqlStament, 20)) {
            roomType = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 20)];
        }
        
        NSMutableDictionary *row = [[NSMutableDictionary alloc] init];
        [row setValue:[NSString stringWithFormat:@"%i",(int)userId] forKey:kRaUserId];
        [row setValue:roomId forKey:kRoomNo];
        [row setValue:roomStatusCode forKey:kRMStatus];
        [row setValue:cleaningStatusId forKey:kCleaningStatusID];
        [row setValue:guestName forKey:kGuestName];
        [row setValue:roomAssignmentID forKey:kRoomAssignmentID];
        [row setValue:raPrioritySortOrder forKey:kRaPrioritySortOrder];
        [row setValue:raFirstName forKey:kRAFirstName];
        [row setValue:raLastName forKey:kRALastName];
        [row setValue:image_cleaningStatus forKey:kCleaningStatus];
        [row setValue:raGuestFirstName forKey:kGuestFirstName];
        [row setValue:raGuestLastName forKey:kGuestLastName];
        [row setValue:raGuestArrivalTime forKey:kRaGuestArrivalTime];
        [row setValue:raGuestDepartureTime forKey:kRaGuestDepartureTime];
        [row setValue:[[NSString alloc]initWithFormat:@"%i",(int)raKindOfRoom] forKey:kRaKindOfRoom];
        [row setValue:[[NSString alloc]initWithFormat:@"%i",(int)raIsMockRoom] forKey:kRaIsMockRoom];
        [row setValue:image_roomStatus forKey:kRoomStatusImage];
        [row setValue:roomAssignmentTime forKey:kRoomAssignmentTime];
        [row setValue:[[NSString alloc]initWithFormat:@"%i",(int)raIsReassignedRoom] forKey:kRaIsReassignedRoom];
        [row setValue:roomVipStatus forKey:kVIP];
        [row setValue:[[NSString alloc]initWithFormat:@"%i",(int)roomStatusId] forKey:kRoomStatusId];
        [row setValue:roomType forKey:kRoomType];
        [result addObject:row];
        status = sqlite3_step(sqlStament);
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            [result removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    return result;
}


-(NSInteger)getTimeDifferenceBetweenStartDate:(NSString *)startDate andEndDate:(NSString *)endDate {
    NSInteger timeDifference = 0;
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSDate *start = [formatter dateFromString:startDate];
    
    NSDate *end = [formatter dateFromString:endDate];
    
    NSTimeInterval timeInterval = [end timeIntervalSinceDate:start];
    timeDifference = timeInterval;
    
    return timeDifference;
}

-(NSDictionary *) getRoomAssignmentDataByRoomAssignmentId:(NSInteger)roomAssignmentId andRaUserId:(NSInteger) raUserId
{
    NSMutableDictionary *result = nil;
    
    NSString *currentDate = nil;
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    [f setDateFormat:@"yyyy-MM-dd"];
    currentDate = [NSString stringWithFormat:@"%@ 00:00:00", [f stringFromDate:[NSDate date]]];
    
    NSString *roomStatusName = [NSString stringWithFormat:@"%@",rstat_name];
    NSString *roomTypeName = [NSString stringWithFormat:@"%@",ams_name];
    if(![[LanguageManagerV2 sharedLanguageManager] currentLanguageIsEqualTo:@"%@", ENGLISH_LANGUAGE]){
        roomStatusName = [NSString stringWithFormat:@"%@",rstat_lang];
        roomTypeName = [NSString stringWithFormat:@"%@", ams_name_lang];
    }
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"%@  %@, %@, %@, %@, %@ ,%@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@  from %@ a left join %@ b on b.%@=a.%@ left join %@ c on a.%@=c.%@ left join %@ d on a.%@=d.%@ left join %@ e on b.%@ = e.%@ where a.ra_id = %i AND a.%@=%d AND %@ >= '%@' AND %@ = 0 order by a.%@",
                                  @"select  ",
                                  ra_room_id,
                                  ra_id,
                                  
                                  //uncomment this for get room status code
                                  //rstat_code,
                                  
                                  //get room status name
                                  roomStatusName,
                                  cstat_image,
                                  room_vip_status,
                                  room_guest_name,
                                  ra_priority_sort_order,
                                  ra_room_inspection_status_id,
                                  cstat_id,
                                  ra_first_name,
                                  ra_last_name,
                                  rstat_id,
                                  ra_guest_first_name,
                                  ra_guest_last_name,
                                  ra_guest_arrival_time,
                                  ra_guest_departure_time,
                                  ra_kind_of_room,
                                  ra_is_mock_room,
                                  rstat_image,
                                  roomTypeName,
                                  
                                  ROOM_ASSIGNMENT,
                                  ROOM,
                                  room_id,
                                  ra_room_id,
                                  
                                  ROOM_STATUS,
                                  ra_room_status_id,
                                  rstat_id,
                                  
                                  CLEANING_STATUS,
                                  ra_room_cleaning_status_id,
                                  cstat_id,
                                  
                                  ROOM_TYPE,
                                  room_type_id,
                                  ams_id, //this is room type id in table room_type
                                  
                                  (int)roomAssignmentId,
                                  ra_user_id,
                                  (int)raUserId,
                                  ra_assigned_date,
                                  currentDate,
                                  
                                  ra_delete,
                                  
                                  //additionalCondition,
                                  
                                  ra_assigned_date
                                  
                                  ];
    
    const char *sql =[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW)
    {
        NSString *roomId = nil;
        if (sqlite3_column_text(self.sqlStament, 0)) {
            roomId = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 0)];
        }
        NSString *roomAssignmentID=[[NSString alloc] initWithFormat:@"%d",sqlite3_column_int(self.sqlStament,1)] ;
        
        NSData *image_cleaningStatus = nil;
        if (sqlite3_column_bytes(sqlStament, 3) > 0) {
            image_cleaningStatus = [NSData dataWithBytes:sqlite3_column_blob(sqlStament, 3) length:sqlite3_column_bytes(sqlStament, 3)];
        }
        
        NSString *roomStatusCode = sqlite3_column_text(sqlStament, 2) == nil ? @"": [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
        
        NSString *roomVipStatus=sqlite3_column_text(sqlStament, 4)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
        
        NSString *guestName=sqlite3_column_text(sqlStament, 5)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
        
        NSString *raPrioritySortOrder = @"";
        if(sqlite3_column_int(self.sqlStament, 6)){
            raPrioritySortOrder =  [NSString stringWithFormat:@"%d", sqlite3_column_int(self.sqlStament, 6)];
        }
        
        NSInteger roomInspectionStatus = sqlite3_column_int(sqlStament, 7);
        
        NSInteger cleaningStatusId = sqlite3_column_int(sqlStament, 8);
        NSString *cleaningStatusIdString =@"";
        if(cleaningStatusId){
            cleaningStatusIdString = [NSString stringWithFormat:@"%d", (int)cleaningStatusId];
        }
        
        NSString *raFirstName = @"";
        if (sqlite3_column_text(sqlStament, 9)) {
            raFirstName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 9)];
        }
        
        NSString *raLastName = @"";
        if (sqlite3_column_text(sqlStament, 10)) {
            raLastName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 10)];
        }
        
        NSInteger roomStatusId = sqlite3_column_int(self.sqlStament, 11);
        NSString *roomStatusIdString = @"";
        if(roomStatusId){
            roomStatusIdString = [NSString stringWithFormat:@"%d", (int)roomStatusId];
        }
        
        NSString *raGuestFirstName = @"";
        if (sqlite3_column_text(sqlStament, 12)) {
            raGuestFirstName = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 12)];
        }
        
        NSString *raGuestLastName = @"";
        if (sqlite3_column_text(sqlStament, 13)) {
            raGuestLastName = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 13)];
        }
        
        NSString *raGuestArrivalTime = @"";
        if (sqlite3_column_text(sqlStament, 14)) {
            raGuestArrivalTime = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 14)];
        }
        
        if([LogFileManager isLogConsole])
        {
            NSLog(@"%@",raGuestArrivalTime);
        }
        
        NSString *raGuestDepartureTime = @"";
        if (sqlite3_column_text(sqlStament, 15)) {
            raGuestDepartureTime = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 15)];
        }
        
        if([LogFileManager isLogConsole])
        {
            NSLog(@"%@",raGuestDepartureTime);
        }
        
        NSInteger raKindOfRoom = sqlite3_column_int(sqlStament, 16);
        NSInteger raIsMockRoom = sqlite3_column_int(sqlStament, 17);
        
        NSData *image_roomStatus = nil;
        if (sqlite3_column_bytes(sqlStament, 18) > 0) {
            image_roomStatus = [[NSData alloc] initWithBytes:sqlite3_column_blob(sqlStament, 18) length:sqlite3_column_bytes(sqlStament, 18)];
        }
        
        NSString *roomTypeNameValue = @"";
        if (sqlite3_column_text(sqlStament, 19)) {
            roomTypeNameValue = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 19)];
        }
        
        result = [[NSMutableDictionary alloc] init];
        [result setValue:[[NSString alloc]initWithFormat:@"%i",(int)raUserId] forKey:kRaUserId];
        [result setValue:roomId forKey:kRoomNo];
        [result setValue:roomStatusIdString forKey:kRoomStatusId];
        [result setValue:roomStatusCode forKey:kRMStatus];
        [result setValue:cleaningStatusIdString forKey:kCleaningStatusID];
        [result setValue:guestName forKey:kGuestName];
        [result setValue:roomAssignmentID forKey:kRoomAssignmentID];
        [result setValue:raPrioritySortOrder forKey:kRaPrioritySortOrder];
        [result setValue:raFirstName forKey:kRAFirstName];
        [result setValue:raLastName forKey:kRALastName];
        [result setValue:image_cleaningStatus forKey:kCleaningStatus];
        [result setValue:raGuestFirstName forKey:kGuestFirstName];
        [result setValue:raGuestLastName forKey:kGuestLastName];
        [result setValue:raGuestArrivalTime forKey:kRaGuestArrivalTime];
        [result setValue:raGuestDepartureTime forKey:kRaGuestDepartureTime];
        [result setValue:[[NSString alloc]initWithFormat:@"%i",(int)raKindOfRoom] forKey:kRaKindOfRoom];
        [result setValue:[[NSString alloc]initWithFormat:@"%i",(int)raIsMockRoom] forKey:kRaIsMockRoom];
        [result setValue:image_roomStatus forKey:kRoomStatusImage];
        [result setValue:roomVipStatus forKey:kVIP];
        [result setValue:[[NSString alloc]initWithFormat:@"%i",(int)roomInspectionStatus] forKey:KInspection_Status_id];
        [result setValue:roomTypeNameValue forKey:kRoomType];
        status = sqlite3_step(sqlStament);
        
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    return result;
}

-(NSDictionary *) getRoomAssignmentDataByRoomAssignmentId:(NSInteger)roomAssignmentId andRaUserId:(NSInteger) raUserId isFilterCurrentDate:(BOOL)isFilterCurrentDate
{
    NSMutableDictionary *result = nil;
    
    if(isFilterCurrentDate)
    {
        result = [[NSMutableDictionary alloc] initWithDictionary:[self getRoomAssignmentDataByRoomAssignmentId:roomAssignmentId andRaUserId:raUserId]];
        return result;
    }
    
    NSString *currentDate = nil;
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    [f setDateFormat:@"yyyy-MM-dd"];
    currentDate = [NSString stringWithFormat:@"%@ 00:00:00", [f stringFromDate:[NSDate date]]];
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"%@  %@, %@, %@, %@, %@ ,%@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@  from %@ a left join %@ b on b.%@=a.%@ left join %@ c on a.%@=c.%@  left join %@ d on a.%@=d.%@  where a.ra_id = %i AND a.%@=%d order by a.%@",
                                  @"select  ",
                                  ra_room_id,
                                  ra_id,
                                  rstat_name,
                                  cstat_image,
                                  room_vip_status,
                                  room_guest_name,
                                  ra_priority_sort_order,
                                  ra_room_inspection_status_id,
                                  cstat_id,
                                  ra_first_name,
                                  ra_last_name,
                                  rstat_id,
                                  ra_guest_first_name,
                                  ra_guest_last_name,
                                  ra_guest_arrival_time,
                                  ra_guest_departure_time,
                                  ra_kind_of_room,
                                  ra_is_mock_room,
                                  rstat_image,
                                  
                                  ROOM_ASSIGNMENT,
                                  ROOM,
                                  room_id,
                                  ra_room_id,
                                  
                                  ROOM_STATUS,
                                  ra_room_status_id,
                                  rstat_id,
                                  
                                  CLEANING_STATUS,
                                  ra_room_cleaning_status_id,
                                  cstat_id,
                                  
                                  (int)roomAssignmentId,
                                  ra_user_id,
                                  (int)raUserId,
                                  
                                  //ra_delete,
                                  
                                  //additionalCondition,
                                  
                                  ra_assigned_date
                                  
                                  ];
    
    const char *sql =[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW)
    {
        NSString *roomId = nil;
        if (sqlite3_column_text(self.sqlStament, 0)) {
            roomId = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 0)];
        }
        NSString *roomAssignmentID=[[NSString alloc] initWithFormat:@"%d",sqlite3_column_int(self.sqlStament,1)] ;
        
        NSData *image_cleaningStatus = nil;
        if (sqlite3_column_bytes(sqlStament, 3) > 0) {
            image_cleaningStatus = [NSData dataWithBytes:sqlite3_column_blob(sqlStament, 3) length:sqlite3_column_bytes(sqlStament, 3)];
        }
        
        NSString *roomStatusCode = sqlite3_column_text(sqlStament, 2) == nil ? @"": [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
        
        NSString *roomVipStatus=sqlite3_column_text(sqlStament, 4)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
        
        NSString *guestName=sqlite3_column_text(sqlStament, 5)==nil?@"":[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
        
        NSString *raPrioritySortOrder = @"";
        if(sqlite3_column_int(self.sqlStament, 6)){
            raPrioritySortOrder =  [NSString stringWithFormat:@"%d", sqlite3_column_int(self.sqlStament, 6)];
        }
        
        NSInteger roomInspectionStatus = sqlite3_column_int(sqlStament, 7);
        
        NSInteger cleaningStatusId = sqlite3_column_int(sqlStament, 8);
        NSString *cleaningStatusIdString =@"";
        if(cleaningStatusId){
            cleaningStatusIdString = [NSString stringWithFormat:@"%d", (int)cleaningStatusId];
        }
        
        NSString *raFirstName = @"";
        if (sqlite3_column_text(sqlStament, 9)) {
            raFirstName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 9)];
        }
        
        NSString *raLastName = @"";
        if (sqlite3_column_text(sqlStament, 10)) {
            raLastName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 10)];
        }
        
        NSInteger roomStatusId = sqlite3_column_int(self.sqlStament, 11);
        NSString *roomStatusIdString = @"";
        if(roomStatusId){
            roomStatusIdString = [NSString stringWithFormat:@"%d", (int)roomStatusId];
        }
        
        NSString *raGuestFirstName = @"";
        if (sqlite3_column_text(sqlStament, 12)) {
            raGuestFirstName = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 12)];
        }
        
        NSString *raGuestLastName = @"";
        if (sqlite3_column_text(sqlStament, 13)) {
            raGuestLastName = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 13)];
        }
        
        NSString *raGuestArrivalTime = @"";
        if (sqlite3_column_text(sqlStament, 14)) {
            raGuestArrivalTime = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 14)];
        }
        
        if([LogFileManager isLogConsole])
        {
            NSLog(@"%@",raGuestArrivalTime);
        }
        
        NSString *raGuestDepartureTime = @"";
        if (sqlite3_column_text(sqlStament, 15)) {
            raGuestDepartureTime = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(sqlStament, 15)];
        }
        
        if([LogFileManager isLogConsole])
        {
            NSLog(@"%@",raGuestDepartureTime);
        }
        
        NSInteger raKindOfRoom = sqlite3_column_int(sqlStament, 16);
        NSInteger raIsMockRoom = sqlite3_column_int(sqlStament, 17);
        
        NSData *image_roomStatus = nil;
        if (sqlite3_column_bytes(sqlStament, 18) > 0) {
            image_roomStatus = [[NSData alloc] initWithBytes:sqlite3_column_blob(sqlStament, 18) length:sqlite3_column_bytes(sqlStament, 18)];
        }
        
        result = [[NSMutableDictionary alloc] init];
        [result setValue:[NSString stringWithFormat:@"%i",(int)raUserId] forKey:kRaUserId];
        [result setValue:roomId forKey:kRoomNo];
        [result setValue:roomStatusIdString forKey:kRoomStatusId];
        [result setValue:roomStatusCode forKey:kRMStatus];
        [result setValue:cleaningStatusIdString forKey:kCleaningStatusID];
        [result setValue:guestName forKey:kGuestName];
        [result setValue:roomAssignmentID forKey:kRoomAssignmentID];
        [result setValue:raPrioritySortOrder forKey:kRaPrioritySortOrder];
        [result setValue:raFirstName forKey:kRAFirstName];
        [result setValue:raLastName forKey:kRALastName];
        [result setValue:image_cleaningStatus forKey:kCleaningStatus];
        [result setValue:raGuestFirstName forKey:kGuestFirstName];
        [result setValue:raGuestLastName forKey:kGuestLastName];
        [result setValue:raGuestArrivalTime forKey:kRaGuestArrivalTime];
        [result setValue:raGuestDepartureTime forKey:kRaGuestDepartureTime];
        [result setValue:[[NSString alloc]initWithFormat:@"%i",(int)raKindOfRoom] forKey:kRaKindOfRoom];
        [result setValue:[[NSString alloc]initWithFormat:@"%i",(int)raIsMockRoom] forKey:kRaIsMockRoom];
        [result setValue:image_roomStatus forKey:kRoomStatusImage];
        [result setValue:roomVipStatus forKey:kVIP];
        [result setValue:[[NSString alloc]initWithFormat:@"%i",(int)roomInspectionStatus] forKey:KInspection_Status_id];
        
        status = sqlite3_step(sqlStament);
        
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    return result;
}

@end
