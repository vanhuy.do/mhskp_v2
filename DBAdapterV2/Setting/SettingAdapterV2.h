//
//  SettingAdapterV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SettingModelV2.h"
#import "DatabaseAdapterV2.h"
#import "DbDefinesV2.h"

@interface SettingAdapterV2 : DatabaseAdapterV2{
    
}
-(int) insertSettingModel:(SettingModelV2 *)settingModel;
-(int) updateSettingModel:(SettingModelV2 *)settingModel;
-(void) loadSettingModel:(SettingModelV2 *)settingModel;

@end

