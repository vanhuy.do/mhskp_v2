//
//  SettingAdapterV2.m
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SettingAdapterV2.h"
#import "ehkDefines.h"

@implementation SettingAdapterV2
-(int) insertSettingModel:(SettingModelV2 *)settingModel
{

    NSString *sqlString = [[NSString alloc] initWithFormat:@"INSERT INTO %@  ( %@, %@, %@, %@, %@ , %@, %@, %@, %@, %@, %@, %@) %@",
                           
                           SETTINGS, 
                           
                           settings_lang, 
                           settings_sync, 
                           settings_last_sync, 
                           settings_notification_type,
                           settings_photo,
                           setting_log_enable,
                           settings_log_amount,
                           settings_ws_url,
                           settings_room_validate_enable,
                           settings_background_mode,
                           settings_heartbeat_server,
                           settings_heartbeat_port,
                           
                           @"VALUES( ? , ? , ? , ? , ? , ?, ?, ?, ?, ?, ?)"];
    
    const char *sql=[sqlString UTF8String];
        
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSInteger  status;
    status = sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL);
    
    if(status != SQLITE_OK)
    {
        
    }

    sqlite3_bind_text(self.sqlStament, 1, [settingModel.settingsLang UTF8String], -1, SQLITE_TRANSIENT); 
    sqlite3_bind_int (self.sqlStament, 2, (int)settingModel.settingsSynch);
    sqlite3_bind_text(self.sqlStament, 3, [settingModel.settingsLastSync UTF8String], -1, SQLITE_TRANSIENT); 
    sqlite3_bind_int (self.sqlStament, 4, (int)settingModel.settingsNotificationType);
    sqlite3_bind_int (self.sqlStament, 5, (int)settingModel.settingsPhoto);
    if (settingModel.settingEnableLog) {
        sqlite3_bind_int (self.sqlStament, 6, 1);
    }
    else {
        sqlite3_bind_int (self.sqlStament, 6, 0);
    }
    sqlite3_bind_int (self.sqlStament, 7, (int)settingModel.settingsLogAmount);
    sqlite3_bind_text(self.sqlStament, 8, [settingModel.settingsWSURL UTF8String], -1, SQLITE_TRANSIENT);
    if (settingModel.settingEnableRoomValidate) {
        sqlite3_bind_int (self.sqlStament, 9, 1);
    }
    // CFG [20160927/CRF-00001432] - Set background mode settings value.
    sqlite3_bind_int (self.sqlStament, 10, (int)settingModel.settingsBackgroundMode);
    sqlite3_bind_text(self.sqlStament, 11, [settingModel.settingsHeartbeatServer UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 12, [settingModel.settingsHeartbeatPort UTF8String], -1, SQLITE_TRANSIENT);
    // CFG [20160927/CRF-00001432] - End.
    
    
    NSInteger returnInt = 0;
    status = sqlite3_step(self.sqlStament);
    
    if(SQLITE_DONE != status)
    {   
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return (int)returnInt;
    }
    else
    {
        sqlite3_reset(self.sqlStament);
        returnInt = 1;
        return (int)returnInt;
    }
    
}


-(int) updateSettingModel:(SettingModelV2 *)settingModel
{

    NSString *sqlString = [[NSString alloc] initWithFormat:@"update %@ set %@ = ?, %@ = ? ,%@ = ? , %@ = ? , %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ? , %@ = ? , %@ = ?  where %@ = ? ",
                           
                           SETTINGS, 
                           
                           settings_lang, 
                           settings_sync, 
                           settings_last_sync, 
                           settings_notification_type,
                           settings_photo,
                           setting_log_enable,
                           settings_log_amount,
                           settings_ws_url,
                           settings_room_validate_enable,
                           settings_background_mode,
                           settings_heartbeat_server,
                           settings_heartbeat_port,
                           @"rowid"
                           
                           ];
        
    const char *sql=[sqlString UTF8String];

    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }

    NSInteger status;
    status = sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL);
        
    if(status != SQLITE_OK)
    {
        
    }
        
    sqlite3_bind_text(self.sqlStament, 1, [settingModel.settingsLang UTF8String], -1, SQLITE_TRANSIENT); 
    sqlite3_bind_int (self.sqlStament, 2, (int)settingModel.settingsSynch);
    sqlite3_bind_text(self.sqlStament, 3, [settingModel.settingsLastSync UTF8String], -1, SQLITE_TRANSIENT); 
    sqlite3_bind_int (self.sqlStament, 4, (int)settingModel.settingsNotificationType);
    sqlite3_bind_int(self.sqlStament, 5, (int)settingModel.settingsPhoto);
    if (settingModel.settingEnableLog) {
        sqlite3_bind_int(self.sqlStament, 6, 1);
    }
    else {
        sqlite3_bind_int(self.sqlStament, 6, 0);
    }
    sqlite3_bind_int(self.sqlStament, 7, (int)settingModel.settingsLogAmount);
    sqlite3_bind_text(self.sqlStament, 8, [settingModel.settingsWSURL UTF8String], -1, SQLITE_TRANSIENT);
    if (settingModel.settingEnableRoomValidate) {
        sqlite3_bind_int(self.sqlStament, 9, 1);
    }
    else {
        sqlite3_bind_int(self.sqlStament, 9, 0);
    }
    
    // CFG [20160927/CRF-00001432] - Set background mode settings value.
    sqlite3_bind_int(self.sqlStament, 10, (int)settingModel.settingsBackgroundMode);
    sqlite3_bind_text(self.sqlStament, 11, [settingModel.settingsHeartbeatServer UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 12, [settingModel.settingsHeartbeatPort UTF8String], -1, SQLITE_TRANSIENT);
    // CFG [20160927/CRF-00001432] - End.
    sqlite3_bind_int (self.sqlStament, 13, (int)settingModel.row_Id);
    
    NSInteger returnInt = 0;
    status =  sqlite3_step(self.sqlStament);
    
    if(SQLITE_DONE != status)
    {   
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return (int)returnInt;
    }
    else
    {
        sqlite3_reset(self.sqlStament);
        returnInt = 1;
        return (int)returnInt;
    }
    
}

-(void) loadSettingModel:(SettingModelV2 *)settingModel
{

    NSString *sqlString = [[NSString alloc] initWithFormat:@"select %@ ,%@ , %@ , %@ , %@ , %@, %@, %@, %@, %@, %@, %@, %@ from %@  ",
                           
                           @"rowid",
                           settings_lang, 
                           settings_sync, 
                           settings_last_sync, 
                           settings_notification_type,
                           settings_photo,
                           setting_log_enable,
                           settings_log_amount,
                           settings_ws_url,
                           settings_room_validate_enable,
                           settings_background_mode,
                           settings_heartbeat_server,
                           settings_heartbeat_port,
                           
                           SETTINGS
                           ];
    
    const char *sql=[sqlString UTF8String];

    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSInteger status;
    status = sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL);
    
    if(status != SQLITE_OK)
    {
        
    }
    
    status =  sqlite3_step(self.sqlStament);
    
    while (status == SQLITE_ROW) 
    {
        settingModel.row_Id=sqlite3_column_int(self.sqlStament, 0);
        
        if (sqlite3_column_text(self.sqlStament, 1)) {
            [settingModel setSettingsLang : [NSString stringWithUTF8String:(char *)sqlite3_column_text(self.sqlStament, 1)]];
        }  
        
        settingModel.settingsSynch = sqlite3_column_int(self.sqlStament, 2);
        
        if (sqlite3_column_text(self.sqlStament, 3)) {
            settingModel.settingsLastSync= [NSString stringWithUTF8String:(char *)sqlite3_column_text(self.sqlStament, 3)];
        }  
        
        
        settingModel.settingsNotificationType=sqlite3_column_int(self.sqlStament, 4);
        
        settingModel.settingsPhoto = sqlite3_column_int(self.sqlStament, 5);
        
        if (sqlite3_column_int(self.sqlStament, 6) == 1) {
            settingModel.settingEnableLog = YES;
        }
        else {
            settingModel.settingEnableLog = NO;
        }
        
        settingModel.settingsLogAmount = sqlite3_column_int(self.sqlStament, 7);
        
        if (sqlite3_column_text(self.sqlStament, 8)) {
            settingModel.settingsWSURL= [NSString stringWithUTF8String:(char *)sqlite3_column_text(self.sqlStament, 8)];
        }
        if (sqlite3_column_int(self.sqlStament, 9) == 1) {
            settingModel.settingEnableRoomValidate = YES;
        }
        else {
            settingModel.settingEnableRoomValidate = NO;
        }
        // CFG [20160927/CRF-00001432] - Get background mode settings from SQL lite.
        if (sqlite3_column_int(self.sqlStament, 10) == 1) {
            settingModel.settingsBackgroundMode = YES;
        }
        else {
            settingModel.settingsBackgroundMode = NO;
        }
        if (sqlite3_column_text(self.sqlStament, 11)) {
            settingModel.settingsHeartbeatServer= [NSString stringWithUTF8String:(char *)sqlite3_column_text(self.sqlStament, 11)];
        }
        if (sqlite3_column_text(self.sqlStament, 12)) {
            settingModel.settingsHeartbeatPort = [NSString stringWithUTF8String:(char *)sqlite3_column_text(self.sqlStament, 12)];
        }
        // CFG [20160927/CRF-00001432] - End.
        
        status = sqlite3_step(self.sqlStament);
    }
    
    if (status== SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    sqlite3_reset(self.sqlStament);
    
}
@end
