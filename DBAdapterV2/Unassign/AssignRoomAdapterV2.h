//
//  AssignRoomAdapterV2.h
//  mHouseKeeping
//
//  Created by TMS TRIBE on 5/4/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DatabaseAdapterV2.h"
#import "AssignRoomModelV2.h"

//@class AssignRoomModelV2;
@interface AssignRoomAdapterV2 : DatabaseAdapterV2{
    
}

-(int)insertAssignRoom:(AssignRoomModelV2*) assignModel ;
-(AssignRoomModelV2 *)loadAssignRoomById:(AssignRoomModelV2*)assignModel;
-(NSMutableArray *)loadAllAssignRoomByUserId:(NSInteger)userID;
-(int)updateAssignRoomData:(AssignRoomModelV2*) assignModel;
-(int)deleteAssignData:(AssignRoomModelV2*) assignModel;
-(NSInteger)numberOfAssignMustSyn:(NSInteger)userID;
-(BOOL)isExistInAssignRoom:(NSInteger)assignID;
@end
