
//
//  AssignRoomAdapterV2.m
//  mHouseKeeping
//
//  Created by TMS TRIBE on 5/4/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "AssignRoomAdapterV2.h"
#import "DbDefinesV2.h"
#import "ehkDefines.h"

@implementation AssignRoomAdapterV2

-(int)insertAssignRoom:(AssignRoomModelV2*) assignModel {

        NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"%@ %@ ( %@ ,%@, %@, %@, %@, %@ ,%@ ) Values( ? , ? , ? , ? , ? , ? , ?)", 
                                      @"insert into",
                                      ASSIGN_ROOM,
                                      ar_id,
                                      ar_assign_id,
                                      ar_housekeeper_id,
                                      ar_assign_date,
                                      ar_remark,
                                      ar_post_status,
                                      ar_supervisor_id
                                      ];
        
        const char *sql=[sqlString UTF8String];
        NSInteger reAccessCount = 0;
        RE_ACCESS_DB:{
            if (reAccessCount > 0) {
                [self close];
                usleep(0.1);
                [self openDatabase];
            }

            reAccessCount++;
        }

		if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
        {
        }
        
        sqlite3_bind_int (self.sqlStament, 1, (int)assignModel.assignRoomID);
        sqlite3_bind_int (self.sqlStament, 2, (int)assignModel.assignRoom_assign_id);
        sqlite3_bind_int (self.sqlStament, 3, (int)assignModel.assignRoom_HouseKeeper_Id);
        
        sqlite3_bind_text(self.sqlStament, 4, [assignModel.assignRoom_Assign_Date UTF8String], -1,SQLITE_TRANSIENT);
        sqlite3_bind_text(self.sqlStament, 5, [assignModel.assignRoom_Remark UTF8String], -1,SQLITE_TRANSIENT);
        
        sqlite3_bind_int (self.sqlStament, 6, (int)assignModel.assignRoom_post_status);
        sqlite3_bind_int (self.sqlStament, 7, (int)assignModel.assignRoom_supervisor_id);
        
        sqlite3_reset(self.sqlStament);
    
    NSInteger returnInt = 0;
    
    NSInteger status = sqlite3_step(self.sqlStament);
    
    if(SQLITE_DONE != status) {  
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return (int)returnInt;
    } else {
        sqlite3_reset(self.sqlStament);
        returnInt = 1;
        return (int)returnInt;
    }

}

-(AssignRoomModelV2 *)loadAssignRoomById:(AssignRoomModelV2*)assignModel{
    NSMutableString *sqlString;
    sqlString = [[NSMutableString alloc] initWithFormat:@"select %@, %@, %@, %@, %@, %@ from %@ where %@=? ",
                 ar_assign_id,
                 ar_housekeeper_id,
                 ar_assign_date,
                 ar_remark,
                 ar_post_status,
                 ar_supervisor_id,
                 ASSIGN_ROOM,
                 ar_id];
    
    const char *sql =[sqlString UTF8String];    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }

        reAccessCount++;
    }

    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
   
    }
    
    sqlite3_bind_int(self.sqlStament, 1, (int)assignModel.assignRoom_assign_id);
    
    NSInteger status = sqlite3_step(self.sqlStament);
    while(status == SQLITE_ROW) 
    {
        
        
        assignModel.assignRoom_assign_id = sqlite3_column_int(sqlStament, 0);
        assignModel.assignRoom_HouseKeeper_Id = sqlite3_column_int(sqlStament, 1);
        
        if (sqlite3_column_text(self.sqlStament, 2) != nil) {
            assignModel.assignRoom_Assign_Date = [NSString stringWithUTF8String:
                                                  (char *)sqlite3_column_text(sqlStament, 2)];
        }
        
        if (sqlite3_column_text(self.sqlStament, 3) != nil) {
            assignModel.assignRoom_Remark = [NSString stringWithUTF8String:
                                             (char *)sqlite3_column_text(sqlStament, 3)];
        }
        
        assignModel.assignRoom_post_status = sqlite3_column_int(sqlStament, 4);
        
        assignModel.assignRoom_supervisor_id = sqlite3_column_int(sqlStament, 5);
        
        status = sqlite3_step(self.sqlStament);
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }

    return assignModel;
}


-(NSMutableArray *)loadAllAssignRoomByUserId:(NSInteger)userID{
    NSMutableString *sqlString;
    sqlString = [[NSMutableString alloc] initWithFormat:@"select  %@ ,%@, %@, %@, %@, %@ from %@ where %@=? ",
                 ar_assign_id,
                 ar_housekeeper_id,
                 ar_assign_date,
                 ar_remark,
                 ar_post_status,
                 ar_supervisor_id,
                 ASSIGN_ROOM,
                 ar_supervisor_id];
    
    const char *sql =[sqlString UTF8String];    
        NSInteger reAccessCount = 0;
        RE_ACCESS_DB:{
            if (reAccessCount > 0) {
                [self close];
                usleep(0.1);
                [self openDatabase];
            }

            reAccessCount++;
        }

    NSMutableArray *result = [NSMutableArray array];
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
     }
    
    sqlite3_bind_int(self.sqlStament, 1, (int)userID);
    
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW) 
    {
        
        AssignRoomModelV2 *assignModel = [[AssignRoomModelV2 alloc] init];
        assignModel.assignRoom_assign_id = sqlite3_column_int(sqlStament, 0);
        assignModel.assignRoom_HouseKeeper_Id = sqlite3_column_int(sqlStament, 1);
        
        if (sqlite3_column_text(self.sqlStament, 2) != nil) {
            assignModel.assignRoom_Assign_Date = [NSString stringWithUTF8String:
                                                  (char *)sqlite3_column_text(sqlStament, 2)];
        }
        
        if (sqlite3_column_text(self.sqlStament, 3) != nil) {
            assignModel.assignRoom_Remark = [NSString stringWithUTF8String:
                                             (char *)sqlite3_column_text(sqlStament, 3)];
        }
        
        assignModel.assignRoom_post_status = sqlite3_column_int(sqlStament, 4);
        
        assignModel.assignRoom_supervisor_id = sqlite3_column_int(sqlStament, 5);
        
        [result addObject:assignModel];
        status = sqlite3_step(sqlStament);
    }
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {      
            [result removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    

    return result;
}



-(NSInteger)numberOfAssignMustSyn:(NSInteger)userID{
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT COUNT(*) FROM %@ WHERE %@ = ?", 
                           ASSIGN_ROOM,
                           ar_supervisor_id];
    
    const char *sql=[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}

    NSInteger result = 0;
        if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
            
            sqlite3_bind_int (self.sqlStament, 1, (int)userID);
            NSInteger status = sqlite3_step(sqlStament);
            while(status == SQLITE_ROW) {
                
                result += sqlite3_column_int(sqlStament, 0);
                
                status = sqlite3_step(sqlStament);
            }
            
            if (status == SQLITE_BUSY) {
                if (reAccessCount <= REACCESS_MAX_TIMES) {      
                    goto RE_ACCESS_DB;
                }
            }

        }
        
    sqlite3_bind_int (self.sqlStament, 1, (int)userID);
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW) {
        
        result += sqlite3_column_int(sqlStament, 0);
        status = sqlite3_step(sqlStament);
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {      
            goto RE_ACCESS_DB;
        }
    }
    

    
    return result;
}


-(int)updateAssignRoomData:(AssignRoomModelV2*) assignModel{
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"UPDATE %@ SET %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?,%@ = ? WHERE %@ = ?",
                           ASSIGN_ROOM,                           
                           ar_assign_id,
                           ar_housekeeper_id,
                           ar_assign_date,
                           ar_remark,
                           ar_post_status,
                           ar_supervisor_id,
                           ar_id];
    
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}

    NSInteger status1=sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status1 != SQLITE_OK) {    }
    
    sqlite3_bind_int(self.sqlStament,1, (int)assignModel.assignRoom_assign_id );
    sqlite3_bind_int(self.sqlStament,2, (int)assignModel.assignRoom_HouseKeeper_Id );
    sqlite3_bind_text(self.sqlStament,3, [assignModel.assignRoom_Assign_Date UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament,4, [assignModel.assignRoom_Remark UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_int(self.sqlStament,5, (int)assignModel.assignRoom_post_status );
    sqlite3_bind_int(self.sqlStament,6, (int)assignModel.assignRoom_supervisor_id );
    
    sqlite3_bind_int(self.sqlStament,7, (int)assignModel.assignRoom_assign_id );
    
    NSInteger returnInt = 0;    
    NSInteger status = sqlite3_step(self.sqlStament);
    if(SQLITE_DONE != status) {   
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return (int)returnInt;
    } else { 
        returnInt = 1;        
        return (int)returnInt;
    }
}

-(int)deleteAssignData:(AssignRoomModelV2*) assignModel {
    NSInteger result = 1;
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"DELETE FROM %@ WHERE %@ = ?", ASSIGN_ROOM, ar_assign_id];
    
    const char *sql = [sqlString UTF8String];
    NSInteger reAccessCount = 0;
        RE_ACCESS_DB:{
            if (reAccessCount > 0) {
                [self close];
                usleep(0.1);
                [self openDatabase];
            }

            reAccessCount++;
        }

    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
        result = 0;
    
    sqlite3_bind_int(self.sqlStament, 1, (int)assignModel.assignRoom_assign_id);
	
    NSInteger status = sqlite3_step(sqlStament);
    
	if (SQLITE_DONE != status) {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
		result = 0;
    }
    
    return (int)result;
}

-(BOOL)isExistInAssignRoom:(NSInteger)assignID{
    NSInteger result = 1;
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@" Select COUNT(*) FROM %@ WHERE %@ = ?", 
                           ASSIGN_ROOM, 
                           ar_assign_id];
    
    const char *sql = [sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}

    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
        result = 0;
    
    sqlite3_bind_int(self.sqlStament, 1, (int)assignID);
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW) {
        
        result = sqlite3_column_int(sqlStament, 0);
        status = sqlite3_step(sqlStament);
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {      
            goto RE_ACCESS_DB;
        }
    }

    
    return result;
   
}
@end
