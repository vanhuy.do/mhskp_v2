//
//  UnassignroomAdapter.h
//  mHouseKeeping
//
//  Created by TMS TRIBE on 5/4/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DatabaseAdapterV2.h"
#import "UnassignModelV2.h"
#import "ZoneModelV2.h"

//@class UnassignModelV2;
@interface UnassignroomAdapterV2 : DatabaseAdapterV2{
    
}
-(int)insertUnassignRoom:(UnassignModelV2*) unassignModel ;
-(UnassignModelV2 *)loadUnassignRoomById:(UnassignModelV2*)unassignModel;
-(NSMutableArray *)loadUnassignRoomByFloorId:(UnassignModelV2*)unassignModel;
-(NSMutableArray *)loadUnassignRoomByZoneId:(ZoneModelV2*)unassignModel;
-(NSMutableArray *)loadUnassignRoomByZoneIdOrderByName:(ZoneModelV2*)unassignModel;
-(NSInteger)loadFloorIDByZoneId:(ZoneModelV2*)zoneItem;
-(int)deleteUnAssignData:(UnassignModelV2*) unAssignModel;
-(int)deleteUnAssignRoomByRoomId:(NSString*)roomNumber;
@end
