//
//  UnassignroomAdapter.m
//  mHouseKeeping
//
//  Created by TMS TRIBE on 5/4/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "UnassignroomAdapterV2.h"
#import "DbDefinesV2.h"
#import "ehkDefines.h"
#import "LogFileManager.h"

@implementation UnassignroomAdapterV2
-(int)insertUnassignRoom:(UnassignModelV2*) unassignModel {
    
        NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"%@ %@ ( %@, %@, %@, %@, %@ ,%@ ,%@,%@,%@,%@,%@) Values( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)", 
                                      @"INSERT into",
                                      UNASSIGN_ROOM,
                                      ur_id,
                                      ur_floor_id,
                                      ur_zone_id,
                                      ur_hotel_id,
                                      ur_room_status_id,
                                      ur_cleaning_status_id,
                                      ur_guest_first_name,
                                      ur_guest_last_name,
                                      ur_room_kind,
                                      ur_room_id,
                                      ur_last_modified
                                      ];
        
        const char *sql=[sqlString UTF8String];
        NSInteger reAccessCount = 0;
        RE_ACCESS_DB:{
            if (reAccessCount > 0) {
                [self close];
                usleep(0.1);
                [self openDatabase];
            }

            reAccessCount++;
        }

        //NSLog(@"sqlString %@",sqlString);
        //        [sqlString release];
		if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
        {
        }
        
        sqlite3_bind_int (self.sqlStament, 1, (int)unassignModel.unassignroom_id);
        sqlite3_bind_int (self.sqlStament, 2, (int)unassignModel.unassignroom_floor_id);
        sqlite3_bind_int (self.sqlStament, 3, (int)unassignModel.unassignroom_zone_id);
        sqlite3_bind_int (self.sqlStament, 4, (int)unassignModel.unassignroom_hotel_id);
        sqlite3_bind_int (self.sqlStament, 5, (int)unassignModel.unassignroom_room_status_id);
        sqlite3_bind_int (self.sqlStament, 6, (int)unassignModel.unassignroom_cleaning_status_id);
        
        sqlite3_bind_text(self.sqlStament, 7, [unassignModel.unassignroom_guest_first_name UTF8String], -1,SQLITE_TRANSIENT);
        sqlite3_bind_text(self.sqlStament, 8, [unassignModel.unassignroom_guest_last_name UTF8String], -1,SQLITE_TRANSIENT);
        
        sqlite3_bind_int (self.sqlStament, 9, (int)unassignModel.unassignroom_room_kind);
        sqlite3_bind_text(self.sqlStament, 10 , [unassignModel.unassignroom_room_id UTF8String], -1,SQLITE_TRANSIENT);
        sqlite3_bind_text(self.sqlStament, 11, [unassignModel.unassignroom_last_modified UTF8String], -1,SQLITE_TRANSIENT);
        
        
        sqlite3_reset(self.sqlStament);
    
    NSInteger returnInt = 0;
    
    NSInteger status = sqlite3_step(self.sqlStament);
    
    if(SQLITE_DONE != status) {  
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return (int)returnInt;
    } else {
        sqlite3_reset(self.sqlStament);
        returnInt = 1;
        return (int)returnInt;
    }
}

-(UnassignModelV2 *)loadUnassignRoomById:(ZoneModelV2*)zoneItem{   
       NSMutableString *sqlString;
    sqlString = [[NSMutableString alloc] initWithFormat:@"select  %@, %@, %@, %@, %@, %@,  %@, %@ , %@,  %@, %@ from %@ where %@=? ",
                 ur_id,
                 ur_floor_id,
                 ur_zone_id,
                 ur_hotel_id,
                 ur_room_status_id,
                 ur_cleaning_status_id,
                 ur_guest_first_name,
                 ur_guest_last_name,
                 ur_room_kind,
                 ur_room_id,
                 ur_last_modified,
                 
                 UNASSIGN_ROOM,
                 ur_room_id];
    
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }

        reAccessCount++;
    }

    UnassignModelV2 *unassignInfo = nil;
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    
    sqlite3_bind_text(self.sqlStament, 1 , [unassignInfo.unassignroom_room_id UTF8String], -1,SQLITE_TRANSIENT);
    
    NSInteger status = sqlite3_step(self.sqlStament);
    while(status == SQLITE_ROW) 
    {
        unassignInfo = [[UnassignModelV2 alloc] init];
        
        unassignInfo.unassignroom_id = sqlite3_column_int(sqlStament, 0);
        unassignInfo.unassignroom_floor_id = sqlite3_column_int(sqlStament, 1);
        unassignInfo.unassignroom_zone_id = sqlite3_column_int(sqlStament, 2);
        unassignInfo.unassignroom_hotel_id = sqlite3_column_int(sqlStament, 3);
        unassignInfo.unassignroom_room_status_id = sqlite3_column_int(sqlStament, 4);
        unassignInfo.unassignroom_cleaning_status_id = sqlite3_column_int(sqlStament, 5);
        
        if (sqlite3_column_text(self.sqlStament, 6) != nil) {
            unassignInfo.unassignroom_guest_first_name=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 6)];
        }
        
        if (sqlite3_column_text(self.sqlStament, 7) != nil) {
            unassignInfo.unassignroom_guest_last_name=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 7)];
        }
        
        unassignInfo.unassignroom_room_kind = sqlite3_column_int(sqlStament, 8);
        if (sqlite3_column_text(self.sqlStament, 9)) {
            unassignInfo.unassignroom_room_id = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 9)];
        }
        
        if (sqlite3_column_text(self.sqlStament, 10) != nil) {
            unassignInfo.unassignroom_last_modified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 10)];
        }           
        
        status = sqlite3_step(self.sqlStament);
    }
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }

    
    
    return unassignInfo;
}

-(NSMutableArray *)loadUnassignRoomByFloorId:(UnassignModelV2*)unassignModel{   
    NSMutableString *sqlString;
    NSMutableArray *result = [NSMutableArray array];
    sqlString = [[NSMutableString alloc] initWithFormat:@"select  %@, %@, %@, %@, %@, %@,  %@, %@ , %@,  %@, %@ from %@ where %@=? ",
                 ur_id,
                 ur_floor_id,
                 ur_zone_id,
                 ur_hotel_id,
                 ur_room_status_id,
                 ur_cleaning_status_id,
                 ur_guest_first_name,
                 ur_guest_last_name,
                 ur_room_kind,
                 ur_room_id,
                 ur_last_modified,
                 
                 UNASSIGN_ROOM,
                 ur_floor_id];
    
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }

        reAccessCount++;
    }

    UnassignModelV2 *unassignInfo = nil;
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {

    }
    
    sqlite3_bind_int(self.sqlStament, 1, (int)unassignInfo.unassignroom_floor_id);
    
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW) 
    {
        unassignInfo = [[UnassignModelV2 alloc] init];
        
        unassignInfo.unassignroom_id = sqlite3_column_int(sqlStament, 0);
        unassignInfo.unassignroom_floor_id = sqlite3_column_int(sqlStament, 1);
        unassignInfo.unassignroom_zone_id = sqlite3_column_int(sqlStament, 2);
        unassignInfo.unassignroom_hotel_id = sqlite3_column_int(sqlStament, 3);
        unassignInfo.unassignroom_room_status_id = sqlite3_column_int(sqlStament, 4);
        unassignInfo.unassignroom_cleaning_status_id = sqlite3_column_int(sqlStament, 5);
        
        if (sqlite3_column_text(self.sqlStament, 6) != nil) {
            unassignInfo.unassignroom_guest_first_name=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 6)];
        }
        
        if (sqlite3_column_text(self.sqlStament, 7) != nil) {
            unassignInfo.unassignroom_guest_last_name=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 7)];
        }
        
        unassignInfo.unassignroom_room_kind = sqlite3_column_int(sqlStament, 8);
        if (sqlite3_column_text(self.sqlStament, 9)) {
            unassignInfo.unassignroom_room_id = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 9)];
        }
        if (sqlite3_column_text(self.sqlStament, 10) != nil) {
            unassignInfo.unassignroom_last_modified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 10)];
        }           
        
        
        [result addObject:unassignInfo];   
        status = sqlite3_step(sqlStament);
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {      
            [result removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }

    return result;
}

-(NSMutableArray *)loadUnassignRoomByZoneIdOrderByName:(ZoneModelV2*)zoneItem{
    NSMutableString *sqlString;
    NSMutableArray *result = [NSMutableArray array];
    sqlString = [[NSMutableString alloc] initWithFormat:@"select distinct %@, %@, %@, %@, %@, %@,  %@, %@ , %@,  %@, %@ from %@ where %@=? order by %@",
                 ur_id,
                 ur_floor_id,
                 ur_zone_id,
                 ur_hotel_id,
                 ur_room_status_id,
                 ur_cleaning_status_id,
                 ur_guest_first_name,
                 ur_guest_last_name,
                 ur_room_kind,
                 ur_room_id,
                 ur_last_modified,
                 
                 UNASSIGN_ROOM,
                 ur_zone_id,
                 ur_guest_first_name];
    
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    UnassignModelV2 *unassignInfo = nil;
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {
        
    }
    
    sqlite3_bind_int(self.sqlStament, 1, (int)zoneItem.zone_id);
    
    NSInteger status = sqlite3_step(sqlStament);
    
    while(status == SQLITE_ROW)
    {
        unassignInfo = [[UnassignModelV2 alloc] init];
        
        unassignInfo.unassignroom_id = sqlite3_column_int(sqlStament, 0);
        unassignInfo.unassignroom_floor_id = sqlite3_column_int(sqlStament, 1);
        unassignInfo.unassignroom_zone_id = sqlite3_column_int(sqlStament, 2);
        unassignInfo.unassignroom_hotel_id = sqlite3_column_int(sqlStament, 3);
        unassignInfo.unassignroom_room_status_id = sqlite3_column_int(sqlStament, 4);
        unassignInfo.unassignroom_cleaning_status_id = sqlite3_column_int(sqlStament, 5);
        
        if (sqlite3_column_text(self.sqlStament, 6) != nil) {
            unassignInfo.unassignroom_guest_first_name=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 6)];
        }
        
        if (sqlite3_column_text(self.sqlStament, 7) != nil) {
            unassignInfo.unassignroom_guest_last_name=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 7)];
        }
        
        unassignInfo.unassignroom_room_kind = sqlite3_column_int(sqlStament, 8);
        if (sqlite3_column_text(self.sqlStament, 9)) {
            unassignInfo.unassignroom_room_id = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 9)];
        }
        if (sqlite3_column_text(self.sqlStament, 10) != nil) {
            unassignInfo.unassignroom_last_modified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 10)];
        }
        
        
        if(![[UnassignManagerV2 sharedUnassignManager] isExistInAssignRoom:unassignInfo.unassignroom_id]){
            [result addObject:unassignInfo];
        } else{
            if([LogFileManager isLogConsole]) {
                NSLog(@"assigned");
            }
        }
        
        status = sqlite3_step(sqlStament);
        
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            [result removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    return result;
}


-(NSMutableArray *)loadUnassignRoomByZoneId:(ZoneModelV2*)zoneItem{
    NSMutableString *sqlString;
    NSMutableArray *result = [NSMutableArray array];
    sqlString = [[NSMutableString alloc] initWithFormat:@"select  %@, %@, %@, %@, %@, %@,  %@, %@ , %@,  %@, %@ from %@ where %@=? ",
                 ur_id,
                 ur_floor_id,
                 ur_zone_id,
                 ur_hotel_id,
                 ur_room_status_id,
                 ur_cleaning_status_id,
                 ur_guest_first_name,
                 ur_guest_last_name,
                 ur_room_kind,
                 ur_room_id,
                 ur_last_modified,
                 
                 UNASSIGN_ROOM,
                 ur_zone_id];
    
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }

        reAccessCount++;
    }

    UnassignModelV2 *unassignInfo = nil;
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {
 
    }
    
    sqlite3_bind_int(self.sqlStament, 1, (int)zoneItem.zone_id);
    
    NSInteger status = sqlite3_step(sqlStament);
    
    while(status == SQLITE_ROW) 
    {
        unassignInfo = [[UnassignModelV2 alloc] init];
        
        unassignInfo.unassignroom_id = sqlite3_column_int(sqlStament, 0);
        unassignInfo.unassignroom_floor_id = sqlite3_column_int(sqlStament, 1);
        unassignInfo.unassignroom_zone_id = sqlite3_column_int(sqlStament, 2);
        unassignInfo.unassignroom_hotel_id = sqlite3_column_int(sqlStament, 3);
        unassignInfo.unassignroom_room_status_id = sqlite3_column_int(sqlStament, 4);
        unassignInfo.unassignroom_cleaning_status_id = sqlite3_column_int(sqlStament, 5);
        
        if (sqlite3_column_text(self.sqlStament, 6) != nil) {
            unassignInfo.unassignroom_guest_first_name=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 6)];
        }
        
        if (sqlite3_column_text(self.sqlStament, 7) != nil) {
            unassignInfo.unassignroom_guest_last_name=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 7)];
        }
        
        unassignInfo.unassignroom_room_kind = sqlite3_column_int(sqlStament, 8);
        if (sqlite3_column_text(self.sqlStament, 9)) {
            unassignInfo.unassignroom_room_id = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 9)];
        }
        if (sqlite3_column_text(self.sqlStament, 10) != nil) {
            unassignInfo.unassignroom_last_modified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 10)];
        }           
        
        
        if(![[UnassignManagerV2 sharedUnassignManager] isExistInAssignRoom:unassignInfo.unassignroom_id]){
            [result addObject:unassignInfo];   
        } else{
            if([LogFileManager isLogConsole]) {
                NSLog(@"assigned");
            }
        }
        
        status = sqlite3_step(sqlStament);
        
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {      
            [result removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }

    return result;
}


-(NSInteger)loadFloorIDByZoneId:(ZoneModelV2*)zoneItem{   
    NSMutableString *sqlString;

    sqlString = [[NSMutableString alloc] initWithFormat:@"select  %@, %@, %@, %@, %@, %@,  %@, %@ , %@,  %@, %@ from %@ where %@=? ",
                 ur_id,
                 ur_floor_id,
                 ur_zone_id,
                 ur_hotel_id,
                 ur_room_status_id,
                 ur_cleaning_status_id,
                 ur_guest_first_name,
                 ur_guest_last_name,
                 ur_room_kind,
                 ur_room_id,
                 ur_last_modified,
                 
                 UNASSIGN_ROOM,
                 ur_zone_id];
    
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }

        reAccessCount++;
    }

    UnassignModelV2 *unassignInfo = nil;
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {

    }
    
    sqlite3_bind_int(self.sqlStament, 1, (int)zoneItem.zone_id);
    
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW) 
    {
        unassignInfo = [[UnassignModelV2 alloc] init];
        
        unassignInfo.unassignroom_id = sqlite3_column_int(sqlStament, 0);
        unassignInfo.unassignroom_floor_id = sqlite3_column_int(sqlStament, 1);
        unassignInfo.unassignroom_zone_id = sqlite3_column_int(sqlStament, 2);
        unassignInfo.unassignroom_hotel_id = sqlite3_column_int(sqlStament, 3);
        unassignInfo.unassignroom_room_status_id = sqlite3_column_int(sqlStament, 4);
        unassignInfo.unassignroom_cleaning_status_id = sqlite3_column_int(sqlStament, 5);
        
        if (sqlite3_column_text(self.sqlStament, 6) != nil) {
            unassignInfo.unassignroom_guest_first_name=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 6)];
        }
        
        if (sqlite3_column_text(self.sqlStament, 7) != nil) {
            unassignInfo.unassignroom_guest_last_name=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 7)];
        }
        
        unassignInfo.unassignroom_room_kind = sqlite3_column_int(sqlStament, 8);
        if (sqlite3_column_text(self.sqlStament, 9)) {
            unassignInfo.unassignroom_room_id = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 9)];
        }
        if (sqlite3_column_text(self.sqlStament, 10) != nil) {
            unassignInfo.unassignroom_last_modified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 10)];
        }           
        
        status = sqlite3_step(sqlStament); 
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {      
            goto RE_ACCESS_DB;
        }
    }

    
    return unassignInfo.unassignroom_floor_id;
}

-(int)deleteUnAssignData:(UnassignModelV2*) unAssignModel{
    NSInteger result = 1;
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"DELETE FROM %@ WHERE %@ = ?", UNASSIGN_ROOM, ur_id];
    
    const char *sql = [sqlString UTF8String];
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }

        reAccessCount++;
    }

    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
        result = 0;
    
    sqlite3_bind_int(self.sqlStament, 1, (int)unAssignModel.unassignroom_id);
	
    NSInteger status = sqlite3_step(sqlStament);
    
	if (SQLITE_DONE != status) {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
		result = 0;
    }
    
    return (int)result;
}

//Delete Unassigned room by Room Number
-(int)deleteUnAssignRoomByRoomId:(NSString*)roomNumber{
    NSInteger result = 1;
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"DELETE FROM %@ WHERE %@ = ?", UNASSIGN_ROOM, ur_room_id];
    
    const char *sql = [sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
        result = 0;
    
    sqlite3_bind_text(self.sqlStament, 1 , [roomNumber UTF8String], -1,SQLITE_TRANSIENT);
	
    NSInteger status = sqlite3_step(sqlStament);
    
	if (SQLITE_DONE != status) {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
		result = 0;
    }
    
    return (int)result;
}

@end
