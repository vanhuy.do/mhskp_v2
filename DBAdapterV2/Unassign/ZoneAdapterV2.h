//
//  ZoneAdapterV2.h
//  mHouseKeeping
//
//  Created by TMS TRIBE on 3/12/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DatabaseAdapterV2.h"
#import "ZoneModelV2.h"

@interface ZoneAdapterV2 : DatabaseAdapterV2{
    
}
-(int)insertZone:(ZoneModelV2*) zoneModel;
-(int)updateZone:(ZoneModelV2*)zoneModel;
-(int)deleteZone:(ZoneModelV2*)zoneModelV2;

-(ZoneModelV2 *)loadzoneByZoneId:(ZoneModelV2*)zoneModel;
-( NSMutableArray*)loadzonesByFloorId:(ZoneModelV2*)zoneModel;
-(ZoneModelV2 *)getZoneById:(int)zoneId;
-(NSMutableArray *)getAllZone;
-(NSString*)getZoneLastModifiedDate;

@end
