//
//  ZoneAdapterV2.m
//  mHouseKeeping
//
//  Created by TMS TRIBE on 3/12/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "ZoneAdapterV2.h"
#import "ehkDefines.h"

@implementation ZoneAdapterV2

-(int)insertZone:(ZoneModelV2*) zoneModel{

        NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"%@ %@ ( %@, %@, %@, %@, %@) Values(?, ?,?,?,?)", 
                                      @"insert into",
                                      ZONE,
                                      zone_id,
                                      zone_name,
                                      zone_name_lang,
                                      zone_floor_id,
                                      zone_last_modified ];
        
        
        const char *sql=[sqlString UTF8String];
        NSInteger reAccessCount = 0;
        RE_ACCESS_DB:{
            if (reAccessCount > 0) {
                [self close];
                usleep(0.1);
                [self openDatabase];
            }

            reAccessCount++;
        }

//        [sqlString release];
		if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
        {
        }
        
        sqlite3_bind_int (self.sqlStament, 1, (int)zoneModel.zone_id);
        sqlite3_bind_text(self.sqlStament, 2, [zoneModel.zone_name UTF8String], -1,SQLITE_TRANSIENT);
        sqlite3_bind_text(self.sqlStament, 3, [zoneModel.zone_name_lang UTF8String], -1,SQLITE_TRANSIENT);
        sqlite3_bind_int (self.sqlStament, 4, (int)zoneModel.zone_floor_id);
        sqlite3_bind_text(self.sqlStament, 5, [zoneModel.zone_last_modified UTF8String], -1,SQLITE_TRANSIENT);
        
        
        sqlite3_reset(self.sqlStament);
    
    NSInteger returnInt = 0;
    
    NSInteger status = sqlite3_step(self.sqlStament);
    
    if(SQLITE_DONE != status) {  
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return (int)returnInt;
    } else {
        sqlite3_reset(self.sqlStament);
        returnInt = 1;
        return (int)returnInt;
    }
}

-(int)updateZone:(ZoneModelV2*)zoneModel{

        NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"UPDATE %@ SET %@=?, %@=?, %@=?, %@=?, %@=? WHERE %@ = %d",
                                      ZONE,
                                      zone_id,
                                      zone_name,
                                      zone_name_lang,
                                      zone_floor_id,
                                      zone_last_modified,
                                      
                                      zone_id,
                                      (int)zoneModel.zone_id
                                      ];
        
        
        
        const char *sql=[sqlString UTF8String];
        NSInteger reAccessCount = 0;
        RE_ACCESS_DB:{
            if (reAccessCount > 0) {
                [self close];
                usleep(0.1);
                [self openDatabase];
            }

            reAccessCount++;
        }

		if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
        {
        }
        
        sqlite3_bind_int (self.sqlStament, 1, (int)zoneModel.zone_id);
        sqlite3_bind_text(self.sqlStament, 2, [zoneModel.zone_name UTF8String], -1,SQLITE_TRANSIENT);
        sqlite3_bind_text(self.sqlStament, 3, [zoneModel.zone_name_lang UTF8String], -1,SQLITE_TRANSIENT);
        sqlite3_bind_int (self.sqlStament, 4, (int)zoneModel.zone_floor_id);
        sqlite3_bind_text(self.sqlStament, 5, [zoneModel.zone_last_modified UTF8String], -1,SQLITE_TRANSIENT);
        
        sqlite3_bind_int (self.sqlStament, 6, (int)zoneModel.zone_id);
        
    
    NSInteger returnInt = 0;    
    NSInteger status = sqlite3_step(self.sqlStament);
    if(SQLITE_DONE != status) {   
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return (int)returnInt;
    } else { 
        returnInt = 1;        
        return (int)returnInt;
    }

}

-(int)deleteZone:(ZoneModelV2*)zoneModelV2{
    NSInteger result=1;

        NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"delete from %@ where %@= ?",FLOOR,floor_id];
		const char *sql=[sqlString UTF8String];
        
        NSInteger reAccessCount = 0;
        RE_ACCESS_DB:{
            if (reAccessCount > 0) {
                [self close];
                usleep(0.1);
                [self openDatabase];
            }

            reAccessCount++;
        }

		if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) result=0;
	
	
    sqlite3_bind_int(self.sqlStament, 1,(int)zoneModelV2.zone_id);
	
    NSInteger status = sqlite3_step(sqlStament);
    
	if (SQLITE_DONE != status) {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
		result = 0;
    }
    
    return (int)result;

}

-(ZoneModelV2 *)loadzoneByZoneId:(ZoneModelV2*)zoneModel{
    NSMutableString *sqlString;
    sqlString = [[NSMutableString alloc] initWithFormat:@"select  %@, %@, %@, %@, %@ from %@ where %@=? ",
                 zone_id,
                 zone_name,
                 zone_name_lang,
                 zone_floor_id,
                 zone_last_modified,
                 
                 ZONE,
                 zone_id];

    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }

        reAccessCount++;
    }

    ZoneModelV2 *zoneInfo = nil;
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {

    }
    
    sqlite3_bind_int(self.sqlStament, 1, (int)zoneModel.zone_id);
    
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW) 
    {
        zoneInfo = [[ZoneModelV2 alloc] init];
        
        zoneInfo.zone_id = sqlite3_column_int(sqlStament, 0);
        
        if (sqlite3_column_text(self.sqlStament, 1) != nil) {
            zoneInfo.zone_name=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
        }
        
        if (sqlite3_column_text(self.sqlStament, 2) != nil) {
            zoneInfo.zone_name_lang=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
        }
        
        zoneInfo.zone_floor_id = sqlite3_column_int(sqlStament, 3);
        
        
        if (sqlite3_column_text(self.sqlStament, 4) != nil) {
            zoneInfo.zone_last_modified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
        }
        
        status = sqlite3_step(sqlStament);
    }
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }

    
    return zoneInfo;

}

-(ZoneModelV2 *)getZoneById:(int)zoneId{
    NSMutableString *sqlString;
    sqlString = [[NSMutableString alloc] initWithFormat:@"select  %@, %@, %@, %@, %@ from %@ where %@ =? ",
                 zone_id,
                 zone_name,
                 zone_name_lang,
                 zone_floor_id,
                 zone_last_modified,
                 
                 ZONE,
                 zone_id];
    
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    ZoneModelV2 *zoneInfo = nil;
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
        
    }
    
    sqlite3_bind_int(self.sqlStament, 1, zoneId);
    
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW)
    {
        zoneInfo = [[ZoneModelV2 alloc] init];
        
        zoneInfo.zone_id = sqlite3_column_int(sqlStament, 0);
        
        if (sqlite3_column_text(self.sqlStament, 1) != nil) {
            zoneInfo.zone_name=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
        }
        
        if (sqlite3_column_text(self.sqlStament, 2) != nil) {
            zoneInfo.zone_name_lang=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
        }
        
        zoneInfo.zone_floor_id = sqlite3_column_int(sqlStament, 3);
        
        
        if (sqlite3_column_text(self.sqlStament, 4) != nil) {
            zoneInfo.zone_last_modified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
        }
        
        status = sqlite3_step(sqlStament);
    }
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    return zoneInfo;
}

-(NSMutableArray *)getAllZone{
    NSMutableArray *resultZone=[[NSMutableArray alloc] init] ;
    NSMutableString *sqlString;
    sqlString = [[NSMutableString alloc] initWithFormat:@"select  %@, %@, %@, %@, %@ from %@",
                 zone_id,
                 zone_name,
                 zone_name_lang,
                 zone_floor_id,
                 zone_last_modified,
                 ZONE];
    
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    
    ZoneModelV2 *zoneInfo = nil;
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {
        
    }
    
    
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW)
    {
        zoneInfo = [[ZoneModelV2 alloc] init];
        
        zoneInfo.zone_id = sqlite3_column_int(sqlStament, 0);
        
        if (sqlite3_column_text(self.sqlStament, 1) != nil) {
            zoneInfo.zone_name=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
        }
        
        if (sqlite3_column_text(self.sqlStament, 2) != nil) {
            zoneInfo.zone_name_lang=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
        }
        
        zoneInfo.zone_floor_id = sqlite3_column_int(sqlStament, 3);
        
        
        if (sqlite3_column_text(self.sqlStament, 4) != nil) {
            zoneInfo.zone_last_modified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
        }
        
        
        [resultZone addObject:zoneInfo];
        
        status = sqlite3_step(sqlStament);
        
    }
    
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            [resultZone removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    
    return resultZone;
    
    
}

-(NSString*)getZoneLastModifiedDate
{
    NSMutableString * sqlString=[[NSMutableString alloc] initWithFormat:@"SELECT MAX(%@) FROM %@", zone_last_modified, ZONE];
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    reAccessCount ++;
}
    
    NSString *lastModified;
    
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        
        status = sqlite3_step(sqlStament);
        while(status == SQLITE_ROW) {
            
            if(sqlite3_column_text(sqlStament, 0)){
                lastModified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 0)];
            }
            return lastModified;
        }
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    return  lastModified;
}
-(NSMutableArray *)loadzonesByFloorId:(ZoneModelV2*)zoneModel{
    NSMutableArray *resultZone=[[NSMutableArray alloc] init] ;
    NSMutableString *sqlString;
    sqlString = [[NSMutableString alloc] initWithFormat:@"select  %@, %@, %@, %@, %@ from %@ a ,%@ b where a.%@ = b.%@ AND b.%@=? GROUP BY b.%@ ",
                 zone_id,
                 zone_name,
                 zone_name_lang,
                 zone_floor_id,
                 zone_last_modified,
                 
                 ZONE,UNASSIGN_ROOM,
                 
                 zone_id,
                 
                 ur_zone_id,
                 
                 ur_floor_id,
                 
                 ur_zone_id];
    
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }

        reAccessCount++;
    }

    
    ZoneModelV2 *zoneInfo = nil;
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {

    }
    
    sqlite3_bind_int(self.sqlStament, 1, (int)zoneModel.zone_floor_id);
    
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW) 
    {
        zoneInfo = [[ZoneModelV2 alloc] init];
        
        zoneInfo.zone_id = sqlite3_column_int(sqlStament, 0);
        
        if (sqlite3_column_text(self.sqlStament, 1) != nil) {
            zoneInfo.zone_name=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
        }
        
        if (sqlite3_column_text(self.sqlStament, 2) != nil) {
            zoneInfo.zone_name_lang=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
        }
        
        zoneInfo.zone_floor_id = sqlite3_column_int(sqlStament, 3);
        
        
        if (sqlite3_column_text(self.sqlStament, 4) != nil) {
            zoneInfo.zone_last_modified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
        }
        
        
        [resultZone addObject:zoneInfo];
        
        status = sqlite3_step(sqlStament);
        
    }
    
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {      
            [resultZone removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }

    
    return resultZone;
    

}


@end
