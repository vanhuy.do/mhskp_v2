//
//  UserAdapterV2.m
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
#import "UserAdapterV2.h"
#import "DbDefinesV2.h"
#import "ehkConvert.h"
#import "MD5Digest.h"
#import "ehkDefines.h"
#import "UserConfigModel.h"
#import "UserAdapterV2Demo.h"

@implementation UserAdapterV2

+ (UserAdapterV2*) createUserAdapter
{
    UserAdapterV2* sharedUserAdapterInstace;
    if(isDemoMode) {
        sharedUserAdapterInstace = [[UserAdapterV2Demo alloc] init];
    } else {
        sharedUserAdapterInstace = [[UserAdapterV2 alloc] init];
    }
    
    return sharedUserAdapterInstace;
}
-(int)insertUserData:(UserModelV2*) userData
{
    if(self.sqlStament == nil) {
        NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"%@ %@ (%@ ,%@ ,%@ ,%@ ,%@ ,%@ , %@ ,%@ , %@, %@, %@, %@, %@, %@) %@", 
                                      @"insert into",
                                      USER,
                                      user_id,
                                      user_name,
                                      user_password,
                                      user_role,
                                      user_fullname,
                                      user_language,
                                      user_title,
                                      user_hotel_id,
                                      user_department,
                                      user_department_id,
                                      user_supervisor_id,
                                      user_last_modified,
                                      allow_block_room,
                                      allow_reorder_room,
                                      @"Values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"];
        
        const char *sql=[sqlString UTF8String];
        
        NSInteger reAccessCount = 0;
        RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
            reAccessCount ++;
        }
       
		if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK){
        }
        
        sqlite3_bind_int (self.sqlStament, 1, userData.userId);
        sqlite3_bind_text(self.sqlStament, 2, [userData.userName UTF8String], -1, SQLITE_TRANSIENT);
//        sqlite3_bind_text(self.sqlStament, 3, [[MD5Digest md5HexDigest:[NSString stringWithFormat:@"%@", userData.userPassword]] UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(self.sqlStament, 3, [userData.userPassword UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(self.sqlStament, 4, [userData.userRole UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(self.sqlStament, 5, [userData.userFullName UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(self.sqlStament, 6, [userData.userLang UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(self.sqlStament, 7, [userData.userTitle UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_int (self.sqlStament, 8, userData.userHotelsId);
        
        sqlite3_bind_text(self.sqlStament, 9, [userData.userDepartment UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_int (self.sqlStament, 10, userData.userDepartmentId);
        sqlite3_bind_int (self.sqlStament, 11, userData.userSupervisorId);
        sqlite3_bind_text(self.sqlStament, 12, [userData.userLastModified UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_int(sqlStament, 13, (int)userData.userAllowBlockRoom);
        sqlite3_bind_int(sqlStament, 14, (int)userData.userAllowReorderRoom);
        
        NSInteger status =  sqlite3_step(self.sqlStament);
        if(SQLITE_DONE != status)
        {
            if (status == SQLITE_BUSY) {
                if (reAccessCount <= REACCESS_MAX_TIMES) {
                    goto RE_ACCESS_DB;
                }
            }
            return 0;
        }
        else
        {
            sqlite3_last_insert_rowid(database);
            
            return 1;
        }

    }
    return 0;
}

-(int)updateUserData:(UserModelV2*) userData
{
    if(self.sqlStament == nil) {
        
        NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"update %@(%@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@) %@",
                                      USER,
                                      user_id,
                                      user_name,
                                      user_password,
                                      user_role,
                                      user_fullname,
                                      user_language,
                                      user_title,
                                      user_department,
                                      user_department_id,
                                      user_supervisor_id,
                                      user_last_modified,
                                      user_hotel_id,
                                      allow_block_room,
                                      allow_reorder_room,
                                      @"Values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"/*,user_id,userData.userId*/];

        const char *sql=[sqlString UTF8String];

        NSInteger reAccessCount = 0;
        RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
            reAccessCount ++;
        }
        
		if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK){
        }
        
        sqlite3_bind_int(self.sqlStament, 1, userData.userId);
        sqlite3_bind_text(self.sqlStament, 2, [userData.userName UTF8String], -1, SQLITE_TRANSIENT);
//        sqlite3_bind_text(self.sqlStament, 3, [[MD5Digest md5HexDigest:userData.userPassword] UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(self.sqlStament, 3, [userData.userPassword UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(self.sqlStament, 4, [userData.userRole UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(self.sqlStament, 5, [userData.userFullName UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(self.sqlStament, 6, [userData.userLang UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(self.sqlStament, 7, [userData.userTitle UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(self.sqlStament, 8, [userData.userDepartment UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_int(self.sqlStament, 9, userData.userDepartmentId);
        sqlite3_bind_int(self.sqlStament, 10, userData.userSupervisorId);
        sqlite3_bind_text(self.sqlStament, 11, [userData.userLastModified UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_int(sqlStament, 12, userData.userHotelsId);
        sqlite3_bind_int(sqlStament, 13, (int)userData.userAllowBlockRoom);
        sqlite3_bind_int(sqlStament, 14, (int)userData.userAllowReorderRoom);
        
        NSInteger status = sqlite3_step(self.sqlStament);
        
        if(SQLITE_DONE != status){
            if (status == SQLITE_BUSY) {
                if (reAccessCount <= REACCESS_MAX_TIMES) {
                    goto RE_ACCESS_DB;
                }
            }
            return 0; // update fail -> return 0
        }
        else{
            return  1; // update ok -> return 1
        }
        
        sqlite3_reset(self.sqlStament);
    }
    return 0;
}

-(int)deleteUserData:(UserModelV2*) userData
{
    NSInteger result=1;
    if(self.sqlStament == nil) {
        NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"delete from %@ where %@= ?",USER,user_id];
		const char *sql=[sqlString UTF8String];

		if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
			result=0;
	}
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    sqlite3_bind_int(self.sqlStament, 1,userData.userId);
	
    NSInteger  status = sqlite3_step(self.sqlStament);
    
	if (SQLITE_DONE != status) {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
		result=0;
    }
    
    return (int)result;
}

-(NSString*)getUserLastModifiedDate
{
    NSMutableString * sqlString=[[NSMutableString alloc] initWithFormat:@"SELECT MAX(%@) FROM %@ ORDER BY %@ ASC", usr_last_modified, USER_LIST, usr_last_modified];
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    reAccessCount ++;
}
    
    NSString *lastModified;
    
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        
        status = sqlite3_step(sqlStament);
        while(status == SQLITE_ROW) {
            
            if(sqlite3_column_text(sqlStament, 0)){
                lastModified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 0)];
            }
            return lastModified;
        }
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    return  lastModified;
}

-(UserModelV2*)getUserByUserId:(int)userId
{
    
    UserModelV2* userData = nil;
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"%@ %@, %@, %@, %@, %@,%@ ,%@, %@, %@, %@, %@, %@, %@, %@ from %@ where %@=%d",
                                  @"select  ",
                                  user_id,
                                  user_name,
                                  user_password,
                                  user_role,
                                  user_fullname,
                                  user_language,
                                  user_title,
                                  user_hotel_id,
                                  user_department,
                                  user_department_id,
                                  user_supervisor_id,
                                  user_last_modified,
                                  allow_block_room,
                                  allow_reorder_room,
                                  USER,
                                  user_id,
                                  userId];
    
    const char *sql =[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    reAccessCount ++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {}
    
    NSInteger status =  sqlite3_step(self.sqlStament);
    while(status == SQLITE_ROW)
    {
        userData.userId = sqlite3_column_int(sqlStament, 0);
        
        if (sqlite3_column_text(sqlStament, 1)!=nil) {
            userData.userName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
        }
        
        if (sqlite3_column_text(sqlStament, 2)!=nil) {
            userData.userPassword=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
        }
        if (sqlite3_column_text(sqlStament, 3)!=nil) {
            userData.userRole= [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
        }
        
        if (sqlite3_column_text(sqlStament, 4)!=nil) {
            
            userData.userFullName= [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
        }
        
        
        if (sqlite3_column_text(sqlStament, 5)!=nil) {
            
            userData.userLang= [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
        }
        
        if (sqlite3_column_text(sqlStament, 6)!=nil) {
            userData.userTitle= [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 6)];
        }
        
        
        userData.userHotelsId=sqlite3_column_int(sqlStament, 7);
        
        if (sqlite3_column_text(sqlStament, 8)!=nil) {
            userData.userDepartment=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 8)];
        }
        
        userData.userDepartmentId=sqlite3_column_int(sqlStament, 9);
        
        userData.userSupervisorId=sqlite3_column_int(sqlStament, 10);
        
        if (sqlite3_column_text(sqlStament, 11)!=nil) {
            userData.userLastModified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 11)];
        }
        
        userData.userAllowBlockRoom = sqlite3_column_int(sqlStament, 12);
        userData.userAllowReorderRoom = sqlite3_column_int(sqlStament, 13);
        
        return userData;
        
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    return userData;
}


-(UserModelV2*)loadUserData:(UserModelV2*)userData
{
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"%@ %@, %@, %@, %@, %@,%@ ,%@, %@, %@, %@, %@, %@, %@, %@ from %@ where %@='%@'", 
                                  @"select  ",
                                  user_id,
                                  user_name,
                                  user_password,
                                  user_role,
                                  user_fullname,
                                  user_language,
                                  user_title,
                                  user_hotel_id,
                                  user_department,
                                  user_department_id,
                                  user_supervisor_id,
                                  user_last_modified,
                                  allow_block_room,
                                  allow_reorder_room,
                                  USER,
                                  user_name,
                                  userData.userName];
    
    const char *sql =[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {}
        
    NSInteger status =  sqlite3_step(self.sqlStament);
    while(status == SQLITE_ROW) 
    {
        userData.userId = sqlite3_column_int(sqlStament, 0);
        
        if (sqlite3_column_text(sqlStament, 1)!=nil) {
            userData.userName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
        }
        
        if (sqlite3_column_text(sqlStament, 2)!=nil) {
            userData.userPassword=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
        }
        if (sqlite3_column_text(sqlStament, 3)!=nil) {
            userData.userRole= [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
        }
        
        if (sqlite3_column_text(sqlStament, 4)!=nil) {
            
            userData.userFullName= [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
        }
        
        
        if (sqlite3_column_text(sqlStament, 5)!=nil) {
            
            userData.userLang= [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
        }
        
        if (sqlite3_column_text(sqlStament, 6)!=nil) {
            userData.userTitle= [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 6)];
        }
        
        
        userData.userHotelsId=sqlite3_column_int(sqlStament, 7);
        
        if (sqlite3_column_text(sqlStament, 8)!=nil) {
            userData.userDepartment=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 8)];
        }
        
        userData.userDepartmentId=sqlite3_column_int(sqlStament, 9);
        
        userData.userSupervisorId=sqlite3_column_int(sqlStament, 10);	
        
        if (sqlite3_column_text(sqlStament, 11)!=nil) {
            userData.userLastModified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 11)];
        }
        
        userData.userAllowBlockRoom = sqlite3_column_int(sqlStament, 12);
        userData.userAllowReorderRoom = sqlite3_column_int(sqlStament, 13);
        
        return userData;
        
    }
        
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }

    return userData;
}

-(userRoles*)getRole:(UserModelV2*) userData
{

    NSMutableString *sqlString=[[NSMutableString alloc] initWithFormat:@"%@ %@ from %@ where %@=?",@"select",user_role,USER,user_id];
    userRoles *result = nil;
    if(self.sqlStament == nil) {
        const char *sql=[sqlString UTF8String];
        
        NSInteger reAccessCount = 0;
        RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
            reAccessCount ++;
        }
        
		if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK){
        }
        
        sqlite3_bind_int(self.sqlStament, 1, userData.userId);
        
        NSInteger status =  sqlite3_step(self.sqlStament);
        
        while (status) {
            result=(userRoles*) sqlite3_column_text(self.sqlStament, 4);
            status = sqlite3_step(self.sqlStament);
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        
    }

    return result;
    
}
/*
 @return : true if user exist , false
 */
-(BOOL)checkExistUser:(UserModelV2*) userData
{
    NSInteger intResult=0;
    NSMutableString *sqlString=[[NSMutableString alloc] initWithFormat:@"%@  from %@ where %@=?",@"select count(*) ",USER,user_name];   
    
    
    if(self.sqlStament == nil) {
        const char *sql=[sqlString UTF8String];
        
        NSInteger reAccessCount = 0;
        RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
            reAccessCount ++;
        }
        
		if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK){
        }
        
        sqlite3_bind_text(self.sqlStament, 1, [userData.userName UTF8String], -1, SQLITE_TRANSIENT);
        
        NSInteger status =  sqlite3_step(self.sqlStament);
        while (status == SQLITE_ROW)
        {
            intResult=sqlite3_column_int(self.sqlStament, 0);
            return intResult;
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }
    
    if(intResult>0) 
        return YES;
    else return  NO;
}

-(BOOL)isAuthenticate:(UserModelV2 *)userData
{

    NSInteger intResult=0;
    NSMutableString *sqlString=[[NSMutableString alloc] initWithFormat:@"select count(*)  from %@ where (%@=?) and (%@=?)",
                                USER,
                                user_name,
                                user_password];   
    
    
    if(self.sqlStament == nil) 
    {
        const char *sql=[sqlString UTF8String];
        
        NSInteger reAccessCount = 0;
        RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
            reAccessCount ++;
        }
        
		if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
        {
        }
        
        sqlite3_bind_text(self.sqlStament, 1, [userData.userName UTF8String], -1, SQLITE_TRANSIENT);
        //sqlite3_bind_text(self.sqlStament, 2, [[MD5Digest md5HexDigest:userData.userPassword] UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(self.sqlStament, 2, [userData.userPassword UTF8String], -1, SQLITE_TRANSIENT);
        NSInteger status =  sqlite3_step(self.sqlStament);
        while (status == SQLITE_ROW) 
        {
            intResult=sqlite3_column_int(self.sqlStament, 0);
            return intResult;
        }
        
        if (status== SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto  RE_ACCESS_DB;
            }
        }
    }

    // check if have this user then return true.
    if(intResult>0) 
        return YES;
    else return  NO;
}

#pragma mark - User configurations

-(int)insertUserConfig:(UserConfigModel*) userConfig
{
    //Last modified inserted
    if(userConfig){
        userConfig.lastModified = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
    }
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"insert into %@ (%@ ,%@ ,%@ ,%@) %@",
                                  USER_CONFIGURATIONS,
                                  user_config_id,
                                  user_config_key,
                                  user_config_value,
                                  last_modified,
                                  @"Values(?, ?, ?, ?)"];
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    reAccessCount ++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK){
    }
    
    sqlite3_bind_int (self.sqlStament, 1, userConfig.userId);
    sqlite3_bind_text(self.sqlStament, 2, [userConfig.key UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 3, [userConfig.value UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 4, [userConfig.lastModified UTF8String], -1, SQLITE_TRANSIENT);
    
    NSInteger status =  sqlite3_step(self.sqlStament);
    if(SQLITE_DONE != status)
    {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return 0;
    }
    else
    {
        sqlite3_last_insert_rowid(database);
        
        return 1;
    }
}

-(int)updateUserConfigurationValue:(NSString*)configValue ByUserId:(int)userId andConfigKey:(NSString*)configKey
{
    NSString *sqlString;
    NSString *lastModified = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    sqlString = [[NSString alloc] initWithFormat:@"UPDATE %@ SET %@ = ?, %@ = ? WHERE %@ = ? AND %@ = ?", USER_CONFIGURATIONS, user_config_value, last_modified, user_config_id, user_config_key];
    
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    reAccessCount ++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK){
    }
    
    sqlite3_bind_text(self.sqlStament, 1, [configValue UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 2, [lastModified UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_int(self.sqlStament, 3, userId);
    sqlite3_bind_text(self.sqlStament, 4, [configKey UTF8String], -1, SQLITE_TRANSIENT);
    
    NSInteger status = sqlite3_step(self.sqlStament);
    
    if(SQLITE_DONE != status){
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return 0; // update fail -> return 0
    }
    else{
        return  1; // update ok -> return 1
    }
    
}

-(int)deleteUserConfigurationsByUserId:(int)userId AndKey:(NSString*)configKey
{
    NSInteger result=1;
    NSMutableString *sqlString = nil;
    
    if(configKey.length > 0 && userId > 0){
        sqlString = [[NSMutableString alloc] initWithFormat:@"delete from %@ where %@ = ? and %@ = ?",USER_CONFIGURATIONS,user_id, user_config_key];
    } else if(configKey.length <= 0 && userId > 0) {
        sqlString = [[NSMutableString alloc] initWithFormat:@"delete from %@ where %@ = ?",USER_CONFIGURATIONS,user_id];
    } else if(userId <= 0 && configKey.length > 0){
        sqlString = [[NSMutableString alloc] initWithFormat:@"delete from %@ where %@ = ?",USER_CONFIGURATIONS, user_config_key];
    } else {
        return 0;
    }
    
    const char *sql=[sqlString UTF8String];
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
        result=0;
	
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    reAccessCount ++;
}
    if(userId > 0){
        sqlite3_bind_int(self.sqlStament, 1,userId);
    }
    
    if(configKey.length > 0){
        sqlite3_bind_text(self.sqlStament, 2, [configKey UTF8String], -1, SQLITE_TRANSIENT);
    }
    
    NSInteger  status = sqlite3_step(self.sqlStament);
    
	if (SQLITE_DONE != status) {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
		result=0;
    }
    
    return (int)result;
}

-(UserConfigModel*)getUserConfigByUserId:(int)userId andKey:(NSString*) userConfigKey
{
    UserConfigModel* userConfigData = nil;
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"select %@, %@, %@, %@, %@ from %@ where %@ = %d and %@ = '%@'",
                                  row_id,
                                  user_config_id,
                                  user_config_key,
                                  user_config_value,
                                  last_modified,
                                  USER_CONFIGURATIONS,
                                  user_config_id,
                                  userId,
                                  user_config_key,
                                  userConfigKey];
    
    const char *sql =[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    reAccessCount ++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {}
    
    NSInteger status =  sqlite3_step(self.sqlStament);
    while(status == SQLITE_ROW)
    {
        userConfigData = [[UserConfigModel alloc] init];
        
        userConfigData.rowId = sqlite3_column_int(sqlStament, 0);
        userConfigData.userId = sqlite3_column_int(sqlStament, 1);
        
        if (sqlite3_column_text(sqlStament, 2)!=nil) {
            userConfigData.key = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
        }
        
        if (sqlite3_column_text(sqlStament, 3)!=nil) {
            userConfigData.value = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
        }
        if (sqlite3_column_text(sqlStament, 4)!=nil) {
            userConfigData.lastModified = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
        }
        break;
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    return userConfigData;
}

#pragma mark - Log User Information

-(int)insertUserLogData:(UserLogModelV2*) userLogData
{
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"insert into %@ (%@ ,%@ ,%@ ,%@) %@",
                                  USER_LOGIN_LOGS,
                                  log_user_id,
                                  log_user_name,
                                  log_user_last_login,
                                  log_user_last_logout,
                                  @"Values(?, ?, ?, ?)"];
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    reAccessCount ++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK){
    }
    
    sqlite3_bind_int (self.sqlStament, 1, userLogData.userId);
    sqlite3_bind_text(self.sqlStament, 2, [userLogData.userName UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 3, [userLogData.userLastLoginDate UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 4, [userLogData.userLastLogoutDate UTF8String], -1, SQLITE_TRANSIENT);
    
    NSInteger status =  sqlite3_step(self.sqlStament);
    if(SQLITE_DONE != status)
    {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return 0;
    }
    else
    {
        sqlite3_last_insert_rowid(database);
        
        return 1;
    }
}

-(int)updateUserLogData:(UserLogModelV2*) userLogData
{
    NSString *sqlString;
    NSString *dateValue;
    if(userLogData.userLastLoginDate)
    {
        sqlString = [[NSString alloc] initWithFormat:@"UPDATE %@ SET %@ = ? WHERE %@ = ?", USER_LOGIN_LOGS, log_user_last_login, log_user_id];
        dateValue = userLogData.userLastLoginDate;
    }
    else if(userLogData.userLastLogoutDate)
    {
        sqlString = [[NSString alloc] initWithFormat:@"UPDATE %@ SET %@ = ? WHERE %@ = ?", USER_LOGIN_LOGS, log_user_last_logout, log_user_id];
        dateValue = userLogData.userLastLogoutDate;
    }
    else //no have any to update
    {
        return 0;
    }
    
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    reAccessCount ++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK){
    }
    
    sqlite3_bind_text(self.sqlStament, 1, [dateValue UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_int(self.sqlStament, 2, userLogData.userId);
    
    NSInteger status = sqlite3_step(self.sqlStament);
    
    if(SQLITE_DONE != status){
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return 0; // update fail -> return 0
    }
    else{
        return  1; // update ok -> return 1
    }

}

-(UserLogModelV2*)getUserLogByUserId:(int)userId
{
    
    UserLogModelV2* userLogData = nil;
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"select %@, %@, %@, %@ from %@ where %@ = %d",
                                  log_user_id,
                                  log_user_name,
                                  log_user_last_login,
                                  log_user_last_logout,
                                  USER_LOGIN_LOGS,
                                  log_user_id,
                                  userId];
    
    const char *sql =[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    reAccessCount ++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {}
    
    NSInteger status =  sqlite3_step(self.sqlStament);
    while(status == SQLITE_ROW)
    {
        UserLogModelV2* userLogData = [[UserLogModelV2 alloc] init];
        userLogData.userId = sqlite3_column_int(sqlStament, 0);
        
        if (sqlite3_column_text(sqlStament, 1)!=nil) {
            userLogData.userName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
        }
        
        if (sqlite3_column_text(sqlStament, 2)!=nil) {
            userLogData.userLastLoginDate=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
        }
        if (sqlite3_column_text(sqlStament, 3)!=nil) {
            userLogData.userLastLogoutDate= [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
        }
        
        return userLogData;
        
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    return userLogData;
}

@end
