//
//  AlertTableViewViewController.h
//  mHouseKeeping
//
//  Created by vinhnguyen on 1/10/14.
//
//

#import <UIKit/UIKit.h>

@interface AlertTableViewViewController : UIView<UITableViewDelegate, UITableViewDataSource>
{
    IBOutlet UITableView *tableContent;
    IBOutlet UILabel *titleView;
    IBOutlet UIButton *btnClose;
    UIView *superViewValue;
    IBOutlet UIImageView *imgTop;
    IBOutlet UIImageView *imgMid;
    IBOutlet UIImageView *imgBott;
}

@property (nonatomic, retain) NSMutableArray *listContents;

//List of IDs need to sent to WS as read message pop up
@property (nonatomic, retain) NSString *listIdsUpdateWS;

-(id) initWithSuperView:(UIView*)superView;
-(void)show;

@end
