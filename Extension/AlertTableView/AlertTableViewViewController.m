//
//  AlertTableViewViewController.m
//  mHouseKeeping
//
//  Created by vinhnguyen on 1/10/14.
//
//

#import "AlertTableViewViewController.h"
#import "DeviceManager.h"

@interface AlertTableViewViewController ()

@end

@implementation AlertTableViewViewController

@synthesize listContents;
@synthesize listIdsUpdateWS;

//- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
//{
//    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
//    if (self) {
//        // Custom initialization
//    }
//    return self;
//}
- (void)viewDidLoad
{
    //[super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
//    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
//        //Clear redundant view for header, footter
//        CGRect frameHeaderFooter = tableContent.tableHeaderView.frame;
//        frameHeaderFooter.size.height = 0.1f;
//        UIView *headerView = [[UIView alloc] initWithFrame:frameHeaderFooter];
//        [tableContent setTableHeaderView:headerView];
//
//        frameHeaderFooter = tableContent.tableFooterView.frame;
//        frameHeaderFooter.size.height = 0.1f;
//        UIView *footerView = [[UIView alloc] initWithFrame:frameHeaderFooter];
//        [tableContent setTableFooterView:footerView];
//        
//        [self loadFlatResource];
//    }
    [titleView setText:[L_alert currentKeyToLanguage]];
    [btnClose setTitle:[L_CLOSE currentKeyToLanguage] forState:UIControlStateNormal];
}
//- (void)didReceiveMemoryWarning
//{
//    [super didReceiveMemoryWarning];
//    // Dispose of any resources that can be recreated.
//}

-(id) initWithSuperView:(UIView*)superView
{
    self = [super initWithFrame:superView.frame];
    [self setBackgroundColor:[UIColor clearColor]];
    if (self) {
        NSArray *nibViews = [[NSBundle mainBundle] loadNibNamed:@"AlertTableViewViewController" owner:self options:nil];
        UIView *alertTableView = [nibViews objectAtIndex:0];
        [alertTableView setFrame:superView.frame];
        [self addSubview:alertTableView];
        superViewValue = superView;
        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
        {
            [self loadFlatResource];
        }
    }
    
    return self;
}

#pragma mark - TableView Delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return listContents.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *alertContent = [listContents objectAtIndex:indexPath.row];
    return [self calculateSize:alertContent] + 10;
}
- (CGFloat)calculateSize:(NSString*)text{
    CGSize maximumSize = CGSizeMake(tableContent.bounds.size.width - 100, CGFLOAT_MAX);
    NSString *myString = text;
    UIFont *myFont = [UIFont fontWithName:@"Helvetica-Bold" size:18.0f];
    CGSize myStringSize = [myString sizeWithFont:myFont
                               constrainedToSize:maximumSize
                                   lineBreakMode:NSLineBreakByWordWrapping];
    
    return myStringSize.height;
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *alertTableCellIdentifier = @"AlertTableIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:alertTableCellIdentifier];
    if(!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:alertTableCellIdentifier];
        
        UIImage *imageWarning = [self imageWithImage:[UIImage imageNamed:@"!.png"] scaledToSize:CGSizeMake(23.0f, 25.0f)];
        //Image
        [cell.imageView setImage:imageWarning];
        
        [cell.textLabel setTextColor:[UIColor whiteColor]];
        [cell.textLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:18.0f]];
        cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
        cell.textLabel.numberOfLines = 0;
        [cell.textLabel sizeToFit];
        [cell setBackgroundColor:[UIColor clearColor]];
        [cell setUserInteractionEnabled:NO];
        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
        {
            cell.textLabel.textColor = [UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.5f];
        }
    }
    
    NSString *alertContent = [listContents objectAtIndex:indexPath.row];
    [cell.textLabel setText:alertContent];
    
    return cell;
}

#pragma mark - User Interact

-(IBAction)btnCloseTouched:(id)sender
{
    //Update as read Message Pop Up to WS
    if(listIdsUpdateWS.length > 0) {
        [self performSelector:@selector(updateReadPopUpMessage) withObject:nil afterDelay:0.0f];
    } else {
        [self removeFromSuperview];
    }
}

-(void) updateReadPopUpMessage
{
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.superview];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getLoadingData]];
    [self.superview addSubview:HUD];
    [HUD show:YES];
    [[HomeViewV2 shareHomeView] waitingLoadingData];
    [[RoomManagerV2 sharedRoomManager] updateReadMessagePopUpByUserId:[UserManagerV2 sharedUserManager].currentUser.userId listID:listIdsUpdateWS];
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
    [self performSelector:@selector(hiddenHUDAfterSaved:) withObject:HUD afterDelay:0.0f];
}

-(void) hiddenHUDAfterSaved:(MBProgressHUD *)HUD{
    [HUD hide:YES];
    [HUD removeFromSuperview];
    [self removeFromSuperview];
}

-(void)show
{
    [superViewValue addSubview:self];
    self.alpha = 0.3f;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    self.alpha = 1.0f;
    [UIView commitAnimations];
    [tableContent reloadData];
}

//MARK: Private function
- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize
{
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}
-(void)loadFlatResource
{
    [imgTop setImage:[UIImage imageNamed:imgBGPopupTopFlat]];
    [imgMid setImage:[UIImage imageNamed:imgBGPopupMidFlat]];
    [imgBott setImage:[UIImage imageNamed:imgBgPopupBottomFlat]];
    [titleView setTextColor:[UIColor darkGrayColor]];
    [btnClose setBackgroundImage:[UIImage imageNamed:imgBtnSelectionPopupFlat] forState:UIControlStateNormal];
    [btnClose setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}
@end
