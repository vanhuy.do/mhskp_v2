//
//  STEncryptorDES.m
//  
//
//  Created by Nimit S. Parekh on 27/01/14.
//
//

#import "STEncryptorDES.h"
#import "NSData+Base64.h"
#import "UserManagerV2.h"

@implementation STEncryptorDES
#pragma mark -
#pragma mark Initialization and deallcation


#pragma mark -
#pragma mark Praivate


#pragma mark -
#pragma mark API

+ (NSString*)encryptData:(NSString*)valueData key:(NSString*)key iv:(NSString*)iv;
{
    if (key == nil || iv == nil || valueData == nil) {
        return valueData;
    }
    
    NSData *data = [valueData dataUsingEncoding:NSUTF8StringEncoding];
    NSData* result = nil;
    
    // setup output buffer
	size_t bufferSize = [data length] + FBENCRYPT_BLOCK_SIZE;
	void *buffer = malloc(bufferSize);
    
    
    NSData *btCryptKey = [[NSData alloc] initWithBase64EncodedString:key options:0];
    NSData *btInitializationVector = [[NSData alloc] initWithBase64EncodedString:iv options:0];

    
    // do encrypt
	size_t encryptedSize = 0;
	CCCryptorStatus cryptStatus = CCCrypt(kCCEncrypt,
                                          FBENCRYPT_ALGORITHM,
                                          kCCOptionPKCS7Padding,
                                          btCryptKey.bytes,
                                          FBENCRYPT_KEY_SIZE,
                                          btInitializationVector.bytes,
                                          [data bytes],
                                          [data length],
                                          buffer,
                                          bufferSize,
										  &encryptedSize);
	if (cryptStatus == kCCSuccess) {
		result = [NSData dataWithBytesNoCopy:buffer length:encryptedSize];
	} else {
        free(buffer);
        NSLog(@"[ERROR] failed to encrypt|CCCryptoStatus: %d", cryptStatus);
    }
	
    return result == nil ? @"" : [result base64EncodedString];
}
+ (NSString*)convertText:(NSString*)values{
    if ([AccessRightModel isEnableEncryptedQRCode]) {
        NSString *strKey = [UserManagerV2 sharedUserManager].strKey;
        NSString *strIV = [UserManagerV2 sharedUserManager].strIV;
        return [STEncryptorDES decryptData:values key:strKey iv:strIV];
    }
    return values;
}
+ (NSString*)decryptData:(NSString*)valueData key:(NSString*)key iv:(NSString*)iv;
{
    if (key == nil || iv == nil || valueData == nil) {
        return valueData;
    }
    NSData *data = [NSData dataFromBase64String:valueData];
    NSData* result = nil;
    
    // setup output buffer
	size_t bufferSize = [data length] + FBENCRYPT_BLOCK_SIZE;
	void *buffer = malloc(bufferSize);
	
    
    NSData *btCryptKey = [[NSData alloc] initWithBase64EncodedString:key options:0];
    NSData *btInitializationVector = [[NSData alloc] initWithBase64EncodedString:iv options:0];
    
    // do decrypt
	size_t decryptedSize = 0;
	CCCryptorStatus cryptStatus = CCCrypt(kCCDecrypt,
                                          FBENCRYPT_ALGORITHM,
                                          kCCOptionPKCS7Padding,
										  btCryptKey.bytes,
                                          FBENCRYPT_KEY_SIZE,
                                          btInitializationVector.bytes,
                                          [data bytes],
                                          [data length],
                                          buffer,
                                          bufferSize,
                                          &decryptedSize);
	
	if (cryptStatus == kCCSuccess) {
		result = [NSData dataWithBytesNoCopy:buffer length:decryptedSize];
	} else {
        free(buffer);
        NSLog(@"[ERROR] failed to decrypt| CCCryptoStatus: %d", cryptStatus);
    }
    
    return result == nil ? @"" : [[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding];
}


#define FBENCRYPT_IV_HEX_LEGNTH (FBENCRYPT_BLOCK_SIZE*2)

+ (NSData*)generateIv
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        srand(time(NULL));
    });
    
    char cIv[FBENCRYPT_BLOCK_SIZE];
    for (int i=0; i < FBENCRYPT_BLOCK_SIZE; i++) {
        cIv[i] = rand() % 256;
    }
    return [NSData dataWithBytes:cIv length:FBENCRYPT_BLOCK_SIZE];
}


+ (NSString*)hexStringForData:(NSData*)data
{
    if (data == nil) {
        return nil;
    }
    
    NSMutableString* hexString = [NSMutableString string];
    
    const unsigned char *p = [data bytes];
    
    for (int i=0; i < [data length]; i++) {
        [hexString appendFormat:@"%02x", *p++];
    }
    return hexString;
}

+ (NSData*)dataForHexString:(NSString*)hexString
{
    if (hexString == nil) {
        return nil;
    }
    
    const char* ch = [[hexString lowercaseString] cStringUsingEncoding:NSUTF8StringEncoding];
    NSMutableData* data = [NSMutableData data];
    while (*ch) {
        char byte = 0;
        if ('0' <= *ch && *ch <= '9') {
            byte = *ch - '0';
        } else if ('a' <= *ch && *ch <= 'f') {
            byte = *ch - 'a' + 10;
        }
        ch++;
        byte = byte << 4;
        if (*ch) {
            if ('0' <= *ch && *ch <= '9') {
                byte += *ch - '0';
            } else if ('a' <= *ch && *ch <= 'f') {
                byte += *ch - 'a' + 10;
            }
            ch++;
        }
        [data appendBytes:&byte length:1];
    }
    return data;
}

@end
