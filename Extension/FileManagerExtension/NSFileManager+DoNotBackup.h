
#define DataDirectory NSCachesDirectory //NSDocumentDirectory

@interface NSFileManager (DoNotBackup)

- (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL;

@end