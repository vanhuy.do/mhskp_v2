//
//  UIImage+Tint.h
//
//  Created by Matt Gemmell on 04/07/2010.
//  Copyright 2010 Instinctive Code.
//

#import <UIKit/UIKit.h>

#define COLOR_NAVIGATION_BAR [UIColor colorWithRed:99/255.0f green:202/255.0f blue:206/255.0f alpha:1.0f]

#define COLOR_PART_RED(color) (((color) >> 16) & 0xff)
#define COLOR_PART_GREEN(color) (((color) >> 8) & 0xff)
#define COLOR_PART_BLUE(color) ( (color) & 0xff)

@interface UIImage (MGTint)

- (UIImage *)imageTintedWithColor:(UIColor *)color;
- (UIImage *)imageTintedWithColor:(UIColor *)color fraction:(CGFloat)fraction;
- (UIImage *)imageNegative;


- (UIImage *)imageByRemovingColor:(uint)color;
- (UIImage *)imageByRemovingColorsWithMinColor:(uint)minColor maxColor:(uint)maxColor;
- (UIImage *)imageByReplacingColor:(uint)color withColor:(uint)newColor;
- (UIImage *)imageByReplacingColorsWithMinColor:(uint)minColor maxColor:(uint)maxColor withColor:(uint)newColor;
- (UIImage *)imageByReplacingColorsWithMinColor:(uint)minColor maxColor:(uint)maxColor withColor:(uint)newColor andAlpha:(float)alpha;

+ (UIImage *)imageWithColor:(UIColor *)color;
+ (UIImage *)imageBeforeiOS7:(NSString*)imageEarlyiOS7 equaliOS7:(NSString*)imageEqualiOS7;

@end
