//
//  NSString+Common.h
//  mHouseKeeping
//
//  Created by Gambogo on 11/18/14.
//
//

#import <Foundation/Foundation.h>

@interface NSString (Common)

-(NSString*) currentKeyToLanguage;

@end
