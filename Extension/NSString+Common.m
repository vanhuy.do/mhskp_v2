//
//  NSString+Common.m
//  mHouseKeeping
//
//  Created by Gambogo on 11/18/14.
//
//

#import "NSString+Common.h"
#import "LanguageManagerV2.h"

@implementation NSString (Common)

-(NSString*) currentKeyToLanguage
{
    return [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:self];
}

@end
