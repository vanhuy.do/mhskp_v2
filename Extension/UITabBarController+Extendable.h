//
//  UITabBarController+Extendable.h
//  mHouseKeeping
//
//  Created by Gambogo on 5/28/15.
//
//

#import <UIKit/UIKit.h>

@interface UITabBarController (Extendable)

-(void) addSubview:(UIView*)currentSubview;

-(void) removeLastSubview;

@end
