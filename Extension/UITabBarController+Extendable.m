//
//  UITabBarController+Extendable.m
//  mHouseKeeping
//
//  Created by Gambogo on 5/28/15.
//
//
#import <objc/runtime.h>
#import "UITabBarController+Extendable.h"

static void * TopviewInserted = &TopviewInserted;

@implementation UITabBarController (Extendable)

-(void) addSubview:(UIView*)currentSubview
{
    [self.view addSubview:currentSubview];
    [self setTopViewInserted:currentSubview];
}

-(void) removeLastSubview
{
    UIView *topView = [self topViewInserted];
    if (topView) {
        if (topView.superview) {
            [topView removeFromSuperview];
            [self setTopViewInserted:nil];
        }
    }
}

- (UIView *)topViewInserted {
    return objc_getAssociatedObject(self, TopviewInserted);
}

- (void)setTopViewInserted:(UIView *)topViewInserted {
    objc_setAssociatedObject(self, TopviewInserted, topViewInserted, OBJC_ASSOCIATION_ASSIGN);
}

@end
