//
//  AdHocMessageManagerV2.h
//  mHouseKeeping
//
//  Created by TMS on 3/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserListAdapterV2.h"
#import "MessageModelV2.h"
#import "MessagePhotoModelV2.h"
#import "MessageSubjectItemAdapterV2.h"
#import "MessageSubjectAdapterV2.h"
#import "MessageReceiverModelV2.h"
#import "ehkDefinesV2.h"
#import "UserModelV2.h"
#import "MBProgressHUD.h"
#import "UserDetailsModelV2.h"

@interface AdHocMessageManagerV2 : NSObject

//Hao Tran[20130927] - Message WS
-(BOOL)loadWSMessageReceivedByUserId:(int)userId groupMessageId:(int)groupMessageId hotelId:(int)hotelId lastModified:(NSString*)lastModified;
-(BOOL)loadWSMessageSentByUserId:(int)userId groupMessageId:(int)groupMessageId hotelId:(int)hotelId lastModified:(NSString*)lastModified;
-(BOOL) loadWSMessageCountNumberByUserId:(int)userId groupMessageId:(int)groupMessageId hotelId:(int)hotelId lastModified:(NSString*)lastModified;
-(BOOL) updateWSMessageStatus:(int)userId messageId:(int)messageId status:(int)status; //Status 1 = read, status 0 = unread
//Hao Tran[20130927] - END

//Hao Tran[20130927] - MESSAGE LOCAL
-(int)deleteMessageByOwnerId:(int) ownerId beforeDate:(NSString*)dateString;
-(int)deleteMessagePhotoBeforeDate:(NSString*)dateString;
-(int)deleteMessageReceiverByOwnerId:(int)ownerId beforeDate:(NSString*)dateString;
//Hao Tran[20130927] - END

//Load attendants from WS and save in userList
-(BOOL)loadWSAttendantByUserId:(int)userId hotelId:(int)hotelId;
-(BOOL)getUserListWith:(NSInteger)userID andHotelID:(NSInteger)hotelID andLastModified:(NSString*)lastModified AndPercentView:(MBProgressHUD*)percentView;
- (BOOL)getUserDetailsRoutine:(NSInteger)userID withHotel:(NSInteger)HTID withTypeID: (NSInteger) TypeID AndPercentView:(MBProgressHUD *)percentView;
-(NSInteger)postMessage:(MessageModelV2*)message receivers:(NSMutableArray*)receiversList photos:(NSMutableArray*)messagePhotos;
- (NSInteger)postMessageDetailsModel:(MessageModelV2*)message receivers:(NSMutableArray*)messageReceiverList photos:(NSMutableArray*)messagePhotos;
-(int)getMessagePhotoWithUserID:(int)userId andGroupId:(NSInteger)groupId;
-(void)getSubjectListWith:(NSInteger)userID andHotelID:(NSInteger)hotelID andLastModified:(NSString*)lastModified AndPercentView:(MBProgressHUD*)percentView;


// UserListAdapter
-(int)insertUserListData:(UserListModelV2*) userListData ;
-(int)updateUserListData:(UserListModelV2*) userListData ;
-(int)deleteUserListData:(UserListModelV2*) userListData ;
//-(int)deleteAllUserListData;
-(UserListModelV2*)loadUserListDataByUserId:(int)userId ;
-(NSMutableArray *) loadAllUserListDataByUserHotelId:(int) userHotelId;
-(NSMutableArray *) loadAllUserListData;
-(NSMutableArray *) loadAllUserListBySupervisorId:(NSInteger) supervisorId;
-(UserListModelV2 *) loadUserListByUserFullName:(NSString *) userFullName;

// UserDetailsAdapter
-(int)insertUserDetailsData:(UserDetailsModelV2*) userListData ;
-(int)updateUserDetailsData:(UserDetailsModelV2*) userListData ;
-(int)deleteAllUserListData;
-(UserDetailsModelV2*)loadUserDetailsDataByUserId:(NSString*)userId ;
-(NSMutableArray *) loadAllUserDetailsData;
// MessageAdapter
-(int)insertMessage:(MessageModelV2*) messageData ;
-(int)insertOfflineMessage:(MessageModelV2*) messageData ;
-(int)updateMessage:(MessageModelV2*) messageData ;
-(int)updateMessageStatus:(MessageModelV2*) model;
-(int)deleteMessageByOwnerId:(int)ownerId;
-(int)deleteMessageByOwnerId:(int)ownerId messageId:(int)messageId;
-(int)deleteMessageByOwnerId:(int)ownerId messageGroupId:(int)messageGroupId;
-(MessageModelV2*)loadMessageByOwnerId:(int)ownerId messageId:(int)messageId;
-(BOOL)hasMessageByOwnerId:(int)ownerid messageId:(int)messageId;
-(NSMutableArray*)loadMessageReceivedByOwnerId:(int)ownerId;
-(NSMutableArray*)loadMessageSentByOwnerId:(int)ownerId;
-(int)countMessageSentByOwnerId:(int)ownerId;
-(int)countMessageInboxByOwnerId:(int)ownerId;
-(int)countMessageInboxUnreadByOwnerId:(int)ownerId;

//MessagePhotoAdapter
-(int)insertMsgPhotoData:(MessagePhotoModelV2*) msgPhotoData ;
-(int)updateMsgPhotoData:(MessagePhotoModelV2*) msgPhotoData ;
-(int)deleteMsgPhotoDataByGroupId:(int)groupId;
-(NSMutableArray *) loadMessagePhotosByGroupId:(int) messageGroupId;
-(int)countMessagePhotoByGroupId:(int)groupId;

//MessageSubjectItemAdapter
-(int)insertMsgSubjectItemData:(MessageSubjectItemModelV2*) msgSubjectItem ;
-(int)updateMsgSubjectItemData:(MessageSubjectItemModelV2*) msgSubjectItem ;
-(int)deleteMsgSubjectItemData:(MessageSubjectItemModelV2*) msgSubjectItem ;
-(MessageSubjectItemModelV2*)loadMsgSubjectItemData:(MessageSubjectItemModelV2*) msgSubjectItem ;
-(NSMutableArray *) loadAllMsgSubjectItemByMsgSubject:(int) smicategory_id;

//MessageSubjetAdapter
-(int)insertMsgSubjectData:(MessageSubjectModelV2*) msgSubject ;
-(int)updateMsgSubjectData:(MessageSubjectModelV2*) msgSubject ;
-(int)deleteMsgSubjectData:(MessageSubjectModelV2*) msgSubject ;
-(MessageSubjectModelV2*)loadMsgSubjectData:(MessageSubjectModelV2*) msgSubject ;
-(NSMutableArray *) loadAllMsgSubject;
//MessageReceiverAdapter
-(int)insertMessageReceiverData:(MessageReceiverModelV2*) messageReceiverData ;
-(int)updateMessageReceiverData:(MessageReceiverModelV2*) messageReceiverData ;
-(NSMutableArray *) loadMessageReceiversByOwnerId:(int)ownerId groupId:(int)groupId;
-(int) countMessageReceiversByOwnerId:(int)ownerId groupId:(int)groupId;
- (BOOL)checkMessageReceiverIfExisted:(MessageReceiverModelV2*)receiver;
- (NSMutableArray*)loadMessageSentByOwnerId:(int)ownerId messageGroupId:(int)messageGroupId;
- (int)deleteMessageReceiverByOwnerId:(int)ownerId messageGroupId:(int)messageGroupId;

-(BOOL)isExistUserListData:(UserListModelV2*) userListData;
-(BOOL)isExistUserDetailsData:(UserDetailsModelV2*) userListData;
// create the instance
+ (AdHocMessageManagerV2*) sharedAdHocMessageManagerV2;
// DungPhan - 20151008: new API
- (BOOL)getReceivedAdHocMessageRoutineByUserId:(int)userId groupMessageId:(int)groupMessageId hotelId:(int)hotelId lastModified:(NSString*)lastModified;
- (BOOL)getSendAdHocMessageRoutineByUserId:(int)userId groupMessageId:(int)groupMessageId hotelId:(int)hotelId lastModified:(NSString*)lastModified;
- (BOOL)getNoOfAdHocMessageRoutineByUserId:(int)userId groupMessageId:(int)groupMessageId hotelId:(int)hotelId lastModified:(NSString*)lastModified;
- (BOOL)postUpdateAdHocMessageRoutineWithUserId:(int)userId messageUpdateDetails:(NSMutableArray*)messageUpdateDetails updateType:(int)updateType;
@end
