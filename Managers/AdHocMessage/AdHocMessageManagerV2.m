//
//  AdHocMessageManagerV2.m
//  mHouseKeeping
//
//  Created by TMS on 3/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AdHocMessageManagerV2.h"
#import "LogFileManager.h"
#import "LogObject.h"
#import "DataBaseExtension.h"
#import "MessagePhotoModelV2.h"
#import "MessageModelV2.h"
#import "MessageReceiverModelV2.h"
#import "NetworkCheck.h"
#import "UserDetailsAdapterV2.h"
#import "Web_x0020_Service.h"
@interface AdHocMessageManagerV2 (PrivateMethods)

-(BOOL) getMessageCategoryListWithUserId:(NSInteger) userId hotelId:(NSInteger) hotelId andLastModified:(NSString *)lastModified AndPercentView:(MBProgressHUD*)percentView;
-(BOOL) getMessageItemListWithUserId:(NSInteger) userId andLastModified:(NSString *) lastModified AndPercentView:(MBProgressHUD*)percentView;

@end

@implementation AdHocMessageManagerV2

NSString *LOADING_GET_USER_LIST = @"Get User List";
NSString *LOADING_MESSAGE_CATEGORY = @"Loading Message Category";
NSString *LOADING_MESSAGE_ITEMS = @"Loading Message Items";

static AdHocMessageManagerV2* sharedAdHocMessageManagerV2Instance = nil;

+ (AdHocMessageManagerV2*) sharedAdHocMessageManagerV2{
    //	if (sharedAdHocMessageManagerV2Instance == nil) {
    //        sharedAdHocMessageManagerV2Instance = [[super allocWithZone:NULL] init];
    //    }
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedAdHocMessageManagerV2Instance = [[AdHocMessageManagerV2 alloc] init];
    });
    return sharedAdHocMessageManagerV2Instance;
}
//+ (id)allocWithZone:(NSZone *)zone
//{
//    return [[self sharedAdHocMessageManagerV2] retain];
//}
//
//- (id)copyWithZone:(NSZone *)zone
//{
//    return self;
//}
//
//- (id)retain
//{
//    return self;
//}
//
//- (NSUInteger)retainCount
//{
//    return NSUIntegerMax;  //denotes an object that cannot be released
//}
//
//
//- (id)autorelease
//{
//    return self;
//}
#pragma mark
#pragma mark Webservice
//Hao Tran[20130927] - Message WS
-(BOOL)loadWSMessageReceivedByUserId:(int)userId groupMessageId:(int)groupMessageId hotelId:(int)hotelId lastModified:(NSString*)lastModified
{
    //No network detected
    if(![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]){
        return NO;
    }
    
    //Required Fields
    if(userId <= 0){return NO;}
    if(hotelId <= 0){return NO;}
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole]){
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_GetReceivedAdHocMessage *request = [[eHousekeepingService_GetReceivedAdHocMessage alloc] init];
    [request setIntHotelID:[NSNumber numberWithInt:hotelId]];
    [request setIntUsrID:[NSNumber numberWithInt:userId]];
    
    //Optional fields
    if(groupMessageId > 0){
        [request setIntGrpMessageID:[NSString stringWithFormat:@"%d", groupMessageId]];
    }
    
    if(lastModified.length > 0){
        [request setStrLastModified:lastModified];
    }
    
    BOOL result = NO;
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetReceivedAdHocMessageUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetReceivedAdHocMessageResponse class]]) {
            eHousekeepingService_GetReceivedAdHocMessageResponse *dataBody = (eHousekeepingService_GetReceivedAdHocMessageResponse *)bodyPart;
            if (dataBody != nil) {
                if ([dataBody.GetReceivedAdHocMessageResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_OK]]
                    || [dataBody.GetReceivedAdHocMessageResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_NO_RERCORD_TO_DISPLAY]]) {
                    
                    result = YES;
                    
                    NSMutableArray *listResults = dataBody.GetReceivedAdHocMessageResult.AdHocMessageList.AdHocMessageList2;
                    for (eHousekeepingService_AdHocMessageList2 *wsCurMessage in listResults) {
                        bool hasMessage = [self hasMessageByOwnerId:userId messageId:[wsCurMessage.amsgID intValue]];
                        if(!hasMessage){
                            MessageModelV2 *curMessage = [[MessageModelV2 alloc] init];
                            // DungPhan - 20150929: Fix can't insert message with single quote symbol to db
//                            curMessage.message_content = wsCurMessage.amsgContent;
                            curMessage.message_content =[wsCurMessage.amsgContent stringByReplacingOccurrencesOfString:@ "'" withString: @"''"];
                            curMessage.message_count_photo_attached = [wsCurMessage.amsgPhotoAttached intValue];
                            curMessage.message_from_user_login_name = wsCurMessage.amsgSendFrom;
                            curMessage.message_from_user_name = wsCurMessage.amsgSendFromLang;
                            curMessage.message_group_id = [wsCurMessage.amsgGrpID intValue];
                            curMessage.message_id = [wsCurMessage.amsgID intValue];
                            curMessage.message_kind = MESSAGE_KIND_INBOX;
                            curMessage.message_last_modified = wsCurMessage.amsgLastModified;
                            curMessage.message_owner_id = userId;
                            curMessage.message_status = [wsCurMessage.amsgStatus intValue];
                            curMessage.message_topic = wsCurMessage.amsgTopic;
                            
                            [self insertMessage:curMessage];
                        }
                    }
                }
            }
        }
    }
    
    return result;
}

-(BOOL)loadWSMessageSentByUserId:(int)userId groupMessageId:(int)groupMessageId hotelId:(int)hotelId lastModified:(NSString*)lastModified
{
    //No network detected
    if(![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]){
        return NO;
    }
    
    //Required Fields
    if(userId <= 0){return NO;}
    if(hotelId <= 0){return NO;}
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole]){
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_GetSentAdHocMessage *request = [[eHousekeepingService_GetSentAdHocMessage alloc] init];
    [request setIntHotelID:[NSNumber numberWithInt:hotelId]];
    [request setIntUsrID:[NSNumber numberWithInt:userId]];
    
    //Optional fields
    if(groupMessageId > 0){
        [request setIntGrpMessageID:[NSString stringWithFormat:@"%d", groupMessageId]];
    }
    
    if(lastModified.length > 0){
        [request setStrLastModified:lastModified];
    }
    
    BOOL result = NO;
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetSentAdHocMessageUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetSentAdHocMessageResponse class]]) {
            eHousekeepingService_GetSentAdHocMessageResponse *dataBody = (eHousekeepingService_GetSentAdHocMessageResponse *)bodyPart;
            if (dataBody != nil) {
                if ([dataBody.GetSentAdHocMessageResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_OK]]
                    || [dataBody.GetSentAdHocMessageResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_NO_RERCORD_TO_DISPLAY]]) {
                    
                    result = YES;
                    
                    NSMutableArray *listResults = dataBody.GetSentAdHocMessageResult.AdHocMessageList.AdHocMessageList2;
                    NSString *todayDateString = [ehkConvert getTodayDateString];
                    
                    for (eHousekeepingService_AdHocMessageList2 *wsCurMessage in listResults) {
                        //MessageModelV2 *curMessage = [self loadMessageByOwnerId:userId messageId:[wsCurMessage.amsgID intValue]];
                        BOOL hasMessage = [self hasMessageByOwnerId:userId messageId:[wsCurMessage.amsgID intValue]];
                        if(!hasMessage){
                            MessageModelV2 *curMessage = [[MessageModelV2 alloc] init];
                            // DungPhan - 20150929: Fix can't insert message with single quote symbol to db
//                            curMessage.message_content = wsCurMessage.amsgContent;
                            curMessage.message_content = [wsCurMessage.amsgContent stringByReplacingOccurrencesOfString:@ "'" withString: @"''"];
                            curMessage.message_count_photo_attached = [wsCurMessage.amsgPhotoAttached intValue];
                            curMessage.message_from_user_login_name = wsCurMessage.amsgSendFrom;
                            curMessage.message_from_user_name = wsCurMessage.amsgSendFromLang;
                            curMessage.message_group_id = [wsCurMessage.amsgGrpID intValue];
                            curMessage.message_id = [wsCurMessage.amsgID intValue];
                            curMessage.message_kind = MESSAGE_KIND_SENT;
                            curMessage.message_last_modified = wsCurMessage.amsgLastModified;
                            curMessage.message_owner_id = userId;
                            curMessage.message_status = MESSAGE_READ;
                            curMessage.message_topic = wsCurMessage.amsgTopic;
                            
                            [self insertMessage:curMessage];
                            
                            int countReceivers = [self countMessageReceiversByOwnerId:userId groupId:(int)curMessage.message_group_id];
                            if(countReceivers <= 0){
                                NSArray *listReceivers = [wsCurMessage.amsgReceivedIDs componentsSeparatedByString:@","];
                                for (NSString *curReceiver in listReceivers) {
                                    MessageReceiverModelV2 *receiver = [[MessageReceiverModelV2 alloc] init];
                                    receiver.message_receiver_group_id = curMessage.message_group_id;
                                    receiver.message_receiver_id = [curReceiver intValue];
                                    receiver.message_receiver_owner_id = userId;
                                    receiver.message_receiver_last_modified = todayDateString;
                                    [self insertMessageReceiverData:receiver];
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    return result;
}

-(BOOL) loadWSMessageCountNumberByUserId:(int)userId groupMessageId:(int)groupMessageId hotelId:(int)hotelId lastModified:(NSString*)lastModified
{
    //No network detected
    if(![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]){
        return NO;
    }
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole]){
        [binding setLogXMLInOut:YES];
    }
    
    //Require fields
    if(hotelId <= 0){return NO;}
    if(userId <= 0){return NO;}
    
    eHousekeepingService_GetNoOfAdHocMessage *request = [[eHousekeepingService_GetNoOfAdHocMessage alloc] init];
    
    //Require fields
    [request setIntHotelID:[NSNumber numberWithInt:hotelId]];
    [request setIntUsrID:[NSNumber numberWithInt:userId]];
    
    //Option fields
    if(groupMessageId > 0){
        [request setIntGrpMessageID:[NSString stringWithFormat:@"%d",groupMessageId]];
    }
    
    if(lastModified.length > 0){
        [request setStrLastModified:lastModified];
    }
    
    BOOL result = NO;
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetNoOfAdHocMessageUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetNoOfAdHocMessageResponse class]]) {
            eHousekeepingService_GetNoOfAdHocMessageResponse *dataBody = (eHousekeepingService_GetNoOfAdHocMessageResponse *)bodyPart;
            if (dataBody != nil) {
                /* No need to check response status
                if ([dataBody.GetNoOfAdHocMessageResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_OK]]
                    || [dataBody.GetNoOfAdHocMessageResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_NO_RERCORD_TO_DISPLAY]]) {
                  */
                if(dataBody.GetNoOfAdHocMessageResult != nil){
                    result = YES;
                    eHousekeepingService_AdHocMessageCounter *messageCounter = dataBody.GetNoOfAdHocMessageResult;
                    if(messageCounter.intReadReceived != nil){
                        [[UserManagerV2 sharedUserManager] setCurrentUserConfig:USER_CONFIG_MESSAGE_READ_RECEIVED value:[NSString stringWithFormat:@"%d", [messageCounter.intReadReceived intValue]]];
                    }
                    
                    if(messageCounter.intSent != nil){
                        [[UserManagerV2 sharedUserManager] setCurrentUserConfig:USER_CONFIG_MESSAGE_SENT value:[NSString stringWithFormat:@"%d", [messageCounter.intSent intValue]]];
                    }
                    
                    if(messageCounter.intUnreadReceive != nil){
                        NSString *countOldValue = [[UserManagerV2 sharedUserManager] getCurrentUserConfigValueByKey:USER_CONFIG_MESSAGE_UNREAD_RECEIVED];
                        if([countOldValue intValue] < [messageCounter.intUnreadReceive intValue]) {
                            [[UserManagerV2 sharedUserManager] setCurrentUserConfig:USER_CONFIG_MESSAGE_IS_SHOW_NOTIFICATION value:@"true"];
                        }
                        [[UserManagerV2 sharedUserManager] setCurrentUserConfig:USER_CONFIG_MESSAGE_UNREAD_RECEIVED value:[NSString stringWithFormat:@"%d", [messageCounter.intUnreadReceive intValue]]];
                    }
                }
                //}
            }
        }
    }
    
    return result;
}

-(BOOL) updateWSMessageStatus:(int)userId messageId:(int)messageId status:(int)status //Status 1 = read, status 0 = unread
{
    //No network detected
    if(![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]){
        return NO;
    }
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole]){
        [binding setLogXMLInOut:YES];
    }
    
    //Require fields
    if(userId <= 0){return NO;}
    
    eHousekeepingService_UpdateAdHocMessageStatus *request = [[eHousekeepingService_UpdateAdHocMessageStatus alloc] init];
    
    [request setIntUsrID:[NSNumber numberWithInt:userId]];
    [request setIntMsgID:[NSNumber numberWithInt:messageId]];
    [request setIntStatus:[NSNumber numberWithInt:status]];
    //Require fields
    
    BOOL result = NO;
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding UpdateAdHocMessageStatusUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_UpdateAdHocMessageStatusResponse class]]) {
            eHousekeepingService_UpdateAdHocMessageStatusResponse *dataBody = (eHousekeepingService_UpdateAdHocMessageStatusResponse *)bodyPart;
            if (dataBody != nil) {
                if ([dataBody.UpdateAdHocMessageStatusResult.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_OK]]
                    || [dataBody.UpdateAdHocMessageStatusResult.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_NO_RERCORD_TO_DISPLAY]]) {
                    
                    result = YES;
                }
            }
        }
    }
    
    return result;
}

//Hao Tran[20130927] - END

-(BOOL)loadWSAttendantByUserId:(int)userId hotelId:(int)hotelId
{
    if (![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        return NO;
    }
    
    BOOL result = NO;
    NSArray *roomAttendantList = [[FindManagerV2 sharedFindManagerV2] findDataAttendantWithUserID:[NSNumber numberWithInt:userId] AndHotelID:[NSNumber numberWithInt:hotelId] AndAttendantName:nil];
    
    NSString *lastModified = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
    if(roomAttendantList.count > 0){
        result = YES;
        
        //Clear old attendants
        NSString *sqlUpdate = [NSString stringWithFormat:@"UPDATE %@ SET %@ = %d WHERE %@ = %d", USER_LIST, user_supervisor_id, 0, user_supervisor_id, userId];
        DataTable *tableDelete = [[DataTable alloc] initWithNameFormat:@"%@",USER_LIST];
        [tableDelete excuteQueryNonSelect:sqlUpdate];
        
        for (NSDictionary *curRA in roomAttendantList) {
            UserListModelV2 *model = nil;
            model = [self loadUserListDataByUserId:[[curRA objectForKey:kAttendantID] intValue]];
            if(model == nil) {
                model = [[UserListModelV2 alloc] init];
                model.userListId = [[curRA objectForKey:kAttendantID] intValue];
                model.userListHotelId = hotelId;
                model.userListName = [curRA objectForKey:kAtdName];
                model.userListFullName = [curRA objectForKey:kAttendantName];
                model.userListFullNameLang = [curRA objectForKey:kAttendantName];
                model.userSupervisorId = userId;
                model.userListLastModified = lastModified;
                
                [self insertUserListData:model];
            } else {
                model.userSupervisorId = userId;
                [self updateUserListData:model];
            }
        }
    } else {
        result = NO;
    }
    
    return result;
}

-(BOOL)getUserListWith:(NSInteger)userID andHotelID:(NSInteger)hotelID andLastModified:(NSString*)lastModified AndPercentView:(MBProgressHUD *)percentView
{
    if (![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        return NO;
    }
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole])
    {
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_GetUserList *request = [[eHousekeepingService_GetUserList alloc] init];
    if (userID > 0) {
        [request setIntUsrID:[NSNumber numberWithInteger:userID]];
    }
    if (hotelID > 0) {
        [request setIntHotelID:[NSNumber numberWithInteger:hotelID]];
    }
    
    if (lastModified.length > 0) {
        // FM [20160330] - Not required to pass in last modified value
        lastModified = @"";
        [request setStrLastModified:lastModified];
    }
    
    BOOL result = YES;
    BOOL deleteAll = NO;
    int deleteResult = 0;
    //    LogObject *logObj = [[LogObject alloc] init];
    //    logObj.dateTime = [NSDate date];
    //    logObj.log = [NSString stringWithFormat:@"<%i><getUserListWith><userID:%i hotelID:%i lastModified:%@>",userID,userID,hotelID,lastModified];
    //    LogFileManager *logManager = [[LogFileManager alloc] init];
    //    [logManager save:logObj];
    
    float currentPercent = 0;
    [self updatePercentView:percentView value:currentPercent description:LOADING_GET_USER_LIST];
    if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL) {
        return NO;
    }
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetUserListUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    for (id bodyPart in responseBodyParts) {
        //        NSLog(@"bodyPart %@",[bodyPart class]);
        
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetUserListResponse class]]) {
            eHousekeepingService_GetUserListResponse *dataBody = (eHousekeepingService_GetUserListResponse *)bodyPart;
            if (dataBody) {
                if ([dataBody.GetUserListResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]]) {
                    
                    //NSArray *roomAttendantList = [[FindManagerV2 sharedFindManagerV2] findDataAttendantWithUserID:[NSNumber numberWithInt:userID] AndHotelID:[NSNumber numberWithInt:hotelID] AndAttendantName:nil];
                    
                    NSMutableArray *userListObject = dataBody.GetUserListResult.UserDetailList.UserDetail;
                    
                    // FM [20160330] - Validate array is null or empty before delete data from local database
                    if ((userListObject.count > 0) && (userListObject)) {
                        deleteResult = [self deleteAllUserListData];
                        
                        if (deleteResult == 1)
                        {
                            deleteAll = YES;
                        }
                    }
                    
                    float stepPercent = [self caculateStepFromCountingObject:(int)[userListObject count]];
                    for (eHousekeepingService_UserDetail *userDetail in userListObject) {
                        if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL)
                        {
                            break;
                        }
                        UserListModelV2 *model = [[UserListModelV2 alloc] init];
                        UserListModelV2 *compareModel = [[UserListModelV2 alloc] init];
                        
                        compareModel.userListId = [userDetail.usrID intValue];
                        //compareModel.usrSupervisor = [[UserManagerV2 sharedUserManager] currentUser].userId;
                        compareModel = [self loadUserListDataByUserId:[userDetail.usrID intValue]];
                        model.userListId = [userDetail.usrID intValue];
                        //model.userListHotelId = [userDetail.usrHotelID intValue];
                        
                        model.userListHotelId = hotelID;
                        model.userListName = userDetail.usrName;
                        model.userListFullName = userDetail.usrFullName;
                        model.userListFullNameLang = userDetail.usrLang;
                        model.usrSupervisor = [userDetail.usrIsSupervisor intValue];
                        model.userListLastModified = userDetail.usrLastModified;
                        
                        /*if(model.usrSupervisor == 0)
                        {
                            for (NSDictionary *roomAttendant in roomAttendantList) {
                                if ([roomAttendant objectForKey:kAttendantID] != nil && [(NSString *)[roomAttendant objectForKey:kAttendantID] integerValue] == [userDetail.usrID integerValue]) {
                                    model.userSupervisorId = userID;
                                    break;
                                }
                            }
                        }*/
                        
                        // FM [20160330]
                        // YES - local data for user list is deleted, perform insert function
                        // NO - local data for user list is not deleted, perform original function
                        if (!deleteAll)
                        {
                            if(![self isExistUserListData:compareModel]){
                                [self insertUserListData:model];
                            }else{
                                [self updateUserListData:model];
                            }
                        }else{
                            [self insertUserListData:model];
                        }
                        
                        [self updatePercentView:percentView value:currentPercent description:LOADING_GET_USER_LIST];
                        currentPercent += stepPercent;
                    }
                    
                }
                else
                    result = NO;
            }
            else
                result = NO;
        }
        else
            result = NO;
    }
    return result;
}

- (BOOL)getUserDetailsRoutine:(NSInteger)userID withHotel:(NSInteger)HTID withTypeID: (NSInteger) TypeID AndPercentView:(MBProgressHUD *)percentView {
    //    NSMutableArray *listResult = [NSMutableArray new];
    if (![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        return NO;
    }
    Web_x0020_Service_Web_x0020_ServiceSoap12Binding *binding = [Web_x0020_Service Web_x0020_ServiceSoap12Binding];
    if([LogFileManager isLogConsole]){
        [binding setLogXMLInOut:YES];
    }
    
    Web_x0020_Service_ElementGetUserDetailsRoutine *request = [[Web_x0020_Service_ElementGetUserDetailsRoutine alloc] init];
    
    [request setUserID:[NSString stringWithFormat:@"%li",(long)userID]];
    [request setHTID:[NSString stringWithFormat:@"%li",(long)HTID]];
    [request setTypeID:[NSString stringWithFormat:@"%li",(long)TypeID]];
    
    BOOL result = YES;
    BOOL deleteAll = NO;
    int deleteResult = 0;
    Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *response = [binding GetUserDetailsRoutine:request];
    
    float currentPercent = 0;
    [self updatePercentView:percentView value:currentPercent description:LOADING_GET_USER_LIST];

    NSArray *responseBodyParts = response.bodyParts; if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL) {
        return NO;
    }
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[Web_x0020_Service_ElementGetUserDetailsRoutineResponse class]]){
            
            Web_x0020_Service_ElementGetUserDetailsRoutineResponse *dataBody = (Web_x0020_Service_ElementGetUserDetailsRoutineResponse *)bodyPart;
            if ([dataBody.GetUserDetailsRoutineResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]]) {
                
                //[self deleteRoomTypeInventoryModelByServiceType:serviceId userId:userId];
                
                NSArray *UserDetailsRoutineListing = dataBody.GetUserDetailsRoutineResult.UserDetailsReponseListing;
                // FM [20160330] - Validate array is null or empty before delete data from local database
                if ((UserDetailsRoutineListing.count > 0) && (UserDetailsRoutineListing)) {
                    deleteResult = [self deleteAllUserDetailsData];
                    
                    if (deleteResult == 1)
                    {
                        deleteAll = YES;
                    }
                }
                 float stepPercent = [self caculateStepFromCountingObject:(int)[UserDetailsRoutineListing count]];
                for (Web_x0020_Service_UserDetailsReponseDetails *userDetails in UserDetailsRoutineListing) {
//                    if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL)
//                    {
//                        break;
//                    }
                    UserDetailsModelV2 *model = [[UserDetailsModelV2 alloc] init];
                    UserDetailsModelV2 *compareModel = [[UserDetailsModelV2 alloc] init];
                    compareModel.userDetailsId = userDetails.UserID;
                    compareModel = [self loadUserDetailsDataByUserId:userDetails.UserID];
                    model.userId = userID;
                    model.userDetailsId = userDetails.UserID;
                    model.userDetailsHotelId = [userDetails.UserHotelID intValue];
                    model.userDetailsName = userDetails.UserName;
                    model.userDetailsFullName = userDetails.UserFullName;
                    model.userSupervisorId = @"";
                    model.userDetailsLastModified = @"";
                    model.usrSupervisor = 0;
                    model.userSupervisorId = 0;
                    if (!deleteAll)
                    {
                        if(![self isExistUserDetailsData:compareModel]){
                            [self insertUserDetailsData:model];
                        }else{
                            [self updateUserDetailsData:model];
                        }
                    }else{
                        [self insertUserDetailsData:model];
                    }
                    [self updatePercentView:percentView value:currentPercent description:LOADING_GET_USER_LIST];
                    currentPercent += stepPercent;
                }
                
            } else
                result = NO;
        } else
            result = NO;
    }
    return result;
}

-(void)getSubjectListWith:(NSInteger)userID andHotelID:(NSInteger)hotelID andLastModified:(NSString*)lastModified AndPercentView:(MBProgressHUD *)percentView{
    
    NSString *result = @">>> GET MessageCategoryList : FAIL";
    if ([self getMessageCategoryListWithUserId:userID hotelId:hotelID andLastModified:lastModified AndPercentView:percentView]) {
        result = @">>> GET MessageCategoryList : SUC";
    };
    
    if([LogFileManager isLogConsole])
    {
        NSLog(@"%@",result);
    }
    
    result = @">>> GET MessageItemList : FAIL";
    if ([self getMessageItemListWithUserId:userID andLastModified:lastModified AndPercentView:percentView]) {
        result = @">>> GET MessageItemList : SUC";
    }
    if([LogFileManager isLogConsole])
    {
        NSLog(@"%@",result);
    }
}

-(BOOL)getMessageCategoryListWithUserId:(NSInteger)userId hotelId:(NSInteger)hotelId andLastModified:(NSString *)lastModified AndPercentView:(MBProgressHUD *)percentView{
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole])
    {
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_GetMessageCategoryList2 *request = [[eHousekeepingService_GetMessageCategoryList2 alloc] init];
    
    [request setIntUsrID:[NSNumber numberWithInteger:userId]];
    [request setIntHotelID:[NSNumber numberWithInteger:hotelId]];
    
    //    LogObject *logObj = [[LogObject alloc] init];
    //    logObj.dateTime = [NSDate date];
    //    logObj.log = [NSString stringWithFormat:@"<%i><getMessageCategoryListWithUserId><userID:%i hotelID:%i lastModified:%@>",userId,userId,hotelId,lastModified];
    //    LogFileManager *logManager = [[LogFileManager alloc] init];
    //    [logManager save:logObj];
    
    float currentPercent = 0;
    [self updatePercentView:percentView value:currentPercent description:LOADING_MESSAGE_CATEGORY];
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetMessageCategoryList2UsingParameters:request];
    
    BOOL result = YES;
    
    NSArray *responseBodyParts = response.bodyParts;
    for (id bodyPart in responseBodyParts) {
        
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetMessageCategoryList2Response class]]) {
            eHousekeepingService_GetMessageCategoryList2Response *dataBody = (eHousekeepingService_GetMessageCategoryList2Response *)bodyPart;
            if (dataBody) {
                if ([dataBody.GetMessageCategoryList2Result.ResponseStatus.respCode integerValue] == RESPONSE_STATUS_OK) {
                    
                    //Delete old data
                    [self deleteAllMessageSubjects];
                    
                    NSMutableArray *categories = dataBody.GetMessageCategoryList2Result.MessageCatList.MessageCategoryList2;
                    MessageSubjectModelV2 *msgSubject = [[MessageSubjectModelV2 alloc] init];
                    float stepPercent = [self caculateStepFromCountingObject:(int)[categories count]];
                    for (eHousekeepingService_MessageCategoryList2 *category in categories) {
                        msgSubject.smc = [category.msgCatID integerValue];
                        msgSubject.smcName = category.msgCatName;
                        msgSubject.smcNameLang = category.msgCatNameLang;
                        msgSubject.smcImage = category.msgCatPhoto;
                        
                        NSInteger statusInsert = [self insertMsgSubjectData:msgSubject];
                        if (statusInsert == 0) {
                            //update message subject
                            [self updateMsgSubjectData:msgSubject];
                        }
                        
                        [self updatePercentView:percentView value:currentPercent description:LOADING_MESSAGE_CATEGORY];
                        currentPercent += stepPercent;
                    }
                }
                else
                    result = NO;
            }
            else
                result = NO;
        }
        else
            result = NO;
    }
    return  result;
}

-(BOOL)getMessageItemListWithUserId:(NSInteger)userId andLastModified:(NSString *)lastModified AndPercentView:(MBProgressHUD *)percentView{
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole])
    {
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_GetMessageItemList2 *request = [[eHousekeepingService_GetMessageItemList2 alloc] init];
    
    [request setIntUsrID:[NSNumber numberWithInteger:userId]];
    
    //    LogObject *logObj = [[LogObject alloc] init];
    //    logObj.dateTime = [NSDate date];
    //    logObj.log = [NSString stringWithFormat:@"<%i><getMessageItemListWithUserId><userID:%i lastModified:%@>",userId,userId,lastModified];
    //    LogFileManager *logManager = [[LogFileManager alloc] init];
    //    [logManager save:logObj];
    
    float currentPercent = 0;
    
    [self updatePercentView:percentView value:currentPercent description:LOADING_MESSAGE_ITEMS];
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetMessageItemList2UsingParameters:request];
    
    BOOL result = YES;
    
    NSArray *responseBodyParts = response.bodyParts;
    for (id bodyPart in responseBodyParts) {
        
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetMessageItemList2Response class]]) {
            eHousekeepingService_GetMessageItemList2Response *dataBody = (eHousekeepingService_GetMessageItemList2Response *)bodyPart;
            if (dataBody) {
                if ([dataBody.GetMessageItemList2Result.ResponseStatus.respCode integerValue] == RESPONSE_STATUS_OK) {
                    
                    //Delete old data
                    [self deleteAllMessageItems];
                    NSMutableArray *items = dataBody.GetMessageItemList2Result.MessageItmList.MessageItemList2;
                    MessageSubjectItemModelV2 *msgItem = [[MessageSubjectItemModelV2 alloc] init];
                    
                    float stepPercent = [self caculateStepFromCountingObject:(int)[items count]];
                    for (eHousekeepingService_MessageItemList2 *item in items) {
                        msgItem.smiId = [item.msgItmID integerValue];
                        msgItem.smiCategoryId = [item.msgItmCatID integerValue];
                        msgItem.smiName = item.msgItmName;
                        msgItem.smiNameLang = item.msgItmNameLang;
                        msgItem.smiImage = item.msgItmPhoto;
                        
                        NSInteger statusInsert = [self insertMsgSubjectItemData:msgItem];
                        if (statusInsert == 0) {
                            [self updateMsgSubjectItemData:msgItem];
                        }
                        [self updatePercentView:percentView value:currentPercent description:LOADING_MESSAGE_ITEMS];
                        currentPercent += stepPercent;
                    }
                }
                else
                    result = NO;
            }
            else
                result = NO;
        }
        else
            result = NO;
    }
    return result;
}
- (NSString *) uniqueAnumberInString:(NSString *) StringNumber AndUserID: (NSString *)userID {
    NSArray *listItems = [StringNumber componentsSeparatedByString:@","];
    NSMutableArray *unique = [NSMutableArray array];
    for (id obj in listItems) {
        if (![unique containsObject:obj]) {
            
            [unique addObject:obj];
            if([obj isEqualToString:userID]) {
                [unique removeObject:obj];
            }
        }
    }
    StringNumber = [unique componentsJoinedByString:@","];
    return StringNumber;
}
// New post message Jul-12
- (NSInteger)postMessageDetailsModel:(MessageModelV2*)message receivers:(NSMutableArray*)messageReceiverList photos:(NSMutableArray*)messagePhotos{
    
    if (messageReceiverList.count > 0) {
        //        int result = -1;
        NSString* receiverList = [NSString stringWithFormat:@"%@",[(UserDetailsModelV2*)[messageReceiverList objectAtIndex:0] userDetailsId]];
        NSString* userCurrentID = [NSString stringWithFormat:@"%ld",(long)[(UserDetailsModelV2*)[messageReceiverList objectAtIndex:0] userId]];
        for (int i=1; i < messageReceiverList.count; i++) {
            UserDetailsModelV2* aReceiver = [messageReceiverList objectAtIndex:i];
            receiverList = [NSString stringWithFormat:@"%@,%@", receiverList,aReceiver.userDetailsId];
        }
        //receiverList = [self uniqueAnumberInString:receiverList AndUserID:userCurrentID];
        NSInteger  messageGroupID = -1;
        NSInteger result = 0;
        //        result = [self postMessageWithUserID:message.msg_Sender_ID andIntSendToID:receiverList andStrTopic:message.selectedSubjectItem.smiName andStrContent:message.msg_Contents andIntHotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId andStrTransactionTime:message.msg_Last_Modified];
        eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
        eHousekeepingService_PostMessage *request = [[eHousekeepingService_PostMessage alloc] init];
        
        if([LogFileManager isLogConsole]){
            [binding setLogXMLInOut:YES];
        }
        
        if (message.message_owner_id) {
            [request setIntUsrID:[NSNumber numberWithInteger:message.message_owner_id]];
        }
        
        if (receiverList) {
            [request setStrSendToID:receiverList];
        }
        
        if (message.message_topic.length > 0) {
            [request setStrTopic:message.message_topic];
        }
        
        if(message.message_content){
            [request setStrContent:message.message_content];
        }
        
        if([UserManagerV2 sharedUserManager].currentUser.userHotelsId > 0){
            [request setIntHotelId:[NSNumber numberWithInt:[UserManagerV2 sharedUserManager].currentUser.userHotelsId]];
        }
        
        if(message.message_last_modified){
            [request setStrTransactionTime:message.message_last_modified];
        }
        
        LogObject *logObj = [[LogObject alloc] init];
        logObj.dateTime = [NSDate date];
        logObj.log = [NSString stringWithFormat:@"<%i><postMessage><intUsrID:%i strSendToID:%@ strTopic:%@ strContent:%@ intHotelId:%i strTransactionTime:%@>",(int)message.message_owner_id,(int)message.message_owner_id,receiverList,message.message_topic,message.message_content,[UserManagerV2 sharedUserManager].currentUser.userHotelsId,message.message_last_modified];
        LogFileManager *logManager = [[LogFileManager alloc] init];
        [logManager save:logObj];
        
        eHousekeepingServiceSoap12BindingResponse *response = [binding PostMessageUsingParameters:request];
        
        NSArray *responseBodyParts = response.bodyParts;
        for (id bodyPart in responseBodyParts) {
            
            if ([bodyPart isKindOfClass:[eHousekeepingService_PostMessageResponse class]]) {
                eHousekeepingService_PostMessageResponse *dataBody = (eHousekeepingService_PostMessageResponse *)bodyPart;
                if (dataBody) {
                    if ([dataBody.PostMessageResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_NEW_RECORD_ADDED]]) {
                        // update follow new msg
                        eHousekeepingService_ArrayOfGroupAdHocMessage* listmessage  = dataBody.PostMessageResult.GrpAdHocMsg;
                        if (listmessage.GroupAdHocMessage) {
                            for (eHousekeepingService_GroupAdHocMessage* aResult in listmessage.GroupAdHocMessage) {
                                if (aResult.gadGrpID /*&& aResult.gadID*/) {
                                    //                                    message.msg_ID = [aResult.gadID intValue];
                                    message.message_group_id = [aResult.gadGrpID integerValue];
                                    // post message
                                    if(messagePhotos.count > 0) {
                                        result =   [self postMessageAttachPhoto:messagePhotos andMsgGroupID:[aResult.gadGrpID intValue]];
                                    }
                                }
                            }
                            eHousekeepingService_GroupAdHocMessage* aResult = [listmessage.GroupAdHocMessage objectAtIndex:0];
                            if (listmessage.GroupAdHocMessage.count > 1) {
                                messageGroupID =  [aResult.gadGrpID integerValue];
                            } else {
                                //                                messageID = [aResult.gadID intValue];
                                messageGroupID =  [aResult.gadGrpID integerValue];
                            }
                        }
                    }
                }
            }
        }
        if([LogFileManager isLogConsole])
        {
            NSLog(@"send message");
        }
        
        return messageGroupID;
    }
    return -1;
}
// New post message Jul-12
- (NSInteger)postMessage:(MessageModelV2*)message receivers:(NSMutableArray*)messageReceiverList photos:(NSMutableArray*)messagePhotos{
    
    if (messageReceiverList.count > 0) {
        //        int result = -1;
        NSString* receiverList = [NSString stringWithFormat:@"%d",(int)[(UserListModelV2*)[messageReceiverList objectAtIndex:0] userListId]];
        for (int i=1; i < messageReceiverList.count; i++) {
            UserListModelV2* aReceiver = [messageReceiverList objectAtIndex:i];
            receiverList = [NSString stringWithFormat:@"%@,%d", receiverList, (int)aReceiver.userListId];
        }
        
        NSInteger  messageGroupID = -1;
        NSInteger result = 0;
        //        result = [self postMessageWithUserID:message.msg_Sender_ID andIntSendToID:receiverList andStrTopic:message.selectedSubjectItem.smiName andStrContent:message.msg_Contents andIntHotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId andStrTransactionTime:message.msg_Last_Modified];
        eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
        eHousekeepingService_PostMessage *request = [[eHousekeepingService_PostMessage alloc] init];
        
        if([LogFileManager isLogConsole]){
            [binding setLogXMLInOut:YES];
        }
        
        if (message.message_owner_id) {
            [request setIntUsrID:[NSNumber numberWithInteger:message.message_owner_id]];
        }
        
        if (receiverList) {
            [request setStrSendToID:receiverList];
        }
        
        if (message.message_topic.length > 0) {
            [request setStrTopic:message.message_topic];
        }
        
        if(message.message_content){
            [request setStrContent:message.message_content];
        }
        
        if([UserManagerV2 sharedUserManager].currentUser.userHotelsId > 0){
            [request setIntHotelId:[NSNumber numberWithInt:[UserManagerV2 sharedUserManager].currentUser.userHotelsId]];
        }
        
        if(message.message_last_modified){
            [request setStrTransactionTime:message.message_last_modified];
        }
        
        LogObject *logObj = [[LogObject alloc] init];
        logObj.dateTime = [NSDate date];
        logObj.log = [NSString stringWithFormat:@"<%i><postMessage><intUsrID:%i strSendToID:%@ strTopic:%@ strContent:%@ intHotelId:%i strTransactionTime:%@>",(int)message.message_owner_id,(int)message.message_owner_id,receiverList,message.message_topic,message.message_content,[UserManagerV2 sharedUserManager].currentUser.userHotelsId,message.message_last_modified];
        LogFileManager *logManager = [[LogFileManager alloc] init];
        [logManager save:logObj];
        
        eHousekeepingServiceSoap12BindingResponse *response = [binding PostMessageUsingParameters:request];
        
        NSArray *responseBodyParts = response.bodyParts;
        for (id bodyPart in responseBodyParts) {
            
            if ([bodyPart isKindOfClass:[eHousekeepingService_PostMessageResponse class]]) {
                eHousekeepingService_PostMessageResponse *dataBody = (eHousekeepingService_PostMessageResponse *)bodyPart;
                if (dataBody) {
                    if ([dataBody.PostMessageResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_NEW_RECORD_ADDED]]) {
                        // update follow new msg
                        eHousekeepingService_ArrayOfGroupAdHocMessage* listmessage  = dataBody.PostMessageResult.GrpAdHocMsg;
                        if (listmessage.GroupAdHocMessage) {
                            for (eHousekeepingService_GroupAdHocMessage* aResult in listmessage.GroupAdHocMessage) {
                                if (aResult.gadGrpID /*&& aResult.gadID*/) {
                                    //                                    message.msg_ID = [aResult.gadID intValue];
                                    message.message_group_id = [aResult.gadGrpID integerValue];
                                    // post message
                                    if(messagePhotos.count > 0) {
                                        result =   [self postMessageAttachPhoto:messagePhotos andMsgGroupID:[aResult.gadGrpID intValue]];
                                    }
                                }
                            }
                            eHousekeepingService_GroupAdHocMessage* aResult = [listmessage.GroupAdHocMessage objectAtIndex:0];
                            if (listmessage.GroupAdHocMessage.count > 1) {
                                messageGroupID =  [aResult.gadGrpID integerValue];
                            } else {
                                //                                messageID = [aResult.gadID intValue];
                                messageGroupID =  [aResult.gadGrpID integerValue];
                            }
                        }
                    }
                }
            }
        }
        if([LogFileManager isLogConsole])
        {
            NSLog(@"send message");
        }
        
        return messageGroupID;
    }
    return -1;
}
/*
 -(NSInteger)postMessageWithUserID:(NSInteger)intUsrID
 andIntSendToID:(NSInteger)intSendToID
 andStrTopic:(NSString*)strTopic
 andStrContent:(NSString*)strContent
 andIntHotelId:(NSInteger)intHotelId
 andStrTransactionTime:(NSString*)strTransactionTime
 {
 NSInteger  messageID = 0;
 eHousekeepingServiceSoap12Binding *binding = [eHousekeepingServiceSvc eHousekeepingServiceSoap12Binding];
 eHousekeepingServiceSvc_PostMessage *request = [[eHousekeepingServiceSvc_PostMessage alloc] init];
 
 if (intUsrID) {
 [request setIntUsrID:[NSNumber numberWithInt:intUsrID]];
 }
 if (intSendToID) {
 [request setIntSendToID:[NSNumber numberWithInt:intSendToID]];
 }
 if (strTopic) {
 [request setStrTopic:strTopic];
 }
 
 if(strContent){
 [request setStrContent:strContent];
 }
 
 if(intHotelId){
 [request setIntHotelId:[NSNumber numberWithInt:intHotelId]];
 }
 
 if(strTransactionTime){
 [request setStrTransactionTime:strTransactionTime];
 }
 
 eHousekeepingServiceSoap12BindingResponse *response = [binding PostMessageUsingParameters:request];
 
 NSArray *responseBodyParts = response.bodyParts;
 for (id bodyPart in responseBodyParts) {
 
 if ([bodyPart isKindOfClass:[eHousekeepingServiceSvc_PostMessageResponse class]]) {
 eHousekeepingServiceSvc_PostMessageResponse *dataBody = (eHousekeepingServiceSvc_PostMessageResponse *)bodyPart;
 if (dataBody) {
 NSLog(@"response sent = %d", [dataBody.PostMessageResult.respCode intValue]);
 if ([dataBody.PostMessageResult.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_NEW_RECORD_ADDED]]) {
 
 messageID = 1;
 }
 NSLog(@"1- %@", [dataBody.attributes description]);
 NSLog(@"2 - %@", dataBody.PostMessageResult.respMsg);
 }
 }
 }
 
 return messageID;
 }
 */

// Post message photo with msg id

-(NSInteger)postMessageAttachPhotoWithMsgId:(NSInteger)msgID {
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    eHousekeepingService_PostMessageAttachPhoto *request = [[eHousekeepingService_PostMessageAttachPhoto alloc] init];
    
    if(msgID){
        //        [request setIntMessageID:[NSNumber numberWithInt:msgID]];
        [request setIntGrpMsgID:[NSNumber numberWithInteger:msgID]];
    }
    
    NSInteger _userID = [[UserManagerV2 sharedUserManager] currentUser].userId;
    
    if(_userID){
        [request setIntUsrID:[NSNumber numberWithInteger:_userID]];
    }
    
    LogObject *logObj = [[LogObject alloc] init];
    logObj.dateTime = [NSDate date];
    logObj.log = [NSString stringWithFormat:@"<%i><postMessageAttachPhotoWithMsgId><intGrpMsgID:%i intUsrID:%i>",(int)_userID,(int)msgID,(int)_userID];
    LogFileManager *logManager = [[LogFileManager alloc] init];
    [logManager save:logObj];
    
    // load all msg photo by msg id -- loadAllMsgPhotoDataByMessageId
    NSMutableArray *listPhoto = nil;
    //NSMutableArray *listPhoto = [[AdHocMessageManagerV2 sharedAdHocMessageManagerV2] loadAllMsgPhotoDataByMessageId:msgID];
    
    // MessagePhotoModelV2
    for (MessagePhotoModelV2 *msgPhotoModel in listPhoto) {
        [request setBinPhoto:msgPhotoModel.message_photo];
        eHousekeepingServiceSoap12BindingResponse *response = [binding PostMessageAttachPhotoUsingParameters:request];
        
        NSArray *responseBodyParts = response.bodyParts;
        for (id bodyPart in responseBodyParts) {
            if ([bodyPart isKindOfClass:[eHousekeepingService_PostMessageAttachPhotoResponse class]]){
                eHousekeepingService_PostMessageAttachPhotoResponse *data = (eHousekeepingService_PostMessageAttachPhotoResponse *)bodyPart;
                if ([data.PostMessageAttachPhotoResult.respCode integerValue] == RESPONSE_STATUS_NEW_RECORD_ADDED) {
                    return YES;
                }
            }
        }
        
    }
    
    return NO;
}


- (NSInteger)postMessageAttachPhoto:(NSMutableArray*)photoList andMsgGroupID:(int)groupId {
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    eHousekeepingService_PostMessageAttachPhoto *request = [[eHousekeepingService_PostMessageAttachPhoto alloc] init];
    if([LogFileManager isLogConsole]){
        [binding setLogXMLInOut:YES];
    }
    
    [request setIntGrpMsgID:[NSNumber numberWithInteger:groupId]];
    NSInteger result = 0;
    
    NSInteger _userID = [[UserManagerV2 sharedUserManager] currentUser].userId;
    
    if(_userID > 0){
        [request setIntUsrID:[NSNumber numberWithInteger:_userID]];
    }
    
    LogObject *logObj = [[LogObject alloc] init];
    logObj.dateTime = [NSDate date];
    logObj.log = [NSString stringWithFormat:@"<%i><postMessageAttachPhotoWithMessage><intGrpMsgID:%i intUsrID:%i>",(int)_userID,groupId,(int)_userID];
    LogFileManager *logManager = [[LogFileManager alloc] init];
    [logManager save:logObj];
    
    // MessagePhotoModelV2
    for (MessagePhotoModelV2 *msgPhotoModel in photoList) {
        msgPhotoModel.message_photo_group_id = groupId;
        [request setBinPhoto:msgPhotoModel.message_photo];
        eHousekeepingServiceSoap12BindingResponse *response = [binding PostMessageAttachPhotoUsingParameters:request];
        
        NSArray *responseBodyParts = response.bodyParts;
        for (id bodyPart in responseBodyParts) {
            if ([bodyPart isKindOfClass:[eHousekeepingService_PostMessageAttachPhotoResponse class]]){
                eHousekeepingService_PostMessageAttachPhotoResponse* dataBody = (eHousekeepingService_PostMessageAttachPhotoResponse*) bodyPart;
                if (dataBody) {
                    if ([dataBody.PostMessageAttachPhotoResult.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_NEW_RECORD_ADDED]]) {
                        result = 1;
                    }
                }
            }
        }
        
    }
    
    return result;
}



-(NSInteger)getUserIdFromUserName:(NSString*)userName andSPUserID: (NSInteger)userID andHotelID:(NSInteger)hotelID andLastModified:(NSString*)lastModified
{
    NSInteger _userID = 0;
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    eHousekeepingService_GetUserList *request = [[eHousekeepingService_GetUserList alloc] init];
    if (userID) {
        [request setIntUsrID:[NSNumber numberWithInteger:userID]];
    }
    if (hotelID) {
        [request setIntHotelID:[NSNumber numberWithInteger:hotelID]];
    }
    
    if (lastModified) {
        [request setStrLastModified:lastModified];
    }
    
    //    LogObject *logObj = [[LogObject alloc] init];
    //    logObj.dateTime = [NSDate date];
    //    logObj.log = [NSString stringWithFormat:@"<%i><getUserIdFromUserName><intUsrID:%i hotelID:%i lastModified:%@>",[NSNumber numberWithInt:userID],[NSNumber numberWithInt:userID],hotelID,lastModified];
    //    LogFileManager *logManager = [[LogFileManager alloc] init];
    //    [logManager save:logObj];
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetUserListUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    for (id bodyPart in responseBodyParts) {
        //        NSLog(@"bodyPart %@",[bodyPart class]);
        
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetUserListResponse class]]) {
            eHousekeepingService_GetUserListResponse *dataBody = (eHousekeepingService_GetUserListResponse *)bodyPart;
            if (dataBody) {
                if ([dataBody.GetUserListResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]]) {
                    
                    NSMutableArray *userListObject = dataBody.GetUserListResult.UserDetailList.UserDetail;
                    
                    for (eHousekeepingService_UserDetail *userDetail in userListObject) {
                        if([userDetail.usrName isEqualToString:userName]){
                            _userID = [userDetail.usrID intValue];
                            
                        }
                        
                    }
                    
                }
            }
        }
    }
    
    return _userID;
}

// Get Message Photo from ws
-(int)getMessagePhotoWithUserID:(int)userId andGroupId:(NSInteger)groupId {
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    if([LogFileManager isLogConsole]){
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_GetMessageAttachPhoto* request = [[eHousekeepingService_GetMessageAttachPhoto alloc] init];
    if (userId > 0) {
        [request setIntUsrID:[NSNumber numberWithInt:userId]];
    } else {
        return 0;
    }
    
    if (groupId > 0) {
        [request setIntGrpMsgID:[NSNumber numberWithInteger:groupId]];
    } else {
        return 0;
    }
    
    int countMessagePhoto = [self countMessagePhotoByGroupId:(int)groupId];
    if(countMessagePhoto > 0){
        return countMessagePhoto;
    }
    
    //    LogObject *logObj = [[LogObject alloc] init];
    //    logObj.dateTime = [NSDate date];
    //    logObj.log = [NSString stringWithFormat:@"<%i><getMessagePhotoWithUserID><intUsrID:%i messageId:%i>",user.userId,user.userId,messageId];
    //    LogFileManager *logManager = [[LogFileManager alloc] init];
    //    [logManager save:logObj];
    
    eHousekeepingServiceSoap12BindingResponse* response = [binding GetMessageAttachPhotoUsingParameters:request];
    NSArray *responseBodyParts = response.bodyParts;
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetMessageAttachPhotoResponse class]]) {
            eHousekeepingService_GetMessageAttachPhotoResponse* dataBody = (eHousekeepingService_GetMessageAttachPhotoResponse*) bodyPart;
            if (dataBody) {
                if ([dataBody.GetMessageAttachPhotoResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]]) {
                    NSArray* listPhoto = dataBody.GetMessageAttachPhotoResult.AdHocMessagePhoto.AdHocPhoto;
                    NSString *todayDateString = [ehkConvert getTodayDateString];
                    countMessagePhoto = 0;
                    for (eHousekeepingService_AdHocPhoto* aPhoto in listPhoto) {
                        MessagePhotoModelV2* aPhotoModel = [[MessagePhotoModelV2 alloc] init];
                        aPhotoModel.message_photo = aPhoto.amaPhoto;
                        aPhotoModel.message_photo_group_id = groupId;
                        aPhotoModel.message_photo_last_modified = todayDateString;
                        [self insertMsgPhotoData:aPhotoModel];
                        countMessagePhoto ++;
                    }
                }
            }
            
        }
        //            eHousekeepingServiceSvc_GetNewMessageResponse *dataBody = (eHousekeepingServiceSvc_GetNewMessageResponse *)bodyPart;
    }
    return countMessagePhoto;
}

#pragma mark GetReceivedAdHocMessageRoutine

// DungPhan - 20151008: new API for Get Received AdHoc Message
- (BOOL)getReceivedAdHocMessageRoutineByUserId:(int)userId groupMessageId:(int)groupMessageId hotelId:(int)hotelId lastModified:(NSString*)lastModified {
    
    //No network detected
    if (![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        return NO;
    }
    
    //Required Fields
    if (userId <= 0) {return NO;}
    if (hotelId <= 0) {return NO;}
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if ([LogFileManager isLogConsole]) {
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_GetReceivedAdHocMessageRoutine *request = [[eHousekeepingService_GetReceivedAdHocMessageRoutine alloc] init];
    [request setPropertyID:[NSString stringWithFormat:@"%d", hotelId]];
    [request setUserID:[NSString stringWithFormat:@"%d", userId]];
    
    //Optional fields
    if (groupMessageId > 0) {
        [request setGroupMessageID:[NSString stringWithFormat:@"%d", groupMessageId]];
    }
    
    if (lastModified.length > 0) {
        [request setLastModifiedDateTime:lastModified];
    }
    
    BOOL result = NO;
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetReceivedAdHocMessageRoutineUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetReceivedAdHocMessageRoutineResponse class]]) {
            eHousekeepingService_GetReceivedAdHocMessageRoutineResponse *dataBody = (eHousekeepingService_GetReceivedAdHocMessageRoutineResponse *)bodyPart;
            if (dataBody != nil) {
                if ([dataBody.GetReceivedAdHocMessageRoutineResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_OK]]
                    || [dataBody.GetReceivedAdHocMessageRoutineResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_NO_RERCORD_TO_DISPLAY]]) {
                    
                    result = YES;
                    
                    NSMutableArray *listResults = dataBody.GetReceivedAdHocMessageRoutineResult.AdHocMessageList.AdHocMessageList2;
                    for (eHousekeepingService_AdHocMessageList2 *wsCurMessage in listResults) {
                        bool hasMessage = [self hasMessageByOwnerId:userId messageId:[wsCurMessage.amsgID intValue]];
                        if (!hasMessage) {
                            
                            MessageModelV2 *curMessage = [[MessageModelV2 alloc] init];
                            // DungPhan - 20150929: Fix can't insert message with single quote symbol to db
                            //                            curMessage.message_content = wsCurMessage.amsgContent;
                            curMessage.message_content =[wsCurMessage.amsgContent stringByReplacingOccurrencesOfString:@ "'" withString: @"''"];
                            curMessage.message_count_photo_attached = [wsCurMessage.amsgPhotoAttached intValue];
                            curMessage.message_from_user_login_name = wsCurMessage.amsgSendFrom;
                            curMessage.message_from_user_name = wsCurMessage.amsgSendFromLang;
                            curMessage.message_group_id = [wsCurMessage.amsgGrpID intValue];
                            curMessage.message_id = [wsCurMessage.amsgID intValue];
                            curMessage.message_kind = MESSAGE_KIND_INBOX;
                            curMessage.message_last_modified = wsCurMessage.amsgLastModified;
                            curMessage.message_owner_id = userId;
                            curMessage.message_status = [wsCurMessage.amsgStatus intValue];
                            curMessage.message_topic = wsCurMessage.amsgTopic;
                            [self insertMessage:curMessage];
                        }
                    }
                }
            }
        }
    }
    return result;
}

#pragma mark getSendAdHocMessageRoutine

// DungPhan - 20151008: new API for Get Send AdHoc Message
- (BOOL)getSendAdHocMessageRoutineByUserId:(int)userId groupMessageId:(int)groupMessageId hotelId:(int)hotelId lastModified:(NSString*)lastModified {
    
    //No network detected
    if (![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        return NO;
    }
    
    //Required Fields
    if (userId <= 0) {return NO;}
    if (hotelId <= 0) {return NO;}
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if ([LogFileManager isLogConsole]) {
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_GetSendAdHocMessageRoutine *request = [[eHousekeepingService_GetSendAdHocMessageRoutine alloc] init];
    [request setPropertyID:[NSString stringWithFormat:@"%d", hotelId]];
    [request setUserID:[NSString stringWithFormat:@"%d", userId]];
    
    //Optional fields
    if (groupMessageId > 0) {
        [request setGroupMessageID:[NSString stringWithFormat:@"%d", groupMessageId]];
    }
    
    if (lastModified.length > 0) {
        [request setLastModifiedDateTime:lastModified];
    }
    
    BOOL result = NO;
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetSendAdHocMessageRoutineUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetSendAdHocMessageRoutineResponse class]]) {
            eHousekeepingService_GetSendAdHocMessageRoutineResponse *dataBody = (eHousekeepingService_GetSendAdHocMessageRoutineResponse *)bodyPart;
            if (dataBody != nil) {
                if ([dataBody.GetSendAdHocMessageRoutineResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_OK]]
                    || [dataBody.GetSendAdHocMessageRoutineResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_NO_RERCORD_TO_DISPLAY]]) {
                    
                    result = YES;
                    
                    NSMutableArray *listResults = dataBody.GetSendAdHocMessageRoutineResult.AdHocMessageList.AdHocMessageList2;
                    NSString *todayDateString = [ehkConvert getTodayDateString];
                    
                    for (eHousekeepingService_AdHocMessageList2 *wsCurMessage in listResults) {
                        
                        BOOL hasMessage = [self hasMessageByOwnerId:userId messageId:[wsCurMessage.amsgID intValue]];
                        if (!hasMessage) {
                            
                            MessageModelV2 *curMessage = [[MessageModelV2 alloc] init];
                            // DungPhan - 20150929: Fix can't insert message with single quote symbol to db
                            // curMessage.message_content = wsCurMessage.amsgContent;
                            curMessage.message_content = [wsCurMessage.amsgContent stringByReplacingOccurrencesOfString:@ "'" withString: @"''"];
                            curMessage.message_count_photo_attached = [wsCurMessage.amsgPhotoAttached intValue];
                            curMessage.message_from_user_login_name = wsCurMessage.amsgSendFrom;
                            curMessage.message_from_user_name = wsCurMessage.amsgSendFromLang;
                            curMessage.message_group_id = [wsCurMessage.amsgGrpID intValue];
                            curMessage.message_id = [wsCurMessage.amsgID intValue];
                            curMessage.message_kind = MESSAGE_KIND_SENT;
                            curMessage.message_last_modified = wsCurMessage.amsgLastModified;
                            curMessage.message_owner_id = userId;
                            curMessage.message_status = MESSAGE_READ;
                            curMessage.message_topic = wsCurMessage.amsgTopic;
                            [self insertMessage:curMessage];
                            
                            MessageReceiverModelV2 *receiver = [[MessageReceiverModelV2 alloc] init];
                            receiver.message_receiver_group_id = [wsCurMessage.amsgGrpID intValue];
                            receiver.message_receiver_id = [wsCurMessage.amsgReceivedIDs intValue];
                            receiver.message_receiver_owner_id = userId;
                            receiver.message_receiver_last_modified = todayDateString;
                            
                            BOOL isExisted  = [self checkMessageReceiverIfExisted:receiver];
                            if (!isExisted) {
                                [self insertMessageReceiverData:receiver];
                            }
                        }
                    }
                }
            }
        }
    }
    return result;
}

#pragma mark GetNoOfAdHocMessageRoutine

// DungPhan - 20151008: new API for Get No Of AdHoc Message
- (BOOL)getNoOfAdHocMessageRoutineByUserId:(int)userId groupMessageId:(int)groupMessageId hotelId:(int)hotelId lastModified:(NSString*)lastModified {
    
    //No network detected
    if (![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        return NO;
    }
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole]){
        [binding setLogXMLInOut:YES];
    }
    
    //Require fields
    if (hotelId <= 0) {return NO;}
    if (userId <= 0) {return NO;}
    
    eHousekeepingService_GetNoOfAdHocMessageRoutine *request = [[eHousekeepingService_GetNoOfAdHocMessageRoutine alloc] init];
    
    //Require fields
    [request setPropertyID:[NSString stringWithFormat:@"%d", hotelId]];
    [request setUserID:[NSString stringWithFormat:@"%d", userId]];
    
    //Option fields
    if (groupMessageId > 0) {
        [request setGroupMessageID:[NSString stringWithFormat:@"%d",groupMessageId]];
    }
    
    if (lastModified.length > 0) {
        [request setLastModifiedDateTime:lastModified];
    }
    
    BOOL result = NO;
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetNoOfAdHocMessageRoutineUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetNoOfAdHocMessageRoutineResponse class]]) {
            eHousekeepingService_GetNoOfAdHocMessageRoutineResponse *dataBody = (eHousekeepingService_GetNoOfAdHocMessageRoutineResponse *)bodyPart;
            if (dataBody != nil) {
                if (dataBody.GetNoOfAdHocMessageRoutineResult != nil) {
                    result = YES;
                    eHousekeepingService_AdHocMessageCounter *messageCounter = dataBody.GetNoOfAdHocMessageRoutineResult;
                    if (messageCounter.intReadReceived != nil) {
                        [[UserManagerV2 sharedUserManager] setCurrentUserConfig:USER_CONFIG_MESSAGE_READ_RECEIVED value:[NSString stringWithFormat:@"%d", [messageCounter.intReadReceived intValue]]];
                    }
                    
                    if (messageCounter.intSent != nil) {
                        [[UserManagerV2 sharedUserManager] setCurrentUserConfig:USER_CONFIG_MESSAGE_SENT value:[NSString stringWithFormat:@"%d", [messageCounter.intSent intValue]]];
                    }
                    
                    if (messageCounter.intUnreadReceive != nil) {
                        NSString *countOldValue = [[UserManagerV2 sharedUserManager] getCurrentUserConfigValueByKey:USER_CONFIG_MESSAGE_UNREAD_RECEIVED];
                        if ([countOldValue intValue] < [messageCounter.intUnreadReceive intValue]) {
                            [[UserManagerV2 sharedUserManager] setCurrentUserConfig:USER_CONFIG_MESSAGE_IS_SHOW_NOTIFICATION value:@"true"];
                        }
                        [[UserManagerV2 sharedUserManager] setCurrentUserConfig:USER_CONFIG_MESSAGE_UNREAD_RECEIVED value:[NSString stringWithFormat:@"%d", [messageCounter.intUnreadReceive intValue]]];
                    }
                }
            }
        }
    }
    return result;
}

#pragma mark PostUpdateAdHocMessageRoutine

// DungPhan - 20151008: new API for Update AdHoc Message
- (BOOL)postUpdateAdHocMessageRoutineWithUserId:(int)userId messageUpdateDetails:(NSMutableArray*)messageUpdateDetails updateType:(int)updateType {
     //Status 1 = read, status 0 = unread
    //No network detected
    if (![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        return NO;
    }
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if ([LogFileManager isLogConsole]) {
        [binding setLogXMLInOut:YES];
    }
    
    //Require fields
    if (userId <= 0) {return NO;}
    
    eHousekeepingService_PostUpdateAdHocMessageRoutine *request = [[eHousekeepingService_PostUpdateAdHocMessageRoutine alloc] init];
    
    eHousekeepingService_ArrayOfMessageUpdateDetails *msgUpdateListing = [[eHousekeepingService_ArrayOfMessageUpdateDetails alloc] init];
    for (int i = 0; i < [messageUpdateDetails count]; i++) {
        MessageModelV2 *msgModel = [messageUpdateDetails objectAtIndex:i];
        eHousekeepingService_MessageUpdateDetails *curDetails = [[eHousekeepingService_MessageUpdateDetails alloc] init];
        if (msgModel) {
            [curDetails setPropertyID:[NSString stringWithFormat:@"%d",[UserManagerV2 sharedUserManager].currentUser.userHotelsId]];
            [curDetails setUserID:[NSString stringWithFormat:@"%d", userId]];
            [curDetails setMessageID:[NSString stringWithFormat:@"%d", (int)msgModel.message_id]];
            [curDetails setUpdateType:[NSString stringWithFormat:@"%d", updateType]];
        }
        [msgUpdateListing.MessageUpdateDetails addObject:curDetails];
    }
    request.MessageUpdateListing = msgUpdateListing;
    
    //Require fields
    
    BOOL result = NO;
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding PostUpdateAdHocMessageRoutineUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_PostUpdateAdHocMessageRoutineResponse class]]) {
            eHousekeepingService_PostUpdateAdHocMessageRoutineResponse *dataBody = (eHousekeepingService_PostUpdateAdHocMessageRoutineResponse *)bodyPart;
            if (dataBody != nil) {
                if ([dataBody.PostUpdateAdHocMessageRoutineResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_OK]]) {
                    
                    result = YES;
                }
            }
        }
    }
    return result;
}

#pragma mark
#pragma mark local database

//Hao Tran[20130927] - MESSAGE LOCAL
-(int)deleteMessageByOwnerId:(int) ownerId beforeDate:(NSString*)dateString
{
    if(ownerId <= 0){
        return 0;
    }
    
    DataTable *tableDelete = [[DataTable alloc] initWithNameFormat:@"%@",MESSAGE_TABLE];
    NSString *queryDelete = [NSString stringWithFormat:@"Delete from %@ Where %@ = %d and %@ < '%@'", MESSAGE_TABLE, message_owner_id, ownerId, message_last_modified, dateString];
    int status = (int)[tableDelete excuteQueryNonSelect:queryDelete];
    
    return status;
}

-(int)deleteMessagePhotoBeforeDate:(NSString*)dateString
{
    DataTable *tableDelete = [[DataTable alloc] initWithNameFormat:@"%@",MESSAGE_PHOTOS];
    NSString *queryDelete = [NSString stringWithFormat:@"Delete from %@ Where %@ < '%@'", MESSAGE_PHOTOS, message_photo_last_modified, dateString];
    int status = (int)[tableDelete excuteQueryNonSelect:queryDelete];
    
    return status;
}

-(int)deleteMessageReceiverByOwnerId:(int)ownerId beforeDate:(NSString*)dateString
{
    if(ownerId <= 0){
        return 0;
    }
    
    DataTable *tableDelete = [[DataTable alloc] initWithNameFormat:@"%@",MESSAGE_RECEIVER];
    NSString *queryDelete = [NSString stringWithFormat:@"Delete from %@ Where %@ = %d and %@ < '%@'", MESSAGE_RECEIVER, message_receiver_owner_id, ownerId, message_receiver_last_modified, dateString];
    int status = (int)[tableDelete excuteQueryNonSelect:queryDelete];
    
    return status;
}

//Hao Tran[20130927] - END

// check exist
-(BOOL)isExistUserListData:(UserListModelV2*) userListData {
    BOOL returnCode=NO;
    UserListAdapterV2 *adapterUserList=[[UserListAdapterV2 alloc] init];
    [adapterUserList openDatabase];
    [adapterUserList resetSqlCommand];
    returnCode=[adapterUserList isExistUserListData:userListData];
    [adapterUserList close];
    return returnCode;
    
}

// UserListAdapter
-(int)insertUserListData:(UserListModelV2*) userListData
{
    int returnCode=0;
    UserListAdapterV2 *adapterUserList=[[UserListAdapterV2 alloc] init];
    [adapterUserList openDatabase];
    [adapterUserList resetSqlCommand];
    returnCode=[adapterUserList insertUserListData:userListData];
    [adapterUserList close];
    //    [adapterUserList release];
    return returnCode;
}
-(int)updateUserListData:(UserListModelV2*) userListData
{
    int returnCode=0;
    UserListAdapterV2 *adapterUserList=[[UserListAdapterV2 alloc] init];
    [adapterUserList openDatabase];
    [adapterUserList resetSqlCommand];
    returnCode=[adapterUserList updateUserListData:userListData];
    [adapterUserList close];
    //    [adapterUserList release];
    return returnCode;
    
    
}
-(int)deleteUserListData:(UserListModelV2*) userListData
{
    
    int returnCode=0;
    UserListAdapterV2 *adapterUserList=[[UserListAdapterV2 alloc] init];
    [adapterUserList openDatabase];
    [adapterUserList resetSqlCommand];
    returnCode=[adapterUserList deleteUserListData:userListData];
    [adapterUserList close];
    //    [adapterUserList release];
    return returnCode;
}

// FM [20160330] - delete user list in local database
-(int)deleteAllUserListData
{
    
    int returnCode=0;
    UserListAdapterV2 *adapterUserList=[[UserListAdapterV2 alloc] init];
    [adapterUserList openDatabase];
    [adapterUserList resetSqlCommand];
    returnCode=[adapterUserList deleteAllUserListData];
    [adapterUserList close];
    //    [adapterUserList release];
    return returnCode;
}

-(UserListModelV2*)loadUserListDataByUserId:(int)userId
{
    UserListAdapterV2 *adapterUserList=[[UserListAdapterV2 alloc] init];
    [adapterUserList openDatabase];
    [adapterUserList resetSqlCommand];
    UserListModelV2 *userlistObject = [adapterUserList loadUserListDataByUserId:userId];
    [adapterUserList close];
    //    [adapterUserList release];
    return userlistObject;
    
}
-(NSMutableArray *) loadAllUserListDataByUserHotelId:(int) userHotelId
{
    UserListAdapterV2 *adapterUserList=[[UserListAdapterV2 alloc] init];
    [adapterUserList openDatabase];
    [adapterUserList resetSqlCommand];
    NSMutableArray *userlistDataArray=[adapterUserList loadAllUserListDataByUserHotelId:userHotelId];
    [adapterUserList close];
    //    [adapterUserList release];
    return userlistDataArray;
    
    
}
-(NSMutableArray *) loadAllUserListData
{
    UserListAdapterV2 *adapterUserList=[[UserListAdapterV2 alloc] init];
    [adapterUserList openDatabase];
    [adapterUserList resetSqlCommand];
    NSMutableArray *userlistDataArray=[adapterUserList loadAllUserListData];
    [adapterUserList close];
    //    [adapterUserList release];
    return userlistDataArray;
    
}

//load all user list by supervisor id
-(NSMutableArray *)loadAllUserListBySupervisorId:(NSInteger)supervisorId {
    UserListAdapterV2 *adapter = [[UserListAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    NSMutableArray *userlist = [adapter loadAllUserListDataOfSupervisorId:supervisorId];
    [adapter close];
    return userlist;
}

//load user list by user full name or user fullname lang
-(UserListModelV2 *)loadUserListByUserFullName:(NSString *)userFullName {
    UserListAdapterV2 *adapter = [[UserListAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    UserListModelV2 *userlist = [adapter loadUserListByUserName:userFullName];
    [adapter close];
    return userlist;
}
// HUY [20170105] - delete user Details list in local database
//[Add UserDetailsAdapterV2 Start]
-(int)deleteAllUserDetailsData
{
    
    int returnCode=0;
    UserDetailsAdapterV2 *adapterUserList=[[UserDetailsAdapterV2 alloc] init];
    [adapterUserList openDatabase];
    [adapterUserList resetSqlCommand];
    returnCode=[adapterUserList deleteAllUserDetailsData];
    [adapterUserList close];
    //    [adapterUserList release];
    return returnCode;
}
-(UserDetailsModelV2*)loadUserDetailsDataByUserId:(NSString *)userId
{
    UserDetailsAdapterV2 *adapterUserList=[[UserDetailsAdapterV2 alloc] init];
    [adapterUserList openDatabase];
    [adapterUserList resetSqlCommand];
    UserDetailsModelV2 *userlistObject = [adapterUserList loadUserDetailsDataByUserId:userId];
    [adapterUserList close];
    return userlistObject;
    
}
// check exist
-(BOOL)isExistUserDetailsData:(UserDetailsModelV2*) userListData {
    BOOL returnCode=NO;
    UserDetailsAdapterV2 *adapterUserList=[[UserDetailsAdapterV2 alloc] init];
    [adapterUserList openDatabase];
    [adapterUserList resetSqlCommand];
    returnCode=[adapterUserList isExistUserDetailsData:userListData];
    [adapterUserList close];
    return returnCode;
    
}
-(int)insertUserDetailsData:(UserDetailsModelV2*) userListData
{
    int returnCode=0;
    UserDetailsAdapterV2 *adapterUserList=[[UserDetailsAdapterV2 alloc] init];
    [adapterUserList openDatabase];
    [adapterUserList resetSqlCommand];
    returnCode=[adapterUserList insertUserDetailsData:userListData];
    [adapterUserList close];
    //    [adapterUserList release];
    return returnCode;
}
-(int)updateUserDetailsData:(UserDetailsModelV2*) userListData
{
    int returnCode=0;
    UserDetailsAdapterV2 *adapterUserList=[[UserDetailsAdapterV2 alloc] init];
    [adapterUserList openDatabase];
    [adapterUserList resetSqlCommand];
    returnCode=[adapterUserList updateUserDetailsData:userListData];
    [adapterUserList close];
    //    [adapterUserList release];
    return returnCode;
    
    
}
//load All User Details List Data
-(NSMutableArray *) loadAllUserDetailsData
{
    UserDetailsAdapterV2 *adapterUserList=[[UserDetailsAdapterV2 alloc] init];
    [adapterUserList openDatabase];
    [adapterUserList resetSqlCommand];
    NSMutableArray *userlistDataArray=[adapterUserList loadAllUserDetailsData];
    [adapterUserList close];
    //    [adapterUserList release];
    return userlistDataArray;
    
}
//[Add UserDetailsAdapterV2 End]
// MessageAdapter
-(int)insertMessage:(MessageModelV2*) model
{
    if(model == nil){
        return 0;
    }
    
    if(model.message_content == nil){
        model.message_content = @"";
    }
    
    if(model.message_from_user_login_name == nil){
        model.message_from_user_login_name = @"";
    }
    
    if(model.message_from_user_name == nil) {
        model.message_from_user_name = @"";
    }
    
    if(model.message_topic == nil){
        model.message_topic = @"";
    }
    
    DataRow *rowInsert = [[DataRow alloc] init];
    rowInsert.tableName = [NSString stringWithFormat:@"%@", MESSAGE_TABLE];
    
    [rowInsert addColumnInt:[NSString stringWithFormat:@"%@",message_id] value:(int)model.message_id]; //key
    [rowInsert addColumnInt:[NSString stringWithFormat:@"%@",message_group_id] value:(int)model.message_group_id];
    [rowInsert addColumnInt:[NSString stringWithFormat:@"%@",message_owner_id] value:(int)model.message_owner_id]; //key
    [rowInsert addColumnInt:[NSString stringWithFormat:@"%@",message_kind] value:(int)model.message_kind];
    [rowInsert addColumnInt:[NSString stringWithFormat:@"%@",message_count_photo_attached] value:(int)model.message_count_photo_attached];
    [rowInsert addColumnInt:[NSString stringWithFormat:@"%@",message_status] value:(int)model.message_status];
    [rowInsert addColumnString:[NSString stringWithFormat:@"%@",message_from_user_login_name] value:model.message_from_user_login_name];
    [rowInsert addColumnString:[NSString stringWithFormat:@"%@",message_from_user_name] value:model.message_from_user_name];
    [rowInsert addColumnString:[NSString stringWithFormat:@"%@",message_topic] value:model.message_topic];
    [rowInsert addColumnString:[NSString stringWithFormat:@"%@",message_content] value:model.message_content];
    [rowInsert addColumnString:[NSString stringWithFormat:@"%@",message_last_modified] value:model.message_last_modified];
    
    return [rowInsert insertDBRow]; //return id inserted
}

-(int)insertOfflineMessage:(MessageModelV2*) model{
    if(model == nil){
        return 0;
    }
    
    if(model.message_content == nil){
        model.message_content = @"";
    }
    
    if(model.message_from_user_login_name == nil){
        model.message_from_user_login_name = @"";
    }
    
    if(model.message_from_user_name == nil) {
        model.message_from_user_name = @"";
    }
    
    if(model.message_topic == nil){
        model.message_topic = @"";
    }
    
    DataRow *rowInsert = [[DataRow alloc] init];
    rowInsert.tableName = [NSString stringWithFormat:@"%@", MESSAGE_TABLE];
    
    [rowInsert addColumnInt:[NSString stringWithFormat:@"%@",message_group_id] value:(int)model.message_group_id];
    [rowInsert addColumnInt:[NSString stringWithFormat:@"%@",message_owner_id] value:(int)model.message_owner_id]; //key
    [rowInsert addColumnInt:[NSString stringWithFormat:@"%@",message_kind] value:(int)model.message_kind];
    [rowInsert addColumnInt:[NSString stringWithFormat:@"%@",message_count_photo_attached] value:(int)model.message_count_photo_attached];
    [rowInsert addColumnInt:[NSString stringWithFormat:@"%@",message_status] value:(int)model.message_status];
    [rowInsert addColumnString:[NSString stringWithFormat:@"%@",message_from_user_login_name] value:model.message_from_user_login_name];
    [rowInsert addColumnString:[NSString stringWithFormat:@"%@",message_from_user_name] value:model.message_from_user_name];
    [rowInsert addColumnString:[NSString stringWithFormat:@"%@",message_topic] value:model.message_topic];
    [rowInsert addColumnString:[NSString stringWithFormat:@"%@",message_content] value:model.message_content];
    [rowInsert addColumnString:[NSString stringWithFormat:@"%@",message_last_modified] value:model.message_last_modified];
    
    return [rowInsert insertDBRow]; //return id inserted
}

-(int)updateMessage:(MessageModelV2*) model
{
    if(model == nil){
        return 0;
    }
    
    if(model.message_content == nil){
        model.message_content = @"";
    }
    
    if(model.message_from_user_login_name == nil){
        model.message_from_user_login_name = @"";
    }
    
    if(model.message_from_user_name == nil) {
        model.message_from_user_name = @"";
    }
    
    if(model.message_topic == nil){
        model.message_topic = @"";
    }
    
    DataRow *rowUpdate = [[DataRow alloc] init];
    rowUpdate.tableName = [NSString stringWithFormat:@"%@", MESSAGE_TABLE];
    
    DataColumn *columnUpdate1 = [rowUpdate addColumnInt:[NSString stringWithFormat:@"%@",message_id] value:(int)model.message_id];
    columnUpdate1.isUpdateCondition = YES;
    DataColumn *columnUpdate2 = [rowUpdate addColumnInt:[NSString stringWithFormat:@"%@",message_owner_id] value:(int)model.message_owner_id];
    columnUpdate2.isUpdateCondition = YES;
    
    [rowUpdate addColumnIntValue:(int)model.message_group_id formatName:@"%@",message_group_id];
    [rowUpdate addColumnIntValue:(int)model.message_kind formatName:@"%@",message_kind];
    [rowUpdate addColumnIntValue:(int)model.message_count_photo_attached formatName:@"%@",message_count_photo_attached];
    [rowUpdate addColumnIntValue:(int)model.message_status formatName:@"%@",message_status];
    [rowUpdate addColumnStringValue:model.message_from_user_login_name formatName:@"%@",message_from_user_login_name];
    [rowUpdate addColumnStringValue:model.message_from_user_name formatName:@"%@",message_from_user_name];
    [rowUpdate addColumnStringValue:model.message_topic formatName:@"%@",message_topic];
    [rowUpdate addColumnStringValue:model.message_content formatName:@"%@",message_content];
    [rowUpdate addColumnStringValue:model.message_last_modified formatName:@"%@",message_last_modified];
    
    return [rowUpdate commitChangeDBRow];
}

-(int)updateMessageStatus:(MessageModelV2*) model{
    if(model == nil){
        return 0;
    }
    
    if(model.message_content == nil){
        model.message_content = @"";
    }
    
    if(model.message_from_user_login_name == nil){
        model.message_from_user_login_name = @"";
    }
    
    if(model.message_from_user_name == nil) {
        model.message_from_user_name = @"";
    }
    
    if(model.message_topic == nil){
        model.message_topic = @"";
    }
    
    DataRow *rowUpdate = [[DataRow alloc] init];
    rowUpdate.tableName = [NSString stringWithFormat:@"%@", MESSAGE_TABLE];
    
    DataColumn *columnUpdate1 = [rowUpdate addColumnInt:[NSString stringWithFormat:@"%@",message_id] value:(int)model.message_id];
    columnUpdate1.isUpdateCondition = YES;
    DataColumn *columnUpdate2 = [rowUpdate addColumnInt:[NSString stringWithFormat:@"%@",message_owner_id] value:(int)model.message_owner_id];
    columnUpdate2.isUpdateCondition = YES;
    
    [rowUpdate addColumnIntValue:MESSAGE_READ formatName:@"%@",message_status];
    
    
    return [rowUpdate commitChangeDBRow];
}

-(int)deleteMessageByOwnerId:(int)ownerId
{
    if(ownerId <= 0){
        return 0;
    }
    
    DataTable *tableDelete = [[DataTable alloc] initWithNameFormat:@"%@",MESSAGE_TABLE];
    NSString *queryDelete = [NSString stringWithFormat:@"Delete from %@ Where %@ = %d", MESSAGE_TABLE, message_owner_id, ownerId];
    int status = (int)[tableDelete excuteQueryNonSelect:queryDelete];
    
    return status;
}

-(int)deleteMessageByOwnerId:(int)ownerId messageId:(int)messageId
{
    if(ownerId <= 0){
        return 0;
    }
    
    DataTable *tableDelete = [[DataTable alloc] initWithNameFormat:@"%@",MESSAGE_TABLE];
    NSString *queryDelete = [NSString stringWithFormat:@"Delete from %@ Where %@ = %d and %@ = %d", MESSAGE_TABLE, message_owner_id, ownerId, message_id, messageId];
    int status = (int)[tableDelete excuteQueryNonSelect:queryDelete];
    
    return status;
}

- (int)deleteMessageByOwnerId:(int)ownerId messageGroupId:(int)messageGroupId {
    
    if (ownerId <= 0) {
        return 0;
    }
    
    DataTable *tableDelete = [[DataTable alloc] initWithNameFormat:@"%@",MESSAGE_TABLE];
    NSString *queryDelete = [NSString stringWithFormat:@"DELETE FROM %@ WHERE %@ = %d AND %@ = %d", MESSAGE_TABLE, message_owner_id, ownerId, message_group_id, messageGroupId];
    int status = (int)[tableDelete excuteQueryNonSelect:queryDelete];
    
    return status;
}

-(MessageModelV2*)loadMessageByOwnerId:(int)ownerId messageId:(int)messageId
{
    if(ownerId <= 0){
        return nil;
    }
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"select %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ from %@ where %@ = %d and %@ = %d",
                                  //select
                                  message_id,
                                  message_group_id,
                                  message_owner_id,
                                  message_kind,
                                  message_count_photo_attached,
                                  message_status,
                                  message_from_user_login_name,
                                  message_from_user_name,
                                  message_topic,
                                  message_content,
                                  message_last_modified,
                                  
                                  //From
                                  MESSAGE_TABLE,
                                  
                                  //where
                                  message_owner_id,
                                  ownerId,
                                  //and
                                  message_id,
                                  messageId];
    
    DataTable *table = [[DataTable alloc] init];
    
    //Row result reference
    DataRow *rowReference = [[DataRow alloc] init];
    [rowReference addColumnFormatInt:@"%@", message_id];
    [rowReference addColumnFormatInt:@"%@", message_group_id];
    [rowReference addColumnFormatInt:@"%@", message_owner_id];
    [rowReference addColumnFormatInt:@"%@", message_kind];
    [rowReference addColumnFormatInt:@"%@", message_count_photo_attached];
    [rowReference addColumnFormatInt:@"%@", message_status];
    [rowReference addColumnFormatString:@"%@", message_from_user_login_name];
    [rowReference addColumnFormatString:@"%@", message_from_user_name];
    [rowReference addColumnFormatString:@"%@", message_topic];
    [rowReference addColumnFormatString:@"%@", message_content];
    [rowReference addColumnFormatString:@"%@", message_last_modified];
    
    [table loadTableDataWithQuery:sqlString referenceRow:rowReference];
    
    MessageModelV2 *model = nil;
    int rowCount = [table countRows];
    
    if(rowCount > 0)
    {
        model = [[MessageModelV2 alloc] init];
        DataRow *row = [table rowWithIndex:0];
        
        model.message_content = [row stringValueWithColumnNameFormat:@"%@",message_content];
        model.message_count_photo_attached = [row intValueWithColumnNameFormat:@"%@",message_count_photo_attached];
        model.message_from_user_login_name = [row stringValueWithColumnNameFormat:@"%@",message_from_user_login_name];
        model.message_from_user_name = [row stringValueWithColumnNameFormat:@"%@",message_from_user_name];
        model.message_group_id = [row intValueWithColumnNameFormat:@"%@",message_group_id];
        model.message_id = [row intValueWithColumnNameFormat:@"%@",message_id];
        model.message_kind = [row intValueWithColumnNameFormat:@"%@",message_kind];
        model.message_last_modified = [row stringValueWithColumnNameFormat:@"%@",message_last_modified];
        model.message_owner_id = [row intValueWithColumnNameFormat:@"%@",message_owner_id];
        model.message_status = [row intValueWithColumnNameFormat:@"%@",message_status];
        model.message_topic = [row stringValueWithColumnNameFormat:@"%@",message_topic];
    }
    
    return model;
}

-(BOOL)hasMessageByOwnerId:(int)ownerid messageId:(int)messageId
{
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"select count(1) from %@ where %@ = %d and %@ = %d",
                                  //count
                                  
                                  //From
                                  MESSAGE_TABLE,
                                  
                                  //where
                                  message_owner_id,
                                  ownerid,
                                  //and
                                  message_id,
                                  messageId];
    
    DataTable *table = [[DataTable alloc] init];
    
    //Row result reference
    DataRow *rowReference = [[DataRow alloc] init];
    [rowReference addColumnFormatInt:@"CountResult"];
    
    [table loadTableDataWithQuery:sqlString referenceRow:rowReference];
    
    int countResult = 0;
    int rowCount = [table countRows];
    if(rowCount > 0)
    {
        DataRow *curRow = [table rowWithIndex:0];
        countResult = [curRow intValueWithColumnNameFormat:@"CountResult"];
    }
    
    return (countResult > 0);
}

-(NSMutableArray*)loadMessageReceivedByOwnerId:(int)ownerId
{
    if(ownerId <= 0){
        return nil;
    }
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"select %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ from %@ where %@ = %d and %@ = %d order by %@ desc",
                                  //select
                                  message_id,
                                  message_group_id,
                                  message_owner_id,
                                  message_kind,
                                  message_count_photo_attached,
                                  message_status,
                                  message_from_user_login_name,
                                  message_from_user_name,
                                  message_topic,
                                  message_content,
                                  message_last_modified,
                                  
                                  //From
                                  MESSAGE_TABLE,
                                  
                                  //where
                                  message_owner_id,
                                  ownerId,
                                  //and
                                  message_kind,
                                  MESSAGE_KIND_INBOX,
                                  message_last_modified];
    
    DataTable *table = [[DataTable alloc] init];
    
    //Row result reference
    DataRow *rowReference = [[DataRow alloc] init];
    [rowReference addColumnFormatInt:@"%@", message_id];
    [rowReference addColumnFormatInt:@"%@", message_group_id];
    [rowReference addColumnFormatInt:@"%@", message_owner_id];
    [rowReference addColumnFormatInt:@"%@", message_kind];
    [rowReference addColumnFormatInt:@"%@", message_count_photo_attached];
    [rowReference addColumnFormatInt:@"%@", message_status];
    [rowReference addColumnFormatString:@"%@", message_from_user_login_name];
    [rowReference addColumnFormatString:@"%@", message_from_user_name];
    [rowReference addColumnFormatString:@"%@", message_topic];
    [rowReference addColumnFormatString:@"%@", message_content];
    [rowReference addColumnFormatString:@"%@", message_last_modified];
    
    [table loadTableDataWithQuery:sqlString referenceRow:rowReference];
    
    NSMutableArray *results = nil;
    int rowCount = [table countRows];
    if(rowCount > 0)
    {
        
        results = [NSMutableArray array];
        for (int i = 0; i < rowCount; i ++) {
            MessageModelV2 *model = [[MessageModelV2 alloc] init];
            DataRow *row = [table rowWithIndex:i];
            
            model.message_content = [row stringValueWithColumnNameFormat:@"%@",message_content];
            model.message_count_photo_attached = [row intValueWithColumnNameFormat:@"%@",message_count_photo_attached];
            model.message_from_user_login_name = [row stringValueWithColumnNameFormat:@"%@",message_from_user_login_name];
            model.message_from_user_name = [row stringValueWithColumnNameFormat:@"%@",message_from_user_name];
            model.message_group_id = [row intValueWithColumnNameFormat:@"%@",message_group_id];
            model.message_id = [row intValueWithColumnNameFormat:@"%@",message_id];
            model.message_kind = [row intValueWithColumnNameFormat:@"%@",message_kind];
            model.message_last_modified = [row stringValueWithColumnNameFormat:@"%@",message_last_modified];
            model.message_owner_id = [row intValueWithColumnNameFormat:@"%@",message_owner_id];
            model.message_status = [row intValueWithColumnNameFormat:@"%@",message_status];
            model.message_topic = [row stringValueWithColumnNameFormat:@"%@",message_topic];
            
            [results addObject:model];
        }
    }
    
    return results;
}

-(NSMutableArray*)loadMessageSentByOwnerId:(int)ownerId
{
    if(ownerId <= 0){
        return nil;
    }
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"select %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ from %@ where %@ = %d and %@ = %d GROUP BY %@ order by %@ desc",
                                  //select
                                  message_id,
                                  message_group_id,
                                  message_owner_id,
                                  message_kind,
                                  message_count_photo_attached,
                                  message_status,
                                  message_from_user_login_name,
                                  message_from_user_name,
                                  message_topic,
                                  message_content,
                                  message_last_modified,
                                  
                                  //From
                                  MESSAGE_TABLE,
                                  
                                  //where
                                  message_owner_id,
                                  ownerId,
                                  //and
                                  message_kind,
                                  MESSAGE_KIND_SENT,
                                  message_group_id,
                                  message_last_modified
                                  ];
    
    DataTable *table = [[DataTable alloc] init];
    
    //Row result reference
    DataRow *rowReference = [[DataRow alloc] init];
    [rowReference addColumnFormatInt:@"%@", message_id];
    [rowReference addColumnFormatInt:@"%@", message_group_id];
    [rowReference addColumnFormatInt:@"%@", message_owner_id];
    [rowReference addColumnFormatInt:@"%@", message_kind];
    [rowReference addColumnFormatInt:@"%@", message_count_photo_attached];
    [rowReference addColumnFormatInt:@"%@", message_status];
    [rowReference addColumnFormatString:@"%@", message_from_user_login_name];
    [rowReference addColumnFormatString:@"%@", message_from_user_name];
    [rowReference addColumnFormatString:@"%@", message_topic];
    [rowReference addColumnFormatString:@"%@", message_content];
    [rowReference addColumnFormatString:@"%@", message_last_modified];
    
    [table loadTableDataWithQuery:sqlString referenceRow:rowReference];
    
    NSMutableArray *results = nil;
    int rowCount = [table countRows];
    if(rowCount > 0)
    {
        
        results = [NSMutableArray array];
        for (int i = 0; i < rowCount; i ++) {
            MessageModelV2 *model = [[MessageModelV2 alloc] init];
            DataRow *row = [table rowWithIndex:i];
            
            model.message_content = [row stringValueWithColumnNameFormat:@"%@",message_content];
            model.message_count_photo_attached = [row intValueWithColumnNameFormat:@"%@",message_count_photo_attached];
            model.message_from_user_login_name = [row stringValueWithColumnNameFormat:@"%@",message_from_user_login_name];
            model.message_from_user_name = [row stringValueWithColumnNameFormat:@"%@",message_from_user_name];
            model.message_group_id = [row intValueWithColumnNameFormat:@"%@",message_group_id];
            model.message_id = [row intValueWithColumnNameFormat:@"%@",message_id];
            model.message_kind = [row intValueWithColumnNameFormat:@"%@",message_kind];
            model.message_last_modified = [row stringValueWithColumnNameFormat:@"%@",message_last_modified];
            model.message_owner_id = [row intValueWithColumnNameFormat:@"%@",message_owner_id];
            model.message_status = [row intValueWithColumnNameFormat:@"%@",message_status];
            model.message_topic = [row stringValueWithColumnNameFormat:@"%@",message_topic];
            
            [results addObject:model];
        }
    }
    
    return results;
}

- (int)countMessageSentByOwnerId:(int)ownerId {
    
    if (ownerId <= 0) {
        return 0;
    }
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"SELECT COUNT(1) FROM %@ WHERE %@ = %d AND %@ = %d GROUP BY %@",
                                  //count
                                  
                                  //From
                                  MESSAGE_TABLE,
                                  
                                  //where
                                  message_owner_id,
                                  ownerId,
                                  //and
                                  message_kind,
                                  MESSAGE_KIND_SENT,
                                  message_group_id
                                  ];
    
    DataTable *table = [[DataTable alloc] init];
    
    //Row result reference
    DataRow *rowReference = [[DataRow alloc] init];
    [rowReference addColumnFormatInt:@"CountResult"];
    
    [table loadTableDataWithQuery:sqlString referenceRow:rowReference];
    
    int result = 0;
    int rowCount = [table countRows];
    if (rowCount > 0) {
        
//        DataRow *row = [table rowWithIndex:0];
//        result = [row intValueWithColumnNameFormat:@"CountResult"];
        result = rowCount;
    }
    
    return result;
}

-(int)countMessageInboxByOwnerId:(int)ownerId
{
    if (ownerId <= 0) {
        return 0;
    }
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"select count(1) from %@ where %@ = %d and %@ = %d",
                                  //count
                                  
                                  //From
                                  MESSAGE_TABLE,
                                  
                                  //where
                                  message_owner_id,
                                  ownerId,
                                  //and
                                  message_kind,
                                  MESSAGE_KIND_INBOX];
    
    DataTable *table = [[DataTable alloc] init];
    
    //Row result reference
    DataRow *rowReference = [[DataRow alloc] init];
    [rowReference addColumnFormatInt:@"CountResult"];
    
    [table loadTableDataWithQuery:sqlString referenceRow:rowReference];
    
    int result = 0;
    int rowCount = [table countRows];
    if(rowCount > 0)
    {
        DataRow *row = [table rowWithIndex:0];
        result = [row intValueWithColumnNameFormat:@"CountResult"];
    }
    
    return result;
}

-(int)countMessageInboxUnreadByOwnerId:(int)ownerId{
    if (ownerId <= 0) {
        return 0;
    }
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"select count(1) from %@ where %@ = %d and %@ = %d and %@ = %d",
                                  //count
                                  
                                  //From
                                  MESSAGE_TABLE,
                                  
                                  //where
                                  message_owner_id,
                                  ownerId,
                                  //and
                                  message_kind,
                                  MESSAGE_KIND_INBOX,
                                  
                                  message_status,
                                  MESSAGE_UNREAD];
    
    DataTable *table = [[DataTable alloc] init];
    
    //Row result reference
    DataRow *rowReference = [[DataRow alloc] init];
    [rowReference addColumnFormatInt:@"CountResult"];
    
    [table loadTableDataWithQuery:sqlString referenceRow:rowReference];
    
    int result = 0;
    int rowCount = [table countRows];
    if(rowCount > 0)
    {
        DataRow *row = [table rowWithIndex:0];
        result = [row intValueWithColumnNameFormat:@"CountResult"];
    }
    
    return result;
}

//MessagePhotoAdapter
-(int)insertMsgPhotoData:(MessagePhotoModelV2*) model
{
    if(model == nil){
        return 0;
    }
    
    if(model.message_photo == nil){
        return 0;
    }
    
    DataRow *rowInsert = [[DataRow alloc] init];
    rowInsert.tableName = [NSString stringWithFormat:@"%@", MESSAGE_PHOTOS];
    
    [rowInsert addColumnDataValue:model.message_photo formatName:@"%@", message_photo];
    [rowInsert addColumnIntValue:(int)model.message_photo_group_id formatName:@"%@", message_photo_group_id];
    [rowInsert addColumnStringValue:model.message_photo_last_modified formatName:@"%@", message_photo_last_modified];
    
    return [rowInsert insertDBRow];
}

-(int)updateMsgPhotoData:(MessagePhotoModelV2*) model
{
    if(model == nil){
        return 0;
    }
    
    DataRow *rowUpdate = [[DataRow alloc] init];
    rowUpdate.tableName = [NSString stringWithFormat:@"%@", MESSAGE_PHOTOS];
    
    DataColumn *columnUpdate1 = [rowUpdate addColumnInt:[NSString stringWithFormat:@"%@",message_photo_group_id] value:(int)model.message_photo_group_id];
    columnUpdate1.isUpdateCondition = YES;
    
    [rowUpdate addColumnIntValue:model.message_photo formatName:@"%@",message_photo];
    [rowUpdate addColumnIntValue:model.message_photo_last_modified formatName:@"%@",message_photo_last_modified];
    
    return [rowUpdate commitChangeDBRow];
}

-(int)deleteMsgPhotoDataByGroupId:(int)groupId
{
    if(groupId <= 0){
        return 0;
    }
    
    DataTable *tableDelete = [[DataTable alloc] initWithNameFormat:@"%@",MESSAGE_PHOTOS];
    NSString *queryDelete = [NSString stringWithFormat:@"Delete from %@ Where %@ = %d", MESSAGE_PHOTOS, message_photo, groupId];
    int status = (int)[tableDelete excuteQueryNonSelect:queryDelete];
    
    return status;
}


-(NSMutableArray *) loadMessagePhotosByGroupId:(int) messageGroupId
{
    if(messageGroupId <= 0) {
        return nil;
    }
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"select %@, %@, %@ from %@ where %@ = %d",
                                  //select
                                  message_photo_group_id,
                                  message_photo,
                                  message_photo_last_modified,
                                  
                                  //From
                                  MESSAGE_PHOTOS,
                                  
                                  //where
                                  message_photo_group_id,
                                  messageGroupId
                                  ];
    
    DataTable *table = [[DataTable alloc] init];
    
    //Row result reference
    DataRow *rowReference = [[DataRow alloc] init];
    [rowReference addColumnFormatInt:@"%@", message_photo_group_id];
    [rowReference addColumnFormatData:@"%@", message_photo];
    [rowReference addColumnFormatInt:@"%@", message_photo_last_modified];
    
    [table loadTableDataWithQuery:sqlString referenceRow:rowReference];
    
    NSMutableArray *results = nil;
    int rowCount = [table countRows];
    if(rowCount > 0)
    {
        
        results = [NSMutableArray array];
        for (int i = 0; i < rowCount; i ++) {
            MessagePhotoModelV2 *model = [[MessagePhotoModelV2 alloc] init];
            DataRow *row = [table rowWithIndex:i];
            
            model.message_photo_group_id = [row intValueWithColumnNameFormat:@"%@",message_photo_group_id];
            model.message_photo = [row dataValueWithColumnNameFormat:@"%@",message_photo];
            model.message_photo_last_modified = [row stringValueWithColumnNameFormat:@"%@",message_photo_last_modified];
            
            [results addObject:model];
        }
    }
    
    return results;
}

-(int)countMessagePhotoByGroupId:(int)groupId
{
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"select count(1) from %@ where %@ = %d",
                                  //count
                                  
                                  //From
                                  MESSAGE_PHOTOS,
                                  
                                  //where
                                  message_photo_group_id,
                                  groupId
                                  ];
    
    DataTable *table = [[DataTable alloc] init];
    
    //Row result reference
    DataRow *rowReference = [[DataRow alloc] init];
    [rowReference addColumnFormatInt:@"CountResult"];
    
    [table loadTableDataWithQuery:sqlString referenceRow:rowReference];
    
    int countResult = 0;
    int rowCount = [table countRows];
    if(rowCount > 0)
    {
        DataRow *curRow = [table rowWithIndex:0];
        countResult = [curRow intValueWithColumnNameFormat:@"CountResult"];
    }
    
    return (countResult > 0);
}

//MessageSubjectItemAdapter
-(int)insertMsgSubjectItemData:(MessageSubjectItemModelV2*) msgSubjectItem
{
    int returnCode=0;
    MessageSubjectItemAdapterV2 *adapterMsgSubjectItem=[[MessageSubjectItemAdapterV2 alloc] init];
    [adapterMsgSubjectItem openDatabase];
    [adapterMsgSubjectItem resetSqlCommand];
    returnCode=[adapterMsgSubjectItem insertMsgSubjectItemData:msgSubjectItem];
    [adapterMsgSubjectItem close];
    //    [adapterMsgSubjectItem release];
    return returnCode;
}
-(int)updateMsgSubjectItemData:(MessageSubjectItemModelV2*) msgSubjectItem
{
    int returnCode=0;
    MessageSubjectItemAdapterV2 *adapterMsgSubjectItem=[[MessageSubjectItemAdapterV2 alloc] init];
    [adapterMsgSubjectItem openDatabase];
    [adapterMsgSubjectItem resetSqlCommand];
    returnCode=[adapterMsgSubjectItem updateMsgSubjectItemData:msgSubjectItem];
    [adapterMsgSubjectItem close];
    //    [adapterMsgSubjectItem release];
    return returnCode;
}
-(int)deleteMsgSubjectItemData:(MessageSubjectItemModelV2*) msgSubjectItem
{
    int returnCode=0;
    MessageSubjectItemAdapterV2 *adapterMsgSubjectItem=[[MessageSubjectItemAdapterV2 alloc] init];
    [adapterMsgSubjectItem openDatabase];
    [adapterMsgSubjectItem resetSqlCommand];
    returnCode=[adapterMsgSubjectItem deleteMsgSubjectItemData:msgSubjectItem];
    [adapterMsgSubjectItem close];
    //    [adapterMsgSubjectItem release];
    return returnCode;
}

-(int)deleteAllMessageItems
{
    DataTable *tableDelete = [[DataTable alloc] initWithNameFormat:@"%@", SUBJECT_MESSAGE_ITEMS];
    int result = (int)[tableDelete excuteQueryNonSelect:[NSString stringWithFormat:@"Delete from %@", SUBJECT_MESSAGE_ITEMS]];
    if(result > 0) {
        if(isDemoMode)
        {
            [NSString stringWithFormat: @"REINDEX %@.%@",DATABASE_NAME_DEMO, SUBJECT_MESSAGE_ITEMS];
        }
        else{
            [NSString stringWithFormat: @"REINDEX %@.%@",DATABASE_NAME, SUBJECT_MESSAGE_ITEMS];
        }
    }
    return result;
}

-(MessageSubjectItemModelV2*)loadMsgSubjectItemData:(MessageSubjectItemModelV2*) msgSubjectItem
{
    MessageSubjectItemAdapterV2 *adapterMsgSubjectItem=[[MessageSubjectItemAdapterV2 alloc] init];
    [adapterMsgSubjectItem openDatabase];
    [adapterMsgSubjectItem resetSqlCommand];
    MessageSubjectItemModelV2 *msgSubjectItemObject=[adapterMsgSubjectItem loadMsgSubjectItemData:msgSubjectItem];
    [adapterMsgSubjectItem close];
    //    [adapterMsgSubjectItem release];
    return msgSubjectItemObject;
    
}
-(NSMutableArray *) loadAllMsgSubjectItemByMsgSubject:(int) smicategory_id
{
    
    MessageSubjectItemAdapterV2 *adapterMsgSubjectItem=[[MessageSubjectItemAdapterV2 alloc] init];
    [adapterMsgSubjectItem openDatabase];
    [adapterMsgSubjectItem resetSqlCommand];
    NSMutableArray *msgSubjectItemArray=[adapterMsgSubjectItem loadAllMsgSubjectItemByMsgSubject:smicategory_id];
    [adapterMsgSubjectItem close];
    //    [adapterMsgSubjectItem release];
    return msgSubjectItemArray;
}

//MessageSubjetAdapter
-(int)insertMsgSubjectData:(MessageSubjectModelV2*) msgSubject
{
    int returnCode=0;
    MessageSubjectAdapterV2 *adapterMsgSubject=[[MessageSubjectAdapterV2 alloc] init];
    [adapterMsgSubject openDatabase];
    [adapterMsgSubject resetSqlCommand];
    returnCode=[adapterMsgSubject insertMsgSubjectData:msgSubject];
    [adapterMsgSubject close];
    //    [adapterMsgSubject release];
    return returnCode;
    
}
-(int)updateMsgSubjectData:(MessageSubjectModelV2*) msgSubject
{
    int returnCode=0;
    MessageSubjectAdapterV2 *adapterMsgSubject=[[MessageSubjectAdapterV2 alloc] init];
    [adapterMsgSubject openDatabase];
    [adapterMsgSubject resetSqlCommand];
    returnCode=[adapterMsgSubject updateMsgSubjectData:msgSubject];
    [adapterMsgSubject close];
    //    [adapterMsgSubject release];
    return returnCode;
}
-(int)deleteMsgSubjectData:(MessageSubjectModelV2*) msgSubject
{ int returnCode=0;
    MessageSubjectAdapterV2 *adapterMsgSubject=[[MessageSubjectAdapterV2 alloc] init];
    [adapterMsgSubject openDatabase];
    [adapterMsgSubject resetSqlCommand];
    returnCode=[adapterMsgSubject deleteMsgSubjectData:msgSubject];
    [adapterMsgSubject close];
    //    [adapterMsgSubject release];
    return returnCode;
}

-(int)deleteAllMessageSubjects
{
    DataTable *tableDelete = [[DataTable alloc] initWithNameFormat:@"%@", SUBJECT_MESSAGE_CATEGORIES];
    int result = (int)[tableDelete excuteQueryNonSelect:[NSString stringWithFormat:@"Delete from %@", SUBJECT_MESSAGE_CATEGORIES]];
    if(result > 0) {
        if(isDemoMode)
        {
            [NSString stringWithFormat: @"REINDEX %@.%@",DATABASE_NAME_DEMO, SUBJECT_MESSAGE_ITEMS];
        }
        else{
            [NSString stringWithFormat: @"REINDEX %@.%@",DATABASE_NAME, SUBJECT_MESSAGE_ITEMS];
        }
    }
    
    return result;
}


-(MessageSubjectModelV2*)loadMsgSubjectData:(MessageSubjectModelV2*) msgSubject
{
    MessageSubjectAdapterV2 *adapterMsgSubject=[[MessageSubjectAdapterV2 alloc] init];
    [adapterMsgSubject openDatabase];
    [adapterMsgSubject resetSqlCommand];
    MessageSubjectModelV2 *msgSubjectObject=[adapterMsgSubject loadMsgSubjectData:msgSubject];
    [adapterMsgSubject close];
    //    [adapterMsgSubject release];
    return msgSubjectObject;
}
-(NSMutableArray *) loadAllMsgSubject
{
    MessageSubjectAdapterV2 *adapterMsgSubject=[[MessageSubjectAdapterV2 alloc] init];
    [adapterMsgSubject openDatabase];
    [adapterMsgSubject resetSqlCommand];
    NSMutableArray * msgSubjectArray=[adapterMsgSubject loadAllMsgSubject];
    [adapterMsgSubject close];
    //    [adapterMsgSubject release];
    return msgSubjectArray;
}

//MessageReceiverAdapter
-(int)insertMessageReceiverData:(MessageReceiverModelV2*) model
{
    if(model == nil){
        return 0;
    }
    
    DataRow *rowInsert = [[DataRow alloc] init];
    rowInsert.tableName = [NSString stringWithFormat:@"%@", MESSAGE_RECEIVER];
    
    [rowInsert addColumnInt:[NSString stringWithFormat:@"%@",message_receiver_group_id] value:(int)model.message_receiver_group_id];
    [rowInsert addColumnInt:[NSString stringWithFormat:@"%@",message_receiver_id] value:(int)model.message_receiver_id];
    [rowInsert addColumnInt:[NSString stringWithFormat:@"%@",message_receiver_last_modified] value:model.message_receiver_last_modified];
    [rowInsert addColumnInt:[NSString stringWithFormat:@"%@",message_receiver_owner_id] value:(int)model.message_receiver_owner_id];
    
    return [rowInsert insertDBRow]; //return id inserted
}

-(int)updateMessageReceiverData:(MessageReceiverModelV2*) model
{
    if(model == nil){
        return 0;
    }
    
    DataRow *rowUpdate = [[DataRow alloc] init];
    rowUpdate.tableName = [NSString stringWithFormat:@"%@", MESSAGE_RECEIVER];
    
    DataColumn *columnUpdate1 = [rowUpdate addColumnInt:[NSString stringWithFormat:@"%@",message_id] value:(int)model.message_receiver_id];
    columnUpdate1.isUpdateCondition = YES;
    
    DataColumn *columnUpdate2 = [rowUpdate addColumnIntValue:(int)model.message_receiver_owner_id formatName:@"%@",message_receiver_owner_id];
    columnUpdate2.isUpdateCondition = YES;
    
    [rowUpdate addColumnIntValue:(int)model.message_receiver_group_id formatName:@"%@",message_receiver_group_id];
    [rowUpdate addColumnIntValue:model.message_receiver_last_modified formatName:@"%@",message_receiver_last_modified];
    
    return [rowUpdate commitChangeDBRow];
}

//Return user list data
-(NSMutableArray *) loadMessageReceiversByOwnerId:(int)ownerId groupId:(int)groupId
{
    if(ownerId <= 0){
        return nil;
    }
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"select b.%@, b.%@, b.%@, b.%@, b.%@, b.%@, b.%@ from %@ a, %@ b where a.%@ = %d and a.%@ = %d and a.%@ = b.%@",
                                  //select
                                  usr_id,
                                  usr_name,
                                  usr_hotel_id,
                                  usr_fullname,
                                  user_fullname_lang,
                                  usr_last_modified,
                                  usr_supervisor,
                                  
                                  //From
                                  MESSAGE_RECEIVER, //a
                                  USER_LIST, //a
                                  
                                  //where
                                  message_receiver_owner_id, //a
                                  ownerId,
                                  message_receiver_group_id, //a
                                  groupId,
                                  message_receiver_id,//a
                                  usr_id
                                  ];
    
    DataTable *table = [[DataTable alloc] init];
    
    //Row result reference
    DataRow *rowReference = [[DataRow alloc] init];
    [rowReference addColumnFormatInt:@"%@", usr_id];
    [rowReference addColumnFormatString:@"%@", usr_name];
    [rowReference addColumnFormatInt:@"%@", usr_hotel_id];
    [rowReference addColumnFormatString:@"%@", usr_fullname];
    [rowReference addColumnFormatString:@"%@", user_fullname_lang];
    [rowReference addColumnFormatString:@"%@", usr_last_modified];
    [rowReference addColumnFormatInt:@"%@", usr_supervisor];
    
    [table loadTableDataWithQuery:sqlString referenceRow:rowReference];
    
    NSMutableArray *results = nil;
    int rowCount = [table countRows];
    if(rowCount > 0)
    {
        results = [NSMutableArray array];
        for (int i = 0; i < rowCount; i ++) {
            UserListModelV2 *model = [[UserListModelV2 alloc] init];
            DataRow *row = [table rowWithIndex:i];
            
            model.userListFullName = [row stringValueWithColumnNameFormat:@"%@",usr_fullname];
            model.userListFullNameLang = [row stringValueWithColumnNameFormat:@"%@",user_fullname_lang];
            model.userListHotelId = [row intValueWithColumnNameFormat:@"%@",usr_hotel_id];
            model.userListId = [row intValueWithColumnNameFormat:@"%@",usr_id];
            model.userListLastModified = [row stringValueWithColumnNameFormat:@"%@",usr_last_modified];
            model.userListName = [row stringValueWithColumnNameFormat:@"%@",usr_name];
            model.usrSupervisor = [row intValueWithColumnNameFormat:@"%@",usr_supervisor];
            
            [results addObject:model];
        }
    }
    
    return results;
}

-(int) countMessageReceiversByOwnerId:(int)ownerId groupId:(int)groupId
{
    DataTable *tableMessage = [[DataTable alloc] initWithName:[NSString stringWithFormat:@"%@",MESSAGE_RECEIVER]];
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT count(1) FROM %@ WHERE %@ = %d and %@ = %d", MESSAGE_RECEIVER, message_receiver_owner_id, ownerId, message_receiver_group_id, groupId];
    DataRow *rowResult = [[DataRow alloc] init];
    [rowResult addColumnInt:@"CountResult"];
    //bind data
    [tableMessage loadTableDataWithQuery:sqlString referenceRow:rowResult];
    
    if([tableMessage countRows] > 0)
    {
        int result = [[[tableMessage rowWithIndex:0] columnWithName:@"CountResult"] fieldValueInt];
        return result;
    }
    
    return 0;
}

- (BOOL)checkMessageReceiverIfExisted:(MessageReceiverModelV2*)receiver {
    
    DataTable *tableMessage = [[DataTable alloc] initWithName:[NSString stringWithFormat:@"%@", MESSAGE_RECEIVER]];
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT count(1) FROM %@ WHERE %@ = %d and %@ = %d and %@ = %d",
                           MESSAGE_RECEIVER,
                           message_receiver_owner_id,
                           (int)receiver.message_receiver_owner_id,
                           message_receiver_group_id,
                           (int)receiver.message_receiver_group_id,
                           message_receiver_id,
                           (int)receiver.message_receiver_id];
    DataRow *rowResult = [[DataRow alloc] init];
    [rowResult addColumnInt:@"CountResult"];
    //bind data
    [tableMessage loadTableDataWithQuery:sqlString referenceRow:rowResult];
    
    if ([tableMessage countRows] > 0) {
        int result = [[[tableMessage rowWithIndex:0] columnWithName:@"CountResult"] fieldValueInt];
        if (result > 0) {
            return YES;
        }
    }
    return NO;
}

- (NSMutableArray*)loadMessageSentByOwnerId:(int)ownerId messageGroupId:(int)messageGroupId {
    if (ownerId <= 0) {
        return nil;
    }
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ = %d AND %@ = %d AND %@ = %d",
                                  //select
                                  message_id,
                                  message_group_id,
                                  message_owner_id,
                                  message_kind,
                                  message_count_photo_attached,
                                  message_status,
                                  message_from_user_login_name,
                                  message_from_user_name,
                                  message_topic,
                                  message_content,
                                  message_last_modified,
                                  
                                  //From
                                  MESSAGE_TABLE,
                                  
                                  //where
                                  message_owner_id,
                                  ownerId,
                                  message_kind,
                                  MESSAGE_KIND_SENT,
                                  message_group_id,
                                  messageGroupId
                                  ];
    
    DataTable *table = [[DataTable alloc] init];
    
    //Row result reference
    DataRow *rowReference = [[DataRow alloc] init];
    [rowReference addColumnFormatInt:@"%@", message_id];
    [rowReference addColumnFormatInt:@"%@", message_group_id];
    [rowReference addColumnFormatInt:@"%@", message_owner_id];
    [rowReference addColumnFormatInt:@"%@", message_kind];
    [rowReference addColumnFormatInt:@"%@", message_count_photo_attached];
    [rowReference addColumnFormatInt:@"%@", message_status];
    [rowReference addColumnFormatString:@"%@", message_from_user_login_name];
    [rowReference addColumnFormatString:@"%@", message_from_user_name];
    [rowReference addColumnFormatString:@"%@", message_topic];
    [rowReference addColumnFormatString:@"%@", message_content];
    [rowReference addColumnFormatString:@"%@", message_last_modified];
    
    [table loadTableDataWithQuery:sqlString referenceRow:rowReference];
    
    NSMutableArray *results = nil;
    int rowCount = [table countRows];
    if (rowCount > 0) {
        
        results = [NSMutableArray array];
        for (int i = 0; i < rowCount; i ++) {
            MessageModelV2 *model = [[MessageModelV2 alloc] init];
            DataRow *row = [table rowWithIndex:i];
            
            model.message_content = [row stringValueWithColumnNameFormat:@"%@",message_content];
            model.message_count_photo_attached = [row intValueWithColumnNameFormat:@"%@",message_count_photo_attached];
            model.message_from_user_login_name = [row stringValueWithColumnNameFormat:@"%@",message_from_user_login_name];
            model.message_from_user_name = [row stringValueWithColumnNameFormat:@"%@",message_from_user_name];
            model.message_group_id = [row intValueWithColumnNameFormat:@"%@",message_group_id];
            model.message_id = [row intValueWithColumnNameFormat:@"%@",message_id];
            model.message_kind = [row intValueWithColumnNameFormat:@"%@",message_kind];
            model.message_last_modified = [row stringValueWithColumnNameFormat:@"%@",message_last_modified];
            model.message_owner_id = [row intValueWithColumnNameFormat:@"%@",message_owner_id];
            model.message_status = [row intValueWithColumnNameFormat:@"%@",message_status];
            model.message_topic = [row stringValueWithColumnNameFormat:@"%@",message_topic];
            [results addObject:model];
        }
    }
    return results;
}

- (int)deleteMessageReceiverByOwnerId:(int)ownerId messageGroupId:(int)messageGroupId {
    if (ownerId <= 0) {
        return 0;
    }
    
    DataTable *tableDelete = [[DataTable alloc] initWithNameFormat:@"%@",MESSAGE_RECEIVER];
    NSString *queryDelete = [NSString stringWithFormat:@"DELETE FROM %@ WHERE %@ = %d AND %@ = %d", MESSAGE_RECEIVER, message_receiver_owner_id, ownerId, message_receiver_group_id, messageGroupId];
    int status = (int)[tableDelete excuteQueryNonSelect:queryDelete];
    
    return status;
}

#pragma mark -  Progress step loading
//return percentage step
-(float) caculateStepFromCountingObject:(int) countValue
{
    if(countValue == 0 || countValue == 1)
        return 100;
    
    float stepValue = (float)(100 / (float)countValue);
    return stepValue;
}

#pragma mark - Update Percent View

-(void)updatePercentView:(MBProgressHUD*)percentView value:(float)percentageValue description:(NSString *)description
{
    if (percentView) {
        NSString *percentString = [[NSString alloc]initWithFormat:@"%.0f%%",percentageValue];
        percentView.detailsLabelFont = percentView.labelFont;
        percentView.detailsLabelText = percentString;
        
        float nextStepProgress = [SyncManagerV2 getNextStepOperations];
        int maxSyncOperations = [SyncManagerV2 getMaxSyncOperations];
        if(nextStepProgress > 0 && maxSyncOperations > 0)
        {
            double currentOperations = round(percentView.progress/nextStepProgress);
            //percentView.labelText = [NSString stringWithFormat:@"%@ - [%0.f/%d]", description, currentOperations, maxSyncOperations];
            percentView.labelText = [NSString stringWithFormat:@"[%0.f/%d]", currentOperations, maxSyncOperations];
        }
        else
        {
            percentView.labelText = description;
        }
    }
}

@end
