//
//  AdditionalJobManagerV2.h
//  mHouseKeeping
//
//  Created by vinhnguyen on 7/13/13.
//
//

#import <Foundation/Foundation.h>
#import "DataBaseExtension.h"
#import "AddJobFloorModelV2.h"
#import "AddJobSearchModelV2.h"
#import "RoomAssignmentInfoViewController.h"
#import "MBProgressHUD.h"
#import "AddJobItemModelV2.h"
#import "AddJobRoomModelV2.h"
#import "AddJobSearchRoomCategoryModelV2.h"
#import "AddJobRoomItemModelV2.h"
#import "AddJobCategoryModelV2.h"
#import "AddJobDetailModelV2.h"
#import "AddJobDetailRecordModelV2.h"
#import "AddJobGuestInfoModelV2.h"
#import "AddJobStatusModel.h"

//enum AddJobStatus {
//    AddJobStatus_Unknown = -1,
//    //Re-use Timer Status Defined
//    AddJobStatus_Pending = TIMER_STATUS_NOT_START,
//    AddJobStatus_Started = TIMER_STATUS_START,
//    AddJobStatus_Pause = TIMER_STATUS_PAUSE,
//    AddJobStatus_Complete = TIMER_STATUS_FINISH,
//    
//};

enum AddJobStatus {
    AddJobStatus_Unknown = -1,
    AddJobStatus_DND = 1,
    AddJobStatus_DoubleLock = 9,
    AddJobStatus_ServiceLater = 2,
    AddJobStatus_DeclinedService = 3,
    AddJobStatus_Started = 4,
    AddJobStatus_Complete = 5,
    AddJobStatus_Pause = 6,
    AddJobStatus_Pending = 7
};

@interface AdditionalJobManagerV2 : NSObject

//MARK: Static Methods
+ (AdditionalJobManagerV2*) sharedAddionalJobManager;
+ (AdditionalJobManagerV2*) updateAdditionalJobManagerInstance;

#pragma mark - WS FUNCTIONS
//WS AddJob Floor Function
//pass tasksFilter = nil for get all additional job floors
-(bool) loadWSAddJobFloorListByUserId:(int)userId hotelId:(int)hotelId tasksFilter:(NSMutableArray*)listTasks;
//WS Add Job Item
-(bool) loadWSAddJobItemListByUserId:(int)userId hotelId:(int)hotelId;
//WS Add Job Status
-(bool) loadWSAddJobStatusByUserId:(int)userId lastModified:(NSString*)lastModified;
//WS Add Job Room
-(bool) loadWSAddJobRoomListByUserId:(int)userId hotelId:(int)hotelId floorId:(int)floorId tasksFilter:(NSMutableArray*)listTasks;
//WS Add Job Room
-(bool) loadWSAddJobRoomByUserId:(int)userId hotelId:(int)hotelId roomNumber:(NSString*)roomNumber;
//WS Add Job Detail
-(bool) loadWSAddJobDetailByUserId:(int)userId ondayAddJobId:(int)ondayAddJobId;
//WS Add Job Count Pending
-(int) loadWSAddJobCountPendingByUserId:(int)userId hotelId:(int)hotelId;
//WS Add Job GuestInfo
-(bool) loadWSAddJobGuestInfoByUserId:(int)userId roomId:(int)roomId;
//WS Add Job GuestInfo Number
//Return N/A for can't get Guest Number
-(NSString*) loadWSAddJobGuestNumberByUserId:(int)userId hotelId:(int)hotelId roomNumber:(NSString*)roomNumber;
//WS Add Job Detail Of Room Assignment
-(bool) loadWSAddJobOfRoomByUserId:(int)userId hotelId:(int)hotelId roomNumber:(NSString*)roomNumber roomAssignId:(int)roomAssignId;
//Post Additional Job Start
-(bool) postAddJobStartWithUserId:(int)userId ondayAddJobId:(int)ondayAddJobId time:(NSString*)timeString;
//Post Additional Job Stop
-(bool) postAddJobStopWithUserId:(int)userId ondayAddJobId:(int)ondayAddJobId time:(NSString*)timeString;
//Post Additional Job Complete
-(bool) postAddJobCompleteWithUserId:(int)userId ondayAddJobId:(int)ondayAddJobId time:(NSString*)timeString;
//Post Additional Job Remark
-(bool) postAddJobRemarkWithUserId:(int)userId ondayAddJobId:(int)ondayAddJobId remark:(NSString*)remark;
//return number of posted additional job
-(int)postAllAddJobByUserId:(int)userId;
//return response code
-(bool)postAddJobStatusByUserId:(int)userId ondayAddJobId:(int)ondayAddJobId addJobStatus:(int)addJobStatus serviceLaterDateTime:(NSString*)serviceLaterDateTime;

#pragma mark - DATABASE FUNCTIONS
//MARK: Delete all additional Job data from yesterday
-(int)deleteAllAddJobDataBeforeToday;
//Check whether additional job data is old or not
-(bool) isUsingOldAddJobData;
//MARK: Local AddJob Floor Function
-(int) insertAddJobFloor:(AddJobFloorModelV2*)model;
-(int) updateAddJobFloor:(AddJobFloorModelV2*)model;
//-(int) deleteAddJobFloorByUserId:(int) userId searchId:(int)searchId;
-(int) deleteAllAddJobFloors;
-(AddJobFloorModelV2*) loadAddJobFloorByUserId:(int)userId floorId:(int)floorId searchId:(int)searchId;
-(int) deleteAddJobFloorsByAddJobFloorModel: (AddJobFloorModelV2*) floorModel;
-(NSMutableArray*) loadAllAddJobFloorsByUserId:(int)userId searchId:(int)searchId;

//MARK: Local AddJob Search Function
-(int) insertAddJobSearch:(AddJobSearchModelV2*)model;
-(int) updateAddJobSearch:(AddJobSearchModelV2*)model;
-(AddJobSearchModelV2*) loadAddJobSearchByUserId:(int)userId tasksFilter:(NSMutableArray*)listItems;
-(int) deleteAddJobSearchBySearchId:(int)searchId;
-(void) deleteAllSearchDataFromYesterday;

//MARK: Local AddJob Status
-(int) insertAddJobStatus:(AddJobStatusModel*)model;
-(UIImage*) loadAddJobStatusImageByStatusId:(int) statusId;
-(AddJobStatusModel*) loadAddJobStatusByStatusId:(int)statusId;
-(int) deleteAllAddJobStatus;

//MARK: Local AddJob Item
-(int) insertAddJobItem:(AddJobItemModelV2*)model;
-(int) updateAddJobItem:(AddJobItemModelV2*)model;
-(int) deleteAddJobItemByUserId:(int)userId;
-(int) deleteAllAddJobItems;
-(AddJobItemModelV2*) loadAddJobItemByUserId:(int)userId itemId:(int)itemid;
-(NSMutableArray*) loadAllAddJobItemByUserId:(int)userId;
-(NSMutableArray*) loadAllAddJobItemsByUserId:(int)userId ondayAddJobId:(int)ondayAddJobId;

//MARK: Local AddJob Room
-(int) insertAddJobRoom:(AddJobRoomModelV2*)model;
-(int) updateAddJobRoom:(AddJobRoomModelV2*)model;
-(int) deleteAddJobRoomByUserId:(int)userId;
-(int) deleteAddJobRoomByUserId:(int)userId floorId:(int)floorId searchId:(int)searchId;
-(int) deleteAddJobRoomByUserId:(int)userId roomId:(int)roomId searchId:(int)searchId;
-(int) deleteAddJobRoomByUserId:(int)userId roomAssignId:(int)roomAssignId searchId:(int)searchId;
-(int) deleteAllAddJobRooms;
-(NSMutableArray*)loadAddJobRoomsByUserId:(int)userId floorId:(int)floorId searchId:(int)searchId;
-(AddJobRoomModelV2*) loadAddJobRoomByUserId:(int)userId roomId:(int)roomId roomAssignId:(int)roomAssignId searchId:(int)searchId;
-(AddJobRoomModelV2*) loadAddJobRoomByRoomNumber:(int)roomId searchId:(int)searchId;
//This function apply for QR code scanning
//QR code storage database use same common data local (without search, searchId = 0)
-(AddJobRoomModelV2*) loadAddJobRoomQRCodeByUserId:(int)userId roomNumber:(NSString*)roomNumber;

//MARK: Local AddJob Category
-(int) insertAddJobCategory:(AddJobCategoryModelV2*)model;
-(int) updateAddJobCategory:(AddJobCategoryModelV2*)model;
-(int) deleteAddJobCategoryByUserId:(int)userId;
-(int) deleteAllAddJobCategories;
//-(NSMutableArray*)loadAddJobCategoriesByUserId:(int)userId;
-(NSString*)loadAddJobCategoriesStringByUserId:(int)userId roomId:(int)roomId roomAssignId:(int)roomAssignId;
-(NSString*)loadAddJobCategoriesStringByUserId:(int)userId roomId:(int)roomId searchId:(int)searchId;
-(AddJobCategoryModelV2*) loadAddJobCategoryByUserId:(int)userId categoryId:(int)categoryId;

//MARK: Local AddJob Detail
-(int) insertAddJobDetail:(AddJobDetailModelV2*)model;
-(int) updateAddJobDetail:(AddJobDetailModelV2*)model;
-(int) deleteAddJobDetailByUserId:(int)userId;
-(int) deleteAllAddJobDetails;
-(NSMutableArray*)loadAddJobDetailsByUserId:(int)userId roomId:(int)roomId roomAssignId:(int)roomAssignId;
-(NSMutableArray*)loadAddJobDetailsByUserId:(int)userId postStatus:(int)postStatus;
-(int)countAddJobDetailsByUserId:(int)userId postStatus:(int)postStatus;
-(AddJobDetailModelV2*) loadAddJobDetailByUserId:(int)userId ondayAddJobId:(int)ondayAddJobId;

//MARK: Local AddJob Detail Record
-(int) insertAddJobDetailRecord:(AddJobDetailRecordModelV2*)model;
-(int) updateAddJobDetailRecord:(AddJobDetailRecordModelV2*)model;
-(int) deleteAddJobDetailRecordByUserId:(int)userId;
-(int) deleteAllAddJobDetailRecords;
-(NSMutableArray*) loadAddJobDetailRecordsByUserId:(int)userId postStatus:(int)postStatus;
//Count AddJobDetailRecord with status
-(int) countAddJobDetailRecordsByUserId:(int)userId postStatus:(int)postStatus;

//MARK: Local AddJob RoomItem
-(int) insertAddJobRoomItem:(AddJobRoomItemModelV2*)model;
-(AddJobRoomItemModelV2*) loadAddJobRoomItemByUserId:(int)userId itemId:(int)itemId;
-(NSMutableArray*) loadListAddJobRoomItemByUserId:(int)userId itemId:(int)itemId;
//-(int) updateAddJobRoomItem:(AddJobRoomItemModelV2*)model;
-(int) deleteAddJobRoomItemByUserId:(int)userId;
-(int) deleteAddJobRoomItemByUserId:(int)userId ondayAddJobId:(int)ondayAddJobId;
-(int) deleteAllAddJobRoomItems;

//MARK: Local AddJob Search Room Category
-(int) insertAddJobSearchRoomCategory:(AddJobSearchRoomCategoryModelV2*)model;
-(int) deleteAddJobSearchRoomCategoryByUserId:(int)userId roomId:(int)roomId;
-(int) deleteAddJobSearchRoomCategoryByUserId:(int)userId roomId:(int)roomId categoryId:(int)categoryId;

//MARK: Local AddJob Guest Info
-(int) insertAddJobGuestInfo:(AddJobGuestInfoModelV2*)model;
-(int) updateAddJobGuestInfo:(AddJobGuestInfoModelV2*)model;
-(int) deleteAddJobGuestInfoByUserId:(int)userId;
-(int) deleteAllAddJobGuestInfo;
-(AddJobGuestInfoModelV2*) loadAddJobGuestInfoByUserId:(int)userId roomId:(int)roomId;

-(NSMutableArray*) sortListNumbers:(NSMutableArray*) listNumber ascending:(BOOL) isAscending;
-(NSString*) convertListTasksIdToString:(NSMutableArray*)listTasksId;
-(NSMutableArray*) convertTaskStringToNumber:(NSString*)tasksValue;

@end
