//
//  CheckListManagerV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 3/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CheckListAdapterV2.h"
#import "CheckListCategoriesAdapterV2.h"
#import "CheckListFormAdapterV2.h"
#import "CheckListItemAdapterV2.h"
#import "CheckListItemCoreAdapterV2.h"
#import "CheckListModelV2.h"
#import "CheckListCategoriesDBModelV2.h"
#import "CheckListTypeModelV2.h"
#import "CheckListDetailContentModelV2.h"
#import "CheckListItemCoreDBModelV2.h"
#import "ChecklistFormScoreModel.h"
#import "ChecklistFormScoreAdapter.h"
#import "ChecklistRoomType.h"

@interface CheckListManagerV2 : NSObject {
    
}

#pragma mark - CheckList 
-(NSInteger) insertCheckList:(CheckListModelV2 *) checkList;
-(NSInteger) updateCheckList:(CheckListModelV2 *) checkList;
-(BOOL) isCheckSaveChkListByRoomId:(NSString*) roomNumber AndUsrId:(NSInteger) usrId;
-(NSMutableArray *)loadAllCheckListByCurrentUserData;
-(CheckListModelV2 *)loadCheckListData:(CheckListModelV2*) checkList;

#pragma mark - CheckList Form
-(NSInteger) insertCheckListForm:(CheckListTypeModelV2 *) checkListForm;
-(NSMutableArray *) loadCheckListFormByCheckListFormScoreData:(NSInteger) chkFormScoreFormId;
-(NSInteger) updateCheckListForm:(CheckListTypeModelV2 *) checkListForm;
-(CheckListTypeModelV2 *)loadCheckListForm:(CheckListTypeModelV2 *) checkListForm;
-(CheckListTypeModelV2 *)loadCheckListFormLatestWithHotelId:(NSInteger)hotelId;
-(NSMutableArray *)loadAllCheckListForm;
-(NSMutableArray *)loadCheckListFormDataByHotelId:(int)hotelId;
-(NSMutableArray*) loadChecklistFormByUserId:(int)userId roomTypeId:(int) roomTypeId;

#pragma mark - CheckList Categories
-(NSInteger) insertCheckListCategories:(CheckListContentTypeModelV2 *) checkListCategories;
-(NSInteger) updateCheckListCategories:(CheckListContentTypeModelV2 *) checkListCategories;
-(NSMutableArray *) loadCheckListCategoriesByChkFormID:(NSInteger) checkListFormID;
-(CheckListContentTypeModelV2 *)loadCheckListCategories:(CheckListContentTypeModelV2*) checkListCategories;
-(CheckListContentTypeModelV2 *)loadCheckListCategoriesLatest;

#pragma mark - CheckList Item
-(NSInteger) insertCheckListItem:(CheckListDetailContentModelV2 *) checkListItem;
-(NSInteger) updateCheckListItem:(CheckListDetailContentModelV2 *) checkListItem;
-(NSInteger)getChecklistItemPassScoreByCheckListItemID:(NSInteger) checklistItemId;
-(NSInteger)getChecklistItemMandatoryScoreByCheckListItemID:(NSInteger) checklistItemId;
-(CheckListDetailContentModelV2 *) loadCheckListItem:(CheckListDetailContentModelV2 *) checkListItem;
-(NSMutableArray*) loadCheckListItemByChkCategoriesId:(NSInteger) checkListCategoriesId formId:(NSInteger)formId;
-(NSMutableArray*) loadCheckListItemByChkFormId:(NSInteger) checkListFormId;
-(CheckListDetailContentModelV2 *)loadCheckListItemLatest;

#pragma mark - CheckList Item Core
-(NSInteger) insertCheckListItemCore:(CheckListItemCoreDBModelV2 *) checkListItemCore;
-(NSMutableArray *) loadCheckListItemCoreByChkListFormScoreIdData:(NSInteger) checkListFormScoreID;
-(int) updateCheckListItemCoreByChkItemId:(CheckListItemCoreDBModelV2 *) checkListItemCore;
-(CheckListItemCoreDBModelV2 *)loadCheckListItemScoreByItemId:(NSInteger)itemId AndFormScoreId:(NSInteger)formScoreId;
-(CheckListItemCoreDBModelV2 *)loadCheckListItemScoreByItemId:(NSInteger)itemId;
-(NSMutableArray *)loadCheckListItemCoreByChkListFormScoreIdData:(NSInteger)checkListFormScoreID AndStatusPost:(NSInteger)statusPost;

#pragma mark - CheckListForm Score
-(BOOL) insertCheckListFormScore:(ChecklistFormScoreModel *) checkListFormScore;
-(BOOL) updateCheckListFormScore:(ChecklistFormScoreModel *) checkListFormScore;
-(int) deletCheckListFormScoreByRoomAssignmentId:(NSInteger) roomAssignmentId;
-(NSMutableArray *) loadChecklistFormScoreByChkListId:(NSInteger) chkListId AndChkUserId:(NSInteger) chkUserId;
-(ChecklistFormScoreModel *) loadCheckListFormScoreByChkListId:(NSInteger) chkListId AndChkFormId:(NSInteger) chkListFormId AndChkUserId:(NSInteger) chkUserId;
-(ChecklistFormScoreModel *)loadCheckListFormScoreByChkListId:(NSInteger)chkListId AndChkFormId:(NSInteger)chkListFormId;

#pragma mark - Delete methods for Checklist forms, Checklist categories, checklist items
-(int)deleteChecklistFormByFormId:(int)formId;
-(int)deleteAllCheckListForm;
-(int)deleteAllChecklistItems;
-(int)deleteChecklistCategoriesByChecklistFormId:(int) formId;
-(int)deleteChecklistItemsByFormId:(int)formId;
-(int)deleteChecklistItemsByCategoryId:(int)categoryId;

#pragma mark - WS CheckList
-(BOOL) getCheckListRoomTypeWSWithUserId:(NSInteger) userId HotelId:(NSInteger) hotelId AndArrayRoomModel:(NSMutableArray *) arrayRoom AndArrayChkList:(NSMutableArray *)arrayChkList;
-(NSInteger) getCheckListRoomTypeWSWithUserId:(NSInteger) userId hotelId:(NSInteger) hotelId lastModified:(NSString*)lastModified;
-(BOOL) getAllCheckListFormWSWithUserId:(NSInteger) userId AndHotelId:(NSInteger) hotelId;
//Hao Tran remove for change WS get checklist
//-(BOOL) getAllCheckListCategoriesWSWithUserId:(NSInteger) userId AndHotelId:(NSInteger) hotelId;

-(BOOL) getCheckListCategoriesWSWithUserId:(NSInteger) userId AndHotelId:(NSInteger) hotelId AndChecklistId:(int)checklistId;

-(BOOL) getAllCheckListItemWSWithUserId:(NSInteger) userId AndHotelId:(NSInteger) hotelId AndChecklistId:(int)checklistId;
-(NSInteger) getCheckListItemScoreForMaidWSWithUserId:(NSInteger) userId DutyAssignId:(NSInteger) roomAssignId AndLastModified:(NSString*) strLastModified;

//Remove for post check list items WS changed
//-(BOOL) postCheckListItemWSWithUserId:(NSInteger) userId RoomAssignId:(NSInteger) roomAssignId AndChkListItem:(CheckListDetailContentModelV2 *) model;

-(BOOL) postCheckListItemWSWithUserId:(NSInteger) userId RoomAssignId:(NSInteger) roomAssignId AndChkListItems:(NSMutableArray *) checklistItems;

-(NSInteger) numberOfMustPostChecklistItemRecords:(NSInteger) userId;

#pragma mark - Implement function check checklist status
-(BOOL)checkChecklistStatusWithRoomAssigmentId:(NSInteger)roomAssignmentId;

#pragma mark - Implement function clear data checklist
-(BOOL)clearDataChecklistWithRoomAssignmentId:(NSInteger)roomAssignmentId andInspectionStatus:(NSInteger)inspectionStatus;

#pragma mark - Post WSLog
-(BOOL)postWSLog:(int)userId message:(NSString*)messageValue messageType:(int)type;

@end
