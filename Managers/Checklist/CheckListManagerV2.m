//
//  CheckListManagerV2.m
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 3/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CheckListManagerV2.h"
#import "LogFileManager.h"
#import "LogObject.h"
#import "DataBaseExtension.h"
#import "ehkDefines.h"
#import "NetworkCheck.h"

@implementation CheckListManagerV2

#pragma mark - CheckList 
-(NSInteger)insertCheckList:(CheckListModelV2 *)checkList {
    CheckListAdapterV2 *adapter = [[CheckListAdapterV2 alloc] init];
    [adapter openDatabase];
    int result = [adapter insertCheckListData:checkList];
    [adapter close];
    return result;
}

-(NSInteger)updateCheckList:(CheckListModelV2 *)checkList {
    CheckListAdapterV2 *adapter = [[CheckListAdapterV2 alloc] init];
    [adapter openDatabase];
     NSInteger result = [adapter updateCheckListData:checkList];
    [adapter close];
    return result;
}

-(BOOL)isCheckSaveChkListByRoomId:(NSString*)roomNumber AndUsrId:(NSInteger)usrId {
    CheckListAdapterV2 *adapter = [[CheckListAdapterV2 alloc] init];
    CheckListModelV2 *model = [[CheckListModelV2 alloc] init];
    model.chkListRoomId = roomNumber;
    model.chkListUserId = usrId;
    [adapter openDatabase];
    model = [adapter loadCheckListModelByRoomIdAndUsrIDData:model];
    [adapter close];
    
    if (model.chkListStatus == tCompleted) {
        return YES;
    } else {
        return NO;
    }
}

-(NSMutableArray *)loadAllCheckListByCurrentUserData {
    CheckListAdapterV2 *adapter = [[CheckListAdapterV2 alloc] init];
    [adapter openDatabase];
    NSMutableArray *array = [adapter loadAllCheckListByCurrentUserData];
    [adapter close];
    return array;
}

-(CheckListModelV2 *)loadCheckListData:(CheckListModelV2 *)checkList {
    CheckListAdapterV2 *adapter = [[CheckListAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter loadCheckListData:checkList];
    [adapter close];
    return checkList;
}

#pragma mark - Checklist Room Type

-(NSInteger)insertChecklistRoomType:(ChecklistRoomType*)model
{
    if(model == nil){
        return 0;
    }
    
    DataRow *rowInsert = [[DataRow alloc] init];
    rowInsert.tableName = [NSString stringWithFormat:@"%@", CHECKLIST_ROOMTYPE];
    
    [rowInsert addColumnIntValue:(int)model.userId formatName:@"%@", clrtUserId];
    [rowInsert addColumnIntValue:(int)model.clrtChecklistID formatName:@"%@", clrtChecklistID];
    [rowInsert addColumnIntValue:(int)model.clrtRoomTypeID formatName:@"%@", clrtRoomTypeID];
    [rowInsert addColumnStringValue:model.lastModified formatName:@"%@", clrtLastModified];
    return [rowInsert insertDBRow];
}

-(NSInteger) deleteChecklistRoomTypesByUserId:(int)userId
{
    DataTable *tableDelete = [[DataTable alloc] initWithNameFormat:@"%@", CHECKLIST_ROOMTYPE];
    NSString *queryDelete = [NSString stringWithFormat:@"Delete from %@ Where %@ = %d", CHECKLIST_ROOMTYPE, clrtUserId, userId];
    int status = (int)[tableDelete excuteQueryNonSelect:queryDelete];
    return status;
}



#pragma mark - CheckList Form
-(NSInteger)insertCheckListForm:(CheckListTypeModelV2 *)checkListForm {
    CheckListFormAdapterV2 *adapter = [[CheckListFormAdapterV2 alloc] init];
    [adapter openDatabase];
    int result = [adapter insertCheckListFormData:checkListForm];
    [adapter close];
    return result;    
}

-(NSMutableArray *)loadCheckListFormByCheckListFormScoreData:(NSInteger)chkFormScoreFormId {
    CheckListFormAdapterV2 *adapter = [[CheckListFormAdapterV2 alloc] init];
    [adapter openDatabase];
    NSMutableArray *array = [adapter loadCheckListFormByCheckListFormScoreData:chkFormScoreFormId];
    [adapter close];
    return  array;
}

-(NSInteger)updateCheckListForm:(CheckListTypeModelV2 *)checkListForm {
    CheckListFormAdapterV2 *adapter = [[CheckListFormAdapterV2 alloc] init];
    [adapter openDatabase];
    NSInteger result = [adapter updateCheckListFormData:checkListForm];
    [adapter close];
    return result;
}

-(CheckListTypeModelV2 *)loadCheckListForm:(CheckListTypeModelV2 *)checkListForm {
    CheckListFormAdapterV2 *adapter = [[CheckListFormAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter loadCheckListFormData:checkListForm];
    [adapter close];
    return checkListForm;
}

-(NSMutableArray *)loadAllCheckListForm {
    CheckListFormAdapterV2 *adapter = [[CheckListFormAdapterV2 alloc] init];
    [adapter openDatabase];
    NSMutableArray *tmp = [adapter loadAllCheckListFormData];
    [adapter close];
    return tmp;
}

-(NSMutableArray *)loadCheckListFormDataByHotelId:(int)hotelId
{
    CheckListFormAdapterV2 *adapter = [[CheckListFormAdapterV2 alloc] init];
    [adapter openDatabase];
    NSMutableArray *tmp = [adapter loadCheckListFormDataByHotelId:hotelId];
    [adapter close];
    return tmp;
}

-(CheckListTypeModelV2 *)loadCheckListFormLatestWithHotelId:(NSInteger)hotelId {
    CheckListFormAdapterV2 *adapter = [[CheckListFormAdapterV2 alloc] init];
    [adapter openDatabase];
    CheckListTypeModelV2 *chkListType = [adapter loadCheckListFormLatestWithHotelId:hotelId];
    [adapter close];
    return chkListType;
}

-(NSMutableArray*) loadChecklistFormByUserId:(int) userId roomTypeId:(int) roomTypeId
{
    CheckListFormAdapterV2 *adapter = [[CheckListFormAdapterV2 alloc] init];
    [adapter openDatabase];
    NSMutableArray *tmp = [adapter loadChecklistFormByUserId:userId roomTypeId:roomTypeId];
    [adapter close];
    return tmp;
}

#pragma mark - CheckList Categories
-(NSInteger)insertCheckListCategories:(CheckListContentTypeModelV2 *)checkListCategories {
    CheckListCategoriesAdapterV2 *adapter = [[CheckListCategoriesAdapterV2 alloc] init];
    [adapter openDatabase];
    int result = [adapter insertCheckListCategoriesData:checkListCategories];
    [adapter close];
    return result;
}

-(NSInteger)updateCheckListCategories:(CheckListContentTypeModelV2 *)checkListCategories {
    CheckListCategoriesAdapterV2 *adapter = [[CheckListCategoriesAdapterV2 alloc] init];
    [adapter openDatabase];
    int result = [adapter updateCheckListCategoriesData:checkListCategories];
    [adapter close];
    return result;
}

-(CheckListContentTypeModelV2 *)loadCheckListCategories:(CheckListContentTypeModelV2 *)checkListCategories {
    CheckListCategoriesAdapterV2 *adapter = [[CheckListCategoriesAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter loadCheckListCategoriesData:checkListCategories];
    [adapter close];
    return checkListCategories;
}

-(NSMutableArray *)loadCheckListCategoriesByChkFormID:(NSInteger)checkListFormID {
    CheckListCategoriesAdapterV2 *adapter = [[CheckListCategoriesAdapterV2 alloc] init];
    [adapter openDatabase];
    NSMutableArray *array = [adapter loadCheckListCategoriesByChkFormIDData:checkListFormID];
    [adapter close];
    return array;
}

-(CheckListContentTypeModelV2 *)loadCheckListCategoriesLatest {
    CheckListCategoriesAdapterV2 *adapter = [[CheckListCategoriesAdapterV2 alloc] init];
    [adapter openDatabase];
    CheckListContentTypeModelV2 *chkListContentType = [adapter loadCheckListCategoriesLatest];
    [adapter close];
    return chkListContentType;
}

#pragma mark - CheckList Item
-(NSInteger)insertCheckListItem:(CheckListDetailContentModelV2 *)checkListItem {
    CheckListItemAdapterV2 *adapter = [[CheckListItemAdapterV2 alloc] init];
    [adapter openDatabase];
    int result = [adapter insertCheckListItemData:checkListItem];
    [adapter close];
    return result;
}

-(NSInteger)updateCheckListItem:(CheckListDetailContentModelV2 *)checkListItem {
    CheckListItemAdapterV2 *adapter = [[CheckListItemAdapterV2 alloc] init];
    [adapter openDatabase];
    int result = [adapter updateCheckListItemData:checkListItem];
    [adapter close];
    return result;
}

-(NSInteger)getChecklistItemPassScoreByCheckListItemID:(NSInteger) checklistItemId {
    CheckListItemAdapterV2 *adapter = [[CheckListItemAdapterV2 alloc] init];
    [adapter openDatabase];
    NSInteger result = [adapter getChecklistItemPassScoreByCheckListItemID:checklistItemId];
    [adapter close];
    return result;
}

-(NSInteger)getChecklistItemMandatoryScoreByCheckListItemID:(NSInteger) checklistItemId {
    CheckListItemAdapterV2 *adapter = [[CheckListItemAdapterV2 alloc] init];
    [adapter openDatabase];
    NSInteger result = [adapter getChecklistItemMandatoryScoreByCheckListItemID:checklistItemId];
    [adapter close];
    return result;
}

-(CheckListDetailContentModelV2 *)loadCheckListItem:(CheckListDetailContentModelV2 *)checkListItem {
    CheckListItemAdapterV2 *adapter = [[CheckListItemAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter loadCheckListItemData:checkListItem];
    [adapter close];
    return checkListItem;
}

-(NSMutableArray *)loadCheckListItemByChkCategoriesId:(NSInteger)checkListCategoriesId formId:(NSInteger)formId{
    CheckListItemAdapterV2 *adapter = [[CheckListItemAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter resetSqlCommand];
     NSMutableArray * array = [adapter loadCheckListItemByChkCategoriesIdData:checkListCategoriesId formId:formId];
    [adapter close];
    return array;
}

-(NSMutableArray *)loadCheckListItemByChkFormId:(NSInteger)checkListFormId {
    CheckListItemAdapterV2 *adapter = [[CheckListItemAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    NSMutableArray * array = [adapter loadCheckListItemByChkFormIdData:checkListFormId];
    [adapter close];
    return array;
}

-(CheckListDetailContentModelV2 *)loadCheckListItemLatest {
    CheckListItemAdapterV2 *adapter = [[CheckListItemAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    CheckListDetailContentModelV2 * chkDetailContent = [adapter loadCheckListItemLatest];
    [adapter close];
    return chkDetailContent;
}

#pragma mark - CheckList Item Core
-(NSInteger)insertCheckListItemCore:(CheckListItemCoreDBModelV2 *)checkListItemCore {
    CheckListItemCoreAdapterV2 *adapter = [[CheckListItemCoreAdapterV2 alloc] init];
    [adapter openDatabase];
    int result = [adapter insertCheckListItemCoreData:checkListItemCore];
    [adapter close];
    return result;
}

-(NSMutableArray *)loadCheckListItemCoreByChkListFormScoreIdData:(NSInteger)checkListFormScoreID {
    CheckListItemCoreAdapterV2 *adapter = [[CheckListItemCoreAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    NSMutableArray * array = [adapter loadCheckListItemCoreByChkListFormScoreIdData:checkListFormScoreID];
    [adapter close];
    return array;
}

-(int)updateCheckListItemCoreByChkItemId:(CheckListItemCoreDBModelV2 *)checkListItemCore {
    CheckListItemCoreAdapterV2 *adapter = [[CheckListItemCoreAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    NSInteger result = [adapter updateCheckListItemCoreByChkListItemIDData:checkListItemCore];
    [adapter close];
    return (int)result;
}

-(void)deleteCheckListItemScoreByItemId:(CheckListItemCoreDBModelV2 *)checkListItemCore {
    CheckListItemCoreAdapterV2 *adapter = [[CheckListItemCoreAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    [adapter deleteCheckListItemCoreData:checkListItemCore];
    [adapter close];
}

-(int)deleteSubmittedChecklistItemsByChecklistFormId:(NSInteger)checklistFormId {
    CheckListItemCoreAdapterV2 *adapter = [[CheckListItemCoreAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    int result = [adapter deleteSubmittedChecklistItemsByChecklistFormId:checklistFormId];
    [adapter close];
    return result;
}

-(int)deleteAllSubmittedChecklistItems {
    CheckListItemCoreAdapterV2 *adapter = [[CheckListItemCoreAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    int result = [adapter deleteAllSubmittedChecklistItems];
    [adapter close];
    return result;
}

-(CheckListItemCoreDBModelV2 *)loadCheckListItemScoreByItemId:(NSInteger)itemId AndFormScoreId:(NSInteger)formScoreId {
    CheckListItemCoreAdapterV2 *adapter = [[CheckListItemCoreAdapterV2 alloc] init];
    [adapter openDatabase];
    CheckListItemCoreDBModelV2 *model = [adapter loadCheckListItemScoreByItemId:itemId AndFormScoreId:formScoreId];
    [adapter close];
    return model;
}

-(CheckListItemCoreDBModelV2 *)loadCheckListItemScoreByItemId:(NSInteger)itemId{
    CheckListItemCoreAdapterV2 *adapter = [[CheckListItemCoreAdapterV2 alloc] init];
    [adapter openDatabase];
    CheckListItemCoreDBModelV2 *model = [adapter loadCheckListItemScoreByItemId:itemId];
    [adapter close];
    return model;
}

-(NSMutableArray *)loadCheckListItemCoreByChkListFormScoreIdData:(NSInteger)checkListFormScoreID AndStatusPost:(NSInteger)statusPost{
    
    CheckListItemCoreAdapterV2 *adapter = [[CheckListItemCoreAdapterV2 alloc] init];
    [adapter openDatabase];
    NSMutableArray * array = [adapter loadCheckListItemCoreByChkListFormScoreIdData:checkListFormScoreID AndStatusPost:statusPost];
    [adapter close];
    return array;
}

#pragma mark - CheckList Form Score
-(BOOL)insertCheckListFormScore:(ChecklistFormScoreModel *)checkListFormScore {
    ChecklistFormScoreAdapter *adapter = [[ChecklistFormScoreAdapter alloc] init];
    [adapter openDatabase];
    BOOL result = [adapter insertChecklistFormScoreModel:checkListFormScore];
    [adapter close];
    return result;
}

-(BOOL)updateCheckListFormScore:(ChecklistFormScoreModel *)checkListFormScore {
    ChecklistFormScoreAdapter *adapter = [[ChecklistFormScoreAdapter alloc] init];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    BOOL result = [adapter updateChecklistFormScoreModel:checkListFormScore];
    [adapter close];
    return result;
}

-(int)deletCheckListFormScoreByRoomAssignmentId:(NSInteger) roomAssignmentId {
    ChecklistFormScoreAdapter *adapter = [[ChecklistFormScoreAdapter alloc] init];
    [adapter openDatabase];
    int result = [adapter deletCheckListFormScoreByRoomAssignmentId:roomAssignmentId];
    [adapter close];
    return result;
}

-(NSMutableArray *)loadChecklistFormScoreByChkListId:(NSInteger)chkListId AndChkUserId:(NSInteger)chkUserId{
    ChecklistFormScoreAdapter *adapter = [[ChecklistFormScoreAdapter alloc] init];
    [adapter openDatabase];
    NSMutableArray *array = [adapter loadChecklistFormScoreByChkListId:chkListId AndChkUserId:chkUserId];
    [adapter close];
    return array;
}

-(ChecklistFormScoreModel *)loadCheckListFormScoreByChkListId:(NSInteger)chkListId AndChkFormId:(NSInteger)chkListFormId AndChkUserId:(NSInteger)chkUserId {
    ChecklistFormScoreAdapter *adapter = [[ChecklistFormScoreAdapter alloc] init];
    [adapter openDatabase];
    ChecklistFormScoreModel *model = [adapter loadCheckListFormScoreByChkListId:chkListId AndChkFormId:chkListFormId AndChkUserId:chkUserId];
    [adapter close];
    return model;
}

-(ChecklistFormScoreModel *)loadCheckListFormScoreByChkListId:(NSInteger)chkListId AndChkFormId:(NSInteger)chkListFormId{
    ChecklistFormScoreAdapter *adapter = [[ChecklistFormScoreAdapter alloc] init];
    [adapter openDatabase];
    ChecklistFormScoreModel *model = [adapter loadCheckListFormScoreByChkListId:chkListId AndChkFormId:chkListFormId];
    [adapter close];
    return model;
}

#pragma mark - Delete methods for Checklist forms, Checklist categories, checklist items

-(int)deleteChecklistByHotelId:(int) hotelId
{
    //Delete checklist with current HotelId
    DataTable *tableDelete = [[DataTable alloc] initWithNameFormat:@"%@",CHECKLIST_FORM];
    NSString *queryDelete = [NSString stringWithFormat:@"Delete from %@ Where %@ = %d", CHECKLIST_FORM, form_hotel_id, hotelId];
    int status = (int)[tableDelete excuteQueryNonSelect:queryDelete];
    return status;
}

//return 0 is no record to delete
-(int)deleteChecklistFormsNotInListFormId:(NSMutableArray*)listFormId
{
    if([listFormId count] <= 0){
        return 0;
    }
    
    NSMutableString *setOfFormId = [[NSMutableString alloc] initWithString:@""];
    for (int i = 0; i< [listFormId count]; i++) {
        
        NSNumber *curFormId = [listFormId objectAtIndex:i];
        int formId = [curFormId intValue];
        [setOfFormId appendFormat:@"%@ <> %d",form_id,formId];
        
        if(i >= [listFormId count] - 1)
        {
            [setOfFormId appendFormat:@""];
        }
        else
        {
            [setOfFormId appendFormat:@"and "];
        }
    }
    
    NSString *tableName = [NSString stringWithFormat:@"%@",CHECKLIST_FORM];
    DataTable *tableSelect = [[DataTable alloc] initWithName:tableName];
    
    NSString *querySelect = [NSString stringWithFormat:@"select %@ from %@ where %@", tableName, form_id, setOfFormId];
    
    DataRow *rowRef = [[DataRow alloc] init];
    NSString *columnName = @"formId";
    [rowRef addColumnInt:columnName];
    
    [tableSelect loadTableDataWithQuery:querySelect referenceRow:rowRef];
    
    int countResults = [tableSelect countRows];
    if(countResults > 0) {
        for (DataRow *curRow in tableSelect.rows) {
            DataColumn *curColumn = [curRow columnWithName:columnName];
            int formId = [(NSNumber*)curColumn.fieldValue intValue];
            
            //Delete old checklist
            [self deleteChecklistItemsByFormId:formId];
            [self deleteChecklistCategoriesByChecklistFormId:formId];
            [self deleteChecklistFormByFormId:formId];
        }
    }
    
    return countResults;
}

-(int)deleteChecklistFormByFormId:(int)formId
{
    NSString *tableName = [NSString stringWithFormat:@"%@",CHECKLIST_FORM];
    DataTable *tableDelete = [[DataTable alloc] initWithName:tableName];
    
    NSString *queryDelete = [NSString stringWithFormat:@"delete from %@ where %@ = %d", tableName, form_id, formId];
    int result = (int)[tableDelete excuteQueryNonSelect:queryDelete];
    
    return result;
}

-(int)deleteAllCheckListForm
{
    NSString *tableName = [NSString stringWithFormat:@"%@",CHECKLIST_FORM];
    DataTable *tableChecklistDelete = [[DataTable alloc] initWithName:tableName];
    
    //null row is delete all rows
    int result = [tableChecklistDelete dropDBRows];
    return result;
}

-(int)deleteChecklistCategoriesByChecklistFormId:(int) formId
{
    NSString *tableName = [NSString stringWithFormat:@"%@",CHECKLIST_CATEGORIES];
    DataTable *tableDelete = [[DataTable alloc] initWithName:tableName];
    
    NSString *queryDelete = [NSString stringWithFormat:@"delete from %@ where %@ = %d", tableName, chc_form_id, formId];
    int result = (int)[tableDelete excuteQueryNonSelect:queryDelete];
    
    return result;
}

-(int)deleteChecklistItemsByFormId:(int)formId
{
    NSString *tableName = [NSString stringWithFormat:@"%@",CHECKLIST_ITEM];
    DataTable *tableDelete = [[DataTable alloc] initWithName:tableName];
    
    NSString *queryDelete = [NSString stringWithFormat:@"delete from %@ where %@ = %d", tableName, chkitem_form_id, formId];
    int result = (int)[tableDelete excuteQueryNonSelect:queryDelete];
    
    return result;
}

-(int)deleteAllChecklistItems
{
    NSString *tableName = [NSString stringWithFormat:@"%@",CHECKLIST_ITEM];
    DataTable *tableDelete = [[DataTable alloc] initWithName:tableName];
    
    NSString *queryDelete = [NSString stringWithFormat:@"delete from %@", tableName];
    int result = (int)[tableDelete excuteQueryNonSelect:queryDelete];
    
    return result;
}

-(int)deleteChecklistItemsByCategoryId:(int)categoryId
{
    NSString *tableName = [NSString stringWithFormat:@"%@",CHECKLIST_ITEM];
    DataTable *tableDelete = [[DataTable alloc] initWithName:tableName];
    
    NSString *queryDelete = [NSString stringWithFormat:@"delete from %@ where %@ = %d", tableName, chkitem_category_id, categoryId];
    int result =(int) [tableDelete excuteQueryNonSelect:queryDelete];
    
    return result;
}

#pragma mark - WS CheckList
#pragma mark - CheckList Room Type WS
-(NSInteger) getCheckListRoomTypeWSWithUserId:(NSInteger)userId hotelId:(NSInteger) hotelId lastModified:(NSString*)lastModified
{
    int returnCode = RESPONSE_STATUS_OK;
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    if([LogFileManager isLogConsole]) {
        [binding setLogXMLInOut:YES];
    }
    eHousekeepingService_GetChecklistRoomType *request = [[eHousekeepingService_GetChecklistRoomType alloc] init];
    
    [request setIntUsrID:[NSNumber numberWithInteger:userId]];
    [request setIntHotelID:[NSNumber numberWithInteger:hotelId]];
    
    if(lastModified.length > 0) {
        [request setStrLastModified:lastModified];
    }
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetChecklistRoomTypeUsingParameters:request];
    NSArray *responseBodyParts = response.bodyParts;
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[SOAPFault class]]) {
            continue;
        }
        
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetChecklistRoomTypeResponse class]]) {
            eHousekeepingService_GetChecklistRoomTypeResponse *body = (eHousekeepingService_GetChecklistRoomTypeResponse *)bodyPart;
            if ([body.GetChecklistRoomTypeResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]]
                || [body.GetChecklistRoomTypeResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_NO_RERCORD_TO_DISPLAY]]
                || [body.GetChecklistRoomTypeResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_NO_NETWORK]])
            {
                
                [self deleteChecklistRoomTypesByUserId:(int)userId];
                NSMutableArray *arrayCheckListRoomType = body.GetChecklistRoomTypeResult.ChkListRmTyp.CheckListRoomType;
                for (eHousekeepingService_CheckListRoomType *chkRoomType in arrayCheckListRoomType) {
                    ChecklistRoomType *checklistRoomType = [[ChecklistRoomType alloc] init];
                    checklistRoomType.clrtRoomTypeID = [chkRoomType.clrtRoomTypeID intValue];
                    checklistRoomType.clrtChecklistID = [chkRoomType.clrtChecklistID intValue];
                    checklistRoomType.lastModified = chkRoomType.clrtLastModified;
                    checklistRoomType.userId = userId;
                    [self insertChecklistRoomType:checklistRoomType];
                }
                
                returnCode = [body.GetChecklistRoomTypeResult.ResponseStatus.respCode intValue];
                
            } else {
                returnCode = RESPONSE_STATUS_ERROR;
            }
        }
    }
    
    return returnCode;
}

-(BOOL)getCheckListRoomTypeWSWithUserId:(NSInteger)userId HotelId:(NSInteger)hotelId AndArrayRoomModel:(NSMutableArray *)arrayRoom AndArrayChkList:(NSMutableArray *)arrayChkList {
    BOOL returnCode = NO;
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
//    [binding setDefaultTimeout:60];
    
    eHousekeepingService_GetChecklistRoomType *request = [[eHousekeepingService_GetChecklistRoomType alloc] init];
    
    [request setIntUsrID:[NSNumber numberWithInteger:userId]];
    [request setIntHotelID:[NSNumber numberWithInteger:hotelId]];
    
//    LogObject *logObj = [[LogObject alloc] init];
//    logObj.dateTime = [NSDate date];
//    logObj.log = [NSString stringWithFormat:@"<%i><getCheckListRoomTypeWSWithUserId><intUsrID:%i intHotelID:%i>",userId,userId,hotelId];
//    LogFileManager *logManager = [[LogFileManager alloc] init];
//    [logManager save:logObj];
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetChecklistRoomTypeUsingParameters:request];
    NSArray *responseBodyParts = response.bodyParts;
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[SOAPFault class]]) {
            continue;   
        }
        
        //Get CheckList Item List
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetChecklistRoomTypeResponse class]]) {
            eHousekeepingService_GetChecklistRoomTypeResponse *body = (eHousekeepingService_GetChecklistRoomTypeResponse *)bodyPart;
            if ([body.GetChecklistRoomTypeResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]]) {                
                NSMutableArray *arrayCheckListRoomType = body.GetChecklistRoomTypeResult.ChkListRmTyp.CheckListRoomType;
                
                for (eHousekeepingService_CheckListRoomType *chkRoomType in arrayCheckListRoomType) {
                    for (RoomModelV2 *roomModel in arrayRoom) {                        
                        if ([chkRoomType.clrtRoomTypeID integerValue] == roomModel.room_TypeId) {                            
                            //Load CheckList Form Score from database
                            for (CheckListModelV2 *chkListModel in arrayChkList) {
                                if ([chkListModel.chkListRoomId isEqualToString:roomModel.room_Id]) {
                                    ChecklistFormScoreModel *chkFormScoreModel = [self loadCheckListFormScoreByChkListId:chkListModel.chkListId AndChkFormId: [chkRoomType.clrtChecklistID integerValue] AndChkUserId:[UserManagerV2 sharedUserManager].currentUser.userId];
                                    
                                    if (chkFormScoreModel.dfId != 0) {                                        
                                        //Update CheckList Form Score
                                        chkFormScoreModel.dfFormId = [chkRoomType.clrtChecklistID integerValue];
                                        chkFormScoreModel.dfChecklistId = chkListModel.chkListId;
//                                        chkFormScoreModel.dfScore = -1; //default value
                                        //chkFormScoreModel.dfLastModified = chkRoomType.clrtLastModified;
                                        [self updateCheckListFormScore:chkFormScoreModel];                                        
                                    } else {
                                       //Insert CheckList Form Score 
                                        ChecklistFormScoreModel *chkFormScoreDB = [[ChecklistFormScoreModel alloc] init];
                                        chkFormScoreDB.dfId = [chkRoomType.clrtID integerValue];
                                        chkFormScoreDB.dfFormId = [chkRoomType.clrtChecklistID integerValue];
                                        chkFormScoreDB.dfChecklistId = chkListModel.chkListId;
                                        chkFormScoreDB.dfUserId = [UserManagerV2 sharedUserManager].currentUser.userId;
                                        chkFormScoreDB.dfScore = -1; //default value
                                        //chkFormScoreDB.dfLastModified = chkRoomType.clrtLastModified;
                                        [self insertCheckListFormScore:chkFormScoreDB];
                                    }
                                }
                            }
                        }
                    }
                }
                
                returnCode = YES;
            } else {
                returnCode = NO;
            }
        }
    }
    
    return returnCode;    
}

#pragma mark - CheckList Form With WS
-(BOOL)getAllCheckListFormWSWithUserId:(NSInteger)userId AndHotelId:(NSInteger)hotelId {
    BOOL returnCode = NO;
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole])
    {
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_GetAllChecklist *request = [[eHousekeepingService_GetAllChecklist alloc] init];
    
//    CheckListTypeModelV2 *chkListType = [self loadCheckListFormLatestWithHotelId:hotelId];
    
    [request setIntUsrID:[NSNumber numberWithInteger:userId]];
    [request setIntHotelID:[NSNumber numberWithInteger:hotelId]];
    
//    if (chkListType.chkTypeLastModified) {
//        [request setStrLastModified:chkListType.chkTypeLastModified];
//    }
    
//    LogObject *logObj = [[LogObject alloc] init];
//    logObj.dateTime = [NSDate date];
//    logObj.log = [NSString stringWithFormat:@"<%i><getAllCheckListFormWSWithUserId><intUsrID:%i intHotelID:%i lastModified:%@>",userId,userId,hotelId,chkListType.chkTypeLastModified];
//    LogFileManager *logManager = [[LogFileManager alloc] init];
//    [logManager save:logObj];
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetAllChecklistUsingParameters:request];
    NSArray *responseBodyParts = response.bodyParts;
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[SOAPFault class]]) {            
            continue;
        }
        
        //GetAllCheckList
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetAllChecklistResponse class]]) {            
            eHousekeepingService_GetAllChecklistResponse *body = (eHousekeepingService_GetAllChecklistResponse *)bodyPart;
            
            if ([body.GetAllChecklistResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]]
                || [body.GetAllChecklistResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_NO_RERCORD_TO_DISPLAY]]) {
                
                //Hao Tran [20141120/Fix Checklist show duplicated] - Fixed checklist showed duplicated for deploy RC
                [self deleteChecklistByHotelId:(int)hotelId];
                
                NSMutableArray *arrayCheckListForm = body.GetAllChecklistResult.SprChkList.SupervisorCheckList;
                
                NSMutableArray *listChecklistFormId = [NSMutableArray array];
                
                for (eHousekeepingService_SupervisorCheckList *supervisorChkList in arrayCheckListForm) {
                    //Get CheckList Form from database
                    CheckListTypeModelV2 *chkListFormModel = [[CheckListTypeModelV2 alloc] init];
                    chkListFormModel.chkTypeId = [supervisorChkList.cltID integerValue];                    
                    
                    //Add checklist form id return from server
                    [listChecklistFormId addObject:supervisorChkList.cltID];
                    
                    [self loadCheckListForm:chkListFormModel];
                    if (chkListFormModel.chkTypeHotelId != 0) {                        
                        if (![chkListFormModel.chkTypeLastModified isEqualToString:supervisorChkList.cltLastModified]){
                            //Update CheckList Form
                            chkListFormModel.chkTypeName = supervisorChkList.cltName;
                            chkListFormModel.chkTypeLangName = supervisorChkList.cltLang;
                            chkListFormModel.chkType = [supervisorChkList.cltChecklistType integerValue];
                            chkListFormModel.chkTypePercentageOverall = [supervisorChkList.cltTotalPointPossible integerValue];
                            chkListFormModel.chkTypePassingScore = [supervisorChkList.cltPassingScore integerValue];
                            chkListFormModel.chkTypeLastModified = supervisorChkList.cltLastModified;
                            chkListFormModel.chkTypeHotelId = hotelId;
                            
                            [self updateCheckListForm:chkListFormModel];                            
                        }
                    } else {
                        //Insert CheckList Form
                        CheckListTypeModelV2 *chkListFormDB = [[CheckListTypeModelV2 alloc] init];
                        chkListFormDB.chkTypeId = [supervisorChkList.cltID integerValue];
                        chkListFormDB.chkTypeName = supervisorChkList.cltName;
                        chkListFormDB.chkTypeLangName = supervisorChkList.cltLang;
                        chkListFormDB.chkType = [supervisorChkList.cltChecklistType integerValue];
                        chkListFormDB.chkTypePercentageOverall = [supervisorChkList.cltTotalPointPossible integerValue];
                        chkListFormDB.chkTypePassingScore = [supervisorChkList.cltPassingScore integerValue];
                        chkListFormDB.chkTypeLastModified = supervisorChkList.cltLastModified;
                        chkListFormDB.chkTypeHotelId = hotelId;
                        
                        [self insertCheckListForm:chkListFormDB];
                    }                    
                }
                
                returnCode = YES;
            } else {
                returnCode = NO;
            }            
        }
    }
    
    return returnCode;    
}

#pragma mark - CheckList Categories WS

//Hao Tran remove for change WS get checklist
//-(BOOL)getAllCheckListCategoriesWSWithUserId:(NSInteger)userId AndHotelId:(NSInteger)hotelId {
//    BOOL returnCode = NO;
//    
//    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
//    
//    //Allow write log or not
//    if([LogFileManager isLogConsole])
//    {
//        [binding setLogXMLInOut:YES];
//    }
//    eHousekeepingService_GetChecklistCategoryList *request = [[eHousekeepingService_GetChecklistCategoryList alloc] init];
//    
//    CheckListContentTypeModelV2 *chkListContentType = [self loadCheckListCategoriesLatest];
//    
//    [request setIntUsrID:[NSNumber numberWithInt:userId]];
//    [request setIntHotelID:[NSNumber numberWithInt:hotelId]];
//    if (chkListContentType.chkLastModified) {
//        [request setStrLastModified:chkListContentType.chkLastModified];
//    }
//    
////    LogObject *logObj = [[LogObject alloc] init];
////    logObj.dateTime = [NSDate date];
////    logObj.log = [NSString stringWithFormat:@"<%i><getAllCheckListCategoriesWSWithUserId><intUsrID:%i intHotelID:%i lastModified:%@>",userId,userId,hotelId,chkListContentType.chkLastModified];
////    LogFileManager *logManager = [[LogFileManager alloc] init];
////    [logManager save:logObj];
//    
//    eHousekeepingServiceSoap12BindingResponse *response = [binding GetChecklistCategoryListUsingParameters:request];
//    NSArray *responseBodyParts = response.bodyParts;
//    
//    for (id bodyPart in responseBodyParts) {
//        if ([bodyPart isKindOfClass:[SOAPFault class]]) {
//            continue;   
//        }
//        
//        //Get CheckList Categories List
//        if ([bodyPart isKindOfClass:[eHousekeepingService_GetChecklistCategoryListResponse class]]) {
//            eHousekeepingService_GetChecklistCategoryListResponse *body = (eHousekeepingService_GetChecklistCategoryListResponse *)bodyPart;
//            
//            if ([body.GetChecklistCategoryListResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]]) {                
//                NSMutableArray *arrayCheckListCategoriesWS = body.GetChecklistCategoryListResult.ChkListCategoryLst.CheckListCategory;
//                
//                for (eHousekeepingService_CheckListCategory *chkCategoryWS in arrayCheckListCategoriesWS) {                    
//                    //Get CheckList Categories from database
//                    CheckListContentTypeModelV2 *chkListCategoryModel = [[CheckListContentTypeModelV2 alloc] init];
//                    chkListCategoryModel.chkContentId = [chkCategoryWS.clcID integerValue];                    
//                    
//                    [self loadCheckListCategories:chkListCategoryModel];
//                    
//                    if (chkListCategoryModel.chkTypeId != 0) {                        
//                        if (![chkListCategoryModel.chkLastModified isEqualToString:chkCategoryWS.clcLastModified]) {
//                            //Update CheckList Category
//                            chkListCategoryModel.chkContentName = chkCategoryWS.clcName;
//                            chkListCategoryModel.chkContentNameLang = chkCategoryWS.clcLang;
//                            chkListCategoryModel.chkLastModified = chkCategoryWS.clcLastModified;
//                            //chkListCategoryModel.chkTypeId = chkCategoryWS.clcID;
//                            [self updateCheckListCategories:chkListCategoryModel];                            
//                        }
//                    } else {
//                        //Insert CheckList Category
//                        CheckListContentTypeModelV2 *chkListCategoryDB = [[CheckListContentTypeModelV2 alloc] init];
//                        chkListCategoryDB.chkContentId = [chkCategoryWS.clcID integerValue];
//                        chkListCategoryDB.chkContentName = chkCategoryWS.clcName;
//                        chkListCategoryDB.chkContentNameLang = chkCategoryWS.clcLang;
//                        chkListCategoryDB.chkLastModified = chkCategoryWS.clcLastModified;
//                        //chkListCategoryModel.chkTypeId = chkCategoryWS.clcID;
//                        [self insertCheckListCategories:chkListCategoryDB];                        
//                    }                    
//                }
//                
//                returnCode = YES;
//            } else {
//                returnCode = NO;
//            }
//        }
//    }
//    
//    return returnCode;
//}

-(BOOL) getCheckListCategoriesWSWithUserId:(NSInteger) userId AndHotelId:(NSInteger) hotelId AndChecklistId:(int)checklistId
{
    BOOL returnCode = NO;
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole]) {
        [binding setLogXMLInOut:YES];
    }
    eHousekeepingService_GetChecklistCategoryList *request = [[eHousekeepingService_GetChecklistCategoryList alloc] init];
    
    //CheckListContentTypeModelV2 *chkListContentType = [self loadCheckListCategoriesLatest];
    
    [request setIntUsrID:[NSNumber numberWithInteger:userId]];
    [request setIntHotelID:[NSNumber numberWithInteger:hotelId]];
    [request setIntCheckListID:[NSNumber numberWithInteger:checklistId]];
    
//    if (chkListContentType.chkLastModified) {
//        [request setStrLastModified:chkListContentType.chkLastModified];
//    }
    
    //    LogObject *logObj = [[LogObject alloc] init];
    //    logObj.dateTime = [NSDate date];
    //    logObj.log = [NSString stringWithFormat:@"<%i><getAllCheckListCategoriesWSWithUserId><intUsrID:%i intHotelID:%i lastModified:%@>",userId,userId,hotelId,chkListContentType.chkLastModified];
    //    LogFileManager *logManager = [[LogFileManager alloc] init];
    //    [logManager save:logObj];
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetChecklistCategoryListUsingParameters:request];
    NSArray *responseBodyParts = response.bodyParts;
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[SOAPFault class]]) {
            continue;
        }
        
        //Get CheckList Categories List
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetChecklistCategoryListResponse class]]) {
            eHousekeepingService_GetChecklistCategoryListResponse *body = (eHousekeepingService_GetChecklistCategoryListResponse *)bodyPart;
            
            if ([body.GetChecklistCategoryListResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]]) {
                NSMutableArray *arrayCheckListCategoriesWS = body.GetChecklistCategoryListResult.ChkListCategoryLst.CheckListCategory;
                
                //delete old data
                [self deleteChecklistCategoriesByChecklistFormId:checklistId];
                
                for (eHousekeepingService_CheckListCategory *chkCategoryWS in arrayCheckListCategoriesWS) {
                    //Get CheckList Categories from database
                    CheckListContentTypeModelV2 *chkListCategoryModel = [[CheckListContentTypeModelV2 alloc] init];
                    chkListCategoryModel.chkContentId = [chkCategoryWS.clcID integerValue];
                    
                    [self loadCheckListCategories:chkListCategoryModel];
                    
                    if (chkListCategoryModel.chkTypeId != 0) {
                        if (![chkListCategoryModel.chkLastModified isEqualToString:chkCategoryWS.clcLastModified]) {
                            //Update CheckList Category
                            chkListCategoryModel.chkContentName = chkCategoryWS.clcName;
                            chkListCategoryModel.chkContentNameLang = chkCategoryWS.clcLang;
                            chkListCategoryModel.chkLastModified = chkCategoryWS.clcLastModified;
                            chkListCategoryModel.chkTypeId = checklistId;
                            [self updateCheckListCategories:chkListCategoryModel];
                        }
                    } else {
                        //Insert CheckList Category
                        CheckListContentTypeModelV2 *chkListCategoryDB = [[CheckListContentTypeModelV2 alloc] init];
                        chkListCategoryDB.chkContentId = [chkCategoryWS.clcID integerValue];
                        chkListCategoryDB.chkContentName = chkCategoryWS.clcName;
                        chkListCategoryDB.chkContentNameLang = chkCategoryWS.clcLang;
                        chkListCategoryDB.chkLastModified = chkCategoryWS.clcLastModified;
                        chkListCategoryDB.chkTypeId = checklistId;
                        [self insertCheckListCategories:chkListCategoryDB];
                    }
                }
                
                returnCode = YES;
            } else {
                returnCode = NO;
            }
        }
    }
    
    return returnCode;
}

#pragma mark - CheckList Item WS
-(BOOL)getAllCheckListItemWSWithUserId:(NSInteger)userId AndHotelId:(NSInteger)hotelId AndChecklistId:(int)checklistId{
    BOOL returnCode = NO;
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole])
    {
        [binding setLogXMLInOut:YES];
    }
//    [binding setDefaultTimeout:30];
    
    eHousekeepingService_GetChecklistItemList *request = [[eHousekeepingService_GetChecklistItemList alloc] init];
    
    //CheckListDetailContentModelV2 *chkDetailContent = [self loadCheckListItemLatest];
    
    [request setIntUsrID:[NSNumber numberWithInteger:userId]];
    [request setIntHotelID:[NSNumber numberWithInteger:hotelId]];
    [request setIntCheckListID:[NSNumber numberWithInteger:checklistId]];
    
//    if (chkDetailContent.chkDetailContentLastUpdate) {
//        [request setStrLastModified:chkDetailContent.chkDetailContentLastUpdate];
//    }
    
//    LogObject *logObj = [[LogObject alloc] init];
//    logObj.dateTime = [NSDate date];
//    logObj.log = [NSString stringWithFormat:@"<%i><getAllCheckListItemWSWithUserId><intUsrID:%i intHotelID:%i lastModified:%@>",userId,userId,hotelId,chkDetailContent.chkDetailContentLastUpdate];
//    LogFileManager *logManager = [[LogFileManager alloc] init];
//    [logManager save:logObj];
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetChecklistItemListUsingParameters:request];
    NSArray *responseBodyParts = response.bodyParts;
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[SOAPFault class]]) {
            continue;   
        }
        
        //Get CheckList Item List
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetChecklistItemListResponse class]]) {            
            eHousekeepingService_GetChecklistItemListResponse *body = (eHousekeepingService_GetChecklistItemListResponse *)bodyPart;
            
            if ([body.GetChecklistItemListResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]]) {
                
                //Delete old checklist items
                //[self deleteAllChecklistItems];
                [self deleteChecklistItemsByFormId:checklistId];
                
                NSMutableArray *arrayCheckListItemWS = body.GetChecklistItemListResult.ChkLstItm.CheckListItem;
                
                for (eHousekeepingService_CheckListItem *chkListItemWS in arrayCheckListItemWS) {                    
                    //Get CheckList Item from database
                    CheckListDetailContentModelV2 *chkListItemModel = [[CheckListDetailContentModelV2 alloc] init];
                    chkListItemModel.chkDetailContentId = [chkListItemWS.cliID integerValue];                    
                    
                    [self loadCheckListItem:chkListItemModel];
                    
                    if (chkListItemModel.chkContentId != 0) {
                        if (![chkListItemModel.chkDetailContentLastUpdate isEqualToString:chkListItemWS.cliLastModified]) {
                            //Update CheckList Item
                            chkListItemModel.chkDetailContentName = chkListItemWS.cliName;
                            chkListItemModel.chkDetailContentLang = chkListItemWS.cliLang;
                            chkListItemModel.chkDetailContentLastUpdate = chkListItemWS.cliLastModified;
                            chkListItemModel.chkContentId = [chkListItemWS.cliCategoryID integerValue];
                            chkListItemModel.chkDetailContentPointInpected = [chkListItemWS.cliRatingValue integerValue];
                            chkListItemModel.chkTypeId = checklistId;
                            chkListItemModel.chkMandatoryPass = [chkListItemWS.cliMandatoryPass integerValue];
                            [self updateCheckListItem:chkListItemModel];
                        }
                    } else {
                        //Insert CheckList Item
                        CheckListDetailContentModelV2 *chkListItemDB = [[CheckListDetailContentModelV2 alloc] init];
                        chkListItemDB.chkDetailContentId = [chkListItemWS.cliID integerValue];
                        chkListItemDB.chkDetailContentName = chkListItemWS.cliName;
                        chkListItemDB.chkDetailContentLang = chkListItemWS.cliLang;
                        chkListItemDB.chkDetailContentLastUpdate = chkListItemWS.cliLastModified;
                        chkListItemDB.chkContentId = [chkListItemWS.cliCategoryID integerValue];
                        chkListItemDB.chkDetailContentPointInpected = [chkListItemWS.cliRatingValue integerValue];
                        chkListItemDB.chkTypeId = checklistId;
                        chkListItemDB.chkMandatoryPass = [chkListItemWS.cliMandatoryPass integerValue];
                        [self insertCheckListItem:chkListItemDB];                 
                    }
                }
                returnCode = YES;
            } else {
                returnCode = NO;
            }
        }
    }
    
    return returnCode;
}

#pragma mark - Get CheckList Score For Maid
-(NSInteger)getCheckListItemScoreForMaidWSWithUserId:(NSInteger)userId DutyAssignId:(NSInteger)roomAssignId AndLastModified:(NSString *)strLastModified {
    NSInteger result = RESPONSE_STATUS_NO_RERCORD_TO_DISPLAY;
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole])
    {
        [binding setLogXMLInOut:YES];
    }
//    [binding setDefaultTimeout:60];
    
    eHousekeepingService_GetCheckListItemScoreForMaid *request = [[eHousekeepingService_GetCheckListItemScoreForMaid alloc] init];
    
    [request setIntUsrID:[NSNumber numberWithInteger:userId]];
    [request setIntDutyAssignID:[NSNumber numberWithInteger:roomAssignId]];
    [request setStrLastModified:strLastModified]; 
    
//    LogObject *logObj = [[LogObject alloc] init];
//    logObj.dateTime = [NSDate date];
//    logObj.log = [NSString stringWithFormat:@"<%i><getCheckListItemScoreForMaidWSWithUserId><intUsrID:%i intDutyAssignID:%i lastModified:%@>",userId,userId,roomAssignId,strLastModified];
//    LogFileManager *logManager = [[LogFileManager alloc] init];
//    [logManager save:logObj];
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetCheckListItemScoreForMaidUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[SOAPFault class]]) {
            continue;   
        }

        if ([bodyPart isKindOfClass:[eHousekeepingService_GetCheckListItemScoreForMaidResponse class]]) {            
            eHousekeepingService_GetCheckListItemScoreForMaidResponse *body = (eHousekeepingService_GetCheckListItemScoreForMaidResponse *)bodyPart;
            
            if ([body.GetCheckListItemScoreForMaidResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]]) {                
                
                //get list checkList item score
                NSMutableArray *arrayCheckListItemScoreWS = body.GetCheckListItemScoreForMaidResult.ChkLstScoreForMaid.CheckListScoreForMaid;
                
                //load data of checkList by roomId
                CheckListModelV2 *chkModel = [[CheckListModelV2 alloc] init];
                chkModel.chkListId  = roomAssignId;
                chkModel.chkListUserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
                [self loadCheckListData:chkModel];
                            
                //load array data of check list form score
                NSMutableArray *arrayChkListFormScore = [self loadChecklistFormScoreByChkListId:chkModel.chkListId AndChkUserId:[UserManagerV2 sharedUserManager].currentUser.userId];
                //Clear old checklist data
                if (arrayCheckListItemScoreWS.count > 0) {
                    [self deleteAllSubmittedChecklistItems];
                }
                
                for (ChecklistFormScoreModel *formScore in arrayChkListFormScore) {
                    if (arrayCheckListItemScoreWS.count > 0) {
                        formScore.dfPostStatus = POST_STATUS_POSTED;
                        formScore.dfScore = -1;
                        [self updateCheckListFormScore:formScore];
                    } else {
                        if (formScore.dfPostStatus == POST_STATUS_POSTED) {
                            formScore.dfPostStatus = POST_STATUS_UN_CHANGED;
                            formScore.dfScore = -1;
                            [self updateCheckListFormScore:formScore];
                        }
                    }
                    
                    for (eHousekeepingService_CheckListScoreForMaid *chkListItemScoreWS in arrayCheckListItemScoreWS) {
                        //load checklist item
                        CheckListDetailContentModelV2 *chkListItem = [[CheckListDetailContentModelV2 alloc] init];
                        chkListItem.chkDetailContentId = [chkListItemScoreWS.clsCheckListItemID integerValue];
                        [self loadCheckListItem:chkListItem];
                        
                        if (chkListItem.chkTypeId == formScore.dfFormId) {                            
                            //load checklist item score
                            CheckListItemCoreDBModelV2 *chkListItemScore =  [self loadCheckListItemScoreByItemId:[chkListItemScoreWS.clsCheckListItemID integerValue] AndFormScoreId:formScore.dfId];
                            
                            if (chkListItemScore.chkItemCoreId != 0) {
                                //Update checklist item score
                                chkListItemScore.chkPostStatus = POST_STATUS_POSTED;
                                chkListItemScore.chkItemCore = [chkListItemScoreWS.clsScore integerValue];
                                [self updateCheckListItemCoreByChkItemId:chkListItemScore];
                            } else {
                                //insert checklist item score
                                CheckListItemCoreDBModelV2 *chkListItemScoreDB = [[CheckListItemCoreDBModelV2 alloc] init];
                                chkListItemScoreDB.chkPostStatus = POST_STATUS_POSTED;
                                chkListItemScoreDB.chkFormScore = formScore.dfId;
                                chkListItemScoreDB.chkItemCore = [chkListItemScoreWS.clsScore integerValue];
                                chkListItemScoreDB.chkItemCoreDetailContentId = [chkListItemScoreWS.clsCheckListItemID integerValue];
                                [self insertCheckListItemCore:chkListItemScoreDB];
                            }
                        }
                    }
                }
                
                result = RESPONSE_STATUS_OK;
            } else {
                result = RESPONSE_STATUS_NO_RERCORD_TO_DISPLAY;
            }
        }
    }
    
    return result;
}

#pragma mark - Post CheckList Items
//Hao Tran Remove for posting check list WS changed
//-(BOOL)postCheckListItemWSWithUserId:(NSInteger)userId RoomAssignId:(NSInteger)roomAssignId AndChkListItem:(CheckListDetailContentModelV2 *)model {
//    BOOL result = NO;
//    
//    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
//    
//    //Allow write log or not
//    if([LogFileManager isLogConsole])
//    {
//        [binding setLogXMLInOut:YES];
//    }
//    
//    eHousekeepingService_PostChecklistItem *request = [[eHousekeepingService_PostChecklistItem alloc] init];
//    
//    if (model.itemCoreModel.chkPostStatus == POST_STATUS_SAVED_UNPOSTED) {        
//        [request setIntUsrID:[NSNumber numberWithInteger:userId]];
//        [request setIntRoomAssignID:[NSNumber numberWithInteger:roomAssignId]];
//        [request setIntChecklistID:[NSNumber numberWithInteger:model.chkTypeId]];
//        [request setIntChecklistItemID:[NSNumber numberWithInteger:model.chkDetailContentId]];
//        [request setIntScoreValue:[NSNumber numberWithInteger:model.itemCoreModel.chkItemCore]];        
//        
//        LogObject *logObj = [[LogObject alloc] init];
//        logObj.dateTime = [NSDate date];
//        logObj.log = [NSString stringWithFormat:@"<%i><postCheckListItemWSWithUserId><intUsrID:%i intDutyAssignID:%i intChecklistID:%i intChecklistItemID:%i intScoreValue:%i>",userId,userId,roomAssignId,model.chkTypeId,model.chkDetailContentId,model.itemCoreModel.chkItemCore];
//        LogFileManager *logManager = [[LogFileManager alloc] init];
//        [logManager save:logObj];
//        
//        eHousekeepingServiceSoap12BindingResponse *response = [binding PostChecklistItemUsingParameters:request];
//        
//        NSArray *responseBodyParts = response.bodyParts;
//        
//        for (id bodyPart in responseBodyParts) {
//            if ([bodyPart isKindOfClass:[eHousekeepingService_PostChecklistItemResponse class]]) {                
//                eHousekeepingService_PostChecklistItemResponse *dataBody = (eHousekeepingService_PostChecklistItemResponse *)bodyPart;
//                
//                if (dataBody != nil) {
//                    if ([dataBody.PostChecklistItemResult.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_NEW_RECORD_ADDED]]) {
//                        //change post status in minibar
//                        result = YES;
//                        
//                        if (model.itemCoreModel.chkPostStatus == POST_STATUS_SAVED_UNPOSTED) {
//                            model.itemCoreModel.chkPostStatus = POST_STATUS_POSTED;
//                            [self updateCheckListItemCoreByChkItemId:model.itemCoreModel];
//                        }                        
//                    }  else {
//                        result = NO;
//                    }
//                }
//            }
//        }
//    } else {
//        return YES;
//    }
//    return result;
//}

-(BOOL) postCheckListItemWSWithUserId:(NSInteger) userId RoomAssignId:(NSInteger) roomAssignId AndChkListItems:(NSMutableArray *) checklistItems
{
    if([checklistItems count] <= 0)
    {
        return NO;
    }
    
    BOOL result = NO;

    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];

    //Allow write log or not
    if([LogFileManager isLogConsole]){
        [binding setLogXMLInOut:YES];
    }

    eHousekeepingService_PostChecklistItems *request = [[eHousekeepingService_PostChecklistItems alloc] init];
    
    //Check if checklist posted
    CheckListDetailContentModelV2 *firstChecklistItem = (CheckListDetailContentModelV2*)[checklistItems objectAtIndex:0];
    
    if (firstChecklistItem.itemCoreModel.chkPostStatus == POST_STATUS_SAVED_UNPOSTED) {
        [request setIntUserID:[NSNumber numberWithInteger:userId]];
        [request setIntRoomAssignID:[NSNumber numberWithInteger:roomAssignId]];
        [request setIntChecklistID:[NSNumber numberWithInteger:firstChecklistItem.chkTypeId]];
        
        eHousekeepingService_ArrayOfPostChecklistItem *postItems = [[eHousekeepingService_ArrayOfPostChecklistItem alloc] init];
        
        for (CheckListDetailContentModelV2 *curCheckListItem in checklistItems) {
            eHousekeepingService_PostChecklistItem *postItem = [[eHousekeepingService_PostChecklistItem alloc] init];
            
            [postItem setIntChecklistItemID:[NSNumber numberWithInteger:curCheckListItem.chkDetailContentId]];
            [postItem setIntScoreValue:[NSNumber numberWithInteger:curCheckListItem.itemCoreModel.chkItemCore]];
            
            [postItems.PostChecklistItem addObject: postItem];
            
            //Bind data checklist item
            LogObject *logObj = [[LogObject alloc] init];
            logObj.dateTime = [NSDate date];
            logObj.log = [NSString stringWithFormat:@"<%i><postCheckListItemWSWithUserId><intUsrID:%i intDutyAssignID:%i intChecklistID:%i intChecklistItemID:%i intScoreValue:%i>",(int)userId,(int)userId,(int)roomAssignId,(int)curCheckListItem.chkTypeId,(int)curCheckListItem.chkDetailContentId,(int)curCheckListItem.itemCoreModel.chkItemCore];
            LogFileManager *logManager = [[LogFileManager alloc] init];
            [logManager save:logObj];
        }
        
        
        [request setPostChkLstItems:postItems];
        
        eHousekeepingServiceSoap12BindingResponse *response = [binding PostChecklistItemsUsingParameters:request];
        NSArray *responseBodyParts = response.bodyParts;
        for (id bodyPart in responseBodyParts) {
            if ([bodyPart isKindOfClass:[eHousekeepingService_PostChecklistItemsResponse class]]) {
                eHousekeepingService_PostChecklistItemsResponse *dataBody = (eHousekeepingService_PostChecklistItemsResponse *)bodyPart;

                if (dataBody != nil) {
                    if ([dataBody.PostChecklistItemsResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_NEW_RECORD_ADDED]]) {
                        //change post status in minibar
                        result = YES;
                        for (CheckListDetailContentModelV2 *curCheckListItem in checklistItems) {
                            curCheckListItem.itemCoreModel.chkPostStatus = POST_STATUS_POSTED;
                            [self updateCheckListItemCoreByChkItemId:curCheckListItem.itemCoreModel];
                        }
                    }  else {
                        result = NO;
                    }
                }
            }
        }
    }
    else
    {
        return YES;
    }
    
    return result;
}
-(NSInteger)numberOfMustPostChecklistItemRecords:(NSInteger)userId {
    NSInteger countPostChklist = 0;
    NSMutableArray *arrayChklist = [self loadAllCheckListByCurrentUserData];
    
    for (CheckListModelV2 *chklistModel in arrayChklist) {
        NSMutableArray *arrayChklistFormScore = [self loadChecklistFormScoreByChkListId:chklistModel.chkListId AndChkUserId:[UserManagerV2 sharedUserManager].currentUser.userId];
        
        for (ChecklistFormScoreModel *chklistFormScore in arrayChklistFormScore) {
            NSMutableArray *arrayPostChklistItemScore = [self loadCheckListItemCoreByChkListFormScoreIdData:chklistFormScore.dfId AndStatusPost:POST_STATUS_SAVED_UNPOSTED];
            
            countPostChklist += arrayPostChklistItemScore.count;
        }
    }
    
    return countPostChklist;
}

#pragma mark - Implement function check checklist status
-(BOOL)checkChecklistStatusWithRoomAssigmentId:(NSInteger)roomAssignmentId {
    BOOL checkChklist =  NO;
    CheckListModelV2 *chklist = [[CheckListModelV2 alloc] init];
    chklist.chkListId = roomAssignmentId;
    chklist.chkListUserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    [self loadCheckListData:chklist];
    
    if (chklist.chkListUserId != 0) {
        if (chklist.chkCore >= 0) {
            checkChklist = YES;
        }
    }
    
    return checkChklist;
}

#pragma mark - Implement function clear data checklist
-(BOOL)clearDataChecklistWithRoomAssignmentId:(NSInteger)roomAssignmentId andInspectionStatus:(NSInteger)inspectionStatus {
    BOOL statusClearData = NO;
    
    if (inspectionStatus == ENUM_INSPECTION_COMPLETED_PASS || inspectionStatus == ENUM_INSPECTION_COMPLETED_FAIL) {
        CheckListModelV2 *chklist = [[CheckListModelV2 alloc] init];
        //clear data checklist
        chklist.chkListId = roomAssignmentId;
        chklist.chkListUserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
        [self loadCheckListData:chklist];
        
        if (chklist.chkListUserId != 0) {
            if (inspectionStatus == ENUM_INSPECTION_COMPLETED_PASS) {
                //if room is pass(VI) update score checklist 90
//                chklist.chkCore = 90;
                chklist.chkListStatus = tChkPass;
            }
            else if (inspectionStatus == ENUM_INSPECTION_COMPLETED_FAIL) {
                //if room is fail(VD) update score checklist 0
//                chklist.chkCore = 0;
                chklist.chkListStatus = tChkFail;
                
            }
            else {
                chklist.chkListStatus = 0;
            }
            
            [self updateCheckList:chklist];
            
            //clear data checklist form score
            NSMutableArray *arrayChklistFormScore = [self loadChecklistFormScoreByChkListId:chklist.chkListId AndChkUserId:[UserManagerV2 sharedUserManager].currentUser.userId];
            
            for (ChecklistFormScoreModel *formScore in arrayChklistFormScore) {
                formScore.dfScore = -1;
                [self updateCheckListFormScore:formScore];
                
                //clear data checklist item score
                NSMutableArray *arrayChklistItemScore = [self loadCheckListItemCoreByChkListFormScoreIdData:formScore.dfId];
                
                for (CheckListItemCoreDBModelV2 *itemScore in arrayChklistItemScore) {
                    itemScore.chkItemCore = -1;
                    itemScore.chkPostStatus = POST_STATUS_UN_CHANGED;
                    [self updateCheckListItemCoreByChkItemId:itemScore];
//                    [self deleteCheckListItemScoreByItemId:itemScore];
                }
            }
            
            statusClearData = YES;
        }
    }
    
    return statusClearData;
}

#pragma mark - Post WSLog
-(BOOL)postWSLog:(int)userId message:(NSString*)messageValue messageType:(int)type
{
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService
                                                  eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole])
    {
        [binding setLogXMLInOut:YES];
    }
    eHousekeepingService_PostWSLog *request = [[eHousekeepingService_PostWSLog
                                                alloc] init];
    request.intUsrID = [NSNumber numberWithInt:userId];
    request.strMessage = messageValue;
    request.intMessageType = [NSNumber numberWithInt:type];
    eHousekeepingServiceSoap12BindingResponse *response = [binding
                                                           PostWSLogUsingParameters:request];
    
    BOOL isPostSuccess = NO;
    
    for (id bodyPart in response.bodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_PostWSLogResponse class]]) {
            eHousekeepingService_PostWSLogResponse *dataBody =
            (eHousekeepingService_PostWSLogResponse *)bodyPart;
            if (dataBody != nil) {
                if ([dataBody.PostWSLogResult.respCode isEqualToNumber:[NSNumber
                                                                        numberWithInteger:RESPONSE_STATUS_OK]])
                {
                    isPostSuccess = YES;
                }
            }
        }
    }
    return isPostSuccess;
}
@end
