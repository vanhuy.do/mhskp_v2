//
//  CleaningTimeManager.h
//  mHouseKeeping
//
//  Created by Dung Phan on 2015/09/22.
//

#import <Foundation/Foundation.h>

@interface CleaningTimeManager : NSObject

+ (id)sharedManager;

@property (nonatomic, strong) RoomRecordDetailModelV2 *roomRecordDetailModel;
@property BOOL isCleaning;

- (void)saveRoomRecordDetail;

@end
