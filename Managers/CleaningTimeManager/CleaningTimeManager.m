//
//  CleaningTimeManager.m
//  mHouseKeeping
//
//  Created by Dung Phan on 2015/09/22.
//

#import "CleaningTimeManager.h"
#import "RoomManagerV2.h"

@implementation CleaningTimeManager

+ (id)sharedManager {
    
    static CleaningTimeManager *sharedCleaningTimeManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedCleaningTimeManager = [[self alloc] init];
    });
    return sharedCleaningTimeManager;
}

- (id)init {
    
    if (self = [super init]) {
        // init
    }
    return self;
}

- (void)saveRoomRecordDetail {
    
    if (![UserManagerV2 isSupervisor] && _isCleaning) {
        NSDateFormatter *timeFormat = [[NSDateFormatter alloc] init];
        [timeFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *now = [NSDate date];
        _roomRecordDetailModel.recd_Stop_Date = [timeFormat stringFromDate:now];
        
        [[RoomManagerV2 sharedRoomManager] insertRoomRecordDetailData:_roomRecordDetailModel];
    }
}

@end
