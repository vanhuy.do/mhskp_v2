//
//  AmenitiesManagerV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 3/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AmenitiesServiceAdapterV2.h"
#import "AmenitiesItemsAdapterV2.h"
#import "AmenitiesCategoriesAdapterV2.h"
#import "AmenitiesItemPriceAdapterV2.h"
#import "AmenitiesOrderDetailsModelV2.h"
#import "CountServiceModelV2.h"
#import "DbDefinesV2.h"
//#import "AmenitiesOrdersAdapterV2.h"
//#import "AmenitiesOrderDetailsAdapterV2.h"
//#import "AmenitiesItemsOrderTempModel.h"

@interface AmenitiesManagerV2 : NSObject

//MARK: Static Methods
+ (AmenitiesManagerV2*) sharedAmenitiesManager;

#pragma mark - Amenities History
//Save data into table Count History
-(void) saveAmenitiesHistoryItems:(NSMutableArray*)listOrders;

#pragma mark - Amenities Service
-(BOOL) insertAmenitiesServiceModel:(AmenitiesServiceModelV2 *)model;
-(BOOL) updateAmenitiesServiceModel:(AmenitiesServiceModelV2 *) model;
-(BOOL) deleteAmenitiesServiceModel:(AmenitiesServiceModelV2 *) model;
-(AmenitiesServiceModelV2 *) loadAmenitiesServiceModelByPrimaryKey:(AmenitiesServiceModelV2 *) model;
-(NSMutableArray *) loadAllAmenitiesService;
-(AmenitiesServiceModelV2 *)loadAmenitiesRoomTypeLatest;

#pragma mark - Amenities Items
-(BOOL) insertAmenitiesItemsModel:(AmenitiesItemsModelV2 *)model;
-(BOOL) updateAmenitiesItemsModel:(AmenitiesItemsModelV2 *) model;
-(BOOL) deleteAmenitiesItemsModel:(AmenitiesItemsModelV2 *) model;
-(AmenitiesItemsModelV2 *) loadAmenitiesItemsModelByPrimaryKey:(AmenitiesItemsModelV2 *) model;
-(NSMutableArray *)loadAllAmenitiesItemsByCategoriesId:(NSInteger)amenitiesCategoriesId AndRoomTypeId:(NSInteger) roomTypeId;
-(NSMutableArray *)loadAllAmenitiesItemsByRoomTypeId:(NSInteger)amenitiesRoomTypeId;
-(AmenitiesItemsModelV2 *)loadAmenitiesItemLatest;
- (NSMutableArray *) loadAmenitiesItemsModelByCateId:(NSInteger) cateId;
- (NSMutableArray *)loadAmenitiesItemsModelByCateId:(NSInteger)cateId andRoomTypeId:(NSInteger)roomTypeId;

#pragma mark - Amenities Categories
-(BOOL) insertAmenitiesCategoriesModel:(AmenitiesCategoriesModelV2 *)model;
-(BOOL) updateAmenitiesCategoriesModel:(AmenitiesCategoriesModelV2 *) model;
-(BOOL) deleteAmenitiesCategoriesModel:(AmenitiesCategoriesModelV2 *) model;
-(AmenitiesCategoriesModelV2 *) loadAmenitiesCategoriesModelByPrimaryKey:(AmenitiesCategoriesModelV2 *) model;
-(NSMutableArray *) loadAllAmenitiesCategoriesByServiceId:(NSInteger) serviceId;
-(NSMutableArray *) loadAllAmenitiesCategories;
-(AmenitiesCategoriesModelV2 *)loadAmenitiesCategoryLatest;
- (NSMutableArray *)loadAllAmenitiesCategoriesByRoomTypeId:(NSInteger)roomTypeId;

#pragma mark - Amenities Item Price
-(BOOL) insertAmenitiesItemPriceModel:(AmenitiesItemPriceModelV2 *)model;
-(BOOL) updateAmenitiesItemPriceModel:(AmenitiesItemPriceModelV2 *) model;
-(BOOL) deleteAmenitiesItemPriceModel:(AmenitiesItemPriceModelV2 *) model;
-(AmenitiesItemPriceModelV2 *) loadAmenitiesItemPriceModelByPrimaryKey:(AmenitiesItemPriceModelV2 *) model;

//Hao Tran - Remove for unsed
/*
#pragma mark - Amenities Orders
-(BOOL) insertAmenitiesOrdersModel:(AmenitiesOrdersModelV2 *)model;
-(BOOL) updateAmenitiesOrdersModel:(AmenitiesOrdersModelV2 *) model;
-(BOOL) deleteAmenitiesOrdersModel:(AmenitiesOrdersModelV2 *) model;
-(AmenitiesOrdersModelV2 *) loadAmenitiesOrdersModelByPrimaryKey:(AmenitiesOrdersModelV2 *) model;
-(AmenitiesOrdersModelV2 *) loadAmenitiesOrdersModelByRoomIdUserIdAndServiceId:(AmenitiesOrdersModelV2 *) model;
-(BOOL)isCheckAmenitiesOrder:(AmenitiesOrdersModelV2 *)model;
-(NSMutableArray *)loadAllAmenitiesOrdersModelByUserId:(NSInteger)userId;
*/

#pragma mark - Amenities Order Details
-(int) insertAmenitiesOrderDetailsModel:(AmenitiesOrderDetailsModelV2 *)model;
-(int) deleteAmenitiesOrderDetailsByUserId:(int)userId transactionDateTime:(NSString*)transactionDateTime;
-(int) deleteAmenitiesOrderDetailsByUserId:(int)userId;
-(int) countTotalAmenitiesByUserId:(int)userId;
-(NSMutableArray*)loadAllAmenitiesByUserId:(int)userId;

/*
#pragma mark - Amenities Order Temp
-(BOOL) insertAmenitiesItemsOrderTempModel:(AmenitiesItemsOrderTempModel *)model;
-(AmenitiesItemsOrderTempModel *) loadAmenitiesItemsOrderTempModelByPrimaryKey:(AmenitiesItemsOrderTempModel *) model;
-(BOOL) updateAmenitiesItemsOrderTempModel:(AmenitiesItemsOrderTempModel *) model;
-(BOOL) deleteAmenitiesItemsOrderTempModel:(AmenitiesItemsOrderTempModel *) model;
-(NSMutableArray *)loadAllAmenitiesItemOrderTempModel;
-(NSMutableArray *)loadAmenitiesItemOrderTempSameCategoryId:(NSInteger)categoryId AndRoomTypeId:(NSInteger)roomTypeId;
-(BOOL) deleteAllAmenitiesItemsOrderTemp;
-(NSMutableArray *)loadAllAmenitiesItemOrderTempByRoomType:(NSInteger)roomType;
*/

#pragma mark - Amenities WS
//-(BOOL) getRoomTypeListWSWithHotelId:(NSInteger) hotelId;
-(BOOL) getAmenitiesCategoryListWSWithUserId:(NSInteger) userId AndHotelId:(NSInteger) hotelId;
-(BOOL) getAmenitiesItemListWSWithUserId:(NSInteger) userId AndHotelId:(NSInteger) hotelId;
//-(BOOL) postAmenitiesItemWSWithUserId:(NSInteger) userId RoomAssignId:(NSInteger) roomAssignId AndAmenitiesItem:(AmenitiesOrderDetailsModelV2 *) model WithDate:(NSString *) date __attribute__((deprecated));
-(int)postAmenitiesByUserId:(int)userId amenitiesItems:(NSMutableArray*)amenitiesItemsList;
//-(NSInteger) numberOfMustPostAmenitiesRecords:(NSInteger) userId;

- (BOOL) postWSLog:(NSInteger)userId Message:(NSString *)messageValue MessageType:(NSInteger)type;

@end
