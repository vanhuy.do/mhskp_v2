//
//  AmenitiesManagerV2.m
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 3/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AmenitiesManagerV2.h"
#import "LogFileManager.h"
#import "LogObject.h"
#import "NetworkCheck.h"
#import "DataBaseExtension.h"
#import "AmenitiesOrderDetailsModelV2.h"
#import "CountHistoryModel.h"
#import "CountHistoryManager.h"

@implementation AmenitiesManagerV2

static AmenitiesManagerV2* sharedAmenitiesManagerInstance = nil;

//MARK: Static Methods
+ (AmenitiesManagerV2*) sharedAmenitiesManager
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedAmenitiesManagerInstance = [[AmenitiesManagerV2 alloc] init];
    });
    
    return sharedAmenitiesManagerInstance;
}

#pragma mark - Amenities History
//Save data into table Count History
//List of AmenitiesOrderDetailsModelV2
-(void) saveAmenitiesHistoryItems:(NSMutableArray*)listOrders
{
    CountHistoryManager *countHistoryManager = [[CountHistoryManager alloc]init];
    CountHistoryModel *countHistoryModel = [[CountHistoryModel alloc]init];
    
    for (AmenitiesOrderDetailsModelV2 *curOrderDetail in listOrders) {
        
        countHistoryModel.room_id = curOrderDetail.amdRoomNumber;
        countHistoryModel.count_item_id = curOrderDetail.amdItemId;
        countHistoryModel.count_collected = curOrderDetail.amdUsedQuantity;
        countHistoryModel.count_new = curOrderDetail.amdNewQuantity;
        countHistoryModel.count_service = cAmenities;
        countHistoryModel.count_date = curOrderDetail.amdTractionDateTime;
        countHistoryModel.count_user_id = curOrderDetail.amdUserId;
        [countHistoryManager insertCountHistoryData:countHistoryModel];
    }
}

#pragma mark - Amenities Service
-(BOOL) insertAmenitiesServiceModel:(AmenitiesServiceModelV2 *)model
{
    AmenitiesServiceAdapterV2 *adapter = [[AmenitiesServiceAdapterV2 alloc] init];
    [adapter openDatabase];
    BOOL result = [adapter insertAmenitiesServiceModel:model];
    [adapter close];
    return result;
}
-(BOOL) updateAmenitiesServiceModel:(AmenitiesServiceModelV2 *) model
{
    AmenitiesServiceAdapterV2 *adapter = [[AmenitiesServiceAdapterV2 alloc] init];
    [adapter openDatabase];
    BOOL result = [adapter updateAmenitiesServiceModel:model];
    [adapter close];
    return result;
}
-(BOOL) deleteAmenitiesServiceModel:(AmenitiesServiceModelV2 *) model
{
    AmenitiesServiceAdapterV2 *adapter = [[AmenitiesServiceAdapterV2 alloc] init];
    [adapter openDatabase];
    BOOL result = [adapter deleteAmenitiesServiceModel:model];
    [adapter close];
    return result;
}
-(AmenitiesServiceModelV2 *) loadAmenitiesServiceModelByPrimaryKey:(AmenitiesServiceModelV2 *) model
{
    AmenitiesServiceAdapterV2 *adapter = [[AmenitiesServiceAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter loadAmenitiesServiceModelByPrimaryKey:model];
    [adapter close];
    return model;
}

-(NSMutableArray *)loadAllAmenitiesService
{
    AmenitiesServiceAdapterV2 *adapter = [[AmenitiesServiceAdapterV2 alloc] init];
    [adapter openDatabase];
    NSMutableArray *array = [adapter loadAllAmenitiesServiceModel];
    [adapter close];
    return array;

}

-(AmenitiesServiceModelV2 *)loadAmenitiesRoomTypeLatest
{
    AmenitiesServiceAdapterV2 *adapter = [[AmenitiesServiceAdapterV2 alloc] init];
    [adapter openDatabase];
    AmenitiesServiceModelV2 *roomTypeLatest = [adapter loadAmenitiesRoomTypeLatest];
    [adapter close];
    return roomTypeLatest;
}

#pragma mark - Amenities Items
-(BOOL) insertAmenitiesItemsModel:(AmenitiesItemsModelV2 *)model
{
    AmenitiesItemsAdapterV2 *adapter = [[AmenitiesItemsAdapterV2 alloc] init];
    [adapter openDatabase];
    BOOL result = [adapter insertAmenitiesItemsModel:model];
    [adapter close];
    return result;
    
}
-(BOOL) updateAmenitiesItemsModel:(AmenitiesItemsModelV2 *) model
{
    AmenitiesItemsAdapterV2 *adapter = [[AmenitiesItemsAdapterV2 alloc] init];
    [adapter openDatabase];
    BOOL result = [adapter updateAmenitiesItemsModel:model];
    [adapter close];
    return result;
    
}
-(BOOL) deleteAmenitiesItemsModel:(AmenitiesItemsModelV2 *) model
{
    AmenitiesItemsAdapterV2 *adapter = [[AmenitiesItemsAdapterV2 alloc] init];
    [adapter openDatabase];
    BOOL result = [adapter deleteAmenitiesItemsModel:model];
    [adapter close];
    return result;
}
-(AmenitiesItemsModelV2 *) loadAmenitiesItemsModelByPrimaryKey:(AmenitiesItemsModelV2 *) model
{
    AmenitiesItemsAdapterV2 *adapter = [[AmenitiesItemsAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter loadAmenitiesItemsModelByPrimaryKey:model];
    [adapter close];
    return model;
}

-(AmenitiesItemsModelV2 *) loadAmenitiesItemsModelByItemId:(NSInteger) itemId
{
    AmenitiesItemsAdapterV2 *adapter = [[AmenitiesItemsAdapterV2 alloc] init];
    [adapter openDatabase];
    AmenitiesItemsModelV2* model = [adapter loadAmenitiesItemsModelByItemId:itemId];
    [adapter close];
    return model;
}

-(NSMutableArray *)loadAllAmenitiesItemsByCategoriesId:(NSInteger)amenitiesCategoriesId AndRoomTypeId:(NSInteger)roomTypeId
{
    AmenitiesItemsAdapterV2 *adapter = [[AmenitiesItemsAdapterV2 alloc] init];
    [adapter openDatabase];
    NSMutableArray *array = [adapter loadAllAmenitiesItemsByCategoriesId:amenitiesCategoriesId AndRoomTypeId:roomTypeId];
    [adapter close];
    return array;

}
-(NSMutableArray *)loadAllAmenitiesItemsByRoomTypeId:(NSInteger)amenitiesRoomTypeId
{
    AmenitiesItemsAdapterV2 *adapter = [[AmenitiesItemsAdapterV2 alloc] init];
    [adapter openDatabase];
    NSMutableArray *array = [adapter loadAllAmenitiesItemsByRoomTypeId:amenitiesRoomTypeId];
    [adapter close];
    return array;
}

-(AmenitiesItemsModelV2 *)loadAmenitiesItemLatest
{
    AmenitiesItemsAdapterV2 *adapter = [[AmenitiesItemsAdapterV2 alloc] init];
    [adapter openDatabase];
    AmenitiesItemsModelV2 *itemLatest = [adapter loadAmenitiesItemLatest];
    [adapter close];
    return itemLatest;
}

- (NSMutableArray *) loadAmenitiesItemsModelByCateId:(NSInteger) cateId{
    AmenitiesItemsAdapterV2 *adapter = [[AmenitiesItemsAdapterV2 alloc] init];
    [adapter openDatabase];
    NSMutableArray *array = [adapter loadAmenitiesItemsModelByCateId:cateId];
    [adapter close];
    return array;
}

- (NSMutableArray *)loadAmenitiesItemsModelByCateId:(NSInteger)cateId andRoomTypeId:(NSInteger)roomTypeId {
    
    AmenitiesItemsAdapterV2 *adapter = [[AmenitiesItemsAdapterV2 alloc] init];
    [adapter openDatabase];
    NSMutableArray *array = [adapter loadAmenitiesItemsModelByCateId:cateId andRoomTypeId:roomTypeId];
    [adapter close];
    return array;
}

-(int)deleteAllAmenitiesItems
{
    DataTable *tableDelete = [[DataTable alloc] initWithNameFormat:@"%@", AMENITIES_ITEMS];
    NSInteger result = [tableDelete excuteQueryNonSelect:[NSString stringWithFormat:@"Delete from %@", AMENITIES_ITEMS]];
    if(result > 0) {
        [NSString stringWithFormat: @"REINDEX %@.%@",DATABASE_NAME, AMENITIES_ITEMS];
    }
    
    return (int)result;
}

#pragma mark - Amenities Categories
-(BOOL) insertAmenitiesCategoriesModel:(AmenitiesCategoriesModelV2 *)model
{
    AmenitiesCategoriesAdapterV2 *adapter = [[AmenitiesCategoriesAdapterV2 alloc] init];
    [adapter openDatabase];
    BOOL result = [adapter insertAmenitiesCategoriesModel:model];
    [adapter close];
    return result;
}
-(BOOL) updateAmenitiesCategoriesModel:(AmenitiesCategoriesModelV2 *) model
{
    AmenitiesCategoriesAdapterV2 *adapter = [[AmenitiesCategoriesAdapterV2 alloc] init];
    [adapter openDatabase];
    BOOL result = [adapter updateAmenitiesCategoriesModel:model];
    [adapter close];
    return result;
    
}
-(BOOL) deleteAmenitiesCategoriesModel:(AmenitiesCategoriesModelV2 *) model
{
    AmenitiesCategoriesAdapterV2 *adapter = [[AmenitiesCategoriesAdapterV2 alloc] init];
    [adapter openDatabase];
    BOOL result = [adapter deleteAmenitiesCategoriesModel:model];
    [adapter close];
    return result;
}
-(AmenitiesCategoriesModelV2 *) loadAmenitiesCategoriesModelByPrimaryKey:(AmenitiesCategoriesModelV2 *) model
{
    AmenitiesCategoriesAdapterV2 *adapter = [[AmenitiesCategoriesAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter loadAmenitiesCategoriesModelByPrimaryKey:model];
    [adapter close];
    return model;
}

-(NSMutableArray *)loadAllAmenitiesCategoriesByServiceId:(NSInteger)serviceId{
    
    AmenitiesCategoriesAdapterV2 *adapter = [[AmenitiesCategoriesAdapterV2 alloc] init];
    [adapter openDatabase];
    NSMutableArray *array = [adapter loadAllAmenitiesCategoriesByServiceId:serviceId];
    [adapter close];
    return array;
}

-(NSMutableArray *)loadAllAmenitiesCategories
{
    AmenitiesCategoriesAdapterV2 *adapter = [[AmenitiesCategoriesAdapterV2 alloc] init];
    [adapter openDatabase];
    NSMutableArray *array = [adapter loadAllAmenitiesCategories];
    [adapter close];
    return array;
}

- (NSMutableArray *)loadAllAmenitiesCategoriesByRoomTypeId:(NSInteger)roomTypeId {
    
    AmenitiesCategoriesAdapterV2 *adapter = [[AmenitiesCategoriesAdapterV2 alloc] init];
    [adapter openDatabase];
    NSMutableArray *array = [adapter loadAllAmenitiesCategoriesByRoomTypeId:roomTypeId];
    [adapter close];
    return array;
}

-(AmenitiesCategoriesModelV2 *)loadAmenitiesCategoryLatest
{
    AmenitiesCategoriesAdapterV2 *adapter = [[AmenitiesCategoriesAdapterV2 alloc] init];
    [adapter openDatabase];
    AmenitiesCategoriesModelV2 *categoryLatest = [adapter loadAmenitiesCategoryLatest];
    [adapter close];
    return categoryLatest;
}

-(int)deleteAllAmenitiesCategories
{
    DataTable *tableDelete = [[DataTable alloc] initWithNameFormat:@"%@", AMENITIES_CATEGORIES];
    NSInteger result = [tableDelete excuteQueryNonSelect:[NSString stringWithFormat:@"Delete from %@", AMENITIES_CATEGORIES]];
    if(result > 0) {
        [NSString stringWithFormat: @"REINDEX %@.%@",DATABASE_NAME, AMENITIES_CATEGORIES];
    }
    
    return (int)result;
}

#pragma mark - Amenities Item Price
-(BOOL) insertAmenitiesItemPriceModel:(AmenitiesItemPriceModelV2 *)model
{
    AmenitiesItemPriceAdapterV2 *adapter = [[AmenitiesItemPriceAdapterV2 alloc] init];
    [adapter openDatabase];
    BOOL result = [adapter insertAmenitiesItemPriceModel:model];
    [adapter close];
    return result;
}
-(BOOL) updateAmenitiesItemPriceModel:(AmenitiesItemPriceModelV2 *) model
{
    AmenitiesItemPriceAdapterV2 *adapter = [[AmenitiesItemPriceAdapterV2 alloc] init];
    [adapter openDatabase];
    BOOL result = [adapter updateAmenitiesItemPriceModel:model];
    [adapter close];
    return result;
}
-(BOOL) deleteAmenitiesItemPriceModel:(AmenitiesItemPriceModelV2 *) model
{
    AmenitiesItemPriceAdapterV2 *adapter = [[AmenitiesItemPriceAdapterV2 alloc] init];
    [adapter openDatabase];
    BOOL result = [adapter deleteAmenitiesItemPriceModel:model];
    [adapter close];
    return result;
}
-(AmenitiesItemPriceModelV2 *) loadAmenitiesItemPriceModelByPrimaryKey:(AmenitiesItemPriceModelV2 *) model
{
    AmenitiesItemPriceAdapterV2 *adapter = [[AmenitiesItemPriceAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter loadAmenitiesItemPriceModelByPrimaryKey:model];
    [adapter close];
    return model;
}

//Hao Tran - Remove for unused
/*
#pragma mark - Amenities Orders
-(BOOL) insertAmenitiesOrdersModel:(AmenitiesOrdersModelV2 *)model
{
    AmenitiesOrdersAdapterV2 *adapter = [[AmenitiesOrdersAdapterV2 alloc] init];
    [adapter openDatabase];
    BOOL result = [adapter insertAmenitiesOrdersModel:model];
    [adapter close];
    return result;
}
-(BOOL) updateAmenitiesOrdersModel:(AmenitiesOrdersModelV2 *) model
{
    AmenitiesOrdersAdapterV2 *adapter = [[AmenitiesOrdersAdapterV2 alloc] init];
    [adapter openDatabase];
    BOOL result = [adapter updateAmenitiesOrdersModel:model];
    [adapter close];
    return result;

}
-(BOOL) deleteAmenitiesOrdersModel:(AmenitiesOrdersModelV2 *) model
{
    AmenitiesOrdersAdapterV2 *adapter = [[AmenitiesOrdersAdapterV2 alloc] init];
    [adapter openDatabase];
    BOOL result = [adapter deleteAmenitiesOrdersModel:model];
    [adapter close];
    return result;

}
-(AmenitiesOrdersModelV2 *) loadAmenitiesOrdersModelByPrimaryKey:(AmenitiesOrdersModelV2 *) model
{
    AmenitiesOrdersAdapterV2 *adapter = [[AmenitiesOrdersAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter loadAmenitiesOrdersModelByPrimaryKey:model];
    [adapter close];
    return model;

}

-(AmenitiesOrdersModelV2 *)loadAmenitiesOrdersModelByRoomIdUserIdAndServiceId:(AmenitiesOrdersModelV2 *)model
{
    AmenitiesOrdersAdapterV2 *adapter = [[AmenitiesOrdersAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter loadAmenitiesOrdersModelByRoomIdUserIdAndServiceId:model];
    [adapter close];
    return model;

}

-(BOOL)isCheckAmenitiesOrder:(AmenitiesOrdersModelV2 *)model
{
    AmenitiesOrdersAdapterV2 *adapter = [[AmenitiesOrdersAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter loadAmenitiesOrdersModelByRoomIdUserIdAndServiceId:model];
    [adapter close];
    if (model.amenId != 0) {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

-(NSMutableArray *)loadAllAmenitiesOrdersModelByUserId:(NSInteger)userId
{
    AmenitiesOrdersAdapterV2 *adapter = [[AmenitiesOrdersAdapterV2 alloc] init];
    [adapter openDatabase];
    NSMutableArray *array = [adapter loadAllAmenitiesOrdersModelByUserId:userId];
    [adapter close];
    return array;
}

*/

#pragma mark - Amenities Order Details
-(int) insertAmenitiesOrderDetailsModel:(AmenitiesOrderDetailsModelV2 *)model
{
    if(model == nil){
        return 0;
    }
    
    if(model.amdRoomNumber.length <= 0){
        model.amdRoomNumber = @"";
    }
    
    if(model.amdTractionDateTime.length <= 0){
        model.amdTractionDateTime = @"";
    }
    
    DataRow *rowInsert = [[DataRow alloc] init];
    rowInsert.tableName = [NSString stringWithFormat:@"%@", AMENITIES_ORDER_DETAILS];
    
    [rowInsert addColumnIntValue:(int)model.amdCategoryId formatName:@"%@", amd_category_id];
    [rowInsert addColumnIntValue:(int)model.amdHotelId formatName:@"%@", amd_hotel_id];
    [rowInsert addColumnIntValue:(int)model.amdItemId formatName:@"%@", amd_item_id];
    [rowInsert addColumnIntValue:(int)model.amdNewQuantity formatName:@"%@", amd_new_quantity];
    [rowInsert addColumnIntValue:(int)model.amdRoomAssignId formatName:@"%@", amd_room_assign_id];
    [rowInsert addColumnStringValue:model.amdRoomNumber formatName:@"%@", amd_room_number];
    [rowInsert addColumnStringValue:model.amdTractionDateTime formatName:@"%@", amd_transaction_date_time];
    [rowInsert addColumnIntValue:(int)model.amdUsedQuantity formatName:@"%@", amd_used_quantity];
    [rowInsert addColumnIntValue:(int)model.amdUserId formatName:@"%@", amd_user_id];
    
    return [rowInsert insertDBRow]; //return id inserted
}

-(int) deleteAmenitiesOrderDetailsByUserId:(int)userId transactionDateTime:(NSString*)transactionDateTime
{
    DataTable *tableDelete = [[DataTable alloc] initWithNameFormat:@"%@", AMENITIES_ORDER_DETAILS];
    NSString *sqlDelete = [NSString stringWithFormat:@"Delete from %@ where %@ = %d and %@ = '%@'", AMENITIES_ORDER_DETAILS, amd_user_id, userId, amd_transaction_date_time, transactionDateTime];
    return (int)[tableDelete excuteQueryNonSelect:sqlDelete];
}

-(int) deleteAmenitiesOrderDetailsByUserId:(int)userId
{
    DataTable *tableDelete = [[DataTable alloc] initWithNameFormat:@"%@", AMENITIES_ORDER_DETAILS];
    NSString *sqlDelete = [NSString stringWithFormat:@"Delete from %@ where %@ = %d", AMENITIES_ORDER_DETAILS, amd_user_id, userId];
    return (int)[tableDelete excuteQueryNonSelect:sqlDelete];
}


-(int) countTotalAmenitiesByUserId:(int)userId
{
    if(userId <= 0){
        return 0;
    }
    
    DataTable *tableCount = [[DataTable alloc] initWithNameFormat:@"%@", AMENITIES_ORDER_DETAILS];
    NSString *sqlCount = [NSString stringWithFormat:@"select count(1) from %@ where %@ = %d", AMENITIES_ORDER_DETAILS, amd_user_id, userId];
    
    DataRow *rowCountRef = [[DataRow alloc] init];
    [rowCountRef addColumnFormatInt:@"CountResult"];
    [tableCount loadTableDataWithQuery:sqlCount referenceRow:rowCountRef];
    
    int rowCount = [tableCount countRows];
    int countResult = 0;
    if(rowCount > 0) {
        DataRow *row = [tableCount rowWithIndex:0];
        countResult = [row intValueWithColumnNameFormat:@"CountResult"];
    }
    
    return countResult;
}

-(NSMutableArray*)loadAllAmenitiesByUserId:(int)userId
{
    if(userId <= 0){
        return nil;
    }
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"select %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ from %@ where %@ = %d",
                                  
                                  //select
                                  amd_category_id,
                                  amd_hotel_id,
                                  amd_id,
                                  amd_item_id,
                                  amd_new_quantity,
                                  amd_room_assign_id,
                                  amd_room_number,
                                  amd_transaction_date_time,
                                  amd_used_quantity,
                                  amd_user_id,
                                  
                                  //From
                                  AMENITIES_ORDER_DETAILS,
                                  
                                  //where
                                  amd_user_id,
                                  userId
                                  ];
    
    DataTable *table = [[DataTable alloc] init];
    
    //Row result reference
    DataRow *rowReference = [[DataRow alloc] init];
    [rowReference addColumnFormatInt:@"%@", amd_category_id];
    [rowReference addColumnFormatInt:@"%@", amd_hotel_id];
    [rowReference addColumnFormatInt:@"%@", amd_id];
    [rowReference addColumnFormatInt:@"%@", amd_item_id];
    [rowReference addColumnFormatInt:@"%@", amd_new_quantity];
    [rowReference addColumnFormatInt:@"%@", amd_room_assign_id];
    [rowReference addColumnFormatString:@"%@", amd_room_number];
    [rowReference addColumnFormatString:@"%@", amd_transaction_date_time];
    [rowReference addColumnFormatInt:@"%@", amd_used_quantity];
    [rowReference addColumnFormatInt:@"%@", amd_user_id];
    
    [table loadTableDataWithQuery:sqlString referenceRow:rowReference];
    
    int rowCount = [table countRows];
    NSMutableArray *results = nil;
    if(rowCount > 0){
        results = [NSMutableArray array];
     
        for (int i = 0; i < rowCount; i ++) {
            
            AmenitiesOrderDetailsModelV2 *model = [[AmenitiesOrderDetailsModelV2 alloc] init];
            DataRow *row = [table rowWithIndex:i];
            
            model.amdCategoryId = [row intValueWithColumnNameFormat:@"%@", amd_category_id];
            model.amdHotelId = [row intValueWithColumnNameFormat:@"%@", amd_hotel_id];
            model.amdId = [row intValueWithColumnNameFormat:@"%@", amd_id];
            model.amdItemId = [row intValueWithColumnNameFormat:@"%@", amd_item_id];
            model.amdNewQuantity = [row intValueWithColumnNameFormat:@"%@", amd_new_quantity];
            model.amdRoomAssignId = [row intValueWithColumnNameFormat:@"%@", amd_room_assign_id];
            model.amdRoomNumber = [row stringValueWithColumnNameFormat:@"%@", amd_room_number];
            model.amdTractionDateTime = [row stringValueWithColumnNameFormat:@"%@", amd_transaction_date_time];
            model.amdUsedQuantity = [row intValueWithColumnNameFormat:@"%@", amd_used_quantity];
            model.amdUserId = [row intValueWithColumnNameFormat:@"%@", amd_user_id];
            
            [results addObject:model];
        }
    }
    
    return results;
}

/*
#pragma mark - Amenities Order Temp
-(BOOL)insertAmenitiesItemsOrderTempModel:(AmenitiesItemsOrderTempModel *)model
{
    AmenitiesItemsOrderTempAdapter *adapter = [[AmenitiesItemsOrderTempAdapter alloc] init];
    [adapter openDatabase];
    BOOL result = [adapter insertAmenitiesItemsOrderTempModel:model];
    [adapter close];
    return result;
}
-(BOOL)updateAmenitiesItemsOrderTempModel:(AmenitiesItemsOrderTempModel *)model
{
    AmenitiesItemsOrderTempAdapter *adapter = [[AmenitiesItemsOrderTempAdapter alloc] init];
    [adapter openDatabase];
    BOOL result = [adapter updateAmenitiesItemsOrderTempModel:model];
    [adapter close];
    return result;

}
-(AmenitiesItemsOrderTempModel *)loadAmenitiesItemsOrderTempModelByPrimaryKey:(AmenitiesItemsOrderTempModel *)model
{
    AmenitiesItemsOrderTempAdapter *adapter = [[AmenitiesItemsOrderTempAdapter alloc] init];
    [adapter openDatabase];
    AmenitiesItemsOrderTempModel *temp = [adapter loadAmenitiesItemsOrderTempModelByPrimaryKey:model];
    [adapter close];
    return temp;

}
-(BOOL)deleteAmenitiesItemsOrderTempModel:(AmenitiesItemsOrderTempModel *)model
{
    AmenitiesItemsOrderTempAdapter *adapter = [[AmenitiesItemsOrderTempAdapter alloc] init];
    [adapter openDatabase];
    BOOL result = [adapter deleteAmenitiesItemsOrderTempModel:model];
    [adapter close];
    return result;
}
-(NSMutableArray *)loadAllAmenitiesItemOrderTempModel
{
    AmenitiesItemsOrderTempAdapter *adapter = [[AmenitiesItemsOrderTempAdapter alloc] init];
    [adapter openDatabase];
    NSMutableArray *array = [adapter loadAllAmenitiesItemOrderTempModel];
    [adapter close];
    return array;
}

-(NSMutableArray *)loadAmenitiesItemOrderTempSameCategoryId:(NSInteger)categoryId AndRoomTypeId:(NSInteger)roomTypeId
{
    AmenitiesItemsOrderTempAdapter *adapter = [[AmenitiesItemsOrderTempAdapter alloc] init];
    [adapter openDatabase];
    NSMutableArray *array = [adapter loadAmenitiesItemOrderTempSameCategoryId:categoryId AndRoomTypeId:roomTypeId];
    [adapter close];
    return array;
}

-(BOOL)deleteAllAmenitiesItemsOrderTemp
{
    AmenitiesItemsOrderTempAdapter *adapter = [[AmenitiesItemsOrderTempAdapter alloc] init];
    [adapter openDatabase];
    BOOL result = [adapter deleteAllAmenitiesItemsOrderTemp];
    [adapter close];
    return result;
}

-(NSMutableArray *)loadAllAmenitiesItemOrderTempByRoomType:(NSInteger)roomType
{
    AmenitiesItemsOrderTempAdapter *adapter = [[AmenitiesItemsOrderTempAdapter alloc] init];
    [adapter openDatabase];
    NSMutableArray *array = [adapter loadAllAmenitiesItemOrderTempByRoomType:roomType];
    [adapter close];
    return array;
}
*/

#pragma mark - Amenities WS
//-(BOOL)getRoomTypeListWSWithHotelId:(NSInteger)hotelId
//{
//    BOOL returnCode = NO;
//    
//    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
//    
//    //Allow write log or not
//    if([LogFileManager isLogConsole])
//    {
//        [binding setLogXMLInOut:YES];
//    }
//    
//    eHousekeepingService_GetRoomTypeList *request = [[eHousekeepingService_GetRoomTypeList alloc] init];
//    
//    //load room type latest
//    AmenitiesServiceModelV2 *roomTypeLatest = [self loadAmenitiesRoomTypeLatest];
//    
//    [request setIntHotelID:[NSNumber numberWithInt:hotelId]];
//    if (roomTypeLatest.amsLastModified) {
//        [request setStrLastModified:roomTypeLatest.amsLastModified];
//    }
//    
////    LogObject *logObj = [[LogObject alloc] init];
////    logObj.dateTime = [NSDate date];
////    logObj.log = [NSString stringWithFormat:@"<><getRoomTypeListWSWithHotelId><intHotelID:%i lastModified:%@>",hotelId,roomTypeLatest.amsLastModified];
////    LogFileManager *logManager = [[LogFileManager alloc] init];
////    [logManager save:logObj];
//    
//    eHousekeepingServiceSoap12BindingResponse *response = [binding GetRoomTypeListUsingParameters:request];
//    NSArray *responseBodyParts = response.bodyParts;
//    
//    for (id bodyPart in responseBodyParts) {
//        if ([bodyPart isKindOfClass:[SOAPFault class]]) {
//            continue;
//        }
//        //Get Room Type List
//        if ([bodyPart isKindOfClass:[eHousekeepingService_GetRoomTypeListResponse class]]) {
//            eHousekeepingService_GetRoomTypeListResponse *body = (eHousekeepingService_GetRoomTypeListResponse *) bodyPart;
//            if ([body.GetRoomTypeListResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]]) {
//                
//                NSMutableArray *arrayRoomTypeWS = body.GetRoomTypeListResult.RoomTypeList.RoomType;
//                for (eHousekeepingService_RoomType *roomTypeWS in arrayRoomTypeWS) {
//                    
//                    //Get RoomType form database
//                    AmenitiesServiceModelV2 *roomType = [[AmenitiesServiceModelV2 alloc] init];
//                    roomType.amsId = [roomTypeWS.rtID integerValue];
//                    [self loadAmenitiesServiceModelByPrimaryKey:roomType];
//                    if (roomType.amsName != nil) {
//                        
//                        if (![roomType.amsLastModified isEqualToString:roomTypeWS.rtLastModifed]) {
//                            //update room type
//                            roomType.amsName = roomTypeWS.rtName;
//                            roomType.amsNameLang = roomTypeWS.rtLang;
//                            roomType.amsImage = UIImagePNGRepresentation([UIImage imageNamed:@"Executive_Floor.png"]);
//                            roomType.amsLastModified = roomTypeWS.rtLastModifed;
//                            [self updateAmenitiesServiceModel:roomType];
//
//                        }
//                                    
//                    }
//                    else
//                    {
//                        //insert room type
//                        AmenitiesServiceModelV2 *roomTypeDB = [[AmenitiesServiceModelV2 alloc] init];
//                        roomTypeDB.amsId = [roomTypeWS.rtID integerValue];
//                        roomTypeDB.amsImage = UIImagePNGRepresentation([UIImage imageNamed:@"Executive_Floor.png"]);
//                        roomTypeDB.amsLastModified = roomTypeWS.rtLastModifed;
//                        roomTypeDB.amsName = roomTypeWS.rtName;
//                        roomTypeDB.amsNameLang = roomTypeWS.rtLang;
//                        [self insertAmenitiesServiceModel:roomTypeDB];
//                    }
//
//                }
//                returnCode = YES;
//            }
//            else
//            {
//                returnCode = NO;
//            }
//        }
//    }
//    return returnCode;
//}

-(BOOL)getAmenitiesCategoryListWSWithUserId:(NSInteger)userId AndHotelId:(NSInteger)hotelId
{
    BOOL returnCode = NO;
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    [binding setLogXMLInOut:YES];
    
//    [binding setDefaultTimeout:30];

    eHousekeepingService_GetAmenitiesCategoryList *request = [[eHousekeepingService_GetAmenitiesCategoryList alloc] init];
    
    //load amenities category latest
//    AmenitiesCategoriesModelV2 *categoryLatest = [self loadAmenitiesCategoryLatest];
    
    [request setIntUsrID:[NSNumber numberWithInteger:userId]];
    [request setIntHotelID:[NSNumber numberWithInteger:hotelId]];
//    if (categoryLatest.amcaLastModified) {
//        [request setStrLastModified:categoryLatest.amcaLastModified];
//    }
    
//    LogObject *logObj = [[LogObject alloc] init];
//    logObj.dateTime = [NSDate date];
//    logObj.log = [NSString stringWithFormat:@"<%i><getAmenitiesCategoryListWSWithUserId><intUsrID:%i intHotelID:%i lastModified:%@>",userId,userId,hotelId,categoryLatest.amcaLastModified];
//    LogFileManager *logManager = [[LogFileManager alloc] init];
//    [logManager save:logObj];
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetAmenitiesCategoryListUsingParameters:request];
    NSArray *responseBodyParts = response.bodyParts;
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[SOAPFault class]]) {
            continue;
        }
        //Get Amenities Category List
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetAmenitiesCategoryListResponse class]]) {
            eHousekeepingService_GetAmenitiesCategoryListResponse *body = (eHousekeepingService_GetAmenitiesCategoryListResponse *) bodyPart;
            if ([body.GetAmenitiesCategoryListResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]]) {
                [self deleteAllAmenitiesCategories];
                NSMutableArray *arrayAmenitiesCategoryWS = body.GetAmenitiesCategoryListResult.AmenitiesCat.AmenitiesCategoryList;
                for (eHousekeepingService_AmenitiesCategoryList *amenitiesCategoryWS in arrayAmenitiesCategoryWS) {
                    
                    //Get amenities category form database
                    AmenitiesCategoriesModelV2 *amenitiesCategory = [[AmenitiesCategoriesModelV2 alloc] init];
                    amenitiesCategory.amcaId = [amenitiesCategoryWS.amcID integerValue];
                    [self loadAmenitiesCategoriesModelByPrimaryKey:amenitiesCategory];
                    
                    if (amenitiesCategory.amcaHotelId != 0) {
                        
                        if (![amenitiesCategory.amcaLastModified isEqualToString:amenitiesCategoryWS.amcLastModified]) 
                        {
                            //update amenities category
                            amenitiesCategory.amcaName = amenitiesCategoryWS.amcName;
                            amenitiesCategory.amcaNameLang = amenitiesCategoryWS.amcLang;
                            amenitiesCategory.amcaImage = amenitiesCategoryWS.amcPicture;
                            amenitiesCategory.amcaLastModified = amenitiesCategoryWS.amcLastModified;
                            amenitiesCategory.amcaHotelId = hotelId;
                            [self updateAmenitiesCategoriesModel:amenitiesCategory];
                        }
                    }
                    else
                    {
                        //insert amenities category
                        AmenitiesCategoriesModelV2 *amenitiesCategoryDB = [[AmenitiesCategoriesModelV2 alloc] init];
                        amenitiesCategoryDB.amcaId = [amenitiesCategoryWS.amcID integerValue];
                        amenitiesCategoryDB.amcaImage = amenitiesCategoryWS.amcPicture;
                        amenitiesCategoryDB.amcaLastModified = amenitiesCategoryWS.amcLastModified;
                        amenitiesCategoryDB.amcaName = amenitiesCategoryWS.amcName;
                        amenitiesCategoryDB.amcaNameLang = amenitiesCategoryWS.amcLang;
                        amenitiesCategoryDB.amcaHotelId = hotelId;
                        [self insertAmenitiesCategoriesModel:amenitiesCategoryDB];
                    }
                    
                }
                returnCode = YES;
            }
            else
            {
                returnCode = NO;
            }
        }
    }
    return returnCode;

}
-(BOOL)getAmenitiesItemListWSWithUserId:(NSInteger)userId AndHotelId:(NSInteger)hotelId
{
    BOOL returnCode = NO;
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
//    [binding setDefaultTimeout:60];
    
    eHousekeepingService_GetAmenitiesItemList *request = [[eHousekeepingService_GetAmenitiesItemList alloc] init];
    
    //load amenities item latest
//    AmenitiesItemsModelV2 *itemLatest = [self loadAmenitiesItemLatest];
    
    [request setIntUsrID:[NSNumber numberWithInteger:userId]];
    [request setIntHotelID:[NSNumber numberWithInteger:hotelId]];
//    if (itemLatest.itemLastModified) {
//        [request setStrLastModified:itemLatest.itemLastModified];
//    }
    
//    LogObject *logObj = [[LogObject alloc] init];
//    logObj.dateTime = [NSDate date];
//    logObj.log = [NSString stringWithFormat:@"<%i><getAmenitiesItemListWSWithUserId><intUsrID:%i intHotelID:%i lastModified:%@>",userId,userId,hotelId,itemLatest.itemLastModified];
//    LogFileManager *logManager = [[LogFileManager alloc] init];
//    [logManager save:logObj];
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetAmenitiesItemListUsingParameters:request];
    NSArray *responseBodyParts = response.bodyParts;
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[SOAPFault class]]) {
            continue;
        }
        //Get Amenities Item List
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetAmenitiesItemListResponse class]]) {
            eHousekeepingService_GetAmenitiesItemListResponse *body = (eHousekeepingService_GetAmenitiesItemListResponse *) bodyPart;
            if ([body.GetAmenitiesItemListResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]]) {
                [self deleteAllAmenitiesItems];
                NSMutableArray *arrayAmenitiesItemWS = body.GetAmenitiesItemListResult.AmenitiesItm.AmenitiesItemList;
                for (eHousekeepingService_AmenitiesItemList *amenitiesItemWS in arrayAmenitiesItemWS) {
                    
                    //Get amenities item form database
                    AmenitiesItemsModelV2 *amenitiesItem = [[AmenitiesItemsModelV2 alloc] init];
                    amenitiesItem.itemId = [amenitiesItemWS.amiID integerValue];
                    amenitiesItem.itemRoomType = [amenitiesItemWS.amiRoomTypeID integerValue];
                    amenitiesItem.itemCategoryId = [amenitiesItemWS.amiCategoryID integerValue];
                    [self loadAmenitiesItemsModelByPrimaryKey:amenitiesItem];
                    
                    if (amenitiesItem.itemName != nil) {
                        
                        if (![amenitiesItem.itemLastModified isEqualToString:amenitiesItemWS.amiLastModified])
                        {
                            //update amenities item
                            amenitiesItem.itemName = amenitiesItemWS.amiName;
                            amenitiesItem.itemLang = amenitiesItemWS.amiLang;
                            amenitiesItem.itemImage = amenitiesItemWS.amiPicture;
                            amenitiesItem.itemLastModified = amenitiesItemWS.amiLastModified;
                            amenitiesItem.itemRoomType = [amenitiesItemWS.amiRoomTypeID integerValue];
                            amenitiesItem.itemCategoryId = [amenitiesItemWS.amiCategoryID integerValue];
                            [self updateAmenitiesItemsModel:amenitiesItem];
                            
                        }
                        
                    }
                    else
                    {
                        //insert amenities item
                        AmenitiesItemsModelV2 *amenitiesItemDB = [[AmenitiesItemsModelV2 alloc] init];
                        amenitiesItemDB.itemId = [amenitiesItemWS.amiID integerValue];
                        amenitiesItemDB.itemName = amenitiesItemWS.amiName;
                        amenitiesItemDB.itemLang = amenitiesItemWS.amiLang;
                        amenitiesItemDB.itemImage = amenitiesItemWS.amiPicture;
                        amenitiesItemDB.itemLastModified = amenitiesItemWS.amiLastModified;
                        amenitiesItemDB.itemRoomType = [amenitiesItemWS.amiRoomTypeID integerValue];
                        amenitiesItemDB.itemCategoryId = [amenitiesItemWS.amiCategoryID integerValue];
                        [self insertAmenitiesItemsModel:amenitiesItemDB];
                    }
                }
                returnCode = YES;
            }
            else
            {
                returnCode = NO;
            }
        }
    }
    return returnCode;

}

/*
-(BOOL)postAmenitiesItemWSWithUserId:(NSInteger)userId RoomAssignId:(NSInteger)roomAssignId AndAmenitiesItem:(AmenitiesOrderDetailsModelV2 *)model WithDate:(NSString *)date
{
    BOOL result = NO;
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole])
    {
        [binding setLogXMLInOut:YES];
    }
    eHousekeepingService_PostAmenitiesItem *request = [[eHousekeepingService_PostAmenitiesItem alloc] init];
    
    if (model.amdPostStatus == POST_STATUS_SAVED_UNPOSTED) {
        
        [request setIntUsrID:[NSNumber numberWithInteger:userId]];
        [request setIntRoomAssignID:[NSNumber numberWithInteger:roomAssignId]];
        [request setIntAmenitiesItemID:[NSNumber numberWithInteger:model.amdItemId]];
        [request setIntNewQuantity:[NSNumber numberWithInteger:model.amdNewQuantity]];
        [request setStrTransactionTime:date];
        
        LogObject *logObj = [[LogObject alloc] init];
        logObj.dateTime = [NSDate date];
        logObj.log = [NSString stringWithFormat:@"<%i><postAmenitiesItemWSWithUserId><intUsrID:%i intRoomAssignID:%i intAmenitiesItemID:%i intNewQuantity:%i TransactionTime:%@>",userId,userId,roomAssignId,model.amdItemId,model.amdNewQuantity,[date description]];
        LogFileManager *logManager = [[LogFileManager alloc] init];
        [logManager save:logObj];
        
        eHousekeepingServiceSoap12BindingResponse *response = [binding PostAmenitiesItemUsingParameters:request];
        
        NSArray *responseBodyParts = response.bodyParts;
        
        for (id bodyPart in responseBodyParts) {
            if ([bodyPart isKindOfClass:[eHousekeepingService_PostAmenitiesItemResponse class]]) {
                
                eHousekeepingService_PostAmenitiesItemResponse *dataBody = (eHousekeepingService_PostAmenitiesItemResponse *)bodyPart;
                
                if (dataBody != nil) {
                    
                    if ([dataBody.PostAmenitiesItemResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_NEW_RECORD_ADDED]]) {
                        //change post status in amenities
                        result = YES;
                        if (model.amdPostStatus == POST_STATUS_SAVED_UNPOSTED) {
                            model.amdPostStatus = POST_STATUS_POSTED;
                            [self updateAmenitiesOrderDetailsModel:model];
                        }
                        
                    } 
                    else {
                        result = NO;
                    }
                }
            }
        }
        
    }
    else {
        
        return YES;
    }
    
    return result;

}
*/

//Post Amenities Items List
-(int)postAmenitiesByUserId:(int)userId amenitiesItems:(NSMutableArray*)amenitiesItemsList
{
    //No Data to post
    if(amenitiesItemsList.count <= 0){
        return RESPONSE_STATUS_ERROR;
    }
    
    //No network
    if(![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        return RESPONSE_STATUS_NO_NETWORK;
    }
    
    int result = RESPONSE_STATUS_ERROR;
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    //Allow write log or not
    if([LogFileManager isLogConsole]) {
        [binding setLogXMLInOut:YES];
    }
    eHousekeepingService_PostAmenitiesItemsList *request = [[eHousekeepingService_PostAmenitiesItemsList alloc] init];
    [request setIntUserId:[NSNumber numberWithInteger:userId]];
    
    eHousekeepingService_ArrayOfAmenitiesItem *arrayAmenities = [[eHousekeepingService_ArrayOfAmenitiesItem alloc] init];
    NSMutableArray *listHistoryPosting = [NSMutableArray array];
    // DungPhan - 20150908: Enable post Posting History for Amenities by Access Rights
    BOOL isEnablePostingHistory = ([[UserManagerV2 sharedUserManager].currentUserAccessRight isFindActionHistoryActive] || [[UserManagerV2 sharedUserManager].currentUserAccessRight isFindActionHistoryAllowedView] || [[UserManagerV2 sharedUserManager].currentUserAccessRight isRoomDetailsActionHistoryActive] || [[UserManagerV2 sharedUserManager].currentUserAccessRight isRoomDetailsActionHistoryAllowedView]);
    BOOL isEnglishLanguage = [[LanguageManagerV2 sharedLanguageManager] currentLanguageIsEqualTo:@"%@", ENGLISH_LANGUAGE];
    
    NSString *transactionTime = @"";
    for (AmenitiesOrderDetailsModelV2 *curAmenDetail in amenitiesItemsList) {
        eHousekeepingService_AmenitiesItem *amenItem = [[eHousekeepingService_AmenitiesItem alloc] init];
            
        [amenItem setIntDutyAssignmentId:[NSNumber numberWithInteger:curAmenDetail.amdRoomAssignId]];
        [amenItem setIntAmenitiesItemId:[NSNumber numberWithInteger:curAmenDetail.amdItemId]];
        [amenItem setIntNewQuantity:[NSNumber numberWithInteger:curAmenDetail.amdNewQuantity]];
        [amenItem setStrTransactionTime:curAmenDetail.amdTractionDateTime];
        transactionTime = curAmenDetail.amdTractionDateTime;
        
        eHousekeepingService_UniqueRoom *uniqueRoom = [[eHousekeepingService_UniqueRoom alloc] init];
        [uniqueRoom setStrRoomNumber:curAmenDetail.amdRoomNumber];
        [uniqueRoom setIntHotelId:[NSNumber numberWithInteger:curAmenDetail.amdHotelId]];
        [amenItem setUrRoom:uniqueRoom];
        [arrayAmenities.AmenitiesItem addObject:amenItem];
        
        //if (isEnablePostingHistory) {
            //History Posting
            eHousekeepingService_PostingHistoryDetails *historyDetail = [[eHousekeepingService_PostingHistoryDetails alloc] init];
            [historyDetail setUserID:[NSString stringWithFormat:@"%d", userId]];
            [historyDetail setRoomNo:curAmenDetail.amdRoomNumber];
            [historyDetail setHotelID:[NSString stringWithFormat:@"%d", [UserManagerV2 sharedUserManager].currentUser.userHotelsId]];
            [historyDetail setModuleID:[NSString stringWithFormat:@"%d", (int)ModuleHistory_Amenities]];
            [historyDetail setNewQuantity:[NSString stringWithFormat:@"%d", (int)curAmenDetail.amdNewQuantity]];
            [historyDetail setUsedQuantity:[NSString stringWithFormat:@"%d", (int)curAmenDetail.amdNewQuantity]];
            [historyDetail setTransactionDateTime:curAmenDetail.amdTractionDateTime];
            
            AmenitiesItemsModelV2 *countItem = [[AmenitiesItemsModelV2 alloc] init];
            countItem.itemId = curAmenDetail.amdItemId;
            countItem = [self loadAmenitiesItemsModelByItemId:curAmenDetail.amdItemId];
            
            AmenitiesCategoriesModelV2 *countCategory = [[AmenitiesCategoriesModelV2 alloc] init];
            countCategory.amcaId = countItem.itemCategoryId;
            countCategory = [self loadAmenitiesCategoriesModelByPrimaryKey:countCategory];
            
            if (isEnglishLanguage) {
                [historyDetail setItemDescription:countItem.itemName];
                [historyDetail setCategoryDescription:countCategory.amcaName];
            } else {
                [historyDetail setItemDescription:countItem.itemLang];
                [historyDetail setCategoryDescription:countCategory.amcaNameLang];
            }
            
            [listHistoryPosting addObject:historyDetail];
        //}
    }
    
    [request setAmenitiesItemsList:arrayAmenities];
    eHousekeepingServiceSoap12BindingResponse *response = [binding PostAmenitiesItemsListUsingParameters:request];
    NSArray *responseBodyParts = response.bodyParts;
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_PostAmenitiesItemsListResponse class]]) {
            
            eHousekeepingService_PostAmenitiesItemsListResponse *dataBody = (eHousekeepingService_PostAmenitiesItemsListResponse *)bodyPart;
            if (dataBody != nil) {
                if ([dataBody.PostAmenitiesItemsListResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_NEW_RECORD_ADDED]]
                    || [dataBody.PostAmenitiesItemsListResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_OK]]) {
                    
                    result = RESPONSE_STATUS_OK;
                    
                    //CRF-1229: Posting history
                    //if (isEnablePostingHistory) {
                        CountManagerV2 *countManager = [[CountManagerV2 alloc] init];
                        [countManager postHistoryPostingWS:listHistoryPosting];
                    //}
                    
                    [self saveAmenitiesHistoryItems:amenitiesItemsList];
                    [self deleteAmenitiesOrderDetailsByUserId:userId transactionDateTime:transactionTime];
                }
            }
        }
    }
    
    return result;
}

/*
-(NSInteger)numberOfMustPostAmenitiesRecords:(NSInteger)userId{
    NSMutableArray *arrayAmenities = [self loadAllAmenitiesOrdersModelByUserId:userId];
    NSInteger countPostAmenities = 0;
    for (AmenitiesOrdersModelV2 *orderModel in arrayAmenities) {
        NSMutableArray *arrayPostAmenities = [self loadAllAmenitiesOrderDetailsByOrderId:orderModel.amenId];
        countPostAmenities += arrayPostAmenities.count;
    }
    return countPostAmenities;
}*/

#pragma mark - Post WSLog
- (BOOL) postWSLog:(NSInteger)userId Message:(NSString *)messageValue MessageType:(NSInteger)type{
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole])
    {
        [binding setLogXMLInOut:YES];
    }
    eHousekeepingService_PostWSLog *request = [[eHousekeepingService_PostWSLog alloc] init];
    request.intUsrID = [NSNumber numberWithInteger:userId];
    request.strMessage = messageValue;
    request.intMessageType = [NSNumber numberWithInteger:type];
    eHousekeepingServiceSoap12BindingResponse *respone = [binding PostWSLogUsingParameters:request];
    BOOL isPostSuccess = NO;
    for (id bodyPart in respone.bodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_PostWSLogResponse class]]) {
            eHousekeepingService_PostWSLogResponse *dataBody = (eHousekeepingService_PostWSLogResponse *)bodyPart;
            if (dataBody != nil) {
                if ([dataBody.PostWSLogResult.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]]) {
                    isPostSuccess = YES;
                }
            }
        }
    }
    return isPostSuccess;
}

@end
