//
//  CountManagerV2.h
//  mHouseKeeping
//
//  Created by TMS on 3/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "CountServiceAdapterV2.h"
#import "CountMoreInforAdapterV2.h"
#import "CountCategoryAdapterV2.h"
#import "CountItemAdapterV2.h"
#import "CountOrderDetailAdapterV2.h"
#import "CountOrderAdapterV2.h"
#import "RoomTypeInventoryAdapter.h"

@interface CountManagerV2 : NSObject

#pragma mark - Count Service
-(BOOL) insertCountService:(CountServiceModelV2 *)model;
-(BOOL) updateCountSerice:(CountServiceModelV2 *) model;
-(BOOL) deleteCountService:(CountServiceModelV2 *) model;
-(CountServiceModelV2 *) loadCountSerice:(CountServiceModelV2 *) model;
-(NSMutableArray *) loadAllCountSerice;

#pragma mark - Count More Infor
-(BOOL)insertCountMoreInfor:(CountMoreInforModelV2 *) model ;
-(BOOL) updateCountMoreInfor:(CountMoreInforModelV2 *) model;
-(BOOL) deleteCountMoreInfor:(CountMoreInforModelV2 *) model;
-(CountMoreInforModelV2 *) loadCountMoreInfor:(CountMoreInforModelV2 *) model;
-(NSMutableArray *) loadAllCountMoreInforByCountCategoryId:(NSInteger) countCategoryId;

#pragma mark - Count Category
-(BOOL)insertCountCategory:(CountCategoryModelV2 *) model ;
-(BOOL) updateCountCategory:(CountCategoryModelV2 *) model;
-(BOOL) deleteCountCategory:(CountCategoryModelV2 *) model;
-(CountCategoryModelV2 *) loadCountCategory:(CountCategoryModelV2 *) model;
-(NSMutableArray *) loadAllCountCategoryByCountServiceId:(NSInteger) countServiceId;
-(CountCategoryModelV2 *) loadCountCategoryByCountServiceIdAndCategoryIdData:(CountCategoryModelV2 *) model;
-(CountCategoryModelV2 *) loadCountCategoryLatestWithHotelId:(NSInteger) hotelId AndCountServiceId:(NSInteger) serviceId;
- (NSMutableArray*)loadAllCountCategoryByCountServiceId:(NSInteger)countServiceId andRoomTypeId:(NSInteger)roomTypeId;

#pragma mark - Count Item
-(BOOL)insertCountItem:(CountItemsModelV2 *) model;
-(BOOL) updateCountItem:(CountItemsModelV2 *) model;
-(BOOL) deleteCountItem:(CountItemsModelV2 *) model;
-(CountItemsModelV2 *) loadCountItem:(CountItemsModelV2 *) model;
-(NSMutableArray *) loadAllCountItemByCountCategoryId:(NSInteger) countCategoryId;
-(CountItemsModelV2 *) loadCountItemsByCountCategoryIdAndCountItemId:(CountItemsModelV2 *) model;
-(CountItemsModelV2 *)loadCountItemsLatestWithHotelId:(NSInteger)hotelId AndServiceId:(NSInteger)serviceId;
- (NSMutableArray*)loadAllCountItemByCountCategoryId:(NSInteger)countCategoryId andRoomTypeId:(NSInteger)roomTypeId;

#pragma mark - Count Order
-(BOOL)insertCountOrder:(CountOrdersModelV2 *) model ;
-(BOOL) updateCountOrder:(CountOrdersModelV2 *) model;
-(BOOL) deleteCountOrderByOrderId:(int)orderId;
-(BOOL) deleteCountOrdersByPostStatus:(int)postStatus serviceId:(int)serviceId;
-(CountOrdersModelV2 *) loadCountOrder:(CountOrdersModelV2 *) model;
-(NSMutableArray *) loadAllCountOrdersByRoomId:(NSInteger) countRoomId;
//-(CountOrdersModelV2 *) loadCountOrdersModelByRoomIdUserIdAndServiceId:(CountOrdersModelV2 *) model;
-(CountOrdersModelV2*) loadCountOrdersModelByRoomAssignId:(int)roomAssignId roomNumber:(NSString*)roomNumber userId:(int)userId serviceId:(int)serviceId postStatus:(int)postStatus;
-(NSMutableArray*) loadAllCountOrdersModelByUserId:(int)userId serviceId:(int)serviceId postStatus:(int)postStatus;
-(NSMutableArray*) loadAllCountOrdersModelByUserId:(int)userId serviceId:(int)serviceId;
-(int) countUnpostOrderItemsByUserId:(int)userId serviceId:(int)serviceId;

//-(BOOL) isCheckCountOrder:(CountOrdersModelV2 *) model;
-(NSMutableArray *)loadAllCountOrdersByUserId:(NSInteger)userId AndServiceId:(NSInteger)serviceId;

#pragma mark - Count Order Detail
-(BOOL)insertCountOrderDetail:(CountOrderDetailsModelV2 *) model ;
-(BOOL) updateCountOrderDetail:(CountOrderDetailsModelV2 *) model;
-(BOOL) deleteCountOrderDetail:(CountOrderDetailsModelV2 *) model;
-(int) deleteCountOrderDetailsByOrderId:(int)orderId;
-(BOOL) deleteCountOrderDetailById:(NSInteger) modelId;
-(CountOrderDetailsModelV2 *) loadCountOrderDetail:(CountOrderDetailsModelV2 *) model;
-(NSMutableArray *) loadAllCountOrderDetailByOrderId:(NSInteger) countOrderId;
-(CountOrderDetailsModelV2 *) loadCountOrderDetailsByItemId:(NSInteger) countItemId;
-(CountOrderDetailsModelV2 *) loadCountOrderDetailsByItemId:(NSInteger) countItemId AndOrderId: (NSInteger) orderId;

#pragma mark - WS Linen
-(BOOL) getLinenCategoryListWSWithUserId:(NSInteger) userId AndHotelId:(NSInteger) hotelId;
-(BOOL) getLinenItemListWSWithUserId:(NSInteger) userId AndHotelId:(NSInteger) hotelId;
-(BOOL) postLinenItemWSWithUserId:(NSInteger) userId roomAssignId:(NSInteger) roomAssignId AndLinenOrderModel:(CountOrderDetailsModelV2 *) model;
-(NSInteger) numberOfMustPostLinenRecords:(NSInteger) userId;
-(NSArray*) postAllLinenItemsWithUserId:(NSInteger)userId roomAssignId:(NSInteger) roomAssignId roomNumber:(NSString*)roomNumber AndLinenOrderModelList:(NSArray *) modelList;

#pragma mark - WS Minibar
-(BOOL) getMinibarCategoryListWSWithUserId:(NSInteger) userId AndHotelId:(NSInteger) hotelId;
-(BOOL) getMinibarItemListWSWithUserId:(NSInteger) userId AndHotelId:(NSInteger) hotelId;
-(BOOL) postMinibarItemWSWithUserId:(NSInteger) userId roomAssignId:(NSInteger) roomAssignId AndMinibarOrderModel:(CountOrderDetailsModelV2 *) model WithOrderId:(NSInteger) orderId;
-(NSInteger) postMinibarOrderWSWithUserId:(NSInteger) userId roomAssignId:(NSInteger) roomAssignId totalQuantity:(NSInteger) totalQuantity serviceCharge:(NSInteger) serviceCharge AndSubTotal:(NSInteger) subTotal WithDate:(NSString*) date;
-(NSInteger)postMinibarOrderWSWithUserId:(NSInteger)userId roomNo:(NSString*)roomNo roomAssignId:(NSInteger)roomAssignId totalQuantity:(NSInteger)totalQuantity serviceCharge:(NSInteger)serviceCharge AndSubTotal:(NSInteger)subTotal WithDate:(NSString *)date;
//-------------update code-------------------------
-(NSInteger) postMinibarOrderWSWithUserId:(NSInteger) userId roomAssignId:(NSInteger) roomAssignId totalQuantity:(NSInteger) totalQuantity serviceCharge:(NSInteger) serviceCharge AndSubTotal:(NSInteger) subTotal WithDate:(NSString*) date WithRoomNo: (NSString*) roomNo;
//-------------update code-------------------------
-(NSInteger) numberOfMustPostMinibarRecords:(NSInteger) userId;
-(NSArray*) postAllMinibarItemsWithUserId:(NSInteger) userId roomAssignId:(NSInteger) roomAssignId AndMinibarOrderModelList:(NSArray *) modelList WithOrderId:(NSInteger) orderId roomNumber:(NSString*)roomNumber;
-(NSArray *)postAllMinibarItemsWithUserId:(NSInteger)userId roomAssignId:(NSInteger)roomAssignId AndMinibarOrderModelList:(NSArray *)modelList WithOrderId:(NSInteger) orderId roomNumber:(NSString*)roomNumber isEnablePostingHistory:(BOOL)isEnablePostingHistory;
-(NSArray *)postHistoryItemsWithUserId:(NSInteger)userId AndMinibarOrderModelList:(NSArray *)modelList roomNumber:(NSString*)roomNumber;
//-------------update code--------------------------
-(NSMutableArray *)loadCountOrdersByUserId:(NSInteger)userId AndServiceId:(NSInteger)serviceId;

#pragma mark - common count order
-(int)postAllPendingCountDataByUserId:(int)userId serviceId:(int)serviceId;

#pragma mark - WS Log
- (BOOL) postWSLog:(NSInteger)userId Message:(NSString *)messageValue MessageType:(NSInteger)type;

#pragma mark - History Posting
-(NSInteger) postHistoryPostingWS:(NSMutableArray*)listHistoryPosting;
-(NSMutableArray*) getPostingHistoryWSByUserId:(int) userId roomNumber:(NSString*)roomNumber hotelId:(int)hotelId moduleId:(int)moduleId dateFilter:(NSString*)dateFilter;

#pragma mark - Count WS for Inventory by Room Type
- (BOOL)getRoomTypeInventoryWSByServiceType:(NSInteger)serviceType userId:(NSInteger)userId hotelId:(NSInteger)hotelId;
@end
