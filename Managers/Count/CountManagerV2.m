//
//  CountManagerV2.m
//  mHouseKeeping
//
//  Created by TMS on 3/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CountManagerV2.h"
#import "LogFileManager.h"
#import "LogObject.h"
#import "eHousekeepingService.h"
#import "UserManagerV2.h"
#import "NetworkCheck.h"
#import "CountHistoryModel.h"
#import "CountHistoryManager.h"
#import "SyncManagerV2.h"
#import "Web_x0020_Service.h"
#import "DeviceManager.h"
#import "MBProgressHUD.h"
#import "CustomAlertViewV2.h"

@implementation CountManagerV2

#pragma mark - Count Service
-(BOOL) insertCountService:(CountServiceModelV2 *)model {
    CountServiceAdapterV2 *adapter = [[CountServiceAdapterV2 alloc] init];
    [adapter openDatabase];
    BOOL result = [adapter insertCountServiceModel:model];
    [adapter close];
    return result;
}

-(BOOL) updateCountSerice:(CountServiceModelV2 *) model {
    CountServiceAdapterV2 *adapter = [[CountServiceAdapterV2 alloc] init];
    [adapter openDatabase];
    BOOL result = [adapter updateCountSericeModel:model];
    [adapter close];
    return result;
}

-(BOOL) deleteCountService:(CountServiceModelV2 *) model {
    CountServiceAdapterV2 *adapter = [[CountServiceAdapterV2 alloc] init];
    [adapter openDatabase];
    BOOL result = [adapter deleteCountServiceModel:model];
    [adapter close];
    return result;
}

-(CountServiceModelV2 *) loadCountSerice:(CountServiceModelV2 *) model {
    CountServiceAdapterV2 *adapter = [[CountServiceAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter loadCountSericeModel:model];
    [adapter close];
    return model;
}

-(NSMutableArray *) loadAllCountSerice
{
    CountServiceAdapterV2 *adapter = [[CountServiceAdapterV2 alloc] init];
    [adapter openDatabase];
    NSMutableArray *array = [adapter loadAllCountSericeModel];
    [adapter close];
    return array;

}

#pragma mark - Count More Infor
-(BOOL)insertCountMoreInfor:(CountMoreInforModelV2 *) model {
    CountMoreInforAdapterV2 *adapter = [[CountMoreInforAdapterV2 alloc] init];
    [adapter openDatabase];
    BOOL result = [adapter insertCountMoreInforModel:model];
    [adapter close];
    return result;
}

-(BOOL) updateCountMoreInfor:(CountMoreInforModelV2 *) model {
    CountMoreInforAdapterV2 *adapter = [[CountMoreInforAdapterV2 alloc] init];
    [adapter openDatabase];
    BOOL result = [adapter updateCountMoreInforModel:model];
    [adapter close];
    return result;
}

-(BOOL) deleteCountMoreInfor:(CountMoreInforModelV2 *) model {
    CountMoreInforAdapterV2 *adapter = [[CountMoreInforAdapterV2 alloc] init];
    [adapter openDatabase];
    BOOL result = [adapter deleteCountMoreInforModel:model];
    [adapter close];
    return result;
}

-(CountMoreInforModelV2 *) loadCountMoreInfor:(CountMoreInforModelV2 *) model {
    CountMoreInforAdapterV2 *adapter = [[CountMoreInforAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter loadCountMoreInforModel:model];
    [adapter close];
    return model;
}

-(NSMutableArray *) loadAllCountMoreInforByCountCategoryId:(NSInteger) countCategoryId {
    CountMoreInforAdapterV2 *adapter = [[CountMoreInforAdapterV2 alloc] init];
    [adapter openDatabase];
    NSMutableArray *array = [adapter loadAllCountMoreInforByCountCategoryId:countCategoryId];
    [adapter close];
    return array;
}

#pragma mark - Count Category
-(BOOL)insertCountCategory:(CountCategoryModelV2 *) model {
    CountCategoryAdapterV2 *adapter = [[CountCategoryAdapterV2 alloc] init];
    [adapter openDatabase];
    BOOL result = [adapter insertCountCategoryModel:model];
    [adapter close];
    return result;
}

-(BOOL) updateCountCategory:(CountCategoryModelV2 *) model {
    CountCategoryAdapterV2 *adapter = [[CountCategoryAdapterV2 alloc] init];
    [adapter openDatabase];
    BOOL result = [adapter updateCountCategoryModel:model];
    [adapter close];
    return result;
}

-(BOOL) deleteCountCategory:(CountCategoryModelV2 *) model {
    CountCategoryAdapterV2 *adapter = [[CountCategoryAdapterV2 alloc] init];
    [adapter openDatabase];
    BOOL result = [adapter deleteCountCategoryModel:model];
    [adapter close];
    return result;
}

-(int) deleteAllCountCategory
{
    DataTable *tableDelete = [[DataTable alloc] initWithNameFormat:@"%@", COUNT_CATEGORIES];
    int result = (int)[tableDelete excuteQueryNonSelect:[NSString stringWithFormat:@"Delete from %@", COUNT_CATEGORIES]];
    if(result > 0) {
        [NSString stringWithFormat: @"REINDEX %@.%@",DATABASE_NAME, COUNT_CATEGORIES];
    }
    
    return result;
}

- (NSInteger)deleteAllCountCategoryByServiceId:(NSInteger)serviceId {
    DataTable *tableDelete = [[DataTable alloc] initWithNameFormat:@"%@",COUNT_CATEGORIES];
    
    NSString *queryDelete = [NSString stringWithFormat:@"DELETE FROM %@ WHERE %@ = %d",
                             COUNT_CATEGORIES,
                             c_service_id,
                             (int)serviceId
                             ];
    int status = (int)[tableDelete excuteQueryNonSelect:queryDelete];
    
    return status;
}

-(CountCategoryModelV2 *) loadCountCategory:(CountCategoryModelV2 *) model {
    CountCategoryAdapterV2 *adapter = [[CountCategoryAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter loadCountCategoryModel:model];
    [adapter close];
    return model;
}

-(NSMutableArray *) loadAllCountCategoryByCountServiceId:(NSInteger) countServiceId {
    CountCategoryAdapterV2 *adapter = [[CountCategoryAdapterV2 alloc] init];
    [adapter openDatabase];
    NSMutableArray *array = [adapter loadAllCountCategoryModelsByCountServiceId:countServiceId];
    [adapter close];
    return array;
}

-(CountCategoryModelV2 *)loadCountCategoryByCountServiceIdAndCategoryIdData:(CountCategoryModelV2 *)model
{
    CountCategoryAdapterV2 *adapter = [[CountCategoryAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter loadCountCategoryByCountServiceIdAndCategoryIdData:model];
    [adapter close];
    return model;
}

-(CountCategoryModelV2 *)loadCountCategoryLatestWithHotelId:(NSInteger)hotelId AndCountServiceId:(NSInteger)serviceId
{
    CountCategoryAdapterV2 *adapter = [[CountCategoryAdapterV2 alloc] init];
    [adapter openDatabase];
    CountCategoryModelV2 *countCategory = [adapter loadCountCategoryLatestWithHotelIdData:hotelId AndCountServiceId:serviceId];
    [adapter close];
    return countCategory;

}

- (NSMutableArray*)loadAllCountCategoryByCountServiceId:(NSInteger)countServiceId andRoomTypeId:(NSInteger)roomTypeId {
    CountCategoryAdapterV2 *adapter = [[CountCategoryAdapterV2 alloc] init];
    [adapter openDatabase];
    NSMutableArray *array = [adapter loadAllCountCategoryModelsByCountServiceId:countServiceId andRoomTypeId:roomTypeId];
    [adapter close];
    return array;
}

#pragma mark - Count Item
-(BOOL)insertCountItem:(CountItemsModelV2 *) model {
    CountItemAdapterV2 *adapter = [[CountItemAdapterV2 alloc] init];
    [adapter openDatabase];
    BOOL result = [adapter insertCountItemsModel:model];
    [adapter close];
    return result;
}

-(BOOL) updateCountItem:(CountItemsModelV2 *) model {
    CountItemAdapterV2 *adapter = [[CountItemAdapterV2 alloc] init];
    [adapter openDatabase];
    BOOL result = [adapter updateCountItemsModel:model];
    [adapter close];
    return result;
}

-(BOOL) deleteCountItem:(CountItemsModelV2 *) model {
    CountItemAdapterV2 *adapter = [[CountItemAdapterV2 alloc] init];
    [adapter openDatabase];
    BOOL result = [adapter deleteCountItemsModel:model];
    [adapter close];
    return result;
}

-(CountItemsModelV2 *) loadCountItem:(CountItemsModelV2 *) model {
    CountItemAdapterV2 *adapter = [[CountItemAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter loadCountItemsModel:model];
    [adapter close];
    return model;
}

-(NSMutableArray *) loadAllCountItemByCountCategoryId:(NSInteger) countCategoryId {
    CountItemAdapterV2 *adapter = [[CountItemAdapterV2 alloc] init];
    [adapter openDatabase];
    NSMutableArray *array = [adapter loadAllCountItemModelByCountCategoryId:countCategoryId];
    [adapter close];
    return array;
}

-(CountItemsModelV2 *)loadCountItemsByCountCategoryIdAndCountItemId:(CountItemsModelV2 *)model
{
    CountItemAdapterV2 *adapter = [[CountItemAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter loadCountItemsByCountCategoryIdAndCountItemIdData:model];
    [adapter close];
    return model;

}

-(CountItemsModelV2 *)loadCountItemsLatestWithHotelId:(NSInteger)hotelId AndServiceId:(NSInteger)serviceId
{
    CountItemAdapterV2 *adapter = [[CountItemAdapterV2 alloc] init];
    [adapter openDatabase];
    CountItemsModelV2 *countItems = [adapter loadCountItemsLatestWithHotelIdData:hotelId AndServiceId:serviceId];
    [adapter close];
    return countItems;
}

-(int)deleteAllCountItems
{
    DataTable *tableDelete = [[DataTable alloc] initWithNameFormat:@"%@", COUNT_ITEMS];
    int result = (int)[tableDelete excuteQueryNonSelect:[NSString stringWithFormat:@"Delete from %@", COUNT_ITEMS]];
    if(result > 0) {
        [NSString stringWithFormat: @"REINDEX %@.%@",DATABASE_NAME, COUNT_ITEMS];
    }
    
    return result;
}

-(int)deleteAllItemsWithCountType:(int)countType
{
    DataTable *tableDelete = [[DataTable alloc] initWithNameFormat:@"%@", COUNT_ITEMS];
    int result = (int)[tableDelete excuteQueryNonSelect:[NSString stringWithFormat:@"Delete from %@ where %@ = %d", COUNT_ITEMS, count_type_id, countType]];
//    if(result > 0) {
//        [NSString stringWithFormat: @"REINDEX %@.%@",DATABASE_NAME, COUNT_ITEMS];
//    }
    
    return result;
}

- (NSMutableArray*)loadAllCountItemByCountCategoryId:(NSInteger)countCategoryId andRoomTypeId:(NSInteger)roomTypeId {
    CountItemAdapterV2 *adapter = [[CountItemAdapterV2 alloc] init];
    [adapter openDatabase];
    NSMutableArray *array = [adapter loadAllCountItemModelByCountCategoryId:countCategoryId andRoomTypeId:roomTypeId];
    [adapter close];
    return array;
}

#pragma mark - Count Order
-(BOOL)insertCountOrder:(CountOrdersModelV2 *) model {
    CountOrderAdapterV2 *adapter = [[CountOrderAdapterV2 alloc] init];
    [adapter openDatabase];
    BOOL result = [adapter insertCountOrdersModel:model];
    [adapter close];
    return result;
}

-(BOOL) updateCountOrder:(CountOrdersModelV2 *) model {
    CountOrderAdapterV2 *adapter = [[CountOrderAdapterV2 alloc] init];
    [adapter openDatabase];
    BOOL result = [adapter updateCountOrdersModel:model];
    [adapter close];
    return result;
}

-(BOOL) deleteCountOrderByOrderId:(int)orderId {
    CountOrderAdapterV2 *adapter = [[CountOrderAdapterV2 alloc] init];
    [adapter openDatabase];
    BOOL result = [adapter deleteCountOrdersModelByOrderId:orderId];
    [adapter close];
    return result;
}

-(BOOL) deleteCountOrdersByPostStatus:(int)postStatus serviceId:(int)serviceId
{
    NSString *queryDelete = [[NSString alloc] initWithFormat:@"Delete from %@ where %@ = %d and %@ = %d", COUNT_ORDERS, scount_post_status, postStatus, scount_service_id, serviceId];
    
    DataTable *tableDelete = [[DataTable alloc] initWithNameFormat:@"%@", COUNT_ORDERS];
    
    return ([tableDelete excuteQueryNonSelect:queryDelete] > 0);
}

-(CountOrdersModelV2 *) loadCountOrder:(CountOrdersModelV2 *) model {
    CountOrderAdapterV2 *adapter = [[CountOrderAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter loadCountOrdersModel:model];
    [adapter close];
    return model;
}

-(NSMutableArray *) loadAllCountOrdersByRoomId:(NSInteger) countRoomId {
    CountOrderAdapterV2 *adapter = [[CountOrderAdapterV2 alloc] init];
    [adapter openDatabase];
    NSMutableArray *array = [adapter loadAllCountOrdersByRoomId:countRoomId];
    [adapter close];
    return array;
}

//-(CountOrdersModelV2 *)loadCountOrdersModelByRoomIdUserIdAndServiceId:(CountOrdersModelV2 *)model
//{
//    CountOrderAdapterV2 *adapter = [[CountOrderAdapterV2 alloc] init];
//    [adapter openDatabase];
//    [adapter loadCountOrdersModelByRoomIdUserIdAndServiceId:model];
//    [adapter close];
//    return model;
//
//}

//-(BOOL)isCheckCountOrder:(CountOrdersModelV2 *)model
//{
//    CountOrderAdapterV2 *adapter = [[CountOrderAdapterV2 alloc] init];
//    [adapter openDatabase];
//    [adapter loadCountOrdersModelByRoomIdUserIdAndServiceId:model];
//    [adapter close];
//    if (model.countOrderId != 0) {
//        return TRUE;
//    }
//    else
//    {
//        return FALSE;
//    }
//}

-(NSMutableArray *)loadAllCountOrdersByUserId:(NSInteger)userId AndServiceId:(NSInteger)serviceId
{
    CountOrderAdapterV2 *adapter = [[CountOrderAdapterV2 alloc] init];
    [adapter openDatabase];
    NSMutableArray *array = [adapter loadAllCountOrdersByUserId:userId AndServiceId:serviceId];
    [adapter close];
    return array;
}

//-------------------------update code----------------------------------
-(NSMutableArray *)loadCountOrdersByUserId:(NSInteger)userId AndServiceId:(NSInteger)serviceId{
    CountOrderAdapterV2 *adapter = [[CountOrderAdapterV2 alloc] init];
    [adapter openDatabase];
    NSMutableArray *array = [adapter loadCountOrdersByUserId:userId AndServiceId:serviceId];
    [adapter close];
    return array;
}
//-------------------------update code----------------------------------

-(CountOrdersModelV2*) loadCountOrdersModelByRoomAssignId:(int)roomAssignId roomNumber:(NSString*)roomNumber userId:(int)userId serviceId:(int)serviceId postStatus:(int)postStatus
{
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ = %d AND %@ = %d AND %@ = %d AND %@ = %d AND %@ = '%@' order by %@ DESC",
                                  scount_id,
                                  scount_room_id,
                                  scount_user_id,
                                  scount_status,
                                  scount_collect_date,
                                  scount_service_id,
                                  scount_room_number,
                                  scount_post_status,
                                  scount_order_ws_id,
                                  
                                  COUNT_ORDERS,
                                  
                                  scount_room_id,
                                  roomAssignId,
                                  
                                  scount_user_id,
                                  userId,
                                  
                                  scount_post_status,
                                  postStatus,
                                  
                                  scount_service_id,
                                  serviceId,
                                  
                                  scount_room_number,
                                  roomNumber,
                                  
                                  //Order by
                                  scount_id
                                  ];
    
    DataTable *table = [[DataTable alloc] init];
    
    //Row result reference
    DataRow *rowReference = [[DataRow alloc] init];
    [rowReference addColumnFormatInt:@"%@", scount_id];
    [rowReference addColumnFormatInt:@"%@", scount_room_id];
    [rowReference addColumnFormatInt:@"%@", scount_user_id];
    [rowReference addColumnFormatInt:@"%@", scount_status];
    [rowReference addColumnFormatString:@"%@", scount_collect_date];
    [rowReference addColumnFormatInt:@"%@", scount_service_id];
    [rowReference addColumnFormatString:@"%@", scount_room_number];
    [rowReference addColumnFormatInt:@"%@", scount_post_status];
    [rowReference addColumnFormatInt:@"%@", scount_order_ws_id];
    
    [table loadTableDataWithQuery:sqlString referenceRow:rowReference];
    int rowCount = [table countRows];
    
    CountOrdersModelV2 *model = [[CountOrdersModelV2 alloc] init];
    if(rowCount > 0)
    {
        DataRow *row = [table rowWithIndex:0];
        model.countOrderId = [row intValueWithColumnNameFormat:@"%@", scount_id];
        model.countRoomAssignId = [row intValueWithColumnNameFormat:@"%@", scount_room_id];
        model.countUserId = [row intValueWithColumnNameFormat:@"%@", scount_user_id];
        model.countStatus = [row intValueWithColumnNameFormat:@"%@", scount_status];
        model.countCollectDate = [row stringValueWithColumnNameFormat:@"%@", scount_collect_date];
        model.countServiceId = [row intValueWithColumnNameFormat:@"%@", scount_service_id];
        model.countRoomNumber = [row stringValueWithColumnNameFormat:@"%@", scount_room_number];
        model.countPostStatus = [row intValueWithColumnNameFormat:@"%@", scount_post_status];
        model.countOrderWSId =  [row intValueWithColumnNameFormat:@"%@", scount_order_ws_id];
    }
    
    return model;
}

-(NSMutableArray*) loadAllCountOrdersModelByUserId:(int)userId serviceId:(int)serviceId postStatus:(int)postStatus
{
    NSMutableArray *results = nil;
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ = %d AND %@ = %d AND %@ = %d",
                                  scount_id,
                                  scount_room_id,
                                  scount_user_id,
                                  scount_status,
                                  scount_collect_date,
                                  scount_service_id,
                                  scount_room_number,
                                  scount_post_status,
                                  scount_order_ws_id,
                                  
                                  COUNT_ORDERS,
                                  
                                  scount_service_id,
                                  serviceId,
                                  
                                  scount_user_id,
                                  userId,
                                  
                                  scount_post_status,
                                  postStatus
                                  ];
    
    DataTable *table = [[DataTable alloc] init];
    
    //Row result reference
    DataRow *rowReference = [[DataRow alloc] init];
    [rowReference addColumnFormatInt:@"%@", scount_id];
    [rowReference addColumnFormatInt:@"%@", scount_room_id];
    [rowReference addColumnFormatInt:@"%@", scount_user_id];
    [rowReference addColumnFormatInt:@"%@", scount_status];
    [rowReference addColumnFormatString:@"%@", scount_collect_date];
    [rowReference addColumnFormatInt:@"%@", scount_service_id];
    [rowReference addColumnFormatString:@"%@", scount_room_number];
    [rowReference addColumnFormatInt:@"%@", scount_post_status];
    [rowReference addColumnFormatInt:@"%@", scount_order_ws_id];
    
    [table loadTableDataWithQuery:sqlString referenceRow:rowReference];
    int rowCount = [table countRows];
    
    if(rowCount > 0)
    {
        results = [NSMutableArray array];
        
        for (int i = 0; i < rowCount; i ++) {
            CountOrdersModelV2 *model = [[CountOrdersModelV2 alloc] init];
            DataRow *row = [table rowWithIndex:0];
            
            model.countOrderId = [row intValueWithColumnNameFormat:@"%@", scount_id];
            model.countRoomAssignId = [row intValueWithColumnNameFormat:@"%@", scount_room_id];
            model.countUserId = [row intValueWithColumnNameFormat:@"%@", scount_user_id];
            model.countStatus = [row intValueWithColumnNameFormat:@"%@", scount_status];
            model.countCollectDate = [row stringValueWithColumnNameFormat:@"%@", scount_collect_date];
            model.countServiceId = [row intValueWithColumnNameFormat:@"%@", scount_service_id];
            model.countRoomNumber = [row stringValueWithColumnNameFormat:@"%@", scount_room_number];
            model.countPostStatus = [row intValueWithColumnNameFormat:@"%@", scount_post_status];
            model.countOrderWSId =  [row intValueWithColumnNameFormat:@"%@", scount_order_ws_id];
            
            [results addObject:model];
        }
    }
    
    return results;
}

-(NSMutableArray*) loadAllCountOrdersModelByUserId:(int)userId serviceId:(int)serviceId
{
    NSMutableArray *results = nil;
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ = %d AND %@ = %d",
                                  scount_id,
                                  scount_room_id,
                                  scount_user_id,
                                  scount_status,
                                  scount_collect_date,
                                  scount_service_id,
                                  scount_room_number,
                                  scount_post_status,
                                  scount_order_ws_id,
                                  
                                  COUNT_ORDERS,
                                  
                                  scount_service_id,
                                  serviceId,
                                  
                                  scount_user_id,
                                  userId
                                  ];
    
    DataTable *table = [[DataTable alloc] init];
    
    //Row result reference
    DataRow *rowReference = [[DataRow alloc] init];
    [rowReference addColumnFormatInt:@"%@", scount_id];
    [rowReference addColumnFormatInt:@"%@", scount_room_id];
    [rowReference addColumnFormatInt:@"%@", scount_user_id];
    [rowReference addColumnFormatInt:@"%@", scount_status];
    [rowReference addColumnFormatString:@"%@", scount_collect_date];
    [rowReference addColumnFormatInt:@"%@", scount_service_id];
    [rowReference addColumnFormatString:@"%@", scount_room_number];
    [rowReference addColumnFormatInt:@"%@", scount_post_status];
    [rowReference addColumnFormatInt:@"%@", scount_order_ws_id];
    
    [table loadTableDataWithQuery:sqlString referenceRow:rowReference];
    int rowCount = [table countRows];
    
    if(rowCount > 0)
    {
        results = [NSMutableArray array];
        
        for (int i = 0; i < rowCount; i ++) {
            CountOrdersModelV2 *model = [[CountOrdersModelV2 alloc] init];
            DataRow *row = [table rowWithIndex:0];
            
            model.countOrderId = [row intValueWithColumnNameFormat:@"%@", scount_id];
            model.countRoomAssignId = [row intValueWithColumnNameFormat:@"%@", scount_room_id];
            model.countUserId = [row intValueWithColumnNameFormat:@"%@", scount_user_id];
            model.countStatus = [row intValueWithColumnNameFormat:@"%@", scount_status];
            model.countCollectDate = [row stringValueWithColumnNameFormat:@"%@", scount_collect_date];
            model.countServiceId = [row intValueWithColumnNameFormat:@"%@", scount_service_id];
            model.countRoomNumber = [row stringValueWithColumnNameFormat:@"%@", scount_room_number];
            model.countPostStatus = [row intValueWithColumnNameFormat:@"%@", scount_post_status];
            model.countOrderWSId =  [row intValueWithColumnNameFormat:@"%@", scount_order_ws_id];
            
            [results addObject:model];
        }
    }
    
    return results;
}

-(int) countUnpostOrderItemsByUserId:(int)userId serviceId:(int)serviceId
{
    int totalCount = 0;
    
    NSMutableString *sqlCountOrder = [[NSMutableString alloc] initWithFormat:@"SELECT count(1) FROM %@ WHERE %@ = %d AND %@ = %d AND %@ = %d",
                                  
                                  COUNT_ORDERS,
                                  
                                  scount_service_id,
                                  serviceId,
                                  
                                  scount_user_id,
                                  userId,
                                  
                                  scount_post_status,
                                  (int)POST_STATUS_SAVED_UNPOSTED
                                      
                                  ];
    
    DataTable *tableCount = [[DataTable alloc] initWithNameFormat:@"%@", COUNT_ORDERS];
    
    DataRow *rowRef = [[DataRow alloc] init];
    [rowRef addColumnFormatInt:@"CountResult"];
    [tableCount loadTableDataWithQuery:sqlCountOrder referenceRow:rowRef];
    
    int rowCount = [tableCount countRows];
    
    if(rowCount > 0){
        totalCount = [[tableCount rowWithIndex:0] intValueWithColumnNameFormat:@"CountResult"];
    }
    
    NSMutableString *sqlCountOrderDetails = [[NSMutableString alloc] initWithFormat:@"SELECT count(1) FROM %@ a, %@ b WHERE a.%@ = %d AND a.%@ = %d AND b.%@ = %d AND a.%@ = b.%@",
                                             
                                             COUNT_ORDERS,
                                             COUNT_ORDER_DETAILS,
                                             
                                             scount_service_id,
                                             serviceId,
                                             
                                             scount_user_id,
                                             userId,
                                             
                                             dcount_pos_status,
                                             (int)POST_STATUS_SAVED_UNPOSTED,
                                             
                                             scount_service_id,
                                             dcount_order_id
                                             ];
    
    tableCount.tableName = [NSString stringWithFormat:@"%@", COUNT_ORDER_DETAILS];
    [tableCount loadTableDataWithQuery:sqlCountOrderDetails referenceRow:rowRef];
    rowCount = [tableCount countRows];
    
    if(rowCount > 0){
        totalCount += [[tableCount rowWithIndex:0] intValueWithColumnNameFormat:@"CountResult"];
    }
    
    return totalCount;
}

#pragma mark - Count Order Detail
-(BOOL)insertCountOrderDetail:(CountOrderDetailsModelV2 *) model {
    CountOrderDetailAdapterV2 *adapter = [[CountOrderDetailAdapterV2 alloc] init];
    [adapter openDatabase];
    BOOL result = [adapter insertCountOrderDetailsModel:model];
    [adapter close];
    return result;
}

-(BOOL) updateCountOrderDetail:(CountOrderDetailsModelV2 *) model {
    CountOrderDetailAdapterV2 *adapter = [[CountOrderDetailAdapterV2 alloc] init];
    [adapter openDatabase];
    BOOL result = [adapter updateCountOrderDetailsModel:model];
    [adapter close];
    return result;
}

-(BOOL) deleteCountOrderDetail:(CountOrderDetailsModelV2 *) model {
    CountOrderDetailAdapterV2 *adapter = [[CountOrderDetailAdapterV2 alloc] init];
    [adapter openDatabase];
    BOOL result = [adapter deleteCountOrderDetailsModel:model];
    [adapter close];
    return result;
}

-(int) deleteCountOrderDetailsByOrderId:(int)orderId
{
    NSString *queryDelete = [[NSString alloc] initWithFormat:@"Delete from %@ where %@ = %d", COUNT_ORDER_DETAILS, dcount_order_id, orderId];
    DataTable *tableDelete = [[DataTable alloc] initWithNameFormat:@"%@", COUNT_ORDER_DETAILS];
    return ([tableDelete excuteQueryNonSelect:queryDelete] > 0);
}

-(BOOL) deleteCountOrderDetailById:(NSInteger)modelId{
    CountOrderDetailAdapterV2 *adapter = [[CountOrderDetailAdapterV2 alloc] init];
    [adapter openDatabase];
    BOOL result = [adapter deleteCountOrderDetailsModelById:modelId];
    [adapter close];
    return result;
}

-(CountOrderDetailsModelV2 *) loadCountOrderDetail:(CountOrderDetailsModelV2 *) model {
    CountOrderDetailAdapterV2 *adapter = [[CountOrderDetailAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter loadCountOrderDetailsModel:model];
    [adapter close];
    return model;
}

-(NSMutableArray *) loadAllCountOrderDetailByOrderId:(NSInteger) countOrderId {
    CountOrderDetailAdapterV2 *adapter = [[CountOrderDetailAdapterV2 alloc] init];
    [adapter openDatabase];
    NSMutableArray *array = [adapter loadAllCountOrderDetailByOrderId:countOrderId];
    [adapter close];
    return array;
}

-(CountOrderDetailsModelV2 *)loadCountOrderDetailsByItemId:(NSInteger)countItemId
{
   
    CountOrderDetailAdapterV2 *adapter = [[CountOrderDetailAdapterV2 alloc] init];
    [adapter openDatabase];
    CountOrderDetailsModelV2 *model = [adapter loadCountOrderDetailsByItemId:countItemId];
    [adapter close];
    return model;
}

-(CountOrderDetailsModelV2 *)loadCountOrderDetailsByItemId:(NSInteger)countItemId AndOrderId:(NSInteger)orderId
{
    CountOrderDetailAdapterV2 *adapter = [[CountOrderDetailAdapterV2 alloc] init];
    [adapter openDatabase];
    CountOrderDetailsModelV2 *model = [adapter loadCountOrderDetailsByItemId:countItemId AndOrderId:orderId];
    [adapter close];
    return model;
}

#pragma mark - Count WS for Linen


-(BOOL)getLinenCategoryListWSWithUserId:(NSInteger)userId AndHotelId:(NSInteger)hotelId
{
    BOOL returnCode = NO;
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    if([LogFileManager isLogConsole])
    {
        [binding setLogXMLInOut:YES];
    }
    eHousekeepingService_GetLinenCategoryList *request = [[eHousekeepingService_GetLinenCategoryList alloc] init];
    
//    CountCategoryModelV2 *linenCategoryLatest = [self loadCountCategoryLatestWithHotelId:hotelId AndCountServiceId:cLinen];
    
    [request setIntUsrID:[NSNumber numberWithInteger:userId]];
    [request setIntHotelID:[NSNumber numberWithInteger:hotelId]];
//    if (linenCategoryLatest.countCategoryLastModified) {
//        [request setStrLastModified:linenCategoryLatest.countCategoryLastModified];
//    }
    
//    LogObject *logObj = [[LogObject alloc] init];
//    logObj.dateTime = [NSDate date];
//    logObj.log = [NSString stringWithFormat:@"<%i><getLinenCategoryListWSWithUserId><intUsrID:%i intHotelID:%i lastModified:%@>",userId,userId,hotelId,linenCategoryLatest.countCategoryLastModified];
//    LogFileManager *logManager = [[LogFileManager alloc] init];
//    [logManager save:logObj];
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetLinenCategoryListUsingParameters:request];
    NSArray *responseBodyParts = response.bodyParts;
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[SOAPFault class]]) {
            continue;   
        }
        
        //Get Linen Categories List
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetLinenCategoryListResponse class]]) {
            
            eHousekeepingService_GetLinenCategoryListResponse *body = (eHousekeepingService_GetLinenCategoryListResponse *)bodyPart;
            
            if ([body.GetLinenCategoryListResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]]) 
            {
            	// [self deleteAllCountCategory];
                [self deleteAllCountCategoryByServiceId:cLinen];
                NSMutableArray *arrayLinenCategoryWS = body.GetLinenCategoryListResult.LinenCat.LinenCategoryList;
                for (eHousekeepingService_LinenCategoryList *linenCategoryWS in arrayLinenCategoryWS) {
                    
                    //Get Linen Categories fro++++++++++++++m database
                    CountCategoryModelV2 *linenCategoryModel = [[CountCategoryModelV2 alloc] init];
                    linenCategoryModel.countCategoryId = [linenCategoryWS.lncID integerValue];
                    linenCategoryModel.countServiceId = cLinen;
                    
                    
                    [self loadCountCategoryByCountServiceIdAndCategoryIdData:linenCategoryModel];
                    
                    if (linenCategoryModel.countHotelId != nil) {
                        
                        if (![linenCategoryModel.countCategoryLastModified isEqualToString:linenCategoryWS.lncLastModified]) {
                            //Update Line Category
                            linenCategoryModel.countCategoryName = linenCategoryWS.lncName;
                            linenCategoryModel.countCategoryNameLang = linenCategoryWS.lncLang;
                            
//                            if (linenCategoryWS.lncPicture == nil) {
//                                linenCategoryModel.countCategoryImage = UIImagePNGRepresentation([UIImage imageNamed:IMAGE_NO_IMAGE_AVAILABLE]);
//                            } else
                            linenCategoryModel.countCategoryImage = linenCategoryWS.lncPicture;
                            linenCategoryModel.countCategoryLastModified = linenCategoryWS.lncLastModified;
                            linenCategoryModel.countServiceId = cLinen;
                            linenCategoryModel.countHotelId = [NSString stringWithFormat:@"%d", [[[UserManagerV2 sharedUserManager] currentUser]userHotelsId]];
                            [self updateCountCategory:linenCategoryModel];
                            
                        }
                    }
                    else
                    {
                        //Insert Line Category
                        CountCategoryModelV2 *linenCategoryDB = [[CountCategoryModelV2 alloc] init];
                        linenCategoryDB.countCategoryId = [linenCategoryWS.lncID integerValue];
                        linenCategoryDB.countCategoryName = linenCategoryWS.lncName;
                        linenCategoryDB.countCategoryNameLang = linenCategoryWS.lncLang;
//                        if (linenCategoryWS.lncPicture == nil) {
//                            
//                            linenCategoryDB.countCategoryImage = UIImagePNGRepresentation([UIImage imageNamed:IMAGE_NO_IMAGE_AVAILABLE]);
// 
//                        } else
                        linenCategoryDB.countCategoryImage = linenCategoryWS.lncPicture;
                        
                        linenCategoryDB.countCategoryLastModified = linenCategoryWS.lncLastModified;
                        linenCategoryDB.countServiceId = cLinen;
                        linenCategoryDB.countHotelId = [NSString stringWithFormat:@"%d", [[[UserManagerV2 sharedUserManager] currentUser]userHotelsId]];
                        [self insertCountCategory:linenCategoryDB];                        
                    }
                    
                }
                returnCode = YES;
            }
            else
            {
                returnCode = NO;
            }
        }
    }
    return returnCode;
    
}

-(BOOL) getLinenItemListWSWithUserId:(NSInteger)userId AndHotelId:(NSInteger)hotelId
{
    BOOL returnCode = NO;
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    if([LogFileManager isLogConsole]){
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_GetLinenItemList *request = [[eHousekeepingService_GetLinenItemList alloc] init];
    
    
//    CountItemsModelV2 *linenItemsLatest = [self loadCountItemsLatestWithHotelId:hotelId AndServiceId:cLinen];
    
    [request setIntUsrID:[NSNumber numberWithInteger:userId]];
    [request setIntHotelID:[NSNumber numberWithInteger:hotelId]];
    
//    if (linenItemsLatest.countItemLastModified) {
//        [request setStrLastModified:linenItemsLatest.countItemLastModified];
//    }
    
//    LogObject *logObj = [[LogObject alloc] init];
//    logObj.dateTime = [NSDate date];
//    logObj.log = [NSString stringWithFormat:@"<%i><getLinenItemListWSWithUserId><intUsrID:%i intHotelID:%i lastModified:%@>",userId,userId,hotelId,linenItemsLatest.countItemLastModified];
//    LogFileManager *logManager = [[LogFileManager alloc] init];
//    [logManager save:logObj];
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetLinenItemListUsingParameters:request];
    
   NSArray *responseBodyParts = response.bodyParts;
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[SOAPFault class]]) {
            continue;   
        }
        
        //Get Linen Item List
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetLinenItemListResponse class]]) {
            
            eHousekeepingService_GetLinenItemListResponse *body = (eHousekeepingService_GetLinenItemListResponse *)bodyPart;
            
            if ([body.GetLinenItemListResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]]) 
            {
                [self deleteAllItemsWithCountType:cLinen];
                
                NSMutableArray *arrayLinenItemWS = body.GetLinenItemListResult.LinenItm.LinenItemList;
                for (eHousekeepingService_LinenItemList *linenItemWS in arrayLinenItemWS) {
                    
                    //Get Linen Item from database
                    CountItemsModelV2 *linenItemModel = [[CountItemsModelV2 alloc] init];
                    linenItemModel.countItemId = [linenItemWS.lniID integerValue];
                    linenItemModel.countCategoryId = [linenItemWS.lniCategoryID integerValue];
                    
                    
                    
                    [self loadCountItemsByCountCategoryIdAndCountItemId:linenItemModel];
                    
                    if (linenItemModel.countItemName != nil) {
                        
                        if (![linenItemModel.countItemLastModified isEqualToString:linenItemWS.lniLastModified]) {
                            //Update Line item
                            linenItemModel.countItemName = linenItemWS.lniName;
                            linenItemModel.countItemLang = linenItemWS.lniLang;
//                            if (linenItemWS.lniPicture == nil) {
//                                linenItemModel.countItemImage = UIImagePNGRepresentation([UIImage imageNamed:IMAGE_NO_IMAGE_AVAILABLE]);
//                            } else
                            linenItemModel.countItemImage = linenItemWS.lniPicture;
                            linenItemModel.countItemLastModified = linenItemWS.lniLastModified;
                            linenItemModel.countCategoryId = [linenItemWS.lniCategoryID integerValue];
                            linenItemModel.countItemType = cLinen;
                            [self updateCountItem:linenItemModel];
                        }
                    }
                    else
                    {
                        //Insert Line item
                        CountItemsModelV2 *countItemDB = [[CountItemsModelV2 alloc] init];
                        countItemDB.countItemId = [linenItemWS.lniID integerValue];
                        countItemDB.countItemName = linenItemWS.lniName;
                        countItemDB.countItemLang = linenItemWS.lniLang;
                        countItemDB.countCategoryId = [linenItemWS.lniCategoryID integerValue];
//                        if (linenItemWS.lniPicture == nil) {
//                            countItemDB.countItemImage = UIImagePNGRepresentation([UIImage imageNamed:IMAGE_NO_IMAGE_AVAILABLE]);
//                        } else
                        countItemDB.countItemImage = linenItemWS.lniPicture;
                        countItemDB.countItemLastModified = linenItemWS.lniLastModified;
                        countItemDB.countItemType = cLinen;
                        [self insertCountItem:countItemDB];
                    }
                    
                }
                returnCode = YES;
            }
            else
            {
                returnCode = NO;
            }
        }
    }
    return returnCode;
    
}

-(BOOL)postLinenItemWSWithUserId:(NSInteger)userId roomAssignId:(NSInteger)roomAssignId AndLinenOrderModel:(CountOrderDetailsModelV2 *)model
{
    BOOL result = NO;
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    eHousekeepingService_PostLinenItem *request = [[eHousekeepingService_PostLinenItem alloc] init];
    
    if (model.countPostStatus == POST_STATUS_SAVED_UNPOSTED) {
        
        [request setIntUsrID:[NSNumber numberWithInteger:userId]];
        [request setIntRoomAssignID:[NSNumber numberWithInteger:roomAssignId]];
        [request setIntLinenItemID:[NSNumber numberWithInteger:model.countItemId]];
        [request setIntNewQuantity:[NSNumber numberWithInteger:model.countNew]];
        [request setIntUsedQuantity:[NSNumber numberWithInteger:model.countCollected]];
        [request setStrTransactionTime:model.countCollectedDate];
        
        LogObject *logObj = [[LogObject alloc] init];
        logObj.dateTime = [NSDate date];
        logObj.log = [NSString stringWithFormat:@"<%i><postLinenItemWSWithUserId><intUsrID:%i intRoomAssignID:%i intLinenItemID:%i intNewQuantity:%i intUsedQuantity:%i transactionTime:%@>",(int)userId,(int)userId,(int)roomAssignId,(int)model.countItemId,(int)model.countNew,(int)model.countCollected,model.countCollectedDate];
        LogFileManager *logManager = [[LogFileManager alloc] init];
        [logManager save:logObj];
        
        eHousekeepingServiceSoap12BindingResponse *response = [binding PostLinenItemUsingParameters:request];
        
        NSArray *responseBodyParts = response.bodyParts;
        
        for (id bodyPart in responseBodyParts) {
            if ([bodyPart isKindOfClass:[eHousekeepingService_PostLinenItemResponse class]]) {
                
                eHousekeepingService_PostLinenItemResponse *dataBody = (eHousekeepingService_PostLinenItemResponse *)bodyPart;
                
                if (dataBody != nil) {
                
                    if ([dataBody.PostLinenItemResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_NEW_RECORD_ADDED]]) {
                        //change post status in linen
                        result = YES;
                        if (model.countPostStatus == POST_STATUS_SAVED_UNPOSTED) {
                            model.countPostStatus = POST_STATUS_POSTED;
                            [self updateCountOrderDetail:model];
                        }
                        
                    } 
                    else {
                        result = NO;
                    }
                }
            }
        }
        
    }
    else {
        
        return YES;
    }
    
    return result;

}

-(NSInteger)numberOfMustPostLinenRecords:(NSInteger)userId{
    NSMutableArray *arrayLinen = [self loadAllCountOrdersByUserId:userId AndServiceId:cLinen];
    NSInteger countPostLinen = 0;
    for (CountOrdersModelV2 *orderModel in arrayLinen) {
        NSMutableArray *arrayPostLinen = [self loadAllCountOrderDetailByOrderId:orderModel.countOrderId];
        countPostLinen += arrayPostLinen.count;
    }
    return countPostLinen;
}

/*
//Post Linen item with room assign ID parameter
-(NSArray *)postAllLinenItemsWithUserId:(NSInteger)userId roomAssignId:(NSInteger)roomAssignId roomNumber:(NSString*)roomNumber AndLinenOrderModelList:(NSArray *)modelList
{
    
    NSMutableArray *postedItems = nil;
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole])
    {
        [binding setLogXMLInOut:YES];
    }
    eHousekeepingService_PostLinenItems *request = [[eHousekeepingService_PostLinenItems alloc] init];
    
    //CRF-1229: Posting History
    NSMutableArray *listHistoryPosting = [NSMutableArray array];
    BOOL isEnablePostingHistory = [[UserManagerV2 sharedUserManager].currentUserAccessRight isEnablePostingHistory];
    BOOL isEnglishLanguage = [[LanguageManagerV2 sharedLanguageManager] currentLanguageIsEqualTo:@"%@", ENGLISH_LANGUAGE];
    
    [request setIntUserID:[NSNumber numberWithInt:userId]];
    [request setIntRoomAssignID:[NSNumber numberWithInt:roomAssignId]];
    
    NSMutableString *postedItemsLog = [[NSMutableString alloc] init];
    eHousekeepingService_ArrayOfPostItem *postedItem = [[eHousekeepingService_ArrayOfPostItem alloc] init];
    for (CountOrderDetailsModelV2 *model in modelList) {
        eHousekeepingService_PostItem *postItem = [[eHousekeepingService_PostItem alloc] init];
        [postItem setIntItemID:[NSNumber numberWithInt:model.countItemId]];
        [postItem setIntNewQuantity:[NSNumber numberWithInt:model.countNew]];
        [postItem setIntUsedQuantity:[NSNumber numberWithInt:model.countCollected]];
        [postItem setStrTransactionTime:model.countCollectedDate];
        [postedItem addPostItem:postItem];
        
        //CRF-1229: Posting History
        if (isEnablePostingHistory) {
            //History Posting
            eHousekeepingService_PostingHistoryDetails *historyDetail = [[eHousekeepingService_PostingHistoryDetails alloc] init];
            [historyDetail setUserID:[NSString stringWithFormat:@"%d", userId]];
            [historyDetail setRoomNo:roomNumber];
            [historyDetail setHotelID:[NSString stringWithFormat:@"%d", [UserManagerV2 sharedUserManager].currentUser.userHotelsId]];
            [historyDetail setModuleID:[NSString stringWithFormat:@"%d", ModuleHistory_Linen]];
            [historyDetail setNewQuantity:[NSString stringWithFormat:@"%d", model.countNew]];
            [historyDetail setUsedQuantity:[NSString stringWithFormat:@"%d", model.countCollected]];
            [historyDetail setTransactionDateTime:model.countCollectedDate];
            
            CountItemsModelV2 *countItem = [[CountItemsModelV2 alloc] init];
            countItem.countItemId = model.countItemId;
            countItem = [self loadCountItem:countItem];
            
            CountCategoryModelV2 *countCategory = [[CountCategoryModelV2 alloc] init];
            countCategory.countCategoryId = countItem.countCategoryId;
            countCategory = [self loadCountCategory:countCategory];
            
            if (isEnglishLanguage) {
                [historyDetail setItemDescription:countItem.countItemName];
                [historyDetail setCategoryDescription:countCategory.countCategoryName];
            } else {
                [historyDetail setItemDescription:countItem.countItemLang];
                [historyDetail setCategoryDescription:countCategory.countCategoryNameLang];
            }
            
            [listHistoryPosting addObject:historyDetail];
        }
        
        [postedItemsLog appendFormat:@"\nintLinenItemID:%i intNewQuantity:%i intUsedQuantity:%i transactionTime%@",model.countItemId,model.countNew,model.countCollected,model.countCollectedDate];
    }
    
    NSNumber *isEnableLog = [[NSUserDefaults standardUserDefaults] objectForKey:ENABLE_LOG];
    if(isEnableLog != nil && [isEnableLog intValue] == 1){
        LogObject *logObj = [[LogObject alloc] init];
        logObj.dateTime = [NSDate date];
        logObj.log = [NSString stringWithFormat:@"<%i><postAllLinenItemsWithUserId><intUsrID:%i intRoomAssignID:%i%@>",userId,userId,roomAssignId,postedItemsLog];
        LogFileManager *logManager = [[LogFileManager alloc] init];
        [logManager save:logObj];
    }
    
    [request setPostItems:postedItem];
    eHousekeepingServiceSoap12BindingResponse *response = [binding PostLinenItemsUsingParameters:request];
    for (id bodyPart in response.bodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_PostLinenItemsResponse class]]) {
            eHousekeepingService_PostLinenItemsResponse *data = (eHousekeepingService_PostLinenItemsResponse*)bodyPart;
            if (data != nil) {
                
                postedItems = [[NSMutableArray alloc] init];
                for (eHousekeepingService_PostedItem *postedItem in data.PostLinenItemsResult.LinenItems.PostedItem) {
                    [postedItems addObject:postedItem.ID_];
                }
                
            }
        }
    }
    
    return postedItems;
}
*/

//Post Linen item with room number parameter
-(NSArray *)postAllLinenItemsWithUserId:(NSInteger)userId roomAssignId:(NSInteger)roomAssignId roomNumber:(NSString*)roomNumber AndLinenOrderModelList:(NSArray *)modelList
{
    NSMutableArray *postedItems = nil;
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole])
    {
        [binding setLogXMLInOut:YES];
    }
    eHousekeepingService_PostingLinenItems *request = [[eHousekeepingService_PostingLinenItems alloc] init];
    
    [request setIntUserID:[NSNumber numberWithInteger:userId]];
    
    //Always set room Assign ID = 0 cause this is required field and this field is not neccessary
    [request setIntRoomAssignID:[NSNumber numberWithInteger:roomAssignId]];
    
    [request setStrRoomNo:roomNumber];
    
    NSMutableString *postedItemsLog = [[NSMutableString alloc] init];
    NSNumber *isEnableLog = [[NSUserDefaults standardUserDefaults] objectForKey:ENABLE_LOG];
    BOOL enabledLog = NO;
    if(isEnableLog != nil && [isEnableLog intValue] == 1){
        enabledLog = YES;
    }
    eHousekeepingService_ArrayOfPostItem *postedItem = [[eHousekeepingService_ArrayOfPostItem alloc] init];
    
    //CRF-1229: Posting History
    NSMutableArray *listHistoryPosting = [NSMutableArray array];
    // DungPhan - 20150908: Enable post Posting History for Linen by Access Rights
    BOOL isEnablePostingHistory = ([[UserManagerV2 sharedUserManager].currentUserAccessRight isFindActionHistoryActive] || [[UserManagerV2 sharedUserManager].currentUserAccessRight isFindActionHistoryAllowedView] || [[UserManagerV2 sharedUserManager].currentUserAccessRight isRoomDetailsActionHistoryActive] || [[UserManagerV2 sharedUserManager].currentUserAccessRight isRoomDetailsActionHistoryAllowedView]);
    BOOL isEnglishLanguage = [[LanguageManagerV2 sharedLanguageManager] currentLanguageIsEqualTo:@"%@", ENGLISH_LANGUAGE];
    
    for (CountOrderDetailsModelV2 *model in modelList) {
        eHousekeepingService_PostItem *postItem = [[eHousekeepingService_PostItem alloc] init];
        [postItem setIntItemID:[NSNumber numberWithInteger:model.countItemId]];
        [postItem setIntNewQuantity:[NSNumber numberWithInteger:model.countNew]];
        [postItem setIntUsedQuantity:[NSNumber numberWithInteger:model.countCollected]];
        [postItem setStrTransactionTime:model.countCollectedDate];
        [postedItem addPostItem:postItem];
        
        //CRF-1229: Posting History
        //if (isEnablePostingHistory) {
            //History Posting
            eHousekeepingService_PostingHistoryDetails *historyDetail = [[eHousekeepingService_PostingHistoryDetails alloc] init];
            [historyDetail setUserID:[NSString stringWithFormat:@"%d", (int)userId]];
            [historyDetail setRoomNo:roomNumber];
            [historyDetail setHotelID:[NSString stringWithFormat:@"%d", [UserManagerV2 sharedUserManager].currentUser.userHotelsId]];
            [historyDetail setModuleID:[NSString stringWithFormat:@"%d", (int)ModuleHistory_Linen]];
            [historyDetail setNewQuantity:[NSString stringWithFormat:@"%d", (int)model.countNew]];
            [historyDetail setUsedQuantity:[NSString stringWithFormat:@"%d", (int)model.countCollected]];
            [historyDetail setTransactionDateTime:model.countCollectedDate];
            
            CountItemsModelV2 *countItem = [[CountItemsModelV2 alloc] init];
            countItem.countItemId = model.countItemId;
            countItem = [self loadCountItem:countItem];
            
            CountCategoryModelV2 *countCategory = [[CountCategoryModelV2 alloc] init];
            countCategory.countCategoryId = countItem.countCategoryId;
            countCategory = [self loadCountCategory:countCategory];
            
            if (isEnglishLanguage) {
                [historyDetail setItemDescription:countItem.countItemName];
                [historyDetail setCategoryDescription:countCategory.countCategoryName];
            } else {
                [historyDetail setItemDescription:countItem.countItemLang];
                [historyDetail setCategoryDescription:countCategory.countCategoryNameLang];
            }
            
            [listHistoryPosting addObject:historyDetail];
        //}
        
        if(enabledLog){
            [postedItemsLog appendFormat:@"\nintLinenItemID:%i intNewQuantity:%i intUsedQuantity:%i transactionTime%@",(int)model.countItemId,(int)model.countNew,(int)model.countCollected,model.countCollectedDate];
        }
    }
    
    if(enabledLog){
        LogObject *logObj = [[LogObject alloc] init];
        logObj.dateTime = [NSDate date];
        logObj.log = [NSString stringWithFormat:@"<%i><postAllLinenItemsWithUserId><intUsrID:%i intRoomNumber:%@%@>",(int)userId,(int)userId,roomNumber,postedItemsLog];
        LogFileManager *logManager = [[LogFileManager alloc] init];
        [logManager save:logObj];
    }
    
    [request setPostItems:postedItem];
    eHousekeepingServiceSoap12BindingResponse *response = [binding PostingLinenItemsUsingParameters:request];
    for (id bodyPart in response.bodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_PostingLinenItemsResponse class]]) {
            eHousekeepingService_PostingLinenItemsResponse *data = (eHousekeepingService_PostingLinenItemsResponse*)bodyPart;
            if (data != nil) {
                
                
                postedItems = [[NSMutableArray alloc] init];
                for (eHousekeepingService_PostedItem *postedItem in data.PostingLinenItemsResult.LinenItems.PostedItem) {
                    [postedItems addObject:postedItem.ID_];
                }
                
                //CRF-1229: Posting history
                //if (isEnablePostingHistory) {
                    [self postHistoryPostingWS:listHistoryPosting];
                //}
            }
        }
    }
    
    return postedItems;
}

#pragma mark - Count WS for Minibar

-(void) hardcodeCategoryMinibarWSWithLinenCateogryModel:(CountCategoryModelV2 *) minibarCategory andLinenCategoryWSModel:(eHousekeepingService_MinibarCategoryList *) minibarCategoryWS{
    
//    if ([minibarCategoryWS.mbcName isEqualToString:@"Beverage"]) {
//        //Beverage
//        minibarCategory.countCategoryImage = UIImagePNGRepresentation([UIImage imageNamed:@"alcoholic.png"]);
//    }
//    
//    if ([minibarCategoryWS.mbcName isEqualToString:@"Food"]) {
//        //Food
//        minibarCategory.countCategoryImage = UIImagePNGRepresentation([UIImage imageNamed:@"snacks.png"]);
//    }
}

-(BOOL) getMinibarCategoryListWSWithUserId:(NSInteger)userId AndHotelId:(NSInteger)hotelId
{
    BOOL returnCode = NO;
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
//    [binding setDefaultTimeout:30];
    if ([LogFileManager isLogConsole]) {
        binding.logXMLInOut = YES;
    }
    eHousekeepingService_GetMinibarCategoryList *request = [[eHousekeepingService_GetMinibarCategoryList alloc] init];
    
//    CountCategoryModelV2 *minibarCategoryLatest = [self loadCountCategoryLatestWithHotelId:hotelId AndCountServiceId:cMinibar];
    
    [request setIntUsrID:[NSNumber numberWithInteger:userId]];
    [request setIntHotelID:[NSNumber numberWithInteger:hotelId]];
//    if (minibarCategoryLatest.countCategoryLastModified) {
//        [request setStrLastModified:minibarCategoryLatest.countCategoryLastModified];
//    }
    
//    LogObject *logObj = [[LogObject alloc] init];
//    logObj.dateTime = [NSDate date];
//    logObj.log = [NSString stringWithFormat:@"<%i><getMinibarCategoryListWSWithUserId><intUsrID:%i intHotelID:%i lastModified:%@>",userId,userId,hotelId,minibarCategoryLatest.countCategoryLastModified];
//    LogFileManager *logManager = [[LogFileManager alloc] init];
//    [logManager save:logObj];
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetMinibarCategoryListUsingParameters:request];
    NSArray *responseBodyParts = response.bodyParts;
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[SOAPFault class]]) {
            continue;   
        }
        
        //Get Minibar Categories List
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetMinibarCategoryListResponse class]]) {
            
            eHousekeepingService_GetMinibarCategoryListResponse *body = (eHousekeepingService_GetMinibarCategoryListResponse *)bodyPart;
            
            if ([body.GetMinibarCategoryListResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]]) 
            {
//                [self deleteAllCountCategory];
                [self deleteAllCountCategoryByServiceId:cMinibar];
                NSMutableArray *arrayMinibarCategoryWS = body.GetMinibarCategoryListResult.MinibarCat.MinibarCategoryList;
                for (eHousekeepingService_MinibarCategoryList *minibarCategoryWS in arrayMinibarCategoryWS) {
                    
                    //Get Minibar Categories from database
                    CountCategoryModelV2 *minibarCategoryModel = [[CountCategoryModelV2 alloc] init];
                    minibarCategoryModel.countCategoryId = [minibarCategoryWS.mbcID integerValue];
                    minibarCategoryModel.countServiceId = cMinibar;
                    
                    
                    [self loadCountCategoryByCountServiceIdAndCategoryIdData:minibarCategoryModel];
                    
                    if (minibarCategoryModel.countHotelId != nil) {
                        
                        if (![minibarCategoryModel.countCategoryLastModified isEqualToString:minibarCategoryWS.mbcLastModified]) {
                            //Update Minibar Category
                            minibarCategoryModel.countCategoryName = minibarCategoryWS.mbcName;
                            minibarCategoryModel.countCategoryNameLang = minibarCategoryWS.mbcLang;
//                            if (minibarCategoryWS.mbcPicture == nil) {
//                                minibarCategoryModel.countCategoryImage = UIImagePNGRepresentation([UIImage imageNamed:IMAGE_NO_IMAGE_AVAILABLE]);
//                            }
//                            else{
                            minibarCategoryModel.countCategoryImage = minibarCategoryWS.mbcPicture;
//                            }
                            minibarCategoryModel.countCategoryLastModified = minibarCategoryWS.mbcLastModified;
                            minibarCategoryModel.countServiceId = cMinibar;
                            minibarCategoryModel.countHotelId = [NSString stringWithFormat:@"%d", [[[UserManagerV2 sharedUserManager] currentUser]userHotelsId]];
                            [self updateCountCategory:minibarCategoryModel];
                            
                        }
                    }
                    else
                    {
                        //Insert Minibar Category
                        CountCategoryModelV2 *minibarCategoryDB = [[CountCategoryModelV2 alloc] init];
                        minibarCategoryDB.countCategoryId = [minibarCategoryWS.mbcID integerValue];
                        minibarCategoryDB.countCategoryName = minibarCategoryWS.mbcName;
                        minibarCategoryDB.countCategoryNameLang = minibarCategoryWS.mbcLang;
//                        if (minibarCategoryWS.mbcPicture == nil) {
//                            minibarCategoryDB.countCategoryImage = UIImagePNGRepresentation([UIImage imageNamed:IMAGE_NO_IMAGE_AVAILABLE]);
                            
//                            [self hardcodeCategoryMinibarWSWithLinenCateogryModel:minibarCategoryDB andLinenCategoryWSModel:minibarCategoryWS];
                            
//                        }
//                        else{
                        minibarCategoryDB.countCategoryImage = minibarCategoryWS.mbcPicture;
//                        }
                        minibarCategoryDB.countCategoryLastModified = minibarCategoryWS.mbcLastModified;
                        minibarCategoryDB.countServiceId = cMinibar;
                        minibarCategoryDB.countHotelId = [NSString stringWithFormat:@"%d", [[[UserManagerV2 sharedUserManager] currentUser]userHotelsId]];
                        [self insertCountCategory:minibarCategoryDB];                        
                    }
                    
                }
                returnCode = YES;
            }
            else
            {
                returnCode = NO;
            }
        }
    }
    return returnCode;
    
}

-(void) hardcodeMinibarItem:(CountItemsModelV2 *) minibar WithMinibarItemWS:(eHousekeepingService_MinibarItemList *) minibarWS {
//    if ([minibarWS.mbiName isEqual:@"Mineral Water"]) {
//        minibar.countItemImage = UIImagePNGRepresentation([UIImage imageNamed:@"minibar_01.png"]);
//    }
//    
//    if ([minibarWS.mbiName isEqual:@"Sparking Water"]) {
//        minibar.countItemImage = UIImagePNGRepresentation([UIImage imageNamed:@"minibar_02.png"]);
//    }
//    
//    if ([minibarWS.mbiName isEqual:@"Coke"]) {
//        minibar.countItemImage = UIImagePNGRepresentation([UIImage imageNamed:@"minibar_03.png"]);
//    }
//    
//    if ([minibarWS.mbiName isEqual:@"Coke Light"]) {
//        minibar.countItemImage = UIImagePNGRepresentation([UIImage imageNamed:@"minibar_04.png"]);
//    }
//    
//    if ([minibarWS.mbiName isEqual:@"Sprite"]) {
//        minibar.countItemImage = UIImagePNGRepresentation([UIImage imageNamed:@"minibar_05.png"]);
//    }
//    
//    if ([minibarWS.mbiName isEqual:@"Local Beer"]) {
//        minibar.countItemImage = UIImagePNGRepresentation([UIImage imageNamed:@"minibar_06.png"]);
//    }
//    
//    if ([minibarWS.mbiName isEqual:@"Imported Beer"]) {
//        minibar.countItemImage = UIImagePNGRepresentation([UIImage imageNamed:@"minibar_07.png"]);
//    }
//    
//    if ([minibarWS.mbiName isEqual:@"Orange Juice"]) {
//        minibar.countItemImage = UIImagePNGRepresentation([UIImage imageNamed:@"minibar_08.png"]);
//    }
//    
//    if ([minibarWS.mbiName isEqual:@"Apple Juice"]) {
//        minibar.countItemImage = UIImagePNGRepresentation([UIImage imageNamed:@"apple_juice.png"]);
//    }
//    
//    if ([minibarWS.mbiName isEqual:@"Red Wine (250ml)"]) {
//        minibar.countItemImage = UIImagePNGRepresentation([UIImage imageNamed:@"RedWine.png"]);
//    }
//    
//    if ([minibarWS.mbiName isEqual:@"White Wine (250ml)"]) {
//        minibar.countItemImage = UIImagePNGRepresentation([UIImage imageNamed:@"white_wine.png"]);
//    }
//    
//    if ([minibarWS.mbiName isEqual:@"Nuts"]) {
//        minibar.countItemImage = UIImagePNGRepresentation([UIImage imageNamed:@"nuts.png"]);
//    }
//    
//    if ([minibarWS.mbiName isEqual:@"Chocolate Bar"]) {
//        minibar.countItemImage = UIImagePNGRepresentation([UIImage imageNamed:@"chocolate_pic.png"]);
//    }
//    
//    if ([minibarWS.mbiName isEqual:@"Cup Noodle"]) {
//        minibar.countItemImage = UIImagePNGRepresentation([UIImage imageNamed:@"cup_noodles.png"]);
//    }
    
}

-(BOOL) getMinibarItemListWSWithUserId:(NSInteger)userId AndHotelId:(NSInteger)hotelId
{
    BOOL returnCode = NO;
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
//    [binding setDefaultTimeout:30];
    
    if ([LogFileManager isLogConsole]) {
        binding.logXMLInOut = YES;
    }
    
    eHousekeepingService_GetMinibarItemList *request = [[eHousekeepingService_GetMinibarItemList alloc] init];
    
    
//    CountItemsModelV2 *minibarItemsLatest = [self loadCountItemsLatestWithHotelId:hotelId AndServiceId:cMinibar];

    
    [request setIntUsrID:[NSNumber numberWithInteger:userId]];
    [request setIntHotelID:[NSNumber numberWithInteger:hotelId]];
//    if (minibarItemsLatest.countItemLastModified) {
//        [request setStrLastModified:minibarItemsLatest.countItemLastModified];
//    }
    
//    LogObject *logObj = [[LogObject alloc] init];
//    logObj.dateTime = [NSDate date];
//    logObj.log = [NSString stringWithFormat:@"<%i><getMinibarItemListWSWithUserId><intUsrID:%i intHotelID:%i lastModified:%@>",userId,userId,hotelId,minibarItemsLatest.countItemLastModified];
//    LogFileManager *logManager = [[LogFileManager alloc] init];
//    [logManager save:logObj];
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetMinibarItemListUsingParameters:request];
    NSArray *responseBodyParts = response.bodyParts;
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[SOAPFault class]]) {
            continue;   
        }
        
        //Get Minibar Item List
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetMinibarItemListResponse class]]) {
            
            eHousekeepingService_GetMinibarItemListResponse *body = (eHousekeepingService_GetMinibarItemListResponse *)bodyPart;
            
            if ([body.GetMinibarItemListResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]]) 
            {
                [self deleteAllItemsWithCountType:cMinibar];
                NSMutableArray *arrayMinibarItemWS = body.GetMinibarItemListResult.MinibarItm.MinibarItemList;
                for (eHousekeepingService_MinibarItemList *minibarItemWS in arrayMinibarItemWS) {
                    
                    //Get Minibar Item from database
                    CountItemsModelV2 *minibarItemModel = [[CountItemsModelV2 alloc] init];
                    minibarItemModel.countItemId = [minibarItemWS.mbiID integerValue];
                    minibarItemModel.countCategoryId = [minibarItemWS.mbiCategoryID integerValue];
                    
                    [self loadCountItemsByCountCategoryIdAndCountItemId:minibarItemModel];
                    
                    if (minibarItemModel.countItemName != nil) {
                        
                        if (![minibarItemModel.countItemLastModified isEqualToString:minibarItemWS.mbiLastModified]) {
                            //Update Minibar item
                            minibarItemModel.countItemName = minibarItemWS.mbiName;
                            minibarItemModel.countItemLang = minibarItemWS.mbiLang;
                            if (minibarItemWS.mbiPicture == nil) {
                                minibarItemModel.countItemImage = UIImagePNGRepresentation([UIImage imageNamed:IMAGE_NO_IMAGE_AVAILABLE]);
                            }
                            else {
                                minibarItemModel.countItemImage = minibarItemWS.mbiPicture;
                            }
                            minibarItemModel.countItemType = cMinibar;
                            minibarItemModel.countItemLastModified = minibarItemWS.mbiLastModified;
                            minibarItemModel.countCategoryId = [minibarItemWS.mbiCategoryID integerValue];
                            [self updateCountItem:minibarItemModel];
                        }
                    }
                    else
                    {
                        //Insert Minibar item
                        CountItemsModelV2 *countItemDB = [[CountItemsModelV2 alloc] init];
                        countItemDB.countItemId = [minibarItemWS.mbiID integerValue];
                        countItemDB.countItemName = minibarItemWS.mbiName;
                        countItemDB.countItemLang = minibarItemWS.mbiLang;
                        countItemDB.countCategoryId = [minibarItemWS.mbiCategoryID integerValue];
                        if (minibarItemWS.mbiPicture == nil) {
//                            countItemDB.countItemImage = UIImagePNGRepresentation([UIImage imageNamed:IMAGE_NO_IMAGE_AVAILABLE]);
                            [self hardcodeMinibarItem:countItemDB WithMinibarItemWS:minibarItemWS];
                        }
                        else{
                            countItemDB.countItemImage = minibarItemWS.mbiPicture;

                        }
                        countItemDB.countItemLastModified = minibarItemWS.mbiLastModified;
                        countItemDB.countItemType = cMinibar;
                        [self insertCountItem:countItemDB];
                    }
                    
                }
                returnCode = YES;
            }
            else
            {
                returnCode = NO;
            }
        }
    }
    return returnCode;

}

-(BOOL)postMinibarItemWSWithUserId:(NSInteger)userId roomAssignId:(NSInteger)roomAssignId AndMinibarOrderModel:(CountOrderDetailsModelV2 *)model WithOrderId:(NSInteger)orderId
{
    BOOL result = NO;
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    eHousekeepingService_PostMinibarItem *request = [[eHousekeepingService_PostMinibarItem alloc] init];
    
    if (model.countPostStatus == POST_STATUS_SAVED_UNPOSTED) {
        
        [request setIntUsrID:[NSNumber numberWithInteger:userId]];
        [request setIntRoomAssignID:[NSNumber numberWithInteger:roomAssignId]];
        [request setIntMinibarOrderID:[NSNumber numberWithInteger:orderId]];
        [request setIntMinibarItemID:[NSNumber numberWithInteger:model.countItemId]];
        [request setIntNewQuantity:[NSNumber numberWithInteger:model.countNew]];
        [request setIntUsedQuantity:[NSNumber numberWithInteger:model.countCollected]];
        [request setStrTransactionTime:model.countCollectedDate];
        
        LogObject *logObj = [[LogObject alloc] init];
        logObj.dateTime = [NSDate date];
        logObj.log = [NSString stringWithFormat:@"<%i><postMinibarItemWSWithUserId><intUsrID:%i intRoomAssignID:%i intMinibarOrderID:%i intMinibarItemID:%i intNewQuantity:%i intUsedQuantity:%i transactionTime:%@>",(int)userId,(int)userId,(int)roomAssignId,(int)orderId,(int)model.countItemId,(int)model.countNew,(int)model.countCollected,model.countCollectedDate];
        LogFileManager *logManager = [[LogFileManager alloc] init];
        [logManager save:logObj];
        
        eHousekeepingServiceSoap12BindingResponse *response = [binding PostMinibarItemUsingParameters:request];
        
        NSArray *responseBodyParts = response.bodyParts;
        
        for (id bodyPart in responseBodyParts) {
            if ([bodyPart isKindOfClass:[eHousekeepingService_PostMinibarItemResponse class]]) {
                
                eHousekeepingService_PostMinibarItemResponse *dataBody = (eHousekeepingService_PostMinibarItemResponse *)bodyPart;
                
                if (dataBody != nil) {
                    
                    if ([dataBody.PostMinibarItemResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_NEW_RECORD_ADDED]]) {
                        //change post status in minibar
                        result = YES;
                        if (model.countPostStatus == POST_STATUS_SAVED_UNPOSTED) {
                            model.countPostStatus = POST_STATUS_POSTED;
                            [self updateCountOrderDetail:model];
                        }
                        
                    } 
                    else {
                        result = NO;
                    }
                }
            }
        }
        
    }
    else {
        
        return YES;
    }
    
    return result;

}
-(NSInteger)postMinibarOrderWSWithUserId:(NSInteger)userId roomNo:(NSString*)roomNo roomAssignId:(NSInteger)roomAssignId totalQuantity:(NSInteger)totalQuantity serviceCharge:(NSInteger)serviceCharge AndSubTotal:(NSInteger)subTotal WithDate:(NSString *)date
{
    NSInteger result = 0;
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole])
    {
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_PostMinibarOrder *request = [[eHousekeepingService_PostMinibarOrder alloc] init];
    
    [request setIntUsrID:[NSNumber numberWithInteger:userId]];
    [request setIntRoomAssignID:[NSNumber numberWithInteger:roomAssignId]];
    [request setIntTotalQuantity:[NSNumber numberWithInteger:totalQuantity]];
    [request setDblServiceCharge:[NSNumber numberWithInteger:serviceCharge]];
    [request setDblSubTotal:[NSNumber numberWithInteger:subTotal]];
    [request setStrTransactionTime:date];
    [request setIntHotelID:[NSNumber numberWithInteger:[[[UserManagerV2 sharedUserManager] currentUser] userHotelsId]]];
    [request setStrRoomNo:roomNo];
    
    LogObject *logObj = [[LogObject alloc] init];
    logObj.dateTime = [NSDate date];
    logObj.log = [NSString stringWithFormat:@"<%i><postMinibarOrderWSWithUserId><intUsrID:%i intRoomAssignID:%i intTotalQuantity:%i dblServiceCharge:%i dblSubTotal:%i transactionTime:%@ intHotelID:%i roomNo:%@>",(int)userId,(int)userId,(int)roomAssignId,(int)totalQuantity,(int)serviceCharge,(int)subTotal,date,[[[UserManagerV2 sharedUserManager] currentUser] userHotelsId],[RoomManagerV2 getCurrentRoomNo]];
    LogFileManager *logManager = [[LogFileManager alloc] init];
    [logManager save:logObj];
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding PostMinibarOrderUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_PostMinibarOrderResponse class]]) {
            
            eHousekeepingService_PostMinibarOrderResponse *dataBody = (eHousekeepingService_PostMinibarOrderResponse *)bodyPart;
            
            if (dataBody != nil) {
                
                if ([dataBody.PostMinibarOrderResult.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_NEW_RECORD_ADDED]]) {
                    //get orderId from WS
                    NSInteger orderIdWS = [dataBody.PostMinibarOrderResult.respMsg integerValue];
                    
                    result = orderIdWS;
                }
            }
        }
    }
    
    return result;
}
-(NSInteger)postMinibarOrderWSWithUserId:(NSInteger)userId roomAssignId:(NSInteger)roomAssignId totalQuantity:(NSInteger)totalQuantity serviceCharge:(NSInteger)serviceCharge AndSubTotal:(NSInteger)subTotal WithDate:(NSString *)date
{
    NSInteger result = 0;
    NSString *roomNo = [[NSString alloc] initWithString:[RoomManagerV2 getCurrentRoomNo]];
    if (roomNo == nil || roomNo.length == 0 || [roomNo isEqualToString:@""]) {
        NSString *message = nil;
        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
            message = [NSString stringWithFormat:@"%@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOM_INVALID]];
        } else {
            message = [NSString stringWithFormat:@"\n\n\n\n\n%@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOM_INVALID]];
        }
        
        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObject:[UIImage imageNamed:@"yes_bt.png"]] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:message delegate:nil cancelButtonTitle:[L_CLOSE currentKeyToLanguage] otherButtonTitles: nil];
        
        if(!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
            UIImageView *alertImage = [[UIImageView alloc] initWithFrame:CGRectMake(80, 35, 120, 120)];
            [alertImage setImage:[UIImage imageNamed:@"icon_wrong room.png"]];
            [alert addSubview:alertImage];
        }
        [alert show];
        return result;
    }
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole])
    {
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_PostMinibarOrder *request = [[eHousekeepingService_PostMinibarOrder alloc] init];
    
    [request setIntUsrID:[NSNumber numberWithInteger:userId]];
    [request setIntRoomAssignID:[NSNumber numberWithInteger:roomAssignId]];
    [request setIntTotalQuantity:[NSNumber numberWithInteger:totalQuantity]];
    [request setDblServiceCharge:[NSNumber numberWithInteger:serviceCharge]];
    [request setDblSubTotal:[NSNumber numberWithInteger:subTotal]];
    [request setStrTransactionTime:date];
    [request setIntHotelID:[NSNumber numberWithInteger:[[[UserManagerV2 sharedUserManager] currentUser] userHotelsId]]];
    [request setStrRoomNo:roomNo ];
    
    LogObject *logObj = [[LogObject alloc] init];
    logObj.dateTime = [NSDate date];
    logObj.log = [NSString stringWithFormat:@"<%i><postMinibarOrderWSWithUserId><intUsrID:%i intRoomAssignID:%i intTotalQuantity:%i dblServiceCharge:%i dblSubTotal:%i transactionTime:%@ intHotelID:%i roomNo:%@>",(int)userId,(int)userId,(int)roomAssignId,(int)totalQuantity,(int)serviceCharge,(int)subTotal,date,[[[UserManagerV2 sharedUserManager] currentUser] userHotelsId],[RoomManagerV2 getCurrentRoomNo]];
    LogFileManager *logManager = [[LogFileManager alloc] init];
    [logManager save:logObj];
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding PostMinibarOrderUsingParameters:request];
        
    NSArray *responseBodyParts = response.bodyParts;
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_PostMinibarOrderResponse class]]) {
            
            eHousekeepingService_PostMinibarOrderResponse *dataBody = (eHousekeepingService_PostMinibarOrderResponse *)bodyPart;
            
            if (dataBody != nil) {
                
                if ([dataBody.PostMinibarOrderResult.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_NEW_RECORD_ADDED]]) {
                    //get orderId from WS
                    NSInteger orderIdWS = [dataBody.PostMinibarOrderResult.respMsg integerValue];
                    
                    result = orderIdWS;
                } 
            }
        }
    }

    return result;
}

//-------------------update code-----------------------------------------------------------
-(NSInteger) postMinibarOrderWSWithUserId:(NSInteger) userId roomAssignId:(NSInteger) roomAssignId totalQuantity:(NSInteger) totalQuantity serviceCharge:(NSInteger) serviceCharge AndSubTotal:(NSInteger) subTotal WithDate:(NSString*) date WithRoomNo: (NSString*) roomNo{
    
    NSInteger result = 0;
    if (roomNo == nil || roomNo.length == 0 || [roomNo isEqualToString:@""]) {
        NSString *message = nil;
        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
            message = [NSString stringWithFormat:@"%@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOM_INVALID]];
        } else {
            message = [NSString stringWithFormat:@"\n\n\n\n\n%@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOM_INVALID]];
        }
        
        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObject:[UIImage imageNamed:@"yes_bt.png"]] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:message delegate:nil cancelButtonTitle:[L_CLOSE currentKeyToLanguage] otherButtonTitles: nil];
        
        if(!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
            UIImageView *alertImage = [[UIImageView alloc] initWithFrame:CGRectMake(80, 35, 120, 120)];
            [alertImage setImage:[UIImage imageNamed:@"icon_wrong room.png"]];
            [alert addSubview:alertImage];
        }
        [alert show];
        return result;
    }
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole])
    {
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_PostMinibarOrder *request = [[eHousekeepingService_PostMinibarOrder alloc] init];
    
    [request setIntUsrID:[NSNumber numberWithInteger:userId]];
    [request setIntRoomAssignID:[NSNumber numberWithInteger:roomAssignId]];
    [request setIntTotalQuantity:[NSNumber numberWithInteger:totalQuantity]];
    [request setDblServiceCharge:[NSNumber numberWithInteger:serviceCharge]];
    [request setDblSubTotal:[NSNumber numberWithInteger:subTotal]];
    [request setStrTransactionTime:date];
    [request setIntHotelID:[NSNumber numberWithInteger:[[[UserManagerV2 sharedUserManager] currentUser] userHotelsId]]];
    [request setStrRoomNo:roomNo];
    
    LogObject *logObj = [[LogObject alloc] init];
    logObj.dateTime = [NSDate date];
    logObj.log = [NSString stringWithFormat:@"<%i><postMinibarOrderWSWithUserId><intUsrID:%i intRoomAssignID:%i intTotalQuantity:%i dblServiceCharge:%i dblSubTotal:%i transactionTime:%@ intHotelID:%i roomNo:%@>",(int)userId,(int)userId,(int)roomAssignId,(int)totalQuantity,(int)serviceCharge,(int)subTotal,date,[[[UserManagerV2 sharedUserManager] currentUser] userHotelsId],[RoomManagerV2 getCurrentRoomNo]];
    LogFileManager *logManager = [[LogFileManager alloc] init];
    [logManager save:logObj];
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding PostMinibarOrderUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_PostMinibarOrderResponse class]]) {
            
            eHousekeepingService_PostMinibarOrderResponse *dataBody = (eHousekeepingService_PostMinibarOrderResponse *)bodyPart;
            
            if (dataBody != nil) {
                
                if ([dataBody.PostMinibarOrderResult.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_NEW_RECORD_ADDED]]) {
                    //get orderId from WS
                    NSInteger orderIdWS = [dataBody.PostMinibarOrderResult.respMsg integerValue];
                    
                    result = orderIdWS;
                }
            }
        }
    }
    
    return result;

}
//-------------------update code-----------------------------------------------------------------------

//------------------add---------------
- (BOOL) postWSLog:(NSInteger)userId Message:(NSString *)messageValue MessageType:(NSInteger)type{
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole])
    {
        [binding setLogXMLInOut:YES];
    }
    eHousekeepingService_PostWSLog *request = [[eHousekeepingService_PostWSLog alloc] init];
    request.intUsrID = [NSNumber numberWithInteger:userId];
    request.strMessage = messageValue;
    request.intMessageType = [NSNumber numberWithInteger:type];
    eHousekeepingServiceSoap12BindingResponse *respone = [binding PostWSLogUsingParameters:request];
    BOOL isPostSuccess = NO;
    for (id bodyPart in respone.bodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_PostWSLogResponse class]]) {
            eHousekeepingService_PostWSLogResponse *dataBody = (eHousekeepingService_PostWSLogResponse *)bodyPart;
            if (dataBody != nil) {
                if ([dataBody.PostWSLogResult.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]]) {
                    isPostSuccess = YES;
                }
            }
        }
    }
    return isPostSuccess;
}
//------------------add---------------
-(NSInteger)numberOfMustPostMinibarRecords:(NSInteger)userId{
    NSMutableArray *arrayMinibar = [self loadAllCountOrdersByUserId:userId AndServiceId:cMinibar];
    NSInteger countPostMinibar = 0;
    for (CountOrdersModelV2 *orderModel in arrayMinibar) {
        NSMutableArray *arrayPostMinibar = [self loadAllCountOrderDetailByOrderId:orderModel.countOrderId];
        countPostMinibar += arrayPostMinibar.count;
    }
    return countPostMinibar;
}

-(NSArray *)postHistoryItemsWithUserId:(NSInteger)userId AndMinibarOrderModelList:(NSArray *)modelList roomNumber:(NSString*)roomNumber{
    NSMutableArray *postedMinibarItem = nil;
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole])
    {
        [binding setLogXMLInOut:YES];
    }
    eHousekeepingService_PostMinibarItems *request = [[eHousekeepingService_PostMinibarItems alloc] init];
    
//    [request setIntUserID:[NSNumber numberWithInteger:userId]];
//    [request setIntRoomAssignID:[NSNumber numberWithInteger:roomAssignId]];
//    [request setIntMinibarOrderID:[NSNumber numberWithInteger:orderId]];
    
    
    eHousekeepingService_ArrayOfPostItem *postedItem = [[eHousekeepingService_ArrayOfPostItem alloc] init];
    NSMutableArray *listHistoryPosting = [NSMutableArray array];
    // DungPhan - 20150908: Enable post Posting History for Minibar by Access Rights
    
    for (CountOrderDetailsModelV2 *model in modelList) {
        
        eHousekeepingService_PostingHistoryDetails *historyDetail = [[eHousekeepingService_PostingHistoryDetails alloc] init];
        [historyDetail setUserID:[NSString stringWithFormat:@"%d", (int)userId]];
        [historyDetail setRoomNo:roomNumber];
        [historyDetail setHotelID:[NSString stringWithFormat:@"%d", [UserManagerV2 sharedUserManager].currentUser.userHotelsId]];
        [historyDetail setModuleID:[NSString stringWithFormat:@"%d", (int)ModuleHistory_Physical]];
        [historyDetail setNewQuantity:[NSString stringWithFormat:@"%d", (int)model.countNew]];
        [historyDetail setUsedQuantity:@""];
        [historyDetail setTransactionDateTime:model.countCollectedDate];
        [listHistoryPosting addObject:historyDetail];
        
    }
    
    [request setPostItems:postedItem];
    eHousekeepingServiceSoap12BindingResponse *response = [binding PostMinibarItemsUsingParameters:request];
    for (id bodyPart in response.bodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_PostMinibarItemsResponse class]]) {
            eHousekeepingService_PostMinibarItemsResponse *data = (eHousekeepingService_PostMinibarItemsResponse*)bodyPart;
            if (data != nil) {
                
                postedMinibarItem = [[NSMutableArray alloc] init];
                for (eHousekeepingService_PostedItem *postedItem in data.PostMinibarItemsResult.MinibarItems.PostedItem) {
                    [postedMinibarItem addObject:postedItem.ID_];
                }
                
                [self postHistoryPostingWS:listHistoryPosting];
            }
        }
    }
    
    return postedMinibarItem;
}
-(NSArray *)postAllMinibarItemsWithUserId:(NSInteger)userId roomAssignId:(NSInteger)roomAssignId AndMinibarOrderModelList:(NSArray *)modelList WithOrderId:(NSInteger) orderId roomNumber:(NSString*)roomNumber isEnablePostingHistory:(BOOL)isEnablePostingHistory
{
    NSMutableArray *postedMinibarItem = nil;
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole])
    {
        [binding setLogXMLInOut:YES];
    }
    eHousekeepingService_PostMinibarItems *request = [[eHousekeepingService_PostMinibarItems alloc] init];
    
    [request setIntUserID:[NSNumber numberWithInteger:userId]];
    [request setIntRoomAssignID:[NSNumber numberWithInteger:roomAssignId]];
    [request setIntMinibarOrderID:[NSNumber numberWithInteger:orderId]];
    
    
    NSMutableString *postedItemsLog = [[NSMutableString alloc] init];
    NSNumber *isEnableLog = [[NSUserDefaults standardUserDefaults] objectForKey:ENABLE_LOG];
    BOOL enabledLog = NO;
    if(isEnableLog != nil && [isEnableLog intValue] == 1){
        enabledLog = YES;
    }
    
    eHousekeepingService_ArrayOfPostItem *postedItem = [[eHousekeepingService_ArrayOfPostItem alloc] init];
    NSMutableArray *listHistoryPosting = [NSMutableArray array];
    // DungPhan - 20150908: Enable post Posting History for Minibar by Access Rights
    
    BOOL isEnglishLanguage = [[LanguageManagerV2 sharedLanguageManager] currentLanguageIsEqualTo:@"%@", ENGLISH_LANGUAGE];
    
    for (CountOrderDetailsModelV2 *model in modelList) {
        eHousekeepingService_PostItem *postItem = [[eHousekeepingService_PostItem alloc] init];
        [postItem setIntItemID:[NSNumber numberWithInteger:model.countItemId]];
        [postItem setIntNewQuantity:[NSNumber numberWithInteger:model.countNew]];
        [postItem setIntUsedQuantity:[NSNumber numberWithInteger:model.countCollected]];
        [postItem setStrTransactionTime:model.countCollectedDate];
        [postedItem addPostItem:postItem];
        
        //if (isEnablePostingHistory) {
            //History Posting
            eHousekeepingService_PostingHistoryDetails *historyDetail = [[eHousekeepingService_PostingHistoryDetails alloc] init];
            [historyDetail setUserID:[NSString stringWithFormat:@"%d", (int)userId]];
            [historyDetail setRoomNo:roomNumber];
            [historyDetail setHotelID:[NSString stringWithFormat:@"%d", [UserManagerV2 sharedUserManager].currentUser.userHotelsId]];
            [historyDetail setModuleID:[NSString stringWithFormat:@"%d", (int)ModuleHistory_Minibar]];
            [historyDetail setNewQuantity:[NSString stringWithFormat:@"%d", (int)model.countNew]];
            [historyDetail setUsedQuantity:[NSString stringWithFormat:@"%d", (int)model.countCollected]];
            [historyDetail setTransactionDateTime:model.countCollectedDate];
            
            CountItemsModelV2 *countItem = [[CountItemsModelV2 alloc] init];
            countItem.countItemId = model.countItemId;
            countItem = [self loadCountItem:countItem];
            
            CountCategoryModelV2 *countCategory = [[CountCategoryModelV2 alloc] init];
            countCategory.countCategoryId = countItem.countCategoryId;
            countCategory = [self loadCountCategory:countCategory];
            
            if (isEnglishLanguage) {
                [historyDetail setItemDescription:countItem.countItemName];
                [historyDetail setCategoryDescription:countCategory.countCategoryName];
            } else {
                [historyDetail setItemDescription:countItem.countItemLang];
                [historyDetail setCategoryDescription:countCategory.countCategoryNameLang];
            }
            
            [listHistoryPosting addObject:historyDetail];
        //}
        
        if(enabledLog){
            [postedItemsLog appendFormat:@"\nintMinibarItemID:%i intNewQuantity:%i intUsedQuantity:%i transactionTime%@",(int)model.countItemId,(int)model.countNew,(int)model.countCollected,model.countCollectedDate];
        }
    }
    
    if (enabledLog) {
        LogObject *logObj = [[LogObject alloc] init];
        logObj.dateTime = [NSDate date];
        logObj.log = [NSString stringWithFormat:@"<%i><postAllMinibarItemsWithUserId><intUsrID:%i intRoomAssignID:%i intMinibarOrderID:%i%@>",(int)userId,(int)userId,(int)roomAssignId,(int)orderId,postedItemsLog];
        LogFileManager *logManager = [[LogFileManager alloc] init];
        [logManager save:logObj];
    }
    
    [request setPostItems:postedItem];
    eHousekeepingServiceSoap12BindingResponse *response = [binding PostMinibarItemsUsingParameters:request];
    for (id bodyPart in response.bodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_PostMinibarItemsResponse class]]) {
            eHousekeepingService_PostMinibarItemsResponse *data = (eHousekeepingService_PostMinibarItemsResponse*)bodyPart;
            if (data != nil) {
                
                postedMinibarItem = [[NSMutableArray alloc] init];
                for (eHousekeepingService_PostedItem *postedItem in data.PostMinibarItemsResult.MinibarItems.PostedItem) {
                    [postedMinibarItem addObject:postedItem.ID_];
                }
                
                //CRF-1229: Posting history
                //if (isEnablePostingHistory) {
                    [self postHistoryPostingWS:listHistoryPosting];
                //}
            }
        }
    }
    
    return postedMinibarItem;
}
-(NSArray *)postAllMinibarItemsWithUserId:(NSInteger)userId roomAssignId:(NSInteger)roomAssignId AndMinibarOrderModelList:(NSArray *)modelList WithOrderId:(NSInteger) orderId roomNumber:(NSString*)roomNumber
{
    BOOL isEnablePostingHistory = ([[UserManagerV2 sharedUserManager].currentUserAccessRight isFindActionHistoryActive] || [[UserManagerV2 sharedUserManager].currentUserAccessRight isFindActionHistoryAllowedView] || [[UserManagerV2 sharedUserManager].currentUserAccessRight isRoomDetailsActionHistoryActive] || [[UserManagerV2 sharedUserManager].currentUserAccessRight isRoomDetailsActionHistoryAllowedView]);
    return [self postAllMinibarItemsWithUserId:userId roomAssignId:roomAssignId AndMinibarOrderModelList:modelList WithOrderId:orderId roomNumber:roomNumber isEnablePostingHistory:isEnablePostingHistory];
}

#pragma mark - History Posting

-(NSInteger) postHistoryPostingWS:(NSMutableArray*)listHistoryPosting
{
    if (listHistoryPosting.count <= 0) {
        return RESPONSE_STATUS_ERROR;
    }
    
    NSInteger responseCode = RESPONSE_STATUS_OK;
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole]) {
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_PostPostingHistoryRoutine *request = [[eHousekeepingService_PostPostingHistoryRoutine alloc] init];
    eHousekeepingService_ArrayOfPostingHistoryDetails *arrayPosting = [[eHousekeepingService_ArrayOfPostingHistoryDetails alloc] init];
    [arrayPosting.PostingHistoryDetails addObjectsFromArray:listHistoryPosting];
    [request setPostingHistoryListing:arrayPosting];
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding PostPostingHistoryRoutineUsingParameters:request];
    for (id bodyPart in response.bodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_PostPostingHistoryRoutineResponse class]]) {
            eHousekeepingService_PostPostingHistoryRoutineResponse *data = (eHousekeepingService_PostPostingHistoryRoutineResponse*)bodyPart;
            if (data != nil) {
                responseCode = [data.PostPostingHistoryRoutineResult.ResponseStatus.respCode integerValue];
            }
        }
    }
    
    return responseCode;
}

-(NSMutableArray*) getPostingHistoryWSByUserId:(int) userId roomNumber:(NSString*)roomNumber hotelId:(int)hotelId moduleId:(int)moduleId dateFilter:(NSString*)dateFilter
{
    
    NSMutableArray *results;
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole]) {
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_GetPostingHistoryRoutine *request = [[eHousekeepingService_GetPostingHistoryRoutine alloc] init];
    [request setUserID:[NSString stringWithFormat:@"%d", userId]];
    [request setRoomNo:roomNumber];
    [request setHTID:[NSString stringWithFormat:@"%d", hotelId]];
    [request setModuleID:[NSString stringWithFormat:@"%d", moduleId]];
    [request setDateFilter:dateFilter];
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetPostingHistoryRoutineUsingParameters:request];
    for (id bodyPart in response.bodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetPostingHistoryRoutineResponse class]]) {
            eHousekeepingService_GetPostingHistoryRoutineResponse *data = (eHousekeepingService_GetPostingHistoryRoutineResponse*)bodyPart;
            
            [data.GetPostingHistoryRoutineResult.ResponseStatus.respCode integerValue];
            results = data.GetPostingHistoryRoutineResult.PostingHistoryListing.PostingHistoryDetails;
        }
    }
    
    return results;
}

#pragma mark - common count order
-(int)postAllPendingCountDataByUserId:(int)userId serviceId:(int)serviceId
{
    int result = 0;
    CountManagerV2 *manager = [[CountManagerV2 alloc] init];
    SyncManagerV2 *syncManager = [[SyncManagerV2 alloc] init];
    
    NSMutableArray *listCountOrderPending = [self loadAllCountOrdersByUserId:userId AndServiceId:serviceId];
    
    for (CountOrdersModelV2 *curCountOrder in listCountOrderPending) {
        
        if(curCountOrder.countPostStatus == POST_STATUS_UN_CHANGED){
            continue;
        }
        
        NSMutableArray *listCountDetails = [self loadAllCountOrderDetailByOrderId:curCountOrder.countOrderId];
        NSInteger totalQuantity = 0;
        
        //save minibar or linen detail
        NSMutableArray *postItems = [[NSMutableArray alloc] init];
        for (CountOrderDetailsModelV2 *curDetail in listCountDetails) {
            if (curDetail.countCollected != 0 || curDetail.countNew != 0) {
                totalQuantity += curDetail.countCollected;
            }
            [postItems addObject:curDetail];
        }
        
        //Only post order for minibar
        if(curCountOrder.countOrderWSId <= 0 && curCountOrder.countServiceId == cMinibar) {
            //curCountOrder.countOrderWSId = [syncManager postMinibarOrderWithUserId:userId roomAssignId:curCountOrder.countRoomAssignId totalQuantity:totalQuantity serviceCharge:0 AndSubTotal:totalQuantity WithDate:curCountOrder.countCollectDate];
            
            curCountOrder.countOrderWSId = [syncManager postMinibarOrderWithUserId:userId roomAssignId:curCountOrder.countRoomAssignId totalQuantity:totalQuantity serviceCharge:0 AndSubTotal:totalQuantity WithDate:curCountOrder.countCollectDate WithRoomNo:curCountOrder.countRoomNumber];
            if(curCountOrder.countOrderWSId > 0) {
                curCountOrder.countPostStatus = POST_STATUS_POSTED;
                [manager updateCountOrder:curCountOrder];
                
            } else {
                [manager updateCountOrder:curCountOrder];
            }
        }
        
        
        //post list of PostItem to new WS for Minibar and Linen
        NSArray *postedItems = nil;
        if(curCountOrder.countServiceId == cMinibar) {
        
            postedItems = [[NSArray alloc] initWithArray:[syncManager postAllMinibarItemsWithUserId:userId roomAssignId:curCountOrder.countRoomAssignId AndMinibarOrderModelList:postItems WithOrderId:curCountOrder.countOrderWSId roomNumber:curCountOrder.countRoomNumber]];
        } else if(curCountOrder.countServiceId == cLinen) {
            
            postedItems = [[NSArray alloc] initWithArray:[syncManager postAllLinenItemsWithUserId:userId roomassignId:(int)curCountOrder.countRoomAssignId roomNumber:curCountOrder.countRoomNumber AndLinenOrderModelList:postItems]];
        }
        
        if ([postedItems count] > 0) {
            result = [postedItems count];
            CountHistoryManager *countHistoryManager = [[CountHistoryManager alloc]init];
            CountHistoryModel *countHistoryModel = [[CountHistoryModel alloc]init];
            
            for (CountOrderDetailsModelV2 *postedItem in postItems) {
                
                countHistoryModel.room_id = curCountOrder.countRoomNumber;
                countHistoryModel.count_item_id = postedItem.countItemId;
                countHistoryModel.count_collected = postedItem.countCollected;
                countHistoryModel.count_new = postedItem.countNew;
                countHistoryModel.count_service = curCountOrder.countServiceId;
                countHistoryModel.count_date = postedItem.countCollectedDate;
                countHistoryModel.count_user_id = userId;
                countHistoryModel.count_id = curCountOrder.countOrderId;
                [countHistoryManager insertCountHistoryData:countHistoryModel];
            }
            
            [self deleteCountOrderDetailsByOrderId:(int)curCountOrder.countOrderId];
            [self deleteCountOrderByOrderId:(int)curCountOrder.countOrderId];
        }
    }
    return result;
}

#pragma mark - Count Room Type

- (BOOL)insertRoomTypeInventoryModel:(RoomTypeInventoryModel *) model {
    RoomTypeInventoryAdapter *adapter = [[RoomTypeInventoryAdapter alloc] init];
    [adapter openDatabase];
    BOOL result = [adapter insertRoomTypeInventoryModel:model];
    [adapter close];
    return result;
}

- (NSInteger)deleteRoomTypeInventoryModelByServiceType:(NSInteger)serviceType userId:(NSInteger)userId {
    DataTable *tableDelete = [[DataTable alloc] initWithNameFormat:@"%@",ROOMTYPE_INVENTORY];
    
    NSString *queryDelete = [NSString stringWithFormat:@"DELETE FROM %@ WHERE %@ = %d AND %@ = %d", ROOMTYPE_INVENTORY, rti_service_item, (int)serviceType, rti_user_id, (int)userId];
    int status = (int)[tableDelete excuteQueryNonSelect:queryDelete];
    
    return status;
}

#pragma mark - Count WS for Inventory by Room Type

// CFG [20160921/CRF-00001275] - Changed API to read from new web services.
//- (BOOL)getRoomTypeInventoryWSByServiceType:(NSInteger)serviceId userId:(NSInteger)userId hotelId:(NSInteger)hotelId {
//    
//    BOOL returnCode = NO;
//    
//    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
//    if ([LogFileManager isLogConsole]) {
//        
//        [binding setLogXMLInOut:YES];
//    }
//    eHousekeepingService_GetRoomTypeInventoryRoutine *request = [[eHousekeepingService_GetRoomTypeInventoryRoutine alloc] init];
//    [request setUserID:[NSString stringWithFormat:@"%d", userId]];
//    [request setPropertyID:[NSString stringWithFormat:@"%d", hotelId]];
//    [request setServiceID:[NSString stringWithFormat:@"%d", serviceId]];
//    
//    eHousekeepingServiceSoap12BindingResponse *response = [binding GetRoomTypeInventoryRoutineUsingParameters:request];
//    NSArray *responseBodyParts = response.bodyParts;
//    
//    for (id bodyPart in responseBodyParts) {
//        if ([bodyPart isKindOfClass:[SOAPFault class]]) {
//            continue;
//        }
//        if ([bodyPart isKindOfClass:[eHousekeepingService_GetRoomTypeInventoryRoutineResponse class]]) {
//            
//            eHousekeepingService_GetRoomTypeInventoryRoutineResponse *body = (eHousekeepingService_GetRoomTypeInventoryRoutineResponse *)bodyPart;
//            
//            if ([body.GetRoomTypeInventoryRoutineResult.ResponseStatuses.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]]) {
//                
//                [self deleteRoomTypeInventoryModelByServiceType:serviceId userId:userId];
//                
//                NSMutableArray *arrayRoomTypeInventoryWS = body.GetRoomTypeInventoryRoutineResult.RoomTypeInventoryListing.RoomTypeInventoryDetails;
//                int userId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
//                int hotelId = [[[UserManagerV2 sharedUserManager] currentUser] userHotelsId];
//                for (eHousekeepingService_RoomTypeInventoryDetails *roomTypeInventoryWS in arrayRoomTypeInventoryWS) {
//                    
//                    RoomTypeInventoryModel *rtiDB = [[RoomTypeInventoryModel alloc] init];
//                    rtiDB.roomTypeId = [roomTypeInventoryWS.RoomTypeID integerValue];
//                    rtiDB.categoryId = [roomTypeInventoryWS.CategoryID integerValue];
//                    rtiDB.itemId = [roomTypeInventoryWS.InventoryID integerValue];
//                    rtiDB.userId = userId;
//                    rtiDB.hotelId = hotelId;
//                    rtiDB.sectionId = [roomTypeInventoryWS.SectionID integerValue];
//                    rtiDB.buildingId = [roomTypeInventoryWS.BuildingID integerValue];
//                    rtiDB.serviceItem = [roomTypeInventoryWS.ServiceItem integerValue];
//                    [self insertRoomTypeInventoryModel:rtiDB];
//                }
//                returnCode = YES;
//            } else {
//                returnCode = NO;
//            }
//        }
//    }
//    return returnCode;
//}

- (BOOL)getRoomTypeInventoryWSByServiceType:(NSInteger)serviceId userId:(NSInteger)userId hotelId:(NSInteger)hotelId {
    
    BOOL returnCode = NO;
    
    Web_x0020_Service_Web_x0020_ServiceSoap12Binding *binding = [Web_x0020_Service Web_x0020_ServiceSoap12Binding];
    if ([LogFileManager isLogConsole]) {
        
        [binding setLogXMLInOut:YES];
    }
    Web_x0020_Service_ElementGetRoomTypeInventoryRoutine *request = [[Web_x0020_Service_ElementGetRoomTypeInventoryRoutine alloc] init];
    [request setUserID:[NSString stringWithFormat:@"%d", (int)userId]];
    [request setPropertyID:[NSString stringWithFormat:@"%d", (int)hotelId]];
    [request setServiceID:[NSString stringWithFormat:@"%d", (int)serviceId]];
    
    Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *response = [binding GetRoomTypeInventoryRoutine:request];
    NSArray *responseBodyParts = response.bodyParts;
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[SOAPFault class]]) {
            continue;
        }
        if ([bodyPart isKindOfClass:[Web_x0020_Service_ElementGetRoomTypeInventoryRoutineResponse class]]) {
            
            Web_x0020_Service_ElementGetRoomTypeInventoryRoutineResponse *body = (Web_x0020_Service_ElementGetRoomTypeInventoryRoutineResponse *)bodyPart;
            
            if ([body.GetRoomTypeInventoryRoutineResult.ResponseStatuses.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]]) {
                
                [self deleteRoomTypeInventoryModelByServiceType:serviceId userId:userId];
                
                NSArray *lRoomTypeInventoryListing = body.GetRoomTypeInventoryRoutineResult.RoomTypeInventoryListing;
                
                for (Web_x0020_Service_RoomTypeInventoryDetails *lRoomTypeInventoryDetails in lRoomTypeInventoryListing) {

                    RoomTypeInventoryModel *rtiDB = [[RoomTypeInventoryModel alloc] init];
                    rtiDB.roomTypeId = [lRoomTypeInventoryDetails.RoomTypeID integerValue];
                    rtiDB.categoryId = [lRoomTypeInventoryDetails.CategoryID integerValue];
                    rtiDB.itemId = [lRoomTypeInventoryDetails.InventoryID integerValue];
                    rtiDB.userId = userId;
                    rtiDB.hotelId = hotelId;
                    rtiDB.sectionId = [lRoomTypeInventoryDetails.SectionID integerValue];
                    rtiDB.buildingId = [lRoomTypeInventoryDetails.BuildingID integerValue];
                    rtiDB.serviceItem = [lRoomTypeInventoryDetails.ServiceItem integerValue];
                    rtiDB.defaultQuantity = [lRoomTypeInventoryDetails.DefaultQuantity integerValue];
                    [self insertRoomTypeInventoryModel:rtiDB];
                    
                }

                returnCode = YES;
            } else {
                returnCode = NO;
            }
        }
    }
    return returnCode;
}
// CFG [20160921/CRF-00001275] - End.

@end
