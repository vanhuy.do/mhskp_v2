//
//  PhysicalCheckManagerV2.h
//  mHouseKeeping
//
//  Created by Quang Nguyen on 2/18/13.
//
//

#import <Foundation/Foundation.h>

@interface PhysicalCheckManagerV2 : NSObject

+ (PhysicalCheckManagerV2*) sharedPhysicalCheckManagerV2;

- (eHousekeepingService_PostPhysicalCheck*) postPhysicalCheckWithUserId:(NSInteger) userId andRoomNo:(NSString*)roomNo andHotelID:(NSInteger) hotelId andStatusId:(NSInteger) statusId andCleaningStatusId:(NSInteger)cleaningStatusId;
- (BOOL) postWSLog:(NSInteger)userId Message:(NSString *)messageValue MessageType:(NSInteger)type;

@end
