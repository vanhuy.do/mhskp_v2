//
//  PhysicalCheckManagerV2.m
//  mHouseKeeping
//
//  Created by Quang Nguyen on 2/18/13.
//
//

#import "PhysicalCheckManagerV2.h"
#import "LogFileManager.h"
#import "eHousekeepingService.h"
#import "ServiceDefines.h"
#import "LanguageManagerV2.h"
#import "ehkDefines.h"
#import "CustomAlertViewV2.h"
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

@implementation PhysicalCheckManagerV2
static PhysicalCheckManagerV2* sharedPhysicalCheckManagerV2Instance = nil;

+ (PhysicalCheckManagerV2*) sharedPhysicalCheckManagerV2
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedPhysicalCheckManagerV2Instance = [[PhysicalCheckManagerV2 alloc] init];
        
    });
    return sharedPhysicalCheckManagerV2Instance;
}

- (eHousekeepingService_PostPhysicalCheck*) postPhysicalCheckWithUserId:(NSInteger) userId andRoomNo:(NSString*)roomNo andHotelID:(NSInteger) hotelId andStatusId:(NSInteger) statusId andCleaningStatusId:(NSInteger)cleaningStatusId
{
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    eHousekeepingService_PostUpdateRoomDetailsRoutine *request = [[eHousekeepingService_PostUpdateRoomDetailsRoutine alloc] init];
    
    //Allow write log or not
    if([LogFileManager isLogConsole])
    {
        [binding setLogXMLInOut:YES];
    }
    
    [request setUserID:[NSString stringWithFormat:@"%d", (int)userId]];
    [request setRoomNo:roomNo];
    [request setPropertyID:[NSString stringWithFormat:@"%d", (int)hotelId]];
    [request setUpdateType:@"0"];
    [request setRoomStatusID:[NSString stringWithFormat:@"%d", (int)statusId]];
    [request setCleaningStatusID:[NSString stringWithFormat:@"%d", (int)cleaningStatusId]];
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding PostUpdateRoomDetailsRoutineUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    for (id bodyPart in responseBodyParts ) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_PostUpdateRoomDetailsRoutineResponse class]]) {
            eHousekeepingService_PostUpdateRoomDetailsRoutineResponse *dataBody = bodyPart;
            if (dataBody != nil) {
                if ([dataBody.PostUpdateRoomDetailsRoutineResult.ResponseStatuses.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_ERROR_IN_UPDATING_RECORD]]) {
                    NSString *message = nil;
                    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                        message = [NSString stringWithFormat:@"%@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_STATUS_CANT_BE_POSTED]];
                    } else {
                        message = [NSString stringWithFormat:@"\n\n\n\n\n%@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_STATUS_CANT_BE_POSTED]];
                    }
                    
                    UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObject:[UIImage imageNamed:@"yes_bt.png"]] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:message delegate:nil cancelButtonTitle:[L_CLOSE currentKeyToLanguage] otherButtonTitles: nil];
                    
                    if(!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                        UIImageView *alertImage = [[UIImageView alloc] initWithFrame:CGRectMake(80, 35, 120, 120)];
                        [alertImage setImage:[UIImage imageNamed:@"icon_wrong room.png"]];
                        [alert addSubview:alertImage];
                    }
                    [alert show];
                }
                if ([dataBody.PostUpdateRoomDetailsRoutineResult.ResponseStatuses.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]]) {
                    return dataBody.PostUpdateRoomDetailsRoutineResult;
                }
            }
        }
    }
    return NULL;
}

- (BOOL) postWSLog:(NSInteger)userId Message:(NSString *)messageValue MessageType:(NSInteger)type{
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService
                                                  eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole])
    {
        [binding setLogXMLInOut:YES];
    }
    eHousekeepingService_PostWSLog *request = [[eHousekeepingService_PostWSLog
                                                alloc] init];
    request.intUsrID = [NSNumber numberWithInteger:userId];
    request.strMessage = messageValue;
    request.intMessageType = [NSNumber numberWithInteger:type];
    eHousekeepingServiceSoap12BindingResponse *response = [binding
                                                           PostWSLogUsingParameters:request];
    
    BOOL isPostSuccess = NO;
    
    for (id bodyPart in response.bodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_PostWSLogResponse class]]) {
            eHousekeepingService_PostWSLogResponse *dataBody =
            (eHousekeepingService_PostWSLogResponse *)bodyPart;
            if (dataBody != nil) {
                if ([dataBody.PostWSLogResult.respCode isEqualToNumber:[NSNumber
                                                                        numberWithInteger:RESPONSE_STATUS_OK]])
                {
                    isPostSuccess = YES;
                }
            }
        }
    }
    return isPostSuccess;
}

@end
