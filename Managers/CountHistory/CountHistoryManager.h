//
//  CountHistoryManager.h
//  mHouseKeeping
//
//  Created by Mac User on 28/03/2013.
//
//

#import <Foundation/Foundation.h>
#import "CountHistoryAdapter.h"
#import "CountHistoryModel.h"

@interface CountHistoryManager : NSObject
-(int) insertCountHistoryData:(CountHistoryModel *) countHistoryModel;
//-(NSMutableArray *) loadAllItemsContainsCountHistoryByUserId:(NSInteger)userId;
-(NSMutableArray *) loadAllCountHistoryByServiceId:(NSInteger) serviceId AndUserId:(NSInteger)userId AndRoomId:(NSString*)roomNumber;
-(NSMutableArray *) loadRoomIdByUserId:(NSInteger)userId AndServiceId:(NSInteger)serviceId;
-(int) deleteDatabaseAfterSomeDays;
@end
