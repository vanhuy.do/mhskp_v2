//
//  CountHistoryManager.m
//  mHouseKeeping
//
//  Created by Mac User on 28/03/2013.
//
//

#import "CountHistoryManager.h"
#import "CountHistoryAdapter.h"
#import "DataBaseExtension.h"
#import "LanguageManagerV2.h"

@implementation CountHistoryManager

-(int) insertCountHistoryData:(CountHistoryModel *) countHistoryModel{
    
    NSInteger returnCode = 0;
    CountHistoryAdapter *adapter = [[CountHistoryAdapter alloc] init];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    returnCode = [adapter insertCountHistoryData:countHistoryModel];
    [adapter close];
    adapter = nil;
    return (int)returnCode;
}

//-(NSMutableArray *) loadAllItemsContainsCountHistoryByUserId:(NSInteger)userId;{
//
//    CountHistoryAdapter *adapter = [[CountHistoryAdapter alloc] init];
//    [adapter openDatabase];
//    NSMutableArray *array = [adapter loadAllItemsContainsCountHistoryByUserId:userId];
//    [adapter close];
//    return array;
//}

//-(CountHistoryModel *) loadAllItemsContainsCountHistory:(CountHistoryModel *)model{
//    CountHistoryAdapter *adapter = [[CountHistoryAdapter alloc] init];
//    [adapter openDatabase];
//    [adapter loadAllItemsContainsCountHistory:model];
//    [adapter close];
//    return model;
//}

-(NSMutableArray *) loadAllCountHistoryByServiceId:(NSInteger) serviceId AndUserId:(NSInteger)userId AndRoomId:(NSString*)roomNumber{

    //Amenities
    if(serviceId == cAmenities) {
        NSMutableArray *array = [self loadAmenitiesHistoriesByUserId:(int)userId roomNumber:roomNumber];
        return array;
        
    } else {
        CountHistoryAdapter *adapter = [[CountHistoryAdapter alloc] init];
        [adapter openDatabase];
        NSMutableArray *array = [adapter loadAllCountHistoryByServiceId:serviceId AndUserId:userId AndRoomId:roomNumber];
        [adapter close];
        return array;
    }
}

-(NSMutableArray *) loadRoomIdByUserId:(NSInteger)userId AndServiceId:(NSInteger)serviceId{
    CountHistoryAdapter *adapter = [[CountHistoryAdapter alloc] init];
    [adapter openDatabase];
    NSMutableArray *array = [adapter loadRoomIdByUserId:userId AndServiceId:serviceId];
    [adapter close];
    return array;
}

-(int) deleteDatabaseAfterSomeDays{
    NSInteger returnCode = 0;
    CountHistoryAdapter *adapter = [[CountHistoryAdapter alloc] init];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    returnCode = [adapter deleteDatabaseAfterSomeDays];
    [adapter close];
    adapter = nil;
    return (int)returnCode;
}

#pragma mark - Private Function
-(NSMutableArray*)loadAmenitiesHistoriesByUserId:(int)userId roomNumber:(NSString*)roomNumber
{
    if(userId <= 0){
        return nil;
    }
    
    BOOL isEnglish = [[LanguageManagerV2 sharedLanguageManager] currentLanguageIsEqualTo:@"%@", ENGLISH_LANGUAGE];
    NSString *amenitiesName = nil;
    if(isEnglish) {
        amenitiesName = [NSString stringWithFormat:@"%@", item_name];
    } else {
        amenitiesName = [NSString stringWithFormat:@"%@", item_lang];
    }
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT a.%@, a.%@, a.%@, a.%@, a.%@, a.%@, a.%@, a.%@, b.%@ FROM %@ a JOIN %@ b ON a.%@ = b.%@  WHERE %@ = %d AND %@ = %d AND %@ = '%@' ORDER BY a.%@ ASC",
                           H_count_id,
                           H_room_id,
                           H_count_item_id,
                           H_count_collected,
                           H_count_new,
                           H_count_service,
                           H_count_date,
                           H_count_user_id,
                           amenitiesName,
                           
                           COUNT_HISTORY,
                           AMENITIES_ITEMS,
                           
                           H_count_item_id,
                           item_id,
                           H_count_service,
                           cAmenities,
                           H_count_user_id,
                           userId,
                           H_room_id,
                           roomNumber,
                           H_room_id
                           ];
    
    DataTable *table = [[DataTable alloc] init];
    
    //Row result reference
    DataRow *rowReference = [[DataRow alloc] init];
    [rowReference addColumnFormatInt:@"%@", H_count_id];
    [rowReference addColumnFormatString:@"%@", H_room_id];
    [rowReference addColumnFormatInt:@"%@", H_count_item_id];
    [rowReference addColumnFormatInt:@"%@", H_count_collected];
    [rowReference addColumnFormatInt:@"%@", H_count_new];
    [rowReference addColumnFormatInt:@"%@", H_count_service];
    [rowReference addColumnFormatString:@"%@",H_count_date];
    [rowReference addColumnFormatInt:@"%@",H_count_user_id];
    [rowReference addColumnFormatString:@"%@",amenitiesName];
    
    [table loadTableDataWithQuery:sqlString referenceRow:rowReference];
    
    int rowCount = [table countRows];
    NSMutableArray *results = nil;
    if(rowCount > 0)
    {
        results = [NSMutableArray array];
        for (int i = 0; i < rowCount; i ++) {
            
            CountHistoryModel *model = [[CountHistoryModel alloc] init];
            DataRow *row = [table rowWithIndex:i];
            
            model.count_collected = [row intValueWithColumnNameFormat:@"%@", H_count_collected];
            model.count_date = [row stringValueWithColumnNameFormat:@"%@", H_count_date];
            model.count_id = [row intValueWithColumnNameFormat:@"%@", H_count_id];
            model.count_item_id = [row intValueWithColumnNameFormat:@"%@", H_count_item_id];
            model.count_item_name = [row stringValueWithColumnNameFormat:@"%@", amenitiesName];
            model.count_new = [row intValueWithColumnNameFormat:@"%@", H_count_new];
            model.count_service = [row intValueWithColumnNameFormat:@"%@", H_count_service];
            model.count_user_id = [row intValueWithColumnNameFormat:@"%@", H_count_user_id];
            model.room_id = [row stringValueWithColumnNameFormat:@"%@", H_room_id];
            
            [results addObject:model];
        }
    }
    
    return results;
}

@end
