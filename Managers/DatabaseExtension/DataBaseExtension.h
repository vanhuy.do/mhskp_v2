//
//  DataBaseExtension.h
//  mHouseKeeping
//
//  Created by vinhnguyen on 7/13/13.
//
//

#import <Foundation/Foundation.h>
#import "DatabaseAdapterV2.h"

//#define VariableName(x) [NSString stringWithFormat:@"%s", #x] // NSLog( @"Value of %s = %d",#x, x)
#define VariableName(property) (@""#property)
#define VariableNameLastComponent(property) [[(@""#property) componentsSeparatedByString:@"."] lastObject]

#define CreateColumnString(property) [DataColumn createColumnString:VariableNameLastComponent(property) value:property]
#define CreateColumnInt(property) [DataColumn createColumnInt:VariableNameLastComponent(property) value:property]
#define CreateColumnFloat(property) [DataColumn createColumnFloat:VariableNameLastComponent(property) value:property]
#define CreateColumnLong(property) [DataColumn createColumnLong:VariableNameLastComponent(property) value:property]
#define CreateColumnDouble(property) [DataColumn createColumnDouble:VariableNameLastComponent(property) value:property]
#define CreateColumnData(property) [DataColumn createColumnData:VariableNameLastComponent(property) value:property]

@class DataColumn,DataRow, DataTable;

enum DataType{
    DataType_String = 1,
    DataType_Int,
    DataType_Float,
    DataType_Double,
    DataType_Long,
    DataType_Data
};

#pragma mark - Base Data Object Defination

@interface DatabaseField : NSObject
@property (assign, nonatomic) enum DataType dataType;
@property (strong, nonatomic) NSString *fieldName;

//fieldValue will parse to NSString if DataTye = DataType_String
//otherwise, will parse to NSNumber.
@property (strong, nonatomic) id fieldValue;

-(id)initWith:(enum DataType)dataTypeValue fieldName:(NSString*)dataName fieldValue:(id)dataValue;
-(id)initWith:(enum DataType)dataTypeValue fieldName:(NSString*)dataName;

//MARK: Field Value parser
-(int)fieldValueInt;
-(float)fieldValueFloat;
-(double)fieldValueDouble;
-(long)fieldValueLong;
-(NSData*)fieldValueData;
-(NSString*)fieldValueString;
@end

#pragma mark - Data Column
@interface DataColumn : DatabaseField

//Column Marked condition will not change if commit update database
@property (nonatomic) BOOL isUpdateCondition;

//Column Marked Key will not insert if commit insert database
@property (nonatomic) BOOL isKeyOnInsert;

//MARK: factory creating column with value
+(DataColumn*)createColumnString:(NSString*)columnName value:(NSString*)stringValue;
+(DataColumn*)createColumnInt:(NSString*)columnName value:(int)intValue;
+(DataColumn*)createColumnFloat:(NSString*)columnName value:(float)floatValue;
+(DataColumn*)createColumnLong:(NSString*)columnName value:(long)longValue;
+(DataColumn*)createColumnDouble:(NSString*)columnName value:(double)doubleValue;
+(DataColumn*)createColumnData:(NSString*)columnName value:(NSData*)dataValue;

@end

#pragma mark - Data Row
@interface DataRow : NSObject

@property (nonatomic, strong) NSMutableArray *columns;
@property (nonatomic, strong) NSString *tableName;

-(id)init;

-(DataColumn*)columnWithIndex:(int)indexColumn;
-(DataColumn*)columnWithName:(NSString*)nameColumn;
-(DataColumn*)columnWithNameFormat:(NSString*)name,...;//String format parameters

//MARK: Quick Access Data
-(NSString*)stringValueWithColumnNameFormat:(NSString*)name,...;
-(int)intValueWithColumnNameFormat:(NSString*)name,...;
-(float)floatValueWithColumnNameFormat:(NSString*)name,...;
-(long)longValueWithColumnNameFormat:(NSString*)name,...;
-(double)doubleValueWithColumnNameFormat:(NSString*)name,...;
-(NSData*)dataValueWithColumnNameFormat:(NSString*)name,...;

-(void)removeColumn:(DataColumn*)columnData;
-(void)removeColumnWithIndex:(int)indexColumn;
-(void)removeColumnWithName:(NSString*)nameColumn;
//-(void)removeColumnWithNameFormat:(NSString*)name,...;//String format parameters

-(void)addColumn:(DataColumn*)columnData;
-(void)insertColumn:(DataColumn*)columnData atIndex:(int)columnIndex;
-(int) countColumns;

//MARK: create column with value
-(DataColumn*)addColumnString:(NSString*)columnName value:(NSString*)stringValue;
-(DataColumn*)addColumnInt:(NSString*)columnName value:(int)intValue;
-(DataColumn*)addColumnFloat:(NSString*)columnName value:(float)floatValue;
-(DataColumn*)addColumnLong:(NSString*)columnName value:(long)longValue;
-(DataColumn*)addColumnDouble:(NSString*)columnName value:(double)doubleValue;
-(DataColumn*)addColumnData:(NSString*)columnName value:(NSData*)dataValue;

//MARK: create column format with value
-(DataColumn*)addColumnStringValue:(NSString*)stringValue formatName:(NSString*)name,...;
-(DataColumn*)addColumnIntValue:(int)intValue formatName:(NSString*)name,...;
-(DataColumn*)addColumnFloatValue:(float)floatValue formatName:(NSString*)name,...;
-(DataColumn*)addColumnLongValue:(long)longValue formatName:(NSString*)name,...;
-(DataColumn*)addColumnDoubleValue:(double)doubleValue formatName:(NSString*)name,...;
-(DataColumn*)addColumnDataValue:(NSData*)dataValue formatName:(NSString*)name,...;

//MARK: Create column with null value
-(DataColumn*)addColumnString:(NSString*)columnName;
-(DataColumn*)addColumnInt:(NSString*)columnName;
-(DataColumn*)addColumnFloat:(NSString*)columnName;
-(DataColumn*)addColumnLong:(NSString*)columnName;
-(DataColumn*)addColumnDouble:(NSString*)columnName;
-(DataColumn*)addColumnData:(NSString*)columnName;

//MARK: Create column format with null value
-(DataColumn*)addColumnFormatString:(NSString*)name,...;
-(DataColumn*)addColumnFormatInt:(NSString*)name,...;
-(DataColumn*)addColumnFormatFloat:(NSString*)name,...;
-(DataColumn*)addColumnFormatLong:(NSString*)name,...;
-(DataColumn*)addColumnFormatDouble:(NSString*)name,...;
-(DataColumn*)addColumnFormatData:(NSString*)name,...;


//MARK:Database change
//Change data if current row have condition
//return 0 if change not success
//return >0 if change success
-(int) commitChangeDBRow;

//Clear data if current row have condition
//return 0 if delete not success
//return >0 if delete success
-(int) dropRow;

//Insert Database Rows
-(int) insertDBRow;

//Insert Database directly with multiplue rows
-(int) insertDBRowWithColumns:(NSArray*)valueColumns;

//Update Database directly with condition columns
-(int) updateDBRowsWithColumns:(NSArray*)valueColumns conditions:(NSArray*) valueConditions;

@end

#pragma mark - Data Table
@interface DataTable : NSObject

@property (nonatomic, strong) NSMutableArray *rows;
@property (nonatomic, strong) NSString *tableName;

-(id)init;
-(id)initWithName:(NSString*)tableNameValue;
-(id)initWithNameFormat:(NSString*)name,...; //String format parameters

-(DataRow*)rowWithIndex:(int)indexRow;
-(int) countRows;

-(void)addRow:(DataRow*)rowData;
-(void)insertRow:(DataRow*)rowData atIndex:(int)rowIndex;

-(void)removeRow:(DataRow*)rowData;
-(void)removeRowWithIndex:(int)indexRow;

//MARK:Database change

//Change data if any rows have condition
//return 0 if change not success
//return >0 if change success
-(int) commitChangeDBRows;

//Clear data if any rows have condition
//return 0 if delete not success
//return >0 if delete success
-(int) dropDBRows;

//Insert Database Rows
-(int) insertDBRows;

//load database
-(BOOL) loadTableDataWithReferenceRow:(DataRow*)rowData conditions:(NSMutableArray*)columnsCondition;
-(BOOL) loadTableDataWithQuery:(NSString*)stringQuery referenceRow:(DataRow*)rowData;

////Return interger >0 if success
-(NSInteger) excuteQueryNonSelect:(NSString*)stringQuery;
@end

#pragma mark - Database Extension
@interface DataBaseExtension : DatabaseAdapterV2

@property (assign) BOOL isWriteLogConsole;

//return row id
-(int) insertTable:(NSString*) tableName fields:(NSMutableArray*)fields;

//return 1 : update ok
//return 0 : update failed
-(int) updateTable:(NSString*) tableName fields:(NSMutableArray*)fields conditions:(NSMutableArray*)conditionFields;

//return 1 : update ok
//return 0 : update failed
-(int) deleteTable:(NSString*) tableName conditions:(NSMutableArray*)conditionFields;

//return fields name and value of fields
-(NSMutableArray*) selectFields:(NSMutableArray*)fields inTable:(NSString*)tableName conditions:(NSMutableArray*)conditionFields othersFilter:(NSString*)othersFilter;

//return array of DataRow
-(NSMutableArray*) selectRows:(DataRow*)rowData inTable:(NSString*)tableName conditions:(NSMutableArray*)conditionFields othersFilter:(NSString*)othersFilter;

//return fields name and value of fields
-(NSMutableArray*) excuteQuerySelect:(NSString*)queryString fields:(NSMutableArray*)selectedFields;

//return array of DataRow
-(NSMutableArray*) excuteQuerySelect:(NSString*)queryString referenceRow:(DataRow*)rowData;

//Return status of sql statement
-(NSInteger) excuteQueryNonSelect:(NSString*)queryString;

#pragma mark - Database Fields Creator

-(DatabaseField*)createStringField:(NSString*)fieldName value:(NSString*)stringValue;
-(DatabaseField*)createIntField:(NSString*)fieldName value:(int)intValue;
-(DatabaseField*)createFloatField:(NSString*)fieldName value:(float)floatValue;
-(DatabaseField*)createLongField:(NSString*)fieldName value:(long)longValue;
-(DatabaseField*)createDoubleField:(NSString*)fieldName value:(double)doubleValue;
-(DatabaseField*)createDataField:(NSString*)fieldName value:(NSData*)dataValue;


//MARK: Create field with null value
-(DatabaseField*)createStringField:(NSString*)fieldName;
-(DatabaseField*)createIntField:(NSString*)fieldName;
-(DatabaseField*)createFloatField:(NSString*)fieldName;
-(DatabaseField*)createLongField:(NSString*)fieldName;
-(DatabaseField*)createDoubleField:(NSString*)fieldName;
-(DatabaseField*)createDataField:(NSString*)fieldName;

@end
