//
//  DataBaseExtension.m
//  mHouseKeeping
//
//  Created by vinhnguyen on 7/13/13.
//
//

#import "DataBaseExtension.h"
#import "LogFileManager.h"

#pragma mark - Base Data Object Defination

@implementation DatabaseField

@synthesize dataType;
@synthesize fieldName;
@synthesize fieldValue;

- (id)copy
{
    return [super copy];
}

-(id)initWith:(enum DataType)dataTypeValue fieldName:(NSString*)dataName fieldValue:(id)dataValue
{
    self = [super init];
    if (self) {
		dataType = dataTypeValue;
        fieldName = dataName;
        fieldValue = dataValue;
	}
    return self;
}

-(id)initWith:(enum DataType)dataTypeValue fieldName:(NSString*)dataName
{
    self = [super init];
    if (self) {
		dataType = dataTypeValue;
        fieldName = dataName;
	}
    return self;
}

//MARK: Field Value parser
-(int)fieldValueInt
{
    int result = [(NSNumber*)fieldValue intValue];
    return result;
}

-(float)fieldValueFloat
{
    float result = [(NSNumber*)fieldValue floatValue];
    return result;
}

-(double)fieldValueDouble
{
    double result = [(NSNumber*)fieldValue doubleValue];
    return result;
}

-(long)fieldValueLong
{
    long result = [(NSNumber*)fieldValue longValue];
    return result;
}

-(NSData*)fieldValueData
{
    NSData *result = (NSData *)fieldValue;
    return result;
}

-(NSString*)fieldValueString
{
    NSString *result = (NSString *)fieldValue;
    return result;
}

@end


#pragma mark - Data Column
@implementation DataColumn

//Column Marked condition will not change if commit update database
@synthesize isUpdateCondition;

//Column Marked Key will not insert if commit insert database
@synthesize isKeyOnInsert;

-(id)initWith:(enum DataType)dataTypeValue fieldName:(NSString *)dataName fieldValue:(id)dataValue
{
    self = [super initWith:dataTypeValue fieldName:dataName fieldValue:dataValue];
    if(self){
        isKeyOnInsert = NO;
        isUpdateCondition = NO;
    }
    return self;
}

//MARK: factory creating column with value
+(DataColumn*)createColumnString:(NSString*)columnName value:(NSString*)stringValue
{
    DataColumn *result = [[DataColumn alloc] initWith:DataType_String fieldName:columnName fieldValue:stringValue];
    return result;
}
+(DataColumn*)createColumnInt:(NSString*)columnName value:(int)intValue
{
    DataColumn *result = [[DataColumn alloc] initWith:DataType_Int fieldName:columnName fieldValue:[NSNumber numberWithInt:intValue]];
    return result;
}

+(DataColumn*)createColumnFloat:(NSString*)columnName value:(float)floatValue
{
    DataColumn *result = [[DataColumn alloc] initWith:DataType_Float fieldName:columnName fieldValue:[NSNumber numberWithFloat:floatValue]];
    return result;
}
+(DataColumn*)createColumnLong:(NSString*)columnName value:(long)longValue
{
    DataColumn *result = [[DataColumn alloc] initWith:DataType_Long fieldName:columnName fieldValue:[NSNumber numberWithLong:longValue]];
    return result;
}
+(DataColumn*)createColumnDouble:(NSString*)columnName value:(double)doubleValue
{
    DataColumn *result = [[DataColumn alloc] initWith:DataType_Double fieldName:columnName fieldValue:[NSNumber numberWithDouble:doubleValue]];
    return result;
}

+(DataColumn*)createColumnData:(NSString *)columnName value:(NSData*)dataValue
{
    DataColumn *result = [[DataColumn alloc] initWith:DataType_Data fieldName:columnName fieldValue:dataValue];
    return result;
}

@end

#pragma mark - Data Row
@implementation DataRow

@synthesize tableName;
@synthesize columns;

- (id)init
{
    self = [super init];
    if(self){
        columns = [[NSMutableArray alloc] init];
    }
    return self;
}

-(DataColumn*)columnWithIndex:(int)indexColumn
{
    if([columns count] > 0)
    {
        return (DataColumn*)[columns objectAtIndex:indexColumn];
    }
    else
    {
        return nil;
    }
}

-(DataColumn*)columnWithName:(NSString*)nameColumn
{
    //Finding column
    DataColumn *columnFound = nil;
    for (DataColumn *curColumn in columns) {
        if ([curColumn.fieldName isEqualToString:nameColumn]) {
            columnFound = curColumn;
            break;
        }
    }
    
    return columnFound;
}

-(DataColumn*)columnWithNameFormat:(NSString*)name,...
{
    NSString *fullContents = nil;
    va_list args;
    va_start(args, name);
    fullContents = [[NSString alloc] initWithFormat:name arguments:args];
    va_end(args);
    
    return [self columnWithName:fullContents];
}

//MARK: Quick Access Data
-(NSString*)stringValueWithColumnNameFormat:(NSString*)name,...
{
    NSString *fullContents = nil;
    va_list args;
    va_start(args, name);
    fullContents = [[NSString alloc] initWithFormat:name arguments:args];
    va_end(args);
    
    DataColumn *columnValue = [self columnWithName:fullContents];
    return [columnValue fieldValueString];
}

-(int)intValueWithColumnNameFormat:(NSString*)name,...
{
    NSString *fullContents = nil;
    va_list args;
    va_start(args, name);
    fullContents = [[NSString alloc] initWithFormat:name arguments:args];
    va_end(args);
    
    DataColumn *columnValue = [self columnWithName:fullContents];
    return  [columnValue fieldValueInt];
}
-(float)floatValueWithColumnNameFormat:(NSString*)name,...
{
    NSString *fullContents = nil;
    va_list args;
    va_start(args, name);
    fullContents = [[NSString alloc] initWithFormat:name arguments:args];
    va_end(args);
    
    DataColumn *columnValue = [self columnWithName:fullContents];
    return  [columnValue fieldValueFloat];
}
-(long)longValueWithColumnNameFormat:(NSString*)name,...
{
    NSString *fullContents = nil;
    va_list args;
    va_start(args, name);
    fullContents = [[NSString alloc] initWithFormat:name arguments:args];
    va_end(args);
    
    DataColumn *columnValue = [self columnWithName:fullContents];
    return  [columnValue fieldValueLong];
}

-(double)doubleValueWithColumnNameFormat:(NSString*)name,...
{
    NSString *fullContents = nil;
    va_list args;
    va_start(args, name);
    fullContents = [[NSString alloc] initWithFormat:name arguments:args];
    va_end(args);
    
    DataColumn *columnValue = [self columnWithName:fullContents];
    return  [columnValue fieldValueDouble];
}

-(NSData*)dataValueWithColumnNameFormat:(NSString*)name,...
{
    NSString *fullContents = nil;
    va_list args;
    va_start(args, name);
    fullContents = [[NSString alloc] initWithFormat:name arguments:args];
    va_end(args);
    
    DataColumn *columnValue = [self columnWithName:fullContents];
    return  [columnValue fieldValueData];
}

-(void)removeColumn:(DataColumn*)columnData
{
    NSMutableArray *arrayRemove = [NSMutableArray array];
    [arrayRemove addObject:columnData];
    [columns removeObjectsInArray:arrayRemove];
}

-(void)removeColumnWithIndex:(int)indexColumn
{
    NSMutableArray *arrayRemove = [NSMutableArray array];
    DataColumn *curColumn = (DataColumn*)[columns objectAtIndex:indexColumn];
    [arrayRemove addObject:curColumn];
    [columns removeObjectsInArray:arrayRemove];
}

-(void)removeColumnWithName:(NSString*)nameColumn
{
    NSMutableArray *arrayRemove = [NSMutableArray array];
    for (DataColumn *curColumn in columns) {
        if ([curColumn.fieldName isEqualToString:nameColumn]) {
            [arrayRemove addObject:curColumn];
            break;
        }
    }
    [columns removeObjectsInArray:arrayRemove];
}

-(void)addColumn:(DataColumn*)columnData
{
    [columns addObject:columnData];
}

-(void)insertColumn:(DataColumn*)columnData atIndex:(int)columnIndex;
{
    [columns insertObject:columnData atIndex:columnIndex];
}

-(int) countColumns
{
    return (int)[columns count];
}

//MARK: Command to Data base
-(int) commitChangeDBRow
{
    NSMutableArray *conditions = [NSMutableArray array];
    NSMutableArray *columnsUpdate = [NSMutableArray array];
    
    for (DataColumn *curColumn in columns) {
        if(curColumn.isUpdateCondition){
            //Add condition columns
            [conditions addObject:curColumn];
        } else {
            [columnsUpdate addObject:curColumn];
        }
    }
    
    if([conditions count] <= 0){
        return 0;
    }
    
    DataBaseExtension *dbManager = [[DataBaseExtension alloc] init];
    [dbManager openDatabase];
    [dbManager resetSqlCommand];
    
    int returnCode = [dbManager updateTable:tableName fields:columnsUpdate conditions:conditions];
    [dbManager close];
    
    return returnCode;
}

-(int) dropRow
{
    
    NSMutableArray *conditions = [NSMutableArray array];
    
    for (DataColumn *curColumn in columns) {
        if(curColumn.isUpdateCondition){
            //Add condition columns
            [conditions addObject:curColumn];
        }
    }
    
    DataBaseExtension *dbManager = [[DataBaseExtension alloc]init] ;
    [dbManager openDatabase];
    [dbManager resetSqlCommand];
    int returnCode = [dbManager deleteTable:tableName conditions:conditions];
    [dbManager close];
    
    return returnCode;
}

//Insert Database Rows with column mark "KEY"
-(int) insertDBRow
{
    //Find columns without key
    NSMutableArray *columnsNoKey = [NSMutableArray array];
    for (DataColumn *curColumn in columns) {
        if (!curColumn.isKeyOnInsert) {
            [columnsNoKey addObject:curColumn];
        }
    }
    
    DataBaseExtension *dbManager = [[DataBaseExtension alloc]init] ;
    [dbManager openDatabase];
    [dbManager resetSqlCommand];
    
    int returnCode = [dbManager insertTable:tableName fields:columnsNoKey];
    [dbManager close];
    
    return returnCode;
}

//Insert Database directly with multiplue rows
-(int) insertDBRowWithColumns:(NSArray*)valueColumns
{
    if(!columns) {
        columns = [NSMutableArray array];
    }
    [columns addObjectsFromArray:valueColumns];
    
    //Find columns without key
    NSMutableArray *columnsNoKey = [NSMutableArray array];
    for (DataColumn *curColumn in columns) {
        if (!curColumn.isKeyOnInsert) {
            [columnsNoKey addObject:curColumn];
        }
    }
    
    DataBaseExtension *dbManager = [[DataBaseExtension alloc]init] ;
    [dbManager openDatabase];
    [dbManager resetSqlCommand];
    
    int returnCode = [dbManager insertTable:tableName fields:columnsNoKey];
    [dbManager close];
    
    return returnCode;
}

//Update Database directly with condition columns
-(int) updateDBRowsWithColumns:(NSArray*)valueColumns conditions:(NSArray*) valueConditions
{
    if(!columns) {
        columns = [NSMutableArray array];
    }
    
    [columns addObjectsFromArray:valueColumns];
    [columns addObjectsFromArray:valueConditions];
    
    NSMutableArray *conditions = [NSMutableArray arrayWithArray:valueConditions];
    NSMutableArray *columnsUpdate = [NSMutableArray arrayWithArray:valueColumns];
    
    for (DataColumn *curColumn in valueConditions) {
        curColumn.isUpdateCondition = YES;
    }
    
    if([conditions count] <= 0){
        return 0;
    }
    
    DataBaseExtension *dbManager = [[DataBaseExtension alloc] init];
    [dbManager openDatabase];
    [dbManager resetSqlCommand];
    
    int returnCode = [dbManager updateTable:tableName fields:columnsUpdate conditions:conditions];
    [dbManager close];
    
    return returnCode;
}

//MARK: create and append column with value
-(DataColumn*)addColumnString:(NSString*)columnName value:(NSString*)stringValue
{
    DataColumn *result = [[DataColumn alloc] initWith:DataType_String fieldName:columnName fieldValue:stringValue];
    [columns addObject:result];
    return result;
}
-(DataColumn*)addColumnInt:(NSString*)columnName value:(int)intValue
{
    DataColumn *result = [[DataColumn alloc] initWith:DataType_Int fieldName:columnName fieldValue:[NSNumber numberWithInt:intValue]];
    [columns addObject:result];
    return result;
}

-(DataColumn*)addColumnFloat:(NSString*)columnName value:(float)floatValue
{
    DataColumn *result = [[DataColumn alloc] initWith:DataType_Float fieldName:columnName fieldValue:[NSNumber numberWithFloat:floatValue]];
    [columns addObject:result];
    return result;
}
-(DataColumn*)addColumnLong:(NSString*)columnName value:(long)longValue
{
    DataColumn *result = [[DataColumn alloc] initWith:DataType_Long fieldName:columnName fieldValue:[NSNumber numberWithLong:longValue]];
    [columns addObject:result];
    return result;
}
-(DataColumn*)addColumnDouble:(NSString*)columnName value:(double)doubleValue
{
    DataColumn *result = [[DataColumn alloc] initWith:DataType_Double fieldName:columnName fieldValue:[NSNumber numberWithDouble:doubleValue]];
    [columns addObject:result];
    return result;
}

-(DataColumn*)addColumnData:(NSString *)columnName value:(NSData*)dataValue
{
    DataColumn *result = [[DataColumn alloc] initWith:DataType_Data fieldName:columnName fieldValue:dataValue];
    [columns addObject:result];
    return result;
}

//MARK: Create and append column with null value
-(DataColumn*)addColumnString:(NSString*)columnName
{
    DataColumn *result = [[DataColumn alloc] initWith:DataType_String fieldName:columnName fieldValue:nil];
    [columns addObject:result];
    return result;
}
-(DataColumn*)addColumnInt:(NSString*)columnName
{
    DataColumn *result = [[DataColumn alloc] initWith:DataType_Int fieldName:columnName fieldValue:nil];
    [columns addObject:result];
    return result;
}
-(DataColumn*)addColumnFloat:(NSString*)columnName
{
    DataColumn *result = [[DataColumn alloc] initWith:DataType_Float fieldName:columnName fieldValue:nil];
    [columns addObject:result];
    return result;
}

-(DataColumn*)addColumnLong:(NSString*)columnName
{
    DataColumn *result = [[DataColumn alloc] initWith:DataType_Long fieldName:columnName fieldValue:nil];
    [columns addObject:result];
    return result;
}
-(DataColumn*)addColumnDouble:(NSString*)columnName
{
    DataColumn *result = [[DataColumn alloc] initWith:DataType_Double fieldName:columnName fieldValue:nil];
    [columns addObject:result];
    return result;
}

-(DataColumn*)addColumnData:(NSString *)columnName
{
    DataColumn *result = [[DataColumn alloc] initWith:DataType_Data fieldName:columnName fieldValue:nil];
    [columns addObject:result];
    return result;
}

//MARK: Create column format with null value
-(DataColumn*)addColumnFormatString:(NSString*)name,...
{
    NSString *fullContents = nil;
    va_list args;
    va_start(args, name);
    fullContents = [[NSString alloc] initWithFormat:name arguments:args];
    va_end(args);
    
    return [self addColumnString:fullContents];
}

-(DataColumn*)addColumnFormatInt:(NSString*)name,...
{
    NSString *fullContents = nil;
    va_list args;
    va_start(args, name);
    fullContents = [[NSString alloc] initWithFormat:name arguments:args];
    va_end(args);
    
    return [self addColumnInt:fullContents];
}

-(DataColumn*)addColumnFormatFloat:(NSString*)name,...
{
    NSString *fullContents = nil;
    va_list args;
    va_start(args, name);
    fullContents = [[NSString alloc] initWithFormat:name arguments:args];
    va_end(args);
    
    return [self addColumnFloat:fullContents];
}

-(DataColumn*)addColumnFormatLong:(NSString*)name,...
{
    NSString *fullContents = nil;
    va_list args;
    va_start(args, name);
    fullContents = [[NSString alloc] initWithFormat:name arguments:args];
    va_end(args);
    
    return [self addColumnLong:fullContents];
}

-(DataColumn*)addColumnFormatDouble:(NSString*)name,...
{
    NSString *fullContents = nil;
    va_list args;
    va_start(args, name);
    fullContents = [[NSString alloc] initWithFormat:name arguments:args];
    va_end(args);
    
    return [self addColumnDouble:fullContents];
}

-(DataColumn*)addColumnFormatData:(NSString*)name,...
{
    NSString *fullContents = nil;
    va_list args;
    va_start(args, name);
    fullContents = [[NSString alloc] initWithFormat:name arguments:args];
    va_end(args);
    
    return [self addColumnData:fullContents];
}


//MARK: create column format with value
-(DataColumn*)addColumnStringValue:(NSString *)stringValue formatName:(NSString *)name, ...
{
    NSString *fullContents = nil;
    va_list args;
    va_start(args, name);
    fullContents = [[NSString alloc] initWithFormat:name arguments:args];
    va_end(args);
    
    return [self addColumnString:fullContents value:stringValue];
}

-(DataColumn*)addColumnIntValue:(int)intValue formatName:(NSString *)name, ...
{
    NSString *fullContents = nil;
    va_list args;
    va_start(args, name);
    fullContents = [[NSString alloc] initWithFormat:name arguments:args];
    va_end(args);
    
    return [self addColumnInt:fullContents value:intValue];
}

-(DataColumn*)addColumnFloatValue:(float)floatValue formatName:(NSString *)name, ...
{
    NSString *fullContents = nil;
    va_list args;
    va_start(args, name);
    fullContents = [[NSString alloc] initWithFormat:name arguments:args];
    va_end(args);
    
    return [self addColumnFloat:fullContents value:floatValue];
}

-(DataColumn*)addColumnLongValue:(long)longValue formatName:(NSString *)name, ...
{
    NSString *fullContents = nil;
    va_list args;
    va_start(args, name);
    fullContents = [[NSString alloc] initWithFormat:name arguments:args];
    va_end(args);
    
    return [self addColumnLong:fullContents value:longValue];
}

-(DataColumn*)addColumnDoubleValue:(double)doubleValue formatName:(NSString *)name, ...
{
    NSString *fullContents = nil;
    va_list args;
    va_start(args, name);
    fullContents = [[NSString alloc] initWithFormat:name arguments:args];
    va_end(args);
    
    return [self addColumnDouble:fullContents value:doubleValue];
}

-(DataColumn*)addColumnDataValue:(NSData *)dataValue formatName:(NSString *)name, ...
{
    NSString *fullContents = nil;
    va_list args;
    va_start(args, name);
    fullContents = [[NSString alloc] initWithFormat:name arguments:args];
    va_end(args);
    
    return [self addColumnData:fullContents value:dataValue];
}

@end

#pragma mark - Data Table
@implementation DataTable

@synthesize tableName;
@synthesize rows = _rows;

-(id)init
{
    self = [super init];
    if(self){
        tableName = @"";
        _rows = [[NSMutableArray alloc] init];
    }
    return self;
}

-(id)initWithName:(NSString*)tableNameValue
{
    self = [super init];
    if(self){
        tableName = tableNameValue;
        _rows = [[NSMutableArray alloc] init];
    }
    return self;
}

-(id)initWithNameFormat:(NSString*)name,...
{
    NSString *fullContents = nil;
    va_list args;
    va_start(args, name);
    fullContents = [[NSString alloc] initWithFormat:name arguments:args];
    va_end(args);
    
    return [self initWithName:fullContents];
}

-(DataRow*)rowWithIndex:(int)indexRow
{
    return (DataRow*)[_rows objectAtIndex:indexRow];
}

-(void)addRow:(DataRow*)rowData
{
    rowData.tableName = tableName;
    [_rows addObject:rowData];
}

-(void)insertRow:(DataRow*)rowData atIndex:(int)rowIndex
{
    rowData.tableName = tableName;
    [_rows insertObject:rowData atIndex:rowIndex];
}
-(int) countRows
{
    return (int)[_rows count];
}

-(void)removeRow:(DataRow*)rowData
{
    NSMutableArray *arrayRemove = [NSMutableArray array];
    [arrayRemove addObject:rowData];
    [_rows removeObjectsInArray:arrayRemove];
}

-(void)removeRowWithIndex:(int)indexRow
{
    NSMutableArray *arrayRemove = [NSMutableArray array];
    DataRow *curRow = (DataRow*)[_rows objectAtIndex:indexRow];
    [arrayRemove addObject:curRow];
    [_rows removeObjectsInArray:arrayRemove];
}

//MARK: Command to Database
-(int) commitChangeDBRows
{
    int countEffected = 0;
    for (DataRow *curRow in _rows) {
        curRow.tableName = tableName;
        countEffected += [curRow commitChangeDBRow];
    }
    return countEffected;
}

//null rows is delete all row
-(int) dropDBRows
{
    int countEffected = 0;
    if([_rows count] > 0)
    {
        for (DataRow *curRow in _rows) {
            countEffected += [curRow dropRow];
        }
    }
    else
    {
        DataRow *curRow = [[DataRow alloc] init];
        curRow.tableName = tableName;
        [curRow dropRow];
    }
    return countEffected;
}

-(int) insertDBRows
{
    int countEffected = 0;
    for (DataRow *curRow in _rows) {
        countEffected += [curRow insertDBRow];
    }
    return countEffected;
}

//load database
//Return true if load success
-(BOOL) loadTableDataWithReferenceRow:(DataRow*)rowData conditions:(NSMutableArray*)columnsCondition
{
    rowData.tableName = tableName;
    
    BOOL success = NO;
    DataBaseExtension *dbManager = [[DataBaseExtension alloc]init] ;
    [dbManager openDatabase];
    [dbManager resetSqlCommand];
    NSMutableArray *resultRows = [dbManager selectRows:rowData inTable:tableName conditions:columnsCondition othersFilter:nil];
    [dbManager close];
    
    if([resultRows count] > 0){
        [_rows removeAllObjects];
        [_rows addObjectsFromArray:resultRows];
        success = YES;
    }
    return success;
}

//load database
//Return true if load success
-(BOOL) loadTableDataWithQuery:(NSString*)stringQuery referenceRow:(DataRow*)rowData
{
    rowData.tableName = tableName;
    BOOL success = NO;
    DataBaseExtension *dbManager = [[DataBaseExtension alloc]init] ;
    [dbManager openDatabase];
    [dbManager resetSqlCommand];
    
    NSMutableArray *resultRows = [dbManager excuteQuerySelect:stringQuery referenceRow:rowData];
    
    [dbManager close];
    
    if([resultRows count] > 0){
        [_rows removeAllObjects];
        [_rows addObjectsFromArray:resultRows];
        success = YES;
    }
    return success;
}

//excute query non select
//Return interger >0 if success
-(NSInteger) excuteQueryNonSelect:(NSString*)stringQuery
{
    
    DataBaseExtension *dbManager = [[DataBaseExtension alloc]init] ;
    [dbManager openDatabase];
    [dbManager resetSqlCommand];
    
    NSInteger result = [dbManager excuteQueryNonSelect:stringQuery];
    
    [dbManager close];
    
    return result;
}

@end



#pragma mark - Database Extension

@implementation DataBaseExtension

@synthesize isWriteLogConsole;

//return row id inserted
-(int) insertTable:(NSString*) tableName fields:(NSMutableArray*)fields
{
    if(isWriteLogConsole && [LogFileManager isLogConsole]){
        NSLog(@"INSERT %@", tableName);
    }
    
    if(tableName.length <= 0){
        if(isWriteLogConsole && [LogFileManager isLogConsole]){
            NSLog(@"INSERT FAIL");
        }
        return 0;
    }
    
    if([fields count] <= 0){
        if(isWriteLogConsole && [LogFileManager isLogConsole]){
            NSLog(@"INSERT FAIL");
        }
        return 0;
    }
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithString:@"insert into "];
    [sqlString appendString:tableName];
    
    //fields name
    NSMutableString *fieldsName = [[NSMutableString alloc] initWithString:@" ("];
    
    //fields value
    NSMutableString *fieldsValue = [[NSMutableString alloc] initWithString:@" Values("];
    
    
    //Remove fieldsString
    NSMutableArray *fieldsString = [NSMutableArray array];
    
    for (int i = 0; i < [fields count]; i++) {
        DatabaseField *currentFields = [fields objectAtIndex:i];
        if(currentFields.fieldName != nil) {
            if(currentFields.dataType == DataType_String)
            {
                [fieldsName appendString: currentFields.fieldName];
                [fieldsValue appendString: [NSString stringWithFormat:@"'%@'",currentFields.fieldValue]];
                [fieldsString addObject:currentFields];
            }
            else
            {
                [fieldsName appendString: currentFields.fieldName];
                [fieldsValue appendString: @"?"];
            }
            //LOG
            [self LogField:currentFields];
        } else {
            if(isWriteLogConsole && [LogFileManager isLogConsole]){
                NSLog(@"FAIL");
            }
            return 0;
        }
        
        if(i >= [fields count] - 1) {
            [fieldsName appendString:@") "];
            [fieldsValue appendString:@")"];
        } else {
            [fieldsName appendString:@", "];
            [fieldsValue appendString:@", "];
        }
    }
    
    //Remove object in fields string because it was already bind in string sql
    [fields removeObjectsInArray:fieldsString];
    
    [sqlString appendString:fieldsName];
    [sqlString appendString:fieldsValue];
    if(isWriteLogConsole){
        [LogFileManager logDebugMode:@"%@", sqlString];
    }
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    reAccessCount ++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK){
    }
    
    //Bind data
    [self bindDataFields:fields startIndex:0];
    
    NSInteger status =  sqlite3_step(self.sqlStament);
    if(SQLITE_DONE != status)
    {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return 0;
    }
    else
    {
        int result = (int)sqlite3_last_insert_rowid(database);
        if(result == 0) {
            result = 1;
        }
        return result;
    }
}

//return 1 : update ok
//return 0 : update failed
-(int) updateTable:(NSString*) tableName fields:(NSMutableArray*)fields conditions:(NSMutableArray*)conditionFields
{
    if(isWriteLogConsole && [LogFileManager isLogConsole]){
        NSLog(@"UPDATE");
    }
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"UPDATE %@", tableName];
    
    //Remove fieldsString
    NSMutableArray *fieldsString = [NSMutableArray array];
    
    //fields name
    NSMutableString *fieldsName = [[NSMutableString alloc] initWithString:@" SET "];
    
    int countNumberOfField = 0;
    for (int i = 0; i < [fields count]; i++) {
        DatabaseField *currentFields = [fields objectAtIndex:i];
        if(currentFields.fieldName != nil) {
            if (currentFields.dataType == DataType_String)
            {
                [fieldsName appendString: [NSString stringWithFormat:@"%@ = '%@'", currentFields.fieldName, currentFields.fieldValue]];
                [fieldsString addObject:currentFields];
            }
            else
            {
                [fieldsName appendString: [NSString stringWithFormat:@"%@ = ?", currentFields.fieldName]];
                countNumberOfField ++;
            }
            [self LogField:currentFields];
        } else {
            if(isWriteLogConsole && [LogFileManager isLogConsole]){
                NSLog(@"FAIL");
            }
            return 0;
        }
        
        if(i >= [fields count] - 1) {
            [fieldsName appendString:@" "];
        } else {
            [fieldsName appendString:@", "];
        }
    }
    
    //Remove field string
    [fields removeObjectsInArray:fieldsString];
    
    NSString *fieldsCondition = [self conditionStringFromFields:conditionFields];
    
    [sqlString appendString:fieldsName];
    [sqlString appendString:@" "];
    [sqlString appendString:fieldsCondition];
    
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    reAccessCount ++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK){
    }
    
    //Bind data
    [self bindDataFields:fields startIndex:0];
    [self bindDataFields:conditionFields startIndex:countNumberOfField];
     
    NSInteger status = sqlite3_step(self.sqlStament);
    
    if(SQLITE_DONE != status){
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        if(isWriteLogConsole && [LogFileManager isLogConsole]){
            NSLog(@"Fail");
        }
        return 0;
    }
    else{
        return  1; // update ok -> return 1
    }
}

//return 1 : update ok
//return 0 : update failed
-(int) deleteTable:(NSString*) tableName conditions:(NSMutableArray*)conditionFields
{
    NSInteger result=1;

    if(isWriteLogConsole && [LogFileManager isLogConsole]){
        NSLog(@"Delete %@", tableName);
    }
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"delete from %@ ",tableName];
    NSString *conditionString = [self conditionStringFromFields:conditionFields];
    
    [sqlString appendString:conditionString];
    
    const char *sql=[sqlString UTF8String];
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
        result=0;
	
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    reAccessCount ++;
}
    
    [self bindDataFields:conditionFields startIndex:0];
	
    NSInteger  status = sqlite3_step(self.sqlStament);
    
	if (SQLITE_DONE != status) {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        if(isWriteLogConsole && [LogFileManager isLogConsole])
        {
            NSLog(@"Delete fail");
        }
		result = 0;
    }
    
    return (int)result;
}

//return listRows data name and value of fields
//Conditions = nil : get all data
//Other filter for order by, having count ....
-(NSMutableArray*) selectFields:(NSMutableArray*)fields inTable:(NSString*)tableName conditions:(NSMutableArray*)conditionFields othersFilter:(NSString*)othersFilter
{
    NSMutableArray *resultRows = nil;
    if(tableName.length <= 0){
        return nil;
    }
    
    if([fields count] <= 0)
    {
        return nil;
    }
    
    NSString *selectString = [self selectStringFromFields:fields];
    if(selectString.length <= 0) {
        return nil;
    }
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"%@ FROM %@ ", selectString, tableName];
    
    NSString *conditionString = [self conditionStringFromFields:conditionFields];
    
    [sqlString appendString:conditionString];
    if(othersFilter.length > 0){
        [sqlString appendString:othersFilter];
    }
    
    const char *sql =[sqlString UTF8String];
    resultRows = [[NSMutableArray alloc] init];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    reAccessCount ++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    
    [self bindDataFields:conditionFields startIndex:0];
    
    NSInteger status =  sqlite3_step(self.sqlStament);
    while(status == SQLITE_ROW)
    {
        NSMutableArray *rowData = [self rowDataFromFields:fields];
        [resultRows addObject:rowData];
        
        status = sqlite3_step(sqlStament);
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    return resultRows;
}

//return array of DataRow
//Conditions = nil : get all data
//Other filter for order by, having count ....
-(NSMutableArray*) selectRows:(DataRow*)rowData inTable:(NSString*)tableName conditions:(NSMutableArray*)conditionFields othersFilter:(NSString*)othersFilter
{
    NSMutableArray *resultRows = nil;
    if(tableName.length <= 0){
        return nil;
    }
    
    if(rowData == nil)
    {
        return nil;
    }
    
    if([rowData.columns count] <= 0)
    {
        return nil;
    }
    
    NSString *selectString = [self selectStringFromFields:rowData.columns];
    if(selectString.length <= 0) {
        return nil;
    }
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"%@ FROM %@ ", selectString, tableName];
    
    NSString *conditionString = [self conditionStringFromFields:conditionFields];
    
    [sqlString appendString:conditionString];
    if(othersFilter.length > 0){
        [sqlString appendString:othersFilter];
    }
    
    const char *sql =[sqlString UTF8String];
    resultRows = [[NSMutableArray alloc] init];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    reAccessCount ++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    
    [self bindDataFields:conditionFields startIndex:0];
    
    NSInteger status =  sqlite3_step(self.sqlStament);
    while(status == SQLITE_ROW)
    {
        DataRow *rowResult = [[DataRow alloc] init];
        rowResult.tableName = rowData.tableName;
        rowResult.columns = [self rowDataFromFields:rowData.columns];
        [resultRows addObject:rowResult];
        
        status = sqlite3_step(sqlStament);
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    return resultRows;
}

//return array of DataRow
-(NSMutableArray*) excuteQuerySelect:(NSString*)queryString referenceRow:(DataRow*)rowData
{
    
    NSMutableArray *resultRows = nil;
    if(queryString.length <= 0){
        return nil;
    }
    
    NSString *sqlString = [NSString stringWithString: queryString];
    
    const char *sql =[sqlString UTF8String];
    resultRows = [[NSMutableArray alloc] init];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    reAccessCount ++;
}

    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        status = sqlite3_step(sqlStament);
        while(status == SQLITE_ROW)
        {
            DataRow *resultRow = [[DataRow alloc] init];
            resultRow.tableName = rowData.tableName;
            
            resultRow.columns = [self rowDataFromFields:rowData.columns];
            [resultRows addObject:resultRow];
            status = sqlite3_step(sqlStament);
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    return resultRows;
}

//return status sqlstatement
-(NSInteger) excuteQueryNonSelect:(NSString*)queryString
{
    
    NSMutableArray *resultRows = nil;
    if(queryString.length <= 0){
        return nil;
    }
    
    NSString *sqlString = [NSString stringWithString: queryString];
    
    const char *sql =[sqlString UTF8String];
    resultRows = [[NSMutableArray alloc] init];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    reAccessCount ++;
}
    
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        status = sqlite3_step(sqlStament);
        while(status == SQLITE_ROW)
        {
            status = sqlite3_step(sqlStament);
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    return status;
}

//return array DataField name and value of fields
-(NSMutableArray*) excuteQuerySelect:(NSString*)queryString fields:(NSMutableArray*)selectedFields
{
    
    NSMutableArray *resultRows = nil;
    if(queryString.length <= 0){
        return nil;
    }
    
    NSString *sqlString = [NSString stringWithString: queryString];
    
    const char *sql =[sqlString UTF8String];
    resultRows = [[NSMutableArray alloc] init];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    reAccessCount ++;
}
    
//    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
//    }
//    
//    NSInteger status =  sqlite3_step(self.sqlStament);
//    while(status == SQLITE_ROW)
//    {
//        NSMutableArray *rowData = [self rowDataFromFields:selectedFields];
//        [resultRows addObject:rowData];
//        
//        status = sqlite3_step(sqlStament);
//    }
    
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        status = sqlite3_step(sqlStament);
        while(status == SQLITE_ROW)
        {
            NSMutableArray *rowData = [self rowDataFromFields:selectedFields];
            [resultRows addObject:rowData];
            status = sqlite3_step(sqlStament);
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    return resultRows;
}

#pragma mark - Data creator

-(DatabaseField*)createStringField:(NSString*)fieldName value:(NSString*)stringValue
{
    DatabaseField *dbField = [[DatabaseField alloc] initWith:DataType_String fieldName:fieldName fieldValue:stringValue];
    return dbField;
}

-(DatabaseField*)createIntField:(NSString*)fieldName value:(int)intValue
{
    DatabaseField *dbField = [[DatabaseField alloc] initWith:DataType_Int fieldName:fieldName fieldValue:[NSNumber numberWithInt:intValue]];
    return dbField;
}

-(DatabaseField*)createFloatField:(NSString*)fieldName value:(float)floatValue
{
    DatabaseField *dbField = [[DatabaseField alloc] initWith:DataType_Float fieldName:fieldName fieldValue:[NSNumber numberWithFloat:floatValue]];
    return dbField;
}

-(DatabaseField*)createLongField:(NSString*)fieldName value:(long)longValue
{
    DatabaseField *dbField = [[DatabaseField alloc] initWith:DataType_Long fieldName:fieldName fieldValue:[NSNumber numberWithLong:longValue]];
    return dbField;
}

-(DatabaseField*)createDoubleField:(NSString*)fieldName value:(double)doubleValue
{
    DatabaseField *dbField = [[DatabaseField alloc] initWith:DataType_Double fieldName:fieldName fieldValue:[NSNumber numberWithDouble:doubleValue]];
    return dbField;
}

-(DatabaseField*)createDataField:(NSString*)fieldName value:(NSData*)dataValue
{
    DatabaseField *dbField = [[DatabaseField alloc] initWith:DataType_Data fieldName:fieldName fieldValue:dataValue];
    return dbField;
}

//MARK: Create field with null value
-(DatabaseField*)createStringField:(NSString*)fieldName
{
    DatabaseField *dbField = [[DatabaseField alloc] initWith:DataType_String fieldName:fieldName fieldValue:nil];
    return dbField;
}

-(DatabaseField*)createIntField:(NSString*)fieldName
{
    DatabaseField *dbField = [[DatabaseField alloc] initWith:DataType_Int fieldName:fieldName fieldValue:nil];
    return dbField;
}

-(DatabaseField*)createFloatField:(NSString*)fieldName
{
    DatabaseField *dbField = [[DatabaseField alloc] initWith:DataType_Float fieldName:fieldName fieldValue:nil];
    return dbField;
}

-(DatabaseField*)createLongField:(NSString*)fieldName
{
    DatabaseField *dbField = [[DatabaseField alloc] initWith:DataType_Long fieldName:fieldName fieldValue:nil];
    return dbField;
}

-(DatabaseField*)createDoubleField:(NSString*)fieldName
{
    DatabaseField *dbField = [[DatabaseField alloc] initWith:DataType_Double fieldName:fieldName fieldValue:nil];
    return dbField;
}

-(DatabaseField*)createDataField:(NSString*)fieldName
{
    DatabaseField *dbField = [[DatabaseField alloc] initWith:DataType_Data fieldName:fieldName fieldValue:nil];
    return dbField;
}

#pragma mark - Private functions

-(NSString*) selectStringFromFields:(NSMutableArray*)selectedFields
{
    if([selectedFields count] > 0)
    {
        //fields condition
        NSMutableString *selectFieldsString = [[NSMutableString alloc] initWithString:@"SELECT "];
        if(isWriteLogConsole && [LogFileManager isLogConsole]){
            NSLog(@"SELECT ");
        }
        
        for (int i = 0; i < [selectedFields count]; i++) {
            DatabaseField *currentFields = [selectedFields objectAtIndex:i];
            if(currentFields.fieldName != nil) {
                
                [selectFieldsString appendString: [NSString stringWithFormat:@"%@", currentFields.fieldName]];
                [self LogField:currentFields];
            } else {
                return @"";
            }
            
            if(i >= [selectedFields count] - 1) {
                [selectFieldsString appendString:@""];
            } else {
                [selectFieldsString appendString:@", "];
            }
        }
        
        return selectFieldsString;
    }
    else
    {
        return @"";
    }
}

-(NSString*) conditionStringFromFields:(NSMutableArray*)conditionFields
{
    if([conditionFields count] > 0)
    {
        //fields condition
        NSMutableString *fieldsCondition = [[NSMutableString alloc] initWithString:@"WHERE "];
        if(isWriteLogConsole && [LogFileManager isLogConsole]){
            NSLog(@"WHERE ");
        }
        
        for (int i = 0; i < [conditionFields count]; i++) {
            DatabaseField *currentFields = [conditionFields objectAtIndex:i];
            if(currentFields.fieldName != nil) {
                
                if(currentFields.dataType == DataType_String)
                {
                    [fieldsCondition appendString: [NSString stringWithFormat:@"%@ like '%@'", currentFields.fieldName, currentFields.fieldValue]];
                }
                else
                {
                    [fieldsCondition appendString: [NSString stringWithFormat:@"%@ = ?", currentFields.fieldName]];
                }
                
                [self LogField:currentFields];
            } else {
                return @"";
            }
            
            if(i >= [conditionFields count] - 1) {
                [fieldsCondition appendString:@""];
            } else {
                [fieldsCondition appendString:@" AND "];
            }
        }
        
        return fieldsCondition;
    }
    else
    {
        return @"";
    }
}

-(void) bindDataFields:(NSMutableArray*)fieldsData startIndex:(int)startIndex
{
    for (int i = 0; i < [fieldsData count]; i++) {
        DatabaseField *currentField = [fieldsData objectAtIndex:i];
        
//        if(currentField.dataType == DataType_String)
//        {
//            int indexRow = startIndex + i + 1;
//            NSString *valueString = (NSString*)currentField.fieldValue;
//            sqlite3_bind_text(self.sqlStament,indexRow , [valueString UTF8String], -1, SQLITE_TRANSIENT);
//        }
//        else

        if(currentField.dataType == DataType_Int)
        {
            int indexRow = startIndex + i + 1 ;
            int valueInt = [(NSNumber*)currentField.fieldValue intValue];
            sqlite3_bind_int (self.sqlStament, indexRow, valueInt);
        }
        else if(currentField.dataType == DataType_Long)
        {
            int indexRow = startIndex + i + 1;
            long valueLong = [(NSNumber*)currentField.fieldValue longValue];
            sqlite3_bind_int64 (self.sqlStament, indexRow, valueLong);
        }
        else if(currentField.dataType == DataType_Float)
        {
            int indexRow = startIndex + i + 1;
            int valueFloat = [(NSNumber*)currentField.fieldValue floatValue];
            sqlite3_bind_double (self.sqlStament, indexRow, valueFloat);
        }
        else if(currentField.dataType == DataType_Double)
        {
            int indexRow = startIndex + i + 1;
            double valueDouble = [(NSNumber*)currentField.fieldValue doubleValue];
            sqlite3_bind_int (self.sqlStament, indexRow, valueDouble);
        }
        else if(currentField.dataType == DataType_Data)
        {
            int indexRow = startIndex + i + 1;
            NSData *valueData = currentField.fieldValue;
            sqlite3_bind_blob(self.sqlStament, indexRow, [valueData bytes], (int)[valueData length], NULL);
        }
    }
}

-(NSMutableArray*)rowDataFromFields:(NSMutableArray*)fieldsData
{
    NSMutableArray *fieldsResult = [[NSMutableArray alloc]init];
    
    int maxIndex = (int)([fieldsData count] - 1);
    int curIndex = 0;
    
    while (curIndex <= maxIndex) {
        
        DatabaseField *currentDataField = nil;
        id currentField = [fieldsData objectAtIndex:curIndex];
        
        if([currentField isKindOfClass:[DataColumn class]])
        {
            currentDataField = [[DataColumn alloc] init];
            currentDataField.dataType = ((DataColumn*)currentField).dataType;
            currentDataField.fieldName = ((DataColumn*)currentField).fieldName;
            ((DataColumn*)currentDataField).isKeyOnInsert = ((DataColumn*)currentField).isKeyOnInsert;
            ((DataColumn*)currentDataField).isUpdateCondition = ((DataColumn*)currentField).isUpdateCondition;
        }
        else
        {
            currentDataField = [[DatabaseField alloc] init];
            currentDataField.dataType = ((DatabaseField*)currentField).dataType;
            currentDataField.fieldName = ((DatabaseField*)currentField).fieldName;
        }
        
        if(currentDataField.dataType == DataType_String){
            if(sqlite3_column_text(sqlStament,curIndex)!=nil) {
                currentDataField.fieldValue = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, curIndex)];
            }
        } else if(currentDataField.dataType == DataType_Int){
            int value = sqlite3_column_int(sqlStament, curIndex);
            currentDataField.fieldValue = [NSNumber numberWithInt: value];
        } else if(currentDataField.dataType == DataType_Float){
            currentDataField.fieldValue = [NSNumber numberWithFloat: sqlite3_column_double(sqlStament, curIndex)];
        }else if(currentDataField.dataType == DataType_Long){
            currentDataField.fieldValue = [NSNumber numberWithLong: sqlite3_column_int64(sqlStament, curIndex)];
        }else if(currentDataField.dataType == DataType_Double){
            currentDataField.fieldValue = [NSNumber numberWithDouble: sqlite3_column_double(sqlStament, curIndex)];
        }else if(currentDataField.dataType == DataType_Data) {
            if (sqlite3_column_bytes(self.sqlStament, curIndex) > 0) {
            currentDataField.fieldValue = [NSData dataWithBytes:sqlite3_column_blob (self.sqlStament, curIndex) length:sqlite3_column_bytes(self.sqlStament, curIndex)];
            }
        }
        
        [fieldsResult addObject:currentDataField];
        
        curIndex ++;
    }
    return fieldsResult;
}


#pragma mark - Log Fields

- (void)LogField:(DatabaseField*)currentField
{
    if(isWriteLogConsole && [LogFileManager isLogConsole]) {
        //Cast data
        if(currentField.dataType == DataType_String){
            NSString *stringLog = [NSString stringWithFormat:@"%@ = %@", currentField.fieldName, (NSString*)currentField.fieldValue];
            NSLog(@"%@",stringLog);
            
        } else if(currentField.dataType == DataType_Int) {
            NSString *stringData = [NSString stringWithFormat:@"%d", [((NSNumber*)currentField.fieldValue) intValue]];
            NSString *stringLog = [NSString stringWithFormat:@"%@ = %@", currentField.fieldName, stringData];
            NSLog(@"%@",stringLog);
            
        } else if(currentField.dataType == DataType_Long) {
            NSString *stringData = [NSString stringWithFormat:@"%ld", [((NSNumber*)currentField.fieldValue) longValue]];
            NSString *stringLog = [NSString stringWithFormat:@"%@ = %@", currentField.fieldName, stringData];
            NSLog(@"%@",stringLog);
            
        } else if(currentField.dataType == DataType_Double){
            NSString *stringData = [NSString stringWithFormat:@"%lf", [((NSNumber*)currentField.fieldValue) doubleValue]];
            NSString *stringLog = [NSString stringWithFormat:@"%@ = %@", currentField.fieldName, stringData];
            NSLog(@"%@",stringLog);
            
        } else if(currentField.dataType == DataType_Float) {
            NSString *stringData = [NSString stringWithFormat:@"%f", [((NSNumber*)currentField.fieldValue) floatValue]];
            NSString *stringLog = [NSString stringWithFormat:@"%@ = %@", currentField.fieldName, stringData];
            NSLog(@"%@",stringLog);
        }
    }
}

@end
