//
//  AdditionalJobManagerV2.m
//  mHouseKeeping
//
//  Created by vinhnguyen on 7/13/13.
//
//

#import "AdditionalJobManagerV2Demo.h"
#import "DataBaseExtension.h"
#import "DbDefinesV2.h"
#import "ehkDefines.h"
#import "AddJobFloorModelV2.h"
#import "AddJobRoomModelV2.h"
#import "AddJobCategoryModelV2.h"
#import "AddJobDetailModelV2.h"
#import "AddJobRoomItemModelV2.h"
#import "FloorModelV2.h"
#import "LogFileManager.h"
#import "LanguageManagerV2.h"
#import "AddJobDetailRecordModelV2.h"
#import "AddJobGuestInfoModelV2.h"
#import "NetworkCheck.h"
#import "AddJobStatusModel.h"
#include <stdio.h>
#include <stdarg.h>

@implementation AdditionalJobManagerV2Demo

//static AdditionalJobManagerV2* sharedAddJobManagerInstance = nil;
//
//+ (AdditionalJobManagerV2*) sharedAddionalJobManager{
//    
//    static dispatch_once_t onceToken;
//    dispatch_once(&onceToken, ^{
//        sharedAddJobManagerInstance = [[AdditionalJobManagerV2 alloc] init];
//    });
//    
//    return sharedAddJobManagerInstance;
//}

#pragma mark - WS FUNCTIONS
//WS Floor Function
//pass tasksFilter = nil for get all additional job floors
-(bool) loadWSAddJobFloorListByUserId:(int)userId hotelId:(int)hotelId tasksFilter:(NSMutableArray*)listTasksId
{
    bool result = NO;
    //No network detected
    if(![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]){
        return result;
    }
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole]){
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_GetAdditionalJobFloorList *request = [[eHousekeepingService_GetAdditionalJobFloorList alloc] init];
    
    [request setIntUserId:[NSNumber numberWithInt:userId]];
    [request setIntHotelID:[NSNumber numberWithInt:hotelId]];
    
    int searchId = 0;
    NSString *todayDateString = [ehkConvert getTodayDateString];
    NSString *taskListString = nil;
    
    if([listTasksId count] > 0){
        taskListString = [self convertListTasksIdToString:listTasksId];
        [request setTaskIDs: taskListString];
    }
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetAdditionalJobFloorListUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetAdditionalJobFloorListResponse class]]) {
            eHousekeepingService_GetAdditionalJobFloorListResponse *dataBody = (eHousekeepingService_GetAdditionalJobFloorListResponse *)bodyPart;
            if (dataBody != nil) {
                if ([dataBody.GetAdditionalJobFloorListResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_OK]]
                    || [dataBody.GetAdditionalJobFloorListResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_NO_RERCORD_TO_DISPLAY]]) {
                    result = YES; //success
                    [self deleteAllAddJobFloors];
                    NSMutableArray *addJobFloorList = dataBody.GetAdditionalJobFloorListResult.AdditionalJobFloorList.AdditionalJobFloor;
                    //get search ID
                    if([listTasksId count] > 0){
                        AddJobSearchModelV2 *searchModel = [self loadAddJobSearchByUserId:userId tasksFilter:listTasksId];
                        if(searchModel){
                            searchId = (int)searchModel.addjob_search_id;
                        } else {
                            AddJobSearchModelV2 *searchModel = [[AddJobSearchModelV2 alloc] init];
                            searchModel.addjob_search_last_modified = todayDateString;
                            searchModel.addjob_search_tasks_id = taskListString;
                            searchModel.addjob_search_user_id = userId;
                            searchId = [self insertAddJobSearch:searchModel];
                        }
                    }
                    
                    if([addJobFloorList count] > 0) {
                        
                        //Insert or update AddJobFloor
                        AddJobFloorModelV2 *addJobFloor = nil;
                        for (eHousekeepingService_AdditionalJobFloor *curAddJobFloor in addJobFloorList) {
                            
                            addJobFloor = [self loadAddJobFloorByUserId:userId floorId:[curAddJobFloor.floorID intValue] searchId:searchId];
                            if(addJobFloor){
                                addJobFloor.addjob_floor_id = [curAddJobFloor.floorID intValue];
                                addJobFloor.addjob_floor_name = curAddJobFloor.floorName;
                                addJobFloor.addjob_floor_name_lang = curAddJobFloor.floorLang;
                                addJobFloor.addjob_floor_last_modified = todayDateString;
                                addJobFloor.addjob_floor_number_of_job = [curAddJobFloor.numberOfJob intValue];
                                addJobFloor.addjob_floor_search_id = searchId;
                                addJobFloor.addjob_floor_user_id = userId;
                                
                                [self updateAddJobFloor:addJobFloor];
                            } else {
                                addJobFloor = [[AddJobFloorModelV2 alloc] init];
                                addJobFloor.addjob_floor_id = [curAddJobFloor.floorID intValue];
                                addJobFloor.addjob_floor_name = curAddJobFloor.floorName;
                                addJobFloor.addjob_floor_name_lang = curAddJobFloor.floorLang;
                                addJobFloor.addjob_floor_last_modified = todayDateString;
                                addJobFloor.addjob_floor_number_of_job = [curAddJobFloor.numberOfJob intValue];
                                addJobFloor.addjob_floor_search_id = searchId;
                                addJobFloor.addjob_floor_user_id = userId;
                                
                                [self insertAddJobFloor:addJobFloor];
                            }
                        }
                    }
                }
            }
        }
    }
    
    return result;
}

//WS Add Job Item
-(bool) loadWSAddJobItemListByUserId:(int)userId hotelId:(int)hotelId
{
    bool result = NO;
    //No network detected
    if(![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]){
        return result;
    }
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole]){
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_GetAdditionalJobTask *request = [[eHousekeepingService_GetAdditionalJobTask alloc] init];
    
    [request setIntHotelID:[NSNumber numberWithInt:hotelId]];
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetAdditionalJobTaskUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetAdditionalJobTaskResponse class]]) {
            eHousekeepingService_GetAdditionalJobTaskResponse *dataBody = (eHousekeepingService_GetAdditionalJobTaskResponse *)bodyPart;
            if (dataBody != nil) {
                if ([dataBody.GetAdditionalJobTaskResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_OK]]) {
                    result = YES; //success
                    NSMutableArray *addJobTasks = dataBody.GetAdditionalJobTaskResult.JobTaskList.JobTask;
                    if([addJobTasks count] > 0) {
                        
                        //Insert or update AddJobItem
                        AddJobItemModelV2 *addJobItem = nil;
                        NSString *todayDateString = [ehkConvert getTodayDateString];
                        
                        for (eHousekeepingService_JobTask *curJobItem in addJobTasks) {
                            
                            addJobItem = [self loadAddJobItemByUserId:userId itemId:[curJobItem.taskID intValue]];
                            
                            if(addJobItem){
                                addJobItem.addjob_item_id = [curJobItem.taskID intValue];
                                addJobItem.addjob_item_user_id = userId;
                                addJobItem.addjob_item_name = curJobItem.taskName;
                                addJobItem.addjob_item_name_lang = curJobItem.taskLang;
                                addJobItem.addjob_item_name_lang2 = curJobItem.taskLang2;
                                addJobItem.addjob_item_last_modified = todayDateString;
                                addJobItem.addjob_item_onday_addjob_id = 0;
                                [self updateAddJobItem:addJobItem];
                                
                            } else {
                                addJobItem = [[AddJobItemModelV2 alloc] init];
                                addJobItem.addjob_item_id = [curJobItem.taskID intValue];
                                addJobItem.addjob_item_user_id = userId;
                                addJobItem.addjob_item_name = curJobItem.taskName;
                                addJobItem.addjob_item_name_lang = curJobItem.taskLang;
                                addJobItem.addjob_item_name_lang2 = curJobItem.taskLang2;
                                addJobItem.addjob_item_last_modified = todayDateString;
                                addJobItem.addjob_item_onday_addjob_id = 0;
                                
                                [self insertAddJobItem:addJobItem];
                            }
                        }
                    }
                }
            }
        }
    }
    
    return result;
}

//WS Add Job Room
-(bool) loadWSAddJobRoomListByUserId:(int)userId hotelId:(int)hotelId floorId:(int)floorId tasksFilter:(NSMutableArray*)listItems
{
    bool result = NO;
    //No network detected
    if(![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]){
        return result;
    }
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole]){
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_GetAdditionalJobRoomByFloor *request = [[eHousekeepingService_GetAdditionalJobRoomByFloor alloc] init];
    
    [request setIntHotelID:[NSNumber numberWithInt:hotelId]];
    [request setIntFloorID:[NSNumber numberWithInt:floorId]];
    [request setIntUserID:[NSNumber numberWithInt:userId]];
    
    int searchId = 0;
    NSString *todayDateString = [ehkConvert getTodayDateString];
    NSString *taskListString = nil;
    
    if(listItems != nil && [listItems count] > 0){
        taskListString = [self convertListTasksIdToString:listItems];
        [request setTaskIds:taskListString];
        AddJobSearchModelV2 *searchModel = [self loadAddJobSearchByUserId:userId tasksFilter:listItems];
        searchId = (int)searchModel.addjob_search_id;
    }
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetAdditionalJobRoomByFloorUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetAdditionalJobRoomByFloorResponse class]]) {
            eHousekeepingService_GetAdditionalJobRoomByFloorResponse *dataBody = (eHousekeepingService_GetAdditionalJobRoomByFloorResponse *)bodyPart;
            if (dataBody != nil) {
                if ([dataBody.GetAdditionalJobRoomByFloorResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_OK]]
                    || [dataBody.GetAdditionalJobRoomByFloorResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_NO_RERCORD_TO_DISPLAY]]) {
                    
                    result = YES; //success
                    
                    //Delete old data
                    [self deleteAddJobRoomByUserId:userId floorId:floorId searchId:searchId];
                    
                    NSMutableArray *addJobRooms = dataBody.GetAdditionalJobRoomByFloorResult.AdditionalJobRoomList.AdditionalJobRoom;
                    
                    //get search ID
                    if([listItems count] > 0){
                        AddJobSearchModelV2 *searchModel = [self loadAddJobSearchByUserId:userId tasksFilter:listItems];
                        if(searchModel){
                            searchId = (int)searchModel.addjob_search_id;
                        } else {
                            searchModel = [[AddJobSearchModelV2 alloc] init];
                            searchModel.addjob_search_last_modified = todayDateString;
                            searchModel.addjob_search_tasks_id = taskListString;
                            searchModel.addjob_search_user_id = userId;
                            searchId = [self insertAddJobSearch:searchModel];
                        }
                    }
                    
                    if([addJobRooms count] > 0) {
                        
                        AddJobRoomModelV2 *addJobRoom = nil;
                        NSString *todayDateString = [ehkConvert getTodayDateString];
                        
                        for (eHousekeepingService_AdditionalJobRoom *curJobRoom in addJobRooms) {
                            
                            //Additional Job Room
                            addJobRoom = [self loadAddJobRoomByUserId:userId roomId:[curJobRoom.roomId intValue] roomAssignId:0 searchId:searchId];
                            if(addJobRoom){
                                addJobRoom.addjob_room_assign_id = 0;
                                addJobRoom.addjob_room_floor_id = floorId;
                                addJobRoom.addjob_room_id = [curJobRoom.roomId intValue];
                                addJobRoom.addjob_room_is_due_out = [curJobRoom.isDueOutRoom boolValue] ? 1 : 0;
                                addJobRoom.addjob_room_last_modified = todayDateString;
                                addJobRoom.addjob_room_number = curJobRoom.roomNo;
                                addJobRoom.addjob_room_number_pending_job = [curJobRoom.totalPendingAdditionalJobList intValue];
                                addJobRoom.addjob_room_search_id = searchId;
                                addJobRoom.addjob_room_status_id = [curJobRoom.roomStatusID intValue];
                                addJobRoom.addjob_room_type_id = [curJobRoom.roomTypeID intValue];
                                addJobRoom.addjob_room_user_id = userId;
                                [self updateAddJobRoom:addJobRoom];
                            } else {
                                addJobRoom = [[AddJobRoomModelV2 alloc] init];
                                addJobRoom.addjob_room_assign_id = 0;
                                addJobRoom.addjob_room_floor_id = floorId;
                                addJobRoom.addjob_room_id = [curJobRoom.roomId intValue];
                                addJobRoom.addjob_room_is_due_out = [curJobRoom.isDueOutRoom boolValue] ? 1 : 0;
                                addJobRoom.addjob_room_last_modified = todayDateString;
                                addJobRoom.addjob_room_number = curJobRoom.roomNo;
                                addJobRoom.addjob_room_number_pending_job = [curJobRoom.totalPendingAdditionalJobList intValue];
                                addJobRoom.addjob_room_remark = curJobRoom.remark;
                                addJobRoom.addjob_room_search_id = searchId;
                                addJobRoom.addjob_room_status_id = [curJobRoom.roomStatusID intValue];
                                addJobRoom.addjob_room_type_id = [curJobRoom.roomTypeID intValue];
                                addJobRoom.addjob_room_user_id = userId;
                                [self insertAddJobRoom:addJobRoom];
                            }
                            
                            for (eHousekeepingService_AdditionalJob *curAddJobDetail in curJobRoom.pendingAdditionalJobList.AdditionalJob) {
                                
                                //Additional Job Category
                                AddJobCategoryModelV2 *curCategory = [self loadAddJobCategoryByUserId:userId categoryId:[curAddJobDetail.additionalJobId intValue]];
                                if(curCategory){
                                    curCategory.addjob_category_id = [curAddJobDetail.additionalJobId intValue];
                                    curCategory.addjob_category_last_modified = todayDateString;
                                    curCategory.addjob_category_name = curAddJobDetail.additionalJobName;
                                    curCategory.addjob_category_name_lang = curAddJobDetail.additionalJobNamend;
                                    curCategory.addjob_category_name_lang2 = curAddJobDetail.additionalJobNamerd;
                                    curCategory.addjob_category_user_id = userId;
                                    
                                    [self updateAddJobCategory:curCategory];
                                } else {
                                    curCategory = [[AddJobCategoryModelV2 alloc] init];
                                    curCategory.addjob_category_id = [curAddJobDetail.additionalJobId intValue];
                                    curCategory.addjob_category_last_modified = todayDateString;
                                    curCategory.addjob_category_name = curAddJobDetail.additionalJobName;
                                    curCategory.addjob_category_name_lang = curAddJobDetail.additionalJobNamend;
                                    curCategory.addjob_category_name_lang2 = curAddJobDetail.additionalJobNamerd;
                                    curCategory.addjob_category_user_id = userId;
                                    
                                    [self insertAddJobCategory:curCategory];
                                }
                                
                                //Additional Job Detail
                                AddJobDetailModelV2 *curDetail = [self loadAddJobDetailByUserId:userId ondayAddJobId:[curAddJobDetail.ondayAdditionalJobId intValue]];
                                if(curDetail){
                                    curDetail.addjob_detail_category_id = [curAddJobDetail.additionalJobId intValue];
                                    curDetail.addjob_detail_last_modified = todayDateString;
                                    curDetail.addjob_detail_onday_addjob_id = [curAddJobDetail.ondayAdditionalJobId intValue];
                                    curDetail.addjob_detail_room_id = [curJobRoom.roomId intValue];
                                    curDetail.addjob_detail_room_number = curJobRoom.roomNo;
                                    curDetail.addjob_detail_user_id = userId;
                                    curDetail.addjob_detail_status = [curAddJobDetail.additionalJobStatus intValue] == 0 ? AddJobStatus_Pending : AddJobStatus_Complete;
                                    [self updateAddJobDetail:curDetail];
                                } else {
                                    curDetail = [[AddJobDetailModelV2 alloc] init];
                                    curDetail.addjob_detail_category_id = [curAddJobDetail.additionalJobId intValue];
                                    curDetail.addjob_detail_expected_cleaning_time = 0;
                                    curDetail.addjob_detail_last_modified = todayDateString;
                                    curDetail.addjob_detail_onday_addjob_id = [curAddJobDetail.ondayAdditionalJobId intValue];
                                    curDetail.addjob_detail_post_status = POST_STATUS_UN_CHANGED;
                                    curDetail.addjob_detail_room_assign_id = 0;
                                    curDetail.addjob_detail_room_id = [curJobRoom.roomId intValue];
                                    curDetail.addjob_detail_room_number = curJobRoom.roomNo;
                                    curDetail.addjob_detail_start_time = nil;
                                    curDetail.addjob_detail_status = [curAddJobDetail.additionalJobStatus intValue] == 0 ? AddJobStatus_Pending : AddJobStatus_Complete;
                                    curDetail.addjob_detail_stop_time = nil;
                                    curDetail.addjob_detail_user_id = userId;
                                    [self insertAddJobDetail:curDetail];
                                }
                                
                                //Categories of room in search mode
                                if(searchId > 0){ //Refresh search data for category of room
                                    [self deleteAddJobSearchRoomCategoryByUserId:userId roomId:[curJobRoom.roomId intValue]]; //delete old search data
                                    
                                    AddJobSearchRoomCategoryModelV2 *searchRoomCategory = [[AddJobSearchRoomCategoryModelV2 alloc] init];
                                    searchRoomCategory.addjob_searchroomcategory_category_id = curDetail.addjob_detail_category_id;
                                    searchRoomCategory.addjob_searchroomcategory_last_modified = todayDateString;
                                    searchRoomCategory.addjob_searchroomcategory_room_id = curDetail.addjob_detail_room_id;
                                    searchRoomCategory.addjob_searchroomcategory_search_id = searchId;
                                    searchRoomCategory.addjob_searchroomcategory_user_id = userId;
                                    [self insertAddJobSearchRoomCategory:searchRoomCategory];
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    return result;
}

//WS Add Job Room
-(bool) loadWSAddJobRoomByUserId:(int)userId hotelId:(int)hotelId roomNumber:(NSString*)roomNumber
{
    bool result = NO;
    //No network detected
    if(![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]){
        return result;
    }
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole]){
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_GetAdditionalJobByRoomNo *request = [[eHousekeepingService_GetAdditionalJobByRoomNo alloc] init];
    
    [request setIntHotelID:[NSNumber numberWithInt:hotelId]];
    [request setIntUserID:[NSNumber numberWithInt:userId]];
    [request setSRoomNo:roomNumber];
    
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetAdditionalJobByRoomNoUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetAdditionalJobByRoomNoResponse class]]) {
            eHousekeepingService_GetAdditionalJobByRoomNoResponse *dataBody = (eHousekeepingService_GetAdditionalJobByRoomNoResponse *)bodyPart;
            if (dataBody != nil) {
                if ([dataBody.GetAdditionalJobByRoomNoResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_OK]]) {
                    
                    result = YES; //success
                    eHousekeepingService_AdditionalJobRoom *curJobRoom = dataBody.GetAdditionalJobByRoomNoResult.AddtionalJobRoom;
                    
                    AddJobRoomModelV2 *addJobRoom = nil;
                    NSString *todayDateString = [ehkConvert getTodayDateString];
                    
                    
                    //Additional Job Room
                    addJobRoom = [self loadAddJobRoomByUserId:userId roomId:[curJobRoom.roomId intValue] roomAssignId:0 searchId:0];
                    if(addJobRoom){
                        addJobRoom.addjob_room_assign_id = 0;
                        addJobRoom.addjob_room_id = [curJobRoom.roomId intValue];
                        addJobRoom.addjob_room_is_due_out = [curJobRoom.isDueOutRoom boolValue] ? 1 : 0;
                        addJobRoom.addjob_room_last_modified = todayDateString;
                        addJobRoom.addjob_room_number = curJobRoom.roomNo;
                        addJobRoom.addjob_room_number_pending_job = [curJobRoom.totalPendingAdditionalJobList intValue];
                        addJobRoom.addjob_room_status_id = [curJobRoom.roomStatusID intValue];
                        addJobRoom.addjob_room_type_id = [curJobRoom.roomTypeID intValue];
                        addJobRoom.addjob_room_user_id = userId;
                        [self updateAddJobRoom:addJobRoom];
                    } else {
                        addJobRoom = [[AddJobRoomModelV2 alloc] init];
                        addJobRoom.addjob_room_assign_id = 0;
                        addJobRoom.addjob_room_floor_id = 0;
                        addJobRoom.addjob_room_id = [curJobRoom.roomId intValue];
                        addJobRoom.addjob_room_is_due_out = [curJobRoom.isDueOutRoom boolValue] ? 1 : 0;
                        addJobRoom.addjob_room_last_modified = todayDateString;
                        addJobRoom.addjob_room_number = curJobRoom.roomNo;
                        addJobRoom.addjob_room_number_pending_job = [curJobRoom.totalPendingAdditionalJobList intValue];
                        addJobRoom.addjob_room_remark = curJobRoom.remark;
                        addJobRoom.addjob_room_search_id = 0;
                        addJobRoom.addjob_room_status_id = [curJobRoom.roomStatusID intValue];
                        addJobRoom.addjob_room_type_id = [curJobRoom.roomTypeID intValue];
                        addJobRoom.addjob_room_user_id = userId;
                        [self insertAddJobRoom:addJobRoom];
                    }
                    
                    for (eHousekeepingService_AdditionalJob *curAddJobDetail in curJobRoom.pendingAdditionalJobList.AdditionalJob) {
                        
                        //Additional Job Category
                        AddJobCategoryModelV2 *curCategory = [self loadAddJobCategoryByUserId:userId categoryId:[curAddJobDetail.additionalJobId intValue]];
                        if(curCategory){
                            curCategory.addjob_category_id = [curAddJobDetail.additionalJobId intValue];
                            curCategory.addjob_category_last_modified = todayDateString;
                            curCategory.addjob_category_name = curAddJobDetail.additionalJobName;
                            curCategory.addjob_category_name_lang = curAddJobDetail.additionalJobNamend;
                            curCategory.addjob_category_name_lang2 = curAddJobDetail.additionalJobNamerd;
                            curCategory.addjob_category_user_id = userId;
                            
                            [self updateAddJobCategory:curCategory];
                        } else {
                            curCategory = [[AddJobCategoryModelV2 alloc] init];
                            curCategory.addjob_category_id = [curAddJobDetail.additionalJobId intValue];
                            curCategory.addjob_category_last_modified = todayDateString;
                            curCategory.addjob_category_name = curAddJobDetail.additionalJobName;
                            curCategory.addjob_category_name_lang = curAddJobDetail.additionalJobNamend;
                            curCategory.addjob_category_name_lang2 = curAddJobDetail.additionalJobNamerd;
                            curCategory.addjob_category_user_id = userId;
                            
                            [self insertAddJobCategory:curCategory];
                        }
                        
                        //Additional Job Detail
                        AddJobDetailModelV2 *curDetail = [self loadAddJobDetailByUserId:userId ondayAddJobId:[curAddJobDetail.ondayAdditionalJobId intValue]];
                        if(curDetail){
                            curDetail.addjob_detail_category_id = [curAddJobDetail.additionalJobId intValue];
                            curDetail.addjob_detail_last_modified = todayDateString;
                            curDetail.addjob_detail_onday_addjob_id = [curAddJobDetail.ondayAdditionalJobId intValue];
                            curDetail.addjob_detail_room_id = [curJobRoom.roomId intValue];
                            curDetail.addjob_detail_room_number = curJobRoom.roomNo;
                            curDetail.addjob_detail_user_id = userId;
                            curDetail.addjob_detail_status = [curAddJobDetail.additionalJobStatus intValue] == 0 ? AddJobStatus_Pending : AddJobStatus_Complete;
                            [self updateAddJobDetail:curDetail];
                        } else {
                            curDetail = [[AddJobDetailModelV2 alloc] init];
                            curDetail.addjob_detail_category_id = [curAddJobDetail.additionalJobId intValue];
                            curDetail.addjob_detail_expected_cleaning_time = 0;
                            curDetail.addjob_detail_last_modified = todayDateString;
                            curDetail.addjob_detail_onday_addjob_id = [curAddJobDetail.ondayAdditionalJobId intValue];
                            curDetail.addjob_detail_post_status = POST_STATUS_UN_CHANGED;
                            curDetail.addjob_detail_room_assign_id = 0;
                            curDetail.addjob_detail_room_id = [curJobRoom.roomId intValue];
                            curDetail.addjob_detail_room_number = curJobRoom.roomNo;
                            curDetail.addjob_detail_start_time = nil;
                            curDetail.addjob_detail_status = [curAddJobDetail.additionalJobStatus intValue] == 0 ? AddJobStatus_Pending : AddJobStatus_Complete;
                            curDetail.addjob_detail_stop_time = nil;
                            curDetail.addjob_detail_user_id = userId;
                            [self insertAddJobDetail:curDetail];
                        }
                    }
                    
                } else if([dataBody.GetAdditionalJobByRoomNoResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_NO_RERCORD_TO_DISPLAY]]){
                    //No have data
                    result = NO;
                }
            }
        }
    }
    
    return result;
}

//WS Add Job Detail
-(bool) loadWSAddJobDetailByUserId:(int)userId ondayAddJobId:(int)ondayAddJobId
{
    
    if(userId <= 0){
        return false;
    }
    
    //No network detected
    if(![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]){
        return false;
    }
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole]){
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_GetAdditionalJobDetail *request = [[eHousekeepingService_GetAdditionalJobDetail alloc] init];
    
    [request setOndayAdditionalJobId:[NSNumber numberWithInt:ondayAddJobId]];
    
    bool result = false;
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetAdditionalJobDetailUsingParameters:request];
    NSArray *responseBodyParts = response.bodyParts;
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetAdditionalJobDetailResponse class]]) {
            eHousekeepingService_GetAdditionalJobDetailResponse *dataBody = (eHousekeepingService_GetAdditionalJobDetailResponse *)bodyPart;
            if (dataBody != nil) {
                if ([dataBody.GetAdditionalJobDetailResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_OK]]
                    || [dataBody.GetAdditionalJobDetailResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_NO_RERCORD_TO_DISPLAY]]) {
                    
                    NSString *todayDateString = [ehkConvert getTodayDateString];
                    
                    result = true;
                    eHousekeepingService_AdditionalJobDetail *wsAddJobDetail = dataBody.GetAdditionalJobDetailResult;
                    AddJobDetailModelV2 *detail = [self loadAddJobDetailByUserId:userId ondayAddJobId:ondayAddJobId];
                    
                    //Only load Additional Job Detail because Additional Job was added with "-loadWSAddJobRoomListByUserId:hotelId:floorId:tasksFilter:"
                    if(detail){
                        detail.addjob_detail_expected_cleaning_time = [wsAddJobDetail.expectedCleaningTime intValue];
                        detail.addjob_detail_last_modified = todayDateString;
                        [self updateAddJobDetail:detail];
                        
                        //Delete old data of current ondayAddJobId
                        [self deleteAddJobRoomItemByUserId:userId ondayAddJobId:ondayAddJobId];
                        
                        //Task list
                        NSMutableArray *wsJobTaskList = wsAddJobDetail.JobTaskList.JobTask;
                        for (eHousekeepingService_JobTask *curJobTask in wsJobTaskList) {
                            AddJobItemModelV2 *item = [self loadAddJobItemByUserId:userId itemId:[curJobTask.taskID intValue]];
                            //Whether or not existing local database of AddJobItem
                            if(item){
                                item.addjob_item_id = [curJobTask.taskID intValue];
                                item.addjob_item_last_modified = todayDateString;
                                item.addjob_item_name = curJobTask.taskName;
                                item.addjob_item_name_lang = curJobTask.taskLang;
                                item.addjob_item_name_lang2 = curJobTask.taskLang2;
                                item.addjob_item_user_id = userId;
                                
                                [self updateAddJobItem:item];
                            } else {
                                item = [[AddJobItemModelV2 alloc] init];
                                item.addjob_item_id = [curJobTask.taskID intValue];
                                item.addjob_item_last_modified = todayDateString;
                                item.addjob_item_name = curJobTask.taskName;
                                item.addjob_item_name_lang = curJobTask.taskLang;
                                item.addjob_item_name_lang2 = curJobTask.taskLang2;
                                item.addjob_item_user_id = userId;
                                
                                [self insertAddJobItem:item];
                            }
                            
                            //Set AddJobItem belong to ondayAddJobId
                            AddJobRoomItemModelV2 *itemOfAddJobDetail = [[AddJobRoomItemModelV2 alloc] init];
                            itemOfAddJobDetail.addjob_roomitem_item_id = item.addjob_item_id;
                            itemOfAddJobDetail.addjob_roomitem_last_modified = todayDateString;
                            itemOfAddJobDetail.addjob_roomitem_onday_addjob_id = ondayAddJobId;
                            itemOfAddJobDetail.addjob_roomitem_user_id = userId;
                            [self insertAddJobRoomItem:itemOfAddJobDetail];
                        }
                        
                    } else {
                        result = false; //No have additional Job Detail to process
                    }
                }
            }
        }
    }
    
    return result;
}

//WS Add Job Count Pending
//Return -1 for fail to count additional job
-(int) loadWSAddJobCountPendingByUserId:(int)userId hotelId:(int)hotelId
{
    //No network detected
    if(![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]){
        return -1;
    }
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole]){
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_GetAdditionalJobCount *request = [[eHousekeepingService_GetAdditionalJobCount alloc] init];
    
    [request setIntHotelID:[NSNumber numberWithInt:hotelId]];
    [request setIntUserID:[NSNumber numberWithInt:userId]];
    
    int result = -1;
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetAdditionalJobCountUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetAdditionalJobCountResponse class]]) {
            eHousekeepingService_GetAdditionalJobCountResponse *dataBody = (eHousekeepingService_GetAdditionalJobCountResponse *)bodyPart;
            if (dataBody != nil) {
                if ([dataBody.GetAdditionalJobCountResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_OK]]
                    || [dataBody.GetAdditionalJobCountResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_NO_RERCORD_TO_DISPLAY]]) {
                    
                    result = 0;
                    result = [dataBody.GetAdditionalJobCountResult.AdditionalJobCount intValue];
                }
            }
        }
    }
    
    return result;
}

//WS Add Job GuestInfo
-(bool) loadWSAddJobGuestInfoByUserId:(int)userId roomId:(int)roomId
{
    //No network detected
    if(![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]){
        return NO;
    }
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole]){
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_GetGuestInfoByRoomID *request = [[eHousekeepingService_GetGuestInfoByRoomID alloc] init];
    
    [request setIntRoomID:[NSNumber numberWithInt:roomId]];
    [request setIntUsrID:[NSNumber numberWithInt:userId]];
    
    bool result = NO;
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetGuestInfoByRoomIDUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetGuestInfoByRoomIDResponse class]]) {
            eHousekeepingService_GetGuestInfoByRoomIDResponse *dataBody = (eHousekeepingService_GetGuestInfoByRoomIDResponse *)bodyPart;
            if (dataBody != nil) {
                if ([dataBody.GetGuestInfoByRoomIDResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_OK]]) {
                    
                    result = YES;
                    
                    //NSString *todayDateString = [ehkConvert getTodayDateString];
                    eHousekeepingService_GuestOfRoom *wsGuestInfo= dataBody.GetGuestInfoByRoomIDResult.GuestInfo;
                    AddJobGuestInfoModelV2 *addJobGuestInfo = [self loadAddJobGuestInfoByUserId:userId roomId:roomId];
                    if(addJobGuestInfo){
                        addJobGuestInfo.addjob_guestinfo_check_in_date = wsGuestInfo.gpCheckInDt;
                        addJobGuestInfo.addjob_guestinfo_check_out_date = wsGuestInfo.gpCheckOutDt;
                        addJobGuestInfo.addjob_guestinfo_guest_name = wsGuestInfo.gpName;
                        //addJobGuestInfo.addjob_guestinfo_guest_number - Guest Number is get from another WS calling
                        addJobGuestInfo.addjob_guestinfo_guest_title = wsGuestInfo.gpTitle;
                        addJobGuestInfo.addjob_guestinfo_housekeeping_name = wsGuestInfo.gpHousekeepingName;
                        addJobGuestInfo.addjob_guestinfo_lang_pref = wsGuestInfo.gpLangPref;
                        addJobGuestInfo.addjob_guestinfo_last_modified = wsGuestInfo.gpLastModified;
                        addJobGuestInfo.addjob_guestinfo_photo = wsGuestInfo.gpPhoto;
                        addJobGuestInfo.addjob_guestinfo_pref_desc = wsGuestInfo.gpPreferenceDesc;
                        addJobGuestInfo.addjob_guestinfo_room_id = roomId;
                        addJobGuestInfo.addjob_guestinfo_special_service = wsGuestInfo.gpSpecialService;
                        addJobGuestInfo.addjob_guestinfo_user_id = userId;
                        addJobGuestInfo.addjob_guestinfo_vip = wsGuestInfo.gpVIP;
                        
                        [self updateAddJobGuestInfo:addJobGuestInfo];
                    } else {
                        addJobGuestInfo = [[AddJobGuestInfoModelV2 alloc] init];
                        
                        addJobGuestInfo.addjob_guestinfo_check_in_date = wsGuestInfo.gpCheckInDt;
                        addJobGuestInfo.addjob_guestinfo_check_out_date = wsGuestInfo.gpCheckOutDt;
                        addJobGuestInfo.addjob_guestinfo_guest_name = wsGuestInfo.gpName;
                        addJobGuestInfo.addjob_guestinfo_guest_number = @"N/A"; // Guest Number: is got from another WS calling
                        addJobGuestInfo.addjob_guestinfo_guest_title = wsGuestInfo.gpTitle;
                        addJobGuestInfo.addjob_guestinfo_housekeeping_name = wsGuestInfo.gpHousekeepingName;
                        addJobGuestInfo.addjob_guestinfo_lang_pref = wsGuestInfo.gpLangPref;
                        addJobGuestInfo.addjob_guestinfo_last_modified = wsGuestInfo.gpLastModified;
                        addJobGuestInfo.addjob_guestinfo_photo = wsGuestInfo.gpPhoto;
                        addJobGuestInfo.addjob_guestinfo_pref_desc = wsGuestInfo.gpPreferenceDesc;
                        addJobGuestInfo.addjob_guestinfo_room_id = roomId;
                        addJobGuestInfo.addjob_guestinfo_special_service = wsGuestInfo.gpSpecialService;
                        addJobGuestInfo.addjob_guestinfo_user_id = userId;
                        addJobGuestInfo.addjob_guestinfo_vip = wsGuestInfo.gpVIP;
                        
                        [self insertAddJobGuestInfo:addJobGuestInfo];
                    }
                } else if([dataBody.GetGuestInfoByRoomIDResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_NO_RERCORD_TO_DISPLAY]]) { //No data to display
                    
                    return YES;
                }
            }
        }
    }
    
    return result;
}

//WS Add Job GuestInfo Number
//Return N/A for can't get Guest Number
-(NSString*) loadWSAddJobGuestNumberByUserId:(int)userId hotelId:(int)hotelId roomNumber:(NSString*)roomNumber
{
    //No network detected
    if(![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]){
        return @"N/A";
    }
    
    if(userId <= 0){
        return @"N/A";
    }
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole]){
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_GetNumberOfGuest *request = [[eHousekeepingService_GetNumberOfGuest alloc] init];
    
    [request setIntHotelID:[NSNumber numberWithInt:hotelId]];
    [request setIntUsrID:[NSNumber numberWithInt:userId]];
    [request setSRoomNo:roomNumber];
    
    NSString *result = @"N/A";
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetNumberOfGuestUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetNumberOfGuestResponse class]]) {
            eHousekeepingService_GetNumberOfGuestResponse *dataBody = (eHousekeepingService_GetNumberOfGuestResponse *)bodyPart;
            if (dataBody != nil) {
                if ([dataBody.GetNumberOfGuestResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_OK]]
                    || [dataBody.GetNumberOfGuestResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_NO_RERCORD_TO_DISPLAY]]) {
                    
                    result = 0;
                    result =  [dataBody.GetNumberOfGuestResult.NumberOfGuest stringValue];
                }
            }
        }
    }
    
    return result;
}

//WS Add Job Detail Of Room Assignment
-(bool) loadWSAddJobOfRoomByUserId:(int)userId hotelId:(int)hotelId roomNumber:(NSString*)roomNumber roomAssignId:(int)roomAssignId
{
    bool result = NO;
    //No network detected
    if(![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]){
        return result;
    }
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole]){
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_GetAdditionalJobOfRoom *request = [[eHousekeepingService_GetAdditionalJobOfRoom alloc] init];
    
    [request setIntHotelID:[NSNumber numberWithInt:hotelId]];
    [request setIntUserID:[NSNumber numberWithInt:userId]];
    [request setIntDutyAssignID:[NSNumber numberWithInt:roomAssignId]];
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetAdditionalJobOfRoomUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetAdditionalJobOfRoomResponse class]]) {
            eHousekeepingService_GetAdditionalJobOfRoomResponse *dataBody = (eHousekeepingService_GetAdditionalJobOfRoomResponse *)bodyPart;
            if (dataBody != nil) {
                if ([dataBody.GetAdditionalJobOfRoomResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_OK]]
                    || [dataBody.GetAdditionalJobOfRoomResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_NO_RERCORD_TO_DISPLAY]]) {
                    
                    result = YES; //success
                    NSMutableArray *addJobDetail = dataBody.GetAdditionalJobOfRoomResult.AdditionalJobList.AdditionalJob;
                    
                    NSString *todayDateString = [ehkConvert getTodayDateString];
                    
                    
                    for (eHousekeepingService_AdditionalJob *curAddJobDetail in addJobDetail) {
                        
                        //Additional Job Category
                        AddJobCategoryModelV2 *curCategory = [self loadAddJobCategoryByUserId:userId categoryId:[curAddJobDetail.additionalJobId intValue]];
                        if(curCategory){
                            curCategory.addjob_category_id = [curAddJobDetail.additionalJobId intValue];
                            curCategory.addjob_category_last_modified = todayDateString;
                            curCategory.addjob_category_name = curAddJobDetail.additionalJobName;
                            curCategory.addjob_category_name_lang = curAddJobDetail.additionalJobNamend;
                            curCategory.addjob_category_name_lang2 = curAddJobDetail.additionalJobNamerd;
                            curCategory.addjob_category_user_id = userId;
                            
                            [self updateAddJobCategory:curCategory];
                        } else {
                            curCategory = [[AddJobCategoryModelV2 alloc] init];
                            curCategory.addjob_category_id = [curAddJobDetail.additionalJobId intValue];
                            curCategory.addjob_category_last_modified = todayDateString;
                            curCategory.addjob_category_name = curAddJobDetail.additionalJobName;
                            curCategory.addjob_category_name_lang = curAddJobDetail.additionalJobNamend;
                            curCategory.addjob_category_name_lang2 = curAddJobDetail.additionalJobNamerd;
                            curCategory.addjob_category_user_id = userId;
                            
                            [self insertAddJobCategory:curCategory];
                        }
                        
                        //Additional Job Detail
                        AddJobDetailModelV2 *curDetail = [self loadAddJobDetailByUserId:userId ondayAddJobId:[curAddJobDetail.ondayAdditionalJobId intValue]];
                        if(curDetail){
                            curDetail.addjob_detail_category_id = [curAddJobDetail.additionalJobId intValue];
                            curDetail.addjob_detail_last_modified = todayDateString;
                            curDetail.addjob_detail_onday_addjob_id = [curAddJobDetail.ondayAdditionalJobId intValue];
                            curDetail.addjob_detail_room_assign_id = roomAssignId;
                            curDetail.addjob_detail_room_number = roomNumber;
                            curDetail.addjob_detail_user_id = userId;
                            curDetail.addjob_detail_status = [curAddJobDetail.additionalJobStatus intValue] == 0 ? AddJobStatus_Pending : AddJobStatus_Complete;
                            [self updateAddJobDetail:curDetail];
                        } else {
                            curDetail = [[AddJobDetailModelV2 alloc] init];
                            curDetail.addjob_detail_category_id = [curAddJobDetail.additionalJobId intValue];
                            curDetail.addjob_detail_expected_cleaning_time = 0;
                            curDetail.addjob_detail_last_modified = todayDateString;
                            curDetail.addjob_detail_onday_addjob_id = [curAddJobDetail.ondayAdditionalJobId intValue];
                            curDetail.addjob_detail_post_status = POST_STATUS_UN_CHANGED;
                            curDetail.addjob_detail_room_assign_id = roomAssignId;
                            curDetail.addjob_detail_room_id = 0;
                            curDetail.addjob_detail_room_number = roomNumber;
                            curDetail.addjob_detail_start_time = nil;
                            curDetail.addjob_detail_status = [curAddJobDetail.additionalJobStatus intValue] == 0 ? AddJobStatus_Pending : AddJobStatus_Complete;
                            curDetail.addjob_detail_stop_time = nil;
                            curDetail.addjob_detail_user_id = userId;
                            [self insertAddJobDetail:curDetail];
                        }
                    }
                }
            }
        }
    }
    
    return result;
}

//Post Additional Job Start
-(bool) postAddJobStartWithUserId:(int)userId ondayAddJobId:(int)ondayAddJobId time:(NSString*)timeString
{
    //No network detected
    if(![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]){
        return NO;
    }
    if(timeString.length <= 0){
        return NO;
    }
    if(ondayAddJobId <= 0){
        return NO;
    }
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole]){
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_StartAdditionalJob *request = [[eHousekeepingService_StartAdditionalJob alloc] init];
    
    [request setOndayAdditionalJobId:[NSNumber numberWithInt:ondayAddJobId]];
    [request setStartedDateTime:timeString];
    
    bool result = NO;
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding StartAdditionalJobUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_StartAdditionalJobResponse class]]) {
            eHousekeepingService_StartAdditionalJobResponse *dataBody = (eHousekeepingService_StartAdditionalJobResponse *)bodyPart;
            if (dataBody != nil) {
                if ([dataBody.StartAdditionalJobResult.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_OK]]
                    || [dataBody.StartAdditionalJobResult.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_NO_RERCORD_TO_DISPLAY]]
                    || [dataBody.StartAdditionalJobResult.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_NEW_RECORD_ADDED]]) {
                    
                    result = YES;
                }
            }
        }
    }
    
    return result;
}

//Post Additional Job Stop
-(bool) postAddJobStopWithUserId:(int)userId ondayAddJobId:(int)ondayAddJobId time:(NSString*)timeString
{
    //No network detected
    if(![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]){
        return NO;
    }
    
    if(timeString.length <= 0){
        return NO;
    }
    if(ondayAddJobId <= 0){
        return NO;
    }
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole]){
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_StopAdditionalJob *request = [[eHousekeepingService_StopAdditionalJob alloc] init];
    
    [request setOndayAdditionalJobId:[NSNumber numberWithInt:ondayAddJobId]];
    [request setStopDateTime:timeString];
    
    bool result = NO;
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding StopAdditionalJobUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_StopAdditionalJobResponse class]]) {
            eHousekeepingService_StopAdditionalJobResponse *dataBody = (eHousekeepingService_StopAdditionalJobResponse *)bodyPart;
            if (dataBody != nil) {
                if ([dataBody.StopAdditionalJobResult.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_OK]]
                    || [dataBody.StopAdditionalJobResult.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_NO_RERCORD_TO_DISPLAY]]
                    || [dataBody.StopAdditionalJobResult.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_NEW_RECORD_ADDED]]) {
                    
                    result = YES;
                }
            }
        }
    }
    
    return result;
}

//Post Additional Job Complete
-(bool) postAddJobCompleteWithUserId:(int)userId ondayAddJobId:(int)ondayAddJobId time:(NSString*)timeString
{
    //No network detected
    if(![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]){
        return NO;
    }
    
    if(timeString.length <= 0){
        return NO;
    }
    
    if(ondayAddJobId <= 0){
        return NO;
    }
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole]){
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_CompleteAdditionalJob *request = [[eHousekeepingService_CompleteAdditionalJob alloc] init];
    
    [request setOndayAdditionalJobId:[NSNumber numberWithInt:ondayAddJobId]];
    [request setCompletedDateTime:timeString];
    [request setUserID:[NSString stringWithFormat:@"%d", userId]];
    bool result = NO;
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding CompleteAdditionalJobUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_CompleteAdditionalJobResponse class]]) {
            eHousekeepingService_CompleteAdditionalJobResponse *dataBody = (eHousekeepingService_CompleteAdditionalJobResponse *)bodyPart;
            if (dataBody != nil) {
                if ([dataBody.CompleteAdditionalJobResult.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_OK]]
                    || [dataBody.CompleteAdditionalJobResult.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_NO_RERCORD_TO_DISPLAY]]
                    || [dataBody.CompleteAdditionalJobResult.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_NEW_RECORD_ADDED]]) {
                    
                    result = YES;
                }
            }
        }
    }
    
    return result;
}

//Post Additional Job Remark
-(bool) postAddJobRemarkWithUserId:(int)userId ondayAddJobId:(int)ondayAddJobId remark:(NSString*)remark
{
    //No network detected
    if(![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]){
        return NO;
    }
    
    if(userId <= 0){
        return NO;
    }
    
    if(ondayAddJobId <= 0){
        return NO;
    }
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole]){
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_UpdateAdditionalJobRemark *request = [[eHousekeepingService_UpdateAdditionalJobRemark alloc] init];
    
    [request setIntUsrID:[NSNumber numberWithInt:userId]];
    [request setIntOnDayJobID:[NSNumber numberWithInt:ondayAddJobId]];
    [request setSRemark:remark];
    
    bool result = NO;
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding UpdateAdditionalJobRemarkUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_UpdateAdditionalJobRemarkResponse class]]) {
            eHousekeepingService_UpdateAdditionalJobRemarkResponse *dataBody = (eHousekeepingService_UpdateAdditionalJobRemarkResponse *)bodyPart;
            if (dataBody != nil) {
                if ([dataBody.UpdateAdditionalJobRemarkResult.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_OK]]
                    || [dataBody.UpdateAdditionalJobRemarkResult.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_NO_RERCORD_TO_DISPLAY]]
                    || [dataBody.UpdateAdditionalJobRemarkResult.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_NEW_RECORD_ADDED]]) {
                    
                    result = YES;
                }
            }
        }
    }
    
    return result;
}

//return number of posted additional job
-(int)postAllAddJobByUserId:(int)userId
{
    int countPosted = 0;
    
    //Post Remark
    NSMutableArray *listAddJobDetailRemarkUnposted = [self loadAddJobDetailsByUserId:userId postStatus:(int)POST_STATUS_SAVED_UNPOSTED];
    for (AddJobDetailModelV2 *curDetail in listAddJobDetailRemarkUnposted) {
        bool isPostedSuccess = [self postAddJobRemarkWithUserId:userId ondayAddJobId:(int)curDetail.addjob_detail_onday_addjob_id remark:curDetail.addjob_detail_remark];
        if(isPostedSuccess) {
            countPosted ++;
            curDetail.addjob_detail_post_status = POST_STATUS_POSTED;
            [self updateAddJobDetail:curDetail];
        }
    }
    
    //Post Additional Job Records time
    NSMutableArray *listAddJobUnposted = [self loadAddJobDetailRecordsByUserId:userId postStatus:(int)POST_STATUS_SAVED_UNPOSTED]; //Load unposted additional job
    for (AddJobDetailRecordModelV2 *curDetailRecord in listAddJobUnposted) {
        bool isPostedSuccess = NO;
        if(curDetailRecord.addjob_detail_record_operation == AddJobStatus_Started){
            isPostedSuccess = [self postAddJobStartWithUserId:userId ondayAddJobId:(int)curDetailRecord.addjob_detail_record_onday_addjob_id time:curDetailRecord.addjob_detail_record_time];
        } else if(curDetailRecord.addjob_detail_record_operation == AddJobStatus_Pause){
            isPostedSuccess = [self postAddJobStopWithUserId:userId ondayAddJobId:(int)curDetailRecord.addjob_detail_record_onday_addjob_id time:curDetailRecord.addjob_detail_record_time];
        } else if(curDetailRecord.addjob_detail_record_operation == AddJobStatus_Complete){
            isPostedSuccess = [self postAddJobCompleteWithUserId:userId ondayAddJobId:(int)curDetailRecord.addjob_detail_record_onday_addjob_id time:curDetailRecord.addjob_detail_record_time];
        }
        
        if(isPostedSuccess){
            countPosted ++;
            curDetailRecord.addjob_detail_record_post_status = POST_STATUS_POSTED;
            [self updateAddJobDetailRecord:curDetailRecord];
        }
    }
    
    return countPosted;
}

#pragma mark - DATABASE FUNCTIONS
//MARK: Delete all additional Job data from yesterday
-(int)deleteAllAddJobDataBeforeToday
{
    NSString *todayDateString = [ehkConvert getTodayDateString];
    int countDelete = 0;
    
    if([self isUsingOldAddJobData]){
        
        DataTable *tableDelete = [[DataTable alloc] initWithNameFormat:@"%@",ADDJOB_SEARCH_ROOM_CATEGORY];
        int countCurrentDelete = [tableDelete dropDBRows];
        countDelete += countCurrentDelete;
        [tableDelete excuteQueryNonSelect:[NSString stringWithFormat: @"REINDEX %@.%@",DATABASE_NAME, ADDJOB_SEARCH_ROOM_CATEGORY]];
        
        tableDelete.tableName = [NSString stringWithFormat:@"%@",ADDJOB_ROOM_ITEM];
        countCurrentDelete = [tableDelete dropDBRows];
        countDelete += countCurrentDelete;
        [tableDelete excuteQueryNonSelect:[NSString stringWithFormat: @"REINDEX %@.%@",DATABASE_NAME, ADDJOB_ROOM_ITEM]];
        
        tableDelete.tableName = [NSString stringWithFormat:@"%@",ADDJOB_ROOM];
        countCurrentDelete = [tableDelete dropDBRows];
        countDelete += countCurrentDelete;
        [tableDelete excuteQueryNonSelect:[NSString stringWithFormat: @"REINDEX %@.%@",DATABASE_NAME, ADDJOB_ROOM]];
        
        tableDelete.tableName = [NSString stringWithFormat:@"%@", ADDJOB_GUEST_INFO];
        countCurrentDelete = [tableDelete dropDBRows];
        countDelete += countCurrentDelete;
        [tableDelete excuteQueryNonSelect:[NSString stringWithFormat: @"REINDEX %@.%@", DATABASE_NAME, ADDJOB_GUEST_INFO]];
        
        tableDelete.tableName = [NSString stringWithFormat:@"%@", ADDJOB_FLOOR];
        countCurrentDelete = [tableDelete dropDBRows];
        countDelete += countCurrentDelete;
        [tableDelete excuteQueryNonSelect:[NSString stringWithFormat: @"REINDEX %@.%@", DATABASE_NAME, ADDJOB_FLOOR]];
        
        tableDelete.tableName = [NSString stringWithFormat:@"%@", ADDJOB_DETAIL_RECORD];
        countCurrentDelete = [tableDelete dropDBRows];
        countDelete += countCurrentDelete;
        [tableDelete excuteQueryNonSelect:[NSString stringWithFormat: @"REINDEX %@.%@", DATABASE_NAME, ADDJOB_DETAIL_RECORD]];
        
        tableDelete.tableName = [NSString stringWithFormat:@"%@", ADDJOB_DETAIL];
        countCurrentDelete = [tableDelete dropDBRows];
        countDelete += countCurrentDelete;
        [tableDelete excuteQueryNonSelect:[NSString stringWithFormat: @"REINDEX %@.%@", DATABASE_NAME, ADDJOB_DETAIL]];
        
        //we will not delete table data following:
        // - ADDJOB_SEARCH
        // - ADDJOB_CATEGORY
        // - ADDJOB_ITEM
        //Because it's rarely change
    }
    
    //save last delete date
    [[UserManagerV2 sharedUserManager] setCurrentUserConfig:USER_CONFIG_DATE_DELETE_ADDJOB value:todayDateString];
    
    return countDelete;
}

//Check whether additional job data is old or not
-(bool) isUsingOldAddJobData
{
    NSString *todayDateString = [ehkConvert getTodayDateString];
    NSString *dateDeleteAddJob = [[UserManagerV2 sharedUserManager] getConfigValueOfCurrentUserByKey:USER_CONFIG_DATE_DELETE_ADDJOB];
    if(dateDeleteAddJob != nil && [dateDeleteAddJob caseInsensitiveCompare:todayDateString] != NSOrderedSame){
        return YES;
    }
    return NO;
}

//MARK: Local AddJob Floor Function
-(int) insertAddJobFloor:(AddJobFloorModelV2*)model
{
    if(model == nil){
        return 0;
    }
    
    DataRow *rowInsert = [[DataRow alloc] init];
    rowInsert.tableName = [NSString stringWithFormat:@"%@", ADDJOB_FLOOR];
    /*DataColumn *columnKey = */[rowInsert addColumnInt:[NSString stringWithFormat:@"%@",addjob_floor_id] value:(int)model.addjob_floor_id];
    //Remove this for auto increase id
    //columnKey.isKeyOnInsert = YES;
    
    [rowInsert addColumnString:[NSString stringWithFormat:@"%@",addjob_floor_name] value:model.addjob_floor_name];
    [rowInsert addColumnString:[NSString stringWithFormat:@"%@",addjob_floor_name_lang] value:model.addjob_floor_name_lang];
    [rowInsert addColumnInt:[NSString stringWithFormat:@"%@",addjob_floor_search_id] value:(int)model.addjob_floor_search_id];
    [rowInsert addColumnInt:[NSString stringWithFormat:@"%@",addjob_floor_user_id] value:(int)model.addjob_floor_user_id];
    [rowInsert addColumnString:[NSString stringWithFormat:@"%@",addjob_floor_last_modified] value:model.addjob_floor_last_modified];
    [rowInsert addColumnInt:[NSString stringWithFormat:@"%@",addjob_floor_number_of_job] value:(int)model.addjob_floor_number_of_job];
    
    return [rowInsert insertDBRow]; //return id inserted
}

-(int) updateAddJobFloor:(AddJobFloorModelV2*)model
{
    if(model == nil){
        return 0;
    }
    
    DataRow *rowUpdate = [[DataRow alloc] init];
    rowUpdate.tableName = [NSString stringWithFormat:@"%@", ADDJOB_FLOOR];
    
    DataColumn *columnUpdate1 = [rowUpdate addColumnInt:[NSString stringWithFormat:@"%@",addjob_floor_id] value:(int)model.addjob_floor_id];
    columnUpdate1.isUpdateCondition = YES;
    DataColumn *columnUpdate2 = [rowUpdate addColumnInt:[NSString stringWithFormat:@"%@",addjob_floor_search_id] value:(int)model.addjob_floor_search_id];
    columnUpdate2.isUpdateCondition = YES;
    DataColumn *columnUpdate3 = [rowUpdate addColumnInt:[NSString stringWithFormat:@"%@",addjob_floor_user_id] value:(int)model.addjob_floor_user_id];
    columnUpdate3.isUpdateCondition = YES;
    
    [rowUpdate addColumnString:[NSString stringWithFormat:@"%@",addjob_floor_name] value:model.addjob_floor_name];
    [rowUpdate addColumnString:[NSString stringWithFormat:@"%@",addjob_floor_name_lang] value:model.addjob_floor_name_lang];
    [rowUpdate addColumnString:[NSString stringWithFormat:@"%@",addjob_floor_last_modified] value:model.addjob_floor_last_modified];
    [rowUpdate addColumnInt:[NSString stringWithFormat:@"%@",addjob_floor_number_of_job] value:(int)model.addjob_floor_number_of_job];
    
    return [rowUpdate commitChangeDBRow];
}

-(int) deleteAddJobFloorsByUserId:(int) userId searchId:(int)searchId
{
    if(searchId < 0){
        searchId = 0;
    }
    
    DataTable *tableAddJobFloor = [[DataTable alloc] initWithNameFormat:@"%@",ADDJOB_FLOOR];
    
    NSString *queryDelete = [NSString stringWithFormat:@"Delete from %@ Where %@ = %d and %@ = %d", ADDJOB_FLOOR, addjob_floor_user_id, userId, addjob_search_id, searchId];
    int status = (int)[tableAddJobFloor excuteQueryNonSelect:queryDelete];
    
    return status;
    
}

-(int) deleteAddJobFloorsByAddJobFloorModel: (AddJobFloorModelV2*) floorModel{
    if(floorModel.addjob_floor_search_id < 0){
        floorModel.addjob_floor_search_id = 0;
    }
    
    DataTable *tableAddJobFloor = [[DataTable alloc] initWithNameFormat:@"%@",ADDJOB_FLOOR];
    
    NSString *queryDelete = [NSString stringWithFormat:@"Delete from %@ Where %@ = %d and %@ = %d", ADDJOB_FLOOR, addjob_floor_id, (int)floorModel.addjob_floor_id, addjob_floor_search_id, (int)floorModel.addjob_floor_search_id];
    int status = (int)[tableAddJobFloor excuteQueryNonSelect:queryDelete];
    
    return status;
    
}

-(int) deleteAllAddJobFloors
{
    DataTable *tableAddJobFloor = [[DataTable alloc] initWithNameFormat:@"%@",ADDJOB_FLOOR];
    return [tableAddJobFloor dropDBRows];
}

-(AddJobFloorModelV2*) loadAddJobFloorByUserId:(int)userId floorId:(int)floorId searchId:(int)searchId
{
    if(floorId <= 0){
        return nil;
    }
    
    if(searchId < 0){
        searchId = 0;
    }
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"select %@, %@, %@, %@, %@, %@, %@ from %@ where %@ = %d and %@ = %d",
                                  
                                  //Select
                                  addjob_floor_id,
                                  addjob_floor_name,
                                  addjob_floor_name_lang,
                                  addjob_floor_user_id,
                                  addjob_floor_search_id,
                                  addjob_floor_last_modified,
                                  addjob_floor_number_of_job,
                                  
                                  //from
                                  ADDJOB_FLOOR,
                                  
                                  //where
//                                  addjob_floor_user_id,
//                                  userId,
                                  //and
                                  addjob_floor_id,
                                  floorId,
                                  //and
                                  addjob_floor_search_id,
                                  searchId];
    
    DataTable *table = [[DataTable alloc] init];
    
    //Row result reference
    DataRow *rowReference = [[DataRow alloc] init];
    [rowReference addColumnFormatInt:@"%@", addjob_floor_id];
    [rowReference addColumnFormatString:@"%@", addjob_floor_name];
    [rowReference addColumnFormatString:@"%@", addjob_floor_name_lang];
    [rowReference addColumnFormatInt:@"%@", addjob_floor_user_id];
    [rowReference addColumnFormatInt:@"%@", addjob_floor_search_id];
    [rowReference addColumnFormatString:@"%@", addjob_floor_last_modified];
    [rowReference addColumnFormatInt:@"%@", addjob_floor_number_of_job];
    
    [table loadTableDataWithQuery:sqlString referenceRow:rowReference];
    
    int rowCount = [table countRows];
    AddJobFloorModelV2 *model = nil;
    if(rowCount > 0)
    {
        model = [[AddJobFloorModelV2 alloc] init];
        DataRow *row = [table rowWithIndex:0];
        DataColumn *curColumn = nil;
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_floor_id];
        model.addjob_floor_id = [curColumn fieldValueInt];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_floor_name];
        model.addjob_floor_name = [curColumn fieldValueString];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_floor_name_lang];
        model.addjob_floor_name_lang = [curColumn fieldValueString];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_floor_user_id];
        model.addjob_floor_user_id = [curColumn fieldValueInt];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_floor_search_id];
        model.addjob_floor_search_id = [curColumn fieldValueInt];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_floor_last_modified];
        model.addjob_floor_last_modified = [curColumn fieldValueString];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_floor_number_of_job];
        model.addjob_floor_number_of_job = [curColumn fieldValueInt];
        
    }
    
    return model;
}

-(NSMutableArray*) loadAllAddJobFloorsByUserId:(int)userId searchId:(int)searchId
{
    if(userId <= 0){
        return nil;
    }
    
    if(searchId < 0){
        searchId = 0;
    }
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"select %@, %@, %@, %@, %@, %@, %@ from %@ where %@ = %d",
                                  
                                  //Select
                                  addjob_floor_id,
                                  addjob_floor_name,
                                  addjob_floor_name_lang,
                                  addjob_floor_user_id,
                                  addjob_floor_search_id,
                                  addjob_floor_last_modified,
                                  addjob_floor_number_of_job,
                                  
                                  //from
                                  ADDJOB_FLOOR,
                                  
                                  //where
//                                  addjob_floor_user_id,
//                                  userId,
                                  
                                  //and
                                  addjob_floor_search_id,
                                  searchId];
    
    DataTable *table = [[DataTable alloc] init];
    
    //Row result reference
    DataRow *rowReference = [[DataRow alloc] init];
    [rowReference addColumnFormatInt:@"%@", addjob_floor_id];
    [rowReference addColumnFormatString:@"%@", addjob_floor_name];
    [rowReference addColumnFormatString:@"%@", addjob_floor_name_lang];
    [rowReference addColumnFormatInt:@"%@", addjob_floor_user_id];
    [rowReference addColumnFormatInt:@"%@", addjob_floor_search_id];
    [rowReference addColumnFormatString:@"%@", addjob_floor_last_modified];
    [rowReference addColumnFormatInt:@"%@",addjob_floor_number_of_job];
    
    [table loadTableDataWithQuery:sqlString referenceRow:rowReference];
    
    int rowCount = [table countRows];
    NSMutableArray *results = nil;
    if(rowCount > 0)
    {
        results = [NSMutableArray array];
        for (int i = 0; i < rowCount; i ++) {
            
            AddJobFloorModelV2 *model = [[AddJobFloorModelV2 alloc] init];
            DataRow *row = [table rowWithIndex:i];
            DataColumn *curColumn = nil;
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_floor_id];
            model.addjob_floor_id = [curColumn fieldValueInt];
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_floor_name];
            model.addjob_floor_name = [curColumn fieldValueString];
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_floor_name_lang];
            model.addjob_floor_name_lang = [curColumn fieldValueString];
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_floor_user_id];
            model.addjob_floor_user_id = [curColumn fieldValueInt];
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_floor_search_id];
            model.addjob_floor_search_id = [curColumn fieldValueInt];
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_floor_last_modified];
            model.addjob_floor_last_modified = [curColumn fieldValueString];
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_floor_number_of_job];
            model.addjob_floor_number_of_job = [curColumn fieldValueInt];
            
            [results addObject:model];
        }
    }
    
    return results;
}

//MARK: Local AddJob Search Function
-(int) insertAddJobSearch:(AddJobSearchModelV2*)model
{
    if(model == nil){
        return 0;
    }
    
    DataRow *rowInsert = [[DataRow alloc] init];
    rowInsert.tableName = [NSString stringWithFormat:@"%@", ADDJOB_SEARCH];
    DataColumn *columnKey = [rowInsert addColumnFormatInt:@"%@", addjob_search_id];
    //auto increase id
    columnKey.isKeyOnInsert = YES;
    
    [rowInsert addColumnStringValue:model.addjob_search_tasks_id formatName:@"%@", addjob_search_items_id];
    [rowInsert addColumnIntValue:(int)model.addjob_search_user_id formatName:@"%@", addjob_search_user_id];
    [rowInsert addColumnStringValue:model.addjob_search_last_modified formatName:@"%@", addjob_search_last_modified];
    
    return [rowInsert insertDBRow];
}

-(int) updateAddJobSearch:(AddJobSearchModelV2*)model
{
    if(model == nil){
        return 0;
    }
    
    DataRow *rowUpdate = [[DataRow alloc] init];
    rowUpdate.tableName = [NSString stringWithFormat:@"%@", ADDJOB_SEARCH];
    
    DataColumn *columnUpdate1 = [rowUpdate addColumnIntValue:(int)model.addjob_search_id formatName:@"%@",addjob_search_id];
    columnUpdate1.isUpdateCondition = YES;
    
    DataColumn *columnUpdate2 = [rowUpdate addColumnIntValue:(int)model.addjob_search_user_id formatName:@"%@",addjob_search_user_id];
    columnUpdate2.isUpdateCondition = YES;
    
    [rowUpdate addColumnStringValue:model.addjob_search_tasks_id formatName:@"%@",addjob_search_user_id];
    [rowUpdate addColumnStringValue:model.addjob_search_last_modified formatName:@"%@",addjob_search_last_modified];
    
    return [rowUpdate commitChangeDBRow];
}


-(AddJobSearchModelV2*) loadAddJobSearchByUserId:(int)userId tasksFilter:(NSMutableArray*)listItems
{
    if(userId <= 0){
        return nil;
    }
    NSString *tasksIdString = [self convertListTasksIdToString:listItems];
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"select %@, %@, %@, %@ from %@ where %@ = '%@'",
                                  
                                  //Select
                                  addjob_search_id,
                                  addjob_search_items_id,
                                  addjob_search_user_id,
                                  addjob_search_last_modified,
                                  
                                  //from
                                  ADDJOB_SEARCH,
                                  
                                  //where
//                                  addjob_search_user_id,
//                                  userId,
                                  //and
                                  addjob_search_items_id,
                                  tasksIdString];
    
    DataTable *table = [[DataTable alloc] init];
    
    //Row result reference
    DataRow *rowReference = [[DataRow alloc] init];
    [rowReference addColumnFormatInt:@"%@", addjob_search_id];
    [rowReference addColumnFormatString:@"%@", addjob_search_items_id];
    [rowReference addColumnFormatInt:@"%@", addjob_search_user_id];
    [rowReference addColumnFormatString:@"%@", addjob_search_last_modified];
    
    [table loadTableDataWithQuery:sqlString referenceRow:rowReference];
    
    int rowCount = [table countRows];
    AddJobSearchModelV2 *model = nil;
    if(rowCount > 0)
    {
        
        model = [[AddJobSearchModelV2 alloc] init];
        DataRow *row = [table rowWithIndex:0];
        DataColumn *curColumn = nil;
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_search_id];
        model.addjob_search_id = [curColumn fieldValueInt];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_search_items_id];
        model.addjob_search_tasks_id = [curColumn fieldValueString];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_search_user_id];
        model.addjob_search_user_id = [curColumn fieldValueInt];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_search_last_modified];
        model.addjob_search_last_modified = [curColumn fieldValueString];
        
    }
    
    return model;
}

-(int) deleteAddJobSearchBySearchId:(int)searchId
{
    DataTable *tableDelete = [[DataTable alloc] initWithNameFormat:@"%@",ADDJOB_SEARCH];
    
    NSString *queryDelete = [NSString stringWithFormat:@"Delete from %@ Where %@ = %d", ADDJOB_SEARCH, addjob_search_id, searchId];
    int status = (int)[tableDelete excuteQueryNonSelect:queryDelete];
    
    return status;
}

-(void) deleteAllSearchDataFromYesterday
{
    //Today string
    NSString *dateTodayString = [ehkConvert getTodayDateString];
    
    //check whether or not delete old search data
    DataTable *tableCount = [[DataTable alloc] initWithNameFormat:@"%@", ADDJOB_SEARCH];
    NSString *queryCount = [NSString stringWithFormat:@"Select count(1) from %@ where %@ < '%@'", ADDJOB_SEARCH, addjob_search_last_modified, dateTodayString];
    DataRow *rowResult = [[DataRow alloc] init];
    NSString *nameColumn = @"CountResult";
    [rowResult addColumnInt: nameColumn];
    [tableCount loadTableDataWithQuery:queryCount referenceRow:rowResult];
    DataColumn *columnResult = [[tableCount rowWithIndex:0] columnWithName:nameColumn];
    int countResult = [columnResult fieldValueInt];
    
    if(countResult > 0) { //Has search data
        DataTable *tableDelete = [[DataTable alloc] initWithNameFormat:@"%@", ADDJOB_SEARCH];
        [tableDelete dropDBRows]; //Delete all records
        [tableDelete excuteQueryNonSelect:[NSString stringWithFormat:@"REINDEX %@",ADDJOB_SEARCH]]; //Reindex table search
        
        tableDelete.tableName = [NSString stringWithFormat:@"%@",ADDJOB_FLOOR];
        NSString *queryDelete = [NSString stringWithFormat:@"Delete from %@ Where %@ < '%@' and %@ > 0", ADDJOB_FLOOR, addjob_floor_last_modified, dateTodayString, addjob_floor_search_id];
        [tableDelete excuteQueryNonSelect:queryDelete];
    }
}

//MARK: Local AddJob Status
-(int) insertAddJobStatus:(AddJobStatusModel*)model
{
    if(model == nil){
        return 0;
    }
    
    DataRow *rowInsert = [[DataRow alloc] init];
    rowInsert.tableName = [NSString stringWithFormat:@"%@", ADDJOB_STATUS];
    
    [rowInsert addColumnIntValue:(int)model.addjob_status_id formatName:@"%@", addjob_status_id];
    [rowInsert addColumnStringValue:model.addjob_status_name formatName:@"%@", addjob_status_name];
    [rowInsert addColumnStringValue:model.addjob_status_lang formatName:@"%@", addjob_status_lang];
    [rowInsert addColumnStringValue:model.addjob_status_last_modified formatName:@"%@", addjob_status_last_modified];
    
    return [rowInsert insertDBRow];
}

-(int) deleteAllAddJobStatus
{
    DataTable *tableDelete = [[DataTable alloc] initWithNameFormat:@"%@", ADDJOB_STATUS];
    return [tableDelete dropDBRows];
}


//MARK: Local AddJob Item

-(int) insertAddJobItem:(AddJobItemModelV2*)model
{
    if(model == nil){
        return 0;
    }
    
    DataRow *rowInsert = [[DataRow alloc] init];
    rowInsert.tableName = [NSString stringWithFormat:@"%@", ADDJOB_ITEM];
    
    [rowInsert addColumnIntValue:(int)model.addjob_item_id formatName:@"%@", addjob_item_id];
    [rowInsert addColumnStringValue:model.addjob_item_name formatName:@"%@", addjob_item_name];
    [rowInsert addColumnStringValue:model.addjob_item_name_lang formatName:@"%@", addjob_item_name_lang];
    [rowInsert addColumnStringValue:model.addjob_item_name_lang2 formatName:@"%@", addjob_item_name_lang2];
    [rowInsert addColumnIntValue:(int)model.addjob_item_onday_addjob_id formatName:@"%@", addjob_item_onday_addjob_id];
    [rowInsert addColumnIntValue:(int)model.addjob_item_user_id formatName:@"%@", addjob_item_user_id];
    [rowInsert addColumnStringValue:model.addjob_item_last_modified formatName:@"%@", addjob_item_last_modified];
    
    return [rowInsert insertDBRow];
}

-(int) updateAddJobItem:(AddJobItemModelV2*)model
{
    if(model == nil){
        return 0;
    }
    
    DataRow *rowUpdate = [[DataRow alloc] init];
    rowUpdate.tableName = [NSString stringWithFormat:@"%@", ADDJOB_ITEM];
    
    DataColumn *columnUpdate1 = [rowUpdate addColumnIntValue:(int)model.addjob_item_id formatName:@"%@",addjob_item_id];
    columnUpdate1.isUpdateCondition = YES;
    
    DataColumn *columnUpdate2 = [rowUpdate addColumnIntValue:(int)model.addjob_item_user_id formatName:@"%@",addjob_item_user_id];
    columnUpdate2.isUpdateCondition = YES;
    
    DataColumn *columnUpdate3 =  [rowUpdate addColumnIntValue:(int)model.addjob_item_onday_addjob_id formatName:@"%@",addjob_item_onday_addjob_id];
    columnUpdate3.isUpdateCondition = YES;
    
    [rowUpdate addColumnStringValue:model.addjob_item_name formatName:@"%@",addjob_item_name];
    [rowUpdate addColumnStringValue:model.addjob_item_name_lang formatName:@"%@",addjob_item_name_lang];
    [rowUpdate addColumnStringValue:model.addjob_item_name_lang2 formatName:@"%@",addjob_item_name_lang2];
    [rowUpdate addColumnIntValue:(int)model.addjob_item_user_id formatName:@"%@",addjob_item_user_id];
    [rowUpdate addColumnStringValue:model.addjob_item_last_modified formatName:@"%@",addjob_item_last_modified];
    
    return [rowUpdate commitChangeDBRow];
}

-(int) deleteAddJobItemByUserId:(int)userId
{
    DataTable *tableDelete = [[DataTable alloc] initWithNameFormat:@"%@",ADDJOB_ITEM];
    
    NSString *queryDelete = [NSString stringWithFormat:@"Delete from %@ Where %@ = %d", ADDJOB_ITEM, addjob_item_user_id, userId];
    int status = (int)[tableDelete excuteQueryNonSelect:queryDelete];
    
    return status;
}

-(int) deleteAllAddJobItems
{
    DataTable *tableDelete = [[DataTable alloc] initWithNameFormat:@"%@",ADDJOB_ITEM];
    return [tableDelete dropDBRows];
}

-(AddJobItemModelV2*) loadAddJobItemByUserId:(int)userId itemId:(int)itemid
{
    if(userId <= 0){
        return nil;
    }
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"select %@, %@, %@, %@, %@, %@, %@ from %@ where %@ = %d and %@ = %d and %@ = %d",
                                  
                                  //Select
                                  addjob_item_id,
                                  addjob_item_last_modified,
                                  addjob_item_name,
                                  addjob_item_name_lang,
                                  addjob_item_name_lang2,
                                  addjob_item_onday_addjob_id,
                                  addjob_item_user_id,
                                  
                                  //from
                                  ADDJOB_ITEM,
                                  
                                  //where
                                  addjob_item_user_id,
                                  userId,
                                  //and
                                  addjob_item_id,
                                  itemid,
                                  //and
                                  addjob_item_onday_addjob_id,
                                  0];
    
    DataTable *table = [[DataTable alloc] init];
    
    //Row result reference
    DataRow *rowReference = [[DataRow alloc] init];
    [rowReference addColumnFormatInt:@"%@", addjob_item_id];
    [rowReference addColumnFormatString:@"%@", addjob_item_last_modified];
    [rowReference addColumnFormatString:@"%@", addjob_item_name];
    [rowReference addColumnFormatString:@"%@", addjob_item_name_lang];
    [rowReference addColumnFormatString:@"%@", addjob_item_name_lang2];
    [rowReference addColumnFormatInt:@"%@", addjob_item_onday_addjob_id];
    [rowReference addColumnFormatInt:@"%@",addjob_item_user_id];
    
    [table loadTableDataWithQuery:sqlString referenceRow:rowReference];
    
    int rowCount = [table countRows];
    AddJobItemModelV2 *model = nil;
    if(rowCount > 0)
    {
        model = [[AddJobItemModelV2 alloc] init];
        DataRow *row = [table rowWithIndex:0];
        DataColumn *curColumn = nil;
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_item_id];
        model.addjob_item_id = [curColumn fieldValueInt];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_item_last_modified];
        model.addjob_item_last_modified = [curColumn fieldValueString];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_item_name];
        model.addjob_item_name = [curColumn fieldValueString];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_item_name_lang];
        model.addjob_item_name_lang = [curColumn fieldValueString];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_item_name_lang2];
        model.addjob_item_onday_addjob_id = [curColumn fieldValueString];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_item_onday_addjob_id];
        model.addjob_item_onday_addjob_id = [curColumn fieldValueInt];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_item_user_id];
        model.addjob_item_user_id = [curColumn fieldValueInt];
        
    }
    
    return model;
}

-(NSMutableArray*) loadAllAddJobItemByUserId:(int)userId
{
    if(userId <= 0){
        return nil;
    }
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"select %@, %@, %@, %@, %@, %@, %@ from %@ where %@ = %d and %@ = %d",
                                  
                                  //Select
                                  addjob_item_id,
                                  addjob_item_last_modified,
                                  addjob_item_name,
                                  addjob_item_name_lang,
                                  addjob_item_name_lang2,
                                  addjob_item_onday_addjob_id,
                                  addjob_item_user_id,
                                  
                                  //from
                                  ADDJOB_ITEM,
                                  
                                  //where
                                  addjob_item_user_id,
                                  userId,
                                  
                                  //and
                                  addjob_item_onday_addjob_id,
                                  0];
    
    DataTable *table = [[DataTable alloc] init];
    
    //Row result reference
    DataRow *rowReference = [[DataRow alloc] init];
    [rowReference addColumnFormatInt:@"%@", addjob_item_id];
    [rowReference addColumnFormatString:@"%@", addjob_item_last_modified];
    [rowReference addColumnFormatString:@"%@", addjob_item_name];
    [rowReference addColumnFormatString:@"%@", addjob_item_name_lang];
    [rowReference addColumnFormatString:@"%@", addjob_item_name_lang2];
    [rowReference addColumnFormatInt:@"%@", addjob_item_onday_addjob_id];
    [rowReference addColumnFormatInt:@"%@",addjob_item_user_id];
    
    [table loadTableDataWithQuery:sqlString referenceRow:rowReference];
    
    int rowCount = [table countRows];
    NSMutableArray *results = nil;
    if(rowCount > 0)
    {
        results = [NSMutableArray array];
        for (int i = 0; i < rowCount; i ++) {
            
            AddJobItemModelV2 *model = [[AddJobItemModelV2 alloc] init];
            DataRow *row = [table rowWithIndex:i];
            DataColumn *curColumn = nil;
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_item_id];
            model.addjob_item_id = [curColumn fieldValueInt];
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_item_last_modified];
            model.addjob_item_last_modified = [curColumn fieldValueString];
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_item_name];
            model.addjob_item_name = [curColumn fieldValueString];
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_item_name_lang];
            model.addjob_item_name_lang = [curColumn fieldValueString];
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_item_name_lang2];
            model.addjob_item_onday_addjob_id = [curColumn fieldValueInt];
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_item_onday_addjob_id];
            model.addjob_item_onday_addjob_id = [curColumn fieldValueString];
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_item_user_id];
            model.addjob_item_user_id = [curColumn fieldValueInt];
            
            [results addObject:model];
        }
    }
    
    return results;
    
}

-(NSMutableArray*) loadAllAddJobItemsByUserId:(int)userId ondayAddJobId:(int)ondayAddJobId
{
    if(userId <= 0){
        return nil;
    }
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"select %@, %@, %@, %@, %@, %@, %@ from %@ a, %@ b where b.%@ = %d and b.%@ = a.%@",
                                  
                                  //Select
                                  addjob_item_id,
                                  addjob_item_last_modified,
                                  addjob_item_name,
                                  addjob_item_name_lang,
                                  addjob_item_name_lang2,
                                  addjob_item_onday_addjob_id,
                                  addjob_item_user_id,
                                  
                                  //from
                                  ADDJOB_ITEM,
                                  ADDJOB_ROOM_ITEM,
                                  
                                  //where
//                                  addjob_item_user_id,
//                                  userId,
                                  
                                  //and
                                  addjob_roomitem_onday_addjob_id,
                                  ondayAddJobId,
                                  //and
//                                  addjob_roomitem_user_id,
//                                  userId,
                                  //and
                                  addjob_roomitem_item_id,
                                  addjob_item_id
                                  ];
    
    DataTable *table = [[DataTable alloc] init];
    
    //Row result reference
    DataRow *rowReference = [[DataRow alloc] init];
    [rowReference addColumnFormatInt:@"%@", addjob_item_id];
    [rowReference addColumnFormatString:@"%@", addjob_item_last_modified];
    [rowReference addColumnFormatString:@"%@", addjob_item_name];
    [rowReference addColumnFormatString:@"%@", addjob_item_name_lang];
    [rowReference addColumnFormatString:@"%@", addjob_item_name_lang2];
    [rowReference addColumnFormatInt:@"%@", addjob_item_onday_addjob_id];
    [rowReference addColumnFormatInt:@"%@",addjob_item_user_id];
    
    [table loadTableDataWithQuery:sqlString referenceRow:rowReference];
    
    int rowCount = [table countRows];
    NSMutableArray *results = nil;
    if(rowCount > 0)
    {
        results = [NSMutableArray array];
        for (int i = 0; i < rowCount; i ++) {
            
            AddJobItemModelV2 *model = [[AddJobItemModelV2 alloc] init];
            DataRow *row = [table rowWithIndex:i];
            DataColumn *curColumn = nil;
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_item_id];
            model.addjob_item_id = [curColumn fieldValueInt];
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_item_last_modified];
            model.addjob_item_last_modified = [curColumn fieldValueString];
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_item_name];
            model.addjob_item_name = [curColumn fieldValueString];
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_item_name_lang];
            model.addjob_item_name_lang = [curColumn fieldValueString];
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_item_name_lang2];
            model.addjob_item_onday_addjob_id = [curColumn fieldValueInt];
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_item_onday_addjob_id];
            model.addjob_item_onday_addjob_id = [curColumn fieldValueString];
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_item_user_id];
            model.addjob_item_user_id = [curColumn fieldValueInt];
            
            [results addObject:model];
        }
    }
    
    return results;
    
}

//MARK: Local AddJob Room
-(int) insertAddJobRoom:(AddJobRoomModelV2*)model
{
    if(model == nil){
        return 0;
    }
    
    //Default value for additional job room remark
    if(model.addjob_room_remark == nil){
        model.addjob_room_remark = @"";
    }
    
    DataRow *rowInsert = [[DataRow alloc] init];
    rowInsert.tableName = [NSString stringWithFormat:@"%@", ADDJOB_ROOM];
    
    [rowInsert addColumnIntValue:(int)model.addjob_room_assign_id formatName:@"%@", addjob_room_assign_id];
    [rowInsert addColumnIntValue:(int)model.addjob_room_floor_id formatName:@"%@", addjob_room_floor_id];
    [rowInsert addColumnIntValue:(int)model.addjob_room_id formatName:@"%@", addjob_room_id];
    [rowInsert addColumnIntValue:(int)model.addjob_room_is_due_out formatName:@"%@", addjob_room_is_due_out];
    [rowInsert addColumnStringValue:model.addjob_room_last_modified formatName:@"%@", addjob_room_last_modified];
    [rowInsert addColumnStringValue:model.addjob_room_number formatName:@"%@", addjob_room_number];
    [rowInsert addColumnIntValue:(int)model.addjob_room_number_pending_job formatName:@"%@", addjob_room_number_pending_job];
    [rowInsert addColumnStringValue:model.addjob_room_remark formatName:@"%@", addjob_room_remark];
    [rowInsert addColumnIntValue:(int)model.addjob_room_search_id formatName:@"%@", addjob_room_search_id];
    [rowInsert addColumnIntValue:(int)model.addjob_room_status_id formatName:@"%@", addjob_room_status_id];
    [rowInsert addColumnIntValue:(int)model.addjob_room_type_id formatName:@"%@", addjob_room_type_id];
    [rowInsert addColumnIntValue:(int)model.addjob_room_user_id formatName:@"%@", addjob_room_user_id];
    
    return [rowInsert insertDBRow];
}

-(int) updateAddJobRoom:(AddJobRoomModelV2*)model
{
    if(model == nil){
        return 0;
    }
    
    //Default value for additional job room remark
    if(model.addjob_room_remark == nil){
        model.addjob_room_remark = @"";
    }
    
    DataRow *rowUpdate = [[DataRow alloc] init];
    rowUpdate.tableName = [NSString stringWithFormat:@"%@", ADDJOB_ROOM];
    
    DataColumn *columnUpdate1 = [rowUpdate addColumnInt:[NSString stringWithFormat:@"%@",addjob_room_id] value:(int)model.addjob_room_id];
    columnUpdate1.isUpdateCondition = YES;
    DataColumn *columnUpdate2 = [rowUpdate addColumnInt:[NSString stringWithFormat:@"%@",addjob_room_search_id] value:(int)model.addjob_room_search_id];
    columnUpdate2.isUpdateCondition = YES;
    DataColumn *columnUpdate3 = [rowUpdate addColumnInt:[NSString stringWithFormat:@"%@",addjob_room_user_id] value:(int)model.addjob_room_user_id];
    columnUpdate3.isUpdateCondition = YES;
    DataColumn *columnUpdate4 = [rowUpdate addColumnIntValue:(int)model.addjob_room_assign_id formatName:@"%@",addjob_room_assign_id];
    columnUpdate4.isUpdateCondition = YES;
    
    [rowUpdate addColumnIntValue:(int)model.addjob_room_floor_id formatName:@"%@",addjob_room_floor_id];
    [rowUpdate addColumnIntValue:(int)model.addjob_room_is_due_out formatName:@"%@",addjob_room_is_due_out];
    [rowUpdate addColumnStringValue:model.addjob_room_last_modified formatName:@"%@",addjob_room_last_modified];
    [rowUpdate addColumnStringValue:model.addjob_room_number formatName:@"%@",addjob_room_number];
    [rowUpdate addColumnIntValue:(int)model.addjob_room_number_pending_job formatName:@"%@",addjob_room_number_pending_job];
    [rowUpdate addColumnStringValue:model.addjob_room_remark formatName:@"%@",addjob_room_remark];
    [rowUpdate addColumnIntValue:(int)model.addjob_room_type_id formatName:@"%@",addjob_room_type_id];
    [rowUpdate addColumnIntValue:(int)model.addjob_room_status_id formatName:@"%@",addjob_room_status_id];
    
    return [rowUpdate commitChangeDBRow];
}

-(int) deleteAddJobRoomByUserId:(int)userId
{
    if(userId <= 0){
        return 0;
    }
    
    DataTable *tableAddJobRoom = [[DataTable alloc] initWithNameFormat:@"%@", ADDJOB_ROOM];
    NSString *queryDelete = [NSString stringWithFormat:@"Delete from %@ Where %@ = %d", ADDJOB_ROOM, addjob_room_user_id, userId];
    int status = (int)[tableAddJobRoom excuteQueryNonSelect:queryDelete];
    
    return status;
}

-(int) deleteAddJobRoomByUserId:(int)userId floorId:(int)floorId searchId:(int)searchId
{
    if(userId <= 0){
        return 0;
    }
    
    if(searchId < 0) {
        searchId = 0;
    }
    
    DataTable *tableAddJobRoom = [[DataTable alloc] initWithNameFormat:@"%@", ADDJOB_ROOM];
    NSString *queryDelete = [NSString stringWithFormat:@"Delete from %@ Where %@ = %d and %@ = %d and %@ = %d", ADDJOB_ROOM, addjob_room_user_id, userId, addjob_room_floor_id, floorId, addjob_room_search_id, searchId];
    int status = (int)[tableAddJobRoom excuteQueryNonSelect:queryDelete];
    
    return status;
}

-(int) deleteAddJobRoomByUserId:(int)userId roomAssignId:(int)roomAssignId searchId:(int)searchId
{
    if(userId <= 0){
        return 0;
    }
    
    if(searchId < 0) {
        searchId = 0;
    }
    
    DataTable *tableAddJobRoom = [[DataTable alloc] initWithNameFormat:@"%@", ADDJOB_ROOM];
    NSString *queryDelete = [NSString stringWithFormat:@"Delete from %@ Where %@ = %d and %@ = %d", ADDJOB_ROOM, addjob_room_assign_id, roomAssignId, addjob_room_search_id, searchId];
    int status = (int)[tableAddJobRoom excuteQueryNonSelect:queryDelete];
    
    return status;
}

-(int) deleteAllAddJobRooms
{
    DataTable *tableAddJobRoom = [[DataTable alloc] initWithNameFormat:@"%@", ADDJOB_ROOM];
    return [tableAddJobRoom dropDBRows];
}

//using QR Code, pass floorId = 0
-(NSMutableArray*)loadAddJobRoomsByUserId:(int)userId floorId:(int)floorId searchId:(int)searchId
{
    if(userId <= 0){
        return nil;
    }
    
    if(searchId < 0){
        searchId = 0;
    }
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"select %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ from %@ where %@ = %d and %@ = %d order by %@ asc",
                                  //select
                                  addjob_room_assign_id,
                                  addjob_room_floor_id,
                                  addjob_room_id,
                                  addjob_room_is_due_out,
                                  addjob_room_last_modified,
                                  addjob_room_number,
                                  addjob_room_number_pending_job,
                                  addjob_room_remark,
                                  addjob_room_search_id,
                                  addjob_room_status_id,
                                  addjob_room_type_id,
                                  addjob_room_user_id,
                                  
                                  //From
                                  ADDJOB_ROOM,
                                  
                                  //where
//                                  addjob_room_user_id,
//                                  userId,
                                  //and
                                  addjob_room_floor_id,
                                  floorId,
                                  //and
                                  addjob_room_search_id,
                                  searchId,
                                  // order by
                                  addjob_room_number];
    
    DataTable *table = [[DataTable alloc] init];
    
    //Row result reference
    DataRow *rowReference = [[DataRow alloc] init];
    [rowReference addColumnFormatInt:@"%@", addjob_room_assign_id];
    [rowReference addColumnFormatInt:@"%@",addjob_room_floor_id];
    [rowReference addColumnFormatInt:@"%@", addjob_room_id];
    [rowReference addColumnFormatInt:@"%@",addjob_room_is_due_out];
    [rowReference addColumnFormatString:@"%@", addjob_room_last_modified];
    [rowReference addColumnFormatString:@"%@",addjob_room_number];
    [rowReference addColumnFormatInt:@"%@", addjob_room_number_pending_job];
    [rowReference addColumnFormatString:@"%@",addjob_room_remark];
    [rowReference addColumnFormatInt:@"%@", addjob_room_search_id];
    [rowReference addColumnFormatInt:@"%@",addjob_room_status_id];
    [rowReference addColumnFormatInt:@"%@", addjob_room_type_id];
    [rowReference addColumnFormatInt:@"%@",addjob_room_user_id];
    
    [table loadTableDataWithQuery:sqlString referenceRow:rowReference];
    
    int rowCount = [table countRows];
    NSMutableArray *results = nil;
    if(rowCount > 0)
    {
        results = [NSMutableArray array];
        for (int i = 0; i < rowCount; i ++) {
            
            AddJobRoomModelV2 *model = [[AddJobRoomModelV2 alloc] init];
            DataRow *row = [table rowWithIndex:i];
            DataColumn *curColumn = nil;
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_room_assign_id];
            model.addjob_room_assign_id = [curColumn fieldValueInt];
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_room_floor_id];
            model.addjob_room_floor_id = [curColumn fieldValueInt];
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_room_id];
            model.addjob_room_id = [curColumn fieldValueInt];
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_room_is_due_out];
            model.addjob_room_is_due_out = [curColumn fieldValueInt];
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_room_last_modified];
            model.addjob_room_last_modified = [curColumn fieldValueString];
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_room_number];
            model.addjob_room_number = [curColumn fieldValueString];
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_room_number_pending_job];
            model.addjob_room_number_pending_job = [curColumn fieldValueInt];
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_room_remark];
            model.addjob_room_remark = [curColumn fieldValueString];
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_room_search_id];
            model.addjob_room_search_id = [curColumn fieldValueInt];
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_room_status_id];
            model.addjob_room_status_id = [curColumn fieldValueInt];
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_room_type_id];
            model.addjob_room_type_id = [curColumn fieldValueInt];
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_room_user_id];
            model.addjob_room_user_id = [curColumn fieldValueInt];
            
            [results addObject:model];
        }
    }
    
    return results;
}

-(AddJobRoomModelV2*) loadAddJobRoomByUserId:(int)userId roomId:(int)roomId roomAssignId:(int)roomAssignId searchId:(int)searchId
{
    if(userId <= 0){
        return nil;
    }
    
    if(roomId < 0){
        roomId = 0;
    }
    
    if(roomAssignId < 0){
        roomAssignId = 0;
    }
    
    if(searchId <0){
        searchId = 0;
    }
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"select %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ from %@ where %@ = %d and %@ = %d",
                                  //select
                                  addjob_room_assign_id,
                                  addjob_room_floor_id,
                                  addjob_room_id,
                                  addjob_room_is_due_out,
                                  addjob_room_last_modified,
                                  addjob_room_number,
                                  addjob_room_number_pending_job,
                                  addjob_room_remark,
                                  addjob_room_search_id,
                                  addjob_room_status_id,
                                  addjob_room_type_id,
                                  addjob_room_user_id,
                                  
                                  //From
                                  ADDJOB_ROOM,
                                  
                                  //where
//                                  addjob_room_user_id,
//                                  userId,
                                  //and
//                                  addjob_room_id,
//                                  roomId,
                                  //and
                                  addjob_room_assign_id,
                                  roomAssignId,
                                  //and
                                  addjob_room_search_id,
                                  searchId];
    
    DataTable *table = [[DataTable alloc] init];
    
    //Row result reference
    DataRow *rowReference = [[DataRow alloc] init];
    [rowReference addColumnFormatInt:@"%@", addjob_room_assign_id];
    [rowReference addColumnFormatInt:@"%@",addjob_room_floor_id];
    [rowReference addColumnFormatInt:@"%@", addjob_room_id];
    [rowReference addColumnFormatInt:@"%@",addjob_room_is_due_out];
    [rowReference addColumnFormatString:@"%@", addjob_room_last_modified];
    [rowReference addColumnFormatString:@"%@",addjob_room_number];
    [rowReference addColumnFormatInt:@"%@", addjob_room_number_pending_job];
    [rowReference addColumnFormatString:@"%@",addjob_room_remark];
    [rowReference addColumnFormatInt:@"%@", addjob_room_search_id];
    [rowReference addColumnFormatInt:@"%@",addjob_room_status_id];
    [rowReference addColumnFormatInt:@"%@", addjob_room_type_id];
    [rowReference addColumnFormatInt:@"%@",addjob_room_user_id];
    
    [table loadTableDataWithQuery:sqlString referenceRow:rowReference];
    
    int rowCount = [table countRows];
    AddJobRoomModelV2 *model = nil;
    
    if(rowCount > 0)
    {
        
        model = [[AddJobRoomModelV2 alloc] init];
        DataRow *row = [table rowWithIndex:0];
        
        DataColumn *curColumn = nil;
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_room_assign_id];
        model.addjob_room_assign_id = [curColumn fieldValueInt];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_room_floor_id];
        model.addjob_room_floor_id = [curColumn fieldValueInt];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_room_id];
        model.addjob_room_id = [curColumn fieldValueInt];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_room_is_due_out];
        model.addjob_room_is_due_out = [curColumn fieldValueInt];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_room_last_modified];
        model.addjob_room_last_modified = [curColumn fieldValueString];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_room_number];
        model.addjob_room_number = [curColumn fieldValueString];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_room_number_pending_job];
        model.addjob_room_number_pending_job = [curColumn fieldValueInt];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_room_remark];
        model.addjob_room_remark = [curColumn fieldValueString];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_room_search_id];
        model.addjob_room_search_id = [curColumn fieldValueInt];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_room_status_id];
        model.addjob_room_status_id = [curColumn fieldValueInt];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_room_type_id];
        model.addjob_room_type_id = [curColumn fieldValueInt];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_room_user_id];
        model.addjob_room_user_id = [curColumn fieldValueInt];
    }
    
    return model;
}


-(AddJobRoomModelV2*) loadAddJobRoomByRoomNumber:(int)roomId searchId:(int)searchId
{
    
    if(searchId <0){
        searchId = 0;
    }
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"select %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ from %@ where %@ = %d and %@ = %d",
                                  //select
                                  addjob_room_assign_id,
                                  addjob_room_floor_id,
                                  addjob_room_id,
                                  addjob_room_is_due_out,
                                  addjob_room_last_modified,
                                  addjob_room_number,
                                  addjob_room_number_pending_job,
                                  addjob_room_remark,
                                  addjob_room_search_id,
                                  addjob_room_status_id,
                                  addjob_room_type_id,
                                  addjob_room_user_id,
                                  
                                  //From
                                  ADDJOB_ROOM,
                                  
                                  //where
                                  addjob_room_id,
                                  roomId,
                                  //and
                                  addjob_room_search_id,
                                  searchId];
    
    DataTable *table = [[DataTable alloc] init];
    
    //Row result reference
    DataRow *rowReference = [[DataRow alloc] init];
    [rowReference addColumnFormatInt:@"%@", addjob_room_assign_id];
    [rowReference addColumnFormatInt:@"%@",addjob_room_floor_id];
    [rowReference addColumnFormatInt:@"%@", addjob_room_id];
    [rowReference addColumnFormatInt:@"%@",addjob_room_is_due_out];
    [rowReference addColumnFormatString:@"%@", addjob_room_last_modified];
    [rowReference addColumnFormatString:@"%@",addjob_room_number];
    [rowReference addColumnFormatInt:@"%@", addjob_room_number_pending_job];
    [rowReference addColumnFormatString:@"%@",addjob_room_remark];
    [rowReference addColumnFormatInt:@"%@", addjob_room_search_id];
    [rowReference addColumnFormatInt:@"%@",addjob_room_status_id];
    [rowReference addColumnFormatInt:@"%@", addjob_room_type_id];
    [rowReference addColumnFormatInt:@"%@",addjob_room_user_id];
    
    [table loadTableDataWithQuery:sqlString referenceRow:rowReference];
    
    int rowCount = [table countRows];
    AddJobRoomModelV2 *model = nil;
    
    if(rowCount > 0)
    {
        
        model = [[AddJobRoomModelV2 alloc] init];
        DataRow *row = [table rowWithIndex:0];
        
        DataColumn *curColumn = nil;
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_room_assign_id];
        model.addjob_room_assign_id = [curColumn fieldValueInt];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_room_floor_id];
        model.addjob_room_floor_id = [curColumn fieldValueInt];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_room_id];
        model.addjob_room_id = [curColumn fieldValueInt];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_room_is_due_out];
        model.addjob_room_is_due_out = [curColumn fieldValueInt];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_room_last_modified];
        model.addjob_room_last_modified = [curColumn fieldValueString];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_room_number];
        model.addjob_room_number = [curColumn fieldValueString];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_room_number_pending_job];
        model.addjob_room_number_pending_job = [curColumn fieldValueInt];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_room_remark];
        model.addjob_room_remark = [curColumn fieldValueString];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_room_search_id];
        model.addjob_room_search_id = [curColumn fieldValueInt];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_room_status_id];
        model.addjob_room_status_id = [curColumn fieldValueInt];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_room_type_id];
        model.addjob_room_type_id = [curColumn fieldValueInt];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_room_user_id];
        model.addjob_room_user_id = [curColumn fieldValueInt];
    }
    
    return model;
}

//This function apply for QR code scanning
//QR code storage database use same common data local (without search, searchId = 0)
-(AddJobRoomModelV2*) loadAddJobRoomQRCodeByUserId:(int)userId roomNumber:(NSString*)roomNumber
{
    if(userId <= 0){
        return nil;
    }
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"select %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ from %@ where %@ = %d and %@ = %@ and %@ = %d",
                                  //select
                                  addjob_room_assign_id,
                                  addjob_room_floor_id,
                                  addjob_room_id,
                                  addjob_room_is_due_out,
                                  addjob_room_last_modified,
                                  addjob_room_number,
                                  addjob_room_number_pending_job,
                                  addjob_room_remark,
                                  addjob_room_search_id,
                                  addjob_room_status_id,
                                  addjob_room_type_id,
                                  addjob_room_user_id,
                                  
                                  //From
                                  ADDJOB_ROOM,
                                  
                                  //where
                                  addjob_room_user_id,
                                  userId,
                                  //and
                                  addjob_room_number,
                                  roomNumber,
                                  //and
                                  addjob_room_search_id,
                                  0];
    
    DataTable *table = [[DataTable alloc] init];
    
    //Row result reference
    DataRow *rowReference = [[DataRow alloc] init];
    [rowReference addColumnFormatInt:@"%@", addjob_room_assign_id];
    [rowReference addColumnFormatInt:@"%@",addjob_room_floor_id];
    [rowReference addColumnFormatInt:@"%@", addjob_room_id];
    [rowReference addColumnFormatInt:@"%@",addjob_room_is_due_out];
    [rowReference addColumnFormatString:@"%@", addjob_room_last_modified];
    [rowReference addColumnFormatString:@"%@",addjob_room_number];
    [rowReference addColumnFormatInt:@"%@", addjob_room_number_pending_job];
    [rowReference addColumnFormatString:@"%@",addjob_room_remark];
    [rowReference addColumnFormatInt:@"%@", addjob_room_search_id];
    [rowReference addColumnFormatInt:@"%@",addjob_room_status_id];
    [rowReference addColumnFormatInt:@"%@", addjob_room_type_id];
    [rowReference addColumnFormatInt:@"%@",addjob_room_user_id];
    
    [table loadTableDataWithQuery:sqlString referenceRow:rowReference];
    
    int rowCount = [table countRows];
    AddJobRoomModelV2 *model = nil;
    
    if(rowCount > 0)
    {
        
        model = [[AddJobRoomModelV2 alloc] init];
        DataRow *row = [table rowWithIndex:0];
        
        DataColumn *curColumn = nil;
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_room_assign_id];
        model.addjob_room_assign_id = [curColumn fieldValueInt];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_room_floor_id];
        model.addjob_room_floor_id = [curColumn fieldValueInt];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_room_id];
        model.addjob_room_id = [curColumn fieldValueInt];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_room_is_due_out];
        model.addjob_room_is_due_out = [curColumn fieldValueInt];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_room_last_modified];
        model.addjob_room_last_modified = [curColumn fieldValueString];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_room_number];
        model.addjob_room_number = [curColumn fieldValueString];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_room_number_pending_job];
        model.addjob_room_number_pending_job = [curColumn fieldValueInt];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_room_remark];
        model.addjob_room_remark = [curColumn fieldValueString];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_room_search_id];
        model.addjob_room_search_id = [curColumn fieldValueInt];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_room_status_id];
        model.addjob_room_status_id = [curColumn fieldValueInt];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_room_type_id];
        model.addjob_room_type_id = [curColumn fieldValueInt];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_room_user_id];
        model.addjob_room_user_id = [curColumn fieldValueInt];
    }
    
    return model;
}


-(int) insertAddJobCategory:(AddJobCategoryModelV2*)model
{
    if(model == nil){
        return 0;
    }
    
    DataRow *rowInsert = [[DataRow alloc] init];
    rowInsert.tableName = [NSString stringWithFormat:@"%@", ADDJOB_CATEGORY];
    
    [rowInsert addColumnIntValue:(int)model.addjob_category_id formatName:@"%@", addjob_category_id];
    [rowInsert addColumnStringValue:model.addjob_category_last_modified formatName:@"%@", addjob_category_last_modified];
    [rowInsert addColumnStringValue:model.addjob_category_name formatName:@"%@", addjob_category_name];
    [rowInsert addColumnStringValue:model.addjob_category_name_lang formatName:@"%@", addjob_category_name_lang];
    [rowInsert addColumnStringValue:model.addjob_category_name_lang2 formatName:@"%@", addjob_category_name_lang2];
    [rowInsert addColumnIntValue:(int)model.addjob_category_user_id formatName:@"%@", addjob_category_user_id];
    return [rowInsert insertDBRow];
    
}

-(int) updateAddJobCategory:(AddJobCategoryModelV2*)model
{
    if(model == nil){
        return 0;
    }
    
    DataRow *rowUpdate = [[DataRow alloc] init];
    rowUpdate.tableName = [NSString stringWithFormat:@"%@", ADDJOB_CATEGORY];
    
    DataColumn *columnUpdate1 = [rowUpdate addColumnInt:[NSString stringWithFormat:@"%@",addjob_category_id] value:(int)model.addjob_category_id];
    columnUpdate1.isUpdateCondition = YES;
    DataColumn *columnUpdate2 = [rowUpdate addColumnInt:[NSString stringWithFormat:@"%@",addjob_category_user_id] value:(int)model.addjob_category_user_id];
    columnUpdate2.isUpdateCondition = YES;
    
    [rowUpdate addColumnStringValue:model.addjob_category_last_modified formatName:@"%@",addjob_category_last_modified];
    [rowUpdate addColumnStringValue:model.addjob_category_name formatName:@"%@",addjob_category_name];
    [rowUpdate addColumnStringValue:model.addjob_category_name_lang formatName:@"%@",addjob_category_name_lang];
    [rowUpdate addColumnStringValue:model.addjob_category_name_lang2 formatName:@"%@",addjob_category_name_lang2];
    
    return [rowUpdate commitChangeDBRow];
}

-(int) deleteAddJobCategoryByUserId:(int)userId
{
    if(userId <= 0){
        return 0; 
    }
    
    DataTable *tableDelete = [[DataTable alloc] initWithNameFormat:@"%@",ADDJOB_CATEGORY];
    NSString *queryDelete = [NSString stringWithFormat:@"Delete from %@ Where %@ = %d", ADDJOB_CATEGORY, addjob_category_user_id, userId];
    int status = (int)[tableDelete excuteQueryNonSelect:queryDelete];
    
    return status;
}

-(int) deleteAllAddJobCategories
{
    DataTable *tableDelete = [[DataTable alloc] initWithNameFormat:@"%@",ADDJOB_CATEGORY];
    return [tableDelete dropDBRows];
}

-(NSMutableArray*)loadAddJoCategoriesByUserId:(int)userId
{
    if(userId <= 0){
        return nil;
    }
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"select %@, %@, %@, %@, %@, %@ from %@ where %@ = %d",
                                  //select
                                  addjob_category_id,
                                  addjob_category_last_modified,
                                  addjob_category_name,
                                  addjob_category_name_lang,
                                  addjob_category_name_lang2,
                                  addjob_category_user_id,
                                  
                                  //From
                                  ADDJOB_CATEGORY,
                                  
                                  //where
                                  addjob_category_user_id,
                                  userId];
    
    DataTable *table = [[DataTable alloc] init];
    
    //Row result reference
    DataRow *rowReference = [[DataRow alloc] init];
    [rowReference addColumnFormatInt:@"%@", addjob_category_id];
    [rowReference addColumnFormatString:@"%@",addjob_category_last_modified];
    [rowReference addColumnFormatString:@"%@", addjob_category_name];
    [rowReference addColumnFormatString:@"%@",addjob_category_name_lang];
    [rowReference addColumnFormatString:@"%@", addjob_category_name_lang2];
    [rowReference addColumnFormatInt:@"%@", addjob_category_user_id];
    
    [table loadTableDataWithQuery:sqlString referenceRow:rowReference];
    
    int rowCount = [table countRows];
    NSMutableArray *results = nil;
    if(rowCount > 0)
    {
        for (int i = 0; i < rowCount; i ++) {
            
            AddJobCategoryModelV2 *model = [[AddJobCategoryModelV2 alloc] init];
            DataRow *row = [table rowWithIndex:i];
            DataColumn *curColumn = nil;
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_category_id];
            model.addjob_category_id = [curColumn fieldValueInt];
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_category_last_modified];
            model.addjob_category_last_modified = [curColumn fieldValueString];
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_category_name];
            model.addjob_category_name = [curColumn fieldValueString];
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_category_name_lang];
            model.addjob_category_name_lang = [curColumn fieldValueString];
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_category_name_lang2];
            model.addjob_category_name_lang2 = [curColumn fieldValueString];
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_category_user_id];
            model.addjob_category_user_id = [curColumn fieldValueInt];
            
            [results addObject:model];
        }
    }
    
    return results;
}

-(NSMutableArray*)loadAddJoCategoriesByUserId:(int)userId roomId:(int)roomId roomAssignId:(int)roomAssignId
{
    if(userId <= 0){
        return nil;
    }
    
    if(roomId < 0){
        roomId = 0;
    }
    
    if(roomAssignId < 0){
        roomAssignId = 0;
    }
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"select a.%@, a.%@, a.%@, a.%@, a.%@, a.%@ from %@ a, %@ b where b.%@ = %d and b.%@ = %d and b.%@ = %d and b.%@ = a.%@",
                                  //select
                                  addjob_category_id,
                                  addjob_category_last_modified,
                                  addjob_category_name,
                                  addjob_category_name_lang,
                                  addjob_category_name_lang2,
                                  addjob_category_user_id,
                                  
                                  //From
                                  ADDJOB_CATEGORY, //a
                                  ADDJOB_DETAIL, //b
                                  
                                  //where
                                  addjob_detail_user_id, //b
                                  userId,
                                  
                                  //and
                                  addjob_detail_room_id, //b
                                  roomId,
                                  
                                  //and
                                  addjob_detail_room_assign_id, //b
                                  roomAssignId,
                                  
                                  //and
                                  addjob_detail_category_id, //b
                                  addjob_category_id //a
                                  ];
    
    DataTable *table = [[DataTable alloc] init];
    
    //Row result reference
    DataRow *rowReference = [[DataRow alloc] init];
    [rowReference addColumnFormatInt:@"%@", addjob_category_id];
    [rowReference addColumnFormatString:@"%@",addjob_category_last_modified];
    [rowReference addColumnFormatString:@"%@", addjob_category_name];
    [rowReference addColumnFormatString:@"%@",addjob_category_name_lang];
    [rowReference addColumnFormatString:@"%@", addjob_category_name_lang2];
    [rowReference addColumnFormatInt:@"%@", addjob_category_user_id];
    
    [table loadTableDataWithQuery:sqlString referenceRow:rowReference];
    
    int rowCount = [table countRows];
    NSMutableArray *results = nil;
    if(rowCount > 0)
    {
        for (int i = 0; i < rowCount; i ++) {
            
            AddJobCategoryModelV2 *model = [[AddJobCategoryModelV2 alloc] init];
            DataRow *row = [table rowWithIndex:i];
            DataColumn *curColumn = nil;
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_category_id];
            model.addjob_category_id = [curColumn fieldValueInt];
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_category_last_modified];
            model.addjob_category_last_modified = [curColumn fieldValueString];
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_category_name];
            model.addjob_category_name = [curColumn fieldValueString];
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_category_name_lang];
            model.addjob_category_name_lang = [curColumn fieldValueString];
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_category_name_lang2];
            model.addjob_category_name_lang2 = [curColumn fieldValueString];
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_category_user_id];
            model.addjob_category_user_id = [curColumn fieldValueInt];
            
            [results addObject:model];
        }
    }
    
    return results;
}

-(NSString*)loadAddJobCategoriesStringByUserId:(int)userId roomId:(int)roomId roomAssignId:(int)roomAssignId;
{
    if(userId <= 0){
        return nil;
    }
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"select a.%@, a.%@, a.%@ from %@ a, %@ b where b.%@ = a.%@ and b.%@ <> %d",
                                  //select
                                  addjob_category_name,
                                  addjob_category_name_lang,
                                  addjob_category_name_lang2,
                                  
                                  //From
                                  ADDJOB_CATEGORY, //a
                                  ADDJOB_DETAIL, //b
                                  
                                  //where
//                                  addjob_detail_user_id, //b
//                                  userId,
                                  
                                  //and
                                  addjob_detail_category_id, //b
                                  addjob_category_id, //a
                                  
                                  //and
//                                  addjob_category_user_id,//a
//                                  userId,
                                  
                                  //and not equal additional job complete
                                  addjob_detail_status,//b
                                  AddJobStatus_Complete
                                  ];
    
    if(roomId > 0){
        [sqlString appendFormat:@" and b.%@ = %d",addjob_detail_room_id, roomId];
    } else if(roomAssignId > 0){
        [sqlString appendFormat:@" and b.%@ = %d", addjob_detail_room_assign_id,roomAssignId];
    }
    
    DataTable *table = [[DataTable alloc] init];
    
    //Row result reference
    DataRow *rowReference = [[DataRow alloc] init];
    [rowReference addColumnFormatString:@"%@", addjob_category_name];
    [rowReference addColumnFormatString:@"%@",addjob_category_name_lang];
    [rowReference addColumnFormatString:@"%@", addjob_category_name_lang2];
    
    [table loadTableDataWithQuery:sqlString referenceRow:rowReference];
    
    int rowCount = [table countRows];
    NSMutableString *result = [[NSMutableString alloc] initWithString:@""];
    
    if(rowCount > 0)
    {
        bool isEnglishLang = [[LanguageManagerV2 sharedLanguageManager] currentLanguageIsEqualTo:@"%@", ENGLISH_LANGUAGE];
        for (int i = 0; i < rowCount; i ++) {
            
            DataRow *row = [table rowWithIndex:i];
            DataColumn *curColumn = nil;
            
            if(isEnglishLang){
                curColumn = [row columnWithNameFormat:@"%@", addjob_category_name];
                [result appendFormat:@"%@", [curColumn fieldValueString]];
            } else {
                curColumn = [row columnWithNameFormat:@"%@", addjob_category_name_lang];
                [result appendFormat:@"%@", [curColumn fieldValueString]];
            }
            
            if(i < rowCount - 1){
                [result appendString:@", "];
            }
            
            //Language 2 is not apply
            //curColumn = [row columnWithNameFormat:@"%@", addjob_category_name_lang2];
        }
    }
    
    return result;
}

//Get Categories string with roomId, current user ID and search ID
//This only apply in search additional job room for show categories string in offline mode
//By default we use "-(NSString*)loadStringJobCategoriesByUserId:(int)userId roomId:(int)roomId roomAssignId:(int)roomAssignId"
-(NSString*)loadAddJobCategoriesStringByUserId:(int)userId roomId:(int)roomId searchId:(int)searchId;
{
    if(userId <= 0){
        return nil;
    }
    
    if(roomId < 0){
        roomId = 0;
    }
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"select a.%@, a.%@, a.%@ from %@ a, %@ b where b.%@ = %d and b.%@ = %d and b.%@ = a.%@",
                                  //select
                                  addjob_category_name,
                                  addjob_category_name_lang,
                                  addjob_category_name_lang2,
                                  
                                  //From
                                  ADDJOB_CATEGORY, //a
                                  ADDJOB_SEARCH_ROOM_CATEGORY, //b
                                  
                                  //where
//                                  addjob_searchroomcategory_user_id, //b
//                                  userId,
                                  
                                  //and
                                  addjob_searchroomcategory_room_id, //b
                                  roomId,
                                  
                                  //and
                                  addjob_searchroomcategory_search_id, //b
                                  searchId,
                                  
                                  //and
                                  addjob_searchroomcategory_category_id, //b
                                  addjob_category_id //a
                                  ];
    
    DataTable *table = [[DataTable alloc] init];
    
    //Row result reference
    DataRow *rowReference = [[DataRow alloc] init];
    [rowReference addColumnFormatString:@"%@", addjob_category_name];
    [rowReference addColumnFormatString:@"%@",addjob_category_name_lang];
    [rowReference addColumnFormatString:@"%@", addjob_category_name_lang2];
    
    [table loadTableDataWithQuery:sqlString referenceRow:rowReference];
    
    int rowCount = [table countRows];
    NSMutableString *result = [[NSMutableString alloc] initWithString:@""];
    
    if(rowCount > 0)
    {
        bool isEnglishLang = [[LanguageManagerV2 sharedLanguageManager] currentLanguageIsEqualTo:@"%@", ENGLISH_LANGUAGE];
        for (int i = 0; i < rowCount; i ++) {
            
            DataRow *row = [table rowWithIndex:i];
            DataColumn *curColumn = nil;
            
            if(isEnglishLang){
                curColumn = [row columnWithNameFormat:@"%@", addjob_category_name];
                [result appendFormat:@"%@", [curColumn fieldValueString]];
            } else {
                curColumn = [row columnWithNameFormat:@"%@", addjob_category_name_lang];
                [result appendFormat:@"%@", [curColumn fieldValueString]];
            }
            
            if(i < rowCount - 1){
                [result appendString:@", "];
            }
            
            //Language 2 is not apply
            //curColumn = [row columnWithNameFormat:@"%@", addjob_category_name_lang2];
        }
    }
    
    return result;
}

-(AddJobCategoryModelV2*) loadAddJobCategoryByUserId:(int)userId categoryId:(int)categoryId
{
    if(userId <= 0){
        return nil;
    }
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"select %@, %@, %@, %@, %@, %@ from %@ where %@ = %d",
                                  //select
                                  addjob_category_id,
                                  addjob_category_last_modified,
                                  addjob_category_name,
                                  addjob_category_name_lang,
                                  addjob_category_name_lang2,
                                  addjob_category_user_id,
                                  
                                  //From
                                  ADDJOB_CATEGORY,
                                  
                                  //where
//                                  addjob_category_user_id,
//                                  userId,
                                  //and
                                  addjob_category_id,
                                  categoryId];
    
    DataTable *table = [[DataTable alloc] init];
    
    //Row result reference
    DataRow *rowReference = [[DataRow alloc] init];
    [rowReference addColumnFormatInt:@"%@", addjob_category_id];
    [rowReference addColumnFormatString:@"%@",addjob_category_last_modified];
    [rowReference addColumnFormatString:@"%@", addjob_category_name];
    [rowReference addColumnFormatString:@"%@",addjob_category_name_lang];
    [rowReference addColumnFormatString:@"%@", addjob_category_name_lang2];
    [rowReference addColumnFormatInt:@"%@", addjob_category_user_id];
    
    [table loadTableDataWithQuery:sqlString referenceRow:rowReference];
    
    int rowCount = [table countRows];
    AddJobCategoryModelV2 *model = nil;
    if(rowCount > 0)
    {
            
        model = [[AddJobCategoryModelV2 alloc] init];
        DataRow *row = [table rowWithIndex:0];
        DataColumn *curColumn = nil;
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_category_id];
        model.addjob_category_id = [curColumn fieldValueInt];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_category_last_modified];
        model.addjob_category_last_modified = [curColumn fieldValueString];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_category_name];
        model.addjob_category_name = [curColumn fieldValueString];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_category_name_lang];
        model.addjob_category_name_lang = [curColumn fieldValueString];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_category_name_lang2];
        model.addjob_category_name_lang2 = [curColumn fieldValueString];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_category_user_id];
        model.addjob_category_user_id = [curColumn fieldValueInt];
        
    }
    
    return model;
}

//MARK: Local AddJob Detail
-(int) insertAddJobDetail:(AddJobDetailModelV2*)model
{
    if(model == nil){
        return 0;
    }
    if(model.addjob_detail_start_time.length <= 0){
        model.addjob_detail_start_time = @"";
    }
    if(model.addjob_detail_stop_time.length <= 0){
        model.addjob_detail_stop_time = @"";
    }
    if(model.addjob_detail_remark.length <= 0){
        model.addjob_detail_remark = @"";
    }
    
    DataRow *rowInsert = [[DataRow alloc] init];
    rowInsert.tableName = [NSString stringWithFormat:@"%@", ADDJOB_DETAIL];
    
    [rowInsert addColumnIntValue:(int)model.addjob_detail_category_id formatName:@"%@", addjob_detail_category_id];
    [rowInsert addColumnIntValue:(int)model.addjob_detail_expected_cleaning_time formatName:@"%@", addjob_detail_expected_cleaning_time];
    [rowInsert addColumnStringValue:model.addjob_detail_last_modified formatName:@"%@", addjob_detail_last_modified];
    [rowInsert addColumnIntValue:(int)model.addjob_detail_onday_addjob_id formatName:@"%@", addjob_detail_onday_addjob_id];
    [rowInsert addColumnIntValue:(int)model.addjob_detail_post_status formatName:@"%@", addjob_detail_post_status];
    [rowInsert addColumnIntValue:(int)model.addjob_detail_room_assign_id formatName:@"%@", addjob_detail_room_assign_id];
    [rowInsert addColumnIntValue:(int)model.addjob_detail_room_id formatName:@"%@", addjob_detail_room_id];
    [rowInsert addColumnStringValue:model.addjob_detail_room_number formatName:@"%@", addjob_detail_room_number];
    [rowInsert addColumnStringValue:model.addjob_detail_start_time formatName:@"%@", addjob_detail_start_time];
    [rowInsert addColumnIntValue:(int)model.addjob_detail_status formatName:@"%@", addjob_detail_status];
    [rowInsert addColumnStringValue:model.addjob_detail_stop_time formatName:@"%@", addjob_detail_stop_time];
    [rowInsert addColumnIntValue:(int)model.addjob_detail_user_id formatName:@"%@", addjob_detail_user_id];
    [rowInsert addColumnStringValue:model.addjob_detail_remark formatName:@"%@", addjob_detail_remark];
    
    return [rowInsert insertDBRow];
}

-(int) updateAddJobDetail:(AddJobDetailModelV2*)model
{
    if(model == nil){
        return 0;
    }
    
    if(model.addjob_detail_room_id < 0){
        model.addjob_detail_room_id = 0;
    }
    
    if(model.addjob_detail_room_assign_id < 0){
        model.addjob_detail_room_assign_id = 0;
    }
    
    if(model.addjob_detail_remark.length <= 0){
        model.addjob_detail_remark = @"";
    }
    
    DataRow *rowUpdate = [[DataRow alloc] init];
    rowUpdate.tableName = [NSString stringWithFormat:@"%@", ADDJOB_DETAIL];
    
    DataColumn *columnUpdate1 = [rowUpdate addColumnInt:[NSString stringWithFormat:@"%@",addjob_detail_user_id] value:(int)model.addjob_detail_user_id];
    columnUpdate1.isUpdateCondition = YES;
    DataColumn *columnUpdate2 = [rowUpdate addColumnInt:[NSString stringWithFormat:@"%@",addjob_detail_onday_addjob_id] value:(int)model.addjob_detail_onday_addjob_id];
    columnUpdate2.isUpdateCondition = YES;
    
    [rowUpdate addColumnInt:[NSString stringWithFormat:@"%@",addjob_detail_room_assign_id] value:(int)model.addjob_detail_room_assign_id];
    [rowUpdate addColumnInt:[NSString stringWithFormat:@"%@",addjob_detail_room_id] value:(int)model.addjob_detail_room_id];
    [rowUpdate addColumnIntValue:(int)model.addjob_detail_category_id formatName:@"%@",addjob_detail_category_id];
    [rowUpdate addColumnIntValue:(int)model.addjob_detail_expected_cleaning_time formatName:@"%@",addjob_detail_expected_cleaning_time];
    [rowUpdate addColumnStringValue:model.addjob_detail_last_modified formatName:@"%@",addjob_detail_last_modified];
    [rowUpdate addColumnIntValue:(int)model.addjob_detail_post_status formatName:@"%@",addjob_detail_post_status];
    [rowUpdate addColumnStringValue:model.addjob_detail_room_number formatName:@"%@",addjob_detail_room_number];
    [rowUpdate addColumnStringValue:model.addjob_detail_start_time formatName:@"%@",addjob_detail_start_time];
    [rowUpdate addColumnIntValue:(int)model.addjob_detail_status formatName:@"%@",addjob_detail_status];
    [rowUpdate addColumnStringValue:model.addjob_detail_stop_time formatName:@"%@",addjob_detail_stop_time];
    [rowUpdate addColumnIntValue:(int)model.addjob_detail_total_time formatName:@"%@",addjob_detail_total_time];
    [rowUpdate addColumnStringValue:model.addjob_detail_remark formatName:@"%@",addjob_detail_remark];
    
    return [rowUpdate commitChangeDBRow];
}

-(int) deleteAddJobDetailByUserId:(int)userId
{
    if(userId <= 0){
        return 0;
    }
    
    DataTable *tableDelete = [[DataTable alloc] initWithNameFormat:@"%@",ADDJOB_DETAIL];
    NSString *queryDelete = [NSString stringWithFormat:@"Delete from %@ Where %@ = %d", ADDJOB_DETAIL, addjob_detail_user_id, userId];
    int status = (int)[tableDelete excuteQueryNonSelect:queryDelete];
    
    return status;
}

-(int) deleteAllAddJobDetails
{
    DataTable *tableDelete = [[DataTable alloc] initWithNameFormat:@"%@",ADDJOB_DETAIL];
    return [tableDelete dropDBRows];
}

-(NSMutableArray*)loadAddJobDetailsByUserId:(int)userId roomId:(int)roomId roomAssignId:(int)roomAssignId
{
    if(userId <= 0){
        return nil;
    }
    
    if(roomId < 0){
        roomId = 0;
    }
    
    if(roomAssignId < 0){
        roomAssignId = 0;
    }
    
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"select %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ from %@ where %@ = %d",
                                  //select
                                  addjob_detail_category_id,
                                  addjob_detail_expected_cleaning_time,
                                  addjob_detail_last_modified,
                                  addjob_detail_onday_addjob_id,
                                  addjob_detail_post_status,
                                  addjob_detail_room_assign_id,
                                  addjob_detail_room_id,
                                  addjob_detail_room_number,
                                  addjob_detail_start_time,
                                  addjob_detail_status,
                                  addjob_detail_stop_time,
                                  addjob_detail_user_id,
                                  addjob_detail_total_time,
                                  addjob_detail_remark,
                                  
                                  //From
                                  ADDJOB_DETAIL
                                  
                                  //where
                                  ,addjob_detail_room_assign_id,
                                  roomAssignId
                                  ];
    
//    if(roomId > 0) {
//        [sqlString appendFormat:@" and %@ = %d", addjob_detail_room_id, roomId];
//    } else if(roomAssignId > 0) {
//        [sqlString appendFormat:@" and %@ = %d", addjob_detail_room_assign_id, roomAssignId];
//    }
    
    DataTable *table = [[DataTable alloc] init];
    
    //Row result reference
    DataRow *rowReference = [[DataRow alloc] init];
    [rowReference addColumnFormatInt:@"%@", addjob_detail_category_id];
    [rowReference addColumnFormatInt:@"%@", addjob_detail_expected_cleaning_time];
    [rowReference addColumnFormatString:@"%@", addjob_detail_last_modified];
    [rowReference addColumnFormatInt:@"%@", addjob_detail_onday_addjob_id];
    [rowReference addColumnFormatInt:@"%@", addjob_detail_post_status];
    [rowReference addColumnFormatInt:@"%@", addjob_detail_room_assign_id];
    [rowReference addColumnFormatInt:@"%@", addjob_detail_room_id];
    [rowReference addColumnFormatString:@"%@", addjob_detail_room_number];
    [rowReference addColumnFormatString:@"%@", addjob_detail_start_time];
    [rowReference addColumnFormatInt:@"%@", addjob_detail_status];
    [rowReference addColumnFormatString:@"%@", addjob_detail_stop_time];
    [rowReference addColumnFormatInt:@"%@", addjob_detail_user_id];
    [rowReference addColumnFormatInt:@"%@", addjob_detail_total_time];
    [rowReference addColumnFormatString:@"%@", addjob_detail_remark];
    
    [table loadTableDataWithQuery:sqlString referenceRow:rowReference];
    
    NSMutableArray *results = nil;
    int rowCount = [table countRows];
    if(rowCount > 0)
    {
        
        results = [NSMutableArray array];
        for (int i = 0; i < rowCount; i ++) {
            AddJobDetailModelV2 *model = [[AddJobDetailModelV2 alloc] init];
            DataRow *row = [table rowWithIndex:i];
            DataColumn *curColumn = nil;
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_detail_category_id];
            model.addjob_detail_category_id = [curColumn fieldValueInt];
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_detail_expected_cleaning_time];
            model.addjob_detail_expected_cleaning_time = [curColumn fieldValueInt];
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_detail_last_modified];
            model.addjob_detail_last_modified = [curColumn fieldValueString];
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_detail_onday_addjob_id];
            model.addjob_detail_onday_addjob_id = [curColumn fieldValueInt];
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_detail_post_status];
            model.addjob_detail_post_status = [curColumn fieldValueInt];
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_detail_room_assign_id];
            model.addjob_detail_room_assign_id = [curColumn fieldValueInt];
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_detail_room_id];
            model.addjob_detail_room_id = [curColumn fieldValueInt];
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_detail_room_number];
            model.addjob_detail_room_number = [curColumn fieldValueString];
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_detail_start_time];
            model.addjob_detail_start_time = [curColumn fieldValueString];
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_detail_status];
            model.addjob_detail_status = [curColumn fieldValueInt];
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_detail_stop_time];
            model.addjob_detail_stop_time = [curColumn fieldValueString];
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_detail_user_id];
            model.addjob_detail_user_id = [curColumn fieldValueInt];
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_detail_total_time];
            model.addjob_detail_total_time = [curColumn fieldValueInt];
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_detail_remark];
            model.addjob_detail_remark = [curColumn fieldValueString];
            
            [results addObject:model];
        }
    }
    
    return results;
}

-(NSMutableArray*)loadAddJobDetailsByUserId:(int)userId postStatus:(int)postStatus
{
    if(userId <= 0){
        return nil;
    }
    
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"select %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ from %@ where %@ = %d and %@ = %d",
                                  //select
                                  addjob_detail_category_id,
                                  addjob_detail_expected_cleaning_time,
                                  addjob_detail_last_modified,
                                  addjob_detail_onday_addjob_id,
                                  addjob_detail_post_status,
                                  addjob_detail_room_assign_id,
                                  addjob_detail_room_id,
                                  addjob_detail_room_number,
                                  addjob_detail_start_time,
                                  addjob_detail_status,
                                  addjob_detail_stop_time,
                                  addjob_detail_user_id,
                                  addjob_detail_total_time,
                                  addjob_detail_remark,
                                  
                                  //From
                                  ADDJOB_DETAIL,
                                  
                                  //where
                                  addjob_detail_user_id,
                                  userId,
                                  addjob_detail_post_status,
                                  postStatus];
    
    DataTable *table = [[DataTable alloc] init];
    
    //Row result reference
    DataRow *rowReference = [[DataRow alloc] init];
    [rowReference addColumnFormatInt:@"%@", addjob_detail_category_id];
    [rowReference addColumnFormatInt:@"%@", addjob_detail_expected_cleaning_time];
    [rowReference addColumnFormatString:@"%@", addjob_detail_last_modified];
    [rowReference addColumnFormatInt:@"%@", addjob_detail_onday_addjob_id];
    [rowReference addColumnFormatInt:@"%@", addjob_detail_post_status];
    [rowReference addColumnFormatInt:@"%@", addjob_detail_room_assign_id];
    [rowReference addColumnFormatInt:@"%@", addjob_detail_room_id];
    [rowReference addColumnFormatString:@"%@", addjob_detail_room_number];
    [rowReference addColumnFormatString:@"%@", addjob_detail_start_time];
    [rowReference addColumnFormatInt:@"%@", addjob_detail_status];
    [rowReference addColumnFormatString:@"%@", addjob_detail_stop_time];
    [rowReference addColumnFormatInt:@"%@", addjob_detail_user_id];
    [rowReference addColumnFormatInt:@"%@", addjob_detail_total_time];
    [rowReference addColumnFormatString:@"%@", addjob_detail_remark];
    
    [table loadTableDataWithQuery:sqlString referenceRow:rowReference];
    
    NSMutableArray *results = nil;
    int rowCount = [table countRows];
    if(rowCount > 0)
    {
        
        results = [NSMutableArray array];
        for (int i = 0; i < rowCount; i ++) {
            AddJobDetailModelV2 *model = [[AddJobDetailModelV2 alloc] init];
            DataRow *row = [table rowWithIndex:i];
            DataColumn *curColumn = nil;
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_detail_category_id];
            model.addjob_detail_category_id = [curColumn fieldValueInt];
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_detail_expected_cleaning_time];
            model.addjob_detail_expected_cleaning_time = [curColumn fieldValueInt];
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_detail_last_modified];
            model.addjob_detail_last_modified = [curColumn fieldValueString];
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_detail_onday_addjob_id];
            model.addjob_detail_onday_addjob_id = [curColumn fieldValueInt];
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_detail_post_status];
            model.addjob_detail_post_status = [curColumn fieldValueInt];
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_detail_room_assign_id];
            model.addjob_detail_room_assign_id = [curColumn fieldValueInt];
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_detail_room_id];
            model.addjob_detail_room_id = [curColumn fieldValueInt];
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_detail_room_number];
            model.addjob_detail_room_number = [curColumn fieldValueString];
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_detail_start_time];
            model.addjob_detail_start_time = [curColumn fieldValueString];
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_detail_status];
            model.addjob_detail_status = [curColumn fieldValueInt];
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_detail_stop_time];
            model.addjob_detail_stop_time = [curColumn fieldValueString];
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_detail_user_id];
            model.addjob_detail_user_id = [curColumn fieldValueInt];
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_detail_total_time];
            model.addjob_detail_total_time = [curColumn fieldValueInt];
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_detail_remark];
            model.addjob_detail_remark = [curColumn fieldValueString];
            
            [results addObject:model];
        }
    }
    
    return results;
}

-(int)countAddJobDetailsByUserId:(int)userId postStatus:(int)postStatus
{
    if(userId <= 0){
        return nil;
    }
    
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"select count(1) from %@ where %@ = %d and %@ = %d",
                                  //count
                                  
                                  //From
                                  ADDJOB_DETAIL,
                                  
                                  //where
                                  addjob_detail_user_id,
                                  userId,
                                  addjob_detail_post_status,
                                  postStatus];
    
    DataTable *table = [[DataTable alloc] init];
    
    //Row result reference
    DataRow *rowReference = [[DataRow alloc] init];
    [rowReference addColumnFormatInt:@"CountResult"];
    [table loadTableDataWithQuery:sqlString referenceRow:rowReference];
    
    int rowCount = [table countRows];
    int countResult = 0;
    if(rowCount > 0) {
        DataRow *row = [table rowWithIndex:0];
        countResult = [row intValueWithColumnNameFormat:@"CountResult"];
    }
    
    return countResult;
}

-(AddJobDetailModelV2*) loadAddJobDetailByUserId:(int)userId ondayAddJobId:(int)ondayAddJobId
{
    if(userId <= 0 || ondayAddJobId < 0){
        return nil;
    }
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"select %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ from %@ where %@ = %d",
                                  //select
                                  addjob_detail_category_id,
                                  addjob_detail_expected_cleaning_time,
                                  addjob_detail_last_modified,
                                  addjob_detail_onday_addjob_id,
                                  addjob_detail_post_status,
                                  addjob_detail_room_assign_id,
                                  addjob_detail_room_id,
                                  addjob_detail_room_number,
                                  addjob_detail_start_time,
                                  addjob_detail_status,
                                  addjob_detail_stop_time,
                                  addjob_detail_user_id,
                                  addjob_detail_total_time,
                                  addjob_detail_remark,
                                  
                                  //From
                                  ADDJOB_DETAIL,
                                  
                                  //where
//                                  addjob_detail_user_id,
//                                  userId,
                                  //and
                                  addjob_detail_onday_addjob_id,
                                  ondayAddJobId
                                  ];
    
    DataTable *table = [[DataTable alloc] init];
    
    //Row result reference
    DataRow *rowReference = [[DataRow alloc] init];
    [rowReference addColumnFormatInt:@"%@", addjob_detail_category_id];
    [rowReference addColumnFormatInt:@"%@", addjob_detail_expected_cleaning_time];
    [rowReference addColumnFormatString:@"%@", addjob_detail_last_modified];
    [rowReference addColumnFormatInt:@"%@", addjob_detail_onday_addjob_id];
    [rowReference addColumnFormatInt:@"%@", addjob_detail_post_status];
    [rowReference addColumnFormatInt:@"%@", addjob_detail_room_assign_id];
    [rowReference addColumnFormatInt:@"%@", addjob_detail_room_id];
    [rowReference addColumnFormatString:@"%@", addjob_detail_room_number];
    [rowReference addColumnFormatString:@"%@", addjob_detail_start_time];
    [rowReference addColumnFormatInt:@"%@", addjob_detail_status];
    [rowReference addColumnFormatString:@"%@", addjob_detail_stop_time];
    [rowReference addColumnFormatInt:@"%@", addjob_detail_user_id];
    [rowReference addColumnFormatInt:@"%@", addjob_detail_total_time];
    [rowReference addColumnFormatString:@"%@", addjob_detail_remark];
    
    [table loadTableDataWithQuery:sqlString referenceRow:rowReference];
    
    AddJobDetailModelV2 *model = nil;
    int rowCount = [table countRows];
    if(rowCount > 0)
    {
        model = [[AddJobDetailModelV2 alloc] init];
        DataRow *row = [table rowWithIndex:0];
        DataColumn *curColumn = nil;
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_detail_category_id];
        model.addjob_detail_category_id = [curColumn fieldValueInt];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_detail_expected_cleaning_time];
        model.addjob_detail_expected_cleaning_time = [curColumn fieldValueInt];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_detail_last_modified];
        model.addjob_detail_last_modified = [curColumn fieldValueString];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_detail_onday_addjob_id];
        model.addjob_detail_onday_addjob_id = [curColumn fieldValueInt];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_detail_post_status];
        model.addjob_detail_post_status = [curColumn fieldValueInt];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_detail_room_assign_id];
        model.addjob_detail_room_assign_id = [curColumn fieldValueInt];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_detail_room_id];
        model.addjob_detail_room_id = [curColumn fieldValueInt];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_detail_room_number];
        model.addjob_detail_room_number = [curColumn fieldValueString];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_detail_start_time];
        model.addjob_detail_start_time = [curColumn fieldValueString];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_detail_status];
        model.addjob_detail_status = [curColumn fieldValueInt];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_detail_stop_time];
        model.addjob_detail_stop_time = [curColumn fieldValueString];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_detail_user_id];
        model.addjob_detail_user_id = [curColumn fieldValueInt];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_detail_total_time];
        model.addjob_detail_total_time = [curColumn fieldValueInt];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_detail_remark];
        model.addjob_detail_remark = [curColumn fieldValueString];
        
    }
    
    return model;
}

//MARK: Local AddJob Detail Record
-(int) insertAddJobDetailRecord:(AddJobDetailRecordModelV2*)model
{
    if(model == nil){
        return 0;
    }
    
    DataRow *rowInsert = [[DataRow alloc] init];
    rowInsert.tableName = [NSString stringWithFormat:@"%@", ADDJOB_DETAIL_RECORD];
    
    [rowInsert addColumnStringValue:model.addjob_detail_record_last_modified formatName:@"%@", addjob_detail_record_last_modified];
    [rowInsert addColumnIntValue:(int)model.addjob_detail_record_onday_addjob_id formatName:@"%@", addjob_detail_record_onday_addjob_id];
    [rowInsert addColumnIntValue:(int)model.addjob_detail_record_operation formatName:@"%@", addjob_detail_record_operation];
    [rowInsert addColumnIntValue:(int)model.addjob_detail_record_post_status formatName:@"%@", addjob_detail_record_post_status];
    [rowInsert addColumnStringValue:model.addjob_detail_record_time formatName:@"%@", addjob_detail_record_time];
    [rowInsert addColumnIntValue:(int)model.addjob_detail_record_user_id formatName:@"%@", addjob_detail_record_user_id];
    
    return [rowInsert insertDBRow];
}

-(int) updateAddJobDetailRecord:(AddJobDetailRecordModelV2*)model
{
    if(model == nil){
        return 0;
    }
    
    DataRow *rowUpdate = [[DataRow alloc] init];
    rowUpdate.tableName = [NSString stringWithFormat:@"%@", ADDJOB_DETAIL_RECORD];
    
    DataColumn *columnUpdate1 = [rowUpdate addColumnIntValue:(int)model.addjob_detail_record_user_id formatName:@"%@",addjob_detail_record_user_id];
    columnUpdate1.isUpdateCondition = YES;
    
    DataColumn *columnUpdate2 = [rowUpdate addColumnIntValue:(int)model.addjob_detail_record_onday_addjob_id formatName:@"%@",addjob_detail_record_onday_addjob_id];
    columnUpdate2.isUpdateCondition = YES;
    
    [rowUpdate addColumnStringValue:model.addjob_detail_record_last_modified formatName:@"%@",addjob_detail_record_last_modified];
    [rowUpdate addColumnIntValue:(int)model.addjob_detail_record_operation formatName:@"%@",addjob_detail_record_operation];
    [rowUpdate addColumnIntValue:(int)model.addjob_detail_record_post_status formatName:@"%@",addjob_detail_record_post_status];
    [rowUpdate addColumnStringValue:model.addjob_detail_record_time formatName:@"%@",addjob_detail_record_time];
    
    return [rowUpdate commitChangeDBRow];
}

-(int) deleteAddJobDetailRecordByUserId:(int)userId
{
    if(userId <= 0){
        return 0;
    }
    
    DataTable *tableDelete = [[DataTable alloc] initWithNameFormat:@"%@",ADDJOB_DETAIL_RECORD];
    NSString *queryDelete = [NSString stringWithFormat:@"Delete from %@ Where %@ = %d", ADDJOB_DETAIL_RECORD, addjob_detail_record_user_id, userId];
    int status = (int)[tableDelete excuteQueryNonSelect:queryDelete];
    
    return status;
}

-(int) deleteAllAddJobDetailRecords
{
    DataTable *tableDelete = [[DataTable alloc] initWithNameFormat:@"%@", ADDJOB_DETAIL_RECORD];
    return [tableDelete dropDBRows];
    
}

-(NSMutableArray*) loadAddJobDetailRecordsByUserId:(int)userId postStatus:(int)postStatus
{
    if(userId <= 0){
        return nil;
    }
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"select %@, %@, %@, %@, %@, %@ from %@ where %@ = %d and %@ = %d",
                                  //Select
                                  addjob_detail_record_last_modified,
                                  addjob_detail_record_onday_addjob_id,
                                  addjob_detail_record_operation,
                                  addjob_detail_record_post_status,
                                  addjob_detail_record_time,
                                  addjob_detail_record_user_id,
                                  
                                  //From
                                  ADDJOB_DETAIL_RECORD,
                                  
                                  //where
                                  addjob_detail_record_user_id,
                                  userId,
                                  
                                  //and
                                  addjob_detail_record_post_status,
                                  postStatus
                                  ];
    
    DataTable *table = [[DataTable alloc] init];
    
    //Row result reference
    DataRow *rowReference = [[DataRow alloc] init];
    [rowReference addColumnFormatString:@"%@", addjob_detail_record_last_modified];
    [rowReference addColumnFormatInt:@"%@",addjob_detail_record_onday_addjob_id];
    [rowReference addColumnFormatInt:@"%@", addjob_detail_record_operation];
    [rowReference addColumnFormatInt:@"%@",addjob_detail_record_post_status];
    [rowReference addColumnFormatString:@"%@", addjob_detail_record_time];
    [rowReference addColumnFormatInt:@"%@", addjob_detail_record_user_id];
    
    [table loadTableDataWithQuery:sqlString referenceRow:rowReference];
    
    int rowCount = [table countRows];
    NSMutableArray *results = nil;
    if(rowCount > 0)
    {
        for (int i = 0; i < rowCount; i ++) {
            
            AddJobDetailRecordModelV2 *model = [[AddJobDetailRecordModelV2 alloc] init];
            DataRow *row = [table rowWithIndex:i];
            DataColumn *curColumn = nil;
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_detail_record_last_modified];
            model.addjob_detail_record_last_modified = [curColumn fieldValueString];
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_detail_record_onday_addjob_id];
            model.addjob_detail_record_onday_addjob_id = [curColumn fieldValueInt];
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_detail_record_operation];
            model.addjob_detail_record_operation = [curColumn fieldValueInt];
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_detail_record_post_status];
            model.addjob_detail_record_post_status = [curColumn fieldValueInt];
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_detail_record_time];
            model.addjob_detail_record_time = [curColumn fieldValueString];
            
            curColumn = [row columnWithNameFormat:@"%@", addjob_detail_record_user_id];
            model.addjob_detail_record_user_id = [curColumn fieldValueInt];
            
            [results addObject:model];
        }
    }
    
    return results;
}

//Count AddJobDetailRecord with status
-(int) countAddJobDetailRecordsByUserId:(int)userId postStatus:(int)postStatus
{
    if(userId <= 0){
        return nil;
    }
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"count(1) from %@ where %@ = %d and %@ = %d",
                                  //Count
                                  
                                  //From
                                  ADDJOB_DETAIL_RECORD,
                                  
                                  //where
                                  addjob_detail_record_user_id,
                                  userId,
                                  
                                  //and
                                  addjob_detail_record_post_status,
                                  postStatus
                                  ];
    
    DataTable *table = [[DataTable alloc] init];
    
    //Row result reference
    DataRow *rowReference = [[DataRow alloc] init];
    [rowReference addColumnFormatString:@"CountResult"];
    
    [table loadTableDataWithQuery:sqlString referenceRow:rowReference];
    
    int rowCount = [table countRows];
    int countResult = 0;
    if(rowCount > 0) {
        countResult = [[[table rowWithIndex:0] columnWithName:@"CountResult"] fieldValueInt];
    }
    
    return countResult;
}

//MARK: Local AddJob RoomItem
-(int) insertAddJobRoomItem:(AddJobRoomItemModelV2*)model
{
    if(model == nil){
        return 0;
    }
    
    DataRow *rowInsert = [[DataRow alloc] init];
    rowInsert.tableName = [NSString stringWithFormat:@"%@", ADDJOB_ROOM_ITEM];
    
    [rowInsert addColumnIntValue:(int)model.addjob_roomitem_item_id formatName:@"%@", addjob_roomitem_item_id];
    [rowInsert addColumnStringValue:model.addjob_roomitem_last_modified formatName:@"%@", addjob_roomitem_last_modified];
    [rowInsert addColumnIntValue:(int)model.addjob_roomitem_onday_addjob_id formatName:@"%@", addjob_roomitem_onday_addjob_id];
    [rowInsert addColumnIntValue:(int)model.addjob_roomitem_user_id formatName:@"%@", addjob_roomitem_user_id];
    
    return [rowInsert insertDBRow];
}

-(AddJobRoomItemModelV2*) loadAddJobRoomItemByUserId:(int)userId itemId:(int)itemId{
    if(userId <= 0){
        return 0;
    }
    if(itemId <= 0){
        return 0;
    }
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"select distinct a.%@, a.%@, a.%@ from %@ a join %@ b on a.%@ = b.%@ and a.%@ = b.%@ where a.%@ = %d and a.%@ = %d order by a.%@",
                                  
                                  //Select
                                  addjob_roomitem_onday_addjob_id,
                                  addjob_roomitem_user_id,
                                  addjob_roomitem_item_id,
                                  
                                  //from
                                  ADDJOB_ROOM_ITEM,
                                  //join
                                  ADDJOB_DETAIL,
                                  
                                  addjob_roomitem_onday_addjob_id,
                                  addjob_detail_onday_addjob_id,
                                  //and
                                  addjob_roomitem_user_id,
                                  addjob_detail_user_id,
                                  
                                  //where
                                  addjob_roomitem_item_id,
                                  itemId,
                                  addjob_roomitem_user_id,
                                  userId,
                                  
                                  addjob_roomitem_onday_addjob_id];
    
    DataTable *table = [[DataTable alloc] init];
    
    //Row result reference
    DataRow *rowReference = [[DataRow alloc] init];
    [rowReference addColumnFormatInt:@"%@", addjob_roomitem_onday_addjob_id];
    [rowReference addColumnFormatInt:@"%@", addjob_roomitem_user_id];
    [rowReference addColumnFormatInt:@"%@", addjob_roomitem_item_id];
    
    [table loadTableDataWithQuery:sqlString referenceRow:rowReference];
    
    AddJobRoomItemModelV2 *model = nil;
    
    int rowCount = [table countRows];
    if(rowCount > 0)
    {
        model = [[AddJobRoomItemModelV2 alloc] init];
        DataRow *row = [table rowWithIndex:0];
        DataColumn *curColumn = nil;
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_roomitem_onday_addjob_id];
        model.addjob_roomitem_onday_addjob_id = [curColumn fieldValueInt];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_roomitem_user_id];
        model.addjob_roomitem_user_id = [curColumn fieldValueInt];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_roomitem_item_id];
        model.addjob_roomitem_item_id = [curColumn fieldValueInt];
    }
    return model;
}

-(NSMutableArray*) loadListAddJobRoomItemByUserId:(int)userId itemId:(int)itemId{
    NSMutableArray *array = [NSMutableArray array];
    if(userId <= 0){
        return 0;
    }
    if(itemId <= 0){
        return 0;
    }
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"select distinct a.%@, a.%@, a.%@ from %@ a join %@ b on a.%@ = b.%@ and a.%@ = b.%@ where a.%@ = %d order by a.%@",
                                  
                                  //Select
                                  addjob_roomitem_onday_addjob_id,
                                  addjob_roomitem_user_id,
                                  addjob_roomitem_item_id,
                                  
                                  //from
                                  ADDJOB_ROOM_ITEM,
                                  //join
                                  ADDJOB_DETAIL,
                                  
                                  addjob_roomitem_onday_addjob_id,
                                  addjob_detail_onday_addjob_id,
                                  //and
                                  addjob_roomitem_user_id,
                                  addjob_detail_user_id,
                                  
                                  //where
                                  addjob_roomitem_item_id,
                                  itemId,
//                                  addjob_roomitem_user_id,
//                                  userId,
                                  
                                  addjob_roomitem_onday_addjob_id];
    
    DataTable *table = [[DataTable alloc] init];
    
    //Row result reference
    DataRow *rowReference = [[DataRow alloc] init];
    [rowReference addColumnFormatInt:@"%@", addjob_roomitem_onday_addjob_id];
    [rowReference addColumnFormatInt:@"%@", addjob_roomitem_user_id];
    [rowReference addColumnFormatInt:@"%@", addjob_roomitem_item_id];
    
    [table loadTableDataWithQuery:sqlString referenceRow:rowReference];
    
    //AddJobRoomItemModelV2 *model = nil;
    
    int rowCount = [table countRows];
    
    for(int i = 0; i< rowCount; i++)
    {
        AddJobRoomItemModelV2 *model = [[AddJobRoomItemModelV2 alloc] init];
        DataRow *row = [table rowWithIndex:i];
        DataColumn *curColumn = nil;
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_roomitem_onday_addjob_id];
        model.addjob_roomitem_onday_addjob_id = [curColumn fieldValueInt];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_roomitem_user_id];
        model.addjob_roomitem_user_id = [curColumn fieldValueInt];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_roomitem_item_id];
        model.addjob_roomitem_item_id = [curColumn fieldValueInt];
        [array addObject:model];
    }
    return array;
}

-(int) deleteAddJobRoomItemByUserId:(int)userId
{
    if(userId <= 0){
        return 0;
    }
    
    DataTable *tableDelete = [[DataTable alloc] initWithNameFormat:@"%@",ADDJOB_ROOM_ITEM];
    NSString *queryDelete = [NSString stringWithFormat:@"Delete from %@ Where %@ = %d", ADDJOB_ROOM_ITEM, addjob_roomitem_user_id, userId];
    int status = (int)[tableDelete excuteQueryNonSelect:queryDelete];
    
    return status;
}

-(int) deleteAddJobRoomItemByUserId:(int)userId ondayAddJobId:(int)ondayAddJobId
{
    if(userId <= 0){
        return 0;
    }
    
    DataTable *tableDelete = [[DataTable alloc] initWithNameFormat:@"%@",ADDJOB_ROOM_ITEM];
    NSString *queryDelete = [NSString stringWithFormat:@"Delete from %@ Where %@ = %d and %@ = %d", ADDJOB_ROOM_ITEM, addjob_roomitem_user_id, userId, addjob_roomitem_onday_addjob_id, ondayAddJobId];
    int status = (int)[tableDelete excuteQueryNonSelect:queryDelete];
    
    return status;
}

-(int) deleteAllAddJobRoomItems
{
    DataTable *tableDelete = [[DataTable alloc] initWithNameFormat:@"%@",ADDJOB_ROOM_ITEM];
    return [tableDelete dropDBRows];
}

//MARK: Local AddJob Search Room Category
-(int) insertAddJobSearchRoomCategory:(AddJobSearchRoomCategoryModelV2*)model
{
    if(model == nil){
        return 0;
    }
    
    DataRow *rowInsert = [[DataRow alloc] init];
    rowInsert.tableName = [NSString stringWithFormat:@"%@", ADDJOB_SEARCH_ROOM_CATEGORY];
    
    [rowInsert addColumnIntValue:(int)model.addjob_searchroomcategory_category_id formatName:@"%@", addjob_searchroomcategory_category_id];
    [rowInsert addColumnStringValue:model.addjob_searchroomcategory_last_modified formatName:@"%@", addjob_searchroomcategory_last_modified];
    [rowInsert addColumnIntValue:(int)model.addjob_searchroomcategory_room_id formatName:@"%@", addjob_searchroomcategory_room_id];
    [rowInsert addColumnIntValue:(int)model.addjob_searchroomcategory_search_id formatName:@"%@", addjob_searchroomcategory_search_id];
    [rowInsert addColumnIntValue:(int)model.addjob_searchroomcategory_user_id formatName:@"%@", addjob_searchroomcategory_user_id];
    
    return [rowInsert insertDBRow];
}

-(int) deleteAddJobSearchRoomCategoryByUserId:(int)userId roomId:(int)roomId
{
    if(userId <= 0){
        return 0;
    }
    
    DataTable *tableDelete = [[DataTable alloc] initWithNameFormat:@"%@",ADDJOB_SEARCH_ROOM_CATEGORY];
    NSString *queryDelete = [NSString stringWithFormat:@"Delete from %@ Where %@ = %d and %@ = %d", ADDJOB_SEARCH_ROOM_CATEGORY, addjob_searchroomcategory_user_id, userId, addjob_searchroomcategory_room_id, roomId];
    int status = (int)[tableDelete excuteQueryNonSelect:queryDelete];
    
    return status;
}

-(int) deleteAddJobSearchRoomCategoryByUserId:(int)userId roomId:(int)roomId categoryId:(int)categoryId
{
    if(userId <= 0){
        return 0;
    }
    
    DataTable *tableDelete = [[DataTable alloc] initWithNameFormat:@"%@",ADDJOB_SEARCH_ROOM_CATEGORY];
    NSString *queryDelete = [NSString stringWithFormat:@"Delete from %@ Where %@ = %d and %@ = %d and %@ = %d", ADDJOB_SEARCH_ROOM_CATEGORY, addjob_searchroomcategory_user_id, userId, addjob_searchroomcategory_room_id, roomId, addjob_searchroomcategory_category_id, categoryId];
    int status = (int)[tableDelete excuteQueryNonSelect:queryDelete];
    
    return status;
}

//MARK: Local AddJob Guest Info
-(int) insertAddJobGuestInfo:(AddJobGuestInfoModelV2*)model
{
    if(model == nil){
        return 0;
    }
    
    if(model.addjob_guestinfo_check_in_date.length <= 0){
        model.addjob_guestinfo_check_in_date = @"";
    }
    
    if(model.addjob_guestinfo_check_out_date.length <= 0){
       model.addjob_guestinfo_check_out_date = @"";
    }
    
    if(model.addjob_guestinfo_pref_desc.length <= 0){
        model.addjob_guestinfo_pref_desc = @"";
    }
    
    if(model.addjob_guestinfo_guest_name.length <= 0){
       model.addjob_guestinfo_guest_name = @"";
    }
    
    if(model.addjob_guestinfo_housekeeping_name.length <= 0){
        model.addjob_guestinfo_housekeeping_name = @"";
    }
    
    if(model.addjob_guestinfo_vip.length <= 0){
        model.addjob_guestinfo_vip = @"";
    }
    
    if(model.addjob_guestinfo_lang_pref.length <= 0){
        model.addjob_guestinfo_lang_pref = @"";
    }
    
    DataRow *rowInsert = [[DataRow alloc] init];
    rowInsert.tableName = [NSString stringWithFormat:@"%@", ADDJOB_GUEST_INFO];
    
    [rowInsert addColumnString:[NSString stringWithFormat:@"%@",addjob_guestinfo_check_in_date] value:model.addjob_guestinfo_check_in_date];
    [rowInsert addColumnString:[NSString stringWithFormat:@"%@",addjob_guestinfo_check_out_date] value:model.addjob_guestinfo_check_out_date];
    [rowInsert addColumnString:[NSString stringWithFormat:@"%@",addjob_guestinfo_guest_name] value:model.addjob_guestinfo_guest_name];
    [rowInsert addColumnString:[NSString stringWithFormat:@"%@",addjob_guestinfo_guest_number] value:model.addjob_guestinfo_guest_number];
    [rowInsert addColumnString:[NSString stringWithFormat:@"%@",addjob_guestinfo_guest_title] value:model.addjob_guestinfo_guest_title];
    [rowInsert addColumnString:[NSString stringWithFormat:@"%@",addjob_guestinfo_housekeeping_name] value:model.addjob_guestinfo_housekeeping_name];
    [rowInsert addColumnString:[NSString stringWithFormat:@"%@",addjob_guestinfo_lang_pref] value:model.addjob_guestinfo_lang_pref];
    [rowInsert addColumnString:[NSString stringWithFormat:@"%@",addjob_guestinfo_last_modified] value:model.addjob_guestinfo_last_modified];
    [rowInsert addColumnData:[NSString stringWithFormat:@"%@",addjob_guestinfo_photo] value:model.addjob_guestinfo_photo];
    [rowInsert addColumnString:[NSString stringWithFormat:@"%@",addjob_guestinfo_pref_desc] value:model.addjob_guestinfo_pref_desc];
    [rowInsert addColumnInt:[NSString stringWithFormat:@"%@",addjob_guestinfo_room_id] value:(int)model.addjob_guestinfo_room_id];
    [rowInsert addColumnString:[NSString stringWithFormat:@"%@",addjob_guestinfo_special_service] value:model.addjob_guestinfo_special_service];
    [rowInsert addColumnInt:[NSString stringWithFormat:@"%@",addjob_guestinfo_user_id] value:(int)model.addjob_guestinfo_user_id];
    [rowInsert addColumnString:[NSString stringWithFormat:@"%@",addjob_guestinfo_vip] value:model.addjob_guestinfo_vip];
    [rowInsert addColumnInt:[NSString stringWithFormat:@"%@",addjob_guestinfo_post_status] value:(int)model.addjob_guestinfo_post_status];
    
    return [rowInsert insertDBRow]; //return id inserted
}

-(int) updateAddJobGuestInfo:(AddJobGuestInfoModelV2*)model
{
    if(model == nil){
        return 0;
    }
    
    if(model.addjob_guestinfo_check_in_date.length <= 0){
        model.addjob_guestinfo_check_in_date = @"";
    }
    
    if(model.addjob_guestinfo_check_out_date.length <= 0){
        model.addjob_guestinfo_check_out_date = @"";
    }
    
    if(model.addjob_guestinfo_pref_desc.length <= 0){
        model.addjob_guestinfo_pref_desc = @"";
    }
    
    if(model.addjob_guestinfo_guest_name.length <= 0){
        model.addjob_guestinfo_guest_name = @"";
    }
    
    if(model.addjob_guestinfo_housekeeping_name.length <= 0){
        model.addjob_guestinfo_housekeeping_name = @"";
    }
    
    if(model.addjob_guestinfo_vip.length <= 0){
        model.addjob_guestinfo_vip = @"";
    }
    
    if(model.addjob_guestinfo_lang_pref.length <= 0){
        model.addjob_guestinfo_lang_pref = @"";
    }
    
    DataRow *rowUpdate = [[DataRow alloc] init];
    rowUpdate.tableName = [NSString stringWithFormat:@"%@", ADDJOB_GUEST_INFO];
    
    DataColumn *columnUpdate1 = [rowUpdate addColumnInt:[NSString stringWithFormat:@"%@",addjob_guestinfo_room_id] value:(int)model.addjob_guestinfo_room_id];
    columnUpdate1.isUpdateCondition = YES;
    DataColumn *columnUpdate2 = [rowUpdate addColumnInt:[NSString stringWithFormat:@"%@",addjob_guestinfo_user_id] value:(int)model.addjob_guestinfo_user_id];
    columnUpdate2.isUpdateCondition = YES;
    
    [rowUpdate addColumnStringValue:model.addjob_guestinfo_check_in_date formatName:@"%@",addjob_guestinfo_check_in_date];
    [rowUpdate addColumnStringValue:model.addjob_guestinfo_check_out_date formatName:@"%@",addjob_guestinfo_check_out_date];
    [rowUpdate addColumnStringValue:model.addjob_guestinfo_guest_name formatName:@"%@",addjob_guestinfo_guest_name];
    [rowUpdate addColumnStringValue:model.addjob_guestinfo_guest_number formatName:@"%@",addjob_guestinfo_guest_number];
    [rowUpdate addColumnStringValue:model.addjob_guestinfo_guest_title formatName:@"%@",addjob_guestinfo_guest_title];
    [rowUpdate addColumnStringValue:model.addjob_guestinfo_housekeeping_name formatName:@"%@",addjob_guestinfo_housekeeping_name];
    [rowUpdate addColumnStringValue:model.addjob_guestinfo_lang_pref formatName:@"%@",addjob_guestinfo_lang_pref];
    [rowUpdate addColumnStringValue:model.addjob_guestinfo_last_modified formatName:@"%@",addjob_guestinfo_last_modified];
    [rowUpdate addColumnDataValue:model.addjob_guestinfo_photo formatName:@"%@",addjob_guestinfo_photo];
    [rowUpdate addColumnStringValue:model.addjob_guestinfo_pref_desc formatName:@"%@",addjob_guestinfo_pref_desc];
    [rowUpdate addColumnStringValue:model.addjob_guestinfo_special_service formatName:@"%@",addjob_guestinfo_special_service];
    [rowUpdate addColumnStringValue:model.addjob_guestinfo_vip formatName:@"%@",addjob_guestinfo_vip];
    [rowUpdate addColumnIntValue:(int)model.addjob_guestinfo_post_status formatName:@"%@",addjob_guestinfo_post_status];
    
    return [rowUpdate commitChangeDBRow];
}

-(int) deleteAddJobGuestInfoByUserId:(int)userId
{
    if(userId <= 0){
        return 0;
    }
    
    DataTable *tableDelete = [[DataTable alloc] initWithNameFormat:@"%@", ADDJOB_GUEST_INFO];
    NSString *queryDelete = [NSString stringWithFormat:@"Delete from %@ Where %@ = %d", ADDJOB_GUEST_INFO, addjob_guestinfo_user_id, userId];
    int status = (int)[tableDelete excuteQueryNonSelect:queryDelete];
    
    return status;
}

-(int) deleteAllAddJobGuestInfo
{
    DataTable *tableDelete = [[DataTable alloc] initWithNameFormat:@"%@", ADDJOB_GUEST_INFO];
    return [tableDelete dropDBRows];
}

-(AddJobGuestInfoModelV2*) loadAddJobGuestInfoByUserId:(int)userId roomId:(int)roomId
{
    if(userId <= 0){
        return nil;
    }
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"select %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ from %@ where %@ = %d and %@ = %d",
                                  //select
                                  addjob_guestinfo_check_in_date,
                                  addjob_guestinfo_check_out_date,
                                  addjob_guestinfo_guest_name,
                                  addjob_guestinfo_guest_number,
                                  addjob_guestinfo_guest_title,
                                  addjob_guestinfo_housekeeping_name,
                                  addjob_guestinfo_lang_pref,
                                  addjob_guestinfo_last_modified,
                                  addjob_guestinfo_photo,
                                  addjob_guestinfo_pref_desc,
                                  addjob_guestinfo_room_id,
                                  addjob_guestinfo_special_service,
                                  addjob_guestinfo_user_id,
                                  addjob_guestinfo_vip,
                                  addjob_guestinfo_post_status,
                                  
                                  //From
                                  ADDJOB_GUEST_INFO,
                                  
                                  //where
                                  addjob_guestinfo_user_id,
                                  userId,
                                  //and
                                  addjob_guestinfo_room_id,
                                  roomId
                                  ];
    
    DataTable *table = [[DataTable alloc] init];
    
    //Row result reference
    DataRow *rowReference = [[DataRow alloc] init];
    [rowReference addColumnFormatString:@"%@", addjob_guestinfo_check_in_date];
    [rowReference addColumnFormatString:@"%@", addjob_guestinfo_check_out_date];
    [rowReference addColumnFormatString:@"%@", addjob_guestinfo_guest_name];
    [rowReference addColumnFormatString:@"%@", addjob_guestinfo_guest_number];
    [rowReference addColumnFormatString:@"%@", addjob_guestinfo_guest_title];
    [rowReference addColumnFormatString:@"%@", addjob_guestinfo_housekeeping_name];
    [rowReference addColumnFormatString:@"%@", addjob_guestinfo_lang_pref];
    [rowReference addColumnFormatString:@"%@", addjob_guestinfo_last_modified];
    [rowReference addColumnFormatData:@"%@", addjob_guestinfo_photo];
    [rowReference addColumnFormatString:@"%@", addjob_guestinfo_pref_desc];
    [rowReference addColumnFormatInt:@"%@", addjob_guestinfo_room_id];
    [rowReference addColumnFormatString:@"%@", addjob_guestinfo_special_service];
    [rowReference addColumnFormatInt:@"%@", addjob_guestinfo_user_id];
    [rowReference addColumnFormatString:@"%@", addjob_guestinfo_vip];
    [rowReference addColumnFormatInt:@"%@", addjob_guestinfo_post_status];
    
    [table loadTableDataWithQuery:sqlString referenceRow:rowReference];
    
    AddJobGuestInfoModelV2 *model = nil;
    int rowCount = [table countRows];
    if(rowCount > 0)
    {
        model = [[AddJobGuestInfoModelV2 alloc] init];
        DataRow *row = [table rowWithIndex:0];
        DataColumn *curColumn = nil;
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_guestinfo_check_in_date];
        model.addjob_guestinfo_check_in_date = [curColumn fieldValueString];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_guestinfo_check_out_date];
        model.addjob_guestinfo_check_out_date = [curColumn fieldValueString];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_guestinfo_guest_name];
        model.addjob_guestinfo_guest_name = [curColumn fieldValueString];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_guestinfo_guest_number];
        model.addjob_guestinfo_guest_number = [curColumn fieldValueString];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_guestinfo_guest_title];
        model.addjob_guestinfo_guest_title = [curColumn fieldValueString];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_guestinfo_housekeeping_name];
        model.addjob_guestinfo_housekeeping_name = [curColumn fieldValueString];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_guestinfo_lang_pref];
        model.addjob_guestinfo_lang_pref = [curColumn fieldValueString];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_guestinfo_last_modified];
        model.addjob_guestinfo_last_modified = [curColumn fieldValueString];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_guestinfo_photo];
        model.addjob_guestinfo_photo = [curColumn fieldValueData];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_guestinfo_pref_desc];
        model.addjob_guestinfo_pref_desc = [curColumn fieldValueString];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_guestinfo_room_id];
        model.addjob_guestinfo_room_id = [curColumn fieldValueInt];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_guestinfo_special_service];
        model.addjob_guestinfo_special_service = [curColumn fieldValueString];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_guestinfo_user_id];
        model.addjob_guestinfo_user_id = [curColumn fieldValueInt];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_guestinfo_vip];
        model.addjob_guestinfo_vip = [curColumn fieldValueString];
        
        curColumn = [row columnWithNameFormat:@"%@", addjob_guestinfo_post_status];
        model.addjob_guestinfo_post_status = [curColumn fieldValueInt];
    }
    
    return model;
}

#pragma mark - PRIVATE FUNCTIONS
-(NSMutableArray*) sortListNumbers:(NSMutableArray*) listNumber ascending:(BOOL) isAscending
{
    NSSortDescriptor *highestToLowest = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending: isAscending];
    [listNumber sortUsingDescriptors:[NSArray arrayWithObject:highestToLowest]];
    return listNumber;
}

-(NSString*) convertListTasksIdToString:(NSMutableArray*)listTasksId
{
    listTasksId = [self sortListNumbers:listTasksId ascending:YES];
    NSMutableString *tasksIdString = [[NSMutableString alloc] init];
    for (int  i = 0; i < [listTasksId count]; i ++) {
        NSNumber *taskIdNumber = [listTasksId objectAtIndex:i];
        [tasksIdString appendFormat:@"%d",[taskIdNumber intValue]];
        if(i < [listTasksId count] - 1){
            [tasksIdString appendString:@","];
        }
    }
    return tasksIdString;
}

-(NSMutableArray*) convertTaskStringToNumber:(NSString*)tasksValue
{
    NSMutableArray *arrayNumbers = [[tasksValue componentsSeparatedByString:@","] mutableCopy];
    NSMutableArray *results = [NSMutableArray array];
    for (NSString *curNumber in arrayNumbers) {
        [results addObject:[NSNumber numberWithInt:[curNumber intValue]]];
    }
    
    return results;
}


@end
