//
//  FindManager.m
//  mHouseKeeping
//
//  Created by Nghia Truong on 4/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FindManagerV2Demo.h"
#import "NetworkCheck.h"
#import "RoomManagerV2.h"
#import "RoomRecordModelV2.h"
#import "RoomServiceLaterModelV2.h"
#import "LogFileManager.h"
#import "LogObject.h"
#import "eHousekeepingService.h"
#import "UserManagerV2Demo.h"

@implementation FindManagerV2Demo

static FindManagerV2 *sharedFindManagerInstance = nil;


#pragma mark - === Find ===
#pragma mark 
-(NSMutableArray *) findDataAttendantWithUserID:(NSNumber *) userID AndHotelID:(NSNumber *) hotelID AndAttendantName:(NSString *) attendantName {
    NSMutableArray *array = [NSMutableArray array];
    NSMutableArray *listLocalUsersDataBase = [[UserManagerV2 sharedUserManager] loadAttendanceUsersBySupervisorId:[userID intValue]];
    
    for(UserModelV2 *attendant in listLocalUsersDataBase) {
        NSString *currentStatusText = nil;
        NSString *currentStatus = @"3";
        NSString *roomAssignID = @"0";
        NSString *currentStatusLang = nil;
        NSString *attendantId = nil;
        NSString *atdName = nil;
        
        NSString *roomNo = @"";
        
        currentStatusText = @"Moving";
        
        currentStatusLang = @"Moving";
        
        attendantId = [NSString stringWithFormat:@"%d",attendant.userId];
        
        atdName = attendant.userName;
        
        NSString *scheduleTime = @"+00:00";
        
        NSDictionary *item = [NSDictionary dictionaryWithObjectsAndKeys:
                              attendantId == nil ? @"" : attendantId, kAttendantID,
                              atdName == nil ? @"" : atdName, kAtdName,
                              attendant.userFullName == nil ? @"" : attendant.userFullName, kAttendantName,
                              currentStatus == nil ? @"" : currentStatus, kCurrentStatus,
                              roomNo == nil ? @"" : roomNo, kRoomNumber,
                              roomAssignID == nil ? @"" : roomAssignID, kRoomAssignmentID,
                              scheduleTime == nil ? @"" : scheduleTime, kTime,
                              currentStatusLang == nil ? @"" : currentStatusLang, kAttendantStatusLang,
                              currentStatusText == nil ? @"" : currentStatusText, kAttendantStatusName, nil];
        
        [array addObject:item];
    }
    
    
    return array;
}

////Hao Tran [20130708]- Remove for find all room assignment and replace with new function below
//-(NSMutableArray *)findRoomAssignmentWithUserID:(NSInteger)userId AndLastModified:(NSString *)lastModified {
//    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
//    
//    //Allow write log or not
//    if([LogFileManager isLogConsole])
//    {
//        [binding setLogXMLInOut:YES];
//    }
//    NSMutableArray *array = [NSMutableArray array];
//    
//    RoomAssignmentModelV2 *roomAssignmentModel = nil;
//    RoomModelV2 *roomModel = nil;
//    RoomRecordModelV2 *roomRecordModel = nil;
//    
//    eHousekeepingService_GetRoomAssignmentList *request = [[eHousekeepingService_GetRoomAssignmentList alloc] init];
//    
//    [request setIntUsrID:[NSNumber numberWithInteger:userId]];
//    
//    if (lastModified != nil) {
//        [request setStrLastModified:lastModified];
//    }
//    
//    LogObject *logObj = [[LogObject alloc] init];
//    logObj.dateTime = [NSDate date];
//    logObj.log = [NSString stringWithFormat:@"<%i><findRoomAssignmentWithUserID><intUsrID:%i lastModified:%@>",userId,userId,lastModified];
//    LogFileManager *logManager = [[LogFileManager alloc] init];
//    [logManager save:logObj];
//    
//    eHousekeepingServiceSoap12BindingResponse *response = [binding GetRoomAssignmentListUsingParameters:request];
//    
//    NSArray *responseBodyParts = response.bodyParts;
//    
//    for (id bodyPart in responseBodyParts) {
//        if ([bodyPart isKindOfClass:[eHousekeepingService_GetRoomAssignmentListResponse 
//                                     class]]) {
//            eHousekeepingService_GetRoomAssignmentListResponse *dataBody = (eHousekeepingService_GetRoomAssignmentListResponse *)bodyPart; 
//            if (dataBody) {
//                if ([dataBody.GetRoomAssignmentListResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]]) {
//                    
//                    NSMutableArray *roomAssignment = dataBody.GetRoomAssignmentListResult.RoomAssignmentList.RoomAssign;
//                    eHousekeepingService_RoomAssign *roomDetail = nil;
//                    
//                    for (eHousekeepingService_RoomAssign *roomAssign in roomAssignment) {
//                        //get room detail
//                        roomDetail = (eHousekeepingService_RoomAssign *)[self findRoomDetailWithUserID:userId andRoomAssignmentID:[roomAssign.raID integerValue]  andLastModified:nil];
//                        
//                        roomAssignmentModel = [[RoomAssignmentModelV2 alloc] init];
//                        roomModel = [[RoomModelV2 alloc] init];
//                        roomRecordModel = [[RoomRecordModelV2 alloc] init];
//                        
//                        roomAssignmentModel.roomAssignment_Id = [roomAssign.raID integerValue];
//                        roomAssignmentModel.roomAssignment_RoomId = [roomAssign.raRoomNo integerValue];
//                        roomAssignmentModel.roomAssignment_UserId = userId;
//                        roomAssignmentModel.roomAssignment_AssignedDate = roomAssign.raAssignedTime;	
//                        roomAssignmentModel.roomAssignment_Priority = [roomDetail.raPriority integerValue];
//                        roomAssignmentModel.roomAssignment_PostStatus = POST_STATUS_UN_CHANGED;		
//                        roomAssignmentModel.roomAssignment_LastModified = roomAssign.raLastModified;
//                        roomAssignmentModel.roomAssignment_Priority_Sort_Order = [roomDetail.raPrioritySortOrder integerValue];
//                        roomAssignmentModel.roomAssignmentHousekeeperId = [roomAssign.raHousekeeperID integerValue];
//                        roomAssignmentModel.roomAssignmentRoomStatusId = [roomAssign.raRoomStatusID integerValue];
//                        roomAssignmentModel.roomAssignmentRoomCleaningStatusId = [roomAssign.raCleaningStatusID integerValue];
//                        roomAssignmentModel.roomAssignmentRoomInspectionStatusId = [roomAssign.raInspectedStatus integerValue];
//                        roomAssignmentModel.roomAssignmentRoomExpectedCleaningTime = [roomAssign.raExpectedCleaningTime integerValue];
//                        roomAssignmentModel.roomAssignmentRoomExpectedInspectionTime = [roomAssign.raExpectedInspectTime integerValue];
//                        roomAssignmentModel.roomAssignmentGuestProfileId = [roomAssign.raGuestProfileID integerValue];
//                        
//                        NSArray *arrayName = [roomAssign.raFullName componentsSeparatedByString:@" "];
//                        
//                        if (arrayName.count > 2) {
//                            roomAssignmentModel.roomAssignmentFirstName = [NSString stringWithFormat:@"%@ %@", [arrayName objectAtIndex:0], [arrayName objectAtIndex:arrayName.count - 1]];
//                        } else {
//                            roomAssignmentModel.roomAssignmentFirstName = roomAssign.raFullName;
//                        }
//
//                        roomAssignmentModel.roomAssignmentGuestFirstName = roomAssign.raGuestFirstName;
//                        roomAssignmentModel.roomAssignmentGuestLastName = roomAssign.raGuestLastName;
//       
//                        //set mockup room local
//                        NSInteger isMockupRoomValue = [roomAssign.raIsMockRoom integerValue];
//                        if(isMockupRoomValue == ENUM_TASK_TYPE_NORMAL)
//                        {
//                            roomAssignmentModel.raIsMockRoom = 0;
//                        }
//                        else if(isMockupRoomValue == ENUM_TASK_TYPE_REASSIGN)
//                        {
//                            roomAssignmentModel.raIsMockRoom = 0;
////                            roomAssignmentModel.raIsReassignedRoom = 1;
//                        }
//                        else if(isMockupRoomValue >= ENUM_TASK_TYPE_MOCKUP)
//                        {
//                            roomAssignmentModel.raIsMockRoom = 1;
//                        }
//                            
//                        
//                        roomAssignmentModel.raKindOfRoom = [roomAssign.raKindOfRoom integerValue];
//                        roomAssignmentModel.raGuestDepartureTime = roomAssign.raGuestDepartureTime;
//                        roomAssignmentModel.raGuestArrivalTime = roomAssign.raGuestArrivalTime;
////                        roomAssignmentModel.raIsReassignedRoom = (roomAssignmentModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_PENDING_SECOND_TIME)?1:0;
//                        
//                        //Quang edit
//                        if([roomAssign.raCleaningStatusID integerValue] == ENUM_CLEANING_STATUS_PENDING_FIRST_TIME && isMockupRoomValue == ENUM_TASK_TYPE_REASSIGN)
//                            roomAssignmentModel.raIsReassignedRoom = 1;
//                        else
//                            roomAssignmentModel.raIsReassignedRoom = 0;
//                        
//                        roomModel.room_Id = [roomAssign.raID integerValue];
//                        roomModel.room_HotelId = [roomDetail.raHotelID integerValue];
//                        roomModel.room_TypeId = [roomDetail.raRoomTypeID integerValue];
//                        roomModel.room_Lang = nil;
//                        roomModel.room_Building_Name = roomAssign.raBuildingName;
//                        roomModel.room_building_namelang = roomAssign.rabuildingLang;
//                        roomModel.room_Guest_Name = roomAssign.raGuestName;
//                        roomModel.room_VIPStatusId = [roomAssign.raVIP integerValue];
//                        roomModel.room_GuestReference = roomAssign.raGuestPreference;
//                        roomModel.room_AdditionalJob = roomDetail.raAdditionalJob;
//                        roomModel.room_PostStatus = POST_STATUS_UN_CHANGED;
//                        roomModel.room_LastModified = roomDetail.raLastModified;
//                        roomModel.room_is_re_cleaning = NO;
//                        roomModel.room_expected_status_id = 0;
//                        roomModel.room_Building_Id = [roomAssign.raBuildingID integerValue];
//                        roomModel.room_zone_id = 0;
//                        roomModel.room_floor_id = [roomAssign.raFloorID integerValue];
//                        
//                        roomRecordModel.rrec_User_Id = userId;
//                        roomRecordModel.rrec_Cleaning_Date = roomAssign.raCleanEndTm;
//                        roomRecordModel.rrec_PostStatus = POST_STATUS_UN_CHANGED;
//                        roomRecordModel.rrec_Cleaning_Start_Time = roomAssign.raCleanEndTm;
//                        roomRecordModel.rrec_Cleaning_End_Time = roomAssign.raCleanStartTm;
//                        roomRecordModel.rrec_Inspection_Start_Time = roomAssign.raInspectStartTm;
//                        roomRecordModel.rrec_Inspection_End_Time = roomAssign.raInspectEndTm;
////                        roomRecordModel.rrec_Cleaning_Total_Pause_Time;
////                        roomRecordModel.rrec_Inspected_Total_Pause_Time;
//                        roomRecordModel.rrec_room_assignment_id = [roomAssign.raID integerValue];
//                        roomRecordModel.rrec_Remark = roomDetail.raRemark;
//                        
//                        NSDateFormatter *dateTimeFormat = [[NSDateFormatter alloc] init];
//                        [dateTimeFormat setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
//                        [dateTimeFormat setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
//                        NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
//                        [dateTimeFormat setLocale:usLocale];
//                        
//                        NSCalendar *calender = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
//                        NSDateComponents *components = nil;
//                        
//                        if (roomAssign.raCleanStartTm != nil && roomAssign.raCleanEndTm != nil) {
//                            NSDate *cleaningStartDate = [dateTimeFormat dateFromString:roomAssign.raCleanStartTm];
//                            NSDate *cleaningEndDate = [dateTimeFormat dateFromString:roomAssign.raCleanEndTm];
//                            components = [calender components:NSHourCalendarUnit|NSMinuteCalendarUnit|NSSecondCalendarUnit fromDate:cleaningStartDate toDate:cleaningEndDate options:0];
//                            roomRecordModel.rrec_Cleaning_Duration = [NSString stringWithFormat:@"%l",components.hour*3600 + components.minute*60 + components.second];
//                        }
//                        
//                        if (roomAssign.raInspectStartTm != nil && roomAssign.raInspectEndTm != nil) {
//                            NSDate *inspectStartDate = [dateTimeFormat dateFromString:roomAssign.raInspectStartTm];
//                            NSDate *inspectEndDate = [dateTimeFormat dateFromString:roomAssign.raInspectEndTm];
//                            components = [calender components:NSHourCalendarUnit|NSMinuteCalendarUnit|NSSecondCalendarUnit fromDate:inspectStartDate toDate:inspectEndDate options:0];
//                            roomRecordModel.rrec_Inspection_Duration = [NSString stringWithFormat:@"%l",components.hour*3600 + components.minute*60 + components.second];
//                        }
//                        
//                        roomAssign.raInspectStartTm = roomDetail.raInspectStartTm;
//                        roomAssign.raInspectEndTm = roomDetail.raInspectEndTm;
//                        
//                        //convert room assignment data to dictionary
//                        NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
//                        
//                        if (roomAssign.raID != nil) {
//                            [dictionary setObject:roomAssign.raID forKey:kraID];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraID];
//                        }
//                        
//                        if (roomAssign.raHousekeeperID != nil) {
//                            [dictionary setObject:roomAssign.raHousekeeperID forKey:kraHousekeeperID];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraHousekeeperID];
//                        }
//                        
//                        if (roomAssign.raRoomNo != nil) {
//                            [dictionary setObject:roomAssign.raRoomNo forKey:kraRoomNo];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraRoomNo];
//                        }
//                        
//                        if (roomAssign.raRoomStatusID != nil) {
//                            [dictionary setObject:roomAssign.raRoomStatusID forKey:kraRoomStatusID];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraRoomStatusID];
//                        }
//                        
//                        if (roomAssign.raRoomTypeID != nil) {
//                            [dictionary setObject:roomAssign.raRoomTypeID forKey:kraRoomTypeID];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraRoomTypeID];
//                        }
//                        
//                        if (roomAssign.raCleaningStatusID != nil) {
//                            [dictionary setObject:roomAssign.raCleaningStatusID forKey:kraCleaningStatusID];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraCleaningStatusID];
//                        }
//                        
//                        if (roomAssign.raFloorID != nil) {
//                            [dictionary setObject:roomAssign.raFloorID forKey:kraFloorID];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraFloorID];
//                        }
//                        
//                        if (roomAssign.raBuildingID != nil) {
//                            [dictionary setObject:[roomAssign.raBuildingID stringValue] forKey:kraBuildingID];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraBuildingID];
//                        }
//                        
//                        if (roomAssign.raBuildingName != nil) {
//                            [dictionary setObject:roomAssign.raBuildingName forKey:kraBuildingName];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraBuildingName];
//                        }
//                        
//                        if (roomAssign.rabuildingLang != nil) {
//                            [dictionary setObject:roomAssign.rabuildingLang forKey:krabuildingLang];
//                        } else {
//                            [dictionary setObject:@"" forKey:krabuildingLang];
//                        }
//                        
//                        if (roomAssign.raPriority != nil) {
//                            [dictionary setObject:roomAssign.raPriority forKey:kraPriority];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraPriority];
//                        }
//                        
//                        if (roomAssign.raPrioritySortOrder != nil) {
//                            [dictionary setObject:[roomAssign.raPrioritySortOrder stringValue] forKey:kraPrioritySortOrder];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraPrioritySortOrder];
//                        }
//                        
//                        if (roomAssign.raExpectedCleaningTime != nil) {
//                            [dictionary setObject:roomAssign.raExpectedCleaningTime forKey:kraExpectedCleaningTime];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraExpectedCleaningTime];
//                        }
//                        
//                        if (roomAssign.raInspectedStatus != nil) {
//                            [dictionary setObject:[roomAssign.raInspectedStatus stringValue] forKey:kraInspectedStatus];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraInspectedStatus];
//                        }
//                        
//                        if (roomAssign.raLastModified != nil) {
//                            [dictionary setObject:roomAssign.raLastModified forKey:kraLastModified];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraLastModified];
//                        }
//                        
//                        if (roomAssign.raCleanStartTm != nil) {
//                            [dictionary setObject:roomAssign.raCleanStartTm forKey:kraCleanStartTm];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraCleanStartTm];
//                        }
//                        
//                        if (roomAssign.raCleanEndTm != nil) {
//                            [dictionary setObject:roomAssign.raCleanEndTm forKey:kraCleanEndTm];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraCleanEndTm];
//                        }
//                        
//                        if (roomAssign.raExpectedInspectTime != nil) {
//                            [dictionary setObject:[roomAssign.raExpectedInspectTime stringValue] forKey:kraExpectedInspectTime];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraExpectedInspectTime];
//                        }
//                        
//                        if (roomAssign.raAssignedTime != nil) {
//                            [dictionary setObject:roomAssign.raAssignedTime forKey:kraAssignedTime];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraAssignedTime];
//                        }
//                        
//                        if (roomAssign.raHotelID != nil) {
//                            [dictionary setObject:roomAssign.raHotelID forKey:kraHotelID];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraHotelID];
//                        }
//                        
//                        if (roomAssign.raInspectStartTm != nil) {
//                            [dictionary setObject:roomAssign.raInspectStartTm forKey:kraInspectStartTm];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraInspectStartTm];
//                        }
//                        
//                        if (roomAssign.raInspectEndTm != nil) {
//                            [dictionary setObject:roomAssign.raInspectEndTm forKey:kraInspectEndTm];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraInspectEndTm];
//                        }
//                        
//                        if (roomAssign.raAdditionalJob != nil) {
//                            [dictionary setObject:roomAssign.raAdditionalJob forKey:kraAdditionalJob];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraAdditionalJob];
//                        }
//                        
//                        if (roomAssign.raRemark != nil) {
//                            [dictionary setObject:roomAssign.raRemark forKey:kraRemark];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraRemark];
//                        }
//                        
//                        if (roomAssign.raVIP != nil) {
//                            [dictionary setObject:roomAssign.raVIP forKey:kraVIP];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraVIP];
//                        }
//                        
//                        if (roomAssign.raGuestFirstName != nil) {
//                            [dictionary setObject:roomAssign.raGuestFirstName forKey:kraGuestFirstName];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraGuestFirstName];
//                        }
//                        
//                        if (roomAssign.raGuestLastName != nil) {
//                            [dictionary setObject:roomAssign.raGuestLastName forKey:kraGuestLastName];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraGuestLastName];
//                        }
//                        
//                        if (roomAssign.raGuestPreference != nil) {
//                            [dictionary setObject:roomAssign.raGuestPreference forKey:kraGuestPreference];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraGuestPreference];
//                        }
//                        
//                        if (roomAssign.raTotalCleaningTime != nil) {
//                            [dictionary setObject:[roomAssign.raTotalCleaningTime stringValue] forKey:kraTotalCleaningTime];
//                        } else {
//                            [dictionary setObject:@"0" forKey:kraTotalCleaningTime];
//                        }
//                        
//                        if (roomAssign.raToTalInspectionTime != nil) {
//                            [dictionary setObject:[roomAssign.raToTalInspectionTime stringValue] forKey:kraTotalInspectedTime];
//                        } else {
//                            [dictionary setObject:@"0" forKey:kraTotalInspectedTime];
//                        }
//                        
//                        if (roomAssign.raGuestArrivalTime != nil) {
//                            [dictionary setObject:roomAssign.raGuestArrivalTime forKey:kraGuestArrivalTime];
//                        }
//                        else {
//                            [dictionary setObject:@"" forKey:kraGuestArrivalTime];
//                        }
//                        
//                        if (roomAssign.raGuestDepartureTime != nil) {
//                            [dictionary setObject:roomAssign.raGuestDepartureTime forKey:kraGuestDepartureTime];
//                        }
//                        else {
//                            [dictionary setObject:@"" forKey:kraGuestDepartureTime];
//                        }
//                        
//                        if (roomAssign.raKindOfRoom != nil) {
//                            [dictionary setObject:[roomAssign.raKindOfRoom stringValue] forKey:kraKindOfRoom];
//                        }
//                        else {
//                            [dictionary setObject:@"0" forKey:kraKindOfRoom];
//                        }
//                        
//                        if (roomAssign.raIsMockRoom != nil) {
//                            [dictionary setObject:[roomAssign.raIsMockRoom stringValue] forKey:kraIsMockRoom];
//                        }
//                        else {
//                            [dictionary setObject:@"0" forKey:kraIsMockRoom];
//                        }
//                        
//                        if (roomAssign.raFullName != nil) {
//                            [dictionary setObject:roomAssign.raFullName forKey:kraRAFullName];
//                        }
//                        
//                        RoomStatusModelV2 *roomStatusModel = [[RoomStatusModelV2 alloc] init];
//                        roomStatusModel.rstat_Id = roomAssignmentModel.roomAssignmentRoomStatusId;
//                        [[RoomManagerV2 sharedRoomManager] loadRoomStatusData:roomStatusModel];
//                        
//                        CleaningStatusModelV2 *cleaningStatusModel = [[CleaningStatusModelV2 alloc] init];
//                        cleaningStatusModel.cstat_Id = roomAssignmentModel.roomAssignmentRoomCleaningStatusId;
//                        [[RoomManagerV2  sharedRoomManager] loadCleaningStatus:cleaningStatusModel];
//                        
//                        NSMutableDictionary *roomAssignmentData = [[NSMutableDictionary alloc] init];
//                        [roomAssignmentData setValue:[NSString stringWithFormat:@"%i",roomAssignmentModel.roomAssignment_UserId] forKey:kRaUserId];
//                        [roomAssignmentData setValue:[NSString stringWithFormat:@"%i",roomAssignmentModel.roomAssignment_RoomId] forKey:kRoomNo];
//                        [roomAssignmentData setValue:[NSString stringWithFormat:@"%i",roomAssignmentModel.roomAssignmentRoomStatusId] forKey:kRoomStatusId];
//                        [roomAssignmentData setValue:[NSString stringWithFormat:@"%i",roomAssignmentModel.roomAssignmentRoomInspectionStatusId] forKey:kRoomInspectedStatusId];
//                        [roomAssignmentData setValue:roomStatusModel.rstat_Code forKey:kRMStatus];
//                        [roomAssignmentData setValue:[NSString stringWithFormat:@"%i",roomAssignmentModel.roomAssignmentRoomCleaningStatusId] forKey:kCleaningStatusID];
//                        [roomAssignmentData setValue:[NSString stringWithFormat:@"%@ %@",roomAssign.raGuestFirstName == nil?@"":roomAssign.raGuestFirstName,roomAssign.raGuestLastName == nil?@"":roomAssign.raGuestLastName] forKey:kGuestName];
//                        [roomAssignmentData setValue:[NSString stringWithFormat:@"%i",roomAssignmentModel.roomAssignment_Id] forKey:kRoomAssignmentID];
//                        [roomAssignmentData setValue:roomAssign.raPrioritySortOrder forKey:kRaPrioritySortOrder];
//                        [roomAssignmentData setValue:roomAssignmentModel.roomAssignmentFirstName forKey:kRAFirstName];
//                        [roomAssignmentData setValue:roomAssignmentModel.roomAssignmentLastName forKey:kRALastName];
//                        [roomAssignmentData setValue:cleaningStatusModel.cstat_image forKey:kCleaningStatus];
//                        [roomAssignmentData setValue:roomAssign.raGuestFirstName forKey:kGuestFirstName];
//                        [roomAssignmentData setValue:roomAssign.raGuestLastName forKey:kGuestLastName];
//                        [roomAssignmentData setValue:roomAssign.raGuestArrivalTime forKey:kRaGuestArrivalTime];
//                        [roomAssignmentData setValue:roomAssign.raGuestDepartureTime forKey:kRaGuestDepartureTime];
//                        [roomAssignmentData setValue:[NSString stringWithFormat:@"%i",roomAssignmentModel.raKindOfRoom] forKey:kRaKindOfRoom];
//                        [roomAssignmentData setValue:[NSString stringWithFormat:@"%i",roomAssignmentModel.raIsMockRoom] forKey:kRaIsMockRoom];
//                        [roomAssignmentData setValue:roomStatusModel.rstat_Image forKey:kRoomStatusImage];
//                        [roomAssignmentData setValue:roomAssignmentModel.roomAssignment_AssignedDate forKey:kRoomAssignmentTime];
//                        [roomAssignmentData setValue:[NSString stringWithFormat:@"%i",roomAssignmentModel.raIsReassignedRoom] forKey:kRaIsReassignedRoom];
//                        [roomAssignmentData setValue:roomAssign.raVIP forKey:kVIP];
//                        
//                        NSMutableDictionary * data = [[NSMutableDictionary alloc] init];
//                        [data setObject:roomAssignmentData forKey:keyRoomData];
//                        [data setObject:roomAssignmentModel forKey:keyRoomAssignmentModel];
//                        [data setObject:roomModel forKey:keyRoomModel];
//                        [data setObject:roomRecordModel forKey:keyRoomRecordModel];
//                        
//                        [array addObject:data];
//                    }                    
//                }                
//            }
//        }
//    }
//    
//    eHousekeepingService_GetPrevRoomAssignmentList *requestPrev = [[eHousekeepingService_GetPrevRoomAssignmentList alloc] init];
//    
//    //set supervisor id
//    [requestPrev setIntUsrID:[NSNumber numberWithInteger:[[[UserManagerV2 sharedUserManager] currentUser] userId]]];
//    
//    eHousekeepingServiceSoap12BindingResponse *responsePrev = [binding GetPrevRoomAssignmentListUsingParameters:requestPrev];
//    
//    NSArray *responsePrevBodyParts = responsePrev.bodyParts;
//    
//    for (id bodyPart in responsePrevBodyParts) {
//        if ([bodyPart isKindOfClass:[eHousekeepingService_GetPrevRoomAssignmentListResponse 
//                                     class]]) {
//            eHousekeepingService_GetPrevRoomAssignmentListResponse *dataBody = (eHousekeepingService_GetPrevRoomAssignmentListResponse *)bodyPart; 
//            
//            if (dataBody) {
//                if ([dataBody.GetPrevRoomAssignmentListResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]]) {
//                    
//                    NSMutableArray *roomAssignment = dataBody.GetPrevRoomAssignmentListResult.RoomAssignmentList.RoomAssign;
//                    
//                    eHousekeepingService_RoomAssign *roomDetail = nil;
//                    
//                    for (eHousekeepingService_RoomAssign *roomAssign in roomAssignment) {
//
//                        //Hao Tran remove - for get all rooms
//                        /*
//                        if ([roomAssign.raHousekeeperID integerValue] != userId) {
//                            continue;
//                        }*/
//                        
//                        //get room detail
//                        roomDetail = (eHousekeepingService_RoomAssign *)[self findRoomDetailWithUserID:userId andRoomAssignmentID:[roomAssign.raID integerValue]  andLastModified:nil];
//                        
//                        //set missing data to room assign
//                        //                        roomAssign.raCleanStopTime = roomDetail.raCleanStopTime;
//                        //                        roomAssign.raInspectStopTime = roomDetail.raInspectStopTime;
//                        roomAssign.raInspectStartTm = roomDetail.raInspectStartTm;
//                        roomAssign.raInspectEndTm = roomDetail.raInspectEndTm;
//                        
//                        //convert room assignment data to dictionary
//                        NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
//                        
//                        if (roomAssign.raID != nil) {
//                            [dictionary setObject:roomAssign.raID forKey:kraID];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraID];
//                        }
//                        
//                        if (roomAssign.raHousekeeperID != nil) {
//                            [dictionary setObject:roomAssign.raHousekeeperID forKey:kraHousekeeperID];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraHousekeeperID];
//                        }
//                        
//                        if (roomAssign.raRoomNo != nil) {
//                            [dictionary setObject:roomAssign.raRoomNo forKey:kraRoomNo];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraRoomNo];
//                        }
//                        
//                        if (roomAssign.raRoomStatusID != nil) {
//                            [dictionary setObject:roomAssign.raRoomStatusID forKey:kraRoomStatusID];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraRoomStatusID];
//                        }
//                        
//                        if (roomAssign.raRoomTypeID != nil) {
//                            [dictionary setObject:roomAssign.raRoomTypeID forKey:kraRoomTypeID];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraRoomTypeID];
//                        }
//                        
//                        if (roomAssign.raCleaningStatusID != nil) {
//                            [dictionary setObject:roomAssign.raCleaningStatusID forKey:kraCleaningStatusID];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraCleaningStatusID];
//                        }
//                        
//                        if (roomAssign.raFloorID != nil) {
//                            [dictionary setObject:roomAssign.raFloorID forKey:kraFloorID];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraFloorID];
//                        }
//                        
//                        if (roomAssign.raBuildingID != nil) {
//                            [dictionary setObject:[roomAssign.raBuildingID stringValue] forKey:kraBuildingID];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraBuildingID];
//                        }
//                        
//                        if (roomAssign.raBuildingName != nil) {
//                            [dictionary setObject:roomAssign.raBuildingName forKey:kraBuildingName];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraBuildingName];
//                        }
//                        
//                        if (roomAssign.rabuildingLang != nil) {
//                            [dictionary setObject:roomAssign.rabuildingLang forKey:krabuildingLang];
//                        } else {
//                            [dictionary setObject:@"" forKey:krabuildingLang];
//                        }
//                        
//                        if (roomAssign.raPriority != nil) {
//                            [dictionary setObject:roomAssign.raPriority forKey:kraPriority];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraPriority];
//                        }
//                        
//                        if (roomAssign.raPrioritySortOrder != nil) {
//                            [dictionary setObject:[roomAssign.raPrioritySortOrder stringValue] forKey:kraPrioritySortOrder];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraPrioritySortOrder];
//                        }
//                        
//                        if (roomAssign.raExpectedCleaningTime != nil) {
//                            [dictionary setObject:roomAssign.raExpectedCleaningTime forKey:kraExpectedCleaningTime];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraExpectedCleaningTime];
//                        }
//                        
//                        if (roomAssign.raInspectedStatus != nil) {
//                            [dictionary setObject:[roomAssign.raInspectedStatus stringValue] forKey:kraInspectedStatus];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraInspectedStatus];
//                        }
//                        
//                        if (roomAssign.raLastModified != nil) {
//                            [dictionary setObject:roomAssign.raLastModified forKey:kraLastModified];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraLastModified];
//                        }
//                        
//                        if (roomAssign.raCleanStartTm != nil) {
//                            [dictionary setObject:roomAssign.raCleanStartTm forKey:kraCleanStartTm];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraCleanStartTm];
//                        }
//                        
//                        if (roomAssign.raCleanEndTm != nil) {
//                            [dictionary setObject:roomAssign.raCleanEndTm forKey:kraCleanEndTm];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraCleanEndTm];
//                        }
//                        
//                        if (roomAssign.raExpectedInspectTime != nil) {
//                            [dictionary setObject:[roomAssign.raExpectedInspectTime stringValue] forKey:kraExpectedInspectTime];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraExpectedInspectTime];
//                        }
//                        
//                        if (roomAssign.raAssignedTime != nil) {
//                            [dictionary setObject:roomAssign.raAssignedTime forKey:kraAssignedTime];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraAssignedTime];
//                        }
//                        
//                        if (roomAssign.raHotelID != nil) {
//                            [dictionary setObject:roomAssign.raHotelID forKey:kraHotelID];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraHotelID];
//                        }
//                        
//                        //                        if (roomAssign.raCleanStopTime != nil) {
//                        //                            [dictionary setObject:[roomAssign.raCleanStopTime stringValue] forKey:kraCleanPauseTime];
//                        //                        } else {
//                        //                            [dictionary setObject:@"" forKey:kraCleanPauseTime];
//                        //                        }
//                        
//                        //                        if (roomAssign.raInspectStopTime != nil) {
//                        //                            [dictionary setObject:[roomAssign.raInspectStopTime stringValue] forKey:kraInspectPauseTime];
//                        //                        } else {
//                        //                            [dictionary setObject:@"" forKey:kraInspectPauseTime];
//                        //                        }
//                        
//                        if (roomAssign.raInspectStartTm != nil) {
//                            [dictionary setObject:roomAssign.raInspectStartTm forKey:kraInspectStartTm];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraInspectStartTm];
//                        }
//                        
//                        if (roomAssign.raInspectEndTm != nil) {
//                            [dictionary setObject:roomAssign.raInspectEndTm forKey:kraInspectEndTm];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraInspectEndTm];
//                        }
//                        
//                        if (roomAssign.raAdditionalJob != nil) {
//                            [dictionary setObject:roomAssign.raAdditionalJob forKey:kraAdditionalJob];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraAdditionalJob];
//                        }
//                        
//                        if (roomAssign.raRemark != nil) {
//                            [dictionary setObject:roomAssign.raRemark forKey:kraRemark];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraRemark];
//                        }
//                        
//                        if (roomAssign.raVIP != nil) {
//                            [dictionary setObject:roomAssign.raVIP forKey:kraVIP];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraVIP];
//                        }
//                        
//                        if (roomAssign.raGuestFirstName != nil) {
//                            [dictionary setObject:roomAssign.raGuestFirstName forKey:kraGuestFirstName];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraGuestFirstName];
//                        }
//                        
//                        if (roomAssign.raGuestLastName != nil) {
//                            [dictionary setObject:roomAssign.raGuestLastName forKey:kraGuestLastName];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraGuestLastName];
//                        }
//                        
//                        if (roomAssign.raGuestPreference != nil) {
//                            [dictionary setObject:roomAssign.raGuestPreference forKey:kraGuestPreference];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraGuestPreference];
//                        }
//                        
//                        if (roomAssign.raTotalCleaningTime != nil) {
//                            [dictionary setObject:[roomAssign.raTotalCleaningTime stringValue] forKey:kraTotalCleaningTime];
//                        } else {
//                            [dictionary setObject:@"0" forKey:kraTotalCleaningTime];
//                        }
//                        
//                        if (roomAssign.raToTalInspectionTime != nil) {
//                            [dictionary setObject:[roomAssign.raToTalInspectionTime stringValue] forKey:kraTotalInspectedTime];
//                        } else {
//                            [dictionary setObject:@"0" forKey:kraTotalInspectedTime];
//                        }                        
//                        
//                        [array addObject:dictionary];
//                    }                    
//                }
//            }
//        }
//    }
//    
//    return array;
//}
//Hao Tran [20130708] - Remove End

//Hao Tran [20130708] - Replace with this one
-(NSMutableArray *)findRoomAssignmentWithUserID:(NSInteger)userId AndSupervisorId:(NSInteger)supervisorId AndLastModified:(NSString *)lastModified {
    NSMutableArray *array = [NSMutableArray array];
    
    RoomAssignmentModelV2 *roomAssignmentModel = nil;
    RoomModelV2 *roomModel = nil;
    RoomRecordModelV2 *roomRecordModel = nil;
    
    //NSString *todayDate = [ehkConvert getTodayDateString];
    NSMutableArray *roomAssignments = [[RoomManagerV2 sharedRoomManager] loadRoomAssignmentsBySuperVisorId:[UserManagerV2 sharedUserManager].currentUser.userId houseKeeperId:userId];
    
    for (RoomAssignmentModelV2 *roomAssign in roomAssignments) {
        
        //        if ([[UserManagerV2 sharedUserManager].currentUserAccessRight isFindRARoomsToday]) {
        //            //Compare time today
        //            BOOL isToday = [self isToday:roomAssign.raAssignedTime];
        //            if(!isToday){
        //                continue;
        //            }
        //        }
        
        //get room detail
        //roomDetail = (eHousekeepingService_RoomAssign *)[self findRoomDetailWithUserID:userId andRoomAssignmentID:[roomAssign.raID integerValue]  andLastModified:nil];
        
        roomAssignmentModel = [[RoomAssignmentModelV2 alloc] init];
        
        roomModel = [[RoomModelV2 alloc] init];
        roomModel.room_Id = roomAssign.roomAssignment_RoomId;
        [[RoomManagerV2 sharedRoomManager] loadRoomModel:roomModel];
        
        RoomRecordModelV2 *roomRecord = [[RoomRecordModelV2 alloc] init];
        roomRecord.rrec_room_assignment_id = roomRecordModel.rrec_room_assignment_id;
        roomRecord.rrec_User_Id = [[[UserManagerV2 sharedUserManager] currentUser] userId];
        roomRecordModel = [[RoomManagerV2 sharedRoomManager] loadRoomRecordData:roomRecordModel];
        if(roomRecordModel == nil) {
            roomRecordModel = [[RoomRecordModelV2 alloc] init];
            roomRecordModel.rrec_Total_Cleaning_Time = 0;
            roomRecordModel.rrec_Total_Inspected_Time = 0;
            roomRecordModel.rrec_Last_Cleaning_Duration = @"0";
            roomRecordModel.rrec_Last_Inspected_Duration = @"0";
        }
        
        roomAssignmentModel.roomAssignment_Id = roomAssign.roomAssignment_Id;
        roomAssignmentModel.roomAssignment_RoomId = roomAssign.roomAssignment_RoomId;
        roomAssignmentModel.roomAssignment_UserId = (int)supervisorId; //fix can't show data for supervisor in find room assignment userId
        roomAssignmentModel.roomAssignment_AssignedDate = roomAssign.roomAssignment_AssignedDate;
        roomAssignmentModel.roomAssignment_LastCleaningDate = roomAssign.roomAssignment_LastCleaningDate;
        roomAssignmentModel.roomAssignment_Priority = roomAssign.roomAssignment_Priority;
        roomAssignmentModel.roomAssignment_PostStatus = (int)POST_STATUS_UN_CHANGED;
        roomAssignmentModel.roomAssignment_LastModified = roomAssign.roomAssignment_LastModified;
        roomAssignmentModel.roomAssignment_Priority_Sort_Order = roomAssign.roomAssignment_Priority_Sort_Order;
        roomAssignmentModel.roomAssignmentHousekeeperId = roomAssign.roomAssignmentHousekeeperId;
        roomAssignmentModel.roomAssignmentRoomStatusId = roomAssign.roomAssignmentRoomStatusId ;
        roomAssignmentModel.roomAssignmentRoomCleaningStatusId = roomAssign.roomAssignmentRoomCleaningStatusId;
        roomAssignmentModel.roomAssignmentRoomInspectionStatusId = roomAssign.roomAssignmentRoomInspectionStatusId;
        roomAssignmentModel.roomAssignmentRoomExpectedCleaningTime = roomAssign.roomAssignmentRoomExpectedCleaningTime;
        roomAssignmentModel.roomAssignmentRoomExpectedInspectionTime = roomAssign.roomAssignmentRoomExpectedInspectionTime;
        roomAssignmentModel.roomAssignmentGuestProfileId = roomAssign.roomAssignmentGuestProfileId;
        
        NSArray *arrayName = [roomAssign.roomAssignmentFirstName componentsSeparatedByString:@" "];
        
        if (arrayName.count > 2) {
            roomAssignmentModel.roomAssignmentFirstName = [NSString stringWithFormat:@"%@ %@", [arrayName objectAtIndex:0], [arrayName objectAtIndex:arrayName.count - 1]];
        } else {
            roomAssignmentModel.roomAssignmentFirstName = roomAssign.roomAssignmentFirstName;
        }
        
        roomAssignmentModel.roomAssignmentGuestFirstName = roomAssign.roomAssignmentGuestFirstName;
        roomAssignmentModel.roomAssignmentGuestLastName = roomAssign.roomAssignmentGuestLastName;
        
        //set mockup room local
        NSInteger isMockupRoomValue = roomAssign.raIsMockRoom ;
        if(isMockupRoomValue == ENUM_TASK_TYPE_NORMAL)
        {
            roomAssignmentModel.raIsMockRoom = 0;
        }
        else if(isMockupRoomValue == ENUM_TASK_TYPE_REASSIGN)
        {
            roomAssignmentModel.raIsMockRoom = 0;
            //roomAssignmentModel.raIsReassignedRoom = 1;
        }
        else if(isMockupRoomValue >= ENUM_TASK_TYPE_MOCKUP)
        {
            roomAssignmentModel.raIsMockRoom = 1;
        }
        
        
        roomAssignmentModel.raKindOfRoom = roomAssign.raKindOfRoom;
        roomAssignmentModel.raGuestDepartureTime = roomAssign.raGuestDepartureTime;
        roomAssignmentModel.raGuestArrivalTime = roomAssign.raGuestArrivalTime;
        
        //Quang edit
        if(roomAssign.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_PENDING_FIRST_TIME && isMockupRoomValue == ENUM_TASK_TYPE_REASSIGN)
            roomAssignmentModel.raIsReassignedRoom = 1;
        else
            roomAssignmentModel.raIsReassignedRoom = 0;
        
        
        NSDateFormatter *dateTimeFormat = [[NSDateFormatter alloc] init];
        [dateTimeFormat setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
        [dateTimeFormat setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
        NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
        [dateTimeFormat setLocale:usLocale];
        
        RoomStatusModelV2 *roomStatusModel = [[RoomStatusModelV2 alloc] init];
        roomStatusModel.rstat_Id = (int)roomAssignmentModel.roomAssignmentRoomStatusId;
        [[RoomManagerV2 sharedRoomManager] loadRoomStatusData:roomStatusModel];
        
        CleaningStatusModelV2 *cleaningStatusModel = [[CleaningStatusModelV2 alloc] init];
        cleaningStatusModel.cstat_Id = (int)roomAssignmentModel.roomAssignmentRoomCleaningStatusId;
        [[RoomManagerV2  sharedRoomManager] loadCleaningStatus:cleaningStatusModel];
        
        NSMutableDictionary *roomAssignmentData = [[NSMutableDictionary alloc] init];
        [roomAssignmentData setValue:[NSString stringWithFormat:@"%i",roomAssignmentModel.roomAssignment_UserId] forKey:kRaUserId];
        [roomAssignmentData setValue:roomAssignmentModel.roomAssignment_RoomId forKey:kRoomNo];
        [roomAssignmentData setValue:[NSString stringWithFormat:@"%i",(int)roomAssignmentModel.roomAssignmentRoomStatusId] forKey:kRoomStatusId];
        [roomAssignmentData setValue:[NSString stringWithFormat:@"%i",(int)roomAssignmentModel.roomAssignmentRoomInspectionStatusId] forKey:kRoomInspectedStatusId];
        
        if([[LanguageManagerV2 sharedLanguageManager] currentLanguageIsEqualTo:@"%@", ENGLISH_LANGUAGE]){
            [roomAssignmentData setValue:roomStatusModel.rstat_Name forKey:kRMStatus];
        } else {
            [roomAssignmentData setValue:roomStatusModel.rstat_Lang forKey:kRMStatus];
        }
        [roomAssignmentData setValue:[NSString stringWithFormat:@"%i",(int)roomAssignmentModel.roomAssignmentRoomCleaningStatusId] forKey:kCleaningStatusID];
        [roomAssignmentData setValue:[NSString stringWithFormat:@"%@ %@",roomAssign.roomAssignmentGuestFirstName == nil? @"" : roomAssign.roomAssignmentGuestFirstName,roomAssign.roomAssignmentGuestLastName == nil? @"" :roomAssign.roomAssignmentGuestLastName] forKey:kGuestName];
        [roomAssignmentData setValue:[NSString stringWithFormat:@"%i",roomAssignmentModel.roomAssignment_Id] forKey:kRoomAssignmentID];
        [roomAssignmentData setValue:[NSNumber numberWithInteger:roomAssign.roomAssignment_Priority_Sort_Order] forKey:kRaPrioritySortOrder];
        [roomAssignmentData setValue:roomAssignmentModel.roomAssignmentFirstName forKey:kRAFirstName];
        [roomAssignmentData setValue:roomAssignmentModel.roomAssignmentLastName forKey:kRALastName];
        [roomAssignmentData setValue:cleaningStatusModel.cstat_image forKey:kCleaningStatus];
        [roomAssignmentData setValue:roomAssign.roomAssignmentGuestFirstName forKey:kGuestFirstName];
        [roomAssignmentData setValue:roomAssign.roomAssignmentGuestLastName forKey:kGuestLastName];
        [roomAssignmentData setValue:roomAssign.raGuestArrivalTime forKey:kRaGuestArrivalTime];
        [roomAssignmentData setValue:roomAssign.raGuestDepartureTime forKey:kRaGuestDepartureTime];
        [roomAssignmentData setValue:[NSString stringWithFormat:@"%i",(int)roomAssignmentModel.raKindOfRoom] forKey:kRaKindOfRoom];
        [roomAssignmentData setValue:[NSString stringWithFormat:@"%i",(int)roomAssignmentModel.raIsMockRoom] forKey:kRaIsMockRoom];
        [roomAssignmentData setValue:roomStatusModel.rstat_Image forKey:kRoomStatusImage];
        [roomAssignmentData setValue:roomAssignmentModel.roomAssignment_AssignedDate forKey:kRoomAssignmentTime];
        [roomAssignmentData setValue:[NSString stringWithFormat:@"%i",(int)roomAssignmentModel.raIsReassignedRoom] forKey:kRaIsReassignedRoom];
        [roomAssignmentData setValue:roomModel.room_VIPStatusId forKey:kVIP];
        
        NSMutableDictionary * data = [[NSMutableDictionary alloc] init];
        [data setObject:roomAssignmentData forKey:keyRoomData];
        [data setObject:roomAssignmentModel forKey:keyRoomAssignmentModel];
        [data setObject:roomModel forKey:keyRoomModel];
        [data setObject:roomRecordModel forKey:keyRoomRecordModel];
        
        [array addObject:data];
    }
    
    
    return array;
}

-(BOOL)isToday:(NSString*) timeStr
{
    BOOL result = NO;
    if (timeStr != nil) {
        NSDateFormatter *dateTimeFormat = [[NSDateFormatter alloc] init];
        [dateTimeFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        //[dateTimeFormat setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
        
        NSDate* cleaningDate = [dateTimeFormat dateFromString:timeStr];
        
        //Remove time from cleaning date
        unsigned int flags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit;
        NSCalendar* calendar = [NSCalendar currentCalendar];
        NSDateComponents* componentRemoved = [calendar components:flags fromDate:cleaningDate];
        NSDate* cleaningDateRemoveTime = [calendar dateFromComponents:componentRemoved];
        
        //Remove time from current date
        componentRemoved = [calendar components:flags fromDate:[NSDate date]];
        NSDate* currentDateRemoveTime = [calendar dateFromComponents:componentRemoved];
        
        [dateTimeFormat setDateFormat:@"dd / MM / yyyy"];
        timeStr = [dateTimeFormat stringFromDate:cleaningDate];
        
        NSCalendar *calender = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        NSDateComponents *components = [calender components:NSDayCalendarUnit fromDate:cleaningDateRemoveTime toDate:currentDateRemoveTime options:0];
        
        if (components.day == 0) {
            //lastCleaningTime = @"Today";
            result = YES;
        }
        //else if (components.day == 1) {
        //    lastCleaningTime = @"Yesterday";
        //}
    }
    
    return result;
}

-(NSObject *)findRoomDetailWithUserID:(NSInteger)userId andRoomAssignmentID:(NSInteger)roomAssignmentId andLastModified:(NSString *)lastModified {
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    eHousekeepingService_GetRoomDetail *request = [[eHousekeepingService_GetRoomDetail alloc] init];
    
    [request setIntUsrID:[NSNumber numberWithInteger:userId]];
    
    if (lastModified != nil) {
        [request setStrLastModified:lastModified];
    }
    
    LogObject *logObj = [[LogObject alloc] init];
    logObj.dateTime = [NSDate date];
    logObj.log = [NSString stringWithFormat:@"<%i><findRoomDetailWithUserID><intUsrID:%i lastModified:%@>",(int)userId,(int)userId,lastModified];
    LogFileManager *logManager = [[LogFileManager alloc] init];
    [logManager save:logObj];
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetRoomDetailUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetRoomDetailResponse class]]) {
            eHousekeepingService_GetRoomDetailResponse *dataBody = (eHousekeepingService_GetRoomDetailResponse *)bodyPart;
            if (dataBody) {
                if ([dataBody.GetRoomDetailResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]]) {
                    
                    eHousekeepingService_RoomAssign *roomAssignment = dataBody.GetRoomDetailResult.RoomDetail;
                    
                    return roomAssignment;
                }
            }
        }
    }
    
    return nil;
}

#pragma mark - WS for Update Room Cleaning Status
-(BOOL)updateCleaningStatusWithUserID:(NSInteger)userId andRoomAssigmentMode:(RoomAssignmentModelV2 *)model{
    if([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp] == NO) return NO;
    
    BOOL result = YES;
    BOOL temp = NO;
    
    if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        if (model.roomAssignment_PostStatus == POST_STATUS_SAVED_UNPOSTED) {
            // posr assign date for Service Later status
            if(model.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_SERVICE_LATER) {
                RoomRecordModelV2 *roomRecord = [[RoomRecordModelV2 alloc] init];
                roomRecord.rrec_User_Id = (int)userId;
                roomRecord.rrec_room_assignment_id = model.roomAssignment_Id;
                roomRecord = [[RoomManagerV2 sharedRoomManager] loadRoomRecordDataByRoomIdAndUserId:roomRecord];
                
                if(roomRecord.rrec_Id > 0) {
                    RoomServiceLaterModelV2 *roomServiceLater = [[RoomServiceLaterModelV2 alloc] init];
                    roomServiceLater.rsl_RecordId = roomRecord.rrec_Id;
                    [[RoomManagerV2 sharedRoomManager] loadRoomServiceLaterDataByRecordId:roomServiceLater];
                    
                    [self postAssignDateForServiceLaterStatusWithUserID:userId andRoomAssignmentID:model.roomAssignment_Id andHouseKeepingID:model.roomAssignmentHousekeeperId andRoomServiceLaterModel:roomServiceLater];
                }
            }
                
            // update Cleaning Status
            temp = [self updateCleaningStatusForWSWithUserID:userId andRoomAssignmentID:model.roomAssignment_Id andCleaningStatusID:model.roomAssignmentRoomCleaningStatusId];
            
            if (temp == YES) {
                model.roomAssignment_PostStatus = (int)POST_STATUS_POSTED;
                [[RoomManagerV2 sharedRoomManager] update:model];
            } else
                result = NO;
        }
    }

    return result;
}

-(BOOL)updateCleaningStatusForWSWithUserID:(NSInteger)userId andRoomAssignmentID:(NSInteger)roomAssignmentId andCleaningStatusID:(NSInteger)cleaningStatusId {
    BOOL result = NO;
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole])
    {
        [binding setLogXMLInOut:YES];
    }
    eHousekeepingService_UpdateRoomCleaningStatus *request = [[eHousekeepingService_UpdateRoomCleaningStatus alloc] init];
    
    [request setIntSupervisorID:[NSNumber numberWithInteger:userId]];
    [request setIntRoomAssignID:[NSNumber numberWithInteger:roomAssignmentId]];
    [request setIntCleaningStatusID:[NSNumber numberWithInteger:cleaningStatusId]];
    
    LogObject *logObj = [[LogObject alloc] init];
    logObj.dateTime = [NSDate date];
    logObj.log = [NSString stringWithFormat:@"<%i><findRoomDetailWithUserID><intSupervisorID:%i intRoomAssignID:%i intCleaningStatusID:%i>",(int)userId,(int)userId,(int)roomAssignmentId,(int)cleaningStatusId];
    LogFileManager *logManager = [[LogFileManager alloc] init];
    [logManager save:logObj];
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding UpdateRoomCleaningStatusUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_UpdateRoomCleaningStatusResponse class]]) {
            eHousekeepingService_UpdateRoomCleaningStatusResponse *dataBody = (eHousekeepingService_UpdateRoomCleaningStatusResponse *)bodyPart;
            if (dataBody != nil) {
                if ([dataBody.UpdateRoomCleaningStatusResult.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_OK]]) {
                    //change post status
                    result = YES;
                } else {
                    result = NO;
                }
            } else {
                result = NO;
            }
        }
    }
    
    return result;
}

#pragma mark - WS for Post Assign Date for Service Later status when Update Room Cleaning Status
-(BOOL) postAssignDateForServiceLaterStatusWithUserID:(NSInteger) userId andRoomAssignmentID:(NSInteger) roomAssignmentId andHouseKeepingID:(NSInteger) houseKeepingId andRoomServiceLaterModel:(RoomServiceLaterModelV2 *)serviceLaterModel {
    BOOL result = NO;
    
    eHousekeepingServiceSoapBinding *binding = [eHousekeepingService eHousekeepingServiceSoapBinding];
   
    eHousekeepingService_AssignRoom *request = [[eHousekeepingService_AssignRoom alloc] init] ;
    [request setIntDutyAssignID:[NSNumber numberWithInteger:roomAssignmentId]];
    [request setIntHousekeeperID:[NSNumber numberWithInteger:houseKeepingId]];
    [request setIntSupervisorID:[NSNumber numberWithInteger:userId]];
    [request setStrAssignDateTime:serviceLaterModel.rsl_Time];
    
    eHousekeepingServiceSoapBindingResponse *response = [binding AssignRoomUsingParameters:request];
//    NSArray *responseHeaders = response.headers;
//    for (id header in responseHeaders) {
//    }
    
    NSArray *responseBody = response.bodyParts;
    for (id bodyPart in responseBody) {
        if ([bodyPart isKindOfClass:[SOAPFault class]]) {
            result = NO;
            continue;
        }
        
        // UpdateAsignRoom
        if ([bodyPart isKindOfClass:[eHousekeepingService_AssignRoomResponse class]]) {
            eHousekeepingService_AssignRoomResponse *body = (eHousekeepingService_AssignRoomResponse *)bodyPart;
            
            if ([body.AssignRoomResult.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]]) {
                RoomServiceLaterModelV2 *roomServiceLater = [[RoomServiceLaterModelV2 alloc] init];
                roomServiceLater.rsl_Id = serviceLaterModel.rsl_Id;
                [[RoomManagerV2 sharedRoomManager] deleteRoomServiceLaterData:roomServiceLater];
                
                result = YES;
            } else {
                result = NO;
            }
        }
    }
    
    return result;
}

@end
