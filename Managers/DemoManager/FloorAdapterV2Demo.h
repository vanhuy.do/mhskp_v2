//
//  FloorAdapterV2.h
//  mHouseKeeping
//
//  Created by TMS TRIBE on 3/12/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DatabaseAdapterV2.h"
#import "FloorModelV2.h"
#import "FloorAdapterV2.h"

@interface FloorAdapterV2Demo : FloorAdapterV2{
    
}
-(int)insertFloor:(FloorModelV2*) floorModel;
-(int)updateFloor:(FloorModelV2*)floorModel;
-(int)deleteFloor:(FloorModelV2*)floorModelV2;
-(NSMutableArray *)loadFloorsByFloorId:(FloorModelV2*)floorModel;
-(FloorModelV2 *)loadFloorByFloorId:(FloorModelV2*)floorModel;
-(NSMutableArray *)loadFloors;
-(NSMutableArray *)loadFloorsHaveUnassignRoom;
-(NSMutableArray *)loadFloorsHaveFindRoom:(NSInteger)userId andRoomStatusID:(NSInteger)roomStatusId;
-(NSMutableArray *)loadFloorsByUserId:(NSInteger)userId filterType:(NSInteger)filterType statusId:(NSInteger)roomStatusId;
-(NSMutableArray *)loadFloorsHaveFindCleaningStatus:(NSInteger)userId andCleaningStatusID:(NSInteger)cleaningStatusId;
-(NSMutableArray *)loadFloorsHaveFindKindOfRoom:(NSInteger)userId andKindOfRoom:(NSInteger)kindOfRoomId;
-(NSMutableArray *)loadFloorsHaveFindInspectedRoom:(NSInteger)userId;
-(NSInteger) numberOfUnAssignRoomOnFloor:(NSInteger)floorId;
-(NSInteger) numberOfUnAssignRoomOnZone:(NSInteger)zoneId;

//Get last modified floor
-(NSString*)getFloorLastModifiedDate;
@end
