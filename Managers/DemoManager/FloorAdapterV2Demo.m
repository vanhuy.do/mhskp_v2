//
//  FloorAdapterV2.m
//  mHouseKeeping
//
//  Created by TMS TRIBE on 3/12/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "FloorAdapterV2Demo.h"
#import "ehkDefines.h"
#import "FindByRoomFloorViewV2.h"
#import "DbDefinesV2.h"
#import "RoomManagerV2.h"

@implementation FloorAdapterV2Demo

-(int)insertFloor:(FloorModelV2*) floorModel {
    
  
        NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"%@ %@ ( %@, %@, %@, %@, %@) Values(?, ?,?,?,?)", 
                                      @"insert into",
                                      FLOOR,
                                      floor_id,
                                      floor_name,
                                      floor_name_lang,
                                      floor_building_id,
                                      floor_last_modified ];
        
        
        const char *sql=[sqlString UTF8String];
        NSInteger reAccessCount = 0;
        RE_ACCESS_DB:{
            if (reAccessCount > 0) {
                [self close];
                usleep(0.1);
                [self openDatabase];
            }

            reAccessCount++;
        }

        //NSLog(@"sqlString %@",sqlString);
//        [sqlString release];
		if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
        {
        }
        
        sqlite3_bind_int (self.sqlStament, 1, (int)floorModel.floor_id);
        sqlite3_bind_text(self.sqlStament, 2, [floorModel.floor_name UTF8String], -1,SQLITE_TRANSIENT);
        sqlite3_bind_text(self.sqlStament, 3, [floorModel.floor_name_lang UTF8String], -1,SQLITE_TRANSIENT);
        sqlite3_bind_int (self.sqlStament, 4, (int)floorModel.floor_building_id);
        sqlite3_bind_text(self.sqlStament, 5, [floorModel.floor_last_modified UTF8String], -1,SQLITE_TRANSIENT);
        
        
        sqlite3_reset(self.sqlStament);
    
    NSInteger returnInt = 0;
    
    NSInteger status = sqlite3_step(self.sqlStament);
    
    if(SQLITE_DONE != status) {  
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return (int)returnInt;
    } else {
        sqlite3_reset(self.sqlStament);
        returnInt = 1;
        return (int)returnInt;
    }

    
}


-(int)updateFloor:(FloorModelV2*)floorModel
{

        NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"UPDATE %@ SET %@=?, %@=?, %@=?, %@=?, %@=? WHERE %@ = %d",
                                      FLOOR,
                                      floor_id,
                                      floor_name,
                                      floor_name_lang,
                                      floor_building_id,
                                      floor_last_modified,
                                      
                                      floor_id,
                                      (int)floorModel.floor_id
                                      ];
        
        
        
		//const char *sql = "insert into user(usr_id, usr_name,usr_password,usr_role,usr_fullname,usr_lang,usr_title,usr_department,usr_department_id,usr_supervisor_id,usr_last_modified) Values(?, ?,?,?,?,?,?,?,?,?,?)";
        const char *sql=[sqlString UTF8String];
    
        NSInteger reAccessCount = 0;
        RE_ACCESS_DB:{
            if (reAccessCount > 0) {
                [self close];
                usleep(0.1);
                [self openDatabase];
            }

        reAccessCount++;
        }
    
    
		if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
        {
        }
        
        sqlite3_bind_int (self.sqlStament, 1, (int)floorModel.floor_id);
        sqlite3_bind_text(self.sqlStament, 2, [floorModel.floor_name UTF8String], -1,SQLITE_TRANSIENT);
        sqlite3_bind_text(self.sqlStament, 3, [floorModel.floor_name_lang UTF8String], -1,SQLITE_TRANSIENT);
        sqlite3_bind_int (self.sqlStament, 4, (int)floorModel.floor_building_id);
        sqlite3_bind_text(self.sqlStament, 5, [floorModel.floor_last_modified UTF8String], -1,SQLITE_TRANSIENT);
        
        sqlite3_bind_int (self.sqlStament, 6, (int)floorModel.floor_id);
        
    
    NSInteger returnInt = 0;    
    NSInteger status = sqlite3_step(self.sqlStament);
    if(SQLITE_DONE != status) {   
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return (int)returnInt;
    } else { 
        returnInt = 1;        
        return (int)returnInt;
    }
}

//@ delete roomAssignment 
-(int)deleteFloor:(FloorModelV2*)floorModelV2
{
    NSInteger result=1;
        NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"delete from %@ where %@= ?",FLOOR,floor_id];
        //        NSLog(@"delete = %@ %d", sqlString,roomAssignment.roomAssignment_Id);
		const char *sql=[sqlString UTF8String];
        NSInteger reAccessCount = 0;
        RE_ACCESS_DB:{
            if (reAccessCount > 0) {
                [self close];
                usleep(0.1);
                [self openDatabase];
            }

        reAccessCount++;
        }

//        [sqlString release];
		if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) result=0;
	
	
    sqlite3_bind_int(self.sqlStament, 1,(int)floorModelV2.floor_id);
	
    NSInteger status = sqlite3_step(sqlStament);
    
	if (SQLITE_DONE != status) {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
		result = 0;
    }
    
    return (int)result;
}

/*****************/
-(NSMutableArray *)loadFloorsByFloorId:(FloorModelV2*)floorModel{   
    NSMutableArray *resultFloor=[[NSMutableArray alloc] init] ;
    NSMutableString *sqlString;
    sqlString = [[NSMutableString alloc] initWithFormat:@"select  %@, %@, %@, %@, %@ from %@ where %@=? ",
                 floor_id,
                 floor_name,
                 floor_name_lang,
                 floor_building_id,
                 floor_last_modified,
                 
                 FLOOR,
                 floor_id];
    
    const char *sql =[sqlString UTF8String];
//    [sqlString release];
        NSInteger reAccessCount = 0;
        RE_ACCESS_DB:{
            if (reAccessCount > 0) {
                [self close];
                usleep(0.1);
                [self openDatabase];
            }

            reAccessCount++;
        }

    FloorModelV2 *floorInfo = nil;
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    
    sqlite3_bind_int(self.sqlStament, 1, (int)floorModel.floor_id);
    
    NSInteger status = sqlite3_step(sqlStament);
    
    while(status == SQLITE_ROW) 
    {
        floorInfo = [[FloorModelV2 alloc] init];
        
        floorInfo.floor_id = sqlite3_column_int(sqlStament, 0);
        
        if (sqlite3_column_text(self.sqlStament, 1) != nil) {
            floorModel.floor_name=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
        }
        
        if (sqlite3_column_text(self.sqlStament, 2) != nil) {
            floorModel.floor_name_lang=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
        }
        
        floorInfo.floor_building_id = sqlite3_column_int(sqlStament, 3);
        
        
        if (sqlite3_column_text(self.sqlStament, 4) != nil) {
            floorModel.floor_last_modified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
        }
        
        
        [resultFloor addObject:floorInfo];
        
        status = sqlite3_step(sqlStament);
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            [resultFloor removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    

    
    return resultFloor;
}

-(NSMutableArray *)loadFloorsByPhysicalCheckFloorId:(FloorModelV2*)floorModel{
    NSMutableArray *resultFloor=[[NSMutableArray alloc] init] ;
    NSMutableString *sqlString;
    sqlString = [[NSMutableString alloc] initWithFormat:@"select  %@, %@, %@, %@, %@ from %@ where %@=? ",
                 floor_id,
                 floor_name,
                 floor_name_lang,
                 floor_building_id,
                 floor_last_modified,
                 
                 FLOOR,
                 floor_id];
    
    const char *sql =[sqlString UTF8String];
    //    [sqlString release];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    FloorModelV2 *floorInfo = nil;
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    
    sqlite3_bind_int(self.sqlStament, 1, (int)floorModel.floor_id);
    
    NSInteger status = sqlite3_step(sqlStament);
    
    while(status == SQLITE_ROW)
    {
        floorInfo = [[FloorModelV2 alloc] init];
        
        floorInfo.floor_id = sqlite3_column_int(sqlStament, 0);
        
        if (sqlite3_column_text(self.sqlStament, 1) != nil) {
            floorModel.floor_name=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
        }
        
        if (sqlite3_column_text(self.sqlStament, 2) != nil) {
            floorModel.floor_name_lang=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
        }
        
        floorInfo.floor_building_id = sqlite3_column_int(sqlStament, 3);
        
        
        if (sqlite3_column_text(self.sqlStament, 4) != nil) {
            floorModel.floor_last_modified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
        }
        
        
        [resultFloor addObject:floorInfo];
        
        status = sqlite3_step(sqlStament);
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            [resultFloor removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    
    
    return resultFloor;
}


-(NSInteger) numberOfUnAssignRoomOnFloor:(NSInteger)floorId
{
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT COUNT(*) FROM %@ WHERE %@ = ?",
                           UNASSIGN_ROOM,
                           ur_floor_id];
    
    const char *sql=[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    NSInteger result = 0;
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
        
        sqlite3_bind_int (self.sqlStament, 1, (int)floorId);
        NSInteger status = sqlite3_step(sqlStament);
        while(status == SQLITE_ROW) {
            
            result += sqlite3_column_int(sqlStament, 0);
            
            status = sqlite3_step(sqlStament);
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        
    }
    
    sqlite3_bind_int (self.sqlStament, 1, (int)floorId);
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW) {
        
        result += sqlite3_column_int(sqlStament, 0);
        status = sqlite3_step(sqlStament);
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    
    
    return result;
}

-(NSInteger) numberOfUnAssignRoomOnZone:(NSInteger)zoneId
{
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT COUNT(*) FROM %@ WHERE %@ = ?",
                           UNASSIGN_ROOM,
                           ur_zone_id];
    
    const char *sql=[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    NSInteger result = 0;
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
        
        sqlite3_bind_int (self.sqlStament, 1, (int)zoneId);
        NSInteger status = sqlite3_step(sqlStament);
        while(status == SQLITE_ROW) {
            
            result += sqlite3_column_int(sqlStament, 0);
            
            status = sqlite3_step(sqlStament);
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        
    }
    
    sqlite3_bind_int (self.sqlStament, 1, (int)zoneId);
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW) {
        
        result += sqlite3_column_int(sqlStament, 0);
        status = sqlite3_step(sqlStament);
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    
    
    return result;
}

-(NSMutableArray *)loadFloorsHaveFindRoom:(NSInteger)userId andRoomStatusID:(NSInteger)roomStatusId
{
    NSMutableArray *resultFloor=[[NSMutableArray alloc] init];
    NSMutableString *sqlString;
    sqlString = [[NSMutableString alloc] initWithFormat:
                 @"select distinct a.%@, a.%@, a.%@, a.%@, a.%@ from %@ a join %@ b on a.%@ = b.%@ join %@ c on b.%@ = c.%@ where c.%@ = %d and c.%@ = 0 order by a.%@",
                 floor_id,
                 floor_name,
                 floor_name_lang,
                 floor_building_id,
                 floor_last_modified,
                 
                 FLOOR,
                 ROOM,
                 
                 floor_id,
                 room_floor_id,
                 
                 ROOM_ASSIGNMENT,
                 room_id,
                 ra_room_id,
                 
                 ra_room_status_id,
                 (int)roomStatusId,
                 
                 ra_delete,
                 floor_name
                 ];
    
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    
    FloorModelV2 *floorInfo = nil;
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
        
    }
    
    
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW)
    {
        floorInfo = [[FloorModelV2 alloc] init];
        
        floorInfo.floor_id = sqlite3_column_int(sqlStament, 0);
        
        if (sqlite3_column_text(self.sqlStament, 1) != nil) {
            floorInfo.floor_name=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
        }
        
        if (sqlite3_column_text(self.sqlStament, 2) != nil) {
            floorInfo.floor_name_lang=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
        }
        
        floorInfo.floor_building_id = sqlite3_column_int(sqlStament, 3);
        
        
        if (sqlite3_column_text(self.sqlStament, 4) != nil) {
            floorInfo.floor_last_modified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
        }
        
        
        [resultFloor addObject:floorInfo];
        
        status = sqlite3_step(sqlStament);
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            [resultFloor removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    return resultFloor;
}

-(NSMutableArray *)loadFloorsByUserId:(NSInteger)userId filterType:(NSInteger)filterType statusId:(NSInteger)statusId
{
    NSMutableArray *resultFloor=[[NSMutableArray alloc] init];
    NSMutableString *sqlString;
    if(filterType == roomOccupiedType) {
        if(statusId == ENUM_ROOM_TYPE_OCCUPIED_OD_DO) {
            
            NSString *currentDate = nil;
            NSDateFormatter *f = [[NSDateFormatter alloc] init];
            [f setDateFormat:@"yyyy-MM-dd"];
            currentDate = [NSString stringWithFormat:@"%@ 00:00:00", [f stringFromDate:[NSDate date]]];
            
            sqlString = [[NSMutableString alloc] initWithFormat:
                         @"select distinct a.%@, a.%@, a.%@, a.%@, a.%@ from %@ a join %@ b on a.%@ = b.%@ join %@ c on b.%@ = c.%@ where c.%@ = %d and c.%@ >= '%@' and c.%@ = 0 order by a.%@",
                         floor_id,
                         floor_name,
                         floor_name_lang,
                         floor_building_id,
                         floor_last_modified,
                         
                         FLOOR,
                         ROOM,
                         
                         floor_id,
                         room_floor_id,
                         
                         ROOM_ASSIGNMENT,
                         room_id,
                         ra_room_id,
                         
                         ra_room_status_id,
                         ENUM_ROOM_STATUS_OD,
                         
                         ra_guest_departure_time,
                         currentDate,
                         
                         ra_delete,
                         floor_name
                         ];
        }
    }
    
    if(sqlString.length <= 0) {
        return nil;
    }
    
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    
    FloorModelV2 *floorInfo = nil;
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
        
    }
    
    
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW)
    {
        floorInfo = [[FloorModelV2 alloc] init];
        
        floorInfo.floor_id = sqlite3_column_int(sqlStament, 0);
        
        if (sqlite3_column_text(self.sqlStament, 1) != nil) {
            floorInfo.floor_name=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
        }
        
        if (sqlite3_column_text(self.sqlStament, 2) != nil) {
            floorInfo.floor_name_lang=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
        }
        
        floorInfo.floor_building_id = sqlite3_column_int(sqlStament, 3);
        
        
        if (sqlite3_column_text(self.sqlStament, 4) != nil) {
            floorInfo.floor_last_modified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
        }
        
        
        [resultFloor addObject:floorInfo];
        
        status = sqlite3_step(sqlStament);
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            [resultFloor removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    return resultFloor;
}


-(NSMutableArray *)loadFloorsHaveFindInspectedRoom:(NSInteger)userId
{
    NSMutableArray *resultFloor=[[NSMutableArray alloc] init];
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:
                                  @"select distinct a.%@, a.%@, a.%@, a.%@, a.%@ from %@ a join %@ b on a.%@ = b.%@ join %@ c on b.%@ = c.%@ where c.%@ = %d and (c.%@ = %d or c.%@ = %d) and c.%@ = 0 order by a.%@",
                                  floor_id,
                                  floor_name,
                                  floor_name_lang,
                                  floor_building_id,
                                  floor_last_modified,
                                  
                                  FLOOR,
                                  ROOM,
                                  
                                  floor_id,
                                  room_floor_id,
                                  
                                  ROOM_ASSIGNMENT,
                                  room_id,
                                  ra_room_id,
                                  ra_user_id,
                                  (int)userId,
                                  
                                  ra_room_status_id,
                                  ENUM_ROOM_STATUS_VI,
                                  ra_room_status_id,
                                  ENUM_ROOM_STATUS_OI,
                                  
                                  ra_delete,
                                  floor_name
                                  ];
    
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    
    FloorModelV2 *floorInfo = nil;
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
        
    }
    
    
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW)
    {
        floorInfo = [[FloorModelV2 alloc] init];
        
        floorInfo.floor_id = sqlite3_column_int(sqlStament, 0);
        
        if (sqlite3_column_text(self.sqlStament, 1) != nil) {
            floorInfo.floor_name=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
        }
        
        if (sqlite3_column_text(self.sqlStament, 2) != nil) {
            floorInfo.floor_name_lang=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
        }
        
        floorInfo.floor_building_id = sqlite3_column_int(sqlStament, 3);
        
        
        if (sqlite3_column_text(self.sqlStament, 4) != nil) {
            floorInfo.floor_last_modified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
        }
        
        
        [resultFloor addObject:floorInfo];
        
        status = sqlite3_step(sqlStament);
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            [resultFloor removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    return resultFloor;
}

-(NSMutableArray *)loadFloorsHaveFindCleaningStatus:(NSInteger)userId andCleaningStatusID:(NSInteger)cleaningStatusId
{
    NSMutableArray *resultFloor=[[NSMutableArray alloc] init];
    NSMutableString *sqlString;
    sqlString = [[NSMutableString alloc] initWithFormat:
                 @"select distinct a.%@, a.%@, a.%@, a.%@, a.%@ from %@ a join %@ b on a.%@ = b.%@ join %@ c on b.%@ = c.%@ where c.%@ = %d and c.%@ = 0 order by a.%@",
                 floor_id,
                 floor_name,
                 floor_name_lang,
                 floor_building_id,
                 floor_last_modified,
                 
                 FLOOR,
                 ROOM,
                 
                 floor_id,
                 room_floor_id,
                 
                 ROOM_ASSIGNMENT,
                 room_id,
                 ra_room_id,
                 
                 ra_room_cleaning_status_id,
                 (int)cleaningStatusId,
                 
                 ra_delete,
                 floor_name
                 ];
    
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    
    FloorModelV2 *floorInfo = nil;
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
        
    }
    
    
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW)
    {
        floorInfo = [[FloorModelV2 alloc] init];
        
        floorInfo.floor_id = sqlite3_column_int(sqlStament, 0);
        
        if (sqlite3_column_text(self.sqlStament, 1) != nil) {
            floorInfo.floor_name=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
        }
        
        if (sqlite3_column_text(self.sqlStament, 2) != nil) {
            floorInfo.floor_name_lang=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
        }
        
        floorInfo.floor_building_id = sqlite3_column_int(sqlStament, 3);
        
        
        if (sqlite3_column_text(self.sqlStament, 4) != nil) {
            floorInfo.floor_last_modified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
        }
        
        
        [resultFloor addObject:floorInfo];
        
        status = sqlite3_step(sqlStament);
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            [resultFloor removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    return resultFloor;
}

-(NSMutableArray *)loadFloorsHaveFindKindOfRoom:(NSInteger)userId andKindOfRoom:(NSInteger)kindOfRoomId
{
    NSMutableArray *resultFloor=[[NSMutableArray alloc] init];
    NSMutableString *sqlString;
    sqlString = [[NSMutableString alloc] initWithFormat:
                 @"select distinct a.%@, a.%@, a.%@, a.%@, a.%@ from %@ a join %@ b on a.%@ = b.%@ join %@ c on b.%@ = c.%@ where c.%@ = %d and c.%@ = 0 order by a.%@",
                 floor_id,
                 floor_name,
                 floor_name_lang,
                 floor_building_id,
                 floor_last_modified,
                 
                 FLOOR,
                 ROOM,
                 
                 floor_id,
                 room_floor_id,
                 
                 ROOM_ASSIGNMENT,
                 room_id,
                 ra_room_id,
                 
                 ra_kind_of_room,
                 (int)kindOfRoomId,
                 
                 ra_delete,
                 floor_name
                 ];
    
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    
    FloorModelV2 *floorInfo = nil;
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
        
    }
    
    
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW)
    {
        floorInfo = [[FloorModelV2 alloc] init];
        
        floorInfo.floor_id = sqlite3_column_int(sqlStament, 0);
        
        if (sqlite3_column_text(self.sqlStament, 1) != nil) {
            floorInfo.floor_name=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
        }
        
        if (sqlite3_column_text(self.sqlStament, 2) != nil) {
            floorInfo.floor_name_lang=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
        }
        
        floorInfo.floor_building_id = sqlite3_column_int(sqlStament, 3);
        
        
        if (sqlite3_column_text(self.sqlStament, 4) != nil) {
            floorInfo.floor_last_modified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
        }
        
        
        [resultFloor addObject:floorInfo];
        
        status = sqlite3_step(sqlStament);
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            [resultFloor removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    return resultFloor;
}

-(NSMutableArray *)loadFloorsHaveUnassignRoom{
    NSMutableArray *resultFloor=[[NSMutableArray alloc] init];
    NSMutableString *sqlString;
    sqlString = [[NSMutableString alloc] initWithFormat:@"select distinct a.%@, a.%@, a.%@, a.%@, a.%@ from %@ a join %@ b on a.%@ = b.%@ order by a.%@",
                 floor_id,
                 floor_name,
                 floor_name_lang,
                 floor_building_id,
                 floor_last_modified,
                 
                 FLOOR,
                 UNASSIGN_ROOM,
                 
                 floor_id,
                 ur_floor_id,
                 floor_name
                 ];
    
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    
    FloorModelV2 *floorInfo = nil;
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
        
    }
    
    
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW)
    {
        floorInfo = [[FloorModelV2 alloc] init];
        
        floorInfo.floor_id = sqlite3_column_int(sqlStament, 0);
        
        if (sqlite3_column_text(self.sqlStament, 1) != nil) {
            floorInfo.floor_name=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
        }
        
        if (sqlite3_column_text(self.sqlStament, 2) != nil) {
            floorInfo.floor_name_lang=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
        }
        
        floorInfo.floor_building_id = sqlite3_column_int(sqlStament, 3);
        
        
        if (sqlite3_column_text(self.sqlStament, 4) != nil) {
            floorInfo.floor_last_modified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
        }
        
        
        [resultFloor addObject:floorInfo];
        
        status = sqlite3_step(sqlStament);
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            [resultFloor removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    return resultFloor;
}


-(NSMutableArray *)loadFloors{
    NSMutableArray *resultFloor=[[NSMutableArray alloc] init];
    NSMutableString *sqlString;
    sqlString = [[NSMutableString alloc] initWithFormat:@"select  %@, %@, %@, %@, %@ from %@",
                 floor_id,
                 floor_name,
                 floor_name_lang,
                 floor_building_id,
                 floor_last_modified,
                 
                 FLOOR];
    
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }

        reAccessCount++;
    }

    
    FloorModelV2 *floorInfo = nil;
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
   
    }
    
    
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW) 
    {
        floorInfo = [[FloorModelV2 alloc] init];
        
        floorInfo.floor_id = sqlite3_column_int(sqlStament, 0);
        
        if (sqlite3_column_text(self.sqlStament, 1) != nil) {
            floorInfo.floor_name=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
        }
        
        if (sqlite3_column_text(self.sqlStament, 2) != nil) {
            floorInfo.floor_name_lang=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
        }
        
        floorInfo.floor_building_id = sqlite3_column_int(sqlStament, 3);
        
        
        if (sqlite3_column_text(self.sqlStament, 4) != nil) {
            floorInfo.floor_last_modified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
        }
        
        
        [resultFloor addObject:floorInfo];
        
        status = sqlite3_step(sqlStament);
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            [resultFloor removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }

    return resultFloor;
}
/*****************/
-(FloorModelV2 *)loadFloorByFloorId:(FloorModelV2*)floorModel{   

    NSMutableString *sqlString;
    sqlString = [[NSMutableString alloc] initWithFormat:@"select  %@, %@, %@, %@, %@ from %@ where %@=? ",
                 floor_id,
                 floor_name,
                 floor_name_lang,
                 floor_building_id,
                 floor_last_modified,
                 
                 
                 FLOOR,
                 floor_id];
    
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
        RE_ACCESS_DB:{
            if (reAccessCount > 0) {
                [self close];
                usleep(0.1);
                [self openDatabase];
            }

            reAccessCount++;
        }

    FloorModelV2 *floorInfo = nil;
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {

    }
    
    sqlite3_bind_int(self.sqlStament, 1, (int)floorModel.floor_id);
    
    NSInteger status = sqlite3_step(sqlStament);
    
    while(status == SQLITE_ROW) 
    {
        floorInfo = [[FloorModelV2 alloc] init];
        
        floorInfo.floor_id = sqlite3_column_int(sqlStament, 0);
        
        if (sqlite3_column_text(self.sqlStament, 1) != nil) {
            floorModel.floor_name=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
        }
        
        if (sqlite3_column_text(self.sqlStament, 2) != nil) {
            floorModel.floor_name_lang=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
        }
        
        floorInfo.floor_building_id = sqlite3_column_int(sqlStament, 3);
        
        
        if (sqlite3_column_text(self.sqlStament, 4) != nil) {
            floorModel.floor_last_modified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
        }
        
        status = sqlite3_step(sqlStament);
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }

    return floorInfo;
}

-(NSString*)getFloorLastModifiedDate
{
    NSMutableString * sqlString=[[NSMutableString alloc] initWithFormat:@"SELECT MAX(%@) FROM %@", floor_last_modified, FLOOR];
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    reAccessCount ++;
}
    
    NSString *lastModified;
    
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        
        status = sqlite3_step(sqlStament);
        while(status == SQLITE_ROW) {
            
            if(sqlite3_column_text(sqlStament, 0)){
                lastModified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 0)];
            }
            return lastModified;
        }
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    return  lastModified;
}

@end
