//
//  GuestInfoAdapterV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 3/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DatabaseAdapterV2.h"
#import "GuestInfoModelV2.h"
#import "GuestInfoAdapterV2.h"

@interface GuestInfoAdapterV2Demo : GuestInfoAdapterV2{
    
}
//-(int)insertGuestInfoData:(GuestInfoModelV2*) guestInfo;
//-(int)updateGuestInfoData:(GuestInfoModelV2*) guestInfo;
-(int)deleteGuestInfoData:(GuestInfoModelV2*) guestInfo;
-(GuestInfoModelV2 *)loadGuestInfoData:(GuestInfoModelV2*) guestInfo;
//-(GuestInfoModelV2 *)loadGuestInfoDataByRoomID:(NSString*)roomNumber;
//-(NSMutableArray *)loadAllGuestInfo;
-(NSInteger) numberOfMustPostGuestInfoRecordsWithUserId:(NSInteger) userId;


@end
