//
//  GuestInfoAdapterV2.m
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 3/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GuestInfoAdapterV2Demo.h"
#import "DbDefinesV2.h"
#import "ehkDefines.h"

@implementation GuestInfoAdapterV2Demo
//-(int)insertGuestInfoData:(GuestInfoModelV2*) guestInfo{
//    NSString *sqlString = [[NSString alloc] initWithFormat:@"INSERT INTO %@ (%@, %@, %@, %@, %@, %@, %@, %@,%@, %@, %@, %@, %@) %@",
//                          
//                           GUEST_INFOS,
//                           
//                           guest_name,
//                           guest_image,
//                           guest_lang_pref,
//                           guest_pref_desc,
//                           guest_room_id,
//                           guest_check_in_time,
//                           guest_check_out_time,
//                           guest_post_status,
//                           guest_last_modified,
//                           vip_number,
//                           housekeeper_name,
//                           guest_number,
//                           guest_type,
//                           @"VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)"];
//    
//    const char *sql =[sqlString UTF8String];
//
//    NSInteger reAccessCount = 0;
//    RE_ACCESS_DB:{
//        if (reAccessCount > 0) {
//            [self close];
//            usleep(0.1);
//            [self openDatabase];
//        }
//        reAccessCount ++;
//    }
//    
//    NSInteger status1=sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
//    if(status1 == SQLITE_OK) {
//        
//        //sqlite3_bind_int(self.sqlStament, 1, guestInfo.guestId);
//        sqlite3_bind_text(self.sqlStament, 1, [guestInfo.guestName UTF8String], -1, SQLITE_TRANSIENT);
//        sqlite3_bind_text(self.sqlStament, 3, [guestInfo.guestLangPref UTF8String], -1,SQLITE_TRANSIENT);
//        sqlite3_bind_text(self.sqlStament, 4, [guestInfo.guestGuestRef UTF8String], -1,SQLITE_TRANSIENT);
//        sqlite3_bind_text(self.sqlStament, 5 , [guestInfo.guestRoomId UTF8String], -1,SQLITE_TRANSIENT);
//        sqlite3_bind_text(self.sqlStament, 6, [guestInfo.guestCheckInTime UTF8String], -1, SQLITE_TRANSIENT);
//        sqlite3_bind_text(self.sqlStament, 7, [guestInfo.guestCheckOutTime UTF8String], -1, SQLITE_TRANSIENT);
//        sqlite3_bind_int(self.sqlStament, 8, guestInfo.guestPostStatus);
//        sqlite3_bind_text(self.sqlStament, 9, [guestInfo.guestLastModified UTF8String], -1, SQLITE_TRANSIENT);
//        sqlite3_bind_int(self.sqlStament, 10, guestInfo.guestNumberVIP);
//        sqlite3_bind_text(self.sqlStament, 11, [guestInfo.guestHousekeeperName UTF8String], -1, SQLITE_TRANSIENT);
//        sqlite3_bind_text(self.sqlStament, 12, [guestInfo.guestNumber UTF8String], -1, SQLITE_TRANSIENT);
//        //    sqlite3_bind_text(self.sqlStament, 8, [guestInfo.guest_Image UTF8String], -1, SQLITE_TRANSIENT);
//        if(guestInfo.guestImage != nil)
//            sqlite3_bind_blob(self.sqlStament, 2, [guestInfo.guestImage bytes], [guestInfo.guestImage length], NULL);
//        else
//            sqlite3_bind_blob(self.sqlStament, 2, nil, -1, NULL);
//        
//        sqlite3_bind_int(self.sqlStament, 13, guestInfo.guestType);
//    }
//    NSInteger status = sqlite3_step(self.sqlStament);
//    if (status == SQLITE_DONE) {
//        int result = sqlite3_last_insert_rowid(self.database);
//        sqlite3_reset(self.sqlStament);
//        return result;
//    }
//    else{
//        if (status == SQLITE_BUSY) {
//            if (reAccessCount <= REACCESS_MAX_TIMES) {
//                goto RE_ACCESS_DB;
//            }
//        }
//    }
//    return  0;
//
//    
//}
//
//-(int)updateGuestInfoData:(GuestInfoModelV2*) guestInfo{
//    
//    NSString *sqlString = [[NSString alloc] initWithFormat:@"UPDATE %@ SET %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ? WHERE %@ = ?",GUEST_INFOS, guest_name,guest_image,guest_lang_pref,guest_pref_desc,guest_room_id,guest_check_in_time,guest_check_out_time,guest_post_status, guest_last_modified, vip_number, housekeeper_name,guest_number, guest_id];
//    const char *sql =[sqlString UTF8String];
//    
//    NSInteger reAccessCount = 0;
//    RE_ACCESS_DB:{
//        if (reAccessCount > 0) {
//            [self close];
//            usleep(0.1);
//            [self openDatabase];
//        }
//        reAccessCount ++;
//    }
//    
//    NSInteger status1=sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
//    if(status1 == SQLITE_OK) {
//        
//        sqlite3_bind_text(self.sqlStament, 1, [guestInfo.guestName UTF8String], -1, SQLITE_TRANSIENT);
//        if(guestInfo.guestImage != nil)
//            sqlite3_bind_blob(self.sqlStament, 2, [guestInfo.guestImage bytes], [guestInfo.guestImage length], NULL);
//        else
//            sqlite3_bind_blob(self.sqlStament, 2, nil, -1, NULL);
//        sqlite3_bind_text(self.sqlStament, 3, [guestInfo.guestLangPref UTF8String], -1,SQLITE_TRANSIENT);
//        sqlite3_bind_text(self.sqlStament, 4, [guestInfo.guestGuestRef UTF8String], -1,SQLITE_TRANSIENT);
//        sqlite3_bind_text(self.sqlStament, 5 , [guestInfo.guestRoomId UTF8String], -1,SQLITE_TRANSIENT);
//        sqlite3_bind_text(self.sqlStament, 6, [guestInfo.guestCheckInTime UTF8String], -1, SQLITE_TRANSIENT);
//        sqlite3_bind_text(self.sqlStament, 7, [guestInfo.guestCheckOutTime UTF8String], -1, SQLITE_TRANSIENT);
//        sqlite3_bind_int(self.sqlStament, 8, guestInfo.guestPostStatus);
//        sqlite3_bind_text(self.sqlStament, 9, [guestInfo.guestLastModified UTF8String], -1, SQLITE_TRANSIENT);
//        sqlite3_bind_int(self.sqlStament, 10, guestInfo.guestNumberVIP);
//        sqlite3_bind_text(self.sqlStament, 11, [guestInfo.guestHousekeeperName UTF8String], -1, SQLITE_TRANSIENT);
//        sqlite3_bind_text(self.sqlStament, 12, [guestInfo.guestNumber UTF8String], -1, SQLITE_TRANSIENT);
//        
//        sqlite3_bind_int(sqlStament, 13, guestInfo.guestId);
//    }
//    
//    NSInteger status = sqlite3_step(self.sqlStament);
//    
//    if (status == SQLITE_DONE) {
//        return 1;
//    }
//    else{
//        if (status == SQLITE_BUSY) {
//            if (reAccessCount <= REACCESS_MAX_TIMES) {
//                goto RE_ACCESS_DB;
//            }
//        }
//    }
//    return  0;
//}

-(int)deleteGuestInfoData:(GuestInfoModelV2*) guestInfo{
    NSInteger result = 1;
    if(self.sqlStament == nil) {
        NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"DELETE FROM %@ WHERE %@ = ?",GUEST_INFOS,guest_id];
		const char *sql = [sqlString UTF8String];
        
		if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
			result = 0;
	}
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
	
    sqlite3_bind_int(self.sqlStament, 1, (int)guestInfo.guestId);
	
    NSInteger status = sqlite3_step(self.sqlStament);
    
	if (SQLITE_DONE != status) {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
		result = 0;
	}
    
    return (int)result;
}

-(GuestInfoModelV2 *)loadGuestInfoData:(GuestInfoModelV2*) guestInfo{
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ?",guest_id, guest_room_id, guest_name, guest_lang_pref, guest_check_in_time, guest_check_out_time, guest_post_status, guest_last_modified, guest_image,vip_number, housekeeper_name,guest_number, GUEST_INFOS, guest_id];
    const char *sql =[sqlString UTF8String];

    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSInteger status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        
        sqlite3_bind_int(self.sqlStament, 1, (int)guestInfo.guestId);
        
        status = sqlite3_step(self.sqlStament);
        
        while(status == SQLITE_ROW)
        {
            if (sqlite3_column_text(self.sqlStament, 1)) {
                guestInfo.guestRoomId = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
            }
            if (sqlite3_column_text(sqlStament, 2)) {
                guestInfo.guestName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
            }
            if (sqlite3_column_text(sqlStament, 3)) {
                guestInfo.guestLangPref = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
            }
            if (sqlite3_column_text(sqlStament, 4)) {
                guestInfo.guestCheckInTime = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
            }
            if (sqlite3_column_text(sqlStament, 5)) {
                guestInfo.guestCheckOutTime = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
            }
            guestInfo.guestPostStatus = sqlite3_column_int(sqlStament, 6);
            if (sqlite3_column_text(sqlStament, 7)) {
                guestInfo.guestLastModified = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 7)];
            }
            //            if (sqlite3_column_text(self.sqlStament, 8)) {
            //                guestInfo.guest_Image = [NSString stringWithUTF8String:(char *)sqlite3_column_text(self.sqlStament, 8)];
            //            }
            if (sqlite3_column_blob(self.sqlStament, 8)) {
                guestInfo.guestImage = [NSData dataWithBytes:sqlite3_column_blob(self.sqlStament, 8) length:sqlite3_column_bytes(self.sqlStament, 8)];
            }
            if (sqlite3_column_int(sqlStament, 9)){
                guestInfo.guestVIPCode = [[NSString alloc] initWithFormat:@"%d",sqlite3_column_int(self.sqlStament,9)] ;
            }
            if (sqlite3_column_text(sqlStament, 10)) {
                guestInfo.guestHousekeeperName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 10)];
            }
            
            if (sqlite3_column_text(sqlStament, 11)) {
                guestInfo.guestNumber = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 11)];
            }
            status = sqlite3_step(self.sqlStament);
        }
        
        if (status== SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }
    if (status== SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    return guestInfo;
}

//-(GuestInfoModelV2 *)loadGuestInfoDataByRoomID:(NSString*)roomNumber{
//    
//    GuestInfoModelV2 *guestInfo;
//    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@, %@, %@,%@, %@, %@, %@ FROM %@ WHERE %@= '%@'",guest_id,guest_name,guest_image,guest_lang_pref,guest_pref_desc,guest_room_id,guest_check_in_time,guest_check_out_time,guest_post_status, guest_last_modified, vip_number, housekeeper_name,guest_number, GUEST_INFOS,guest_room_id,roomNumber];
//    const char *sql =[sqlString UTF8String];
//
//    NSInteger reAccessCount = 0;
//    RE_ACCESS_DB:{
//        if (reAccessCount > 0) {
//            [self close];
//            usleep(0.1);
//            [self openDatabase];
//        }
//        reAccessCount ++;
//    }
//    
//    NSInteger status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
//    if(status == SQLITE_OK) {
//        
//        status =  sqlite3_step(self.sqlStament);
//        
//        while(status == SQLITE_ROW)
//        {
//            guestInfo = [[GuestInfoModelV2 alloc] init];
//            guestInfo.guestId = sqlite3_column_int(sqlStament, 0);
//            if (sqlite3_column_text(self.sqlStament, 5)) {
//                guestInfo.guestRoomId = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
//            }
//            if (sqlite3_column_text(sqlStament,1)) {
//                guestInfo.guestName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament,1)];
//            }
//            if(sqlite3_column_blob(sqlStament, 2)){
//                NSData *image = [[NSData alloc] initWithBytes:sqlite3_column_blob(sqlStament, 2) length:sqlite3_column_bytes(sqlStament, 2)];
//                [guestInfo setGuestImage:image];
//            }
//            if (sqlite3_column_text(sqlStament, 3)) {
//                guestInfo.guestLangPref = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
//            }
//            if (sqlite3_column_text(sqlStament,4)) {
//                guestInfo.guestGuestRef = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament,4)];
//            }
//            if (sqlite3_column_text(sqlStament, 6)) {
//                guestInfo.guestCheckInTime = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament,6)];
//            }
//            if (sqlite3_column_text(sqlStament, 7)) {
//                guestInfo.guestCheckOutTime = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament,7)];
//            }
//            guestInfo.guestPostStatus = sqlite3_column_int(sqlStament,8);
//            if (sqlite3_column_text(sqlStament,9)) {
//                guestInfo.guestLastModified = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament,9)];
//            }
//            guestInfo.guestNumberVIP = sqlite3_column_int(sqlStament, 10);
//            if (sqlite3_column_text(sqlStament, 11)) {
//                guestInfo.guestHousekeeperName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 11)];
//            }
//            
//            if (sqlite3_column_text(sqlStament, 12)) {
//                guestInfo.guestNumber = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 12)];
//            }
//            
//            status = sqlite3_step(self.sqlStament);
//        }
//        
//        if (status == SQLITE_BUSY) {
//            if (reAccessCount <= REACCESS_MAX_TIMES) {
//                goto RE_ACCESS_DB;
//            }
//        }
//    }
//    if (status == SQLITE_BUSY) {
//        if (reAccessCount <= REACCESS_MAX_TIMES) {
//            goto RE_ACCESS_DB;
//        }
//    }
//    return guestInfo;
//}

//-(NSMutableArray *)loadAllGuestInfo{
//    NSMutableArray *array = [NSMutableArray array];
//    GuestInfoModelV2 *guestInfo;
//    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@, %@, %@,%@, %@, %@, %@ FROM %@",guest_id,guest_name,guest_image,guest_lang_pref,guest_pref_desc,guest_room_id,guest_check_in_time,guest_check_out_time,guest_post_status, guest_last_modified, vip_number, housekeeper_name, guest_number, GUEST_INFOS];
//    const char *sql =[sqlString UTF8String];
//
//    NSInteger reAccessCount = 0;
//    RE_ACCESS_DB:{
//        if (reAccessCount > 0) {
//            [self close];
//            usleep(0.1);
//            [self openDatabase];
//        }
//        reAccessCount ++;
//    }
//    
//    NSInteger status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
//    if(status == SQLITE_OK) {
//        
//        status = sqlite3_step(self.sqlStament);
//        
//        while(status == SQLITE_ROW)
//        {
//            guestInfo = [[GuestInfoModelV2 alloc] init];
//            guestInfo.guestId = sqlite3_column_int(sqlStament, 0);
//            if (sqlite3_column_text(self.sqlStament, 5)) {
//                guestInfo.guestRoomId = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
//            }
//            if (sqlite3_column_text(sqlStament,1)) {
//                guestInfo.guestName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament,1)];
//            }
//            if(sqlite3_column_blob(sqlStament, 2)){
//                NSData *image = [[NSData alloc] initWithBytes:sqlite3_column_blob(sqlStament, 2) length:sqlite3_column_bytes(sqlStament, 2)];
//                [guestInfo setGuestImage:image];
//            }
//            if (sqlite3_column_text(sqlStament, 3)) {
//                guestInfo.guestLangPref = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
//            }
//            if (sqlite3_column_text(sqlStament,4)) {
//                guestInfo.guestGuestRef = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament,4)];
//            }
//            if (sqlite3_column_text(sqlStament, 6)) {
//                guestInfo.guestCheckInTime = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament,6)];
//            }
//            if (sqlite3_column_text(sqlStament, 7)) {
//                guestInfo.guestCheckOutTime = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament,7)];
//            }
//            guestInfo.guestPostStatus = sqlite3_column_int(sqlStament,8);
//            if (sqlite3_column_text(sqlStament,9)) {
//                guestInfo.guestLastModified = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament,9)];
//            }
//            guestInfo.guestNumberVIP = sqlite3_column_int(sqlStament, 10);
//            if (sqlite3_column_text(sqlStament, 11)) {
//                guestInfo.guestHousekeeperName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 11)];
//            }
//            if (sqlite3_column_text(sqlStament, 12)) {
//                guestInfo.guestNumber = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 12)];
//            }
//            [array addObject:guestInfo];
//            
//            status = sqlite3_step(self.sqlStament);
//        }
//        
//        if (status == SQLITE_BUSY) {
//            if (reAccessCount <= REACCESS_MAX_TIMES) {
//                [array removeAllObjects];
//                goto RE_ACCESS_DB;
//            }
//        }
//    }
//    if (status == SQLITE_BUSY) {
//        if (reAccessCount <= REACCESS_MAX_TIMES) {
//            [array removeAllObjects];
//            goto RE_ACCESS_DB;
//        }
//    }    
//    return array;
//}

-(NSInteger)numberOfMustPostGuestInfoRecordsWithUserId:(NSInteger) userId {
    NSString *sqlString = [NSString stringWithFormat:@"SELECT COUNT(*) FROM %@ LEFT JOIN %@ WHERE %@ = %@ AND %@ = %d AND %@ = %d", 
                           GUEST_INFOS, 
                           ROOM_ASSIGNMENT, 
                           
                           ra_room_id, 
                           guest_id, 
                           
                           guest_post_status, 
                           (int)POST_STATUS_SAVED_UNPOSTED,
                           
                           ra_user_id, 
                           (int)userId
                           
                           ];
    const char *sql = [sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    NSInteger status = sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL);
    if (status == SQLITE_OK) {
        
        status = sqlite3_step(self.sqlStament);
        
        while (status == SQLITE_ROW) {
            
            return sqlite3_column_int(sqlStament, 0);
            status = sqlite3_step(self.sqlStament);
            
        }
        if (status== SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }
    if (status== SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }

    
    return 0;
}

@end
