//
//  GuideAdapterV2.h
//  mHouseKeeping
//
//  Created by Nghia Truong on 3/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DatabaseAdapterV2.h"
#import "DbDefinesV2.h"
#import "GuideModelV2.h"
#import "GuideAdapterV2.h"

@interface GuideAdapterV2Demo : GuideAdapterV2 {
    
}

-(int) insertGuideData:(GuideModelV2 *) guide;
-(int) updateGuideData:(GuideModelV2 *) guide;
-(int) deleteGuideData:(GuideModelV2 *) guide;
-(GuideModelV2 *) loadGuideData:(GuideModelV2 *) guide;
//-(GuideModelV2 *) loadGuideDataByRoomSectionId:(GuideModelV2 *) guide;
-(GuideModelV2 *) loadGuideDataByRoomTypeId:(GuideModelV2 *) guide;
-(NSMutableArray *) loadAllGuide;
//-(NSMutableArray *) loadAllGuideByRoomSectionId:(int) roomSectionId;
-(NSMutableArray *) loadAllGuideByRoomTypeId:(int) roomTypeId;
-(BOOL)haveGuideItemsByRoomType:(NSInteger)roomTypeId;
-(NSMutableArray *)loadAllGuideHaveGuideItems;

-(NSString *) getLatestLastModifiedGuide;


@end
