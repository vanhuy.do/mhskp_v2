//
//  GuideAdapterV2.m
//  mHouseKeeping
//
//  Created by Nghia Truong on 3/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GuideAdapterV2Demo.h"
#import "LogFileManager.h"
#import "ehkDefines.h"

@implementation GuideAdapterV2Demo

-(int)insertGuideData:(GuideModelV2 *)guide {
    NSString *sqlString = [[NSString alloc] initWithFormat:@"INSERT INTO %@ (%@, %@, %@, %@, %@, %@, %@) %@", GUIDELINES, guide_id, guide_room_type_id, guide_name, guide_name_lang, guide_image, guide_last_updated, guide_category_id,@"VALUES(?, ?, ?, ?, ?, ?, ?)"];
    
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    if(sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
        
    }
    
    sqlite3_bind_int (self.sqlStament, 1, guide.guide_Id);
    sqlite3_bind_int (self.sqlStament, 2, guide.guide_RoomTypeId);
    sqlite3_bind_text(self.sqlStament, 3, [guide.guide_Name UTF8String],
                      -1,SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 4, [guide.guide_NameLang UTF8String],
                      -1,SQLITE_TRANSIENT);
    if (guide.guide_Image) {
        sqlite3_bind_blob(self.sqlStament, 5, [guide.guide_Image bytes], (int)[guide.guide_Image length], NULL);
    }
    sqlite3_bind_text(self.sqlStament, 6, [guide.guide_LastUpdated UTF8String],
                      -1,SQLITE_TRANSIENT);
    sqlite3_bind_int (self.sqlStament, 7, guide.guide_CategoryId);
    sqlite3_reset(self.sqlStament);
    
    NSInteger temp = sqlite3_step(self.sqlStament);
    
    NSInteger returnInt = 0;
    
    if(SQLITE_DONE != temp) {
        if (temp == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return (int)returnInt;
    } else {
        sqlite3_last_insert_rowid(self.database);
        sqlite3_reset(self.sqlStament);
        returnInt = 1;
        return (int)returnInt;
    }
}

-(int)updateGuideData:(GuideModelV2 *)guide {
    NSString *sqlString = [[NSString alloc] initWithFormat:@"UPDATE %@ SET %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ? WHERE %@ = ? AND %@ = ?", GUIDELINES, guide_id, guide_room_type_id, guide_name, guide_name_lang, guide_image, guide_last_updated, guide_category_id, guide_id , guide_room_type_id];
    
    const char *sql = [sqlString UTF8String];
    
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
        if([LogFileManager isLogConsole])
        {
            NSLog(@"UPdate guide data fail with error code = %d - %p", sqlite3_errcode(self.database), sqlite3_errmsg(self.database));
        }
    }
    
    sqlite3_bind_int (self.sqlStament, 1, guide.guide_Id);
    sqlite3_bind_int (self.sqlStament, 2, guide.guide_RoomTypeId);
    sqlite3_bind_text(self.sqlStament, 3, [guide.guide_Name UTF8String],
                      -1,SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 4, [guide.guide_NameLang UTF8String],
                      -1,SQLITE_TRANSIENT);
    
    
    sqlite3_bind_blob(self.sqlStament, 5, [guide.guide_Image bytes], (int)[guide.guide_Image length], NULL);
    sqlite3_bind_text(self.sqlStament, 6, [guide.guide_LastUpdated UTF8String],
                      -1,SQLITE_TRANSIENT);
    sqlite3_bind_int (self.sqlStament, 7, guide.guide_CategoryId);
    sqlite3_bind_int (self.sqlStament, 8, guide.guide_Id);
    sqlite3_bind_int (self.sqlStament, 9, guide.guide_RoomTypeId);
    
    NSInteger returnInt = 0;
    
    if(SQLITE_DONE != sqlite3_step(self.sqlStament)) {
        return (int)returnInt;
    } else {
        returnInt = 1;
        
        return (int)returnInt;
    }
}

-(int)deleteGuideData:(GuideModelV2 *)guide {
    NSInteger result = 1;
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"DELETE FROM %@ WHERE %@ = ?", GUIDELINES, guide_id];
    
    const char *sql = [sqlString UTF8String];
    
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
        result = 0;
	
    sqlite3_bind_int(self.sqlStament, 1, guide.guide_Id);
	
	if (SQLITE_DONE != sqlite3_step(self.sqlStament)) 
		result = 0;
    
    return (int)result;
}

-(GuideModelV2 *)loadGuideData:(GuideModelV2 *)guide {
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ? AND %@ = ?", guide_id, guide_room_type_id, guide_name, guide_name_lang, guide_image, guide_last_updated, guide_category_id, GUIDELINES, guide_id, guide_room_type_id];
    
    const char *sql =[sqlString UTF8String];
    
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {
        sqlite3_bind_int(self.sqlStament, 1, guide.guide_Id);
        sqlite3_bind_int(self.sqlStament, 2, guide.guide_RoomTypeId);
        
        while(sqlite3_step(sqlStament) == SQLITE_ROW) {
            guide.guide_Id = sqlite3_column_int(self.sqlStament, 0);
            guide.guide_RoomTypeId = sqlite3_column_int(self.sqlStament, 1);
            if (sqlite3_column_text(self.sqlStament, 2)) {
                guide.guide_Name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
            }
            if (sqlite3_column_text(self.sqlStament, 3)) {
                guide.guide_NameLang = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
            }
            if (sqlite3_column_bytes(self.sqlStament, 4) > 0) {
                guide.guide_Image = [NSData dataWithBytes:sqlite3_column_blob
                                     (self.sqlStament, 4) length:sqlite3_column_bytes(self.sqlStament, 4)];
            }
            if (sqlite3_column_text(self.sqlStament, 5)) {
                guide.guide_LastUpdated = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
            }
            
            guide.guide_CategoryId = sqlite3_column_int(self.sqlStament, 6);
        }
    }
    
    return guide;
}

//-(GuideModelV2 *)loadGuideDataByRoomSectionId:(GuideModelV2 *)guide {
//    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ?", guide_id, guide_room_section_id, guide_room_type_id, guide_name, guide_name_lang, guide_image, guide_last_updated, GUIDELINES, guide_room_section_id];
//    
//    const char *sql =[sqlString UTF8String];
//    
//    
//    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {        
//        sqlite3_bind_int(self.sqlStament, 1, guide.guide_RoomSectionId);
//        
//        while(sqlite3_step(sqlStament) == SQLITE_ROW) {
//            guide.guide_Id = sqlite3_column_int(self.sqlStament, 0);
//            guide.guide_RoomSectionId = sqlite3_column_int(self.sqlStament, 1);
//            guide.guide_RoomTypeId = sqlite3_column_int(self.sqlStament, 2);
//            if (sqlite3_column_text(self.sqlStament, 3)) {
//                guide.guide_Name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
//            }
//            if (sqlite3_column_text(self.sqlStament, 4)) {
//                guide.guide_NameLang = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
//            }
//            if (sqlite3_column_bytes(self.sqlStament, 5) > 0) {
//                guide.guide_Image = [NSData dataWithBytes:sqlite3_column_blob
//                                     (self.sqlStament, 5) length:sqlite3_column_bytes(self.sqlStament, 5)];
//            } 
//            if (sqlite3_column_text(self.sqlStament, 6)) {
//                guide.guide_LastUpdated = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 6)];
//            }
//        }
//    }
//    
//    return guide;
//}

-(GuideModelV2 *)loadGuideDataByRoomTypeId:(GuideModelV2 *)guide {
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ?", guide_id, guide_room_type_id, guide_name, guide_name_lang, guide_image, guide_last_updated, guide_category_id, GUIDELINES, guide_room_type_id];
    
    const char *sql =[sqlString UTF8String];
    
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {        
        sqlite3_bind_int(self.sqlStament, 1, guide.guide_RoomTypeId);
        
        while(sqlite3_step(sqlStament) == SQLITE_ROW) {
            guide.guide_Id = sqlite3_column_int(self.sqlStament, 0);
            guide.guide_RoomTypeId = sqlite3_column_int(self.sqlStament, 1);
            if (sqlite3_column_text(self.sqlStament, 2)) {
                guide.guide_Name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
            }
            if (sqlite3_column_text(self.sqlStament, 3)) {
                guide.guide_NameLang = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
            }
            if (sqlite3_column_bytes(self.sqlStament, 4) > 0) {
                guide.guide_Image = [NSData dataWithBytes:sqlite3_column_blob
                                     (self.sqlStament, 4) length:sqlite3_column_bytes(self.sqlStament, 4)];
            } 
            if (sqlite3_column_text(self.sqlStament, 5)) {
                guide.guide_LastUpdated = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
            }
            
            guide.guide_CategoryId = sqlite3_column_int(self.sqlStament, 6);
        }
    }
    
    return guide;
}

-(NSMutableArray *)loadAllGuide {
    NSMutableArray *array = [NSMutableArray array];
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@ FROM %@", guide_id, guide_room_type_id, guide_name, guide_name_lang, guide_image, guide_last_updated, guide_category_id, GUIDELINES];
    
    const char *sql =[sqlString UTF8String];
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {
        
        while(sqlite3_step(sqlStament) == SQLITE_ROW) {
            GuideModelV2 *guide = [[GuideModelV2 alloc] init];
            guide.guide_Id = sqlite3_column_int(self.sqlStament, 0);
            guide.guide_RoomTypeId = sqlite3_column_int(self.sqlStament, 1);
            if (sqlite3_column_text(self.sqlStament, 2)) {
                guide.guide_Name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
            }
            if (sqlite3_column_text(self.sqlStament, 3)) {
                guide.guide_NameLang = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
            }
            if (sqlite3_column_bytes(self.sqlStament, 4) > 0) {
                guide.guide_Image = [NSData dataWithBytes:sqlite3_column_blob
                                     (self.sqlStament, 4) length:sqlite3_column_bytes(self.sqlStament, 4)];
            } 
            if (sqlite3_column_text(self.sqlStament, 5)) {
                guide.guide_LastUpdated = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
            }
            
            guide.guide_CategoryId = sqlite3_column_int(self.sqlStament, 6);
            
            [array addObject:guide];
            
        }
    }    
    
    return array;
}

//-(NSMutableArray *)loadAllGuideByRoomSectionId:(int)roomSectionId {
//    NSMutableArray *array = [NSMutableArray array];
//    
//    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ?", guide_id, guide_room_section_id, guide_room_type_id, guide_name, guide_name_lang, guide_image, guide_last_updated, GUIDELINES, guide_room_section_id];
//    
//    const char *sql =[sqlString UTF8String];
//    
//    
//    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {
//        sqlite3_bind_int(self.sqlStament, 1, roomSectionId);
//        
//        while(sqlite3_step(sqlStament) == SQLITE_ROW) {
//            GuideModelV2 *guide = [[GuideModelV2 alloc] init];
//            guide.guide_Id = sqlite3_column_int(self.sqlStament, 0);
//            guide.guide_RoomSectionId = sqlite3_column_int(self.sqlStament, 1);
//            guide.guide_RoomTypeId = sqlite3_column_int(self.sqlStament, 2);
//            if (sqlite3_column_text(self.sqlStament, 3)) {
//                guide.guide_Name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
//            }
//            if (sqlite3_column_text(self.sqlStament, 4)) {
//                guide.guide_NameLang = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
//            }
//            if (sqlite3_column_bytes(self.sqlStament, 5) > 0) {
//                guide.guide_Image = [NSData dataWithBytes:sqlite3_column_blob
//                                     (self.sqlStament, 5) length:sqlite3_column_bytes(self.sqlStament, 5)];
//            } 
//            if (sqlite3_column_text(self.sqlStament, 6)) {
//                guide.guide_LastUpdated = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 6)];
//            }
//            
//            [array addObject:guide];
//            
//        }
//    }    
//    
//    return array;
//}

-(NSMutableArray *)loadAllGuideByRoomTypeId:(int)roomTypeId {
    NSMutableArray *array = [NSMutableArray array];
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT a.%@, a.%@, a.%@, a.%@, a.%@, a.%@, a.%@ FROM %@ a",
                           
                           //Select a
                           guide_id,
                           guide_room_type_id,
                           guide_name,
                           guide_name_lang,
                           guide_image,
                           guide_last_updated,
                           guide_category_id,
                           
                           //From a, b
                           GUIDELINES];
    
    const char *sql = [sqlString UTF8String];
    
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {
        
        while(sqlite3_step(sqlStament) == SQLITE_ROW) {
            GuideModelV2 *guide = [[GuideModelV2 alloc] init];
            guide.guide_Id = sqlite3_column_int(self.sqlStament, 0);
            guide.guide_RoomTypeId = sqlite3_column_int(self.sqlStament, 1);
            if (sqlite3_column_text(self.sqlStament, 2)) {
                guide.guide_Name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
            }
            if (sqlite3_column_text(self.sqlStament, 3)) {
                guide.guide_NameLang = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
            }
            if (sqlite3_column_bytes(self.sqlStament, 4) > 0) {
                guide.guide_Image = [NSData dataWithBytes:sqlite3_column_blob
                                     (self.sqlStament, 4) length:sqlite3_column_bytes(self.sqlStament, 4)];
            } 
            if (sqlite3_column_text(self.sqlStament, 5)) {
                guide.guide_LastUpdated = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
            }
            
            guide.guide_CategoryId = sqlite3_column_int(self.sqlStament, 6);
            
            [array addObject:guide];
            
        }
    }    
    
    return array;
}

-(BOOL)haveGuideItemsByRoomType:(NSInteger)roomTypeId {
    
    BOOL result = NO;
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT count(a.%@) FROM %@ a, %@ b WHERE a.%@ = b.%@ AND a.%@ = ?",
                           //Select
                           guide_id,
                           
                           //From
                           GUIDELINES,
                           GUIDE_ITEMS,
                           
                           //where
                           guide_id,
                           guide_id,
                           
                           //and roomTypeId
                           guide_room_type_id];
    
    const char *sql = [sqlString UTF8String];
    
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {
        sqlite3_bind_int(self.sqlStament, 1, (int)roomTypeId);
        
        while(sqlite3_step(sqlStament) == SQLITE_ROW) {
            int countRows = sqlite3_column_int(self.sqlStament, 0);
            if(countRows > 0) {
                result = YES;
            }
        }
    }
    
    return result;
}

-(NSMutableArray *)loadAllGuideHaveGuideItems {
    NSMutableArray *array = [NSMutableArray array];
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT a.%@, a.%@, a.%@, a.%@, a.%@, a.%@ FROM %@ a, %@ b WHERE a.%@ = b.%@ GROUP BY a.%@",
                           //Select
                           guide_id,
                           guide_room_type_id,
                           guide_name,
                           guide_name_lang,
                           guide_image,
                           guide_last_updated,
                           
                           //From
                           GUIDELINES,
                           GUIDE_ITEMS,
                           
                           //where
                           guide_id,
                           guide_id,
                           
                           //group by
                           guide_id];
    
    const char *sql = [sqlString UTF8String];
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {
        
        while(sqlite3_step(sqlStament) == SQLITE_ROW) {
            GuideModelV2 *guide = [[GuideModelV2 alloc] init];
            guide.guide_Id = sqlite3_column_int(self.sqlStament, 0);
            guide.guide_RoomTypeId = sqlite3_column_int(self.sqlStament, 1);
            if (sqlite3_column_text(self.sqlStament, 2)) {
                guide.guide_Name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
            }
            if (sqlite3_column_text(self.sqlStament, 3)) {
                guide.guide_NameLang = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
            }
            if (sqlite3_column_bytes(self.sqlStament, 4) > 0) {
                guide.guide_Image = [NSData dataWithBytes:sqlite3_column_blob
                                     (self.sqlStament, 4) length:sqlite3_column_bytes(self.sqlStament, 4)];
            }
            if (sqlite3_column_text(self.sqlStament, 5)) {
                guide.guide_LastUpdated = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
            }
            
            [array addObject:guide];
            
        }
    }
    
    return array;
}

//get latest last modified of guide table
-(NSString *)getLatestLastModifiedGuide {
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@ FROM %@ ORDER BY %@ DESC LIMIT 1", guide_last_updated, GUIDELINES, guide_last_updated];
    
    const char *sql =[sqlString UTF8String];
    
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {
        
        while(sqlite3_step(sqlStament) == SQLITE_ROW) {
            
            if (sqlite3_column_text(sqlStament, 0)) {
                return [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 0)];
            }
            
        }
    }    
    
    return @"";
}

@end
