//
//  OtherActivityManager.h
//  eHouseKeeping
//
//  Created by DungPhan on 10/16/2015.
//

#import <Foundation/Foundation.h>
#import "MBProgressHUD.h"
#import "OtherActivityManager.h"

@interface OtherActivityManagerDemo : OtherActivityManager

//+ (OtherActivityManager*)sharedOtherActivityManager;
//+ (OtherActivityManager*)updateOtherActivityManagerInstance;

#pragma mark - DB FUNCTIONS
#pragma mark - Other Activity
- (int)insertOtherActivity:(OtherActivityModel*)model;
- (int)updateOtherActivity:(OtherActivityModel*)model;
- (int)deleteAllOtherActivities;
- (OtherActivityModel*)loadOtherActivityById:(int)otherActivityId;

#pragma mark - Other Activity Status
- (int)insertOtherActivityStatus:(OtherActivityStatusModel*)model;
- (int)deleteAllOtherActivityStatus;

#pragma mark - Other Activity Location
- (int)insertOtherActivityLocation:(OtherActivityLocationModel*)model;
- (int)deleteAllOtherActivityLocations;

#pragma mark - Other Activity Assignment
- (int)insertOtherActivityAssignment:(OtherActivityAssignmentModel*)model;
- (int)updateOtherActivityAssignment:(OtherActivityAssignmentModel*)model;
- (int)deleteAllOtherActivityAssignments;
- (int)deleteOtherActivityAssignmentByUserId:(int)userId;
- (int)deleteOtherActivityAssignmentByUserId:(int)userId ondayOtherActivityId:(int)ondayOtherActivityId;
- (NSMutableArray*)loadOtherActivityAssignmentsByUserId:(int)userId;
- (OtherActivityAssignmentModel*)loadOtherActivityAssignmentByUserId:(int)userId ondayOtherActivityId:(int)ondayOtherActivityId;

#pragma mark - Other Activity Record
- (int)insertOtherActivityRecord:(OtherActivityRecordModel*)model;
- (int)updateOtherActivityRecord:(OtherActivityRecordModel*)model;
- (int)deleteAllOtherActivityRecords;
- (int)deleteOtherActivityRecordByUserId:(int)userId;
- (int)deleteOtherActivityRecordByUserId:(int)userId ondayOtherActivityId:(int)ondayOtherActivityId;
- (NSMutableArray*)loadOtherActivityRecordsByUserId:(int)userId postStatus:(int)postStatus;
- (int)countOtherActivityRecordsByUserId:(int)userId postStatus:(int)postStatus;

#pragma mark - WS FUNCTIONS
// GET FUNCTIONS
- (bool)loadWSOtherActivitiesStatusByUserId:(int)userId lastModified:(NSString*)lastModified;
- (bool)loadWSOtherActivitiesLocationByUserId:(int)userId hotelId:(int)hotelId lastModified:(NSString*)lastModified;
- (bool)loadWSOtherActivityAssignmentByUserId:(int)userId hotelId:(int)hotelId buildingId:(int)buildingId dateTime:(NSString*)dateTime;
// POST FUNCTIONS
- (int)postAllOtherActivityByUserId:(int)userId;
- (bool)postOtherActivityStatusByUserId:(int)userId ondayOtherActivityId:(int)ondayOtherActivityId otherActivityStatus:(int)otherActivityStatus time:(NSString*)time remark:(NSString*)remark durationSpent:(int)durationSpent;

@end
