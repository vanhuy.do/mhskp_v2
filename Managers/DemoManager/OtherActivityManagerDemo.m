//
//  OtherActivityManager.m
//  eHouseKeeping
//
//  Created by DungPhan on 10/16/2011.
//

#import "OtherActivityManagerDemo.h"
#import "ehkConvert.h"
#import "ServiceDefines.h"
#import "LogFileManager.h"
#import "LogObject.h"
#import "eHousekeepingService.h"
#import "TasksManagerV2.h"
#import "DbDefinesV2.h"
#import "DataBaseExtension.h"
#import "NetworkCheck.h"
#import "DeviceManager.h"
#import "ServiceDefines.h"
#import "PopupMessageItemModel.h"


@implementation OtherActivityManagerDemo

#pragma mark - DB FUNCTIONS
#pragma mark - Other Activity
- (int)insertOtherActivity:(OtherActivityModel *)model {
    
    if (model == nil) {
        return 0;
    }
    
    DataRow *rowInsert = [[DataRow alloc] init];
    rowInsert.tableName = [NSString stringWithFormat:@"%@", OTHER_ACTIVITY];
    
    [rowInsert addColumnIntValue:model.oa_id formatName:@"%@", oa_id];
    [rowInsert addColumnStringValue:model.oa_name formatName:@"%@", oa_name];
    [rowInsert addColumnStringValue:model.oa_name_lang formatName:@"%@", oa_name_lang];
    [rowInsert addColumnStringValue:model.oa_code formatName:@"%@", oa_code];
    [rowInsert addColumnStringValue:model.oa_desc formatName:@"%@", oa_desc];
    [rowInsert addColumnStringValue:model.oa_desc_lang formatName:@"%@", oa_desc_lang];
    [rowInsert addColumnStringValue:model.oa_last_modified formatName:@"%@", oa_last_modified];
    
    return [rowInsert insertDBRow];
}

- (int)updateOtherActivity:(OtherActivityModel *)model {
    
    if (model == nil) {
        return 0;
    }
    
    DataRow *rowUpdate = [[DataRow alloc] init];
    rowUpdate.tableName = [NSString stringWithFormat:@"%@", OTHER_ACTIVITY];
    
    DataColumn *columnUpdate1 = [rowUpdate addColumnInt:[NSString stringWithFormat:@"%@", oa_id] value:model.oa_id];
    columnUpdate1.isUpdateCondition = YES;
    
    [rowUpdate addColumnString:[NSString stringWithFormat:@"%@", oa_name] value:model.oa_name];
    [rowUpdate addColumnString:[NSString stringWithFormat:@"%@", oa_name_lang] value:model.oa_name_lang];
    [rowUpdate addColumnString:[NSString stringWithFormat:@"%@", oa_code] value:model.oa_code];
    [rowUpdate addColumnString:[NSString stringWithFormat:@"%@", oa_desc] value:model.oa_desc];
    [rowUpdate addColumnString:[NSString stringWithFormat:@"%@", oa_desc_lang] value:model.oa_desc_lang];
    [rowUpdate addColumnString:[NSString stringWithFormat:@"%@", oa_last_modified] value:model.oa_last_modified];
    
    return [rowUpdate commitChangeDBRow];
}

- (int)deleteAllOtherActivities {
    
    DataTable *tableDelete = [[DataTable alloc] initWithNameFormat:@"%@", OTHER_ACTIVITY];
    return [tableDelete dropDBRows];
}

- (OtherActivityModel*)loadOtherActivityById:(int)otherActivityId {
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ = %d",
                                  // SELECT
                                  oa_id,
                                  oa_name,
                                  oa_name_lang,
                                  oa_code,
                                  oa_desc,
                                  oa_desc_lang,
                                  oa_last_modified,
                                  
                                  // FROM
                                  OTHER_ACTIVITY,
                                  
                                  // WHERE
                                  oa_id,
                                  otherActivityId
                                  ];
    
    DataTable *table = [[DataTable alloc] init];
    
    //Row result reference
    DataRow *rowReference = [[DataRow alloc] init];
    [rowReference addColumnFormatInt:@"%@", oa_id];
    [rowReference addColumnFormatString:@"%@", oa_name];
    [rowReference addColumnFormatString:@"%@", oa_name_lang];
    [rowReference addColumnFormatString:@"%@", oa_code];
    [rowReference addColumnFormatString:@"%@", oa_desc];
    [rowReference addColumnFormatString:@"%@", oa_desc_lang];
    [rowReference addColumnFormatString:@"%@", oa_last_modified];
    
    [table loadTableDataWithQuery:sqlString referenceRow:rowReference];
    
    OtherActivityModel *model = nil;
    
    int rowCount = [table countRows];
    if (rowCount > 0) {
        model = [[OtherActivityModel alloc] init];
        DataRow *row = [table rowWithIndex:0];
        DataColumn *curColumn = nil;
        
        curColumn = [row columnWithNameFormat:@"%@", oa_id];
        model.oa_id = [curColumn fieldValueInt];
        
        curColumn = [row columnWithNameFormat:@"%@", oa_name];
        model.oa_name = [curColumn fieldValueString];
        
        curColumn = [row columnWithNameFormat:@"%@", oa_name_lang];
        model.oa_name_lang = [curColumn fieldValueString];
        
        curColumn = [row columnWithNameFormat:@"%@", oa_code];
        model.oa_code = [curColumn fieldValueString];
        
        curColumn = [row columnWithNameFormat:@"%@", oa_desc];
        model.oa_desc = [curColumn fieldValueString];
        
        curColumn = [row columnWithNameFormat:@"%@", oa_desc_lang];
        model.oa_desc_lang = [curColumn fieldValueString];
        
        curColumn = [row columnWithNameFormat:@"%@", oa_last_modified];
        model.oa_last_modified = [curColumn fieldValueString];
    }
    return model;
}

#pragma mark - Other Activity Status

- (int)insertOtherActivityStatus:(OtherActivityStatusModel *)model {
    
    if (model == nil) {
        return 0;
    }
    
    DataRow *rowInsert = [[DataRow alloc] init];
    rowInsert.tableName = [NSString stringWithFormat:@"%@", OTHER_ACTIVITY_STATUS];
    
    [rowInsert addColumnIntValue:model.oas_id formatName:@"%@", oas_id];
    [rowInsert addColumnStringValue:model.oas_name formatName:@"%@", oas_name];
    [rowInsert addColumnStringValue:model.oas_lang formatName:@"%@", oas_lang];
    [rowInsert addColumnDataValue:model.oas_image formatName:@"%@", oas_image];
    [rowInsert addColumnStringValue:model.oas_last_modified formatName:@"%@", oas_last_modified];
    
    return [rowInsert insertDBRow];
}

- (int)deleteAllOtherActivityStatus {
    
    DataTable *tableDelete = [[DataTable alloc] initWithNameFormat:@"%@", OTHER_ACTIVITY_STATUS];
    return [tableDelete dropDBRows];
}

#pragma mark - Other Activity Location

- (int)insertOtherActivityLocation:(OtherActivityLocationModel *)model {
    
    if (model == nil) {
        return 0;
    }
    
    DataRow *rowInsert = [[DataRow alloc] init];
    rowInsert.tableName = [NSString stringWithFormat:@"%@", OTHER_ACTIVITY_LOCATION];
    
    [rowInsert addColumnIntValue:model.oal_id formatName:@"%@", oal_id];
    [rowInsert addColumnStringValue:model.oal_name formatName:@"%@", oal_name];
    [rowInsert addColumnStringValue:model.oal_name_lang formatName:@"%@", oal_name_lang];
    [rowInsert addColumnStringValue:model.oal_code formatName:@"%@", oal_code];
    [rowInsert addColumnStringValue:model.oal_desc formatName:@"%@", oal_desc];
    [rowInsert addColumnStringValue:model.oal_desc_lang formatName:@"%@", oal_desc_lang];
    [rowInsert addColumnIntValue:model.oal_hotel_id formatName:@"%@", oal_hotel_id];
    [rowInsert addColumnStringValue:model.oal_last_modified formatName:@"%@", oal_last_modified];
    
    return [rowInsert insertDBRow];
}

- (int)deleteAllOtherActivityLocations {
    
    DataTable *tableDelete = [[DataTable alloc] initWithNameFormat:@"%@", OTHER_ACTIVITY_LOCATION];
    return [tableDelete dropDBRows];
}

#pragma mark - Other Activity Assignment

- (int)insertOtherActivityAssignment:(OtherActivityAssignmentModel *)model {
    
    if (model == nil) {
        return 0;
    }
    
    DataRow *rowInsert = [[DataRow alloc] init];
    rowInsert.tableName = [NSString stringWithFormat:@"%@", OTHER_ACTIVITY_ASSIGNMENT];
    
    [rowInsert addColumnIntValue:model.oaa_id formatName:@"%@", oaa_id];
    [rowInsert addColumnIntValue:model.oaa_activity_id formatName:@"%@", oaa_activity_id];
    [rowInsert addColumnIntValue:model.oaa_activities_id formatName:@"%@", oaa_activities_id];
    [rowInsert addColumnIntValue:model.oaa_location_id formatName:@"%@", oaa_location_id];
    [rowInsert addColumnIntValue:model.oaa_status_id formatName:@"%@", oaa_status_id];
    [rowInsert addColumnStringValue:model.oaa_remark formatName:@"%@", oaa_remark];
    [rowInsert addColumnIntValue:model.oaa_duration formatName:@"%@", oaa_duration];
    [rowInsert addColumnIntValue:model.oaa_reminder formatName:@"%@", oaa_reminder];
    [rowInsert addColumnIntValue:model.oaa_remind formatName:@"%@", oaa_remind];
    [rowInsert addColumnStringValue:model.oaa_assign_date formatName:@"%@", oaa_assign_date];
    [rowInsert addColumnStringValue:model.oaa_start_time formatName:@"%@", oaa_start_time];
    [rowInsert addColumnStringValue:model.oaa_end_time formatName:@"%@", oaa_end_time];
    [rowInsert addColumnStringValue:model.oaa_user_start_time formatName:@"%@", oaa_user_start_time];
    [rowInsert addColumnStringValue:model.oaa_user_end_time formatName:@"%@", oaa_user_end_time];
    [rowInsert addColumnStringValue:model.oaa_user_stop_time formatName:@"%@", oaa_user_stop_time];
    [rowInsert addColumnStringValue:model.oaa_decline_service_time formatName:@"%@", oaa_decline_service_time];
    [rowInsert addColumnIntValue:model.oaa_duration_spent formatName:@"%@", oaa_duration_spent];
    [rowInsert addColumnIntValue:model.oaa_local_duration formatName:@"%@", oaa_local_duration];
    [rowInsert addColumnIntValue:model.oaa_user_id formatName:@"%@", oaa_user_id];
    [rowInsert addColumnIntValue:model.oaa_is_single formatName:@"%@", oaa_is_single];
    [rowInsert addColumnIntValue:model.oaa_deleted formatName:@"%@", oaa_deleted];
    [rowInsert addColumnStringValue:model.oaa_last_modified formatName:@"%@", oaa_last_modified];
    
    return [rowInsert insertDBRow];
}

- (int)updateOtherActivityAssignment:(OtherActivityAssignmentModel *)model {
    
    if (model == nil) {
        return 0;
    }
    
    DataRow *rowUpdate = [[DataRow alloc] init];
    rowUpdate.tableName = [NSString stringWithFormat:@"%@", OTHER_ACTIVITY_ASSIGNMENT];
    
    DataColumn *columnUpdate1 = [rowUpdate addColumnInt:[NSString stringWithFormat:@"%@", oaa_id] value:model.oaa_id];
    columnUpdate1.isUpdateCondition = YES;
    
    [rowUpdate addColumnInt:[NSString stringWithFormat:@"%@", oaa_activity_id] value:model.oaa_activity_id];
    [rowUpdate addColumnInt:[NSString stringWithFormat:@"%@", oaa_activities_id] value:model.oaa_activities_id];
    [rowUpdate addColumnInt:[NSString stringWithFormat:@"%@", oaa_location_id] value:model.oaa_location_id];
    [rowUpdate addColumnInt:[NSString stringWithFormat:@"%@", oaa_status_id] value:model.oaa_status_id];
    [rowUpdate addColumnString:[NSString stringWithFormat:@"%@", oaa_remark] value:model.oaa_remark];
    [rowUpdate addColumnInt:[NSString stringWithFormat:@"%@", oaa_duration] value:model.oaa_duration];
    [rowUpdate addColumnInt:[NSString stringWithFormat:@"%@", oaa_reminder] value:model.oaa_reminder];
    [rowUpdate addColumnInt:[NSString stringWithFormat:@"%@", oaa_remind] value:model.oaa_remind];
    [rowUpdate addColumnString:[NSString stringWithFormat:@"%@", oaa_assign_date] value:model.oaa_assign_date];
    [rowUpdate addColumnString:[NSString stringWithFormat:@"%@", oaa_start_time] value:model.oaa_start_time];
    [rowUpdate addColumnString:[NSString stringWithFormat:@"%@", oaa_end_time] value:model.oaa_end_time];
    [rowUpdate addColumnString:[NSString stringWithFormat:@"%@", oaa_user_start_time] value:model.oaa_user_start_time];
    [rowUpdate addColumnString:[NSString stringWithFormat:@"%@", oaa_user_end_time] value:model.oaa_user_end_time];
    [rowUpdate addColumnString:[NSString stringWithFormat:@"%@", oaa_user_stop_time] value:model.oaa_user_stop_time];
    [rowUpdate addColumnString:[NSString stringWithFormat:@"%@", oaa_decline_service_time] value:model.oaa_decline_service_time];
    [rowUpdate addColumnInt:[NSString stringWithFormat:@"%@", oaa_duration_spent] value:model.oaa_duration_spent];
    [rowUpdate addColumnInt:[NSString stringWithFormat:@"%@", oaa_local_duration] value:model.oaa_local_duration];
    [rowUpdate addColumnInt:[NSString stringWithFormat:@"%@", oaa_user_id] value:model.oaa_user_id];
    [rowUpdate addColumnInt:[NSString stringWithFormat:@"%@", oaa_is_single] value:model.oaa_is_single];
    [rowUpdate addColumnInt:[NSString stringWithFormat:@"%@", oaa_deleted] value:model.oaa_deleted];
    [rowUpdate addColumnString:[NSString stringWithFormat:@"%@", oaa_last_modified] value:model.oaa_last_modified];
    
    return [rowUpdate commitChangeDBRow];
}

- (int)deleteAllOtherActivityAssignments {
    
    DataTable *tableDelete = [[DataTable alloc] initWithNameFormat:@"%@", OTHER_ACTIVITY_ASSIGNMENT];
    return [tableDelete dropDBRows];
}

- (int)deleteOtherActivityAssignmentByUserId:(int)userId {
    DataTable *tableDelete = [[DataTable alloc] initWithNameFormat:@"%@", OTHER_ACTIVITY_ASSIGNMENT];
    
    NSString *queryDelete = [NSString stringWithFormat:@"DELETE FROM %@ WHERE %@ = %d",
                             // DELETE FROM
                             OTHER_ACTIVITY_ASSIGNMENT,
                             // WHERE
                             oaa_user_id,
                             userId];
    int status = (int)[tableDelete excuteQueryNonSelect:queryDelete];
    
    return status;
}

- (int)deleteOtherActivityAssignmentByUserId:(int)userId ondayOtherActivityId:(int)ondayOtherActivityId {
    DataTable *tableDelete = [[DataTable alloc] initWithNameFormat:@"%@", OTHER_ACTIVITY_ASSIGNMENT];
    
    NSString *queryDelete = [NSString stringWithFormat:@"DELETE FROM %@ WHERE %@ = %d AND %@ = %d",
                             // DELETE FROM
                             OTHER_ACTIVITY_ASSIGNMENT,
                             // WHERE
                             oaa_user_id,
                             userId,
                             oar_onday_activity_id,
                             ondayOtherActivityId];
    int status = (int)[tableDelete excuteQueryNonSelect:queryDelete];
    
    return status;
}

- (NSMutableArray*)loadOtherActivityAssignmentsByUserId:(int)userId {
    
    if (userId <= 0) {
        return nil;
    }
    
    NSString *currentDate = nil;
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    [f setDateFormat:@"yyyy-MM-dd"];
    currentDate = [NSString stringWithFormat:@"%@ 00:00:00", [f stringFromDate:[NSDate date]]];
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ = %d AND %@ = %d AND %@ <> %d AND %@ >= '%@'",
                                  // SELECT
                                  oaa_id,
                                  oaa_activity_id,
                                  oaa_activities_id,
                                  oaa_location_id,
                                  oaa_status_id,
                                  oaa_remark,
                                  oaa_duration,
                                  oaa_reminder,
                                  oaa_remind,
                                  oaa_assign_date,
                                  oaa_start_time,
                                  oaa_end_time,
                                  oaa_user_start_time,
                                  oaa_user_end_time,
                                  oaa_user_stop_time,
                                  oaa_decline_service_time,
                                  oaa_duration_spent,
                                  oaa_local_duration,
                                  oaa_user_id,
                                  oaa_is_single,
                                  oaa_deleted,
                                  oaa_last_modified,
                                  // FROM
                                  OTHER_ACTIVITY_ASSIGNMENT,
                                  // WHERE
                                  oaa_user_id,
                                  userId,
                                  oaa_deleted,
                                  0,
                                  oaa_status_id,
                                  OtherActivityStatus_Complete,
                                  oaa_assign_date,
                                  currentDate
                                  ];
    
    DataTable *table = [[DataTable alloc] init];
    
    //Row result reference
    DataRow *rowReference = [[DataRow alloc] init];
    [rowReference addColumnFormatInt:@"%@", oaa_id];
    [rowReference addColumnFormatInt:@"%@", oaa_activity_id];
    [rowReference addColumnFormatInt:@"%@", oaa_activities_id];
    [rowReference addColumnFormatInt:@"%@", oaa_location_id];
    [rowReference addColumnFormatInt:@"%@", oaa_status_id];
    [rowReference addColumnFormatString:@"%@", oaa_remark];
    [rowReference addColumnFormatInt:@"%@", oaa_duration];
    [rowReference addColumnFormatInt:@"%@", oaa_reminder];
    [rowReference addColumnFormatInt:@"%@", oaa_remind];
    [rowReference addColumnFormatString:@"%@", oaa_assign_date];
    [rowReference addColumnFormatString:@"%@", oaa_start_time];
    [rowReference addColumnFormatString:@"%@", oaa_end_time];
    [rowReference addColumnFormatString:@"%@", oaa_user_start_time];
    [rowReference addColumnFormatString:@"%@", oaa_user_end_time];
    [rowReference addColumnFormatString:@"%@", oaa_user_stop_time];
    [rowReference addColumnFormatString:@"%@", oaa_decline_service_time];
    [rowReference addColumnFormatInt:@"%@", oaa_duration_spent];
    [rowReference addColumnFormatInt:@"%@", oaa_local_duration];
    [rowReference addColumnFormatInt:@"%@", oaa_user_id];
    [rowReference addColumnFormatInt:@"%@", oaa_is_single];
    [rowReference addColumnFormatInt:@"%@", oaa_deleted];
    [rowReference addColumnFormatString:@"%@", oaa_last_modified];
    
    [table loadTableDataWithQuery:sqlString referenceRow:rowReference];
    
    int rowCount = [table countRows];
    NSMutableArray *results = nil;
    if(rowCount > 0)
    {
        results = [NSMutableArray array];
        for (int i = 0; i < rowCount; i ++) {
            
            OtherActivityAssignmentModel *model = [[OtherActivityAssignmentModel alloc] init];
            DataRow *row = [table rowWithIndex:i];
            DataColumn *curColumn = nil;
            
            curColumn = [row columnWithNameFormat:@"%@", oaa_id];
            model.oaa_id = [curColumn fieldValueInt];
            
            curColumn = [row columnWithNameFormat:@"%@", oaa_activity_id];
            model.oaa_activity_id = [curColumn fieldValueInt];
            
            curColumn = [row columnWithNameFormat:@"%@", oaa_activities_id];
            model.oaa_activities_id = [curColumn fieldValueInt];
            
            curColumn = [row columnWithNameFormat:@"%@", oaa_location_id];
            model.oaa_location_id = [curColumn fieldValueInt];
            
            curColumn = [row columnWithNameFormat:@"%@", oaa_status_id];
            model.oaa_status_id = [curColumn fieldValueInt];
            
            curColumn = [row columnWithNameFormat:@"%@", oaa_remark];
            model.oaa_remark = [curColumn fieldValueString];
            
            curColumn = [row columnWithNameFormat:@"%@", oaa_duration];
            model.oaa_duration = [curColumn fieldValueInt];
            
            curColumn = [row columnWithNameFormat:@"%@", oaa_reminder];
            model.oaa_reminder = [curColumn fieldValueInt];
            
            curColumn = [row columnWithNameFormat:@"%@", oaa_remind];
            model.oaa_remind = [curColumn fieldValueInt];
            
            curColumn = [row columnWithNameFormat:@"%@", oaa_assign_date];
            model.oaa_assign_date = [curColumn fieldValueString];
            
            curColumn = [row columnWithNameFormat:@"%@", oaa_start_time];
            model.oaa_start_time = [curColumn fieldValueString];
            
            curColumn = [row columnWithNameFormat:@"%@", oaa_end_time];
            model.oaa_end_time = [curColumn fieldValueString];
            
            curColumn = [row columnWithNameFormat:@"%@", oaa_user_start_time];
            model.oaa_user_start_time = [curColumn fieldValueString];
            
            curColumn = [row columnWithNameFormat:@"%@", oaa_user_end_time];
            model.oaa_user_end_time = [curColumn fieldValueString];
            
            curColumn = [row columnWithNameFormat:@"%@", oaa_user_stop_time];
            model.oaa_user_stop_time = [curColumn fieldValueString];
            
            curColumn = [row columnWithNameFormat:@"%@", oaa_decline_service_time];
            model.oaa_decline_service_time = [curColumn fieldValueString];
            
            curColumn = [row columnWithNameFormat:@"%@", oaa_duration_spent];
            model.oaa_duration_spent = [curColumn fieldValueInt];
            
            curColumn = [row columnWithNameFormat:@"%@", oaa_local_duration];
            model.oaa_local_duration = [curColumn fieldValueInt];
            
            curColumn = [row columnWithNameFormat:@"%@", oaa_user_id];
            model.oaa_user_id = [curColumn fieldValueInt];
            
            curColumn = [row columnWithNameFormat:@"%@", oaa_is_single];
            model.oaa_is_single = [curColumn fieldValueInt];
            
            curColumn = [row columnWithNameFormat:@"%@", oaa_deleted];
            model.oaa_deleted = [curColumn fieldValueInt];
            
            curColumn = [row columnWithNameFormat:@"%@", oaa_last_modified];
            model.oaa_last_modified = [curColumn fieldValueString];
            
            [results addObject:model];
        }
    }
    return results;
}

- (OtherActivityAssignmentModel*)loadOtherActivityAssignmentByUserId:(int)userId ondayOtherActivityId:(int)ondayOtherActivityId {
    
    if (userId <= 0) {
        return nil;
    }
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ = %d AND %@ = %d",
                                  // SELECT
                                  oaa_id,
                                  oaa_activity_id,
                                  oaa_activities_id,
                                  oaa_location_id,
                                  oaa_status_id,
                                  oaa_remark,
                                  oaa_duration,
                                  oaa_reminder,
                                  oaa_remind,
                                  oaa_assign_date,
                                  oaa_start_time,
                                  oaa_end_time,
                                  oaa_user_start_time,
                                  oaa_user_end_time,
                                  oaa_user_stop_time,
                                  oaa_decline_service_time,
                                  oaa_duration_spent,
                                  oaa_local_duration,
                                  oaa_user_id,
                                  oaa_is_single,
                                  oaa_deleted,
                                  oaa_last_modified,
                                  // FROM
                                  OTHER_ACTIVITY_ASSIGNMENT,
                                  
                                  // WHERE
                                  oaa_user_id,
                                  userId,
                                  oaa_id,
                                  ondayOtherActivityId
                                  ];
    
    DataTable *table = [[DataTable alloc] init];
    
    //Row result reference
    DataRow *rowReference = [[DataRow alloc] init];
    [rowReference addColumnFormatInt:@"%@", oaa_id];
    [rowReference addColumnFormatString:@"%@", oaa_activity_id];
    [rowReference addColumnFormatString:@"%@", oaa_activities_id];
    [rowReference addColumnFormatString:@"%@", oaa_location_id];
    [rowReference addColumnFormatString:@"%@", oaa_status_id];
    [rowReference addColumnFormatString:@"%@", oaa_remark];
    [rowReference addColumnFormatString:@"%@", oaa_duration];
    [rowReference addColumnFormatString:@"%@", oaa_reminder];
    [rowReference addColumnFormatString:@"%@", oaa_remind];
    [rowReference addColumnFormatString:@"%@", oaa_assign_date];
    [rowReference addColumnFormatString:@"%@", oaa_start_time];
    [rowReference addColumnFormatString:@"%@", oaa_end_time];
    [rowReference addColumnFormatString:@"%@", oaa_user_start_time];
    [rowReference addColumnFormatString:@"%@", oaa_user_end_time];
    [rowReference addColumnFormatString:@"%@", oaa_user_stop_time];
    [rowReference addColumnFormatString:@"%@", oaa_decline_service_time];
    [rowReference addColumnFormatString:@"%@", oaa_duration_spent];
    [rowReference addColumnFormatString:@"%@", oaa_local_duration];
    [rowReference addColumnFormatString:@"%@", oaa_user_id];
    [rowReference addColumnFormatString:@"%@", oaa_is_single];
    [rowReference addColumnFormatString:@"%@", oaa_deleted];
    [rowReference addColumnFormatString:@"%@", oaa_last_modified];
    
    [table loadTableDataWithQuery:sqlString referenceRow:rowReference];
    
    OtherActivityAssignmentModel *model = nil;
    
    int rowCount = [table countRows];
    if (rowCount > 0) {
        model = [[OtherActivityAssignmentModel alloc] init];
        DataRow *row = [table rowWithIndex:0];
        DataColumn *curColumn = nil;
        
        curColumn = [row columnWithNameFormat:@"%@", oaa_id];
        model.oaa_id = [curColumn fieldValueInt];
        
        curColumn = [row columnWithNameFormat:@"%@", oaa_activity_id];
        model.oaa_activity_id = [curColumn fieldValueInt];
        
        curColumn = [row columnWithNameFormat:@"%@", oaa_activities_id];
        model.oaa_activities_id = [curColumn fieldValueInt];
        
        curColumn = [row columnWithNameFormat:@"%@", oaa_location_id];
        model.oaa_location_id = [curColumn fieldValueInt];
        
        curColumn = [row columnWithNameFormat:@"%@", oaa_status_id];
        model.oaa_status_id = [curColumn fieldValueInt];
        
        curColumn = [row columnWithNameFormat:@"%@", oaa_remark];
        model.oaa_remark = [curColumn fieldValueString];
        
        curColumn = [row columnWithNameFormat:@"%@", oaa_duration];
        model.oaa_duration = [curColumn fieldValueInt];
        
        curColumn = [row columnWithNameFormat:@"%@", oaa_reminder];
        model.oaa_reminder = [curColumn fieldValueInt];
        
        curColumn = [row columnWithNameFormat:@"%@", oaa_remind];
        model.oaa_remind = [curColumn fieldValueInt];
        
        curColumn = [row columnWithNameFormat:@"%@", oaa_assign_date];
        model.oaa_assign_date = [curColumn fieldValueString];
        
        curColumn = [row columnWithNameFormat:@"%@", oaa_start_time];
        model.oaa_start_time = [curColumn fieldValueString];
        
        curColumn = [row columnWithNameFormat:@"%@", oaa_end_time];
        model.oaa_end_time = [curColumn fieldValueString];
        
        curColumn = [row columnWithNameFormat:@"%@", oaa_user_start_time];
        model.oaa_user_start_time = [curColumn fieldValueString];
        
        curColumn = [row columnWithNameFormat:@"%@", oaa_user_end_time];
        model.oaa_user_end_time = [curColumn fieldValueString];
        
        curColumn = [row columnWithNameFormat:@"%@", oaa_user_stop_time];
        model.oaa_user_stop_time = [curColumn fieldValueString];
        
        curColumn = [row columnWithNameFormat:@"%@", oaa_decline_service_time];
        model.oaa_decline_service_time = [curColumn fieldValueString];
        
        curColumn = [row columnWithNameFormat:@"%@", oaa_duration_spent];
        model.oaa_duration_spent = [curColumn fieldValueInt];
        
        curColumn = [row columnWithNameFormat:@"%@", oaa_local_duration];
        model.oaa_local_duration = [curColumn fieldValueInt];
        
        curColumn = [row columnWithNameFormat:@"%@", oaa_user_id];
        model.oaa_user_id = [curColumn fieldValueInt];
        
        curColumn = [row columnWithNameFormat:@"%@", oaa_is_single];
        model.oaa_is_single = [curColumn fieldValueInt];
        
        curColumn = [row columnWithNameFormat:@"%@", oaa_deleted];
        model.oaa_deleted = [curColumn fieldValueInt];
        
        curColumn = [row columnWithNameFormat:@"%@", oaa_last_modified];
        model.oaa_last_modified = [curColumn fieldValueString];
    }
    return model;
}

#pragma mark - Other Activity Record

- (int)insertOtherActivityRecord:(OtherActivityRecordModel *)model {
    
    if (model == nil) {
        return 0;
    }
    
    DataRow *rowInsert = [[DataRow alloc] init];
    rowInsert.tableName = [NSString stringWithFormat:@"%@", OTHER_ACTIVITY_RECORD];
    
    [rowInsert addColumnIntValue:model.oar_id formatName:@"%@", oar_id];
    [rowInsert addColumnIntValue:model.oar_onday_activity_id formatName:@"%@", oar_onday_activity_id];
    [rowInsert addColumnIntValue:model.oar_user_id formatName:@"%@", oar_user_id];
    [rowInsert addColumnIntValue:model.oar_operation formatName:@"%@", oar_operation];
    [rowInsert addColumnIntValue:model.oar_duration formatName:@"%@", oar_duration];
    [rowInsert addColumnIntValue:model.oar_post_status formatName:@"%@", oar_post_status];
    [rowInsert addColumnStringValue:model.oar_time formatName:@"%@", oar_time];
    [rowInsert addColumnStringValue:model.oar_remark formatName:@"%@", oar_remark];
    [rowInsert addColumnStringValue:model.oar_last_modified formatName:@"%@", oar_last_modified];
    
    return [rowInsert insertDBRow];
}

- (int)updateOtherActivityRecord:(OtherActivityRecordModel *)model {
    
    if (model == nil) {
        return 0;
    }
    
    DataRow *rowUpdate = [[DataRow alloc] init];
    rowUpdate.tableName = [NSString stringWithFormat:@"%@", OTHER_ACTIVITY_RECORD];
    
    DataColumn *columnUpdate1 = [rowUpdate addColumnInt:[NSString stringWithFormat:@"%@", oar_id] value:model.oar_id];
    columnUpdate1.isUpdateCondition = YES;
    
    [rowUpdate addColumnInt:[NSString stringWithFormat:@"%@", oar_onday_activity_id] value:model.oar_onday_activity_id];
    [rowUpdate addColumnInt:[NSString stringWithFormat:@"%@", oar_user_id] value:model.oar_user_id];
    [rowUpdate addColumnInt:[NSString stringWithFormat:@"%@", oar_operation] value:model.oar_operation];
    [rowUpdate addColumnInt:[NSString stringWithFormat:@"%@", oar_duration] value:model.oar_duration];
    [rowUpdate addColumnInt:[NSString stringWithFormat:@"%@", oar_post_status] value:model.oar_post_status];
    [rowUpdate addColumnString:[NSString stringWithFormat:@"%@", oar_time] value:model.oar_time];
    [rowUpdate addColumnString:[NSString stringWithFormat:@"%@", oar_remark] value:model.oar_remark];
    [rowUpdate addColumnString:[NSString stringWithFormat:@"%@", oar_last_modified] value:model.oar_last_modified];
    
    return [rowUpdate commitChangeDBRow];
}

- (int)deleteAllOtherActivityRecords {
    
    DataTable *tableDelete = [[DataTable alloc] initWithNameFormat:@"%@", OTHER_ACTIVITY_RECORD];
    return [tableDelete dropDBRows];
}

- (int)deleteOtherActivityRecordByUserId:(int)userId {
    DataTable *tableDelete = [[DataTable alloc] initWithNameFormat:@"%@", OTHER_ACTIVITY_RECORD];
    
    NSString *queryDelete = [NSString stringWithFormat:@"DELETE FROM %@ WHERE %@ = %d",
                             // DELETE FROM
                             OTHER_ACTIVITY_RECORD,
                             // WHERE
                             oar_user_id,
                             userId];
    int status = (int)[tableDelete excuteQueryNonSelect:queryDelete];
    
    return status;
}

- (int)deleteOtherActivityRecordByUserId:(int)userId ondayOtherActivityId:(int)ondayOtherActivityId {
    DataTable *tableDelete = [[DataTable alloc] initWithNameFormat:@"%@", OTHER_ACTIVITY_RECORD];
    
    NSString *queryDelete = [NSString stringWithFormat:@"DELETE FROM %@ WHERE %@ = %d AND %@ = %d",
                             // DELETE FROM
                             OTHER_ACTIVITY_RECORD,
                             // WHERE
                             oar_user_id,
                             userId,
                             oar_onday_activity_id,
                             ondayOtherActivityId];
    int status = (int)[tableDelete excuteQueryNonSelect:queryDelete];
    
    return status;
}

- (NSMutableArray*)loadOtherActivityRecordsByUserId:(int)userId postStatus:(int)postStatus {
    
    if (userId <= 0) {
        return nil;
    }
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ = %d AND %@ = %d",
                                  // SELECT
                                  oar_id,
                                  oar_onday_activity_id,
                                  oar_user_id,
                                  oar_operation,
                                  oar_duration,
                                  oar_post_status,
                                  oar_time,
                                  oar_remark,
                                  oar_last_modified,
                                  
                                  // FROM
                                  OTHER_ACTIVITY_RECORD,
                                  
                                  // WHERE
                                  oar_user_id,
                                  userId,
                                  oar_post_status,
                                  postStatus];
    
    DataTable *table = [[DataTable alloc] init];
    
    //Row result reference
    DataRow *rowReference = [[DataRow alloc] init];
    [rowReference addColumnFormatInt:@"%@", oar_id];
    [rowReference addColumnFormatInt:@"%@", oar_onday_activity_id];
    [rowReference addColumnFormatInt:@"%@", oar_user_id];
    [rowReference addColumnFormatInt:@"%@", oar_operation];
    [rowReference addColumnFormatInt:@"%@", oar_duration];
    [rowReference addColumnFormatInt:@"%@", oar_post_status];
    [rowReference addColumnFormatString:@"%@", oar_time];
    [rowReference addColumnFormatString:@"%@", oar_remark];
    [rowReference addColumnFormatString:@"%@", oar_last_modified];
    
    [table loadTableDataWithQuery:sqlString referenceRow:rowReference];
    
    int rowCount = [table countRows];
    NSMutableArray *results = nil;
    if(rowCount > 0)
    {
        results = [NSMutableArray array];
        for (int i = 0; i < rowCount; i ++) {
            
            OtherActivityRecordModel *model = [[OtherActivityRecordModel alloc] init];
            DataRow *row = [table rowWithIndex:i];
            DataColumn *curColumn = nil;
            
            curColumn = [row columnWithNameFormat:@"%@", oar_id];
            model.oar_id = [curColumn fieldValueInt];
            
            curColumn = [row columnWithNameFormat:@"%@", oar_onday_activity_id];
            model.oar_onday_activity_id = [curColumn fieldValueInt];
            
            curColumn = [row columnWithNameFormat:@"%@", oar_user_id];
            model.oar_user_id = [curColumn fieldValueInt];
            
            curColumn = [row columnWithNameFormat:@"%@", oar_operation];
            model.oar_operation = [curColumn fieldValueInt];
            
            curColumn = [row columnWithNameFormat:@"%@", oar_duration];
            model.oar_duration = [curColumn fieldValueInt];
            
            curColumn = [row columnWithNameFormat:@"%@", oar_post_status];
            model.oar_post_status = [curColumn fieldValueInt];
            
            curColumn = [row columnWithNameFormat:@"%@", oar_time];
            model.oar_time = [curColumn fieldValueString];
            
            curColumn = [row columnWithNameFormat:@"%@", oar_remark];
            model.oar_remark = [curColumn fieldValueString];
            
            curColumn = [row columnWithNameFormat:@"%@", oar_last_modified];
            model.oar_last_modified = [curColumn fieldValueString];
            
            [results addObject:model];
        }
    }
    return results;
}

- (int)countOtherActivityRecordsByUserId:(int)userId postStatus:(int)postStatus {
    
    if (userId <= 0) {
        return nil;
    }
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"SELECT COUNT(1) FROM %@ WHERE %@ = %d AND %@ = %d",
                                  // COUNT(1) FROM
                                  OTHER_ACTIVITY_RECORD,
                                  // WHERE
                                  oar_user_id,
                                  userId,
                                  oar_post_status,
                                  postStatus
                                  ];
    
    DataTable *table = [[DataTable alloc] init];
    
    //Row result reference
    DataRow *rowReference = [[DataRow alloc] init];
    [rowReference addColumnFormatString:@"CountResult"];
    
    [table loadTableDataWithQuery:sqlString referenceRow:rowReference];
    
    int rowCount = [table countRows];
    int countResult = 0;
    if (rowCount > 0) {
        countResult = [[[table rowWithIndex:0] columnWithName:@"CountResult"] fieldValueInt];
    }
    
    return countResult;
}

#pragma mark - WS FUNCTIONS
#pragma mark
// GET FUNCTIONS
- (bool)loadWSOtherActivitiesStatusByUserId:(int)userId lastModified:(NSString*)lastModified {
    
    bool result = NO;
    //No network detected
    if (![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        return result;
    }
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if ([LogFileManager isLogConsole]) {
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_GetOtherActivitiesStatusRoutine *request = [[eHousekeepingService_GetOtherActivitiesStatusRoutine alloc] init];
    
    [request setUserID:[NSString stringWithFormat:@"%d", userId]];
    [request setLastModified:lastModified];
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetOtherActivitiesStatusRoutineUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetOtherActivitiesStatusRoutineResponse class]]) {
            eHousekeepingService_GetOtherActivitiesStatusRoutineResponse *dataBody = (eHousekeepingService_GetOtherActivitiesStatusRoutineResponse *)bodyPart;
            
            if (dataBody != nil) {
                if ([dataBody.GetOtherActivitiesStatusRoutineResult.Response.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_OK]]) {
                    result = YES; //success
                    
                    //Delete old records
                    [self deleteAllOtherActivityStatus];
                    
                    NSMutableArray *otherActivityStatus = dataBody.GetOtherActivitiesStatusRoutineResult.ActivityStatusList.OtherActivityStatus;
                    NSString *todayDateString = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
                    
                    if ([otherActivityStatus count] > 0) {
                        
                        for (eHousekeepingService_OtherActivityStatus *curActStatus in otherActivityStatus) {
                            
                            OtherActivityStatusModel *othActStatus = [[OtherActivityStatusModel alloc] init];
                            othActStatus.oas_id = [curActStatus.StatusID intValue];
                            othActStatus.oas_name = curActStatus.StatusName;
                            othActStatus.oas_lang = curActStatus.StatusLang;
                            othActStatus.oas_image = curActStatus.StatusImage;
                            othActStatus.oas_last_modified = todayDateString;
                            [self insertOtherActivityStatus:othActStatus];
                        }
                    }
                }
            }
        }
    }
    return result;
}

- (bool)loadWSOtherActivitiesLocationByUserId:(int)userId hotelId:(int)hotelId lastModified:(NSString*)lastModified {
    
    bool result = NO;
    //No network detected
    if (![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        return result;
    }
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if ([LogFileManager isLogConsole]) {
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_GetOtherActivitiesLocationRoutine *request = [[eHousekeepingService_GetOtherActivitiesLocationRoutine alloc] init];
    
    [request setUserID:[NSString stringWithFormat:@"%d", userId]];
    [request setPropertyID:[NSString stringWithFormat:@"%d", hotelId]];
    [request setLastModified:lastModified];
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetOtherActivitiesLocationRoutineUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetOtherActivitiesLocationRoutineResponse class]]) {
            eHousekeepingService_GetOtherActivitiesLocationRoutineResponse *dataBody = (eHousekeepingService_GetOtherActivitiesLocationRoutineResponse *)bodyPart;
            
            if (dataBody != nil) {
                if ([dataBody.GetOtherActivitiesLocationRoutineResult.Response.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_OK]]) {
                    result = YES; //success
                    
                    //Delete old records
                    [self deleteAllOtherActivityLocations];
                    
                    NSMutableArray *otherActivityLocation = dataBody.GetOtherActivitiesLocationRoutineResult.ActivityLocationList.OtherActivityLocation;
                    NSString *todayDateString = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
                    
                    if ([otherActivityLocation count] > 0) {
                        
                        for (eHousekeepingService_OtherActivityLocation *curActLocation in otherActivityLocation) {
                            
                            OtherActivityLocationModel *othActLocation = [[OtherActivityLocationModel alloc] init];
                            othActLocation.oal_id = [curActLocation.LocationID intValue];
                            othActLocation.oal_name = curActLocation.LocationName;
                            othActLocation.oal_name_lang = curActLocation.LocationNameLang;
                            othActLocation.oal_code = curActLocation.LocationCode;
                            othActLocation.oal_desc = curActLocation.LocationDesc;
                            othActLocation.oal_desc_lang = curActLocation.LocationDescLang;
                            othActLocation.oal_hotel_id = [curActLocation.LocationHotelID intValue];
                            othActLocation.oal_last_modified = todayDateString;
                            [self insertOtherActivityLocation:othActLocation];
                        }
                    }
                }
            }
        }
    }
    return result;
}

- (bool)loadWSOtherActivityAssignmentByUserId:(int)userId hotelId:(int)hotelId buildingId:(int)buildingId dateTime:(NSString*)dateTime {
    
    bool result = NO;
    //No network detected
    if (![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        return result;
    }
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if ([LogFileManager isLogConsole]) {
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_GetOtherActivityAssignmentRoutine *request = [[eHousekeepingService_GetOtherActivityAssignmentRoutine alloc] init];
    
    [request setUserID:[NSString stringWithFormat:@"%d", userId]];
    [request setPropertyID:[NSString stringWithFormat:@"%d", hotelId]];
    [request setBuildingID:[NSString stringWithFormat:@"%d", buildingId]];
    [request setDateTime:dateTime];
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetOtherActivityAssignmentRoutineUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetOtherActivityAssignmentRoutineResponse class]]) {
            eHousekeepingService_GetOtherActivityAssignmentRoutineResponse *dataBody = (eHousekeepingService_GetOtherActivityAssignmentRoutineResponse *)bodyPart;
            
            if (dataBody != nil) {
                if ([dataBody.GetOtherActivityAssignmentRoutineResult.Response.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_OK]]) {
                    result = YES; //success
                    
                    NSMutableArray *otherActivityDetails = dataBody.GetOtherActivityAssignmentRoutineResult.OtherActvityDetailsListing.OtherActivityDetails;
                    
                    if ([otherActivityDetails count] > 0) {
                        
                        NSString *todayDateString = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
                        
                        for (eHousekeepingService_OtherActivityDetails *curActDetail in otherActivityDetails) {
                            
                            OtherActivityModel *otherActivityModel = [self loadOtherActivityById:[curActDetail.OnDayActivitiesID intValue]];
                            if (otherActivityModel) {
                                otherActivityModel.oa_name = curActDetail.OnDayActivitiesName;
                                otherActivityModel.oa_name_lang = curActDetail.OnDayActivitiesNameLang;
                                otherActivityModel.oa_code = curActDetail.OnDayActivitiesCode;
                                otherActivityModel.oa_desc = curActDetail.OnDayActivitiesDesc;
                                otherActivityModel.oa_desc_lang = curActDetail.OnDayActivitiesDescLang;
                                otherActivityModel.oa_last_modified = todayDateString;
                                [self updateOtherActivity:otherActivityModel];
                            } else {
                                otherActivityModel = [[OtherActivityModel alloc] init];
                                otherActivityModel.oa_id = [curActDetail.OnDayActivitiesID intValue];
                                otherActivityModel.oa_name = curActDetail.OnDayActivitiesName;
                                otherActivityModel.oa_name_lang = curActDetail.OnDayActivitiesNameLang;
                                otherActivityModel.oa_code = curActDetail.OnDayActivitiesCode;
                                otherActivityModel.oa_desc = curActDetail.OnDayActivitiesDesc;
                                otherActivityModel.oa_desc_lang = curActDetail.OnDayActivitiesDescLang;
                                otherActivityModel.oa_last_modified = todayDateString;
                                [self insertOtherActivity:otherActivityModel];
                            }
                            OtherActivityAssignmentModel *assModel = [self loadOtherActivityAssignmentByUserId:userId ondayOtherActivityId:[curActDetail.OnDayID intValue]];
                            if (assModel) {
                                assModel.oaa_activity_id = [curActDetail.OnDayActivityID intValue];
                                assModel.oaa_activities_id = [curActDetail.OnDayActivitiesID intValue];
                                assModel.oaa_location_id = [curActDetail.OnDayLocationID intValue];
                                assModel.oaa_status_id = [curActDetail.OnDayStatusID intValue];
                                assModel.oaa_remark = curActDetail.OnDayRemark;
                                assModel.oaa_duration = [curActDetail.OnDayDuration intValue];
                                assModel.oaa_reminder = [curActDetail.OnDayReminder intValue];
                                assModel.oaa_remind = [curActDetail.OnDayRemind intValue];
                                assModel.oaa_assign_date = curActDetail.OnDayAssignDate;
                                assModel.oaa_start_time = curActDetail.OnDayStartTime;
                                assModel.oaa_end_time = curActDetail.OnDayEndTime;
                                assModel.oaa_user_start_time = curActDetail.OnDayStaffStartTime;
                                assModel.oaa_user_end_time = curActDetail.OnDayStaffEndTime;
                                assModel.oaa_user_stop_time = curActDetail.OnDayStaffStopTime;
                                assModel.oaa_decline_service_time = curActDetail.OnDayDeclineServiceTime;
                                assModel.oaa_duration_spent = [curActDetail.OnDayDurationSpent intValue];
                                assModel.oaa_user_id = [curActDetail.OnDayStaffID intValue];
                                assModel.oaa_is_single = [curActDetail.OnDayIsSingle intValue];
                                assModel.oaa_deleted = [curActDetail.OnDayDeleted intValue];
                                assModel.oaa_last_modified = todayDateString;
                                [self updateOtherActivityAssignment:assModel];
                            } else {
                                assModel = [[OtherActivityAssignmentModel alloc] init];
                                assModel.oaa_id = [curActDetail.OnDayID intValue];
                                assModel.oaa_activity_id = [curActDetail.OnDayActivityID intValue];
                                assModel.oaa_activities_id = [curActDetail.OnDayActivitiesID intValue];
                                assModel.oaa_location_id = [curActDetail.OnDayLocationID intValue];
                                assModel.oaa_status_id = [curActDetail.OnDayStatusID intValue];
                                assModel.oaa_remark = curActDetail.OnDayRemark;
                                assModel.oaa_duration = [curActDetail.OnDayDuration intValue];
                                assModel.oaa_reminder = [curActDetail.OnDayReminder intValue];
                                assModel.oaa_remind = [curActDetail.OnDayRemind intValue];
                                assModel.oaa_assign_date = curActDetail.OnDayAssignDate;
                                assModel.oaa_start_time = curActDetail.OnDayStartTime;
                                assModel.oaa_end_time = curActDetail.OnDayEndTime;
                                assModel.oaa_user_start_time = curActDetail.OnDayStaffStartTime;
                                assModel.oaa_user_end_time = curActDetail.OnDayStaffEndTime;
                                assModel.oaa_user_stop_time = curActDetail.OnDayStaffStopTime;
                                assModel.oaa_decline_service_time = curActDetail.OnDayDeclineServiceTime;
                                assModel.oaa_duration_spent = [curActDetail.OnDayDurationSpent intValue];
                                assModel.oaa_user_id = [curActDetail.OnDayStaffID intValue];
                                assModel.oaa_is_single = [curActDetail.OnDayIsSingle intValue];
                                assModel.oaa_deleted = [curActDetail.OnDayDeleted intValue];
                                assModel.oaa_last_modified = todayDateString;
                                [self insertOtherActivityAssignment:assModel];
                            }
                        }
                    }
                }
            }
        }
    }
    return result;
}

// POST FUNCTIONS
- (bool)postOtherActivityStatusByUserId:(int)userId ondayOtherActivityId:(int)ondayOtherActivityId otherActivityStatus:(int)otherActivityStatus time:(NSString*)time remark:(NSString*)remark durationSpent:(int)durationSpent {
    
    //No network detected
    if (![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        return NO;
    }
    
    if (userId <= 0) {
        return NO;
    }
    
    if (ondayOtherActivityId <= 0) {
        return NO;
    }
    
    if (otherActivityStatus <= 0) {
        return NO;
    }
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole]){
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_PostUpdateOtherActivityStatusRoutine *request = [[eHousekeepingService_PostUpdateOtherActivityStatusRoutine alloc] init];
    
    [request setUserID:[NSString stringWithFormat:@"%d",userId]];
    [request setActivityID:[NSString stringWithFormat:@"%d", ondayOtherActivityId]];
    [request setStatusID:[NSString stringWithFormat:@"%d", otherActivityStatus]];
    [request setDurationSpent:[NSString stringWithFormat:@"%d", durationSpent]];
    
    if (time.length > 0) {
        [request setTime:time];
    } else {
        [request setTime:@""];
    }
    
    if (remark.length > 0) {
        [request setRemark:remark];
    } else {
        [request setRemark:@""];
    }
    
    bool result = NO;
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding PostUpdateOtherActivityStatusRoutineUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_PostUpdateOtherActivityStatusRoutineResponse class]]) {
            eHousekeepingService_PostUpdateOtherActivityStatusRoutineResponse *dataBody = (eHousekeepingService_PostUpdateOtherActivityStatusRoutineResponse *)bodyPart;
            if (dataBody != nil) {
                if ([dataBody.PostUpdateOtherActivityStatusRoutineResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_OK]]
                    || [dataBody.PostUpdateOtherActivityStatusRoutineResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_NO_RERCORD_TO_DISPLAY]]
                    || [dataBody.PostUpdateOtherActivityStatusRoutineResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_NEW_RECORD_ADDED]]) {
                    
                    result = YES;
                }
            }
        }
    }
    return result;
}

- (int)postAllOtherActivityByUserId:(int)userId {
    
    int countPosted = 0;
    NSMutableArray *listOtherActivityUnposted = [self loadOtherActivityRecordsByUserId:userId postStatus:(int)POST_STATUS_SAVED_UNPOSTED]; //Load unposted Other Activity
    
    for (OtherActivityRecordModel *curDetailRecord in listOtherActivityUnposted) {
        
        bool isPostedSuccess = NO;
        isPostedSuccess = [self postOtherActivityStatusByUserId:userId ondayOtherActivityId:curDetailRecord.oar_onday_activity_id otherActivityStatus:curDetailRecord.oar_operation time:curDetailRecord.oar_time remark:curDetailRecord.oar_remark durationSpent:curDetailRecord.oar_duration];
        
        if (isPostedSuccess) {
            countPosted ++;
            curDetailRecord.oar_post_status = (int)POST_STATUS_POSTED;
            [self updateOtherActivityRecord:curDetailRecord];
        }
    }
    return countPosted;
}

@end
