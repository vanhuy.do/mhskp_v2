//
//  RoomAdapter.h
//  eHouseKeeping
//
//  Created by KhanhNguyen on 6/3/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RoomModelV2.h"
#import "DatabaseAdapter.h"
#import "ZoneModelV2.h"
#import "UnassignManagerV2.h"
#import "RoomAdapterV2.h"

//enum ENUM_SEARCH_TYPE {
//    ENUM_SEARCH_ROOM_NUMBER = 1,
//    ENUM_SEARCH_ROOM_STATUS = 2,
//    ENUM_SEARCH_VIP_CODE =  3,
//    ENUM_SEARCH_ZONE_NO = 4
//};

@interface RoomAdapterV2Demo : RoomAdapterV2 {
    
}

-(int)insertRoomData:(RoomModelV2*) room;
-(int)updateRoomData:(RoomModelV2*) room;
-(int)updateRoomModelReassign:(RoomModelV2*) room;
-(int)deleteRoomData:(RoomModelV2*) room;
-(void)loadRoomData:(RoomModelV2*) room;
-(NSMutableArray *)loadAllRoomType;
-(NSMutableArray *)loadAllRoom;
-(NSMutableArray *)loadRoomByZoneId:(ZoneModelV2*)zoneModel;
-(NSMutableArray*)searchRoomsByRoomId:(NSInteger)roomId roomStatus:(NSInteger)roomStatus vipcode :(NSString*)vipcode withSearchType:(enum ENUM_SEARCH_TYPE) searchType;
-(NSMutableArray *) loadAllRoomByUserId:(NSInteger) userId;

-(NSInteger) numberOfMustPostRoomRecordsWithUserId:(NSInteger) userId;
@end
