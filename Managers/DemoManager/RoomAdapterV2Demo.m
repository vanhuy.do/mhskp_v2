//
//  RoomAdapter.m
//  eHouseKeeping
//
//  Created by KhanhNguyen on 6/3/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "RoomAdapterV2Demo.h"
#import "DbDefinesV2.h"
#import "ehkDefines.h"

@implementation RoomAdapterV2Demo

-(int)insertRoomData:(RoomModelV2*) room
{
    

        NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"%@ %@ (%@,%@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ ,%@ ,%@) %@", 
                                      @"insert into",
                                      ROOM,
                                      room_id,
                                      room_hotel_id,
                                      room_type_id,
                                      room_lang,
                                      room_building_name,
                                      room_guest_name,
                                      room_building_namelang,
                                      room_vip_status,
                                      room_guest_reference,
                                      room_additional_job,
                                      room_post_status,
                                      room_last_modified,
                                      
                                      room_is_re_cleaning,
                                      room_expected_status_id,
                                      room_zone_id,
                                      room_floor_id,
                                      room_building_id,
                                      @"Values( ?,?,?,?,?,?,?,?,?,?,?,?,?,?,? ,?,?)"];
        

        const char *sql=[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }

    reAccessCount++;
}

		if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
        {
        }

    sqlite3_bind_text(self.sqlStament, 1 , [room.room_Id UTF8String], -1,SQLITE_TRANSIENT);
    sqlite3_bind_int (self.sqlStament, 2, room.room_HotelId);
    sqlite3_bind_int (self.sqlStament, 3, room.room_TypeId);
    sqlite3_bind_text(self.sqlStament, 4, [room.room_Lang UTF8String], -1,SQLITE_TRANSIENT);

    sqlite3_bind_text(self.sqlStament, 5 ,[room.room_Building_Name UTF8String], -1,SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 6 ,[room.room_Guest_Name UTF8String], -1,SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 7 ,[room.room_building_namelang UTF8String], -1,SQLITE_TRANSIENT);
    
    sqlite3_bind_int (self.sqlStament, 8, [room.room_VIPStatusId intValue]);
    
    sqlite3_bind_text(self.sqlStament, 9 ,[room.room_GuestReference UTF8String], -1,SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 10, [room.room_AdditionalJob UTF8String], -1,SQLITE_TRANSIENT);
    sqlite3_bind_int (self.sqlStament, 11, room.room_PostStatus);
    sqlite3_bind_text(self.sqlStament, 12, [room.room_LastModified UTF8String], -1,SQLITE_TRANSIENT);
    
    sqlite3_bind_int (self.sqlStament, 13, (int)room.room_is_re_cleaning);
    sqlite3_bind_int (self.sqlStament, 14, (int)room.room_expected_status_id);
    sqlite3_bind_int (self.sqlStament, 15, (int)room.room_zone_id);
    sqlite3_bind_int (self.sqlStament, 16, (int)room.room_floor_id);
    sqlite3_bind_int (self.sqlStament, 17, (int)room.room_Building_Id);
    
    NSInteger returnInt = 0;    
    NSInteger status = sqlite3_step(self.sqlStament);
    
    if(SQLITE_DONE != status) {  
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return (int)returnInt;
    } else {
        sqlite3_reset(self.sqlStament);
        returnInt = 1;
        return (int)returnInt;
    }
}

-(int)updateRoomData:(RoomModelV2*) room
{

        NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"%@ %@ SET %@=?, %@=?,%@=?, %@=?, %@=?, %@=?, %@=? ,%@=? ,%@=? ,%@=? ,%@=?,%@=?,%@=?,%@=? ,%@=? ,%@ =? WHERE %@=?", 
                                      @"update ",
                                      ROOM,
                                      room_id,
                                      room_hotel_id,
                                      room_type_id,
                                      room_lang,
                                      room_building_name,                                      
                                      room_guest_name,
                                      room_building_namelang,
                                      room_vip_status,
                                      room_guest_reference,
                                      room_additional_job,
                                      room_post_status,
                                      room_last_modified,
                                      room_is_re_cleaning,
                                      room_expected_status_id,
                                      room_zone_id,
                                      room_floor_id,
                                      
                                      room_id];
        
        
        const char *sql=[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }

    reAccessCount++;
}

		if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
        {
        }
        
        sqlite3_bind_text(self.sqlStament, 1 , [room.room_Id UTF8String], -1,SQLITE_TRANSIENT);
        sqlite3_bind_int (self.sqlStament, 2, room.room_HotelId);
        sqlite3_bind_int (self.sqlStament, 3, room.room_TypeId);
        sqlite3_bind_text(self.sqlStament, 4, [room.room_Lang UTF8String], -1,SQLITE_TRANSIENT);
        
        sqlite3_bind_text(self.sqlStament, 5 ,[room.room_Building_Name UTF8String], -1,SQLITE_TRANSIENT);
        sqlite3_bind_text(self.sqlStament, 6 ,[room.room_Guest_Name UTF8String], -1,SQLITE_TRANSIENT);
        sqlite3_bind_text(self.sqlStament, 7 ,[room.room_building_namelang UTF8String], -1,SQLITE_TRANSIENT);
        
        sqlite3_bind_int (self.sqlStament, 8, [room.room_VIPStatusId intValue]);
        
        sqlite3_bind_text(self.sqlStament, 9 ,[room.room_GuestReference UTF8String], -1,SQLITE_TRANSIENT);
        sqlite3_bind_text(self.sqlStament, 10, [room.room_AdditionalJob UTF8String], -1,SQLITE_TRANSIENT);
        sqlite3_bind_int (self.sqlStament, 11, room.room_PostStatus);
        sqlite3_bind_text(self.sqlStament, 12, [room.room_LastModified UTF8String], -1,SQLITE_TRANSIENT);
        
        sqlite3_bind_int (self.sqlStament, 13, (int)room.room_is_re_cleaning);
        sqlite3_bind_int (self.sqlStament, 14, (int)room.room_expected_status_id);
        sqlite3_bind_int (self.sqlStament, 15, (int)room.room_zone_id);
        sqlite3_bind_int (self.sqlStament, 16, (int)room.room_floor_id);
        // paramater as ID
        sqlite3_bind_text(self.sqlStament, 17 , [room.room_Id UTF8String], -1,SQLITE_TRANSIENT);
        
        
        sqlite3_reset(self.sqlStament);
    
    NSInteger returnInt = 0;    
    NSInteger status = sqlite3_step(self.sqlStament);
    if(SQLITE_DONE != status) {   
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return (int) returnInt;
    } else { 
        returnInt = 1;        
        return (int)returnInt;
    }
    
}
-(int)updateRoomModelReassign:(RoomModelV2*) room
{
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"%@ %@ SET %@=? ,%@ =? WHERE %@=?",
                                  @"update ",
                                  ROOM,
                                  room_post_status,
                                  room_is_re_cleaning,
                    
                                  room_id];
    
    
    const char *sql=[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
    {
    }
    
    sqlite3_bind_int (self.sqlStament, 1, room.room_PostStatus);
    sqlite3_bind_int (self.sqlStament, 2, (int)room.room_is_re_cleaning);
    // paramater as ID
    sqlite3_bind_text(self.sqlStament, 3 , [room.room_Id UTF8String], -1,SQLITE_TRANSIENT);
    
    
    sqlite3_reset(self.sqlStament);
    
    NSInteger returnInt = 0;    
    NSInteger status = sqlite3_step(self.sqlStament);
    if(SQLITE_DONE != status) {   
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return (int)returnInt;
    } else { 
        returnInt = 1;        
        return (int)returnInt;
    }
}

-(int)deleteRoomData:(RoomModelV2*) room
{
    NSInteger result = 1;

        NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"%@ %@ WHERE %@=?", @"detele from ",ROOM,room_id];
        const char *sql=[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }

    reAccessCount++;
}

		if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
        {
            result = 0;
        }
        sqlite3_bind_text(self.sqlStament, 1 , [room.room_Id UTF8String], -1,SQLITE_TRANSIENT);
        
        
        sqlite3_reset(self.sqlStament);
    
    NSInteger status = sqlite3_step(sqlStament);
    
	if (SQLITE_DONE != status) {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
		result = 0;
    }
    
    return (int)result;
    
}

-(void)loadRoomData:(RoomModelV2*) room
{
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"%@    %@,%@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ ,%@ ,%@ ,%@  from %@ where %@=?", 
                                  @"select",
                                  room_id,
                                  room_hotel_id,
                                  room_type_id,
                                  room_lang,
                                  room_building_name,
                                  room_guest_name,
                                  room_building_namelang,
                                  room_vip_status,
                                  room_guest_reference,
                                  room_additional_job,
                                  room_post_status,
                                  room_last_modified,
                                  
                                  room_is_re_cleaning,
                                  room_expected_status_id,
                                  room_zone_id,
                                  room_floor_id,
                                  room_building_id,
                                  
                                  ROOM,room_id];

    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }

    reAccessCount++;
}

    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    
    sqlite3_bind_text(self.sqlStament, 1 , [room.room_Id UTF8String], -1,SQLITE_TRANSIENT);
    
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW) 
    {
        if (sqlite3_column_text(self.sqlStament, 0)) {
            room.room_Id = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 0)];
        }
        room.room_HotelId=sqlite3_column_int(self.sqlStament, 1);
        room.room_TypeId=sqlite3_column_int(self.sqlStament, 2);
        
        if (sqlite3_column_text(sqlStament, 3)!=nil) {
            room.room_Lang=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
        }
        
        if (sqlite3_column_text(sqlStament, 4)) {
            room.room_Building_Name=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
        }
        if (sqlite3_column_text(sqlStament, 5)) {
            room.room_Guest_Name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
        }
        
        if (sqlite3_column_text(sqlStament, 6)) {
            room.room_building_namelang = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 6)];
        }
        room.room_VIPStatusId=[[NSString alloc] initWithFormat:@"%d", sqlite3_column_int(sqlStament, 7)];
        if (sqlite3_column_text(sqlStament, 8)!=nil) {
            room.room_GuestReference=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 8)];
        }
        if (sqlite3_column_text(sqlStament, 9)!=nil) {
            room.room_AdditionalJob=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 9)];
        }
        if (sqlite3_column_int(self.sqlStament, 10)) {
            room.room_PostStatus=sqlite3_column_int(self.sqlStament, 10);
        }
        if (sqlite3_column_text(sqlStament, 11)) {
            room.room_LastModified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 11)];
        }
        
        
        room.room_is_re_cleaning=sqlite3_column_int(self.sqlStament, 12);
        room.room_expected_status_id = sqlite3_column_int(self.sqlStament, 13);
        room.room_zone_id=sqlite3_column_int(self.sqlStament, 14);
        room.room_floor_id=sqlite3_column_int(self.sqlStament, 15);
        room.room_Building_Id=sqlite3_column_int(self.sqlStament, 16);
        
        status = sqlite3_step(sqlStament);
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }

    
}

-(NSMutableArray *)loadAllRoomType{
    NSMutableArray *array = [NSMutableArray array];
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"%@ %@  from %@", 
                                  @"select",
                                  room_type_id,
                                  ROOM];
    
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }

    reAccessCount++;
}
    

    NSInteger tempRoomType;
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW) 
    {
        tempRoomType = sqlite3_column_int(self.sqlStament, 0);
        [array addObject:[NSNumber numberWithInt:(int)tempRoomType]];
        status = sqlite3_step(sqlStament);
    }
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                [array removeAllObjects];
                goto RE_ACCESS_DB;
            }
        }

    }

    
    return array;
}

-(NSMutableArray *)loadAllRoom{
    NSMutableArray *array = [NSMutableArray array];
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"%@   %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ from %@", 
                                  @"select",
                                  room_id,
                                  room_hotel_id,
                                  room_type_id,
                                  room_lang,
                                  room_location_code,
                                  room_building_name,
                                  room_vip_status,
                                  room_guest_reference,
                                  room_additional_job,
                                  room_post_status,
                                  room_last_modified,
                                  room_floor_id,
                                  
                                  ROOM];
    
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }

    reAccessCount++;
}

    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW) 
    {
        RoomModelV2 *room = [[RoomModelV2 alloc] init];
        if (sqlite3_column_text(self.sqlStament, 0)) {
            room.room_Id = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 0)];
        }
        room.room_HotelId=sqlite3_column_int(self.sqlStament, 1);
        room.room_TypeId=sqlite3_column_int(self.sqlStament, 2);
        if (sqlite3_column_text(sqlStament, 3)!=nil) {
            room.room_Lang=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
        }
        
        room.room_LocationCode=sqlite3_column_int(sqlStament, 4);
        if (sqlite3_column_text(sqlStament, 5)) {
            room.room_Building_Name=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
        }
        room.room_VIPStatusId=[[NSString alloc] initWithFormat:@"%d", sqlite3_column_int(sqlStament, 6)];
        if (sqlite3_column_text(sqlStament, 7)!=nil) {
            room.room_GuestReference=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 7)];
        }
        if (sqlite3_column_text(sqlStament, 8)!=nil) {
            room.room_AdditionalJob=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 8)];
        }
        if (sqlite3_column_int(self.sqlStament, 9)) {
            room.room_PostStatus=sqlite3_column_int(self.sqlStament, 9);
        }
        if (sqlite3_column_text(sqlStament, 10)) {
            room.room_LastModified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 10)];
        }
        
        room.room_floor_id=sqlite3_column_int(self.sqlStament, 11);
        
        [array addObject:room];
        status = sqlite3_step(sqlStament);
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    

    return array;
}

-(NSMutableArray *)loadRoomByZoneId:(ZoneModelV2*)zoneModel{
    NSMutableArray *array = [NSMutableArray array];
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"%@   %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ ,%@ ,%@  %@ from %@ where %@=?", 
                                  @"select",
                                  room_id,
                                  room_hotel_id,
                                  room_type_id,
                                  room_lang,
                                  room_building_name,
                                  room_guest_name,
                                  room_building_namelang,
                                  room_vip_status,
                                  room_guest_reference,
                                  room_additional_job,
                                  room_post_status,
                                  room_last_modified,
                                  
                                  room_is_re_cleaning,
                                  room_expected_status_id,
                                  room_zone_id,
                                  room_floor_id,
                                  
                                  ROOM,room_zone_id];
    
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }

    reAccessCount++;
}


   
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {
    }
    
    sqlite3_bind_int(self.sqlStament, 1, (int)zoneModel.zone_id);
    
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW) 
    {
        RoomModelV2 *room = [[RoomModelV2 alloc] init];
        if (sqlite3_column_text(self.sqlStament, 0)) {
            room.room_Id = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 0)];
        }
        room.room_HotelId=sqlite3_column_int(self.sqlStament, 1);
        room.room_TypeId=sqlite3_column_int(self.sqlStament, 2);
        if (sqlite3_column_text(sqlStament, 3)!=nil) {
            room.room_Lang=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
        }
        
        if (sqlite3_column_text(sqlStament, 4)) {
            room.room_Building_Name=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
        }
        if (sqlite3_column_text(sqlStament, 5)) {
            room.room_Guest_Name=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
        }
        
        if (sqlite3_column_text(sqlStament, 6)) {
            room.room_building_namelang=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 6)];
        }
        
        room.room_VIPStatusId=[[NSString alloc] initWithFormat:@"%d", sqlite3_column_int(sqlStament, 7)];
        
        if (sqlite3_column_text(sqlStament, 8)!=nil) {
            room.room_GuestReference=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 8)];
        }
        if (sqlite3_column_text(sqlStament, 9)!=nil) {
            room.room_AdditionalJob=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 9)];
        }
        if (sqlite3_column_int(self.sqlStament, 10)) {
            room.room_PostStatus=sqlite3_column_int(self.sqlStament, 10);
        }
        if (sqlite3_column_text(sqlStament, 11)) {
            room.room_LastModified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 11)];
        }
        
        room.room_is_re_cleaning=sqlite3_column_int(self.sqlStament, 12);
        room.room_expected_status_id=sqlite3_column_int(self.sqlStament, 13);
        room.room_zone_id=sqlite3_column_int(self.sqlStament, 14);
        room.room_floor_id=sqlite3_column_int(self.sqlStament, 15);
        
        [array addObject:room];
        status = sqlite3_step(sqlStament);
        
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }

    return array;
}

-(NSMutableArray*)searchRoomsByRoomId:(NSInteger)roomId roomStatus:(NSInteger)roomStatus vipcode :(NSString*)vipcode withSearchType:(enum ENUM_SEARCH_TYPE) searchType{

    NSMutableArray *array = [NSMutableArray array];
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"%@    %@,%@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ ,%@ ,%@  from %@ where %@=?", 
                                  @"select",
                                  room_id,
                                  room_hotel_id,
                                  room_type_id,
                                  room_lang,
                                  room_building_name,
                                  room_guest_name,
                                  room_building_namelang,
                                  room_vip_status,
                                  room_guest_reference,
                                  room_additional_job,
                                  room_post_status,
                                  room_last_modified,
                                  
                                  room_is_re_cleaning,
                                  room_expected_status_id,
                                  room_zone_id,
                                  room_floor_id,
                                  ROOM,room_zone_id];
    
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }

    reAccessCount++;
}

    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {
    }
    
    //bind data        
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW) 
    {
        RoomModelV2 *room = [[RoomModelV2 alloc] init];
        if (sqlite3_column_text(self.sqlStament, 0)) {
            room.room_Id = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 0)];
        }
        room.room_HotelId=sqlite3_column_int(self.sqlStament, 1);
        //room.room_StatusId=sqlite3_column_int(self.sqlStament, 2);
        room.room_TypeId=sqlite3_column_int(self.sqlStament, 2);
        if (sqlite3_column_text(sqlStament, 3)!=nil) {
            room.room_Lang=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
        }
        
        if (sqlite3_column_text(sqlStament, 4)) {
            room.room_Building_Name=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
        }
        if (sqlite3_column_text(sqlStament, 5)) {
            room.room_Guest_Name=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
        }
        
        if (sqlite3_column_text(sqlStament, 6)) {
            room.room_building_namelang=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 6)];
        }
        
        room.room_VIPStatusId=[[NSString alloc] initWithFormat:@"%d", sqlite3_column_int(sqlStament, 7)];
        
        if (sqlite3_column_text(sqlStament, 8)!=nil) {
            room.room_GuestReference=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 8)];
        }
        if (sqlite3_column_text(sqlStament, 9)!=nil) {
            room.room_AdditionalJob=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 9)];
        }
        if (sqlite3_column_int(self.sqlStament, 10)) {
            room.room_PostStatus=sqlite3_column_int(self.sqlStament, 10);
        }
        if (sqlite3_column_text(sqlStament, 11)) {
            room.room_LastModified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 11)];
        }
        
        room.room_is_re_cleaning=sqlite3_column_int(self.sqlStament, 12);
        room.room_expected_status_id=sqlite3_column_int(self.sqlStament, 13);
        room.room_zone_id=sqlite3_column_int(self.sqlStament, 14);
        room.room_floor_id=sqlite3_column_int(self.sqlStament, 15);
        [array addObject:room];
        status = sqlite3_step(sqlStament);
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    

    return array;


}

-(NSMutableArray *)loadAllRoomByUserId:(NSInteger)userId
{
    NSMutableArray *resultRoom = [[NSMutableArray alloc] init] ;
    NSMutableString *sqlString;
    
    sqlString = [[NSMutableString alloc] initWithFormat:@"select  %@ from %@ where %@=? ",
                 ra_room_id,
                 ROOM_ASSIGNMENT,
                 ra_user_id];  
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }

    reAccessCount++;
}

    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {    }
    sqlite3_bind_int(self.sqlStament, 1, (int)userId);
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW) 
    {
        NSString *roomId = nil;
        
        if (sqlite3_column_text(self.sqlStament, 0)) {
            roomId = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 0)];
        }
        
        [resultRoom addObject:roomId];
        status = sqlite3_step(sqlStament);
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            [resultRoom removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }

    return resultRoom;

}


-(NSInteger) numberOfMustPostRoomRecordsWithUserId:(NSInteger)userId {
    NSString *sqlString = [NSString stringWithFormat:@"SELECT COUNT(*) FROM %@ WHERE %@ = %d AND %@ = %d", 
                           ROOM_RECORDS, 
                           
                           rrec_pos_status, 
                           (int)POST_STATUS_SAVED_UNPOSTED,
                           
                           rrec_user_id,
                           (int)userId
                           ];
    const char *sql = [sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}

    if (sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {
    }
    
    NSInteger status = sqlite3_step(sqlStament);
    while (status == SQLITE_ROW) {
        return sqlite3_column_int(sqlStament, 0);
        status = sqlite3_step(sqlStament);
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }

        
    }

    
    return 0;
}


@end
