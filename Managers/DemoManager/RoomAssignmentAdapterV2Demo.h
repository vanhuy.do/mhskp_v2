//
//  RoomAssignmentAdapter.h
//  eHouseKeeping
//
//  Created by KhanhNguyen on 6/3/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DatabaseAdapter.h"
#import "RoomAssignmentModelV2.h"
#import "RoomAssignmentAdapterV2.h"
@interface RoomAssignmentAdapterV2Demo : RoomAssignmentAdapterV2 {
    //NSMutableArray *result;
}
-(int)insertRoomAssignmentData:(RoomAssignmentModelV2*) roomAssignment;
-(int)updateRoomAssignmentData:(RoomAssignmentModelV2*) roomAssignment;
-(int)updateClleaningStatusRoomAssignmentModel:(RoomAssignmentModelV2*) roomAssignment;
-(int)deleteRoomAssignmentData:(RoomAssignmentModelV2*) roomAssignment;
-(RoomAssignmentModelV2 *)loadRoomAsignment:(RoomAssignmentModelV2 *)raModel;

//Get last modified
-(NSString*)getRoomAssignmentLastModifiedDateByUserId:(int)userId;
-(NSMutableArray*)loadRoomAssignmentData:(BOOL)isComplete;
-(NSMutableArray *)loadRoomAsignmentByUserID:(RoomAssignmentModelV2 *)raModel;
-(NSMutableArray *)loadRoomAssignmentsByUserId:(NSInteger) userId;
-(NSMutableArray *)loadRoomAssignmentsBySuperVisorId:(int)supervisorId houseKeeperId:(int)houseKeeperId;
-(bool)getRoomAssignmentByUserIDAndRoomId:(RoomAssignmentModelV2*)raModel;
-(bool)getRoomIdByRoomAssignment:(RoomAssignmentModelV2*)raModel;
-(RoomAssignmentModelV2 *)loadRoomAsignmentByRoomIdAndUserID:(RoomAssignmentModelV2 *)raModel;
//@property (nonatomic,strong) NSMutableArray *result;
-(NSInteger)maxPrioprity;
-(NSInteger)minPrioprity;
-(NSInteger) numberOfMustPostRoomAssignmentRecordsWithSupervisorId:(NSInteger) userId;
-(NSInteger) numberOfMustPostRoomAssignmentRecordsWithUserId:(NSInteger)userId ;

-(void) deleteOldRoomAssignmentsWithUserId:(NSInteger ) userId;
-(NSMutableArray *) loadAllCurrentDateRoomAssignmentsWithUserId:(NSInteger) userId;
-(void) setDeleteKey:(NSInteger)deleteId forRoomAssignmentId:(NSInteger) raId andUserId:(NSInteger) userId;
@end
