//
//  RoomManager.h
//  eHouseKeeping
//
//  Created by Khanh Nguyen on 08/06/2011.
//  Copyright 2011 TMA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RoomModelV2.h"
#import "RoomAdapterV2.h"
#import "CleaningStatusModelV2.h"
#import "DateTimeUtility.h"
#import "UserManagerV2.h"
#import "ehkDefines.h"
#import "RoomAssignmentAdapterV2.h"
#import "GuestInfoModelV2.h"
#import "GuestInfoModelV2.h"
#import "SuperRoomModelV2.h"
#import "SupRoomAssignmentAdapterV2.h"
#import "CleaningStatusAdapterV2.h"
#import "RoomStatusAdapterV2.h"
#import "RoomStatusModelV2.h"
#import "RoomRecordAdapterV2.h"
#import "RoomRecordModelV2.h"
#import "RoomRemarkAdapterV2.h"
#import "RoomRemarkModelV2.h"
#import "HousekeeperRecordModelV2.h"
#import "RoomServiceLaterAdapterV2.h"
#import "RoomServiceLaterModelV2.h"
#import "HotelModelV2.h"
#import "HotelAdapterV2.h"
#import "HotelManagerV2.h"
#import "LocationModelV2.h"
#import "LocationAdapterV2.h"
#import "InspectionStatusAdapter.h"
#import "ReassignRoomAssignmentAdapter.h"
#import "RoomRecordDetailAdapterV2.h"
#import "MBProgressHUD.h"
#import "ProfileNoteV2.h"
#import "ProfileNoteTypeV2.h"
#import "ServiceDefines.h"
#import "RoomBlocking.h"
#import "RoomManagerV2.h"


@interface RoomManagerV2Demo : RoomManagerV2 {

}


-(int) updateGuestInfoByUserId:(int)userId hotelId:(int)hotelId roomNumber:(NSString*)roomNumber;

-(BOOL) postGuestInfo:(NSInteger) UsrID WithRoomAssignID:(NSInteger) raID;
-(BOOL) postAllGuestInfoChanged;

-(void) updateLocation;

+(NSString*)getCurrentRoomNo;
+(void)setCurrentRoomNo:(NSString*) roomNo;
+(void)resetCurrentRoomNo;
-(NSMutableArray*)getCleaningStatus;

+(NSInteger)getCurrentCleaningStatus;
+(void)setCurrentCleaningStatus:(NSInteger)cleaningStatus;
+(void)resetCurrentCleaningStatus;


-(void)upDateTimeRoomRecordModel:(int)cleaning_status_id;
-(NSString*)getImgCleaningStatus:(int)cstatID;

#pragma mark - RoomAssignment Model
-(NSInteger)insert:(RoomAssignmentModelV2*)roomAssignment;
-(NSInteger)update:(RoomAssignmentModelV2*)roomAssignment;
-(NSInteger)delete:(RoomAssignmentModelV2*)roomAssignment;
-(void)load:(RoomAssignmentModelV2*)roomAssignment;

//Get last modified of room assignment by user id
-(NSString*)getRoomAssignmentLastModifiedDateByUserId:(int)userId;

//Any change query in this function, you should change "-(int)countAllRoomAssignmentsByUserID:(int)userId" too
-(NSMutableArray*)loadAllRoomAssignmentsByUserID:(RoomAssignmentModelV2*)roomAssignment;
//Any change query in this function, you should change "-(NSMutableArray*)loadAllRoomAssignmentsByUserID:(RoomAssignmentModelV2*)roomAssignment" too
-(int)countAllRoomAssignmentsByUserID:(int)userId;

-(NSMutableArray *) loadAllRoomAssignmentsByUserId:(NSInteger) userId;
-(NSMutableArray *)loadRoomAssignmentsBySuperVisorId:(int)supervisorId houseKeeperId:(NSInteger)houseKeeperId;
-(bool)getRoomAssignmentByUserIDAndRoomID:(RoomAssignmentModelV2*)roomAssignment;
-(bool)getRoomIdByRoomAssignment:(RoomAssignmentModelV2*)roomAssignment;
-(void)loadRoomAsignmentByRoomIdAndUserID:(RoomAssignmentModelV2*)roomAssignment;
-(NSInteger)loadMaxPrioprity;
-(int)reloadRoomStatusWSByUserId:(int)userId roomAssignmentId:(int)assignmentId currentRoomStatusId:(int)currentStatusId;

-(BOOL)getRoomAssignmentWSByUserID:(NSInteger)userID AndIsSupervisor:(BOOL)isSupervisor AndLastModified:(NSString *)strLastModified AndPercentView:(MBProgressHUD *)percentView;
-(int) postRoomAssignmentWSByUserID:(NSInteger) userID AndRaModel:(RoomAssignmentModelV2 *) model;
-(int) postRoomInspectionWSByUserID:(NSInteger) userID AndRaModel:(RoomAssignmentModelV2 *) model;
-(int) reAssignRoomAssignmentWSByReassignRoomAssignmentModel:(ReassignRoomAssignmentModel *) reModel;
-(int)setUnassignedRoomWSByReassignRoomAssignmentModel:(ReassignRoomAssignmentModel *)reModel;
-(NSInteger) updateCleaningStatusWSByUserId:(NSInteger) userId withRoomAssignmentModel:(RoomAssignmentModelV2 *) roomAssignmentModel;

-(NSInteger) numberOfMustPostRoomAssignmentRecordsWithSupervisorId:(NSInteger) userId;
-(NSInteger)numberOfRoomAssignmentMustPost;
-(NSInteger) loadMinPrioprity;
-(void) deleteOldRoomAssignmentsWithUserId:(NSInteger) userId;

//If any change this function in the future must change query of "-(int) countAllRoomAssignmentsWithUserId:(NSInteger) userId" too
-(NSMutableArray *) getAllRoomAssignmentsWithUserId:(NSInteger) userId;
//If any change this function in the future must change query of "-(NSMutableArray *) getAllRoomAssignmentsWithUserId:(NSInteger) userId" too
-(int) countAllRoomAssignmentsWithUserId:(NSInteger) userId;

-(void) setDeleteKey:(NSInteger)deleteId forRoomAssignmentId:(NSInteger) raId andUserId:(NSInteger) userId;
-(void) checkAndRemoveMissingRoomAssignments:(NSMutableArray *) roomAssigns andUserId:(NSInteger) userId;

#pragma mark - === Update Sequence Room Assignment ===
#pragma mark
-(BOOL) changeRoomAssignmentSequenceFromrRoomAssignId:(NSInteger) fromRaId toRoomAssignId:(NSInteger) toRaId forUserId:(NSInteger) userId;
 //An Rework for ReassignRoom
-(void)reassignRoom:(RoomModelV2*) room andRoomAssignmentModelV2:(RoomAssignmentModelV2*) roomAssigns;
#pragma mark - Room Model
-(NSInteger)insertRoomModel:(RoomModelV2*)roomModel;
-(NSInteger)updateRoomModel:(RoomModelV2*)roomModel;
-(void)loadRoomModel:(RoomModelV2*)roomModel;
+(NSMutableArray *)getAllRoomType;
+(NSMutableArray *)getAllRoom;

//Hao Tran - Fix enhance loading data in sync
//-(BOOL) getRoomWSByUserID:(NSInteger) userID AndRaID:(NSInteger) raID AndLastModified:(NSString *) strLastModified AndLastCleaningDate:(NSString *) lastCleaningDate;
-(BOOL) getRoomWSByUserID:(NSInteger) userID AndRaID:(NSInteger) raID AndLastModified:(NSString *) strLastModified shouldUpdateRoomRecord:(BOOL)shouldUpdateRoomRecord;

-(BOOL) isValidRoomWSByUserId:(NSInteger) userID roomNumber:(NSString*)roomNumber hotelId: (NSString*) hotelID;
-(NSMutableArray *) loadAllRoomUserId:(NSInteger) userId;

#pragma mark - Room Blocking
-(int) insertRoomBlocking:(RoomBlocking*)model;
-(int) updateRoomBlocking:(RoomBlocking*)model;
-(int) deleteAllRoomBlockingByUserId:(int)userId;
-(int) unlockAllRoomBlockingByUserId:(int) userId;
-(int) unlockRoomBlockingByUserId:(int) userId roomNumber: (NSString*)roomNumber;
-(RoomBlocking*) loadRoomBlockingByUserId:(int)userId roomNumber:(NSString*) roomNumber;
//checking whether has room blocking data or not
-(int) countRoomBlockingByUserId:(int)userId roomNumber:(NSString*)roomNumber;

-(int) getWSOOSBlockRoomListByUserId:(NSInteger)userId hotelId:(NSInteger)hotelId listRoomAssignmentCompare:(NSMutableArray*)listRoomCompare;
//NOTE: this function must be called after function -(int) getWSOOSBlockRoomListByUserId:(NSInteger)userId hotelId:(NSInteger)hotelId listRoomAssignmentCompare:(NSMutableArray*) listRoomCompare
//it is mandatory
-(int) getWSReleaseRoomListByUserId:(NSInteger)userId hotelId:(NSInteger)hotelId listRoomAssignmentCompare:(NSMutableArray*) listRoomCompare;

//remarkType = RemarkType_RoomRemark => get room remark
//remarkType = RemarkType_PhyshicalCheck => get room remarks physical check
-(NSString*) getRoomRemarksByUserId:(NSInteger)userId hotelId:(NSInteger)hotelId remarkType:(int)remarkType roomNumber:(NSString*)roomNumber;

//remarkType = RemarkType_RoomRemark => update room remark
//remarkType = RemarkType_PhyshicalCheck => update room remarks physical check
-(int) updateRoomRemarksByUserId:(NSInteger)userId hotelId:(NSInteger)hotelId remarkType:(int)remarkType roomNumber:(NSString*)roomNumber remarkValue:(NSString*)remarkValue;

#pragma mark - Guest Info
-(NSInteger)insertGuestInfo:(GuestInfoModelV2*)guestInfoModel;
//-(NSInteger)updateGuestInfo:(GuestInfoModelV2*)guestInfoModel;
-(void)loadGuestInfo:(GuestInfoModelV2*)guestInfoModel;
-(void)loadByRoomIDGuestInfo:(GuestInfoModelV2*)guestInfoModel;
-(GuestInfoModelV2 *)loadByRoomID:(NSString*) roomNumber;
-(NSMutableArray*)loadGuestsByUserId:(int)userId roomNumber:(NSString*)roomNumber guestType:(int)guestType;
-(BOOL)isDueOutRoomByRoomId:(NSInteger) roomId;

//WS ProfileNote
-(BOOL)loadWSProfileNoteByUserId:(int)userId hotelId:(int)hotelId roomNumber:(NSString*)roomNumber;

//Localdatabase
//Guest Info Profile Note Type
-(NSMutableArray*) loadProfileNoteTypesByUserId:(int)userId roomNumber:(NSString*)roomNumber;
-(NSMutableArray*) loadProfileNoteTypesByUserId:(int)userId roomNumber:(NSString*)roomNumber guestId:(int)guestId;
-(int) insertProfileNoteType:(ProfileNoteTypeV2*)model;
-(int) deleteProfileNoteTypeByUserId:(int)userId roomNumber:(NSString*)roomNumber;
-(int) deleteGuestByUserId:(int)userId roomNumber:(NSString*)roomNumber guestType:(int)guestType;
-(int) updateProfileNoteType:(ProfileNoteTypeV2*)model;

//Guest Info Profile Note
-(NSMutableArray*) loadProfileNotesByUserId:(int)userId profileNoteTypeId:(int)profileNoteTypeId;
//Guest Info Profile Note
-(NSMutableArray*) loadProfileNotesByUserId:(int)userId roomNumber:(NSString*)roomNumber guestId:(int)guestId;

-(int) insertProfileNote:(ProfileNoteV2*)model;
-(int) deleteProfileNoteByUserId:(int)userId profileNoteTypeId:(int)profileNoteTypeId;

#pragma mark - SuperRoomModel
-(NSMutableArray *) getAllRoomInspectionWithSupervisorId:(NSInteger) userId;
-(NSMutableArray *) getAllRoomAssignmentDatas:(NSInteger) userId;
-(void)getRoomAssignments;
-(void)getRoomInspection;
-(void)getRoomAssignmentComplete;
-(void)getTotalRoomComlete;
-(void)getRoomInspectionComplete;
-(void)getInspectedRoom;
-(void)getRoomInspectedPassed;
-(void)getRoomInspectedFailed;
-(NSMutableArray*)getRoomTypesCurrentUser;
-(NSString*)getPropertyName:(SuperRoomModelV2*)superRoomModel;
-(NSString*)getbuildingName:(SuperRoomModelV2*)superRoomModel;
-(NSString*)getHouseKeeperName;
-(NSString*)getRoomStatus:(SuperRoomModelV2*)superRoomModel;
-(NSData*)getHotelLogo:(SuperRoomModelV2*)superRoomModel;
-(NSDictionary *) getRoomAssignmentDataByRoomAssignmentId:(NSInteger)roomAssignmentId andRaUserId:(NSInteger)raUserId;
-(NSDictionary *) getRoomAssignmentDataByRoomId:(NSInteger)roomId andRaUserId:(NSInteger)raUserId;
-(NSDictionary *) getRoomAssignmentDataByRoomAssignmentId:(NSInteger)roomAssignmentId andRaUserId:(NSInteger)raUserId isFilterCurrentDate:(BOOL)isFilterCurrentDate;

#pragma mark - Cleaning Status Model
-(int)insertCleaningStatus:(CleaningStatusModelV2*)cleaningStatus;
-(void)loadCleaningStatus:(CleaningStatusModelV2*)cleaningStatus;
-(int)updateCleaningStatus:(CleaningStatusModelV2*)cleaningStatus;
-(NSMutableArray*)loadAllCleaningStatus;
-(NSMutableArray*)loadAllCleaningStatusWithCleaningStatusID:(NSInteger)clsID;
-(NSMutableArray *)loadAllCleaningStatusForSupervisor;
-(NSString*)getCleaningStatusLastModifiedDate;

//-(NSMutableArray*)loadMaidCleaningStatusByRoomStatus:(BOOL)isOcupied;
//-(NSMutableArray*)loadSuperVisorCleaningStatusByRoomStatus:(BOOL)isOcupied;

-(BOOL) getAllCleaningStatusWS:(NSString *) strLastModified AndPercentView:(MBProgressHUD*)percentView;

// Loacation Model
/*
-(int)insertLocationModel:(LocationModel*)locationModel;
-(int)updateLocationModel:(LocationModel*)locationModel;
-(void)loadLocationModel:(LocationModel*)locationModel;
*/
#pragma mark - Room Record Model
-(int) insertRoomRecordData:(RoomRecordModelV2 *) record;
-(int) updateRoomRecordData:(RoomRecordModelV2 *) record;
-(int) deleteRoomRecordData:(RoomRecordModelV2 *) record;
-(RoomRecordModelV2 *) loadRoomRecordData:(RoomRecordModelV2 *) record;
-(RoomRecordModelV2 *) loadRoomRecordDataByRoomIdAndUserId:(RoomRecordModelV2 *) record;
-(NSMutableArray *) loadAllRoomRecordByCurrentUser;
-(NSMutableArray *) loadAllRoomRecord;

//Hao Tran - Remove for unneccessary function
//-(NSInteger) getCleaningPauseTime:(NSInteger) raID AndUserID:(NSInteger) userID;
//-(NSInteger) getInspectedPauseTime:(NSInteger) raID AndUserID:(NSInteger) userID; 

-(NSInteger)numberOfRoomServiceLaterMustPost;

#pragma mark - room record detail

-(int) insertRoomRecordDetailData:(RoomRecordDetailModelV2 *) record;
-(int) updateRoomRecordDetailData:(RoomRecordDetailModelV2 *) record;
-(int) deleteRoomRecordDetailData:(RoomRecordDetailModelV2 *) record;
-(RoomRecordDetailModelV2 *) loadRoomRecordDetailData:(RoomRecordDetailModelV2 *) record;
-(RoomRecordDetailModelV2 *) loadRoomRecordDataDetailByRoomAssignmentIdAndUserId:(RoomRecordDetailModelV2 *) record;
-(NSInteger) numberOfRoomRecordDetails:(RoomRecordDetailModelV2 *) record;
-(NSMutableArray *) loadAllRoomRecordDetailsByRoomAssignmentId:(NSInteger) raId andUserId:(NSInteger) userId;
-(NSInteger) numberOfRoomRecordDetailMustSyn;
-(NSMutableArray *) loadAllRoomRecordDetailByCurrentUser:(NSInteger)userID;
-(BOOL)postRoomRecordDetail:(RoomRecordDetailModelV2*)roomRecordDetail;
-(BOOL)getRoomRecordDetail:(NSInteger)userId andLastModified:(NSString*)lastModified;

#pragma mark - Room Status Model
+(NSString*)getRoomStatusCodeByEnum:(enum ENUM_ROOM_STATUS)roomStatus;

-(int) insertRoomStatusData:(RoomStatusModelV2 *) status;
-(int) updateRoomStatusData:(RoomStatusModelV2 *) status;
-(int) deleteRoomStatusData:(RoomStatusModelV2 *) status;
//get last modified date RS
-(NSString*)getRoomStatusLastModifiedDate;
-(RoomStatusModelV2 *) loadRoomStatusData:(RoomStatusModelV2 *) status;
-(NSMutableArray *) loadAllRoomStatus;
-(NSMutableArray *) loadAllMaidRoomStatusByIsOcupied:(BOOL)isOcupied;
-(NSMutableArray *) loadAllSuperVisorRoomStatusByIsOcupied:(BOOL)isOcupied;
-(RoomStatusModelV2 *) loadRoomStatusByStatusNameData:(RoomStatusModelV2 *) status;
-(NSMutableArray *) loadAllSuperVisorBlockRoomStatusByIsOcupied:(BOOL)isOcupied;
-(NSMutableArray *) loadAllSuperVisorRoomStatusByIsOcupied:(BOOL)isOcupied andChekList:(NSInteger)tchk;
-(BOOL) getAllRoomStatusWS:(NSString *) strLastModified AndPercentView:(MBProgressHUD*)percentView;

//conrad
-(NSMutableArray *) loadAllMaidRoomStatus;

#pragma mark - inspection Status Model
-(BOOL) insertInspectionStatusModel:(InspectionStatusModel *)model;
-(InspectionStatusModel *) loadInspectionStatusModelByPrimaryKey:(int)inspectionStatusId;
-(BOOL) updateInspectionStatusModel:(InspectionStatusModel *) model;
-(BOOL) deleteInspectionStatusModel:(InspectionStatusModel *) model;
-(void) getInspectionStatusWSWithUserId:(NSInteger) userId andLastModified:(NSString *) lastModified AndPercentView:(MBProgressHUD*)percentView;

#pragma mark - Room Remark Model
-(int) insertRoomRemarkData:(RoomRemarkModelV2 *) remark;
-(int) updateRoomRemarkData:(RoomRemarkModelV2 *) remark;
-(int) deleteRoomRemarkData:(RoomRemarkModelV2 *) remark;
-(RoomRemarkModelV2 *) loadRoomRemarkData:(RoomRemarkModelV2 *) remark;
-(RoomRemarkModelV2 *) loadRoomRemarkDataByRecordId:(RoomRemarkModelV2 *) remark;
-(int)countRoomRemarkData:(NSInteger)recordID;
-(NSMutableArray *) loadAllRoomRemark;
-(NSMutableArray *) loadAllRoomRemarkByRecordId:(int) recordId;
-(BOOL) postRoomRemarkWSWithRoomRemark:(RoomRemarkModelV2 *) remark WithUserId:(NSInteger) userId andRoomAssignmentId:(NSInteger) raId;
-(NSInteger) numberOfMustPostGuestInfoRecordsWithUserId:(NSInteger) userId;

#pragma mark - Room Service Later Model
-(int) insertRoomServiceLaterData:(RoomServiceLaterModelV2 *) service;
-(int) updateRoomServiceLaterData:(RoomServiceLaterModelV2 *) service;
-(int) deleteRoomServiceLaterData:(RoomServiceLaterModelV2 *) service;
-(RoomServiceLaterModelV2 *) loadRoomServiceLaterData:(RoomServiceLaterModelV2 *) service;
-(RoomServiceLaterModelV2 *) loadRoomServiceLaterDataByRecordId:(RoomServiceLaterModelV2 *) service;
-(NSMutableArray *) loadAllRoomServiceLaterByRecordId:(int) recordId;
-(NSMutableArray *) loadAllRoomServiceLater;

#pragma mark - === ReAssign Room Assignment ===
#pragma mark
-(BOOL) insertReassignRoomAssignmentModel:(ReassignRoomAssignmentModel *)model;
-(ReassignRoomAssignmentModel *) loadReassignRoomAssignmentModelByPrimaryKey:(ReassignRoomAssignmentModel *) model;
-(BOOL) updateReassignRoomAssignmentModel:(ReassignRoomAssignmentModel *) model;
-(BOOL) deleteReassignRoomAssignmentModel:(ReassignRoomAssignmentModel *) model;
-(NSInteger) numberOfMustPostReassignRecordsWithUserId:(NSInteger) userId;
-(NSMutableArray *) loadAllReassignRoomAssignmentModelByUserId:(NSInteger) userId;
-(NSInteger)updateRoomHaveBeenRemove:(NSInteger) userID AndRaModel:(RoomAssignmentModelV2 *) model;
//-(BOOL)GetFindRoomAssignmentListWSByUserID:(NSInteger)userID AndRoomstatus:(NSInteger)roomStatusID AndLastModified:(NSString *)strLastModified ;
-(BOOL)GetFindRoomAssignmentListWSByUserID:(NSInteger)userID AndRoomstatus:(NSInteger)roomStatusID AndFilterType:(NSInteger)filterType AndLastModified:(NSString *)strLastModified AndPercentView:(MBProgressHUD*)percentView;
-(BOOL)GetFindRoomAssignmentListWSByUserID:(NSInteger)userID AndFloorId:(NSInteger)floorId AndRoomstatus:(NSInteger)roomStatusID AndFilterType:(NSInteger)filterType AndPercentView:(MBProgressHUD*)percentView;
-(BOOL)GetFindRoomDetailsListWSByUserID:(NSInteger)userID AndFloorId:(NSInteger)floorId AndBuildingId:(NSInteger)buildingId AndHotelId:(NSInteger)HTID AndFilterType:(NSInteger) filterType AndGeneralFilterType:(NSString*) generalFilterType AndPercentView:(MBProgressHUD*)percentView;
-(void)getFloorListFromWSWith:(NSInteger)userId AndHotelId:(NSInteger)hotelId AndBuildingId:(NSInteger)buildingId AndRoomStatus:(NSInteger)statusId AndFilterType:(NSInteger)filterType AndPercentView:(MBProgressHUD *)percentView;

-(NSMutableArray*)getOnlineFloorListWith:(NSInteger)userId AndHotelId:(NSInteger)hotelId AndBuildingId:(NSInteger)buildingId AndRoomStatus:(NSInteger)statusId AndFilterType:(NSInteger)filterType AndPercentView:(MBProgressHUD *)percentView;

-(NSMutableArray *) getAllRoomInspectionWithSupervisorId:(NSInteger) userId andRoomStatusID:(NSInteger)roomID;
-(NSMutableArray *) getAllRoomByFloorId:(NSInteger)floorId AndUserId:(NSInteger)userId AndRoomStatus:(NSInteger)roomStatusId;
-(NSMutableArray *) getAllRoomByFloorId:(NSInteger)floorId userId:(NSInteger)userId filterType:(NSInteger)filterType statusId:(NSInteger)statusId;
-(NSMutableArray *) getUnassginRoomsByFloorId:(NSInteger)floorId AndUserId:(NSInteger)userId;
-(NSMutableArray *) getUnassginRoomsByUserId:(NSInteger)userId;
-(NSMutableArray *) getAllRoomByFloorId:(NSInteger)floorId AndUserId:(NSInteger)userId;
-(NSMutableArray *) getAllOfRoomByFloorId:(NSInteger)floorId AndUserId:(NSInteger)userId;
-(NSMutableArray *) getAllRoomByFloorId:(NSInteger)floorId AndUserId:(NSInteger)userId AndCleaningStatus:(NSInteger)cleaningStatusId;
-(NSMutableArray *) getAllRoomByFloorId:(NSInteger)floorId AndUserId:(NSInteger)userId AndKindOfRoom:(NSInteger)kindOfRoom;
-(NSMutableArray *) getAllRoomByRoomByUserId:(NSInteger)userId AndKindOfRoom:(NSInteger)kindOfRoom;
-(NSMutableArray*)getOnlineFindFloorDetailListWith:(NSInteger)userId AndHotelId:(NSInteger)hotelId AndBuildingId:(NSInteger)buildingId AndGeneralFilter:(NSString*)generalFilter AndFilterType:(NSInteger)filterType AndPercentView:(MBProgressHUD *)percentView;

-(NSMutableArray *) getAllRoomByFloorId:(NSInteger)floorId userId:(NSInteger)userId filterType:(NSInteger)filterType generalFilter:(NSString*)generalFilter;
-(NSMutableArray *) getRoomDetailsListByFloorId:(NSInteger)floorId userId:(NSInteger)userId filterType:(NSInteger)filterType generalFilter:(NSString*)generalFilter;
#pragma mark - Alert Room Behind Schedule & Rush/Queue Room

//Return list of PopupMsgItem
-(NSMutableArray*) getPopUpMessageFromWSByUserId:(int)userId lastModified:(NSString*)lastModified;

//List ID is NSString with comma between them e.g 1,2,3,4 ...
-(int) updateReadMessagePopUpByUserId:(int)userId listID:(NSString*)listIds;
#pragma mark - Post WSLog
-(BOOL)postWSLog:(int)userId message:(NSString*)messageValue messageType:(int)type;
-(int)updateIsCheckedRa: (NSInteger)userID houseKeeperId:(NSString*)houseKeeperId;
-(int)updateIsCheckedFind: (NSInteger)raID;
@end






