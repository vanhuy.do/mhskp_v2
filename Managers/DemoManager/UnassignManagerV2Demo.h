//
//  UnassignManagerV2.h
//  mHouseKeeping
//
//  Created by TMS TRIBE on 3/12/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZoneAdapterV2.h"
#import "FloorAdapterV2.h"
#import "ZoneModelV2.h"
#import "RoomModelV2.h"
#import "RoomAdapterV2.h"

#import "UnassignroomAdapterV2.h"
#import "AssignRoomAdapterV2.h"
#import "MBProgressHUD.h"
#import "UnassignManagerV2.h"

#define LOADING_GET_ZONE_ROOM_LIST @"Get Zone Room"
#define LOADING_GET_FLOOR_LIST @"Loading Floor List"

@interface UnassignManagerV2Demo : UnassignManagerV2{
    
}

#pragma mark - 
#pragma mark Floor
-(NSInteger) insertFloor:(FloorModelV2*)floorModelV2;
-(NSInteger) updateFloor:(FloorModelV2*)floorModelV2;
-(int)deleteFloor:(FloorModelV2*)floorModelV2;
-(BOOL)isExistFloor:(FloorModelV2*)floorModelV2;
-(NSMutableArray*)loadFloorsByFloorId:(FloorModelV2*)floorModelV2;
-(FloorModelV2*)loadFloorByFloorId:(FloorModelV2*)floorModelV2;
-(NSMutableArray *)loadFloors;
-(NSMutableArray *)loadFloorsHaveUnAssignRoom;
-(NSMutableArray *)loadFloorsHaveFindRoom:(NSInteger)userId andRoomStatusID:(NSInteger)roomStatusId;
-(NSMutableArray *)loadFloorsHaveFindCleaningStatus:(NSInteger)userId andCleaningStatusID:(NSInteger)cleaningStatusId;
-(NSMutableArray *)loadFloorsHaveFindKindOfRoom:(NSInteger)userId andKindOfRoom:(NSInteger)kindOfRoomId;

-(NSMutableArray *)loadFloorsByUserId:(NSInteger)userId filterType:(NSInteger)filterType statusId:(NSInteger)statusId;

-(NSMutableArray *)loadFloorsHaveFindInspectedRoom:(NSInteger)userId;
-(NSInteger) numberOfUnAssignRoomOnFloor:(NSInteger)floorId;
-(NSInteger) numberOfUnAssignRoomOnZone:(NSInteger)zoneId;
-(NSString*)getFloorLastModifiedDate;

#pragma mark - 
#pragma mark Zone
-(NSInteger) insertZone:(ZoneModelV2*)zoneModelV2;
-(NSInteger) updateZone:(ZoneModelV2*)zoneModelV2;
-(int)deleteZone:(ZoneModelV2*)zoneModelV2;
-(NSMutableArray*)loadZonesByFloorId:(ZoneModelV2*)zoneModelV2;
-(ZoneModelV2*)loadZonesByZoneId:(ZoneModelV2*)zoneModelV2;
-(ZoneModelV2*)getZoneById:(int)zoneId;

//get last modified date zone
-(NSString*)getZoneLastModifiedDate;
-(NSMutableArray*)loadRoombyZoneId:(ZoneModelV2*)zoneModel;


-(void):(UserModelV2*)_user andLastModified:(NSString*)lastModified AndPercentView:(MBProgressHUD*)percentView;
-(int)postAssignRoom:(AssignRoomModelV2*)assignRoom;

#pragma mark - 
#pragma mark Unassign

-(int)insertUnassignRoom:(UnassignModelV2*) unassignModel;
-(UnassignModelV2 *)loadUnassignRoomById:(UnassignModelV2*)unassignModel;
-(NSMutableArray *)loadUnassignRoomByFloorId:(UnassignModelV2*)unassignModel;
-(NSInteger)loadFloorIDByZoneId:(ZoneModelV2*)zoneItem;
-(NSInteger)deleteUnassign:(UnassignModelV2*) unassignModel;
-(NSInteger)deleteUnassignByRoomId:(NSString*) roomNumber;
#pragma mark - 
#pragma mark Assign

-(int)insertAssignRoom:(AssignRoomModelV2*) assignModel;
-(AssignRoomModelV2 *)loadAssignRoomById:(AssignRoomModelV2*)assignModel;
-(int)updateAssignRoom:(AssignRoomModelV2*) assignModel ;
-(void)getUnassignRoomListWithUser:(UserModelV2*)_user andLastModified:(NSString*)lastmodified;
-(NSMutableArray *)loadUnassignRoomByZoneId:(UnassignModelV2*)unassignModel;
-(void)getZoneRoomListWithUser:(UserModelV2*)_user andlastModified:(NSString*)lastModidfied AndPercentView:(MBProgressHUD*)percentView;
-(NSInteger)deleteAssign:(AssignRoomModelV2*) assignModel;
-(NSMutableArray *)loadAllAssignRoomByUserId:(NSInteger)userID;

-(NSInteger) numberOfAssignRoomMustSyn;

-(BOOL)isExistInAssignRoom:(NSInteger)assignID;
@end
