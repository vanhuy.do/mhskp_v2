//
//  UnassignManagerV2.m
//  mHouseKeeping
//
//  Created by TMS TRIBE on 3/12/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "UnassignManagerV2Demo.h"
#import "LogFileManager.h"
#import "LogObject.h"


@implementation UnassignManagerV2Demo


// get floor list
-(void)getFloorListFromWSWith:(UserModelV2*)_user andLastModified:(NSString*)lastModified AndPercentView:(MBProgressHUD *)percentView{
  
    NSMutableArray *buildingList = [NSMutableArray array];    
    ////// get building list from service : //////
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole])
    {
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_GetBuildingList *requestBL = [[eHousekeepingService_GetBuildingList alloc] init];
    [requestBL setIntHotelID:[NSNumber numberWithInt:_user.userHotelsId]];
    
    //Hao Tran remove
//    if(lastModified){
//      [requestBL setStrLastModified:lastModified];      
//    }
    //Hao Tran remove - END
    
//    LogObject *logObj = [[LogObject alloc] init];
//    logObj.dateTime = [NSDate date];
//    logObj.log = [NSString stringWithFormat:@"<%i><getFloorListFromWSWith><intHotelID:%i lastModified:%@>",_user.userId,_user.userHotelsId,lastModified];
//    LogFileManager *logManager = [[LogFileManager alloc] init];
//    [logManager save:logObj];
    
    float currentPercent = 0;
    [self updatePercentView:percentView value:currentPercent description:LOADING_GET_FLOOR_LIST];
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetBuildingListUsingParameters:requestBL];
    
    NSArray *responseBodyParts = response.bodyParts;
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetBuildingListResponse class]]){
            
             eHousekeepingService_GetBuildingListResponse *dataBody = (eHousekeepingService_GetBuildingListResponse *)bodyPart; 
             if (dataBody) {
             if ([dataBody.GetBuildingListResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]]) {
                 NSMutableArray *unassignListObject = dataBody.GetBuildingListResult.BuildingList.Building;
                 float stepPercent = [self caculateStepFromCountingObject:((float)[unassignListObject count]/2.0f)];
                 stepPercent = (stepPercent > 50) ? 50 : stepPercent;
                 
                 for (eHousekeepingService_Building *item in unassignListObject) {
                     [buildingList addObject:item];
                     currentPercent += stepPercent;
                     currentPercent = (currentPercent > 50 ) ? 50 : currentPercent;
                     [self updatePercentView:percentView value:currentPercent description:LOADING_GET_FLOOR_LIST];
                   }
                }
            }
        }
    }
    
    
    // get roomAssignment and get buildingId from roomassignment
    RoomAssignmentModelV2 *roomAssignment = [[RoomAssignmentModelV2 alloc] init];
    roomAssignment.roomAssignment_UserId = [UserManagerV2 sharedUserManager].currentUser.userId;
    
    NSMutableArray *roomAssignmentList = [[RoomManagerV2 sharedRoomManager] loadAllRoomAssignmentsByUserID:roomAssignment];
    if([roomAssignmentList count] > 0){
      roomAssignment = (RoomAssignmentModelV2*)[roomAssignmentList objectAtIndex:0];  
    }
    
    RoomModelV2 *roomModel = [[RoomModelV2 alloc] init];
    roomModel.room_Id = roomAssignment.roomAssignment_RoomId;
    [[RoomManagerV2 sharedRoomManager] loadRoomModel:roomModel];
    
    // get floor from buiding list
    eHousekeepingService_GetFloorList *requestFL = [[eHousekeepingService_GetFloorList alloc] init];    
    [requestFL setIntHotelID:[NSNumber numberWithInt:_user.userHotelsId]];

    //Hao Tran - Hard code building id if BuildingId doesn't has data
    if(roomModel.room_Building_Id <= 0){
       [requestFL setIntBuildingID:[NSNumber numberWithInt:1]];
    } else {
        [requestFL setIntBuildingID:[NSNumber numberWithInt:(int)roomModel.room_Building_Id]];
    }
    
    //Hao Tran - Add parameter for get Floor with last modified
    [requestFL setStrLastModified: lastModified];
    
    currentPercent = 50;
    [self updatePercentView:percentView value:currentPercent description:LOADING_GET_FLOOR_LIST];
    eHousekeepingServiceSoap12BindingResponse *responseFL = [binding GetFloorListUsingParameters:requestFL];
    
    responseBodyParts = responseFL.bodyParts;
    
    for (id bodyPart in responseBodyParts) {
        if([bodyPart isKindOfClass:[eHousekeepingService_GetFloorListResponse class]]){
            eHousekeepingService_GetFloorListResponse *dataBody = 
            (eHousekeepingService_GetFloorListResponse*)bodyPart;
            if(dataBody){
                if([dataBody.GetFloorListResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]] ){
                    
                    NSMutableArray *floorList = dataBody.GetFloorListResult.FloorList.Floor;
                    float stepPercent = [self caculateStepFromCountingObject:((float)[floorList count]/2.0f)];
                    stepPercent = (stepPercent > 50) ? 50 : stepPercent;
                    
                    for (eHousekeepingService_Floor *item in floorList) {
                        FloorModelV2 *_floor = [[FloorModelV2 alloc] init];
                        _floor.floor_id = [item.flID intValue];
                        _floor.floor_building_id = [item.flBuildingID intValue];
                        _floor.floor_name = item.flName;
                        _floor.floor_name_lang = item.flLang;
                        _floor.floor_last_modified = item.flLastModifed;
                        
                        //Check to update floor
                        if([[UnassignManagerV2 sharedUnassignManager] isExistFloor:_floor]){
                            [[UnassignManagerV2 sharedUnassignManager] updateFloor:_floor];
                        }
                        else{
                            [[UnassignManagerV2 sharedUnassignManager] insertFloor:_floor];
                        }
                        
                        currentPercent += stepPercent;
                        currentPercent = (currentPercent > 50 ) ? 50 : currentPercent;
                        [self updatePercentView:percentView value:currentPercent description:LOADING_GET_FLOOR_LIST];
                    }
                    
                }
            }
        }        
    }
}

-(void)getZoneRoomListWithUser:(UserModelV2*)_user andlastModified:(NSString*)lastModidfied AndPercentView:(MBProgressHUD *)percentView{
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    if([LogFileManager isLogConsole]){
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_GetZoneList *request = [[eHousekeepingService_GetZoneList alloc] init];
    NSMutableArray *zoneList = [NSMutableArray array];
    [request setIntHotelID:[NSNumber numberWithInt:_user.userHotelsId]];
    
    if(lastModidfied){
        [request setStrLastModified:lastModidfied];
    }
    
//    LogObject *logObj = [[LogObject alloc] init];
//    logObj.dateTime = [NSDate date];
//    logObj.log = [NSString stringWithFormat:@"<%i><getZoneRoomListWithUser><intHotelID:%i lastModified:%@>",_user.userId,_user.userHotelsId,lastModidfied];
//    LogFileManager *logManager = [[LogFileManager alloc] init];
//    [logManager save:logObj];
    
    float currentPercent = 0;
    [self updatePercentView:percentView value:currentPercent description:LOADING_GET_ZONE_ROOM_LIST];
    
    if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL){return;}
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetZoneListUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetZoneListResponse class]]){
            
            eHousekeepingService_GetZoneListResponse *dataBody = (eHousekeepingService_GetZoneListResponse *)bodyPart; 
            if (dataBody) {
                if ([dataBody.GetZoneListResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]]) {
                    NSMutableArray *zoneListObject = dataBody.GetZoneListResult.ZnLst.ZoneList;
                    for (eHousekeepingService_ZoneList *item in zoneListObject) {
                        if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL){return;}
                        [zoneList addObject:item];
                    }
                }
            }
        }
    }
    
    float stepPercent = [self caculateStepFromCountingObject:(int)[zoneList count]];
    for (eHousekeepingService_ZoneList *item in zoneList) {
        if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL){return;}
        ZoneModelV2 *zoneModel = [[ZoneModelV2 alloc] init];
        zoneModel.zone_id = [item.znID intValue];
        zoneModel.zone_name = item.znName;
        zoneModel.zone_last_modified = item.znLastModified;
        zoneModel.zone_floor_id = [[UnassignManagerV2 sharedUnassignManager] loadFloorIDByZoneId:zoneModel];
       
        //Hao Tran check existing zone to update
        if([[UnassignManagerV2 sharedUnassignManager] getZoneById:(int)zoneModel.zone_id]){
            [[UnassignManagerV2 sharedUnassignManager] updateZone:zoneModel];
        }
        else{
            [[UnassignManagerV2 sharedUnassignManager] insertZone:zoneModel];
        }
        
        [self updatePercentView:percentView value:currentPercent description:LOADING_GET_ZONE_ROOM_LIST];
        currentPercent += stepPercent;
    }
}

-(int)postAssignRoom:(AssignRoomModelV2*)assignRoom{
    
    int result = 0;
    eHousekeepingServiceSoapBinding *binding = [eHousekeepingService eHousekeepingServiceSoapBinding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole])
    {
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_AssignRoom *request = [[eHousekeepingService_AssignRoom alloc] init] ;
    [request setIntDutyAssignID:[NSNumber numberWithInt:(int)assignRoom.assignRoom_assign_id ]];
    [request setIntHousekeeperID:[NSNumber numberWithInt:(int)assignRoom.assignRoom_HouseKeeper_Id]];
    [request setIntSupervisorID:[NSNumber numberWithInt:[UserManagerV2 sharedUserManager].currentUser.userId]];
    [request setStrAssignDateTime:assignRoom.assignRoom_Assign_Date];
    [request setStrRemark:assignRoom.assignRoom_Remark];
    
    if (assignRoom.assignRoom_post_status != POST_STATUS_SAVED_UNPOSTED) {
        
         result = NO;
    }
    eHousekeepingServiceSoapBindingResponse *response = [binding AssignRoomUsingParameters:request];
//    NSArray *responseHeaders = response.headers;
//    for (id header in responseHeaders) {
//    }
    
    NSArray *responseBody = response.bodyParts;
    for (id bodyPart in responseBody) {
        if ([bodyPart isKindOfClass:[SOAPFault class]]) {
            continue;
        }
        
        // UpdateAsignRoom        
        if ([bodyPart isKindOfClass:[eHousekeepingService_AssignRoomResponse class]]) {
            eHousekeepingService_AssignRoomResponse *body = (eHousekeepingService_AssignRoomResponse *)bodyPart;
            
            if ([body.AssignRoomResult.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]]) {
                assignRoom.assignRoom_post_status = POST_STATUS_POSTED;
                
                [[UnassignManagerV2 sharedUnassignManager] deleteAssign:assignRoom];
                
                UnassignModelV2 *unassignModel = [[UnassignModelV2 alloc] init];
                unassignModel.unassignroom_id = assignRoom.assignRoom_assign_id;
                [[UnassignManagerV2 sharedUnassignManager] deleteUnassign:unassignModel];
                
                result = RESPONSE_STATUS_OK;
            } else {
                
                result = [body.AssignRoomResult.respCode intValue];
            }
        }
    }
    
    return result;

}

-(void)getUnassignRoomListWithUser:(UserModelV2*)_user andLastModified:(NSString*)lastmodified{
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole]) {
        [binding setLogXMLInOut:YES];
    }
    eHousekeepingService_GetUnassignRoomList *request = [[eHousekeepingService_GetUnassignRoomList alloc] init];
    if(_user.userId){
        [request setIntUsrID:[NSNumber numberWithInt:_user.userId]];
    }
    
    if(_user.userHotelsId){
        [request setIntHotelID:[NSNumber numberWithInt:_user.userHotelsId]];
    }
    
//    if(lastmodified){
//        [request setStrLastModified:lastmodified];
//    }
    
//    LogObject *logObj = [[LogObject alloc] init];
//    logObj.dateTime = [NSDate date];
//    logObj.log = [NSString stringWithFormat:@"<%i><getUnassignRoomListWithUser><intUsrID:%i intHotelID:%i lastModified:%@>",_user.userId,_user.userId,_user.userHotelsId,lastmodified];
//    LogFileManager *logManager = [[LogFileManager alloc] init];
//    [logManager save:logObj];
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetUnassignRoomListUsingParameters:request];
    NSArray *responseBodyParts = response.bodyParts;
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetUnassignRoomListResponse class]]){
            eHousekeepingService_GetUnassignRoomListResponse *dataBody = 
            (eHousekeepingService_GetUnassignRoomListResponse*)bodyPart;
            if(dataBody){
                if([dataBody.GetUnassignRoomListResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]] ){
                    NSMutableArray *unassignRoomList = dataBody.GetUnassignRoomListResult.UnAssgnLst.UnassignRoomList;
                    
                    for (eHousekeepingService_UnassignRoomList *item in unassignRoomList) {
                        UnassignModelV2 *model = [[UnassignModelV2 alloc] init];
                        model.unassignroom_id  = [item.uarRoomAssignID intValue];
                        model.unassignroom_floor_id = [item.uarFloorID intValue];
                        model.unassignroom_hotel_id = [item.uarHotelID intValue];
                        model.unassignroom_zone_id = [item.uarZoneID intValue];
                        model.unassignroom_room_status_id = [item.uarRoomStatusID intValue];
                        model.unassignroom_room_id = item.uarRoomNo;
                        model.unassignroom_guest_first_name = item.uarGuestFirstName;
                        model.unassignroom_guest_last_name = item.uarGuestLastName;
                        model.unassignroom_room_kind = [item.uarVIP intValue];
                        
                        //Hao Tran[20130527/Fix duplicate Room] - Fix duplicate room number in the past
                        [[UnassignManagerV2 sharedUnassignManager] deleteUnassignByRoomId:model.unassignroom_room_id];
                        //Hao Tran[20130527/Fix duplicate Room] - END
                        
                        // insert unassign room
                        [[UnassignManagerV2 sharedUnassignManager] insertUnassignRoom:model];
                    }
                }
            }
        }
    }
}

#pragma mark - 
#pragma mark Unassign
-(NSInteger)loadFloorIDByZoneId:(ZoneModelV2*)zoneItem{
    UnassignroomAdapterV2 *adapter = [[UnassignroomAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    int result = (int)[adapter loadFloorIDByZoneId:zoneItem];
    [adapter close];
    //    [adapter release];
    adapter = nil;
    return result;
    
}

-(int)insertUnassignRoom:(UnassignModelV2*) unassignModel {
    UnassignroomAdapterV2 *adapter = [[UnassignroomAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    int result = [adapter insertUnassignRoom:unassignModel];
    [adapter close];
    //    [adapter release];
    adapter = nil;
    return result;

}

-(NSMutableArray*)loadRoombyZoneId:(ZoneModelV2*)zoneModel{
    UnassignroomAdapterV2 *adapter = [[UnassignroomAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter resetSqlCommand];
//    NSMutableArray* result = [adapter loadUnassignRoomByZoneId:zoneModel];
    NSMutableArray* result = [adapter loadUnassignRoomByZoneIdOrderByName:zoneModel];
    //    [adapter release];
    adapter = nil;
    return result;    
}

-(NSInteger)deleteUnassign:(UnassignModelV2*) unassignModel{
    UnassignroomAdapterV2 *adapter = [[UnassignroomAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    NSInteger result = [adapter deleteUnAssignData:unassignModel];
    //    [adapter release];
    adapter = nil;
    return result;    
}

-(NSInteger)deleteUnassignByRoomId:(NSString*) roomNumber{
    UnassignroomAdapterV2 *adapter = [[UnassignroomAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    NSInteger result = [adapter deleteUnAssignRoomByRoomId:roomNumber];
    //    [adapter release];
    adapter = nil;
    return result;
}

-(UnassignModelV2 *)loadUnassignRoomById:(UnassignModelV2*)unassignModel{
    UnassignroomAdapterV2 *adapter = [[UnassignroomAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    UnassignModelV2* result = [adapter loadUnassignRoomById:unassignModel];
    [adapter close];
    //    [adapter release];
    adapter = nil;
    return result;

}

-(NSMutableArray *)loadUnassignRoomByFloorId:(UnassignModelV2*)unassignModel{
    UnassignroomAdapterV2 *adapter = [[UnassignroomAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    NSMutableArray* result = [adapter loadUnassignRoomByFloorId:unassignModel];
    [adapter close];
    //    [adapter release];
    adapter = nil;
    return result;

}

-(NSMutableArray *)loadUnassignRoomByZoneId:(UnassignModelV2*)unassignModel{
    UnassignroomAdapterV2 *adapter = [[UnassignroomAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    NSMutableArray* result = [adapter loadUnassignRoomByFloorId:unassignModel];
    [adapter close];
    //    [adapter release];
    adapter = nil;
    return result;
    
}
#pragma mark - 
#pragma mark Assign

-(int)insertAssignRoom:(AssignRoomModelV2*) assignModel {
    AssignRoomAdapterV2 *adapter = [[AssignRoomAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    int result = [adapter insertAssignRoom:assignModel];
    [adapter close];
    //    [adapter release];
    adapter = nil;
    return result;

}

-(int)updateAssignRoom:(AssignRoomModelV2*) assignModel {
    AssignRoomAdapterV2 *adapter = [[AssignRoomAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    int result = [adapter updateAssignRoomData:assignModel];
    [adapter close];
    //    [adapter release];
    adapter = nil;
    return result;
    
}


-(AssignRoomModelV2 *)loadAssignRoomById:(AssignRoomModelV2*)assignModel{
    AssignRoomAdapterV2 *adapter = [[AssignRoomAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    AssignRoomModelV2* result = [adapter loadAssignRoomById:assignModel];
    [adapter close];
    //    [adapter release];
    adapter = nil;
    return result;
}

-(NSInteger)deleteAssign:(AssignRoomModelV2*)assignModel{
    AssignRoomAdapterV2 *adapter = [[AssignRoomAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    NSInteger result = [adapter deleteAssignData:assignModel];
    [adapter close];
    //    [adapter release];
    adapter = nil;
    return result;
    
}

-(NSMutableArray *)loadAllAssignRoomByUserId:(NSInteger)userID {
    AssignRoomAdapterV2 *adapter = [[AssignRoomAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    NSMutableArray *result = [adapter loadAllAssignRoomByUserId:userID];
    [adapter close];
    //    [adapter release];
    adapter = nil;
    return result;
}

-(BOOL)isExistInAssignRoom:(NSInteger)assignID{
    AssignRoomAdapterV2 *adapter = [[AssignRoomAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    BOOL result = [adapter isExistInAssignRoom:assignID];
    [adapter close];
    //    [adapter release];
    adapter = nil;
    return result;

}
#pragma mark - 
#pragma mark Floor
-(NSInteger) insertFloor:(FloorModelV2*)floorModelV2{
    FloorAdapterV2 *adapter = [FloorAdapterV2 createFloorAdapter];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    int result = [adapter insertFloor:floorModelV2];
    [adapter close];
//    [adapter release];
    adapter = nil;
    return result;
}

-(NSInteger) updateFloor:(FloorModelV2*)floorModelV2{
    FloorAdapterV2 *adapter = [FloorAdapterV2 createFloorAdapter];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    int result = [adapter updateFloor:floorModelV2];
    [adapter close];
//    [adapter release];
    adapter = nil;
    return result;
    
}

-(int)deleteFloor:(FloorModelV2*)floorModelV2{
    FloorAdapterV2 *adapter = [FloorAdapterV2 createFloorAdapter];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    int result = [adapter deleteFloor:floorModelV2];
    [adapter close];
//    [adapter release];
    adapter = nil;
    return result;

}

-(BOOL)isExistFloor:(FloorModelV2*)floorModelV2{
    //checking existing floors
    NSMutableArray *floors = [[UnassignManagerV2 sharedUnassignManager] loadFloorsByFloorId:floorModelV2];
    if([floors count]>0)
    {
        return TRUE;
    }
    
    return FALSE;
}

//HaoTran[20130619/ Get last date modified]
-(NSString*)getFloorLastModifiedDate
{
    FloorAdapterV2 *adapter1 = [FloorAdapterV2 createFloorAdapter];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    NSString *result = nil;
    result = [adapter1 getFloorLastModifiedDate];
    [adapter1 close];
    adapter1 = nil;
    return result;
}
//HaoTran[20130619/ Get last date modified] - END

-(NSMutableArray*)loadFloorsByFloorId:(FloorModelV2*)floorModelV2{
    FloorAdapterV2 *adapter = [FloorAdapterV2 createFloorAdapter];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    NSMutableArray *result = [adapter loadFloorsByFloorId:floorModelV2];
    [adapter close];
//    [adapter release];
    adapter = nil;
    return result;
    
}

-(FloorModelV2*)loadFloorByFloorId:(FloorModelV2*)floorModelV2{
    FloorAdapterV2 *adapter = [FloorAdapterV2 createFloorAdapter];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    FloorModelV2 *result = [adapter loadFloorByFloorId:floorModelV2];
    [adapter close];
//    [adapter release];
    adapter = nil;
    return result;
    
}

-(NSMutableArray *)loadFloors{
    FloorAdapterV2 *adapter = [FloorAdapterV2 createFloorAdapter];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    NSMutableArray *result = [adapter loadFloors];
    [adapter close];
//    [adapter release];
    adapter = nil;
    return result;
    
}

-(NSMutableArray *)loadFloorsHaveUnAssignRoom{
    FloorAdapterV2 *adapter = [FloorAdapterV2 createFloorAdapter];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    NSMutableArray *result = [adapter loadFloorsHaveUnassignRoom];
    [adapter close];
    //    [adapter release];
    adapter = nil;
    return result;
    
}

-(NSMutableArray *)loadFloorsHaveFindRoom:(NSInteger)userId andRoomStatusID:(NSInteger)roomStatusId {
    FloorAdapterV2 *adapter = [FloorAdapterV2 createFloorAdapter];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    NSMutableArray *result = [adapter loadFloorsHaveFindRoom:userId andRoomStatusID:roomStatusId];
    [adapter close];
    //    [adapter release];
    adapter = nil;
    return result;
    
}

-(NSMutableArray *)loadFloorsHaveFindInspectedRoom:(NSInteger)userId
{
    FloorAdapterV2 *adapter = [FloorAdapterV2 createFloorAdapter];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    NSMutableArray *result = [adapter loadFloorsHaveFindInspectedRoom:userId];
    [adapter close];
    adapter = nil;
    return result;
}

-(NSMutableArray *)loadFloorsHaveFindCleaningStatus:(NSInteger)userId andCleaningStatusID:(NSInteger)cleaningStatusId
{
    FloorAdapterV2 *adapter = [FloorAdapterV2 createFloorAdapter];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    NSMutableArray *result = [adapter loadFloorsHaveFindCleaningStatus:userId andCleaningStatusID:cleaningStatusId];
    [adapter close];
    //    [adapter release];
    adapter = nil;
    return result;
}

-(NSMutableArray *)loadFloorsHaveFindKindOfRoom:(NSInteger)userId andKindOfRoom:(NSInteger)kindOfRoomId
{
    FloorAdapterV2 *adapter = [FloorAdapterV2 createFloorAdapter];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    NSMutableArray *result = [adapter loadFloorsHaveFindKindOfRoom:userId andKindOfRoom:kindOfRoomId];
    [adapter close];
    //    [adapter release];
    adapter = nil;
    return result;
}

-(NSMutableArray *)loadFloorsByUserId:(NSInteger)userId filterType:(NSInteger)filterType statusId:(NSInteger)statusId {
    FloorAdapterV2 *adapter = [FloorAdapterV2 createFloorAdapter];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    NSMutableArray *result = [adapter loadFloorsByUserId:userId filterType:filterType statusId:statusId];
    [adapter close];
    //    [adapter release];
    adapter = nil;
    return result;
    
}

#pragma mark - 
#pragma mark Zone
-(NSInteger) insertZone:(ZoneModelV2*)zoneModelV2{
    ZoneAdapterV2 *adapter = [[ZoneAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    int result = [adapter insertZone:zoneModelV2];
    [adapter close];
//    [adapter release];
    adapter = nil;
    return result;
    
}

-(NSInteger) updateZone:(ZoneModelV2*)zoneModelV2{
    ZoneAdapterV2 *adapter = [[ZoneAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    int result = [adapter updateZone:zoneModelV2];
    [adapter close];
//    [adapter release];
    adapter = nil;
    return result;
    
}

-(int)deleteZone:(ZoneModelV2*)zoneModelV2{
    ZoneAdapterV2 *adapter = [[ZoneAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    int result = [adapter deleteZone:zoneModelV2];
    [adapter close];
//    [adapter release];
    adapter = nil;
    return result;
}

-(NSMutableArray*)loadZonesByFloorId:(ZoneModelV2*)zoneModelV2{
    ZoneAdapterV2 *adapter = [[ZoneAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    NSMutableArray *result = [adapter loadzonesByFloorId:zoneModelV2];
    [adapter close];
//    [adapter release];
    adapter = nil;
    return result;
    
}

-(ZoneModelV2*)loadZonesByZoneId:(ZoneModelV2*)zoneModelV2{
    ZoneAdapterV2 *adapter = [[ZoneAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    ZoneModelV2 *result = [adapter loadzoneByZoneId:zoneModelV2];
    [adapter close];
//    [adapter release];
    adapter = nil;
    return result;
}

-(ZoneModelV2*)getZoneById:(int)zoneId{
    ZoneAdapterV2 *adapter = [[ZoneAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    ZoneModelV2 *result = [adapter getZoneById:zoneId];
    [adapter close];
    //    [adapter release];
    adapter = nil;
    return result;
}

//HaoTran[20130619/ Get last date modified]
-(NSString*)getZoneLastModifiedDate
{
    ZoneAdapterV2 *adapter1 = [[ZoneAdapterV2 alloc] init];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    NSString *result = nil;
    result = [adapter1 getZoneLastModifiedDate];
    [adapter1 close];
    adapter1 = nil;
    return result;
}
//HaoTran[20130619/ Get last date modified] - END

-(NSMutableArray*)searchRoomsByRoomId:(NSInteger)roomId roomStatus:(NSInteger)roomStatus vipcode :(NSString*)vipcode withSearchType:(enum ENUM_SEARCH_TYPE) searchType{
    
    RoomAdapterV2 *adapter = [[RoomAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    NSMutableArray* result = [adapter loadRoomByZoneId:nil];
//    [adapter release];
    adapter = nil;
    return result;
}

-(NSInteger) numberOfUnAssignRoomOnFloor:(NSInteger)floorId
{
    FloorAdapterV2 *adapter = [FloorAdapterV2 createFloorAdapter];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    NSInteger result = [adapter numberOfUnAssignRoomOnFloor:floorId];
    [adapter close];
    adapter = nil;
    return result;
}

-(NSInteger) numberOfUnAssignRoomOnZone:(NSInteger)zoneId
{
    FloorAdapterV2 *adapter = [FloorAdapterV2 createFloorAdapter];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    NSInteger result = [adapter numberOfUnAssignRoomOnZone:zoneId];
    [adapter close];
    adapter = nil;
    return result;
}

-(NSInteger) numberOfAssignRoomMustSyn{
    AssignRoomAdapterV2 *adapter = [[AssignRoomAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    NSInteger userID = [UserManagerV2 sharedUserManager].currentUser.userId;
    NSInteger result = [adapter numberOfAssignMustSyn:userID];
    [adapter close];
    //    [adapter release];
    adapter = nil;
    return result;
}

#pragma mark -  Progress step loading
//return percentage step
-(float) caculateStepFromCountingObject:(int) countValue
{
    if(countValue == 0 || countValue == 1)
        return 100;
    
    float stepValue = (float)(100 / (float)countValue);
    return stepValue;
}
#pragma mark - Update Percent View

-(void)updatePercentView:(MBProgressHUD*)percentView value:(float)percentageValue description:(NSString *)description
{
    if (percentView) {
        NSString *percentString = [[NSString alloc]initWithFormat:@"%.0f%%",percentageValue];
        percentView.detailsLabelFont = percentView.labelFont;
        percentView.detailsLabelText = percentString;
        
        float nextStepProgress = [SyncManagerV2 getNextStepOperations];
        int maxSyncOperations = [SyncManagerV2 getMaxSyncOperations];
        if(nextStepProgress > 0 && maxSyncOperations > 0)
        {
            double currentOperations = round(percentView.progress/nextStepProgress);
            //percentView.labelText = [NSString stringWithFormat:@"%@ - [%0.f/%d]", description, currentOperations, maxSyncOperations];
            percentView.labelText = [NSString stringWithFormat:@"[%0.f/%d]", currentOperations, maxSyncOperations];
        }
        else
        {
            percentView.labelText = description;
        }
    }
}
@end
