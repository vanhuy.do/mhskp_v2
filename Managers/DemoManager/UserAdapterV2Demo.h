//
//  UserAdapterV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DatabaseAdapterV2.h"
#import "UserModelV2.h"
#import "UserLogModelV2.h"
#import "UserConfigModel.h"
#import "UserAdapterV2.h"

@interface UserAdapterV2Demo : UserAdapterV2 {
    //UserModel *result;
}
-(int)insertUserData:(UserModelV2*) user ;
-(int)updateUserData:(UserModelV2*) user;
-(int)deleteUserData:(UserModelV2*) user;
-(UserModelV2*)loadUserData:(UserModelV2*)userData;
-(userRoles*)getRole:(UserModelV2*) user;
-(BOOL)checkExistUser:(UserModelV2*) userData;
-(BOOL)isAuthenticate:(UserModelV2*) user;
-(NSString*)getUserLastModifiedDate;
-(UserModelV2*)getUserByUserId:(int)userId;

#pragma mark - User Configuration
-(int)insertUserConfig:(UserConfigModel*) userConfig;
-(int)updateUserConfigurationValue:(NSString*)configValue ByUserId:(int)userId andConfigKey:(NSString*)configKey;
-(UserConfigModel*)getUserConfigByUserId:(int)userId andKey:(NSString*) userConfigKey;
-(int)deleteUserConfigurationsByUserId:(int)userId AndKey:(NSString*)configKey;

#pragma mark - Log User Information
-(int)insertUserLogData:(UserLogModelV2*) userLogData;
-(int)updateUserLogData:(UserLogModelV2*) userLogData;
-(UserLogModelV2*)getUserLogByUserId:(int)userId;

@end