//
//  UserManagerV2.m
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
#import "UserManagerV2Demo.h"
#import "LogFileManager.h"
#import "LogObject.h"
#import "AccessRightAdapter.h"
#import "ehkDefinesV2.h"
#import "eHousekeepingService.h"
#import "UserConfigModel.h"
#import "DataBaseExtension.h"

//static UserManagerV2_demo *sharedInstance=nil;

@implementation UserManagerV2Demo

//@synthesize adapter;
//@synthesize currentUser;
@synthesize currentUserAccessRight = _currentUserAccessRight;


/*
 this function post data from database local to server
 */
/*
 -(void) postUserData{
 
 eHousekeepingServiceSoapBinding *binding = [eHousekeepingService eHousekeepingServiceSoapBinding];
 //binding.logXMLInOut = YES;
 eHousekeepingService_AuthenticateUser *reqAuthenticateUser=[[eHousekeepingService_AuthenticateUser alloc] init];
 
 
 //for release resource.
 [reqAuthenticateUser release];
 [binding release];
 }*/

-(AccessRightModel*) currentUserAccessRight
{
    
    if(_currentUserAccessRight == nil)
    {
        _currentUserAccessRight = [self loadAccessRightModel:self.currentUser.userId];
        //Still nothing data, create temporary data
        if(_currentUserAccessRight == nil){
            _currentUserAccessRight = [[AccessRightModel alloc] init];
        }
    }
    return _currentUserAccessRight;
}

-(BOOL)checkExistUser:(NSString*)user_Name userPassword:(NSString*)password
{
    UserModelV2 *user=[[UserModelV2 alloc] init];
    BOOL check=NO;
    user.userName=user_Name;
    user.userPassword=password;
    check=[self checkExistUser:user];
//    [user release];
    
    return check;
}

/*
 this function update data from server to local database
 */
-(void) updateUserData:(NSString*)user_name userPassword:(NSString*)user_password
{
    UserModelV2 *model = [[UserModelV2 alloc] init];
    model.userName = user_name;
    [self loadUser:model];
    
    if(model && [model.userPassword isEqualToString:user_password]) {
        
        if([LogFileManager isLogConsole]) {
            UserConfigModel *userConfig = [[UserConfigModel alloc] init];
            userConfig.userId = model.userId;
            userConfig.value = user_password;
            NSString *keyConfig = [NSString stringWithFormat:@"%@", DEBUG_USER_COMFIG_PASSWORD];
            userConfig.key = keyConfig;
            
            NSString *valueCompare = [self getConfigValueByUserID:userConfig.userId AndKey:keyConfig];
            if(valueCompare.length <= 0)
            {
                [self insertUserConfiguration:userConfig];
            }
            else
            {
                [self updateUserConfigValue:userConfig.value ByUserId:model.userId andConfigKey:keyConfig];
            }
        }
        
        AccessRightModel *accessRightModel = [self loadAccessRightModel:model.userId];
        [self getAccessRightFromWS:model.userId WithOption:(accessRightModel.userId == 0)];
    }
}

-(void) getAccessRightFromWS:(NSInteger)userId WithOption:(BOOL) isNew
{
    AccessRightModel *accessRightModel = [self loadAccessRightModel:userId];
    self.currentUserAccessRight = accessRightModel;
    return;
}
#pragma mark - User Configurations WS
-(void) getUserConfigurationsFromWS:(NSInteger)userId
{
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    if([LogFileManager isLogConsole]){
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_GetCommonConfigurations *getUserConfigurationsRequest = [[eHousekeepingService_GetCommonConfigurations alloc] init];
    [getUserConfigurationsRequest setIntUsrID:[NSNumber numberWithInteger:userId]];
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetCommonConfigurationsUsingParameters:getUserConfigurationsRequest];
    
    for (id bodyPart in response.bodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetCommonConfigurationsResponse class]]) {
            eHousekeepingService_GetCommonConfigurationsResponse *body = bodyPart;
            eHousekeepingService_CommonConfigurationsList *configList = body.GetCommonConfigurationsResult;
            
            for (eHousekeepingService_WSSettings *currentSetting in configList.CommonConfigurationsList.WSSettings) {
                UserConfigModel *userConfig = [self getUserConfigBy:(int)userId andKey:currentSetting.ccModule];
 
                //Check existing data
                if(userConfig != nil){
                    [self updateUserConfigValue:currentSetting.ccValue ByUserId:(int)userId andConfigKey:currentSetting.ccModule];
                } else {
                    userConfig = [[UserConfigModel alloc] init];
                    userConfig.userId = (int)userId;
                    userConfig.key = currentSetting.ccModule;
                    userConfig.value = currentSetting.ccValue;
                    [self insertUserConfiguration:userConfig];
                }
            }
        }
    }
}

/*
 signin offline then set current user.
 */
-(BOOL) signIn:(NSString*)userName userPassword:(NSString*)passWord
{
    BOOL check=NO;
    UserModelV2 *user=[[UserModelV2 alloc] init];
    user.userName = userName;
    user.userPassword = passWord;
    
    check = [self isAuthenticateUser:user];
    if (check) {
        [self loadUser:user];
        [self setCurrentUser:user];
        
    }
    
    BOOL isUserDefaultConfiged = [self isCurrentUserAlreadyConfiged];
    if(!isUserDefaultConfiged){
        [super createDefaultConfigurationsOfCurrentUser];
    }
    
    return check;
}
-(UserModelV2*)getCurrentUser
{
    @synchronized(self) {
		return currentUser;
	}
}

/*
 -(UserModel*)currentUser
 {
 @synchronized(self) {
 return currentUser;
 }	
 }
 */
+(BOOL)isSupervisor{
    if ( [[self sharedUserManager] currentUser] != nil) {
        if ([[self sharedUserManager] currentUser].userSupervisorId) {
            return YES;
        }
    }
    return NO;
}
////
/*
 static UserManager* sharedUserManagerInstance = nil;
 
 + (UserManager*) sharedUserManager{
 if (sharedUserManagerInstance == nil) {
 sharedUserManagerInstance = [[super allocWithZone:NULL] init];
 }
 return sharedUserManagerInstance;
 }
 + (id)allocWithZone:(NSZone *)zone
 {
 return [[self sharedUserManager] retain];
 }
 */
////

//+ (id)allocWithZone:(NSZone *)zone
//{
//    @synchronized(self) {
//        if (sharedInstance == nil) {
//            sharedInstance = [super allocWithZone:zone];
//            return sharedInstance;  // assignment and return on first allocation
//        }
//    }
//	
//    return nil; //on subsequent allocation attempts return nil
//}

//- (id)copyWithZone:(NSZone *)zone
//{
//    return self;
//}

//- (id)retain
//{
//    return self;
//}

//- (id)autorelease
//{
//    return self;
//}

//- (NSUInteger)retainCount
//{
//    return NSUIntegerMax;  // This is sooo not zero
//}

- (id)init
{
	@synchronized(self) {
//		[super init];	
		currentUser = [[UserModelV2 alloc] init];
		return self;
	}
}

-(void)dealloc
{
//    [adapter release];
//    [currentUser release];
//    [super dealloc];
    
}
/*
 -(void)setCurrentUser:(UserModel*)user
 {
 
 @synchronized(self) {
 if (currentUser != user) {
 //[currentUser release];
 currentUser = [user retain];
 NSLog(@"%d",currentUser.user_Id);
 }
 }
 
 }*/

#pragma mark -
#pragma mark insert, load data for usermodel

//HaoTran[20130607/ Get last date modified] - Get last user modified date by current user
-(NSString*)getUserLastModifiedDate
{
    UserAdapterV2 *adapter1 = [UserAdapterV2 createUserAdapter];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    NSString *result = nil;
    result = [adapter1 getUserLastModifiedDate];
    [adapter1 close];
    //[adapter1 release];
    adapter1 = nil;
    return result;
}
//HaoTran[20130607/ Get last date modified] - END

//HaoTran[20130607/ Get User by User ID] - Get User by User ID
-(UserModelV2*) getUserByUserId:(int)userId
{
    UserModelV2* result = nil;
    UserAdapterV2 *adapter1 = [UserAdapterV2 createUserAdapter];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    result = [adapter1 getUserByUserId:userId];
    [adapter1 close];
    adapter1 = nil;
    return result;
}
//HaoTran[20130607/ Get User by User ID] - END

-(NSInteger)insert:(UserModelV2*)userModel
{
    /*
     if (adapter == nil) {
     adapter = [[UserAdapter alloc] init];
     [adapter openDatabase];
     [adapter resetSqlCommand];
     return [adapter insertUserData:userModel];
     [adapter close];
     [adapter release];
     adapter = nil;
     } else {
     [adapter openDatabase];
     [adapter resetSqlCommand];
     return [adapter insertUserData:userModel];
     [adapter close];
     }*/
    UserAdapterV2 *adapter1 = [UserAdapterV2 createUserAdapter];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    NSInteger returnValue = [adapter1 insertUserData:userModel];
    [adapter1 close];
//    [adapter1 release];
    return returnValue;
}
-(BOOL)checkExistUser:(UserModelV2*)userModel
{
    /*
     if (adapter == nil) {
     adapter = [[UserAdapter alloc] init];
     [adapter openDatabase];
     [adapter resetSqlCommand];
     BOOL result=NO;
     result=[adapter checkExistUser:userModel];
     [adapter close];
     [adapter release];
     adapter = nil;
     return result;
     } else {
     [adapter openDatabase];
     [adapter resetSqlCommand];
     BOOL result=NO;
     result=[adapter checkExistUser:userModel];
     [adapter close];
     return result;
     }
     */
    UserAdapterV2 *adapter1 = [UserAdapterV2 createUserAdapter];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    BOOL result=NO;
    result=[adapter1 checkExistUser:userModel];
    [adapter1 close];
//    [adapter1 release];
    adapter1 = nil;
    return result;
}
/**/
-(BOOL)isAuthenticateUser:(UserModelV2*)userModel
{
    /*
     if (adapter == nil) {
     adapter = [[UserAdapter alloc] init];
     [adapter openDatabase];
     [adapter resetSqlCommand];
     BOOL result = [adapter isAuthenticate:userModel];
     [adapter close];
     [adapter release];
     adapter = nil;
     return result;
     } else {
     [adapter openDatabase];
     [adapter resetSqlCommand];
     BOOL result = [adapter isAuthenticate:userModel];
     [adapter close];
     return result;
     }*/
    UserAdapterV2 *adapter1 = [UserAdapterV2 createUserAdapter];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    BOOL result = [adapter1 isAuthenticate:userModel];
    [adapter1 close];
//    [adapter1 release];
    adapter1 = nil;
    return result;
}
-(userRoles*)authorization:(UserModelV2*)userModel
{
    /*
     if (adapter == nil) {
     adapter = [[UserAdapter alloc] init];
     [adapter openDatabase];
     [adapter resetSqlCommand];
     userRoles *role = [adapter getRole:userModel];
     [adapter close];
     [adapter release];
     adapter = nil;
     return role;
     } else {
     [adapter openDatabase];
     [adapter resetSqlCommand];
     userRoles *role = [adapter getRole:userModel];
     [adapter close];
     return role;
     }*/
    UserAdapterV2 *adapter1 = [UserAdapterV2 createUserAdapter];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    userRoles *role = [adapter1 getRole:userModel];
    [adapter1 close];
//    [adapter1 release];
    adapter1 = nil;
    return role;
}
-(void) loadUser:(UserModelV2*)userData
{
    /*
     if (adapter == nil) {
     adapter = [[UserAdapter alloc] init];
     [adapter openDatabase];
     [adapter resetSqlCommand];
     [adapter loadUserData:userData];
     [adapter close];
     [adapter release];
     adapter = nil;
     } else {
     [adapter openDatabase];
     [adapter resetSqlCommand];
     [adapter loadUserData:userData];
     [adapter close];
     }*/
    UserAdapterV2 *adapter1 = [UserAdapterV2 createUserAdapter];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    [adapter1 loadUserData:userData];
    [adapter1 close];
//    [adapter1 release];
    adapter1 = nil;
}

-(NSMutableArray*) loadAllUsers
{
    DataTable *tableUsers = [[DataTable alloc] initWithName:[NSString stringWithFormat:@"%@",USER]];
    
    DataRow *rowRef = [[DataRow alloc] init];
    [rowRef addColumnInt:[NSString stringWithFormat:@"%@",user_id]];
    [rowRef addColumnString:[NSString stringWithFormat:@"%@",user_name]];
    [rowRef addColumnString:[NSString stringWithFormat:@"%@",user_password]];
    [rowRef addColumnInt:[NSString stringWithFormat:@"%@",user_role]];
    [rowRef addColumnString:[NSString stringWithFormat:@"%@",user_fullname]];
    [rowRef addColumnString:[NSString stringWithFormat:@"%@",user_language]];
    [rowRef addColumnString:[NSString stringWithFormat:@"%@",user_title]];
    [rowRef addColumnInt:[NSString stringWithFormat:@"%@",user_hotel_id]];
    [rowRef addColumnString:[NSString stringWithFormat:@"%@",user_department]];
    [rowRef addColumnInt:[NSString stringWithFormat:@"%@",user_department_id]];
    [rowRef addColumnInt:[NSString stringWithFormat:@"%@",user_supervisor_id]];
    [rowRef addColumnString:[NSString stringWithFormat:@"%@",user_last_modified]];
    [rowRef addColumnInt:[NSString stringWithFormat:@"%@",allow_block_room]];
    [rowRef addColumnInt:[NSString stringWithFormat:@"%@",allow_reorder_room]];
    
    [tableUsers loadTableDataWithReferenceRow:rowRef conditions:nil];
    
    int countRows = [tableUsers countRows];
    if(countRows > 0)
    {
        NSMutableArray *listUser = [[NSMutableArray alloc] init];
        for (int i = 0; i < countRows; i ++) {
            UserModelV2 *curUser = [[UserModelV2 alloc] init];
            DataRow *curRow = [tableUsers rowWithIndex:i];
            DataColumn *curColumn = nil;
            
            curColumn = [curRow columnWithName:[NSString stringWithFormat:@"%@",user_id]];
            curUser.userId = [curColumn fieldValueInt];
            
            curColumn = [curRow columnWithName:[NSString stringWithFormat:@"%@",user_name]];
            curUser.userName = [curColumn fieldValueString];
            
            curColumn = [curRow columnWithName:[NSString stringWithFormat:@"%@",user_password]];
            curUser.userPassword = [curColumn fieldValueString];
            
            curColumn = [curRow columnWithName:[NSString stringWithFormat:@"%@",user_role]];
            curUser.userRole = [curColumn fieldValueString];
            
            curColumn = [curRow columnWithName:[NSString stringWithFormat:@"%@",user_fullname]];
            curUser.userFullName = [curColumn fieldValueString];
            
            curColumn = [curRow columnWithName:[NSString stringWithFormat:@"%@",user_language]];
            curUser.userLang = [curColumn fieldValueString];
            
            curColumn = [curRow columnWithName:[NSString stringWithFormat:@"%@",user_title]];
            curUser.userTitle = [curColumn fieldValueString];
            
            curColumn = [curRow columnWithName:[NSString stringWithFormat:@"%@",user_hotel_id]];
            curUser.userHotelsId = [curColumn fieldValueInt];
            
            curColumn = [curRow columnWithName:[NSString stringWithFormat:@"%@",user_department]];
            curUser.userDepartment = [curColumn fieldValueString];
            
            curColumn = [curRow columnWithName:[NSString stringWithFormat:@"%@",user_department_id]];
            curUser.userDepartmentId = [curColumn fieldValueInt];
            
            curColumn = [curRow columnWithName:[NSString stringWithFormat:@"%@",user_supervisor_id]];
            curUser.userSupervisorId = [curColumn fieldValueInt];
            
            curColumn = [curRow columnWithName:[NSString stringWithFormat:@"%@",user_last_modified]];
            curUser.userLastModified = [curColumn fieldValueString];
            
            curColumn = [curRow columnWithName:[NSString stringWithFormat:@"%@",allow_block_room]];
            curUser.userAllowBlockRoom = [curColumn fieldValueInt];
            
            curColumn = [curRow columnWithName:[NSString stringWithFormat:@"%@",allow_reorder_room]];
            curUser.userAllowReorderRoom = [curColumn fieldValueInt];
            
            [listUser addObject:curUser];
        }
        
        return  listUser;
    }
    
    return nil;
}

-(NSMutableArray*) loadAttendanceUsersBySupervisorId:(int)userId
{
    DataTable *tableUsers = [[DataTable alloc] initWithName:[NSString stringWithFormat:@"%@",USER]];
    
    DataRow *rowRef = [[DataRow alloc] init];
    [rowRef addColumnInt:[NSString stringWithFormat:@"%@",user_id]];
    [rowRef addColumnString:[NSString stringWithFormat:@"%@",user_name]];
    [rowRef addColumnString:[NSString stringWithFormat:@"%@",user_password]];
    [rowRef addColumnInt:[NSString stringWithFormat:@"%@",user_role]];
    [rowRef addColumnString:[NSString stringWithFormat:@"%@",user_fullname]];
    [rowRef addColumnString:[NSString stringWithFormat:@"%@",user_language]];
    [rowRef addColumnString:[NSString stringWithFormat:@"%@",user_title]];
    [rowRef addColumnInt:[NSString stringWithFormat:@"%@",user_hotel_id]];
    [rowRef addColumnString:[NSString stringWithFormat:@"%@",user_department]];
    [rowRef addColumnInt:[NSString stringWithFormat:@"%@",user_department_id]];
    [rowRef addColumnInt:[NSString stringWithFormat:@"%@",user_supervisor_id]];
    [rowRef addColumnString:[NSString stringWithFormat:@"%@",user_last_modified]];
    [rowRef addColumnInt:[NSString stringWithFormat:@"%@",allow_block_room]];
    [rowRef addColumnInt:[NSString stringWithFormat:@"%@",allow_reorder_room]];
    
    //[tableUsers loadTableDataWithReferenceRow:rowRef conditions:nil];
    NSString *sqlString = [NSString stringWithFormat:
                           @"select a.%@,a.%@,a.%@,a.%@,a.%@,a.%@,a.%@,a.%@,a.%@,a.%@,a.%@,a.%@,a.%@,a.%@ from %@ a, %@ b where a.%@ = b.%@ and b.%@ = %d",
                           //Select
                           user_id,
                           user_name,
                           user_password,
                           user_role,
                           user_fullname,
                           user_language,
                           user_title,
                           user_hotel_id,
                           user_department,
                           user_department_id,
                           user_supervisor_id,
                           user_last_modified,
                           allow_block_room,
                           allow_reorder_room,
                           
                           //From
                           USER,
                           USER_LIST,
                           
                           user_id,
                           usr_id,
                           
                           user_supervisor_id,
                           userId
                           ];
    [tableUsers loadTableDataWithQuery:sqlString referenceRow:rowRef];
    
    int countRows = [tableUsers countRows];
    if(countRows > 0)
    {
        NSMutableArray *listUser = [[NSMutableArray alloc] init];
        for (int i = 0; i < countRows; i ++) {
            UserModelV2 *curUser = [[UserModelV2 alloc] init];
            DataRow *curRow = [tableUsers rowWithIndex:i];
            DataColumn *curColumn = nil;
            
            curColumn = [curRow columnWithName:[NSString stringWithFormat:@"%@",user_id]];
            curUser.userId = [curColumn fieldValueInt];
            
            curColumn = [curRow columnWithName:[NSString stringWithFormat:@"%@",user_name]];
            curUser.userName = [curColumn fieldValueString];
            
            curColumn = [curRow columnWithName:[NSString stringWithFormat:@"%@",user_password]];
            curUser.userPassword = [curColumn fieldValueString];
            
            curColumn = [curRow columnWithName:[NSString stringWithFormat:@"%@",user_role]];
            curUser.userRole = [curColumn fieldValueString];
            
            curColumn = [curRow columnWithName:[NSString stringWithFormat:@"%@",user_fullname]];
            curUser.userFullName = [curColumn fieldValueString];
            
            curColumn = [curRow columnWithName:[NSString stringWithFormat:@"%@",user_language]];
            curUser.userLang = [curColumn fieldValueString];
            
            curColumn = [curRow columnWithName:[NSString stringWithFormat:@"%@",user_title]];
            curUser.userTitle = [curColumn fieldValueString];
            
            curColumn = [curRow columnWithName:[NSString stringWithFormat:@"%@",user_hotel_id]];
            curUser.userHotelsId = [curColumn fieldValueInt];
            
            curColumn = [curRow columnWithName:[NSString stringWithFormat:@"%@",user_department]];
            curUser.userDepartment = [curColumn fieldValueString];
            
            curColumn = [curRow columnWithName:[NSString stringWithFormat:@"%@",user_department_id]];
            curUser.userDepartmentId = [curColumn fieldValueInt];
            
            curColumn = [curRow columnWithName:[NSString stringWithFormat:@"%@",user_supervisor_id]];
            curUser.userSupervisorId = [curColumn fieldValueInt];
            
            curColumn = [curRow columnWithName:[NSString stringWithFormat:@"%@",user_last_modified]];
            curUser.userLastModified = [curColumn fieldValueString];
            
            curColumn = [curRow columnWithName:[NSString stringWithFormat:@"%@",allow_block_room]];
            curUser.userAllowBlockRoom = [curColumn fieldValueInt];
            
            curColumn = [curRow columnWithName:[NSString stringWithFormat:@"%@",allow_reorder_room]];
            curUser.userAllowReorderRoom = [curColumn fieldValueInt];
            
            [listUser addObject:curUser];
        }
        
        return  listUser;
    }
    
    return nil;
}

-(BOOL)updateUserModel:(UserModelV2 *)model {
    //update user model
    UserAdapterV2 *adapter1 = [UserAdapterV2 createUserAdapter];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    [adapter1 updateUserData:model];
    [adapter1 close];
    adapter1 = nil;
    return YES;
}

-(void) insertAccessRightModel:(AccessRightModel *)accessRightModel
{
    AccessRightAdapter *accessRightAdapter = [[AccessRightAdapter alloc] init];
    [accessRightAdapter openDatabase];
    [accessRightAdapter resetSqlCommand];
    [accessRightAdapter insertAccessRight:accessRightModel];
    [accessRightAdapter close];
}

-(void) updateAccessRightModel:(AccessRightModel *)accessRightModel
{
    AccessRightAdapter *accessRightAdapter = [[AccessRightAdapter alloc] init];
    [accessRightAdapter openDatabase];
    [accessRightAdapter resetSqlCommand];
    [accessRightAdapter updateAccessRight:accessRightModel];
    [accessRightAdapter close];
}

-(AccessRightModel *) loadAccessRightModel:(NSInteger)userId
{
    AccessRightAdapter *accessRightAdapter = [[AccessRightAdapter alloc] init];
    [accessRightAdapter openDatabase];
    [accessRightAdapter resetSqlCommand];
    AccessRightModel *accessRightModel = [accessRightAdapter loadAccessRightByUserId:userId];
    [accessRightAdapter close];
    return accessRightModel;
}
-(int)getBuildingIdByUserId:(int)userId
{
    DataTable *table = [[DataTable alloc] initWithName:@"TableResut"];
    NSString *raRoomNumberField = [NSString stringWithFormat:@"%@",ra_room_id];
    NSString *sqlQuery = [NSString stringWithFormat:@"select %@ from %@ where %@ = %d LIMIT 1", ra_room_id, ROOM_ASSIGNMENT, ra_user_id, userId];
    DataRow *rowRef = [[DataRow alloc] init];
    [rowRef addColumnString:raRoomNumberField];
    [table loadTableDataWithQuery:sqlQuery referenceRow:rowRef];
    
    NSString *roomNumber = nil;
    if([table countRows] > 0){
        roomNumber = [[[table rowWithIndex:0] columnWithName:raRoomNumberField] fieldValueString];
    }
    
    int buildingId = 0;
    if(roomNumber > 0){
        RoomModelV2 *roomModel = [[RoomModelV2 alloc] init];
        roomModel.room_Id = roomNumber;
        [[RoomManagerV2 sharedRoomManager] loadRoomModel:roomModel];
        if(roomModel.room_Building_Id > 0){
            buildingId = (int)roomModel.room_Building_Id;
        } else {
            buildingId = 1;
        }
    } else {
        buildingId = 1;
    }
    
    return buildingId;
    
}


#pragma mark - User Common Configurations

-(NSString*)getCommonUserConfigValueByKey:(NSString*)configKey
{
    return [self getConfigValueByUserID:UserConfigPermission_CommonUser AndKey:configKey];
}

-(NSInteger)setCommontUserConfig:(NSString*)configKey value:(NSString*)configValue
{
    int userId = UserConfigPermission_CommonUser;
    int result = 0;
    
    UserConfigModel *userConfig = nil;
    NSString *valueRecord = [self getCommonUserConfigValueByKey:configKey];
    
    if(valueRecord.length > 0)
    {
        result = [self updateUserConfigValue:configValue ByUserId:userId andConfigKey:configKey] ? 1 : 0 ;
    }
    else
    {
        userConfig = [[UserConfigModel alloc] init];
        userConfig.userId = userId;
        userConfig.key = configKey;
        userConfig.value = configValue;
        result = (int)[self insertUserConfiguration:userConfig];
    }
    
    
    return result;
}

-(NSString*)getCurrentUserConfigValueByKey:(NSString*)configKey
{
    return  [self getConfigValueOfCurrentUserByKey:configKey];
}

-(NSInteger)setCurrentUserConfig:(NSString*)configKey value:(NSString*)configValue
{
    int userId = [UserManagerV2 sharedUserManager].currentUser.userId;
    int result = 0;
    if(userId > 0)
    {
        UserConfigModel *userConfig = nil;
        NSString *valueRecord = [self getCurrentUserConfigValueByKey:configKey];
 
        if(valueRecord.length > 0 || [valueRecord isEqualToString:@""])
        {
            result = [self updateUserConfigValue:configValue ByUserId:userId andConfigKey:configKey] ? 1 : 0 ;
        }
        else
        {
            userConfig = [[UserConfigModel alloc] init];
            userConfig.userId = userId;
            userConfig.key = configKey;
            userConfig.value = configValue;
            result = (int)[self insertUserConfiguration:userConfig];
        }
    }

    return result;
}

-(NSInteger)insertUserConfiguration:(UserConfigModel*)userConfig
{
    UserAdapterV2 *adapter1 = [UserAdapterV2 createUserAdapter];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    NSInteger returnValue = [adapter1 insertUserConfig:userConfig];
    [adapter1 close];
    return returnValue;
}

-(BOOL)updateUserConfigValue:(NSString*) configValue ByUserId:(int)userId andConfigKey:(NSString *)configKey
{
    UserAdapterV2 *adapter1 = [UserAdapterV2 createUserAdapter];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    int result = [adapter1 updateUserConfigurationValue:configValue ByUserId:userId andConfigKey:configKey];
    [adapter1 close];
    adapter1 = nil;
    return (result > 0);
}

-(BOOL)updateConfigValueOfCurrentUser:(NSString*)configValue andConfigKey:(NSString*)configKey
{
    BOOL result = [self updateUserConfigValue:configValue ByUserId:[UserManagerV2 sharedUserManager].currentUser.userId andConfigKey:configKey];
    return result;
}

-(UserConfigModel*) getUserConfigBy:(int)userId andKey:(NSString*)configKey
{
    if(userId < 0)
        return nil;
    if(configKey.length <= 0)
        return nil;
    
    UserConfigModel* result = nil;
    UserAdapterV2 *adapter1 = [UserAdapterV2 createUserAdapter];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    result = [adapter1 getUserConfigByUserId:userId andKey:configKey];
    [adapter1 close];
    adapter1 = nil;
    return result;
}

-(NSString*) getConfigValueOfCurrentUserByKey:(NSString*) configKey
{
    int currentUserId = [[UserManagerV2 sharedUserManager] currentUser].userId;
    UserConfigModel *userConfig = [self getUserConfigBy:currentUserId andKey:configKey];
    if(userConfig){
        return userConfig.value;
    }
    
    return nil;
}

-(NSString*) getConfigValueByUserID:(int)userId AndKey:(NSString*) configKey
{
    UserConfigModel *userConfig = [self getUserConfigBy:userId andKey:configKey];
    if(userConfig){
        return userConfig.value;
    }
    
    return nil;
}

//if userId <= 0  we will delete all config value for parameter "configKey"
//if configkey.length <= 0 we will delete all config value for parameter "userId"
-(BOOL) deleteUserConfigByUserId:(int)userId andKey:(NSString*)configKey
{
    UserAdapterV2 *adapter1 = [UserAdapterV2 createUserAdapter];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    int result = [adapter1 deleteUserConfigurationsByUserId:userId AndKey:configKey];
    [adapter1 close];
    adapter1 = nil;
    return (result > 0);
}

-(BOOL) isCurrentUserAlreadyConfiged
{
    NSString *value = [self getConfigValueOfCurrentUserByKey:USER_CONFIG_IS_ALREADY_CONFIGED];
    BOOL result = NO;
    
    if(value.length > 0){
        if([value caseInsensitiveCompare:@"yes"] == NSOrderedSame
           || [value caseInsensitiveCompare:@"true"] == NSOrderedSame
           || [value caseInsensitiveCompare:@"1"] == NSOrderedSame)
        {
            result = YES;
        }
    }
    
    return result;
}

-(void) createDefaultConfigurationsOfCurrentUser
{
    UserConfigModel *userConfig = [[UserConfigModel alloc] init];
    userConfig.userId = [UserManagerV2 sharedUserManager].currentUser.userId;
    
    //config validate room number
    userConfig.key = USER_CONFIG_IS_VALIDATE_ROOM_NUMBER;
    userConfig.value = @"false";
    [self insertUserConfiguration:userConfig];
    
    //set user configed
    userConfig.key = USER_CONFIG_IS_ALREADY_CONFIGED;
    userConfig.value = @"true";
    [self insertUserConfiguration:userConfig];
    
}

#pragma mark - User log information

-(NSInteger)insertUserLog:(UserLogModelV2*)userLogModel
{
    UserAdapterV2 *adapter1 = [UserAdapterV2 createUserAdapter];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    NSInteger returnValue = [adapter1 insertUserLogData:userLogModel];
    [adapter1 close];
    return returnValue;
}

-(BOOL)updateUserLog:(UserLogModelV2*)userLogModel
{
    UserAdapterV2 *adapter1 = [UserAdapterV2 createUserAdapter];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    [adapter1 updateUserLogData:userLogModel];
    [adapter1 close];
    adapter1 = nil;
    return YES;
}

-(UserLogModelV2*) getUserLogByUserId:(int)userId
{
    UserLogModelV2* result = nil;
    UserAdapterV2 *adapter1 = [UserAdapterV2 createUserAdapter];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    result = [adapter1 getUserLogByUserId:userId];
    [adapter1 close];
    adapter1 = nil;
    return result;
}

@end

