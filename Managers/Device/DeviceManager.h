//
//  DeviceManager.h
//  mHouseKeeping
//
//  Created by vinhnguyen on 6/14/13.
//
//

#import <Foundation/Foundation.h>

//MARK: Config Application
#define VersionSelected         Version2P1
//#define VersionSelected         Version2P2

#define isOpenSettingViewOnLoad false
//#define isOpenSettingViewOnLoad true

//END config here

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

#define Version2P1              @"2.1"
#define Version2P2              @"2.2"

enum DeviceScreenKind{
    DeviceScreenKindStandard3_5 = 1,
    DeviceScreenKindRetina3_5,
    DeviceScreenKindRetina4_0
};

enum DeviceScreenSize{
    DeviceScreenSizeStandardHeight4_0 = 568,
    DeviceScreenSizeRetinaHeight4_0 = 1136,
    DeviceScreenSizeStandardHeight3_5 = 480,
    DeviceScreenSizeRetinaHeight3_5 = 960,
    
    DeviceScreenSizeStandardWidth3_5 = 320
};

@interface DeviceManager : NSObject

//Load identifier for vendor
//Effect for iOS 6 and later
+(NSString*) identifierForVendor;

//Get Screen size kind of current device
+(enum DeviceScreenKind) getDeviceScreenKind;

//Get screen size of current device
//Only get screen size for standard screen (retina auto parse to standard)
+(CGFloat)getScreenDeviceWidth;
+(CGFloat)getScreenDeviceHeight;
+(CGRect)getScreendDeviceBounds;

//Get version selected of application
+(NSString*) versionSelected;
+(bool) versionSelectedIsEqualTo:(NSString*)versionValue;

@end
