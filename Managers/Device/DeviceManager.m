//
//  DeviceManager.m
//  mHouseKeeping
//
//  Created by vinhnguyen on 6/14/13.
//
//

#import "DeviceManager.h"
#import "LogFileManager.h"
#import "NetworkCheck.h"

@implementation DeviceManager

static NSString *versionSelected = VersionSelected;

//Load identifier for vendor
//Effect for iOS 6 and later
+(NSString*) identifierForVendor
{
    if ([[UIDevice currentDevice] respondsToSelector:@selector(identifierForVendor)]) {
        NSString *uniqueString = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
        return uniqueString;
    } else {
        return nil;
    }
}

//Get version selected of application
+(NSString*) versionSelected
{
    return versionSelected;
}

+(bool) versionSelectedIsEqualTo:(NSString*)versionValue
{
    return [versionSelected isEqualToString:versionValue];
}

//Get screen size
+(CGFloat)getScreenDeviceWidth
{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    return screenRect.size.width;
}

+(CGFloat)getScreenDeviceHeight
{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    return screenRect.size.height;
}

+(CGRect)getScreendDeviceBounds
{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    return screenRect;
}

+(enum DeviceScreenKind) getDeviceScreenKind
{
    if ([UIScreen mainScreen].scale == 2.0f) {
        CGSize result = [[UIScreen mainScreen] bounds].size;
        CGFloat scale = [UIScreen mainScreen].scale;
        result = CGSizeMake(result.width * scale, result.height * scale);
        
        if(result.height == 960){
            if([LogFileManager isLogConsole]){
                NSLog(@"iPhone 4, 4s Retina Resolution");
            }
            return DeviceScreenKindRetina3_5;
        }
        if(result.height >= 1136){
            if([LogFileManager isLogConsole]){
                NSLog(@"iPhone 5 Resolution");
            }
            return DeviceScreenKindRetina4_0;
        }
    } else {
         if([LogFileManager isLogConsole]){
             NSLog(@"iPhone Standard Resolution");
         }
        return DeviceScreenKindStandard3_5;
    }
    
    return DeviceScreenKindStandard3_5;
}

@end
