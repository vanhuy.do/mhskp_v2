//
//  EngineerCaseViewManagerV2.h
//  mHouseKeeping
//
//  Created by ThuongNM on 3/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EngineerCaseViewAdapterV2.h"
#import "EngineerCaseViewModelV2.h"
#import "EngineeringCaseDetailAdapterV2.h"
#import "EngineeringCaseDetailModelV2.h"
#import "EngineeringImageAdapterV2.h"
#import "EngineerImageModelV2.h"
#import "EngineeringItemAdapterV2.h"
#import "EngineeringItemModelV2.h"
#import "EngineeringCategoriesModeV2.h"
#import "EngineeringCatagoriesAdapterV2.h"
#import "LocationAdapterV2.h"
#import "LocationModelV2.h"

@interface EngineerCaseViewManagerV2 : NSObject {
    
}

// create the instance
+ (EngineerCaseViewManagerV2 *) sharedEngineerCaseViewManagerV2;

#pragma mark - DB for Engineer View
-(NSInteger) insertEngineerCaseViewModelV2:(EngineerCaseViewModelV2 *)model;
-(NSInteger) updateEngineerCaseViewModelV2:(EngineerCaseViewModelV2 *)model;
-(NSInteger) deleteEngineerCaseViewModelV2:(EngineerCaseViewModelV2*)model;
-(int)deleteAllEngineerCaseViewData:(int) detail_Id;
-(NSMutableArray*)loadAllEngineerCaseViewModelV2:(NSInteger)detail_id;
-(EngineerCaseViewModelV2 *)loadEngineerCaseModelByCategogyItem:(EngineerCaseViewModelV2 *)model;

#pragma mark - DB for Engineer Image
-(NSInteger) insertEngineerImageModelV2:(EngineerImageModelV2 *)model;
-(NSInteger) updateEngineerImageModelV2:(EngineerImageModelV2 *)model;
-(NSInteger) deleteEngineerImageModelV2:(EngineerImageModelV2*)model;
-(int)deleteAllEngineerImageData:(int)ID;
-(NSMutableArray *) loadAlLEngineerImageByDetailID:(int)ID;

#pragma mark - DB for Engineer Detail 
-(NSInteger) insertEngineeringCaseDetailModelV2:
    (EngineeringCaseDetailModelV2 *)model;
-(NSInteger) updateEngineeringCaseDetailModelV2:
    (EngineeringCaseDetailModelV2 *)model;
-(NSInteger) deleteEngineeringCaseDetailModelV2:(EngineeringCaseDetailModelV2*)model;
-(EngineeringCaseDetailModelV2 *)loadEngineerDetailModelByRoomIdUserIdAndServiceId:(EngineeringCaseDetailModelV2 *)model;
-(NSMutableArray*)loadAllEngineerDetailModelByRoomIdUserId:(NSInteger)userID;

#pragma mark - DB for Engineer Category
-(NSInteger) insertEngineeringCategoriesModeV2:(EngineeringCategoriesModeV2 *)model;
-(NSInteger) updateEngineeringCategoriesModeV2:(EngineeringCategoriesModeV2 *)model;
-(NSInteger) deleteEngineeringCategoriesModeV2:(EngineeringCategoriesModeV2*)model;
-(NSMutableArray*)loadAllEngineeringCategoriesModeV2;
-(NSMutableArray*)loadAllEngineeringCategoriesID:(int)ID;
//Get last date modified
-(NSString*)getEngineeringCategoryLastModifiedDate;

#pragma mark - DB for Engineer Item
-(NSInteger) insertEngineeringItemModelV2:(EngineeringItemModelV2 *)model;
-(NSInteger) updateEngineeringItemModelV2:(EngineeringItemModelV2 *)model;
-(NSInteger) deleteEngineeringItemModelV2:(EngineeringItemModelV2*)model;
-(NSMutableArray*)loadAllEngineeringItemModelV2;
-(NSMutableArray*)loadAllEngineeringItembyItemCategogy:(NSInteger)categogy_id item_id:(NSInteger)item_id;
-(NSMutableArray*)loadAllEngineeringItemID:(int)ID;

#pragma mark - DB for Location
-(int) insertLocationData:(LocationModelV2 *) location;
-(int) updateLocationData:(LocationModelV2 *) location;
-(int) deleteLocationData:(LocationModelV2 *) location;
-(LocationModelV2 *) loadLocationData:(LocationModelV2 *) location;
-(LocationModelV2 *) loadLocationDataByHotelId:(LocationModelV2 *) location;
-(NSMutableArray *) loadAllLocationByHotelId:(int) hotelId;
-(NSMutableArray *) loadAllLocation;

#pragma mark - WS for Engineering Case
//-(NSMutableArray *) getDataEngineeringCategoryWithUserID:(NSNumber *) userID AndLastModified:(NSString *) lastModified AndPercentView:(MBProgressHUD*)percentView;
-(NSMutableArray *) getDataEngineeringCategoryWithUserID:(NSNumber *) userID lastModified:(NSString *) lastModified AndHotelID: (NSString *) hotelId AndPercentView:(MBProgressHUD *)percentView;

-(NSMutableArray *) getDataEngineeringItemWithUserID:(NSNumber *) userID AndHotelID:(NSNumber *) hotelID AndLastModified:(NSString *) lastModified;

//Remove for change new WS post Engineering
//-(BOOL) postECAttachPhotoWithUserID:(NSNumber *) userID AndRoomAssignID:(NSNumber *) roomAssignID AndPhoto:(NSData *) photo;
//-(BOOL) postEngineeringCaseWithUserID:(NSNumber *) userID AndRoomAssignID:(NSNumber *) roomAssignID AndEngineeringItemID:(NSNumber *) itemID AndReportedTime:(NSString *) time AndRemark:(NSString *) remark;

//Add for new WS post Engineering (include posting image item)
-(BOOL) postEngineeringCaseWithUserID:(NSNumber *) userID AndRoomAssignID:(NSString *) roomNumber AndEngineeringItemID:(NSNumber *) itemID AndReportedTime:(NSString *) time AndRemark:(NSString *) remark AndPhoto:(NSData *)photo AndExtension:(NSString*)extensionImage;

#pragma mark - calculate number of record must sync
+(NSInteger) numberOfEngineeringCaseItemMustSyn;
+(NSInteger) numberOfECAttachPhotoMustSyn;

#pragma mark - Post WSLog
- (BOOL) postWSLog:(NSInteger)userId Message:(NSString *)messageValue MessageType:(NSInteger)type;
@end
