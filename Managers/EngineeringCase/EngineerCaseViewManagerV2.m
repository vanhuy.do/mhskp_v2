//
//  EngineerCaseViewManagerV2.m
//  mHouseKeeping
//
//  Created by ThuongNM on 3/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "EngineerCaseViewManagerV2.h"
#import "LogFileManager.h"
#import "LogObject.h"

@implementation EngineerCaseViewManagerV2


static EngineerCaseViewManagerV2 *sharedEngineerManagerInstance = nil;

+ (EngineerCaseViewManagerV2*) sharedEngineerCaseViewManagerV2 {
	if (sharedEngineerManagerInstance == nil) {
        sharedEngineerManagerInstance = [[super alloc] init];
    }
    
    return sharedEngineerManagerInstance;
}

#pragma mark - DB for Engineer View
-(NSInteger) insertEngineerCaseViewModelV2:(EngineerCaseViewModelV2 *)model{    
    NSInteger returnCode = 0;
    EngineerCaseViewAdapterV2 *adapter1 = [[EngineerCaseViewAdapterV2 alloc] init];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    returnCode = [adapter1 insertEngineerCaseViewData:model];
    [adapter1 close];
    adapter1 = nil;
    
    return returnCode; 
}

-(NSInteger) updateEngineerCaseViewModelV2:(EngineerCaseViewModelV2 *)model{    
    NSInteger status = 0;
    EngineerCaseViewAdapterV2 *adapter = [[EngineerCaseViewAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    status = [adapter updateEngineerCaseViewData:model];
    [adapter close];

    return status;
}

-(NSInteger) deleteEngineerCaseViewModelV2:(EngineerCaseViewModelV2*)model{    
    NSInteger status = 0;
    EngineerCaseViewAdapterV2 *adapter = [[EngineerCaseViewAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    status = [adapter deleteEngineerCaseViewData:model];
    [adapter close];
    
    return status;
}

-(int)deleteAllEngineerCaseViewData:(int)detail_Id{    
    NSInteger status = 0;
    EngineerCaseViewAdapterV2 *adapter = [[EngineerCaseViewAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    status = [adapter deleteAllEngineerCaseViewData:detail_Id];
    [adapter close];
    
    return (int)status;
}

-(NSMutableArray*)loadAllEngineerCaseViewModelV2:(NSInteger)detail_id{
    EngineerCaseViewAdapterV2 *adapter = [[EngineerCaseViewAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    NSMutableArray *array = [adapter loadAllEngineerCaseViewModelV2:detail_id];
    [adapter close];
    
    return array;
}

-(EngineerCaseViewModelV2 *)loadEngineerCaseModelByCategogyItem:(EngineerCaseViewModelV2 *)model{    
    EngineerCaseViewAdapterV2 *adapter = [[EngineerCaseViewAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter  loadEngineerCaseModelByCategogyItem:model];
    [adapter close];
    
    return model;
}

#pragma mark - DB for Engineer Image
-(NSInteger) insertEngineerImageModelV2:(EngineerImageModelV2 *)model{    
    NSInteger returnCode = 0;
    EngineeringImageAdapterV2 *adapter1 = [[EngineeringImageAdapterV2 alloc] init];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    returnCode = [adapter1 insertEngineerImageData:model];
    [adapter1 close];
    adapter1 = nil;
    
    return returnCode; 
}

-(NSInteger) updateEngineerImageModelV2:(EngineerImageModelV2 *)model{    
    NSInteger returnCode = 0;
    EngineeringImageAdapterV2 *adapter1 = [[EngineeringImageAdapterV2 alloc] init];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    returnCode = [adapter1 updateEngineerImageData:model];
    [adapter1 close];
    adapter1 = nil;
    
    return returnCode;
}

-(NSInteger) deleteEngineerImageModelV2:(EngineerImageModelV2*)model{    
    NSInteger returnCode = 0;
    EngineeringImageAdapterV2 *adapter1 = [[EngineeringImageAdapterV2 alloc] init];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    returnCode = [adapter1 deleteEngineerImageData:model];
    [adapter1 close];
    adapter1 = nil;
    
    return returnCode;
}

-(int)deleteAllEngineerImageData:(int)ID {
    NSInteger returnCode = 0;
    EngineeringImageAdapterV2 *adapter1 = [[EngineeringImageAdapterV2 alloc] init];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    returnCode = [adapter1 deleteAllEngineerImageData:ID];
    [adapter1 close];
    adapter1 = nil;
    
    return (int)returnCode;
}

-(NSMutableArray *) loadAlLEngineerImageByDetailID:(int)ID {    
    EngineeringImageAdapterV2 *adapter = [[EngineeringImageAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    NSMutableArray *array = [adapter loadAllEngineerImageDataByDetailId:ID];
    [adapter close];
    
    return array;
}

#pragma mark - DB for Engineer Detail 
-(NSInteger) insertEngineeringCaseDetailModelV2:(EngineeringCaseDetailModelV2 *)model{
    NSInteger returnCode = 0;
    EngineeringCaseDetailAdapterV2 *adapter1 = [[EngineeringCaseDetailAdapterV2 
                                                 alloc] init];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    returnCode = [adapter1 insertEngineeringCaseDetailData:model];
    [adapter1 close];
    adapter1 = nil;
    
    return returnCode;
}

-(NSInteger) updateEngineeringCaseDetailModelV2:(EngineeringCaseDetailModelV2 *)model{
    NSInteger returnCode = 0;
    EngineeringCaseDetailAdapterV2 *adapter1 = [[EngineeringCaseDetailAdapterV2 
                                                 alloc] init];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    returnCode = [adapter1 updateEngineeringCaseDetailData:model];
    [adapter1 close];
    adapter1 = nil;
    
    return returnCode;
}

-(NSInteger) deleteEngineeringCaseDetailModelV2:(EngineeringCaseDetailModelV2*)model{
    NSInteger returnCode = 0;
    EngineeringCaseDetailAdapterV2 *adapter1 = [[EngineeringCaseDetailAdapterV2 
                                                 alloc] init];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    returnCode = [adapter1 deleteEngineeringCaseDetailData:model];
    [adapter1 close];
    adapter1 = nil;
    
    return returnCode;
}
-(EngineeringCaseDetailModelV2 *)loadEngineerDetailModelByRoomIdUserIdAndServiceId:(EngineeringCaseDetailModelV2 *)model{
    EngineeringCaseDetailAdapterV2 *adapter = [[EngineeringCaseDetailAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter loadEngineerDetailModelByRoomIdUserIdAndServiceId:model];
    [adapter close];
    
    return model;
}

-(NSMutableArray*)loadAllEngineerDetailModelByRoomIdUserId:(NSInteger)userID {
    EngineeringCaseDetailAdapterV2 *adapter = [[EngineeringCaseDetailAdapterV2 alloc] init];
    [adapter openDatabase];
    NSMutableArray *result = [adapter loadAllEngineerDetailModelByRoomIdUserId:userID];
    [adapter close];
    
    return result;

}

#pragma mark - DB for Engineer Category
-(NSInteger) insertEngineeringCategoriesModeV2:(EngineeringCategoriesModeV2 *)model{
    NSInteger returnCode = 0;
    EngineeringCatagoriesAdapterV2 *adapter1 = [[EngineeringCatagoriesAdapterV2 
                                                 alloc] init];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    returnCode = [adapter1 insertEngineeringCatagoriesData:model];
    [adapter1 close];
    adapter1 = nil;
    
    return returnCode;
}

-(NSInteger) updateEngineeringCategoriesModeV2:(EngineeringCategoriesModeV2 *)model{    
    NSInteger returnCode = 0;
    EngineeringCatagoriesAdapterV2 *adapter1 = [[EngineeringCatagoriesAdapterV2 
                                                 alloc] init];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    returnCode = [adapter1 updateEngineeringCatagoriesData:model];
    [adapter1 close];
    adapter1 = nil;
    
    return returnCode;
}

-(NSInteger) deleteEngineeringCategoriesModeV2:(EngineeringCategoriesModeV2*)model{    
    NSInteger returnCode = 0;
    EngineeringCatagoriesAdapterV2 *adapter1 = [[EngineeringCatagoriesAdapterV2 
                                                 alloc] init];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    returnCode = [adapter1 deleteEngineeringCatagoriesData:model];
    [adapter1 close];
    adapter1 = nil;
    
    return returnCode;
}

-(NSMutableArray*)loadAllEngineeringCategoriesModeV2{    
    EngineeringCatagoriesAdapterV2 *adapter = [[EngineeringCatagoriesAdapterV2 
                                                alloc] init];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    NSMutableArray *array = [adapter loadAllEngineeringCatagoriesModelV2];
    [adapter close];

    return array;
}

-(EngineeringCategoriesModeV2*)loadEngineeringCategoriesModeV2ById:(int)categoryId{
    EngineeringCatagoriesAdapterV2 *adapter = [[EngineeringCatagoriesAdapterV2
                                                alloc] init];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    EngineeringCategoriesModeV2 *result = [adapter loadEngineeringCategoriesModeV2ById:categoryId];
    [adapter close];
    
    return result;
}

-(NSMutableArray*)loadAllEngineeringCategoriesID:(int)ID{
    EngineeringCatagoriesAdapterV2 *adapter = [[EngineeringCatagoriesAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    NSMutableArray *array = [adapter loadAllEngineeringCatagoriesID:ID];
    [adapter close];

    return array;
}

//HaoTran[20130607/ Get last date modified] - Get last user modified date Engineering Category
-(NSString*)getEngineeringCategoryLastModifiedDate
{
    EngineeringCatagoriesAdapterV2 *adapter1 = [[EngineeringCatagoriesAdapterV2 alloc] init];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    NSString *result = nil;
    result = [adapter1 getEngineeringCategoryLastModifiedDate];
    [adapter1 close];
    //[adapter1 release];
    adapter1 = nil;
    return result;
}
//HaoTran[20130618/ Get last date modified] - END

-(int)deleteAllEngineeringCategories
{
    DataTable *tableDelete = [[DataTable alloc] initWithNameFormat:@"%@", engineering_categories];
    int result = (int)[tableDelete excuteQueryNonSelect:[NSString stringWithFormat:@"Delete from %@", engineering_categories]];
    if(result > 0) {
        [NSString stringWithFormat: @"REINDEX %@.%@",DATABASE_NAME, engineering_categories];
    }
    
    return result;
}

#pragma mark - DB for Engineer Item
-(NSInteger) insertEngineeringItemModelV2:(EngineeringItemModelV2 *)model{
    NSInteger returnCode = 0;
    EngineeringItemAdapterV2 *adapter1 = [[EngineeringItemAdapterV2 alloc] init];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    returnCode = [adapter1 insertengineeringItemData:model];
    [adapter1 close];
    adapter1 = nil;
    
    return returnCode;
}

-(NSInteger) updateEngineeringItemModelV2:(EngineeringItemModelV2 *)model{
    NSInteger returnCode = 0;
    EngineeringItemAdapterV2 *adapter1 = [[EngineeringItemAdapterV2 alloc] init];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    returnCode = [adapter1 updatengineeringItemData:model];
    [adapter1 close];
    adapter1 = nil;
    
    return returnCode;
}

-(NSInteger) deleteEngineeringItemModelV2:(EngineeringItemModelV2*)model{
    NSInteger returnCode = 0;
    EngineeringItemAdapterV2 *adapter1 = [[EngineeringItemAdapterV2 alloc] init];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    returnCode = [adapter1 deletengineeringItemData:model];
    [adapter1 close];
    adapter1 = nil;
    
    return returnCode;
}

-(NSMutableArray*)loadAllEngineeringItemModelV2{
    EngineeringItemAdapterV2 *adapter = [[EngineeringItemAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    NSMutableArray *array = [adapter loadAllEngineeringItemModelV2];
    [adapter close];

    return array;
}

-(NSMutableArray*)loadAllEngineeringItembyItemCategogy:(NSInteger)categogy_id item_id:(NSInteger)item_id{    
    EngineeringItemAdapterV2 *adapter = [[EngineeringItemAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    NSMutableArray *array = [adapter loadAllEngineeringItembyItemCategogy:categogy_id item_id:item_id];
    [adapter close];
    
    return array;
}

-(NSMutableArray*)loadAllEngineeringItemID:(int)ID{    
    EngineeringItemAdapterV2 *adapter = [[EngineeringItemAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    NSMutableArray *array = [adapter loadAllEngineeringItemID:ID];
    [adapter close];
    
    return array;
}

-(EngineeringItemModelV2 *) loadEngineeringItemModel:(NSInteger) itemId {
    EngineeringItemAdapterV2 *adapter = [[EngineeringItemAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    EngineeringItemModelV2 *engineeringItem = [adapter loadEngineeringItemModelV2:itemId];
    [adapter close];
    
    return engineeringItem;
}

-(int)deleteAllEngineeringItems
{
    DataTable *tableDelete = [[DataTable alloc] initWithNameFormat:@"%@", engineering_items];
    int result = (int)[tableDelete excuteQueryNonSelect:[NSString stringWithFormat:@"Delete from %@", engineering_items]];
    if(result > 0) {
        [NSString stringWithFormat: @"REINDEX %@.%@",DATABASE_NAME, engineering_items];
    }
    
    return result;
}

#pragma mark - DB for Location
-(int) insertLocationData:(LocationModelV2 *) location {
    LocationAdapterV2 *locationAdapter = [[LocationAdapterV2 alloc] init];
    [locationAdapter openDatabase];
    [locationAdapter resetSqlCommand];
    int result = [locationAdapter insertLocationData:location];
    [locationAdapter close];
    locationAdapter = nil;
    
    return result;
}

-(int) updateLocationData:(LocationModelV2 *) location {
    LocationAdapterV2 *locationAdapter = [[LocationAdapterV2 alloc] init];
    [locationAdapter openDatabase];
    [locationAdapter resetSqlCommand];
    int result = [locationAdapter updateLocationData:location];
    [locationAdapter close];
    locationAdapter = nil;
    
    return result;
}

-(int) deleteLocationData:(LocationModelV2 *) location {
    LocationAdapterV2 *locationAdapter = [[LocationAdapterV2 alloc] init];
    [locationAdapter openDatabase];
    [locationAdapter resetSqlCommand];
    int result = [locationAdapter deleteLocationData:location];
    [locationAdapter close];
    locationAdapter = nil;
    
    return result;
}

-(LocationModelV2 *) loadLocationData:(LocationModelV2 *) location {
    LocationAdapterV2 *locationAdapter = [[LocationAdapterV2 alloc] init];
    [locationAdapter openDatabase];
    [locationAdapter resetSqlCommand]; 
    [locationAdapter loadLocationData:location];
    [locationAdapter close];
    locationAdapter = nil;
    
    return location;
}

-(LocationModelV2 *) loadLocationDataByHotelId:(LocationModelV2 *) location {
    LocationAdapterV2 *locationAdapter = [[LocationAdapterV2 alloc] init];
    [locationAdapter openDatabase];
    [locationAdapter resetSqlCommand]; 
    [locationAdapter loadLocationDataByHotelId:location];
    [locationAdapter close];
    locationAdapter = nil;
    
    return location;
}

-(NSMutableArray *) loadAllLocationByHotelId:(int) hotelId {
    LocationAdapterV2 *locationAdapter = [[LocationAdapterV2 alloc] init];
    [locationAdapter openDatabase];
    [locationAdapter resetSqlCommand];    
    NSMutableArray * results = [locationAdapter loadAllLocationByHotelId:hotelId];
    [locationAdapter close];
    locationAdapter = nil;
    
    return results;
}

-(NSMutableArray *) loadAllLocation {
    LocationAdapterV2 *locationAdapter = [[LocationAdapterV2 alloc] init];
    [locationAdapter openDatabase];
    [locationAdapter resetSqlCommand];    
    NSMutableArray * results = [locationAdapter loadAllLocation];
    [locationAdapter close];
    locationAdapter = nil;
    
    return results;
}

#pragma mark - WS for Engineering Case
-(NSMutableArray *) getDataEngineeringCategoryWithUserID:(NSNumber *) userID lastModified:(NSString *) lastModified AndHotelID: (NSNumber *) hotelId AndPercentView:(MBProgressHUD *)percentView{
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole])
    {
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_GetEngineeringCategoryList *request = [[eHousekeepingService_GetEngineeringCategoryList alloc] init];

    [request setIntUsrID:userID];
    
//    if(lastModified) {
//        [request setStrLastModified:lastModified];
//    }
    [request setStrHotel_ID:[NSString stringWithFormat:@"%@", hotelId]];
    
//    LogObject *logObj = [[LogObject alloc] init];
//    logObj.dateTime = [NSDate date];
//    logObj.log = [NSString stringWithFormat:@"<%i><getDataEngineeringCategoryWithUserID><intUsrID:%i lastModified:%@>",userID,userID,lastModified];
//    LogFileManager *logManager = [[LogFileManager alloc] init];
//    [logManager save:logObj];
    
    float currentPercent = 0;
    [self updatePercentView:percentView value:currentPercent description:[L_RA_LOADING_DATA currentKeyToLanguage]];
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetEngineeringCategoryListUsingParameters:request];
    
    NSMutableArray *array = [NSMutableArray array];
    
    NSArray *responseBodyParts = response.bodyParts;
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetEngineeringCategoryListResponse class]]) {
            eHousekeepingService_GetEngineeringCategoryListResponse *dataBody = (eHousekeepingService_GetEngineeringCategoryListResponse *)bodyPart; 
            if (dataBody) {
                if ([dataBody.GetEngineeringCategoryListResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]]) {
                    [self deleteAllEngineeringCategories];
                    float stepPercent = [self caculateStepFromCountingObject:(int)[dataBody.GetEngineeringCategoryListResult.EngineeringCatList.EngineeringCategoryList count]];
                    for(eHousekeepingService_EngineeringCategoryList *category in dataBody.GetEngineeringCategoryListResult.EngineeringCatList.EngineeringCategoryList) {
                        EngineeringCategoriesModeV2 *cateModel = [[EngineeringCategoriesModeV2 alloc] init];
                        cateModel.enca_id = [category.encCatID integerValue];
                        cateModel.enca_name = category.encCatName;
                        cateModel.enca_lang_name = category.encCatLang;
                        cateModel.enca_image = category.encPicture;
                        cateModel.enca_last_modified = category.encCatLastModified;
                        
                        NSMutableArray *resultCategory = [self loadAllEngineeringCategoriesID:(int)cateModel.enca_id];
                        if([resultCategory count] > 0)
                        {
                            [self updateEngineeringCategoriesModeV2:cateModel];
                        }
                        else
                        {
                            [self insertEngineeringCategoriesModeV2:cateModel];
                        }
                        
                        [array addObject:cateModel];
                        [self updatePercentView:percentView value:currentPercent description:[L_RA_LOADING_DATA currentKeyToLanguage]];
                        currentPercent += stepPercent;
                    }                    
                }
            }
        }
    }
    
    return array;
}

-(NSMutableArray *) getDataEngineeringItemWithUserID:(NSNumber *) userID AndHotelID:(NSNumber *) hotelID AndLastModified:(NSString *) lastModified {
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole])
    {
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_GetEngineeringItem *request = [[eHousekeepingService_GetEngineeringItem alloc] init];
    
    [request setIntUsrID:userID];
    
    [request setIntHotelID:hotelID];
    
//    if(lastModified) {
//        [request setStrLastModified:lastModified];
//    }
    
//    LogObject *logObj = [[LogObject alloc] init];
//    logObj.dateTime = [NSDate date];
//    logObj.log = [NSString stringWithFormat:@"<%i><getDataEngineeringItemWithUserID><intUsrID:%i intHotelID:%i lastModified:%@>",userID,userID,hotelID,lastModified];
//    LogFileManager *logManager = [[LogFileManager alloc] init];
//    [logManager save:logObj];
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetEngineeringItemUsingParameters:request];
    
    NSMutableArray *array = [NSMutableArray array];
    
    NSArray *responseBodyParts = response.bodyParts;
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetEngineeringItemResponse class]]) {
            eHousekeepingService_GetEngineeringItemResponse *dataBody = (eHousekeepingService_GetEngineeringItemResponse *)bodyPart; 
            if (dataBody) {
                if ([dataBody.GetEngineeringItemResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]]) {
                    [self deleteAllEngineeringItems];
                    for(eHousekeepingService_EngineeringItemList *item in dataBody.GetEngineeringItemResult.EngineeringItmList.EngineeringItemList) {
                        EngineeringItemModelV2 *itemModel = [[EngineeringItemModelV2 
                                                              alloc] init];
                        itemModel.eni_id = [item.engID integerValue];
                        itemModel.eni_name = item.engName;
                        itemModel.eni_name_lang = item.engLang;
                        itemModel.eni_category_id = [item.engCategoryID integerValue];
                        itemModel.eni_last_modified = item.engLastModified;
                        itemModel.eni_post_id = item.engServiceItemCode;
                        
                        NSInteger isSuccess = [self insertEngineeringItemModelV2:itemModel];
                        
                        if(isSuccess == 0)
                            [self updateEngineeringItemModelV2:itemModel];
                        
                        [array addObject:itemModel];
                    }                    
                }
            }
        }
    }
    
    return array;
}

//Hao Tran remove for change new WS
//-(BOOL) postEngineeringCaseWithUserID:(NSNumber *) userID AndRoomAssignID:(NSNumber *) roomAssignID AndEngineeringItemID:(NSNumber *) itemID AndReportedTime:(NSString *) time AndRemark:(NSString *) remark {
//    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
//    
//    //Allow write log or not
//    if([LogFileManager isLogConsole])
//    {
//        [binding setLogXMLInOut:YES];
//    }
//    
//    eHousekeepingService_PostEngineeringCase *request = [[eHousekeepingService_PostEngineeringCase alloc] init];
//    
//#warning fix later
//    EngineeringItemModelV2 *engineeringItemModel = [self loadEngineeringItemModel:[itemID integerValue]];
//    
//
////    [request setIntUsrID:userID];
////    [request setIntRoomAssignID:roomAssignID];
////    [request setIntEngineeringItemID:itemID];
////    [request setStrReportedTime:time];
////    [request setStrRemark:remark];
//    
//    /*
//     <tem:pSender>?</tem:pSender>
//     <!--Optional:-->
//     <tem:pAction>?</tem:pAction>
//     <!--Optional:-->
//     <tem:pUsername>?</tem:pUsername>
//     <!--Optional:-->
//     <tem:pPassword>?</tem:pPassword>
//     <!--Optional:-->
//     <tem:pPropertyID>?</tem:pPropertyID>
//     <!--Optional:-->
//     <tem:pServiceType>?</tem:pServiceType>
//     <!--Optional:-->
//     <tem:pRoomNo>?</tem:pRoomNo>
//     <!--Optional:-->
//     <tem:pItemCode>?</tem:pItemCode>
//     <!--Optional:-->
//     <tem:pLocationCode>?</tem:pLocationCode>
//     <!--Optional:-->
//     <tem:pRemark>?</tem:pRemark>
//     */
//    
//    [request setPSender:@"eHousekeeping"];
//    [request setPAction:@"Immediate"]; //Immediate, Scheduled, Repeated
//    [request setPUsername:[[[UserManagerV2 sharedUserManager] currentUser] userName]];
////    [request setPPassword:[[[UserManagerV2 sharedUserManager] currentUser] userPassword]];
//    [request setPPassword:@""];
//    [request setPPropertyID:@"1"];
//    [request setPServiceType:@"Guest"]; //InterDept, Guest
//    
//    
//    [request setPRoomNo:[NSString stringWithFormat:@"%i",[roomAssignID integerValue]]];
//    [request setPItemCode:engineeringItemModel.eni_post_id];
//    [request setPLocationCode:@""];
//    [request setPRemark:remark];
//    
//    LogObject *logObj = [[LogObject alloc] init];
//    logObj.dateTime = [NSDate date];
//    logObj.log = [NSString stringWithFormat:@"<%i><postEngineeringCaseWithUserID><PSender:%i PAction:%@ PUsername:%@ PPassword:%@ PPropertyID:%@ PServiceType:%@ PRoomNo:%@ PItemCode:%@ PLocationCode:%@ PRemark:%@>",userID.intValue,userID.intValue,@"",@"",@"",@"",@"",@"",@"",@"",remark];
//    LogFileManager *logManager = [[LogFileManager alloc] init];
//    [logManager save:logObj];
//    
//    eHousekeepingServiceSoap12BindingResponse *response = [binding PostEngineeringCaseUsingParameters:request];
//    
//    NSArray *responseBodyParts = response.bodyParts;
//    
//    for (id bodyPart in responseBodyParts) {
//        if ([bodyPart isKindOfClass:[eHousekeepingService_PostEngineeringCaseResponse class]]) {
//            eHousekeepingService_PostEngineeringCaseResponse *dataBody = (eHousekeepingService_PostEngineeringCaseResponse *)bodyPart; 
//            if (dataBody) {
//                if ([dataBody.PostEngineeringCaseResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_NEW_RECORD_ADDED]]) {                    
//                    return YES;
//                }
//            }
//        }
//    }
//    
//    return NO;
//}

-(BOOL) postEngineeringCaseWithUserID:(NSNumber *) userID AndRoomAssignID:(NSString *) roomNumber AndEngineeringItemID:(NSNumber *) itemID AndReportedTime:(NSString *) time AndRemark:(NSString *) remark AndPhoto:(NSData *)photo AndExtension:(NSString*)extensionImage {
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole])
    {
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_PosteCnJob *request = [[eHousekeepingService_PosteCnJob alloc] init];
    
    EngineeringItemModelV2 *engineeringItemModel = [self loadEngineeringItemModel:[itemID integerValue]];
    
    //Hard code
    [request setStrSender:POSTECNJOB_SENDER];
    [request setStrAction:POSTECNJOB_ACTION]; //Immediate, Scheduled, Repeated
    //[request setStrUsername:POSTECNJOB_USERNAME];
    [request setStrUsername:[UserManagerV2 sharedUserManager].currentUser.userName];
    //[request setStrPassword:POSTECNJOB_PASSWORD];
    [request setStrPassword:[UserManagerV2 sharedUserManager].currentUser.userPassword];
    //[request setStrPropertyID:POSTECNJOB_PROPERTY_ID];
    [request setStrPropertyID:[NSString stringWithFormat:@"%d", [UserManagerV2 sharedUserManager].currentUser.userHotelsId]];
    [request setStrServiceType:POSTECNJOB_SERVICE_TYPE];
    
    //room data
    [request setStrRoomNo:roomNumber];
    [request setStrItemCode:engineeringItemModel.eni_post_id];
    [request setStrLocationCode:@""];
    [request setStrRemark:remark];
    
    if(photo != nil){
        [request setBinImage:photo];
        [request setStrImageExtension:extensionImage];
    } else {
        [request setBinImage:[@"" dataUsingEncoding:NSUTF8StringEncoding]];
        [request setStrImageExtension:@""];
    }
    
    NSMutableArray *listHistoryPosting = [NSMutableArray array];
    // DungPhan - 20150908: Enable post Posting History for Engineering by Access Rights
    BOOL isEnablePostingHistory = ([[UserManagerV2 sharedUserManager].currentUserAccessRight isFindActionHistoryActive] || [[UserManagerV2 sharedUserManager].currentUserAccessRight isFindActionHistoryAllowedView] || [[UserManagerV2 sharedUserManager].currentUserAccessRight isRoomDetailsActionHistoryActive] || [[UserManagerV2 sharedUserManager].currentUserAccessRight isRoomDetailsActionHistoryAllowedView]);
    BOOL isEnglishLanguage = [[LanguageManagerV2 sharedLanguageManager] currentLanguageIsEqualTo:@"%@", ENGLISH_LANGUAGE];
    
    //if (isEnablePostingHistory) {
        //History Posting
        eHousekeepingService_PostingHistoryDetails *historyDetail = [[eHousekeepingService_PostingHistoryDetails alloc] init];
        [historyDetail setUserID:[NSString stringWithFormat:@"%d", [userID intValue]]];
        [historyDetail setRoomNo:roomNumber];
        [historyDetail setHotelID:[NSString stringWithFormat:@"%d", [UserManagerV2 sharedUserManager].currentUser.userHotelsId]];
        [historyDetail setModuleID:[NSString stringWithFormat:@"%d", (int)ModuleHistory_Engineering]];
        [historyDetail setNewQuantity:[NSString stringWithFormat:@"%d", 0]];
        [historyDetail setUsedQuantity:[NSString stringWithFormat:@"%d", 0]];
        [historyDetail setTransactionDateTime:[ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"]];
        
        EngineeringItemModelV2 *countItem = [[EngineeringItemModelV2 alloc] init];
        countItem = [self loadEngineeringItemModel:[itemID intValue]];
        
        EngineeringCategoriesModeV2 *countCategory = [[EngineeringCategoriesModeV2 alloc] init];
        countCategory = [self loadEngineeringCategoriesModeV2ById:(int)countItem.eni_category_id];
        
        if (isEnglishLanguage) {
            [historyDetail setItemDescription:countItem.eni_name];
            [historyDetail setCategoryDescription:countCategory.enca_name];
        } else {
            [historyDetail setItemDescription:countItem.eni_name_lang];
            [historyDetail setCategoryDescription:countCategory.enca_lang_name];
        }
        
        [listHistoryPosting addObject:historyDetail];
    //}
    
    LogObject *logObj = [[LogObject alloc] init];
    logObj.dateTime = [NSDate date];
    logObj.log = [NSString stringWithFormat:@"<%i><postEngineeringCaseWithUserID><PSender:%i PAction:%@ PUsername:%@ PPassword:%@ PPropertyID:%@ PServiceType:%@ PRoomNo:%@ PItemCode:%@ PLocationCode:%@ PRemark:%@>",userID.intValue,userID.intValue,@"",@"",@"",@"",@"",@"",@"",@"",remark];
    LogFileManager *logManager = [[LogFileManager alloc] init];
    [logManager save:logObj];
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding PosteCnJobUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_PosteCnJobResponse class]]) {
            eHousekeepingService_PosteCnJobResponse *dataBody = (eHousekeepingService_PosteCnJobResponse *)bodyPart;
            if (dataBody) {
                if ([dataBody.PosteCnJobResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_NEW_RECORD_ADDED]]
                    || [dataBody.PosteCnJobResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]]) {
                    
                    //CRF-1229: Posting history
                    //if (isEnablePostingHistory) {
                        CountManagerV2 *countManager = [[CountManagerV2 alloc] init];
                        [countManager postHistoryPostingWS:listHistoryPosting];
                    //}
                    
                    return YES;
                }
            }
        }
    }
    
    return NO;
}

//Remove for change new WS
//-(BOOL) postECAttachPhotoWithUserID:(NSNumber *) userID AndRoomAssignID:(NSNumber *)roomAssignID AndPhoto:(NSData *)photo {
//    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
//    eHousekeepingService_PostECAttachPhoto *request = [[eHousekeepingService_PostECAttachPhoto alloc] init];
//    
//    if([LogFileManager isLogConsole]){
//        [binding setLogXMLInOut:YES];
//    }
//    [request setIntUsrID:userID];
//    [request setIntRoomAssignID:roomAssignID];
//    [request setBinPhoto:photo];
//    
//    LogObject *logObj = [[LogObject alloc] init];
//    logObj.dateTime = [NSDate date];
//    logObj.log = [NSString stringWithFormat:@"<%i><postECAttachPhotoWithUserID><intUsrID:%i intRoomAssignID:%i BinPhoto:photadata>",userID.intValue,userID.intValue,roomAssignID.intValue];
//    LogFileManager *logManager = [[LogFileManager alloc] init];
//    [logManager save:logObj];
//    
//    eHousekeepingServiceSoap12BindingResponse *response = [binding PostECAttachPhotoUsingParameters:request];
//    
//    NSArray *responseBodyParts = response.bodyParts;
//    
//    for (id bodyPart in responseBodyParts) {
//        if ([bodyPart isKindOfClass:[eHousekeepingService_PostECAttachPhotoResponse class]]) {
//            eHousekeepingService_PostECAttachPhotoResponse *dataBody = (eHousekeepingService_PostECAttachPhotoResponse *)bodyPart; 
//            if (dataBody) {
//                if ([dataBody.PostECAttachPhotoResult.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_NEW_RECORD_ADDED]]) {
//                    return YES;
//                }
//            }
//        }
//    }
//    
//    return NO;
//}

#pragma mark - calculate number of record must sync
+(NSInteger) numberOfEngineeringCaseItemMustSyn{
    NSInteger returnCode = 0;
    EngineeringCaseDetailAdapterV2 *adapter1 = [[EngineeringCaseDetailAdapterV2 
                                                 alloc] init];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    EngineeringCaseDetailModelV2 *model = [[EngineeringCaseDetailModelV2 alloc] init];
    model.ecd_user_id = [UserManagerV2 sharedUserManager].currentUser.userId;
    returnCode = [adapter1 numberOfEngineeringCaseItemMustSyn:model];
    [adapter1 close];
    adapter1 = nil;
    
    return returnCode;
}

+(NSInteger) numberOfECAttachPhotoMustSyn{
    
    EngineeringCaseDetailAdapterV2 *adapter1 = [[EngineeringCaseDetailAdapterV2 
                                                 alloc] init];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    EngineeringCaseDetailModelV2 *engineeringCaseDetaiModel = [[EngineeringCaseDetailModelV2 alloc] init];
    engineeringCaseDetaiModel.ecd_user_id = [UserManagerV2 sharedUserManager].currentUser.userId;
    NSMutableArray *listengineeringCaseDetaiId;
    listengineeringCaseDetaiId = [adapter1 listEngineeringCaseItemId:engineeringCaseDetaiModel];
    [adapter1 close];
    adapter1 = nil;
    
    
    NSInteger returnCode = 0;
    EngineeringImageAdapterV2 *adapter2 = [[EngineeringImageAdapterV2 alloc] init];
    [adapter2 openDatabase];
    [adapter2 resetSqlCommand];
    returnCode = [adapter2 numberOfECAttachPhotoMustSyn:listengineeringCaseDetaiId];
    [adapter1 close];
    adapter1 = nil;
    
    return returnCode; 
}

#pragma mark -  Progress step loading
//return percentage step
-(float) caculateStepFromCountingObject:(int) countValue
{
    if(countValue == 0 || countValue == 1)
        return 100;
    
    float stepValue = (float)(100 / (float)countValue);
    return stepValue;
}
#pragma mark - Update Percent View

-(void)updatePercentView:(MBProgressHUD*)percentView value:(float)percentageValue description:(NSString *)description
{
    if (percentView) {
        NSString *percentString = [[NSString alloc]initWithFormat:@"%.0f%%",percentageValue];
        percentView.detailsLabelFont = percentView.labelFont;
        percentView.detailsLabelText = percentString;
        
        float nextStepProgress = [SyncManagerV2 getNextStepOperations];
        int maxSyncOperations = [SyncManagerV2 getMaxSyncOperations];
        if(nextStepProgress > 0 && maxSyncOperations > 0)
        {
            double currentOperations = round(percentView.progress/nextStepProgress);
            //percentView.labelText = [NSString stringWithFormat:@"%@ - [%0.f/%d]", description, currentOperations, maxSyncOperations];
            percentView.labelText = [NSString stringWithFormat:@"[%0.f/%d]", currentOperations, maxSyncOperations];
        }
        else
        {
            percentView.labelText = description;
        }
    }
}

#pragma mark - Post WSLog
- (BOOL) postWSLog:(NSInteger)userId Message:(NSString *)messageValue MessageType:(NSInteger)type{
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole])
    {
        [binding setLogXMLInOut:YES];
    }
    eHousekeepingService_PostWSLog *request = [[eHousekeepingService_PostWSLog alloc] init];
    request.intUsrID = [NSNumber numberWithInt:(int)userId];
    request.strMessage = messageValue;
    request.intMessageType = [NSNumber numberWithInt:(int)type];
    eHousekeepingServiceSoap12BindingResponse *respone = [binding PostWSLogUsingParameters:request];
    BOOL isPostSuccess = NO;
    for (id bodyPart in respone.bodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_PostWSLogResponse class]]) {
            eHousekeepingService_PostWSLogResponse *dataBody = (eHousekeepingService_PostWSLogResponse *)bodyPart;
            if (dataBody != nil) {
                if ([dataBody.PostWSLogResult.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]]) {
                    isPostSuccess = YES;
                }
            }
        }
    }
    return isPostSuccess;
}
@end
