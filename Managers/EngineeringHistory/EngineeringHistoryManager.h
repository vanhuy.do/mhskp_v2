//
//  EngineeringHistoryManager.h
//  mHouseKeeping
//
//  Created by Mac User on 02/04/2013.
//
//

#import <Foundation/Foundation.h>
#import "EngineeringHistoryModel.h"
#import "EngineeringHistoryAdapter.h"

@interface EngineeringHistoryManager : NSObject

-(NSInteger) insertEngineeringHistory:(EngineeringHistoryModel *)model;
-(NSMutableArray *) loadRoomIdByUserId:(NSInteger)userId;
-(NSMutableArray *) loadAllEngineeringHistoryByUserId:(NSInteger)userId AndRoomId:(NSString*)roomNumber;
-(int) deleteDatabaseAfterSomeDays;
@end
