//
//  EngineeringHistoryManager.m
//  mHouseKeeping
//
//  Created by Mac User on 02/04/2013.
//
//

#import "EngineeringHistoryManager.h"
#import "EngineeringHistoryAdapter.h"

@implementation EngineeringHistoryManager

-(NSInteger) insertEngineeringHistory:(EngineeringHistoryModel *)model{
    
    NSInteger returnCode = 0;
    EngineeringHistoryAdapter *adapter = [[EngineeringHistoryAdapter alloc] init];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    returnCode = [adapter insertEngineeringHistory:model];
    [adapter close];
    adapter = nil;
    return returnCode;
}

-(NSMutableArray *) loadRoomIdByUserId:(NSInteger)userId{
    
    EngineeringHistoryAdapter *adapter = [[EngineeringHistoryAdapter alloc] init];
    [adapter openDatabase];
    NSMutableArray *array = [adapter loadRoomIdByUserId:userId];
    [adapter close];
    return array;
}

-(NSMutableArray *) loadAllEngineeringHistoryByUserId:(NSInteger)userId AndRoomId:(NSString*)roomNumber{
//    CountHistoryAdapter *adapter = [[CountHistoryAdapter alloc] init];
    EngineeringHistoryAdapter *adapter = [[EngineeringHistoryAdapter alloc] init];
    [adapter openDatabase];
    NSMutableArray *array = [adapter loadAllEngineeringHistoryByUserId:userId AndRoomId:roomNumber];
    [adapter close];
    return array;
}

-(int) deleteDatabaseAfterSomeDays{
    NSInteger returnCode = 0;
    EngineeringHistoryAdapter *adapter = [[EngineeringHistoryAdapter alloc] init];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    returnCode = [adapter deleteDatabaseAfterSomeDays];
    [adapter close];
    adapter = nil;
    return (int)returnCode;
}
@end
