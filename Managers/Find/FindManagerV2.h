//
//  FindManager.h
//  mHouseKeeping
//
//  Created by Nghia Truong on 4/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RoomServiceLaterModelV2.h"
#import "RoomAssignmentModelV2.h"

#define kAttendantName      @"AttendantName"
#define kCurrentStatus      @"CurrentStatus"
#define kTime               @"Time"
#define kRoomNumber         @"RoomNumber"
#define kRoomAssignmentID   @"RoomAssignmentID"
#define kAttendantStatusLang  @"atdStatusLang"
#define kAttendantStatusName    @"atdStatusName"
#define kAttendantID        @"AttendantID"
#define kAtdName            @"AtdName"

#define kraID               @"raID"
#define kraHousekeeperID    @"raHousekeeperID"
#define kraRoomNo           @"raRoomNo"
#define kraRoomStatusID     @"raRoomStatusID"
#define kraCleaningStatusID @"raCleaningStatusID"
#define kraRoomTypeID       @"raRoomTypeID"
#define kraFloorID          @"raFloorID"
#define kraBuildingID       @"raBuildingID"
#define kraBuildingName     @"raBuildingName"
#define krabuildingLang     @"rabuildingLang"
#define kraVIP              @"raVIP"
#define kraPriority         @"raPriority"
#define kraExpectedCleaningTime @"raExpectedCleaningTime"
#define kraPrioritySortOrder    @"raPrioritySortOrder"
#define kraInspectedStatus      @"raInspectedStatus"
#define kraLastModified     @"raLastModified"
#define kraCleanStartTm     @"raCleanStartTm"
#define kraCleanEndTm       @"raCleanEndTm"
#define kraExpectedInspectTime  @"raExpectedInspectTime"
#define kraAssignedTime     @"raAssignedTime"
#define kraHotelID          @"raHotelID"
#define kraCleanPauseTime   @"raCleanPauseTime"
#define kraInspectPauseTime @"raInspectPauseTime"
#define kraInspectStartTm   @"raInspectStartTm"
#define kraInspectEndTm     @"raInspectEndTm"
#define kraAdditionalJob    @"kraAdditionalJob"
#define kraRemark           @"raRemark"
#define kraGuestFirstName   @"raGuestFirstName"
#define kraGuestLastName    @"raGuestLastName"
#define kraGuestPreference  @"raGuestPreference"
#define kraTotalCleaningTime    @"raTotalCleaningTime"
#define kraTotalInspectedTime   @"raTotalInspectedTime"
#define kraGuestArrivalTime     @"raGuestArrivalTime"
#define kraGuestDepartureTime   @"raGuestDepartureTime"
#define kraKindOfRoom           @"raKindOfRoom"
#define kraIsMockRoom           @"raIsMockRoom"
#define kraRAFullName           @"raFullName"
#define kraCheckedRa            @"raCheckedRa"
#define kLastCleaningDate       @"raLastCleaningDate"

#define keyRoomData             @"RoomData"
#define keyRoomAssignmentModel  @"RoomAssignmentModel"
#define keyRoomModel            @"RoomModel"
#define keyRoomRecordModel      @"RoomRecordModel"

@interface FindManagerV2 : NSObject {
    
}

// create the instance
+ (FindManagerV2 *) sharedFindManagerV2;
+ (FindManagerV2*) updateRoomManagerInstance;

#pragma mark - WS for Find
-(NSMutableArray *) findDataAttendantWithUserID:(NSNumber *) userID AndHotelID:(NSNumber *) hotelID AndAttendantName:(NSString *) attendantName;

-(NSMutableArray *) findRoomAssignmentWithUserID:(NSInteger) userId hotelId:(NSInteger)hotelId AndSupervisorId:(NSInteger) supervisorId AndLastModified:(NSString *) lastModified;
-(NSObject *) findRoomDetailWithUserID:(NSInteger) userId andRoomAssignmentID:(NSInteger) roomAssignmentId andLastModified:(NSString *) lastModified;

#pragma mark - WS for Update Room Cleaning Status
-(BOOL)updateCleaningStatusWithUserID:(NSInteger)userId andRoomAssigmentMode:(RoomAssignmentModelV2 *)model;
-(BOOL)updateCleaningStatusForWSWithUserID:(NSInteger)userId andRoomAssignmentID:(NSInteger)roomAssignmentId andCleaningStatusID:(NSInteger)cleaningStatusId;

#pragma mark - WS for Post Assign Date for Service Later status when Update Room Cleaning Status
-(BOOL) postAssignDateForServiceLaterStatusWithUserID:(NSInteger) userId andRoomAssignmentID:(NSInteger) roomAssignmentId andHouseKeepingID:(NSInteger) houseKeepingId andRoomServiceLaterModel:(RoomServiceLaterModelV2 *) serviceLaterModel;

@end
