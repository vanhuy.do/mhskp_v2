//
//  FindManager.m
//  mHouseKeeping
//
//  Created by Nghia Truong on 4/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FindManagerV2.h"
#import "NetworkCheck.h"
#import "RoomManagerV2.h"
#import "RoomRecordModelV2.h"
#import "RoomServiceLaterModelV2.h"
#import "LogFileManager.h"
#import "LogObject.h"
#import "eHousekeepingService.h"
#import "FindManagerV2Demo.h"
#import "RoomManagerV2.h"

@implementation FindManagerV2

static FindManagerV2 *sharedFindManagerInstance = nil;

+ (FindManagerV2*) sharedFindManagerV2 {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if(isDemoMode) {
            if (sharedFindManagerInstance == nil || ![sharedFindManagerInstance isKindOfClass:FindManagerV2Demo.class]) {
                sharedFindManagerInstance = [[FindManagerV2Demo alloc] init];
            }
        } else {
            if (sharedFindManagerInstance == nil || [sharedFindManagerInstance isKindOfClass:FindManagerV2Demo.class]) {
                sharedFindManagerInstance = [[FindManagerV2 alloc] init];
            }
        }
    });
    return sharedFindManagerInstance;
}

+ (FindManagerV2*) updateRoomManagerInstance{
    if(isDemoMode) {
        if (sharedFindManagerInstance == nil || ![sharedFindManagerInstance isKindOfClass:FindManagerV2Demo.class]) {
            sharedFindManagerInstance = [[FindManagerV2Demo alloc] init];
        }
    } else {
        if (sharedFindManagerInstance == nil || [sharedFindManagerInstance isKindOfClass:FindManagerV2Demo.class]) {
            sharedFindManagerInstance = [[FindManagerV2 alloc] init];
        }
    }
    return sharedFindManagerInstance;
}
#pragma mark - === Find ===
#pragma mark 
-(NSMutableArray *) findDataAttendantWithUserID:(NSNumber *) userID AndHotelID:(NSNumber *) hotelID AndAttendantName:(NSString *) attendantName {
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole])
    {
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_FindAttendant *request = [[eHousekeepingService_FindAttendant alloc] init];
    
    [request setIntUsrID:userID];
    [request setIntHotelID:hotelID];
    
    if(attendantName) {
        [request setStrAttendantName:attendantName];
    }
    
    LogObject *logObj = [[LogObject alloc] init];
    logObj.dateTime = [NSDate date];
    logObj.log = [NSString stringWithFormat:@"<%i><findDataAttendantWithUserID><intUsrID:%i intHotelID:%i attendantName:%@>",userID.intValue,userID.intValue,hotelID.intValue,attendantName];
    LogFileManager *logManager = [[LogFileManager alloc] init];
    [logManager save:logObj];
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding FindAttendantUsingParameters:request];
    
    NSMutableArray *array = [NSMutableArray array];
    
    NSArray *responseBodyParts = response.bodyParts;
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_FindAttendantResponse 
                                     class]]) {
            eHousekeepingService_FindAttendantResponse *dataBody = (eHousekeepingService_FindAttendantResponse *)bodyPart; 
            if (dataBody) {
                if ([dataBody.FindAttendantResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]]) {
                    for(eHousekeepingService_AttendantList *attendant in dataBody.FindAttendantResult.AttendantList.AttendantList) {                        
                        NSString *currentStatusText = nil;
                        NSString *currentStatus = [attendant.atdCurrentStatus stringValue];
                        NSString *roomAssignID = nil;                        
                        NSString *currentStatusLang = nil;
                        NSString *attendantId = nil;
                        NSString *atdName = nil;
                        
                        NSString *roomNo = attendant.atdRoomNo;
                        roomAssignID = [attendant.atdRoomAssignID stringValue];   
                        
                        currentStatusText = attendant.atdStatusName;
                            
                        currentStatusLang = attendant.atdStatusLang;
                        
                        attendantId = [attendant.atdID stringValue];
                        
                        atdName = attendant.atdName;
                        
                        NSDictionary *item = [NSDictionary dictionaryWithObjectsAndKeys:
                          attendantId == nil ? @"" : attendantId, kAttendantID,
                          atdName == nil ? @"" : atdName, kAtdName,
                          attendant.atdFullName == nil ? @"" : attendant.atdFullName, kAttendantName, 
                          currentStatus == nil ? @"" : currentStatus, kCurrentStatus, 
                          roomNo == nil ? @"" : roomNo, kRoomNumber, 
                          roomAssignID == nil ? @"" : roomAssignID, kRoomAssignmentID, 
                          attendant.atdCurrentScheduleTime == nil ? @"" : attendant.atdCurrentScheduleTime, kTime, 
                          currentStatusLang == nil ? @"" : currentStatusLang, kAttendantStatusLang, 
                          currentStatusText == nil ? @"" : currentStatusText, kAttendantStatusName, nil];
                                                                       
                        [array addObject:item];
                    }                    
                }
            }
        }
    }
    
    return array;
}

////Hao Tran [20130708]- Remove for find all room assignment and replace with new function below
//-(NSMutableArray *)findRoomAssignmentWithUserID:(NSInteger)userId AndLastModified:(NSString *)lastModified {
//    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
//    
//    //Allow write log or not
//    if([LogFileManager isLogConsole])
//    {
//        [binding setLogXMLInOut:YES];
//    }
//    NSMutableArray *array = [NSMutableArray array];
//    
//    RoomAssignmentModelV2 *roomAssignmentModel = nil;
//    RoomModelV2 *roomModel = nil;
//    RoomRecordModelV2 *roomRecordModel = nil;
//    
//    eHousekeepingService_GetRoomAssignmentList *request = [[eHousekeepingService_GetRoomAssignmentList alloc] init];
//    
//    [request setIntUsrID:[NSNumber numberWithInteger:userId]];
//    
//    if (lastModified != nil) {
//        [request setStrLastModified:lastModified];
//    }
//    
//    LogObject *logObj = [[LogObject alloc] init];
//    logObj.dateTime = [NSDate date];
//    logObj.log = [NSString stringWithFormat:@"<%i><findRoomAssignmentWithUserID><intUsrID:%i lastModified:%@>",userId,userId,lastModified];
//    LogFileManager *logManager = [[LogFileManager alloc] init];
//    [logManager save:logObj];
//    
//    eHousekeepingServiceSoap12BindingResponse *response = [binding GetRoomAssignmentListUsingParameters:request];
//    
//    NSArray *responseBodyParts = response.bodyParts;
//    
//    for (id bodyPart in responseBodyParts) {
//        if ([bodyPart isKindOfClass:[eHousekeepingService_GetRoomAssignmentListResponse 
//                                     class]]) {
//            eHousekeepingService_GetRoomAssignmentListResponse *dataBody = (eHousekeepingService_GetRoomAssignmentListResponse *)bodyPart; 
//            if (dataBody) {
//                if ([dataBody.GetRoomAssignmentListResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]]) {
//                    
//                    NSMutableArray *roomAssignment = dataBody.GetRoomAssignmentListResult.RoomAssignmentList.RoomAssign;
//                    eHousekeepingService_RoomAssign *roomDetail = nil;
//                    
//                    for (eHousekeepingService_RoomAssign *roomAssign in roomAssignment) {
//                        //get room detail
//                        roomDetail = (eHousekeepingService_RoomAssign *)[self findRoomDetailWithUserID:userId andRoomAssignmentID:[roomAssign.raID integerValue]  andLastModified:nil];
//                        
//                        roomAssignmentModel = [[RoomAssignmentModelV2 alloc] init];
//                        roomModel = [[RoomModelV2 alloc] init];
//                        roomRecordModel = [[RoomRecordModelV2 alloc] init];
//                        
//                        roomAssignmentModel.roomAssignment_Id = [roomAssign.raID integerValue];
//                        roomAssignmentModel.roomAssignment_RoomId = [roomAssign.raRoomNo integerValue];
//                        roomAssignmentModel.roomAssignment_UserId = userId;
//                        roomAssignmentModel.roomAssignment_AssignedDate = roomAssign.raAssignedTime;	
//                        roomAssignmentModel.roomAssignment_Priority = [roomDetail.raPriority integerValue];
//                        roomAssignmentModel.roomAssignment_PostStatus = POST_STATUS_UN_CHANGED;		
//                        roomAssignmentModel.roomAssignment_LastModified = roomAssign.raLastModified;
//                        roomAssignmentModel.roomAssignment_Priority_Sort_Order = [roomDetail.raPrioritySortOrder integerValue];
//                        roomAssignmentModel.roomAssignmentHousekeeperId = [roomAssign.raHousekeeperID integerValue];
//                        roomAssignmentModel.roomAssignmentRoomStatusId = [roomAssign.raRoomStatusID integerValue];
//                        roomAssignmentModel.roomAssignmentRoomCleaningStatusId = [roomAssign.raCleaningStatusID integerValue];
//                        roomAssignmentModel.roomAssignmentRoomInspectionStatusId = [roomAssign.raInspectedStatus integerValue];
//                        roomAssignmentModel.roomAssignmentRoomExpectedCleaningTime = [roomAssign.raExpectedCleaningTime integerValue];
//                        roomAssignmentModel.roomAssignmentRoomExpectedInspectionTime = [roomAssign.raExpectedInspectTime integerValue];
//                        roomAssignmentModel.roomAssignmentGuestProfileId = [roomAssign.raGuestProfileID integerValue];
//                        
//                        NSArray *arrayName = [roomAssign.raFullName componentsSeparatedByString:@" "];
//                        
//                        if (arrayName.count > 2) {
//                            roomAssignmentModel.roomAssignmentFirstName = [NSString stringWithFormat:@"%@ %@", [arrayName objectAtIndex:0], [arrayName objectAtIndex:arrayName.count - 1]];
//                        } else {
//                            roomAssignmentModel.roomAssignmentFirstName = roomAssign.raFullName;
//                        }
//
//                        roomAssignmentModel.roomAssignmentGuestFirstName = roomAssign.raGuestFirstName;
//                        roomAssignmentModel.roomAssignmentGuestLastName = roomAssign.raGuestLastName;
//       
//                        //set mockup room local
//                        NSInteger isMockupRoomValue = [roomAssign.raIsMockRoom integerValue];
//                        if(isMockupRoomValue == ENUM_TASK_TYPE_NORMAL)
//                        {
//                            roomAssignmentModel.raIsMockRoom = 0;
//                        }
//                        else if(isMockupRoomValue == ENUM_TASK_TYPE_REASSIGN)
//                        {
//                            roomAssignmentModel.raIsMockRoom = 0;
////                            roomAssignmentModel.raIsReassignedRoom = 1;
//                        }
//                        else if(isMockupRoomValue >= ENUM_TASK_TYPE_MOCKUP)
//                        {
//                            roomAssignmentModel.raIsMockRoom = 1;
//                        }
//                            
//                        
//                        roomAssignmentModel.raKindOfRoom = [roomAssign.raKindOfRoom integerValue];
//                        roomAssignmentModel.raGuestDepartureTime = roomAssign.raGuestDepartureTime;
//                        roomAssignmentModel.raGuestArrivalTime = roomAssign.raGuestArrivalTime;
////                        roomAssignmentModel.raIsReassignedRoom = (roomAssignmentModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_PENDING_SECOND_TIME)?1:0;
//                        
//                        //Quang edit
//                        if([roomAssign.raCleaningStatusID integerValue] == ENUM_CLEANING_STATUS_PENDING_FIRST_TIME && isMockupRoomValue == ENUM_TASK_TYPE_REASSIGN)
//                            roomAssignmentModel.raIsReassignedRoom = 1;
//                        else
//                            roomAssignmentModel.raIsReassignedRoom = 0;
//                        
//                        roomModel.room_Id = [roomAssign.raID integerValue];
//                        roomModel.room_HotelId = [roomDetail.raHotelID integerValue];
//                        roomModel.room_TypeId = [roomDetail.raRoomTypeID integerValue];
//                        roomModel.room_Lang = nil;
//                        roomModel.room_Building_Name = roomAssign.raBuildingName;
//                        roomModel.room_building_namelang = roomAssign.rabuildingLang;
//                        roomModel.room_Guest_Name = roomAssign.raGuestName;
//                        roomModel.room_VIPStatusId = [roomAssign.raVIP integerValue];
//                        roomModel.room_GuestReference = roomAssign.raGuestPreference;
//                        roomModel.room_AdditionalJob = roomDetail.raAdditionalJob;
//                        roomModel.room_PostStatus = POST_STATUS_UN_CHANGED;
//                        roomModel.room_LastModified = roomDetail.raLastModified;
//                        roomModel.room_is_re_cleaning = NO;
//                        roomModel.room_expected_status_id = 0;
//                        roomModel.room_Building_Id = [roomAssign.raBuildingID integerValue];
//                        roomModel.room_zone_id = 0;
//                        roomModel.room_floor_id = [roomAssign.raFloorID integerValue];
//                        
//                        roomRecordModel.rrec_User_Id = userId;
//                        roomRecordModel.rrec_Cleaning_Date = roomAssign.raCleanEndTm;
//                        roomRecordModel.rrec_PostStatus = POST_STATUS_UN_CHANGED;
//                        roomRecordModel.rrec_Cleaning_Start_Time = roomAssign.raCleanEndTm;
//                        roomRecordModel.rrec_Cleaning_End_Time = roomAssign.raCleanStartTm;
//                        roomRecordModel.rrec_Inspection_Start_Time = roomAssign.raInspectStartTm;
//                        roomRecordModel.rrec_Inspection_End_Time = roomAssign.raInspectEndTm;
////                        roomRecordModel.rrec_Cleaning_Total_Pause_Time;
////                        roomRecordModel.rrec_Inspected_Total_Pause_Time;
//                        roomRecordModel.rrec_room_assignment_id = [roomAssign.raID integerValue];
//                        roomRecordModel.rrec_Remark = roomDetail.raRemark;
//                        
//                        NSDateFormatter *dateTimeFormat = [[NSDateFormatter alloc] init];
//                        [dateTimeFormat setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
//                        [dateTimeFormat setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
//                        NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
//                        [dateTimeFormat setLocale:usLocale];
//                        
//                        NSCalendar *calender = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
//                        NSDateComponents *components = nil;
//                        
//                        if (roomAssign.raCleanStartTm != nil && roomAssign.raCleanEndTm != nil) {
//                            NSDate *cleaningStartDate = [dateTimeFormat dateFromString:roomAssign.raCleanStartTm];
//                            NSDate *cleaningEndDate = [dateTimeFormat dateFromString:roomAssign.raCleanEndTm];
//                            components = [calender components:NSHourCalendarUnit|NSMinuteCalendarUnit|NSSecondCalendarUnit fromDate:cleaningStartDate toDate:cleaningEndDate options:0];
//                            roomRecordModel.rrec_Cleaning_Duration = [NSString stringWithFormat:@"%l",components.hour*3600 + components.minute*60 + components.second];
//                        }
//                        
//                        if (roomAssign.raInspectStartTm != nil && roomAssign.raInspectEndTm != nil) {
//                            NSDate *inspectStartDate = [dateTimeFormat dateFromString:roomAssign.raInspectStartTm];
//                            NSDate *inspectEndDate = [dateTimeFormat dateFromString:roomAssign.raInspectEndTm];
//                            components = [calender components:NSHourCalendarUnit|NSMinuteCalendarUnit|NSSecondCalendarUnit fromDate:inspectStartDate toDate:inspectEndDate options:0];
//                            roomRecordModel.rrec_Inspection_Duration = [NSString stringWithFormat:@"%l",components.hour*3600 + components.minute*60 + components.second];
//                        }
//                        
//                        roomAssign.raInspectStartTm = roomDetail.raInspectStartTm;
//                        roomAssign.raInspectEndTm = roomDetail.raInspectEndTm;
//                        
//                        //convert room assignment data to dictionary
//                        NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
//                        
//                        if (roomAssign.raID != nil) {
//                            [dictionary setObject:roomAssign.raID forKey:kraID];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraID];
//                        }
//                        
//                        if (roomAssign.raHousekeeperID != nil) {
//                            [dictionary setObject:roomAssign.raHousekeeperID forKey:kraHousekeeperID];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraHousekeeperID];
//                        }
//                        
//                        if (roomAssign.raRoomNo != nil) {
//                            [dictionary setObject:roomAssign.raRoomNo forKey:kraRoomNo];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraRoomNo];
//                        }
//                        
//                        if (roomAssign.raRoomStatusID != nil) {
//                            [dictionary setObject:roomAssign.raRoomStatusID forKey:kraRoomStatusID];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraRoomStatusID];
//                        }
//                        
//                        if (roomAssign.raRoomTypeID != nil) {
//                            [dictionary setObject:roomAssign.raRoomTypeID forKey:kraRoomTypeID];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraRoomTypeID];
//                        }
//                        
//                        if (roomAssign.raCleaningStatusID != nil) {
//                            [dictionary setObject:roomAssign.raCleaningStatusID forKey:kraCleaningStatusID];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraCleaningStatusID];
//                        }
//                        
//                        if (roomAssign.raFloorID != nil) {
//                            [dictionary setObject:roomAssign.raFloorID forKey:kraFloorID];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraFloorID];
//                        }
//                        
//                        if (roomAssign.raBuildingID != nil) {
//                            [dictionary setObject:[roomAssign.raBuildingID stringValue] forKey:kraBuildingID];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraBuildingID];
//                        }
//                        
//                        if (roomAssign.raBuildingName != nil) {
//                            [dictionary setObject:roomAssign.raBuildingName forKey:kraBuildingName];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraBuildingName];
//                        }
//                        
//                        if (roomAssign.rabuildingLang != nil) {
//                            [dictionary setObject:roomAssign.rabuildingLang forKey:krabuildingLang];
//                        } else {
//                            [dictionary setObject:@"" forKey:krabuildingLang];
//                        }
//                        
//                        if (roomAssign.raPriority != nil) {
//                            [dictionary setObject:roomAssign.raPriority forKey:kraPriority];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraPriority];
//                        }
//                        
//                        if (roomAssign.raPrioritySortOrder != nil) {
//                            [dictionary setObject:[roomAssign.raPrioritySortOrder stringValue] forKey:kraPrioritySortOrder];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraPrioritySortOrder];
//                        }
//                        
//                        if (roomAssign.raExpectedCleaningTime != nil) {
//                            [dictionary setObject:roomAssign.raExpectedCleaningTime forKey:kraExpectedCleaningTime];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraExpectedCleaningTime];
//                        }
//                        
//                        if (roomAssign.raInspectedStatus != nil) {
//                            [dictionary setObject:[roomAssign.raInspectedStatus stringValue] forKey:kraInspectedStatus];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraInspectedStatus];
//                        }
//                        
//                        if (roomAssign.raLastModified != nil) {
//                            [dictionary setObject:roomAssign.raLastModified forKey:kraLastModified];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraLastModified];
//                        }
//                        
//                        if (roomAssign.raCleanStartTm != nil) {
//                            [dictionary setObject:roomAssign.raCleanStartTm forKey:kraCleanStartTm];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraCleanStartTm];
//                        }
//                        
//                        if (roomAssign.raCleanEndTm != nil) {
//                            [dictionary setObject:roomAssign.raCleanEndTm forKey:kraCleanEndTm];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraCleanEndTm];
//                        }
//                        
//                        if (roomAssign.raExpectedInspectTime != nil) {
//                            [dictionary setObject:[roomAssign.raExpectedInspectTime stringValue] forKey:kraExpectedInspectTime];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraExpectedInspectTime];
//                        }
//                        
//                        if (roomAssign.raAssignedTime != nil) {
//                            [dictionary setObject:roomAssign.raAssignedTime forKey:kraAssignedTime];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraAssignedTime];
//                        }
//                        
//                        if (roomAssign.raHotelID != nil) {
//                            [dictionary setObject:roomAssign.raHotelID forKey:kraHotelID];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraHotelID];
//                        }
//                        
//                        if (roomAssign.raInspectStartTm != nil) {
//                            [dictionary setObject:roomAssign.raInspectStartTm forKey:kraInspectStartTm];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraInspectStartTm];
//                        }
//                        
//                        if (roomAssign.raInspectEndTm != nil) {
//                            [dictionary setObject:roomAssign.raInspectEndTm forKey:kraInspectEndTm];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraInspectEndTm];
//                        }
//                        
//                        if (roomAssign.raAdditionalJob != nil) {
//                            [dictionary setObject:roomAssign.raAdditionalJob forKey:kraAdditionalJob];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraAdditionalJob];
//                        }
//                        
//                        if (roomAssign.raRemark != nil) {
//                            [dictionary setObject:roomAssign.raRemark forKey:kraRemark];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraRemark];
//                        }
//                        
//                        if (roomAssign.raVIP != nil) {
//                            [dictionary setObject:roomAssign.raVIP forKey:kraVIP];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraVIP];
//                        }
//                        
//                        if (roomAssign.raGuestFirstName != nil) {
//                            [dictionary setObject:roomAssign.raGuestFirstName forKey:kraGuestFirstName];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraGuestFirstName];
//                        }
//                        
//                        if (roomAssign.raGuestLastName != nil) {
//                            [dictionary setObject:roomAssign.raGuestLastName forKey:kraGuestLastName];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraGuestLastName];
//                        }
//                        
//                        if (roomAssign.raGuestPreference != nil) {
//                            [dictionary setObject:roomAssign.raGuestPreference forKey:kraGuestPreference];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraGuestPreference];
//                        }
//                        
//                        if (roomAssign.raTotalCleaningTime != nil) {
//                            [dictionary setObject:[roomAssign.raTotalCleaningTime stringValue] forKey:kraTotalCleaningTime];
//                        } else {
//                            [dictionary setObject:@"0" forKey:kraTotalCleaningTime];
//                        }
//                        
//                        if (roomAssign.raToTalInspectionTime != nil) {
//                            [dictionary setObject:[roomAssign.raToTalInspectionTime stringValue] forKey:kraTotalInspectedTime];
//                        } else {
//                            [dictionary setObject:@"0" forKey:kraTotalInspectedTime];
//                        }
//                        
//                        if (roomAssign.raGuestArrivalTime != nil) {
//                            [dictionary setObject:roomAssign.raGuestArrivalTime forKey:kraGuestArrivalTime];
//                        }
//                        else {
//                            [dictionary setObject:@"" forKey:kraGuestArrivalTime];
//                        }
//                        
//                        if (roomAssign.raGuestDepartureTime != nil) {
//                            [dictionary setObject:roomAssign.raGuestDepartureTime forKey:kraGuestDepartureTime];
//                        }
//                        else {
//                            [dictionary setObject:@"" forKey:kraGuestDepartureTime];
//                        }
//                        
//                        if (roomAssign.raKindOfRoom != nil) {
//                            [dictionary setObject:[roomAssign.raKindOfRoom stringValue] forKey:kraKindOfRoom];
//                        }
//                        else {
//                            [dictionary setObject:@"0" forKey:kraKindOfRoom];
//                        }
//                        
//                        if (roomAssign.raIsMockRoom != nil) {
//                            [dictionary setObject:[roomAssign.raIsMockRoom stringValue] forKey:kraIsMockRoom];
//                        }
//                        else {
//                            [dictionary setObject:@"0" forKey:kraIsMockRoom];
//                        }
//                        
//                        if (roomAssign.raFullName != nil) {
//                            [dictionary setObject:roomAssign.raFullName forKey:kraRAFullName];
//                        }
//                        
//                        RoomStatusModelV2 *roomStatusModel = [[RoomStatusModelV2 alloc] init];
//                        roomStatusModel.rstat_Id = roomAssignmentModel.roomAssignmentRoomStatusId;
//                        [[RoomManagerV2 sharedRoomManager] loadRoomStatusData:roomStatusModel];
//                        
//                        CleaningStatusModelV2 *cleaningStatusModel = [[CleaningStatusModelV2 alloc] init];
//                        cleaningStatusModel.cstat_Id = roomAssignmentModel.roomAssignmentRoomCleaningStatusId;
//                        [[RoomManagerV2  sharedRoomManager] loadCleaningStatus:cleaningStatusModel];
//                        
//                        NSMutableDictionary *roomAssignmentData = [[NSMutableDictionary alloc] init];
//                        [roomAssignmentData setValue:[NSString stringWithFormat:@"%i",roomAssignmentModel.roomAssignment_UserId] forKey:kRaUserId];
//                        [roomAssignmentData setValue:[NSString stringWithFormat:@"%i",roomAssignmentModel.roomAssignment_RoomId] forKey:kRoomNo];
//                        [roomAssignmentData setValue:[NSString stringWithFormat:@"%i",roomAssignmentModel.roomAssignmentRoomStatusId] forKey:kRoomStatusId];
//                        [roomAssignmentData setValue:[NSString stringWithFormat:@"%i",roomAssignmentModel.roomAssignmentRoomInspectionStatusId] forKey:kRoomInspectedStatusId];
//                        [roomAssignmentData setValue:roomStatusModel.rstat_Code forKey:kRMStatus];
//                        [roomAssignmentData setValue:[NSString stringWithFormat:@"%i",roomAssignmentModel.roomAssignmentRoomCleaningStatusId] forKey:kCleaningStatusID];
//                        [roomAssignmentData setValue:[NSString stringWithFormat:@"%@ %@",roomAssign.raGuestFirstName == nil?@"":roomAssign.raGuestFirstName,roomAssign.raGuestLastName == nil?@"":roomAssign.raGuestLastName] forKey:kGuestName];
//                        [roomAssignmentData setValue:[NSString stringWithFormat:@"%i",roomAssignmentModel.roomAssignment_Id] forKey:kRoomAssignmentID];
//                        [roomAssignmentData setValue:roomAssign.raPrioritySortOrder forKey:kRaPrioritySortOrder];
//                        [roomAssignmentData setValue:roomAssignmentModel.roomAssignmentFirstName forKey:kRAFirstName];
//                        [roomAssignmentData setValue:roomAssignmentModel.roomAssignmentLastName forKey:kRALastName];
//                        [roomAssignmentData setValue:cleaningStatusModel.cstat_image forKey:kCleaningStatus];
//                        [roomAssignmentData setValue:roomAssign.raGuestFirstName forKey:kGuestFirstName];
//                        [roomAssignmentData setValue:roomAssign.raGuestLastName forKey:kGuestLastName];
//                        [roomAssignmentData setValue:roomAssign.raGuestArrivalTime forKey:kRaGuestArrivalTime];
//                        [roomAssignmentData setValue:roomAssign.raGuestDepartureTime forKey:kRaGuestDepartureTime];
//                        [roomAssignmentData setValue:[NSString stringWithFormat:@"%i",roomAssignmentModel.raKindOfRoom] forKey:kRaKindOfRoom];
//                        [roomAssignmentData setValue:[NSString stringWithFormat:@"%i",roomAssignmentModel.raIsMockRoom] forKey:kRaIsMockRoom];
//                        [roomAssignmentData setValue:roomStatusModel.rstat_Image forKey:kRoomStatusImage];
//                        [roomAssignmentData setValue:roomAssignmentModel.roomAssignment_AssignedDate forKey:kRoomAssignmentTime];
//                        [roomAssignmentData setValue:[NSString stringWithFormat:@"%i",roomAssignmentModel.raIsReassignedRoom] forKey:kRaIsReassignedRoom];
//                        [roomAssignmentData setValue:roomAssign.raVIP forKey:kVIP];
//                        
//                        NSMutableDictionary * data = [[NSMutableDictionary alloc] init];
//                        [data setObject:roomAssignmentData forKey:keyRoomData];
//                        [data setObject:roomAssignmentModel forKey:keyRoomAssignmentModel];
//                        [data setObject:roomModel forKey:keyRoomModel];
//                        [data setObject:roomRecordModel forKey:keyRoomRecordModel];
//                        
//                        [array addObject:data];
//                    }                    
//                }                
//            }
//        }
//    }
//    
//    eHousekeepingService_GetPrevRoomAssignmentList *requestPrev = [[eHousekeepingService_GetPrevRoomAssignmentList alloc] init];
//    
//    //set supervisor id
//    [requestPrev setIntUsrID:[NSNumber numberWithInteger:[[[UserManagerV2 sharedUserManager] currentUser] userId]]];
//    
//    eHousekeepingServiceSoap12BindingResponse *responsePrev = [binding GetPrevRoomAssignmentListUsingParameters:requestPrev];
//    
//    NSArray *responsePrevBodyParts = responsePrev.bodyParts;
//    
//    for (id bodyPart in responsePrevBodyParts) {
//        if ([bodyPart isKindOfClass:[eHousekeepingService_GetPrevRoomAssignmentListResponse 
//                                     class]]) {
//            eHousekeepingService_GetPrevRoomAssignmentListResponse *dataBody = (eHousekeepingService_GetPrevRoomAssignmentListResponse *)bodyPart; 
//            
//            if (dataBody) {
//                if ([dataBody.GetPrevRoomAssignmentListResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]]) {
//                    
//                    NSMutableArray *roomAssignment = dataBody.GetPrevRoomAssignmentListResult.RoomAssignmentList.RoomAssign;
//                    
//                    eHousekeepingService_RoomAssign *roomDetail = nil;
//                    
//                    for (eHousekeepingService_RoomAssign *roomAssign in roomAssignment) {
//
//                        //Hao Tran remove - for get all rooms
//                        /*
//                        if ([roomAssign.raHousekeeperID integerValue] != userId) {
//                            continue;
//                        }*/
//                        
//                        //get room detail
//                        roomDetail = (eHousekeepingService_RoomAssign *)[self findRoomDetailWithUserID:userId andRoomAssignmentID:[roomAssign.raID integerValue]  andLastModified:nil];
//                        
//                        //set missing data to room assign
//                        //                        roomAssign.raCleanStopTime = roomDetail.raCleanStopTime;
//                        //                        roomAssign.raInspectStopTime = roomDetail.raInspectStopTime;
//                        roomAssign.raInspectStartTm = roomDetail.raInspectStartTm;
//                        roomAssign.raInspectEndTm = roomDetail.raInspectEndTm;
//                        
//                        //convert room assignment data to dictionary
//                        NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
//                        
//                        if (roomAssign.raID != nil) {
//                            [dictionary setObject:roomAssign.raID forKey:kraID];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraID];
//                        }
//                        
//                        if (roomAssign.raHousekeeperID != nil) {
//                            [dictionary setObject:roomAssign.raHousekeeperID forKey:kraHousekeeperID];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraHousekeeperID];
//                        }
//                        
//                        if (roomAssign.raRoomNo != nil) {
//                            [dictionary setObject:roomAssign.raRoomNo forKey:kraRoomNo];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraRoomNo];
//                        }
//                        
//                        if (roomAssign.raRoomStatusID != nil) {
//                            [dictionary setObject:roomAssign.raRoomStatusID forKey:kraRoomStatusID];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraRoomStatusID];
//                        }
//                        
//                        if (roomAssign.raRoomTypeID != nil) {
//                            [dictionary setObject:roomAssign.raRoomTypeID forKey:kraRoomTypeID];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraRoomTypeID];
//                        }
//                        
//                        if (roomAssign.raCleaningStatusID != nil) {
//                            [dictionary setObject:roomAssign.raCleaningStatusID forKey:kraCleaningStatusID];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraCleaningStatusID];
//                        }
//                        
//                        if (roomAssign.raFloorID != nil) {
//                            [dictionary setObject:roomAssign.raFloorID forKey:kraFloorID];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraFloorID];
//                        }
//                        
//                        if (roomAssign.raBuildingID != nil) {
//                            [dictionary setObject:[roomAssign.raBuildingID stringValue] forKey:kraBuildingID];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraBuildingID];
//                        }
//                        
//                        if (roomAssign.raBuildingName != nil) {
//                            [dictionary setObject:roomAssign.raBuildingName forKey:kraBuildingName];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraBuildingName];
//                        }
//                        
//                        if (roomAssign.rabuildingLang != nil) {
//                            [dictionary setObject:roomAssign.rabuildingLang forKey:krabuildingLang];
//                        } else {
//                            [dictionary setObject:@"" forKey:krabuildingLang];
//                        }
//                        
//                        if (roomAssign.raPriority != nil) {
//                            [dictionary setObject:roomAssign.raPriority forKey:kraPriority];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraPriority];
//                        }
//                        
//                        if (roomAssign.raPrioritySortOrder != nil) {
//                            [dictionary setObject:[roomAssign.raPrioritySortOrder stringValue] forKey:kraPrioritySortOrder];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraPrioritySortOrder];
//                        }
//                        
//                        if (roomAssign.raExpectedCleaningTime != nil) {
//                            [dictionary setObject:roomAssign.raExpectedCleaningTime forKey:kraExpectedCleaningTime];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraExpectedCleaningTime];
//                        }
//                        
//                        if (roomAssign.raInspectedStatus != nil) {
//                            [dictionary setObject:[roomAssign.raInspectedStatus stringValue] forKey:kraInspectedStatus];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraInspectedStatus];
//                        }
//                        
//                        if (roomAssign.raLastModified != nil) {
//                            [dictionary setObject:roomAssign.raLastModified forKey:kraLastModified];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraLastModified];
//                        }
//                        
//                        if (roomAssign.raCleanStartTm != nil) {
//                            [dictionary setObject:roomAssign.raCleanStartTm forKey:kraCleanStartTm];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraCleanStartTm];
//                        }
//                        
//                        if (roomAssign.raCleanEndTm != nil) {
//                            [dictionary setObject:roomAssign.raCleanEndTm forKey:kraCleanEndTm];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraCleanEndTm];
//                        }
//                        
//                        if (roomAssign.raExpectedInspectTime != nil) {
//                            [dictionary setObject:[roomAssign.raExpectedInspectTime stringValue] forKey:kraExpectedInspectTime];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraExpectedInspectTime];
//                        }
//                        
//                        if (roomAssign.raAssignedTime != nil) {
//                            [dictionary setObject:roomAssign.raAssignedTime forKey:kraAssignedTime];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraAssignedTime];
//                        }
//                        
//                        if (roomAssign.raHotelID != nil) {
//                            [dictionary setObject:roomAssign.raHotelID forKey:kraHotelID];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraHotelID];
//                        }
//                        
//                        //                        if (roomAssign.raCleanStopTime != nil) {
//                        //                            [dictionary setObject:[roomAssign.raCleanStopTime stringValue] forKey:kraCleanPauseTime];
//                        //                        } else {
//                        //                            [dictionary setObject:@"" forKey:kraCleanPauseTime];
//                        //                        }
//                        
//                        //                        if (roomAssign.raInspectStopTime != nil) {
//                        //                            [dictionary setObject:[roomAssign.raInspectStopTime stringValue] forKey:kraInspectPauseTime];
//                        //                        } else {
//                        //                            [dictionary setObject:@"" forKey:kraInspectPauseTime];
//                        //                        }
//                        
//                        if (roomAssign.raInspectStartTm != nil) {
//                            [dictionary setObject:roomAssign.raInspectStartTm forKey:kraInspectStartTm];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraInspectStartTm];
//                        }
//                        
//                        if (roomAssign.raInspectEndTm != nil) {
//                            [dictionary setObject:roomAssign.raInspectEndTm forKey:kraInspectEndTm];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraInspectEndTm];
//                        }
//                        
//                        if (roomAssign.raAdditionalJob != nil) {
//                            [dictionary setObject:roomAssign.raAdditionalJob forKey:kraAdditionalJob];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraAdditionalJob];
//                        }
//                        
//                        if (roomAssign.raRemark != nil) {
//                            [dictionary setObject:roomAssign.raRemark forKey:kraRemark];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraRemark];
//                        }
//                        
//                        if (roomAssign.raVIP != nil) {
//                            [dictionary setObject:roomAssign.raVIP forKey:kraVIP];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraVIP];
//                        }
//                        
//                        if (roomAssign.raGuestFirstName != nil) {
//                            [dictionary setObject:roomAssign.raGuestFirstName forKey:kraGuestFirstName];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraGuestFirstName];
//                        }
//                        
//                        if (roomAssign.raGuestLastName != nil) {
//                            [dictionary setObject:roomAssign.raGuestLastName forKey:kraGuestLastName];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraGuestLastName];
//                        }
//                        
//                        if (roomAssign.raGuestPreference != nil) {
//                            [dictionary setObject:roomAssign.raGuestPreference forKey:kraGuestPreference];
//                        } else {
//                            [dictionary setObject:@"" forKey:kraGuestPreference];
//                        }
//                        
//                        if (roomAssign.raTotalCleaningTime != nil) {
//                            [dictionary setObject:[roomAssign.raTotalCleaningTime stringValue] forKey:kraTotalCleaningTime];
//                        } else {
//                            [dictionary setObject:@"0" forKey:kraTotalCleaningTime];
//                        }
//                        
//                        if (roomAssign.raToTalInspectionTime != nil) {
//                            [dictionary setObject:[roomAssign.raToTalInspectionTime stringValue] forKey:kraTotalInspectedTime];
//                        } else {
//                            [dictionary setObject:@"0" forKey:kraTotalInspectedTime];
//                        }                        
//                        
//                        [array addObject:dictionary];
//                    }                    
//                }
//            }
//        }
//    }
//    
//    return array;
//}
//Hao Tran [20130708] - Remove End

//Hao Tran [20130708] - Replace with this one
-(NSMutableArray *)findRoomAssignmentWithUserID:(NSInteger)userId hotelId:(NSInteger)hotelId AndSupervisorId:(NSInteger)supervisorId AndLastModified:(NSString *)lastModified {
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole]){
        [binding setLogXMLInOut:YES];
    }
    
    //rooms Respond for cached rooms response
    NSMutableArray *roomsResponsed = [NSMutableArray array];
    
    //array for return room result
    NSMutableArray *array = [NSMutableArray array];
    
    RoomAssignmentModelV2 *roomAssignmentModel = nil;
    RoomModelV2 *roomModel = nil;
    RoomRecordModelV2 *roomRecordModel = nil;
    
    //Find room part 1
    eHousekeepingService_GetRoomAssignmentList *request = [[eHousekeepingService_GetRoomAssignmentList alloc] init];
    
    [request setIntUsrID:[NSNumber numberWithInteger:userId]];
    
    if (lastModified.length > 0) {
        [request setStrLastModified:lastModified];
    }
    
    LogObject *logObj = [[LogObject alloc] init];
    logObj.dateTime = [NSDate date];
    logObj.log = [NSString stringWithFormat:@"<%i><findRoomAssignmentWithUserID><intUsrID:%i lastModified:%@>",(int)userId,(int)userId,lastModified];
    LogFileManager *logManager = [[LogFileManager alloc] init];
    [logManager save:logObj];
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetRoomAssignmentListUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetRoomAssignmentListResponse class]]) {
            eHousekeepingService_GetRoomAssignmentListResponse *dataBody = (eHousekeepingService_GetRoomAssignmentListResponse *)bodyPart;
            if (dataBody) {
                if ([dataBody.GetRoomAssignmentListResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]]) {
                    NSMutableArray *roomAssignment = dataBody.GetRoomAssignmentListResult.RoomAssignmentList.RoomAssign;
                    [roomsResponsed addObjectsFromArray:roomAssignment];
                }
            }
        }
    }
    
//     Remove after discuss with Chris on issue duplicated room
//    //Find room part 2
//    //Finding Rooms were managed by supervisor ID and filter with Room Attendant ID
//    eHousekeepingService_GetPrevRoomAssignmentList *requestPrev = [[eHousekeepingService_GetPrevRoomAssignmentList alloc] init];
//    [requestPrev setIntUsrID:[NSNumber numberWithInteger:supervisorId]];
//    if(lastModified.length > 0) {
//        [requestPrev setStrLastModified:lastModified];
//    }
//    
//    eHousekeepingServiceSoap12BindingResponse *responsePrev = [binding GetPrevRoomAssignmentListUsingParameters:requestPrev];
//    NSArray *responsePrevBodyParts = responsePrev.bodyParts;
//    for (id bodyPart in responsePrevBodyParts) {
//        if ([bodyPart isKindOfClass:[eHousekeepingService_GetPrevRoomAssignmentListResponse class]]) {
//            eHousekeepingService_GetPrevRoomAssignmentListResponse *dataBody = (eHousekeepingService_GetPrevRoomAssignmentListResponse *)bodyPart;
//            
//            if (dataBody) {
//                if ([dataBody.GetPrevRoomAssignmentListResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]]) {
//                    NSMutableArray *roomAssignments = dataBody.GetPrevRoomAssignmentListResult.RoomAssignmentList.RoomAssign;
//                    for (eHousekeepingService_RoomAssign *currentRoomAssign in roomAssignments) {
//                        //match with rooms of user ID is finding
//                        if([currentRoomAssign.raHousekeeperID intValue] == userId){
//                            [roomsResponsed addObject:currentRoomAssign];
//                            
//                            //Check to get room Detail data
//                            RoomModelV2 *roomModel = [[RoomModelV2 alloc] init];
//                            roomModel.room_Id = currentRoomAssign.raRoomNo;
//                            roomModel.room_AdditionalJob = currentRoomAssign.raAdditionalJob;
//                            roomModel.room_Building_Name = currentRoomAssign.raBuildingName;
//                            roomModel.room_Building_Id = [currentRoomAssign.raBuildingID intValue];
//                            roomModel.room_building_namelang = currentRoomAssign.rabuildingLang;
//                            roomModel.room_Guest_Name = [NSString stringWithFormat:@"%@%@", currentRoomAssign.raGuestFirstName == nil ? @"" : [NSString stringWithFormat:@"%@ ", currentRoomAssign.raGuestFirstName], currentRoomAssign.raGuestLastName == nil ? @"" : [NSString stringWithFormat:@"%@", currentRoomAssign.raGuestLastName]];
//                            roomModel.room_GuestReference = currentRoomAssign.raGuestPreference;
//                            roomModel.room_HotelId = [currentRoomAssign.raHotelID integerValue];
//                            roomModel.room_LastModified = currentRoomAssign.raLastModified;
//                            
//                            roomModel.room_PostStatus = POST_STATUS_UN_CHANGED;
//                            roomModel.room_remark = currentRoomAssign.raRemark;
//                            roomModel.room_TypeId = [currentRoomAssign.raRoomTypeID integerValue];
//                            roomModel.room_VIPStatusId = currentRoomAssign.raVIP;
//                            roomModel.room_zone_id = [currentRoomAssign.raFloorID integerValue];
//                            
//                            RoomModelV2 *compareRoom = [[RoomModelV2 alloc] init];
//                            compareRoom.room_Id = currentRoomAssign.raRoomNo;
//                            
//                            [[RoomManagerV2 sharedRoomManager] loadRoomModel:compareRoom];
//                            
//                            if(compareRoom.room_LastModified.length > 0){
//                                if (![compareRoom.room_LastModified isEqualToString:currentRoomAssign.raLastModified]
//                                    || ![compareRoom.room_VIPStatusId isEqualToString:currentRoomAssign.raVIP])
//                                {
//                                    [[RoomManagerV2 sharedRoomManager] updateRoomModel:roomModel];
//                                }
//                            } else {
//                                [[RoomManagerV2 sharedRoomManager] insertRoomModel:roomModel];
//                            }
//                            
//                            //Hao Tran - Remove for enhance getting data
//                            /*
//                            compareRoom.room_VIPStatusId = 0;
//                            [[RoomManagerV2 sharedRoomManager] getRoomWSByUserID:supervisorId AndRaID:[currentRoomAssign.raID integerValue] AndLastModified:nil AndLastCleaningDate:currentRoomAssign.raLastRoomCleaningDate];
//                            */
//                        }
//                    }
//                }
//            }
//        }
//    }

    if([roomsResponsed count] <= 0){
        return nil;
    }
    
    NSMutableArray *listRoomCompareBlocked = [NSMutableArray array];
    
    //Hao Tran [20141210/Fixed duplicated Room Number] - Fix RC issues
    roomsResponsed = [self removeDuplicatedRooms:roomsResponsed];
    
    //Get rooms detail and return result
    eHousekeepingService_RoomAssign *roomDetail = nil;
    for (eHousekeepingService_RoomAssign *roomAssign in roomsResponsed) {
        
        if ([[UserManagerV2 sharedUserManager].currentUserAccessRight isFindRARoomsToday]) {
            //Compare time today
            BOOL isToday = [self isToday:roomAssign.raAssignedTime];
            if(!isToday){
                continue;
            }
        }
        
        //get room detail
        roomDetail = (eHousekeepingService_RoomAssign *)[self findRoomDetailWithUserID:userId andRoomAssignmentID:[roomAssign.raID integerValue]  andLastModified:nil];
        
        roomAssignmentModel = [[RoomAssignmentModelV2 alloc] init];
        roomModel = [[RoomModelV2 alloc] init];
        roomRecordModel = [[RoomRecordModelV2 alloc] init];
        
        roomAssignmentModel.roomAssignment_Id = [roomAssign.raID intValue];
        roomAssignmentModel.roomAssignment_RoomId = roomAssign.raRoomNo;
        [listRoomCompareBlocked addObject:roomAssign.raRoomNo]; //To compare blocked room or not
        roomAssignmentModel.roomAssignment_UserId = (int)supervisorId; //fix can't show data for supervisor in find room assignment userId
        roomAssignmentModel.roomAssignment_AssignedDate = roomAssign.raAssignedTime;
        
        //20150420 - Fixed can't show previous date
        //roomAssignmentModel.roomAssignment_LastCleaningDate = roomAssign.raLastRoomCleaningDate;
        roomAssignmentModel.roomAssignment_LastCleaningDate = roomAssign.raCleanEndTm;
        roomAssignmentModel.roomAssignment_Priority = [roomDetail.raPriority intValue];
        roomAssignmentModel.roomAssignment_PostStatus = (int)POST_STATUS_UN_CHANGED;
        roomAssignmentModel.roomAssignment_LastModified = roomAssign.raLastModified;
        roomAssignmentModel.roomAssignment_Priority_Sort_Order = [roomDetail.raPrioritySortOrder integerValue];
        roomAssignmentModel.roomAssignmentHousekeeperId = [roomAssign.raHousekeeperID integerValue];
        roomAssignmentModel.roomAssignmentRoomStatusId = [roomAssign.raRoomStatusID integerValue];
        roomAssignmentModel.roomAssignmentRoomCleaningStatusId = [roomAssign.raCleaningStatusID integerValue];
        roomAssignmentModel.roomAssignmentRoomInspectionStatusId = [roomAssign.raInspectedStatus integerValue];
        roomAssignmentModel.roomAssignmentRoomExpectedCleaningTime = [roomAssign.raExpectedCleaningTime integerValue];
        roomAssignmentModel.roomAssignmentRoomExpectedInspectionTime = [roomAssign.raExpectedInspectTime integerValue];
        roomAssignmentModel.roomAssignmentGuestProfileId = [roomAssign.raGuestProfileID integerValue];
        roomAssignmentModel.roomAssignmentCleaningCredit = roomAssign.raCleaningCredit;
        NSArray *arrayName = [roomAssign.raFullName componentsSeparatedByString:@" "];
        
        if (arrayName.count > 2) {
            roomAssignmentModel.roomAssignmentFirstName = [NSString stringWithFormat:@"%@ %@", [arrayName objectAtIndex:0], [arrayName objectAtIndex:arrayName.count - 1]];
        } else {
            roomAssignmentModel.roomAssignmentFirstName = roomAssign.raFullName;
        }
        
        roomAssignmentModel.roomAssignmentGuestFirstName = roomAssign.raGuestFirstName;
        roomAssignmentModel.roomAssignmentGuestLastName = roomAssign.raGuestLastName;
        
        //set mockup room local
        NSInteger isMockupRoomValue = [roomAssign.raIsMockRoom integerValue];
        if(isMockupRoomValue == ENUM_TASK_TYPE_NORMAL)
        {
            roomAssignmentModel.raIsMockRoom = 0;
        }
        else if(isMockupRoomValue == ENUM_TASK_TYPE_REASSIGN)
        {
            roomAssignmentModel.raIsMockRoom = 0;
            //roomAssignmentModel.raIsReassignedRoom = 1;
        }
        else if(isMockupRoomValue >= ENUM_TASK_TYPE_MOCKUP)
        {
            roomAssignmentModel.raIsMockRoom = 1;
        }
        
        
        roomAssignmentModel.raKindOfRoom = [roomAssign.raKindOfRoom integerValue];
        roomAssignmentModel.raGuestDepartureTime = roomAssign.raGuestDepartureTime;
        roomAssignmentModel.raGuestArrivalTime = roomAssign.raGuestArrivalTime;
        //roomAssignmentModel.raIsReassignedRoom = (roomAssignmentModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_PENDING_SECOND_TIME)?1:0;
        
        //Quang edit
        if([roomAssign.raCleaningStatusID integerValue] == ENUM_CLEANING_STATUS_PENDING_FIRST_TIME && isMockupRoomValue == ENUM_TASK_TYPE_REASSIGN)
            roomAssignmentModel.raIsReassignedRoom = 1;
        else
            roomAssignmentModel.raIsReassignedRoom = 0;
        
        roomModel.room_Id = roomAssign.raID;
        roomModel.room_HotelId = [roomDetail.raHotelID intValue];
        roomModel.room_TypeId = [roomDetail.raRoomTypeID intValue];
        roomModel.room_Lang = nil;
        roomModel.room_Building_Name = roomAssign.raBuildingName;
        roomModel.room_building_namelang = roomAssign.rabuildingLang;
        roomModel.room_Guest_Name = roomAssign.raGuestName;
        roomModel.room_VIPStatusId = roomAssign.raVIP;
        roomModel.room_GuestReference = roomAssign.raGuestPreference;
        roomModel.room_AdditionalJob = roomDetail.raAdditionalJob;
        roomModel.room_PostStatus = (int)POST_STATUS_UN_CHANGED;
        roomModel.room_LastModified = roomDetail.raLastModified;
        roomModel.room_is_re_cleaning = NO;
        roomModel.room_expected_status_id = 0;
        roomModel.room_Building_Id = [roomAssign.raBuildingID integerValue];
        roomModel.room_zone_id = 0;
        roomModel.room_floor_id = [roomAssign.raFloorID integerValue];
        
        roomRecordModel.rrec_User_Id = (int)supervisorId; // remove userId for fix can't show data in room detail
        roomRecordModel.rrec_Cleaning_Date = roomAssign.raCleanEndTm;
        roomRecordModel.rrec_PostStatus = (int)POST_STATUS_UN_CHANGED;
        roomRecordModel.rrec_Cleaning_Start_Time = roomAssign.raCleanStartTm;
        roomRecordModel.rrec_Cleaning_End_Time = roomAssign.raCleanEndTm;
        roomRecordModel.rrec_Inspection_Start_Time = roomAssign.raInspectStartTm;
        roomRecordModel.rrec_Inspection_End_Time = roomAssign.raInspectEndTm;
        //roomRecordModel.rrec_Cleaning_Total_Pause_Time;
        //roomRecordModel.rrec_Inspected_Total_Pause_Time;
        roomRecordModel.rrec_room_assignment_id = [roomAssign.raID intValue];
        roomRecordModel.rrec_Remark = roomDetail.raRemark;
        
        NSDateFormatter *dateTimeFormat = [[NSDateFormatter alloc] init];
        [dateTimeFormat setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
        [dateTimeFormat setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
        NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
        [dateTimeFormat setLocale:usLocale];
        
        //Hao Tran - Remove for change property
        /*
        NSCalendar *calender = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        NSDateComponents *components = nil;
        
        if (roomAssign.raCleanStartTm != nil && roomAssign.raCleanEndTm != nil) {
            NSDate *cleaningStartDate = [dateTimeFormat dateFromString:roomAssign.raCleanStartTm];
            NSDate *cleaningEndDate = [dateTimeFormat dateFromString:roomAssign.raCleanEndTm];
            components = [calender components:NSHourCalendarUnit|NSMinuteCalendarUnit|NSSecondCalendarUnit fromDate:cleaningStartDate toDate:cleaningEndDate options:0];
            roomRecordModel.rrec_Cleaning_Duration = [NSString stringWithFormat:@"%ld", (long)(components.hour*3600 + components.minute*60 + components.second)];
        }
        
        if (roomAssign.raInspectStartTm != nil && roomAssign.raInspectEndTm != nil) {
            NSDate *inspectStartDate = [dateTimeFormat dateFromString:roomAssign.raInspectStartTm];
            NSDate *inspectEndDate = [dateTimeFormat dateFromString:roomAssign.raInspectEndTm];
            components = [calender components:NSHourCalendarUnit|NSMinuteCalendarUnit|NSSecondCalendarUnit fromDate:inspectStartDate toDate:inspectEndDate options:0];
            roomRecordModel.rrec_Inspection_Duration = [NSString stringWithFormat:@"%ld",(long)(components.hour*3600 + components.minute*60 + components.second)];
        }
         */
        
        roomRecordModel.rrec_Total_Cleaning_Time = [roomAssign.raTotalCleaningTime intValue] * 60;
        roomRecordModel.rrec_Total_Inspected_Time = [roomAssign.raToTalInspectionTime intValue] * 60;
        
        roomAssign.raInspectStartTm = roomDetail.raInspectStartTm;
        roomAssign.raInspectEndTm = roomDetail.raInspectEndTm;
        
        RoomStatusModelV2 *roomStatusModel = [[RoomStatusModelV2 alloc] init];
        roomStatusModel.rstat_Id = (int)roomAssignmentModel.roomAssignmentRoomStatusId;
        [[RoomManagerV2 sharedRoomManager] loadRoomStatusData:roomStatusModel];
        
        CleaningStatusModelV2 *cleaningStatusModel = [[CleaningStatusModelV2 alloc] init];
        cleaningStatusModel.cstat_Id = (int)roomAssignmentModel.roomAssignmentRoomCleaningStatusId;
        [[RoomManagerV2  sharedRoomManager] loadCleaningStatus:cleaningStatusModel];
        
        RoomTypeModel *roomTypeModel = [[RoomTypeModel alloc] init];
        roomTypeModel.amsId = [roomAssign.raRoomTypeID intValue];
        [[RoomManagerV2 sharedRoomManager] loadRoomTypeModelByPrimaryKey:roomTypeModel];
        
        NSMutableDictionary *roomAssignmentData = [[NSMutableDictionary alloc] init];
        [roomAssignmentData setValue:[NSString stringWithFormat:@"%i",roomAssignmentModel.roomAssignment_UserId] forKey:kRaUserId];
        [roomAssignmentData setValue:roomAssignmentModel.roomAssignment_RoomId forKey:kRoomNo];
        [roomAssignmentData setValue:[NSString stringWithFormat:@"%i",(int)roomAssignmentModel.roomAssignmentRoomStatusId] forKey:kRoomStatusId];
        [roomAssignmentData setValue:[NSString stringWithFormat:@"%i",(int)roomAssignmentModel.roomAssignmentRoomInspectionStatusId] forKey:kRoomInspectedStatusId];
        
        if([[LanguageManagerV2 sharedLanguageManager] currentLanguageIsEqualTo:@"%@", ENGLISH_LANGUAGE]){
            [roomAssignmentData setValue:roomStatusModel.rstat_Name forKey:kRMStatus];
            [roomAssignmentData setValue:roomTypeModel.amsName forKey:kRoomType];
        } else {
            [roomAssignmentData setValue:roomStatusModel.rstat_Lang forKey:kRMStatus];
            [roomAssignmentData setValue:roomTypeModel.amsNameLang forKey:kRoomType];
        }
        
        [roomAssignmentData setValue:[NSString stringWithFormat:@"%i",(int)roomAssignmentModel.roomAssignmentRoomCleaningStatusId] forKey:kCleaningStatusID];
        [roomAssignmentData setValue:[NSString stringWithFormat:@"%@ %@",roomAssign.raGuestFirstName == nil?@"":roomAssign.raGuestFirstName,roomAssign.raGuestLastName == nil?@"":roomAssign.raGuestLastName] forKey:kGuestName];
        [roomAssignmentData setValue:[NSString stringWithFormat:@"%i",roomAssignmentModel.roomAssignment_Id] forKey:kRoomAssignmentID];
        [roomAssignmentData setValue:roomAssign.raPrioritySortOrder forKey:kRaPrioritySortOrder];
        [roomAssignmentData setValue:roomAssignmentModel.roomAssignmentFirstName forKey:kRAFirstName];
        [roomAssignmentData setValue:roomAssignmentModel.roomAssignmentLastName forKey:kRALastName];
        [roomAssignmentData setValue:cleaningStatusModel.cstat_image forKey:kCleaningStatus];
        [roomAssignmentData setValue:roomAssign.raGuestFirstName forKey:kGuestFirstName];
        [roomAssignmentData setValue:roomAssign.raGuestLastName forKey:kGuestLastName];
        [roomAssignmentData setValue:roomAssign.raGuestArrivalTime forKey:kRaGuestArrivalTime];
        [roomAssignmentData setValue:roomAssign.raGuestDepartureTime forKey:kRaGuestDepartureTime];
        [roomAssignmentData setValue:[NSString stringWithFormat:@"%i",(int)roomAssignmentModel.raKindOfRoom] forKey:kRaKindOfRoom];
        [roomAssignmentData setValue:[NSString stringWithFormat:@"%i",(int)roomAssignmentModel.raIsMockRoom] forKey:kRaIsMockRoom];
        [roomAssignmentData setValue:roomStatusModel.rstat_Image forKey:kRoomStatusImage];
        [roomAssignmentData setValue:roomAssignmentModel.roomAssignment_AssignedDate forKey:kRoomAssignmentTime];
        [roomAssignmentData setValue:[NSString stringWithFormat:@"%i",(int)roomAssignmentModel.raIsReassignedRoom] forKey:kRaIsReassignedRoom];
        [roomAssignmentData setValue:roomAssign.raVIP forKey:kVIP];
        
        NSMutableDictionary * data = [[NSMutableDictionary alloc] init];
        [data setObject:roomAssignmentData forKey:keyRoomData];
        [data setObject:roomAssignmentModel forKey:keyRoomAssignmentModel];
        [data setObject:roomModel forKey:keyRoomModel];
        [data setObject:roomRecordModel forKey:keyRoomRecordModel];
        
        [array addObject:data];
    }
    
    [[RoomManagerV2  sharedRoomManager] getWSOOSBlockRoomListByUserId:userId hotelId:hotelId listRoomAssignmentCompare:listRoomCompareBlocked];
    
    return array;
}

-(BOOL)isToday:(NSString*) timeStr
{
    BOOL result = NO;
    if (timeStr != nil) {
        NSDateFormatter *dateTimeFormat = [[NSDateFormatter alloc] init];
        [dateTimeFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        //[dateTimeFormat setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
        
        NSDate* cleaningDate = [dateTimeFormat dateFromString:timeStr];
        
        //Remove time from cleaning date
        unsigned int flags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit;
        NSCalendar* calendar = [NSCalendar currentCalendar];
        NSDateComponents* componentRemoved = [calendar components:flags fromDate:cleaningDate];
        NSDate* cleaningDateRemoveTime = [calendar dateFromComponents:componentRemoved];
        
        //Remove time from current date
        componentRemoved = [calendar components:flags fromDate:[NSDate date]];
        NSDate* currentDateRemoveTime = [calendar dateFromComponents:componentRemoved];
        
        [dateTimeFormat setDateFormat:@"dd / MM / yyyy"];
        timeStr = [dateTimeFormat stringFromDate:cleaningDate];
        
        NSCalendar *calender = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        NSDateComponents *components = [calender components:NSDayCalendarUnit fromDate:cleaningDateRemoveTime toDate:currentDateRemoveTime options:0];
        
        if (components.day == 0) {
            //lastCleaningTime = @"Today";
            result = YES;
        }
        //else if (components.day == 1) {
        //    lastCleaningTime = @"Yesterday";
        //}
    }
    
    return result;
}

-(NSMutableArray*) removeDuplicatedRooms:(NSMutableArray*) listRoomAssignment
{
    NSMutableArray *listRemove = [NSMutableArray array];
    for (int i = 0; i < listRoomAssignment.count - 1; i++) {
        eHousekeepingService_RoomAssign *curRoom = [listRoomAssignment objectAtIndex:i];

        if ([listRemove containsObject:curRoom]) {
            continue;
        }
        
        for (int j = i + 1; j < listRoomAssignment.count; j ++) {
            eHousekeepingService_RoomAssign *curCompare = [listRoomAssignment objectAtIndex:j];
            //Compare duplicated on room number
            if([curRoom.raRoomNo isEqualToString:curCompare.raRoomNo]) {
                [listRemove addObject:curCompare];
            }
        }
    }
    
    [listRoomAssignment removeObjectsInArray:listRemove];
    return listRoomAssignment;
}

-(NSObject *)findRoomDetailWithUserID:(NSInteger)userId andRoomAssignmentID:(NSInteger)roomAssignmentId andLastModified:(NSString *)lastModified {
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    eHousekeepingService_GetRoomDetail *request = [[eHousekeepingService_GetRoomDetail alloc] init];
    
    [request setIntUsrID:[NSNumber numberWithInteger:userId]];
    
    if (lastModified != nil) {
        [request setStrLastModified:lastModified];
    }
    
    LogObject *logObj = [[LogObject alloc] init];
    logObj.dateTime = [NSDate date];
    logObj.log = [NSString stringWithFormat:@"<%i><findRoomDetailWithUserID><intUsrID:%i lastModified:%@>",(int)userId,(int)userId,lastModified];
    LogFileManager *logManager = [[LogFileManager alloc] init];
    [logManager save:logObj];
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetRoomDetailUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetRoomDetailResponse class]]) {
            eHousekeepingService_GetRoomDetailResponse *dataBody = (eHousekeepingService_GetRoomDetailResponse *)bodyPart;
            if (dataBody) {
                if ([dataBody.GetRoomDetailResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]]) {
                    
                    eHousekeepingService_RoomAssign *roomAssignment = dataBody.GetRoomDetailResult.RoomDetail;
                    
                    return roomAssignment;
                }
            }
        }
    }
    
    return nil;
}

#pragma mark - WS for Update Room Cleaning Status
-(BOOL)updateCleaningStatusWithUserID:(NSInteger)userId andRoomAssigmentMode:(RoomAssignmentModelV2 *)model{
    if([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp] == NO) return NO;
    
    BOOL result = YES;
    BOOL temp = NO;
    
    if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        if (model.roomAssignment_PostStatus == POST_STATUS_SAVED_UNPOSTED) {
            // posr assign date for Service Later status
            if(model.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_SERVICE_LATER) {
                RoomRecordModelV2 *roomRecord = [[RoomRecordModelV2 alloc] init];
                roomRecord.rrec_User_Id = (int)userId;
                roomRecord.rrec_room_assignment_id = model.roomAssignment_Id;
                roomRecord = [[RoomManagerV2 sharedRoomManager] loadRoomRecordDataByRoomIdAndUserId:roomRecord];
                
                if(roomRecord.rrec_Id > 0) {
                    RoomServiceLaterModelV2 *roomServiceLater = [[RoomServiceLaterModelV2 alloc] init];
                    roomServiceLater.rsl_RecordId = roomRecord.rrec_Id;
                    [[RoomManagerV2 sharedRoomManager] loadRoomServiceLaterDataByRecordId:roomServiceLater];
                    
                    [self postAssignDateForServiceLaterStatusWithUserID:userId andRoomAssignmentID:model.roomAssignment_Id andHouseKeepingID:model.roomAssignmentHousekeeperId andRoomServiceLaterModel:roomServiceLater];
                }
            }
                
            // update Cleaning Status
            temp = [self updateCleaningStatusForWSWithUserID:userId andRoomAssignmentID:model.roomAssignment_Id andCleaningStatusID:model.roomAssignmentRoomCleaningStatusId];
            
            if (temp == YES) {
                model.roomAssignment_PostStatus = (int)POST_STATUS_POSTED;
                [[RoomManagerV2 sharedRoomManager] update:model];
            } else
                result = NO;
        }
    }

    return result;
}

-(BOOL)updateCleaningStatusForWSWithUserID:(NSInteger)userId andRoomAssignmentID:(NSInteger)roomAssignmentId andCleaningStatusID:(NSInteger)cleaningStatusId {
    BOOL result = NO;
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole])
    {
        [binding setLogXMLInOut:YES];
    }
    eHousekeepingService_UpdateRoomCleaningStatus *request = [[eHousekeepingService_UpdateRoomCleaningStatus alloc] init];
    
    [request setIntSupervisorID:[NSNumber numberWithInteger:userId]];
    [request setIntRoomAssignID:[NSNumber numberWithInteger:roomAssignmentId]];
    [request setIntCleaningStatusID:[NSNumber numberWithInteger:cleaningStatusId]];
    
    LogObject *logObj = [[LogObject alloc] init];
    logObj.dateTime = [NSDate date];
    logObj.log = [NSString stringWithFormat:@"<%i><findRoomDetailWithUserID><intSupervisorID:%i intRoomAssignID:%i intCleaningStatusID:%i>",(int)userId,(int)userId,(int)roomAssignmentId,(int)cleaningStatusId];
    LogFileManager *logManager = [[LogFileManager alloc] init];
    [logManager save:logObj];
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding UpdateRoomCleaningStatusUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_UpdateRoomCleaningStatusResponse class]]) {
            eHousekeepingService_UpdateRoomCleaningStatusResponse *dataBody = (eHousekeepingService_UpdateRoomCleaningStatusResponse *)bodyPart;
            if (dataBody != nil) {
                if ([dataBody.UpdateRoomCleaningStatusResult.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_OK]]) {
                    //change post status
                    result = YES;
                } else {
                    result = NO;
                }
            } else {
                result = NO;
            }
        }
    }
    
    return result;
}

#pragma mark - WS for Post Assign Date for Service Later status when Update Room Cleaning Status
-(BOOL) postAssignDateForServiceLaterStatusWithUserID:(NSInteger) userId andRoomAssignmentID:(NSInteger) roomAssignmentId andHouseKeepingID:(NSInteger) houseKeepingId andRoomServiceLaterModel:(RoomServiceLaterModelV2 *)serviceLaterModel {
    BOOL result = NO;
    
    eHousekeepingServiceSoapBinding *binding = [eHousekeepingService eHousekeepingServiceSoapBinding];
   
    eHousekeepingService_AssignRoom *request = [[eHousekeepingService_AssignRoom alloc] init] ;
    [request setIntDutyAssignID:[NSNumber numberWithInteger:roomAssignmentId]];
    [request setIntHousekeeperID:[NSNumber numberWithInteger:houseKeepingId]];
    [request setIntSupervisorID:[NSNumber numberWithInteger:userId]];
    [request setStrAssignDateTime:serviceLaterModel.rsl_Time];
    
    eHousekeepingServiceSoapBindingResponse *response = [binding AssignRoomUsingParameters:request];
//    NSArray *responseHeaders = response.headers;
    
    NSArray *responseBody = response.bodyParts;
    for (id bodyPart in responseBody) {
        if ([bodyPart isKindOfClass:[SOAPFault class]]) {
            result = NO;
            continue;
        }
        
        // UpdateAsignRoom
        if ([bodyPart isKindOfClass:[eHousekeepingService_AssignRoomResponse class]]) {
            eHousekeepingService_AssignRoomResponse *body = (eHousekeepingService_AssignRoomResponse *)bodyPart;
            
            if ([body.AssignRoomResult.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]]) {
                RoomServiceLaterModelV2 *roomServiceLater = [[RoomServiceLaterModelV2 alloc] init];
                roomServiceLater.rsl_Id = serviceLaterModel.rsl_Id;
                [[RoomManagerV2 sharedRoomManager] deleteRoomServiceLaterData:roomServiceLater];
                
                result = YES;
            } else {
                result = NO;
            }
        }
    }
    
    return result;
}

@end
