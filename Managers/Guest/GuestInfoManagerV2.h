//
//  GuestInfoManagerV2.h
//  mHouseKeeping
//
//  Created by ThuongNM on 3/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GuestInfoModelV2.h"
#import "DatabaseAdapterV2.h"
#import "GuestInfoAdapterV2.h"
@interface GuestInfoManagerV2 : NSObject{
    
}
-(int)insertGuestInfoData:(GuestInfoModelV2*) guestInfo;
-(int)updateGuestInfoData:(GuestInfoModelV2*) guestInfo;
-(int)deleteGuestInfoData:(GuestInfoModelV2*) guestInfo;
-(GuestInfoModelV2 *)loadGuestInfoData:(GuestInfoModelV2*) guestInfo;
-(GuestInfoModelV2 *)loadGuestInfoDataByRoomID:(NSString*) roomNumber;
-(NSMutableArray *)loadAllGuestInfo;
@end
