//
//  GuestInfoManagerV2.m
//  mHouseKeeping
//
//  Created by ThuongNM on 3/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GuestInfoManagerV2.h"

@implementation GuestInfoManagerV2

-(int)insertGuestInfoData:(GuestInfoModelV2*) guestInfo{
    
    int returnCode=0;
//    GuestInfoAdapterV2 *adapter=[[GuestInfoAdapterV2 alloc] init];
//    [adapter openDatabase];
//    [adapter resetSqlCommand];
//    returnCode=[adapter insertGuestInfoData:guestInfo];
//    [adapter close];
    return returnCode;

}
-(int)updateGuestInfoData:(GuestInfoModelV2*) guestInfo{
    
    int returnCode=0;
//    GuestInfoAdapterV2 *adapter=[[GuestInfoAdapterV2 alloc] init];
//    [adapter openDatabase];
//    [adapter resetSqlCommand];
//    returnCode=[adapter updateGuestInfoData:guestInfo];
//    [adapter close];
    return returnCode;
}
-(int)deleteGuestInfoData:(GuestInfoModelV2*) guestInfo{
    int returnCode=0;
    GuestInfoAdapterV2 *adapter=[[GuestInfoAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    returnCode=[adapter deleteGuestInfoData:guestInfo];
    [adapter close];
    return returnCode;
}
-(GuestInfoModelV2 *)loadGuestInfoData:(GuestInfoModelV2*) guestInfo{
    GuestInfoAdapterV2 *adapter=[[GuestInfoAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    GuestInfoModelV2 *model=[adapter loadGuestInfoData:guestInfo];
    [adapter close];
    return model;
}
-(GuestInfoModelV2 *)loadGuestInfoDataByRoomID:(NSString*) roomNumber{
//    GuestInfoAdapterV2 *adapter=[[GuestInfoAdapterV2 alloc] init];
//    [adapter openDatabase];
//    [adapter resetSqlCommand];
//    GuestInfoModelV2 *model=[adapter loadGuestInfoDataByRoomID:roomNumber];
//    [adapter close];
//    return model;
    return nil;
}
-(NSMutableArray *)loadAllGuestInfo{
//    GuestInfoAdapterV2 *adapter=[[GuestInfoAdapterV2 alloc] init];
//    [adapter openDatabase];
//    [adapter resetSqlCommand];
//    NSMutableArray *array=[adapter loadAllGuestInfo];
//    [adapter close];
//    return array;
    return nil;
}
@end
