//
//  GuidelineManagerV2.h
//  mHouseKeeping
//
//  Created by TMS on 3/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GuideAdapterV2.h"
#import "GuideItemAdapterV2.h"
#import "GuideItemDetail.h"

@interface GuidelineManagerV2 : NSObject{
    GuideAdapterV2* guideAdapter;
}
@property (nonatomic, retain) GuideAdapterV2* guideAdapter;

//+ (GuidelineManagerV2*) sharedGuidesManager;
+ (GuidelineManagerV2*) sharedGuidelineManagerV2;
-(GuideModelV2*)loadGuideModel:(GuideModelV2*)guideModel;
-(NSInteger)insertGuideModel:(GuideModelV2*)guideModel;
-(NSInteger)updateGuideModel:(GuideModelV2*)guideModel;
-(NSMutableArray*)loadAddByRoomTypeGuideModel;
-(NSMutableArray *) loadAllGuideByRoomType:(NSInteger) roomType;
-(BOOL)haveGuideItemsByRoomType:(NSInteger)roomTypeId;
-(NSMutableArray *)loadAllGuideHaveGuideItems;
-(NSString *) getLatestLastModifiedOfGuide;

#pragma mark - GuideItemDetail
-(int)insertGuideItemDetail:(GuideItemDetail*)model;
-(int)deleteGuideItemDetailByGuideItemId:(int)guideItemId;
-(NSMutableArray*)loadGuideItemDetailsByGuideItemId:(int)guideItemId;

#pragma mark - GuideItem manager
-(void)loadGuideItemModel:(GuideItemsV2*)guideModel;
-(NSInteger)insertGuideItemModel:(GuideItemsV2*)guideModel;
-(NSInteger)updateGuideItemModel:(GuideItemsV2*)guideModel;
-(NSMutableArray*)loadAllGuideItemByCategoryId:(int)categoryId;
-(NSString *) getLatestLastModifiedOfGuideItem;

-(void) getGuideWSWithHotelId:(NSInteger) hotelId buidingId:(int)buidingId roomTypeId:(int)roomTypeId andLastModified:(NSString *) lastModified;
-(void) getGuideListWSWithHotelId:(NSInteger) hotelId categoryId:(int)categoryId andLastModified:(NSString *) lastModified;
    
@end
