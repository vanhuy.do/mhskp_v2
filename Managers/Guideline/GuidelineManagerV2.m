//
//  GuidelineManagerV2.m
//  mHouseKeeping
//
//  Created by TMS on 3/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GuidelineManagerV2.h"
#import "LogFileManager.h"
#import "DataBaseExtension.h"
#import "eHousekeepingService.h"
#import "ServiceDefines.h"

@implementation GuidelineManagerV2
@synthesize guideAdapter;
static GuidelineManagerV2* sharedGuidelineManagerV2Instance = nil;

+ (GuidelineManagerV2*) sharedGuidelineManagerV2;
{
	if (sharedGuidelineManagerV2Instance == nil) {
        sharedGuidelineManagerV2Instance = [[self alloc] init];
    }
    return sharedGuidelineManagerV2Instance;	
}

#pragma mark - 
#pragma mark Guide manager

-(GuideModelV2*)loadGuideModel:(GuideModelV2*)guideModel{
    
    GuideAdapterV2 *guideAdapter1 = [GuideAdapterV2 createGuideAdapter];
    [guideAdapter1 openDatabase];
    [guideAdapter1 resetSqlCommand];
    [guideAdapter1 loadGuideData:guideModel];
    [guideAdapter1 close];
    
    return guideModel;
}

-(NSInteger)insertGuideModel:(GuideModelV2*)guideModel{
    GuideAdapterV2 *guideAdapter1 = [GuideAdapterV2 createGuideAdapter];
    [guideAdapter1 openDatabase];
    [guideAdapter1 resetSqlCommand];
    int result =  [guideAdapter1 insertGuideData:guideModel];
    [guideAdapter1 close];
    
    guideAdapter1 = nil;
    return result;
}

-(NSInteger)updateGuideModel:(GuideModelV2*)guideModel{
    GuideAdapterV2 *guideAdapter1 = [GuideAdapterV2 createGuideAdapter];
    [guideAdapter1 openDatabase];
    [guideAdapter1 resetSqlCommand];
    int result = [guideAdapter1 updateGuideData:guideModel];
    [guideAdapter1 close];
    
    guideAdapter1 = nil;
    return result;
}
-(NSMutableArray*)loadAddByRoomTypeGuideModel
{
    GuideAdapterV2 *guideAdapter1 = [GuideAdapterV2 createGuideAdapter];
    [guideAdapter1 openDatabase];
    [guideAdapter1 resetSqlCommand];
    NSMutableArray* array = [guideAdapter1 loadAllGuide];
    [guideAdapter1 close];
    
    guideAdapter1 = nil;
    return array;
}

-(NSMutableArray *)loadAllGuideByRoomType:(NSInteger)roomType {
    GuideAdapterV2 *adapter = [GuideAdapterV2 createGuideAdapter];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    NSMutableArray *array = [adapter loadAllGuideByRoomTypeId:(int)roomType];
    [adapter close];
    adapter = nil;
    return array;
}

-(BOOL)haveGuideItemsByRoomType:(NSInteger)roomTypeId {
    GuideAdapterV2 *adapter = [GuideAdapterV2 createGuideAdapter];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    BOOL result = [adapter haveGuideItemsByRoomType:roomTypeId];
    [adapter close];
    adapter = nil;
    return result;
}

-(NSMutableArray *)loadAllGuideHaveGuideItems{
    GuideAdapterV2 *adapter = [GuideAdapterV2 createGuideAdapter];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    NSMutableArray *array = [adapter loadAllGuideHaveGuideItems];
    [adapter close];
    adapter = nil;
    return array;
}

-(NSString *)getLatestLastModifiedOfGuide {
    GuideAdapterV2 *adapter = [GuideAdapterV2 createGuideAdapter];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    NSString *lastModified = [adapter getLatestLastModifiedGuide];
    [adapter close];
    adapter = nil;
    return lastModified;
}

#pragma mark - GuideItemDetail

-(int)insertGuideItemDetail:(GuideItemDetail*)model
{
    GuideItemAdapterV2 *guideAdapter1 = [GuideItemAdapterV2 createGuideItemAdapter];
    [guideAdapter1 openDatabase];
    [guideAdapter1 resetSqlCommand];
    int result =  [guideAdapter1 insertGuideItemDetail:model];
    [guideAdapter1 close];
    
    guideAdapter1 = nil;
    return result;
}

-(int)deleteGuideItemDetailByGuideItemId:(int)guideItemId
{
    GuideItemAdapterV2 *guideAdapter1 = [GuideItemAdapterV2 createGuideItemAdapter];
    [guideAdapter1 openDatabase];
    [guideAdapter1 resetSqlCommand];
    int result =  [guideAdapter1 deleteGuideItemDetailsByGuideItemId:guideItemId];
    [guideAdapter1 close];
    
    guideAdapter1 = nil;
    return result;
}

-(NSMutableArray*)loadGuideItemDetailsByGuideItemId:(int)guideItemId
{
    NSMutableArray *listResults = nil;
    GuideItemAdapterV2 *guideAdapter1 = [GuideItemAdapterV2 createGuideItemAdapter];
    [guideAdapter1 openDatabase];
    [guideAdapter1 resetSqlCommand];
    listResults = [guideAdapter1 loadGuideItemDetailByGuideItemId:guideItemId];
    [guideAdapter1 close];
    guideAdapter1 = nil;
    
    return listResults;
}

#pragma mark - GuideItem manager
-(void)loadGuideItemModel:(GuideItemsV2*)guideModel{
    
    GuideItemAdapterV2 *guideAdapter1 = [GuideItemAdapterV2 createGuideItemAdapter];
    [guideAdapter1 openDatabase];
    [guideAdapter1 resetSqlCommand];
    [guideAdapter1 loadGuideItem:guideModel];
    [guideAdapter1 close];
    
    guideAdapter1 = nil;
}

-(NSInteger)insertGuideItemModel:(GuideItemsV2*)guideModel{
    GuideItemAdapterV2 *guideAdapter1 = [GuideItemAdapterV2 createGuideItemAdapter];
    [guideAdapter1 openDatabase];
    [guideAdapter1 resetSqlCommand];
    int result =  [guideAdapter1 insertGuideItem:guideModel];
    [guideAdapter1 close];
    
    guideAdapter1 = nil;
    return result;
}

-(NSInteger)updateGuideItemModel:(GuideItemsV2*)guideModel{
    GuideItemAdapterV2 *guideAdapter1 = [GuideItemAdapterV2 createGuideItemAdapter];
    [guideAdapter1 openDatabase];
    [guideAdapter1 resetSqlCommand];
    int result = [guideAdapter1 updateGuideItem:guideModel];
    [guideAdapter1 close];
    
    guideAdapter1 = nil;
    return result;
}


-(NSMutableArray*)loadAllGuideItemByCategoryId:(int)categoryId
{
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ = %d", gitem_id, guide_category_id, gitem_name, gitem_name_lang, gitem_content, gitem_content_lang, gitem_image, gitem_last_modified, GUIDE_ITEMS, guide_category_id,categoryId];
    
    DataTable *tableGuideItems = [[DataTable alloc] initWithName:[NSString stringWithFormat:@"%@", GUIDE_ITEMS]];
    
    DataRow *rowRef = [[DataRow alloc] init];
    
    NSString *gitem_idStr = [NSString stringWithFormat:@"%@",gitem_id];
    [rowRef addColumnInt:gitem_idStr];
    
    NSString *guide_category_idStr = [NSString stringWithFormat:@"%@",guide_category_id];
    [rowRef addColumnInt:guide_category_idStr];
    
    NSString *gitem_nameStr= [NSString stringWithFormat:@"%@",gitem_name];
    [rowRef addColumnString:gitem_nameStr];
    
    NSString *gitem_name_langStr = [NSString stringWithFormat:@"%@",gitem_name_lang];
    [rowRef addColumnString:gitem_name_langStr];
    
    NSString *gitem_contentStr = [NSString stringWithFormat:@"%@",gitem_content];
    [rowRef addColumnString:gitem_contentStr];
    
    NSString *gitem_content_langStr = [NSString stringWithFormat:@"%@",gitem_content_lang];
    [rowRef addColumnString:gitem_content_langStr];
    
    NSString *gitem_imageStr = [NSString stringWithFormat:@"%@",gitem_image];
    [rowRef addColumnData:gitem_imageStr];
    
    NSString *gitem_last_modifiedStr = [NSString stringWithFormat:@"%@",gitem_last_modified];
    [rowRef addColumnString:gitem_last_modifiedStr];
    
    [tableGuideItems loadTableDataWithQuery:sqlString referenceRow:rowRef];
    
    NSMutableArray *results = nil;
    if([tableGuideItems countRows] > 0)
    {
        results = [NSMutableArray array];
        
        for (DataRow *curRow in tableGuideItems.rows) {
            GuideItemsV2 *guidItem = [[GuideItemsV2 alloc] init];
            
            guidItem.gitem_id = [[curRow columnWithName:gitem_idStr] fieldValueInt];
            
            guidItem.guide_CategoryId = [[curRow columnWithName:guide_category_idStr] fieldValueInt] ;
            
            guidItem.gitem_Name = [[curRow columnWithName:gitem_nameStr] fieldValueString];
            
            guidItem.gitem_NameLang = [[curRow columnWithName:gitem_name_langStr] fieldValueString];
            
            guidItem.gitem_content = [[curRow columnWithName:gitem_contentStr] fieldValueString];
            
            guidItem.gitem_content_lang = [[curRow columnWithName:gitem_content_langStr] fieldValueString];
            
            guidItem.gitem_Image = [[curRow columnWithName:gitem_imageStr] fieldValueData];
            
            guidItem.gitem_last_modified = [[curRow columnWithName:gitem_last_modifiedStr] fieldValueString];
            
            [results addObject:guidItem];
            
        }
    }
    
    return results;
}

-(NSString *)getLatestLastModifiedOfGuideItem {
    GuideItemAdapterV2 *adapter = [[GuideItemAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    NSString *lastmodified = [adapter getLatestLastModifiedGuideItem];
    [adapter close];
    adapter = nil;
    return lastmodified;
}

#pragma mark - === Guide WS ===
#pragma mark


-(void)addImageGuideLineForDemo:(eHousekeepingService_RoomSectionList*)guideSVCItem
                  AndGuideModel:(GuideModelV2*)guideLineModel {
    guideLineModel.guide_Image = UIImagePNGRepresentation([UIImage imageNamed:@"default_white.png"]);
//    if([guideSVCItem.rsetName caseInsensitiveCompare:@"bed room"] == NSOrderedSame ){
//        guideLineModel.guide_Image = UIImageJPEGRepresentation([UIImage imageNamed:@"Bedroom.JPG"], 0);
//    } else if([guideSVCItem.rsetName caseInsensitiveCompare:@"bath room"] == NSOrderedSame){
//        guideLineModel.guide_Image = UIImageJPEGRepresentation([UIImage imageNamed:@"Bathroom.JPG"], 0);
//    } else if([guideSVCItem.rsetName caseInsensitiveCompare:@"mini bar"] == NSOrderedSame){
//        guideLineModel.guide_Image = UIImageJPEGRepresentation([UIImage imageNamed:@"MiniBar.JPG"], 0);
//    } else if([guideSVCItem.rsetName caseInsensitiveCompare:@"towel"] == NSOrderedSame){
//        guideLineModel.guide_Image = UIImageJPEGRepresentation([UIImage imageNamed:@"Towel.JPG"], 0);
//    } else if([guideSVCItem.rsetName caseInsensitiveCompare: @"writing table"] == NSOrderedSame){
//        guideLineModel.guide_Image = UIImageJPEGRepresentation([UIImage imageNamed:@"Writing table.JPG"], 0);
//    } else if([guideSVCItem.rsetName caseInsensitiveCompare:@"living room"] == NSOrderedSame){
//        guideLineModel.guide_Image = UIImagePNGRepresentation([UIImage imageNamed:@"livingroom_pic.png"]);
//    }
}

//get Room Section via WS for Guide
-(void)getGuideWSWithHotelId:(NSInteger)hotelId buidingId:(int)buidingId roomTypeId:(int)roomTypeId andLastModified:(NSString *)lastModified {
    eHousekeepingServiceSoapBinding *binding = [eHousekeepingService eHousekeepingServiceSoapBinding];
    
    if([LogFileManager isLogConsole]){
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_GetRoomSectionList *request = [[eHousekeepingService_GetRoomSectionList alloc] init];
    [request setIntHotelID:[NSNumber numberWithInteger:hotelId]];
    [request setIntRoomType:[NSNumber numberWithInt:roomTypeId]];
    [request setIntBuildingID:[NSNumber numberWithInt:buidingId]];
    
    if (lastModified) {
        [request setStrLastModified:lastModified];
    }
    
    eHousekeepingServiceSoapBindingResponse *response = [binding GetRoomSectionListUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetRoomSectionListResponse class]]) {
            eHousekeepingService_GetRoomSectionListResponse *body = (eHousekeepingService_GetRoomSectionListResponse *)bodyPart;
            
            if ([body.GetRoomSectionListResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]])
            {
                NSMutableArray *result = body.GetRoomSectionListResult.RmSec.RoomSectionList;
                for (eHousekeepingService_RoomSectionList *item in result) {
                    GuideModelV2 *model = [[GuideModelV2 alloc] init];
                    model.guide_Id = [item.rsetID intValue];
                    model.guide_RoomTypeId = [item.rsetRoomTypeID intValue];
                    
                    [self loadGuideModel:model];
                    if(model.guide_Name != nil){
                        //Add pseudo image if don't have any image from server
//                        if(![model.guide_LastUpdated isEqualToString:item.rsetLastModified]){
                        if (item.rsetPicture && item.rsetPicture.length > 0) {
                            model.guide_Image = item.rsetPicture;
                        }else{
                            [self addImageGuideLineForDemo:item AndGuideModel:model];
                        }
                        model.guide_LastUpdated = item.rsetLastModified;
                        model.guide_Name = item.rsetName;
                        model.guide_NameLang = item.rsetLang;
                        model.guide_CategoryId = [item.sgc_CategoryID intValue];
                        [self updateGuideModel:model];
//                        }
                    }
                    else{
                        //Add pseudo image if don't have any image from server
                        if(item.rsetPicture && item.rsetPicture.length > 0){
                            model.guide_Image = item.rsetPicture;
                        }else{
                            [self addImageGuideLineForDemo:item AndGuideModel:model];
                        }
                        model.guide_LastUpdated = item.rsetLastModified;
                        model.guide_Name = item.rsetName;
                        model.guide_NameLang = item.rsetLang;
                        model.guide_CategoryId = [item.sgc_CategoryID intValue];
                        [self insertGuideModel:model];
                    }
                }
            }
        }
    }
}

-(void)addImageGuideListForDemo:(eHousekeepingService_RoomSetupList*)guideSVCItem
                  AndGuideModel:(GuideItemsV2*)guideItemModel {
    if([LogFileManager isLogConsole])
    {
        NSLog(@"guideSVCItem.rsgRoomSectionName %@",guideSVCItem.rsgRoomSectionName);
    }
    
    if([guideSVCItem.rsgRoomSectionName caseInsensitiveCompare:@"bath room"] == NSOrderedSame){
        guideItemModel.gitem_Image = UIImagePNGRepresentation([UIImage imageNamed:@"bathroom_1.png"]);
    } else if([guideSVCItem.rsgRoomSectionName caseInsensitiveCompare:@"bed room"] == NSOrderedSame){
        guideItemModel.gitem_Image =UIImagePNGRepresentation([UIImage imageNamed:@"bedroom_1.png"]) ;
    } else if([guideSVCItem.rsgRoomSectionName caseInsensitiveCompare: @"mini bar"] == NSOrderedSame){
        guideItemModel.gitem_Image = UIImagePNGRepresentation([UIImage imageNamed:@"minibar_1.png"]);
    } else if([guideSVCItem.rsgRoomSectionName caseInsensitiveCompare:@"writing table"] == NSOrderedSame){
        guideItemModel.gitem_Image = UIImagePNGRepresentation([UIImage imageNamed:@"Writing table_1.png"]);
        
    } else if([guideSVCItem.rsgRoomSectionName caseInsensitiveCompare:@"towel"] == NSOrderedSame){
        guideItemModel.gitem_Image = UIImagePNGRepresentation([UIImage imageNamed:@"towel_1.png"]);
        
    } else if([guideSVCItem.rsgRoomSectionName caseInsensitiveCompare:@"living room"] == NSOrderedSame){
        guideItemModel.gitem_Image = UIImagePNGRepresentation([UIImage imageNamed:@"livingroom_pic.png"]);
    }

}

//get guide items via WS
-(void)getGuideListWSWithHotelId:(NSInteger)hotelId categoryId:(int)categoryId andLastModified:(NSString *)lastModified {
    eHousekeepingServiceSoapBinding *binding = [eHousekeepingService eHousekeepingServiceSoapBinding];
    
    if([LogFileManager isLogConsole]){
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_GetRoomSetGuideList *request = [[eHousekeepingService_GetRoomSetGuideList alloc] init];
    [request setIntHotelID:[NSNumber numberWithInteger:hotelId]];
    [request setIntCategoryID:[NSNumber numberWithInt:categoryId]];
    if (lastModified) {
        [request setStrLastModified:lastModified];
    }
    
    eHousekeepingServiceSoapBindingResponse *response = [binding GetRoomSetGuideListUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetRoomSetGuideListResponse class]]) {
            eHousekeepingService_GetRoomSetGuideListResponse *body = (eHousekeepingService_GetRoomSetGuideListResponse *)bodyPart;
            
            if ([body.GetRoomSetGuideListResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]]) 
            {
                NSMutableArray *guidelist = body.GetRoomSetGuideListResult.RoomSetupList.RoomSetupList;
                for (eHousekeepingService_RoomSetupList *item in guidelist) {
                    
                    //Delete old detail data
                    [self deleteGuideItemDetailByGuideItemId:[item.rsgID intValue]];
                    
                    GuideItemsV2 *model = [[GuideItemsV2 alloc] init];
                    model.gitem_id = [item.rsgID intValue];
                    model.guide_CategoryId = categoryId; //[item.rsgRoomSectionID integerValue];
                    model.gitem_content = item.rsgDesc;
                    model.gitem_content_lang = item.rsgLang;
                    model.gitem_Image = item.rsgPicture;
                    model.gitem_last_modified = item.rsgLastModified;
                    //20152601 - Fix show name of guideline item
//                    model.gitem_Name = item.rsgRoomSectionName;
//                    model.gitem_NameLang = item.rsgRoomSectionLang;
                    model.gitem_Name = item.sgc_Name;
                    model.gitem_NameLang = item.sgc_Lang;

                    /*
                    //Add Pseudo Image
                    if(!model.gitem_Image){
                        [self addImageGuideListForDemo:item AndGuideModel:model];
                    }*/
                    
                    for (NSData *curImage in item.rsgImages.base64Binary) {
                        GuideItemDetail *curDetail = [[GuideItemDetail alloc] init];
                        curDetail.gitem_id = [item.rsgID intValue];
                        curDetail.gitem_image = curImage;
                        [[GuidelineManagerV2 sharedGuidelineManagerV2] insertGuideItemDetail:curDetail];
                    }
                    
                    BOOL insertOK = [[GuidelineManagerV2 sharedGuidelineManagerV2] insertGuideItemModel:model];
                    if (insertOK == NO) {
                        GuideItemsV2 *gitem = [[GuideItemsV2 alloc] init];
                        gitem.gitem_id = [item.rsgID intValue];
                        [self loadGuideItemModel:gitem];
                        if (![gitem.gitem_last_modified isEqualToString:model.gitem_last_modified]) {
                            [[GuidelineManagerV2 sharedGuidelineManagerV2] updateGuideItemModel:model];
                        }
                    }
                }
            }
        }
    }
}




@end





