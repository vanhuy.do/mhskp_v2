//
//  HotelManagerV2.h
//  mHouseKeeping
//
//  Created by TMS TRIBE on 3/20/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HotelAdapterV2.h"
#import "MBProgressHUD.h"
@interface HotelManagerV2 : NSObject{
    
}


@property (nonatomic, strong) HotelAdapterV2 *adapter; 

+ (HotelManagerV2*) sharedHotelManager;

// hotel adapter
-(int) insertHotelModel:(HotelModelV2*)hotelModel;
-(int)  updateHotelModel:(HotelModelV2*)hotelModel;
-(void) loadHotelModel:(HotelModelV2*)hotelModel;
-(void) getHotelInfoOfUserID:(NSNumber *)userID andHotel :(NSNumber *)hotelID andlstModifier:(NSString *)lastModifier AndPercentView:(MBProgressHUD*)percentView;
@end
