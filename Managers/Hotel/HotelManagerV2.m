//
//  HotelManagerV2.m
//  mHouseKeeping
//
//  Created by TMS TRIBE on 3/20/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "HotelManagerV2.h"
#import "ServiceDefines.h"
#import "LogFileManager.h"
#import "LogObject.h"
#import "NetworkCheck.h"

@implementation HotelManagerV2

NSString *LOADING_GET_HOTEL_INFO = @"Get Hotel Info";

@synthesize adapter;
static HotelManagerV2* sharedHotelManagerInstance = nil;

+ (HotelManagerV2*) sharedHotelManager{
	if (sharedHotelManagerInstance == nil) {
        sharedHotelManagerInstance = [[self alloc] init];
    }
    return sharedHotelManagerInstance;
}

// HotelModel
-(int) insertHotelModel:(HotelModelV2*)hotelModel{

    HotelAdapterV2 *adapter1 = [[HotelAdapterV2 alloc] init];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    int result =  [adapter1 insertHotelData:hotelModel];
    [adapter1 close];

    adapter1 = nil;
    return result;
}

-(int) updateHotelModel:(HotelModelV2*)hotelModel {
    HotelAdapterV2 *adapter1 = [[HotelAdapterV2 alloc] init];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    int result = [adapter1 updateHotelData:hotelModel];
    [adapter1 close];

    adapter1 = nil;
    return result;
}

-(void) loadHotelModel:(HotelModelV2*)hotelModel {
    HotelAdapterV2 *adapter1 = [[HotelAdapterV2 alloc] init];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    [adapter1 loadHotelData:hotelModel];
    [adapter1 close];

    adapter1 = nil;
}
-(void) getHotelInfoOfUserID:(NSNumber *)userID andHotel :(NSNumber *)hotelID andlstModifier:(NSString *)lastModifier AndPercentView:(MBProgressHUD *)percentView
{
    if (![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        return;
    }
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole]) {
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_GetHotelInfo *request = [[eHousekeepingService_GetHotelInfo alloc] init];
    if (userID) {
        [request setIntUsrID:userID];
    }
    if (hotelID) {
        [request setIntHotelID:hotelID];
    }
    if (lastModifier) {
        [request setStrLastModified:lastModifier];
    }
    
//    LogObject *logObj = [[LogObject alloc] init];
//    logObj.dateTime = [NSDate date];
//    logObj.log = [NSString stringWithFormat:@"<%i><getHotelInfoOfUserID><intUsrID:%i intHotelID:%i lastModifier:%@>",userID,userID,hotelID,lastModifier];
//    LogFileManager *logManager = [[LogFileManager alloc] init];
//    [logManager save:logObj];
    
    if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL){return;}
    
    float currentPercent = 0;
    [self updatePercentView:percentView value:currentPercent description:LOADING_GET_HOTEL_INFO];
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetHotelInfoUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetHotelInfoResponse class]]) {
           eHousekeepingService_GetHotelInfoResponse *dataBody = (eHousekeepingService_GetHotelInfoResponse *)bodyPart; 
            if (dataBody) {
                if ([dataBody.GetHotelInfoResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]]) {
                    eHousekeepingService_Hotel *hotelObject=dataBody.GetHotelInfoResult.Hotel;
                    HotelModelV2 *hotelModel=[[HotelModelV2 alloc] init];
                    hotelModel.hotel_id=[hotelObject.htID intValue];
                    hotelModel.hotel_name_lang=hotelObject.htName;
                    hotelModel.hotel_lang=hotelObject.htLang;
                    hotelModel.hotel_logo=hotelObject.htLogo;
                    hotelModel.hotel_last_modified=hotelObject.htLastModified;
                     
                    HotelModelV2 *hotelCompared=[[HotelModelV2 alloc] init];
                    hotelCompared.hotel_id=[hotelObject.htID intValue];
                    [self loadHotelModel:hotelCompared];
                    //if (hotelCompared.hotel_id==0) { // Tuan vo
                    if (!hotelCompared.hotel_lang) {
                        [self insertHotelModel:hotelModel];
                    } else if (![hotelCompared.hotel_last_modified isEqualToString:hotelModel.hotel_last_modified])
                    {
                        [self updateHotelModel:hotelModel];
                    }
                   
                    
                }
            }
        }
    }
}

#pragma mark - Update Percent View

-(void)updatePercentView:(MBProgressHUD*)percentView value:(float)percentageValue description:(NSString *)description
{
    if (percentView) {
        NSString *percentString = [[NSString alloc]initWithFormat:@"%.0f%%",percentageValue];
        percentView.detailsLabelFont = percentView.labelFont;
        percentView.detailsLabelText = percentString;
        
        float nextStepProgress = [SyncManagerV2 getNextStepOperations];
        int maxSyncOperations = [SyncManagerV2 getMaxSyncOperations];
        if(nextStepProgress > 0 && maxSyncOperations > 0)
        {
            double currentOperations = round(percentView.progress/nextStepProgress);
            //percentView.labelText = [NSString stringWithFormat:@"%@ - [%0.f/%d]", description, currentOperations, maxSyncOperations];
            percentView.labelText = [NSString stringWithFormat:@"[%0.f/%d]", currentOperations, maxSyncOperations];
        }
        else
        {
            percentView.labelText = description;
        }
    }
}

#pragma mark -  Progress step loading
//return percentage step
-(float) caculateStepFromCountingObject:(int) countValue
{
    if(countValue == 0 || countValue == 1)
        return 100;
    
    float stepValue = (float)(100 / (float)countValue);
    return stepValue;
}
@end
