//
//  LanguageManagerV2.h
//  mHouseKeeping
//
//  Created by TMS on 3/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LanguageAdapterV2.h"
#import "LanguageModelV2.h"
#import "eHousekeepingService.h"

@interface LanguageManagerV2 : NSObject <NSXMLParserDelegate>{
    NSMutableDictionary *dic_language;
    LanguageAdapterV2* languageAdapter;
    @private NSString *currentElementName;
    @private NSMutableDictionary *tempDictionary;
    @private BOOL returnNow;

}

@property (nonatomic, strong) LanguageAdapterV2 *languageAdapter;
@property (nonatomic, strong) NSMutableDictionary *dic_language;
@property (nonatomic, strong) NSString *currentElementName;
@property (nonatomic, strong) NSMutableDictionary *tempDictionary;

+(NSMutableArray *)updateLanguageList;
+ (LanguageManagerV2*) sharedLanguageManager;

-(NSString *)getCurrentLanguage;
-(bool)currentLanguageIsEqualTo:(NSString*)languageSelected,...;
-(void)setCurrentLanguage:(NSString *) lang;
-(int) deleteAllLanguages;

-(NSMutableDictionary *)getDictionaryLanguage:(NSString *)langCode;
-(void)setDictionaryLanguage:(NSMutableDictionary *)dic;
-(NSMutableDictionary *) parseXMLToDictionary:(NSString *)langCode;
-(NSMutableDictionary *)getCurrentDictionaryLanguageByCurrentUserLang:(NSString *)langCode;
-(NSMutableDictionary *)getCurrentDictionaryLanguage;

// Language Model
-(NSString*) insertLanguageModel:(LanguageModelV2*)languageModels;
-(int) updateLanguageModel:(LanguageModelV2*)languageModels;
-(void) loadLanguageModel:(LanguageModelV2*)languageModels;
-(NSMutableArray*)loadAllLanguageModel;
-(void) loadLanguageModelByLanguageName:(LanguageModelV2 *) model;

-(NSString *) getStringLanguageByStringId:(id) stringId;

//check is ENGLISH language
-(BOOL) isEnglishLanguage;

//functions get string of language
-(NSString *) getUserID;
-(NSString *) getTitleLogin;
-(NSString *) getTitlePassword;
-(NSString *) getTitleWelcomeMsg;
-(NSString *) getCount;
-(NSString *) getLaundry;
-(NSString *) getGuide;
-(NSString *) getROOMASSIGNMENT;
-(NSString *) getLOCATION;
-(NSString *) getTOWER;
-(NSString *) getFLOORNO;
-(NSString *) getPROPERTY;
-(NSString *) getBUILDING;
-(NSString *) getCompletedRooms;
-(NSString *) getHOME;
-(NSString *) getRoom;
-(NSString*)getRoomDetailTitle;
-(NSString *) getGuest;
-(NSString *) getRMStatus;
-(NSString *) getNoOfJobs;
-(NSString *) getVIP;
-(NSString *) getDuration;
-(NSString *) getCleaningStatus;
-(NSString *) getHousekeeperInfo;
-(NSString *) getHousekeeper;
-(NSString *) getTotalsRoomsCleaned;
-(NSString *) getTotalTimeUsed;
-(NSString *) getAverageCleaningTime;
-(NSString *) getRoomDetail;
-(NSString *) getGuestInfo;
-(NSString *) getExpectedCleaningTime;
-(NSString *) getGuestPreference;
-(NSString *) getAdditionalJob;
-(NSString *) getLOSTANDFOUND;
-(NSString *) getENGINEERING;
-(NSString *) getGUIDELINE;
-(NSString *) getCOUNT;
-(NSString *) getLAUNDRY;
-(NSString *) getGUIDELINES;
-(NSString *) getMESSAGELIST;
-(NSString *) getMESSAGEDETAIL;
-(NSString *) getNEWMESSAGE;
-(NSString *) getMessageTopic;
-(NSString *) getMessageTemplate;
-(NSString *) getMessage;
-(NSString *) getUser;
-(NSString *) getVIPStatus;
-(NSString *) getLanguagePreference;
-(NSString *) getCheckIn;
-(NSString *) getCheckOut;
-(NSString *) getSelectAction;
-(NSString *) getLoadingData;
-(NSString *) getPleasewaitdot;
-(NSString *) getCheckListRoom;
-(NSString *) getBed;
-(NSString *) getBathRoom;
-(NSString *) getWritingDesk;
-(NSString *) getLivingRoom;
-(NSString *) getMirror;
-(NSString *) getPass;
-(NSString *) getFail;
-(NSString *) getRating;
-(NSString *) getComment;
-(NSString *) getLanguage;
-(NSString *) getDone;
-(NSString *) getCancel;
-(NSString *) getOK;
-(NSString *) getYes;
-(NSString *) getNo;
-(NSString *) get7pt;
-(NSString *) getMiniBar;
-(NSString *) getCHARGEABLE;
-(NSString *) getNONCHARGEABLE;
-(NSString *) getBeverage;
-(NSString *) getSnack;
-(NSString *) getWater;
-(NSString *) getItems;
-(NSString *) getQuantity;
-(NSString *) getPrice;
-(NSString *) getEngineer;
//-(NSString *) getMoveUp;
//-(NSString *) getMoveDown;
//-(NSString *) getViewDetails;
-(NSString *) getEngineeringCase;
-(NSString *) getLocation;
-(NSString *) getTypeofItem;
-(NSString *) getTop10Items;
-(NSString *) getRemark;
-(NSString *) getIssue;
-(NSString *) getATTACHPHOTO;
-(NSString *) getDISPATCH;
-(NSString *) getSelectLocation;
-(NSString *) getSelectPhoto;
-(NSString *) getLostandFountCase;
-(NSString *) getCategories;
-(NSString *) getColour;
-(NSString *) getOnlyavailableforSupervisor;
-(NSString *) getIncorrectUsernameorPassworddot;
-(NSString *) getLoadingRoomData;
-(NSString *) getAreYouSure;
-(NSString *) getSigningIndot;
-(NSString *) getSavesuccessfully;
-(NSString *) getSentsuccessfully;
-(NSString *) getErrorSent;
-(NSString *) getMessageResetDb;
-(NSString *) getMessagePeriod7Days;
-(NSString *) getMessagePeriod30Days;
-(NSString *) getSyncompleted;
-(NSString *) getSyncingdot;
-(NSString *) getUserlocked;
-(NSString *) getPleaseinputvaliddate;
-(NSString *) getName;
-(NSString *) getQuantity;
-(NSString *) getPrice;
-(NSString *) getNew;
-(NSString *) getDeleteAll;
-(NSString *) getView;
-(NSString *) getAddNew;
-(NSString *) getCollected;
-(NSString *) getTotalCharge;
-(NSString *) getFrom;
-(NSString *) getAmenities;
-(NSString *) getLinen;
-(NSString *) getDelete;
-(NSString *) getTakePhoto;
-(NSString *) getEdit;
-(NSString *) getSend;
-(NSString *) getNonetworkconnection;
-(NSString *) getNOROOMASSIGNED;
-(NSString *) getNOMESSAGE;
-(NSString *) getSavingData;
-(NSString *) getSendingData;
-(NSString *) getSendingfailedpleasetryagain;
-(NSString *) getSendingsuccessfully;
-(NSString *) getRefreshingdatahascompleted;
-(NSString *) getCHECKLIST;
-(NSString *) getMESSAGE;
-(NSString *) getSAVE;
-(NSString *) getSYNCNOW;
-(NSString *) getSETTING;
-(NSString *) getLOGOUT;
//-(NSString *) getLanguagepackloaded;
//-(NSString *) getDataerror;
//-(NSString *) getNodataloaded;
//-(NSString *) getOthererror;
//-(NSString *) getDND;
//-(NSString *) getSERVICELATER;
//-(NSString *) getDENIEDSERVICE;
//-(NSString *) getSTART;
//-(NSString *) getFINISH;
//-(NSString *) getPAUSE;
-(NSString *) getROOMCOMPLETED;
-(NSString *) getBack;
-(NSString *) getAction;
-(NSString *) getSearch;
-(NSString *) getError;
-(NSString *) getSyncincomplete;
-(NSString *) getNewmessage;
-(NSString *) getListuser;
-(NSString *) getCantsavewhileediting;
-(NSString *) getYoucanonlyattach3;
-(NSString *) getNomoreitem;
-(NSString *) getAdditemsuccessful;
-(NSString *) getUpdateitemsuccessful;
-(NSString *) getfrom5to60;
-(NSString *) getPleasechoose;

-(NSString *) getMsgNotSupervisor;
-(NSString *) getMsgIncorrectLogin;
-(NSString *) getMsgWaitingRoomDetail;
-(NSString *) getMsgWaitingRoomDetailContent;
-(NSString *) getMsgLogout;
-(NSString *) getMsgAuthenticating;
-(NSString *) getMsgAuthenticatingContent;
-(NSString *) getMsgSaveMessage;
-(NSString *) getMsgSyncMessage;
-(NSString *) getMsgSyncRunningMessage;
-(NSString *) getMsgAlertSaveAsign;
-(NSString *) getRaSavingData;
-(NSString *) getSendingData;
-(NSString *) getSendingDataFail;
-(NSString *) getSendingDataSuccesfully;
-(NSString *) getMsgRefreshingData;
-(NSString *) getError;
-(NSString *) getOK;
-(NSString *) getCancel;
-(NSString *) getYes;
-(NSString *) getNo;
-(NSString *) getLogout;
-(NSString *) getBtnSyncNow;
-(NSString *) getMsgSyncIncomplete;
-(NSString *) getMsgCanNotSaveWhenEdit;
-(NSString *) getRaLoadingData;
-(NSString *) getNoWifi;
-(NSString *) getMsgCanOnlyChoose3Picture;
-(NSString *) getMsgDoNotHaveMoreItemToAdd;
-(NSString *) getMsgAddItemSuccessfully;
-(NSString *) getMsgUpdateItemSuccessfully;
-(NSString *) getDone;
-(NSString *) getComment;
-(NSString *) getMsgSyncPeriodSetting;
-(NSString *) getMsgPleaseChoose;
-(NSString *) getLocations;
-(NSString *) getTypeOfItem;
-(NSString *) getTop10Item;
-(NSString *) getQuantity;
-(NSString *) getColour;
-(NSString *) getCategories;
-(NSString *) getBack;
-(NSString *) getNoRoomAssigned;
-(NSString *) getDialogTotalCharge;
-(NSString *) getLanguage;
-(NSString *) getBtnSetting;
-(NSString *) getEdit;
-(NSString*) getCounts;
-(NSString *) getLaundryRegular;
-(NSString *) getLaundryExpress;
-(NSString *) getRegular;
-(NSString *) getExpress;
-(NSString *) getMale;
-(NSString *) getFemale;
-(NSString *) getInstruction;
-(NSString *) getGrandTotal;
-(NSString *) getAddToCart;
-(NSString *) getUsed;
-(NSString *) getCart;
-(NSString *) getSubtotal;
-(NSString *) getPointsReceived;
-(NSString *) getPointsPossible;
-(NSString *) getPointsTotal;
-(NSString *) getRoomStandards;
-(NSString *) getChecklistStatus;
-(NSString *) getOverral;
-(NSString *)getUnassignedRoom;
-(NSString *)getAssignedRoom;

#pragma mark - Find Function
-(NSString *) getAttendant;
-(NSString *) getRoomTitle;
-(NSString *) getRoomTitleByFindStatus;
-(NSString *) getRoomTitleByFindRoomNo;
-(NSString *) getAdditionalJobTitle;
-(NSString *) getNoResultsFound;
-(NSString *) getFind;
-(NSString *) getRoomAttendant;
-(NSString *) getBy;
-(NSString *) getAll;
-(NSString *) getByRoomStatus;
-(NSString *) getByRoomNo;
-(NSString *) getCurrent;
-(NSString *) getTime;
-(NSString *) getCleaningRoom;
-(NSString *) getRAName;
-(NSString *) getALL;
-(NSString *) getNoRoom;
-(NSString *) getFailInspection;
-(NSString *) getCompletedRoom;
-(NSString *) getPassInspection;
-(NSString *) getPendingRoom;
-(NSString *) getAheadSchedule;
-(NSString *) getRoomPending;
-(NSString *) getRoomComplete;

#pragma mark - Message Alert Box
-(NSString *) getMsgAlertSaveCheckList;
-(NSString *) getMsgAlertSaveCheckListDetail;
-(NSString *) getMsgAlertErrorCheckListDetail;
-(NSString *) getMsgAlertSaveCounts;
-(NSString *) getMsgAlertDiscardCounts;

-(NSString *) getMsgPostFailMessage;
-(NSString *) getMsgAlertSaveRoomDetail;
-(NSString *) getAlertTitle;
-(NSString *) getMsg_Dispatch_This_For_PickUp;
@end
