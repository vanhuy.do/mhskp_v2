//
//  LogFileManager.h
//  mHouseKeeping
//
//  Created by Khanh Phan on 11/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LogObject.h"

@interface LogFileManager : NSObject
{
    
}

-(void) save:(LogObject *) logContent;
-(NSArray *) getLogContent:(int) daysAgo inLimit:(int)limit;

//Allow write log in console debug or not
+(void)setLogConsole:(BOOL)isWriteLog;

//return true if allow write log console
//return false if not
+(BOOL)isLogConsole;

//Write Log in Debug mode
+(void)logDebugMode:(NSString *)content,...;

//MARK: Only apply in debug mode
//-(void)saveUserName:(NSString*) userName password:(NSString*)password;

@end
