//
//  LogFileManager.m
//  mHouseKeeping
//
//  Created by Khanh Phan on 11/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LogFileManager.h"
#import "ehkDefines.h"
#import "NSFileManager+DoNotBackup.h"

@implementation LogFileManager

static bool isConsoleLog = NO;

//Allow write log in console debug or not
+(void)setLogConsole:(BOOL)isWriteLog
{
    //    [[NSUserDefaults standardUserDefaults] setBool:isWriteLog forKey:CONSOLE_LOG];
    isConsoleLog = isWriteLog;
}

//return true if allow write log console
//return false if not
+(BOOL)isLogConsole
{
    //    BOOL isAllowWriteLog = [[NSUserDefaults standardUserDefaults] boolForKey:CONSOLE_LOG];
    return isConsoleLog;
}

//Write Log in Debug mode
+(void)logDebugMode:(NSString *)content,...
{
    if(isConsoleLog)
    {
        NSString *fullContents;
        va_list args;
        va_start(args, content);
        fullContents = [[NSString alloc] initWithFormat:content arguments:args];
        va_end(args);

        NSLog(@"%@",fullContents);
    }

    NSNumber *enableLog = [[NSUserDefaults standardUserDefaults] objectForKey:ENABLE_LOG];
    if (enableLog != nil && enableLog.intValue == 0) {
        return;
    }else{
        NSString *fullContents;
        va_list args;
        va_start(args, content);
        fullContents = [[NSString alloc] initWithFormat:content arguments:args];
        va_end(args);

        LogObject *logObj = [[LogObject alloc] init];
        logObj.dateTime = [NSDate date];
        logObj.log = fullContents;
        LogFileManager *logManager = [[LogFileManager alloc] init];
        [logManager save:logObj];
    }
}


-(void) save:(LogObject *)logContent
{
    NSNumber *enableLog = [[NSUserDefaults standardUserDefaults] objectForKey:ENABLE_LOG];
    if (enableLog != nil && enableLog.intValue == 0) {
        return;
    }

    NSDate *lastSaveDate = [[NSUserDefaults standardUserDefaults] objectForKey:LAST_LOGGING_DATE];
    NSDate *now = [NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd-MM-yyyy"];
    NSString *keyRoot = [dateFormat stringFromDate:now];
    [dateFormat setDateFormat:@"dd-MM-yyyy HH:mm:ss"];
    now = [dateFormat dateFromString:[NSString stringWithFormat:@"%@ %@",keyRoot,@"00:00:00"]];

    //    NSLog(@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:LAST_LOGGING_DATE]);
    //20150501 - Fix crash when login
    //int dayDistance = [self getDayOfYear:now] - [self getDayOfYear:lastSaveDate];

    NSCalendar *calendar = [NSCalendar currentCalendar];

    [calendar rangeOfUnit:NSDayCalendarUnit startDate:&lastSaveDate
                 interval:NULL forDate:lastSaveDate];
    [calendar rangeOfUnit:NSDayCalendarUnit startDate:&now
                 interval:NULL forDate:now];

    NSDateComponents *difference = [calendar components:NSDayCalendarUnit
                                               fromDate:lastSaveDate
                                                 toDate:now options:0];
    //NSLog(@"%i", [difference day]);
    int dayDistance = ABS([difference day]);

    int lastSavePos = [[[NSUserDefaults standardUserDefaults] objectForKey:LAST_LOG_FILE_POSITION] intValue];

    int pos = (lastSavePos + dayDistance)%7;

    if (dayDistance > 0) {
        [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithInt:pos] forKey:LAST_LOG_FILE_POSITION];
        [[NSUserDefaults standardUserDefaults] setValue:now forKey:LAST_LOGGING_DATE];
    }

    NSString *fileName = [NSString stringWithFormat:@"%i.plist",pos];

    NSString *error = nil;
    NSPropertyListFormat format;

    BOOL wasCreateToSave = YES;
    //NSString *rootPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *rootPath = [NSSearchPathForDirectoriesInDomains(DataDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *pListPath = [rootPath stringByAppendingPathComponent:fileName];
    if (![[NSFileManager defaultManager] fileExistsAtPath:pListPath]) {
        NSString *source = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%i",pos] ofType:@"plist"];
        [[NSFileManager defaultManager] copyItemAtPath:source toPath:pListPath error:nil];
        wasCreateToSave = NO;
    }

    NSMutableArray *content = nil;
    NSData *plistData = [[NSFileManager defaultManager] contentsAtPath:pListPath];
    NSMutableArray *rootElement = (NSMutableArray*)[(NSDictionary *)[NSPropertyListSerialization propertyListFromData:plistData mutabilityOption:NSPropertyListMutableContainersAndLeaves format:&format errorDescription:&error] objectForKey:keyRoot];
    if (!rootElement && wasCreateToSave) {
        if([LogFileManager isLogConsole])
        {
            NSLog(@"Save in new file.");
        }
        content = [[NSMutableArray alloc] init];
    }
    else {
        content = [[NSMutableArray alloc] initWithArray:rootElement];
    }

    NSMutableDictionary *newDic = [[NSMutableDictionary alloc] init];

    NSMutableDictionary *element = [[NSMutableDictionary alloc] init];
    [element setValue:logContent.dateTime forKey:@"dateTime"];
    [element setValue:logContent.log forKey:@"log"];
    [content addObject:element];

    [newDic setValue:content forKey:keyRoot];

    plistData = [NSPropertyListSerialization dataFromPropertyList:newDic format:NSPropertyListXMLFormat_v1_0 errorDescription:&error];
    if (plistData) {
        [plistData writeToFile:pListPath atomically:YES];
    }
    else {
        if([LogFileManager isLogConsole])
        {
            NSLog(@"Error in saveData: %@",error);
        }
    }
}

-(NSArray *) getLogContent:(int)daysAgo inLimit:(int)limit
{
    NSDate *now = [NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd-MM-YYYY"];

    NSDate *selectedDate = [now dateByAddingTimeInterval:-daysAgo*24*60*60];
    NSString *keyRoot = [dateFormat stringFromDate:selectedDate];

    NSDate *lastSaveDate = (NSDate*)[[NSUserDefaults standardUserDefaults] objectForKey:LAST_LOGGING_DATE];
    NSUInteger days = [self getDayOfYear:now] - [self getDayOfYear:lastSaveDate];
    if (days > 6) {
        return [NSMutableArray new];
    }

    days = days - daysAgo;
    int current = [[[NSUserDefaults standardUserDefaults] objectForKey:LAST_LOG_FILE_POSITION] intValue];
    int selectedPos = (int)(current + 7  + days);
    selectedPos = selectedPos%7;
    NSString *fileName = [NSString stringWithFormat:@"%i%@",selectedPos,@".plist"];

    NSString *error = nil;
    NSPropertyListFormat format;

    //NSString *rootPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *rootPath = [NSSearchPathForDirectoriesInDomains(DataDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *pListPath = [rootPath stringByAppendingPathComponent:fileName];
    if (![[NSFileManager defaultManager] fileExistsAtPath:pListPath]) {
        NSString *source = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%i",selectedPos] ofType:@"plist"];
        [[NSFileManager defaultManager] copyItemAtPath:source toPath:pListPath error:nil];
    }

    NSData *plistData = [[NSFileManager defaultManager] contentsAtPath:pListPath];
    NSMutableArray *rootElement = (NSMutableArray*)[(NSDictionary *)[NSPropertyListSerialization propertyListFromData:plistData mutabilityOption:NSPropertyListMutableContainersAndLeaves format:&format errorDescription:&error] objectForKey:keyRoot];
    if([LogFileManager isLogConsole])
    {
        if (!rootElement) {
            NSLog(@"Error: %@ Format: %d", error, (int)format);
        }
    }

    dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd-MM-YYYY hh:mm a"];
    int size = (int)([rootElement count]>limit?limit:[rootElement count]);

    NSMutableArray *log = [[NSMutableArray alloc] initWithCapacity:size];

    int startIndex = (int)([rootElement count]>limit?([rootElement count] - limit):0);

    for (int i = startIndex; i < startIndex + size; i++) {
        NSDictionary *dic = [rootElement objectAtIndex:i];
        NSDate *date = [dic objectForKey:@"dateTime"];
        NSString *content = [NSString stringWithFormat:@"<%@>%@",[dateFormat stringFromDate:date],[dic objectForKey:@"log"]];
        //        NSLog(@"%@",content);
        [log addObject:content];
    }

    //    for (NSDictionary *dic in rootElement) {
    //        NSDate *date = [dic objectForKey:@"dateTime"];
    //        NSString *content = [NSString stringWithFormat:@"<%@>%@",[dateFormat stringFromDate:date],[dic objectForKey:@"log"]];
    //        NSLog(@"%@",content);
    //        [log addObject:content];
    //    }

    return log;
}

-(NSUInteger) getDayOfYear:(NSDate *) date {
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    return [calendar ordinalityOfUnit:NSDayCalendarUnit inUnit:NSYearCalendarUnit forDate:date];
}

@end
