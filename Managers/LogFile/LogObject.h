//
//  LogObject.h
//  mHouseKeeping
//
//  Created by Khanh Phan on 11/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LogObject : NSObject
{
    
}

@property (strong, nonatomic) NSDate *dateTime;
@property (strong, nonatomic) NSString *log;

@end
