//
//  LogWSFileManager.h
//  mHouseKeeping
//
//  Created by Adam Hong Xiang Guang on 25/09/2017.
//
//

@interface LogWSFileManager : NSObject

+(void)saveWSLog:(NSString *)logContent;

@end
