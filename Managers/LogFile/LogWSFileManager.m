//
//  LogWSFileManager.m
//  mHouseKeeping
//
//  Created by Adam Hong Xiang Guang on 25/09/2017.
//
//

#import "LogWSFileManager.h"
#import "LogObject.h"
#import "LogFileManager.h"
#import "NSFileManager+DoNotBackup.h"


@implementation LogWSFileManager

+(void)saveWSLog:(NSString *)logContent{
    BOOL isWSLogEnable = [[NSUserDefaults standardUserDefaults] boolForKey:ENABLE_LOGWS];
    if(isWSLogEnable){
        LogObject *logObject = [[LogObject alloc]init];
        logObject.dateTime = [NSDate date];
        logObject.log = logContent;
        
        NSDate *lastSaveDate = [[NSUserDefaults standardUserDefaults] objectForKey:LAST_LOGGING_DATE];
        NSDate *now = [NSDate date];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"dd-MM-yyyy"];
        NSString *keyRoot = [dateFormat stringFromDate:now];
        [dateFormat setDateFormat:@"dd-MM-yyyy HH:mm:ss"];
        now = [dateFormat dateFromString:[NSString stringWithFormat:@"%@ %@",keyRoot,@"00:00:00"]];
        
        //    NSLog(@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:LAST_LOGGING_DATE]);
        //20150501 - Fix crash when login
        //int dayDistance = [self getDayOfYear:now] - [self getDayOfYear:lastSaveDate];
        
        NSCalendar *calendar = [NSCalendar currentCalendar];
        
        [calendar rangeOfUnit:NSDayCalendarUnit startDate:&lastSaveDate
                     interval:NULL forDate:lastSaveDate];
        [calendar rangeOfUnit:NSDayCalendarUnit startDate:&now
                     interval:NULL forDate:now];
        
        NSDateComponents *difference = [calendar components:NSDayCalendarUnit
                                                   fromDate:lastSaveDate
                                                     toDate:now options:0];
        //NSLog(@"%i", [difference day]);
        int dayDistance = ABS([difference day]);
        
        int lastSavePos = [[[NSUserDefaults standardUserDefaults] objectForKey:LAST_LOG_FILE_POSITION] intValue];
        
        int pos = (lastSavePos + dayDistance)%7;
        
        if (dayDistance > 0) {
            [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithInt:pos] forKey:LAST_LOG_FILE_POSITION];
            [[NSUserDefaults standardUserDefaults] setValue:now forKey:LAST_LOGGING_DATE];
        }
        
        NSString *fileName = [NSString stringWithFormat:@"%i.plist",pos];
        
        NSString *error = nil;
        NSPropertyListFormat format;
        
        BOOL wasCreateToSave = YES;
        //NSString *rootPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *rootPath = [NSSearchPathForDirectoriesInDomains(DataDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *pListPath = [rootPath stringByAppendingPathComponent:fileName];
        if (![[NSFileManager defaultManager] fileExistsAtPath:pListPath]) {
            NSString *source = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%i",pos] ofType:@"plist"];
            [[NSFileManager defaultManager] copyItemAtPath:source toPath:pListPath error:nil];
            wasCreateToSave = NO;
        }
        
        NSMutableArray *content = nil;
        NSData *plistData = [[NSFileManager defaultManager] contentsAtPath:pListPath];
        NSMutableArray *rootElement = (NSMutableArray*)[(NSDictionary *)[NSPropertyListSerialization propertyListFromData:plistData mutabilityOption:NSPropertyListMutableContainersAndLeaves format:&format errorDescription:&error] objectForKey:keyRoot];
        if (!rootElement && wasCreateToSave) {
            if([LogFileManager isLogConsole])
            {
                NSLog(@"Save in new file.");
            }
            content = [[NSMutableArray alloc] init];
        }
        else {
            content = [[NSMutableArray alloc] initWithArray:rootElement];
        }
        
        NSMutableDictionary *newDic = [[NSMutableDictionary alloc] init];
        
        NSMutableDictionary *element = [[NSMutableDictionary alloc] init];
        [element setValue:logObject.dateTime forKey:@"dateTime"];
        [element setValue:logObject.log forKey:@"log"];
        [content addObject:element];
        
        [newDic setValue:content forKey:keyRoot];
        
        plistData = [NSPropertyListSerialization dataFromPropertyList:newDic format:NSPropertyListXMLFormat_v1_0 errorDescription:&error];
        if (plistData) {
            [plistData writeToFile:pListPath atomically:YES];
        }
        else {
            if([LogFileManager isLogConsole])
            {
                NSLog(@"Error in saveData: %@",error);
            }
        }    }
}

@end
