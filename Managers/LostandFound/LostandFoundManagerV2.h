//
//  LostandFoundManagerV2.h
//  mHouseKeeping
//
//  Created by TMS on 3/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LaFCategoryAdapterV2.h"
#import "LaFItemAdapterV2.h"
#import "LaFImagesAdapterV2.h"
#import "LostandFoundDetailAdapterV2.h"
#import "LostandFoundAdapterV2.h"
#import "LaFColorAdapterV2.h"
#import "ehkConvert.h"

@interface LostandFoundManagerV2 : NSObject

// create the instance
+ (LostandFoundManagerV2*) sharedLostandFoundManagerV2;

#pragma mark - L&F Category into DB
-(int)insertCategoryData:(LFCategoryModelV2 *) category;
-(int)updateCategoryData:(LFCategoryModelV2 *) category;
-(int)deleteCategoryData:(LFCategoryModelV2 *) category;
-(LFCategoryModelV2 *)loadCategoryData:(LFCategoryModelV2 *) categoryData;
-(NSMutableArray *)loadAllCategoryData;

#pragma mark - L&F Item into DB
-(int)insertItemData:(LFItemModelV2 *) item;
-(int)updateItemData:(LFItemModelV2 *) item;
-(int)deleteItemData:(LFItemModelV2 *) item;
-(LFItemModelV2 *)loadItemData:(LFItemModelV2 *) itemData;
-(NSMutableArray *)loadAllItemData;
-(NSMutableArray *)loadAllItemDataByCategoryId:(int) categoryId;

#pragma mark - L&F Image into DB
-(int)insertImageData:(LFImageModelV2 *) imageData;
-(int)updateImageData:(LFImageModelV2 *) imageData;
-(int)deleteImageData:(LFImageModelV2 *) imageData;
-(int)deleteAllImageDataByDetailId:(int) lfDetailId;
-(LFImageModelV2 *)loadImageData:(LFImageModelV2 *) imageData;
-(NSMutableArray *)loadAllImageDataByLFDetailId:(int) lfDetailId;

#pragma mark - L&F into DB
-(int)insertLaFData:(LostandFoundModelV2 *) lafData;
-(int)updateLaFData:(LostandFoundModelV2 *) lafData;
-(int)deleteLaFData:(LostandFoundModelV2 *) lafData;
-(int)deleteAllLaFData:(int) lfDetailId;
-(LostandFoundModelV2 *)loadLaFData:(LostandFoundModelV2 *) lafData;
-(NSMutableArray *)loadAllLaFDataByLaFDetailId:(int) lfDetailId;

#pragma mark - L&F Detail into DB
-(int)insertLaFDetailData:(LostandFoundDetailModelV2 *) lafDetailData;
-(int)updateLaFDetailData:(LostandFoundDetailModelV2 *) lafDetailData;
-(int)deleteLaFDetailData:(LostandFoundDetailModelV2 *) lafDetailData;
-(LostandFoundDetailModelV2 *)loadLaFDetailData:(LostandFoundDetailModelV2 *) lafDetailData;
-(LostandFoundDetailModelV2 *)loadLaFDetailDataBy_UserIdand_RoomId:(LostandFoundDetailModelV2 *) lafDetailData;
-(NSMutableArray *) loadAllLaFDetailByUserId:(NSInteger) userId;
 
#pragma mark - L&F Color into DB
-(int)insertColorData:(LFColorModelV2 *) colorData;
-(int)updateColorData:(LFColorModelV2 *) colorData;
-(int)deleteColorData:(LFColorModelV2 *) colorData;
-(LFColorModelV2*)loadColorData:(LFColorModelV2 *) colorData;
-(NSMutableArray *)loadAllColors;

#pragma mark - Get L&F Category Data from WS
//-(void)getLaFCategoryFromWS:(NSNumber *) userId andLastModifier:(NSString *) lastModifier;
-(void)getLaFCategoryFromWS:(NSNumber *)userId
            andLastModifier:(NSString *)lastModifier andHotelID:(NSString *)hotelId;
#pragma mark - Get L&F Item Data from WS
//-(void)getLaFItemFromWS:(NSNumber *) userId andLastModifier:(NSString *) lastModifier;
-(void)getLaFItemFromWS:(NSNumber *)userId andLastModifier: (NSString*)lastModifier andHotelID:(NSString *)hotelId;
#pragma mark - Get L&F Color Data from WS
-(void)getLaFColorFromWS:(NSNumber *)userId andLastModifier:(NSString*)lastModifier;

#pragma mark- Post L&F Data from WS
-(int)postLostandFound:(LostandFoundDetailModelV2 *) lfDetail;

#pragma mark - Post L&F Photo from WS
-(BOOL)postLaFPhoto:(LostandFoundDetailModelV2 *) lafDetail andPhoto:(LFImageModelV2 *) lfPhoto;

#pragma mark - Scale Image Photo
- (UIImage*)resizeImage:(UIImage*)image maxSize:(CGFloat)maxSize;

#pragma mark - === Calculate Must Post L&F ===
#pragma mark
-(NSInteger) numberOfMustPostLaFRecords;

#pragma mark - Post WSLog
- (BOOL) postWSLog:(NSInteger)userId Message:(NSString *)messageValue MessageType:(NSInteger)type;

@end
