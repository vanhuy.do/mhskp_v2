//
//  LostandFoundManagerV2.m
//  mHouseKeeping
//
//  Created by TMS on 3/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LostandFoundManagerV2.h"
#import "LogFileManager.h"
#import "LogObject.h"
#import "eHousekeepingService.h"
#import "DataBaseExtension.h"
#import "UserManagerV2.h"

@implementation LostandFoundManagerV2

static LostandFoundManagerV2* sharedLostandFoundManagerV2Instance = nil;

+ (LostandFoundManagerV2*) sharedLostandFoundManagerV2 {
//	if (sharedLostandFoundManagerV2Instance == nil) {
//        sharedLostandFoundManagerV2Instance = [[super allocWithZone:NULL] init];
//    }
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedLostandFoundManagerV2Instance = [[LostandFoundManagerV2 alloc] init];
    });
    return sharedLostandFoundManagerV2Instance;
}

//+ (id)allocWithZone:(NSZone *)zone
//{
//    return [[self sharedLostandFoundManagerV2] retain];
//}
//
//- (id)copyWithZone:(NSZone *)zone
//{
//    return self;
//}
//
//- (id)retain
//{
//    return self;
//}
//
//- (NSUInteger)retainCount
//{
//    return NSUIntegerMax;  //denotes an object that cannot be released
//}
//
//
//- (id)autorelease
//{
//    return self;
//}
//

#pragma mark - L&F Category into DB 
-(int)insertCategoryData:(LFCategoryModelV2*) category {
    int returnCode = 0;
    
    /*
     if thread != sync thread
     {
        flag stop sync = YES
        while(flag can get data == NO) {
            wait ...
        }
     }
     
     if thread == sync thread {
        while (flag stop sync == YES) {
            wait ...
        }
     
        flag can get data = NO
     }
     
     */
    
    LaFCategoryAdapterV2 *adapterCategory = [[LaFCategoryAdapterV2 alloc] init];
    [adapterCategory openDatabase];
    [adapterCategory resetSqlCommand];
    returnCode = [adapterCategory insertCategoryData:category];
    [adapterCategory close];
    
    /*
     if thread == sync method
     {
        flag can get data = YES
     }
     
     if thread != sync method
     {
        flag stop sync = NO
     }
     */
    
    return returnCode;
}

-(int)updateCategoryData:(LFCategoryModelV2*) category {
    int returnCode = 0;
    LaFCategoryAdapterV2 *adapterCategory = [[LaFCategoryAdapterV2 alloc] init];
    [adapterCategory openDatabase];
    [adapterCategory resetSqlCommand];
    returnCode = [adapterCategory updateCategoryData:category];
    [adapterCategory close];
//    [adapterCategory release];
    return returnCode;
}

-(int)deleteCategoryData:(LFCategoryModelV2*) category {
    int returnCode = 0;
    LaFCategoryAdapterV2 *adapterCategory = [[LaFCategoryAdapterV2 alloc] init];
    [adapterCategory openDatabase];
    [adapterCategory resetSqlCommand];
    returnCode = [adapterCategory deleteCategoryData:category];
    [adapterCategory close];
//    [adapterCategory release];
    return returnCode;
}

-(LFCategoryModelV2*)loadCategoryData:(LFCategoryModelV2*)categoryData {
    LaFCategoryAdapterV2 *adapterCategory = [[LaFCategoryAdapterV2 alloc] init];
    [adapterCategory openDatabase];
    [adapterCategory resetSqlCommand];
    LFCategoryModelV2 *categoryObject = [adapterCategory loadCategoryData:categoryData];
    [adapterCategory close];

    return categoryObject;
}

-(NSMutableArray *)loadAllCategoryData {
    LaFCategoryAdapterV2 *adapterCategory = [[LaFCategoryAdapterV2 alloc] init];
    [adapterCategory openDatabase];
    [adapterCategory resetSqlCommand];
    NSMutableArray *categoryDataArray = [adapterCategory loadAllCategoryData];
    [adapterCategory close];
//    [adapterCategory release];
    
    return categoryDataArray;
}

-(int)deleteAllCategoryData
{
    DataTable *tableDelete = [[DataTable alloc] initWithNameFormat:@"%@", LAF_CATIGORIES];
    int result = (int)[tableDelete excuteQueryNonSelect:[NSString stringWithFormat:@"Delete from %@", LAF_CATIGORIES]];
    if(result > 0) {
        [NSString stringWithFormat: @"REINDEX %@.%@",DATABASE_NAME, LAF_CATIGORIES];
    }
    return result;
}

#pragma mark - L&F Item into DB
-(int)insertItemData:(LFItemModelV2*) item { 
    int returnCode = 0;
    LaFItemAdapterV2 *adapterItem = [[LaFItemAdapterV2 alloc] init];
    [adapterItem openDatabase];
    [adapterItem resetSqlCommand];
    returnCode = [adapterItem insertItemData:item];
    [adapterItem close];
//    [adapterItem release];
    
    return returnCode;
}

-(int)updateItemData:(LFItemModelV2*) item {
    int returnCode = 0;
    LaFItemAdapterV2 *adapterItem = [[LaFItemAdapterV2 alloc] init];
    [adapterItem openDatabase];
    [adapterItem resetSqlCommand];
    returnCode = [adapterItem updateItemData:item];
    [adapterItem close];
//    [adapterItem release];

    return returnCode;
}

-(int)deleteItemData:(LFItemModelV2*) item {
    int returnCode = 0;
    LaFItemAdapterV2 *adapterItem = [[LaFItemAdapterV2 alloc] init];
    [adapterItem openDatabase];
    [adapterItem resetSqlCommand];
    returnCode = [adapterItem deleteItemData:item];
    [adapterItem close];
//    [adapterItem release];

    return returnCode;
}

-(LFItemModelV2*)loadItemData:(LFItemModelV2*)itemData {
    LaFItemAdapterV2 *adapterItem = [[LaFItemAdapterV2 alloc] init];
    [adapterItem openDatabase];
    [adapterItem resetSqlCommand];
    LFItemModelV2 *itemObject = [adapterItem loadItemData:itemData];
    [adapterItem close];
//    [adapterItem release];

    return itemObject;
}

-(NSMutableArray *) loadAllItemData {   
    LaFItemAdapterV2 *adapterItem = [[LaFItemAdapterV2 alloc] init];
    [adapterItem openDatabase];
    [adapterItem resetSqlCommand];
    NSMutableArray *itemDataArray = [adapterItem loadAllItemData];
    [adapterItem close];
//    [adapterItem release];

    return itemDataArray;
}

-(NSMutableArray *) loadAllItemDataByCategoryId:(int) categoryId {
    LaFItemAdapterV2 *adapterItem = [[LaFItemAdapterV2 alloc] init];
    [adapterItem openDatabase];
    [adapterItem resetSqlCommand];
    NSMutableArray *itemDataArray = [adapterItem loadAllItemDataByCategoryId:
                                     categoryId];
    [adapterItem close];
//    [adapterItem release];

    return itemDataArray;
}

-(int)deleteAllItemData
{
    DataTable *tableDelete = [[DataTable alloc] initWithNameFormat:@"%@", LAF_ITEMS];
    int result = (int)[tableDelete excuteQueryNonSelect:[NSString stringWithFormat:@"Delete from %@", LAF_ITEMS]];
    if(result > 0) {
        [NSString stringWithFormat: @"REINDEX %@.%@",DATABASE_NAME, LAF_ITEMS];
    }
    
    return result;
}

#pragma mark - L&F Image into DB
-(int)insertImageData:(LFImageModelV2*) imageData { 
    int returnCode = 0;
    LaFImagesAdapterV2 *adapterImage = [[LaFImagesAdapterV2 alloc] init];
    [adapterImage openDatabase];
    [adapterImage resetSqlCommand];
    returnCode = [adapterImage insertImageData:imageData];
    [adapterImage close];
//    [adapterImage release];

    return returnCode;    
}

-(int)updateImageData:(LFImageModelV2*) imageData {
    int returnCode = 0;
    LaFImagesAdapterV2 *adapterImage = [[LaFImagesAdapterV2 alloc] init];
    [adapterImage openDatabase];
    [adapterImage resetSqlCommand];
    returnCode = [adapterImage updateImageData:imageData];
    [adapterImage close];
//    [adapterImage release];
    
    return returnCode;
}

-(int)deleteImageData:(LFImageModelV2*) imageData {    
    int returnCode = 0;
    LaFImagesAdapterV2 *adapterImage = [[LaFImagesAdapterV2 alloc] init];
    [adapterImage openDatabase];
    [adapterImage resetSqlCommand];
    returnCode = [adapterImage deleteImageData:imageData];
    [adapterImage close];
//    [adapterImage release];
    
    return returnCode;
}

-(int)deleteAllImageDataByDetailId:(int) lfDetailId {
    int returnCode = 0;
    LaFImagesAdapterV2 *adapterImage = [[LaFImagesAdapterV2 alloc] init];
    [adapterImage openDatabase];
    [adapterImage resetSqlCommand];
    returnCode = [adapterImage deleteAllImageDataByDetailId:lfDetailId];
    [adapterImage close];
    //    [adapterImage release];
    
    return returnCode;
}

-(LFImageModelV2*)loadImageData:(LFImageModelV2*)imageData {
    LaFImagesAdapterV2 *adapterImage = [[LaFImagesAdapterV2 alloc] init];
    [adapterImage openDatabase];
    [adapterImage resetSqlCommand];
    LFImageModelV2 *imageObject = [adapterImage loadImageData:imageData];
    [adapterImage close];
//    [adapterImage release];
    
    return imageObject;
}

-(NSMutableArray *) loadAllImageDataByLFDetailId:(int) lfDetailId {
    LaFImagesAdapterV2 *adapterImage = [[LaFImagesAdapterV2 alloc] init];
    [adapterImage openDatabase];
    [adapterImage resetSqlCommand];
    NSMutableArray *imageDataArray = [adapterImage loadAllImageDataByLFDetailId:lfDetailId];
    [adapterImage close];
//    [adapterImage release];
    
    return imageDataArray;
}

#pragma mark - L&F into DB
-(int)insertLaFData:(LostandFoundModelV2*) lafData {
    int returnCode = 0;
    LostandFoundAdapterV2 *adapterLostandFound = [[LostandFoundAdapterV2 alloc] init];
    [adapterLostandFound openDatabase];
    [adapterLostandFound resetSqlCommand];
    returnCode = [adapterLostandFound insertLaFData:lafData];
    [adapterLostandFound close];
//    [adapterLostandFound release];
    
    return returnCode;
}

-(int)updateLaFData:(LostandFoundModelV2*) lafData  {
    int returnCode = 0;
    LostandFoundAdapterV2 *adapterLostandFound = [[LostandFoundAdapterV2 alloc] init];
    [adapterLostandFound openDatabase];
    [adapterLostandFound resetSqlCommand];
    returnCode = [adapterLostandFound updateLaFData:lafData];
    [adapterLostandFound close];
//    [adapterLostandFound release];
    
    return returnCode;
}

-(int)deleteLaFData:(LostandFoundModelV2*) lafData {
    int returnCode = 0;
    LostandFoundAdapterV2 *adapterLostandFound = [[LostandFoundAdapterV2 alloc] init];
    [adapterLostandFound openDatabase];
    [adapterLostandFound resetSqlCommand];
    returnCode = [adapterLostandFound deleteLaFData:lafData];
    [adapterLostandFound close];
//    [adapterLostandFound release];
    
    return returnCode;
}

-(int)deleteAllLaFData:(int)lfDetailId {
    int returnCode = 0;
    LostandFoundAdapterV2 *adapterLostandFound = [[LostandFoundAdapterV2 alloc] init];
    [adapterLostandFound openDatabase];
    [adapterLostandFound resetSqlCommand];
    returnCode = [adapterLostandFound deleteAllLaFData:lfDetailId];
    [adapterLostandFound close];
    //    [adapterLostandFound release];
    
    return returnCode;
}

-(LostandFoundModelV2*)loadLaFData:(LostandFoundModelV2*) lafData {
    LostandFoundAdapterV2 *adapterLostandFound = [[LostandFoundAdapterV2 alloc] init];
    [adapterLostandFound openDatabase];
    [adapterLostandFound resetSqlCommand];
    LostandFoundModelV2 *lafObject = [adapterLostandFound loadLaFData:lafData];
    [adapterLostandFound close];
//    [adapterLostandFound release];
    
    return lafObject;
}

-(NSMutableArray *) loadAllLaFDataByLaFDetailId:(int) lfDetailId {
    LostandFoundAdapterV2 *adapterLostandFound = [[LostandFoundAdapterV2 alloc] init];
    [adapterLostandFound openDatabase];
    [adapterLostandFound resetSqlCommand];
     NSMutableArray *lafDataArray = [adapterLostandFound loadAllLaFDataByLaFDetailId:lfDetailId];
    [adapterLostandFound close];
//    [adapterLostandFound release];
    
    return lafDataArray;
}

#pragma mark - L&F Detail into DB
-(int)insertLaFDetailData:(LostandFoundDetailModelV2*) lafDetailData { 
    int returnCode = 0;
    LostandFoundDetailAdapterV2 *adapterLostandFoundDetail = [[LostandFoundDetailAdapterV2 alloc] init];
    [adapterLostandFoundDetail openDatabase];
    [adapterLostandFoundDetail resetSqlCommand];
    returnCode = [adapterLostandFoundDetail insertLaFDetailData:lafDetailData];
    [adapterLostandFoundDetail close];
//    [adapterLostandFoundDetail release];
    
    return returnCode;
}

-(int)updateLaFDetailData:(LostandFoundDetailModelV2*) lafDetailData {
    int returnCode = 0;
    LostandFoundDetailAdapterV2 *adapterLostandFoundDetail = [[LostandFoundDetailAdapterV2 alloc] init];
    [adapterLostandFoundDetail openDatabase];
    [adapterLostandFoundDetail resetSqlCommand];
    returnCode = [adapterLostandFoundDetail updateLaFDetailData:lafDetailData];
    [adapterLostandFoundDetail close];
//    [adapterLostandFoundDetail release];
    
    return returnCode;
}

-(int)deleteLaFDetailData:(LostandFoundDetailModelV2*) lafDetailData {
    int returnCode = 0;
    LostandFoundDetailAdapterV2 *adapterLostandFoundDetail = [[LostandFoundDetailAdapterV2 alloc] init];
    [adapterLostandFoundDetail openDatabase];
    [adapterLostandFoundDetail resetSqlCommand];
    returnCode = [adapterLostandFoundDetail deleteLaFDetailData:lafDetailData];
    [adapterLostandFoundDetail close];
    
    return returnCode;
}

-(LostandFoundDetailModelV2*)loadLaFDetailData:(LostandFoundDetailModelV2*)lafDetailData {
    LostandFoundDetailAdapterV2 *adapterLostandFoundDetail = [[LostandFoundDetailAdapterV2 alloc] init];
    [adapterLostandFoundDetail openDatabase];
    [adapterLostandFoundDetail resetSqlCommand];
    LostandFoundDetailModelV2 *lafObject = [adapterLostandFoundDetail loadLaFDetailData:lafDetailData];
    [adapterLostandFoundDetail close];
//    [adapterLostandFoundDetail release];

    return lafObject;
}

-(LostandFoundDetailModelV2*)loadLaFDetailDataBy_UserIdand_RoomId:(LostandFoundDetailModelV2*)lafDetailData {
    LostandFoundDetailAdapterV2 *adapterLostandFoundDetail = [[LostandFoundDetailAdapterV2 alloc] init];
    [adapterLostandFoundDetail openDatabase];
    [adapterLostandFoundDetail resetSqlCommand];
    LostandFoundDetailModelV2  *lafObject = [adapterLostandFoundDetail loadLaFDetailDataBy_UserIdand_RoomId:lafDetailData];
    [adapterLostandFoundDetail close];
//    [adapterLostandFoundDetail release];
    
    return lafObject;
}

-(NSMutableArray *)loadAllLaFDetailByUserId:(NSInteger)userId {
    LostandFoundDetailAdapterV2 *adapter = [[LostandFoundDetailAdapterV2 alloc] init];
    [adapter openDatabase];
    NSMutableArray *results = [adapter loadAllLostAndFoundDetailByUserId:userId];
    [adapter close];
    
    return results;
}

#pragma mark - L&F Color into DB
-(int)insertColorData:(LFColorModelV2*) colorData  {
    int returnCode = 0;
    LaFColorAdapterV2 *adapterLaFColor = [[LaFColorAdapterV2 alloc] init];
    [adapterLaFColor openDatabase];
    [adapterLaFColor resetSqlCommand];
    returnCode = [adapterLaFColor insertColorData:colorData];
    [adapterLaFColor close];
//    [adapterLaFColor release];

    return returnCode;
}

-(int)updateColorData:(LFColorModelV2*) colorData {
    int returnCode = 0;
     LaFColorAdapterV2 *adapterLaFColor = [[LaFColorAdapterV2 alloc] init];
    [adapterLaFColor openDatabase];
    [adapterLaFColor resetSqlCommand];
    returnCode = [adapterLaFColor updateColorData:colorData];
    [adapterLaFColor close];
//    [adapterLaFColor release];
    
    return returnCode;
}

-(int)deleteColorData:(LFColorModelV2*) colorData {
    int returnCode = 0;
    LaFColorAdapterV2 *adapterLaFColor = [[LaFColorAdapterV2 alloc] init];
    [adapterLaFColor openDatabase];
    [adapterLaFColor resetSqlCommand];
    returnCode = [adapterLaFColor deleteColorData:colorData];
    [adapterLaFColor close];
//    [adapterLaFColor release];

    return returnCode;
}

-(LFColorModelV2*)loadColorData:(LFColorModelV2*)colorData {
    LaFColorAdapterV2 *adapterLaFColor = [[LaFColorAdapterV2 alloc] init];
    [adapterLaFColor openDatabase];
    [adapterLaFColor resetSqlCommand];
    LFColorModelV2  *lafColorObject = [adapterLaFColor loadColorData:colorData];
    [adapterLaFColor close];
//    [adapterLaFColor release];

    return lafColorObject;
}

-(NSMutableArray *)loadAllColors {    
    LaFColorAdapterV2 *adapterLaFColor = [[LaFColorAdapterV2 alloc] init];
    [adapterLaFColor openDatabase];
    [adapterLaFColor resetSqlCommand];
    NSMutableArray *lafColorArray = [adapterLaFColor loadAllColors];
    [adapterLaFColor close];
//    [adapterLaFColor release];
    
    return lafColorArray;
}

#pragma mark - Get L&F Category Data from WS
-(void)getLaFCategoryFromWS:(NSNumber *)userId
            andLastModifier:(NSString *)lastModifier andHotelID:(NSString *)hotelId{
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    eHousekeepingService_GetLFCategoryList *request = [[eHousekeepingService_GetLFCategoryList alloc] init];
    
    if (userId) {
        [request setIntUsrID:userId];
    }
    
//    if (lastModifier) {
//        [request setStrLastModified:lastModifier];
//    }
    if (hotelId) {
        [request setStrHotel_ID:hotelId];
    }
    
//    LogObject *logObj = [[LogObject alloc] init];
//    logObj.dateTime = [NSDate date];
//    logObj.log = [NSString stringWithFormat:@"<%i><getLaFCategoryFromWS><intUsrID:%i lastModified:%@>",userId,userId,lastModifier];
//    LogFileManager *logManager = [[LogFileManager alloc] init];
//    [logManager save:logObj];
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetLFCategoryListUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    for (id bodyPart in responseBodyParts ) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetLFCategoryListResponse class]]) {
            eHousekeepingService_GetLFCategoryListResponse *dataBody=bodyPart;
            if (dataBody != nil) {
                if ([dataBody.GetLFCategoryListResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]]) {
                    [self deleteAllCategoryData];
                    
                    NSMutableArray *arrayLFCat = dataBody.GetLFCategoryListResult.LostAndFoundCategory.LostAndFoundCategory;
                    for (eHousekeepingService_LostAndFoundCategory *lfCatObject in arrayLFCat) {
                        LFCategoryModelV2 *modelObject = [[LFCategoryModelV2 alloc] init];
                        modelObject.lfcID = [lfCatObject.lfcID integerValue];
                        modelObject.lfcName = lfCatObject.lfcName;
                        modelObject.lfcLang = lfCatObject.lfcLang;
                        modelObject.lfcImage = lfCatObject.lfcPicture;
                        modelObject.lfcLastModified = lfCatObject.lfcLastModified;
                        
                        int isSuccess = [self insertCategoryData:modelObject];
                        
                        if(isSuccess == 0)
                            [self updateCategoryData:modelObject];
                    }                   
                }
            }            
        }
    }
}

#pragma mark - Get L&F Item Data from WS
-(void)getLaFItemFromWS:(NSNumber *)userId andLastModifier: (NSString*)lastModifier andHotelID:(NSString *)hotelId{
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    if ([LogFileManager isLogConsole]) {
        [binding setLogXMLInOut:YES];
    }
    eHousekeepingService_GetLFItemList *request = [[eHousekeepingService_GetLFItemList alloc] init];
    
    if (userId) {
        [request setIntUsrID:userId];
    }
    
//    if (lastModifier) {
//        [request setStrLastModified:lastModifier];
//    }
    if (hotelId) {
        [request setStrHotel_ID:hotelId];
    }
//    LogObject *logObj = [[LogObject alloc] init];
//    logObj.dateTime = [NSDate date];
//    logObj.log = [NSString stringWithFormat:@"<%i><getLaFItemFromWS><intUsrID:%i lastModified:%@>",userId.intValue,userId.intValue,lastModifier];
//    LogFileManager *logManager = [[LogFileManager alloc] init];
//    [logManager save:logObj];
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetLFItemListUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    for (id bodyPart in responseBodyParts ) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetLFItemListResponse class]]) {
            eHousekeepingService_GetLFItemListResponse *dataBody = bodyPart;
            if (dataBody != nil) {
                if ([dataBody.GetLFItemListResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]]) {
                    [self deleteAllItemData];
                    NSMutableArray *arrayLFItem = dataBody.GetLFItemListResult.LostAndFoundItemTypeList.LostAndFoundItemTypeList;
                    
                    for (eHousekeepingService_LostAndFoundItemTypeList *lfItemObject in arrayLFItem) {
                        LFItemModelV2 *modelObject = [[LFItemModelV2 alloc] init];
                        modelObject.lfitID = [lfItemObject.lfitID integerValue];
                        modelObject.lfitName = lfItemObject.lfitName;
                        modelObject.lfitLang = lfItemObject.lfitLang;
                        modelObject.lafi_category_id = [lfItemObject.lfitCategoryID integerValue];
                        modelObject.lfitLastModified = lfItemObject.lfcLastModified;
                        modelObject.lfitImage = lfItemObject.lfcPicture;
                        
                        int isSuccess = [self insertItemData:modelObject];
                        
                        if(isSuccess == 0)
                            [self updateItemData:modelObject];
                    }
                }
            }            
        }
    }
}

#pragma mark - Get L&F Color Data from WS
-(void)getLaFColorFromWS:(NSNumber *)userId andLastModifier:(NSString*)lastModifier {
    
    //Hao Tran - Hard code color (like Android version)
    NSString *todayDateString = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
    LFColorModelV2 *modelObject = [[LFColorModelV2 alloc] init];
    
    //RED
    modelObject.lfcID = 1;
    modelObject.htmlCode = @"#FF0000";
    modelObject.lfc_lang = @"RED";
    modelObject.lfc_Name = @"RED";
    modelObject.lfc_LastModifier =  todayDateString;
    [self insertColorData:modelObject];
    
    //BLACK
    modelObject.lfcID = 2;
    modelObject.htmlCode = @"#000000";
    modelObject.lfc_lang = @"BLACK";
    modelObject.lfc_Name = @"BLACK";
    modelObject.lfc_LastModifier =  todayDateString;
    [self insertColorData:modelObject];
    
    //BLUE
    modelObject.lfcID = 3;
    modelObject.htmlCode = @"#0000FF";
    modelObject.lfc_lang = @"BLUE";
    modelObject.lfc_Name = @"BLUE";
    modelObject.lfc_LastModifier =  todayDateString;
    [self insertColorData:modelObject];
    
    //GREY
    modelObject.lfcID = 4;
    modelObject.htmlCode = @"#808080";
    modelObject.lfc_lang = @"GREY";
    modelObject.lfc_Name = @"GREY";
    modelObject.lfc_LastModifier =  todayDateString;
    [self insertColorData:modelObject];
    
    //LIME GREEN
    modelObject.lfcID = 5;
    modelObject.htmlCode = @"#41A317";
    modelObject.lfc_lang = @"LIME GREEN";
    modelObject.lfc_Name = @"LIME GREEN";
    modelObject.lfc_LastModifier =  todayDateString;
    [self insertColorData:modelObject];
    
    //ORANGE
    modelObject.lfcID = 6;
    modelObject.htmlCode = @"#FFA500";
    modelObject.lfc_lang = @"ORANGE";
    modelObject.lfc_Name = @"ORANGE";
    modelObject.lfc_LastModifier =  todayDateString;
    [self insertColorData:modelObject];
    
    //PINK
    modelObject.lfcID = 7;
    modelObject.htmlCode = @"#FFAFBE";
    modelObject.lfc_lang = @"PINK";
    modelObject.lfc_Name = @"PINK";
    modelObject.lfc_LastModifier =  todayDateString;
    [self insertColorData:modelObject];
    
    //PURPLE
    modelObject.lfcID = 8;
    modelObject.htmlCode = @"#8E35EF";
    modelObject.lfc_lang = @"PURPLE";
    modelObject.lfc_Name = @"PURPLE";
    modelObject.lfc_LastModifier =  todayDateString;
    [self insertColorData:modelObject];
    
    //ROYAL BLUE
    modelObject.lfcID = 9;
    modelObject.htmlCode = @"#2B60DE";
    modelObject.lfc_lang = @"ROYAL BLUE";
    modelObject.lfc_Name = @"ROYAL BLUE";
    modelObject.lfc_LastModifier =  todayDateString;
    [self insertColorData:modelObject];
    
    //SPRING GREEN
    modelObject.lfcID = 10;
    modelObject.htmlCode = @"#4AA02C";
    modelObject.lfc_lang = @"SPRING GREEN";
    modelObject.lfc_Name = @"SPRING GREEN";
    modelObject.lfc_LastModifier =  todayDateString;
    [self insertColorData:modelObject];
    
    //YELLOW
    modelObject.lfcID = 11;
    modelObject.htmlCode = @"#FFFF00";
    modelObject.lfc_lang = @"YELLOW";
    modelObject.lfc_Name = @"YELLOW";
    modelObject.lfc_LastModifier =  todayDateString;
    [self insertColorData:modelObject];
    
    //WHITE
    modelObject.lfcID = 12;
    modelObject.htmlCode = @"#FFFFFF";
    modelObject.lfc_lang = @"WHITE";
    modelObject.lfc_Name = @"WHITE";
    modelObject.lfc_LastModifier =  todayDateString;
    [self insertColorData:modelObject];
    
    //Hao Tran - Remove for hardcode color (like Android version)
    //For remove hardcode, uncomment below code and comment hardcode above
    /*
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    eHousekeepingService_GetLFColorList *request = [[eHousekeepingService_GetLFColorList alloc] init];
    
    if([LogFileManager isLogConsole]){
        [binding setLogXMLInOut:YES];
    }
    
    if (userId) {
        [request setIntUsrID:userId];
    }
    
    if (lastModifier.length > 0) {
        [request setStrLastModified:lastModifier];
    }
    
//    LogObject *logObj = [[LogObject alloc] init];
//    logObj.dateTime = [NSDate date];
//    logObj.log = [NSString stringWithFormat:@"<%i><getLaFColorFromWS><intUsrID:%i lastModified:%@>",userId.intValue,userId.intValue,lastModifier];
//    LogFileManager *logManager = [[LogFileManager alloc] init];
//    [logManager save:logObj];
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetLFColorListUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    for (id bodyPart in responseBodyParts ) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetLFColorListResponse class]]) {
            eHousekeepingService_GetLFColorListResponse *dataBody = bodyPart;
            if (dataBody != nil) {
                if ([dataBody.GetLFColorListResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]]) {
                    
                    NSMutableArray *arrayLFColor = dataBody.GetLFColorListResult.LostNFoundColorList.LostAndFoundColorList;
                    
                    for (eHousekeepingService_LostAndFoundColorList *lfColorObject in arrayLFColor) {
                        LFColorModelV2 *modelObject = [[LFColorModelV2 alloc] init];
                        modelObject.lfcID = [lfColorObject.lfcID integerValue];
                        modelObject.htmlCode = lfColorObject.lfcHTMLColorCode;
                        modelObject.lfc_lang = lfColorObject.lfcLang;
                        modelObject.lfc_Name = lfColorObject.lfcName;
                        modelObject.lfc_LastModifier = lfColorObject.lfcLastModified;
                        
                        LFColorModelV2 *compareOject = [[LFColorModelV2 alloc] init];
                        compareOject.lfcID = modelObject.lfcID;
                        int result = [self insertColorData:modelObject];
                        if (result == 0) {
                            [self loadColorData:compareOject];
                            if ([compareOject.lfc_LastModifier compare:modelObject.lfc_LastModifier] == NSOrderedAscending) {
                                
                                [self updateColorData:modelObject];
                            }
                        }                        
                    }
                }
            }            
        }
    }
     */
}

#pragma mark- Post L&F Data from WS
-(int)postLostandFound:(LostandFoundDetailModelV2*)lfDetail {
    int responseCode = 0;
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole])
    {
        [binding setLogXMLInOut:YES];
    }
    eHousekeepingService_PostLostAndFoundRoutine *request = [[eHousekeepingService_PostLostAndFoundRoutine alloc] init];
    eHousekeepingService_ArrayOfLostAndFoundItemDetails *lfListing = [[eHousekeepingService_ArrayOfLostAndFoundItemDetails alloc] init];
    
    NSMutableArray *listHistoryPosting = [NSMutableArray array];
    // DungPhan - 20150908: Enable post Posting History for Lost and Found by Access Rights
    BOOL isEnablePostingHistory = ([[UserManagerV2 sharedUserManager].currentUserAccessRight isFindActionHistoryActive] || [[UserManagerV2 sharedUserManager].currentUserAccessRight isFindActionHistoryAllowedView] || [[UserManagerV2 sharedUserManager].currentUserAccessRight isRoomDetailsActionHistoryActive] || [[UserManagerV2 sharedUserManager].currentUserAccessRight isRoomDetailsActionHistoryAllowedView]);
    BOOL isEnglishLanguage = [[LanguageManagerV2 sharedLanguageManager] currentLanguageIsEqualTo:@"%@", ENGLISH_LANGUAGE];
    
    for (int i = 0; i < [lfDetail.lafArray count]; i++) {
        
        LostandFoundModelV2 *lafModel = [lfDetail.lafArray objectAtIndex:i];
        eHousekeepingService_LostAndFoundItemDetails *curDetailRequest = [[eHousekeepingService_LostAndFoundItemDetails alloc] init];
        curDetailRequest.ImageAttachmentListing = [[eHousekeepingService_ArrayOfImageAttachmentDetails alloc] init];
        
        if ([lfDetail.lafPhoto count] > 0) {
            
            for (int j = 0; j < [lfDetail.lafPhoto count]; j++) {
                eHousekeepingService_ImageAttachmentDetails *imageDetail = [[eHousekeepingService_ImageAttachmentDetails alloc] init];
                LFImageModelV2 *imageModel = lfDetail.lafPhoto[j];
                [imageDetail setContent:[imageModel.lafImgImage base64Encoding]];
                [curDetailRequest.ImageAttachmentListing.ImageAttachmentDetails addObject:imageDetail];
            }
        } else { //Fixed for WS response 0 if don't have image
            eHousekeepingService_ImageAttachmentDetails *imageDetail = [[eHousekeepingService_ImageAttachmentDetails alloc] init];
            [curDetailRequest.ImageAttachmentListing.ImageAttachmentDetails addObject:imageDetail];
        }
        
        if (lafModel) {
            [curDetailRequest setCategoryID:[NSString stringWithFormat:@"%d", (int)lafModel.lafCategoryId]];
            [curDetailRequest setItemID:[NSString stringWithFormat:@"%d", (int)lafModel.lafItemId]];
            [curDetailRequest setColorID:[NSString stringWithFormat:@"%d", (int)lafModel.lafColorId]];
            [curDetailRequest setQuantity:[NSString stringWithFormat:@"%d",(int)lafModel.laf_item_quantity]];
        }
        
        if (lfDetail) {
            [curDetailRequest setRemarks:lfDetail.lafDetailRemark];
            [curDetailRequest setDutyAssignmentID:[NSString stringWithFormat:@"%d",(int)lfDetail.lafRoomAssignmentId]];
            [curDetailRequest setUserID:[NSString stringWithFormat:@"%d", (int)lfDetail.lafDetailUserId]];
            [curDetailRequest setFoundDateTime:lfDetail.lafDetailDate];
            [curDetailRequest setHotelID:[NSString stringWithFormat:@"%d",[UserManagerV2 sharedUserManager].currentUser.userHotelsId]];
            [curDetailRequest setRoomNo:lfDetail.lafDetailRoomId];
        }
        
        [lfListing.LostAndFoundItemDetails addObject:curDetailRequest];
        
        //if (isEnablePostingHistory) {
            
            //History Posting
            eHousekeepingService_PostingHistoryDetails *historyDetail = [[eHousekeepingService_PostingHistoryDetails alloc] init];
            [historyDetail setUserID:[NSString stringWithFormat:@"%d", [UserManagerV2 sharedUserManager].currentUser.userId]];
            [historyDetail setRoomNo:lfDetail.lafDetailRoomId];
            [historyDetail setHotelID:[NSString stringWithFormat:@"%d", [UserManagerV2 sharedUserManager].currentUser.userHotelsId]];
            [historyDetail setModuleID:[NSString stringWithFormat:@"%d", (int)ModuleHistory_LostAndFound]];
            [historyDetail setNewQuantity:[NSString stringWithFormat:@"%d", (int)lafModel.laf_item_quantity]];
            [historyDetail setUsedQuantity:[NSString stringWithFormat:@"%d", (int)lafModel.laf_item_quantity]];
            [historyDetail setTransactionDateTime:lfDetail.lafDetailDate];
            
            LFItemModelV2 *countItem = [[LFItemModelV2 alloc] init];
            countItem.lfitID = lafModel.lafItemId;
            countItem = [self loadItemData:countItem];
            
            LFCategoryModelV2 *countCategory = [[LFCategoryModelV2 alloc] init];
            countCategory.lfcID = lafModel.lafCategoryId;
            countCategory = [self loadCategoryData:countCategory];
            
            if (isEnglishLanguage) {
                [historyDetail setItemDescription:countItem.lfitName];
                [historyDetail setCategoryDescription:countCategory.lfcName];
            } else {
                [historyDetail setItemDescription:countItem.lfitLang];
                [historyDetail setCategoryDescription:countCategory.lfcLang];
            }
            
            [listHistoryPosting addObject:historyDetail];
        //}
        
    }
    
    request.LostAndFoundItemsListing = lfListing;
    eHousekeepingServiceSoap12BindingResponse *response = [binding PostLostAndFoundRoutineUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    for (id bodyPart in responseBodyParts ) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_PostLostAndFoundRoutineResponse class]]) {
            eHousekeepingService_PostLostAndFoundRoutineResponse *dataBody = bodyPart;
            
            if (dataBody != nil) {
                
                if ([dataBody.PostLostAndFoundRoutineResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_NEW_RECORD_ADDED]]
                    || [dataBody.PostLostAndFoundRoutineResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]]) {
                    
                    responseCode = RESPONSE_STATUS_OK;
                    
                    //CRF-1229: Posting history
                    //if (isEnablePostingHistory) {
                        CountManagerV2 *countManager = [[CountManagerV2 alloc] init];
                        [countManager postHistoryPostingWS:listHistoryPosting];
                    //}
                    
                    //delete lost and found data after post successful
                    [[LostandFoundManagerV2 sharedLostandFoundManagerV2] deleteAllLaFData:(int)lfDetail.lafDetailId];
                    
                    //load all laf of laf detail
                    NSMutableArray *lafModels = [[LostandFoundManagerV2 sharedLostandFoundManagerV2] loadAllLaFDataByLaFDetailId:(int)lfDetail.lafDetailId];
                    
                    //load all laf images of laf detail
                    NSMutableArray *lafImages = [[LostandFoundManagerV2 sharedLostandFoundManagerV2] loadAllImageDataByLFDetailId:(int)lfDetail.lafDetailId];
                    
                    if (lafModels.count == 0 && lafImages.count == 0) {
                        [[LostandFoundManagerV2 sharedLostandFoundManagerV2] deleteLaFDetailData:lfDetail];
                    }
                }
            }
        }
    }

    return responseCode;
}

#pragma mark - Post L&F Photo from WS
-(BOOL)postLaFPhoto:(LostandFoundDetailModelV2 *)lafDetail andPhoto :(LFImageModelV2 *)lfPhoto {   
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    eHousekeepingService_PostLFAttachPhoto *request = [[eHousekeepingService_PostLFAttachPhoto alloc] init];
    
    //Allow write log or not
    if([LogFileManager isLogConsole])
    {
        [binding setLogXMLInOut:YES];
    }
    if (lafDetail) {        
        [request setIntRoomAssignID:[NSNumber numberWithInt:
                                     [lafDetail.lafDetailRoomId intValue]]];
        [request setIntUsrID:[NSNumber numberWithInteger:lafDetail.lafDetailUserId]];
    }
    
    if (lfPhoto.lafImgImage) {
        [request setBinPhoto:lfPhoto.lafImgImage];
    }
    
    LogObject *logObj = [[LogObject alloc] init];
    logObj.dateTime = [NSDate date];
    logObj.log = [NSString stringWithFormat:@"<%i><postLaFPhoto><intRoomAssignID:%@ intUsrID:%i binPhoto:photo>",(int)lafDetail.lafDetailUserId,lafDetail.lafDetailRoomId,(int)lafDetail.lafDetailUserId];
    LogFileManager *logManager = [[LogFileManager alloc] init];
    [logManager save:logObj];
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding PostLFAttachPhotoUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    
    for (id bodyPart in responseBodyParts ) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_PostLFAttachPhotoResponse class]]) {
            eHousekeepingService_PostLFAttachPhotoResponse *dataBody = bodyPart;
            if (dataBody != nil) {
                if ([dataBody.PostLFAttachPhotoResult.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_NEW_RECORD_ADDED]]) {
                    
                    //delete an image after post successful
                    [[LostandFoundManagerV2 sharedLostandFoundManagerV2] deleteImageData:lfPhoto];
                    
                    return YES;
                }
            }            
        }
    }
    
    return NO;
}

#pragma mark - === Calculate Must Post L&F ===
#pragma mark
-(NSInteger) numberOfMustPostLaFRecords {
    NSInteger numberOfMustPostLaFRecords = 0;
    
    //lost and found detail records
    LostandFoundDetailAdapterV2 *lafDetailAdapter = [[LostandFoundDetailAdapterV2 alloc] init];
    [lafDetailAdapter openDatabase];
    numberOfMustPostLaFRecords += [lafDetailAdapter numberOfLaFDetailMustPostRecords];
    [lafDetailAdapter close];
    
    //load all laf details
    NSMutableArray *lafDetails = [self loadAllLaFDetailByUserId:[[[UserManagerV2 sharedUserManager] currentUser] userId]];
    
    for (LostandFoundDetailModelV2 *lafDetail in lafDetails) {
        //lost and found records
        LostandFoundAdapterV2 *lafAdapter = [[LostandFoundAdapterV2 alloc] init];
        [lafAdapter openDatabase];
        numberOfMustPostLaFRecords += [lafAdapter numberOfLaFMustPostRecordsByLaFDetailId:lafDetail.lafDetailId];
        [lafAdapter close];
        
        //lost and found image records
        LaFImagesAdapterV2 *lafImagesAdapter = [[LaFImagesAdapterV2 alloc] init];
        [lafImagesAdapter openDatabase];
        numberOfMustPostLaFRecords += [lafImagesAdapter numberOfLaFImagesMustPostRecordsByLaFDetailId:lafDetail.lafDetailId];
        [lafImagesAdapter close];
    }
    
    return numberOfMustPostLaFRecords;
}

#pragma mark - Image Scale

- (UIImage *)imageByCroppingImage:(UIImage *)image toSize:(CGSize)size
{
    double x = (image.size.width - size.width) / 2.0;
    double y = (image.size.height - size.height) / 2.0;
    
    CGRect cropRect = CGRectMake(x, y, size.height, size.width);
    CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], cropRect);
    
    UIImage *cropped = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    
    return cropped;
}

- (UIImage*)resizeImage:(UIImage*)image maxSize:(CGFloat)maxSize{
    
    CGFloat oldWidth = image.size.width;
    CGFloat oldHeight = image.size.height;
    CGFloat newHeight, newWidth;
    
    if (oldHeight > oldWidth) {
        if (oldHeight > maxSize) {
            newHeight = maxSize;
            newWidth = newHeight * oldWidth / oldHeight;
        } else {
            return image;
        }
    } else {
        if (oldWidth > maxSize) {
            newWidth = maxSize;
            newHeight = newWidth * oldHeight / oldWidth;
        } else {
            return image;
        }
    }
    
    CGSize newSize = CGSizeMake(newWidth, newHeight);
    
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

#pragma mark - Post WSLog
- (BOOL) postWSLog:(NSInteger)userId Message:(NSString *)messageValue MessageType:(NSInteger)type{
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole])
    {
        [binding setLogXMLInOut:YES];
    }
    eHousekeepingService_PostWSLog *request = [[eHousekeepingService_PostWSLog alloc] init];
    request.intUsrID = [NSNumber numberWithInteger:userId];
    request.strMessage = messageValue;
    request.intMessageType = [NSNumber numberWithInteger:type];
    eHousekeepingServiceSoap12BindingResponse *respone = [binding PostWSLogUsingParameters:request];
    BOOL isPostSuccess = NO;
    for (id bodyPart in respone.bodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_PostWSLogResponse class]]) {
            eHousekeepingService_PostWSLogResponse *dataBody = (eHousekeepingService_PostWSLogResponse *)bodyPart;
            if (dataBody != nil) {
                if ([dataBody.PostWSLogResult.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]]) {
                    isPostSuccess = YES;
                }
            }
        }
    }
    return isPostSuccess;
}

@end
