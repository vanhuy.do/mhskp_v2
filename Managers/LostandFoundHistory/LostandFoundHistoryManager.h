//
//  LostandFoundHistoryManager.h
//  mHouseKeeping
//
//  Created by Quang Nguyen on 4/8/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LostandFoundHistoryModel.h"
#import "LostandFoundHistoryAdapter.h"

@interface LostandFoundHistoryManager : NSObject
-(int) insertLaFHistoryData:(LostandFoundHistoryModel *)lafhModel;
-(NSMutableArray *) loadRoomIdByUserId:(NSInteger)userId;
-(NSMutableArray *) loadAllLaFHistoryByUserId:(NSInteger)userId AndRoomId:(NSString*)roomNumber;
-(int) deleteDatabaseAfterSomeDays;
@end
