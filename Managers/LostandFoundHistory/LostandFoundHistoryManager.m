//
//  LostandFoundHistoryManager.m
//  mHouseKeeping
//
//  Created by Quang Nguyen on 4/8/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "LostandFoundHistoryManager.h"
#import "LostandFoundHistoryAdapter.h"

@implementation LostandFoundHistoryManager
-(int) insertLaFHistoryData:(LostandFoundHistoryModel *)lafhModel{
    NSInteger returnCode = 0;
    LostandFoundHistoryAdapter *adapter = [[LostandFoundHistoryAdapter alloc] init];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    returnCode = [adapter insertLaFHistoryData:lafhModel];
    [adapter close];
    adapter = nil;
    return (int)returnCode;
}

-(NSMutableArray *) loadRoomIdByUserId:(NSInteger)userId{
    LostandFoundHistoryAdapter *adapter = [[LostandFoundHistoryAdapter alloc] init];
    [adapter openDatabase];
    NSMutableArray *array = [adapter loadRoomIdByUserId:userId];
    [adapter close];
    return array;
}

-(NSMutableArray *) loadAllLaFHistoryByUserId:(NSInteger)userId AndRoomId:(NSString*)roomNumber{
    LostandFoundHistoryAdapter *adapter = [[LostandFoundHistoryAdapter alloc] init];
    [adapter openDatabase];
    NSMutableArray *array = [adapter loadAllLaFHistoryByUserId:userId AndRoomId:roomNumber];
    return array;

}
-(int) deleteDatabaseAfterSomeDays{
    NSInteger returnCode = 0;
    LostandFoundHistoryAdapter *adapter = [[LostandFoundHistoryAdapter alloc] init];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    returnCode = [adapter deleteDatabaseAfterSomeDays];
    [adapter close];
    adapter = nil;
    return (int)returnCode;
}
@end
