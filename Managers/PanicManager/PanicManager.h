//
//  PanicManager.h
//  mHouseKeeping
//
//  Created by vinhnguyen on 12/16/13.
//
//

#import <Foundation/Foundation.h>
#import <MessageUI/MessageUI.h>
#import <AudioToolbox/AudioToolbox.h>
#import "PanicReceiver.h"

//Change here for change alert sound file
#define AlertFileNameSound @"alert"
#define AlertFileSoundExtension @"mp3"

@interface PanicManager : NSObject <MFMessageComposeViewControllerDelegate>
{
    AVAudioPlayer *audioPlayer;
}

//MARK: Static Methods
+ (PanicManager*) sharedPanicManager;
+ (BOOL) isRunningPanic;

#pragma mark - WS FUNCTIONS
//Parametters isOnlyConfig for only get config for panic button
-(int)getPanicConfigByUserId:(int)userId hotelId:(int)hotelId lastModified:(NSString*)lastModified;

#pragma mark - DATABASE FUNCTIONS
-(int) insertPanicReceiver:(PanicReceiver*) model;
-(int) updatePanicReceivers:(PanicReceiver*) model;
-(int) deletePanicReceiversByOwnerId:(int)ownerId receiverId:(int)receiverId;
-(int) deletePanicReceiversByOwnerId:(int)ownerId;
-(NSMutableArray*) loadPanicReceiversByOwnerId:(int)ownerId;
-(PanicReceiver*) loadPanicReceiverByOwnerId:(int)ownerId receiverId:(int)receiverId;

#pragma mark - Panic Notificatin Process
-(void) playAlarmSound;
-(void) stopAlarmSound;
//MARK: These processes will interact wiht WS and Local Device
-(void) panicNowWithTabbarController:(UITabBarController*)tabbarController;
-(void) stopPanic;

@end
