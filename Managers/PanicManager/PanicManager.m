//
//  PanicManager.m
//  mHouseKeeping
//
//  Created by vinhnguyen on 12/16/13.
//
//

#import "PanicManager.h"
#import "DataBaseExtension.h"
#import "DbDefinesV2.h"
#import "LogFileManager.h"
#import "NetworkCheck.h"
#import "AdHocMessageManagerV2.h"
#import "LanguageManagerV2.h"
#import "iToast.h"
#import "RoomManagerV2.h"
#import "ehkDefinesV2.h"
#import "CustomAlertViewV2.h"

@implementation PanicManager

static bool isRunningPanic = NO;
static PanicManager* sharedPanicManagerInstance = nil;

//MARK: Static Methods
+ (PanicManager*) sharedPanicManager
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedPanicManagerInstance = [[PanicManager alloc] init];
    });
    
    return sharedPanicManagerInstance;
}

+ (BOOL) isRunningPanic
{
    return isRunningPanic;
}

#pragma mark - WS FUNCTIONS

//Parametters isOnlyConfig for only get config for panic button
-(int)getPanicConfigByUserId:(int)userId hotelId:(int)hotelId lastModified:(NSString*)lastModified
{
    //No network detected
    if(![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]){
        return RESPONSE_STATUS_NO_NETWORK;
    }
    
    if(userId <= 0){
        return RESPONSE_STATUS_ERROR;
    }
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole]){
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_GetPanicAlertConfig *request = [[eHousekeepingService_GetPanicAlertConfig alloc] init];
    
    [request setIntUsrID:[NSNumber numberWithInt:userId]];
    [request setIntHotelID:[NSNumber numberWithInt:hotelId]];
    
    if(lastModified.length > 0) {
        [request setStrLastModified:lastModified];
    }
    
    int result = RESPONSE_STATUS_ERROR;
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetPanicAlertConfigUsingParameters:request];
    NSArray *responseBodyParts = response.bodyParts;
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetPanicAlertConfigResponse class]]) {
            eHousekeepingService_GetPanicAlertConfigResponse *dataBody = (eHousekeepingService_GetPanicAlertConfigResponse *)bodyPart;
            if (dataBody != nil) {
                if ([dataBody.GetPanicAlertConfigResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_OK]]) {
                    
                    result = RESPONSE_STATUS_OK;
                    
                    if(dataBody.GetPanicAlertConfigResult.isAdhocMsg) {
                        [[UserManagerV2 sharedUserManager] setCurrentUserConfig:USER_CONFIG_PANIC_IS_ADHOC_MESSAGE value:[dataBody.GetPanicAlertConfigResult.isAdhocMsg stringValue]];
                    }
                    
                    if(dataBody.GetPanicAlertConfigResult.isAlarmSound) {
                        [[UserManagerV2 sharedUserManager] setCurrentUserConfig:USER_CONFIG_PANIC_IS_ALARM_SOUND value:[dataBody.GetPanicAlertConfigResult.isAlarmSound stringValue]];
                    }
                    
                    if(dataBody.GetPanicAlertConfigResult.isDirectCall) {
                        [[UserManagerV2 sharedUserManager] setCurrentUserConfig:USER_CONFIG_PANIC_IS_DIRECT_CALL value:[dataBody.GetPanicAlertConfigResult.isDirectCall stringValue]];
                    }
                    
                    if(dataBody.GetPanicAlertConfigResult.isEConnectJob) {
                        [[UserManagerV2 sharedUserManager] setCurrentUserConfig:USER_CONFIG_PANIC_IS_ECONNECT_JOB value:[dataBody.GetPanicAlertConfigResult.isEConnectJob stringValue]];
                    }
                    
                    if(dataBody.GetPanicAlertConfigResult.isEmail) {
                        [[UserManagerV2 sharedUserManager] setCurrentUserConfig:USER_CONFIG_PANIC_IS_EMAIL value:[dataBody.GetPanicAlertConfigResult.isEmail stringValue]];
                    }
                    
                    if(dataBody.GetPanicAlertConfigResult.isSMS) {
                        [[UserManagerV2 sharedUserManager] setCurrentUserConfig:USER_CONFIG_PANIC_IS_SMS value:[dataBody.GetPanicAlertConfigResult.isSMS stringValue]];
                    }
                    
                    if(dataBody.GetPanicAlertConfigResult.msgContent) {
                        [[UserManagerV2 sharedUserManager] setCurrentUserConfig:USER_CONFIG_PANIC_MESSAGE_CONTENT value:dataBody.GetPanicAlertConfigResult.msgContent];
                    }
                    
                    if(dataBody.GetPanicAlertConfigResult.msgSubject) {
                        [[UserManagerV2 sharedUserManager] setCurrentUserConfig:USER_CONFIG_PANIC_MESSAGE_SUBJECT value:dataBody.GetPanicAlertConfigResult.msgSubject];
                    }
                    
                    if(dataBody.GetPanicAlertConfigResult.raPhone) {
                        [[UserManagerV2 sharedUserManager] setCurrentUserConfig:USER_CONFIG_PANIC_RA_PHONE value:dataBody.GetPanicAlertConfigResult.raPhone];
                    }
                    
                    if(dataBody.GetPanicAlertConfigResult.supName) {
                        [[UserManagerV2 sharedUserManager] setCurrentUserConfig:USER_CONFIG_PANIC_SUP_NAME value:dataBody.GetPanicAlertConfigResult.supName];
                    }
                    if(dataBody.GetPanicAlertConfigResult.supStaffID) {
                        [[UserManagerV2 sharedUserManager] setCurrentUserConfig:USER_CONFIG_PANIC_SUP_STAFF_ID value:dataBody.GetPanicAlertConfigResult.supStaffID];
                    }
                    
                    //Delete old data
                    [self deletePanicReceiversByOwnerId:userId];
                    
                    eHousekeepingService_ArrayOfReceiver *wsReceivers = dataBody.GetPanicAlertConfigResult.receivers;
                    for (eHousekeepingService_Receiver *curReceiver in wsReceivers.Receiver) {
                        //Save receivers of current panic config
                        PanicReceiver *panicReceiver = [[PanicReceiver alloc] init];
                        panicReceiver.panic_receivers_email = curReceiver.email;
                        panicReceiver.panic_receivers_owner_id = userId;
                        panicReceiver.panic_receivers_phone_number = curReceiver.mobile;
                        panicReceiver.panic_receivers_receiver_id = [curReceiver.staffID intValue];
                        panicReceiver.panic_receivers_receiver_name = curReceiver.staffName;
                        panicReceiver.panic_receivers_is_direct_call = [curReceiver.isDirectCall boolValue];
                        [self insertPanicReceiver:panicReceiver];
                    }
                }
            }
        }
    }
    
    return result;
}


#pragma mark - DATABASE FUNCTIONS
-(int) insertPanicReceiver:(PanicReceiver*) model
{
    if(model == nil){
        return 0;
    }
    
    if(model.panic_receivers_email.length <= 0){
        model.panic_receivers_email = @"";
    }
    
    if(model.panic_receivers_phone_number.length <= 0){
        model.panic_receivers_phone_number = @"";
    }
    
    if(model.panic_receivers_receiver_name.length <= 0){
        model.panic_receivers_receiver_name = @"";
    }
    
    DataRow *rowInsert = [[DataRow alloc] init];
    rowInsert.tableName = [NSString stringWithFormat:@"%@", PANIC_RECEIVERS];
    
    [rowInsert addColumnStringValue:model.panic_receivers_email formatName:@"%@", panic_receivers_email];
    [rowInsert addColumnIntValue:model.panic_receivers_owner_id formatName:@"%@", panic_receivers_owner_id];
    [rowInsert addColumnStringValue:model.panic_receivers_phone_number formatName:@"%@", panic_receivers_phone_number];
    [rowInsert addColumnIntValue:model.panic_receivers_receiver_id formatName:@"%@", panic_receivers_receiver_id];
    [rowInsert addColumnStringValue:model.panic_receivers_receiver_name formatName:@"%@", panic_receivers_receiver_name];
    [rowInsert addColumnIntValue:(model.panic_receivers_is_direct_call ? 1 : 0) formatName:@"%@", panic_receivers_is_direct_call];
    
    return [rowInsert insertDBRow]; //return id inserted
}

-(int) updatePanicReceivers:(PanicReceiver*) model
{
    if(model == nil){
        return 0;
    }
    
    if(model.panic_receivers_email.length <= 0){
        model.panic_receivers_email = @"";
    }
    
    if(model.panic_receivers_phone_number.length <= 0){
        model.panic_receivers_phone_number = @"";
    }
    
    if(model.panic_receivers_receiver_name.length <= 0){
        model.panic_receivers_receiver_name = @"";
    }
    
    DataRow *rowUpdate = [[DataRow alloc] init];
    rowUpdate.tableName = [NSString stringWithFormat:@"%@", PANIC_RECEIVERS];
    
    DataColumn *columnUpdate1 = [rowUpdate addColumnInt:[NSString stringWithFormat:@"%@",panic_receivers_owner_id] value:model.panic_receivers_owner_id];
    columnUpdate1.isUpdateCondition = YES;
    DataColumn *columnUpdate2 = [rowUpdate addColumnInt:[NSString stringWithFormat:@"%@",panic_receivers_receiver_id] value:model.panic_receivers_receiver_id];
    columnUpdate2.isUpdateCondition = YES;
    
    [rowUpdate addColumnStringValue:model.panic_receivers_email formatName:@"%@",panic_receivers_email];
    [rowUpdate addColumnIntValue:model.panic_receivers_phone_number formatName:@"%@",panic_receivers_phone_number];
    [rowUpdate addColumnIntValue:model.panic_receivers_receiver_name formatName:@"%@",panic_receivers_receiver_name];
    [rowUpdate addColumnIntValue:(model.panic_receivers_is_direct_call ? 1 : 0) formatName:@"%@",panic_receivers_is_direct_call];
    
    return [rowUpdate commitChangeDBRow];
}

-(int) deletePanicReceiversByOwnerId:(int)ownerId receiverId:(int)receiverId
{
    if(ownerId <= 0){
        return 0;
    }
    
    if(receiverId <= 0){
        return 0;
    }
    
    DataTable *tableDelete = [[DataTable alloc] initWithNameFormat:@"%@", PANIC_RECEIVERS];
    NSString *queryDelete = [NSString stringWithFormat:@"Delete from %@ Where %@ = %d and %@ = %d", PANIC_RECEIVERS, panic_receivers_owner_id, ownerId, panic_receivers_receiver_id, receiverId];
    int status = (int)[tableDelete excuteQueryNonSelect:queryDelete];
    
    return status;
}

-(int) deletePanicReceiversByOwnerId:(int)ownerId
{
    if(ownerId <= 0){
        return 0;
    }
    
    DataTable *tableDelete = [[DataTable alloc] initWithNameFormat:@"%@", PANIC_RECEIVERS];
    NSString *queryDelete = [NSString stringWithFormat:@"Delete from %@ Where %@ = %d", PANIC_RECEIVERS, panic_receivers_owner_id, ownerId];
    int status = (int)[tableDelete excuteQueryNonSelect:queryDelete];
    
    return status;
}

-(NSMutableArray*) loadPanicReceiversByOwnerId:(int)ownerId
{
    if(ownerId <= 0){
        return nil;
    }
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"select %@, %@, %@, %@, %@,%@ from %@ where %@ = %d",
                                  //Select
                                  panic_receivers_email,
                                  panic_receivers_owner_id,
                                  panic_receivers_phone_number,
                                  panic_receivers_receiver_id,
                                  panic_receivers_receiver_name,
                                  panic_receivers_is_direct_call,
                                  
                                  //From
                                  PANIC_RECEIVERS,
                                  
                                  //where
                                  panic_receivers_owner_id,
                                  ownerId
                                  ];
    
    DataTable *table = [[DataTable alloc] init];
    
    //Row result reference
    DataRow *rowReference = [[DataRow alloc] init];
    [rowReference addColumnFormatString:@"%@", panic_receivers_email];
    [rowReference addColumnFormatInt:@"%@",panic_receivers_owner_id];
    [rowReference addColumnFormatString:@"%@", panic_receivers_phone_number];
    [rowReference addColumnFormatInt:@"%@",panic_receivers_receiver_id];
    [rowReference addColumnFormatString:@"%@", panic_receivers_receiver_name];
    [rowReference addColumnFormatInt:@"%@",panic_receivers_is_direct_call];
    
    [table loadTableDataWithQuery:sqlString referenceRow:rowReference];
    
    int rowCount = [table countRows];
    NSMutableArray *results = nil;
    if(rowCount > 0)
    {
        results = [NSMutableArray array];
        for (int i = 0; i < rowCount; i ++) {
            
            PanicReceiver *model = [[PanicReceiver alloc] init];
            DataRow *row = [table rowWithIndex:i];
            
            model.panic_receivers_email = [row stringValueWithColumnNameFormat:@"%@", panic_receivers_email];
            model.panic_receivers_owner_id = [row intValueWithColumnNameFormat:@"%@", panic_receivers_owner_id];
            model.panic_receivers_phone_number = [row stringValueWithColumnNameFormat:@"%@", panic_receivers_phone_number];
            model.panic_receivers_receiver_id = [row intValueWithColumnNameFormat:@"%@", panic_receivers_receiver_id];
            model.panic_receivers_receiver_name = [row stringValueWithColumnNameFormat:@"%@", panic_receivers_receiver_name];
            model.panic_receivers_is_direct_call = ([row intValueWithColumnNameFormat:@"%@", panic_receivers_is_direct_call] > 0);
            [results addObject:model];
        }
    }
    
    return results;
}

-(PanicReceiver*) loadPanicReceiverByOwnerId:(int)ownerId receiverId:(int)receiverId
{
    if(ownerId <= 0){
        return nil;
    }
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"select %@, %@, %@, %@, %@, %@ from %@ where %@ = %d and %@ = %d",
                                  //Select
                                  panic_receivers_email,
                                  panic_receivers_owner_id,
                                  panic_receivers_phone_number,
                                  panic_receivers_receiver_id,
                                  panic_receivers_receiver_name,
                                  panic_receivers_is_direct_call,
                                  
                                  //From
                                  PANIC_RECEIVERS,
                                  
                                  //where
                                  panic_receivers_owner_id,
                                  ownerId,
                                  
                                  //and
                                  panic_receivers_receiver_id,
                                  receiverId
                                  ];
    
    DataTable *table = [[DataTable alloc] init];
    
    //Row result reference
    DataRow *rowReference = [[DataRow alloc] init];
    [rowReference addColumnFormatString:@"%@", panic_receivers_email];
    [rowReference addColumnFormatInt:@"%@",panic_receivers_owner_id];
    [rowReference addColumnFormatString:@"%@", panic_receivers_phone_number];
    [rowReference addColumnFormatInt:@"%@",panic_receivers_receiver_id];
    [rowReference addColumnFormatString:@"%@", panic_receivers_receiver_name];
    [rowReference addColumnFormatInt:@"%@",panic_receivers_is_direct_call];
    
    [table loadTableDataWithQuery:sqlString referenceRow:rowReference];
    
    int rowCount = [table countRows];
    PanicReceiver *result = nil;
    if(rowCount > 0)
    {
        result = [[PanicReceiver alloc] init];
        DataRow *row = [table rowWithIndex:0];
        
        result.panic_receivers_email = [row stringValueWithColumnNameFormat:@"%@", panic_receivers_email];
        result.panic_receivers_owner_id = [row intValueWithColumnNameFormat:@"%@", panic_receivers_owner_id];
        result.panic_receivers_phone_number = [row stringValueWithColumnNameFormat:@"%@", panic_receivers_phone_number];
        result.panic_receivers_receiver_id = [row intValueWithColumnNameFormat:@"%@", panic_receivers_receiver_id];
        result.panic_receivers_receiver_name = [row stringValueWithColumnNameFormat:@"%@", panic_receivers_receiver_name];
        result.panic_receivers_is_direct_call = ([row intValueWithColumnNameFormat:@"%@", panic_receivers_is_direct_call] > 0);
    }
    
    return result;
}

#pragma mark - Panic Notificatin Process
-(void) playAlarmSound
{
    if(!audioPlayer){
        NSURL* musicFile = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:AlertFileNameSound ofType:AlertFileSoundExtension]];
        audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:musicFile error:nil];
    }
    
    //Max volume
    [audioPlayer setVolume:1.0f];
    audioPlayer.numberOfLoops = -1;
    [audioPlayer play];
}

-(void) stopAlarmSound
{
    [audioPlayer stop];
}

//Return count of receivers SMS
-(int) sendSMSPanicByUser:(UserModelV2*)currentUser receivers:(NSMutableArray*)receivers content:(NSString*)content tabbarPopUp:(UITabBarController*)tabbarPopUp
{
    if(![MFMessageComposeViewController canSendText]) {
        [[[[iToast makeText:@"Your device doesn't support SMS!"] setGravity:iToastGravityBottom] setDuration:iToastDurationLong] show];
    }
    
    NSMutableArray *recipents = [NSMutableArray array];
    for (PanicReceiver *curReceiver in receivers) {
        if(curReceiver.panic_receivers_phone_number.length > 0) {
            [recipents addObject:curReceiver.panic_receivers_phone_number];
        }
    }
    
    if(recipents.count <= 0){
        return 0;
    }
    
    MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
    messageController.wantsFullScreenLayout = NO;
    messageController.messageComposeDelegate = self;
    [messageController setRecipients:recipents];
    [messageController setBody:content];
    // Present message view controller on screen
    //[tabbarPopUp presentViewController:messageController animated:YES completion:nil];
    [tabbarPopUp presentViewController:messageController animated:NO completion:nil];
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    
    return (int)receivers.count;
}


//posting Panic Alert
-(int)postingPanicAlertByUserId:(int)userId hotelId:(int)hotelId
{
    //No network detected
    if(![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]){
        return RESPONSE_STATUS_NO_NETWORK;
    }
    
    if(userId <= 0){
        return RESPONSE_STATUS_ERROR;
    }
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole]){
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_PostingPanicAlert *request = [[eHousekeepingService_PostingPanicAlert alloc] init];
    
    [request setIntUsrID:[NSNumber numberWithInt:userId]];
    [request setIntHotelID:[NSNumber numberWithInt:hotelId]];
    
    int result = RESPONSE_STATUS_ERROR;
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding PostingPanicAlertUsingParameters:request];
    NSArray *responseBodyParts = response.bodyParts;
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_PostingPanicAlertResponse class]]) {
            eHousekeepingService_PostingPanicAlertResponse *dataBody = (eHousekeepingService_PostingPanicAlertResponse *)bodyPart;
            if (dataBody != nil) {
                if ([dataBody.PostingPanicAlertResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_OK]]) {
                    result = RESPONSE_STATUS_OK;
                }
            }
        }
    }
    
    return result;
}

//WARNING: Posting Panic to WS is other thread to make app can run smoothly
-(void) panicNowWithTabbarController:(UITabBarController*)tabbarController
{
    isRunningPanic = YES;
    UserManagerV2 *userManager = [UserManagerV2 sharedUserManager];
    UserModelV2 *currentUser = [UserManagerV2 sharedUserManager].currentUser;
    NSMutableArray *panicReceivers = [self loadPanicReceiversByOwnerId:currentUser.userId];
    
    //Log local files
    LogObject *logObj = [[LogObject alloc] init];
    logObj.dateTime = [NSDate date];
    logObj.log = [NSString stringWithFormat:@"<%d><PanicAlert> <UserId>%d</UserId> <LastModified>%@</LastModified> </PanicAlert></%d>",currentUser.userId, currentUser.userId, [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM"], currentUser.userId];
    LogFileManager *logManager = [[LogFileManager alloc] init];
    [logManager save:logObj];
    
    //Initialize data
    NSString *subjectConfig = [[UserManagerV2 sharedUserManager] getCurrentUserConfigValueByKey:USER_CONFIG_PANIC_MESSAGE_SUBJECT];
    NSString *contentConfig = [[UserManagerV2 sharedUserManager] getCurrentUserConfigValueByKey:USER_CONFIG_PANIC_MESSAGE_CONTENT];
    
    //Remove template strings
    subjectConfig = [self relaceTemplateForString:subjectConfig];
    contentConfig = [self relaceTemplateForString:contentConfig];
    
    //SOUND
    NSString *isPlayAlarm = [userManager getCurrentUserConfigValueByKey:USER_CONFIG_PANIC_IS_ALARM_SOUND];
    if(isPlayAlarm.length > 0 && [isPlayAlarm caseInsensitiveCompare:@"true"] == NSOrderedSame){
        [self playAlarmSound];
    }
    
    //Post Panic to WS
    //WARNING: Posting Panic to WS is other thread to make app can run smoothly
    //Using GCD
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, (unsigned long)NULL), ^(void) {
        int responseStatus = [self postingPanicAlertByUserId:currentUser.userId hotelId:currentUser.userHotelsId];
        
        if(responseStatus == RESPONSE_STATUS_NO_NETWORK){
            //Show Toast No Network in main thread
            dispatch_async(dispatch_get_main_queue(), ^{
                if(!isDemoMode) {
                    [[[[iToast makeText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_internet_exception]] setGravity:iToastGravityBottom] setDuration:iToastDurationLong] show];
                }
            });
        } else if(responseStatus != RESPONSE_STATUS_NEW_RECORD_ADDED && responseStatus != RESPONSE_STATUS_OK) {
            //Show Toast error on post
            dispatch_async(dispatch_get_main_queue(), ^{
                [[[[iToast makeText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_error_posting]] setGravity:iToastGravityBottom] setDuration:iToastDurationLong] show];
            });
        }
    });
    
    //SAVE DIRECT CALL LIST
    if (panicReceivers.count > 0) {
        NSString *isPhoneCall = [[UserManagerV2 sharedUserManager] getCurrentUserConfigValueByKey:USER_CONFIG_PANIC_IS_DIRECT_CALL];
        if(isPhoneCall.length > 0 && [isPhoneCall caseInsensitiveCompare:@"true"] == NSOrderedSame) {
            
            //Find phone number to save
            for (PanicReceiver *curReceiver in panicReceivers) {
                if(curReceiver.panic_receivers_is_direct_call){
                    if(curReceiver.panic_receivers_phone_number.length > 0){
                        [userManager setCurrentUserConfig:USER_CONFIG_PANIC_DIRECT_CALL_NUMBER value:curReceiver.panic_receivers_phone_number];
                        break;
                    }
                }
            }
        } else {
            [userManager setCurrentUserConfig:USER_CONFIG_PANIC_DIRECT_CALL_NUMBER value:@""];
        }
    } else {
        [userManager setCurrentUserConfig:USER_CONFIG_PANIC_DIRECT_CALL_NUMBER value:@""];
    }
    
    //SEND SMS
    NSString *isSMS = [userManager getCurrentUserConfigValueByKey:USER_CONFIG_PANIC_IS_SMS];
    if(isSMS.length > 0 && [isSMS caseInsensitiveCompare:@"true"] == NSOrderedSame){
        
        //Contents of sms will be sent
        NSString *smsContent = contentConfig;
        if(smsContent.length <= 0) {
            smsContent = subjectConfig;
        }
        
        //sendSMSPanicByUser will trigger "directCall" func after send
        int countReceivers = [self sendSMSPanicByUser:currentUser receivers:panicReceivers content:smsContent tabbarPopUp:tabbarController];
        
        //Send SMS only send if having phone number
        //If we don't have any receiver, we make a call straight away
        if(countReceivers <= 0){
            [self directCall];
        }
    } else {
        
        //Call straight away if SMS was not enabled
        [self directCall];
    }
}

-(void) stopPanic
{
    isRunningPanic = NO;
    [audioPlayer stop];
}

-(void)directCall
{
    //DIRECT CALL AFTER SEND SMS
    NSString *isPhoneCall = [[UserManagerV2 sharedUserManager] getCurrentUserConfigValueByKey:USER_CONFIG_PANIC_IS_DIRECT_CALL];
    if(isPhoneCall.length > 0 && [isPhoneCall caseInsensitiveCompare:@"true"] == NSOrderedSame) {
        NSString *phoneNumber =  [[UserManagerV2 sharedUserManager] getCurrentUserConfigValueByKey:USER_CONFIG_PANIC_DIRECT_CALL_NUMBER];
        
        //Only call if have phone number
        if(phoneNumber.length > 0) {
            //NSURL* callUrl = [NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", phoneNumber]];
            NSURL* callUrl = [NSURL URLWithString:[NSString stringWithFormat:@"telprompt:%@", phoneNumber]];
            //check  Call Function available only in iphone
            if([[UIApplication sharedApplication] canOpenURL:callUrl]) {
                [[UIApplication sharedApplication] openURL:callUrl];
            } else {
                UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:[L_alert currentKeyToLanguage] message:[L_only_available_on_call_function_warning currentKeyToLanguage] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getOK] otherButtonTitles: nil];
                [alert show];
            }
        }
    }
}

#pragma mark - PRIVATE FUNCTIONS

//MARK: SMS Deletegate
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    switch (result) {
        case MessageComposeResultCancelled:
            break;
            
        case MessageComposeResultFailed:
        {
            [[[[iToast makeText:@"Failed to send SMS"] setGravity:iToastGravityBottom] setDuration:iToastDurationLong] show];
            break;
        }
            
        case MessageComposeResultSent:
        {
            //[[[[iToast makeText:@"Sent SMS  sucessful"] setGravity:iToastGravityBottom] setDuration:iToastDurationLong] show];
        }
            break;
            
        default:
            break;
    }
    
    [controller dismissViewControllerAnimated:YES completion:nil];
    
    //Call after sms response
    [self directCall];
}

-(NSString*)relaceTemplateForString:(NSString*)content
{
    UserManagerV2 *userManager = [UserManagerV2 sharedUserManager];
    UserModelV2 *currentUser = userManager.currentUser;
    
    if(content.length <= 0) {
        content = [NSString stringWithFormat:@"Panic Alert - %@",currentUser.userName];
        return content;
    }
    
    NSRange rangeString = [content rangeOfString:@"[Phone no.]"];
    if(rangeString.length > 0) {
        NSString *raPhone = [userManager getCurrentUserConfigValueByKey:USER_CONFIG_PANIC_RA_PHONE];
        if(raPhone.length <= 0) {
            raPhone = @"";
        }
        content = [content stringByReplacingOccurrencesOfString:@"[Phone no.]" withString:raPhone];
    }
    //Restore to declaration
    rangeString.length = 0;
    rangeString.location = 0;
    
    rangeString = [content rangeOfString:@"[Location]"];
    if(rangeString.length > 0) {
        NSString *location = [RoomManagerV2 getCurrentRoomNo];
        if(location.length <= 0) {
            location = @"";
        }
        content = [content stringByReplacingOccurrencesOfString:@"[Location]" withString:location];
    }
    //Restore to declaration
    rangeString.length = 0;
    rangeString.location = 0;
    
    rangeString = [content rangeOfString:@"[Date]"];
    if(rangeString.length > 0) {
        NSString *dateString = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd"];
        if(dateString.length <= 0){
            dateString = @"";
        }
        content = [content stringByReplacingOccurrencesOfString:@"[Date]" withString:dateString];
    }
    //Restore to declaration
    rangeString.length = 0;
    rangeString.location = 0;
    
    rangeString = [content rangeOfString:@"[Time]"];
    if(rangeString.length > 0) {
        NSString *timeStr = [ehkConvert GetDateTimeNowWithFormat:@"HH:mm:ss"];
        if(timeStr.length <= 0)  {
            timeStr = @"";
        }
        content = [content stringByReplacingOccurrencesOfString:@"[Time]" withString:timeStr];
    }
    //Restore to declaration
    rangeString.length = 0;
    rangeString.location = 0;
    
    rangeString = [content rangeOfString:@"[Sender full name]"];
    if(rangeString.length > 0) {
        NSString *senderFullName = currentUser.userFullName;
        if(senderFullName.length <= 0) {
            senderFullName = @"";
        }
        content = [content stringByReplacingOccurrencesOfString:@"[Sender full name]" withString:senderFullName];
    }
    //Restore to declaration
    rangeString.length = 0;
    rangeString.location = 0;
    
    rangeString = [content rangeOfString:@"[Sender staff ID]"];
    if(rangeString.length > 0) {
        NSString *senderStaffId = [NSString stringWithFormat:@"%d", currentUser.userId];
        if(senderStaffId.length <= 0) {
            senderStaffId = @"";
        }
        content = [content stringByReplacingOccurrencesOfString:@"[Sender staff ID]" withString:senderStaffId];
    }
    //Restore to declaration
    rangeString.length = 0;
    rangeString.location = 0;
    
    rangeString = [content rangeOfString:@"[Supervisor full name]"];
    if(rangeString.length > 0) {
        NSString *supervisorFullName = [userManager getCurrentUserConfigValueByKey:USER_CONFIG_PANIC_SUP_NAME];
        if(supervisorFullName.length <= 0) {
            supervisorFullName = @"";
        }
        content = [content stringByReplacingOccurrencesOfString:@"[Supervisor full name]" withString:supervisorFullName];
    }
    //Restore to declaration
    rangeString.length = 0;
    rangeString.location = 0;
    
    rangeString = [content rangeOfString:@"[staff name]"];
    if(rangeString.length > 0) {
        NSString *staffName = currentUser.userName;
        if(staffName.length <= 0) {
            staffName = @"";
        }
        content = [content stringByReplacingOccurrencesOfString:@"[staff name]" withString:staffName];
    }
    //Restore to declaration
    rangeString.length = 0;
    rangeString.location = 0;
    
    rangeString = [content rangeOfString:@"[Supervisor staff ID]"];
    if(rangeString.length > 0) {
        NSString *supervisorStaffID = [userManager getCurrentUserConfigValueByKey:USER_CONFIG_PANIC_SUP_STAFF_ID];
        if(supervisorStaffID.length <= 0) {
            supervisorStaffID = @"";
        }
        content = [content stringByReplacingOccurrencesOfString:@"[Supervisor staff ID]" withString:supervisorStaffID];
    }
    //Restore to declaration
    rangeString.length = 0;
    rangeString.location = 0;
    
    return content;
}

@end
