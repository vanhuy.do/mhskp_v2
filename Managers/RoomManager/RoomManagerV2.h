//
//  RoomManager.h
//  eHouseKeeping
//
//  Created by Khanh Nguyen on 08/06/2011.
//  Copyright 2011 TMA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RoomModelV2.h"
#import "RoomAdapterV2.h"
#import "CleaningStatusModelV2.h"
#import "DateTimeUtility.h"
#import "UserManagerV2.h"
#import "ehkDefines.h"
#import "RoomAssignmentAdapterV2.h"
#import "GuestInfoModelV2.h"
#import "GuestInfoModelV2.h"
#import "SuperRoomModelV2.h"
#import "SupRoomAssignmentAdapterV2.h"
#import "CleaningStatusAdapterV2.h"
#import "RoomStatusAdapterV2.h"
#import "RoomStatusModelV2.h"
#import "RoomRecordAdapterV2.h"
#import "RoomRecordModelV2.h"
#import "RoomRemarkAdapterV2.h"
#import "RoomRemarkModelV2.h"
#import "HousekeeperRecordModelV2.h"
//#import "HousekeeperRecordAdapterV2.h"
#import "RoomServiceLaterAdapterV2.h"
#import "RoomServiceLaterModelV2.h"
#import "HotelModelV2.h"
#import "HotelAdapterV2.h"
#import "HotelManagerV2.h"
#import "LocationModelV2.h"
#import "LocationAdapterV2.h"
#import "InspectionStatusAdapter.h"
#import "ReassignRoomAssignmentAdapter.h"
#import "RoomRecordDetailAdapterV2.h"
#import "MBProgressHUD.h"
#import "ProfileNoteV2.h"
#import "ProfileNoteTypeV2.h"
#import "ServiceDefines.h"
#import "RoomBlocking.h"
#import "LanguageManagerV2.h"
#import "RoomRecordHistory.h"
#import "RoomRecordHistoryAdapter.h"
#import "SupervisorFilterDetail.h"

#define LOADING_GET_ROOM_ASSIGNMENT @"Get Room Assignment"
#define LOADING_GET_INSPECTION_STATUS @"Get Inspection Status"
#define LOADING_GET_ALL_ROOM_STATUS @"Get Room Status"
#define LOADING_GET_ALL_CLEANING_STATUS @"Get Cleaning Status"
#define LOADING_FIND_ROOM_ASSIGNMENT @"Finding Room Assignment"
#define LOADING_FIND_FLOOR_LIST @"Finding Floor List"

#define roomStatusType      1
#define cleaningStatusType  2
#define kindOfRoomType      3
#define unassignType        4
#define roomOccupiedType    5
#define findRoomNo          6
#define inspectedCase       -2
#define allRoomType         7
#define arrivalType         8
enum ENUM_ROOM_STATUS {
    ENUM_ROOM_STATUS_UNASSIGN = 0,
    ENUM_ROOM_STATUS_VC = 1,
    ENUM_ROOM_STATUS_VD = 2,
    ENUM_ROOM_STATUS_OC = 3,
    ENUM_ROOM_STATUS_OD = 4,
    ENUM_ROOM_STATUS_VI = 5,
    ENUM_ROOM_STATUS_OI = 6,
    ENUM_ROOM_STATUS_VPU = 7,
    ENUM_ROOM_STATUS_OPU = 8,
    ENUM_ROOM_STATUS_VCB = 9,
    ENUM_ROOM_STATUS_OOO = 10,
    ENUM_ROOM_STATUS_OOS = 11,
    ENUM_ROOM_STATUS_OC_SO = 13
};

enum ENUM_CLEANING_STATUS {
    ENUM_CLEANING_STATUS_COMPLETED = 5,
    ENUM_CLEANING_STATUS_PAUSE = 6,
    ENUM_CLEANING_STATUS_DECLINE_SERVICE = 3,
    ENUM_CLEANING_STATUS_SERVICE_LATER = 2,
    
    ENUM_CLEANING_STATUS_START = 4,
    ENUM_CLEANING_STATUS_PENDING_FIRST_TIME = 7,
    ENUM_CLEANING_STATUS_PENDING_SECOND_TIME = 8,
    ENUM_CLEANING_STATUS_DND = 1,
    ENUM_CLEANING_STATUS_DOUBLE_LOCK = 9
};

enum ENUM_INSPECTION_STATUS {
    ENUM_INSPECTION_PENDING = 0,
    ENUM_INSPECTION_STARTED = 1,
    ENUM_INSPECTION_PAUSE = 2,
    ENUM_INSPECTION_COMPLETED_PASS = 3,
    ENUM_INSPECTION_COMPLETED_FAIL = 4,
};

enum ENUM_COUNT_TIME {
    NOT_START = -1,
    START = 0,
    PAUSE = 1,
    FINISH = 2,
    STOP = 3
};

enum ENUM_ROOMDETAIL_STATUS {
    ENUM_MAID_INSPECTED_COMPLETED = 1,
    ENUM_MAID_NOT_INSPECTED_COMPLETED = 2,
    ENUM_MAID_NOT_COMPLETED = 3,
    ENUM_SUPER_COMPLETED = 4,
    ENUM_SUPER_NOTCOMPLETED = 5
};

enum ENUM_KIND_OF_ROOM {
    ENUM_KIND_OF_ROOM_NORMAL = 0,
    ENUM_KIND_OF_ROOM_QUEUE = 1,
    ENUM_KIND_OF_ROOM_RUSH = 2,
    ENUM_KIND_OF_ROOM_OCCUPIED_DUEOUT = 4,
    ENUM_KIND_OF_ROOM_SPECIAL_ASSIGNMENT = 8, // CFG [20160913/CRF-00001439]
    ENUM_KIND_OF_ROOM_PROFILE_NOTES_EXISTS = 16, // CFG [20160928/CRF-00000827]
    ENUM_KIND_OF_ROOM_STAY_OVER_EXISTS = 32, // CRF [20161106/CRF-00001293]
    ENUM_KIND_OF_ROOM_INSPECTION_CROSS_ZONE = 64, // CRF [20161221/CRF-0001557]
};

enum ENUM_ROOM_TYPE_OCCUPIED {
    ENUM_ROOM_TYPE_OCCUPIED_OD_DO = 1,
    ENUM_ROOM_TYPE_ROOM_OCCUPIED_DUEOUT = 2
};

enum ENUM_TASK_TYPE{
    ENUM_TASK_TYPE_NORMAL = 1,
    ENUM_TASK_TYPE_REASSIGN = 2,
    ENUM_TASK_TYPE_MOCKUP = 3
};

// CFG [20160914/CRF-00001439] - Special assignment method enum's. 0 = No special assignment, 1 = Primary RA only update, 2 = Anyone can update, but must be online.
enum ENUM_ROOM_ASSIGNMENT_SPECIAL_ASSIGNMENT_METHOD {
    ENUM_ROOM_ASSIGNMENT_SPECIAL_ASSIGNMENT_METHOD_DISABLED = 0,
    ENUM_ROOM_ASSIGNMENT_SPECIAL_ASSIGNMENT_METHOD_PRIMARY_RA_ONLY = 1,
    ENUM_ROOM_ASSIGNMENT_SPECIAL_ASSIGNMENT_METHOD_BOTH_RA_MUST_BE_ONLINE = 2
};
// CFG [20160914/CRF-00001439] - End.

// CFG [20161221/CRF-00001557] - Inspection cross zone method enum's. 0 = No inspection cross zone, 1 = Primary RA only update, 2 = Anyone can update, but must be online.
enum ENUM_INSPECTION_CROSS_ZONE_METHOD {
    ENUM_INSPECTION_CROSS_ZONE_METHOD_DISABLED = 0,
    ENUM_INSPECTION_CROSS_ZONE_METHOD_PRIMARY_SUP_ONLY = 1,
    ENUM_INSPECTION_CROSS_ZONE_METHOD_BOTH_SUP_MUST_BE_ONLINE = 2
};
// CFG [20161221/CRF-00001557] - End.


@interface RoomManagerV2 : NSObject {
//    RoomAdapterV2 *adapter;
    RoomAssignmentAdapterV2 *roomAssignmentAdapter;
//    GuestInfoAdapter *guestInfoAdapter;    
    SupRoomAssignmentAdapterV2* superRoomAdapter;
    NSMutableArray *roomAssignmentList;
    CleaningStatusAdapterV2 *cleaningStatusAdapter;
//    LocationAdapter *locationAdapter;
    RoomStatusAdapterV2 *roomStatusAdapter;
    RoomRecordAdapterV2 *roomRecordAdapter;
    RoomRemarkAdapterV2 *roomRemarkAdapter;
}

//@property (nonatomic, strong) RoomAdapterV2 *adapter;
@property (nonatomic, strong) RoomAssignmentAdapterV2 *roomAssignmentAdapter;
//@property (nonatomic, retain) GuestInfoAdapter *;
@property (nonatomic, strong) SupRoomAssignmentAdapterV2* superRoomAdapter;
@property (nonatomic, strong) NSMutableArray *roomAssignmentList;
@property (nonatomic, strong) CleaningStatusAdapterV2 *cleaningStatusAdapter;
//@property (nonatomic, retain) LocationAdapter *locationAdapter;
@property (nonatomic, strong) RoomStatusAdapterV2 *roomStatusAdapter;
@property (nonatomic, strong) RoomRecordAdapterV2 *roomRecordAdapter;
@property (nonatomic, strong) RoomRemarkAdapterV2 *roomRemarkAdapter;
@property (nonatomic, assign) BOOL isFindRoom;
//-(void) postRoomStatus;
+ (RoomManagerV2*) sharedRoomManager;
+ (RoomManagerV2*) updateRoomManagerInstance;
//-(BOOL) updateRoomStatus;

#pragma mark - Supervisor Filter
/*!
 CRF-1259: Display filter for Supervisor Home Screen
 */
-(int) getSupervisorFilterRoutineByUserId:(int)userId buildingId:(int)buildingId hotelId:(int)hotelId listRoomAssignmentCompare:(NSMutableArray*) listRoomCompare;
-(int) updateGuestInfoByUserId:(int)userId hotelId:(int)hotelId roomNumber:(NSString*)roomNumber;
-(int) updateRoomStatusByUserId:(int)userId roomNo:(NSString*)roomNo statusId:(int)statusId cleaningStatusId:(int)cleaningId;
//-(BOOL) updateCleaningStatus;
-(BOOL) postGuestInfo:(NSInteger) UsrID WithRoomAssignID:(NSInteger) raID;
-(BOOL) postAllGuestInfoChanged;

-(void) updateLocation;

+(NSString*)getCurrentRoomNo;
+(void)setCurrentRoomNo:(NSString*) roomNo;
+(void)resetCurrentRoomNo;
-(NSMutableArray*)getCleaningStatus;

+(NSInteger)getCurrentCleaningStatus;
+(void)setCurrentCleaningStatus:(NSInteger)cleaningStatus;
+(void)resetCurrentCleaningStatus;


-(void)upDateTimeRoomRecordModel:(int)cleaning_status_id;
-(NSString*)getImgCleaningStatus:(int)cstatID;
//-(void)otherActionOnCleaningStatus:(int)cleaning_status_id;

#pragma mark - Supervisor Filter Database

-(NSInteger) insertSupervisorFiltersRoutine:(SupervisorFilterDetail*)model;
-(NSInteger) deleteSupervisorFiltersRoutineByUserId:(int)userId;
-(NSMutableArray*) loadSupervisorFiltersRoutineByUserId:(int)userId;
-(NSMutableArray*) loadSupervisorFiltersRoutineByUserId:(int)userId typeId:(int)typeId;
-(int) countSupervisorFiltersRoutineByUserId:(int)userId;

#pragma mark - RoomAssignment Model
-(NSInteger)insert:(RoomAssignmentModelV2*)roomAssignment;
-(NSInteger)update:(RoomAssignmentModelV2*)roomAssignment;
-(NSInteger)delete:(RoomAssignmentModelV2*)roomAssignment;
-(void)load:(RoomAssignmentModelV2*)roomAssignment;

//Get last modified of room assignment by user id
-(NSString*)getRoomAssignmentLastModifiedDateByUserId:(int)userId;

//Any change query in this function, you should change "-(int)countAllRoomAssignmentsByUserID:(int)userId" too
-(NSMutableArray*)loadAllRoomAssignmentsByUserID:(RoomAssignmentModelV2*)roomAssignment;
//Any change query in this function, you should change "-(NSMutableArray*)loadAllRoomAssignmentsByUserID:(RoomAssignmentModelV2*)roomAssignment" too
-(int)countAllRoomAssignmentsByUserID:(int)userId;

-(NSMutableArray *) loadAllRoomAssignmentsByUserId:(NSInteger) userId;
-(NSMutableArray *)loadRoomAssignmentsBySuperVisorId:(int)supervisorId houseKeeperId:(NSInteger)houseKeeperId;
-(bool)getRoomAssignmentByUserIDAndRoomID:(RoomAssignmentModelV2*)roomAssignment;
-(bool)getRoomIdByRoomAssignment:(RoomAssignmentModelV2*)roomAssignment;
-(void)loadRoomAsignmentByRoomIdAndUserID:(RoomAssignmentModelV2*)roomAssignment;
-(NSInteger)loadMaxPrioprity;
-(int)reloadRoomStatusWSByUserId:(int)userId roomAssignmentId:(int)assignmentId currentRoomStatusId:(int)currentStatusId;

-(int)getRoomTypeListWSWithHotelId:(NSInteger)hotelId;
-(BOOL)getRoomAssignmentWSByUserID:(NSInteger)userID AndIsSupervisor:(BOOL)isSupervisor AndLastModified:(NSString *)strLastModified AndPercentView:(MBProgressHUD *)percentView;
-(int) postRoomAssignmentWSByUserID:(NSInteger) userID AndRaModel:(RoomAssignmentModelV2 *) model;
-(int) postRoomInspectionWSByUserID:(NSInteger) userID AndRaModel:(RoomAssignmentModelV2 *) model WithStringRemark: (NSString*) remark;
-(int) reAssignRoomAssignmentWSByReassignRoomAssignmentModel:(ReassignRoomAssignmentModel *) reModel;
-(int) setUnassignedRoomWSByReassignRoomAssignmentModel:(ReassignRoomAssignmentModel *)reModel;
-(NSInteger) updateCleaningStatusWSByUserId:(NSInteger) userId withRoomAssignmentModel:(RoomAssignmentModelV2 *) roomAssignmentModel;

-(NSInteger) numberOfMustPostRoomAssignmentRecordsWithSupervisorId:(NSInteger) userId;
-(NSInteger)numberOfRoomAssignmentMustPost;
-(NSInteger) loadMinPrioprity;
-(void) deleteOldRoomAssignmentsWithUserId:(NSInteger) userId;
-(int)deleteAllRoomAssignment;

//If any change this function in the future must change query of "-(int) countAllRoomAssignmentsWithUserId:(NSInteger) userId" too
-(NSMutableArray *) getAllRoomAssignmentsWithUserId:(NSInteger) userId;
//If any change this function in the future must change query of "-(NSMutableArray *) getAllRoomAssignmentsWithUserId:(NSInteger) userId" too
-(int) countAllRoomAssignmentsWithUserId:(NSInteger) userId;

-(void) setDeleteKey:(NSInteger)deleteId forRoomAssignmentId:(NSInteger) raId andUserId:(NSInteger) userId;
-(void) checkAndRemoveMissingRoomAssignments:(NSMutableArray *) roomAssigns andUserId:(NSInteger) userId;

#pragma mark - === Update Sequence Room Assignment ===
#pragma mark
-(BOOL) changeRoomAssignmentSequenceFromrRoomAssignId:(NSInteger) fromRaId toRoomAssignId:(NSInteger) toRaId forUserId:(NSInteger) userId;
 //An Rework for ReassignRoom
-(void)reassignRoom:(RoomModelV2*) room andRoomAssignmentModelV2:(RoomAssignmentModelV2*) roomAssigns;
#pragma mark - Room Model
-(NSInteger)insertRoomModel:(RoomModelV2*)roomModel;
-(NSInteger)updateRoomModel:(RoomModelV2*)roomModel;
-(int)deleteAllRoom;
-(void)loadRoomModel:(RoomModelV2*)roomModel;
+(NSMutableArray *)getAllRoomType;
+(NSMutableArray *)getAllRoom;

//Hao Tran - Fix enhance loading data in sync
//-(BOOL) getRoomWSByUserID:(NSInteger) userID AndRaID:(NSInteger) raID AndLastModified:(NSString *) strLastModified AndLastCleaningDate:(NSString *) lastCleaningDate;
-(BOOL) getRoomWSByUserID:(NSInteger) userID AndRaID:(NSInteger) raID AndLastModified:(NSString *) strLastModified shouldUpdateRoomRecord:(BOOL)shouldUpdateRoomRecord;
-(BOOL) isValidRoomWSByUserId:(NSInteger) userID roomNumber:(NSString*)roomNumber hotelId: (NSString*) hotelID;
-(NSMutableArray *) loadAllRoomUserId:(NSInteger) userId;

#pragma mark - Room Type
-(RoomTypeModel *) loadRoomTypeModelByPrimaryKey:(RoomTypeModel *) model;
-(NSMutableArray *)loadAllRoomType;

#pragma mark - Room Blocking
-(int) insertRoomBlocking:(RoomBlocking*)model;
-(int) updateRoomBlocking:(RoomBlocking*)model;
-(int) deleteAllRoomBlockingByUserId:(int)userId;
-(int) unlockAllRoomBlockingByUserId:(int) userId;
-(int) unlockRoomBlockingByUserId:(int) userId roomNumber: (NSString*)roomNumber;
-(RoomBlocking*) loadRoomBlockingByUserId:(int)userId roomNumber:(NSString*) roomNumber;
//checking whether has room blocking data or not
-(int) countRoomBlockingByUserId:(int)userId roomNumber:(NSString*)roomNumber;

-(int) getWSOOSBlockRoomListByUserId:(NSInteger)userId hotelId:(NSInteger)hotelId listRoomAssignmentCompare:(NSMutableArray*)listRoomCompare;
//NOTE: this function must be called after function -(int) getWSOOSBlockRoomListByUserId:(NSInteger)userId hotelId:(NSInteger)hotelId listRoomAssignmentCompare:(NSMutableArray*) listRoomCompare
//it is mandatory
-(int) getWSReleaseRoomListByUserId:(NSInteger)userId hotelId:(NSInteger)hotelId listRoomAssignmentCompare:(NSMutableArray*) listRoomCompare;

//remarkType = RemarkType_RoomRemark => get room remark
//remarkType = RemarkType_PhyshicalCheck => get room remarks physical check
-(NSString*) getRoomRemarksByUserId:(NSInteger)userId hotelId:(NSInteger)hotelId remarkType:(int)remarkType roomNumber:(NSString*)roomNumber;

//remarkType = RemarkType_RoomRemark => update room remark
//remarkType = RemarkType_PhyshicalCheck => update room remarks physical check
-(int) updateRoomRemarksByUserId:(NSInteger)userId hotelId:(NSInteger)hotelId remarkType:(int)remarkType roomNumber:(NSString*)roomNumber remarkValue:(NSString*)remarkValue;

#pragma mark - Guest Info
-(NSInteger)insertGuestInfo:(GuestInfoModelV2*)guestInfoModel;
//-(NSInteger)updateGuestInfo:(GuestInfoModelV2*)guestInfoModel;
-(void)loadGuestInfo:(GuestInfoModelV2*)guestInfoModel;
-(void)loadByRoomIDGuestInfo:(GuestInfoModelV2*)guestInfoModel;
-(GuestInfoModelV2 *)loadByRoomID:(NSString*) roomNumber;
-(NSMutableArray*)loadGuestsByUserId:(int)userId roomNumber:(NSString*)roomNumber guestType:(int)guestType;
-(BOOL)isDueOutRoomByRoomId:(NSInteger) roomId;

//WS ProfileNote
-(BOOL)loadWSProfileNoteByUserId:(int)userId hotelId:(int)hotelId roomNumber:(NSString*)roomNumber;

//WS Search Guest
-(NSMutableArray*) findGuestStayedHistoryRoutineWSByUserId:(int)userId searchKey:(NSString*)searchKey hotelId:(int)hotelId buildingId:(int)buildingId filterMethod:(int)filterMethod filterRange:(int)filterRange pageNumber:(int)pageNumber;

//Localdatabase
//Guest Info Profile Note Type
-(NSMutableArray*) loadProfileNoteTypesByUserId:(int)userId roomNumber:(NSString*)roomNumber;
-(NSMutableArray*) loadProfileNoteTypesByUserId:(int)userId roomNumber:(NSString*)roomNumber guestId:(int)guestId;
-(int) insertProfileNoteType:(ProfileNoteTypeV2*)model;
-(int) deleteProfileNoteTypeByUserId:(int)userId roomNumber:(NSString*)roomNumber;
-(int) deleteGuestByUserId:(int)userId roomNumber:(NSString*)roomNumber guestType:(int)guestType;
-(int) updateProfileNoteType:(ProfileNoteTypeV2*)model;

//Guest Info Profile Note
-(NSMutableArray*) loadProfileNotesByUserId:(int)userId profileNoteTypeId:(int)profileNoteTypeId;
//Guest Info Profile Note
-(NSMutableArray*) loadProfileNotesByUserId:(int)userId roomNumber:(NSString*)roomNumber guestId:(int)guestId;

-(int) insertProfileNote:(ProfileNoteV2*)model;
-(int) deleteProfileNoteByUserId:(int)userId profileNoteTypeId:(int)profileNoteTypeId;
-(int) deleteProfileNoteByUserId:(int)userId roomNumber:(NSString*)roomNumber;

#pragma mark - SuperRoomModel
-(NSMutableArray *) getAllRoomInspectionWithSupervisorId:(NSInteger) userId;
-(NSMutableArray *) getAllRoomAssignmentDatas:(NSInteger) userId;
-(void)getRoomAssignments;
-(void)getRoomInspection;
-(void)getRoomInspectionByFilterType:(int)filterType filterDetailId:(int)filterDetail;
-(void)getRoomAssignmentComplete;
-(void)getTotalRoomComlete;
-(void)getRoomInspectionComplete;
-(void)getInspectedRoom;
-(void)getRoomInspectedPassed;
-(void)getRoomInspectedFailed;
-(NSMutableArray*)getRoomTypesCurrentUser;
-(NSString*)getPropertyName:(SuperRoomModelV2*)superRoomModel;
-(NSString*)getbuildingName:(SuperRoomModelV2*)superRoomModel;
-(NSString*)getHouseKeeperName;
-(NSString*)getRoomStatus:(SuperRoomModelV2*)superRoomModel;
-(NSData*)getHotelLogo:(SuperRoomModelV2*)superRoomModel;
-(NSDictionary *) getRoomAssignmentDataByRoomAssignmentId:(NSInteger)roomAssignmentId andRaUserId:(NSInteger)raUserId;
-(NSDictionary *) getRoomAssignmentDataByRoomId:(NSInteger)roomId andRaUserId:(NSInteger)raUserId;
-(NSDictionary *) getRoomAssignmentDataByRoomAssignmentId:(NSInteger)roomAssignmentId andRaUserId:(NSInteger)raUserId isFilterCurrentDate:(BOOL)isFilterCurrentDate;

#pragma mark - Cleaning Status Model
-(int)insertCleaningStatus:(CleaningStatusModelV2*)cleaningStatus;
-(void)loadCleaningStatus:(CleaningStatusModelV2*)cleaningStatus;
-(int)updateCleaningStatus:(CleaningStatusModelV2*)cleaningStatus;
-(NSMutableArray*)loadAllCleaningStatus;
-(NSMutableArray*)loadAllCleaningStatusWithCleaningStatusID:(NSInteger)clsID;
-(NSMutableArray *)loadAllCleaningStatusForSupervisor;
-(NSString*)getCleaningStatusLastModifiedDate;

//-(NSMutableArray*)loadMaidCleaningStatusByRoomStatus:(BOOL)isOcupied;
//-(NSMutableArray*)loadSuperVisorCleaningStatusByRoomStatus:(BOOL)isOcupied;

-(BOOL) getAllCleaningStatusWS:(NSString *) strLastModified AndPercentView:(MBProgressHUD*)percentView;

// Loacation Model
/*
-(int)insertLocationModel:(LocationModel*)locationModel;
-(int)updateLocationModel:(LocationModel*)locationModel;
-(void)loadLocationModel:(LocationModel*)locationModel;
*/
#pragma mark - Room Record Model
-(int) insertRoomRecordData:(RoomRecordModelV2 *) record;
-(int) updateRoomRecordData:(RoomRecordModelV2 *) record;
-(int) deleteRoomRecordData:(RoomRecordModelV2 *) record;
-(RoomRecordModelV2 *) loadRoomRecordData:(RoomRecordModelV2 *) record;
-(RoomRecordModelV2 *) loadRoomRecordDataByRoomIdAndUserId:(RoomRecordModelV2 *) record;
-(NSMutableArray *) loadAllRoomRecordByCurrentUser;
-(NSMutableArray *) loadAllRoomRecord;

#pragma mark - Room Record History Model
-(int) insertRoomRecordHistoryData:(RoomRecordHistory *) record;
-(int) updateRoomRecordHistoryData:(RoomRecordHistory *) record;
-(int) deleteRoomRecordHistoryData:(RoomRecordHistory *) record;
-(int) countUnpostRoomRecordHistoryByUserId:(NSInteger) userId;
-(RoomRecordHistory *) loadRoomRecordHistoryData:(RoomRecordHistory *) record;
-(RoomRecordHistory *) loadRoomRecordHistoryDataByRoomIdAndUserId:(RoomRecordHistory *) record;
-(NSMutableArray *) loadAllRoomRecordHistoryByCurrentUser;
-(NSMutableArray *) loadAllUnpostRoomRecordHistoryByCurrentUser;
-(NSMutableArray *) loadAllRoomRecordHistory;

//Hao Tran - Remove for unneccessary function
//-(NSInteger) getCleaningPauseTime:(NSInteger) raID AndUserID:(NSInteger) userID;
//-(NSInteger) getInspectedPauseTime:(NSInteger) raID AndUserID:(NSInteger) userID; 

-(NSInteger)numberOfRoomServiceLaterMustPost;

#pragma mark - room record detail

-(int) insertRoomRecordDetailData:(RoomRecordDetailModelV2 *) record;
-(int) updateRoomRecordDetailData:(RoomRecordDetailModelV2 *) record;
-(int) deleteRoomRecordDetailData:(RoomRecordDetailModelV2 *) record;
-(RoomRecordDetailModelV2 *) loadRoomRecordDetailData:(RoomRecordDetailModelV2 *) record;
-(RoomRecordDetailModelV2 *) loadRoomRecordDataDetailByRoomAssignmentIdAndUserId:(RoomRecordDetailModelV2 *) record;
-(NSInteger) numberOfRoomRecordDetails:(RoomRecordDetailModelV2 *) record;
-(NSMutableArray *) loadAllRoomRecordDetailsByRoomAssignmentId:(NSInteger) raId andUserId:(NSInteger) userId;
-(NSInteger) numberOfRoomRecordDetailMustSyn;
-(NSMutableArray *) loadAllRoomRecordDetailByCurrentUser:(NSInteger)userID;
-(BOOL)postRoomRecordDetail:(RoomRecordDetailModelV2*)roomRecordDetail;
-(BOOL)getRoomRecordDetail:(NSInteger)userId andLastModified:(NSString*)lastModified;

#pragma mark - Room Status Model
+(NSString*)getRoomStatusCodeByEnum:(enum ENUM_ROOM_STATUS)roomStatus;

-(int) insertRoomStatusData:(RoomStatusModelV2 *) status;
-(int) updateRoomStatusData:(RoomStatusModelV2 *) status;
-(int) deleteRoomStatusData:(RoomStatusModelV2 *) status;
-(int)deleteAllRoomStatusData;
//get last modified date RS
-(NSString*)getRoomStatusLastModifiedDate;
-(RoomStatusModelV2 *) loadRoomStatusData:(RoomStatusModelV2 *) status;
-(NSMutableArray *) loadAllRoomStatus;
-(NSMutableArray *) loadAllMaidRoomStatusByIsOcupied:(BOOL)isOcupied;
-(NSMutableArray *) loadAllSuperVisorRoomStatusByIsOcupied:(BOOL)isOcupied;
-(RoomStatusModelV2 *) loadRoomStatusByStatusNameData:(RoomStatusModelV2 *) status;
-(NSMutableArray *) loadAllSuperVisorBlockRoomStatusByIsOcupied:(BOOL)isOcupied;
-(NSMutableArray *) loadAllSuperVisorRoomStatusByIsOcupied:(BOOL)isOcupied andChekList:(NSInteger)tchk;
-(BOOL) getAllRoomStatusWS:(NSString *) strLastModified AndPercentView:(MBProgressHUD*)percentView;

//conrad
-(NSMutableArray *) loadAllMaidRoomStatus;

#pragma mark - inspection Status Model
-(BOOL) insertInspectionStatusModel:(InspectionStatusModel *)model;
-(InspectionStatusModel *) loadInspectionStatusModelByPrimaryKey:(int)inspectionStatusId;
-(BOOL) updateInspectionStatusModel:(InspectionStatusModel *) model;
-(BOOL) deleteInspectionStatusModel:(InspectionStatusModel *) model;
-(void) getInspectionStatusWSWithUserId:(NSInteger) userId andLastModified:(NSString *) lastModified AndPercentView:(MBProgressHUD*)percentView;

#pragma mark - Room Remark Model
-(int) insertRoomRemarkData:(RoomRemarkModelV2 *) remark;
-(int) updateRoomRemarkData:(RoomRemarkModelV2 *) remark;
-(int) deleteRoomRemarkData:(RoomRemarkModelV2 *) remark;
-(RoomRemarkModelV2 *) loadRoomRemarkData:(RoomRemarkModelV2 *) remark;
-(RoomRemarkModelV2 *) loadRoomRemarkDataByRecordId:(RoomRemarkModelV2 *) remark;
-(int)countRoomRemarkData:(NSInteger)recordID;
- (int)countRoomRemarkUnPost;
-(NSMutableArray *) loadAllRoomRemark;
-(NSMutableArray *) loadAllRoomRemarkUnPost;
-(NSMutableArray *) loadAllRoomRemarkByRecordId:(int) recordId;
-(BOOL) postRoomRemarkWSWithRoomRemark:(RoomRemarkModelV2 *) remark WithUserId:(NSInteger) userId andRoomAssignmentId:(NSInteger) raId;
-(NSInteger) numberOfMustPostGuestInfoRecordsWithUserId:(NSInteger) userId;

#pragma mark - Room Service Later Model
-(int) insertRoomServiceLaterData:(RoomServiceLaterModelV2 *) service;
-(int) updateRoomServiceLaterData:(RoomServiceLaterModelV2 *) service;
-(int) deleteRoomServiceLaterData:(RoomServiceLaterModelV2 *) service;
-(RoomServiceLaterModelV2 *) loadRoomServiceLaterData:(RoomServiceLaterModelV2 *) service;
-(RoomServiceLaterModelV2 *) loadRoomServiceLaterDataByRecordId:(RoomServiceLaterModelV2 *) service;
-(NSMutableArray *) loadAllRoomServiceLaterByRecordId:(int) recordId;
-(NSMutableArray *) loadAllRoomServiceLater;

#pragma mark - === ReAssign Room Assignment ===
#pragma mark
-(BOOL) insertReassignRoomAssignmentModel:(ReassignRoomAssignmentModel *)model;
-(ReassignRoomAssignmentModel *) loadReassignRoomAssignmentModelByPrimaryKey:(ReassignRoomAssignmentModel *) model;
-(BOOL) updateReassignRoomAssignmentModel:(ReassignRoomAssignmentModel *) model;
-(BOOL) deleteReassignRoomAssignmentModel:(ReassignRoomAssignmentModel *) model;
-(NSInteger) numberOfMustPostReassignRecordsWithUserId:(NSInteger) userId;
-(NSMutableArray *) loadAllReassignRoomAssignmentModelByUserId:(NSInteger) userId;
-(NSInteger)updateRoomHaveBeenRemove:(NSInteger) userID AndRaModel:(RoomAssignmentModelV2 *) model;

-(BOOL)GetFindRoomAssignmentListWSByUserID:(NSInteger)userID AndRoomstatus:(NSInteger)roomStatusID AndFilterType:(NSInteger)filterType AndLastModified:(NSString *)strLastModified AndHotelID:(NSString*)hotelId AndPercentView:(MBProgressHUD *)percentView;
-(BOOL)GetFindRoomAssignmentListWSByUserID:(NSInteger)userID AndFloorId:(NSInteger)floorId AndRoomstatus:(NSInteger)roomStatusID AndFilterType:(NSInteger)filterType AndPercentView:(MBProgressHUD*)percentView;
-(BOOL)GetFindRoomDetailsListWSByUserID:(NSInteger)userID AndFloorId:(NSInteger)floorId AndBuildingId:(NSInteger)buildingId AndHotelId:(NSInteger)HTID AndFilterType:(NSInteger) filterType AndGeneralFilterType:(NSString*) generalFilterType AndPercentView:(MBProgressHUD*)percentView;
-(NSMutableArray*)GetFindRoomDetailsList1WSByUserID:(NSInteger)userID AndFloorId:(NSInteger)floorId AndBuildingId:(NSInteger)buildingId AndHotelId:(NSInteger)HTID AndFilterType:(NSInteger) filterType AndGeneralFilterType:(NSString*) generalFilterType AndPercentView:(MBProgressHUD*)percentView;
-(NSArray*)GetZoneFindRoomDetailsListWSByUserID:(NSInteger)userID AndFloorId:(NSInteger)floorId AndBuildingId:(NSInteger)buildingId AndHotelId:(NSInteger)HTID AndFilterType:(NSInteger) filterType AndGeneralFilterType:(NSString*) generalFilterType AndPercentView:(MBProgressHUD*)percentView;
-(void)getFloorListFromWSWith:(NSInteger)userId AndHotelId:(NSInteger)hotelId AndBuildingId:(NSInteger)buildingId AndRoomStatus:(NSInteger)statusId AndFilterType:(NSInteger)filterType AndPercentView:(MBProgressHUD *)percentView;

-(NSMutableArray*)getOnlineFloorListWith:(NSInteger)userId AndHotelId:(NSInteger)hotelId AndBuildingId:(NSInteger)buildingId AndRoomStatus:(NSInteger)statusId AndFilterType:(NSInteger)filterType AndPercentView:(MBProgressHUD *)percentView;
-(NSArray*)GetFloorFindRoomDetailsListWSByUserID:(NSInteger)userID AndFloorId:(NSString*)floorId AndBuildingId:(NSInteger)buildingId AndHotelId:(NSInteger)HTID AndFilterType:(NSInteger) filterType AndGeneralFilterType:(NSString*) generalFilterType AndPercentView:(MBProgressHUD*)percentView;
-(NSMutableArray *) getAllRoomInspectionWithSupervisorId:(NSInteger) userId andRoomStatusID:(NSInteger)roomID;
-(NSMutableArray *) getAllRoomByFloorId:(NSInteger)floorId AndUserId:(NSInteger)userId AndRoomStatus:(NSInteger)roomStatusId;
-(NSMutableArray *) getAllRoomByFloorId:(NSInteger)floorId userId:(NSInteger)userId filterType:(NSInteger)filterType statusId:(NSInteger)statusId;
-(NSMutableArray *) getUnassginRoomsByFloorId:(NSInteger)floorId AndUserId:(NSInteger)userId;
-(NSMutableArray *) getUnassginRoomsByUserId:(NSInteger)userId;
-(NSMutableArray *) getAllRoomByFloorId:(NSInteger)floorId AndUserId:(NSInteger)userId;
-(NSMutableArray *) getAllOfRoomByFloorId:(NSInteger)floorId AndUserId:(NSInteger)userId;
-(NSMutableArray *) getAllOfRoomByFloorId:(NSInteger)floorId AndUserId:(NSInteger)userId andFilter:(int)kindOfRoom;
-(NSMutableArray *) getAllRoomByFloorId:(NSInteger)floorId AndUserId:(NSInteger)userId AndCleaningStatus:(NSInteger)cleaningStatusId;
-(NSMutableArray *) getAllRoomByFloorId:(NSInteger)floorId AndUserId:(NSInteger)userId AndKindOfRoom:(NSInteger)kindOfRoom;
-(NSMutableArray *) getAllRoomByRoomByUserId:(NSInteger)userId AndKindOfRoom:(NSInteger)kindOfRoom;
-(NSMutableArray*)getOnlineFindFloorDetailListWith:(NSInteger)userId AndHotelId:(NSInteger)hotelId AndBuildingId:(NSInteger)buildingId AndGeneralFilter:(NSString*)generalFilter AndFilterType:(NSInteger)filterType AndPercentView:(MBProgressHUD *)percentView;

-(NSMutableArray *) getAllRoomByFloorId:(NSInteger)floorId userId:(NSInteger)userId filterType:(NSInteger)filterType generalFilter:(NSString*)generalFilter;
-(NSMutableArray *) getRoomDetailsListByFloorId:(NSInteger)floorId userId:(NSInteger)userId filterType:(NSInteger)filterType generalFilter:(NSString*)generalFilter;
-(NSMutableArray *) getZoneRoomDetailsListByFloorId:(NSInteger)floorId userId:(NSInteger)userId filterType:(NSInteger)filterType generalFilter:(NSString*)generalFilter;
-(NSMutableArray *) getFloorRoomDetailsListByFloorId:(NSString* )floorId userId:(NSInteger)userId filterType:(NSInteger)filterType generalFilter:(NSString*)generalFilter;
-(NSMutableArray *) getZoneRoomDetailsListByFloorId:(NSInteger)floorId userId:(NSInteger)userId filterType:(NSInteger)filterType generalFilter:(NSString*)generalFilter;
#pragma mark - Alert Room Behind Schedule & Rush/Queue Room

//Return list of PopupMsgItem
-(NSMutableArray*) getPopUpMessageFromWSByUserId:(int)userId lastModified:(NSString*)lastModified;

//List ID is NSString with comma between them e.g 1,2,3,4 ...
-(int) updateReadMessagePopUpByUserId:(int)userId listID:(NSString*)listIds;
#pragma mark - Post WSLog
-(BOOL)postWSLog:(int)userId message:(NSString*)messageValue messageType:(int)type;
-(int)updateIsCheckedRa: (NSInteger)userID houseKeeperId:(NSString*)houseKeeperId;
-(int)updateIsCheckedFind: (NSInteger)raID;
-(BOOL)postLogInvalidStartRoomWithUserID:(NSInteger)userId userName:(NSString*)userName prevRoomId:(NSString*)prevRoomId prevRoomNo:(NSString*)prevRoomNo newRoomId:(NSString*)newRoomId newRoomNo:(NSString*)newRoomNo;

#pragma mark - Get Room Type By Room Number
- (RoomTypeModel*)getRoomTypeByRoomNoDBWithUserId:(NSInteger)userId andRoomNumber:(NSString*)roomNumber;
- (RoomTypeModel*)getRoomTypeByRoomNoWSWithUserId:(NSInteger)userId hotelId:(NSInteger)hotelId roomNumber:(NSString*)roomNumber;
@end






