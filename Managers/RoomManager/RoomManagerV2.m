
//
//  RoomManager.m
//  eHouseKeeping
//
//  Created by Khanh Nguyen on 08/06/2011.
//  Copyright 2011 TMA. All rights reserved.
//

#import "RoomManagerV2.h"
#import "GuestInfoModelV2.h"
#import "ehkConvert.h"
#import "RoomModelV2.h"
#import "ServiceDefines.h"
#import "GuestInfoAdapterV2.h"
#import "RoomManagerV2.h"
#import "GuestInfoManagerV2.h"
#import "CheckListManagerV2.h"
#import "CheckListModelV2.h"
#import "LogFileManager.h"
#import "LogObject.h"
#import "RoomAdapterV2.h"
#import "eHousekeepingService.h"
#import "TasksManagerV2.h"
//#import "HomeViewV2.h"
#import "DbDefinesV2.h"
#import "SupRoomAssignmentAdapterV2.h"
#import "DataBaseExtension.h"
#import "AdditionalJobManagerV2.h"
#import "NetworkCheck.h"
#import "ProfileNoteTypeV2.h"
#import "ProfileNoteV2.h"
#import "DeviceManager.h"
#import "ServiceDefines.h"
#import "PopupMessageItemModel.h"
#import "RoomBlocking.h"
#import "RoomManagerV2.h"
#import "RoomManagerV2Demo.h"
#import "RoomAssignmentAdapterV2.h"
#import "FindByRoomFloorViewV2.h"
#import "RoomTypeModel.h"


@implementation RoomManagerV2
#define dateTimeDefaultReturn  @"19000101:000000" //define value of date time return from server : 1900-01-01 00:00:00


//@synthesize adapter;
@synthesize roomAssignmentAdapter;
//@synthesize guestInfoAdapter;
@synthesize superRoomAdapter;
@synthesize roomAssignmentList;
@synthesize cleaningStatusAdapter;
//@synthesize locationAdapter;
@synthesize roomRecordAdapter;
@synthesize roomStatusAdapter;
@synthesize roomRemarkAdapter;
@synthesize isFindRoom;
static NSString * _currentRoomNo = @"";
static NSInteger _currentCleaningStatus = 0;

static RoomManagerV2* sharedRoomManagerInstance = nil;

+ (RoomManagerV2*) sharedRoomManager{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if(isDemoMode) {
            if (sharedRoomManagerInstance == nil || ![sharedRoomManagerInstance isKindOfClass:RoomManagerV2Demo.class]) {
                sharedRoomManagerInstance = [[RoomManagerV2Demo alloc] init];
            }
        } else {
            if (sharedRoomManagerInstance == nil || [sharedRoomManagerInstance isKindOfClass:RoomManagerV2Demo.class]) {
                sharedRoomManagerInstance = [[RoomManagerV2 alloc] init];
            }
        }
    });
    return sharedRoomManagerInstance;
}

+ (RoomManagerV2*) updateRoomManagerInstance{
    if(isDemoMode) {
        if (sharedRoomManagerInstance == nil || ![sharedRoomManagerInstance isKindOfClass:RoomManagerV2Demo.class]) {
            sharedRoomManagerInstance = [[RoomManagerV2Demo alloc] init];
        }
    } else {
        if (sharedRoomManagerInstance == nil || [sharedRoomManagerInstance isKindOfClass:RoomManagerV2Demo.class]) {
            sharedRoomManagerInstance = [[RoomManagerV2 alloc] init];
        }
    }
    return sharedRoomManagerInstance;
}

- (id)copyWithZone:(NSZone *)zone
{
    return self;
}

#pragma mark - Supervisor Filter
/*!
 CRF-1259: Display filter for Supervisor Home Screen
 */
-(int) getSupervisorFilterRoutineByUserId:(int)userId buildingId:(int)buildingId hotelId:(int)hotelId listRoomAssignmentCompare:(NSMutableArray*) listRoomCompare
{
    int returnCode = RESPONSE_STATUS_ERROR;
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole]) {
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_GetSupervisorFiltersRoutine *request = [[eHousekeepingService_GetSupervisorFiltersRoutine alloc] init];
    [request setUserID:[NSString stringWithFormat:@"%d", userId]];
    [request setBuildingID:[NSString stringWithFormat:@"%d", buildingId]];
    [request setHTID:[NSString stringWithFormat:@"%d", hotelId]];
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetSupervisorFiltersRoutineUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetSupervisorFiltersRoutineResponse class]]) {
            eHousekeepingService_GetSupervisorFiltersRoutineResponse *body = (eHousekeepingService_GetSupervisorFiltersRoutineResponse *)bodyPart;
            
            if ([body.GetSupervisorFiltersRoutineResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]]){
                returnCode = RESPONSE_STATUS_OK;
                
                [self deleteSupervisorFiltersRoutineByUserId:userId];
                
                NSMutableArray *listSupFilters = body.GetSupervisorFiltersRoutineResult.SupervisorFiltersListing.SupervisorFiltersDetails;
                
                
                for (eHousekeepingService_SupervisorFiltersDetails *curSupFilter in listSupFilters) {
                    for (NSString *curRoomAssignment in listRoomCompare) {
                        if([curRoomAssignment caseInsensitiveCompare:curSupFilter.RoomNo] == NSOrderedSame) {
                            SupervisorFilterDetail *curModel = [[SupervisorFilterDetail alloc] init];
                            curModel.filter_user_id = userId;
                            curModel.filter_type = [curSupFilter.Type intValue];
                            curModel.filter_id = [curSupFilter.ID_ intValue];
                            curModel.filter_name = curSupFilter.Name;
                            curModel.filter_lang = curSupFilter.Lang;
                            curModel.filter_hotel_id = [curSupFilter.HotelID intValue];
                            curModel.filter_room_number = curSupFilter.RoomNo;
                            [self insertSupervisorFiltersRoutine:curModel];
                            break;
                        }
                    }
                }
            }
        }
    }
    
    return returnCode;
}

-(int) updateRoomStatusByUserId:(int)userId roomNo:(NSString*)roomNo statusId:(int)statusId cleaningStatusId:(int)cleaningId{
    int returnCode = RESPONSE_STATUS_ERROR;
    eHousekeepingServiceSoapBinding *binding = [eHousekeepingService eHousekeepingServiceSoapBinding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole]) {
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_UpdateRoomStatus *request = [[eHousekeepingService_UpdateRoomStatus alloc] init];
    
    [request setIntUserID:[NSNumber numberWithInt:userId]];
    [request setStrRoomNo:roomNo];
    [request setIntStatusID:[NSNumber numberWithInt:statusId]];
    [request setIntCleaningStatusID:[NSNumber numberWithInt:cleaningId]];
    
    eHousekeepingServiceSoapBindingResponse *response = [binding UpdateRoomStatusUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_UpdateRoomStatusResponse class]]) {
            eHousekeepingService_UpdateRoomStatusResponse *body = (eHousekeepingService_UpdateRoomStatusResponse *)bodyPart;
            
            if ([body.UpdateRoomStatusResult.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]]){
                returnCode = RESPONSE_STATUS_OK;
                
//                [self deleteSupervisorFiltersRoutineByUserId:userId];
//                
//                NSMutableArray *listSupFilters = body.GetSupervisorFiltersRoutineResult.SupervisorFiltersListing.SupervisorFiltersDetails;
//                
//                
//                for (eHousekeepingService_SupervisorFiltersDetails *curSupFilter in listSupFilters) {
//                    for (NSString *curRoomAssignment in listRoomCompare) {
//                        if([curRoomAssignment caseInsensitiveCompare:curSupFilter.RoomNo] == NSOrderedSame) {
//                            SupervisorFilterDetail *curModel = [[SupervisorFilterDetail alloc] init];
//                            curModel.filter_user_id = userId;
//                            curModel.filter_type = [curSupFilter.Type intValue];
//                            curModel.filter_id = [curSupFilter.ID_ intValue];
//                            curModel.filter_name = curSupFilter.Name;
//                            curModel.filter_lang = curSupFilter.Lang;
//                            curModel.filter_hotel_id = [curSupFilter.HotelID intValue];
//                            curModel.filter_room_number = curSupFilter.RoomNo;
//                            [self insertSupervisorFiltersRoutine:curModel];
//                            break;
//                        }
//                    }
//                }
            }
        }
    }
    
    return returnCode;
}
#pragma mark - GuestInfo Methods

/**
 * update GuestInfo
 * call service get data
 * save data to database
 */

-(int) updateGuestInfoByUserId:(int)userId hotelId:(int)hotelId roomNumber:(NSString*)roomNumber{

    int returnCode = RESPONSE_STATUS_ERROR;
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole]) {
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_GetGuestInformation *request = [[eHousekeepingService_GetGuestInformation alloc] init];
    [request setIntHotelId:[NSNumber numberWithInt:hotelId]];
    [request setStrRoomNumber:roomNumber];
    
//    LogObject *logObj = [[LogObject alloc] init];
//    logObj.dateTime = [NSDate date];
//    logObj.log = [NSString stringWithFormat:@"<%i><updateGuestInfo><intUsrID:%i intRoomAssignID:%i>",UsrID,UsrID,raID];
//    LogFileManager *logManager = [[LogFileManager alloc] init];
//    [logManager save:logObj];
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetGuestInformationUsingParameters:request];
 
    NSArray *responseBodyParts = response.bodyParts;
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetGuestInformationResponse class]]) {
            eHousekeepingService_GetGuestInformationResponse *body = (eHousekeepingService_GetGuestInformationResponse *)bodyPart;
            
            if ([body.GetGuestInformationResult.mResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]]){
                
                returnCode = RESPONSE_STATUS_OK;
                
                eHousekeepingService_GuestRoomInformation *mGuestRoomInfo = body.GetGuestInformationResult.mGuestRoomInformation;
                NSMutableArray *listCurrrentGuests = body.GetGuestInformationResult.mCurrentGuestList.GuestInformation;
                NSMutableArray *listArrivalGuests = body.GetGuestInformationResult.mArrivalGuestList.GuestInformation;
                
                //Temporary Data Model for Insert
                GuestInfoModelV2 *guestInfoModel = [[GuestInfoModelV2 alloc] init];
                ProfileNoteTypeV2 *profileNoteType = [[ProfileNoteTypeV2 alloc] init];
                ProfileNoteV2 *profileNote = [[ProfileNoteV2 alloc] init];
                
                profileNoteType.profilenote_type_last_modified = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
                profileNote.profilenote_last_modified = profileNoteType.profilenote_type_last_modified;
                profileNoteType.profilenote_type_roomnumber = roomNumber;
                profileNote.profilenote_room_number = roomNumber;
                profileNoteType.profilenote_type_user_id = userId;
                profileNote.profilenote_user_id = userId;
                
                //Delete all of guests belong to this rooms
                [self deleteGuestByUserId:userId roomNumber:roomNumber guestType:GuestTypeArrival];
                [self deleteGuestByUserId:userId roomNumber:roomNumber guestType:GuestTypeCurrent];
                //Delete all of profile note belong to this rooms
                [self deleteProfileNoteByUserId:userId roomNumber:roomNumber];
                [self deleteProfileNoteTypeByUserId:userId roomNumber:roomNumber];
                
                for (eHousekeepingService_GuestInformation *curGuestWS in listCurrrentGuests) {
                    guestInfoModel.guestName = curGuestWS.strName;
                    guestInfoModel.guestRoomId = roomNumber;
                    guestInfoModel.guestLangPref = curGuestWS.strLangPreference;
                    if(curGuestWS.strCheckInDate.length > 0) {
                        guestInfoModel.guestCheckInTime = [ehkConvert DateToStringWithString:curGuestWS.strCheckInDate fromFormat:@"yyyy-MM-dd HH:mm:ss" toFormat:[ehkConvert getyyyyMMddhhmm]];
                    } else {
                        guestInfoModel.guestCheckInTime = @"";
                    }
                    
                    if(curGuestWS.strCheckOutDate.length > 0) {
                        guestInfoModel.guestCheckOutTime = [ehkConvert DateToStringWithString:curGuestWS.strCheckOutDate fromFormat:@"yyyy-MM-dd HH:mm:ss" toFormat:[ehkConvert getyyyyMMddhhmm]];
                    } else {
                        guestInfoModel.guestCheckOutTime = @"";
                    }
                    guestInfoModel.guestPostStatus = POST_STATUS_UN_CHANGED;
                    guestInfoModel.guestLastModified = curGuestWS.strLastModified;
                    guestInfoModel.guestImage = curGuestWS.binaryPhoto;
                    guestInfoModel.guestVIPCode = curGuestWS.strVIP;
                    guestInfoModel.guestHousekeeperName = mGuestRoomInfo.strHousekeeperName.length > 0 ? mGuestRoomInfo.strHousekeeperName : @"";
                    guestInfoModel.guestPostStatus = POST_STATUS_UN_CHANGED;
                    guestInfoModel.guestNumber = [NSString stringWithFormat:@"%d", [mGuestRoomInfo.intNoOfCurrentGuest intValue]];
                    guestInfoModel.guestType = GuestTypeCurrent;
                    //guestInfoModel.guestGuestRef = curGuestWS.strPreferenceDescription;
                    guestInfoModel.guestSpecialService = curGuestWS.strSpecialService;
                    guestInfoModel.guestPreferenceCode = curGuestWS.strPreferenceCodes;
                    guestInfoModel.guestId = [self insertGuestInfo:guestInfoModel];
                    
                    if(guestInfoModel.guestId > 0) {
                        NSMutableArray *listWSGuestProfileNote = curGuestWS.mGuestProfileNoteList.GuestProfileNoteDetail;
                        
                        for (eHousekeepingService_GuestProfileNoteDetail *curProfileNote in listWSGuestProfileNote) {
                            
                            if(curProfileNote.strTypeCode.length > 0
                               || curProfileNote.strNoteDescription.length > 0
                               || curProfileNote.strTypeDescription.length > 0) {
                                
                                profileNoteType.profilenote_type_code = curProfileNote.strTypeCode;
                                profileNoteType.profilenote_type_description = curProfileNote.strTypeDescription;
                                profileNoteType.profilenote_type_guest_id = guestInfoModel.guestId;
                                
                                profileNoteType.profilenote_type_id = [self insertProfileNoteType:profileNoteType];
                                
                                if(profileNoteType.profilenote_type_id > 0) {
                                    profileNote.profilenote_description = curProfileNote.strNoteDescription;
                                    profileNote.profilenote_guest_id = guestInfoModel.guestId;
                                    profileNote.profilenote_type_id = profileNoteType.profilenote_type_id;
                                    [self insertProfileNote:profileNote];
                                }
                            }
                        }

                    }
                }
                
                //Arrialval Guests in room
                for (eHousekeepingService_GuestInformation *curGuestWS in listArrivalGuests) {
                    guestInfoModel.guestName = curGuestWS.strName;
                    guestInfoModel.guestRoomId = roomNumber;
                    guestInfoModel.guestLangPref = curGuestWS.strLangPreference;
                    
                    if(curGuestWS.strCheckInDate.length > 0) {
                        guestInfoModel.guestCheckInTime = [ehkConvert DateToStringWithString:curGuestWS.strCheckInDate fromFormat:@"yyyy-MM-dd HH:mm:ss" toFormat:[ehkConvert getyyyyMMddhhmm]];
                    } else {
                        guestInfoModel.guestCheckInTime = @"";
                    }
                    
                    if(curGuestWS.strCheckOutDate.length > 0) {
                        guestInfoModel.guestCheckOutTime = [ehkConvert DateToStringWithString:curGuestWS.strCheckOutDate fromFormat:@"yyyy-MM-dd HH:mm:ss" toFormat:[ehkConvert getyyyyMMddhhmm]];
                    } else {
                        guestInfoModel.guestCheckOutTime = @"";
                    }
                    
                    guestInfoModel.guestPostStatus = POST_STATUS_UN_CHANGED;
                    guestInfoModel.guestLastModified = curGuestWS.strLastModified;
                    guestInfoModel.guestImage = curGuestWS.binaryPhoto;
                    guestInfoModel.guestVIPCode = curGuestWS.strVIP;
                    guestInfoModel.guestHousekeeperName = mGuestRoomInfo.strHousekeeperName;
                    guestInfoModel.guestPostStatus = POST_STATUS_UN_CHANGED;
                    guestInfoModel.guestNumber = [NSString stringWithFormat:@"%d", [mGuestRoomInfo.intNoOfArrivalGuest intValue]];
                    guestInfoModel.guestType = GuestTypeArrival;
                    //guestInfoModel.guestGuestRef = curGuestWS.strPreferenceDescription;
                    guestInfoModel.guestSpecialService = curGuestWS.strSpecialService;
                    guestInfoModel.guestPreferenceCode = curGuestWS.strPreferenceCodes;
                    guestInfoModel.guestId = [self insertGuestInfo:guestInfoModel];
                    
                    if(guestInfoModel.guestId > 0) {
                        NSMutableArray *listWSGuestProfileNote = curGuestWS.mGuestProfileNoteList.GuestProfileNoteDetail;
                        
                        for (eHousekeepingService_GuestProfileNoteDetail *curProfileNote in listWSGuestProfileNote) {
                            if(curProfileNote.strTypeCode.length > 0
                               || curProfileNote.strNoteDescription.length > 0
                               || curProfileNote.strTypeDescription) {
                                
                                profileNoteType.profilenote_type_code = curProfileNote.strTypeCode;
                                profileNoteType.profilenote_type_description = curProfileNote.strTypeDescription;
                                profileNoteType.profilenote_type_guest_id = guestInfoModel.guestId;
                                profileNoteType.profilenote_type_id = [self insertProfileNoteType:profileNoteType];
                                
                                if(profileNoteType.profilenote_type_id > 0) {
                                    profileNote.profilenote_description = curProfileNote.strNoteDescription;
                                    profileNote.profilenote_guest_id = guestInfoModel.guestId;
                                    profileNote.profilenote_type_id = profileNoteType.profilenote_type_id;
                                    [self insertProfileNote:profileNote];
                                }
                            }
                        }
                        
                    }
                }
                
            } else {
                if ([body.GetGuestInformationResult.mResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_NO_RERCORD_TO_DISPLAY]]) {
                    returnCode = RESPONSE_STATUS_OK;
                }
            }
        }
    }
    
    return returnCode;
}

/**
 check changed
 call soap post data
 */

-(BOOL)postGuestInfo:(NSInteger)UsrID WithRoomAssignID:(NSInteger)raID{
//#warning disable Post Guest Info
    //disable post guest info
    return YES;
    
    eHousekeepingServiceSoapBinding *binding = [eHousekeepingService eHousekeepingServiceSoapBinding];
    //    [binding setDefaultTimeout:60];
    
    //Allow write log or not
    if([LogFileManager isLogConsole]) {
        [binding setLogXMLInOut:YES];
    }
    eHousekeepingService_UpdateGuestInfo *request = [[eHousekeepingService_UpdateGuestInfo alloc] init] ;
    [request setIntUsrID:[NSNumber numberWithInt:UsrID]];
    [request setIntRoomAssignID:[NSNumber numberWithInt:raID]];
    
    RoomAssignmentModelV2 *raModel = [[RoomAssignmentModelV2 alloc] init];
    raModel.roomAssignment_Id = (int)raID;
    raModel.roomAssignment_UserId = (int)UsrID;
    [self load:raModel];
    RoomModelV2 *rModel = [[RoomModelV2 alloc] init];
    rModel.room_Id = raModel.roomAssignment_RoomId;

    [self loadRoomModel:rModel];
    
    GuestInfoModelV2 *gModel = [[GuestInfoModelV2 alloc] init];
    gModel.guestRoomId = rModel.room_Id;
    [[RoomManagerV2 sharedRoomManager] loadByRoomIDGuestInfo:gModel];
    //[gModel loadByRoomID];
    
    if (gModel.guestPostStatus != POST_STATUS_SAVED_UNPOSTED) {
 
        return YES;
    }
    
    
    [request setStrGuestPref:rModel.room_GuestReference];

    
    eHousekeepingServiceSoapBindingResponse *response = [binding UpdateGuestInfoUsingParameters:request];
//    NSArray *responseHeaders = response.headers;
//    for (id header in responseHeaders) {
//        
//    }
    
    NSArray *responseBody = response.bodyParts;
    for (id bodyPart in responseBody) {
        if ([bodyPart isKindOfClass:[SOAPFault class]]) {
            continue;
        }
        
         // UpdateGuestInfo
         
        if ([bodyPart isKindOfClass:[eHousekeepingService_UpdateGuestInfoResponse class]]) {
            eHousekeepingService_UpdateGuestInfoResponse *body = (eHousekeepingService_UpdateGuestInfoResponse *)bodyPart;
            
            if ([body.UpdateGuestInfoResult.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]]) {
                gModel.guestPostStatus = POST_STATUS_POSTED;
                //[[RoomManagerV2 sharedRoomManager] updateGuestInfo:gModel];
                //[gModel update];
                return YES;
            }
            
        }
    }
    return NO;
}


-(BOOL)postAllGuestInfoChanged{
    BOOL wasPostAll = YES;
    RoomAssignmentModelV2 *ramodel = [[RoomAssignmentModelV2 alloc] init];
    ramodel.roomAssignment_UserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    NSMutableArray *raArray = [[RoomManagerV2 sharedRoomManager] loadAllRoomAssignmentsByUserID:ramodel];
    for (RoomAssignmentModelV2 *raModel in raArray) {
        if ([self postGuestInfo:[UserManagerV2 sharedUserManager].currentUser.userId WithRoomAssignID:raModel.roomAssignment_Id] == NO) {
            wasPostAll = NO;
        }
    }
    return wasPostAll; // post all ok
}

#pragma mark -
#pragma mark RoomDetail Methods
/*
-(void)updateRoomDetail{
    eHousekeepingServiceSoapBinding *binding = [eHousekeepingService eHousekeepingServiceSoapBinding];
    //binding.logXMLInOut = YES;
    eHousekeepingService_GetRoomDetail *request = [[eHousekeepingService_GetRoomDetail alloc] init];
    [request setIntUsrID:[NSNumber numberWithInt:165]];
    [request setIntRoomAssignID:[NSNumber numberWithInt:1]];
    //[binding GetRoomDetailAsyncUsingParameters:request delegate:self];
    eHousekeepingServiceSoapBindingResponse *response = [binding GetRoomDetailUsingParameters:request];
   
    NSArray *responseHeaders = response.headers;
    for (id header in responseHeaders) {
        
    }
    NSArray *responseBodyParts = response.bodyParts;
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[SOAPFault class]]) {
            
            continue;
        }
        
         // GetRoomDetail
        /
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetRoomDetailResponse class]]) {
            eHousekeepingService_GetRoomDetailResponse *body = (eHousekeepingService_GetRoomDetailResponse *)bodyPart;
            //Now extract the RoomDetail from the response
            eHousekeepingService_RoomAssign *roomDetail = body.GetRoomDetailResult.RoomDetail;
            
            RoomModelV2 *roomModel = [[RoomModelV2 alloc] init];
            roomModel.room_Id = 165;
            roomModel.room_HotelId = [roomDetail.raRoomNo intValue];
            roomModel.room_StatusId = [roomDetail.raRoomStatusID intValue];
            roomModel.room_TypeId = [roomDetail.raRoomTypeID intValue];
            //roomModel.room_Lang = "lang";
            roomModel.room_LocationCode = [roomDetail.raFloorID intValue];
            roomModel.room_VIPStatusId = [roomDetail.raVIP intValue];
            roomModel.room_CleaningStatusId = [roomDetail.raCleaningStatusID intValue];
            roomModel.room_ExpectCleaningTime = roomDetail.raExpectedCleaningTime;
            roomModel.room_GuestReference = roomDetail.raGuestPreference;
            roomModel.room_AdditionalJob = roomDetail.raAddtionalJob;
            //roomModel.room_PostStatus
            //roomModel.room_LastModified = roomDetail.raLastModified;
            //roomModel.room_Building_Name=roomDetail.r
            //Update RoomModel
            if([self updateRoomModel:roomModel] == 0)
            {
                if([self insertRoomModel:roomModel] == 0)
                {
                    //                    NSLog(@"insert roomModel fail");
                }
                else
                {
                    //insert roomModel ok
                }
            }
            //NSLog(@"roomDetail: %@", roomDetail);
        }
    }
}
*/
/*
-(void)postCheckListInpect:(NSInteger)raID And:(RoomModelV2 *)rModel {
    CheckListModel *chkModel = [[CheckListModel alloc] init];
    chkModel.chklist_Room_Id = rModel.room_Id;
    chkModel.chklist_User_Id = [UserManager sharedUserManager].currentUser.user_Id;
    //[[ChecklistManager sharedChecklistManager] ];
    [[ChecklistManager sharedChecklistManager] loadByRoomIdAndUsrID:chkModel];
    //[chkModel loadByRoomIdAndUsrID];
    if (chkModel.chklist_Status == chkStatusNotCompleted) {
        return;
    }
    
    eHousekeepingServiceSoapBinding *binding = [eHousekeepingService eHousekeepingServiceSoapBinding];
    eHousekeepingService_UpdateRoomAssignmentSuper *request = [[eHousekeepingService_UpdateRoomAssignmentSuper alloc] init];
    
    [request setIntUsrID:[NSNumber numberWithInteger:[UserManager sharedUserManager].currentUser.user_Id]];
    [request setIntRoomAssignID:[NSNumber numberWithInteger:raID]];
    [request setIntRoomStatusID:[NSNumber numberWithInteger:rModel.room_StatusId]];
    
    [request setDtInspectedDt:[ehkConvert DateToStringWithString:chkModel.chklist_Inspect_Date fromFormat:@"yyyyMMdd:HHmmss" toFormat:@"yyyy-MM-dd HH:mm:ss"]];
    if (chkModel.chklist_Status == chkStatusPass) {
        [request setIntInspectedStatus:[NSNumber numberWithInteger:1]];
    } else {
        if (chkModel.chklist_Status == chkStatusFail) {
            [request setIntInspectedStatus:[NSNumber numberWithInteger:0]];
        }
    }
    
    [request setStrInspectedRmks:@""];
    
    eHousekeepingServiceSoapBindingResponse *response = [binding UpdateRoomAssignmentSuperUsingParameters:request];
    
       
    NSArray *responseBodyParts = response.bodyParts;
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[SOAPFault class]]) {
            continue;
        }
        
         // UpdateRoomAssignmentSuper
        
        if ([bodyPart isKindOfClass:[eHousekeepingService_UpdateRoomAssignmentSuperResponse class]]) {
            eHousekeepingService_UpdateRoomAssignmentSuperResponse *body = (eHousekeepingService_UpdateRoomAssignmentSuperResponse *)bodyPart;
            if ([body.UpdateRoomAssignmentSuperResult.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_ERROR_IN_UPDATING_RECORD]]) {
                //update RoomModel but cleaning status does not update
                rModel.room_PostStatus = POST_STATUS_POSTED;
                [self updateRoomModel:rModel];
                //                NSLog(@"post Check list ok -------------");
            }
        }
    }
}
*/
/*
-(void)postRoomDetailSuper:(RoomModelV2 *)rModel{
    CheckListModel *chkModel = [[CheckListModel alloc] init];
    chkModel.chklist_Room_Id = rModel.room_Id;
    chkModel.chklist_User_Id = [UserManager sharedUserManager].currentUser.user_Id;
    //[[ChecklistManager sharedChecklistManager] ];
    [[ChecklistManager sharedChecklistManager] loadByRoomIdAndUsrID:chkModel];
    //[chkModel loadByRoomIdAndUsrID];
    if (chkModel.chklist_Status == chkStatusNotCompleted) {
        return;
    }
    
    eHousekeepingServiceSoapBinding *binding = [eHousekeepingService eHousekeepingServiceSoapBinding];
    eHousekeepingService_UpdateRoomAssignmentSuper *request = [[eHousekeepingService_UpdateRoomAssignmentSuper alloc] init];
    
    [request setIntUsrID:[NSNumber numberWithInteger:[UserManager sharedUserManager].currentUser.user_Id]];
    [request setIntRoomAssignID:[NSNumber numberWithInteger:[TasksManager getCurrentRoomAssignment]]];
    [request setIntRoomStatusID:[NSNumber numberWithInteger:rModel.room_StatusId]];
    
    [request setDtInspectedDt:[ehkConvert DateToStringWithString:chkModel.chklist_Inspect_Date fromFormat:@"yyyyMMdd:HHmmss" toFormat:@"yyyy-MM-dd HH:mm:ss"]];
    if (chkModel.chklist_Status == chkStatusPass) {
        [request setIntInspectedStatus:[NSNumber numberWithInteger:1]];
    } else {
        if (chkModel.chklist_Status == chkStatusFail) {
            [request setIntInspectedStatus:[NSNumber numberWithInteger:0]];
        }
    }
    
    [request setStrInspectedRmks:@""];
    
    eHousekeepingServiceSoapBindingResponse *response = [binding UpdateRoomAssignmentSuperUsingParameters:request];
    
      
    NSArray *responseBodyParts = response.bodyParts;
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[SOAPFault class]]) {
            continue;
        }
        
        // UpdateRoomAssignmentSuper
         
        if ([bodyPart isKindOfClass:[eHousekeepingService_UpdateRoomAssignmentSuperResponse class]]) {
            eHousekeepingService_UpdateRoomAssignmentSuperResponse *body = (eHousekeepingService_UpdateRoomAssignmentSuperResponse *)bodyPart;
            if ([body.UpdateRoomAssignmentSuperResult.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_ERROR_IN_UPDATING_RECORD]]) {
                //update RoomModel but cleaning status does not update
                rModel.room_PostStatus = POST_STATUS_POSTED;
                [self updateRoomModel:rModel];
                //                NSLog(@"post Check list ok -------------");
            }
        }
    }
}
*/
/*
#pragma mark -
#pragma mark Cleaning Status Methods
-(BOOL)updateCleaningStatus
{
    BOOL returnCode = NO;
    
    eHousekeepingServiceSoapBinding *binding = [eHousekeepingService eHousekeepingServiceSoapBinding];
    //binding.logXMLInOut = YES;
    eHousekeepingService_GetCleaningStatusList *request = [[eHousekeepingService_GetCleaningStatusList alloc] init];
    eHousekeepingServiceSoapBindingResponse *response = [binding GetCleaningStatusListUsingParameters:request];

    // eHousekeepingService_CleaningStatus *cleaningStatusModelSVC=
    
    
    // eHousekeepingService_CleaningStatus *cleaningStatusModel=
    NSArray *responseBodyParts = response.bodyParts;
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[SOAPFault class]]) {
            
            continue;
        }
        
         // GetRoomDetail
        
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetCleaningStatusListResponse class]]) {
            eHousekeepingService_GetCleaningStatusListResponse *body = (eHousekeepingService_GetCleaningStatusListResponse *)bodyPart;
            if ([body.GetCleaningStatusListResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]]) {
                returnCode = YES;
            } else {
                returnCode = NO;
            }
            //Now extract the RoomDetail from the response
            NSMutableArray *cleaningStatusList=body.GetCleaningStatusListResult.CleaningStatusList.CleaningStatus;
            for (int i=0; i< [cleaningStatusList count]; i++) {
                eHousekeepingService_CleaningStatus *cleaningStatusModelSVC=[cleaningStatusList objectAtIndex:i];
                CleaningStatusModel *cleaningStatusModel=[[CleaningStatusModel alloc] init];
                cleaningStatusModel.cstat_Id=[cleaningStatusModelSVC.csID intValue];
                cleaningStatusModel.cstat_image=[self getImgCleaningStatus:cleaningStatusModel.cstat_Id];
                cleaningStatusModel.cstat_Name=cleaningStatusModelSVC.csName;
                cleaningStatusModel.cstat_Language=cleaningStatusModelSVC.csLang;
                
                //cleaningStatusModel.
                //if ([cleaningStatusModel insert] == 0) 
                if ([[RoomManager sharedRoomManager] insertCleaningStatus:cleaningStatusModel])
                {
                    //[cleaningStatusModel update];
                }
                

            }
            
        }
    }
    return returnCode;
}
 */
 
-(NSString*)getImgCleaningStatus:(int)cstatID
{
    switch (cstatID) 
    {
        case 1:
            return [NSString stringWithFormat:@"%@", dnd_icon] ;
            break;
        case 2:
            return [NSString stringWithFormat:@"%@", service_later]  ;
            break;
        case 3:
            return [NSString stringWithFormat:@"%@", reject_icon]  ;
            break;
        case 4:
            return [NSString stringWithFormat:@"%@", start_icon]  ;
            break;
        case 5:
            return [NSString stringWithFormat:@"%@", done_icon]  ;
            break;
        case 6:
            return [NSString stringWithFormat:@"%@", pause_icon] ;
            break;
        default:
            return [NSString stringWithFormat:@"%@", reject_icon];
    }
}
/*
-(BOOL)updateRoomStatus{
    BOOL returnCode = NO;
    
    eHousekeepingServiceSoapBinding *binding = [eHousekeepingService eHousekeepingServiceSoapBinding];
    //binding.logXMLInOut = YES;
    eHousekeepingService_GetRoomStatusList *request = [[eHousekeepingService_GetRoomStatusList alloc] init];
    eHousekeepingServiceSoapBindingResponse *response = [binding GetRoomStatusListUsingParameters:request];
    NSArray *responseBodyParts = response.bodyParts;
    for (id bodyPart in responseBodyParts) {
        
        // SOAP Fault Error
         
        if ([bodyPart isKindOfClass:[SOAPFault class]]) {
            // You can get the error like this:
            NSLog(@"error: %@", ((SOAPFault *)bodyPart).simpleFaultString);
        }
        
        
        // GetRoomStatusList
               
        if([bodyPart isKindOfClass:[eHousekeepingService_GetRoomStatusListResponse class]]) {
            eHousekeepingService_GetRoomStatusListResponse *body = (eHousekeepingService_GetRoomStatusListResponse *)bodyPart;
            
            if ([body.GetRoomStatusListResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]]) {
                returnCode = YES;
            } else {
                returnCode = NO;
            }
            
            // Now you can extract the RoomStatusArray from the response
            NSMutableArray *roomStatusArray = body.GetRoomStatusListResult.RoomStatusList.RoomStatus;
            for (eHousekeepingService_RoomStatus *roomStatus in roomStatusArray) {
                RoomStatusModelV2 *model = [[RoomStatusModelV2 alloc] init];
                model.rstat_Id = [roomStatus.rsID integerValue];
                model.rstat_Code = roomStatus.rsCode;
                model.rstat_Name = roomStatus.rsName;
                model.rstat_Lang = roomStatus.rsLang;
                model.rstat_Image = nil;
                model.rstat_LastModified = roomStatus.rsLastModifed;
                //NSLog(@"model: %@", model);
                if ([self insertRoomStatusData:model]) {
                    [self updateRoomStatusData:model];
                }
                
//                 if ([model insert] == 0) {
//                 [model update];
//                 }
             
            }
        }
    }

    
    return returnCode;
}
*/
#pragma mark - GetSetCurrentRoomNo

+(NSString*)getCurrentRoomNo{
    return _currentRoomNo;
}
+(void)setCurrentRoomNo:(NSString*)roomNo{
    _currentRoomNo = roomNo;
}
+(void)resetCurrentRoomNo{
    _currentRoomNo = 0;
}


#pragma mark - GetSetCurrentCleaningStatus

+(NSInteger)getCurrentCleaningStatus{
    return _currentCleaningStatus;
}
+(void)setCurrentCleaningStatus:(NSInteger)cleaningStatus{
    _currentCleaningStatus = cleaningStatus;
}
+(void)resetCurrentCleaningStatus{
    _currentCleaningStatus = 0;
}
#pragma mark -
#pragma mark eHouseKeepingDelegate Methods
/*
-(void)operation:(eHousekeepingServiceSoapBindingOperation *)operation completedWithResponse:(eHousekeepingServiceSoapBindingResponse *)response{
    NSArray *responseHeaders = response.headers;
    for (id header in responseHeaders) {
        // here do what you want with the headers, if there's anything of value in them
    }
    NSArray *responseBodyParts = response.bodyParts;
    for (id bodyPart in responseBodyParts) {
        
        // SOAP Fault Error
        
        if ([bodyPart isKindOfClass:[SOAPFault class]]) {
            // You can get the error like this:
            NSLog(@"error: %@", ((SOAPFault *)bodyPart).simpleFaultString);
            continue;
        }
        
       
        //  GetRoomStatusList
                 
        if([bodyPart isKindOfClass:[eHousekeepingService_GetRoomStatusListResponse class]]) {
            eHousekeepingService_GetRoomStatusListResponse *body = (eHousekeepingService_GetRoomStatusListResponse *)bodyPart;
            // Now you can extract the RoomStatusArray from the response
            NSMutableArray *roomStatusArray = body.GetRoomStatusListResult.RoomStatusList.RoomStatus;
            for (eHousekeepingService_RoomStatus *roomStatus in roomStatusArray) {
                RoomStatusModelV2 *model = [[RoomStatusModelV2 alloc] init];
                model.rstat_Id = [roomStatus.rsID integerValue];
                model.rstat_Code = roomStatus.rsCode;
                model.rstat_Name = roomStatus.rsName;              
                model.rstat_Lang = roomStatus.rsLang;
                model.rstat_Image = nil;
                model.rstat_LastModified = roomStatus.rsLastModifed;
                //                NSLog(@"model: %@", model);
                [self insertRoomStatusData:model];
                //[model insert];
              
            }
            continue;
        }
        
         // GetRoomDetail
         
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetRoomDetailResponse class]]) {
            //eHousekeepingService_GetRoomDetailResponse *body = (eHousekeepingService_GetRoomDetailResponse *)bodyPart;
            //Now extract the RoomDetail from the response
            //eHousekeepingService_RoomAssign *roomDetail = body.GetRoomDetailResult.RoomDetail;
            
        }
       
         // GetGuestInfo
         
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetGuestInfoResponse class]]) {
            eHousekeepingService_GetGuestInfoResponse *body = (eHousekeepingService_GetGuestInfoResponse *)bodyPart;
            
            eHousekeepingService_Guest *guest = body.GetGuestInfoResult.GuestInfo;
            GuestInfoModel *guestInfoModel = [[GuestInfoModel alloc] init];
            //guestInfoModel.guest_Id
            guestInfoModel.guest_Name = guest.gpName;
            //guestInfoModel.guest_Room_Id
            guestInfoModel.guest_Lang_Pref = guest.gpLangPref;
            guestInfoModel.guest_Check_In_Time = [ehkConvert DateToStringWithString:guest.gpCheckInDt fromFormat:@"yyyy-MM-dd HH:mm" toFormat:[ehkConvert getyyyyMMddhhmm]];
            guestInfoModel.guest_Check_Out_Time = [ehkConvert DateToStringWithString:guest.gpCheckOutDt fromFormat:@"yyyy-MM-dd HH:mm" toFormat:[ehkConvert getyyyyMMddhhmm]];
            //guestInfoModel.guest_Post_Status
            //guestInfoModel.guest_Last_Modified
            [[RoomManager sharedRoomManager] insertGuestInfo:guestInfoModel];
            //[guestInfoModel insert];
        }
    }
}
*/
-(void)updateLocation{
    
}
#pragma mark
#pragma mark Function for cleaning status update .

/**
 * getCleaningStatus
 * Date : 
 *
 * @return list all of Cleaning status.
 *
 
 * @author Chinh.Bui <>
 * @since 
 */
-(NSMutableArray*)getCleaningStatus
{
    
    //CleaningStatusModel *cleaningStatusModel=[[CleaningStatusModel alloc] init] ;
    //NSMutableArray *array = [[cleaningStatusModel loadAll] retain];
    NSMutableArray *array = [NSMutableArray array];
    [array setArray: [self loadAllCleaningStatus]];
    //[cleaningStatusModel release];
    
    return array;
    // return [[cleaningStatusModel loadAll] retain];
}


// @ this function for update start time and finished time
-(void)upDateTimeRoomRecordModel:(int)cleaning_status_id
{
    RoomRecordModelV2 *rRecordModel=[[RoomRecordModelV2 alloc] init];
    rRecordModel.rrec_room_assignment_id = (int)[TasksManagerV2 getCurrentRoomAssignment];
    rRecordModel.rrec_User_Id=[UserManagerV2 sharedUserManager].currentUser.userId;
    [self loadRoomRecordDataByRoomIdAndUserId:rRecordModel];
    
    if (rRecordModel.rrec_Id == 0) {
        [self insertRoomRecordData:rRecordModel];
    }
    
    // check if user start , and date tine is not default then return 
    if(cleaning_status_id == 4 && ![rRecordModel.rrec_Cleaning_Start_Time isEqualToString:dateTimeDefaultReturn] )
    {
        return;
    }
    switch (cleaning_status_id) {
        case 4:     // case start  update Cleaning Start time.
        {
            rRecordModel.rrec_Cleaning_Start_Time=[DateTimeUtility getDateTimeNow];
        }
            break;
        case 5:     // case finished -> update cleaning End time  .
        {
            rRecordModel.rrec_Cleaning_End_Time=[DateTimeUtility getDateTimeNow];
        }
            break;
            
        default:
            break;
    }
    
    
    if(rRecordModel.rrec_Id==0)
    {
        [self insertRoomRecordData:rRecordModel];
    }
    else
    {  
        [self updateRoomRecordData:rRecordModel];
    }

}

#pragma mark - Supervisor Filter Database

-(NSInteger) insertSupervisorFiltersRoutine:(SupervisorFilterDetail*)model
{
    if(model == nil){
        return 0;
    }
    
    DataRow *rowInsert = [[DataRow alloc] init];
    rowInsert.tableName = [NSString stringWithFormat:@"%@", SUPERVISOR_FILTER_DETAIL];
    
    [rowInsert addColumnIntValue:model.filter_user_id formatName:@"%@", filter_user_id];
    [rowInsert addColumnIntValue:model.filter_type formatName:@"%@", filter_type];
    [rowInsert addColumnIntValue:model.filter_id formatName:@"%@", filter_id];
    [rowInsert addColumnStringValue:model.filter_name formatName:@"%@", filter_name];
    [rowInsert addColumnStringValue:model.filter_lang formatName:@"%@", filter_lang];
    [rowInsert addColumnStringValue:model.filter_room_number formatName:@"%@", filter_room_number];
    [rowInsert addColumnIntValue:model.filter_hotel_id formatName:@"%@", filter_hotel_id];
    
    return [rowInsert insertDBRow];
}

-(NSInteger) deleteSupervisorFiltersRoutineByUserId:(int)userId
{
    DataTable *tableDelete = [[DataTable alloc] initWithNameFormat:@"%@",SUPERVISOR_FILTER_DETAIL];
    
    NSString *queryDelete = [NSString stringWithFormat:@"Delete from %@ where %@ = %d", SUPERVISOR_FILTER_DETAIL, filter_user_id, userId];
    int status = (int)[tableDelete excuteQueryNonSelect:queryDelete];
    
    return status;
}

-(NSMutableArray*) loadSupervisorFiltersRoutineByUserId:(int)userId
{
    if(userId <= 0){
        return nil;
    }
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"select %@, %@, %@, %@, %@, %@, %@ from %@ where %@ = %d",
                                  
                                  //Select
                                  filter_user_id,
                                  filter_type,
                                  filter_id,
                                  filter_name,
                                  filter_lang,
                                  filter_room_number,
                                  filter_hotel_id,
                                  
                                  //from
                                  SUPERVISOR_FILTER_DETAIL,
                                  
                                  //where
                                  filter_user_id,
                                  userId
                                  ];
    
    DataTable *table = [[DataTable alloc] init];
    
    //Row result reference
    DataRow *rowReference = [[DataRow alloc] init];
    [rowReference addColumnFormatInt:@"%@", filter_user_id];
    [rowReference addColumnFormatInt:@"%@", filter_type];
    [rowReference addColumnFormatInt:@"%@", filter_id];
    [rowReference addColumnFormatString:@"%@", filter_name];
    [rowReference addColumnFormatString:@"%@", filter_lang];
    [rowReference addColumnFormatString:@"%@", filter_room_number];
    [rowReference addColumnFormatInt:@"%@", filter_hotel_id];
    
    [table loadTableDataWithQuery:sqlString referenceRow:rowReference];
    
    int rowCount = [table countRows];
    NSMutableArray *results = nil;
    if(rowCount > 0)
    {
        results = [NSMutableArray array];
        for (int i = 0; i < rowCount; i ++) {
            
            SupervisorFilterDetail *model = [[SupervisorFilterDetail alloc] init];
            DataRow *row = [table rowWithIndex:i];
            DataColumn *curColumn = nil;
            
            curColumn = [row columnWithNameFormat:@"%@", filter_user_id];
            model.filter_user_id = [curColumn fieldValueInt];
            
            curColumn = [row columnWithNameFormat:@"%@", filter_type];
            model.filter_type = [curColumn fieldValueInt];
            
            curColumn = [row columnWithNameFormat:@"%@", filter_id];
            model.filter_id = [curColumn fieldValueInt];
            
            curColumn = [row columnWithNameFormat:@"%@", filter_name];
            model.filter_name = [curColumn fieldValueString];
            
            curColumn = [row columnWithNameFormat:@"%@", filter_lang];
            model.filter_lang = [curColumn fieldValueString];
            
            curColumn = [row columnWithNameFormat:@"%@", filter_room_number];
            model.filter_room_number = [curColumn fieldValueString];
            
            curColumn = [row columnWithNameFormat:@"%@", filter_hotel_id];
            model.filter_hotel_id = [curColumn fieldValueInt];
            
            [results addObject:model];
        }
    }
    
    return results;
}

-(NSMutableArray*) loadSupervisorFiltersRoutineByUserId:(int)userId typeId:(int)typeId
{
    if(userId <= 0){
        return nil;
    }
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"select %@, %@, %@, %@, %@, %@, %@ from %@ where %@ = %d and %@ = %d group by %@, %@",
                                  
                                  //Select
                                  filter_user_id,
                                  filter_type,
                                  filter_id,
                                  filter_name,
                                  filter_lang,
                                  filter_room_number,
                                  filter_hotel_id,
                                  
                                  //from
                                  SUPERVISOR_FILTER_DETAIL,
                                  
                                  //where
                                  filter_user_id,
                                  userId,
                                  
                                  //and
                                  filter_type,
                                  typeId,
                                  
                                  //group by
                                  filter_type,
                                  filter_id
                                  ];
    
    DataTable *table = [[DataTable alloc] init];
    
    //Row result reference
    DataRow *rowReference = [[DataRow alloc] init];
    [rowReference addColumnFormatInt:@"%@", filter_user_id];
    [rowReference addColumnFormatInt:@"%@", filter_type];
    [rowReference addColumnFormatInt:@"%@", filter_id];
    [rowReference addColumnFormatString:@"%@", filter_name];
    [rowReference addColumnFormatString:@"%@", filter_lang];
    [rowReference addColumnFormatString:@"%@", filter_room_number];
    [rowReference addColumnFormatInt:@"%@", filter_hotel_id];
    
    [table loadTableDataWithQuery:sqlString referenceRow:rowReference];
    
    int rowCount = [table countRows];
    NSMutableArray *results = nil;
    if(rowCount > 0)
    {
        results = [NSMutableArray array];
        for (int i = 0; i < rowCount; i ++) {
            
            SupervisorFilterDetail *model = [[SupervisorFilterDetail alloc] init];
            DataRow *row = [table rowWithIndex:i];
            DataColumn *curColumn = nil;
            
            curColumn = [row columnWithNameFormat:@"%@", filter_user_id];
            model.filter_user_id = [curColumn fieldValueInt];
            
            curColumn = [row columnWithNameFormat:@"%@", filter_type];
            model.filter_type = [curColumn fieldValueInt];
            
            curColumn = [row columnWithNameFormat:@"%@", filter_id];
            model.filter_id = [curColumn fieldValueInt];
            
            curColumn = [row columnWithNameFormat:@"%@", filter_name];
            model.filter_name = [curColumn fieldValueString];
            
            curColumn = [row columnWithNameFormat:@"%@", filter_lang];
            model.filter_lang = [curColumn fieldValueString];
            
            curColumn = [row columnWithNameFormat:@"%@", filter_room_number];
            model.filter_room_number = [curColumn fieldValueString];
            
            curColumn = [row columnWithNameFormat:@"%@", filter_hotel_id];
            model.filter_hotel_id = [curColumn fieldValueInt];
            
            [results addObject:model];
        }
    }
    
    return results;
}

-(int) countSupervisorFiltersRoutineByUserId:(int)userId
{
    if(userId <= 0){
        return nil;
    }
    
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"select count(1) from %@ where %@ = %d",
                                  //count
                                  
                                  //From
                                  SUPERVISOR_FILTER_DETAIL,
                                  
                                  //where
                                  filter_user_id,
                                  userId];
    
    DataTable *table = [[DataTable alloc] init];
    
    //Row result reference
    DataRow *rowReference = [[DataRow alloc] init];
    [rowReference addColumnFormatInt:@"CountResult"];
    [table loadTableDataWithQuery:sqlString referenceRow:rowReference];
    
    int rowCount = [table countRows];
    int countResult = 0;
    if(rowCount > 0) {
        DataRow *row = [table rowWithIndex:0];
        countResult = [row intValueWithColumnNameFormat:@"CountResult"];
    }
    
    return countResult;
}

#pragma mark -  insert, update, delete for room assignment

-(NSInteger) insert:(RoomAssignmentModelV2*)roomAssignment
{
    
    roomAssignment.roomAssignment_LastModified = [[HomeViewV2 shareHomeView] getTempLastModifiedRoomAssignment];
    
    RoomAssignmentAdapterV2 *roomAssignmentAdapter1 = [RoomAssignmentAdapterV2 createRoomAssignmentAdapter];
    [roomAssignmentAdapter1 openDatabase];
    [roomAssignmentAdapter1 resetSqlCommand];
    int result = [roomAssignmentAdapter1 insertRoomAssignmentData:roomAssignment];
    [roomAssignmentAdapter1 close];
    
    roomAssignmentAdapter1 = nil;
    return result;
}
-(NSInteger) update:(RoomAssignmentModelV2*)roomAssignment
{
    /*
     if (roomAssignmentAdapter == nil) {
     roomAssignmentAdapter = [[RoomAssignmentAdapter alloc] init];
     [roomAssignmentAdapter openDatabase];
     [roomAssignmentAdapter resetSqlCommand];
     int result = [roomAssignmentAdapter updateRoomAssignmentData:roomAssignment];
     [roomAssignmentAdapter close];
     [roomAssignmentAdapter release];
     roomAssignmentAdapter = nil;
     return result;
     } else {
     [roomAssignmentAdapter openDatabase];
     [roomAssignmentAdapter resetSqlCommand];
     int result = [roomAssignmentAdapter updateRoomAssignmentData:roomAssignment];
     [roomAssignmentAdapter close];
     return result;
     }*/
    RoomAssignmentAdapterV2 *roomAssignmentAdapter1 = [RoomAssignmentAdapterV2 createRoomAssignmentAdapter];
    [roomAssignmentAdapter1 openDatabase];
    [roomAssignmentAdapter1 resetSqlCommand];
    int result = [roomAssignmentAdapter1 updateRoomAssignmentData:roomAssignment];
    [roomAssignmentAdapter1 close];
 
    roomAssignmentAdapter1 = nil;
    return result;
}

-(NSInteger) delete:(RoomAssignmentModelV2*)roomAssignment
{
    /*
     if (roomAssignmentAdapter == nil) {
     roomAssignmentAdapter = [[RoomAssignmentAdapter alloc] init];
     [roomAssignmentAdapter openDatabase];
     [roomAssignmentAdapter resetSqlCommand];
     int result = [roomAssignmentAdapter deleteRoomAssignmentData:roomAssignment];
     [roomAssignmentAdapter close];
     [roomAssignmentAdapter release];
     roomAssignmentAdapter = nil;
     return result;
     } else {
     [roomAssignmentAdapter openDatabase];
     [roomAssignmentAdapter resetSqlCommand];
     int result = [roomAssignmentAdapter deleteRoomAssignmentData:roomAssignment];
     [roomAssignmentAdapter close];
     return result;
     }*/
    RoomAssignmentAdapterV2 *roomAssignmentAdapter1 = [RoomAssignmentAdapterV2 createRoomAssignmentAdapter];
    [roomAssignmentAdapter1 openDatabase];
    [roomAssignmentAdapter1 resetSqlCommand];
    int result = [roomAssignmentAdapter1 deleteRoomAssignmentData:roomAssignment];
    [roomAssignmentAdapter1 close];

    roomAssignmentAdapter1 = nil;
    return result;
}
-(void) load:(RoomAssignmentModelV2*)roomAssignment
{
    /*
     if (roomAssignmentAdapter == nil) {
     roomAssignmentAdapter = [[RoomAssignmentAdapter alloc] init];
     [roomAssignmentAdapter openDatabase];
     [roomAssignmentAdapter resetSqlCommand];
     [roomAssignmentAdapter loadRoomAsignment:roomAssignment];
     [roomAssignmentAdapter close];
     [roomAssignmentAdapter release];
     roomAssignmentAdapter = nil;
     } else {
     [roomAssignmentAdapter openDatabase];
     [roomAssignmentAdapter resetSqlCommand];
     [roomAssignmentAdapter loadRoomAsignment:roomAssignment];
     [roomAssignmentAdapter close];
     }*/
    
    RoomAssignmentAdapterV2 *roomAssignmentAdapter1 = [RoomAssignmentAdapterV2 createRoomAssignmentAdapter];
    [roomAssignmentAdapter1 openDatabase];
    [roomAssignmentAdapter1 resetSqlCommand];
    [roomAssignmentAdapter1 loadRoomAsignment:roomAssignment];
    [roomAssignmentAdapter1 close];
 
    roomAssignmentAdapter1 = nil;
    
}

-(NSMutableArray *)loadAllRoomAssignmentsByUserId:(NSInteger)userId {
    RoomAssignmentAdapterV2 *radapter = [RoomAssignmentAdapterV2 createRoomAssignmentAdapter];
    [radapter openDatabase];
    NSMutableArray *results = [radapter loadRoomAssignmentsByUserId:userId];
    [radapter close];
    return results;
}

//HaoTran[20130619/ Get last date modified]
-(NSString*)getRoomAssignmentLastModifiedDateByUserId:(int)userId
{
    RoomAssignmentAdapterV2 *adapter1 = [RoomAssignmentAdapterV2 createRoomAssignmentAdapter];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    NSString *result = nil;
    result = [adapter1 getRoomAssignmentLastModifiedDateByUserId:userId];
    [adapter1 close];
    adapter1 = nil;
    return result;
}
//HaoTran[20130619/ Get last date modified] - END

//Any change query in this function, you should change "-(int)countAllRoomAssignmentsByUserID:(int)userId" too
-(NSMutableArray*)loadAllRoomAssignmentsByUserID:(RoomAssignmentModelV2*)roomAssignment
{
    /*
     if (roomAssignmentAdapter == nil) {
     roomAssignmentAdapter = [[RoomAssignmentAdapter alloc] init];
     [roomAssignmentAdapter openDatabase];
     [roomAssignmentAdapter resetSqlCommand];
     NSMutableArray* result = [roomAssignmentAdapter loadRoomAsignmentByUserID:roomAssignment];
     [roomAssignmentAdapter close];
     [roomAssignmentAdapter release];
     roomAssignmentAdapter = nil;
     return result;
     } else {
     [roomAssignmentAdapter openDatabase];
     [roomAssignmentAdapter resetSqlCommand];
     NSMutableArray* result = [roomAssignmentAdapter loadRoomAsignmentByUserID:roomAssignment];
     [roomAssignmentAdapter close];
     return result;
     }*/
    RoomAssignmentAdapterV2 *roomAssignmentAdapter1 = [RoomAssignmentAdapterV2 createRoomAssignmentAdapter];
    [roomAssignmentAdapter1 openDatabase];
    [roomAssignmentAdapter1 resetSqlCommand];
    NSMutableArray* result = [roomAssignmentAdapter1 loadRoomAsignmentByUserID:roomAssignment];
    [roomAssignmentAdapter1 close];
    
    roomAssignmentAdapter1 = nil;
    return result;
}

//Any change query in this function, you should change "-(NSMutableArray*)loadAllRoomAssignmentsByUserID:(RoomAssignmentModelV2*)roomAssignment" too
-(int)countAllRoomAssignmentsByUserID:(int)userId
{
    NSMutableString *sqlQueryCount = [[NSMutableString alloc] initWithFormat:@"select count(1) from %@ where %@ = %d AND %@ = 0",
                                      //count
                                      
                                      ROOM_ASSIGNMENT,
                                      ra_user_id,
                                      userId,
                                      
                                      ra_delete];
    
    DataTable *tableCount = [[DataTable alloc] init];
    
    //Row result reference
    DataRow *rowReference = [[DataRow alloc] init];
    [rowReference addColumnFormatInt:@"CountResult"];
    
    [tableCount loadTableDataWithQuery:sqlQueryCount referenceRow:rowReference];
    
    int countResult = 0;
    int rowCount = [tableCount countRows];
    if(rowCount > 0) {
        DataRow *row = [tableCount rowWithIndex:0];
        countResult = [row intValueWithColumnNameFormat:@"CountResult"];
    }
    
    return countResult;
}

-(bool)getRoomAssignmentByUserIDAndRoomID:(RoomAssignmentModelV2*)roomAssignment
{
    /*
     if (roomAssignmentAdapter == nil) {
     roomAssignmentAdapter = [[RoomAssignmentAdapter alloc] init];
     [roomAssignmentAdapter openDatabase];
     [roomAssignmentAdapter resetSqlCommand];
     bool result = [roomAssignmentAdapter getRoomAssignmentByUserIDAndRoomId:roomAssignment];
     [roomAssignmentAdapter close];
     [roomAssignmentAdapter release];
     roomAssignmentAdapter = nil;
     return result;
     } else {
     [roomAssignmentAdapter openDatabase];
     [roomAssignmentAdapter resetSqlCommand];
     bool result = [roomAssignmentAdapter getRoomAssignmentByUserIDAndRoomId:roomAssignment];
     [roomAssignmentAdapter close];
     return result;
     }*/
    RoomAssignmentAdapterV2 *roomAssignmentAdapter1 = [RoomAssignmentAdapterV2 createRoomAssignmentAdapter];
    [roomAssignmentAdapter1 openDatabase];
    [roomAssignmentAdapter1 resetSqlCommand];
    bool result = [roomAssignmentAdapter1 getRoomAssignmentByUserIDAndRoomId:roomAssignment];
    [roomAssignmentAdapter1 close];

    roomAssignmentAdapter1 = nil;
    return result;
    
}

-(bool)getRoomIdByRoomAssignment:(RoomAssignmentModelV2*)roomAssignment
{
    /*
     if (roomAssignmentAdapter == nil) {
     roomAssignmentAdapter = [[RoomAssignmentAdapter alloc] init];
     [roomAssignmentAdapter openDatabase];
     [roomAssignmentAdapter resetSqlCommand];
     bool result = [roomAssignmentAdapter getRoomAssignmentByUserIDAndRoomId:roomAssignment];
     [roomAssignmentAdapter close];
     [roomAssignmentAdapter release];
     roomAssignmentAdapter = nil;
     return result;
     } else {
     [roomAssignmentAdapter openDatabase];
     [roomAssignmentAdapter resetSqlCommand];
     bool result = [roomAssignmentAdapter getRoomAssignmentByUserIDAndRoomId:roomAssignment];
     [roomAssignmentAdapter close];
     return result;
     }*/
    RoomAssignmentAdapterV2 *roomAssignmentAdapter1 = [RoomAssignmentAdapterV2 createRoomAssignmentAdapter];
    [roomAssignmentAdapter1 openDatabase];
    [roomAssignmentAdapter1 resetSqlCommand];
    bool result = [roomAssignmentAdapter1 getRoomIdByRoomAssignment:roomAssignment];
    [roomAssignmentAdapter1 close];
    
    roomAssignmentAdapter1 = nil;
    return result;
    
}

//An Rework for ReassignRoom
-(void)reassignRoom:(RoomModelV2*) room andRoomAssignmentModelV2:(RoomAssignmentModelV2*) roomAssigns
{
    RoomAdapterV2 *roomadapter = [RoomAdapterV2 createRoomAdapter];
    [roomadapter openDatabase];
    [roomadapter resetSqlCommand];
    [roomadapter updateRoomModelReassign:room];
    
    RoomAssignmentAdapterV2 *roomAssignmentAdapter1 = [RoomAssignmentAdapterV2 createRoomAssignmentAdapter];
    [roomAssignmentAdapter1 openDatabase];
    [roomAssignmentAdapter1 resetSqlCommand];
    [roomAssignmentAdapter1 updateClleaningStatusRoomAssignmentModel:roomAssigns];
    
    
}
-(void)loadRoomAsignmentByRoomIdAndUserID:(RoomAssignmentModelV2*)roomAssignment
{
    /*
     if (roomAssignmentAdapter == nil) {
     roomAssignmentAdapter = [[RoomAssignmentAdapter alloc] init];
     [roomAssignmentAdapter openDatabase];
     [roomAssignmentAdapter resetSqlCommand];
     [roomAssignmentAdapter loadRoomAsignmentByRoomIdAndUserID:roomAssignment];
     [roomAssignmentAdapter close];
     [roomAssignmentAdapter release];
     roomAssignmentAdapter = nil;
     } else {
     [roomAssignmentAdapter openDatabase];
     [roomAssignmentAdapter resetSqlCommand];
     [roomAssignmentAdapter loadRoomAsignmentByRoomIdAndUserID:roomAssignment];
     [roomAssignmentAdapter close];
     }*/
    RoomAssignmentAdapterV2 *roomAssignmentAdapter1 = [RoomAssignmentAdapterV2 createRoomAssignmentAdapter];
    [roomAssignmentAdapter1 openDatabase];
    [roomAssignmentAdapter1 resetSqlCommand];
    [roomAssignmentAdapter1 loadRoomAsignmentByRoomIdAndUserID:roomAssignment];
    [roomAssignmentAdapter1 close];

    roomAssignmentAdapter1 = nil;
    
}

-(NSInteger)loadMaxPrioprity;{
    
    RoomAssignmentAdapterV2 *roomAssignmentAdapter1 = [RoomAssignmentAdapterV2 createRoomAssignmentAdapter];
    [roomAssignmentAdapter1 openDatabase];
    [roomAssignmentAdapter1 resetSqlCommand];
    NSInteger result = [roomAssignmentAdapter1 maxPrioprity];
    [roomAssignmentAdapter1 close];
    
    roomAssignmentAdapter1 = nil;
    
    return result;
}

-(NSInteger) loadMinPrioprity;
{
    RoomAssignmentAdapterV2 *roomAssignmentAdtr = [RoomAssignmentAdapterV2 createRoomAssignmentAdapter];
    [roomAssignmentAdtr openDatabase];
    [roomAssignmentAdtr resetSqlCommand];
    NSInteger result = [roomAssignmentAdtr minPrioprity];
    
    roomAssignmentAdtr = nil;
    
    return result;
    
}

-(void)checkAndRemoveMissingRoomAssignments:(NSMutableArray *)roomAssigns andUserId:(NSInteger) userId{
    
    NSMutableArray *raModels = [self getAllRoomAssignmentsWithUserId:userId];
    
    NSMutableArray *deleteArray = [NSMutableArray array];
    
    BOOL isRemoved = YES;
    
    for (RoomAssignmentModelV2 *ramodel in raModels) {
        
        isRemoved = YES;
        
        for (NSInteger index = 0 ; index < roomAssigns.count; index ++) {
            eHousekeepingService_RoomAssign *raassign = [roomAssigns objectAtIndex:index];
            if ([raassign.raID integerValue] == ramodel.roomAssignment_Id) {
                isRemoved = NO;
                
                //remove delete Key for Room Assignment Id
                [self setDeleteKey:UNDELETE_ROOM_ASSIGNMENT_FLAG forRoomAssignmentId:ramodel.roomAssignment_Id andUserId:userId];
                
                break;
            }
        }
        
        if (isRemoved == YES) {
            [deleteArray addObject:ramodel];
        }
    }
    
    //set delete key for all object in deleteArray
    for (RoomAssignmentModelV2 *ramodel in deleteArray) {
        [self setDeleteKey:DELETE_ROOM_ASSIGNMENT_FLAG forRoomAssignmentId:ramodel.roomAssignment_Id andUserId:userId];
    }
}

-(int)getRoomTypeListWSWithHotelId:(NSInteger)hotelId
{
    int returnCode = RESPONSE_STATUS_OK;
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole]) {
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_GetRoomTypeList *request = [[eHousekeepingService_GetRoomTypeList alloc] init];
    [request setIntHotelID:[NSNumber numberWithInteger:hotelId]];
    
    //    LogObject *logObj = [[LogObject alloc] init];
    //    logObj.dateTime = [NSDate date];
    //    logObj.log = [NSString stringWithFormat:@"<><getRoomTypeListWSWithHotelId><intHotelID:%i lastModified:%@>",hotelId,roomTypeLatest.amsLastModified];
    //    LogFileManager *logManager = [[LogFileManager alloc] init];
    //    [logManager save:logObj];
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetRoomTypeListUsingParameters:request];
    NSArray *responseBodyParts = response.bodyParts;
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[SOAPFault class]]) {
            continue;
        }
        
        //Get Room Type List
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetRoomTypeListResponse class]]) {
            eHousekeepingService_GetRoomTypeListResponse *body = (eHousekeepingService_GetRoomTypeListResponse *) bodyPart;
            if ([body.GetRoomTypeListResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]]) {
                
                NSMutableArray *arrayRoomTypeWS = body.GetRoomTypeListResult.RoomTypeList.RoomType;
                for (eHousekeepingService_RoomType *roomTypeWS in arrayRoomTypeWS) {
                    
                    //Get RoomType form database
                    RoomTypeModel *roomTypeModel = [[RoomTypeModel alloc] init];
                    roomTypeModel.amsId = [roomTypeWS.rtID integerValue];
                    [self loadRoomTypeModelByPrimaryKey:roomTypeModel];
                    
                    if (roomTypeModel.amsName != nil) {
                        
                        //update room type
                        roomTypeModel.amsName = roomTypeWS.rtName;
                        roomTypeModel.amsNameLang = roomTypeWS.rtLang;
                        //roomType.amsImage = UIImagePNGRepresentation([UIImage imageNamed:@"Executive_Floor.png"]);
                        roomTypeModel.amsLastModified = roomTypeWS.rtLastModifed;
                        [self updateRoomTypeModel:roomTypeModel];
                        
                    } else {
                        
                        //insert room type
                        roomTypeModel.amsId = [roomTypeWS.rtID integerValue];
                        //roomTypeDB.amsImage = UIImagePNGRepresentation([UIImage imageNamed:@"Executive_Floor.png"]);
                        roomTypeModel.amsLastModified = roomTypeWS.rtLastModifed;
                        roomTypeModel.amsName = roomTypeWS.rtName;
                        roomTypeModel.amsNameLang = roomTypeWS.rtLang;
                        [self insertRoomTypeModel:roomTypeModel];
                    }
                }
                
                returnCode = RESPONSE_STATUS_OK;
            } else {
                
                returnCode = RESPONSE_STATUS_ERROR;
            }
        }
    }
    return returnCode;
}

-(BOOL)getRoomAssignmentWSByUserID:(NSInteger)userID AndIsSupervisor:(BOOL)isSupervisor AndLastModified:(NSString *)strLastModified AndPercentView:(MBProgressHUD *)percentView{
    
    NSMutableArray *listRoomsCompare = nil;
    if (![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        return NO;
    }
    BOOL result = NO;
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole]){
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_GetRoomAssignmentList *request = [[eHousekeepingService_GetRoomAssignmentList alloc] init];
        
    [request setIntUsrID:[NSNumber numberWithInteger:userID]];
    if (strLastModified) {
        [request setStrLastModified:strLastModified];
    }
    
    if(isSupervisor) {
        [request setIntRoomStatus:[NSString stringWithFormat:@"%d,%d,%d,%d",ENUM_ROOM_STATUS_VC, ENUM_ROOM_STATUS_OC, ENUM_ROOM_STATUS_VPU, ENUM_ROOM_STATUS_OPU]];
    }

//    [request setIntInspectionStatus:@""];
    
//    LogObject *logObj = [[LogObject alloc] init];
//    logObj.dateTime = [NSDate date];
//    logObj.log = [NSString stringWithFormat:@"<%i><getRoomAssignmentWSByUserID><intUsrID:%i lastModified:%@>",userID,userID,strLastModified];
//    LogFileManager *logManager = [[LogFileManager alloc] init];
//    [logManager save:logObj];
    float currentPercent = 0;
    [self updatePercentView:percentView value:currentPercent description:LOADING_GET_ROOM_ASSIGNMENT];
    if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL)
    {
        return NO;
    }
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetRoomAssignmentListUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetRoomAssignmentListResponse class]]) {
            eHousekeepingService_GetRoomAssignmentListResponse *dataBody = (eHousekeepingService_GetRoomAssignmentListResponse *)bodyPart;
            if (dataBody != nil) {
                if ([dataBody.GetRoomAssignmentListResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_OK]]
                    || [dataBody.GetRoomAssignmentListResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_NOROOM_ASSIGNMENT_IS_FOUND]]) {
                    result = YES;
                    
                    //delete old room assignment here
                    //[[RoomManagerV2 sharedRoomManager] deleteOldRoomAssignmentsWithUserId:userID];
                    
                    //Delete everything related to RoomAssignment, for fixing Raddisson Blue issue
                    [[RoomManagerV2 sharedRoomManager] deleteAllRoomAssignment];
                    [[RoomManagerV2 sharedRoomManager] deleteAllRoom];
                    
                    NSMutableArray *roomAssignments = dataBody.GetRoomAssignmentListResult.RoomAssignmentList.RoomAssign;
                    listRoomsCompare = [NSMutableArray array];
                    
                    //check and remove missing room assignments
                    [self checkAndRemoveMissingRoomAssignments:roomAssignments andUserId:userID];

                    float stepPercent = [self caculateStepFromCountingObject:(int)[roomAssignments count]];
                    for (eHousekeepingService_RoomAssign *roomAssign in roomAssignments) {
                        if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL) {
                            break;
                        }
                        
                        [listRoomsCompare addObject:roomAssign.raRoomNo];
                        
                        RoomAssignmentModelV2 *raModel = [[RoomAssignmentModelV2 alloc] init];
                        RoomAssignmentModelV2 *compareRaModel = [[RoomAssignmentModelV2 alloc] init];
                        
                        compareRaModel.roomAssignment_Id = (int)[roomAssign.raID integerValue];
                        compareRaModel.roomAssignment_UserId = (int)userID;
                        
                        raModel.roomAssignment_Id = [roomAssign.raID intValue];
                        raModel.roomAssignment_AssignedDate = roomAssign.raAssignedTime;
                        
                        //20150420 - Fixed can't show previous date
                        //raModel.roomAssignment_LastCleaningDate = roomAssign.raLastRoomCleaningDate;
                        raModel.roomAssignment_LastCleaningDate = roomAssign.raCleanEndTm;
                        raModel.roomAssignment_LastModified = roomAssign.raLastModified;
                        raModel.roomAssignment_PostStatus = (int)POST_STATUS_UN_CHANGED;
                        raModel.roomAssignment_Priority = [roomAssign.raPriority intValue];
                        raModel.roomAssignment_Priority_Sort_Order = [roomAssign.raPrioritySortOrder integerValue];
                        raModel.roomAssignment_RoomId = roomAssign.raRoomNo;
                        raModel.roomAssignment_UserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
                        raModel.roomAssignmentHousekeeperId = [roomAssign.raHousekeeperID integerValue];
                        raModel.roomAssignmentCleaningCredit = roomAssign.raCleaningCredit;
                        //insert room status, cleaning status, inspection status
                        raModel.roomAssignmentRoomStatusId = [roomAssign.raRoomStatusID integerValue];
                        raModel.roomAssignmentRoomCleaningStatusId = [roomAssign.raCleaningStatusID integerValue];
                        raModel.roomAssignmentRoomInspectionStatusId = [roomAssign.raInspectedStatus integerValue];
                        raModel.roomAssignmentChecklistRemark = roomAssign.raChecklistRemarks;
                        //insert room expected cleaning time, expected inspection time, guest profile id
                        raModel.roomAssignmentRoomExpectedCleaningTime = [roomAssign.raExpectedCleaningTime integerValue];
                        raModel.roomAssignmentRoomExpectedInspectionTime = [roomAssign.raExpectedInspectTime integerValue];
                        raModel.roomAssignmentGuestProfileId = [roomAssign.raGuestProfileID integerValue];
                        
                        NSArray *arrayName = [roomAssign.raFullName componentsSeparatedByString:@" "];
                        
                        if (arrayName.count > 2) {
                            raModel.roomAssignmentFirstName = [NSString stringWithFormat:@"%@ %@", [arrayName objectAtIndex:0], [arrayName objectAtIndex:arrayName.count - 1]];
                        } else {
                            raModel.roomAssignmentFirstName = roomAssign.raFullName;
                        }
                        
                        raModel.raGuestArrivalTime = roomAssign.raGuestArrivalTime;
                        raModel.raGuestDepartureTime = roomAssign.raGuestDepartureTime;
//                        raModel.raIsMockRoom = [roomAssign.raIsMockRoom integerValue];
                        raModel.raKindOfRoom = [roomAssign.raKindOfRoom integerValue];
                        
                        
                        //set mockup room local
                        NSInteger isMockupRoomValue = [roomAssign.raIsMockRoom integerValue];
                        if(isMockupRoomValue == ENUM_TASK_TYPE_NORMAL)
                        {
                            raModel.raIsMockRoom = 0;
                        }
                        else if(isMockupRoomValue == ENUM_TASK_TYPE_REASSIGN)
                        {
                            raModel.raIsMockRoom = 0;
//                            raModel.raIsReassignedRoom = 1;
                        }
                        else if(isMockupRoomValue >= ENUM_TASK_TYPE_MOCKUP)
                        {
                            raModel.raIsMockRoom = 1;
                        }

                        [self load:compareRaModel];
                        
                        //set property raIsReassginedRoom for show view
                        if(isMockupRoomValue == ENUM_TASK_TYPE_REASSIGN
                           && ([roomAssign.raCleaningStatusID integerValue] == ENUM_CLEANING_STATUS_PENDING_FIRST_TIME
                               || [roomAssign.raCleaningStatusID integerValue] == ENUM_CLEANING_STATUS_DECLINE_SERVICE
                               || [roomAssign.raCleaningStatusID integerValue] == ENUM_CLEANING_STATUS_DND
                               || [roomAssign.raCleaningStatusID integerValue] == ENUM_CLEANING_STATUS_PAUSE))
                        {
                            raModel.raIsReassignedRoom = 1;
                        }
                        else
                        {
                            raModel.raIsReassignedRoom = 0;
                        }
                        
                        if (compareRaModel.roomAssignment_RoomId.length > 0) {
                            if (![compareRaModel.roomAssignment_LastModified isEqualToString:roomAssign.raLastModified]) {
                                //raModel.raIsReassignedRoom = (raModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_PENDING_SECOND_TIME?1:0);
                                [self update:raModel];
                            }
                        } else {
                            //raModel.raIsReassignedRoom = (raModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_PENDING_SECOND_TIME?1:0);
                            [self insert:raModel];
                        }
                        
                        //Hao Tran - Remove for enhance loading data
                        /*
                        if(raModel.roomAssignmentRoomStatusId == room_Status_Id_OD ||
                           //Hao Tran fix can't show guest information in room status OPU
                           raModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OPU ||
                           raModel.roomAssignmentRoomStatusId == room_Status_Id_OC)
                        {
                            [self updateGuestInfo:userID WithRoomAssignID:raModel.roomAssignment_Id];
                        }*/
                        
                        //insert Room Detail data
                        RoomModelV2 *roomModel = [[RoomModelV2 alloc] init];
                        roomModel.room_Id = roomAssign.raRoomNo;
                        roomModel.room_AdditionalJob = roomAssign.raAdditionalJob;
                        roomModel.room_Building_Name = roomAssign.raBuildingName;
                        roomModel.room_Building_Id = [roomAssign.raBuildingID intValue];
                        roomModel.room_building_namelang = roomAssign.rabuildingLang;
                        roomModel.room_Guest_Name = [NSString stringWithFormat:@"%@%@", roomAssign.raGuestFirstName == nil ? @"" : [NSString stringWithFormat:@"%@ ", roomAssign.raGuestFirstName], roomAssign.raGuestLastName == nil ? @"" : [NSString stringWithFormat:@"%@", roomAssign.raGuestLastName]];
                        roomModel.room_GuestReference = roomAssign.raGuestPreference;
                        roomModel.room_HotelId = [roomAssign.raHotelID intValue];
                        roomModel.room_LastModified = roomAssign.raLastModified;
                        
                        roomModel.room_PostStatus = (int)POST_STATUS_UN_CHANGED;
                        roomModel.room_remark = roomAssign.raRemark;
                        roomModel.room_TypeId = [roomAssign.raRoomTypeID intValue];
                        roomModel.room_VIPStatusId = roomAssign.raVIP;
                        roomModel.room_floor_id = [roomAssign.raFloorID integerValue];
                        
                        RoomModelV2 *compareRoom = [[RoomModelV2 alloc] init];
                        compareRoom.room_Id = roomAssign.raRoomNo;
                        
                        [self loadRoomModel:compareRoom];
                        
                        //if (compareRoom.room_VIPStatusId != 0) {
                        if(compareRoom.room_LastModified.length > 0){
                            //Not need post room info because when call get RA, did check to post Room Assignment
                            if (![compareRoom.room_LastModified isEqualToString:roomAssign.raLastModified])
                            {
                                [self updateRoomModel:roomModel];
                            }
                        } else {
                            [self insertRoomModel:roomModel];
                        }
                        
                        //Hao Tran - Remove for enhance sync. This command will be called in RoomAssignmentInfoViewController
                        //compareRoom.room_VIPStatusId = 0;
                        //[self getRoomWSByUserID:userID AndRaID:[roomAssign.raID integerValue] AndLastModified:nil AndLastCleaningDate:roomAssign.raLastRoomCleaningDate];
                        //Hao Tran - END
                        
                        [self updatePercentView:percentView value:currentPercent description:LOADING_GET_ROOM_ASSIGNMENT];
                        currentPercent += stepPercent;
                    }
                }
            }
        }
    }
    
    [[RoomManagerV2  sharedRoomManager] getWSOOSBlockRoomListByUserId:userID hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId listRoomAssignmentCompare:listRoomsCompare];
    
    if ([[UserManagerV2 sharedUserManager].currentUserAccessRight isEnabledSupHomeFilter]) {
         NSInteger buildingId = [[NSUserDefaults standardUserDefaults] integerForKey:[NSString stringWithFormat:@"%@%d", CURRENT_BUILDING_ID, [UserManagerV2 sharedUserManager].currentUser.userId]];
        [[RoomManagerV2 sharedRoomManager] getSupervisorFilterRoutineByUserId:(int)userID buildingId:(int)buildingId hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId listRoomAssignmentCompare:listRoomsCompare];
    }
    
    if([[UserManagerV2 sharedUserManager].currentUserAccessRight isEnableReleaseRoom]) {
        [[RoomManagerV2  sharedRoomManager] getWSReleaseRoomListByUserId:userID hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId listRoomAssignmentCompare:listRoomsCompare];
    }
    
    return result;
}

-(BOOL)GetFindRoomAssignmentListWSByUserID:(NSInteger)userID AndFloorId:(NSInteger)floorId AndRoomstatus:(NSInteger)roomStatusID AndFilterType:(NSInteger)filterType AndPercentView:(MBProgressHUD*)percentView
{
    isFindRoom = YES;
    NSMutableArray *listRoomsCompare = nil;
    BOOL result = NO;
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole])
    {
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_GetSupervisorFindRoomAssignmentLists *request = [[eHousekeepingService_GetSupervisorFindRoomAssignmentLists alloc] init];
    
    //setting parametters
    [request setIntSupervisorID:[NSNumber numberWithInteger:userID]];
    [request setIntFloorID:[NSNumber numberWithInteger:floorId]];
    [request setIntFilterType:[NSNumber numberWithInteger:filterType]];
    [request setIntStatusID:[NSNumber numberWithInteger:roomStatusID]];
    
    float currentPercent = 0;
    [self updatePercentView:percentView value:currentPercent description:[L_loading_find_room_assignment currentKeyToLanguage]];
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetSupervisorFindRoomAssignmentListsUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    
    NSDate *now = [NSDate date];
    NSString *strNow = [dateFormat stringFromDate:now];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [dateFormat setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    now = [dateFormat dateFromString:[NSString stringWithFormat:@"%@ 00:00:00",strNow]];
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetSupervisorFindRoomAssignmentListsResponse class]]) {
            eHousekeepingService_GetSupervisorFindRoomAssignmentListsResponse *dataBody = (eHousekeepingService_GetSupervisorFindRoomAssignmentListsResponse *)bodyPart;
            if (dataBody != nil) {
                if ([dataBody.GetSupervisorFindRoomAssignmentListsResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_OK]]) {
                    result = YES;
                    NSMutableArray *roomAssignments = dataBody.GetSupervisorFindRoomAssignmentListsResult.RoomAssignmentList.RoomAssign;
                    
                    listRoomsCompare = [NSMutableArray array];
                    
                    //check and remove missing room assignments
                    //[self checkAndRemoveMissingRoomAssignments:roomAssignments andUserId:userID];
                    float stepPercent = [self caculateStepFromCountingObject:(int)[roomAssignments count]];
                    for (eHousekeepingService_RoomAssign *roomAssign in roomAssignments) {

                        //REMOVE FILTER DATE TIME NOW, WE WILL GET ALL ROOM REGARDLESS OF DATE TIME
                        //NSDate *assignedDate = [dateFormat dateFromString:roomAssign.raAssignedTime];
                        //if ([assignedDate compare:now] == NSOrderedAscending) {
                        //  continue;
                        //}
                        [listRoomsCompare addObject:roomAssign.raRoomNo];
                        RoomAssignmentModelV2 *raModel = [[RoomAssignmentModelV2 alloc] init];
                        RoomAssignmentModelV2 *compareRaModel = [[RoomAssignmentModelV2 alloc] init];
                        
                        RoomModelV2 *roomModel = [[RoomModelV2 alloc] init];
                        
                        compareRaModel.roomAssignment_Id = [roomAssign.raID intValue];
                        compareRaModel.roomAssignment_UserId = (int)userID;
                        
                        raModel.roomAssignment_Id = [roomAssign.raID intValue];
                        raModel.roomAssignment_AssignedDate = roomAssign.raAssignedTime;
                        //20150420 - Fixed can't show previous date
                        //raModel.roomAssignment_LastCleaningDate = roomAssign.raLastRoomCleaningDate;
                        raModel.roomAssignment_LastCleaningDate = roomAssign.raCleanEndTm;
                        raModel.roomAssignment_LastModified = roomAssign.raLastModified;
                        raModel.roomAssignment_PostStatus = (int)POST_STATUS_UN_CHANGED;
                        raModel.roomAssignment_Priority = [roomAssign.raPriority intValue];
                        raModel.roomAssignment_Priority_Sort_Order = [roomAssign.raPrioritySortOrder integerValue];
                        raModel.roomAssignment_RoomId = roomAssign.raRoomNo;
                        raModel.roomAssignment_UserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
                        raModel.roomAssignmentHousekeeperId = [roomAssign.raHousekeeperID integerValue];
                        
                        //insert room status, cleaning status, inspection status
                        raModel.roomAssignmentRoomStatusId = [roomAssign.raRoomStatusID integerValue];
                        raModel.roomAssignmentRoomCleaningStatusId = [roomAssign.raCleaningStatusID integerValue];
                        raModel.roomAssignmentRoomInspectionStatusId = [roomAssign.raInspectedStatus integerValue];
                        raModel.roomAssignmentInspectedScore = [roomAssign.raInspectedScore integerValue];
                        
                        //insert room expected cleaning time, expected inspection time, guest profile id
                        raModel.roomAssignmentRoomExpectedCleaningTime = [roomAssign.raExpectedCleaningTime integerValue];
                        raModel.roomAssignmentRoomExpectedInspectionTime = [roomAssign.raExpectedInspectTime integerValue];
                        raModel.roomAssignmentGuestProfileId = [roomAssign.raGuestProfileID integerValue];
                        raModel.raGuestArrivalTime = roomAssign.raGuestArrivalTime;
                        raModel.raGuestDepartureTime = roomAssign.raGuestDepartureTime;
                        raModel.raKindOfRoom = [roomAssign.raKindOfRoom integerValue];
                        
                        //                        raModel.raIsMockRoom = [roomAssign.raIsMockRoom integerValue];
                        //set mockup room local
                        NSInteger isMockupRoomValue = [roomAssign.raIsMockRoom integerValue];
                        if(isMockupRoomValue == ENUM_TASK_TYPE_NORMAL)
                        {
                            raModel.raIsMockRoom = 0;
                        }
                        else if(isMockupRoomValue == ENUM_TASK_TYPE_REASSIGN)
                        {
//                            raModel.raIsReassignedRoom = 1;
                            raModel.raIsMockRoom = 0;
                        }
                        else if(isMockupRoomValue >= ENUM_TASK_TYPE_MOCKUP)
                        {
                            raModel.raIsMockRoom = 1;
                        }
                        
                        //Hao Tran - Fix can not show reassign image
                        if([roomAssign.raCleaningStatusID integerValue] == ENUM_CLEANING_STATUS_PENDING_FIRST_TIME
                           && isMockupRoomValue == ENUM_TASK_TYPE_REASSIGN){
                            raModel.raIsReassignedRoom = 1;
                        } else {
                            raModel.raIsReassignedRoom = 0;
                        }
                        
                        NSArray *arrayName = [roomAssign.raFullName componentsSeparatedByString:@" "];
                        
                        if (arrayName.count > 2) {
                            raModel.roomAssignmentFirstName = [NSString stringWithFormat:@"%@ %@", [arrayName objectAtIndex:0], [arrayName objectAtIndex:arrayName.count - 1]];
                        } else {
                            raModel.roomAssignmentFirstName = roomAssign.raFullName;
                        }
                        
                        [self load:compareRaModel];
                        
                        if (compareRaModel.roomAssignment_RoomId.length > 0) {
                            if (![compareRaModel.roomAssignment_LastModified isEqualToString:roomAssign.raLastModified]) {
                                
                                //Hao Tran - Remove because this code will not read at first time load WS
                                //Quang edit
                                /*
                                if([roomAssign.raCleaningStatusID integerValue] == ENUM_CLEANING_STATUS_PENDING_FIRST_TIME
                                   && isMockupRoomValue == ENUM_TASK_TYPE_REASSIGN){
                                    raModel.raIsReassignedRoom = 1;
                                } else {
                                    raModel.raIsReassignedRoom = 0;
                                }
                                */
                                //raModel.raIsReassignedRoom = ([roomAssign.raCleaningStatusID integerValue] == ENUM_CLEANING_STATUS_PENDING_SECOND_TIME?1:0);
                                [self update:raModel];
                            }
                        } else {
                            [self insert:raModel];
                        }
                        
                        //Hao Tran - Remove for enhance loading data
                        /*
                        if(raModel.roomAssignmentRoomStatusId == room_Status_Id_OC ||
                           raModel.roomAssignmentRoomStatusId == room_Status_Id_OD)
                        {
                            [self updateGuestInfo:userID WithRoomAssignID:raModel.roomAssignment_Id];
                        }*/
                        
                        //insert Room Detail data
                        roomModel.room_Id = roomAssign.raRoomNo;
                        roomModel.room_AdditionalJob = roomAssign.raAdditionalJob;
                        roomModel.room_Building_Name = roomAssign.raBuildingName;
                        roomModel.room_Building_Id = [roomAssign.raBuildingID intValue];
                        roomModel.room_building_namelang = roomAssign.rabuildingLang;
                        //                    room.room_expected_status_id = ;
                        roomModel.room_Guest_Name = [NSString stringWithFormat:@"%@%@", roomAssign.raGuestFirstName == nil ? @"" : [NSString stringWithFormat:@"%@ ", roomAssign.raGuestFirstName], roomAssign.raGuestLastName == nil ? @"" : [NSString stringWithFormat:@"%@", roomAssign.raGuestLastName]];
                        roomModel.room_GuestReference = roomAssign.raGuestPreference;
                        roomModel.room_HotelId = [roomAssign.raHotelID intValue];
                        //                    room.room_is_re_cleaning = ;
                        //                    room.room_isInpected = ;
                        //                    room.room_isReassigned = ;
                        //                    room.room_Lang = ;
                        //                    room.room_LocationCode = ;
                        roomModel.room_LastModified = roomAssign.raLastModified;
                        
                        roomModel.room_PostStatus = (int)POST_STATUS_UN_CHANGED;
                        roomModel.room_remark = roomAssign.raRemark;
                        roomModel.room_TypeId = [roomAssign.raRoomTypeID intValue];
                        roomModel.room_VIPStatusId = roomAssign.raVIP;
                        roomModel.room_floor_id = [roomAssign.raFloorID integerValue];
                        
                        
                        RoomModelV2 *compareRoom = [[RoomModelV2 alloc] init];
                        compareRoom.room_Id = roomAssign.raRoomNo;
                        
                        [self loadRoomModel:compareRoom];
                        
                        //if (compareRoom.room_VIPStatusId != 0) {
                        if(compareRoom.room_LastModified.length > 0){
                            //Not need post room info because when call get RA, did check to post Room Assignment
                            if (![compareRoom.room_LastModified isEqualToString:roomAssign.raLastModified])
                            {
                                [self updateRoomModel:roomModel];
                            }
                        }else {
                            [self insertRoomModel:roomModel];
                        }
                        
                        //Hao Tran - Remove for enhance sync. This command will be called in RoomAssignmentInfoViewController
                        /*
                        compareRoom.room_VIPStatusId = 0;
                        [self getRoomWSByUserID:userID AndRaID:[roomAssign.raID integerValue] AndLastModified:nil AndLastCleaningDate:roomAssign.raLastRoomCleaningDate];
                        */
                        
                        currentPercent += stepPercent;
                        [self updatePercentView:percentView value:currentPercent description:[L_loading_find_room_assignment currentKeyToLanguage]];
                    }
                }
            }
        }
    }
    
    [[RoomManagerV2  sharedRoomManager] getWSOOSBlockRoomListByUserId:userID hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId listRoomAssignmentCompare:listRoomsCompare];
    
    if([[UserManagerV2 sharedUserManager].currentUserAccessRight isEnableReleaseRoom]) {
        [[RoomManagerV2  sharedRoomManager] getWSReleaseRoomListByUserId:userID hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId listRoomAssignmentCompare:listRoomsCompare];
    }
    return result;
}
//CRF-00000516 / Find Function Enhancement
-(NSArray*)GetZoneFindRoomDetailsListWSByUserID:(NSInteger)userID AndFloorId:(NSInteger)floorId AndBuildingId:(NSInteger)buildingId AndHotelId:(NSInteger)HTID AndFilterType:(NSInteger) filterType AndGeneralFilterType:(NSString*) generalFilterType AndPercentView:(MBProgressHUD*)percentView
{
    NSMutableArray *resultArray = [NSMutableArray new];
    isFindRoom = YES;
    NSMutableArray *listRoomsCompare = nil;
    BOOL result = NO;
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole])
    {
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_GetFindRoomDetailsRequest *request = [[eHousekeepingService_GetFindRoomDetailsRequest alloc] init];
    
    //setting parametters
    [request setUserID:[NSNumber numberWithInteger:userID]];
    [request setFloorID:[NSString stringWithFormat:@"%ld",(long)floorId]];
    [request setBuildingID:[NSNumber numberWithInteger:buildingId]];
    [request setHTID:[NSNumber numberWithInteger:HTID]];
    [request setFilterType:[NSNumber numberWithInteger:filterType]];
    [request setGeneralFilter:generalFilterType];
    
    eHousekeepingService_GetFindRoomDetailsList *roomList = [[eHousekeepingService_GetFindRoomDetailsList alloc] init];
    roomList.GetFindRoomDetailsRequest = request;
    float currentPercent = 0;
    [self updatePercentView:percentView value:currentPercent description:[L_loading_find_room_assignment currentKeyToLanguage]];
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetFindRoomDetailsListUsingParameters:roomList];
    
    NSArray *responseBodyParts = response.bodyParts;
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    
    NSDate *now = [NSDate date];
    NSString *strNow = [dateFormat stringFromDate:now];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [dateFormat setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    now = [dateFormat dateFromString:[NSString stringWithFormat:@"%@ 00:00:00",strNow]];
    
    //Hao Tran - Fixed server didn't return correct KindOfRoomType value (Hard code KindOfRoomType)
    int kindOfRoomTypeValue = [generalFilterType intValue];
//    if(filterType == kindOfRoomType) {
//        kindOfRoomTypeValue = [generalFilterType intValue];
//    }
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetFindRoomDetailsListResponse class]]) {
            eHousekeepingService_GetFindRoomDetailsListResponse *dataBody = (eHousekeepingService_GetFindRoomDetailsListResponse *)bodyPart;
            if (dataBody != nil) {
                if ([dataBody.GetFindRoomDetailsListResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_OK]] ||
                    [dataBody.GetFindRoomDetailsListResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_NOROOM_ASSIGNMENT_IS_FOUND]]) {
                    result = YES;
                    NSMutableArray *roomAssignments = dataBody.GetFindRoomDetailsListResult.RoomAssignmentListing.RoomAssignmentDetails;
                    listRoomsCompare = [NSMutableArray array];
                    //check and remove missing room assignments
                    //[self checkAndRemoveMissingRoomAssignments:roomAssignments andUserId:userID];
                    float stepPercent = [self caculateStepFromCountingObject:(int)[roomAssignments count]];
                    for (eHousekeepingService_RoomAssignmentDetails *roomAssign in roomAssignments) {
                        
                        //REMOVE FILTER DATE TIME NOW, WE WILL GET ALL ROOM REGARDLESS OF DATE TIME
                        //NSDate *assignedDate = [dateFormat dateFromString:roomAssign.raAssignedTime];
                        //if ([assignedDate compare:now] == NSOrderedAscending) {
                        //  continue;
                        //}
                        [listRoomsCompare addObject:roomAssign.RoomNo];
                        RoomAssignmentModelV2 *raModel = [[RoomAssignmentModelV2 alloc] init];
                        RoomAssignmentModelV2 *compareRaModel = [[RoomAssignmentModelV2 alloc] init];
                        
                        RoomModelV2 *roomModel = [[RoomModelV2 alloc] init];
                        
                        compareRaModel.roomAssignment_Id = [roomAssign.ID_ intValue];
                        compareRaModel.roomAssignment_UserId = (int)userID;
                        
                        raModel.roomAssignment_Id = [roomAssign.ID_ intValue];
                        raModel.roomAssignment_AssignedDate = roomAssign.AssignedTime;
                        //20150420 - Fixed can't show previous date
                        //raModel.roomAssignment_LastCleaningDate = roomAssign.LastRoomCleaningDate;
                        raModel.roomAssignment_LastCleaningDate = roomAssign.CleaningEndTime;
                        raModel.roomAssignment_LastModified = roomAssign.LastModified;
                        raModel.roomAssignment_PostStatus = (int)POST_STATUS_UN_CHANGED;
                        raModel.roomAssignment_Priority = roomAssign.Priority;
                        raModel.roomAssignment_Priority_Sort_Order = roomAssign.PrioritySortOrder;
                        raModel.roomAssignment_RoomId = roomAssign.RoomNo;
                        raModel.roomAssignment_UserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
                        raModel.roomAssignmentHousekeeperId = [roomAssign.HousekeeperID integerValue];
                        raModel.roomAssignmentCleaningCredit = roomAssign.CleaningCredit;
                        //insert room status, cleaning status, inspection status
                        raModel.roomAssignmentRoomStatusId = [roomAssign.RoomStatusID integerValue];
                        raModel.roomAssignmentRoomCleaningStatusId = [roomAssign.CleaningStatusID integerValue];
                        raModel.roomAssignmentRoomInspectionStatusId = [roomAssign.InspectionStatusID integerValue];
                        raModel.roomAssignmentInspectedScore = [roomAssign.InspectionScore integerValue];
                        
                        //insert room expected cleaning time, expected inspection time, guest profile id
                        raModel.roomAssignmentRoomExpectedCleaningTime = [roomAssign.ExpectedCleaningTime integerValue];
                        raModel.roomAssignmentRoomExpectedInspectionTime = [roomAssign.ExpectedInspectionTime integerValue];
                        raModel.roomAssignmentGuestProfileId = [roomAssign.GuestProfileID integerValue];
                        raModel.raGuestArrivalTime = roomAssign.GuestArrivalTime;
                        raModel.raGuestDepartureTime = roomAssign.GuestDepartureTime;
                        
                        //Fix server didn't return kindOfRoomType value
                        NSString *strKindOfRoom = [NSString stringWithFormat:@"%@", roomAssign.KindOfRoom];
                        raModel.raKindOfRoom = [strKindOfRoom intValue];// != nil ? [roomAssign.KindOfRoom integerValue] : kindOfRoomTypeValue;
                        //> 0 ? kindOfRoomTypeValue : [roomAssign.KindOfRoom integerValue];
//                        raModel.raKindOfRoom = [roomAssign.KindOfRoom integerValue];
                        //set mockup room local
                        raModel.roomAssignmentZoneId =kindOfRoomTypeValue;
                        NSInteger isMockupRoomValue = [roomAssign.IsMockRoom integerValue];
                        if(isMockupRoomValue == ENUM_TASK_TYPE_NORMAL)
                        {
                            raModel.raIsMockRoom = 0;
                        }
                        else if(isMockupRoomValue == ENUM_TASK_TYPE_REASSIGN)
                        {
                            //2015-05-04 - Compare with Android and fixed OC/OI can't show reassign icon in room list
                            raModel.raIsReassignedRoom = 1;
                            raModel.raIsMockRoom = 0;
                        }
                        else if(isMockupRoomValue >= ENUM_TASK_TYPE_MOCKUP)
                        {
                            raModel.raIsMockRoom = 1;
                        }
                        
                        //2015-05-04 - Compare with Android and fixed OC/OI can't show reassign icon in room list
                        //Hao Tran - Fix can not show reassign image
                        /*if([roomAssign.CleaningStatusID integerValue] == ENUM_CLEANING_STATUS_PENDING_FIRST_TIME
                         && isMockupRoomValue == ENUM_TASK_TYPE_REASSIGN){
                         raModel.raIsReassignedRoom = 1;
                         } else {
                         raModel.raIsReassignedRoom = 0;
                         }*/
                        
                        NSArray *arrayName = [roomAssign.HousekeeperFullname componentsSeparatedByString:@" "];
                        
                        if (arrayName.count > 2) {
                            raModel.roomAssignmentFirstName = [NSString stringWithFormat:@"%@ %@", [arrayName objectAtIndex:0], [arrayName objectAtIndex:arrayName.count - 1]];
                        } else {
                            raModel.roomAssignmentFirstName = roomAssign.HousekeeperFullname;
                        }
                        
                        [self load:compareRaModel];
                        
                        if (compareRaModel.roomAssignment_RoomId.length > 0) {
                            // CFG [20170306/BugtrackerID:00015120] - Should always update.
                            //if (![compareRaModel.roomAssignment_LastModified isEqualToString:roomAssign.LastModified]) {
                                
                                //Hao Tran - Remove because this code will not read at first time load WS
                                //Quang edit
                                /*
                                 if([roomAssign.raCleaningStatusID integerValue] == ENUM_CLEANING_STATUS_PENDING_FIRST_TIME
                                 && isMockupRoomValue == ENUM_TASK_TYPE_REASSIGN){
                                 raModel.raIsReassignedRoom = 1;
                                 } else {
                                 raModel.raIsReassignedRoom = 0;
                                 }
                                 */
                                //raModel.raIsReassignedRoom = ([roomAssign.raCleaningStatusID integerValue] == ENUM_CLEANING_STATUS_PENDING_SECOND_TIME?1:0);
                                [self update:raModel];
                            //}
                            // CFG [20170306/BugtrackerID:00015120] - End.
                        } else {
                            [self insert:raModel];
                            [self updateIsCheckedFind:[roomAssign.ID_ integerValue]];
                        }
                        
                        //Hao Tran - Remove for enhance loading data
                        /*
                         if(raModel.roomAssignmentRoomStatusId == room_Status_Id_OC ||
                         raModel.roomAssignmentRoomStatusId == room_Status_Id_OD)
                         {
                         [self updateGuestInfo:userID WithRoomAssignID:raModel.roomAssignment_Id];
                         }*/
                        
                        //insert Room Detail data
                        roomModel.room_Id = roomAssign.RoomNo;
                        roomModel.room_AdditionalJob = @"";
                        roomModel.room_Building_Name = roomAssign.BuildingName;
                        roomModel.room_Building_Id = [roomAssign.BuildingID intValue];
                        roomModel.room_building_namelang = roomAssign.BuildingLang;
                        //                    room.room_expected_status_id = ;
                        if(roomAssign.GuestName && ![roomAssign.GuestName isEqualToString:@""]){
                            roomModel.room_Guest_Name = roomAssign.GuestName;
                        }
                        else{
                            NSString *guestName = @"";
                            guestName = [guestName stringByAppendingString:[NSString stringWithFormat:@"%@ %@", roomAssign.GuestFirstName, roomAssign.GuestLastName]];
                            roomModel.room_Guest_Name = guestName;
                            
                        }
                        roomModel.room_GuestReference = roomAssign.GuestPreference;
                        roomModel.room_HotelId = [roomAssign.HTID intValue];
                        //                    room.room_is_re_cleaning = ;
                        //                    room.room_isInpected = ;
                        //                    room.room_isReassigned = ;
                        //                    room.room_Lang = ;
                        //                    room.room_LocationCode = ;
                        roomModel.room_LastModified = roomAssign.LastModified;
                        
                        roomModel.room_PostStatus = (int)POST_STATUS_UN_CHANGED;
                        roomModel.room_remark = roomAssign.Remark;
                        roomModel.room_TypeId = [roomAssign.RoomTypeID intValue];
                        roomModel.room_VIPStatusId = roomAssign.GuestVIPCode;
                        roomModel.room_floor_id = 0;
                        
                        
                        RoomModelV2 *compareRoom = [[RoomModelV2 alloc] init];
                        compareRoom.room_Id = roomAssign.RoomNo;
                        
                        [self loadRoomModel:compareRoom];
                        
                        //if (compareRoom.room_VIPStatusId != 0) {
                        if(compareRoom.room_LastModified.length > 0){
                            //Not need post room info because when call get RA, did check to post Room Assignment
                            if (![compareRoom.room_LastModified isEqualToString:roomAssign.LastModified])
                            {
                                [self updateRoomModel:roomModel];
                            }
                        }else {
                            [self insertRoomModel:roomModel];
                        }
                        
                        //Hao Tran - Remove for enhance sync. This command will be called in RoomAssignmentInfoViewController
                        /*
                         compareRoom.room_VIPStatusId = 0;
                         [self getRoomWSByUserID:userID AndRaID:[roomAssign.raID integerValue] AndLastModified:nil AndLastCleaningDate:roomAssign.raLastRoomCleaningDate];
                         */
                        
                        currentPercent += stepPercent;
                        [self updatePercentView:percentView value:currentPercent description:[L_loading_find_room_assignment currentKeyToLanguage]];
                    }
                }
            }
        }
    }
    
    
    [[RoomManagerV2  sharedRoomManager] getWSOOSBlockRoomListByUserId:userID hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId listRoomAssignmentCompare:listRoomsCompare];
    
    if([[UserManagerV2 sharedUserManager].currentUserAccessRight isEnableReleaseRoom]) {
        [[RoomManagerV2  sharedRoomManager] getWSReleaseRoomListByUserId:userID hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId listRoomAssignmentCompare:listRoomsCompare];
    }
    return resultArray;
}
-(NSArray*)GetFloorFindRoomDetailsListWSByUserID:(NSInteger)userID AndFloorId:(NSString*)floorId AndBuildingId:(NSInteger)buildingId AndHotelId:(NSInteger)HTID AndFilterType:(NSInteger) filterType AndGeneralFilterType:(NSString*) generalFilterType AndPercentView:(MBProgressHUD*)percentView
{
    NSMutableArray *resultArray = [NSMutableArray new];
    isFindRoom = YES;
    NSMutableArray *listRoomsCompare = nil;
    BOOL result = NO;
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole])
    {
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_GetFindRoomDetailsRequest *request = [[eHousekeepingService_GetFindRoomDetailsRequest alloc] init];
    
    //setting parametters
    [request setUserID:[NSNumber numberWithInteger:userID]];
    [request setFloorID:[NSString stringWithFormat:@"%@",floorId]];
    [request setBuildingID:[NSNumber numberWithInteger:buildingId]];
    [request setHTID:[NSNumber numberWithInteger:HTID]];
    [request setFilterType:[NSNumber numberWithInteger:filterType]];
    [request setGeneralFilter:generalFilterType];
    
    eHousekeepingService_GetFindRoomDetailsList *roomList = [[eHousekeepingService_GetFindRoomDetailsList alloc] init];
    roomList.GetFindRoomDetailsRequest = request;
    float currentPercent = 0;
    [self updatePercentView:percentView value:currentPercent description:[L_loading_find_room_assignment currentKeyToLanguage]];
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetFindRoomDetailsListUsingParameters:roomList];
    
    NSArray *responseBodyParts = response.bodyParts;
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    
    NSDate *now = [NSDate date];
    NSString *strNow = [dateFormat stringFromDate:now];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [dateFormat setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    now = [dateFormat dateFromString:[NSString stringWithFormat:@"%@ 00:00:00",strNow]];
    
    //Hao Tran - Fixed server didn't return correct KindOfRoomType value (Hard code KindOfRoomType)
    int kindOfRoomTypeValue = [generalFilterType intValue];
    //    if(filterType == kindOfRoomType) {
    //        kindOfRoomTypeValue = [generalFilterType intValue];
    //    }
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetFindRoomDetailsListResponse class]]) {
            eHousekeepingService_GetFindRoomDetailsListResponse *dataBody = (eHousekeepingService_GetFindRoomDetailsListResponse *)bodyPart;
            if (dataBody != nil) {
                if ([dataBody.GetFindRoomDetailsListResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_OK]] ||
                    [dataBody.GetFindRoomDetailsListResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_NOROOM_ASSIGNMENT_IS_FOUND]]) {
                    result = YES;
                    NSMutableArray *roomAssignments = dataBody.GetFindRoomDetailsListResult.RoomAssignmentListing.RoomAssignmentDetails;
                    listRoomsCompare = [NSMutableArray array];
                    //check and remove missing room assignments
                    //[self checkAndRemoveMissingRoomAssignments:roomAssignments andUserId:userID];
                    float stepPercent = [self caculateStepFromCountingObject:(int)[roomAssignments count]];
                    for (eHousekeepingService_RoomAssignmentDetails *roomAssign in roomAssignments) {
                        
                        //REMOVE FILTER DATE TIME NOW, WE WILL GET ALL ROOM REGARDLESS OF DATE TIME
                        //NSDate *assignedDate = [dateFormat dateFromString:roomAssign.raAssignedTime];
                        //if ([assignedDate compare:now] == NSOrderedAscending) {
                        //  continue;
                        //}
                        [listRoomsCompare addObject:roomAssign.RoomNo];
                        RoomAssignmentModelV2 *raModel = [[RoomAssignmentModelV2 alloc] init];
                        RoomAssignmentModelV2 *compareRaModel = [[RoomAssignmentModelV2 alloc] init];
                        
                        RoomModelV2 *roomModel = [[RoomModelV2 alloc] init];
                        
                        compareRaModel.roomAssignment_Id = [roomAssign.ID_ intValue];
                        compareRaModel.roomAssignment_UserId = (int)userID;
                        
                        raModel.roomAssignment_Id = [roomAssign.ID_ intValue];
                        raModel.roomAssignment_AssignedDate = roomAssign.AssignedTime;
                        //20150420 - Fixed can't show previous date
                        //raModel.roomAssignment_LastCleaningDate = roomAssign.LastRoomCleaningDate;
                        raModel.roomAssignment_LastCleaningDate = roomAssign.CleaningEndTime;
                        raModel.roomAssignment_LastModified = roomAssign.LastModified;
                        raModel.roomAssignment_PostStatus = (int)POST_STATUS_UN_CHANGED;
                        raModel.roomAssignment_Priority = roomAssign.Priority;
                        raModel.roomAssignment_Priority_Sort_Order = roomAssign.PrioritySortOrder;
                        raModel.roomAssignment_RoomId = roomAssign.RoomNo;
                        raModel.roomAssignment_UserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
                        raModel.roomAssignmentHousekeeperId = [roomAssign.HousekeeperID integerValue];
                        raModel.roomAssignmentCleaningCredit = roomAssign.CleaningCredit;
                        //insert room status, cleaning status, inspection status
                        raModel.roomAssignmentRoomStatusId = [roomAssign.RoomStatusID integerValue];
                        raModel.roomAssignmentRoomCleaningStatusId = [roomAssign.CleaningStatusID integerValue];
                        raModel.roomAssignmentRoomInspectionStatusId = [roomAssign.InspectionStatusID integerValue];
                        raModel.roomAssignmentInspectedScore = [roomAssign.InspectionScore integerValue];
                        
                        //insert room expected cleaning time, expected inspection time, guest profile id
                        raModel.roomAssignmentRoomExpectedCleaningTime = [roomAssign.ExpectedCleaningTime integerValue];
                        raModel.roomAssignmentRoomExpectedInspectionTime = [roomAssign.ExpectedInspectionTime integerValue];
                        raModel.roomAssignmentGuestProfileId = [roomAssign.GuestProfileID integerValue];
                        raModel.raGuestArrivalTime = roomAssign.GuestArrivalTime;
                        raModel.raGuestDepartureTime = roomAssign.GuestDepartureTime;
                        
                        //Fix server didn't return kindOfRoomType value
                        NSString *strKindOfRoom = [NSString stringWithFormat:@"%@", roomAssign.KindOfRoom];
                        raModel.raKindOfRoom = [strKindOfRoom intValue];// != nil ? [roomAssign.KindOfRoom integerValue] : kindOfRoomTypeValue;
                        //> 0 ? kindOfRoomTypeValue : [roomAssign.KindOfRoom integerValue];
                        //                        raModel.raKindOfRoom = [roomAssign.KindOfRoom integerValue];
                        //set mockup room local
                        raModel.roomAssignmentZoneId =kindOfRoomTypeValue;
                        NSInteger isMockupRoomValue = [roomAssign.IsMockRoom integerValue];
                        if(isMockupRoomValue == ENUM_TASK_TYPE_NORMAL)
                        {
                            raModel.raIsMockRoom = 0;
                        }
                        else if(isMockupRoomValue == ENUM_TASK_TYPE_REASSIGN)
                        {
                            //2015-05-04 - Compare with Android and fixed OC/OI can't show reassign icon in room list
                            raModel.raIsReassignedRoom = 1;
                            raModel.raIsMockRoom = 0;
                        }
                        else if(isMockupRoomValue >= ENUM_TASK_TYPE_MOCKUP)
                        {
                            raModel.raIsMockRoom = 1;
                        }
                        
                        //2015-05-04 - Compare with Android and fixed OC/OI can't show reassign icon in room list
                        //Hao Tran - Fix can not show reassign image
                        /*if([roomAssign.CleaningStatusID integerValue] == ENUM_CLEANING_STATUS_PENDING_FIRST_TIME
                         && isMockupRoomValue == ENUM_TASK_TYPE_REASSIGN){
                         raModel.raIsReassignedRoom = 1;
                         } else {
                         raModel.raIsReassignedRoom = 0;
                         }*/
                        
                        NSArray *arrayName = [roomAssign.HousekeeperFullname componentsSeparatedByString:@" "];
                        
                        if (arrayName.count > 2) {
                            raModel.roomAssignmentFirstName = [NSString stringWithFormat:@"%@ %@", [arrayName objectAtIndex:0], [arrayName objectAtIndex:arrayName.count - 1]];
                        } else {
                            raModel.roomAssignmentFirstName = roomAssign.HousekeeperFullname;
                        }
                        
                        [self load:compareRaModel];
                        
                        if (compareRaModel.roomAssignment_RoomId.length > 0) {
                            if (![compareRaModel.roomAssignment_LastModified isEqualToString:roomAssign.LastModified]) {
                                
                                //Hao Tran - Remove because this code will not read at first time load WS
                                //Quang edit
                                /*
                                 if([roomAssign.raCleaningStatusID integerValue] == ENUM_CLEANING_STATUS_PENDING_FIRST_TIME
                                 && isMockupRoomValue == ENUM_TASK_TYPE_REASSIGN){
                                 raModel.raIsReassignedRoom = 1;
                                 } else {
                                 raModel.raIsReassignedRoom = 0;
                                 }
                                 */
                                //raModel.raIsReassignedRoom = ([roomAssign.raCleaningStatusID integerValue] == ENUM_CLEANING_STATUS_PENDING_SECOND_TIME?1:0);
                                [self update:raModel];
                            }
                        } else {
                            [self insert:raModel];
                            [self updateIsCheckedFind:[roomAssign.ID_ integerValue]];
                        }
                        
                        //Hao Tran - Remove for enhance loading data
                        /*
                         if(raModel.roomAssignmentRoomStatusId == room_Status_Id_OC ||
                         raModel.roomAssignmentRoomStatusId == room_Status_Id_OD)
                         {
                         [self updateGuestInfo:userID WithRoomAssignID:raModel.roomAssignment_Id];
                         }*/
                        
                        //insert Room Detail data
                        roomModel.room_Id = roomAssign.RoomNo;
                        roomModel.room_AdditionalJob = @"";
                        roomModel.room_Building_Name = roomAssign.BuildingName;
                        roomModel.room_Building_Id = [roomAssign.BuildingID intValue];
                        roomModel.room_building_namelang = roomAssign.BuildingLang;
                        //                    room.room_expected_status_id = ;
                        if(roomAssign.GuestName && ![roomAssign.GuestName isEqualToString:@""]){
                            roomModel.room_Guest_Name = roomAssign.GuestName;
                        }
                        else{
                            NSString *guestName = @"";
                            guestName = [guestName stringByAppendingString:[NSString stringWithFormat:@"%@ %@", roomAssign.GuestFirstName, roomAssign.GuestLastName]];
                            roomModel.room_Guest_Name = guestName;
                            
                        }
                        roomModel.room_GuestReference = roomAssign.GuestPreference;
                        roomModel.room_HotelId = [roomAssign.HTID intValue];
                        //                    room.room_is_re_cleaning = ;
                        //                    room.room_isInpected = ;
                        //                    room.room_isReassigned = ;
                        //                    room.room_Lang = ;
                        //                    room.room_LocationCode = ;
                        roomModel.room_LastModified = roomAssign.LastModified;
                        
                        roomModel.room_PostStatus = (int)POST_STATUS_UN_CHANGED;
                        roomModel.room_remark = roomAssign.Remark;
                        roomModel.room_TypeId = 9;
                        roomModel.room_VIPStatusId = roomAssign.GuestVIPCode;
                        roomModel.room_floor_id =  [roomAssign.FloorID integerValue];
                        
                        
                        RoomModelV2 *compareRoom = [[RoomModelV2 alloc] init];
                        compareRoom.room_Id = roomAssign.RoomNo;
                        
                        [self loadRoomModel:compareRoom];
                        
                        //if (compareRoom.room_VIPStatusId != 0) {
                        if(compareRoom.room_LastModified.length > 0){
                            //Not need post room info because when call get RA, did check to post Room Assignment
                            if (![compareRoom.room_LastModified isEqualToString:roomAssign.LastModified])
                            {
                                [self updateRoomModel:roomModel];
                            }
                        }else {
                            [self insertRoomModel:roomModel];
                        }
                        
                        //Hao Tran - Remove for enhance sync. This command will be called in RoomAssignmentInfoViewController
                        /*
                         compareRoom.room_VIPStatusId = 0;
                         [self getRoomWSByUserID:userID AndRaID:[roomAssign.raID integerValue] AndLastModified:nil AndLastCleaningDate:roomAssign.raLastRoomCleaningDate];
                         */
                        
                        currentPercent += stepPercent;
                        [self updatePercentView:percentView value:currentPercent description:[L_loading_find_room_assignment currentKeyToLanguage]];
                    }
                }
            }
        }
    }
    
    
    [[RoomManagerV2  sharedRoomManager] getWSOOSBlockRoomListByUserId:userID hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId listRoomAssignmentCompare:listRoomsCompare];
    
    if([[UserManagerV2 sharedUserManager].currentUserAccessRight isEnableReleaseRoom]) {
        [[RoomManagerV2  sharedRoomManager] getWSReleaseRoomListByUserId:userID hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId listRoomAssignmentCompare:listRoomsCompare];
    }
    return resultArray;
}

-(BOOL)GetFindRoomDetailsListWSByUserID:(NSInteger)userID AndFloorId:(NSInteger)floorId AndBuildingId:(NSInteger)buildingId AndHotelId:(NSInteger)HTID AndFilterType:(NSInteger) filterType AndGeneralFilterType:(NSString*) generalFilterType AndPercentView:(MBProgressHUD*)percentView
{
    isFindRoom = YES;
    NSMutableArray *listRoomsCompare = nil;
    BOOL result = NO;
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole])
    {
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_GetFindRoomDetailsRequest *request = [[eHousekeepingService_GetFindRoomDetailsRequest alloc] init];
    
    //setting parametters
    [request setUserID:[NSNumber numberWithInteger:userID]];
    [request setFloorID:[NSString stringWithFormat:@"%ld",(long)floorId]];
    [request setBuildingID:[NSNumber numberWithInteger:buildingId]];
    [request setHTID:[NSNumber numberWithInteger:HTID]];
    [request setFilterType:[NSNumber numberWithInteger:filterType]];
    [request setGeneralFilter:generalFilterType];
    
    eHousekeepingService_GetFindRoomDetailsList *roomList = [[eHousekeepingService_GetFindRoomDetailsList alloc] init];
    roomList.GetFindRoomDetailsRequest = request;
    float currentPercent = 0;
    [self updatePercentView:percentView value:currentPercent description:[L_loading_find_room_assignment currentKeyToLanguage]];
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetFindRoomDetailsListUsingParameters:roomList];
    
    NSArray *responseBodyParts = response.bodyParts;
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    
    NSDate *now = [NSDate date];
    NSString *strNow = [dateFormat stringFromDate:now];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [dateFormat setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    now = [dateFormat dateFromString:[NSString stringWithFormat:@"%@ 00:00:00",strNow]];
    
    //Hao Tran - Fixed server didn't return correct KindOfRoomType value (Hard code KindOfRoomType)
    int kindOfRoomTypeValue = 0;
    if(filterType == kindOfRoomType) {
        kindOfRoomTypeValue = [generalFilterType intValue];
    }
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetFindRoomDetailsListResponse class]]) {
            eHousekeepingService_GetFindRoomDetailsListResponse *dataBody = (eHousekeepingService_GetFindRoomDetailsListResponse *)bodyPart;
            if (dataBody != nil) {
                if ([dataBody.GetFindRoomDetailsListResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_OK]] ||
                    [dataBody.GetFindRoomDetailsListResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_NOROOM_ASSIGNMENT_IS_FOUND]]) {
                    result = YES;
                    NSMutableArray *roomAssignments = dataBody.GetFindRoomDetailsListResult.RoomAssignmentListing.RoomAssignmentDetails;
                    listRoomsCompare = [NSMutableArray array];
                    //check and remove missing room assignments
                    //[self checkAndRemoveMissingRoomAssignments:roomAssignments andUserId:userID];
                    float stepPercent = [self caculateStepFromCountingObject:(int)[roomAssignments count]];
                    for (eHousekeepingService_RoomAssignmentDetails *roomAssign in roomAssignments) {
                        
                        //REMOVE FILTER DATE TIME NOW, WE WILL GET ALL ROOM REGARDLESS OF DATE TIME
                        //NSDate *assignedDate = [dateFormat dateFromString:roomAssign.raAssignedTime];
                        //if ([assignedDate compare:now] == NSOrderedAscending) {
                        //  continue;
                        //}
                        [listRoomsCompare addObject:roomAssign.RoomNo];
                        RoomAssignmentModelV2 *raModel = [[RoomAssignmentModelV2 alloc] init];
                        RoomAssignmentModelV2 *compareRaModel = [[RoomAssignmentModelV2 alloc] init];
                        
                        RoomModelV2 *roomModel = [[RoomModelV2 alloc] init];
                        
                        compareRaModel.roomAssignment_Id = [roomAssign.ID_ intValue];
                        compareRaModel.roomAssignment_UserId = (int)userID;
                        
                        raModel.roomAssignment_Id = [roomAssign.ID_ intValue];
                        raModel.roomAssignment_AssignedDate = roomAssign.AssignedTime;
                        //20150420 - Fixed can't show previous date
                        //raModel.roomAssignment_LastCleaningDate = roomAssign.LastRoomCleaningDate;
                        raModel.roomAssignment_LastCleaningDate = roomAssign.CleaningEndTime;
                        raModel.roomAssignment_LastModified = roomAssign.LastModified;
                        raModel.roomAssignment_PostStatus = (int)POST_STATUS_UN_CHANGED;
                        raModel.roomAssignment_Priority = roomAssign.Priority;
                        raModel.roomAssignment_Priority_Sort_Order = roomAssign.PrioritySortOrder;
                        raModel.roomAssignment_RoomId = roomAssign.RoomNo;
                        raModel.roomAssignment_UserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
                        raModel.roomAssignmentHousekeeperId = [roomAssign.HousekeeperID integerValue];
                        raModel.roomAssignmentCleaningCredit = roomAssign.CleaningCredit;
                        //insert room status, cleaning status, inspection status
                        raModel.roomAssignmentRoomStatusId = [roomAssign.RoomStatusID integerValue];
                        raModel.roomAssignmentRoomCleaningStatusId = [roomAssign.CleaningStatusID integerValue];
                        raModel.roomAssignmentRoomInspectionStatusId = [roomAssign.InspectionStatusID integerValue];
                        raModel.roomAssignmentInspectedScore = [roomAssign.InspectionScore integerValue];
                        raModel.roomAssignmentChecklistRemark = roomAssign.ChecklistRemarks;
                        //insert room expected cleaning time, expected inspection time, guest profile id
                        raModel.roomAssignmentRoomExpectedCleaningTime = [roomAssign.ExpectedCleaningTime integerValue];
                        raModel.roomAssignmentRoomExpectedInspectionTime = [roomAssign.ExpectedInspectionTime integerValue];
                        raModel.roomAssignmentGuestProfileId = [roomAssign.GuestProfileID integerValue];
                        raModel.raGuestArrivalTime = roomAssign.GuestArrivalTime;
                        raModel.raGuestDepartureTime = roomAssign.GuestDepartureTime;

                        //Fix server didn't return kindOfRoomType value
                        raModel.raKindOfRoom = kindOfRoomTypeValue > 0 ? kindOfRoomTypeValue : [roomAssign.KindOfRoom integerValue];
                        if (filterType == arrivalType) {
                            raModel.raKindOfRoom = arrivalType;
                        }
                        
//                        raModel.raKindOfRoom = [roomAssign.KindOfRoom integerValue];
                        //set mockup room local
                        NSInteger isMockupRoomValue = [roomAssign.IsMockRoom integerValue];
                        if(isMockupRoomValue == ENUM_TASK_TYPE_NORMAL)
                        {
                            raModel.raIsMockRoom = 0;
                        }
                        else if(isMockupRoomValue == ENUM_TASK_TYPE_REASSIGN)
                        {
                            //2015-05-04 - Compare with Android and fixed OC/OI can't show reassign icon in room list
                            raModel.raIsReassignedRoom = 1;
                            raModel.raIsMockRoom = 0;
                        }
                        else if(isMockupRoomValue >= ENUM_TASK_TYPE_MOCKUP)
                        {
                            raModel.raIsMockRoom = 1;
                        }
                        
                        //2015-05-04 - Compare with Android and fixed OC/OI can't show reassign icon in room list
                        //Hao Tran - Fix can not show reassign image
                        /*if([roomAssign.CleaningStatusID integerValue] == ENUM_CLEANING_STATUS_PENDING_FIRST_TIME
                           && isMockupRoomValue == ENUM_TASK_TYPE_REASSIGN){
                            raModel.raIsReassignedRoom = 1;
                        } else {
                            raModel.raIsReassignedRoom = 0;
                        }*/
                        
                        NSArray *arrayName = [roomAssign.HousekeeperFullname componentsSeparatedByString:@" "];
                        
                        if (arrayName.count > 2) {
                            raModel.roomAssignmentFirstName = [NSString stringWithFormat:@"%@ %@", [arrayName objectAtIndex:0], [arrayName objectAtIndex:arrayName.count - 1]];
                        } else {
                            raModel.roomAssignmentFirstName = roomAssign.HousekeeperFullname;
                        }
                        
                        [self load:compareRaModel];
                        
                        
                        if (compareRaModel.roomAssignment_RoomId.length > 0) {
                            if (![compareRaModel.roomAssignment_LastModified isEqualToString:roomAssign.LastModified]) {
                                
                                //Hao Tran - Remove because this code will not read at first time load WS
                                //Quang edit
                                /*
                                 if([roomAssign.raCleaningStatusID integerValue] == ENUM_CLEANING_STATUS_PENDING_FIRST_TIME
                                 && isMockupRoomValue == ENUM_TASK_TYPE_REASSIGN){
                                 raModel.raIsReassignedRoom = 1;
                                 } else {
                                 raModel.raIsReassignedRoom = 0;
                                 }
                                 */
                                //raModel.raIsReassignedRoom = ([roomAssign.raCleaningStatusID integerValue] == ENUM_CLEANING_STATUS_PENDING_SECOND_TIME?1:0);
                                [self update:raModel];
                            } else {
                                compareRaModel.roomAssignmentChecklistRemark = roomAssign.ChecklistRemarks;
                                [self update:compareRaModel];
                                
                            }
                        } else {
                            [self insert:raModel];
                            [self updateIsCheckedFind:[roomAssign.ID_ integerValue]];
                        }
                        
                        //Hao Tran - Remove for enhance loading data
                        /*
                         if(raModel.roomAssignmentRoomStatusId == room_Status_Id_OC ||
                         raModel.roomAssignmentRoomStatusId == room_Status_Id_OD)
                         {
                         [self updateGuestInfo:userID WithRoomAssignID:raModel.roomAssignment_Id];
                         }*/
                        
                        //insert Room Detail data
                        roomModel.room_Id = roomAssign.RoomNo;
                        roomModel.room_AdditionalJob = @"";
                        roomModel.room_Building_Name = roomAssign.BuildingName;
                        roomModel.room_Building_Id = [roomAssign.BuildingID intValue];
                        roomModel.room_building_namelang = roomAssign.BuildingLang;
                        //                    room.room_expected_status_id = ;
                        if(roomAssign.GuestName && ![roomAssign.GuestName isEqualToString:@""]){
                            roomModel.room_Guest_Name = roomAssign.GuestName;
                        }
                        else{
                            NSString *guestName = @"";
                            guestName = [guestName stringByAppendingString:[NSString stringWithFormat:@"%@ %@", roomAssign.GuestFirstName, roomAssign.GuestLastName]];
                            roomModel.room_Guest_Name = guestName;
                            
                        }
                        roomModel.room_GuestReference = roomAssign.GuestPreference;
                        roomModel.room_HotelId = [roomAssign.HTID intValue];
                        //                    room.room_is_re_cleaning = ;
                        //                    room.room_isInpected = ;
                        //                    room.room_isReassigned = ;
                        //                    room.room_Lang = ;
                        //                    room.room_LocationCode = ;
                        roomModel.room_LastModified = roomAssign.LastModified;
                        
                        roomModel.room_PostStatus = (int)POST_STATUS_UN_CHANGED;
                        roomModel.room_remark = roomAssign.Remark;
                        roomModel.room_TypeId = [roomAssign.RoomTypeID intValue];
                        roomModel.room_VIPStatusId = roomAssign.GuestVIPCode;
                        roomModel.room_floor_id = [roomAssign.FloorID integerValue];
                        
                        
                        RoomModelV2 *compareRoom = [[RoomModelV2 alloc] init];
                        compareRoom.room_Id = roomAssign.RoomNo;
                        
                        [self loadRoomModel:compareRoom];
                        
                        //if (compareRoom.room_VIPStatusId != 0) {
                        if(compareRoom.room_LastModified.length > 0){
                            //Not need post room info because when call get RA, did check to post Room Assignment
                            if (![compareRoom.room_LastModified isEqualToString:roomAssign.LastModified])
                            {
                                [self updateRoomModel:roomModel];
                            }
                        }else {
                            [self insertRoomModel:roomModel];
                        }
                        
                        //Hao Tran - Remove for enhance sync. This command will be called in RoomAssignmentInfoViewController
                        /*
                         compareRoom.room_VIPStatusId = 0;
                         [self getRoomWSByUserID:userID AndRaID:[roomAssign.raID integerValue] AndLastModified:nil AndLastCleaningDate:roomAssign.raLastRoomCleaningDate];
                         */
                        
                        currentPercent += stepPercent;
                        [self updatePercentView:percentView value:currentPercent description:[L_loading_find_room_assignment currentKeyToLanguage]];
                        
                    }
                }
            }
        }
    }
    [[RoomManagerV2  sharedRoomManager] getWSOOSBlockRoomListByUserId:userID hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId listRoomAssignmentCompare:listRoomsCompare];
    
    if([[UserManagerV2 sharedUserManager].currentUserAccessRight isEnableReleaseRoom]) {
        [[RoomManagerV2  sharedRoomManager] getWSReleaseRoomListByUserId:userID hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId listRoomAssignmentCompare:listRoomsCompare];
    }
    return result;
}


-(NSMutableArray*)GetFindRoomDetailsList1WSByUserID:(NSInteger)userID AndFloorId:(NSInteger)floorId AndBuildingId:(NSInteger)buildingId AndHotelId:(NSInteger)HTID AndFilterType:(NSInteger) filterType AndGeneralFilterType:(NSString*) generalFilterType AndPercentView:(MBProgressHUD*)percentView
{
    NSMutableArray *resultArray = [NSMutableArray new];
    isFindRoom = YES;
    NSMutableArray *listRoomsCompare = nil;
//    BOOL result = NO;
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole])
    {
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_GetFindRoomDetailsRequest *request = [[eHousekeepingService_GetFindRoomDetailsRequest alloc] init];
    
    //setting parametters
    [request setUserID:[NSNumber numberWithInteger:userID]];
    [request setFloorID:[NSString stringWithFormat:@"%ld",(long)floorId]];
    [request setBuildingID:[NSNumber numberWithInteger:buildingId]];
    [request setHTID:[NSNumber numberWithInteger:HTID]];
    [request setFilterType:[NSNumber numberWithInteger:filterType]];
    [request setGeneralFilter:generalFilterType];
    
    eHousekeepingService_GetFindRoomDetailsList *roomList = [[eHousekeepingService_GetFindRoomDetailsList alloc] init];
    roomList.GetFindRoomDetailsRequest = request;
    float currentPercent = 0;
    [self updatePercentView:percentView value:currentPercent description:[L_loading_find_room_assignment currentKeyToLanguage]];
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetFindRoomDetailsListUsingParameters:roomList];
    
    NSArray *responseBodyParts = response.bodyParts;
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    
    NSDate *now = [NSDate date];
    NSString *strNow = [dateFormat stringFromDate:now];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [dateFormat setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    now = [dateFormat dateFromString:[NSString stringWithFormat:@"%@ 00:00:00",strNow]];
    
    //Hao Tran - Fixed server didn't return correct KindOfRoomType value (Hard code KindOfRoomType)
    int kindOfRoomTypeValue = 0;
    if(filterType == kindOfRoomType) {
        kindOfRoomTypeValue = [generalFilterType intValue];
    }
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetFindRoomDetailsListResponse class]]) {
            eHousekeepingService_GetFindRoomDetailsListResponse *dataBody = (eHousekeepingService_GetFindRoomDetailsListResponse *)bodyPart;
            if (dataBody != nil) {
                if ([dataBody.GetFindRoomDetailsListResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_OK]] ||
                    [dataBody.GetFindRoomDetailsListResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_NOROOM_ASSIGNMENT_IS_FOUND]]) {
                    
                    NSMutableArray *roomAssignments = dataBody.GetFindRoomDetailsListResult.RoomAssignmentListing.RoomAssignmentDetails;
                    listRoomsCompare = [NSMutableArray array];
                    //check and remove missing room assignments
                    //[self checkAndRemoveMissingRoomAssignments:roomAssignments andUserId:userID];
                    float stepPercent = [self caculateStepFromCountingObject:(int)[roomAssignments count]];
                    for (eHousekeepingService_RoomAssignmentDetails *roomAssign in roomAssignments) {
                        
                        //REMOVE FILTER DATE TIME NOW, WE WILL GET ALL ROOM REGARDLESS OF DATE TIME
                        //NSDate *assignedDate = [dateFormat dateFromString:roomAssign.raAssignedTime];
                        //if ([assignedDate compare:now] == NSOrderedAscending) {
                        //  continue;
                        //}
                        [listRoomsCompare addObject:roomAssign.RoomNo];
                        RoomAssignmentModelV2 *raModel = [[RoomAssignmentModelV2 alloc] init];
                        RoomAssignmentModelV2 *compareRaModel = [[RoomAssignmentModelV2 alloc] init];
                        
                        RoomModelV2 *roomModel = [[RoomModelV2 alloc] init];
                        
                        compareRaModel.roomAssignment_Id = [roomAssign.ID_ intValue];
                        compareRaModel.roomAssignment_UserId = (int)userID;
                        
                        raModel.roomAssignment_Id = [roomAssign.ID_ intValue];
                        raModel.roomAssignment_AssignedDate = roomAssign.AssignedTime;
                        //20150420 - Fixed can't show previous date
                        //raModel.roomAssignment_LastCleaningDate = roomAssign.LastRoomCleaningDate;
                        raModel.roomAssignment_LastCleaningDate = roomAssign.CleaningEndTime;
                        raModel.roomAssignment_LastModified = roomAssign.LastModified;
                        raModel.roomAssignment_PostStatus = (int)POST_STATUS_UN_CHANGED;
                        raModel.roomAssignment_Priority = roomAssign.Priority;
                        raModel.roomAssignment_Priority_Sort_Order = roomAssign.PrioritySortOrder;
                        raModel.roomAssignment_RoomId = roomAssign.RoomNo;
                        raModel.roomAssignment_UserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
                        raModel.roomAssignmentHousekeeperId = [roomAssign.HousekeeperID integerValue];
                        raModel.roomAssignmentCleaningCredit = roomAssign.CleaningCredit;
                        //insert room status, cleaning status, inspection status
                        raModel.roomAssignmentRoomStatusId = [roomAssign.RoomStatusID integerValue];
                        raModel.roomAssignmentRoomCleaningStatusId = [roomAssign.CleaningStatusID integerValue];
                        raModel.roomAssignmentRoomInspectionStatusId = [roomAssign.InspectionStatusID integerValue];
                        raModel.roomAssignmentInspectedScore = [roomAssign.InspectionScore integerValue];
                        
                        //insert room expected cleaning time, expected inspection time, guest profile id
                        raModel.roomAssignmentRoomExpectedCleaningTime = [roomAssign.ExpectedCleaningTime integerValue];
                        raModel.roomAssignmentRoomExpectedInspectionTime = [roomAssign.ExpectedInspectionTime integerValue];
                        raModel.roomAssignmentGuestProfileId = [roomAssign.GuestProfileID integerValue];
                        raModel.raGuestArrivalTime = roomAssign.GuestArrivalTime;
                        raModel.raGuestDepartureTime = roomAssign.GuestDepartureTime;
                        
                        //Fix server didn't return kindOfRoomType value
                        raModel.raKindOfRoom = kindOfRoomTypeValue > 0 ? kindOfRoomTypeValue : [roomAssign.KindOfRoom integerValue];
                        //                        raModel.raKindOfRoom = [roomAssign.KindOfRoom integerValue];
                        //set mockup room local
                        NSInteger isMockupRoomValue = [roomAssign.IsMockRoom integerValue];
                        if(isMockupRoomValue == ENUM_TASK_TYPE_NORMAL)
                        {
                            raModel.raIsMockRoom = 0;
                        }
                        else if(isMockupRoomValue == ENUM_TASK_TYPE_REASSIGN)
                        {
                            //2015-05-04 - Compare with Android and fixed OC/OI can't show reassign icon in room list
                            raModel.raIsReassignedRoom = 1;
                            raModel.raIsMockRoom = 0;
                        }
                        else if(isMockupRoomValue >= ENUM_TASK_TYPE_MOCKUP)
                        {
                            raModel.raIsMockRoom = 1;
                        }
                        
                        //2015-05-04 - Compare with Android and fixed OC/OI can't show reassign icon in room list
                        //Hao Tran - Fix can not show reassign image
                        /*if([roomAssign.CleaningStatusID integerValue] == ENUM_CLEANING_STATUS_PENDING_FIRST_TIME
                         && isMockupRoomValue == ENUM_TASK_TYPE_REASSIGN){
                         raModel.raIsReassignedRoom = 1;
                         } else {
                         raModel.raIsReassignedRoom = 0;
                         }*/
                        
                        NSArray *arrayName = [roomAssign.HousekeeperFullname componentsSeparatedByString:@" "];
                        
                        if (arrayName.count > 2) {
                            raModel.roomAssignmentFirstName = [NSString stringWithFormat:@"%@ %@", [arrayName objectAtIndex:0], [arrayName objectAtIndex:arrayName.count - 1]];
                        } else {
                            raModel.roomAssignmentFirstName = roomAssign.HousekeeperFullname;
                        }
                        
                        [self load:compareRaModel];
                        
                        if (compareRaModel.roomAssignment_RoomId.length > 0) {
                            if (![compareRaModel.roomAssignment_LastModified isEqualToString:roomAssign.LastModified]) {
                                
                                //Hao Tran - Remove because this code will not read at first time load WS
                                //Quang edit
                                /*
                                 if([roomAssign.raCleaningStatusID integerValue] == ENUM_CLEANING_STATUS_PENDING_FIRST_TIME
                                 && isMockupRoomValue == ENUM_TASK_TYPE_REASSIGN){
                                 raModel.raIsReassignedRoom = 1;
                                 } else {
                                 raModel.raIsReassignedRoom = 0;
                                 }
                                 */
                                //raModel.raIsReassignedRoom = ([roomAssign.raCleaningStatusID integerValue] == ENUM_CLEANING_STATUS_PENDING_SECOND_TIME?1:0);
                                [self update:raModel];
                            }
                        } else {
                            [self insert:raModel];
                            [self updateIsCheckedFind:[roomAssign.ID_ integerValue]];
                        }
                        
                        //Hao Tran - Remove for enhance loading data
                        /*
                         if(raModel.roomAssignmentRoomStatusId == room_Status_Id_OC ||
                         raModel.roomAssignmentRoomStatusId == room_Status_Id_OD)
                         {
                         [self updateGuestInfo:userID WithRoomAssignID:raModel.roomAssignment_Id];
                         }*/
                        
                        //insert Room Detail data
                        roomModel.room_Id = roomAssign.RoomNo;
                        roomModel.room_AdditionalJob = @"";
                        roomModel.room_Building_Name = roomAssign.BuildingName;
                        roomModel.room_Building_Id = [roomAssign.BuildingID intValue];
                        roomModel.room_building_namelang = roomAssign.BuildingLang;
                        //                    room.room_expected_status_id = ;
                        if(roomAssign.GuestName && ![roomAssign.GuestName isEqualToString:@""]){
                            roomModel.room_Guest_Name = roomAssign.GuestName;
                        }
                        else{
                            NSString *guestName = @"";
                            guestName = [guestName stringByAppendingString:[NSString stringWithFormat:@"%@ %@", roomAssign.GuestFirstName, roomAssign.GuestLastName]];
                            roomModel.room_Guest_Name = guestName;
                            
                        }
                        roomModel.room_GuestReference = roomAssign.GuestPreference;
                        roomModel.room_HotelId = [roomAssign.HTID intValue];
                        //                    room.room_is_re_cleaning = ;
                        //                    room.room_isInpected = ;
                        //                    room.room_isReassigned = ;
                        //                    room.room_Lang = ;
                        //                    room.room_LocationCode = ;
                        roomModel.room_LastModified = roomAssign.LastModified;
                        
                        roomModel.room_PostStatus = (int)POST_STATUS_UN_CHANGED;
                        roomModel.room_remark = roomAssign.Remark;
                        roomModel.room_TypeId = [roomAssign.RoomTypeID intValue];
                        roomModel.room_VIPStatusId = roomAssign.GuestVIPCode;
                        roomModel.room_floor_id = [roomAssign.FloorID integerValue];
                        
                        
                        RoomModelV2 *compareRoom = [[RoomModelV2 alloc] init];
                        compareRoom.room_Id = roomAssign.RoomNo;
                        
                        [self loadRoomModel:compareRoom];
                        
                        //if (compareRoom.room_VIPStatusId != 0) {
                        if(compareRoom.room_LastModified.length > 0){
                            //Not need post room info because when call get RA, did check to post Room Assignment
                            if (![compareRoom.room_LastModified isEqualToString:roomAssign.LastModified])
                            {
                                [self updateRoomModel:roomModel];
                            }
                        }else {
                            [self insertRoomModel:roomModel];
                        }
                        
                        //Hao Tran - Remove for enhance sync. This command will be called in RoomAssignmentInfoViewController
                        /*
                         compareRoom.room_VIPStatusId = 0;
                         [self getRoomWSByUserID:userID AndRaID:[roomAssign.raID integerValue] AndLastModified:nil AndLastCleaningDate:roomAssign.raLastRoomCleaningDate];
                         */
                        
                        currentPercent += stepPercent;
                        [self updatePercentView:percentView value:currentPercent description:[L_loading_find_room_assignment currentKeyToLanguage]];
                        
                        
                        NSMutableDictionary *row = [[NSMutableDictionary alloc] init];
                        [row setValue:[NSString stringWithFormat:@"%d", (int)userID] forKey:kRaUserId];
                        [row setValue:[NSString stringWithFormat:@"%d", (int)roomAssign.RoomNo] forKey:kRoomNo];
                        [row setValue:[NSString stringWithFormat:@"%d", (int)roomAssign.RoomStatusID] forKey:kRMStatus];
                        [row setValue:[NSString stringWithFormat:@"%d", (int)roomAssign.CleaningStatusID] forKey:kCleaningStatusID];
                        [row setValue:roomAssign.GuestName forKey:kGuestName];
                        [row setValue:roomAssign.PrioritySortOrder forKey:kRaPrioritySortOrder];
                        [row setValue:[NSString stringWithFormat:@"%d", (int)raModel.roomAssignment_Id] forKey:kRoomAssignmentID];
                        [row setValue:raModel.roomAssignmentFirstName forKey:kRAFirstName];
                        [row setValue:raModel.roomAssignmentLastName forKey:kRALastName];
                        [row setValue:roomAssign.GuestFirstName forKey:kGuestFirstName];
                        [row setValue:roomAssign.GuestLastName forKey:kGuestLastName];
                        [row setValue:roomAssign.GuestArrivalTime forKey:kRaGuestArrivalTime];
                        [row setValue:roomAssign.GuestDepartureTime forKey:kRaGuestDepartureTime];
                        [row setValue:roomAssign.KindOfRoom forKey:kRaKindOfRoom];
                        [row setValue:roomAssign.IsMockRoom forKey:kRaIsMockRoom];
                        
                        //                        [row setValue:roomAssignmentTime forKey:kRoomAssignmentTime];
                        [row setValue:roomAssign.AssignedTime forKey:kRoomAssignmentTime];
                        [row setValue:[NSString stringWithFormat:@"%d", (int)roomAssign.RoomStatusID] forKey:kRoomStatusId];
                        [row setValue:[NSString stringWithFormat:@"%d", (int)roomAssign.HousekeeperID] forKey:kraHousekeeperID];
                        [row setValue:roomAssign.LastRoomCleaningDate forKey:kLastCleaningDate];
                        [row setValue:[NSString stringWithFormat:@"%d", (int)roomAssign.RoomTypeID] forKey:kRoomType];
                        
//                        [row setValue:[[NSString alloc] initWithFormat:@"%i",(int)raIsReassignedRoom] forKey:kRaIsReassignedRoom];
//                        [row setValue:roomVipStatus forKey:kVIP];
//                        [row setValue:image_roomStatus forKey:kRoomStatusImage];
//                        [row setValue:image_cleaningStatus forKey:kCleaningStatus];
                        
                        [resultArray addObject:row];
                    }
                }
            }
        }
    }
    [[RoomManagerV2  sharedRoomManager] getWSOOSBlockRoomListByUserId:userID hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId listRoomAssignmentCompare:listRoomsCompare];
    
    if([[UserManagerV2 sharedUserManager].currentUserAccessRight isEnableReleaseRoom]) {
        [[RoomManagerV2  sharedRoomManager] getWSReleaseRoomListByUserId:userID hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId listRoomAssignmentCompare:listRoomsCompare];
    }
    return resultArray;
}


-(BOOL)GetFindRoomAssignmentListWSByUserID:(NSInteger)userID AndRoomstatus:(NSInteger)roomStatusID AndFilterType:(NSInteger)filterType AndLastModified:(NSString *)strLastModified AndHotelID:(NSString*)hotelId AndPercentView:(MBProgressHUD *)percentView{
    BOOL result = NO;
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole])
    {
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_GetFindRoomAssignmentLists *request = [[eHousekeepingService_GetFindRoomAssignmentLists alloc] init];
    
    [request setIntSupervisorID:[NSNumber numberWithInteger:userID]];
    [request setIntStatusID:[NSNumber numberWithInteger:roomStatusID]];
    [request setIntFilterType:[NSNumber numberWithInteger:filterType]];
    [request setStrHotel_ID:hotelId];
    if (strLastModified) {
        [request setStrLastModified:strLastModified];
    }
    
    //    [request setIntRoomStatus:@""];
    //    [request setIntInspectionStatus:@""];
    
//    LogObject *logObj = [[LogObject alloc] init];
//    logObj.dateTime = [NSDate date];
//    logObj.log = [NSString stringWithFormat:@"<%i><GetFindRoomAssignmentListWSByUserID><intUsrID:%i intRoomStatusID:%i lastModified:%@>",userID,userID,roomStatusID,strLastModified];
//    LogFileManager *logManager = [[LogFileManager alloc] init];
//    [logManager save:logObj];
    
    float currentPercent = 0;
    [self updatePercentView:percentView value:currentPercent description:[L_loading_find_room_assignment currentKeyToLanguage]];
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetFindRoomAssignmentListsUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    
    NSDate *now = [NSDate date];
    NSString *strNow = [dateFormat stringFromDate:now];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [dateFormat setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    now = [dateFormat dateFromString:[NSString stringWithFormat:@"%@ 00:00:00",strNow]];
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetFindRoomAssignmentListsResponse class]]) {
            eHousekeepingService_GetFindRoomAssignmentListsResponse *dataBody = (eHousekeepingService_GetFindRoomAssignmentListsResponse *)bodyPart;
            if (dataBody != nil) {
                if ([dataBody.GetFindRoomAssignmentListsResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_OK]]) {
                    result = YES;
                    NSMutableArray *roomAssignments = dataBody.GetFindRoomAssignmentListsResult.RoomAssignmentList.RoomAssign;
                    
                    //check and remove missing room assignments
                    //[self checkAndRemoveMissingRoomAssignments:roomAssignments andUserId:userID];
                    float stepPercent = [self caculateStepFromCountingObject:(int)[roomAssignments count]];
                    for (eHousekeepingService_RoomAssign *roomAssign in roomAssignments) {

                        //it will not filter with current date
                        //NSDate *assignedDate = [dateFormat dateFromString:roomAssign.raAssignedTime];
                        //if ([assignedDate compare:now] == NSOrderedAscending) {
                        //    continue;
                        //}
                        
                        RoomAssignmentModelV2 *raModel = [[RoomAssignmentModelV2 alloc] init];
                        RoomAssignmentModelV2 *compareRaModel = [[RoomAssignmentModelV2 alloc] init];
                        
                        RoomModelV2 *roomModel = [[RoomModelV2 alloc] init];

                        compareRaModel.roomAssignment_Id = [roomAssign.raID intValue];
                        compareRaModel.roomAssignment_UserId = (int)userID;
                                                
                        raModel.roomAssignment_Id = [roomAssign.raID intValue];
                        raModel.roomAssignment_AssignedDate = roomAssign.raAssignedTime;
                        
                        //20150420 - Fixed can't show previous date
                        //raModel.roomAssignment_LastCleaningDate = roomAssign.raLastRoomCleaningDate;
                        raModel.roomAssignment_LastCleaningDate = roomAssign.raCleanEndTm;
                        raModel.roomAssignment_LastModified = roomAssign.raLastModified;
                        raModel.roomAssignment_PostStatus = (int)POST_STATUS_UN_CHANGED;
                        raModel.roomAssignment_Priority = [roomAssign.raPriority intValue];
                        raModel.roomAssignment_Priority_Sort_Order = [roomAssign.raPrioritySortOrder integerValue];
                        raModel.roomAssignment_RoomId = roomAssign.raRoomNo;
                        raModel.roomAssignment_UserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
                        raModel.roomAssignmentHousekeeperId = [roomAssign.raHousekeeperID integerValue];
                        
                        //insert room status, cleaning status, inspection status
                        raModel.roomAssignmentRoomStatusId = [roomAssign.raRoomStatusID integerValue];
                        raModel.roomAssignmentRoomCleaningStatusId = [roomAssign.raCleaningStatusID integerValue];
                        raModel.roomAssignmentRoomInspectionStatusId = [roomAssign.raInspectedStatus integerValue];
                        raModel.roomAssignmentInspectedScore = [roomAssign.raInspectedScore integerValue];
                        
                        //insert room expected cleaning time, expected inspection time, guest profile id
                        raModel.roomAssignmentRoomExpectedCleaningTime = [roomAssign.raExpectedCleaningTime integerValue];
                        raModel.roomAssignmentRoomExpectedInspectionTime = [roomAssign.raExpectedInspectTime integerValue];
                        raModel.roomAssignmentGuestProfileId = [roomAssign.raGuestProfileID integerValue];
                        raModel.raGuestArrivalTime = roomAssign.raGuestArrivalTime;
                        raModel.raGuestDepartureTime = roomAssign.raGuestDepartureTime;
                        raModel.raKindOfRoom = [roomAssign.raKindOfRoom integerValue];
                        
//                        raModel.raIsMockRoom = [roomAssign.raIsMockRoom integerValue];
                        //set mockup room local
                        NSInteger isMockupRoomValue = [roomAssign.raIsMockRoom integerValue];
                        if(isMockupRoomValue == ENUM_TASK_TYPE_NORMAL)
                        {
                            raModel.raIsMockRoom = 0;
                        }
                        else if(isMockupRoomValue == ENUM_TASK_TYPE_REASSIGN)
                        {
                            raModel.raIsMockRoom = 0;
//                            raModel.raIsReassignedRoom = 1;
                        }
                        else if(isMockupRoomValue >= ENUM_TASK_TYPE_MOCKUP)
                        {
                            raModel.raIsMockRoom = 1;
                        }
                        
                        NSArray *arrayName = [roomAssign.raFullName componentsSeparatedByString:@" "];
                        
                        if (arrayName.count > 2) {
                            raModel.roomAssignmentFirstName = [NSString stringWithFormat:@"%@ %@", [arrayName objectAtIndex:0], [arrayName objectAtIndex:arrayName.count - 1]];
                        } else {
                            raModel.roomAssignmentFirstName = roomAssign.raFullName;
                        }
                        
                        [self load:compareRaModel];
                        
                        if (compareRaModel.roomAssignment_RoomId.length > 0) {
                            if (![compareRaModel.roomAssignment_LastModified isEqualToString:roomAssign.raLastModified]) {
                                //Quang edit
                                if([roomAssign.raCleaningStatusID integerValue] == ENUM_CLEANING_STATUS_PENDING_FIRST_TIME && isMockupRoomValue == ENUM_TASK_TYPE_REASSIGN)
                                    raModel.raIsReassignedRoom = 1;
                                else
                                    raModel.raIsReassignedRoom = 0;
                                
//                                raModel.raIsReassignedRoom = ([roomAssign.raCleaningStatusID integerValue] == ENUM_CLEANING_STATUS_PENDING_SECOND_TIME?1:0);
                                [self update:raModel];
                            }
                        } else {
                            [self insert:raModel];
                        }
                        
//                        BOOL tempResult = YES;
//                        tempResult =
                        if(raModel.roomAssignmentRoomStatusId == room_Status_Id_OC ||
                           raModel.roomAssignmentRoomStatusId == room_Status_Id_OD)
                        {
                            //Removed because of changing WS for Gueset
                            //[self updateGuestInfo:userID WithRoomAssignID:raModel.roomAssignment_Id];
                            [self updateGuestInfoByUserId:(int)userID hotelId:[roomAssign.raHotelID intValue] roomNumber:roomAssign.raRoomNo];
                        }
                        
                        compareRaModel.roomAssignment_RoomId = @"";
                        //insert Room Detail data
                        roomModel.room_Id = roomAssign.raRoomNo;
                        roomModel.room_AdditionalJob = roomAssign.raAdditionalJob;
                        roomModel.room_Building_Name = roomAssign.raBuildingName;
                        roomModel.room_Building_Id = [roomAssign.raBuildingID intValue];
                        roomModel.room_building_namelang = roomAssign.rabuildingLang;
                        //                    room.room_expected_status_id = ;
                        roomModel.room_Guest_Name = [NSString stringWithFormat:@"%@%@", roomAssign.raGuestFirstName == nil ? @"" : [NSString stringWithFormat:@"%@ ", roomAssign.raGuestFirstName], roomAssign.raGuestLastName == nil ? @"" : [NSString stringWithFormat:@"%@", roomAssign.raGuestLastName]];
                        roomModel.room_GuestReference = roomAssign.raGuestPreference;
                        roomModel.room_HotelId = [roomAssign.raHotelID intValue];
                        //                    room.room_is_re_cleaning = ;
                        //                    room.room_isInpected = ;
                        //                    room.room_isReassigned = ;
                        //                    room.room_Lang = ;
                        //                    room.room_LocationCode = ;
                        roomModel.room_LastModified = roomAssign.raLastModified;
                        
                        roomModel.room_PostStatus = (int)POST_STATUS_UN_CHANGED;
                        roomModel.room_remark = roomAssign.raRemark;
                        roomModel.room_TypeId = [roomAssign.raRoomTypeID intValue];
                        roomModel.room_VIPStatusId = roomAssign.raVIP;
                        roomModel.room_floor_id = [roomAssign.raFloorID integerValue];
                        
                        
                        RoomModelV2 *compareRoom = [[RoomModelV2 alloc] init];
                        compareRoom.room_Id = roomAssign.raRoomNo;
                        
                        [self loadRoomModel:compareRoom];
                        
                        //if (compareRoom.room_VIPStatusId != 0) {
                        if(compareRoom.room_LastModified.length > 0){
                            //Not need post room info because when call get RA, did check to post Room Assignment
                            if (![compareRoom.room_LastModified isEqualToString:roomAssign.raLastModified])
                            {
                                [self updateRoomModel:roomModel];
                            }
                        }else {
                            [self insertRoomModel:roomModel];
                        }
                        
                        //Hao Tran - Remove for enhance sync. This command will be called in RoomAssignmentInfoViewController
                        /*
                        compareRoom.room_VIPStatusId = 0;
                        [self getRoomWSByUserID:userID AndRaID:[roomAssign.raID integerValue] AndLastModified:nil AndLastCleaningDate:roomAssign.raLastRoomCleaningDate];
                        */
                        
                        currentPercent += stepPercent;
                        [self updatePercentView:percentView value:currentPercent description:[L_loading_find_room_assignment currentKeyToLanguage]];
                    }
                    
                    
                }
            }
        }
    }
    return result;
}

//-(BOOL)GetFindRoomAssignmentListWSByUserID:(NSInteger)userID AndRoomstatus:(NSInteger)roomStatusID AndLastModified:(NSString *)strLastModified {
//    BOOL result = NO;
//    
//    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
//    binding.logXMLInOut = YES;
//    
//    eHousekeepingService_GetFindRoomAssignmentList *request = [[eHousekeepingService_GetFindRoomAssignmentList alloc] init];
//    
//    [request setIntSupervisorID:[NSNumber numberWithInteger:userID]];
//    [request setIntRoomStatusID:[NSNumber numberWithInteger:roomStatusID]];
//    if (strLastModified) {
//        [request setStrLastModified:strLastModified];
//    }
//    
//    //    [request setIntRoomStatus:@""];
//    //    [request setIntInspectionStatus:@""];
//    
//    //    LogObject *logObj = [[LogObject alloc] init];
//    //    logObj.dateTime = [NSDate date];
//    //    logObj.log = [NSString stringWithFormat:@"<%i><GetFindRoomAssignmentListWSByUserID><intUsrID:%i intRoomStatusID:%i lastModified:%@>",userID,userID,roomStatusID,strLastModified];
//    //    LogFileManager *logManager = [[LogFileManager alloc] init];
//    //    [logManager save:logObj];
//    
//    eHousekeepingServiceSoap12BindingResponse *response = [binding GetFindRoomAssignmentListUsingParameters:request];
//    
//    NSArray *responseBodyParts = response.bodyParts;
//    
//    for (id bodyPart in responseBodyParts) {
//        if ([bodyPart isKindOfClass:[eHousekeepingService_GetFindRoomAssignmentListResponse class]]) {
//            eHousekeepingService_GetFindRoomAssignmentListResponse *dataBody = (eHousekeepingService_GetFindRoomAssignmentListResponse *)bodyPart;
//            if (dataBody != nil) {
//                if ([dataBody.GetFindRoomAssignmentListResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_OK]]) {
//                    result = YES;
//                    NSMutableArray *roomAssignments = dataBody.GetFindRoomAssignmentListResult.RoomAssignmentList.RoomAssign;
//                    
//                    RoomAssignmentModelV2 *raModel = [[RoomAssignmentModelV2 alloc] init];
//                    RoomAssignmentModelV2 *compareRaModel = [[RoomAssignmentModelV2 alloc] init];
//                    
//                    RoomModelV2 *roomModel = [[RoomModelV2 alloc] init];
//                    
//                    //check and remove missing room assignments
//                    //[self checkAndRemoveMissingRoomAssignments:roomAssignments andUserId:userID];
//                    
//                    for (eHousekeepingService_RoomAssign *roomAssign in roomAssignments) {
//                        compareRaModel.roomAssignment_Id = [roomAssign.raID integerValue];
//                        compareRaModel.roomAssignment_UserId = userID;
//                        
//                        raModel.roomAssignment_Id = [roomAssign.raID integerValue];
//                        raModel.roomAssignment_AssignedDate = roomAssign.raAssignedTime;
//                        raModel.roomAssignment_LastModified = roomAssign.raLastModified;
//                        raModel.roomAssignment_PostStatus = POST_STATUS_UN_CHANGED;
//                        raModel.roomAssignment_Priority = [roomAssign.raPriority integerValue];
//                        raModel.roomAssignment_Priority_Sort_Order = [roomAssign.raPrioritySortOrder integerValue];
//                        raModel.roomAssignment_RoomId = [roomAssign.raRoomNo integerValue];
//                        raModel.roomAssignment_UserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
//                        raModel.roomAssignmentHousekeeperId = [roomAssign.raHousekeeperID integerValue];
//                        
//                        //insert room status, cleaning status, inspection status
//                        raModel.roomAssignmentRoomStatusId = [roomAssign.raRoomStatusID integerValue];
//                        raModel.roomAssignmentRoomCleaningStatusId = [roomAssign.raCleaningStatusID integerValue];
//                        raModel.roomAssignmentRoomInspectionStatusId = [roomAssign.raInspectedStatus integerValue];
//                        
//                        //insert room expected cleaning time, expected inspection time, guest profile id
//                        raModel.roomAssignmentRoomExpectedCleaningTime = [roomAssign.raExpectedCleaningTime integerValue];
//                        raModel.roomAssignmentRoomExpectedInspectionTime = [roomAssign.raExpectedInspectTime integerValue];
//                        raModel.roomAssignmentGuestProfileId = [roomAssign.raGuestProfileID integerValue];
//                        
//                        NSArray *arrayName = [roomAssign.raFullName componentsSeparatedByString:@" "];
//                        
//                        if (arrayName.count > 2) {
//                            raModel.roomAssignmentFirstName = [NSString stringWithFormat:@"%@ %@", [arrayName objectAtIndex:0], [arrayName objectAtIndex:arrayName.count - 1]];
//                        } else {
//                            raModel.roomAssignmentFirstName = roomAssign.raFullName;
//                        }
//                        
//                        [self load:compareRaModel];
//                        
//                        if (compareRaModel.roomAssignment_RoomId != 0) {
//                            if (![compareRaModel.roomAssignment_LastModified isEqualToString:roomAssign.raLastModified]) {
//                                [self update:raModel];
//                            }
//                        } else {
//                            [self insert:raModel];
//                        }
//                        
//                        BOOL tempResult = YES;
//                        tempResult = [self updateGuestInfo:userID WithRoomAssignID:raModel.roomAssignment_Id];
//                        
//                        //                        if (tempResult == NO) {
//                        //                            result = tempResult;
//                        //                        }
//                        
//                        compareRaModel.roomAssignment_RoomId = 0;
//                        
//                        //insert Room Detail data
//                        roomModel.room_Id = [roomAssign.raRoomNo integerValue];
//                        roomModel.room_AdditionalJob = roomAssign.raAdditionalJob;
//                        roomModel.room_Building_Name = roomAssign.raBuildingName;
//                        roomModel.room_Building_Id = [roomAssign.raBuildingID intValue];
//                        roomModel.room_building_namelang = roomAssign.rabuildingLang;
//                        //                    room.room_expected_status_id = ;
//                        roomModel.room_Guest_Name = [NSString stringWithFormat:@"%@%@", roomAssign.raGuestFirstName == nil ? @"" : [NSString stringWithFormat:@"%@ ", roomAssign.raGuestFirstName], roomAssign.raGuestLastName == nil ? @"" : [NSString stringWithFormat:@"%@", roomAssign.raGuestLastName]];
//                        roomModel.room_GuestReference = roomAssign.raGuestPreference;
//                        roomModel.room_HotelId = [roomAssign.raHotelID integerValue];
//                        //                    room.room_is_re_cleaning = ;
//                        //                    room.room_isInpected = ;
//                        //                    room.room_isReassigned = ;
//                        //                    room.room_Lang = ;
//                        //                    room.room_LocationCode = ;
//                        roomModel.room_LastModified = roomAssign.raLastModified;
//                        
//                        roomModel.room_PostStatus = POST_STATUS_UN_CHANGED;
//                        roomModel.room_remark = roomAssign.raRemark;
//                        roomModel.room_TypeId = [roomAssign.raRoomTypeID integerValue];
//                        roomModel.room_VIPStatusId = [roomAssign.raVIP integerValue];
//                        roomModel.room_zone_id = [roomAssign.raFloorID integerValue];
//                        
//                        
//                        RoomModelV2 *compareRoom = [[RoomModelV2 alloc] init];
//                        compareRoom.room_Id = [roomAssign.raRoomNo integerValue];
//                        
//                        [self loadRoomModel:compareRoom];
//                        
//                        if (compareRoom.room_VIPStatusId != 0) {
//                            //Not need post room info because when call get RA, did check to post Room Assignment
//                            if (![compareRoom.room_LastModified isEqualToString:roomAssign.raLastModified]) {
//                                [self updateRoomModel:roomModel];
//                            }
//                        } else {
//                            [self insertRoomModel:roomModel];
//                        }
//                        
//                        compareRoom.room_VIPStatusId = 0;
//                        
//                        [self getRoomWSByUserID:userID AndRaID:[roomAssign.raID integerValue] AndLastModified:nil AndLastCleaningDate:roomAssign.raLastRoomCleaningDate];
//                    }
//                }
//            }
//        }
//    }
//    return result;
//}
-(int)postRoomInspectionWSByUserID:(NSInteger)userID AndRaModel:(RoomAssignmentModelV2 *)model WithStringRemark:(NSString *)remark {
    int result = RESPONSE_STATUS_NO_NETWORK;
    
    //Hao Tran - Remove for post all kind of room status
    /*
    //will not send inspection information OC room
    //return success notice
    if(model.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OC){
        return YES;
    }
    */
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole])
    {
        [binding setLogXMLInOut:YES];
    }
    eHousekeepingService_UpdateInspection *request = [[eHousekeepingService_UpdateInspection alloc] init];
    
    [request setIntUsrID:[NSNumber numberWithInteger:userID]];
    [request setIntRoomAssignID:[NSNumber numberWithInteger:model.roomAssignment_Id]];
    [request setStrChecklistRemarks:remark];
    if (model.roomAssignment_PostStatus == POST_STATUS_SAVED_UNPOSTED) {
        //load overall score
        CheckListManagerV2 *chkmanager = [[CheckListManagerV2 alloc] init];
        CheckListModelV2 *chkmodel = [[CheckListModelV2 alloc] init];
        chkmodel.chkListId = model.roomAssignment_Id;
        chkmodel.chkListUserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
        [chkmanager loadCheckListData:chkmodel];
        [request setIntOverallScore:[NSNumber numberWithInteger:chkmodel.chkCore]];
        
        //set room status
        [request setIntRoomStatus:[NSNumber numberWithInteger:model.roomAssignmentRoomStatusId]];
        
        //set inspected status
        [request setIntInspectStatus:[NSNumber numberWithInteger:model.roomAssignmentRoomInspectionStatusId]];
        
        RoomRecordModelV2 *record = [[RoomRecordModelV2 alloc] init];
       
        record.rrec_room_assignment_id = model.roomAssignment_Id;
        record.rrec_User_Id = (int)userID;
        [[RoomManagerV2 sharedRoomManager] loadRoomRecordDataByRoomIdAndUserId:record];
        
        //always post start time & end time inspected
        [request setStrInspectedStartTime:record.rrec_Inspection_Start_Time];
        [request setStrInspectedEndTime:record.rrec_Inspection_End_Time];
        [request setStrPauseTime:@"1900-01-01 00:00:00"];
        if(record != nil && record.rrec_Remark.length > 0) {
            [request setStrInspectRemark:record.rrec_Remark];
        }
        
        //set ra total cleaning time
        NSDate *startDate = nil;
        NSDate *endDate = nil;
        NSDateFormatter *f = [[NSDateFormatter alloc] init];
        [f setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        startDate = [f dateFromString:record.rrec_Inspection_Start_Time];
        endDate = [f dateFromString:record.rrec_Inspection_End_Time];
        
        //Hao Tran - Remove for fix wrong data
        /*
         NSTimeInterval timeDifference = [endDate timeIntervalSinceDate:startDate];
         NSInteger timeDifferenceInt = (NSInteger) timeDifference;
         NSInteger raTotalInspectionTime = 0;
         raTotalInspectionTime = timeDifferenceInt - [record.rrec_Inspected_Total_Pause_Time integerValue];
         if (raTotalInspectionTime < 0) {
         raTotalInspectionTime = 0;
         }
         */

//        AdamHong [20180125/SA timer] - record.rrec_Total_Inspected_Time shouldn't divided with 60 because it is seconds.
//        NSInteger raTotalInspectionTime = record.rrec_Total_Inspected_Time;
//        if(raTotalInspectionTime > 0){
//            raTotalInspectionTime = raTotalInspectionTime / 60;
//        }
//        
//        [request setIntTotalInspectTime:[NSNumber numberWithInteger:raTotalInspectionTime]];

        [request setIntTotalInspectTime:[NSNumber numberWithInteger:record.rrec_Total_Inspected_Time]];
        
        NSNumber *enableLogNumber = [[NSUserDefaults standardUserDefaults] objectForKey:ENABLE_LOG];
        if(enableLogNumber != nil && [enableLogNumber intValue] == 1){
            LogObject *logObj = [[LogObject alloc] init];
            logObj.dateTime = [NSDate date];
            logObj.log = [NSString stringWithFormat:@"<%i><postRoomInspectionWSByUserID><intUsrID:%i intRoomStatusID:%i intOverallScor:i% intRoomStatus:%i intInspectStatus:%i nspectedStartTime:%@ inspectedEndTime:%@ intTotalInspectTime:%i>",(int)userID,(int)userID,model.roomAssignment_Id,(int)chkmodel.chkCore,(int)model.roomAssignmentRoomStatusId,(int)model.roomAssignmentRoomInspectionStatusId,record.rrec_Inspection_Start_Time,record.rrec_Inspection_End_Time,(int)record.rrec_Total_Inspected_Time];
            LogFileManager *logManager = [[LogFileManager alloc] init];
            [logManager save:logObj];
        }
        
        eHousekeepingServiceSoap12BindingResponse *response = [binding UpdateInspectionUsingParameters:request];
        
        NSArray *responseBodyParts = response.bodyParts;
        
        for (id bodyPart in responseBodyParts) {
            if ([bodyPart isKindOfClass:[eHousekeepingService_UpdateInspectionResponse class]]) {
                eHousekeepingService_UpdateInspectionResponse *dataBody = (eHousekeepingService_UpdateInspectionResponse *)bodyPart;
                if (dataBody != nil) {
                    if ([dataBody.UpdateInspectionResult.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_OK]] || [dataBody.UpdateInspectionResult.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_NEW_RECORD_ADDED]]) {
                        //change post status
                        result = RESPONSE_STATUS_OK;
//                        if (room.room_PostStatus == POST_STATUS_SAVED_UNPOSTED) {
//                            room.room_PostStatus = POST_STATUS_POSTED;
//                            [self updateRoomModel:room];
//                        }
                        if (record.rrec_PostStatus == POST_STATUS_SAVED_UNPOSTED) {
                            record.rrec_PostStatus = (int)POST_STATUS_POSTED;
                            [self updateRoomRecordData:record];
                        }
                        
//                        for (RoomRemarkModelV2 *remark in remarks) {
//                            if (room.room_PostStatus == POST_STATUS_SAVED_UNPOSTED) {
//                                remark.rm_PostStatus = POST_STATUS_POSTED;
//                                [self updateRoomRemarkData:remark];
//                            }
//                        }
                    } else if ([dataBody.UpdateInspectionResult.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_INVALID_ROOM_STATUS]]) {
                        result = RESPONSE_STATUS_INVALID_ROOM_STATUS;
                    }else {
                        result = RESPONSE_STATUS_ERROR;
                    }
                }
            }
        }
        
    } else {
        return RESPONSE_STATUS_OK;
    }
    
    return result;
}
 
-(NSInteger)updateRoomHaveBeenRemove:(NSInteger) userID AndRaModel:(RoomAssignmentModelV2 *) model{
    NSInteger result =0 ;
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole])
    {
        [binding setLogXMLInOut:YES];
    }
    eHousekeepingService_UpdateRoomAssignment *request = [[eHousekeepingService_UpdateRoomAssignment alloc] init];
    
    [request setIntUsrID:[NSNumber numberWithInteger:userID]];
    [request setIntRoomAssignID:[NSNumber numberWithInteger:model.roomAssignment_Id]];
    [request setIntRoomStatusID:[NSNumber numberWithInteger:model.roomAssignmentRoomStatusId]];
    [request setIntCleaningStatusID:[NSNumber numberWithInteger:model.roomAssignmentRoomCleaningStatusId]];
    [request setRaRemark:@""];
    [request setIntTotalCleaningTime:@""];
    [request setStrCleanStartTm:@""];
    [request setStrCleanEndTm:@""];
    
    LogObject *logObj = [[LogObject alloc] init];
    logObj.dateTime = [NSDate date];
    logObj.log = [NSString stringWithFormat:@"<%i><updateRoomHaveBeenRemove><intUsrID:%i intRoomAssignID:%i intRoomStatusID:%i intCleaningStatusID:%i RaRemark:%@ intTotalCleaningTime:%@ cleanStartTm:%@ cleanEndTm:%@>",(int)userID,(int)userID,model.roomAssignment_Id,(int)model.roomAssignmentRoomStatusId,(int)model.roomAssignmentRoomCleaningStatusId,@"",@"",@"",@""];
    LogFileManager *logManager = [[LogFileManager alloc] init];
    [logManager save:logObj];
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding UpdateRoomAssignmentUsingParameters:request];
    NSArray *responseBodyParts = response.bodyParts;
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_UpdateRoomAssignmentResponse class]]) {
            eHousekeepingService_UpdateRoomAssignmentResponse *dataBody = (eHousekeepingService_UpdateRoomAssignmentResponse *)bodyPart;
            if (dataBody != nil) {
                
                if ([dataBody.UpdateRoomAssignmentResult.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_OK]] || [dataBody.UpdateRoomAssignmentResult.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_NEW_RECORD_ADDED]]) {
                    //change post status
                    result = RESPONSE_STATUS_OK;
                    
                } else if([dataBody.UpdateRoomAssignmentResult.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_ROOM_HAVE_BEEN_DELETED]]) {
                    [self setDeleteKey:DELETE_ROOM_ASSIGNMENT_FLAG forRoomAssignmentId:model.roomAssignment_Id andUserId:userID];
                    result = RESPONSE_STATUS_ROOM_HAVE_BEEN_DELETED;
                } else if([dataBody.UpdateRoomAssignmentResult.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_INVALID_ROOM_STATUS]]) {
                    result = RESPONSE_STATUS_INVALID_ROOM_STATUS;
                }
            }
        }
    }
    
    return result;
}


-(int)postRoomAssignmentWSByUserID:(NSInteger)userID AndRaModel:(RoomAssignmentModelV2 *)model {
    int result = RESPONSE_STATUS_OK;
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole]) {
        [binding setLogXMLInOut:YES];
    }
    eHousekeepingService_UpdateRoomAssignment *request = [[eHousekeepingService_UpdateRoomAssignment alloc] init];
    
    [request setIntUsrID:[NSNumber numberWithInteger:userID]];
    [request setIntRoomAssignID:[NSNumber numberWithInteger:model.roomAssignment_Id]];
    
    RoomModelV2 *room = [[RoomModelV2 alloc] init];
    room.room_Id = model.roomAssignment_RoomId;
    [self loadRoomModel:room];
    
    if (model.roomAssignment_PostStatus == POST_STATUS_SAVED_UNPOSTED) {
        [request setIntRoomStatusID:[NSNumber numberWithInteger:model.roomAssignmentRoomStatusId]];
        [request setIntCleaningStatusID:[NSNumber numberWithInteger:model.roomAssignmentRoomCleaningStatusId]];
        
        RoomRecordModelV2 *record = [[RoomRecordModelV2 alloc] init];

        record.rrec_room_assignment_id = model.roomAssignment_Id;
        record.rrec_User_Id = (int)userID;
        [[RoomManagerV2 sharedRoomManager] loadRoomRecordDataByRoomIdAndUserId:record];
        [request setRaRemark:record.rrec_Remark];
        
        //20141121 - Fixed for WS response error if Start Time & End Time are null
        //always post start time & end time clean
        [request setStrCleanStartTm:record.rrec_Cleaning_Start_Time.length > 0 ? record.rrec_Cleaning_Start_Time : @""];
        [request setStrCleanEndTm:record.rrec_Cleaning_End_Time.length > 0 ? record.rrec_Cleaning_End_Time : @""];
        [request setStrPauseTime:record.rrec_cleaning_pause_time > 0 ? record.rrec_cleaning_pause_time : @""];
        
        //set ra total cleaning time
        NSDateFormatter *f = [[NSDateFormatter alloc] init];
        [f setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        //Hao Tran - Remove for fix wrong data
        /*
         NSTimeInterval timeDifference = [endDate timeIntervalSinceDate:startDate];
         NSInteger timeDifferenceInt = (NSInteger) timeDifference;
         NSInteger raTotalCleaningTime = 0;
         raTotalCleaningTime = timeDifferenceInt - [record.rrec_Cleaning_Total_Pause_Time integerValue];
         if (raTotalCleaningTime < 0) {
         raTotalCleaningTime = 0;
         }*/
        
        NSInteger raTotalCleaningTime = 0;
        // DungPhan - 20150918: Calculate total cleaning time as seconds, old logic is minutes - Discuss with FM
        raTotalCleaningTime = record.rrec_Total_Cleaning_Time;
//        if(raTotalCleaningTime > 0){
//            raTotalCleaningTime = raTotalCleaningTime / 60;
//        }
        
        [request setIntTotalCleaningTime:[NSString stringWithFormat:@"%d", (int)raTotalCleaningTime]];
        
        NSNumber *enableLogNumber = [[NSUserDefaults standardUserDefaults] objectForKey:ENABLE_LOG];
        if(enableLogNumber != nil && [enableLogNumber intValue] == 1){
            LogObject *logObj = [[LogObject alloc] init];
            logObj.dateTime = [NSDate date];
            logObj.log = [NSString stringWithFormat:@"<%i><postRoomAssignmentWSByUserID><intUsrID:%i intRoomAssignID:%i intRoomStatusID:%i intCleaningStatusID:%i RaRemark:%@ cleanStartTm:%@ cleanEndTm:%@ intTotalCleaningTime:%d>",(int)userID,(int)userID,model.roomAssignment_Id,(int)model.roomAssignmentRoomStatusId,(int)model.roomAssignmentRoomCleaningStatusId,room.room_remark,record.rrec_Cleaning_Start_Time,record.rrec_Cleaning_End_Time,(int)raTotalCleaningTime];
            LogFileManager *logManager = [[LogFileManager alloc] init];
            [logManager save:logObj];
        }
        
        //SG-70548 : Save to post sequence cleaning room for RA if RA is in offline mode
        if (![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
            if(![UserManagerV2 isSupervisor]) {
                
                //Don't save unposted status for RA for sync
                //This will avoid RA sync this to server, RA will post Room Record History for sequence posting room
                if (room.room_PostStatus == POST_STATUS_SAVED_UNPOSTED) {
                    room.room_PostStatus = (int)POST_STATUS_POSTED;
                    [self updateRoomModel:room];
                }
                
                if (record.rrec_PostStatus == POST_STATUS_SAVED_UNPOSTED) {
                    record.rrec_PostStatus = (int)POST_STATUS_POSTED;
                    [self updateRoomRecordData:record];
                }
                
                if (model.roomAssignment_PostStatus == POST_STATUS_SAVED_UNPOSTED) {
                    model.roomAssignment_PostStatus = (int)POST_STATUS_POSTED;
                    [self update:model];
                }
                
                RoomRecordHistory *roomRecordHistory = [[RoomRecordHistory alloc] init];
                roomRecordHistory.rrec_User_Id = [request.intUsrID intValue];
                roomRecordHistory.rrec_room_assignment_id = [request.intRoomAssignID intValue];
                roomRecordHistory.rrec_room_status_id = [request.intRoomStatusID intValue];
                roomRecordHistory.rrec_cleaning_status_id = [request.intCleaningStatusID intValue];
                roomRecordHistory.rrec_Cleaning_Start_Time = request.strCleanStartTm;
                roomRecordHistory.rrec_Cleaning_End_Time = request.strCleanEndTm;
                roomRecordHistory.rrec_Remark = request.raRemark;
                roomRecordHistory.rrec_cleaning_pause_time = request.strPauseTime;
                roomRecordHistory.rrec_Total_Cleaning_Time = [request.intTotalCleaningTime intValue];
                roomRecordHistory.rrec_PostStatus = (int)POST_STATUS_SAVED_UNPOSTED;
                
                [self insertRoomRecordHistoryData:roomRecordHistory];
            }
        }
        
        eHousekeepingServiceSoap12BindingResponse *response = [binding UpdateRoomAssignmentUsingParameters:request];
        
        NSArray *responseBodyParts = response.bodyParts;
        
        if(responseBodyParts == nil) {
            return NO;
        }
        
        for (id bodyPart in responseBodyParts) {
            if ([bodyPart isKindOfClass:[eHousekeepingService_UpdateRoomAssignmentResponse class]]) {
                eHousekeepingService_UpdateRoomAssignmentResponse *dataBody = (eHousekeepingService_UpdateRoomAssignmentResponse *)bodyPart;
                if (dataBody != nil) {
                    
                    int responseCode = [dataBody.UpdateRoomAssignmentResult.respCode intValue];
                    if (responseCode == RESPONSE_STATUS_OK
                        || responseCode == RESPONSE_STATUS_NEW_RECORD_ADDED)
                    {
                        //change post status
                        result = RESPONSE_STATUS_OK;
                        
                        if (room.room_PostStatus == POST_STATUS_SAVED_UNPOSTED) {
                            room.room_PostStatus = (int)POST_STATUS_POSTED;
                            [self updateRoomModel:room];
                        }
                        
                        if (record.rrec_PostStatus == POST_STATUS_SAVED_UNPOSTED) {
                            record.rrec_PostStatus = (int)POST_STATUS_POSTED;
                            [self updateRoomRecordData:record];
                        }
                        
                        if (model.roomAssignment_PostStatus == POST_STATUS_SAVED_UNPOSTED) {
                            model.roomAssignment_PostStatus = (int)POST_STATUS_POSTED;
                            [self update:model];
                        }
                        
                    } else if (responseCode == RESPONSE_STATUS_INVALID_ROOM_STATUS) {
                        result = RESPONSE_STATUS_INVALID_ROOM_STATUS;
                    } else {
                        result = RESPONSE_STATUS_ERROR;
                    }
                }
            }
        }
    }
    
    return result;
}


//reasign room asign via ws
-(int)reAssignRoomAssignmentWSByReassignRoomAssignmentModel:(ReassignRoomAssignmentModel *)reModel {
    
    //get binding
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    
    //Allow write log or not
    if([LogFileManager isLogConsole])
    {
        [binding setLogXMLInOut:YES];
    }
    
    //new request
    eHousekeepingService_AddReassignRoom *request = [[eHousekeepingService_AddReassignRoom alloc] init];
    
    //set user
    [request setIntUsrID:[NSNumber numberWithInteger:reModel.reassignUserId]];
    
    //set room no
    [request setStrRoomNo:reModel.reassignRoomId];
    
    //set assign date yyyy/MM/dd HH:mm:ss
    [request setStrAssignDateTime:reModel.reassignDatetime];
    
    //set expected cleaning time
    [request setIntExpectedCleaningTime:[NSNumber numberWithInteger:reModel.reasignExpectedCleaningTime]];
    
    //set room status id
    [request setIntRoomStatus:[NSNumber numberWithInteger:reModel.reassignRoomStatus]];
    
    //set cleaning status id
    [request setIntCleaningStatus:[NSNumber numberWithInteger:reModel.reassignCleaningStatus]];
    
    //set housekeeper id
    [request setIntHousekeeperID:[NSNumber numberWithInteger:reModel.reassignHousekeeperId]];
    
    //set room type id
    [request setIntRoomTypeID:[NSNumber numberWithInteger:reModel.reassignRoomType]];
    
    //set floor id
    [request setIntFloorID:[NSNumber numberWithInteger:reModel.reassignFloorId]];
    
    //set guest profile id
    [request setIntGuestProfileID:[NSNumber numberWithInteger:reModel.reassignGuestProfileId]];
    //set Duty Assignment Id
    [request setIntDutyAssignmentID: [NSNumber numberWithInteger:reModel.reassignRaId]];//An Rework
//    request.setInt
    //set hotel id
    [request setIntHotelID:[NSNumber numberWithInteger:reModel.reassignHotelId]];
    if (reModel.cleaningCredit && reModel.cleaningCredit.length > 0) {
        [request setCleaningCredit:reModel.cleaningCredit];
    }
    if (reModel.reassignRemark.length > 0) {
        [request setStrRemark:reModel.reassignRemark];
    }
    
    LogObject *logObj = [[LogObject alloc] init];
    logObj.dateTime = [NSDate date];
    logObj.log = [NSString stringWithFormat:@"<%i><reAssignRoomAssignmentWSByReassignRoomAssignmentModel><intUsrID:%i roomNo:%@ assignDateTime:%@ intExpectedCleaningTime:%i intRoomStatus:%i intCleaningStatus:%i intHousekeeperID:%i intRoomTypeID:%i intFloorID:%i intGuestProfileID:%i intHotelID:%i>",(int)reModel.reassignUserId,(int)reModel.reassignUserId,reModel.reassignRoomId,reModel.reassignDatetime,(int)reModel.reasignExpectedCleaningTime,(int)reModel.reassignRoomStatus,(int)reModel.reassignCleaningStatus,(int)reModel.reassignHousekeeperId,(int)reModel.reassignRoomType,(int)reModel.reassignFloorId,(int)reModel.reassignGuestProfileId,(int)reModel.reassignHotelId];
    LogFileManager *logManager = [[LogFileManager alloc] init];
    [logManager save:logObj];
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding AddReassignRoomUsingParameters:request];
    
    int result = 0;
    NSArray *responseBodyParts = response.bodyParts;
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_AddReassignRoomResponse class]]) {
            eHousekeepingService_AddReassignRoomResponse *dataBody = (eHousekeepingService_AddReassignRoomResponse *)bodyPart;
            if (dataBody != nil) {
                
                if ([dataBody.AddReassignRoomResult.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_NEW_RECORD_ADDED]]) {
                    // reassign successful
                    
                    //delete reassign record
                    [[RoomManagerV2 sharedRoomManager] deleteReassignRoomAssignmentModel:reModel];
                    result = [dataBody.AddReassignRoomResult.respCode intValue];
                    
                    return result;
                } else {
                    result = [dataBody.AddReassignRoomResult.respCode intValue];
                }
            }
        }
    }
    
    return result;
}

-(int)setUnassignedRoomWSByReassignRoomAssignmentModel:(ReassignRoomAssignmentModel *)reModel {
    
    //get binding
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole]) {
        [binding setLogXMLInOut:YES];
    }
    
    //new request
    eHousekeepingService_PostUnassignRoomRoutine *request = [[eHousekeepingService_PostUnassignRoomRoutine alloc] init];
    
    [request setUserID:[NSString stringWithFormat:@"%d", (int)reModel.reassignUserId]];
    [request setDutyAssignmentID:[NSString stringWithFormat:@"%d", (int)reModel.reassignRaId]];
    [request setPropertyID:[NSString stringWithFormat:@"%d", (int)reModel.reassignHotelId]];
    [request setCleaningStatus:[NSString stringWithFormat:@"%d", (int)reModel.reassignCleaningStatus]];
    
    if (reModel.reassignRemark.length > 0) {
        [request setRemark:reModel.reassignRemark];
    }
    
    LogObject *logObj = [[LogObject alloc] init];
    logObj.dateTime = [NSDate date];
    logObj.log = [NSString stringWithFormat:@"<%i><rSetUnassignedRoomWSByReassignRoomAssignmentModel><intUsrID:%i roomNo:%@ assignDateTime:%@ intExpectedCleaningTime:%i intRoomStatus:%i intCleaningStatus:%i intHousekeeperID:%i intRoomTypeID:%i intFloorID:%i intGuestProfileID:%i intHotelID:%i>",(int)reModel.reassignUserId,(int)reModel.reassignUserId,reModel.reassignRoomId,reModel.reassignDatetime,(int)reModel.reasignExpectedCleaningTime,(int)reModel.reassignRoomStatus,(int)reModel.reassignCleaningStatus,(int)reModel.reassignHousekeeperId,(int)reModel.reassignRoomType,(int)reModel.reassignFloorId,(int)reModel.reassignGuestProfileId,(int)reModel.reassignHotelId];
    LogFileManager *logManager = [[LogFileManager alloc] init];
    [logManager save:logObj];
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding PostUnassignRoomRoutineUsingParameters:request];
    
    int result = 0;
    NSArray *responseBodyParts = response.bodyParts;
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_PostUnassignRoomRoutineResponse class]]) {
            eHousekeepingService_PostUnassignRoomRoutineResponse *dataBody = (eHousekeepingService_PostUnassignRoomRoutineResponse *)bodyPart;
            if (dataBody != nil) {
                
                if ([dataBody.PostUnassignRoomRoutineResult.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_NEW_RECORD_ADDED]]) {
                    // reassign successful
                    
                    //delete reassign record
                    [[RoomManagerV2 sharedRoomManager] deleteReassignRoomAssignmentModel:reModel];
                    result = [dataBody.PostUnassignRoomRoutineResult.respCode intValue];
                    
                    return result;
                } else {
                    result = [dataBody.PostUnassignRoomRoutineResult.respCode intValue];
                }
            }
        }
    }
    
    return result;
}


-(NSInteger)numberOfMustPostRoomAssignmentRecordsWithSupervisorId:(NSInteger)userId {
    RoomAssignmentAdapterV2 *raadapter = [RoomAssignmentAdapterV2 createRoomAssignmentAdapter];
    [raadapter openDatabase];
    NSInteger numberOfMustPostRecords = [raadapter numberOfMustPostRoomAssignmentRecordsWithSupervisorId:userId];
    [raadapter close];
    return numberOfMustPostRecords;
}

-(NSInteger)numberOfRoomAssignmentMustPost{
    RoomAssignmentAdapterV2 *raadapter = [RoomAssignmentAdapterV2 createRoomAssignmentAdapter];
    [raadapter openDatabase];
    NSInteger numberOfMustPostRecords = [raadapter numberOfMustPostRoomAssignmentRecordsWithUserId:[UserManagerV2 sharedUserManager].currentUser.userId];
    [raadapter close];
    return numberOfMustPostRecords;
}

-(void)deleteOldRoomAssignmentsWithUserId:(NSInteger)userId {
    RoomAssignmentAdapterV2 *raadapter = [RoomAssignmentAdapterV2 createRoomAssignmentAdapter];
    [raadapter openDatabase];
    [raadapter deleteOldRoomAssignmentsWithUserId:userId];
    [raadapter close];
}

-(int)deleteAllRoomAssignment
{
    DataTable *tableAddJobRoom = [[DataTable alloc] initWithNameFormat:@"%@", ROOM_ASSIGNMENT];
    NSString *queryDelete = [[NSMutableString alloc] initWithFormat:@"DELETE FROM %@", ROOM_ASSIGNMENT];
    
    int status = (int)[tableAddJobRoom excuteQueryNonSelect:queryDelete];
    return status;
}

//If any change this function in the future must change query of "-(int) countAllRoomAssignmentsWithUserId:(NSInteger) userId" too
-(NSMutableArray *) getAllRoomAssignmentsWithUserId:(NSInteger) userId {
    RoomAssignmentAdapterV2 *raadapter = [RoomAssignmentAdapterV2 createRoomAssignmentAdapter];
    [raadapter openDatabase];
    NSMutableArray *ramodels = [raadapter loadAllCurrentDateRoomAssignmentsWithUserId:userId];
    [raadapter close];
    return ramodels;
}

//If any change this function in the future must change query of "-(NSMutableArray *) getAllRoomAssignmentsWithUserId:(NSInteger) userId" too
-(int) countAllRoomAssignmentsWithUserId:(NSInteger) userId {
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    [f setDateFormat:@"yyyy-MM-dd"];
    NSString *currentDate = [NSString stringWithFormat:@"%@ 00:00:00", [f stringFromDate:[NSDate date]]];
    NSMutableString *sqlQueryCount = [[NSMutableString alloc] initWithFormat:@"select count(1) from %@ where %@ = %d AND %@ > '%@' AND %@ = 0",
                                      //Count
                                      ROOM_ASSIGNMENT,
                                      ra_user_id,
                                      (int)userId,
                                      
                                      ra_assigned_date,
                                      currentDate,
                                      
                                      ra_delete
                                      ];
    
    DataTable *tableCount = [[DataTable alloc] init];
    
    //Row result reference
    DataRow *rowReference = [[DataRow alloc] init];
    [rowReference addColumnFormatInt:@"CountResult"];
    
    [tableCount loadTableDataWithQuery:sqlQueryCount referenceRow:rowReference];
    
    int countResult = 0;
    int rowCount = [tableCount countRows];
    if(rowCount > 0)
    {
        DataRow *row = [tableCount rowWithIndex:0];
        countResult = [row intValueWithColumnNameFormat:@"CountResult"];
    }
    
    return countResult;
}

-(void)setDeleteKey:(NSInteger)deleteId forRoomAssignmentId:(NSInteger)raId andUserId:(NSInteger)userId{
    RoomAssignmentAdapterV2 *raadapter = [RoomAssignmentAdapterV2 createRoomAssignmentAdapter];
    [raadapter openDatabase];
    [raadapter setDeleteKey:deleteId forRoomAssignmentId:raId andUserId:userId];
    [raadapter close];
}

-(int)reloadRoomStatusWSByUserId:(int)userId roomAssignmentId:(int)assignmentId currentRoomStatusId:(int)currentStatusId
{
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    
    //Allow write log or not
    if([LogFileManager isLogConsole])
    {
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_GetDiscrepantRoomStatus *request = [[eHousekeepingService_GetDiscrepantRoomStatus alloc] init];
    
    [request setIntUsrID:[NSNumber numberWithInteger:userId]];
    [request setIntRoomAssignID:[NSNumber numberWithInteger:assignmentId]];
    [request setIntRoomStatusID:[NSNumber numberWithInteger:currentStatusId]];
    
    int roomStatusFromWS = currentStatusId;
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetDiscrepantRoomStatusUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetDiscrepantRoomStatusResponse class]]) {
            eHousekeepingService_GetDiscrepantRoomStatusResponse *dataBody = (eHousekeepingService_GetDiscrepantRoomStatusResponse *)bodyPart;
            if (dataBody != nil) {
                if ([dataBody.GetDiscrepantRoomStatusResult.ResponseStatus.respCode integerValue] == RESPONSE_STATUS_OK) {
                    eHousekeepingService_GetDiscrepantRoomStatusList *dataWS = dataBody.GetDiscrepantRoomStatusResult;
                    roomStatusFromWS = (int)[dataWS.intNewRoomStatus integerValue];
                    break;//exit loop
                }
            }
        }
    }
    
    return roomStatusFromWS;
}

#pragma mark - === Change Sequence Room Assignment ===
#pragma mark
-(BOOL) changeRoomAssignmentSequenceFromrRoomAssignId:(NSInteger) fromRaId toRoomAssignId:(NSInteger) toRaId forUserId:(NSInteger) userId {
    BOOL result = YES;
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    
    //Allow write log or not
    if([LogFileManager isLogConsole])
    {
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_ChangeRoomAssignmentSequence *request = [[eHousekeepingService_ChangeRoomAssignmentSequence alloc] init];
    
    [request setIntUsrID:[NSNumber numberWithInteger:userId]];
    [request setIntRoomAssignID:[NSNumber numberWithInteger:fromRaId]];
    [request setIntNextRoomAssignID:[NSNumber numberWithInteger:toRaId]];
    
    LogObject *logObj = [[LogObject alloc] init];
    logObj.dateTime = [NSDate date];
    logObj.log = [NSString stringWithFormat:@"<%i><changeRoomAssignmentSequenceFromrRoomAssignId><intUsrID:%i intRoomAssignID:%i intNextRoomAssignID:%i>",(int)userId,(int)userId,(int)fromRaId,(int)toRaId];
    LogFileManager *logManager = [[LogFileManager alloc] init];
    [logManager save:logObj];
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding ChangeRoomAssignmentSequenceUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;      
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_ChangeRoomAssignmentSequenceResponse class]]) {
            eHousekeepingService_ChangeRoomAssignmentSequenceResponse *dataBody = (eHousekeepingService_ChangeRoomAssignmentSequenceResponse *)bodyPart;
            if (dataBody != nil) {
                if ([dataBody.ChangeRoomAssignmentSequenceResult.respCode integerValue] == RESPONSE_STATUS_OK) {
                    result = YES;
                }
            }
        }
    }
    
    return result;
}

#pragma mark
#pragma mark insert, update, delete for room model
//Hao Tran - Remove for unneccessary function
/*
-(NSInteger)getCleaningPauseTime:(NSInteger)raID AndUserID:(NSInteger)userID{
    NSInteger pausetime = 0;
    
    RoomAssignmentModelV2 *ramodel = [[RoomAssignmentModelV2 alloc] init];
    ramodel.roomAssignment_Id = raID;
    ramodel.roomAssignment_UserId = userID;
    [self load:ramodel];
    RoomRecordModelV2 *record = [[RoomRecordModelV2 alloc] init];
    record.rrec_User_Id = userID;
    record.rrec_room_assignment_id = ramodel.roomAssignment_Id;
    [self loadRoomRecordDataByRoomIdAndUserId:record];
    NSDate *startCleaning = nil;
    NSDate *endCleaning = nil;
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    [f setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    startCleaning = [f dateFromString:record.rrec_Cleaning_Start_Time];
    endCleaning = [f dateFromString:record.rrec_Cleaning_End_Time];
    NSTimeInterval timeDifference = [endCleaning timeIntervalSinceDate:startCleaning];
    pausetime = timeDifference - [record.rrec_Cleaning_Duration integerValue];
    if (pausetime != [record.rrec_Cleaning_Total_Pause_Time integerValue]) {
        if([LogFileManager isLogConsole])
        {
            NSLog(@"Pause time incorrect");
        }
    }
    return pausetime;
}

-(NSInteger)getInspectedPauseTime:(NSInteger)raID AndUserID:(NSInteger)userID{
    NSInteger pausetime = 0;
    
    RoomAssignmentModelV2 *ramodel = [[RoomAssignmentModelV2 alloc] init];
    ramodel.roomAssignment_Id = raID;
    [self load:ramodel];
    RoomRecordModelV2 *record = [[RoomRecordModelV2 alloc] init];
    record.rrec_User_Id = userID;
    record.rrec_room_assignment_id = ramodel.roomAssignment_Id;
    [self loadRoomRecordDataByRoomIdAndUserId:record];
    NSDate *startInspected = nil;
    NSDate *endInspected = nil;
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    [f setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    startInspected = [f dateFromString:record.rrec_Inspection_Start_Time];
    endInspected = [f dateFromString:record.rrec_Inspection_End_Time];
    NSTimeInterval timeDifference = [endInspected timeIntervalSinceDate:startInspected];
    pausetime = timeDifference - [record.rrec_Inspection_Duration integerValue];
    
    return pausetime;
}
 */

//Hao Tran - Remove for enhance loading data in sync
//shouldUpdateRoomRecord = YES : allow update room record (etc: Cleaning Time, Inspecting Time...) from WS response
//shouldUpdateRoomRecord = NO : don't allow update room record (etc: Cleaning Time, Inspecting Time...) from WS response
-(BOOL) getRoomWSByUserID:(NSInteger) userID AndRaID:(NSInteger) raID AndLastModified:(NSString *)strLastModified shouldUpdateRoomRecord:(BOOL)shouldUpdateRoomRecord {
    
    if (![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        return NO;
    }
    
    BOOL result = YES;
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole]) {
        [binding setLogXMLInOut:YES];
    }
    eHousekeepingService_GetRoomDetail *request = [[eHousekeepingService_GetRoomDetail alloc] init];
    
    [request setIntUsrID:[NSNumber numberWithInteger:userID]];
    [request setIntRoomAssignID:[NSNumber numberWithInteger:raID]];
    if (strLastModified.length > 0) {
        [request setStrLastModified:strLastModified];
    }
    
//    LogObject *logObj = [[LogObject alloc] init];
//    logObj.dateTime = [NSDate date];
//    logObj.log = [NSString stringWithFormat:@"<%i><getRoomWSByUserID><intUsrID:%i intRoomAssignID:%i lastModified:%@>",userID,userID,raID,strLastModified];
//    LogFileManager *logManager = [[LogFileManager alloc] init];
//    [logManager save:logObj];
    
    if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL) {
        return NO;
    }
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetRoomDetailUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    if(responseBodyParts == nil) {
        return NO;
    }
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetRoomDetailResponse class]]) {
            eHousekeepingService_GetRoomDetailResponse *dataBody = (eHousekeepingService_GetRoomDetailResponse *)bodyPart;
            if (dataBody != nil) {
                if ([dataBody.GetRoomDetailResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_OK]]) {
                    eHousekeepingService_RoomAssign *roomDetail = dataBody.GetRoomDetailResult.RoomDetail;
                    
                    RoomModelV2 *roomModel = [[RoomModelV2 alloc] init];
                    //insert Room Detail data
                    roomModel.room_Id = roomDetail.raRoomNo;
                    roomModel.room_AdditionalJob = roomDetail.raAdditionalJob;
                    roomModel.room_Building_Name = roomDetail.raBuildingName;
                    roomModel.room_Building_Id = [roomDetail.raBuildingID intValue];
                    roomModel.room_building_namelang = roomDetail.rabuildingLang;
                    roomModel.room_Guest_Name = [NSString stringWithFormat:@"%@%@", roomDetail.raGuestFirstName == nil ? @"" : [NSString stringWithFormat:@"%@ ", roomDetail.raGuestFirstName], roomDetail.raGuestLastName == nil ? @"" : [NSString stringWithFormat:@"%@", roomDetail.raGuestLastName]];
                    roomModel.room_GuestReference = roomDetail.raGuestPreference;
                    roomModel.room_HotelId = [roomDetail.raHotelID intValue];
                    roomModel.room_LastModified = roomDetail.raLastModified;

                    roomModel.room_PostStatus = (int)POST_STATUS_UN_CHANGED;
                    roomModel.room_remark = roomDetail.raRemark;
                    roomModel.room_TypeId = [roomDetail.raRoomTypeID intValue];
                    roomModel.room_VIPStatusId = roomDetail.raVIP;
                    roomModel.room_floor_id = [roomDetail.raFloorID integerValue];
                    //roomDetail.raAdditionalJob=roomDetail.raAdditionalJob

                    RoomModelV2 *compareRoom = [[RoomModelV2 alloc] init];
                    compareRoom.room_Id = roomDetail.raRoomNo;

                    [self loadRoomModel:compareRoom];

                    //if (compareRoom.room_VIPStatusId != 0) {
                    if(compareRoom.room_LastModified.length > 0){
                        //Not need post room info because when call get RA, did check to post Room Assignment
                        if (![compareRoom.room_LastModified isEqualToString:roomDetail.raLastModified]) {
                            [self updateRoomModel:roomModel];
                        }
                    }else {
                        [self insertRoomModel:roomModel];
                    }
                    
                    //compareRoom.room_VIPStatusId = 0;
                    
                    //only update priority sort order, inspected start time & inspected end time in room record
                    RoomAssignmentModelV2 *ramodel = [[RoomAssignmentModelV2 alloc] init];
                    ramodel.roomAssignment_Id = [roomDetail.raID intValue];
                    ramodel.roomAssignment_UserId = (int)userID;
                    [self load:ramodel];
                    ramodel.roomAssignment_Priority_Sort_Order = [roomDetail.raPrioritySortOrder integerValue];
                    ramodel.roomAssignment_Priority = [roomDetail.raPriority intValue];
                    ramodel.roomAssignmentRoomInspectionStatusId = [roomDetail.raInspectedStatus integerValue];
                    ramodel.roomAssignmentCleaningCredit = roomDetail.raCleaningCredit;
                    //[Hao Tran / 20150514] SG-71576 : Update room assignment detail before start cleaning room
                    //Only apply for Room Attendant when cleaning room
                    if (![UserManagerV2 isSupervisor]) {
                        ramodel.roomAssignmentRoomStatusId = [roomDetail.raRoomStatusID intValue];
                        ramodel.roomAssignmentRoomCleaningStatusId = [roomDetail.raCleaningStatusID intValue];
                    }
                    [self update:ramodel];
                    
                    //shouldUpdateRoomRecord = YES : allow update room record (etc: Cleaning Time, Inspecting Time...) from WS response
                    if (shouldUpdateRoomRecord) {
                        RoomRecordModelV2 *record = [[RoomRecordModelV2 alloc] init];
                        //set last cleaning date here
                        NSDateFormatter *format = [[NSDateFormatter alloc] init];
                        [format setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                        NSDate *lastDate = [format dateFromString:ramodel.roomAssignment_LastCleaningDate];
                        [format setDateFormat:@"dd / MM / yyyy"];
                        record.rrec_Cleaning_Date = [format stringFromDate:lastDate];
                        record.rrec_room_assignment_id = ramodel.roomAssignment_Id;
                        record.rrec_User_Id = (int)userID;
                        [self loadRoomRecordDataByRoomIdAndUserId:record];
                        record.rrec_Inspection_Start_Time = roomDetail.raInspectStartTm;
                        record.rrec_Inspection_End_Time = roomDetail.raInspectEndTm;

                //Adam - [20180125/SA Timer] - Remove product 60 from rrec_Total_Inspected_Time 000
                        record.rrec_Total_Inspected_Time = [roomDetail.raToTalInspectionTime intValue];
                        
                        //Hao Tran - Remove for unneccessary
                        /*
                         NSDate *startDate = nil;
                         NSDate *endDate = nil;
                         NSDateFormatter *f = [[NSDateFormatter alloc] init];
                         [f setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                         startDate = [f dateFromString:record.rrec_Inspection_Start_Time];
                         endDate = [f dateFromString:record.rrec_Inspection_End_Time];
                         
                         NSTimeInterval timeDifference = [endDate timeIntervalSinceDate:startDate];
                         NSInteger timeDifferenceInt = (NSInteger) timeDifference;
                         record.rrec_Inspected_Total_Pause_Time = [NSString stringWithFormat:@"%d", (timeDifferenceInt - [record.rrec_Inspection_Duration integerValue]) > 0 ? (timeDifferenceInt - [record.rrec_Inspection_Duration integerValue]) : 0];
                         
                         record.rrec_Cleaning_Start_Time = roomDetail.raCleanStartTm;
                         record.rrec_Cleaning_End_Time = roomDetail.raCleanEndTm;
                         record.rrec_Cleaning_Duration = [NSString stringWithFormat:@"%d", [roomDetail.raTotalCleaningTime integerValue] * 60];
                         
                         startDate = [f dateFromString:record.rrec_Cleaning_Start_Time];
                         endDate = [f dateFromString:record.rrec_Cleaning_End_Time];
                         
                         timeDifference = [endDate timeIntervalSinceDate:startDate];
                         timeDifferenceInt = (NSInteger) timeDifference;
                         record.rrec_Cleaning_Total_Pause_Time = [NSString stringWithFormat:@"%d", (timeDifferenceInt - [record.rrec_Cleaning_Duration integerValue]) > 0 ? (timeDifferenceInt - [record.rrec_Cleaning_Duration integerValue]) : 0];
                         */
                        
                        record.rrec_Cleaning_Start_Time = roomDetail.raCleanStartTm;
                        record.rrec_Cleaning_End_Time = roomDetail.raCleanEndTm;

                        /*
                        //CRF-711: "Time Counter" for room cleaning should continue from where it stopped
                        if ([[UserManagerV2 sharedUserManager].currentUserAccessRight continueFromPausedTime]) {
                            //Don't update total cleaning time
                        } else {
                            record.rrec_Total_Cleaning_Time = [roomDetail.raTotalCleaningTime intValue] * 60;
                        }
                        */
                        // DungPhan - 20150918: Get total cleaning time (as seconds, old logic is minutes) from server - Discuss with FM
                        record.rrec_Total_Cleaning_Time = [roomDetail.raTotalCleaningTime intValue];
                        
                        record.rrec_Remark = roomDetail.raRemark;
                        record.rrec_PostStatus = (int)POST_STATUS_UN_CHANGED;
                        
                        if (record.rrec_Id == 0) {
                            [self insertRoomRecordData:record];
                        } else 
                            [self updateRoomRecordData:record];
                    }
                } else {
                    result = NO;
                }
            } else {
                result = NO;
            }
        }
    }
    
    return result;
}

-(BOOL) isValidRoomWSByUserId:(NSInteger) userID roomNumber:(NSString*)roomNumber hotelId: (NSString*) hotelID
{
    BOOL result = NO;
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole])
    {
        [binding setLogXMLInOut:YES];
    }
    eHousekeepingService_ValidateRoomNo *request = [[eHousekeepingService_ValidateRoomNo alloc] init];
    [request setIntUsrID: [NSNumber numberWithInteger:userID]];
    [request setStrRoomNo:roomNumber];
    [request setStrHotel_ID:hotelID];
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding ValidateRoomNoUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_ValidateRoomNoResponse class]]) {
            eHousekeepingService_ValidateRoomNoResponse *dataBody = (eHousekeepingService_ValidateRoomNoResponse *)bodyPart;
            if (dataBody != nil) {
                if ([dataBody.ValidateRoomNoResult.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_OK]]) {
                    result = YES;
                }
            }
        }
    }
    
    return result;
}

-(NSInteger)insertRoomModel:(RoomModelV2*)roomModel
{
    /*
     if (adapter  == nil) {
     adapter = [[RoomAdapter alloc] init];
     [adapter openDatabase];
     [adapter resetSqlCommand];
     int result = [adapter insertRoomData:roomModel];
     [adapter close];
     [adapter release];
     adapter = nil;
     return result;
     } else {
     [adapter openDatabase];
     [adapter resetSqlCommand];
     int result = [adapter insertRoomData:roomModel];
     [adapter close];
     return result;
     }*/
    RoomAdapterV2 *adapter1 = [RoomAdapterV2 createRoomAdapter];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    int result = [adapter1 insertRoomData:roomModel];
    [adapter1 close];

    adapter1 = nil;
    return result;    
    
}

-(int)deleteAllRoom
{
    DataTable *tableAddJobRoom = [[DataTable alloc] initWithNameFormat:@"%@", ROOM];
    NSString *queryDelete = [[NSMutableString alloc] initWithFormat:@"DELETE FROM %@", ROOM];
    
    int status = (int)[tableAddJobRoom excuteQueryNonSelect:queryDelete];
    return status;
}

-(NSInteger)updateRoomModel:(RoomModelV2*)roomModel
{
    /*
     if (adapter  == nil) {
     adapter = [[RoomAdapter alloc] init];
     [adapter openDatabase];
     [adapter resetSqlCommand];
     int result = [adapter updateRoomData:roomModel];
     [adapter close];
     [adapter release];
     adapter = nil;
     return result;
     } else {
     [adapter openDatabase];
     [adapter resetSqlCommand];
     int result = [adapter updateRoomData:roomModel];
     [adapter close];
     return result;
     }
     */
    RoomAdapterV2 *adapter1 = [RoomAdapterV2 createRoomAdapter];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    int result = [adapter1 updateRoomData:roomModel];
    [adapter1 close];

    adapter1 = nil;
    return result;
}

-(void)loadRoomModel:(RoomModelV2*)roomModel
{
    /*
     if (adapter  == nil) {
     adapter = [[RoomAdapter alloc] init];
     [adapter openDatabase];
     [adapter resetSqlCommand];
     [adapter loadRoomData:roomModel];
     [adapter close];
     [adapter release];
     adapter = nil;
     } else {
     [adapter openDatabase];
     [adapter resetSqlCommand];
     [adapter loadRoomData:roomModel];
     [adapter close];
     }*/
    RoomAdapterV2 *adapter1 = [RoomAdapterV2 createRoomAdapter];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    [adapter1 loadRoomData:roomModel];
    [adapter1 close];
 
    adapter1 = nil;
}

+(NSMutableArray *)getAllRoomType{
    RoomAdapterV2 *rAdapter = [RoomAdapterV2 createRoomAdapter];
    [rAdapter openDatabase];
    NSMutableArray *array = [NSMutableArray arrayWithArray:[rAdapter loadAllRoomType]];
    [rAdapter close];

    return array;
}

+(NSMutableArray *)getAllRoom{
    RoomAdapterV2 *rAdapter = [RoomAdapterV2 createRoomAdapter];
    [rAdapter openDatabase];
    NSMutableArray *array = [NSMutableArray arrayWithArray:[rAdapter loadAllRoom]];
    [rAdapter close];
 
    return array;
}

-(NSMutableArray *)loadAllRoomUserId:(NSInteger)userId
{
    RoomAdapterV2 *rAdapter = [RoomAdapterV2 createRoomAdapter];
    [rAdapter openDatabase];
    NSMutableArray *array = [rAdapter loadAllRoomByUserId:userId];
    [rAdapter close];
    
    NSMutableArray *result = [NSMutableArray array];
    for (NSString *roomId in array) {
        [rAdapter openDatabase];
        
        RoomModelV2 *model = [[RoomModelV2 alloc] init];
        model.room_Id = roomId;
        [rAdapter loadRoomData:model];
        
//        if ( model.room_VIPStatusId == ENUM_ROOM_STATUS_VC || model.room_VIPStatusId == ENUM_ROOM_STATUS_OC) {
//            [result addObject:model];
//        }
        
        [rAdapter close];
    }
    return result;
    
}

-(int) getWSOOSBlockRoomListByUserId:(NSInteger)userId hotelId:(NSInteger)hotelId listRoomAssignmentCompare:(NSMutableArray*) listRoomCompare {
    
    if (![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        return RESPONSE_STATUS_NO_NETWORK;
    }
    
    int result = RESPONSE_STATUS_NO_NETWORK;
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole]) {
        [binding setLogXMLInOut:YES];
    }
    eHousekeepingService_GetOOSBlockRoomsList *request = [[eHousekeepingService_GetOOSBlockRoomsList alloc] init];
    
    [request setIntUsrID:[NSNumber numberWithInteger:userId]];
    [request setIntHTID:[NSNumber numberWithInteger:hotelId]];
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetOOSBlockRoomsListUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    if(responseBodyParts == nil) {
        return RESPONSE_STATUS_ERROR;
    }
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetOOSBlockRoomsListResponse class]]) {
            eHousekeepingService_GetOOSBlockRoomsListResponse *dataBody = (eHousekeepingService_GetOOSBlockRoomsListResponse *)bodyPart;
            if (dataBody != nil) {
                if ([dataBody.GetOOSBlockRoomsListResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_OK]]) {
                    
                    result = RESPONSE_STATUS_OK;
                    
                    //set all room to status unlock
                    //[self unlockAllRoomBlockingByUserId:userId];
                    if(dataBody.GetOOSBlockRoomsListResult.BlockRoomList != nil && dataBody.GetOOSBlockRoomsListResult.BlockRoomList.BlockRoomDetails != nil) {
                        NSMutableArray *listBlockRoomDetail = dataBody.GetOOSBlockRoomsListResult.BlockRoomList.BlockRoomDetails;
                        
                        for (NSString *curRoomAssignment in listRoomCompare) {
                            //Unblock all rooms by current user ID
                            //Note: RoomBlocking Table is also use for getting release room from WS
                            //this will not be called at getting release room from WS
                            [self unlockRoomBlockingByUserId:(int)userId roomNumber:curRoomAssignment];
                            for (eHousekeepingService_BlockRoomDetails *blockRoomDetail in listBlockRoomDetail) {
                                
                                if([curRoomAssignment caseInsensitiveCompare:blockRoomDetail.strRoomNo] == NSOrderedSame) {
                                    RoomBlocking *roomBlocking = [[RoomBlocking alloc] init];
                                    roomBlocking.roomblocking_is_blocked = RoomBlockingStatus_Blocked;
                                    roomBlocking.roomblocking_reason = blockRoomDetail.strOOSReason;
                                    roomBlocking.roomblocking_remark_physical_check = blockRoomDetail.strOOSRemarks;
                                    roomBlocking.roomblocking_room_number = blockRoomDetail.strRoomNo;
                                    roomBlocking.roomblocking_user_id = userId;
                                    roomBlocking.roomblocking_oosdurations = blockRoomDetail.strOOSDuration;
                                    
                                    //Has data to update
                                    if([self countRoomBlockingByUserId:(int)userId roomNumber:blockRoomDetail.strRoomNo] > 0) {
                                        [self updateRoomBlocking:roomBlocking];
                                    } else {
                                        [self insertRoomBlocking:roomBlocking];
                                    }
                                    
                                    break;
                                }
                            }
                        }
                    }
                } else if([dataBody.GetOOSBlockRoomsListResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_NO_RERCORD_TO_DISPLAY]]) {
                    
                    result = RESPONSE_STATUS_NO_RERCORD_TO_DISPLAY;
                }
            }
        }
    }
    
    return result;
}

//NOTE: this function must be called after function -(int) getWSOOSBlockRoomListByUserId:(NSInteger)userId hotelId:(NSInteger)hotelId listRoomAssignmentCompare:(NSMutableArray*) listRoomCompare
//it is mandatory
-(int) getWSReleaseRoomListByUserId:(NSInteger)userId hotelId:(NSInteger)hotelId listRoomAssignmentCompare:(NSMutableArray*) listRoomCompare {
    
    if (![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        return RESPONSE_STATUS_NO_NETWORK;
    }
    
    int result = RESPONSE_STATUS_NO_NETWORK;
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole]) {
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_GetReleaseRoomsList *request = [[eHousekeepingService_GetReleaseRoomsList alloc] init];
    
    [request setIntUsrID:[NSNumber numberWithInteger:userId]];
    [request setIntHTID:[NSNumber numberWithInteger:hotelId]];
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetReleaseRoomsListUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    if(responseBodyParts == nil) {
        return RESPONSE_STATUS_ERROR;
    }
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetReleaseRoomsListResponse class]]) {
            eHousekeepingService_GetReleaseRoomsListResponse *dataBody = (eHousekeepingService_GetReleaseRoomsListResponse *)bodyPart;
            if (dataBody != nil) {
                if ([dataBody.GetReleaseRoomsListResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_OK]]) {
                    
                    result = RESPONSE_STATUS_OK;
                    
                    //set all room to status unlock
                    //[self unlockAllRoomBlockingByUserId:userId];
                    if(dataBody.GetReleaseRoomsListResult.ReleaseRoomsList != nil && dataBody.GetReleaseRoomsListResult.ReleaseRoomsList.ReleaseRoomDetails != nil) {
                        NSMutableArray *listBlockRoomDetail = dataBody.GetReleaseRoomsListResult.ReleaseRoomsList.ReleaseRoomDetails;
                        
                        for (NSString *curRoomAssignment in listRoomCompare) {
                            
                            //[self unlockRoomBlockingByUserId:userId roomNumber:curRoomAssignment];
                            
                            for (eHousekeepingService_ReleaseRoomDetails *blockRoomDetail in listBlockRoomDetail) {
                                
                                if([curRoomAssignment caseInsensitiveCompare:blockRoomDetail.strRoomNo] == NSOrderedSame) {
                                    RoomBlocking *roomBlocking = [[RoomBlocking alloc] init];
                                    roomBlocking.roomblocking_is_blocked = RoomBlockingStatus_Release;
                                    roomBlocking.roomblocking_reason = blockRoomDetail.strReason;
                                    roomBlocking.roomblocking_remark_physical_check = blockRoomDetail.strRemarks;
                                    roomBlocking.roomblocking_room_number = blockRoomDetail.strRoomNo;
                                    roomBlocking.roomblocking_user_id = userId;
                                    
                                    //Has data to update
                                    if([self countRoomBlockingByUserId:(int)userId roomNumber:blockRoomDetail.strRoomNo] > 0) {
                                        [self updateRoomBlocking:roomBlocking];
                                    } else {
                                        [self insertRoomBlocking:roomBlocking];
                                    }
                                    
                                    break;
                                }
                            }
                        }
                    }
                } else if([dataBody.GetReleaseRoomsListResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_NO_RERCORD_TO_DISPLAY]]) {
                    
                    result = RESPONSE_STATUS_NO_RERCORD_TO_DISPLAY;
                }
            }
        }
    }
    
    return result;
}

-(NSString*) getRoomRemarksByUserId:(NSInteger)userId hotelId:(NSInteger)hotelId remarkType:(int)remarkType roomNumber:(NSString*)roomNumber {
    
    if (![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        return nil;
    }
    
    NSString *result = @"";
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole]) {
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_GetRoomRemarks *request = [[eHousekeepingService_GetRoomRemarks alloc] init];
    
    [request setIntUsrID:[NSNumber numberWithInteger:userId]];
    [request setIntHTID:[NSNumber numberWithInteger:hotelId]];
    [request setStrRoomNo:roomNumber];
    [request setIntRemarksType:[NSNumber numberWithInteger:remarkType]];
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetRoomRemarksUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    if(responseBodyParts == nil) {
        return nil;
    }
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetRoomRemarksResponse class]]) {
            eHousekeepingService_GetRoomRemarksResponse *dataBody = (eHousekeepingService_GetRoomRemarksResponse *)bodyPart;
            if (dataBody != nil) {
                if ([dataBody.GetRoomRemarksResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_OK]]) {
                    
                    return dataBody.GetRoomRemarksResult.Remarks;
                } else if([dataBody.GetRoomRemarksResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_NO_RERCORD_TO_DISPLAY]]) {
                    
                    return dataBody.GetRoomRemarksResult.Remarks;
                }
            }
        }
    }
    
    return result;
}

-(int) updateRoomRemarksByUserId:(NSInteger)userId hotelId:(NSInteger)hotelId remarkType:(int)remarkType roomNumber:(NSString*)roomNumber remarkValue:(NSString*)remarkValue {
    
    if (![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        return RESPONSE_STATUS_NO_NETWORK;
    }
    
    int result = RESPONSE_STATUS_NO_NETWORK;
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole]) {
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_UpdateRoomRemarks *request = [[eHousekeepingService_UpdateRoomRemarks alloc] init];
    
    [request setIntUsrID:[NSNumber numberWithInteger:userId]];
    [request setIntHTID:[NSNumber numberWithInteger:hotelId]];
    [request setStrRoomNo:roomNumber];
    [request setIntRemarksType:[NSNumber numberWithInteger:remarkType]];
    [request setStrRemarks:remarkValue];
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding UpdateRoomRemarksUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    if(responseBodyParts == nil) {
        return RESPONSE_STATUS_ERROR;
    }
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_UpdateRoomRemarksResponse class]]) {
            eHousekeepingService_UpdateRoomRemarksResponse *dataBody = (eHousekeepingService_UpdateRoomRemarksResponse *)bodyPart;
            if (dataBody != nil) {
                if ([dataBody.UpdateRoomRemarksResult.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_OK]]) {
                    return RESPONSE_STATUS_OK;
                }
            }
        }
    }
    
    return result;
}

#pragma mark - Room Type

-(BOOL) insertRoomTypeModel:(RoomTypeModel *)model
{
    RoomAdapterV2 *adapter = [[RoomAdapterV2 alloc] init];
    [adapter openDatabase];
    BOOL result = [adapter insertRoomTypeModel:model];
    [adapter close];
    return result;
}
-(BOOL) updateRoomTypeModel:(RoomTypeModel *) model
{
    RoomAdapterV2 *adapter = [[RoomAdapterV2 alloc] init];
    [adapter openDatabase];
    BOOL result = [adapter updateRoomTypeModel:model];
    [adapter close];
    return result;
}
-(BOOL) deleteRoomTypeModel:(RoomTypeModel *) model
{
    RoomAdapterV2 *adapter = [[RoomAdapterV2 alloc] init];
    [adapter openDatabase];
    BOOL result = [adapter deleteRoomTypeModel:model];
    [adapter close];
    return result;
}

-(RoomTypeModel *) loadRoomTypeModelByPrimaryKey:(RoomTypeModel *) model
{
    RoomAdapterV2 *adapter = [[RoomAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter loadRoomTypeModelByPrimaryKey:model];
    [adapter close];
    return model;
}

-(NSMutableArray *)loadAllRoomType
{
    RoomAdapterV2 *adapter = [[RoomAdapterV2 alloc] init];
    [adapter openDatabase];
    NSMutableArray *array = [adapter loadAllRoomTypeModel];
    [adapter close];
    return array;
    
}

#pragma mark - Room Blocking

-(int) insertRoomBlocking:(RoomBlocking*)model
{
    if(model == nil){
        return 0;
    }
    
    if(model.roomblocking_room_number.length <= 0){
        return 0;
    }
    
    if(model.roomblocking_user_id <= 0){
        return 0;
    }
    
    if(model.roomblocking_reason.length <= 0){
        model.roomblocking_reason = @"";
    }
    
    if(model.roomblocking_remark_physical_check.length <= 0){
        model.roomblocking_remark_physical_check = @"";
    }
    
    if(model.roomblocking_oosdurations.length <= 0){
        model.roomblocking_oosdurations = @"";
    }
    
    DataRow *rowInsert = [[DataRow alloc] init];
    rowInsert.tableName = [NSString stringWithFormat:@"%@", ROOM_BLOCKING];
    [rowInsert addColumnStringValue:model.roomblocking_room_number formatName:@"%@",roomblocking_room_number];
    [rowInsert addColumnIntValue:(int)model.roomblocking_user_id formatName:@"%@", roomblocking_user_id];
    [rowInsert addColumnIntValue:(int)model.roomblocking_is_blocked formatName:@"%@",roomblocking_is_blocked];
    [rowInsert addColumnStringValue:model.roomblocking_remark_physical_check formatName:@"%@", roomblocking_remark_physical_check];
    [rowInsert addColumnStringValue:model.roomblocking_reason formatName:@"%@", roomblocking_reason];
    [rowInsert addColumnStringValue:model.roomblocking_oosdurations formatName:@"%@", roomblocking_oosdurations];
    
    return [rowInsert insertDBRow]; //return id inserted

}

-(int) updateRoomBlocking:(RoomBlocking*)model
{
    if(model == nil){
        return 0;
    }
    
    if(model.roomblocking_room_number.length <= 0){
        return 0;
    }
    
    if(model.roomblocking_user_id <= 0){
        return 0;
    }
    
    if(model.roomblocking_reason.length <= 0){
        model.roomblocking_reason = @"";
    }
    
    if(model.roomblocking_remark_physical_check.length <= 0){
        model.roomblocking_remark_physical_check = @"";
    }
    
    if(model.roomblocking_oosdurations.length <= 0){
        model.roomblocking_oosdurations = @"";
    }
    
    DataRow *rowUpdate = [[DataRow alloc] init];
    rowUpdate.tableName = [NSString stringWithFormat:@"%@", ROOM_BLOCKING];
    
    DataColumn *columnUpdate1 = [rowUpdate addColumnInt:[NSString stringWithFormat:@"%@",roomblocking_user_id] value:(int)model.roomblocking_user_id];
    columnUpdate1.isUpdateCondition = YES;
    DataColumn *columnUpdate2 = [rowUpdate addColumnString:[NSString stringWithFormat:@"%@",roomblocking_room_number] value:model.roomblocking_room_number];
    columnUpdate2.isUpdateCondition = YES;
    
    [rowUpdate addColumnStringValue:model.roomblocking_reason formatName:@"%@",roomblocking_reason];
    [rowUpdate addColumnStringValue:model.roomblocking_remark_physical_check formatName:@"%@",roomblocking_remark_physical_check];
    [rowUpdate addColumnIntValue:(int)model.roomblocking_is_blocked formatName:@"%@",roomblocking_is_blocked];
    [rowUpdate addColumnStringValue:model.roomblocking_oosdurations formatName:@"%@",roomblocking_oosdurations];
    
    return [rowUpdate commitChangeDBRow];
}

-(int) deleteAllRoomBlockingByUserId:(int) userId
{
    DataTable *tableDelete = [[DataTable alloc] initWithNameFormat:@"%@",ROOM_BLOCKING];
    
    NSString *queryDelete = [NSString stringWithFormat:@"Delete from %@ Where %@ = %d", ROOM_BLOCKING, roomblocking_user_id, userId];
    int status = (int)[tableDelete excuteQueryNonSelect:queryDelete];
    
    return status;
}

-(int) unlockAllRoomBlockingByUserId:(int) userId
{
    DataTable *tableUnlock = [[DataTable alloc] initWithNameFormat:@"%@", ROOM_BLOCKING];
    NSString *queryUnlock = [NSString stringWithFormat:@"update %@ set %@ = %d Where %@ = %d", ROOM_BLOCKING,roomblocking_is_blocked, 0, roomblocking_user_id, userId];
    int status = (int)[tableUnlock excuteQueryNonSelect:queryUnlock];
    return status;
}

-(int) unlockRoomBlockingByUserId:(int) userId roomNumber: (NSString*)roomNumber{
    DataTable *tableUnlock = [[DataTable alloc] initWithNameFormat:@"%@", ROOM_BLOCKING];
    NSString *queryUnlock = [NSString stringWithFormat:@"update %@ set %@ = %d Where %@ = %d and %@ = '%@'", ROOM_BLOCKING,roomblocking_is_blocked, 0, roomblocking_user_id, userId, roomblocking_room_number, roomNumber];
    int status = (int)[tableUnlock excuteQueryNonSelect:queryUnlock];
    return status;
}

-(RoomBlocking*) loadRoomBlockingByUserId:(int)userId roomNumber:(NSString*) roomNumber
{
    if(roomNumber.length <= 0) {
        return  nil;
    }
    
    RoomBlocking *result = nil;
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"select %@, %@, %@, %@, %@, %@ from %@ where %@ = %d and %@ = '%@'",
                                  //select
                                  roomblocking_is_blocked,
                                  roomblocking_reason,
                                  roomblocking_remark_physical_check,
                                  roomblocking_room_number,
                                  roomblocking_user_id,
                                  roomblocking_oosdurations,
                                  
                                  //From
                                  ROOM_BLOCKING,
                                  
                                  //where
                                  roomblocking_user_id,
                                  userId,
                                  //and
                                  roomblocking_room_number,
                                  roomNumber];
    
    DataTable *table = [[DataTable alloc] init];
    
    //Row result reference
    DataRow *rowReference = [[DataRow alloc] init];
    [rowReference addColumnFormatInt:@"%@", roomblocking_is_blocked];
    [rowReference addColumnFormatString:@"%@",roomblocking_reason];
    [rowReference addColumnFormatString:@"%@", roomblocking_remark_physical_check];
    [rowReference addColumnFormatString:@"%@",roomblocking_room_number];
    [rowReference addColumnFormatInt:@"%@", roomblocking_user_id];
    [rowReference addColumnFormatString:@"%@",roomblocking_oosdurations];
    [table loadTableDataWithQuery:sqlString referenceRow:rowReference];
    
    int rowCount = [table countRows];
    if(rowCount > 0)
    {
        result = [[RoomBlocking alloc] init];
        DataRow *row = [table rowWithIndex:0];
        
        result.roomblocking_is_blocked = [row intValueWithColumnNameFormat:@"%@", roomblocking_is_blocked];
        result.roomblocking_reason = [row stringValueWithColumnNameFormat:@"%@", roomblocking_reason];
        result.roomblocking_remark_physical_check = [row stringValueWithColumnNameFormat:@"%@", roomblocking_remark_physical_check];
        result.roomblocking_room_number = [row stringValueWithColumnNameFormat:@"%@", roomblocking_room_number];
        result.roomblocking_user_id = [row intValueWithColumnNameFormat:@"%@", roomblocking_user_id];
        result.roomblocking_oosdurations = [row stringValueWithColumnNameFormat:@"%@", roomblocking_oosdurations];
    }
    
    return result;
}

//checking whether has room blocking data or not
-(int) countRoomBlockingByUserId:(int)userId roomNumber:(NSString*)roomNumber
{
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"select count(1) from %@ where %@ = %d and %@ = '%@'",
                                  //count
                                  
                                  //From
                                  ROOM_BLOCKING,
                                  
                                  //where
                                  roomblocking_user_id,
                                  userId,
                                  
                                  //and
                                  roomblocking_room_number,
                                  roomNumber];
    
    DataTable *table = [[DataTable alloc] init];
    
    //Row result reference
    DataRow *rowReference = [[DataRow alloc] init];
    [rowReference addColumnFormatInt:@"CountResult"];
    [table loadTableDataWithQuery:sqlString referenceRow:rowReference];
    
    int rowCount = [table countRows];
    int countResult = 0;
    if(rowCount > 0) {
        DataRow *row = [table rowWithIndex:0];
        countResult = [row intValueWithColumnNameFormat:@"CountResult"];
    }
    
    return countResult;
}

#pragma mark
#pragma mark insert, update, delete for guest info model
-(NSInteger)insertGuestInfo:(GuestInfoModelV2*)guestInfoModel{
    /*
     if (guestInfoAdapter == nil) {
     guestInfoAdapter = [[GuestInfoAdapter alloc] init];
     [guestInfoAdapter openDatabase];
     [guestInfoAdapter resetSqlCommand];
     int result = [guestInfoAdapter insertGuestInfoData:guestInfoModel];
     [guestInfoAdapter close];
     [guestInfoAdapter release];
     guestInfoAdapter = nil;
     return result;
     } else {
     [guestInfoAdapter openDatabase];
     [guestInfoAdapter resetSqlCommand];
     int result = [guestInfoAdapter insertGuestInfoData:guestInfoModel];
     [guestInfoAdapter close];
     return result;
     }*/
//    GuestInfoAdapterV2 *guestInfoAdapter1 = [GuestInfoAdapterV2 createGuestInfoAdapter];
//    [guestInfoAdapter1 openDatabase];
//    [guestInfoAdapter1 resetSqlCommand];
//    int result = [guestInfoAdapter1 insertGuestInfoData:guestInfoModel];
//    [guestInfoAdapter1 close];
//
//    guestInfoAdapter1 = nil;
//    return result;
    
    if(guestInfoModel == nil){
        return 0;
    }
    
    if(guestInfoModel.guestName.length <= 0) {
       guestInfoModel.guestName = @"";
    }
    
    if(guestInfoModel.guestLangPref.length <= 0) {
        guestInfoModel.guestLangPref = @"";
    }
    
    if(guestInfoModel.guestRoomId.length <= 0) {
        return 0;
    }
    
    if(guestInfoModel.guestCheckInTime.length <= 0) {
        guestInfoModel.guestCheckInTime = @"";
    }
    
    if(guestInfoModel.guestCheckOutTime.length <= 0) {
        guestInfoModel.guestCheckOutTime = @"";
    }
    
    if(guestInfoModel.guestLastModified.length <= 0) {
        guestInfoModel.guestLastModified = @"";
    }
    
    if(guestInfoModel.guestHousekeeperName.length <= 0) {
        guestInfoModel.guestHousekeeperName = @"";
    }
    
    if(guestInfoModel.guestNumber.length <= 0) {
        guestInfoModel.guestNumber = @"";
    }
    
    if(guestInfoModel.guestSpecialService.length <= 0) {
        guestInfoModel.guestSpecialService = @"";
    }
    
    if(guestInfoModel.guestPreferenceCode.length <= 0) {
        guestInfoModel.guestPreferenceCode = @"";
    }
    
    DataRow *rowInsert = [[DataRow alloc] init];
    rowInsert.tableName = [NSString stringWithFormat:@"%@", GUEST_INFOS];
    
    [rowInsert addColumnStringValue:guestInfoModel.guestName formatName:@"%@", guest_name];
    [rowInsert addColumnDataValue:guestInfoModel.guestImage formatName:@"%@", guest_image];
    [rowInsert addColumnStringValue:guestInfoModel.guestLangPref formatName:@"%@", guest_lang_pref];
    [rowInsert addColumnStringValue:guestInfoModel.guestRoomId formatName:@"%@", guest_room_id];
    [rowInsert addColumnStringValue:guestInfoModel.guestCheckInTime formatName:@"%@", guest_check_in_time];
    [rowInsert addColumnStringValue:guestInfoModel.guestCheckOutTime formatName:@"%@", guest_check_out_time];
    [rowInsert addColumnIntValue:(int)guestInfoModel.guestPostStatus formatName:@"%@", guest_post_status];
    [rowInsert addColumnStringValue:guestInfoModel.guestLastModified formatName:@"%@", guest_last_modified];
    [rowInsert addColumnStringValue:guestInfoModel.guestVIPCode formatName:@"%@", vip_number];
    [rowInsert addColumnStringValue:guestInfoModel.guestHousekeeperName formatName:@"%@", housekeeper_name];
    [rowInsert addColumnStringValue:guestInfoModel.guestNumber formatName:@"%@", guest_number];
    [rowInsert addColumnIntValue:(int)guestInfoModel.guestType formatName:@"%@", guest_type];
    //[rowInsert addColumnStringValue:guestInfoModel.guestGuestRef formatName:@"%@", guest_pref_desc];
    [rowInsert addColumnIntValue:guestInfoModel.guestTitle formatName:@"%@", guest_title];
    [rowInsert addColumnStringValue:guestInfoModel.guestSpecialService formatName:@"%@", guest_special_service];
    [rowInsert addColumnStringValue:guestInfoModel.guestPreferenceCode formatName:@"%@", guest_preference_code];
    
    return [rowInsert insertDBRow];
    
}

//-(NSInteger)updateGuestInfo:(GuestInfoModelV2*)guestInfoModel{
//    /*
//     if (guestInfoAdapter == nil) {
//     guestInfoAdapter = [[GuestInfoAdapter alloc] init];
//     [guestInfoAdapter openDatabase];
//     [guestInfoAdapter resetSqlCommand];
//     int result = [guestInfoAdapter updateGuestInfoData:guestInfoModel];
//     [guestInfoAdapter close];
//     [guestInfoAdapter release];
//     guestInfoAdapter = nil;
//     return result;
//     } else {
//     [guestInfoAdapter openDatabase];
//     [guestInfoAdapter resetSqlCommand];
//     int result = [guestInfoAdapter updateGuestInfoData:guestInfoModel];
//     [guestInfoAdapter close];
//     return result;
//     }*/
//    GuestInfoAdapterV2 *guestInfoAdapter1 = [GuestInfoAdapterV2 createGuestInfoAdapter];
//    [guestInfoAdapter1 openDatabase];
//    [guestInfoAdapter1 resetSqlCommand];
//    int result = [guestInfoAdapter1 updateGuestInfoData:guestInfoModel];
//    [guestInfoAdapter1 close];
//    
//    guestInfoAdapter1 = nil;
//    return result;
//    
//}

-(void)loadGuestInfo:(GuestInfoModelV2*)guestInfoModel{
    /*
     if (guestInfoAdapter == nil) {
     guestInfoAdapter = [[GuestInfoAdapter alloc] init];
     [guestInfoAdapter openDatabase];
     [guestInfoAdapter resetSqlCommand];
     [guestInfoAdapter loadGuestInfoData:guestInfoModel];
     [guestInfoAdapter close];
     [guestInfoAdapter release];
     guestInfoAdapter = nil;
     } else {
     [guestInfoAdapter openDatabase];
     [guestInfoAdapter resetSqlCommand];
     [guestInfoAdapter loadGuestInfoData:guestInfoModel];
     [guestInfoAdapter close];
     }*/
    GuestInfoAdapterV2 *guestInfoAdapter1 = [GuestInfoAdapterV2 createGuestInfoAdapter];
    [guestInfoAdapter1 openDatabase];
    [guestInfoAdapter1 resetSqlCommand];
    [guestInfoAdapter1 loadGuestInfoData:guestInfoModel];
    [guestInfoAdapter1 close];
  
    guestInfoAdapter1 = nil;
    
}

-(void)loadByRoomIDGuestInfo:(GuestInfoModelV2*)guestInfoModel{
    /*
     if (guestInfoAdapter == nil) {
     guestInfoAdapter = [[GuestInfoAdapter alloc] init];
     [guestInfoAdapter openDatabase];
     [guestInfoAdapter resetSqlCommand];
     [guestInfoAdapter loadGuestInfoDataByRoomID:guestInfoModel];
     [guestInfoAdapter close];
     [guestInfoAdapter release];
     guestInfoAdapter = nil;
     } else {
     [guestInfoAdapter openDatabase];
     [guestInfoAdapter resetSqlCommand];
     [guestInfoAdapter loadGuestInfoDataByRoomID:guestInfoModel];
     [guestInfoAdapter close];
     }*/
//    GuestInfoAdapterV2 *guestInfoAdapter1 = [GuestInfoAdapterV2 createGuestInfoAdapter];
//    [guestInfoAdapter1 openDatabase];
//    [guestInfoAdapter1 resetSqlCommand];
//    [guestInfoAdapter1 loadGuestInfoDataByRoomID:guestInfoModel.guestRoomId];
//    [guestInfoAdapter1 close];
// 
//    guestInfoAdapter1 = nil;
}

-(GuestInfoModelV2 *)loadByRoomID:(NSString*) roomNumber
{
    GuestInfoModelV2 *guestInfoModel = nil;
    GuestInfoAdapterV2 *guestInfoAdapter1 = [GuestInfoAdapterV2 createGuestInfoAdapter];
    [guestInfoAdapter1 openDatabase];
    [guestInfoAdapter1 resetSqlCommand];
    ///guestInfoModel = [guestInfoAdapter1 loadGuestInfoDataByRoomID:roomNumber];
    [guestInfoAdapter1 close];
    
    guestInfoAdapter1 = nil;
    return guestInfoModel;
}

-(NSMutableArray*)loadGuestsByUserId:(int)userId roomNumber:(NSString*)roomNumber guestType:(int)guestType
{
    if(userId <= 0){
        return nil;
    }
    
    if(roomNumber.length <= 0) {
        return nil;
    }
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@, %@, %@,%@, %@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ = '%@' and %@ = %d",
                           guest_id,
                           guest_name,
                           guest_image,
                           guest_lang_pref,
                           //guest_pref_desc,
                           guest_room_id,
                           guest_check_in_time,
                           guest_check_out_time,
                           guest_post_status,
                           guest_last_modified,
                           vip_number,
                           housekeeper_name,
                           guest_number,
                           guest_type,
                           guest_title,
                           guest_special_service,
                           guest_preference_code,
                           
                           GUEST_INFOS,
                           
                           guest_room_id,
                           roomNumber,
                           guest_type,
                           guestType];
    
    DataTable *table = [[DataTable alloc] init];
    //Row result reference
    DataRow *rowReference = [[DataRow alloc] init];
    [rowReference addColumnFormatInt:@"%@", guest_id];
    [rowReference addColumnFormatString:@"%@", guest_name];
    [rowReference addColumnFormatData:@"%@", guest_image];
    [rowReference addColumnFormatString:@"%@", guest_lang_pref];
    [rowReference addColumnFormatString:@"%@", guest_room_id];
    [rowReference addColumnFormatString:@"%@", guest_check_in_time];
    [rowReference addColumnFormatString:@"%@", guest_check_out_time];
    [rowReference addColumnFormatInt:@"%@", guest_post_status];
    [rowReference addColumnFormatString:@"%@", guest_last_modified];
    [rowReference addColumnFormatString:@"%@", vip_number];
    [rowReference addColumnFormatString:@"%@", housekeeper_name];
    [rowReference addColumnFormatString:@"%@", guest_number];
    [rowReference addColumnFormatString:@"%@", guest_type];
    //[rowReference addColumnFormatString:@"%@", guest_pref_desc];
    [rowReference addColumnFormatString:@"%@", guest_title];
    [rowReference addColumnFormatString:@"%@", guest_special_service];
    [rowReference addColumnFormatString:@"%@", guest_preference_code];
    
    [table loadTableDataWithQuery:sqlString referenceRow:rowReference];
    
    int rowCount = [table countRows];
    NSMutableArray *results = nil;
    if(rowCount > 0)
    {
        results = [NSMutableArray array];
        for (int i = 0; i < rowCount; i ++) {
            
            GuestInfoModelV2 *model = [[GuestInfoModelV2 alloc] init];
            DataRow *row = [table rowWithIndex:i];
            
            model.guestId = [row intValueWithColumnNameFormat:@"%@", guest_id];
            model.guestName = [row stringValueWithColumnNameFormat:@"%@", guest_name];
            model.guestImage = [row dataValueWithColumnNameFormat:@"%@", guest_image];
            model.guestLangPref = [row stringValueWithColumnNameFormat:@"%@", guest_lang_pref];
            model.guestRoomId = [row stringValueWithColumnNameFormat:@"%@", guest_room_id];
            model.guestCheckInTime = [row stringValueWithColumnNameFormat:@"%@", guest_check_in_time];
            model.guestCheckOutTime = [row stringValueWithColumnNameFormat:@"%@", guest_check_out_time];
            model.guestPostStatus = [row intValueWithColumnNameFormat:@"%@", guest_post_status];
            model.guestLastModified = [row stringValueWithColumnNameFormat:@"%@", guest_last_modified];
            model.guestVIPCode = [row stringValueWithColumnNameFormat:@"%@", vip_number];
            model.guestHousekeeperName = [row stringValueWithColumnNameFormat:@"%@", housekeeper_name];
            model.guestNumber = [row stringValueWithColumnNameFormat:@"%@", guest_number];
            model.guestType = [row intValueWithColumnNameFormat:@"%@", guest_type];
            //model.guestGuestRef = [row stringValueWithColumnNameFormat:@"%@", guest_pref_desc];
            model.guestTitle = [row stringValueWithColumnNameFormat:@"%@", guest_title];
            model.guestPreferenceCode = [row stringValueWithColumnNameFormat:@"%@", guest_preference_code];
            model.guestSpecialService = [row stringValueWithColumnNameFormat:@"%@", guest_special_service];
            
            [results addObject:model];
        }
    }
    
    return results;
}

-(BOOL)isDueOutRoomByRoomId:(NSInteger) roomId
{
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@ FROM %@ WHERE %@ = %d",guest_check_out_time, GUEST_INFOS ,guest_room_id,(int)roomId];
    DataTable *tableGuest = [[DataTable alloc] initWithName:[NSString stringWithFormat:@"%@",GUEST_INFOS]];
    DataRow *rowRef = [[DataRow alloc] init];
    [rowRef addColumnString:[NSString stringWithFormat:@"%@",guest_check_out_time]];
    
    [tableGuest loadTableDataWithQuery:sqlString referenceRow:rowRef];
    
    if([tableGuest countRows] > 0)
    {
        NSString *strGuestCheckOutTime = [[[tableGuest rowWithIndex:0] columnWithName:[NSString stringWithFormat:@"%@",guest_check_out_time]] fieldValueString];
        strGuestCheckOutTime = [self convertGuestInfoDateTime:strGuestCheckOutTime];
        BOOL isDueOutRoom = [self isDueOutRoomByDateString:strGuestCheckOutTime];
        return isDueOutRoom;
    }
    
    return FALSE;
}
//WS ProfileNote
-(BOOL)loadWSProfileNoteByUserId:(int)userId hotelId:(int)hotelId roomNumber:(NSString*)roomNumber
{
    //No network detected
    if(![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]){
        return NO;
    }
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole]){
        [binding setLogXMLInOut:YES];
    }
    
    //Require fields
    if(hotelId <= 0){return NO;}
    if(userId <= 0){return NO;}
    if(roomNumber <= 0){return NO;}
    
    eHousekeepingService_GetProfileNote *request = [[eHousekeepingService_GetProfileNote alloc] init];
    
    //Require fields
    [request setIntHotelID:[NSNumber numberWithInt:hotelId]];
    [request setIntUserID:[NSNumber numberWithInt:userId]];
    [request setSRoomNo:roomNumber];
    
    BOOL result = NO;
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetProfileNoteUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetProfileNoteResponse class]]) {
            eHousekeepingService_GetProfileNoteResponse *dataBody = (eHousekeepingService_GetProfileNoteResponse *)bodyPart;
            if (dataBody != nil) {
                if ([dataBody.GetProfileNoteResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_OK]]
                    || [dataBody.GetProfileNoteResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_NO_RERCORD_TO_DISPLAY]]) {
                    
                    result = YES;
                    
                    NSMutableArray *listProfileNoteTypes = dataBody.GetProfileNoteResult.ProfileNoteTypeList.ProfileNoteType;
                    NSString *todayDateString = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
                    int userId = [UserManagerV2 sharedUserManager].currentUser.userId;
                    [self deleteProfileNoteTypeByUserId:userId roomNumber:roomNumber];
                    [self deleteProfileNoteByUserId:userId roomNumber:roomNumber];
                    
                    for (eHousekeepingService_ProfileNoteType *wsCurNoteType in listProfileNoteTypes) {
                        
                        ProfileNoteTypeV2 *curProfileNoteType = [[ProfileNoteTypeV2 alloc] init];
                        curProfileNoteType.profilenote_type_code = wsCurNoteType.typeCode;
                        curProfileNoteType.profilenote_type_description = wsCurNoteType.typeDescription;
                        curProfileNoteType.profilenote_type_id = [wsCurNoteType.typeID intValue];
                        curProfileNoteType.profilenote_type_last_modified = todayDateString;
                        curProfileNoteType.profilenote_type_roomnumber = roomNumber;
                        curProfileNoteType.profilenote_type_user_id = userId;
                        
                        [self insertProfileNoteType:curProfileNoteType];    
                        for (eHousekeepingService_ProfileNote *wsCurNote in wsCurNoteType.ProfileNoteList.ProfileNote) {
                            ProfileNoteV2 *curProfileNote = [[ProfileNoteV2 alloc] init];
                            curProfileNote.profilenote_description = wsCurNote.NoteDescription;
                            curProfileNote.profilenote_last_modified = todayDateString;
                            curProfileNote.profilenote_room_number = roomNumber;
                            curProfileNote.profilenote_type_id = curProfileNoteType.profilenote_type_id;
                            curProfileNote.profilenote_user_id = userId;
                            
                            [self insertProfileNote:curProfileNote];
                        }
                        
                    }
                }
            }
        }
    }
    
    return result;
}


//WS Search Guest
-(NSMutableArray*) findGuestStayedHistoryRoutineWSByUserId:(int)userId searchKey:(NSString*)searchKey hotelId:(int)hotelId buildingId:(int)buildingId filterMethod:(int)filterMethod filterRange:(int)filterRange pageNumber:(int)pageNumber {
    //No network detected
    if(![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]){
        return nil;
    }
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole]){
        [binding setLogXMLInOut:YES];
    }
    
    //Require fields
    if(hotelId <= 0){return nil;}
    if(userId <= 0){return nil;}
    if(buildingId <= 0){return nil;}
    
    eHousekeepingService_FindGuestStayedHistoryRoutine *request = [[eHousekeepingService_FindGuestStayedHistoryRoutine alloc] init];
    
    [request setUserID:[NSString stringWithFormat:@"%d", userId]];
    [request setPropertyID:[NSString stringWithFormat:@"%d", hotelId]];
    [request setBuildingID:[NSString stringWithFormat:@"%d", buildingId]];
    [request setFilterStatus:[NSString stringWithFormat:@"%d",filterMethod]];
    [request setFilterRange:[NSString stringWithFormat:@"%d",filterRange]];
    [request setPageNumber:[NSString stringWithFormat:@"%d",pageNumber]];
    [request setSearch:searchKey];
    
    NSMutableArray *result = NO;
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding FindGuestStayedHistoryRoutineUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_FindGuestStayedHistoryRoutineResponse class]]) {
            eHousekeepingService_FindGuestStayedHistoryRoutineResponse *dataBody = (eHousekeepingService_FindGuestStayedHistoryRoutineResponse *)bodyPart;
            
            if (dataBody != nil) {
                result = dataBody.FindGuestStayedHistoryRoutineResult.FindGuestInformationList.FindGuestInformation;
            }
        }
    }
    
    return result;
}

//Localdatabase
//Guest Info Profile Note Type
-(NSMutableArray*) loadProfileNoteTypesByUserId:(int)userId roomNumber:(NSString*)roomNumber
{
    if(userId <= 0){
        return nil;
    }
    
    if(roomNumber.length <= 0){
        return nil;
    }
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"select %@, %@, %@, %@, %@, %@, %@ from %@ where %@ = %d and %@ = '%@'",
                                  //select
                                  profilenote_type_code,
                                  profilenote_type_description,
                                  profilenote_type_id,
                                  profilenote_type_last_modified,
                                  profilenote_type_roomnumber,
                                  profilenote_type_user_id,
                                  profilenote_type_guest_id,
                                  
                                  //From
                                  PROFILE_NOTE_TYPE,
                                  
                                  //where
                                  profilenote_type_user_id,
                                  userId,
                                  
                                  //and
                                  profilenote_type_roomnumber,
                                  roomNumber];
    
    DataTable *table = [[DataTable alloc] init];
    
    //Row result reference
    DataRow *rowReference = [[DataRow alloc] init];
    [rowReference addColumnFormatString:@"%@", profilenote_type_code];
    [rowReference addColumnFormatString:@"%@", profilenote_type_description];
    [rowReference addColumnFormatInt:@"%@", profilenote_type_id];
    [rowReference addColumnFormatString:@"%@", profilenote_type_last_modified];
    [rowReference addColumnFormatInt:@"%@", profilenote_type_roomnumber];
    [rowReference addColumnFormatInt:@"%@", profilenote_type_user_id];
    [rowReference addColumnFormatInt:@"%@", profilenote_type_guest_id];
    
    [table loadTableDataWithQuery:sqlString referenceRow:rowReference];
    
    NSMutableArray *results = nil;
    int rowCount = [table countRows];
    if(rowCount > 0)
    {
        
        results = [NSMutableArray array];
        for (int i = 0; i < rowCount; i ++) {
            ProfileNoteTypeV2 *model = [[ProfileNoteTypeV2 alloc] init];
            DataRow *row = [table rowWithIndex:i];
            
            model.profilenote_type_code = [row stringValueWithColumnNameFormat:@"%@",profilenote_type_code];
            model.profilenote_type_description = [row stringValueWithColumnNameFormat:@"%@",profilenote_type_description];
            model.profilenote_type_id = [row intValueWithColumnNameFormat:@"%@",profilenote_type_id];
            model.profilenote_type_last_modified = [row stringValueWithColumnNameFormat:@"%@",profilenote_type_last_modified];
            model.profilenote_type_roomnumber = [row stringValueWithColumnNameFormat:@"%@",profilenote_type_roomnumber];
            model.profilenote_type_user_id = [row intValueWithColumnNameFormat:@"%@",profilenote_type_user_id];
            model.profilenote_type_guest_id = [row intValueWithColumnNameFormat:@"%@",profilenote_type_guest_id];
            
            [results addObject:model];
        }
    }
    
    return results;
}

-(NSMutableArray*) loadProfileNoteTypesByUserId:(int)userId roomNumber:(NSString*)roomNumber guestId:(int)guestId
{
    if(userId <= 0){
        return nil;
    }
    
    if(roomNumber.length <= 0){
        return nil;
    }
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"select %@, %@, %@, %@, %@, %@, %@ from %@ where %@ = %d and %@ = '%@' and %@ = %d group by %@",
                                  //select
                                  profilenote_type_code,
                                  profilenote_type_description,
                                  profilenote_type_id,
                                  profilenote_type_last_modified,
                                  profilenote_type_roomnumber,
                                  profilenote_type_user_id,
                                  profilenote_type_guest_id,
                                  
                                  //From
                                  PROFILE_NOTE_TYPE,
                                  
                                  //where
                                  profilenote_type_user_id,
                                  userId,
                                  
                                  //and
                                  profilenote_type_roomnumber,
                                  roomNumber,
                                  
                                  //and
                                  profilenote_type_guest_id,
                                  guestId,
                                  
                                  profilenote_type_code];
    
    DataTable *table = [[DataTable alloc] init];
    
    //Row result reference
    DataRow *rowReference = [[DataRow alloc] init];
    [rowReference addColumnFormatString:@"%@", profilenote_type_code];
    [rowReference addColumnFormatString:@"%@", profilenote_type_description];
    [rowReference addColumnFormatInt:@"%@", profilenote_type_id];
    [rowReference addColumnFormatString:@"%@", profilenote_type_last_modified];
    [rowReference addColumnFormatInt:@"%@", profilenote_type_roomnumber];
    [rowReference addColumnFormatInt:@"%@", profilenote_type_user_id];
    [rowReference addColumnFormatInt:@"%@", profilenote_type_guest_id];
    
    [table loadTableDataWithQuery:sqlString referenceRow:rowReference];
    
    NSMutableArray *results = nil;
    int rowCount = [table countRows];
    if(rowCount > 0)
    {
        
        results = [NSMutableArray array];
        for (int i = 0; i < rowCount; i ++) {
            ProfileNoteTypeV2 *model = [[ProfileNoteTypeV2 alloc] init];
            DataRow *row = [table rowWithIndex:i];
            
            model.profilenote_type_code = [row stringValueWithColumnNameFormat:@"%@",profilenote_type_code];
            model.profilenote_type_description = [row stringValueWithColumnNameFormat:@"%@",profilenote_type_description];
            model.profilenote_type_id = [row intValueWithColumnNameFormat:@"%@",profilenote_type_id];
            model.profilenote_type_last_modified = [row stringValueWithColumnNameFormat:@"%@",profilenote_type_last_modified];
            model.profilenote_type_roomnumber = [row stringValueWithColumnNameFormat:@"%@",profilenote_type_roomnumber];
            model.profilenote_type_user_id = [row intValueWithColumnNameFormat:@"%@",profilenote_type_user_id];
            model.profilenote_type_guest_id = [row intValueWithColumnNameFormat:@"%@",profilenote_type_guest_id];
            
            [results addObject:model];
        }
    }
    
    return results;
}

-(int) insertProfileNoteType:(ProfileNoteTypeV2*)model
{
    if(model == nil){
        return 0;
    }
    
    if(model.profilenote_type_code == nil){
        model.profilenote_type_code = @"";
    }
    
    if(model.profilenote_type_description == nil){
        model.profilenote_type_description = @"";
    }
    
    DataRow *rowInsert = [[DataRow alloc] init];
    rowInsert.tableName = [NSString stringWithFormat:@"%@", PROFILE_NOTE_TYPE];
    
    [rowInsert addColumnString:[NSString stringWithFormat:@"%@",profilenote_type_code] value:model.profilenote_type_code];
    [rowInsert addColumnString:[NSString stringWithFormat:@"%@",profilenote_type_description] value:model.profilenote_type_description];
    [rowInsert addColumnInt:[NSString stringWithFormat:@"%@",profilenote_type_id] value:(int)model.profilenote_type_id];
    [rowInsert addColumnString:[NSString stringWithFormat:@"%@",profilenote_type_last_modified] value:model.profilenote_type_last_modified];
    [rowInsert addColumnString:[NSString stringWithFormat:@"%@",profilenote_type_roomnumber] value:model.profilenote_type_roomnumber];
    [rowInsert addColumnInt:[NSString stringWithFormat:@"%@",profilenote_type_user_id] value:(int)model.profilenote_type_user_id];
    [rowInsert addColumnInt:[NSString stringWithFormat:@"%@",profilenote_type_guest_id] value:(int)model.profilenote_type_guest_id];
    
    return [rowInsert insertDBRow]; //return id inserted
}

-(int) deleteProfileNoteTypeByUserId:(int)userId roomNumber:(NSString*)roomNumber
{
    if(userId <= 0){
        return 0;
    }
    
    if(roomNumber <= 0){
        return 0;
    }
    
    DataTable *tableDelete = [[DataTable alloc] initWithNameFormat:@"%@",PROFILE_NOTE_TYPE];
    NSString *queryDelete = [NSString stringWithFormat:@"Delete from %@ Where %@ = %d and %@ = '%@'", PROFILE_NOTE_TYPE, profilenote_type_user_id, userId, profilenote_type_roomnumber, roomNumber];
    int status = (int)[tableDelete excuteQueryNonSelect:queryDelete];
    
    return status;
}

-(int) deleteGuestByUserId:(int)userId roomNumber:(NSString*)roomNumber guestType:(int)guestType
{
    if(userId <= 0){
        return 0;
    }
    
    if(roomNumber.length <= 0){
        return 0;
    }
    
    DataTable *tableDelete = [[DataTable alloc] initWithNameFormat:@"%@",GUEST_INFOS];
    NSString *queryDelete = [NSString stringWithFormat:@"Delete from %@ Where %@ = '%@' and %@ = %d", GUEST_INFOS, guest_room_id, roomNumber, guest_type, guestType];
    int status = (int)[tableDelete excuteQueryNonSelect:queryDelete];
    
    return status;
}


-(int) updateProfileNoteType:(ProfileNoteTypeV2*)model
{
    if(model == nil){
        return 0;
    }
    
    if(model.profilenote_type_user_id <= 0){
        return 0;
    }
    
    if(model.profilenote_type_roomnumber <= 0) {
        return 0;
    }
    
    if(model.profilenote_type_code == nil){
        model.profilenote_type_code = @"";
    }
    
    if(model.profilenote_type_description == nil){
        model.profilenote_type_description = @"";
    }
    
    DataRow *rowUpdate = [[DataRow alloc] init];
    rowUpdate.tableName = [NSString stringWithFormat:@"%@", PROFILE_NOTE_TYPE];
    
    DataColumn *columnUpdate1 = [rowUpdate addColumnInt:[NSString stringWithFormat:@"%@",profilenote_type_user_id] value:(int)model.profilenote_type_user_id];
    columnUpdate1.isUpdateCondition = YES;
    DataColumn *columnUpdate2 = [rowUpdate addColumnString:[NSString stringWithFormat:@"%@",profilenote_type_roomnumber] value:model.profilenote_type_roomnumber];
    columnUpdate2.isUpdateCondition = YES;
    
    [rowUpdate addColumnIntValue:model.profilenote_type_code formatName:@"%@",profilenote_type_code];
    [rowUpdate addColumnIntValue:model.profilenote_type_description formatName:@"%@",profilenote_type_description];
    [rowUpdate addColumnIntValue:model.profilenote_type_last_modified formatName:@"%@",profilenote_type_last_modified];
    [rowUpdate addColumnIntValue:(int)model.profilenote_type_id formatName:@"%@",profilenote_type_id];
    [rowUpdate addColumnIntValue:(int)model.profilenote_type_guest_id formatName:@"%@",profilenote_type_guest_id];
    
    return [rowUpdate commitChangeDBRow];
}

//Guest Info Profile Note
-(NSMutableArray*) loadProfileNotesByUserId:(int)userId profileNoteTypeId:(int)profileNoteTypeId
{
    if(userId <= 0){
        return nil;
    }
    
    if(profileNoteTypeId <= 0){
        return nil;
    }
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"select %@, %@, %@, %@, %@, %@ from %@ where %@ = %d and %@ = %d",
                                  //select
                                  profilenote_description,
                                  profilenote_type_id,
                                  profilenote_last_modified,
                                  profilenote_user_id,
                                  profilenote_room_number,
                                  profilenote_guest_id,
                                  
                                  //From
                                  PROFILE_NOTE,
                                  
                                  //where
                                  profilenote_user_id,
                                  userId,
                                  
                                  //and
                                  profilenote_type_id,
                                  profileNoteTypeId];
    
    DataTable *table = [[DataTable alloc] init];
    
    //Row result reference
    DataRow *rowReference = [[DataRow alloc] init];
    [rowReference addColumnFormatString:@"%@", profilenote_description];
    [rowReference addColumnFormatInt:@"%@", profilenote_type_id];
    [rowReference addColumnFormatString:@"%@", profilenote_last_modified];
    [rowReference addColumnFormatInt:@"%@", profilenote_user_id];
    [rowReference addColumnFormatInt:@"%@", profilenote_room_number];
    [rowReference addColumnFormatInt:@"%@", profilenote_guest_id];
    
    [table loadTableDataWithQuery:sqlString referenceRow:rowReference];
    
    NSMutableArray *results = nil;
    int rowCount = [table countRows];
    if(rowCount > 0)
    {
        
        results = [NSMutableArray array];
        for (int i = 0; i < rowCount; i ++) {
            ProfileNoteV2 *model = [[ProfileNoteV2 alloc] init];
            DataRow *row = [table rowWithIndex:i];
            
            model.profilenote_description = [row stringValueWithColumnNameFormat:@"%@",profilenote_description];
            model.profilenote_type_id = [row intValueWithColumnNameFormat:@"%@",profilenote_type_id];
            model.profilenote_last_modified = [row stringValueWithColumnNameFormat:@"%@",profilenote_last_modified];
            model.profilenote_user_id = [row intValueWithColumnNameFormat:@"%@",profilenote_user_id];
            model.profilenote_room_number = [row stringValueWithColumnNameFormat:@"%@",profilenote_room_number];
            model.profilenote_guest_id = [row intValueWithColumnNameFormat:@"%@",profilenote_guest_id];
            [results addObject:model];
        }
    }
    
    return results;
}

//Guest Info Profile Note
-(NSMutableArray*) loadProfileNotesByUserId:(int)userId roomNumber:(NSString*)roomNumber guestId:(int)guestId
{
    if(userId <= 0){
        return nil;
    }
    
    if(guestId <= 0){
        return nil;
    }
    
    if(roomNumber.length <= 0){
        return nil;
    }
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"select %@, %@, %@, %@, %@, %@ from %@ where %@ = %d and %@ = '%@' and %@ = %d",
                                  //select
                                  profilenote_description,
                                  profilenote_type_id,
                                  profilenote_last_modified,
                                  profilenote_user_id,
                                  profilenote_room_number,
                                  profilenote_guest_id,
                                  
                                  //From
                                  PROFILE_NOTE,
                                  
                                  //where
                                  profilenote_user_id,
                                  userId,
                                  
                                  //and
                                  profilenote_room_number,
                                  roomNumber,
                                  
                                  //and
                                  profilenote_guest_id,
                                  guestId
                                  ];
    
    DataTable *table = [[DataTable alloc] init];
    
    //Row result reference
    DataRow *rowReference = [[DataRow alloc] init];
    [rowReference addColumnFormatString:@"%@", profilenote_description];
    [rowReference addColumnFormatInt:@"%@", profilenote_type_id];
    [rowReference addColumnFormatString:@"%@", profilenote_last_modified];
    [rowReference addColumnFormatInt:@"%@", profilenote_user_id];
    [rowReference addColumnFormatInt:@"%@", profilenote_room_number];
    [rowReference addColumnFormatInt:@"%@", profilenote_guest_id];
    
    [table loadTableDataWithQuery:sqlString referenceRow:rowReference];
    
    NSMutableArray *results = nil;
    int rowCount = [table countRows];
    if(rowCount > 0)
    {
        
        results = [NSMutableArray array];
        for (int i = 0; i < rowCount; i ++) {
            ProfileNoteV2 *model = [[ProfileNoteV2 alloc] init];
            DataRow *row = [table rowWithIndex:i];
            
            model.profilenote_description = [row stringValueWithColumnNameFormat:@"%@",profilenote_description];
            model.profilenote_type_id = [row intValueWithColumnNameFormat:@"%@",profilenote_type_id];
            model.profilenote_last_modified = [row stringValueWithColumnNameFormat:@"%@",profilenote_last_modified];
            model.profilenote_user_id = [row intValueWithColumnNameFormat:@"%@",profilenote_user_id];
            model.profilenote_room_number = [row stringValueWithColumnNameFormat:@"%@",profilenote_room_number];
            model.profilenote_guest_id = [row intValueWithColumnNameFormat:@"%@",profilenote_guest_id];
            [results addObject:model];
        }
    }
    
    return results;
}

-(int) insertProfileNote:(ProfileNoteV2*)model
{
    if(model == nil){
        return 0;
    }
    
    if(model.profilenote_type_id <= 0){
        return 0;
    }
    
    if(model.profilenote_user_id <= 0){
        return 0;
    }
    
    if(model.profilenote_description.length <= 0) {
        model.profilenote_description = @"";
    }
    
    DataRow *rowInsert = [[DataRow alloc] init];
    rowInsert.tableName = [NSString stringWithFormat:@"%@", PROFILE_NOTE];
    
    [rowInsert addColumnString:[NSString stringWithFormat:@"%@",profilenote_description] value:model.profilenote_description]; 
    [rowInsert addColumnString:[NSString stringWithFormat:@"%@",profilenote_last_modified] value:model.profilenote_last_modified];
    [rowInsert addColumnInt:[NSString stringWithFormat:@"%@",profilenote_user_id] value:(int)model.profilenote_user_id];
    [rowInsert addColumnInt:[NSString stringWithFormat:@"%@",profilenote_type_id] value:(int)model.profilenote_type_id];
    [rowInsert addColumnString:[NSString stringWithFormat:@"%@",profilenote_room_number] value:model.profilenote_room_number];
    [rowInsert addColumnInt:[NSString stringWithFormat:@"%@",profilenote_guest_id] value:(int)model.profilenote_guest_id];
    
    return [rowInsert insertDBRow]; //return id inserted
}

-(int) deleteProfileNoteByUserId:(int)userId profileNoteTypeId:(int)profileNoteTypeId
{
    if(userId <= 0) {
        return 0;
    }
    
    if(profileNoteTypeId <= 0){
        return 0;
    }
    
    DataTable *tableDelete = [[DataTable alloc] initWithNameFormat:@"%@",PROFILE_NOTE];
    NSString *queryDelete = [NSString stringWithFormat:@"Delete from %@ Where %@ = %d and %@ = %d", PROFILE_NOTE, profilenote_user_id, userId, profilenote_type_id, profileNoteTypeId];
    int status = (int)[tableDelete excuteQueryNonSelect:queryDelete];
    
    return status;
}

-(int) deleteProfileNoteByUserId:(int)userId roomNumber:(NSString*)roomNumber
{
    if(userId <= 0) {
        return 0;
    }
    
    if(roomNumber <= 0){
        return 0;
    }
    
    DataTable *tableDelete = [[DataTable alloc] initWithNameFormat:@"%@",PROFILE_NOTE];
    NSString *queryDelete = [NSString stringWithFormat:@"Delete from %@ Where %@ = %d and %@ = '%@'", PROFILE_NOTE, profilenote_user_id, userId, profilenote_room_number, roomNumber];
    int status = (int)[tableDelete excuteQueryNonSelect:queryDelete];
    
    return status;
}

//MARK: date function for Guest Info Data
//Private function to convert Guest Info date time
-(NSString*)convertGuestInfoDateTime:(NSString*)guestDateTime
{
    NSString *dateStr = nil;
    NSDate *dateResult = nil;
    NSDateFormatter *formater = [[NSDateFormatter alloc] init];
    [formater setDateFormat:@"yyyyMMdd:HHmmss"];
    dateResult = [formater dateFromString:guestDateTime];
    [formater setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    dateStr = [formater stringFromDate:dateResult];
    return dateStr;
}

//whether is Due Out Room or not
//PRD 2.1 wrote: Due out room (will be checked out today).
-(BOOL)isDueOutRoomByDateString:(NSString*)checkOutDateString
{
    BOOL result = NO;
    if (checkOutDateString.length > 0) {
        NSDateFormatter *dateTimeFormat = [[NSDateFormatter alloc] init];
        [dateTimeFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        NSDate* checkoutDate = [dateTimeFormat dateFromString:checkOutDateString];
        
        //Remove time from cleaning date
        unsigned int flags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit;
        NSCalendar* calendar = [NSCalendar currentCalendar];
        NSDateComponents* componentRemoved = [calendar components:flags fromDate:checkoutDate];
        NSDate* checkoutDateRemoveTime = [calendar dateFromComponents:componentRemoved];
        
        //Remove time from current date
        componentRemoved = [calendar components:flags fromDate:[NSDate date]];
        NSDate* currentDateRemoveTime = [calendar dateFromComponents:componentRemoved];
        
        NSCalendar *calender = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        NSDateComponents *components = [calender components:NSDayCalendarUnit fromDate:checkoutDateRemoveTime toDate:currentDateRemoveTime options:0];
        
        if (components.day == 0) {
            result = YES;
        }
    }
    
    return result;
}

-(NSInteger) numberOfMustPostGuestInfoRecordsWithUserId:(NSInteger)userId {
    GuestInfoAdapterV2 *gadapter = [GuestInfoAdapterV2 createGuestInfoAdapter];
    [gadapter openDatabase];
    NSInteger numberOfMustPostRecords = [gadapter numberOfMustPostGuestInfoRecordsWithUserId:userId];
    [gadapter close];
    return numberOfMustPostRecords;
}

#pragma mark -
#pragma mark SuperRoomModel Array Methods
-(NSMutableArray *) getAllRoomInspectionWithSupervisorId:(NSInteger) userId {
    SupRoomAssignmentAdapterV2 *sadapter = [SupRoomAssignmentAdapterV2 createSupRoomAssignmentAdapter];
    [sadapter openDatabase];
    [sadapter resetSqlCommand];
    NSMutableArray *temp = [sadapter getAllRoomInspectionWithSupervisorId:userId];
    [sadapter close];
    return temp;
}

-(NSMutableArray *) getAllRoomInspectionWithSupervisorId:(NSInteger) userId andRoomStatusID:(NSInteger)roomID {
    SupRoomAssignmentAdapterV2 *sadapter = [SupRoomAssignmentAdapterV2 createSupRoomAssignmentAdapter];
    [sadapter openDatabase];
    [sadapter resetSqlCommand];
    NSMutableArray *temp = [sadapter getAllRoomInspectionWithSupervisorId:userId andRoomStatusID:roomID];
    [sadapter close];
    return temp;
}

-(NSMutableArray *) getAllRoomByFloorId:(NSInteger)floorId AndUserId:(NSInteger)userId AndRoomStatus:(NSInteger)roomStatusId
{
    SupRoomAssignmentAdapterV2 *sadapter = [SupRoomAssignmentAdapterV2 createSupRoomAssignmentAdapter];
    [sadapter openDatabase];
    [sadapter resetSqlCommand];

    NSMutableArray *temp = [sadapter getAllRoomByFloorId:floorId AndUserId:userId AndRoomStatus:roomStatusId];
    [sadapter close];
    return temp;
}

-(NSMutableArray *) getAllRoomByFloorId:(NSInteger)floorId userId:(NSInteger)userId filterType:(NSInteger)filterType statusId:(NSInteger)statusId
{
    SupRoomAssignmentAdapterV2 *sadapter = [SupRoomAssignmentAdapterV2 createSupRoomAssignmentAdapter];
    [sadapter openDatabase];
    [sadapter resetSqlCommand];
    NSMutableArray *temp = [sadapter getAllRoomByFloorId:floorId userId:userId filterType:filterType statusId:statusId];
    [sadapter close];
    return temp;
}
-(NSMutableArray *) getAllRoomByFloorId:(NSInteger)floorId userId:(NSInteger)userId filterType:(NSInteger)filterType generalFilter:(NSString*)generalFilter
{
    SupRoomAssignmentAdapterV2 *sadapter = [SupRoomAssignmentAdapterV2 createSupRoomAssignmentAdapter];
    [sadapter openDatabase];
    [sadapter resetSqlCommand];
    NSMutableArray *temp = [sadapter getAllRoomByFloorId:floorId userId:userId filterType:filterType generalFilter:generalFilter];
    [sadapter close];
    return temp;
}
-(NSMutableArray *) getZoneRoomDetailsListByFloorId:(NSInteger)floorId userId:(NSInteger)userId filterType:(NSInteger)filterType generalFilter:(NSString*)generalFilter
{
    SupRoomAssignmentAdapterV2 *sadapter = [SupRoomAssignmentAdapterV2 createSupRoomAssignmentAdapter];
    [sadapter openDatabase];
    [sadapter resetSqlCommand];
    NSMutableArray *temp = [sadapter getZoneRoomDetailsListByFloorId:floorId userId:userId filterType:filterType generalFilter:generalFilter];
    [sadapter close];
    return temp;
}
//[Huy]
-(NSMutableArray *) getFloorRoomDetailsListByFloorId:(NSString* )floorId userId:(NSInteger)userId filterType:(NSInteger)filterType generalFilter:(NSString*)generalFilter
{
    SupRoomAssignmentAdapterV2 *sadapter = [SupRoomAssignmentAdapterV2 createSupRoomAssignmentAdapter];
    [sadapter openDatabase];
    [sadapter resetSqlCommand];
    NSMutableArray *temp = [sadapter getFloorRoomDetailsListByFloorId:floorId userId:userId filterType:filterType generalFilter:generalFilter];
    [sadapter close];
    return temp;
}

-(NSMutableArray *) getRoomDetailsListByFloorId:(NSInteger)floorId userId:(NSInteger)userId filterType:(NSInteger)filterType generalFilter:(NSString*)generalFilter
{
    SupRoomAssignmentAdapterV2 *sadapter = [SupRoomAssignmentAdapterV2 createSupRoomAssignmentAdapter];
    [sadapter openDatabase];
    [sadapter resetSqlCommand];
    NSMutableArray *temp = [sadapter getRoomDetailsListByFloorId:floorId userId:userId filterType:filterType generalFilter:generalFilter];
    [sadapter close];
    return temp;
}

-(NSMutableArray *) getUnassginRoomsByFloorId:(NSInteger)floorId AndUserId:(NSInteger)userId
{
    SupRoomAssignmentAdapterV2 *sadapter = [SupRoomAssignmentAdapterV2 createSupRoomAssignmentAdapter];
    [sadapter openDatabase];
    [sadapter resetSqlCommand];
    
    NSMutableArray *temp = [sadapter getUnassignRoomsByFloorId:floorId AndUserId:userId];
    [sadapter close];
    return temp;
}

-(NSMutableArray *) getUnassginRoomsByUserId:(NSInteger)userId
{
    SupRoomAssignmentAdapterV2 *sadapter = [SupRoomAssignmentAdapterV2 createSupRoomAssignmentAdapter];
    [sadapter openDatabase];
    [sadapter resetSqlCommand];
    
    NSMutableArray *temp = [sadapter getUnassignRoomsByUserId:userId];
    [sadapter close];
    return temp;
}

-(NSMutableArray *) getAllRoomByFloorId:(NSInteger)floorId AndUserId:(NSInteger)userId
{
    SupRoomAssignmentAdapterV2 *sadapter = [SupRoomAssignmentAdapterV2 createSupRoomAssignmentAdapter];
    [sadapter openDatabase];
    [sadapter resetSqlCommand];
    
    NSMutableArray *temp = [sadapter getAllRoomByFloorId:floorId AndUserId:userId];
    [sadapter close];
    return temp;
}

-(NSMutableArray *) getAllOfRoomByFloorId:(NSInteger)floorId AndUserId:(NSInteger)userId{
    SupRoomAssignmentAdapterV2 *sadapter = [SupRoomAssignmentAdapterV2 createSupRoomAssignmentAdapter];
    [sadapter openDatabase];
    [sadapter resetSqlCommand];
    
    NSMutableArray *temp = [sadapter getAllOfRoomByFloorId:floorId AndUserId:userId];
    [sadapter close];
    return temp;
}
-(NSMutableArray *) getAllOfRoomByFloorId:(NSInteger)floorId AndUserId:(NSInteger)userId andFilter:(int)kindOfRoom{
    SupRoomAssignmentAdapterV2 *sadapter = [SupRoomAssignmentAdapterV2 createSupRoomAssignmentAdapter];
    [sadapter openDatabase];
    [sadapter resetSqlCommand];
    
    NSMutableArray *temp = [sadapter getAllOfRoomByFloorId:floorId AndUserId:userId andFilter:kindOfRoom];
    [sadapter close];
    return temp;
}

-(NSMutableArray *) getAllRoomByFloorId:(NSInteger)floorId AndUserId:(NSInteger)userId AndCleaningStatus:(NSInteger)cleaningStatusId
{
    SupRoomAssignmentAdapterV2 *sadapter = [SupRoomAssignmentAdapterV2 createSupRoomAssignmentAdapter];
    [sadapter openDatabase];
    [sadapter resetSqlCommand];
    
    NSMutableArray *temp = [sadapter getAllRoomByFloorId:floorId AndUserId:userId AndCleaningStatus:cleaningStatusId];
    [sadapter close];
    return temp;
}

-(NSMutableArray *) getAllRoomByFloorId:(NSInteger)floorId AndUserId:(NSInteger)userId AndKindOfRoom:(NSInteger)kindOfRoom
{
    SupRoomAssignmentAdapterV2 *sadapter = [SupRoomAssignmentAdapterV2 createSupRoomAssignmentAdapter];
    [sadapter openDatabase];
    [sadapter resetSqlCommand];
    
    NSMutableArray *temp = [sadapter getAllRoomByFloorId:floorId AndUserId:userId AndKindOfRoom:kindOfRoom];
    [sadapter close];
    return temp;
}

-(NSMutableArray *) getAllRoomByRoomByUserId:(NSInteger)userId AndKindOfRoom:(NSInteger)kindOfRoom
{
    SupRoomAssignmentAdapterV2 *sadapter = [SupRoomAssignmentAdapterV2 createSupRoomAssignmentAdapter];
    [sadapter openDatabase];
    [sadapter resetSqlCommand];
    
    NSMutableArray *temp = [sadapter getAllRoomByUserId:userId AndKindOfRoom:kindOfRoom];
    [sadapter close];
    return temp;
}

-(NSMutableArray *) getAllRoomAssignmentDatas:(NSInteger) userId
{
    SupRoomAssignmentAdapterV2 *superRoomAdapter1 = [SupRoomAssignmentAdapterV2 createSupRoomAssignmentAdapter];
    [superRoomAdapter1 openDatabase];
    [superRoomAdapter1 resetSqlCommand];
    NSMutableArray *result = [superRoomAdapter1 getAllRoomAssignmentDatas:userId];
    [superRoomAdapter1 close]; 
    return result;
}

-(void)getRoomAssignments
{
    SupRoomAssignmentAdapterV2 *superRoomAdapter1 = [SupRoomAssignmentAdapterV2 createSupRoomAssignmentAdapter];
    [superRoomAdapter1 openDatabase];
    [superRoomAdapter1 resetSqlCommand];
    self.roomAssignmentList = [superRoomAdapter1 getRoomAssigmentSup];
    [superRoomAdapter1 close];    
}

-(NSDictionary *) getRoomAssignmentDataByRoomAssignmentId:(NSInteger)roomAssignmentId andRaUserId:(NSInteger)raUserId
{
    SupRoomAssignmentAdapterV2 *superRoomAdapter1 = [SupRoomAssignmentAdapterV2 createSupRoomAssignmentAdapter];
    [superRoomAdapter1 openDatabase];
    [superRoomAdapter1 resetSqlCommand];
    NSDictionary *roomAssignmentData = [superRoomAdapter1 getRoomAssignmentDataByRoomAssignmentId:roomAssignmentId andRaUserId:raUserId];
    [superRoomAdapter1 close];
    return roomAssignmentData;
}

-(NSDictionary *) getRoomAssignmentDataByRoomId:(NSInteger)roomId andRaUserId:(NSInteger)raUserId
{
    DataTable *tableQuery = [[DataTable alloc] initWithName:[NSString stringWithFormat:@"%@",ROOM_ASSIGNMENT]];
    DataRow *rowRef = [[DataRow alloc] init];
    NSString *roomIdName = [NSString stringWithFormat:@"%@",ra_room_id];
    NSString *roomAssignmentIdName = [NSString stringWithFormat:@"%@",ra_id];
    [rowRef addColumnInt:roomIdName];
    [rowRef addColumnInt:roomAssignmentIdName];
    NSString *sqlQuery = [NSString stringWithFormat:@"select %@, %@ from %@ where %@ = %d and %@ = %d",ra_room_id, ra_id, ROOM_ASSIGNMENT, ra_room_id, (int)roomId, ra_user_id, (int)raUserId];
    [tableQuery loadTableDataWithQuery:sqlQuery referenceRow:rowRef];
    
    int roomAssigmentId = [[[tableQuery rowWithIndex:0] columnWithName:roomAssignmentIdName] fieldValueInt];
    NSDictionary *result = [self getRoomAssignmentDataByRoomAssignmentId:roomAssigmentId andRaUserId:raUserId];
    return result;
}

-(NSDictionary *) getRoomAssignmentDataByRoomAssignmentId:(NSInteger)roomAssignmentId andRaUserId:(NSInteger)raUserId isFilterCurrentDate:(BOOL)isFilterCurrentDate
{
    SupRoomAssignmentAdapterV2 *superRoomAdapter1 = [SupRoomAssignmentAdapterV2 createSupRoomAssignmentAdapter];
    [superRoomAdapter1 openDatabase];
    [superRoomAdapter1 resetSqlCommand];
    NSDictionary *roomAssignmentData = [superRoomAdapter1 getRoomAssignmentDataByRoomAssignmentId:roomAssignmentId andRaUserId:raUserId isFilterCurrentDate:isFilterCurrentDate];
    [superRoomAdapter1 close];
    return roomAssignmentData;
}

-(void)getRoomInspectionByFilterType:(int)filterType filterDetailId:(int)filterDetailId
{
    SupRoomAssignmentAdapterV2 *superRoomAdapter1 = [SupRoomAssignmentAdapterV2 createSupRoomAssignmentAdapter];
    [superRoomAdapter1 openDatabase];
    [superRoomAdapter1 resetSqlCommand];
    self.roomAssignmentList = [superRoomAdapter1 getRoomAssigmentSupByFilterType:filterType filterDetailId:filterDetailId];
    [superRoomAdapter1 close];
}

-(void)getRoomInspection
{
    SupRoomAssignmentAdapterV2 *superRoomAdapter1 = [SupRoomAssignmentAdapterV2 createSupRoomAssignmentAdapter];
    [superRoomAdapter1 openDatabase];
    [superRoomAdapter1 resetSqlCommand];
    self.roomAssignmentList = [superRoomAdapter1 getRoomAssigmentSup];
    [superRoomAdapter1 close];  
}

-(void)getRoomAssignmentComplete
{
    SupRoomAssignmentAdapterV2 *superRoomAdapter1 = [SupRoomAssignmentAdapterV2 createSupRoomAssignmentAdapter];
    [superRoomAdapter1 openDatabase];
    [superRoomAdapter1 resetSqlCommand];
    NSMutableArray *listRoomCompleted = [[NSMutableArray alloc] init];
    NSMutableArray *listRoomCompletedTemp = [[NSMutableArray alloc] init];
    listRoomCompletedTemp = [superRoomAdapter1 getRoomAssigmentSupComlete];
    NSMutableSet *listRoomNo = [[NSMutableSet alloc] init];
    for(NSDictionary *dic in listRoomCompletedTemp)
    {
        if(![listRoomNo containsObject:[dic objectForKey:@"RoomNo"]])
        {
            [listRoomCompleted addObject:dic];
        }
        [listRoomNo addObject:[dic objectForKey:@"RoomNo"]];
        
    }
    self.roomAssignmentList = listRoomCompleted;
    //self.roomAssignmentList=[superRoomAdapter1 getRoomAssigmentSupComlete];
    [superRoomAdapter1 close];
}

-(void)getTotalRoomComlete{
    SupRoomAssignmentAdapterV2 *superRoomAdapter1 = [SupRoomAssignmentAdapterV2 createSupRoomAssignmentAdapter];
    [superRoomAdapter1 openDatabase];
    [superRoomAdapter1 resetSqlCommand];
    
    NSMutableArray *listTotalRoomCompleted = [[NSMutableArray alloc] init];
    NSMutableArray *listTotalRoomCompletedTemp = [superRoomAdapter1 getTotalRoomComlete];
    
    NSMutableSet *listRoomAssignmentID = [[NSMutableSet alloc] init];
    for(NSDictionary *dic in listTotalRoomCompletedTemp)
    {
        if(![listRoomAssignmentID containsObject:[dic objectForKey:kRoomAssignmentID]])
        {
            [listTotalRoomCompleted addObject:dic];
        }
        [listRoomAssignmentID addObject:[dic objectForKey:kRoomAssignmentID]];
        
    }
    self.roomAssignmentList = listTotalRoomCompleted;
    //self.roomAssignmentList=[superRoomAdapter1 getTotalRoomComlete];
    [superRoomAdapter1 close];
}

-(void)getInspectedRoom
{
    SupRoomAssignmentAdapterV2 *superRoomAdapter1 = [SupRoomAssignmentAdapterV2 createSupRoomAssignmentAdapter];
    [superRoomAdapter1 openDatabase];
    [superRoomAdapter1 resetSqlCommand];
    self.roomAssignmentList = [superRoomAdapter1 getInspectedRoom];
    [superRoomAdapter1 close];
}
- (void)getRoomInspectedPassed{
    SupRoomAssignmentAdapterV2 *superRoomAdapter1 = [SupRoomAssignmentAdapterV2 createSupRoomAssignmentAdapter];
    [superRoomAdapter1 openDatabase];
    [superRoomAdapter1 resetSqlCommand];
    
    NSMutableArray *listRoomInspectedPassed = [[NSMutableArray alloc] init];
    NSMutableArray *listRoomInspectedPassedTemp = [superRoomAdapter1 getRoomInspectedPassed];
    
    NSMutableSet *listRoomAssignmentID = [[NSMutableSet alloc] init];
    for(NSDictionary *dic in listRoomInspectedPassedTemp)
    {
        if(![listRoomAssignmentID containsObject:[dic objectForKey:kRoomAssignmentID]])
        {
            [listRoomInspectedPassed addObject:dic];
        }
        [listRoomAssignmentID addObject:[dic objectForKey:kRoomAssignmentID]];
        
    }
    self.roomAssignmentList = listRoomInspectedPassed;
    //self.roomAssignmentList=[superRoomAdapter1 getRoomInspectedPassed];
    [superRoomAdapter1 close];
}
- (void)getRoomInspectedFailed {
    SupRoomAssignmentAdapterV2 *superRoomAdapter1 = [SupRoomAssignmentAdapterV2 createSupRoomAssignmentAdapter];
    [superRoomAdapter1 openDatabase];
    [superRoomAdapter1 resetSqlCommand];
    
    NSMutableArray *listRoomInspectedFailed = [[NSMutableArray alloc] init];
    NSMutableArray *listRoomInspectedFailedTemp = [superRoomAdapter1 getRoomInspectedFailed];
    
    NSMutableSet *listRoomAssignmentID = [[NSMutableSet alloc] init];
    for(NSDictionary *dic in listRoomInspectedFailedTemp)
    {
        if(![listRoomAssignmentID containsObject:[dic objectForKey:kRoomAssignmentID]])
        {
            [listRoomInspectedFailed addObject:dic];
        }
        [listRoomAssignmentID addObject:[dic objectForKey:kRoomAssignmentID]];
        
    }
    self.roomAssignmentList = listRoomInspectedFailed;
    //self.roomAssignmentList=[superRoomAdapter1 getRoomInspectedFailed];
    [superRoomAdapter1 close];
}

/***Chinh X.Bui**/

-(void)getRoomInspectionComplete
{
    
    SupRoomAssignmentAdapterV2 *superRoomAdapter1 = [SupRoomAssignmentAdapterV2 createSupRoomAssignmentAdapter];
    [superRoomAdapter1 openDatabase];
    [superRoomAdapter1 resetSqlCommand];
    self.roomAssignmentList=[superRoomAdapter1 getRoomInspectionComlete];
    [superRoomAdapter1 close];

    superRoomAdapter1 = nil;
    
}


-(NSMutableArray*)getRoomTypesCurrentUser
{
    /*
     if (superRoomAdapter == nil) {
     superRoomAdapter = [[SupRoomAssignmentAdapter alloc] init];
     [superRoomAdapter openDatabase];
     [superRoomAdapter resetSqlCommand];
     NSMutableArray* result = [superRoomAdapter getRoomTypesCurrentUser];
     [superRoomAdapter close];
     [superRoomAdapter release];
     superRoomAdapter = nil;
     return result;
     } else {
     [superRoomAdapter openDatabase];
     [superRoomAdapter resetSqlCommand];
     NSMutableArray* result = [superRoomAdapter getRoomTypesCurrentUser];
     [superRoomAdapter close];
     return result;
     }*/
    SupRoomAssignmentAdapterV2 *superRoomAdapter1 = [SupRoomAssignmentAdapterV2 createSupRoomAssignmentAdapter];
    [superRoomAdapter1 openDatabase];
    [superRoomAdapter1 resetSqlCommand];
    NSMutableArray* result = [superRoomAdapter1 getRoomTypesCurrentUser];
    [superRoomAdapter1 close];

    superRoomAdapter1 = nil;
    return result;
    
    
}

-(NSString*)getPropertyName:(SuperRoomModelV2*)superRoomModel;
{
    superRoomModel.roomAssignmentModel=[[RoomAssignmentModelV2 alloc] init];
    superRoomModel.roomModelForGetBuildingName=[[RoomModelV2 alloc] init];
    superRoomModel.roomAssignmentModel.roomAssignment_UserId = [[UserManagerV2 sharedUserManager] currentUser].userId;
    NSMutableArray *raArray = [[RoomManagerV2 sharedRoomManager] loadAllRoomAssignmentsByUserID:superRoomModel.roomAssignmentModel];
    if ([raArray count] <= 0) {
        return @"";
    }
    superRoomModel.roomAssignmentModel = [raArray objectAtIndex:0];
    superRoomModel.roomModelForGetBuildingName.room_Id = superRoomModel.roomAssignmentModel.roomAssignment_RoomId;
    
    [[RoomManagerV2 sharedRoomManager] loadRoomModel:superRoomModel.roomModelForGetBuildingName];
    
    if ([[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]]) {
        return superRoomModel.roomModelForGetBuildingName.room_Building_Name;
    } else {
        return superRoomModel.roomModelForGetBuildingName.room_building_namelang;
    }
    
}

-(NSData*)getHotelLogo:(SuperRoomModelV2*)superRoomModel {
    //if (superRoomModel.hotelModel == nil) {
    superRoomModel.hotelModel = [[HotelModelV2 alloc] init];
    superRoomModel.hotelModel.hotel_id = [[UserManagerV2 sharedUserManager] currentUser].userHotelsId;
    
    [[HotelManagerV2 sharedHotelManager] loadHotelModel:superRoomModel.hotelModel];
    
    return superRoomModel.hotelModel.hotel_logo;
}


-(NSString*)getbuildingName:(SuperRoomModelV2*)superRoomModel
{
    superRoomModel.hotelModel =[[HotelModelV2 alloc] init];
    superRoomModel.hotelModel.hotel_id=[[UserManagerV2 sharedUserManager] currentUser].userHotelsId;
    [[HotelManagerV2 sharedHotelManager] loadHotelModel:superRoomModel.hotelModel];
    //[superRoomModel.hotelModel load];
    if ([[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]]) {
        return superRoomModel.hotelModel.hotel_name_lang;
    } else {
        return superRoomModel.hotelModel.hotel_lang;
    }
    
}


-(NSString*)getHouseKeeperName
{
    return [[UserManagerV2 sharedUserManager] currentUser].userFullName;
}

-(NSString*)getRoomStatus:(SuperRoomModelV2*)superRoomModel
{
    return superRoomModel.roomStatusModel.rstat_Code;
}

//find floor list created date:2013-03-29
-(void)getFloorListFromWSWith:(NSInteger)userId AndHotelId:(NSInteger)hotelId AndBuildingId:(NSInteger)buildingId AndRoomStatus:(NSInteger)statusId AndFilterType:(NSInteger)filterType AndPercentView:(MBProgressHUD *)percentView{
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole])
    {
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_GetSupervisorFindFloorList *requestFloorList = [[eHousekeepingService_GetSupervisorFindFloorList alloc] init];
    
    //set parameters
    [requestFloorList setIntSupervisorID:[NSNumber numberWithInteger:userId]];
    
    //Hard code for buildingId <= 0
    if(buildingId <= 0) {
        buildingId = 1;
    }
    [requestFloorList setIntBuildingID:[NSNumber numberWithInteger:buildingId]];
    [requestFloorList setIntHotelID:[NSNumber numberWithInteger:hotelId]];
    [requestFloorList setIntFilterType:[NSNumber numberWithInteger:filterType]];
    [requestFloorList setIntStatusID: [NSNumber numberWithInteger:statusId]];
    
    float currentPercent = 0;
    [self updatePercentView:percentView value:currentPercent description:[L_loading_find_floor_list currentKeyToLanguage]];
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetSupervisorFindFloorListUsingParameters:requestFloorList];
    
    NSArray *responseBodyParts = response.bodyParts;
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetSupervisorFindFloorListResponse class]]){
            
            eHousekeepingService_GetSupervisorFindFloorListResponse *dataBody = (eHousekeepingService_GetSupervisorFindFloorListResponse *)bodyPart;
            if (dataBody) {
                if ([dataBody.GetSupervisorFindFloorListResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]]) {
                    
                    NSMutableArray *floorList = dataBody.GetSupervisorFindFloorListResult.FloorList.Floor;
                    
                    float stepPercent = [self caculateStepFromCountingObject:(int)[floorList count]];
                    
                    for (eHousekeepingService_Floor *item in floorList) {
                        FloorModelV2 *_floor = [[FloorModelV2 alloc] init];
                        _floor.floor_id = [item.flID intValue];
                        _floor.floor_building_id = [item.flBuildingID intValue];
                        _floor.floor_name = item.flName;
                        _floor.floor_name_lang = item.flLang;
                        _floor.floor_last_modified = item.flLastModifed;
                        
                        //checking existing floor
                        if([[UnassignManagerV2 sharedUnassignManager] isExistFloor:_floor])
                        {
                            [[UnassignManagerV2 sharedUnassignManager] updateFloor:_floor];
                        }
                        else
                        {
                            [[UnassignManagerV2 sharedUnassignManager] insertFloor:_floor];
                        }
                        
                        currentPercent += stepPercent;
                        [self updatePercentView:percentView value:currentPercent description:[L_loading_find_floor_list currentKeyToLanguage]];
                    }

                }
            }
        }
    }
}

//find online floor list created date:2013-03-29
-(NSMutableArray*)getOnlineFloorListWith:(NSInteger)userId AndHotelId:(NSInteger)hotelId AndBuildingId:(NSInteger)buildingId AndRoomStatus:(NSInteger)statusId AndFilterType:(NSInteger)filterType AndPercentView:(MBProgressHUD *)percentView{
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole])
    {
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_GetSupervisorFindFloorList *requestFloorList = [[eHousekeepingService_GetSupervisorFindFloorList alloc] init];
    
    NSMutableArray *results = [NSMutableArray array];
    //set parameters
    [requestFloorList setIntSupervisorID:[NSNumber numberWithInteger:userId]];
    if(buildingId <= 0){
        buildingId = 1;
    }
    [requestFloorList setIntBuildingID:[NSNumber numberWithInteger:buildingId]];
    [requestFloorList setIntHotelID:[NSNumber numberWithInteger:hotelId]];
    [requestFloorList setIntFilterType:[NSNumber numberWithInteger:filterType]];
    [requestFloorList setIntStatusID: [NSNumber numberWithInteger:statusId]];
    
    float currentPercent = 0;
    [self updatePercentView:percentView value:currentPercent description:[L_loading_find_floor_list currentKeyToLanguage]];
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetSupervisorFindFloorListUsingParameters:requestFloorList];
    
    NSArray *responseBodyParts = response.bodyParts;
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetSupervisorFindFloorListResponse class]]){
            
            eHousekeepingService_GetSupervisorFindFloorListResponse *dataBody = (eHousekeepingService_GetSupervisorFindFloorListResponse *)bodyPart;
            if (dataBody) {
                if ([dataBody.GetSupervisorFindFloorListResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]]) {
                    
                    NSMutableArray *floorList = dataBody.GetSupervisorFindFloorListResult.FloorList.Floor;
                    
                    float stepPercent = [self caculateStepFromCountingObject:(int)[floorList count]];
                    
                    for (eHousekeepingService_Floor *item in floorList) {
                        FloorModelV2 *_floor = [[FloorModelV2 alloc] init];
                        _floor.floor_id = [item.flID intValue];
                        _floor.floor_building_id = [item.flBuildingID intValue];
                        _floor.floor_name = item.flName;
                        _floor.floor_name_lang = item.flLang;
                        _floor.floor_last_modified = item.flLastModifed;
                        
                        [results addObject:_floor];
                        
                        //checking existing floor
                        if([[UnassignManagerV2 sharedUnassignManager] isExistFloor:_floor])
                        {
                            [[UnassignManagerV2 sharedUnassignManager] updateFloor:_floor];
                        }
                        else
                        {
                            [[UnassignManagerV2 sharedUnassignManager] insertFloor:_floor];
                        }
                        
                        currentPercent += stepPercent;
                        [self updatePercentView:percentView value:currentPercent description:[L_loading_find_floor_list currentKeyToLanguage]];
                    }
                    
                }
            }
        }
    }
    
    return results;
}

-(NSMutableArray*)getOnlineFindFloorDetailListWith:(NSInteger)userId AndHotelId:(NSInteger)hotelId AndBuildingId:(NSInteger)buildingId AndGeneralFilter:(NSString*)generalFilter AndFilterType:(NSInteger)filterType AndPercentView:(MBProgressHUD *)percentView{
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole])
    {
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_GetFindFloorDetailsRequest *requestFloorList =
    [[eHousekeepingService_GetFindFloorDetailsRequest alloc] init];
    NSMutableArray *results = [NSMutableArray array];
    
    [requestFloorList setUserID: [NSNumber numberWithInteger:userId]];
    [requestFloorList setBuildingID: [NSNumber numberWithInteger:buildingId]];
    [requestFloorList setHTID: [NSNumber numberWithInteger:hotelId]];
    [requestFloorList setFilterType: [NSNumber numberWithInteger:filterType]];
    [requestFloorList setGeneralFilter:generalFilter];
    
    eHousekeepingService_GetFindFloorDetailsList *floorList = [[eHousekeepingService_GetFindFloorDetailsList alloc] init];
    floorList.GetFindFloorDetailsRequest = requestFloorList;
    
    float currentPercent = 0;
    [self updatePercentView:percentView value:currentPercent description:[L_loading_find_floor_list currentKeyToLanguage]];
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetFindFloorDetailsListUsingParameters:floorList];
    
    NSArray *responseBodyParts = response.bodyParts;
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetFindFloorDetailsListResponse class]]){
            
            eHousekeepingService_GetFindFloorDetailsListResponse *dataBody = (eHousekeepingService_GetFindFloorDetailsListResponse *)bodyPart;
            if (dataBody) {
                if ([dataBody.GetFindFloorDetailsListResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]]) {
                    
                    NSMutableArray *floorList = dataBody.GetFindFloorDetailsListResult.FloorListing.FloorDetails;
                    
                    float stepPercent = [self caculateStepFromCountingObject:(int)[floorList count]];
                    
                    for (eHousekeepingService_FloorDetails *item in floorList) {
                        FloorModelV2 *_floor = [[FloorModelV2 alloc] init];
                        _floor.floor_id = [item.ID_ intValue];
                        _floor.floor_building_id = [item.BuildingID intValue];
                        _floor.floor_name = item.Name;
                        _floor.floor_name_lang = item.Lang;
                        _floor.floor_last_modified = @"";
                        [results addObject:_floor];
                        
                        //checking existing floor
                        if([[UnassignManagerV2 sharedUnassignManager] isExistFloor:_floor])
                        {
                            [[UnassignManagerV2 sharedUnassignManager] updateFloor:_floor];
                        }
                        else
                        {
                            [[UnassignManagerV2 sharedUnassignManager] insertFloor:_floor];
                        }
                        
                        currentPercent += stepPercent;
                        [self updatePercentView:percentView value:currentPercent description:[L_loading_find_floor_list currentKeyToLanguage]];
                    }
                    
                }
            }
        }
    }
    
    return results;
}

#pragma mark -
#pragma mark CleaningStatusModel Methods

-(int)insertCleaningStatus:(CleaningStatusModelV2*)cleaningStatus
{
    /*
     if (cleaningStatusAdapter == nil) {
     cleaningStatusAdapter = [[CleaningStatusAdapter alloc] init];
     [cleaningStatusAdapter openDatabase];
     [cleaningStatusAdapter resetSqlCommand];
     int result = [cleaningStatusAdapter insert:cleaningStatus];
     [cleaningStatusAdapter close];
     [cleaningStatusAdapter release];
     cleaningStatusAdapter = nil;
     return result;
     } else {
     [cleaningStatusAdapter openDatabase];
     [cleaningStatusAdapter resetSqlCommand];
     int result = [cleaningStatusAdapter insert:cleaningStatus];
     [cleaningStatusAdapter close];
     return result;
     
     }*/
    CleaningStatusAdapterV2 *cleaningStatusAdapter1 = [[CleaningStatusAdapterV2 alloc] init];
    [cleaningStatusAdapter1 openDatabase];
    [cleaningStatusAdapter1 resetSqlCommand];
    int result = [cleaningStatusAdapter1 insert:cleaningStatus];
    [cleaningStatusAdapter1 close];
   
    cleaningStatusAdapter1 = nil;
    return result;
}
-(void)loadCleaningStatus:(CleaningStatusModelV2*)cleaningStatus
{
    CleaningStatusAdapterV2 *cleaningStatusAdapter1 = [[CleaningStatusAdapterV2 alloc] init];
    [cleaningStatusAdapter1 openDatabase];
    [cleaningStatusAdapter1 resetSqlCommand];
    [cleaningStatusAdapter1 loadCleaningStatus:cleaningStatus];
    [cleaningStatusAdapter1 close];

    cleaningStatusAdapter1 = nil;
}
-(int)updateIsCheckedRa: (NSInteger)userID houseKeeperId:(NSString*)houseKeeperId
{
    SupRoomAssignmentAdapterV2 *supRoomAssignment = [SupRoomAssignmentAdapterV2 createSupRoomAssignmentAdapter];
    [supRoomAssignment openDatabase];
    [supRoomAssignment resetSqlCommand];
    int result = [supRoomAssignment updateIsCheckedRa:userID houseKeeperId:houseKeeperId];
    [supRoomAssignment close];
    supRoomAssignment = nil;
    return result;
}
-(int)updateIsCheckedFind: (NSInteger)raID
{
    SupRoomAssignmentAdapterV2 *supRoomAssignment = [SupRoomAssignmentAdapterV2 createSupRoomAssignmentAdapter];
    [supRoomAssignment openDatabase];
    [supRoomAssignment resetSqlCommand];
    int result = [supRoomAssignment updateIsCheckedFind:raID];
    [supRoomAssignment close];
    supRoomAssignment = nil;
    return result;
}
-(int)updateCleaningStatus:(CleaningStatusModelV2*)cleaningStatus
{
    CleaningStatusAdapterV2 *cleaningStatusAdapter1 = [[CleaningStatusAdapterV2 alloc] init];
    [cleaningStatusAdapter1 openDatabase];
    [cleaningStatusAdapter1 resetSqlCommand];
    int result = [cleaningStatusAdapter1 updateCleaningStatus:cleaningStatus];
    [cleaningStatusAdapter1 close];

    cleaningStatusAdapter1 = nil;
    return result;
}

-(BOOL) isExist:(CleaningStatusModelV2 *)cleaningStatusModel
{
    CleaningStatusAdapterV2 *cleaningStatusAdapter1 = [[CleaningStatusAdapterV2 alloc] init];
    [cleaningStatusAdapter1 openDatabase];
    [cleaningStatusAdapter1 resetSqlCommand];
    BOOL result = [cleaningStatusAdapter1 isExist:cleaningStatusModel];
    [cleaningStatusAdapter1 close];
    cleaningStatusAdapter1 = nil;
    return result;
}

-(NSMutableArray *)loadRoomAssignmentsBySuperVisorId:(int)supervisorId houseKeeperId:(NSInteger)houseKeeperId {
    RoomAssignmentAdapterV2 *radapter = [RoomAssignmentAdapterV2 createRoomAssignmentAdapter];
    [radapter openDatabase];
    NSMutableArray *results = [radapter loadRoomAssignmentsBySuperVisorId:supervisorId houseKeeperId:(int)houseKeeperId];
    [radapter close];
    return results;
}

//HaoTran[20130619/ Get last date modified]
-(NSString*)getCleaningStatusLastModifiedDate
{
    CleaningStatusAdapterV2 *adapter1 = [[CleaningStatusAdapterV2 alloc] init];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    NSString *result = nil;
    result = [adapter1 getCleaningStatusLastModifiedDate];
    [adapter1 close];
    adapter1 = nil;
    return result;
}
//HaoTran[20130619/ Get last date modified] - END

-(NSMutableArray*)loadAllCleaningStatus
{
    CleaningStatusAdapterV2 *cleaningStatusAdapter1 = [[CleaningStatusAdapterV2 alloc] init];
    [cleaningStatusAdapter1 openDatabase];
    [cleaningStatusAdapter1 resetSqlCommand];
    NSMutableArray* result = [cleaningStatusAdapter1 loadAllCleaningStatus];
    [cleaningStatusAdapter1 close];

    cleaningStatusAdapter1 = nil;
    return result;
}

-(NSMutableArray*)loadAllCleaningStatusWithCleaningStatusID:(NSInteger)clsID{
    CleaningStatusAdapterV2 *cleaningStatusAdapter1 = [[CleaningStatusAdapterV2 alloc] init];
    [cleaningStatusAdapter1 openDatabase];
    [cleaningStatusAdapter1 resetSqlCommand];
    NSMutableArray* result = [cleaningStatusAdapter1 loadAllCleaningStatusWithCleaningStatusID:clsID];
    [cleaningStatusAdapter1 close];
    
    cleaningStatusAdapter1 = nil;
    return result;
}

-(NSMutableArray *)loadAllCleaningStatusForSupervisor {
    CleaningStatusAdapterV2 *cleaningStatusAdapter1 = [[CleaningStatusAdapterV2 alloc] init];
    [cleaningStatusAdapter1 openDatabase];
    [cleaningStatusAdapter1 resetSqlCommand];
    NSMutableArray* result = [cleaningStatusAdapter1 loadAllCleaningStatusForSupervisor];
    [cleaningStatusAdapter1 close];
    
    cleaningStatusAdapter1 = nil;
    return result;
}

-(BOOL)getAllCleaningStatusWS:(NSString *)strLastModified AndPercentView:(MBProgressHUD*)percentView {
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole]){
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_GetCleaningStatusList *request = [[eHousekeepingService_GetCleaningStatusList alloc] init];
    
    if (strLastModified) {
        [request setStrLastModified:strLastModified];
    }
    
    BOOL result = YES;
    
//    LogObject *logObj = [[LogObject alloc] init];
//    logObj.dateTime = [NSDate date];
//    logObj.log = [NSString stringWithFormat:@"<><getAllCleaningStatusWS><lastModified:%@>",strLastModified];
//    LogFileManager *logManager = [[LogFileManager alloc] init];
//    [logManager save:logObj];
    
    float currentPercent = 0;
    [self updatePercentView:percentView value:currentPercent description:LOADING_GET_ALL_CLEANING_STATUS];
    if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL){
        return NO;
    }
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetCleaningStatusListUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetCleaningStatusListResponse class]]) {
            eHousekeepingService_GetCleaningStatusListResponse *dataBody = (eHousekeepingService_GetCleaningStatusListResponse *)bodyPart;
            if (dataBody != nil) {
                if ([dataBody.GetCleaningStatusListResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_OK]]) {
                    NSMutableArray *cleaningStatuses = dataBody.GetCleaningStatusListResult.CleaningStatusList.CleaningStatus;
                   
                    float stepPercent = [self caculateStepFromCountingObject:(int)[cleaningStatuses count]];
                    for (eHousekeepingService_CleaningStatus *cleaningStatus in cleaningStatuses) {
                        if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL){
                            break;
                        }
                        CleaningStatusModelV2 *model = [[CleaningStatusModelV2 alloc] init];
                        model.cstat_Id = [cleaningStatus.csID intValue];
                        model.cstat_image = cleaningStatus.csIcon;
                        model.cstat_Language = cleaningStatus.csLang;
                        model.cstat_Name = cleaningStatus.csName;
                        model.cstat_last_modified = cleaningStatus.csLastModifed;
                        NSString *imageFileName = nil;
                        // HaoTran[20130508/Fix Layout] - Check to add image data if any missing cleaning status image from server
                        if(model.cstat_image == nil) {
                            if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                                if(model.cstat_Id == ENUM_CLEANING_STATUS_DND){
                                    imageFileName = imgDNDFlat;
                                } else if(model.cstat_Id == ENUM_CLEANING_STATUS_DOUBLE_LOCK){
                                    imageFileName = imgDoubleLockFlat;
                                } else if(model.cstat_Id == ENUM_CLEANING_STATUS_DECLINE_SERVICE){
                                    imageFileName = imgDeclinedServiceFlat;
                                } else if(model.cstat_Id == ENUM_CLEANING_STATUS_SERVICE_LATER) {
                                    imageFileName = imgServiceLaterFlat;
                                } else if(model.cstat_Id == ENUM_CLEANING_STATUS_COMPLETED) {
                                    imageFileName = imgCompleteFlat;
                                } else if(model.cstat_Id == ENUM_CLEANING_STATUS_PENDING_FIRST_TIME
                                          || model.cstat_Id == ENUM_CLEANING_STATUS_PENDING_SECOND_TIME) {
                                    imageFileName = imgPendingFlat;
                                } else if(model.cstat_Id == ENUM_CLEANING_STATUS_PAUSE) {
                                    imageFileName = imgStopFlat;
                                } else if(model.cstat_Id == ENUM_CLEANING_STATUS_START) {
                                    imageFileName = imgStartFlat;
                                }
                            } else {
                                if(model.cstat_Id == ENUM_CLEANING_STATUS_DND){
                                    imageFileName = imgDND;
                                } else if(model.cstat_Id == ENUM_CLEANING_STATUS_DECLINE_SERVICE){
                                    imageFileName = imgDeclinedService;
                                } else if(model.cstat_Id == ENUM_CLEANING_STATUS_SERVICE_LATER) {
                                    imageFileName = imgServiceLater;
                                } else if(model.cstat_Id == ENUM_CLEANING_STATUS_COMPLETED) {
                                    imageFileName = imgComplete;
                                } else if(model.cstat_Id == ENUM_CLEANING_STATUS_PENDING_FIRST_TIME
                                          || model.cstat_Id == ENUM_CLEANING_STATUS_PENDING_SECOND_TIME) {
                                    imageFileName = imgPending;
                                } else if(model.cstat_Id == ENUM_CLEANING_STATUS_PAUSE) {
                                    imageFileName = imgStop;
                                } else if(model.cstat_Id == ENUM_CLEANING_STATUS_START) {
                                    imageFileName = imgStart;
                                }
                            }
                            model.cstat_image = UIImagePNGRepresentation([UIImage imageNamed:imageFileName]);
                        } else { //Cleaning status for iOS 7 Style
                            if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                                if(model.cstat_Id == ENUM_CLEANING_STATUS_DND){
                                    imageFileName = imgDNDFlat;
                                } else if(model.cstat_Id == ENUM_CLEANING_STATUS_DOUBLE_LOCK){
                                    imageFileName = imgDoubleLockFlat;
                                } else if(model.cstat_Id == ENUM_CLEANING_STATUS_DECLINE_SERVICE){
                                    imageFileName = imgDeclinedServiceFlat;
                                } else if(model.cstat_Id == ENUM_CLEANING_STATUS_SERVICE_LATER) {
                                    imageFileName = imgServiceLaterFlat;
                                } else if(model.cstat_Id == ENUM_CLEANING_STATUS_COMPLETED) {
                                    imageFileName = imgCompleteFlat;
                                } else if(model.cstat_Id == ENUM_CLEANING_STATUS_PENDING_FIRST_TIME
                                          || model.cstat_Id == ENUM_CLEANING_STATUS_PENDING_SECOND_TIME) {
                                    imageFileName = imgPendingFlat;
                                } else if(model.cstat_Id == ENUM_CLEANING_STATUS_PAUSE) {
                                    imageFileName = imgStopFlat;
                                } else if(model.cstat_Id == ENUM_CLEANING_STATUS_START) {
                                    imageFileName = imgStartFlat;
                                }
                                model.cstat_image = UIImagePNGRepresentation([UIImage imageNamed:imageFileName]);
                            }
                        }
                        // HaoTran[20130508/Fix Layout] - End
                        
                        if ([self isExist:model]) {
                            [self updateCleaningStatus:model];
                        }
                        else {
                            [self insertCleaningStatus:model];
                        }
                        [self updatePercentView:percentView value:currentPercent description:LOADING_GET_ALL_CLEANING_STATUS];
                        currentPercent += stepPercent;
                    }
                }
                else
                    result = NO;
            }
            else
                result = NO;
        }
        else
            result = NO;
    }
    return result;
}


#pragma mark - inspection Status Model
-(BOOL) insertInspectionStatusModel:(InspectionStatusModel *)model{
    InspectionStatusAdapter *inspectionAdapter1 = [[InspectionStatusAdapter alloc] init];
    [inspectionAdapter1 openDatabase];
    [inspectionAdapter1 resetSqlCommand];
    BOOL result = NO;
    result =  [inspectionAdapter1 insertInspectionStatusModel:model];
    
    [inspectionAdapter1 close];
    
    inspectionAdapter1 = nil;
    return result;

}

-(InspectionStatusModel *) loadInspectionStatusModelByPrimaryKey:(int)inspectionStatusId{
    InspectionStatusAdapter *inspectionAdapter1 = [[InspectionStatusAdapter alloc] init];
    [inspectionAdapter1 openDatabase];
    [inspectionAdapter1 resetSqlCommand];
    InspectionStatusModel *model =  [inspectionAdapter1 loadInspectionStatusModelByPrimaryKey:inspectionStatusId];
    
    [inspectionAdapter1 close];
    
    inspectionAdapter1 = nil;
    return model;

}

-(BOOL) updateInspectionStatusModel:(InspectionStatusModel *) model{
    InspectionStatusAdapter *inspectionAdapter1 = [[InspectionStatusAdapter alloc] init];
    [inspectionAdapter1 openDatabase];
    [inspectionAdapter1 resetSqlCommand];
    BOOL result = NO;
    result =  [inspectionAdapter1 updateInspectionStatusModel:model];
    
    [inspectionAdapter1 close];
    
    inspectionAdapter1 = nil;
    return result;

}

-(BOOL) deleteInspectionStatusModel:(InspectionStatusModel *) model{
    InspectionStatusAdapter *inspectionAdapter1 = [[InspectionStatusAdapter alloc] init];
    [inspectionAdapter1 openDatabase];
    [inspectionAdapter1 resetSqlCommand];
    BOOL result = NO;
    result =  [inspectionAdapter1 deleteInspectionStatusModel:model];    
    [inspectionAdapter1 close];    
    inspectionAdapter1 = nil;
    return result;
}

-(void)getInspectionStatusWSWithUserId:(NSInteger)userId andLastModified:(NSString *)lastModified AndPercentView:(MBProgressHUD *)percentView {
    
    if (![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        return;
    }
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    eHousekeepingService_GetInspectionStatus *request = [[eHousekeepingService_GetInspectionStatus alloc] init];
    
    [request setIntUsrID:[NSNumber numberWithInteger:userId]];
    
    if (lastModified.length > 0) {
        [request setStrLastModified:lastModified];
    }
    
    //    LogObject *logObj = [[LogObject alloc] init];
    //    logObj.dateTime = [NSDate date];
    //    logObj.log = [NSString stringWithFormat:@"<%i><getInspectionStatusWSWithUserId><intUsrID:%i lastModified:%@>",userId,userId,lastModified];
    //    LogFileManager *logManager = [[LogFileManager alloc] init];
    //    [logManager save:logObj];
    
    float currentPercent = 0;
    [self updatePercentView:percentView value:currentPercent description:LOADING_GET_INSPECTION_STATUS];
    
    if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL){return;}
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetInspectionStatusUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetInspectionStatusResponse class]]) {
            eHousekeepingService_GetInspectionStatusResponse *dataBody = (eHousekeepingService_GetInspectionStatusResponse *)bodyPart;
            if (dataBody != nil) {
                if ([dataBody.GetInspectionStatusResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]]) {
                    
                    NSMutableArray *inspectionStatuses = dataBody.GetInspectionStatusResult.InspStatusLst.InspectionStatusList;
                    
                    float stepPercent = [self caculateStepFromCountingObject:(int)[inspectionStatuses count]];
                    [self updatePercentView:percentView value:currentPercent description:LOADING_GET_INSPECTION_STATUS];
                    
                    UserManagerV2 *userManager = [UserManagerV2 sharedUserManager];
                    
                    for (eHousekeepingService_InspectionStatusList *status in inspectionStatuses) {
                        InspectionStatusModel *ismodel = [self loadInspectionStatusModelByPrimaryKey:[status.inspID intValue]];
                        if(ismodel != nil) {
                            ismodel.istatImage = status.inspPicture;
                            ismodel.istatName = status.inspName;
                            ismodel.istatLang = status.inspLang;
                            [self updateInspectionStatusModel:ismodel];
                        } else {
                            ismodel = [[InspectionStatusModel alloc] init];
                            ismodel.istatId = [status.inspID intValue];
                            ismodel.istatImage = status.inspPicture;
                            ismodel.istatName = status.inspName;
                            ismodel.istatLang = status.inspLang;
                            [self insertInspectionStatusModel:ismodel];
                        }
                        
                        [userManager setCurrentUserConfig:USER_CONFIG_ROOM_INSPECTION_LASTMODIFIED value:status.inspLastModified];
                        
                        [self updatePercentView:percentView value:currentPercent description:LOADING_GET_INSPECTION_STATUS];
                        currentPercent += stepPercent;
                    }
                    
                }
            }
        }
    }
}

#pragma mark -
#pragma mark LocationModel Methods
-(int)insertLocationModel:(LocationModelV2*)locationModel {
    LocationAdapterV2 *locationAdapter1 = [[LocationAdapterV2 alloc] init];
    [locationAdapter1 openDatabase];
    [locationAdapter1 resetSqlCommand];
    //[locationAdapter1 insertLocationModel:locationModel]
    int result = 1;
    [locationAdapter1 close];
  
    locationAdapter1 = nil;
    return result;
    
}

-(int)updateLocationModel:(LocationModelV2*)locationModel {
    LocationAdapterV2 *locationAdapter1 = [[LocationAdapterV2 alloc] init];
    [locationAdapter1 openDatabase];
    [locationAdapter1 resetSqlCommand];
    // [locationAdapter1 updatedLocationModel:locationModel]
    int result = 1;
    [locationAdapter1 close];

    locationAdapter1 = nil;
    return result;
}

-(void)loadLocationModel:(LocationModelV2*)locationModel {
    LocationAdapterV2 *locationAdapter1 = [[LocationAdapterV2 alloc] init];
    [locationAdapter1 openDatabase];
    [locationAdapter1 resetSqlCommand];
//    [locationAdapter1 loadLocationModel:locationModel];
    [locationAdapter1 close];
  
    locationAdapter1 = nil;
    
}

#pragma mark - Room Status Model for DB
+(NSString*)getRoomStatusCodeByEnum:(enum ENUM_ROOM_STATUS)roomStatus{
    
    switch (roomStatus) {
       
        case ENUM_ROOM_STATUS_OC:{
            return @"OC";
        } break;
            
        case ENUM_ROOM_STATUS_OD:{
            return @"OD";
        } break;
        case ENUM_ROOM_STATUS_OI:{
            return @"OI";
        } break;
        case ENUM_ROOM_STATUS_OOO:{
            return @"OOO";
        } break;
        case ENUM_ROOM_STATUS_OOS:{
            return @"OOS";
        } break;
        case ENUM_ROOM_STATUS_OPU:{
            return @"OPU";
        } break;
        case ENUM_ROOM_STATUS_VC:{
            return @"VC";
        } break;
        case ENUM_ROOM_STATUS_VCB:{
            return @"VCB";
        } break;
        case ENUM_ROOM_STATUS_VD:{
            return @"VD";
        } break;
        case ENUM_ROOM_STATUS_VI:{
            return @"VI";
        } break;
        case ENUM_ROOM_STATUS_VPU:{
            return @"VPU";
        } break;
            
        case ENUM_ROOM_STATUS_OC_SO:{
            return @"OC-SO";
        } break;
        
        default: {
            return @"";
        }
            break;
    }
    
    return @"";
}

-(int)insertRoomStatusData:(RoomStatusModelV2 *)status {
    RoomStatusAdapterV2 *roomStatusAdapter1 = [[RoomStatusAdapterV2 alloc] init];
    [roomStatusAdapter1 openDatabase];
    [roomStatusAdapter1 resetSqlCommand];
    int result = [roomStatusAdapter1 insertRoomStatusData:status];
    [roomStatusAdapter1 close];

    roomStatusAdapter1 = nil;
    
    return result;
}

-(int)updateRoomStatusData:(RoomStatusModelV2 *)status {
    RoomStatusAdapterV2 *roomStatusAdapter1 = [[RoomStatusAdapterV2 alloc] init];
    [roomStatusAdapter1 openDatabase];
    [roomStatusAdapter1 resetSqlCommand];
    int result = [roomStatusAdapter1 updateRoomStatusData:status];
    [roomStatusAdapter1 close];

    roomStatusAdapter1 = nil;
    
    return result;
}

-(int)deleteRoomStatusData:(RoomStatusModelV2 *)status {    
    RoomStatusAdapterV2 *roomStatusAdapter1 = [[RoomStatusAdapterV2 alloc] init];
    [roomStatusAdapter1 openDatabase];
    [roomStatusAdapter1 resetSqlCommand];    
    int result = [roomStatusAdapter1 deleteRoomStatusData:status];
    [roomStatusAdapter1 close];

    
    return result;
}

-(int)deleteAllRoomStatusData{
    RoomStatusAdapterV2 *roomStatusAdapter1 = [[RoomStatusAdapterV2 alloc] init];
    [roomStatusAdapter1 openDatabase];
    [roomStatusAdapter1 resetSqlCommand];
    int result = [roomStatusAdapter1 deleteAllRoomStatus];
    [roomStatusAdapter1 close];
    
    return result;
}

-(RoomStatusModelV2 *)loadRoomStatusData:(RoomStatusModelV2 *)status {
    RoomStatusAdapterV2 *roomStatusAdapter1 = [[RoomStatusAdapterV2 alloc] init];
    [roomStatusAdapter1 openDatabase];
    [roomStatusAdapter1 resetSqlCommand];
    [roomStatusAdapter1 loadRoomStatusData:status];
    [roomStatusAdapter1 close];

    roomStatusAdapter1 = nil;
    
    return status;
}

-(NSMutableArray *)loadAllRoomStatus {
    RoomStatusAdapterV2 *roomStatusAdapter1 = [[RoomStatusAdapterV2 alloc] init];
    [roomStatusAdapter1 openDatabase];
    [roomStatusAdapter1 resetSqlCommand];    
    NSMutableArray * results = [roomStatusAdapter1 loadAllRoomStatus];
    [roomStatusAdapter1 close];

    roomStatusAdapter1 = nil;
    
    return results;
}

-(NSMutableArray *) loadAllMaidRoomStatusByIsOcupied:(BOOL)isOcupied{
    RoomStatusAdapterV2 *roomStatusAdapter1 = [[RoomStatusAdapterV2 alloc] init];
    [roomStatusAdapter1 openDatabase];
    [roomStatusAdapter1 resetSqlCommand];    
    NSMutableArray * results = [roomStatusAdapter1 loadAllMaidRoomStatusByIsOcupied:isOcupied];
    [roomStatusAdapter1 close];
    
    roomStatusAdapter1 = nil;
    
    return results;
    
}

-(NSMutableArray *) loadAllMaidRoomStatus
{
    RoomStatusAdapterV2 *roomStatusAdapter1 = [[RoomStatusAdapterV2 alloc] init];
    [roomStatusAdapter1 openDatabase];
    [roomStatusAdapter1 resetSqlCommand];    
    NSMutableArray * results = [roomStatusAdapter1 loadAllMaidRoomStatus];
    [roomStatusAdapter1 close];
    
    roomStatusAdapter1 = nil;
    
    return results;
}

-(NSMutableArray *) loadAllSuperVisorRoomStatusByIsOcupied:(BOOL)isOcupied{
    RoomStatusAdapterV2 *roomStatusAdapter1 = [[RoomStatusAdapterV2 alloc] init];
    [roomStatusAdapter1 openDatabase];
    [roomStatusAdapter1 resetSqlCommand];    
    NSMutableArray * results = [roomStatusAdapter1 loadAllSuperVisorRoomStatusByIsOcupied:isOcupied];
    [roomStatusAdapter1 close];
    
    roomStatusAdapter1 = nil;
    
    return results;
}

-(NSMutableArray *) loadAllSuperVisorBlockRoomStatusByIsOcupied:(BOOL)isOcupied{
    RoomStatusAdapterV2 *roomStatusAdapter1 = [[RoomStatusAdapterV2 alloc] init];
    [roomStatusAdapter1 openDatabase];
    [roomStatusAdapter1 resetSqlCommand];    
    NSMutableArray * results = [roomStatusAdapter1 loadAllSuperVisorBlockRoomStatusByIsOcupied:isOcupied];
    [roomStatusAdapter1 close];
    
    roomStatusAdapter1 = nil;
    
    return results;
}

-(NSMutableArray *) loadAllSuperVisorRoomStatusByIsOcupied:(BOOL)isOcupied andChekList:(NSInteger)tchk{
    RoomStatusAdapterV2 *roomStatusAdapter1 = [[RoomStatusAdapterV2 alloc] init];
    [roomStatusAdapter1 openDatabase];
    [roomStatusAdapter1 resetSqlCommand];    
    NSMutableArray * results = [roomStatusAdapter1 loadAllSuperVisorRoomStatusByIsOcupied:isOcupied andChekList:tchk];
    [roomStatusAdapter1 close];
    
    roomStatusAdapter1 = nil;
    
    return results;

}
-(RoomStatusModelV2 *)loadRoomStatusByStatusNameData:(RoomStatusModelV2 *)status
{
    RoomStatusAdapterV2 *roomStatusAdapter1 = [[RoomStatusAdapterV2 alloc] init];
    [roomStatusAdapter1 openDatabase];
    [roomStatusAdapter1 resetSqlCommand];
    [roomStatusAdapter1 loadRoomStatusByStatusNameData:status];
    [roomStatusAdapter1 close];
    roomStatusAdapter1 = nil;
    return status;
}

//HaoTran[20130619/ Get last date modified]
-(NSString*)getRoomStatusLastModifiedDate
{
    RoomStatusAdapterV2 *adapter1 = [[RoomStatusAdapterV2 alloc] init];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    NSString *result = nil;
    result = [adapter1 getRoomStatusLastModifiedDate];
    [adapter1 close];
    adapter1 = nil;
    return result;
}
//HaoTran[20130619/ Get last date modified] - END

// V1
static NSMutableArray *roomStatusModelArray = nil;
+(void)updateRoomStatusModelArray{
    RoomStatusAdapterV2 *radapter = [[RoomStatusAdapterV2 alloc] init];
    [radapter openDatabase];
    roomStatusModelArray = [radapter loadAllRoomStatus];
    [radapter close];

} 

+(NSMutableArray *) roomStatusModelArray{
    if (roomStatusModelArray == nil) {
        roomStatusModelArray = [[NSMutableArray alloc] init];
    }
    [self updateRoomStatusModelArray];
    
    return roomStatusModelArray;
}

-(BOOL)getAllRoomStatusWS:(NSString *) strLastModified AndPercentView:(MBProgressHUD *)percentView{
    if (![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        return NO;
    }
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole]){
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_GetRoomStatusList *request = [[eHousekeepingService_GetRoomStatusList alloc] init];
    if (strLastModified) {
        [request setStrLastModified:strLastModified];
    }
    
    BOOL result = YES;
//    LogObject *logObj = [[LogObject alloc] init];
//    logObj.dateTime = [NSDate date];
//    logObj.log = [NSString stringWithFormat:@"<><getAllRoomStatusWS><lastModified:%@>",strLastModified];
//    LogFileManager *logManager = [[LogFileManager alloc] init];
//    [logManager save:logObj];
    
    float currentPercent = 0;
    [self updatePercentView:percentView value:currentPercent description:LOADING_GET_ALL_ROOM_STATUS];
    if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL) {
        return NO;
    }
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetRoomStatusListUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetRoomStatusListResponse class]]) {
            eHousekeepingService_GetRoomStatusListResponse *dataBody = (eHousekeepingService_GetRoomStatusListResponse *)bodyPart;
            if (dataBody != nil) {
                if ([dataBody.GetRoomStatusListResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]]) {
                    
                    //20150512 - Don't force delete room status
                    //[self deleteAllRoomStatusData];
                    NSMutableArray *roomStatuses = dataBody.GetRoomStatusListResult.RoomStatusList.RoomStatus;
                    RoomStatusModelV2 *model = [[RoomStatusModelV2 alloc] init];
                    RoomStatusModelV2 *compareModel = [[RoomStatusModelV2 alloc] init];
                    float stepPercent = [self caculateStepFromCountingObject:(int)[roomStatuses count]];
                    for (eHousekeepingService_RoomStatus *roomStatus in roomStatuses) {
                        if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL) {
                            break;
                        }
                        
                        compareModel.rstat_Id = [roomStatus.rsID intValue];
                        
                        [self loadRoomStatusData:compareModel];
                        
                        model.rstat_Id = [roomStatus.rsID intValue];
                        model.rstat_Code = roomStatus.rsCode;
                        model.rstat_Image = roomStatus.rsIconImage;
                        model.rstat_Lang = roomStatus.rsLang;
                        model.rstat_Name = roomStatus.rsName;
                        model.rstat_LastModified = roomStatus.rsLastModifed;
                        model.rstat_PhysicalCheck = [roomStatus.rsPhysicalCheck intValue];
                        
                        //HaoTran[20130508/Fix Layout] - Check to add image data if any missing room status image from server
                        if(model.rstat_Image == nil)
                        {
                            NSString *imageName = nil;
                            if (model.rstat_Id == ENUM_ROOM_STATUS_VC) {
                                imageName = @"vc.png";
                            } else if (model.rstat_Id == ENUM_ROOM_STATUS_VD) {
                                imageName = @"vd.png";
                            } else if (model.rstat_Id == ENUM_ROOM_STATUS_OC) {
                                imageName = @"oc.png";
                            } else if (model.rstat_Id == ENUM_ROOM_STATUS_OD) {
                                imageName = @"od.png";
                            } else if (model.rstat_Id == ENUM_ROOM_STATUS_VI) {
                                imageName = @"vi.png";
                            } else if (model.rstat_Id == ENUM_ROOM_STATUS_OI) {
                                imageName = @"oi.png";
                            } else if (model.rstat_Id == ENUM_ROOM_STATUS_VPU) {
                                imageName = @"vpu.png";
                            } else if (model.rstat_Id == ENUM_ROOM_STATUS_OPU) {
                                imageName = @"opu.png";
                            } else if (model.rstat_Id == ENUM_ROOM_STATUS_VCB) {
                                imageName = @"vcb.png";
                            } else if (model.rstat_Id == ENUM_ROOM_STATUS_OOO) {
                                imageName = @"ooo.png";
                            } else if (model.rstat_Id == ENUM_ROOM_STATUS_OC_SO) {
                                imageName = @"oc-so.png";
                            } else if (model.rstat_Id == ENUM_ROOM_STATUS_OOS) {
                                imageName = @"oos.png";
                            }
                            
                            model.rstat_Image = UIImagePNGRepresentation([UIImage imageNamed:imageName]);
                        }
                        //HaoTran[20130508/Fix Layout] - End
                        
                        if (compareModel.rstat_Code == nil) {
                            [self insertRoomStatusData:model];
                        }
                        
                        if (![compareModel.rstat_LastModified isEqualToString:roomStatus.rsLastModifed]) {
                            [self updateRoomStatusData:model];
                        }
                        compareModel.rstat_Id = 0;
                        [self updatePercentView:percentView value:currentPercent description:LOADING_GET_ALL_ROOM_STATUS];
                        currentPercent += stepPercent;
                    }
                }
                else
                    result = NO;
            }
            else
                result = NO;
        }
        else
            result = NO;
    }
    return result;
}

#pragma mark - Room Record Model for DB
-(int)insertRoomRecordData:(RoomRecordModelV2 *)record{
    RoomRecordAdapterV2 *roomRecordAdapter1 = [[RoomRecordAdapterV2 alloc] init];
    [roomRecordAdapter1 openDatabase];
    [roomRecordAdapter1 resetSqlCommand];
    int result = [roomRecordAdapter1 insertRoomRecordData:record];
    [roomRecordAdapter1 close];
  
    roomRecordAdapter1 = nil;
    
    return result;
}

-(int)updateRoomRecordData:(RoomRecordModelV2 *)record {
    RoomRecordAdapterV2 *roomRecordAdapter1 = [[RoomRecordAdapterV2 alloc] init];
    [roomRecordAdapter1 openDatabase];
    [roomRecordAdapter1 resetSqlCommand];
    int result = [roomRecordAdapter1 updateRoomRecordData:record];
    [roomRecordAdapter1 close];
    
    roomRecordAdapter1 = nil;
    
    return result;
}

-(int)deleteRoomRecordData:(RoomRecordModelV2 *)record {    
    RoomRecordAdapterV2 *roomRecordAdapter1 = [[RoomRecordAdapterV2 alloc] init];
    [roomRecordAdapter1 openDatabase];
    [roomRecordAdapter1 resetSqlCommand];    
    int result = [roomRecordAdapter1 deleteRoomRecordData:record];
    [roomRecordAdapter1 close];
    return result;
}

-(RoomRecordModelV2 *)loadRoomRecordData:(RoomRecordModelV2 *)record {
    RoomRecordAdapterV2 *roomRecordAdapter1 = [[RoomRecordAdapterV2 alloc] init];
    [roomRecordAdapter1 openDatabase];
    [roomRecordAdapter1 resetSqlCommand]; 
    [roomRecordAdapter1 loadRoomRecordData:record];
    [roomRecordAdapter1 close];

    roomRecordAdapter1 = nil;
    
    return record;
}

-(RoomRecordModelV2 *)loadRoomRecordDataByRoomIdAndUserId:(RoomRecordModelV2 *)record {
    RoomRecordAdapterV2 *roomRecordAdapter1 = [[RoomRecordAdapterV2 alloc] init];
    [roomRecordAdapter1 openDatabase];
    [roomRecordAdapter1 resetSqlCommand]; 
    [roomRecordAdapter1 loadRoomRecordDataByRoomIdAndUserId:record];
    [roomRecordAdapter1 close];

    roomRecordAdapter1 = nil;
    
    return record;
}

-(NSMutableArray *)loadAllRoomRecord {
    RoomRecordAdapterV2 *roomRecordAdapter1 = [[RoomRecordAdapterV2 alloc] init];
    [roomRecordAdapter1 openDatabase];
    [roomRecordAdapter1 resetSqlCommand];    
    NSMutableArray * results = [roomRecordAdapter1 loadAllRoomRecord];
    [roomRecordAdapter1 close];

    roomRecordAdapter1 = nil;
    
    return results;
}

-(NSMutableArray *)loadAllRoomRecordByCurrentUser {
    RoomRecordAdapterV2 *roomRecordAdapter1 = [[RoomRecordAdapterV2 alloc] init];
    [roomRecordAdapter1 openDatabase];
    [roomRecordAdapter1 resetSqlCommand];    
    NSMutableArray * results = [roomRecordAdapter1 loadAllRoomRecordByCurrentUser];
    [roomRecordAdapter1 close];

    roomRecordAdapter1 = nil;
    
    return results;
}

#pragma mark - Room Record History for DB

-(int)insertRoomRecordHistoryData:(RoomRecordHistory *)record{
    RoomRecordHistoryAdapter *roomRecordAdapter1 = [[RoomRecordHistoryAdapter alloc] init];
    [roomRecordAdapter1 openDatabase];
    [roomRecordAdapter1 resetSqlCommand];
    int result = [roomRecordAdapter1 insertRoomRecordHistoryData:record];
    [roomRecordAdapter1 close];
    
    roomRecordAdapter1 = nil;
    
    return result;
}

-(int)updateRoomRecordHistoryData:(RoomRecordHistory *)record {
    RoomRecordHistoryAdapter *roomRecordAdapter1 = [[RoomRecordHistoryAdapter alloc] init];
    [roomRecordAdapter1 openDatabase];
    [roomRecordAdapter1 resetSqlCommand];
    int result = [roomRecordAdapter1 updateRoomRecordHistoryData:record];
    [roomRecordAdapter1 close];
    
    roomRecordAdapter1 = nil;
    
    return result;
}

-(int)deleteRoomRecordHistoryData:(RoomRecordHistory *)record {
    RoomRecordHistoryAdapter *roomRecordAdapter1 = [[RoomRecordHistoryAdapter alloc] init];
    [roomRecordAdapter1 openDatabase];
    [roomRecordAdapter1 resetSqlCommand];
    int result = [roomRecordAdapter1 deleteRoomRecordHistoryData:record];
    [roomRecordAdapter1 close];
    return result;
}

-(int) countUnpostRoomRecordHistoryByUserId:(NSInteger) userId
{
    int totalCount = 0;
    
    NSMutableString *sqlCountOrder = [[NSMutableString alloc] initWithFormat:@"SELECT count(*) FROM %@ WHERE %@ = %d AND %@ = %d",
                                      
                                      ROOM_RECORD_HISTORY,
                                      
                                      rrec_user_id,
                                      (int)userId,
                                      
                                      rrec_pos_status,
                                      (int)POST_STATUS_SAVED_UNPOSTED
                                      ];
    
    DataTable *tableCount = [[DataTable alloc] initWithNameFormat:@"%@", COUNT_ORDERS];
    
    DataRow *rowRef = [[DataRow alloc] init];
    [rowRef addColumnFormatInt:@"CountResult"];
    [tableCount loadTableDataWithQuery:sqlCountOrder referenceRow:rowRef];
    
    int rowCount = [tableCount countRows];
    
    if(rowCount > 0){
        totalCount = [[tableCount rowWithIndex:0] intValueWithColumnNameFormat:@"CountResult"];
    }
    
    return totalCount;
}

-(RoomRecordHistory *)loadRoomRecordHistoryData:(RoomRecordHistory *)record {
    RoomRecordHistoryAdapter *roomRecordAdapter1 = [[RoomRecordHistoryAdapter alloc] init];
    [roomRecordAdapter1 openDatabase];
    [roomRecordAdapter1 resetSqlCommand];
    [roomRecordAdapter1 loadRoomRecordHistoryData:record];
    [roomRecordAdapter1 close];
    
    roomRecordAdapter1 = nil;
    
    return record;
}

-(RoomRecordHistory *)loadRoomRecordHistoryDataByRoomIdAndUserId:(RoomRecordHistory *)record {
    RoomRecordHistoryAdapter *roomRecordAdapter1 = [[RoomRecordHistoryAdapter alloc] init];
    [roomRecordAdapter1 openDatabase];
    [roomRecordAdapter1 resetSqlCommand];
    [roomRecordAdapter1 loadRoomRecordHistoryDataByRoomIdAndUserId:record];
    [roomRecordAdapter1 close];
    
    roomRecordAdapter1 = nil;
    
    return record;
}

-(NSMutableArray *)loadAllRoomRecordHistory {
    RoomRecordHistoryAdapter *roomRecordAdapter1 = [[RoomRecordHistoryAdapter alloc] init];
    [roomRecordAdapter1 openDatabase];
    [roomRecordAdapter1 resetSqlCommand];
    NSMutableArray * results = [roomRecordAdapter1 loadAllRoomRecordHistory];
    [roomRecordAdapter1 close];
    
    roomRecordAdapter1 = nil;
    
    return results;
}

-(NSMutableArray *)loadAllRoomRecordHistoryByCurrentUser {
    RoomRecordHistoryAdapter *roomRecordAdapter1 = [[RoomRecordHistoryAdapter alloc] init];
    [roomRecordAdapter1 openDatabase];
    [roomRecordAdapter1 resetSqlCommand];
    NSMutableArray * results = [roomRecordAdapter1 loadAllRoomRecordHistoryByCurrentUser];
    [roomRecordAdapter1 close];
    
    roomRecordAdapter1 = nil;
    
    return results;
}

-(NSMutableArray *)loadAllUnpostRoomRecordHistoryByCurrentUser {
    RoomRecordHistoryAdapter *roomRecordAdapter1 = [[RoomRecordHistoryAdapter alloc] init];
    [roomRecordAdapter1 openDatabase];
    [roomRecordAdapter1 resetSqlCommand];
    NSMutableArray * results = [roomRecordAdapter1 loadAllUnpostRoomRecordHistoryByCurrentUser];
    [roomRecordAdapter1 close];
    
    roomRecordAdapter1 = nil;
    
    return results;
}

#pragma mark - room record detail

-(int) insertRoomRecordDetailData:(RoomRecordDetailModelV2 *) record{
    RoomRecordDetailAdapterV2 *roomRecordAdapter1 = [[RoomRecordDetailAdapterV2 alloc] init];
    [roomRecordAdapter1 openDatabase];
    [roomRecordAdapter1 resetSqlCommand];
    int result = [roomRecordAdapter1 insertRoomRecordDetailData:record];
    [roomRecordAdapter1 close];
    
    roomRecordAdapter1 = nil;
    
    return result;
    
}
-(int) updateRoomRecordDetailData:(RoomRecordDetailModelV2 *) record{
    RoomRecordDetailAdapterV2 *roomRecordAdapter1 = [[RoomRecordDetailAdapterV2 alloc] init];
    [roomRecordAdapter1 openDatabase];
    [roomRecordAdapter1 resetSqlCommand];
    int result = [roomRecordAdapter1 updateRoomRecordDetailData:record];
    [roomRecordAdapter1 close];
    
    roomRecordAdapter1 = nil;
    
    return result;

}
-(int) deleteRoomRecordDetailData:(RoomRecordDetailModelV2 *) record{
    RoomRecordDetailAdapterV2 *roomRecordAdapter1 = [[RoomRecordDetailAdapterV2 alloc] init];
    [roomRecordAdapter1 openDatabase];
    [roomRecordAdapter1 resetSqlCommand];
    int result = [roomRecordAdapter1 deleteRoomRecordDetailData:record];
    [roomRecordAdapter1 close];
    
    roomRecordAdapter1 = nil;
    
    return result;

}
-(RoomRecordDetailModelV2 *) loadRoomRecordDetailData:(RoomRecordDetailModelV2 *) record{
    RoomRecordDetailAdapterV2 *roomRecordAdapter1 = [[RoomRecordDetailAdapterV2 alloc] init];
    [roomRecordAdapter1 openDatabase];
    [roomRecordAdapter1 resetSqlCommand];
    RoomRecordDetailModelV2 *result = [roomRecordAdapter1 loadRoomRecordDetailData:record];
    [roomRecordAdapter1 close];
    
    roomRecordAdapter1 = nil;
    
    return result;

}
-(RoomRecordDetailModelV2 *) loadRoomRecordDataDetailByRoomAssignmentIdAndUserId:(RoomRecordDetailModelV2 *) record{
    RoomRecordDetailAdapterV2 *roomRecordAdapter1 = [[RoomRecordDetailAdapterV2 alloc] init];
    [roomRecordAdapter1 openDatabase];
    [roomRecordAdapter1 resetSqlCommand];
    RoomRecordDetailModelV2 *result = [roomRecordAdapter1 loadRoomRecordDataDetailByRoomAssignmentIdAndUserId:record];
    [roomRecordAdapter1 close];
    
    roomRecordAdapter1 = nil;
    
    return result;

}

-(NSMutableArray *)loadAllRoomRecordDetailsByRoomAssignmentId:(NSInteger)raId andUserId:(NSInteger)userId {
    RoomRecordDetailAdapterV2 *adapter = [[RoomRecordDetailAdapterV2 alloc] init];
    [adapter openDatabase];
    NSMutableArray *recordDetails = [adapter loadAllRoomRecordDetailsByRoomAssignmentId:raId andUserId:userId];
    [adapter close];
    
    return recordDetails;
}

-(NSInteger) numberOfRoomRecordDetails:(RoomRecordDetailModelV2 *) record{
    RoomRecordDetailAdapterV2 *roomRecordAdapter1 = [[RoomRecordDetailAdapterV2 alloc] init];
    [roomRecordAdapter1 openDatabase];
    [roomRecordAdapter1 resetSqlCommand];
    int result = (int)[roomRecordAdapter1 numberOfRoomRecordDetails:record];
    [roomRecordAdapter1 close];
    
    roomRecordAdapter1 = nil;
    
    return result;
}
-(NSInteger) numberOfRoomRecordDetailMustSyn{
    RoomRecordDetailAdapterV2 *roomRecordAdapter1 = [[RoomRecordDetailAdapterV2 alloc] init];
    [roomRecordAdapter1 openDatabase];
    [roomRecordAdapter1 resetSqlCommand];
    int result = (int)[roomRecordAdapter1 numberOfRoomRecordDetailMustSyn];
    [roomRecordAdapter1 close];
    
    roomRecordAdapter1 = nil;
    
    return result;

}

-(NSMutableArray *) loadAllRoomRecordDetailByCurrentUser:(NSInteger)userID{
    RoomRecordDetailAdapterV2 *roomRecordAdapter1 = [[RoomRecordDetailAdapterV2 alloc] init];
    [roomRecordAdapter1 openDatabase];
    [roomRecordAdapter1 resetSqlCommand];
    NSMutableArray *result = [roomRecordAdapter1 loadAllRoomRecordDetailByCurrentUser:userID];
    [roomRecordAdapter1 close];
    
    roomRecordAdapter1 = nil;
    
    return result;

}

-(BOOL)postRoomRecordDetail:(RoomRecordDetailModelV2*)roomRecordDetail{
    if([LogFileManager isLogConsole])
    {
        NSLog(@"postRoomRecordDetail");
    }
    return YES;
}

-(BOOL)getRoomRecordDetail:(NSInteger)userId andLastModified:(NSString*)lastModified{
    if([LogFileManager isLogConsole])
    {
        NSLog(@"getRoomRecordDetail");
    }
    return YES;
}

#pragma mark - Room Remark Model for DB
-(int)insertRoomRemarkData:(RoomRemarkModelV2 *)remark {
    RoomRemarkAdapterV2 *roomRemarkAdapter1 = [[RoomRemarkAdapterV2 alloc] init];
    [roomRemarkAdapter1 openDatabase];
    [roomRemarkAdapter1 resetSqlCommand];
    int result = [roomRemarkAdapter1 insertRoomRemarkData:remark];
    [roomRemarkAdapter1 close];

    roomRemarkAdapter1 = nil;
    
    return result;
}

-(int)countRoomRemarkData:(NSInteger)recordID {
    RoomRemarkAdapterV2 *roomRemarkAdapter1 = [[RoomRemarkAdapterV2 alloc] init];
    [roomRemarkAdapter1 openDatabase];
    [roomRemarkAdapter1 resetSqlCommand];
    int result = (int)[roomRemarkAdapter1 countRecordsByRecordId:(int)recordID];
    [roomRemarkAdapter1 close];

    roomRemarkAdapter1 = nil;
    return result;
}
- (int)countRoomRemarkUnPost{
    RoomRemarkAdapterV2 *roomRemarkAdapter1 = [[RoomRemarkAdapterV2 alloc] init];
    [roomRemarkAdapter1 openDatabase];
    [roomRemarkAdapter1 resetSqlCommand];
    int result = (int)[roomRemarkAdapter1 countRecordUnPost];
    [roomRemarkAdapter1 close];
    
    roomRemarkAdapter1 = nil;
    return result;
}

-(int)updateRoomRemarkData:(RoomRemarkModelV2 *)remark {
    RoomRemarkAdapterV2 *roomRemarkAdapter1 = [[RoomRemarkAdapterV2 alloc] init];
    [roomRemarkAdapter1 openDatabase];
    [roomRemarkAdapter1 resetSqlCommand];
    int result = [roomRemarkAdapter1 updateRoomRemarkData:remark];
    [roomRemarkAdapter1 close];

    roomRemarkAdapter1 = nil;
    
    return result;
}

-(int)deleteRoomRemarkData:(RoomRemarkModelV2 *)remark{    
    RoomRemarkAdapterV2 *roomRemarkAdapter1 = [[RoomRemarkAdapterV2 alloc] init];
    [roomRemarkAdapter1 openDatabase];
    [roomRemarkAdapter1 resetSqlCommand];   
    int result = [roomRemarkAdapter1 deleteRoomRemarkData:remark];
    [roomRemarkAdapter1 close];
    
    return result;
}

-(RoomRemarkModelV2 *)loadRoomRemarkData:(RoomRemarkModelV2 *)remark {
    RoomRemarkAdapterV2 *roomRemarkAdapter1 = [[RoomRemarkAdapterV2 alloc] init];
    [roomRemarkAdapter1 openDatabase];
    [roomRemarkAdapter1 resetSqlCommand];   
    [roomRemarkAdapter1 loadRoomRemarkData:remark];
    [roomRemarkAdapter1 close];

    roomRemarkAdapter1 = nil;
    
    return remark;
}

-(RoomRemarkModelV2 *)loadRoomRemarkDataByRecordId:(RoomRemarkModelV2 *)remark {
    RoomRemarkAdapterV2 *roomRemarkAdapter1 = [[RoomRemarkAdapterV2 alloc] init];
    [roomRemarkAdapter1 openDatabase];
    [roomRemarkAdapter1 resetSqlCommand];   
    [roomRemarkAdapter1 loadRoomRemarkDataByRecordId:remark];
    [roomRemarkAdapter1 close];

    roomRemarkAdapter1 = nil;
    
    return remark;
}

-(NSMutableArray *)loadAllRoomRemark {
    RoomRemarkAdapterV2 *roomRemarkAdapter1 = [[RoomRemarkAdapterV2 alloc] init];
    [roomRemarkAdapter1 openDatabase];
    [roomRemarkAdapter1 resetSqlCommand];  
    NSMutableArray * results = [roomRemarkAdapter1 loadAllRoomRemark];
    [roomRemarkAdapter1 close];

    roomRemarkAdapter1 = nil;
    
    return results;
}
-(NSMutableArray *) loadAllRoomRemarkUnPost{
    RoomRemarkAdapterV2 *roomRemarkAdapter1 = [[RoomRemarkAdapterV2 alloc] init];
    [roomRemarkAdapter1 openDatabase];
    [roomRemarkAdapter1 resetSqlCommand];
    NSMutableArray * results = [roomRemarkAdapter1 loadAllRoomRemarkUnPost];
    [roomRemarkAdapter1 close];
    
    roomRemarkAdapter1 = nil;
    
    return results;
}
-(NSMutableArray *)loadAllRoomRemarkByRecordId:(int)recordId {
    RoomRemarkAdapterV2 *roomRemarkAdapter1 = [[RoomRemarkAdapterV2 alloc] init];
    [roomRemarkAdapter1 openDatabase];
    [roomRemarkAdapter1 resetSqlCommand];  
    NSMutableArray * results = [roomRemarkAdapter1 loadAllRoomRemarkByRecordId:recordId];
    [roomRemarkAdapter1 close];

    roomRemarkAdapter1 = nil;
    
    return results;
}

-(BOOL) postRoomRemarkWSWithRoomRemark:(RoomRemarkModelV2 *) remark WithUserId:(NSInteger) userId andRoomAssignmentId:(NSInteger) raId {
//    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingServiceSvc eHousekeepingServiceSoap12Binding];
//    
//    eHousekeepingServiceSvc_PauseCleaning *request = [[eHousekeepingServiceSvc_PauseCleaning alloc] init];
//   
//    [request setIntUsrID:[NSNumber numberWithInteger:userId]];
//    [request setIntRoomAssignID:[NSNumber numberWithInteger:raId]];
//    [request setStrRemark:remark.rm_content];
//    [request setStrPauseTime:remark.rm_clean_pause_time];
//    
//    eHousekeepingServiceSoap12BindingResponse *response = [binding PauseCleaningUsingParameters:request];
//    
//    NSArray *responseBodyParts = response.bodyParts;
//    
//    for (id bodyPart in responseBodyParts) {
//        if ([bodyPart isKindOfClass:[eHousekeepingServiceSvc_PauseCleaningResponse class]]) {
//            eHousekeepingServiceSvc_PauseCleaningResponse *dataBody = (eHousekeepingServiceSvc_PauseCleaningResponse *)bodyPart;
//            if (dataBody != nil) {
//                if ([dataBody.PauseCleaningResult.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_NEW_RECORD_ADDED]]) {
//                    
//                    remark.rm_PostStatus = POST_STATUS_POSTED;
//                    //remove remark
//                    [[RoomManagerV2 sharedRoomManager] updateRoomRemarkData:remark];
//                    
//                    return YES;
//                }
//            }
//        }
//    }
//    return NO;
    return YES;
}

#pragma mark - Room Service Later Model
-(int) insertRoomServiceLaterData:(RoomServiceLaterModelV2 *) service {
    RoomServiceLaterAdapterV2 *serviceAdapter = [[RoomServiceLaterAdapterV2 alloc] init];
    [serviceAdapter openDatabase];
    [serviceAdapter resetSqlCommand];
    int result = [serviceAdapter insertRoomServiceLaterData:service];
    [serviceAdapter close];

    serviceAdapter = nil;
    
    return result;
}

-(int) updateRoomServiceLaterData:(RoomServiceLaterModelV2 *) service {
    RoomServiceLaterAdapterV2 *serviceAdapter = [[RoomServiceLaterAdapterV2 alloc] init];
    [serviceAdapter openDatabase];
    [serviceAdapter resetSqlCommand];
    int result = [serviceAdapter updateRoomServiceLaterData:service];
    [serviceAdapter close];

    serviceAdapter = nil;
    
    return result;
}

-(int) deleteRoomServiceLaterData:(RoomServiceLaterModelV2 *) service {
    RoomServiceLaterAdapterV2 *serviceAdapter = [[RoomServiceLaterAdapterV2 alloc] init];
    [serviceAdapter openDatabase];
    [serviceAdapter resetSqlCommand];    
    int result = [serviceAdapter deleteRoomServiceLaterData:service];
    [serviceAdapter close];
  
    serviceAdapter = nil;
    
    return result;
}

-(RoomServiceLaterModelV2 *) loadRoomServiceLaterData:(RoomServiceLaterModelV2 *) service {
    RoomServiceLaterAdapterV2 *serviceAdapter = [[RoomServiceLaterAdapterV2 alloc] init];
    [serviceAdapter openDatabase];
    [serviceAdapter resetSqlCommand]; 
    [serviceAdapter loadRoomServiceLaterData:service];
    [serviceAdapter close];
    
    serviceAdapter = nil;
    
    return service;
}

-(RoomServiceLaterModelV2 *) loadRoomServiceLaterDataByRecordId:(RoomServiceLaterModelV2 *) service {
    RoomServiceLaterAdapterV2 *serviceAdapter = [[RoomServiceLaterAdapterV2 alloc] init];
    [serviceAdapter openDatabase];
    [serviceAdapter resetSqlCommand]; 
    [serviceAdapter loadRoomServiceLaterDataByRecordId:service];
    [serviceAdapter close];
 
    serviceAdapter = nil;
    
    return service;
}

-(NSMutableArray *) loadAllRoomServiceLaterByRecordId:(int) recordId {
    RoomServiceLaterAdapterV2 *serviceAdapter = [[RoomServiceLaterAdapterV2 alloc] init];
    [serviceAdapter openDatabase];
    [serviceAdapter resetSqlCommand];    
    NSMutableArray * results = [serviceAdapter loadAllRoomServiceLaterByRecordId:recordId];
    [serviceAdapter close];

    serviceAdapter = nil;
    
    return results;
}

-(NSMutableArray *) loadAllRoomServiceLater {
    RoomServiceLaterAdapterV2 *serviceAdapter = [[RoomServiceLaterAdapterV2 alloc] init];
    [serviceAdapter openDatabase];
    [serviceAdapter resetSqlCommand];    
    NSMutableArray * results = [serviceAdapter loadAllRoomServiceLater];
    [serviceAdapter close];
  
    serviceAdapter = nil;
    
    return results;
}

-(NSInteger)numberOfRoomServiceLaterMustPost{
    // load room record  
    RoomRecordAdapterV2 *roomRecordAdapter1 = [[RoomRecordAdapterV2 alloc] init];
    [roomRecordAdapter1 openDatabase];
    [roomRecordAdapter1 resetSqlCommand];
    
    RoomRecordModelV2 *_roomRecordV2 = [[RoomRecordModelV2 alloc] init];
    _roomRecordV2.rrec_User_Id = [UserManagerV2 sharedUserManager].currentUser.userId;
    NSMutableArray *roomRecords = [roomRecordAdapter1 loadAllRoomRecordDataByUserId:_roomRecordV2];
    [roomRecordAdapter1 close];
    roomRecordAdapter1 = nil;
    
    
    RoomServiceLaterAdapterV2 *serviceAdapter = [[RoomServiceLaterAdapterV2 alloc] init];
    [serviceAdapter openDatabase];
    [serviceAdapter resetSqlCommand];    
    NSInteger result = [serviceAdapter numberOfRoomServiceLaterMustPost:roomRecords];
    [serviceAdapter close];
    
    serviceAdapter = nil;
    
    return result;
    
}

#pragma mark - === ReAssign Room Assignment ===
#pragma mark
-(BOOL) insertReassignRoomAssignmentModel:(ReassignRoomAssignmentModel *)model {
    ReassignRoomAssignmentAdapter *radapter = [[ReassignRoomAssignmentAdapter alloc] init];
    [radapter openDatabase];
    BOOL result = [radapter insertReassignRoomAssignmentModel:model];
    [radapter close];
    return result;
}

-(ReassignRoomAssignmentModel *) loadReassignRoomAssignmentModelByPrimaryKey:(ReassignRoomAssignmentModel *) model {
    ReassignRoomAssignmentAdapter *radapter = [[ReassignRoomAssignmentAdapter alloc] init];
    [radapter openDatabase];
    ReassignRoomAssignmentModel *result = [radapter loadReassignRoomAssignmentModelByPrimaryKey:model];
    [radapter close];
    return result;
}

-(BOOL) updateReassignRoomAssignmentModel:(ReassignRoomAssignmentModel *) model {
    ReassignRoomAssignmentAdapter *radapter = [[ReassignRoomAssignmentAdapter alloc] init];
    [radapter openDatabase];
    BOOL result = [radapter updateReassignRoomAssignmentModel:model];
    [radapter close];
    return result;
}

-(BOOL) deleteReassignRoomAssignmentModel:(ReassignRoomAssignmentModel *) model {
    ReassignRoomAssignmentAdapter *radapter = [[ReassignRoomAssignmentAdapter alloc] init];
    [radapter openDatabase];
    BOOL result = [radapter deleteReassignRoomAssignmentModel:model];
    [radapter close];
    return result;
}

-(NSInteger)numberOfMustPostReassignRecordsWithUserId:(NSInteger)userId {
    ReassignRoomAssignmentAdapter *radapter = [[ReassignRoomAssignmentAdapter alloc] init];
    [radapter openDatabase];
    NSInteger records = [radapter numberOfMustPostReAssignRecordsWithUserId:userId];
    [radapter close];
    return records;
}

-(NSMutableArray *)loadAllReassignRoomAssignmentModelByUserId:(NSInteger)userId {
    ReassignRoomAssignmentAdapter *radapter = [[ReassignRoomAssignmentAdapter alloc] init];
    [radapter openDatabase];
    NSMutableArray *results = [radapter loadAllReassignRoomAssignmentByUserId:userId];
    [radapter close];
    return results;
}

-(NSInteger) updateCleaningStatusWSByUserId:(NSInteger) userId withRoomAssignmentModel:(RoomAssignmentModelV2 *) roomAssignmentModel
{
    NSInteger result = 0;
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole])
    {
        [binding setLogXMLInOut:YES];
    }
    eHousekeepingService_UpdateCleaningStatus *request = [[eHousekeepingService_UpdateCleaningStatus alloc] init];
    
    [request setIntUserID:[NSNumber numberWithInteger:userId]];
    [request setIntRoomAssignmentID:[NSNumber numberWithInt:roomAssignmentModel.roomAssignment_Id]];
    [request setIntStatusID:[NSNumber numberWithInteger:roomAssignmentModel.roomAssignmentRoomCleaningStatusId]];
    
    if (true) {//roomAssignmentModel.roomAssignment_PostStatus == POST_STATUS_SAVED_UNPOSTED) {
        
//        LogObject *logObj = [[LogObject alloc] init];
//        logObj.dateTime = [NSDate date];
//        logObj.log = [NSString stringWithFormat:@"<%i><postRoomAssignmentWSByUserID><intUsrID:%i intRoomAssignID:%i intRoomStatusID:%i intCleaningStatusID:%i RaRemark:%@ cleanStartTm:%@ cleanEndTm:%@ intTotalCleaningTime:%d>",userID,userID,model.roomAssignment_Id,model.roomAssignmentRoomStatusId,model.roomAssignmentRoomCleaningStatusId,room.room_remark,record.rrec_Cleaning_Start_Time,record.rrec_Cleaning_End_Time,raTotalCleaningTime];
//        LogFileManager *logManager = [[LogFileManager alloc] init];
//        [logManager save:logObj];
        
        eHousekeepingServiceSoap12BindingResponse *response = [binding UpdateCleaningStatusUsingParameters:request];
        
        NSArray *responseBodyParts = response.bodyParts;
        
        for (id bodyPart in responseBodyParts) {
            if ([bodyPart isKindOfClass:[eHousekeepingService_UpdateCleaningStatusResponse class]]) {
                eHousekeepingService_UpdateCleaningStatusResponse *dataBody = (eHousekeepingService_UpdateCleaningStatusResponse *)bodyPart;
                if (dataBody != nil) {
                    
                    if ([dataBody.UpdateCleaningStatusResult.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_OK]]) {
                        //change post status
                        result = RESPONSE_STATUS_OK;
                        if (roomAssignmentModel.roomAssignment_PostStatus == POST_STATUS_SAVED_UNPOSTED) {
                            roomAssignmentModel.roomAssignment_PostStatus = (int)POST_STATUS_POSTED;
                            [self update:roomAssignmentModel];
                        }
                        
                    } else if([dataBody.UpdateCleaningStatusResult.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_ROOM_HAVE_BEEN_DELETED]]) {
                        [self setDeleteKey:DELETE_ROOM_ASSIGNMENT_FLAG forRoomAssignmentId:roomAssignmentModel.roomAssignment_Id andUserId:roomAssignmentModel.roomAssignment_UserId];
                        result = RESPONSE_STATUS_ROOM_HAVE_BEEN_DELETED;
                    }
                }
            }
        }
        
    }
    
    return result;
}

#pragma mark -  Progress step loading
//return percentage step
-(float) caculateStepFromCountingObject:(int) countValue
{
    if(countValue == 0 || countValue == 1)
        return 100;
    
    float stepValue = (float)(100 / (float)countValue);
    return stepValue;
}

#pragma mark - Update Percent View

-(void)updatePercentView:(MBProgressHUD*)percentView value:(float)percentageValue description:(NSString *)description
{
    if (percentView) {
        NSString *percentString = [[NSString alloc]initWithFormat:@"%.0f%%",percentageValue];
        percentView.detailsLabelFont = percentView.labelFont;
        percentView.detailsLabelText = percentString;
        
        float nextStepProgress = [SyncManagerV2 getNextStepOperations];
        int maxSyncOperations = [SyncManagerV2 getMaxSyncOperations];
        if(nextStepProgress > 0 && maxSyncOperations > 0)
        {
            double currentOperations = round(percentView.progress/nextStepProgress);
            //percentView.labelText = [NSString stringWithFormat:@"%@ - [%0.f/%d]", description, currentOperations, maxSyncOperations];
            percentView.labelText = [NSString stringWithFormat:@"[%0.f/%d]", currentOperations, maxSyncOperations];
        }
        else
        {
            percentView.labelText = description;
        }
    }
}

#pragma mark - Room Behind Schedule & Rush/Queue Room

//Return list of PopupMsgItem
-(NSMutableArray*) getPopUpMessageFromWSByUserId:(int)userId lastModified:(NSString*)lastModified
{
    //No network detected……
    if(![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]){
        return nil;
    }
    
    if(userId <= 0){
        return nil;
    }
    
    NSMutableArray *listMessages = nil;
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole]) {
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_GetPopupMsgList *request = [[eHousekeepingService_GetPopupMsgList alloc] init];
    
    [request setIntUsrID:[NSNumber numberWithInt:userId]];
    if(lastModified.length > 0) {
        [request setStrLastModified:lastModified];
    }
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetPopupMsgListUsingParameters:request];
    NSArray *responseBodyParts = response.bodyParts;
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetPopupMsgListResponse class]]) {
            eHousekeepingService_GetPopupMsgListResponse *dataBody = (eHousekeepingService_GetPopupMsgListResponse *)bodyPart;
            
            if (dataBody != nil) {
                
                if ([dataBody.GetPopupMsgListResult.ResponseStatus.respCode integerValue] == RESPONSE_STATUS_OK) {
                    
                    if(dataBody.GetPopupMsgListResult) {
                        
                        if(dataBody.GetPopupMsgListResult.PopupMsgItems) {
                            listMessages = [NSMutableArray array];
                            NSMutableArray *listWSMsgItems = dataBody.GetPopupMsgListResult.PopupMsgItems.PopupMsgItem;
                            
                            for (eHousekeepingService_PopupMsgItem *curWSMsgItem in listWSMsgItems) {
                                PopupMessageItemModel *curMsgPopupItem = [[PopupMessageItemModel alloc] init];
                                curMsgPopupItem.popupId = [curWSMsgItem.popupID intValue];
                                curMsgPopupItem.popupMessage = curWSMsgItem.popupMsg;
                                curMsgPopupItem.popupDateTime = curWSMsgItem.popupDateTime;
                                [listMessages addObject:curMsgPopupItem];
                            }
                        }
                    }
                }
            }
        }
    }
    
    return listMessages;
}

//List ID is NSString with comma between them e.g 1,2,3,4 ...
-(int) updateReadMessagePopUpByUserId:(int)userId listID:(NSString*)listIds
{
    //No network detected……
    if(![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]){
        return RESPONSE_STATUS_NO_NETWORK;
    }
    
    if(userId <= 0){
        return RESPONSE_STATUS_ERROR;
    }
    
    if(listIds.length <= 0) {
        return RESPONSE_STATUS_ERROR;
    }
    
    int result = RESPONSE_STATUS_ERROR;
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole]) {
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_UpdatePopupMsgStatus *request = [[eHousekeepingService_UpdatePopupMsgStatus alloc] init];
    
    [request setIntUsrID:[NSNumber numberWithInt:userId]];
    [request setIntListPopupMsgID:listIds];
    [request setIntStatus:[NSNumber numberWithInt:1]]; //1 as already read pop up
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding UpdatePopupMsgStatusUsingParameters:request];
    NSArray *responseBodyParts = response.bodyParts;
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_UpdatePopupMsgStatusResponse class]]) {
            eHousekeepingService_UpdatePopupMsgStatusResponse *dataBody = (eHousekeepingService_UpdatePopupMsgStatusResponse *)bodyPart;
            
            if (dataBody != nil) {
                if ([dataBody.UpdatePopupMsgStatusResult.respCode integerValue] == RESPONSE_STATUS_OK) {
                    result = RESPONSE_STATUS_OK;
                }
            }
        }
    }
    
    return result;
}

#pragma mark - Post WSLog
-(BOOL)postWSLog:(int)userId message:(NSString*)messageValue messageType:(int)type
{
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService
                                                  eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole])
    {
        [binding setLogXMLInOut:YES];
    }
    eHousekeepingService_PostWSLog *request = [[eHousekeepingService_PostWSLog
                                                alloc] init];
    request.intUsrID = [NSNumber numberWithInt:userId];
    request.strMessage = messageValue;
    request.intMessageType = [NSNumber numberWithInt:type];
    eHousekeepingServiceSoap12BindingResponse *response = [binding
                                                           PostWSLogUsingParameters:request];
    
    BOOL isPostSuccess = NO;
    
    for (id bodyPart in response.bodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_PostWSLogResponse class]]) {
            eHousekeepingService_PostWSLogResponse *dataBody =
            (eHousekeepingService_PostWSLogResponse *)bodyPart;
            if (dataBody != nil) {
                if ([dataBody.PostWSLogResult.respCode isEqualToNumber:[NSNumber
                                                                        numberWithInteger:RESPONSE_STATUS_OK]])
                {
                    isPostSuccess = YES;
                }
            }
        }
    }
    return isPostSuccess;
}

#pragma mark - Post LogInvalidStartRoom
- (BOOL)postRoomStatus:(RoomRemarkModelV2 *)remark WithUserId:(NSInteger)userId{
    return YES;
}
-(BOOL)postLogInvalidStartRoomWithUserID:(NSInteger)userId userName:(NSString*)userName prevRoomId:(NSString*)prevRoomId prevRoomNo:(NSString*)prevRoomNo newRoomId:(NSString*)newRoomId newRoomNo:(NSString*)newRoomNo {
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService
                                                  eHousekeepingServiceSoap12Binding];
    //Allow write log or not
    if([LogFileManager isLogConsole]) {
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_LogInvalidStartRoom *request = [[eHousekeepingService_LogInvalidStartRoom
                                                alloc] init];
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    request.strUser_ID = [NSString stringWithFormat:@"%ld", (long)userId ];
    request.strUser_Name = userName;
    request.intPrevious_DAID = [f numberFromString:prevRoomId];
    request.strPrevious_RoomNo = prevRoomNo;
    request.intNew_DAID = [f numberFromString:newRoomId];
    request.strNew_RoomNo = newRoomNo;
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding LogInvalidStartRoomUsingParameters:request];
    
    BOOL isPostSuccess = NO;
    
    for (id bodyPart in response.bodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_LogInvalidStartRoomResponse class]]) {
            eHousekeepingService_LogInvalidStartRoomResponse *dataBody =
            (eHousekeepingService_LogInvalidStartRoomResponse *)bodyPart;
            if (dataBody != nil) {
                if ([dataBody.LogInvalidStartRoomResult.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_OK]]) {
                    isPostSuccess = YES;
                }
            }
        }
    }
    return isPostSuccess;
}

#pragma mark - Get Room Type By Room Number

- (RoomTypeModel*)getRoomTypeByRoomNoDBWithUserId:(NSInteger)userId andRoomNumber:(NSString*)roomNumber {
    
    RoomAssignmentAdapterV2 *adapter = [RoomAssignmentAdapterV2 createRoomAssignmentAdapter];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    RoomTypeModel *model = [adapter getRoomTypeByRoomNoWithUserId:userId andRoomNumber:roomNumber];
    [adapter close];
    adapter = nil;
    return model;
}

- (RoomTypeModel*)getRoomTypeByRoomNoWSWithUserId:(NSInteger)userId hotelId:(NSInteger)hotelId roomNumber:(NSString*)roomNumber {
    
    if (![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        return nil;
    }
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole]) {
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_GetRoomTypeByRoomNoRoutine *request = [[eHousekeepingService_GetRoomTypeByRoomNoRoutine alloc] init];
    
    [request setUserID:[NSString stringWithFormat:@"%d", (int)userId]];
    [request setPropertyID:[NSString stringWithFormat:@"%d", (int)hotelId]];
    [request setRoomNo:roomNumber];
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetRoomTypeByRoomNoRoutineUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    if(responseBodyParts == nil) {
        return nil;
    }
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetRoomTypeByRoomNoRoutineResponse class]]) {
            eHousekeepingService_GetRoomTypeByRoomNoRoutineResponse *dataBody = (eHousekeepingService_GetRoomTypeByRoomNoRoutineResponse *)bodyPart;
            if (dataBody != nil) {
                if ([dataBody.GetRoomTypeByRoomNoRoutineResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInteger:RESPONSE_STATUS_OK]]) {
                    eHousekeepingService_RoomType *roomTypeWS = dataBody.GetRoomTypeByRoomNoRoutineResult.RoomTypeList.RoomType[0];
                    RoomTypeModel *model = [[RoomTypeModel alloc] init];
                    model.amsId = [roomTypeWS.rtID integerValue];
                    model.amsName = roomTypeWS.rtName;
                    model.amsNameLang = roomTypeWS.rtLang;
                    model.amsLastModified = roomTypeWS.rtLastModifed;
                    return model;
                }
            }
        }
    }
    return nil;
}

@end
