//
//  RoomRecordAdapterV2.h
//  mHouseKeeping
//
//  Created by Nghia Truong on 3/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DatabaseAdapterV2.h"
#import "RoomRecordHistory.h"
#import "UserManagerV2.h"
#import "DbDefinesV2.h"

@interface RoomRecordHistoryAdapter : DatabaseAdapterV2 {
    
}

-(int) insertRoomRecordHistoryData:(RoomRecordHistory *) record;
-(int) updateRoomRecordHistoryData:(RoomRecordHistory *) record;
-(int) deleteRoomRecordHistoryData:(RoomRecordHistory *) record;
-(RoomRecordHistory *) loadRoomRecordHistoryData:(RoomRecordHistory *) record;
-(RoomRecordHistory *) loadRoomRecordHistoryDataByRoomIdAndUserId:(RoomRecordHistory *) record;
-(NSMutableArray *) loadAllRoomRecordHistoryByCurrentUser;
-(NSMutableArray *) loadAllUnpostRoomRecordHistoryByCurrentUser;
-(NSMutableArray *) loadAllRoomRecordHistory;

@end
