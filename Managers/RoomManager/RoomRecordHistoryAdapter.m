//
//  RoomRecordAdapterV2.m
//  mHouseKeeping
//
//  Created by Nghia Truong on 3/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "RoomRecordHistoryAdapter.h"
#import "ehkDefines.h"

@implementation RoomRecordHistoryAdapter

-(int)insertRoomRecordHistoryData:(RoomRecordHistory *)record {
    NSString *sqlString = [[NSString alloc] initWithFormat:@"INSERT INTO %@ ( %@ , %@, %@, %@, %@, %@, %@, %@ ,%@, %@, %@, %@ , %@, %@, %@, %@) %@",
                           ROOM_RECORD_HISTORY,
                           rrec_roomAssignment_id,
                           rrec_user_id,
                           rrec_cleaning_start_time,
                           rrec_cleaning_end_time,
                           rrec_inspection_start_time,
                           rrec_inspection_end_time,
                           rrec_total_cleaning_time,
                           rrec_cleaning_date,
                           rrec_pos_status,
                           rrec_last_cleaning_duration,
                           rrec_last_inspected_duration,
                           rrec_total_inspected_time,
                           rrec_remark,
                           rrec_room_status_id,
                           rrec_cleaning_status_id,
                           rrec_cleaning_pause_time,
                           @"VALUES(?, ?, ?, ?, ?, ?, ? ,? ,? ,?, ?, ? , ?, ?, ?, ?)"];
    
    const char *sql=[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    if(sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
        
    }
    
    //    sqlite3_bind_int (self.sqlStament, 1, record.rrec_Id);
    sqlite3_bind_int (self.sqlStament, 1, record.rrec_room_assignment_id);
    sqlite3_bind_int (self.sqlStament, 2, record.rrec_User_Id);
    sqlite3_bind_text(self.sqlStament, 3, [record.rrec_Cleaning_Start_Time UTF8String],
                      -1,SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 4, [record.rrec_Cleaning_End_Time UTF8String],
                      -1,SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 5, [record.rrec_Inspection_Start_Time UTF8String],
                      -1,SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 6, [record.rrec_Inspection_End_Time UTF8String],
                      -1,SQLITE_TRANSIENT);
    
    sqlite3_bind_int (self.sqlStament, 7, record.rrec_Total_Cleaning_Time);
    sqlite3_bind_text(self.sqlStament, 8, [record.rrec_Cleaning_Date UTF8String],
                      -1,SQLITE_TRANSIENT);
    sqlite3_bind_int (self.sqlStament, 9, record.rrec_PostStatus);
    sqlite3_bind_text(self.sqlStament, 10, [record.rrec_Last_Cleaning_Duration UTF8String],
                      -1,SQLITE_TRANSIENT);
    sqlite3_bind_text(sqlStament, 11, [record.rrec_Last_Inspected_Duration UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_int (self.sqlStament, 12, record.rrec_Total_Inspected_Time);
    sqlite3_bind_text(sqlStament, 13, [record.rrec_Remark UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_int (self.sqlStament, 14, record.rrec_room_status_id);
    sqlite3_bind_int (self.sqlStament, 15, record.rrec_cleaning_status_id);
    sqlite3_bind_text(sqlStament, 16, [record.rrec_cleaning_pause_time UTF8String], -1, SQLITE_TRANSIENT);
    
    NSInteger returnInt = 0;
    
    NSInteger status = sqlite3_step(self.sqlStament);
    
    if(SQLITE_DONE != status) {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return (int)returnInt;
    } else {
        sqlite3_reset(self.sqlStament);
        returnInt = 1;
        return (int)returnInt;
    }
}

-(int)updateRoomRecordHistoryData:(RoomRecordHistory *)record {
    NSString *sqlString = [[NSString alloc] initWithFormat:@"UPDATE %@ SET %@ = ?,%@ = ?,%@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ? , %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ? WHERE %@ = ?", ROOM_RECORD_HISTORY,
                           rrec_id,
                           rrec_roomAssignment_id,
                           rrec_user_id,
                           rrec_cleaning_start_time,
                           rrec_cleaning_end_time,
                           rrec_inspection_start_time,
                           rrec_inspection_end_time,
                           rrec_total_cleaning_time,
                           rrec_cleaning_date,
                           rrec_pos_status,
                           rrec_last_cleaning_duration,
                           rrec_last_inspected_duration,
                           rrec_total_inspected_time,
                           rrec_remark,
                           rrec_room_status_id,
                           rrec_cleaning_status_id,
                           rrec_cleaning_pause_time,
                           
                           rrec_id];
    
    const char *sql = [sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
        
    }
    
    sqlite3_bind_int (self.sqlStament, 1, record.rrec_Id);
    sqlite3_bind_int (self.sqlStament, 2, record.rrec_room_assignment_id);
    sqlite3_bind_int (self.sqlStament, 3, record.rrec_User_Id);
    sqlite3_bind_text(self.sqlStament, 4, [record.rrec_Cleaning_Start_Time UTF8String],
                      -1,SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 5, [record.rrec_Cleaning_End_Time UTF8String],
                      -1,SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 6, [record.rrec_Inspection_Start_Time UTF8String],
                      -1,SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 7, [record.rrec_Inspection_End_Time UTF8String],
                      -1,SQLITE_TRANSIENT);
    sqlite3_bind_int (self.sqlStament, 8, record.rrec_Total_Cleaning_Time);
    
    sqlite3_bind_text(self.sqlStament, 9, [record.rrec_Cleaning_Date UTF8String],
                      -1,SQLITE_TRANSIENT);
    sqlite3_bind_int (self.sqlStament, 10, record.rrec_PostStatus);
    sqlite3_bind_text(self.sqlStament, 11, [record.rrec_Last_Cleaning_Duration UTF8String],
                      -1,SQLITE_TRANSIENT);
    
    sqlite3_bind_text(sqlStament, 12, [record.rrec_Last_Inspected_Duration UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_int (self.sqlStament, 13, record.rrec_Total_Inspected_Time);
    
    sqlite3_bind_text(sqlStament, 14, [record.rrec_Remark UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_int (self.sqlStament, 15, record.rrec_room_status_id);
    sqlite3_bind_int (self.sqlStament, 16, record.rrec_cleaning_status_id);
    sqlite3_bind_text(sqlStament, 17, [record.rrec_cleaning_pause_time UTF8String], -1, SQLITE_TRANSIENT);
    
    sqlite3_bind_int (self.sqlStament, 18, record.rrec_Id);
    
    NSInteger returnInt = 0;
    NSInteger status = sqlite3_step(self.sqlStament);
    if(SQLITE_DONE != status) {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return (int)returnInt;
    } else {
        returnInt = 1;
        return (int)returnInt;
    }
}

-(int)deleteRoomRecordHistoryData:(RoomRecordHistory *)record {
    NSInteger result = 1;
    
    NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"DELETE FROM %@ WHERE %@ = ?", ROOM_RECORD_HISTORY, rrec_id];
    
    const char *sql = [sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
        result = 0;
	
    sqlite3_bind_int(self.sqlStament, 1, record.rrec_Id);
	
    NSInteger status = sqlite3_step(sqlStament);
    
	if (SQLITE_DONE != status) {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
		result = 0;
    }
    
    return (int)result;
    
}

-(RoomRecordHistory *)loadRoomRecordHistoryData:(RoomRecordHistory *)record {
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@,%@,%@,%@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ?",
                           rrec_id,
                           rrec_roomAssignment_id,
                           rrec_user_id,
                           rrec_cleaning_start_time,
                           rrec_cleaning_end_time,
                           rrec_inspection_start_time,
                           rrec_inspection_end_time,
                           rrec_total_cleaning_time,
                           rrec_cleaning_date,
                           rrec_pos_status,
                           rrec_last_cleaning_duration,
                           rrec_last_inspected_duration,
                           rrec_total_inspected_time,
                           rrec_remark,
                           rrec_room_status_id,
                           rrec_cleaning_status_id,
                           rrec_cleaning_pause_time,
                           
                           ROOM_RECORD_HISTORY,
                           rrec_id];
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {
        sqlite3_bind_int(self.sqlStament, 1, record.rrec_Id);
        NSInteger status = sqlite3_step(sqlStament);
        while(status == SQLITE_ROW) {
            record.rrec_Id = sqlite3_column_int(self.sqlStament, 0);
            record.rrec_room_assignment_id = sqlite3_column_int(self.sqlStament, 1);
            record.rrec_User_Id = sqlite3_column_int(self.sqlStament, 2);
            if (sqlite3_column_text(self.sqlStament, 3)) {
                record.rrec_Cleaning_Start_Time=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
            }
            if (sqlite3_column_text(self.sqlStament, 4)) {
                record.rrec_Cleaning_End_Time = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
            }
            if (sqlite3_column_text(self.sqlStament, 5)) {
                record.rrec_Inspection_Start_Time = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
            }
            
            if (sqlite3_column_text(self.sqlStament, 6)) {
                record.rrec_Inspection_End_Time = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 6)];
            }
            
            //if (sqlite3_column_text(self.sqlStament, 7)) {
            record.rrec_Total_Cleaning_Time = sqlite3_column_int(self.sqlStament, 7);
            //}
            if (sqlite3_column_text(self.sqlStament, 8)) {
                record.rrec_Cleaning_Date = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 8)];
            }
            
            record.rrec_PostStatus = sqlite3_column_int(self.sqlStament, 9);
            
            if (sqlite3_column_text(self.sqlStament, 10)) {
                record.rrec_Last_Cleaning_Duration = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 10)];
            }
            if (sqlite3_column_text(sqlStament, 11)) {
                record.rrec_Last_Inspected_Duration = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 11)];
            }
            //if (sqlite3_column_text(sqlStament, 12)) {
            record.rrec_Total_Inspected_Time = sqlite3_column_int(self.sqlStament, 12);
            //}
            
            if (sqlite3_column_text(sqlStament, 13)) {
                record.rrec_Remark = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 13)];
            }
            
            record.rrec_room_status_id = sqlite3_column_int(self.sqlStament, 14);
            record.rrec_cleaning_status_id = sqlite3_column_int(self.sqlStament, 15);
            
            if (sqlite3_column_text(sqlStament, 16)) {
                record.rrec_cleaning_pause_time = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 16)];
            }
            
            status = sqlite3_step(sqlStament);
            
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        
    }
    
    return record;
}

-(RoomRecordHistory *)loadRoomRecordHistoryDataByRoomIdAndUserId:(RoomRecordHistory *)record {
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ? AND %@ = ?",
                           rrec_id,
                           rrec_roomAssignment_id,
                           rrec_user_id,
                           rrec_cleaning_start_time,
                           rrec_cleaning_end_time,
                           
                           rrec_inspection_start_time,
                           rrec_inspection_end_time,
                           rrec_total_cleaning_time,
                           rrec_cleaning_date,
                           rrec_pos_status,
                           rrec_last_cleaning_duration,
                           rrec_last_inspected_duration,
                           rrec_total_inspected_time,
                           rrec_remark,
                           rrec_room_status_id,
                           rrec_cleaning_status_id,
                           rrec_cleaning_pause_time,
                           
                           ROOM_RECORD_HISTORY,
                           rrec_roomAssignment_id,
                           rrec_user_id];
    
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    
    sqlite3_bind_int(self.sqlStament, 1, record.rrec_room_assignment_id);
    sqlite3_bind_int(self.sqlStament, 2, record.rrec_User_Id);
    
    
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW) {
        record.rrec_Id = sqlite3_column_int(self.sqlStament, 0);
        record.rrec_room_assignment_id = sqlite3_column_int(self.sqlStament, 1);
        record.rrec_User_Id = sqlite3_column_int(self.sqlStament, 2);
        
        if (sqlite3_column_text(self.sqlStament, 3)) {
            record.rrec_Cleaning_Start_Time=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
        }
        if (sqlite3_column_text(self.sqlStament, 4)) {
            record.rrec_Cleaning_End_Time = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
        }
        
        if (sqlite3_column_text(self.sqlStament, 5)) {
            record.rrec_Inspection_Start_Time = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
        }
        
        if (sqlite3_column_text(self.sqlStament, 6)) {
            record.rrec_Inspection_End_Time = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 6)];
        }
        
        
        //if (sqlite3_column_text(self.sqlStament, 7)) {
        record.rrec_Total_Cleaning_Time = sqlite3_column_int(self.sqlStament, 7);
        //}
        
        if (sqlite3_column_text(self.sqlStament, 8)) {
            record.rrec_Cleaning_Date = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 8)];
        }
        record.rrec_PostStatus = sqlite3_column_int(self.sqlStament, 9);
        
        if (sqlite3_column_text(self.sqlStament, 10)) {
            record.rrec_Last_Cleaning_Duration = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 10)];
        }
        
        if (sqlite3_column_text(sqlStament, 11)) {
            record.rrec_Last_Inspected_Duration = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 11)];
        }
        
        //if (sqlite3_column_text(sqlStament, 12)) {
        record.rrec_Total_Inspected_Time = sqlite3_column_int(self.sqlStament, 12);
        //}
        
        if (sqlite3_column_text(sqlStament, 13)) {
            record.rrec_Remark = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 13)];
        }
        
        record.rrec_room_status_id = sqlite3_column_int(self.sqlStament, 14);
        record.rrec_cleaning_status_id = sqlite3_column_int(self.sqlStament, 15);
        
        if (sqlite3_column_text(sqlStament, 16)) {
            record.rrec_cleaning_pause_time = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 16)];
        }
        
        status = sqlite3_step(sqlStament);
        
    }
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    
    return record;
}

-(NSMutableArray *)loadAllRoomRecordHistoryByCurrentUser {
    NSMutableArray *array = [NSMutableArray array];
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ?",
                           rrec_id,
                           rrec_roomAssignment_id,
                           rrec_user_id,
                           rrec_cleaning_start_time,
                           rrec_cleaning_end_time,
                           rrec_inspection_start_time,
                           rrec_inspection_end_time,
                           
                           rrec_total_cleaning_time,
                           rrec_cleaning_date,
                           rrec_pos_status,
                           rrec_last_cleaning_duration,
                           rrec_last_inspected_duration,
                           rrec_total_inspected_time,
                           rrec_remark,
                           rrec_room_status_id,
                           rrec_cleaning_status_id,
                           rrec_cleaning_pause_time,
                           
                           ROOM_RECORD_HISTORY,
                           rrec_user_id];
    
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    
    sqlite3_bind_int(self.sqlStament, 1, [UserManagerV2 sharedUserManager]
                     .currentUser.userId);
    
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW) {
        RoomRecordHistory *record = [[RoomRecordHistory alloc] init];
        record.rrec_Id = sqlite3_column_int(self.sqlStament, 0);
        record.rrec_room_assignment_id = sqlite3_column_int(self.sqlStament, 1);
        record.rrec_User_Id = sqlite3_column_int(self.sqlStament, 2);
        if (sqlite3_column_text(self.sqlStament, 3)) {
            record.rrec_Cleaning_Start_Time=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
        }
        if (sqlite3_column_text(self.sqlStament, 4)) {
            record.rrec_Cleaning_End_Time = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
        }
        if (sqlite3_column_text(self.sqlStament, 5)) {
            record.rrec_Inspection_Start_Time = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
        }
        
        if (sqlite3_column_text(self.sqlStament, 6)) {
            record.rrec_Inspection_End_Time = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 6)];
        }
        
        
        //if (sqlite3_column_text(self.sqlStament, 7)) {
        record.rrec_Total_Cleaning_Time = sqlite3_column_int(self.sqlStament, 7);
        //}
        if (sqlite3_column_text(self.sqlStament, 8)) {
            record.rrec_Cleaning_Date = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 8)];
        }
        record.rrec_PostStatus = sqlite3_column_int(self.sqlStament, 9);
        
        if (sqlite3_column_text(self.sqlStament, 10)) {
            record.rrec_Last_Cleaning_Duration = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 10)];
        }
        
        if (sqlite3_column_text(sqlStament, 11)) {
            record.rrec_Last_Inspected_Duration = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 11)];
        }
        
        //if (sqlite3_column_text(sqlStament, 12)) {
        record.rrec_Total_Inspected_Time = sqlite3_column_int(self.sqlStament, 12);
        //}
        
        if (sqlite3_column_text(sqlStament, 13)) {
            record.rrec_Remark = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 13)];
        }
        
        record.rrec_room_status_id = sqlite3_column_int(self.sqlStament, 14);
        record.rrec_cleaning_status_id = sqlite3_column_int(self.sqlStament, 15);
        
        if (sqlite3_column_text(sqlStament, 16)) {
            record.rrec_cleaning_pause_time = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 16)];
        }
        
        [array addObject:record];
        status = sqlite3_step(sqlStament);
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    
    return array;
}

-(NSMutableArray *)loadAllUnpostRoomRecordHistoryByCurrentUser {
    NSMutableArray *array = [NSMutableArray array];
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ? AND %@ = ? ORDER BY %@ ASC",
                           rrec_id,
                           rrec_roomAssignment_id,
                           rrec_user_id,
                           rrec_cleaning_start_time,
                           rrec_cleaning_end_time,
                           rrec_inspection_start_time,
                           rrec_inspection_end_time,
                           
                           rrec_total_cleaning_time,
                           rrec_cleaning_date,
                           rrec_pos_status,
                           rrec_last_cleaning_duration,
                           rrec_last_inspected_duration,
                           rrec_total_inspected_time,
                           rrec_remark,
                           rrec_room_status_id,
                           rrec_cleaning_status_id,
                           rrec_cleaning_pause_time,
                           
                           ROOM_RECORD_HISTORY,
                           rrec_user_id,
                           rrec_pos_status,
                           
                           //order by
                           rrec_id];
    
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    
    sqlite3_bind_int(self.sqlStament, 1, [UserManagerV2 sharedUserManager].currentUser.userId);
    sqlite3_bind_int(self.sqlStament, 2, (int)POST_STATUS_SAVED_UNPOSTED);
    
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW) {
        RoomRecordHistory *record = [[RoomRecordHistory alloc] init];
        record.rrec_Id = sqlite3_column_int(self.sqlStament, 0);
        record.rrec_room_assignment_id = sqlite3_column_int(self.sqlStament, 1);
        record.rrec_User_Id = sqlite3_column_int(self.sqlStament, 2);
        if (sqlite3_column_text(self.sqlStament, 3)) {
            record.rrec_Cleaning_Start_Time=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
        }
        if (sqlite3_column_text(self.sqlStament, 4)) {
            record.rrec_Cleaning_End_Time = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
        }
        if (sqlite3_column_text(self.sqlStament, 5)) {
            record.rrec_Inspection_Start_Time = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
        }
        
        if (sqlite3_column_text(self.sqlStament, 6)) {
            record.rrec_Inspection_End_Time = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 6)];
        }
        
        
        //if (sqlite3_column_text(self.sqlStament, 7)) {
        record.rrec_Total_Cleaning_Time = sqlite3_column_int(self.sqlStament, 7);
        //}
        if (sqlite3_column_text(self.sqlStament, 8)) {
            record.rrec_Cleaning_Date = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 8)];
        }
        record.rrec_PostStatus = sqlite3_column_int(self.sqlStament, 9);
        
        if (sqlite3_column_text(self.sqlStament, 10)) {
            record.rrec_Last_Cleaning_Duration = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 10)];
        }
        
        if (sqlite3_column_text(sqlStament, 11)) {
            record.rrec_Last_Inspected_Duration = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 11)];
        }
        
        //if (sqlite3_column_text(sqlStament, 12)) {
        record.rrec_Total_Inspected_Time = sqlite3_column_int(self.sqlStament, 12);
        //}
        
        if (sqlite3_column_text(sqlStament, 13)) {
            record.rrec_Remark = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 13)];
        }
        
        record.rrec_room_status_id = sqlite3_column_int(self.sqlStament, 14);
        record.rrec_cleaning_status_id = sqlite3_column_int(self.sqlStament, 15);
        
        if (sqlite3_column_text(sqlStament, 16)) {
            record.rrec_cleaning_pause_time = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 16)];
        }
        [array addObject:record];
        status = sqlite3_step(sqlStament);
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    
    return array;
}

-(NSMutableArray *)loadAllRoomRecordHistory {
    NSMutableArray *array = [NSMutableArray array];
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@ ,%@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ FROM %@ ",
                           rrec_id,
                           rrec_roomAssignment_id,
                           rrec_user_id,
                           rrec_cleaning_start_time,
                           rrec_cleaning_end_time,
                           rrec_inspection_start_time,
                           rrec_inspection_end_time,
                           rrec_total_cleaning_time,
                           rrec_cleaning_date,
                           rrec_pos_status,
                           rrec_last_cleaning_duration,
                           rrec_last_inspected_duration,
                           rrec_total_inspected_time,
                           rrec_remark,
                           rrec_room_status_id,
                           rrec_cleaning_status_id,
                           rrec_cleaning_pause_time,
                           
                           ROOM_RECORD_HISTORY];
    
    const char *sql =[sqlString UTF8String];
    NSInteger reAccessCount = 0;
RE_ACCESS_DB:{
    if (reAccessCount > 0) {
        [self close];
        usleep(0.1);
        [self openDatabase];
    }
    
    reAccessCount++;
}
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK) {
    }
    
    NSInteger status = sqlite3_step(sqlStament);
    while(status == SQLITE_ROW) {
        RoomRecordHistory *record = [[RoomRecordHistory alloc] init];
        record.rrec_Id = sqlite3_column_int(self.sqlStament, 0);
        record.rrec_room_assignment_id = sqlite3_column_int(self.sqlStament, 1);
        record.rrec_User_Id = sqlite3_column_int(self.sqlStament, 2);
        if (sqlite3_column_text(self.sqlStament, 3)) {
            record.rrec_Cleaning_Start_Time=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
        }
        if (sqlite3_column_text(self.sqlStament, 4)) {
            record.rrec_Cleaning_End_Time = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
        }
        if (sqlite3_column_text(self.sqlStament, 5)) {
            record.rrec_Inspection_Start_Time = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
        }
        
        if (sqlite3_column_text(self.sqlStament, 6)) {
            record.rrec_Inspection_End_Time = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 6)];
        }
        
        //if (sqlite3_column_text(self.sqlStament, 7)) {
        record.rrec_Total_Cleaning_Time = sqlite3_column_int(self.sqlStament, 7);
        //}
        if (sqlite3_column_text(self.sqlStament, 8)) {
            record.rrec_Cleaning_Date = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 8)];
        }
        record.rrec_PostStatus = sqlite3_column_int(self.sqlStament, 9);
        
        if (sqlite3_column_text(self.sqlStament, 10)) {
            record.rrec_Last_Cleaning_Duration = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 10)];
        }
        
        if (sqlite3_column_text(sqlStament, 11)) {
            record.rrec_Last_Inspected_Duration = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 11)];
        }
        
        //if (sqlite3_column_text(sqlStament, 12)) {
        record.rrec_Total_Inspected_Time = sqlite3_column_int(self.sqlStament, 12);
        //}
        
        if (sqlite3_column_text(sqlStament, 13)) {
            record.rrec_Remark = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 13)];
        }
        
        record.rrec_User_Id = sqlite3_column_int(self.sqlStament, 14);
        record.rrec_User_Id = sqlite3_column_int(self.sqlStament, 15);
        
        if (sqlite3_column_text(sqlStament, 16)) {
            record.rrec_cleaning_pause_time = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 16)];
        }
        
        [array addObject:record];
        status = sqlite3_step(sqlStament);
    }
    if (status == SQLITE_BUSY) {
        if (reAccessCount < REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    return array;
}
@end
