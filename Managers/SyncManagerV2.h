//
//  SyncManagerV2.h
//  mHouseKeeping
//
//  Created by TMS on 4/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GuestInfoModelV2.h"
#import "RoomManagerV2.h"
#import "CheckListManagerV2.h"
#import "CountManagerV2.h"
#import "AmenitiesManagerV2.h"
//#import "LaundryOrderDetailModelV2.h"
//#import "LaundryInstructionsOrderModelV2.h"
#import "LostandFoundDetailModelV2.h"
#import "LostandFoundModelV2.h"
#import "LFImageModelV2.h"
#import "ReassignRoomAssignmentModel.h"
#import "FindManagerV2.h"
#import "MBProgressHUD.h"
#import "OtherActivityManager.h"

@interface SyncManagerV2 : NSObject <MBProgressHUDDelegate>{
    UIButton *buttonCancel;
}
//set static count total sync operations
+(void) setMaxSyncOperations:(int)maxValue;

//set Next Step Of Sync
+(void) setNextStepOperations:(float)nextValue;

//get static count total sync operations
+(int)getMaxSyncOperations;

//get Next Step Of Sync
+(float)getNextStepOperations;

#pragma mark - === GuestInfo ===
#pragma mark
-(BOOL) postGuestInfo:(GuestInfoModelV2 *) model WithRoomAssignID:(NSInteger) raID;
-(BOOL) postAllGuestInfoWithUserId:(NSInteger) userId AndPercentView:(MBProgressHUD*)percentView;

#pragma mark - === RoomAssignment ===
#pragma mark
-(BOOL) getRoomAssignmentWithUserID:(NSInteger) userID AndLastModified:(NSString *) strLastModified AndPercentView:(MBProgressHUD*)percentView;
-(int) postRoomAssignmentWithUserID:(NSInteger) userID AndRaModel:(RoomAssignmentModelV2 *) model;
-(BOOL) postRoomCleaningStatusByUserId:(NSInteger) userId withRoomAssignmentModel:(RoomAssignmentModelV2 *) roomAssignmentModel;
-(int) postRoomInspectionWithUserID:(NSInteger) userID AndRaModel:(RoomAssignmentModelV2 *) model;
-(BOOL) postAllRoomAssignmentWithUserId:(NSInteger) userId AndPercentView:(MBProgressHUD*)percentView;
-(BOOL)postAllUnpostRoomRecordHistoryWithUserId:(NSInteger)userId AndPercentView:(MBProgressHUD *)percentView;
-(BOOL) postRoomRemarkWithUserID:(NSInteger) userID;
-(int) postReAssignRoomWithReAssignModel:(ReassignRoomAssignmentModel *)reModel;
-(BOOL) postAllReAssignRoomWithUserId:(NSInteger) userId AndPercentView:(MBProgressHUD*)percentView;

#pragma mark - === Hotel info ===
#pragma mark
//- (BOOL)getHotelInfoOfUserID:(NSNumber *)userID andHotel :(NSNumber *)hotelID andlstModifier:(NSString *)lastModifier;

#pragma mark - === CheckList ===
#pragma mark
-(BOOL)getAllCheckListFormWithUserId:(NSInteger)userId AndHotelId:(NSInteger)hotelId;

//Remove for change WS post array checklist items
//-(BOOL)getAllCheckListCategoriesWithUserId:(NSInteger)userId AndHotelId:(NSInteger)hotelId;
-(BOOL)getCheckListCategoriesWithUserId:(NSInteger)userId AndHotelId:(NSInteger)hotelId AndChecklistId:(int)checklistId;

//Remove for change WS
//-(BOOL)getAllCheckListItemWithUserId:(NSInteger)userId AndHotelId:(NSInteger)hotelId;

-(BOOL)getCheckListItemsWithUserId:(NSInteger)userId AndHotelId:(NSInteger)hotelId AndChecklistId:(int)checklistId;

-(BOOL) getCheckListRoomTypeWithUserId:(NSInteger) userId HotelId:(NSInteger) hotelId AndArrayRoomModel:(NSMutableArray *) arrayRoom AndArrayChkList:(NSMutableArray *)arrayChkList;
-(NSInteger) getCheckListItemScoreForMaidWithUserId:(NSInteger) userId DutyAssignId:(NSInteger) roomAssignId AndLastModified:(NSString*) strLastModified;

//Remove for change WS post array checklist items
//-(BOOL)postCheckListItemWithUserId:(NSInteger)userId RoomAssignId:(NSInteger)roomAssignId AndChkListItem:(CheckListDetailContentModelV2 *)model;

-(BOOL)postCheckListItemsWithUserId:(NSInteger)userId RoomAssignId:(NSInteger)roomAssignId AndChkListItems:(NSMutableArray *)checklistItems;

-(BOOL) postAllChecklistItemScoreWithUserId:(NSInteger) userId AndPercentView:(MBProgressHUD*)percentView;

#pragma mark - === Count WS for Linen ===
#pragma mark
-(BOOL)getLinenCategoryListWithUserId:(NSInteger)userId AndHotelId:(NSInteger)hotelId;
-(BOOL) getLinenItemListWithUserId:(NSInteger)userId AndHotelId:(NSInteger)hotelId;
-(BOOL)postLinenItemWithUserId:(NSInteger)userId roomAssignId:(NSInteger)roomAssignId AndLinenOrderModel:(CountOrderDetailsModelV2 *)model;
-(BOOL) postAllLinenItemWithUserId:(NSInteger) userId AndPercentView:(MBProgressHUD*)percentView;
-(NSArray *) postAllLinenItemsWithUserId:(NSInteger)userId roomAssignId:(NSInteger)roomAssignId roomNumber:(NSString*)roomNumber AndLinenOrderModelList:(NSArray *) list;
-(NSArray *) postAllLinenItemsWithUserId:(NSInteger)userId roomassignId:(int)roomAssignId roomNumber:(NSString*)roomNumber AndLinenOrderModelList:(NSArray *)list;

#pragma mark - === Count WS for Minibar ===
#pragma mark
-(BOOL) getMinibarCategoryListWithUserId:(NSInteger)userId AndHotelId:(NSInteger)hotelId;
-(BOOL) getMinibarItemListWithUserId:(NSInteger)userId AndHotelId:(NSInteger)hotelId;
-(BOOL)postMinibarItemWithUserId:(NSInteger)userId roomAssignId:(NSInteger)roomAssignId AndMinibarOrderModel:(CountOrderDetailsModelV2 *)model WithOrderId:(NSInteger)orderId;

-(NSInteger) postMinibarOrderWithUserId:(NSInteger) userId roomAssignId:(NSInteger) roomAssignId totalQuantity:(NSInteger) totalQuantity serviceCharge:(NSInteger) serviceCharge AndSubTotal:(NSInteger) subTotal WithDate:(NSString*) date;
//--------------------------------update code---------------------------------------
-(NSInteger) postMinibarOrderWithUserId:(NSInteger) userId roomAssignId:(NSInteger) roomAssignId totalQuantity:(NSInteger) totalQuantity serviceCharge:(NSInteger) serviceCharge AndSubTotal:(NSInteger) subTotal WithDate:(NSString*) date WithRoomNo:(NSString *) roomNo;
//--------------------------------update code---------------------------------------

-(BOOL) postAllMinibarItemWithUserId:(NSInteger) userId AndPercentView:(MBProgressHUD*)percentView;
-(NSArray *) postAllMinibarItemsWithUserId:(NSInteger)userId roomAssignId:(NSInteger)roomAssignId AndMinibarOrderModelList:(NSArray*)list WithOrderId:(NSInteger)orderId roomNumber:(NSString*)roomNumber;
-(NSArray *) postAllMinibarItemsWithUserId:(NSInteger)userId roomAssignId:(NSInteger)roomAssignId AndMinibarOrderModelList:(NSArray *)list WithOrderId:(NSInteger)orderId roomNumber:(NSString*)roomNumber isEnablePostingHistory:(BOOL)isEnablePostingHistory;
#pragma mark - === Count WS for Amenities ===
#pragma mark
//-(BOOL) getRoomTypeListWithHotelId:(NSInteger) hotelId;
-(BOOL) getAmenitiesCategoryListWithUserId:(NSInteger) userId AndHotelId:(NSInteger) hotelId;
-(BOOL) getAmenitiesItemListWithUserId:(NSInteger) userId AndHotelId:(NSInteger) hotelId;
//-(BOOL) postAmenitiesItemWithUserId:(NSInteger) userId RoomAssignId:(NSInteger) roomAssignId AndAmenitiesItem:(AmenitiesOrderDetailsModelV2 *) model WithDate:(NSString *) date;
-(BOOL) postAllAmenitiesItemWithUserId:(NSInteger) userId AndPercentView:(MBProgressHUD*) percentView;

/*
#pragma mark - === Count WS for Laundry ===
#pragma mark
-(BOOL)getAllLaundryServiceByUserID:(NSNumber *)userId atLocation:(NSNumber*)hotelId andDate:(NSString*)lastModified;
-(BOOL)getAllLaundryCategoryByUserId:(NSNumber *)userID atHotel:(NSNumber *)userHotelID andlastDate: (NSString *)lastModified;
-(BOOL)getAllLaundryItemByUserId :(NSNumber *)userID andmodifiedDate:(NSString *)modifiedDate;
-(BOOL)getAllLaundryItemPriceByUserId : (NSNumber *)userID andmodifiedDate:(NSString *)modifiedDate;
-(int)postLaundryOrder:(LaudryOrderModelV2*)lr_Order_Model andsubQuantity:(int)subQuantity andServiceID:(int)serviceID andHotel: (int)HotelID andsubTotal:(CGFloat) subTotal;
-(BOOL)postLaundryItem:(LaundryOrderDetailModelV2*)lr_Order_Detail_Model andLaundryOrder: (int)laundry_Order_Id anduser:(int)userId andTransactionTime:(NSString *)transaction_time;
-(BOOL) postAllLaundryItemWithUserId:(NSInteger) userId AndPercentView:(MBProgressHUD*)percentView;
*/

#pragma mark - === Count WS for Laundry Intruction ===
#pragma mark
- (eHousekeepingService_PostPhysicalCheck*) postPhysicalCheckWithUserId:(NSInteger) userId andRoomNo:(NSString*)roomNo andHotelID:(NSInteger) hotelId andStatusId:(NSInteger) statusId andCleaningStatusId:(NSInteger)cleaningStatusId;

/*
#pragma mark - === Count WS for Laundry Intruction ===
#pragma mark
-(BOOL)getAllLaundryInstructionByUserId : (NSNumber *)userID andmodifiedDate:(NSString *)modifiedDate;
-(int)postLaundryInstructionOrder:(LaundryInstructionsOrderModelV2*)lr_Instruction_Order_Model andLaundryOrder: (int)laundry_Order_Id anduser:(int)userId;
-(BOOL) postAllLaundryInstructionOrderWithUserId:(NSInteger) userId AndPercentView:(MBProgressHUD*)percentView;
*/

#pragma mark - === Calculate Number Sync Record ===
#pragma mark
-(void) calculateRecordMustSync;

#pragma mark - === Lost and Found ===
#pragma mark
#pragma mark  Get L&F Category Data from WS
//-(void)getLaFCategoryFromWS:(NSInteger) userId andLastModifier:(NSString *) lastModifier;

#pragma mark - Get L&F Item Data from WS
//-(void)getLaFItemFromWS:(NSInteger) userId andLastModifier:(NSString *) lastModifier;

#pragma mark - Get L&F Color Data from WS
-(void)getLaFColorFromWS:(NSInteger)userId andLastModifier:(NSString *)lastModifier;

#pragma mark- Post L&F Data from WS
-(BOOL)postLostandFound:(LostandFoundDetailModelV2 *) lfDetail;

#pragma mark - Post L&F Photo from WS
-(BOOL)postLaFPhoto:(LostandFoundDetailModelV2 *) lafDetail andPhoto:(LFImageModelV2 *) lfPhoto;

-(BOOL) postAllLostAndFoundWithUserId:(NSInteger) userId AndPercentView:(MBProgressHUD*)percentView;

#pragma mark - === Update Must Post Records ===
#pragma mark
-(void) updateMustPostRecords;


#pragma mark - ===  Get Engineering Category  ===
//-(BOOL) getDataEngineeringCategoryWithUserIDFromWS:(NSInteger ) userID AndLastModified:(NSString *) lastModified;

#pragma mark - ===  Get Engineering Item  ===
-(BOOL) getDataEngineeringItemWithUserIDFromWS:(NSInteger) userID AndHotelID:(NSInteger) hotelID AndLastModified:(NSString *) lastModified AndPercentView:(MBProgressHUD*)percentView;
#pragma mark - ===  Post Engineering Case  ===
-(BOOL) postEngineeringCaseWithUserID:(NSInteger) userID AndPercentView:(MBProgressHUD*)percentView;

-(BOOL)getRoomRecordDetailListWithUserFromWS:(NSInteger)userId andlastModified:(NSString*)lastModidfied;

#pragma mark - ===  Get Floor List Room ===
#pragma mark
-(BOOL)getFloorListFromWSWithFromWS:(NSInteger)hotelID andLastModified:(NSString*)lastModified AndPercentView:(MBProgressHUD*)percentView;

#pragma mark - ===  Get Zone List Room ===
-(BOOL)getZoneRoomListWithUserFromWS:(NSInteger)hotelID andlastModified:(NSString*)lastModidfied AndPercentView:(MBProgressHUD*)percentView;

//-(BOOL)getUnAssignRoomListWithUserFromWS:(NSInteger)hotelID andlastModified:(NSString*)lastModidfied;

#pragma mark - ===  Post Unassign Room ===
-(BOOL)postAssignRoom:(NSInteger)userID AndPercentView:(MBProgressHUD*)percentView;
-(int)postAssignRoomWithAssignModel:(AssignRoomModelV2 *)assignRoom;

#pragma mark - === Update Must Post Records ===
#pragma mark
-(void) updateMustPostRecords;

-(BOOL)postRoomRoomRecordDetail:(NSInteger)userID;

#pragma mark - === Sync manager ===
#pragma mark
//- (BOOL) update;
- (BOOL) update:(BOOL)isManual AndPercentView:(MBProgressHUD *)percentView;
- (BOOL) post:(BOOL)isManual AndPercentView:(MBProgressHUD *)percentView;
- (BOOL) get:(BOOL)isManual AndPercentView:(MBProgressHUD *)percentView;
//- (BOOL) getFindRoomAssignmentWithUserID:(NSInteger)userID AndRoomStatusID:(NSInteger)roomStausID AndLastModified:(NSString *)strLastModified;
#pragma PostRoomCleaningStatus

-(BOOL) postRoomCleaningtoServerWithUSerID:(NSInteger)userID AndRoomAssigmentModel:(RoomAssignmentModelV2 *)model;

//---------add----
#pragma mark - Log WS
- (BOOL) postWSLogForPhysicalCheck:(NSInteger)userId Message:(NSString *)messageValue MessageType:(NSInteger)type;
- (BOOL) postWSLogCountService:(NSInteger)userId Message:(NSString *)messageValue MessageType:(NSInteger)type;
//- (BOOL) postWSLogLaundry:(NSInteger)userId Message:(NSString *)messageValue MessageType:(NSInteger)type;
- (BOOL) postWSLogAmenities:(NSInteger)userId Message:(NSString *)messageValue MessageType:(NSInteger)type;
- (BOOL) postWSLogRoomAsgnMent:(NSInteger)userId Message:(NSString *)messageValue MessageType:(NSInteger)type;
- (BOOL) postWSLogCheckListDetail:(NSInteger)userId Message:(NSString *)messageValue MessageType:(NSInteger)type;

#pragma mark - Room Type Inventory WS
- (BOOL)getRoomTypeInventoryWSByServiceType:(NSInteger)serviceType userId:(NSInteger)userId hotelId:(NSInteger)hotelId;
@end
