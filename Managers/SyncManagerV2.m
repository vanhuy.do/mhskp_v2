//
//  SyncManagerV2.m
//  mHouseKeeping
//
//  Created by TMS on 4/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SyncManagerV2.h"
#import "GuestInfoModelV2.h"
#import "NetworkCheck.h"
#import "AdHocMessageManagerV2.h"
//#import "LaundryManagerV2.h"
//#import "HomeViewV2.h"
#import "LostandFoundManagerV2.h"
#import "EngineerCaseViewManagerV2.h"
#import "FindManagerV2.h"
#import "PhysicalCheckManagerV2.h"
#import "MBProgressHUD.h"
#import "SettingViewV2.h"
#import "SettingAdapterV2.h"
#import "ehkDefinesV2.h"
#import "AmenitiesManagerV2.h"
#import "RoomManagerV2.h"
#import "CheckListManagerV2.h"
#import "LogFileManager.h"
#import "PanicManager.h"
#import "HomeViewV2.h"
#import "DeviceManager.h"
#import "CustomAlertViewV2.h"

@implementation SyncManagerV2
//- (BOOL)getFindRoomAssignmentWithUserID:(NSInteger)userID AndRoomStatusID:(NSInteger)roomStausID AndLastModified:(NSString *)strLastModified{
//    
//}
//- (void)getLaFCategoryFromWS:(NSInteger)userId andLastModifier:(NSString *)lastModifier{
//    
//}
//- (void)getLaFItemFromWS:(NSInteger)userId andLastModifier:(NSString *)lastModifier{
//    
//}
//Apply for count percentage operations when syncing
//Not important, it's only for view progress bar
static float nextStepProgress = 0 ;
static int maxSyncOperations = 0;

//set static count total sync operations
+(void) setMaxSyncOperations:(int)maxValue
{
    maxSyncOperations = maxValue;
}

//get Next Step Of Sync
+(void) setNextStepOperations:(float)nextValue
{
    nextStepProgress = nextValue;
}

//get static count total sync operations
+(int) getMaxSyncOperations
{
    return maxSyncOperations;
}

//get Next Step Of Sync
+(float) getNextStepOperations
{
    return nextStepProgress;
}

NSString *LOADING_POST_CHECKLIST = @"Posting Checklist";
//NSString *LOADING_POST_LAUNDRY = @"Posting Laundry";
NSString *LOADING_POST_MINIBAR = @"Posting Minibar";
NSString *LOADING_POST_INSTRUCTION = @"Posting Instruction";
NSString *LOADING_POST_LINEN = @"Posting Linen";
NSString *LOADING_POST_AMENITIES = @"Posting Amenities";
NSString *LOADING_POST_ENGINEERING = @"Posting Engineering";
NSString *LOADING_POST_LOSTANDFOUND = @"Posting L&F";
NSString *LOADING_POST_REASSIGN = @"Posting Re-assign";
NSString *LOADING_POST_ASSIGN = @"Posting Assign";
NSString *LOADING_POST_GUEST_INFO = @"Posting Guest Info";
NSString *LOADING_POST_REMARK = @"Posting Remark Info";
NSString *LOADING_POST_ASSIGMENT = @"Posting Assignment";

-(BOOL)postGuestInfo:(GuestInfoModelV2 *)model WithRoomAssignID:(NSInteger)raID {
    //check network
    if([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp] == NO) return NO;
    
    //check if model have changed ==> post
    if (model.guestPostStatus == POST_STATUS_SAVED_UNPOSTED) {
        BOOL result = NO;
        result = [[RoomManagerV2 sharedRoomManager] postGuestInfo:[[UserManagerV2 sharedUserManager] currentUser].userId WithRoomAssignID:raID];
        return result;
    } else {
        return YES;
    }
    return NO;
}
- (BOOL)postAllRemarkRoomUserId:(NSInteger)userId AndPercentView:(MBProgressHUD *)percentView{
    NSMutableArray *ramodels = [[RoomManagerV2 sharedRoomManager] loadAllRoomRemarkUnPost];
    for (RoomRemarkModelV2 *ramodel in ramodels) {
        //break syncing
        if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL)
        {
            break;
        }
        int status = [[RoomManagerV2 sharedRoomManager] updateRoomRemarksByUserId:[UserManagerV2 sharedUserManager].currentUser.userId hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId remarkType:RemarkType_RoomRemark roomNumber:ramodel.rm_roomassignment_roomId remarkValue:ramodel.rm_content];
        int status1 = [[RoomManagerV2 sharedRoomManager] updateRoomRemarksByUserId:[UserManagerV2 sharedUserManager].currentUser.userId hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId remarkType:RemarkType_PhyshicalCheck roomNumber:ramodel.rm_roomassignment_roomId remarkValue:ramodel.rm_physical_content];
        if (status == RESPONSE_STATUS_OK && status1 == RESPONSE_STATUS_OK) {
            ramodel.rm_PostStatus = 1;
            [[RoomManagerV2 sharedRoomManager] updateRoomRemarkData:ramodel];
        }
        
    }
    
    return YES;
}
-(BOOL)postAllGuestInfoWithUserId:(NSInteger)userId AndPercentView:(MBProgressHUD *)percentView{
    BOOL status = YES;
    BOOL tempStatus = NO;
    
    //load all room assignments
    NSMutableArray *ramodels = [[RoomManagerV2 sharedRoomManager] loadAllRoomAssignmentsByUserId:userId];
    float currentPercent = 0;
    float stepPercent = [self caculateStepFromCountingObject:(int)[ramodels count]];
    [self updatePercentView:percentView value:currentPercent description:LOADING_POST_GUEST_INFO];
    for (RoomAssignmentModelV2 *ramodel in ramodels) {
        //break syncing
        if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL)
        {
            break;
        }
        //load guest info record
        GuestInfoModelV2 *gmodel = [[GuestInfoModelV2 alloc] init];
        gmodel.guestId = ramodel.roomAssignment_RoomId;
        [[RoomManagerV2 sharedRoomManager] loadGuestInfo:gmodel];
        
        //post a guest info record
        tempStatus = [self postGuestInfo:gmodel WithRoomAssignID:ramodel.roomAssignment_Id];
        
        //assign status
        if (tempStatus == NO) {
            status = NO;
        }
        currentPercent += stepPercent;
        [self updatePercentView:percentView value:currentPercent description:LOADING_POST_GUEST_INFO];
    }
    
    return status;
}

#pragma mark - === Room Assignment ===
#pragma mark

-(BOOL) getFindRoomAssignmentWithUserID:(NSInteger)userID AndHotelId:(NSString*)hotelId AndRoomStatusID:(NSInteger)roomStausID AndLastModified:(NSString *)strLastModified{
    
    BOOL result = YES;
    BOOL tempResult = NO;
    if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        //post all ra if needed first after that get back
        RoomAssignmentModelV2 *ramodel = [[RoomAssignmentModelV2 alloc] init];
        ramodel.roomAssignment_UserId = (int)userID;
        NSMutableArray *ramodels = [[RoomManagerV2 sharedRoomManager] loadAllRoomAssignmentsByUserID:ramodel];
        
        for (RoomAssignmentModelV2 *model in ramodels) {
            
            if ([UserManagerV2 isSupervisor] == YES) {
                int responseCode = [self postRoomInspectionWithUserID:userID AndRaModel:model];
                if(responseCode == RESPONSE_STATUS_OK) {
                    result = YES;
                }
            } else {
                int responseCode = [self postRoomAssignmentWithUserID:userID AndRaModel:model];
                if(responseCode == RESPONSE_STATUS_OK || responseCode == RESPONSE_STATUS_NEW_RECORD_ADDED){
                    tempResult = YES;
                    result = YES;
                } else {
                    tempResult = NO;
                    result = NO;
                }
            }
        }
        
        //delete old room assignment here
        [[RoomManagerV2 sharedRoomManager] deleteOldRoomAssignmentsWithUserId:userID];
        
        //get Room Detail
        tempResult = [[RoomManagerV2 sharedRoomManager] GetFindRoomAssignmentListWSByUserID:userID AndRoomstatus:roomStausID AndFilterType:1 AndLastModified:strLastModified AndHotelID:hotelId AndPercentView:nil];
        if (tempResult == NO) {
            result = NO;
        }
    }
    return result;
    
}

-(BOOL) postRoom:(NSInteger)userID
{
    BOOL result = YES;
    int tempResult = RESPONSE_STATUS_OK;
    if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        //post all ra if needed first after that get back
        RoomAssignmentModelV2 *ramodel = [[RoomAssignmentModelV2 alloc] init];
        ramodel.roomAssignment_UserId = (int)userID;
        NSMutableArray *ramodels = [[RoomManagerV2 sharedRoomManager] loadAllRoomAssignmentsByUserID:ramodel];
        
        for (RoomAssignmentModelV2 *model in ramodels) {
            //break syncing
            if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL)
            {
                break;
            }
            if ([UserManagerV2 isSupervisor] == YES) {
                tempResult = [self postRoomInspectionWithUserID:userID AndRaModel:model];
                //                NSLog(@"tempResult postRoomInspectionWithUserID %d", tempResult);
            } else {
                tempResult = [self postRoomAssignmentWithUserID:userID AndRaModel:model];
                //                NSLog(@"tempResult postRoomAssignmentWithUserID %d", tempResult);
            }
            
            if (tempResult != RESPONSE_STATUS_OK && tempResult != RESPONSE_STATUS_NEW_RECORD_ADDED) {
                result = NO;
            }
        }
    }
    return result;
}

-(BOOL) getRoomAssignmentWithUserID:(NSInteger)userID AndLastModified:(NSString *)strLastModified AndPercentView:(MBProgressHUD *)percentView
{
    BOOL result = YES;
    BOOL tempResult = NO;
    
    if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        //        //post all ra if needed first after that get back
        //        RoomAssignmentModelV2 *ramodel = [[RoomAssignmentModelV2 alloc] init];
        //        ramodel.roomAssignment_UserId = userID;
        //        NSMutableArray *ramodels = [[RoomManagerV2 sharedRoomManager] loadAllRoomAssignmentsByUserID:ramodel];
        //
        //        for (RoomAssignmentModelV2 *model in ramodels) {
        //
        //            if ([UserManagerV2 isSupervisor] == YES) {
        //                tempResult = [self postRoomInspectionWithUserID:userID AndRaModel:model];
        ////                NSLog(@"tempResult postRoomInspectionWithUserID %d", tempResult);
        //            } else {
        //                tempResult = [self postRoomAssignmentWithUserID:userID AndRaModel:model];
        ////                NSLog(@"tempResult postRoomAssignmentWithUserID %d", tempResult);
        //            }
        //
        //            if (tempResult == NO) {
        //                result = NO;
        //            }
        //        }
        
        if([LogFileManager isLogConsole])
        {
            NSLog(@"%@",strLastModified);
        }
        
        strLastModified = nil; // An Rework
        
        //get Room Detail
        tempResult = [[RoomManagerV2 sharedRoomManager] getRoomAssignmentWSByUserID:userID AndIsSupervisor:[UserManagerV2 isSupervisor] AndLastModified:strLastModified AndPercentView:percentView];
        if([LogFileManager isLogConsole])
        {
            NSLog(@"%@",strLastModified);
            NSLog(@"tempResult %d", tempResult);
        }
        if (tempResult == NO) {
            result = NO;
            if([LogFileManager isLogConsole])
            {
                NSLog(@"result == no");
            }
        }
    }
    
    return result;
    
}

-(int) postRoomAssignmentWithRoomRecordHistory:(RoomRecordHistory*) roomRecordHistory
{
    int result = RESPONSE_STATUS_OK;
    if (![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        return RESPONSE_STATUS_NO_NETWORK;
    }
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole]) {
        [binding setLogXMLInOut:YES];
    }
    eHousekeepingService_UpdateRoomAssignment *request = [[eHousekeepingService_UpdateRoomAssignment alloc] init];
    
    [request setIntUsrID:[NSNumber numberWithInt:roomRecordHistory.rrec_User_Id]];
    [request setIntRoomAssignID:[NSNumber numberWithInt:roomRecordHistory.rrec_room_assignment_id]];
    [request setIntRoomStatusID:[NSNumber numberWithInt:roomRecordHistory.rrec_room_status_id]];
    [request setIntCleaningStatusID:[NSNumber numberWithInt:roomRecordHistory.rrec_cleaning_status_id]];
    [request setStrCleanStartTm:roomRecordHistory.rrec_Cleaning_Start_Time];
    [request setStrPauseTime:roomRecordHistory.rrec_cleaning_pause_time];
    [request setStrCleanEndTm:roomRecordHistory.rrec_Cleaning_End_Time];
    [request setRaRemark:roomRecordHistory.rrec_Remark];
    [request setIntTotalCleaningTime:[NSString stringWithFormat:@"%d", roomRecordHistory.rrec_Total_Cleaning_Time]];
    
    if ([request.intCleaningStatusID integerValue] == ENUM_CLEANING_STATUS_START) {
        NSLog(@"Room Cleaning: Start");
    } else if([request.intCleaningStatusID integerValue] == ENUM_CLEANING_STATUS_PAUSE) {
        NSLog(@"Room Cleaning: Stop");
    } else if([request.intCleaningStatusID integerValue] == ENUM_CLEANING_STATUS_COMPLETED) {
        NSLog(@"Room Cleaning: Complete");
    }
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding UpdateRoomAssignmentUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    
    if(responseBodyParts == nil) {
        return NO;
    }
    
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_UpdateRoomAssignmentResponse class]]) {
            eHousekeepingService_UpdateRoomAssignmentResponse *dataBody = (eHousekeepingService_UpdateRoomAssignmentResponse *)bodyPart;
            if (dataBody != nil) {
                
                result = [dataBody.UpdateRoomAssignmentResult.respCode intValue];
            }
        }
    }
    
    return result;
}

-(int)postRoomAssignmentWithUserID:(NSInteger)userID AndRaModel:(RoomAssignmentModelV2 *)model {
    //check network
    if(isDemoMode){
        return RESPONSE_STATUS_OK;
    }
    if([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp] == NO) return NO;
    int responseCode = RESPONSE_STATUS_NO_NETWORK;
    
    if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        if (model.roomAssignment_PostStatus == POST_STATUS_SAVED_UNPOSTED) {
            
            if(model.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_SERVICE_LATER){
                // get roomRecord iD:
                RoomRecordModelV2 *roomRecordV2 = [[RoomRecordModelV2 alloc] init];
                roomRecordV2.rrec_User_Id = [UserManagerV2 sharedUserManager].currentUser.userId;
                roomRecordV2.rrec_room_assignment_id = model.roomAssignment_Id;
                [[RoomManagerV2 sharedRoomManager] loadRoomRecordDataByRoomIdAndUserId:roomRecordV2];
                
                // load service later :
                
                RoomServiceLaterModelV2 *roomServiceLater = [[RoomServiceLaterModelV2 alloc] init];
                roomServiceLater.rsl_RecordId = roomRecordV2.rrec_Id;
                [[RoomManagerV2 sharedRoomManager] loadRoomServiceLaterDataByRecordId:roomServiceLater];
                
                AssignRoomModelV2 *assignModel = [[AssignRoomModelV2 alloc] init];
                assignModel.assignRoom_assign_id = model.roomAssignment_Id;
                assignModel.assignRoom_HouseKeeper_Id = model.roomAssignment_UserId;
                assignModel.assignRoom_Assign_Date = roomServiceLater.rsl_Time;
                assignModel.assignRoom_Remark = roomRecordV2.rrec_Remark;
                
                responseCode = [[UnassignManagerV2 sharedUnassignManager] postAssignRoom:assignModel];
                if(responseCode == RESPONSE_STATUS_NEW_RECORD_ADDED || responseCode == RESPONSE_STATUS_OK) {
                    //post ra
                    responseCode = [[RoomManagerV2 sharedRoomManager] postRoomAssignmentWSByUserID:userID AndRaModel:model];
                }
                
                [[RoomManagerV2 sharedRoomManager] deleteRoomServiceLaterData:roomServiceLater];
                
            } else { //Normal way to post Room Assignment
                
                responseCode = [[RoomManagerV2 sharedRoomManager] postRoomAssignmentWSByUserID:userID AndRaModel:model];
            }
            
            if (responseCode == RESPONSE_STATUS_NEW_RECORD_ADDED || responseCode == RESPONSE_STATUS_OK) {
                
                model.roomAssignment_PostStatus = (int)POST_STATUS_POSTED;
                [[RoomManagerV2 sharedRoomManager] update:model];
            }
        }
    }
    
    // nguyenducquang : remove Remark
    //    temp = [self postRoomRemarkWithUserID:userID];
    //    if (temp == NO) {
    //        result = NO;
    //    }
    
    return responseCode;
}

-(BOOL) postRoomCleaningStatusByUserId:(NSInteger) userId withRoomAssignmentModel:(RoomAssignmentModelV2 *) roomAssignmentModel
{
    if([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp] == NO)
        return NO;
    
    BOOL result = YES;
    BOOL temp = NO;
    
    if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        if (roomAssignmentModel.roomAssignment_PostStatus == POST_STATUS_SAVED_UNPOSTED) {
            
            temp = [[RoomManagerV2 sharedRoomManager] updateCleaningStatusWSByUserId:userId withRoomAssignmentModel:roomAssignmentModel];
            if (temp == YES) {
                roomAssignmentModel.roomAssignment_PostStatus = (int)POST_STATUS_POSTED;
                [[RoomManagerV2 sharedRoomManager] update:roomAssignmentModel];
            } else
                result = NO;
        }
    }
    
    return result;
}

-(int)postRoomInspectionWithUserID:(NSInteger)userID AndRaModel:(RoomAssignmentModelV2 *)model {
    //check network
    
    int result = RESPONSE_STATUS_OK;
    if(isDemoMode)
    {
        int temp = RESPONSE_STATUS_OK;
        
        if (model.roomAssignment_PostStatus == POST_STATUS_SAVED_UNPOSTED) {
            //post ra
            temp = [[RoomManagerV2 sharedRoomManager] postRoomInspectionWSByUserID:userID AndRaModel:model WithStringRemark:model.roomAssignmentChecklistRemark];
            if (temp == RESPONSE_STATUS_OK || temp == RESPONSE_STATUS_NEW_RECORD_ADDED) {
                model.roomAssignment_PostStatus = (int)POST_STATUS_POSTED;
                [[RoomManagerV2 sharedRoomManager] update:model];
            } else {
                result = temp;
            }
        }
    }
    else{
        if([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp] == NO) return NO;
        
        int temp = RESPONSE_STATUS_OK;
        if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
            if (model.roomAssignment_PostStatus == POST_STATUS_SAVED_UNPOSTED) {
                //post ra
                temp = [[RoomManagerV2 sharedRoomManager] postRoomInspectionWSByUserID:userID AndRaModel:model WithStringRemark:model.roomAssignmentChecklistRemark];
                if (temp == RESPONSE_STATUS_OK || temp == RESPONSE_STATUS_NEW_RECORD_ADDED) {
                    model.roomAssignment_PostStatus = (int)POST_STATUS_POSTED;
                    [[RoomManagerV2 sharedRoomManager] update:model];
                } else {
                    result = temp;
                }
            }
        }
    }
    
    return result;
}

-(BOOL)postRoomRemarkWithUserID:(NSInteger)userID {
    //check network
    if([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp] == NO) return NO;
    
    BOOL result = YES;
    BOOL temp = NO;
    if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp] == YES) {
        RoomAssignmentModelV2 *ramodel = [[RoomAssignmentModelV2 alloc] init];
        RoomRecordModelV2 *record = [[RoomRecordModelV2 alloc] init];
        NSMutableArray *remarks = nil;
        
        ramodel.roomAssignment_UserId = (int)userID;
        //get all room assignment models
        NSMutableArray *ramodels = [[RoomManagerV2 sharedRoomManager] loadAllRoomAssignmentsByUserID:ramodel];
        
        record.rrec_User_Id = (int)userID;
        
        //post remark of all room assignment
        for (RoomAssignmentModelV2 *model in ramodels) {
            record.rrec_room_assignment_id = model.roomAssignment_Id;
            
            //load room record
            [[RoomManagerV2 sharedRoomManager] loadRoomRecordDataByRoomIdAndUserId:record];
            
            //load all room remarks
            remarks = [[RoomManagerV2 sharedRoomManager] loadAllRoomRemarkByRecordId:record.rrec_Id];
            
            //post all room remarks
            for (RoomRemarkModelV2 *remark in remarks) {
                if (remark.rm_PostStatus == POST_STATUS_SAVED_UNPOSTED) {
                    temp = [[RoomManagerV2 sharedRoomManager] postRoomRemarkWSWithRoomRemark:remark WithUserId:userID andRoomAssignmentId:model.roomAssignment_Id];
                    if (temp == NO) {
                        result = NO;
                    }
                }
            }
        }
    }
    
    return result;
}

//post reassign room, if success will delete reassign room in local db
-(int) postReAssignRoomWithReAssignModel:(ReassignRoomAssignmentModel *)reModel {

    //check network
    if([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp] == NO) return NO;
    
    int result = 0;
    
    if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp] == YES) {
        
        if (reModel.reassignHousekeeperId <= 0) { //Set room to unassign
            result = [[RoomManagerV2 sharedRoomManager] setUnassignedRoomWSByReassignRoomAssignmentModel:reModel];
        } else {
            result = [[RoomManagerV2 sharedRoomManager] reAssignRoomAssignmentWSByReassignRoomAssignmentModel:reModel];
        }
        
        // DungPhan-20150708-Discussion with FM: Delete reassign room when resp code is 12025, 12026, 12027, 12028, 12029, 12030, 12031 for not repost when sync again
        if (result == RESPONSE_STATUS_ERROR_REASSIGN_DATE_IN_THE_PAST
            || result == RESPONSE_STATUS_ERROR_REASSIGN_OUT_OF_STAFF_WORKSHIFT
            || result == RESPONSE_STATUS_ERROR_REASSIGN_OUT_OF_REMAINED_TIME
            || result == RESPONSE_STATUS_ERROR_ASSIGNMENT_NOT_EXIST
            || result == RESPONSE_STATUS_ERROR_STAFF_NOT_ASSIGNED
            || result == RESPONSE_STATUS_ERROR_NOT_CURRENT_DAY_ROOM_ASSIGNMENT
            || result == RESPONSE_STATUS_ERROR_END_CLEANING_TIME_EXCEED_CURRENT_DAY_ASSIGNMENT)
        {
            [[RoomManagerV2 sharedRoomManager] deleteReassignRoomAssignmentModel:reModel];
        }
    }
    
    //Hao Tran - Remove for unecessary command
    /*
    if (result==YES) {
        [[RoomManagerV2 sharedRoomManager] deleteReassignRoomAssignmentModel:reModel];
    }*/
    return result;
}

-(BOOL)postAllReAssignRoomWithUserId:(NSInteger)userId AndPercentView:(MBProgressHUD *)percentView{
    BOOL status = YES, temp = YES;
    
    //load all reassign model in local db
    NSMutableArray *reassigns = [[RoomManagerV2 sharedRoomManager] loadAllReassignRoomAssignmentModelByUserId:userId];
    
    float currentPercent = 0;
    float stepPercent = [self caculateStepFromCountingObject:(int)[reassigns count]];
    [self updatePercentView:percentView value:currentPercent description:LOADING_POST_REASSIGN];
    for (ReassignRoomAssignmentModel *reassign in reassigns) {
        //break syncing
        if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL)
        {
            break;
        }
        temp = [self postReAssignRoomWithReAssignModel:reassign];
        
        if (temp == NO) {
            status = NO;
        }
        
        currentPercent += stepPercent;
        [self updatePercentView:percentView value:currentPercent description:LOADING_POST_REASSIGN];
    }
    
    return status;
}

-(BOOL)postAllRoomAssignmentWithUserId:(NSInteger)userId AndPercentView:(MBProgressHUD *)percentView{
    BOOL status = YES;
    
    //load all room assignments
    NSMutableArray *ramodels = [[RoomManagerV2 sharedRoomManager] loadAllRoomAssignmentsByUserId:userId];
    
    float currentPercent = 0;
    float stepPercent = [self caculateStepFromCountingObject:(int)[ramodels count]];
    [self updatePercentView:percentView value:currentPercent description:LOADING_POST_ASSIGMENT];
    for (RoomAssignmentModelV2 *ramodel in ramodels) {
        //break syncing
        if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL)
        {
            break;
        }
        
        if ([UserManagerV2 isSupervisor] == YES) {
            //supervisor
            int responseCode = [self postRoomInspectionWithUserID:userId AndRaModel:ramodel];
            
            //assign status
            if (responseCode != RESPONSE_STATUS_OK && responseCode != RESPONSE_STATUS_NEW_RECORD_ADDED) {
                status = NO;
            }
        }
        
        //SG-70548 : Don't use this for RA any more, use "postRoomAssignmentWithRoomRecordHistory"
        /*
        else {
            //maid
            
            int responseCode = [self postRoomAssignmentWithUserID:userId AndRaModel:ramodel];
            if(responseCode == RESPONSE_STATUS_OK || responseCode == RESPONSE_STATUS_NEW_RECORD_ADDED) {
                status = YES;
            } else {
                status = NO;
            }
        }*/
        
        currentPercent += stepPercent;
        [self updatePercentView:percentView value:currentPercent description:LOADING_POST_ASSIGMENT];
    }
    
    return status;
}

-(BOOL)postAllUnpostRoomRecordHistoryWithUserId:(NSInteger)userId AndPercentView:(MBProgressHUD *)percentView{
   
    BOOL status = YES;
    
    //load all room assignments
    NSMutableArray *ramodels = [[RoomManagerV2 sharedRoomManager] loadAllUnpostRoomRecordHistoryByCurrentUser];
    
    float currentPercent = 0;
    float stepPercent = [self caculateStepFromCountingObject:(int)[ramodels count]];
    [self updatePercentView:percentView value:currentPercent description:LOADING_POST_ASSIGMENT];
    for (RoomRecordHistory *ramodel in ramodels) {
        //break syncing
        if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL) {
            break;
        }
        
        int responseCode = [self postRoomAssignmentWithRoomRecordHistory:ramodel];
        if(responseCode == RESPONSE_STATUS_OK || responseCode == RESPONSE_STATUS_NEW_RECORD_ADDED) {
            status = YES;
            
            ramodel.rrec_PostStatus = (int)POST_STATUS_POSTED;
            [[RoomManagerV2 sharedRoomManager] updateRoomRecordHistoryData:ramodel];
        }
        else {
            status = NO;
            
            //Force update room to posted if room is not valid to Server anymore
            if (responseCode == RESPONSE_STATUS_INVALID_ROOM_STATUS
                || responseCode == RESPONSE_STATUS_INVALID_ROOM_CLEANING_STATUS
                || responseCode == RESPONSE_STATUS_INVALID_ROOM_CLEANING_START_TIME
                || responseCode == RESPONSE_STATUS_INVALID_ROOM_CLEANING_END_TIME
                || responseCode == RESPONSE_STATUS_INVALID_ROOMASSIGNMENT_ID)
            {
                ramodel.rrec_PostStatus = (int)POST_STATUS_POSTED;
                [[RoomManagerV2 sharedRoomManager] updateRoomRecordHistoryData:ramodel];
            }
        }
        
        
        currentPercent += stepPercent;
        [self updatePercentView:percentView value:currentPercent description:LOADING_POST_ASSIGMENT];
    }
    
    return status;
}

#pragma mark - === Hotel info ===
#pragma mark
- (BOOL)getHotelInfoOfUserID:(NSNumber *)userID andHotel :(NSNumber *)hotelID andlstModifier:(NSString *)lastModifier AndPercentView:(MBProgressHUD *)percentView{
    if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        [[HotelManagerV2 sharedHotelManager] getHotelInfoOfUserID:userID andHotel:hotelID andlstModifier:lastModifier AndPercentView:percentView];
    }
    return TRUE;
}
#pragma mark - === Hotel info ===
#pragma mark

-(BOOL)getUserListOfUserID:(NSNumber *)userID andHotel :(NSNumber *)hotelID andlstModifier:(NSString *)lastModifier AndPercentView:(MBProgressHUD*)percentView {
    if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        [[AdHocMessageManagerV2 sharedAdHocMessageManagerV2] getUserListWith:[userID intValue] andHotelID:[hotelID intValue] andLastModified:lastModifier AndPercentView:percentView];
    }
    return YES;
}

#pragma mark - === Sync manager ===
#pragma mark
-(BOOL)update:(BOOL)isManual AndPercentView:(MBProgressHUD *)percentView {
    if(isDemoMode)
        return YES;
    if([LogFileManager isLogConsole])
    {
        NSLog(@"======================== START SYNC ==========================");
        NSLog(@">>> POST ALL FIRST");
    }
    maxSyncOperations = 13 + 8; //13:post & 8:get
    
    //For similar with android
    if(isManual)
    {
        if([UserManagerV2 isSupervisor]) {
            maxSyncOperations = 29;
        } else {
            maxSyncOperations = 23;
        }
    }
    else //auto synch
    {
        if([UserManagerV2 isSupervisor]) {
            maxSyncOperations = 15;
        } else {
            maxSyncOperations = 10;
        }
    }
    
    //    if ([UserManagerV2 isSupervisor])
    //    {
    //        maxSyncOperations += 2;
    //    }
    
    nextStepProgress = (float) (1/(float)maxSyncOperations);
    
    bool result = NO;
    
    //create cancel function
    //[percentView setHasCancelButton:YES];
    result = [self post:isManual AndPercentView:percentView];
    
    //Only get for manual sync
    if(isManual) {
        if([MBProgressHUD getCancelStatus] != PRESSED_CANCEL) {
            result = [self get:isManual AndPercentView:percentView];
        }
    }
    
    nextStepProgress = 0;
    maxSyncOperations = 0;
    
    //clear cancel function
    percentView.hasCancelButton = NO;
    
    //update number of must post records in main thread to update number
    [self performSelectorOnMainThread:@selector(updateMustPostRecords) withObject:nil waitUntilDone:NO];
    
    //sync finish
    if([LogFileManager isLogConsole])
    {
        NSLog(@"======================== FINISH SYNC ==========================");
    }
    return result;
}

-(BOOL)post:(BOOL)isManual AndPercentView:(MBProgressHUD *)percentView{
    //Get message for auto sync and manual sync
    HomeViewV2 *homeView = [HomeViewV2 shareHomeView];
    if(homeView){
        //sync message
        BOOL isShowHUD = NO;
        [homeView messageReciever:isShowHUD callFrom:IS_PUSHED_FROM_HOME];
        
        //Check to show pop up messages from WS
        [homeView getMessagePopupFromWS];
    }
    //create cancel function
    [percentView setHasCancelButton:NO];
    
    NSInteger userId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    
    BOOL status = YES, tempStatus = YES;
    
    //button cancel pressed when syncing
    if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL){ return NO;}
    
    //only supervisor post checklist
    if ([UserManagerV2 isSupervisor]){
        //post check lists
        tempStatus = [self postAllChecklistItemScoreWithUserId:userId AndPercentView:percentView];
        if(tempStatus == NO) {status = NO;}
    }
    percentView.progress += nextStepProgress;
    
    //button cancel pressed when syncing
    if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL) { return NO;}
    
    if(ENABLE_ADDITIONAL_JOB){
        [[AdditionalJobManagerV2 sharedAddionalJobManager] postAllAddJobByUserId:(int)userId];
    }
    percentView.progress += nextStepProgress;
    
    //button cancel pressed when syncing
    if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL) { return NO;}
    
    /*
    //post laundry
    if(ENABLE_LAUNDRY){
        tempStatus = [self postAllLaundryItemWithUserId:userId AndPercentView:percentView];
        if(tempStatus == NO) {status = NO;}
    }*/
    
    percentView.progress += nextStepProgress;
    
    //button cancel pressed when syncing
    if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL){return NO;}
    
    /*
    //post laundry instruction
    if(ENABLE_LAUNDRY){
        tempStatus = [self postAllLaundryInstructionOrderWithUserId:userId AndPercentView:percentView];
        if(tempStatus == NO) {status = NO;}
    }*/
    percentView.progress += nextStepProgress;
    
    //button cancel pressed when syncing
    if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL){return NO;}
    
    //post minibar
    tempStatus = [self postAllMinibarItemWithUserId:userId AndPercentView:percentView];
    if(tempStatus == NO) {status = NO;}
    percentView.progress += nextStepProgress;
    
    if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL){return NO;}
    
    //post linen
    tempStatus = [self postAllLinenItemWithUserId:userId AndPercentView:percentView];
    if(tempStatus == NO) {status = NO;}
    percentView.progress += nextStepProgress;
    
    if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL){return NO;}
    
    //post amenities
    if([[AmenitiesManagerV2 sharedAmenitiesManager] countTotalAmenitiesByUserId:(int)userId] > 0) {
        tempStatus = [self postAllAmenitiesItemWithUserId:userId AndPercentView:percentView];
        if(tempStatus == NO) {
            status = NO;
        } else {
            //Delete all posted data
            [[AmenitiesManagerV2 sharedAmenitiesManager] deleteAmenitiesOrderDetailsByUserId:(int)userId];
        }
    }
    percentView.progress += nextStepProgress;
    
    //button cancel pressed when syncing
    if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL){return NO;}
    
    //post engineering case
    tempStatus = [self postEngineeringCaseWithUserID:userId AndPercentView:percentView];
    if(tempStatus == NO) {status = NO;}
    percentView.progress += nextStepProgress;
    
    //button cancel pressed when syncing
    if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL){return NO;}
    
    //post lost and found
    tempStatus = [self postAllLostAndFoundWithUserId:userId AndPercentView:percentView];
    if(tempStatus == NO) {status = NO;}
    percentView.progress += nextStepProgress;
    
    //post reassign
    tempStatus = [self postAllReAssignRoomWithUserId:userId AndPercentView:percentView];
    if(tempStatus == NO) {status = NO;}
    percentView.progress += nextStepProgress;
    
    //button cancel pressed when syncing
    if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL){return NO;}
    
    //post unassign
    tempStatus = [self postAssignRoom:userId AndPercentView:percentView];
    if(tempStatus == NO) {status = NO;}
    percentView.progress += nextStepProgress;
    
    //button cancel pressed when syncing
    if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL){return NO;}
    
    //post guest info
    tempStatus = [self postAllGuestInfoWithUserId:userId AndPercentView:percentView];
    if(tempStatus == NO) {status = NO;}
    percentView.progress += nextStepProgress;
    
    //button cancel pressed when syncing
    if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL){return NO;}
    
    //post room assignment
    if ([UserManagerV2 isSupervisor]) {
        tempStatus = [self postAllRoomAssignmentWithUserId:userId AndPercentView:percentView];
    } else {
        tempStatus = [self postAllUnpostRoomRecordHistoryWithUserId:userId AndPercentView:percentView];
    }
    if(tempStatus == NO) {status = NO;}
    percentView.progress += nextStepProgress;
    
    //button cancel pressed when syncing
    if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL){return NO;}

    // Only post other activity when INI enable
    if (![UserManagerV2 isSupervisor]) {
        if ([[UserManagerV2 sharedUserManager].currentUserAccessRight isEnableOtherActivity]) {
            tempStatus = [[OtherActivityManager sharedOtherActivityManager] postAllOtherActivityByUserId:(int)userId];
        }
    }
    if ([UserManagerV2 isSupervisor]) {
        [self postAllRemarkRoomUserId:userId AndPercentView:percentView];
    }
    if(tempStatus == NO) {status = NO;}
    percentView.progress += nextStepProgress;
    
    //button cancel pressed when syncing
    if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL){return NO;}
    
    
    
    
    //post room
    if(![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp])
        return NO;
    
    tempStatus = [self postRoom:[[[UserManagerV2 sharedUserManager] currentUser] userId]];
    if(tempStatus == NO) {status = NO;}
    percentView.progress += nextStepProgress;
    
    //    tempStatus = [self postRoomRoomRecordDetail:userId];
    //    if (tempStatus == NO) {
    //        status = NO;
    //    }
    
    //TODO - this is belong to GET FUNCTION
    //
    //20140905: Begin Get Room Assignment and count AdditionalJob for both Auto and Manual Sync
    //Get Room Assignment
    percentView.progress += nextStepProgress;
    if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL){ return NO;}
    
    tempStatus = [self getRoomAssignmentWithUserID:[[[UserManagerV2 sharedUserManager] currentUser] userId] AndLastModified:[[HomeViewV2 shareHomeView] getTempLastModifiedRoomAssignment] AndPercentView: percentView];
    //Get Room Type list from WS
    [[RoomManagerV2 sharedRoomManager] getRoomTypeListWSWithHotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId];
    // Only update other activity list when INI enable
    if (![UserManagerV2 isSupervisor]) {
        if ([[UserManagerV2 sharedUserManager].currentUserAccessRight isEnableOtherActivity]) {
            NSInteger buildingId = [[NSUserDefaults standardUserDefaults] integerForKey:[NSString stringWithFormat:@"%@%d", CURRENT_BUILDING_ID, [UserManagerV2 sharedUserManager].currentUser.userId]];
            [[OtherActivityManager sharedOtherActivityManager] loadWSOtherActivityAssignmentByUserId:[UserManagerV2 sharedUserManager].currentUser.userId hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId buildingId:(int)buildingId dateTime:[ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"]];
        }
    }
    
    //TODO - END
    
    if(tempStatus == NO) {status = NO;}
    percentView.progress += nextStepProgress;
    
    //Additional Job
    if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL){ return NO;}
    
    int countPendingJobs = [[AdditionalJobManagerV2 sharedAddionalJobManager] loadWSAddJobCountPendingByUserId:(int)userId hotelId:[[[UserManagerV2 sharedUserManager] currentUser] userHotelsId]];
    if(countPendingJobs >= 0) {[HomeViewV2 shareHomeView].countPendingAddJobs = countPendingJobs;} //Only get new count pending job for success WS
    
    if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL){ return NO;}
    //End Get Room Assignment and count AdditionalJob for both Auto and Manual Sync
    
    return YES;
}

-(BOOL)get:(BOOL)isManual AndPercentView:(MBProgressHUD *)percentView {
    
    BOOL result = YES, returnCode = NO;
    
    if(![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp])
        return NO;
    int userID = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    int hotelID = [[[UserManagerV2 sharedUserManager] currentUser] userHotelsId];
    if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL){ return NO;}
    [[HotelManagerV2 sharedHotelManager] getHotelInfoOfUserID:[NSNumber numberWithInt:userID] andHotel:[NSNumber numberWithInt:hotelID] andlstModifier:nil AndPercentView:percentView];
    percentView.progress += nextStepProgress;
    if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL){ return NO;}
    
//    returnCode = [self getRoomAssignmentWithUserID:[[[UserManagerV2 sharedUserManager] currentUser] userId] AndLastModified:[[HomeViewV2 shareHomeView] getTempLastModifiedRoomAssignment] AndPercentView: percentView];
//    
//    if (returnCode == NO) { result = NO; }
//    percentView.progress += nextStepProgress;
    
    //Get message for auto sync and manual sync
    HomeViewV2 *homeView = [HomeViewV2 shareHomeView];
    if(homeView){
        //sync message
        BOOL isShowHUD = NO;
        [homeView messageReciever:isShowHUD callFrom:IS_PUSHED_FROM_HOME];
        
        //Check to show pop up messages from WS
        [homeView getMessagePopupFromWS];
    }
    
    //Get panic config
    [[PanicManager sharedPanicManager] getPanicConfigByUserId:[UserManagerV2 sharedUserManager].currentUser.userId hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId lastModified:nil];
    
    percentView.progress += nextStepProgress;
    
    //only get for manual sync
    if (isManual)
    {
        if(![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp])
            return NO;
        
        
        //Additional Job
//        if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL){ return NO;}
//        
//        int countPendingJobs = [[AdditionalJobManagerV2 sharedAddionalJobManager] loadWSAddJobCountPendingByUserId:userID hotelId:hotelID];
//        if(countPendingJobs >= 0) {[HomeViewV2 shareHomeView].countPendingAddJobs = countPendingJobs;} //Only get new count pending job for success WS
        
        if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL){ return NO;}
        
        //HaoTran[20130619 /Enhance get room status from WS] - Start
        NSString *lastModifiedRoomStatus = [[RoomManagerV2 sharedRoomManager] getRoomStatusLastModifiedDate];
        returnCode = [[RoomManagerV2 sharedRoomManager] getAllRoomStatusWS:lastModifiedRoomStatus AndPercentView:percentView];
        if (returnCode == NO) { result = NO; }
        
        percentView.progress += nextStepProgress;
        //HaoTran[20130619 /Enhance get room status from WS] - END
        
        if(![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp])
            return NO;
        
        if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL){ return NO; }
        
        //Hao Tran Enhance for get cleaning status
        NSString *cleaningStatusLastModified = [[RoomManagerV2 sharedRoomManager] getCleaningStatusLastModifiedDate];
        returnCode = [[RoomManagerV2 sharedRoomManager] getAllCleaningStatusWS:cleaningStatusLastModified AndPercentView:percentView];
        if (returnCode == NO) { result = NO; }
        percentView.progress += nextStepProgress;
        
        if(![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp])
            return NO;
        
        if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL){return NO;}
        
        /*
        //Enhance With Last Modified data from WS
        //Change lastModifiedDateUser = @"" for get all user list
        NSString *lastModifiedDateUser = [[UserManagerV2 sharedUserManager] getUserLastModifiedDate];
        returnCode = [[AdHocMessageManagerV2 sharedAdHocMessageManagerV2] getUserListWith:userID andHotelID:hotelID andLastModified:lastModifiedDateUser AndPercentView:percentView];
        if (returnCode == NO) { result = NO; }
         */
        percentView.progress += nextStepProgress;
        
        /* Hao Tran Remove - 20130618
         Remove for Enhance because Get Engineering got in Engineering on load
         -----------------------------------------------------------------------
         if([LogFileManager isLogConsole]) {
         log = @">>> GET EngineeringCategory : FAIL";
         }
         
         if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL)
         {
         return NO;
         }
         if ([self getDataEngineeringCategoryWithUserIDFromWS:userID AndLastModified:nil AndPercentView:percentView]) {
         if([LogFileManager isLogConsole]) {
         log = @">>> GET EngineeringCategory : SUC";
         }
         }
         */
        percentView.progress += nextStepProgress;
        
        /* Hao Tran Remove - 20130618
         Remove for Enhance because Get Engineering got in Engineering on load
         -----------------------------------------------------------------------
         if([LogFileManager isLogConsole]) {
         NSLog(@"%@",log);
         }
         
         if(![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp])
         return NO;
         if([LogFileManager isLogConsole]) {
         log = @">>> GET EngineeringItem : FAIL";
         }
         if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL)
         {
         return NO;
         }
         if ([self  getDataEngineeringItemWithUserIDFromWS:userID AndHotelID:hotelID AndLastModified:nil AndPercentView:percentView]) {
         if([LogFileManager isLogConsole]) {
         log = @">>> GET EngineeringItem : SUC";
         }
         }
         */
        
        percentView.progress += nextStepProgress;
        
        if ([UserManagerV2 isSupervisor])
        {
            if(![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp])
                return NO;
            
            if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL){ return NO;}
            
            //Hao Tran Enhance load floor from WS
            NSString *floorLastModified = [[UnassignManagerV2 sharedUnassignManager] getFloorLastModifiedDate];
            returnCode = [self getFloorListFromWSWithFromWS:hotelID andLastModified:floorLastModified AndPercentView:percentView];
            if (returnCode == NO) { result = NO; }
            percentView.progress += nextStepProgress;
            
            if(![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp])
                return NO;
            if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL) { return NO;}
            if([[[UserManagerV2 sharedUserManager] currentUserAccessRight] hasUnassignRoom]) {
                //Hao Tran Enhance for get data from WS
                NSString *zoneLastModified = [[UnassignManagerV2 sharedUnassignManager] getZoneLastModifiedDate];
                returnCode = [self getZoneRoomListWithUserFromWS:hotelID andlastModified:zoneLastModified AndPercentView:percentView];
                if (returnCode == NO) { result = NO; }
                percentView.progress += nextStepProgress;
            }
        }
    }
    
    //    if(![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp])
    //        return NO;
    //    log = @">>> GET RoomRecordDetailList : FAIL";
    //    if ([self getRoomRecordDetailListWithUserFromWS:userID andlastModified:nil]) {
    //        log = @">>> GET RoomRecordDetailList : SUC";
    //    }
    //    NSLog(@"%@",log);
    
    //    if(![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp])
    //        return NO;
    //
    //    [[AdHocMessageManagerV2 sharedAdHocMessageManagerV2] getSubjectListWith:userID andHotelID:hotelID andLastModified:nil AndPercentView:percentView];
    return YES;
}

#pragma mark - CheckList
-(BOOL)getAllCheckListFormWithUserId:(NSInteger)userId AndHotelId:(NSInteger)hotelId {
    BOOL result = YES;
    CheckListManagerV2 *manager = [[CheckListManagerV2 alloc] init];
    
    if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        result = [manager getAllCheckListFormWSWithUserId:userId AndHotelId:hotelId];
    } else {
        result = NO;
    }
    
    return result;
}

//Hao Tran remove for change new WS get checklist
//-(BOOL)getAllCheckListCategoriesWithUserId:(NSInteger)userId AndHotelId:(NSInteger)hotelId {
//    BOOL result = YES;
//    CheckListManagerV2 *manager = [[CheckListManagerV2 alloc] init];
//
//    if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
//        result = [manager getAllCheckListCategoriesWSWithUserId:userId AndHotelId:hotelId];
//    } else {
//        result = NO;
//    }
//
//    return result;
//}

-(BOOL)getCheckListCategoriesWithUserId:(NSInteger)userId AndHotelId:(NSInteger)hotelId AndChecklistId:(int)checklistId
{
    BOOL result = YES;
    CheckListManagerV2 *manager = [[CheckListManagerV2 alloc] init];
    
    if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        result = [manager getCheckListCategoriesWSWithUserId:userId AndHotelId:hotelId AndChecklistId:checklistId];
    } else {
        result = NO;
    }
    
    return result;
}

//Remove for new WS get checklist Items
//-(BOOL)getAllCheckListItemWithUserId:(NSInteger)userId AndHotelId:(NSInteger)hotelId {
//    BOOL result = YES;
//    CheckListManagerV2 *manager = [[CheckListManagerV2 alloc] init];
//
//    if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
//        result = [manager getAllCheckListItemWSWithUserId:userId AndHotelId:hotelId AndChecklistId:];
//    } else {
//        result = NO;
//    }
//
//    return result;
//}

-(BOOL)getCheckListItemsWithUserId:(NSInteger)userId AndHotelId:(NSInteger)hotelId AndChecklistId:(int)checklistId
{
    BOOL result = YES;
    CheckListManagerV2 *manager = [[CheckListManagerV2 alloc] init];
    
    if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        result = [manager getAllCheckListItemWSWithUserId:userId AndHotelId:hotelId AndChecklistId:checklistId];
    } else {
        result = NO;
    }
    
    return result;
}

-(BOOL) getCheckListRoomTypeWithUserId:(NSInteger) userId HotelId:(NSInteger) hotelId AndArrayRoomModel:(NSMutableArray *) arrayRoom AndArrayChkList:(NSMutableArray *)arrayChkList {
    BOOL result = YES;
    CheckListManagerV2 *manager = [[CheckListManagerV2 alloc] init];
    
    if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        result = [manager getCheckListRoomTypeWSWithUserId:userId HotelId:hotelId AndArrayRoomModel:arrayRoom AndArrayChkList:arrayChkList];
    } else {
        result = NO;
    }
    
    return result;
}

-(NSInteger)getCheckListItemScoreForMaidWithUserId:(NSInteger)userId DutyAssignId:(NSInteger)roomAssignId AndLastModified:(NSString *)strLastModified {
    NSInteger result = RESPONSE_STATUS_OK;
    CheckListManagerV2 *manager = [[CheckListManagerV2 alloc] init];
    
    if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        result = [manager getCheckListItemScoreForMaidWSWithUserId:userId DutyAssignId:roomAssignId AndLastModified:strLastModified];
    } else {
        result = RESPONSE_STATUS_NO_NETWORK;
    }
    
    return result;
}

//Hao Tran Remove for change WS
//-(BOOL)postCheckListItemWithUserId:(NSInteger)userId RoomAssignId:(NSInteger)roomAssignId AndChkListItem:(CheckListDetailContentModelV2 *)model {
//    BOOL result = YES;
//    CheckListManagerV2 *manager = [[CheckListManagerV2 alloc] init];
//
//    if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
//        result = [manager postCheckListItemWSWithUserId:userId RoomAssignId:roomAssignId AndChkListItem:model];
//    } else {
//        result = NO;
//    }
//
//    return result;
//}

-(BOOL)postCheckListItemsWithUserId:(NSInteger)userId RoomAssignId:(NSInteger)roomAssignId AndChkListItems:(NSMutableArray *)checklistItems
{
    BOOL result = YES;
    CheckListManagerV2 *manager = [[CheckListManagerV2 alloc] init];
    if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        result = [manager postCheckListItemWSWithUserId:userId RoomAssignId:roomAssignId AndChkListItems:checklistItems];
    } else {
        result = NO;
    }
    
    return result;
}

-(BOOL)postAllChecklistItemScoreWithUserId:(NSInteger)userId AndPercentView:(MBProgressHUD *)percentView {
    BOOL status = YES;
    BOOL tempStatus = NO;
    CheckListManagerV2 *manager = [[CheckListManagerV2 alloc] init];
    
    //load all room assignments
    NSMutableArray *ramodels = [[RoomManagerV2 sharedRoomManager] loadAllRoomAssignmentsByUserId:userId];
    
    float currentPercent = 0;
    float stepPercent = [self caculateStepFromCountingObject:(int)[ramodels count]];
    [self updatePercentView:percentView value:currentPercent description:LOADING_POST_CHECKLIST];
    
    for (RoomAssignmentModelV2 *ramodel in ramodels) {
        //break syncing
        if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL)
        {
            break;
        }
        //load checklist
        CheckListModelV2 *chklist = [[CheckListModelV2 alloc] init];
        chklist.chkListId = ramodel.roomAssignment_Id;
        chklist.chkListUserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
        [manager loadCheckListData:chklist];
        
        //load all checklist form score
        NSMutableArray *arrayChklistFormScore = [manager loadChecklistFormScoreByChkListId:chklist.chkListId AndChkUserId:[[[UserManagerV2 sharedUserManager] currentUser] userId]];
        
        for (ChecklistFormScoreModel *chklistFormScore in arrayChklistFormScore) {
            //break syncing
            if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL)
            {
                break;
            }
            
            //load all checklist item score no post
            NSMutableArray *arrayPostChklistItemScore = [manager loadCheckListItemCoreByChkListFormScoreIdData:chklistFormScore.dfId AndStatusPost:POST_STATUS_SAVED_UNPOSTED];
            NSMutableArray *postItems = [NSMutableArray array];
            
            for (CheckListItemCoreDBModelV2 *chklistItemScore in arrayPostChklistItemScore) {
                //break syncing
                if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL)
                {
                    break;
                }
                
                CheckListDetailContentModelV2 *model = [[CheckListDetailContentModelV2 alloc] init];
                model.chkTypeId = chklistFormScore.dfFormId;
                model.chkDetailContentId = chklistItemScore.chkItemCoreDetailContentId;
                model.itemCoreModel = chklistItemScore;
                
                [postItems addObject:model];
            }
            
            tempStatus = [self postCheckListItemsWithUserId:userId RoomAssignId:ramodel.roomAssignment_Id AndChkListItems:postItems];
            if(tempStatus == NO){
                status = NO;
            }
        }
        
        currentPercent += stepPercent;
        [self updatePercentView:percentView value:currentPercent description:LOADING_POST_CHECKLIST];
        
    }
    
    return status;
}

#pragma mark - Count WS for Linen
-(BOOL)getLinenCategoryListWithUserId:(NSInteger)userId AndHotelId:(NSInteger)hotelId
{
    BOOL result = YES;
    CountManagerV2 *manager = [[CountManagerV2 alloc] init];
    if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        
        result = [manager getLinenCategoryListWSWithUserId:userId AndHotelId:hotelId];
    }
    else
    {
        result = NO;
    }
    return result;
    
}
-(BOOL) getLinenItemListWithUserId:(NSInteger)userId AndHotelId:(NSInteger)hotelId
{
    BOOL result = YES;
    CountManagerV2 *manager = [[CountManagerV2 alloc] init];
    if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        result = [manager getLinenItemListWSWithUserId:userId AndHotelId:hotelId];
    }
    else
    {
        result = NO;
    }
    return result;
    
}

-(BOOL)postLinenItemWithUserId:(NSInteger)userId roomAssignId:(NSInteger)roomAssignId AndLinenOrderModel:(CountOrderDetailsModelV2 *)model
{
    BOOL result = YES;
    CountManagerV2 *manager = [[CountManagerV2 alloc] init];
    if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        
        result = [manager postLinenItemWSWithUserId:userId roomAssignId:roomAssignId AndLinenOrderModel:model];
    }
    else
    {
        result = NO;
    }
    return result;
}

-(NSArray *) postAllLinenItemsWithUserId:(NSInteger)userId roomAssignId:(NSInteger)roomAssignId roomNumber:(NSString*)roomNumber AndLinenOrderModelList:(NSArray *)list
{
    CountManagerV2 *manager = [[CountManagerV2 alloc] init];
    if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        
        return [manager postAllLinenItemsWithUserId:userId roomAssignId:roomAssignId roomNumber:roomNumber AndLinenOrderModelList:list];
    }
    return nil;
}

-(NSArray *) postAllLinenItemsWithUserId:(NSInteger)userId roomassignId:(int)roomAssignId roomNumber:(NSString*)roomNumber AndLinenOrderModelList:(NSArray *)list
{
    CountManagerV2 *manager = [[CountManagerV2 alloc] init];
    if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        return [manager postAllLinenItemsWithUserId:userId roomAssignId:roomAssignId roomNumber:roomNumber AndLinenOrderModelList:list];
    }
    return nil;
}

-(BOOL)postAllLinenItemWithUserId:(NSInteger)userId AndPercentView:(MBProgressHUD *)percentView{
    BOOL status = YES;
    
    //    old WS
    //    BOOL tempStatus = NO;
    
    CountManagerV2 *manager = [[CountManagerV2 alloc] init];
    
    //load all linen order
    //    NSMutableArray *arrayLinen = [manager loadAllCountOrdersByUserId:userId AndServiceId:cLinen];
    NSMutableArray *arrayLinen = [manager loadCountOrdersByUserId:userId AndServiceId:cLinen];
    float currentPercent = 0;
    float stepPercent = [self caculateStepFromCountingObject:(int)[arrayLinen count]];
    [self updatePercentView:percentView value:currentPercent description:LOADING_POST_LINEN];
    for (CountOrdersModelV2 *orderModel in arrayLinen) {
        //break syncing
        if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL)
        {
            break;
        }
        //load all linen order detail by order id
        
        //New WS
        NSArray *arrayPostedLinen = [self postAllLinenItemsWithUserId:userId roomAssignId:orderModel.countRoomAssignId roomNumber:orderModel.countRoomNumber  AndLinenOrderModelList:[manager loadAllCountOrderDetailByOrderId:orderModel.countOrderId]];
        //        for (NSNumber *postedItemId in arrayPostedLinen) {
        //            [manager deleteCountOrderDetailById:[postedItemId integerValue]];
        //        }
        if (arrayPostedLinen != nil)
        {
            for (CountOrderDetailsModelV2 *postedItem in [manager loadAllCountOrderDetailByOrderId:orderModel.countOrderId]) {
                [manager deleteCountOrderDetail:postedItem];
            }
        }
        
        
        
        //        old WS
        //
        //        for (CountOrderDetailsModelV2 *orderDetail in arrayPostLinen) {
        //            tempStatus = [self postLinenItemWithUserId:userId roomAssignId:orderModel.countRoomId AndLinenOrderModel:orderDetail];
        //
        //            //assign status
        //            if (tempStatus == NO) {
        //                status = NO;
        //            }
        //            else{
        //                [manager deleteCountOrderDetail:orderDetail];
        //            }
        //        }
        currentPercent += stepPercent;
        [self updatePercentView:percentView value:currentPercent description:LOADING_POST_LINEN];
    }
    
    return status;
}

#pragma mark - Count WS for Minibar
-(BOOL) getMinibarCategoryListWithUserId:(NSInteger)userId AndHotelId:(NSInteger)hotelId
{
    BOOL result = YES;
    CountManagerV2 *manager = [[CountManagerV2 alloc] init];
    if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        
        result = [manager getMinibarCategoryListWSWithUserId:userId AndHotelId:hotelId];
    }
    else
    {
        result = NO;
    }
    return result;
    
}

-(BOOL) getMinibarItemListWithUserId:(NSInteger)userId AndHotelId:(NSInteger)hotelId
{
    BOOL result = YES;
    CountManagerV2 *manager = [[CountManagerV2 alloc] init];
    if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        
        result = [manager getMinibarItemListWSWithUserId:userId AndHotelId:hotelId];
    }
    else
    {
        result = NO;
    }
    return result;
    
}

-(BOOL)postMinibarItemWithUserId:(NSInteger)userId roomAssignId:(NSInteger)roomAssignId AndMinibarOrderModel:(CountOrderDetailsModelV2 *)model WithOrderId:(NSInteger)orderId
{
    BOOL result = YES;
    CountManagerV2 *manager = [[CountManagerV2 alloc] init];
    if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        
        result = [manager postMinibarItemWSWithUserId:userId roomAssignId:roomAssignId AndMinibarOrderModel:model WithOrderId:orderId];
    }
    else
    {
        result = NO;
    }
    return result;
    
}
-(NSArray *) postAllMinibarItemsWithUserId:(NSInteger)userId roomAssignId:(NSInteger)roomAssignId AndMinibarOrderModelList:(NSArray *)list WithOrderId:(NSInteger)orderId roomNumber:(NSString*)roomNumber isEnablePostingHistory:(BOOL)isEnablePostingHistory{
    CountManagerV2 *manager = [[CountManagerV2 alloc] init];
    if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        
        return [manager postAllMinibarItemsWithUserId:userId roomAssignId:roomAssignId AndMinibarOrderModelList:list WithOrderId:orderId roomNumber:roomNumber isEnablePostingHistory:isEnablePostingHistory];
    }
    
    return nil;
}
-(NSArray *) postAllMinibarItemsWithUserId:(NSInteger)userId roomAssignId:(NSInteger)roomAssignId AndMinibarOrderModelList:(NSArray *)list WithOrderId:(NSInteger)orderId roomNumber:(NSString*)roomNumber
{
    CountManagerV2 *manager = [[CountManagerV2 alloc] init];
    if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        
        return [manager postAllMinibarItemsWithUserId:userId roomAssignId:roomAssignId AndMinibarOrderModelList:list WithOrderId:orderId roomNumber:roomNumber];
    }
    
    return nil;
}

-(NSInteger)postMinibarOrderWithUserId:(NSInteger)userId roomAssignId:(NSInteger)roomAssignId totalQuantity:(NSInteger)totalQuantity serviceCharge:(NSInteger)serviceCharge AndSubTotal:(NSInteger)subTotal WithDate:(NSString *)date
{
    NSInteger result = 0;
    CountManagerV2 *manager = [[CountManagerV2 alloc] init];
    if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        
        result = [manager postMinibarOrderWSWithUserId:userId roomAssignId:roomAssignId totalQuantity:totalQuantity serviceCharge:serviceCharge AndSubTotal:subTotal WithDate:date];
    }
    return result;
}

//---------------------Update code-------------------------------
-(NSInteger) postMinibarOrderWithUserId:(NSInteger) userId roomAssignId:(NSInteger) roomAssignId totalQuantity:(NSInteger) totalQuantity serviceCharge:(NSInteger) serviceCharge AndSubTotal:(NSInteger) subTotal WithDate:(NSString*) date WithRoomNo:(NSString *) roomNo{
    NSInteger result = 0;
    CountManagerV2 *manager = [[CountManagerV2 alloc]init];
    if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        result = [manager postMinibarOrderWSWithUserId:userId roomAssignId:roomAssignId totalQuantity:totalQuantity serviceCharge:serviceCharge AndSubTotal:subTotal WithDate:date WithRoomNo:roomNo];
    }
    return result;
}
//---------------------Update code-------------------------------

-(BOOL)postAllMinibarItemWithUserId:(NSInteger)userId AndPercentView:(MBProgressHUD *)percentView{
    BOOL status = YES;
    
    //    old WS
    //    BOOL tempStatus = NO;
    
    
    CountManagerV2 *manager = [[CountManagerV2 alloc] init];
    
    //load all minibar order
    //    NSMutableArray *arrayMinibar = [manager loadAllCountOrdersByUserId:userId AndServiceId:cMinibar];
    NSMutableArray *arrayMinibar = [manager loadCountOrdersByUserId:userId AndServiceId:cMinibar];
    
    float currentPercent = 0;
    float stepPercent = [self caculateStepFromCountingObject:(int)[arrayMinibar count]];
    [self updatePercentView:percentView value:currentPercent description:LOADING_POST_MINIBAR];
    
    for (CountOrdersModelV2 *orderModel in arrayMinibar) {
        //break syncing
        if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL)
        {
            break;
        }
        NSInteger totalQuantity = 0;
        //        if ([orderModel countStatus] == POST_STATUS_POSTED) {
        //            continue;
        //        }
        //load all count order detail by order id
        NSMutableArray *arrayPostMinibar = [manager loadAllCountOrderDetailByOrderId:orderModel.countOrderId];
        
        for (CountOrderDetailsModelV2 *orderDetail in arrayPostMinibar) {
            //calculator total minibar used
            totalQuantity += orderDetail.countCollected;
            
        }
        // Conver ra_id to room_id
        RoomAssignmentModelV2 *roomAssignmentModel = [[RoomAssignmentModelV2 alloc]init];
        roomAssignmentModel.roomAssignment_Id = (int)orderModel.countRoomAssignId;
        roomAssignmentModel.roomAssignment_UserId = (int)userId;
        [[RoomManagerV2 sharedRoomManager] load:roomAssignmentModel];
        //NSString *roomNo = roomAssignmentModel.roomAssignment_RoomId;
        
        //post minibar` order to get orderId
        //        NSInteger orderId = [self postMinibarOrderWithUserId:userId roomAssignId:orderModel.countRoomId totalQuantity:totalQuantity serviceCharge:0 AndSubTotal:totalQuantity WithDate:orderModel.countCollectDate];
        NSString *roomNo = [RoomManagerV2 getCurrentRoomNo];
        
        if (roomNo == nil || roomNo.length == 0 || [roomNo isEqualToString:@""]) {
            NSString *message = nil;
            if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                message = [NSString stringWithFormat:@"%@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOM_INVALID]];
            } else {
                message = [NSString stringWithFormat:@"\n\n\n\n\n%@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOM_INVALID]];
            }
            message = [NSString stringWithFormat:@"SYNC - %@", message];
            UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObject:[UIImage imageNamed:@"yes_bt.png"]] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:message delegate:nil cancelButtonTitle:[L_CLOSE currentKeyToLanguage] otherButtonTitles: nil];
            
            if(!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                UIImageView *alertImage = [[UIImageView alloc] initWithFrame:CGRectMake(80, 35, 120, 120)];
                [alertImage setImage:[UIImage imageNamed:@"icon_wrong room.png"]];
                [alert addSubview:alertImage];
            }
            [alert show];
        }else{
            NSInteger orderId = [self postMinibarOrderWithUserId:userId roomAssignId:orderModel.countRoomAssignId totalQuantity:totalQuantity serviceCharge:0 AndSubTotal:totalQuantity WithDate:orderModel.countCollectDate WithRoomNo:orderModel.countRoomNumber];
            
            //post minibar order detail
            //new WS
            NSArray *postedItems = [self postAllMinibarItemsWithUserId:userId roomAssignId:orderModel.countRoomAssignId AndMinibarOrderModelList:arrayPostMinibar WithOrderId:orderId roomNumber:orderModel.countRoomNumber];
            
            //        for (NSNumber *postedItemId in postedItems) {
            //            [manager deleteCountOrderDetailById:[postedItemId integerValue]];
            //        }
            if (postedItems != nil)
            {
                for (CountOrderDetailsModelV2 *postedItem in arrayPostMinibar) {
                    [manager deleteCountOrderDetail:postedItem];
                }
                [manager deleteCountOrderByOrderId:(int)orderModel.countOrderId];
            }
        }
        
        //        old WS
        //        for (CountOrderDetailsModelV2 *orderDetail in arrayPostMinibar) {
        //
        //            tempStatus = [self postMinibarItemWithUserId:userId roomAssignId:orderModel.countRoomId AndMinibarOrderModel:orderDetail WithOrderId:orderId];
        //
        //            //assign status
        //            if (tempStatus == NO) {
        //                status = NO;
        //            }
        //            else
        //            {
        //                [manager deleteCountOrderDetail:orderDetail];
        //            }
        //        }
        currentPercent += stepPercent;
        [self updatePercentView:percentView value:currentPercent description:LOADING_POST_MINIBAR];
    }
    
    return status;
}

#pragma mark - Count WS for Amenities

//-(BOOL)getRoomTypeListWithHotelId:(NSInteger)hotelId
//{
//    BOOL result = YES;
//    AmenitiesManagerV2 *manager = [AmenitiesManagerV2 sharedAmenitiesManager];
//    if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
//        
//        result = [manager getRoomTypeListWSWithHotelId:hotelId];
//    }
//    else
//    {
//        result = NO;
//    }
//    return result;
//}

-(BOOL)getAmenitiesCategoryListWithUserId:(NSInteger)userId AndHotelId:(NSInteger)hotelId
{
    BOOL result = YES;
    AmenitiesManagerV2 *manager = [AmenitiesManagerV2 sharedAmenitiesManager];
    if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        
        result = [manager getAmenitiesCategoryListWSWithUserId:userId AndHotelId:hotelId];
    }
    else
    {
        result = NO;
    }
    return result;
    
}
-(BOOL)getAmenitiesItemListWithUserId:(NSInteger)userId AndHotelId:(NSInteger)hotelId
{
    BOOL result = YES;
    AmenitiesManagerV2 *manager = [AmenitiesManagerV2 sharedAmenitiesManager];
    if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        
        result = [manager getAmenitiesItemListWSWithUserId:userId AndHotelId:hotelId];
    }
    else
    {
        result = NO;
    }
    return result;
    
}

/*
-(BOOL)postAmenitiesItemWithUserId:(NSInteger)userId RoomAssignId:(NSInteger)roomAssignId AndAmenitiesItem:(AmenitiesOrderDetailsModelV2 *)model WithDate:(NSString *)date
{
    BOOL result = YES;
    AmenitiesManagerV2 *manager = [[AmenitiesManagerV2 alloc] init];
    if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        
        result = [manager postAmenitiesItemWSWithUserId:userId RoomAssignId:roomAssignId AndAmenitiesItem:model WithDate:date];
    }
    else
    {
        result = NO;
    }
    return result;
}
*/

-(BOOL)postAllAmenitiesItemWithUserId:(NSInteger)userId AndPercentView:(MBProgressHUD *)percentView {
    AmenitiesManagerV2 *amenManager = [AmenitiesManagerV2 sharedAmenitiesManager];
    NSMutableArray *listAmenities = [amenManager loadAllAmenitiesByUserId:(int)userId];
    int result = [amenManager postAmenitiesByUserId:(int)userId amenitiesItems:listAmenities];
    if(result == RESPONSE_STATUS_OK || result == RESPONSE_STATUS_NEW_RECORD_ADDED) {
        return YES;
    }
    
    return NO;
}

/*
#pragma mark - Count WS for Laundry
-(BOOL)getAllLaundryServiceByUserID:(NSNumber *)userId atLocation:(NSNumber *)hotelId andDate:(NSString *)lastModified
{
    BOOL result = YES;
    if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        [[LaundryManagerV2 sharedLaundryManagerV2] getAllLaundryServiceFromWSByUserID:userId atLocation:hotelId andDate:lastModified];
        result = YES;
    }
    else
    {
        result = NO;
    }
    return result;
}
-(BOOL)getAllLaundryCategoryByUserId:(NSNumber *)userID atHotel:(NSNumber *)userHotelID andlastDate:(NSString *)lastModified
{
    BOOL result = YES;
    if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        [[LaundryManagerV2 sharedLaundryManagerV2] getAllLaundryCategoryFromWSByUserId:userID atHotel:userHotelID andlastDate:lastModified];
        result = YES;
    }
    else
    {
        result = NO;
    }
    return result;
}
-(BOOL)getAllLaundryItemByUserId:(NSNumber *)userID andmodifiedDate:(NSString *)modifiedDate
{
    BOOL result = YES;
    if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        [[LaundryManagerV2 sharedLaundryManagerV2] getAllLaundryItemFromWSByUserId:userID andmodifiedDate:modifiedDate];
        result = YES;
    }
    else
    {
        result = NO;
    }
    return result;
    
}
-(BOOL)getAllLaundryItemPriceByUserId:(NSNumber *)userID andmodifiedDate:(NSString *)modifiedDate
{
    BOOL result = YES;
    if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        [[LaundryManagerV2 sharedLaundryManagerV2] getAllLaundryItemPriceFromWSByUserId:userID andmodifiedDate:modifiedDate];
        result = YES;
    }
    else
    {
        result = NO;
    }
    return result;
    
}
-(int)postLaundryOrder:(LaudryOrderModelV2 *)lr_Order_Model andsubQuantity:(int)subQuantity andServiceID:(int)serviceID andHotel:(int)HotelID andsubTotal:(CGFloat)subTotal
{
    int result = 0;
    if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        
        result = [[LaundryManagerV2 sharedLaundryManagerV2] postLaundryOrder:lr_Order_Model andsubQuantity:subQuantity andServiceID:serviceID andHotel:HotelID andsubTotal:subTotal];
        
    }
    return result;
}

-(BOOL)postLaundryItem:(LaundryOrderDetailModelV2 *)lr_Order_Detail_Model andLaundryOrder:(int)laundry_Order_Id anduser:(int)userId andTransactionTime:(NSString *)transaction_time
{
    BOOL result = NO;
    if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        
        result = [[LaundryManagerV2 sharedLaundryManagerV2] postLaundryItem:lr_Order_Detail_Model andLaundryOrder:laundry_Order_Id anduser:userId andTransactionTime:transaction_time];
        
    }
    return result;
}

-(BOOL)postAllLaundryItemWithUserId:(NSInteger)userId AndPercentView:(MBProgressHUD *)percentView{
    BOOL status = YES;
    BOOL tempStatus = NO;
    NSInteger total = 0;
    CGFloat subTotal = 0;
    LaundryManagerV2 *manager = [[LaundryManagerV2 alloc] init];
    
    //load all laundry service
    NSMutableArray *arrayService = [manager loadAllLaundryServicesModelV2];
    
    //load all laundry order
    NSMutableArray *arrayLaundry = [manager loadAllLaundryOrderByUserId:userId];
    
    float currentPercent = 0;
    float stepPercent = [self caculateStepFromCountingObject:[arrayLaundry count]/2];
    [self updatePercentView:percentView value:currentPercent description:LOADING_POST_LAUNDRY];
    
    //get total and subTotal
    for (LaudryOrderModelV2 *laundryOrderModel in arrayLaundry) {
        //break syncing
        if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL)
        {
            break;
        }
        //load all laundry order detail by order id
        NSMutableArray *arrayPostLaundry = [manager loadAllLaundryOrderDetailsByOrderId:laundryOrderModel.laundryOrderId];
        
        for (LaundryOrderDetailModelV2 *orderDetail in arrayPostLaundry) {
            //break syncing
            if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL)
            {
                break;
            }
            //calculator total and subTotal
            total += orderDetail.lod_quantity;
            LaundryItemPriceModelV2 *lItemPrice = [manager loadLaundryItemPriceModelV2ByLaundryItemID:orderDetail.lod_item_id];
            subTotal += lItemPrice.lip_price * orderDetail.lod_quantity;
        }
        
        currentPercent += stepPercent;
        currentPercent = (currentPercent > 50) ? 50 : currentPercent;
        [self updatePercentView:percentView value:currentPercent description:LOADING_POST_LAUNDRY];
        
    }
    
    stepPercent = [self caculateStepFromCountingObject:[arrayService count]/2];
    //post all laundry item
    for (LaundryServicesModelV2 *service in arrayService) {
        //break syncing
        if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL)
        {
            break;
        }
        for (LaudryOrderModelV2 *laundryOrderModel in arrayLaundry) {
            //break syncing
            if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL)
            {
                break;
            }
            //get laundry order id
            NSInteger orderId = [self postLaundryOrder:laundryOrderModel andsubQuantity:total andServiceID:service.lservices_id andHotel:[[[UserManagerV2 sharedUserManager] currentUser] userHotelsId] andsubTotal:subTotal];
            
            //load all laundry order detail by order id and service id
            NSMutableArray *arrayPostLaundryOrder = [manager loadAllLaundryOrderDetailsByServiceId:service.lservices_id AndOrderId:laundryOrderModel.laundryOrderId];
            
            //post laundry order detail
            for (LaundryOrderDetailModelV2 *lorderDetail in arrayPostLaundryOrder) {
                
                //break syncing
                if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL)
                {
                    break;
                }
                
                tempStatus = [self postLaundryItem:lorderDetail andLaundryOrder:orderId anduser:userId andTransactionTime:laundryOrderModel.laundryOrderCollectDate];
                
                //assign status
                if (tempStatus == NO) {
                    status = NO;
                }
                else
                {
                    [manager deleteLaundryOrderDetailModelV2:lorderDetail];
                }
            }
        }
        
        currentPercent += stepPercent;
        [self updatePercentView:percentView value:currentPercent description:LOADING_POST_LAUNDRY];
    }
    
    return status;
}
*/

//----------------- Start add Post WSLog---------------
- (BOOL) postWSLogForPhysicalCheck:(NSInteger)userId Message:(NSString *)messageValue MessageType:(NSInteger)type
{
    if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        return [[PhysicalCheckManagerV2 sharedPhysicalCheckManagerV2] postWSLog:userId Message:messageValue MessageType:type];
    }
    return NO;
}

- (BOOL) postWSLogCountService:(NSInteger)userId Message:(NSString *)messageValue MessageType:(NSInteger)type
{
    CountManagerV2 *Countmanager = [[CountManagerV2 alloc] init];
    if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        
        return [Countmanager postWSLog:userId Message:messageValue MessageType:type];
    }
    return NO;
}

/*
- (BOOL) postWSLogLaundry:(NSInteger)userId Message:(NSString *)messageValue MessageType:(NSInteger)type
{
    if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        return [[LaundryManagerV2 sharedLaundryManagerV2] postWSLog:userId Message:messageValue MessageType:type];
    }
    return NO;
}*/

- (BOOL) postWSLogAmenities:(NSInteger)userId Message:(NSString *)messageValue MessageType:(NSInteger)type
{
    AmenitiesManagerV2 *AmenManager = [AmenitiesManagerV2 sharedAmenitiesManager];
    if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        return [AmenManager postWSLog:userId Message:messageValue MessageType:1];
    }
    return NO;
}

- (BOOL) postWSLogRoomAsgnMent:(NSInteger)userId Message:(NSString *)messageValue MessageType:(NSInteger)type
{
    RoomManagerV2 *roomManager = [[RoomManagerV2 alloc] init];
    if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        return [roomManager postWSLog:(int)userId message:messageValue messageType:(int)type];
    }
    return NO;
}

- (BOOL) postWSLogCheckListDetail:(NSInteger)userId Message:(NSString *)messageValue MessageType:(NSInteger)type
{
    CheckListManagerV2 *checkListManager = [[CheckListManagerV2 alloc] init];
    if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        return [checkListManager postWSLog:(int)userId message:messageValue messageType:(int)type];
    }
    return NO;
}
//-----------------End add post WSLog--------------------


- (eHousekeepingService_PostPhysicalCheck*) postPhysicalCheckWithUserId:(NSInteger) userId andRoomNo:(NSString*)roomNo andHotelID:(NSInteger) hotelId andStatusId:(NSInteger) statusId andCleaningStatusId:(NSInteger)cleaningStatusId
{
    if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        
        return [[PhysicalCheckManagerV2 sharedPhysicalCheckManagerV2] postPhysicalCheckWithUserId:userId andRoomNo:roomNo andHotelID:hotelId andStatusId:statusId andCleaningStatusId:cleaningStatusId];
        
    }
    return NULL;
}

#pragma mark - Count WS for Laundry Intruction
/*
-(BOOL)getAllLaundryInstructionByUserId:(NSNumber *)userID andmodifiedDate:(NSString *)modifiedDate
{
    BOOL result = YES;
    if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        [[LaundryManagerV2 sharedLaundryManagerV2] getAllLaundryInstructionFromWSByUserId:userID andModifiedDate:modifiedDate];
        result = YES;
    }
    else
    {
        result = NO;
    }
    return result;
}
-(int)postLaundryInstructionOrder:(id)lr_Instruction_Order_Model andLaundryOrder:(int)laundry_Order_Id anduser:(int)userId
{
    int result = 0;
    if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        result = [[LaundryManagerV2 sharedLaundryManagerV2] postLaundryInstructionOrder:lr_Instruction_Order_Model andLaundryOrder:laundry_Order_Id anduser:userId];
    }
    return result;
}



-(BOOL)postAllLaundryInstructionOrderWithUserId:(NSInteger)userId AndPercentView:(MBProgressHUD *)percentView
{
    BOOL status = YES;
    BOOL tempStatus = NO;
    NSInteger total = 0;
    CGFloat subTotal = 0;
    LaundryManagerV2 *manager = [[LaundryManagerV2 alloc] init];
    
    //load all laundry service
    NSMutableArray *arrayService = [manager loadAllLaundryServicesModelV2];
    
    //load all laundry order
    NSMutableArray *arrayLaundry = [manager loadAllLaundryOrderByUserId:userId];
    
    float currentPercent = 0;
    float stepPercent = [self caculateStepFromCountingObject:([arrayLaundry count]/2)];
    [self updatePercentView:percentView value:currentPercent description:LOADING_POST_INSTRUCTION];
    
    //get total and subTotal
    for (LaudryOrderModelV2 *laundryOrderModel in arrayLaundry) {
        //break syncing
        if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL)
        {
            break;
        }
        //load all laundry order detail by order id
        NSMutableArray *arrayPostLaundry = [manager loadAllLaundryOrderDetailsByOrderId:laundryOrderModel.laundryOrderId];
        
        for (LaundryOrderDetailModelV2 *orderDetail in arrayPostLaundry) {
            //break syncing
            if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL)
            {
                break;
            }
            //calculator total and subTotal
            total += orderDetail.lod_quantity;
            LaundryItemPriceModelV2 *lItemPrice = [manager loadLaundryItemPriceModelV2ByLaundryItemID:orderDetail.lod_item_id];
            subTotal += lItemPrice.lip_price * orderDetail.lod_quantity;
        }
        
        currentPercent += stepPercent;
        currentPercent = (currentPercent > 50) ? 50 : currentPercent;
        [self updatePercentView:percentView value:currentPercent description:LOADING_POST_INSTRUCTION];
    }
    
    stepPercent = [self caculateStepFromCountingObject:([arrayService count]/2)];
    for (LaundryServicesModelV2 *service in arrayService) {
        //break syncing
        if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL)
        {
            break;
        }
        for (LaudryOrderModelV2 *laundryOrderModel in arrayLaundry) {
            //break syncing
            if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL)
            {
                break;
            }
            //get laundry order id
            NSInteger orderId = [self postLaundryOrder:laundryOrderModel andsubQuantity:total andServiceID:service.lservices_id andHotel:[[[UserManagerV2 sharedUserManager] currentUser] userHotelsId] andsubTotal:subTotal];
            
            //load all laundry instruction by order id
            NSMutableArray *arrayPostLaundryInstruction = [manager loadAllLaundryInstructionsOrderModelV2ID:laundryOrderModel.laundryOrderId];
            
            //post laundry instruction order
            for (LaundryInstructionsOrderModelV2 *lInstructionOrder in arrayPostLaundryInstruction) {
                //break syncing
                if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL)
                {
                    break;
                }
                tempStatus = [self postLaundryInstructionOrder:lInstructionOrder andLaundryOrder:orderId anduser:userId];
                
                //assign status
                if (tempStatus == NO) {
                    status = NO;
                }
                else{
                    [manager deleteLaundryInstructionsOrderModelV2:lInstructionOrder];
                }
            }
        }
        currentPercent += stepPercent;
        [self updatePercentView:percentView value:currentPercent description:LOADING_POST_INSTRUCTION];
    }
    return status;
    
}
*/

#pragma mark - === Calculate Must Sync Records ===
#pragma mark
//calculate all record pending for syncing
-(void)calculateRecordMustSync {
    
    
    //update number record sync in home view
    [[HomeViewV2 shareHomeView] updateRecordSyncWithNumber:0];
}

#pragma mark - === Lost and Found ===
#pragma mark
#pragma mark  Get L&F Category Data from WS
-(void)getLaFCategoryFromWS:(NSInteger) userId andHotelId:(NSString*) hotelId andLastModifier:(NSString *) lastModifier {
//    [[LostandFoundManagerV2 sharedLostandFoundManagerV2] getLaFCategoryFromWS:[NSNumber numberWithInteger:userId] andLastModifier:lastModifier];
    [[LostandFoundManagerV2 sharedLostandFoundManagerV2] getLaFCategoryFromWS:[NSNumber numberWithInteger:userId] andLastModifier:lastModifier andHotelID:hotelId];
}

#pragma mark - Get L&F Item Data from WS
-(void)getLaFItemFromWS:(NSInteger) userId andHotelId:(NSString*) hotelId andLastModifier:(NSString *) lastModifier {
//    [[LostandFoundManagerV2 sharedLostandFoundManagerV2] getLaFItemFromWS:[NSNumber numberWithInteger:userId] andLastModifier:lastModifier];
    [[LostandFoundManagerV2 sharedLostandFoundManagerV2] getLaFItemFromWS:[NSNumber numberWithInteger:userId] andLastModifier:lastModifier andHotelID:hotelId];
}

#pragma mark - Get L&F Color Data from WS
-(void)getLaFColorFromWS:(NSInteger)userId andLastModifier:(NSString*)lastModifier {
    [[LostandFoundManagerV2 sharedLostandFoundManagerV2] getLaFColorFromWS:[NSNumber numberWithInteger:userId] andLastModifier:lastModifier];
}

#pragma mark- Post L&F Data from WS
-(BOOL)postLostandFound:(LostandFoundDetailModelV2 *) lfDetail {
    
    if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp] == NO) {
        //no network connection
        return NO;
    }
    
    return [[LostandFoundManagerV2 sharedLostandFoundManagerV2] postLostandFound:lfDetail];
}

#pragma mark - Post L&F Photo from WS
-(BOOL)postLaFPhoto:(LostandFoundDetailModelV2 *) lafDetail andPhoto:(LFImageModelV2 *) lfPhoto {
    
    if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp] == NO) {
        //no network connection
        return NO;
    }
    
    return [[LostandFoundManagerV2 sharedLostandFoundManagerV2] postLaFPhoto:lafDetail andPhoto:lfPhoto];
}

-(BOOL)postAllLostAndFoundWithUserId:(NSInteger)userId AndPercentView:(MBProgressHUD *)percentView{
    BOOL status = YES, tempStatus = YES;
    
    //load all lost and found detail
    NSMutableArray *lafDetails = [[LostandFoundManagerV2 sharedLostandFoundManagerV2] loadAllLaFDetailByUserId:userId];
    
    float currentPercent = 0;
    float stepPercent = [self caculateStepFromCountingObject:((float)[lafDetails count]/2.0f)];
    [self updatePercentView:percentView value:currentPercent description:LOADING_POST_LOSTANDFOUND];
    
    //post all laf photo first
    //load all photo of lost and found detail
    for (LostandFoundDetailModelV2 *lafDetail in lafDetails) {
        //break syncing
        if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL)
        {
            break;
        }
        NSMutableArray *lafImages = [[LostandFoundManagerV2 sharedLostandFoundManagerV2] loadAllImageDataByLFDetailId:(int)lafDetail.lafDetailId];
        
        for (LFImageModelV2 *lafImage in lafImages) {
            //break syncing
            if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL)
            {
                break;
            }
            tempStatus = [self postLaFPhoto:lafDetail andPhoto:lafImage];
            
            if (tempStatus == NO) {
                status = NO;
            }
        }
        
        currentPercent += stepPercent;
        currentPercent = (currentPercent > 50)? 50: currentPercent;
        [self updatePercentView:percentView value:currentPercent description:LOADING_POST_LOSTANDFOUND];
        
    }
    //post all laf detail after that
    //load all lost and found models by lost and found detail model
    for (LostandFoundDetailModelV2 *lafDetail in lafDetails) {
        //break syncing
        if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL)
        {
            break;
        }
        NSMutableArray *lafModels = [[LostandFoundManagerV2 sharedLostandFoundManagerV2] loadAllLaFDataByLaFDetailId:(int)lafDetail.lafDetailId];
        
        lafDetail.lafArray = lafModels;
        tempStatus = [self postLostandFound:lafDetail];
        
        currentPercent += stepPercent;
        [self updatePercentView:percentView value:currentPercent description:LOADING_POST_LOSTANDFOUND];
    }
    
    return status;
}

#pragma mark - ===  Get Engineering Category  ===
#pragma mark
-(BOOL) getDataEngineeringCategoryWithUserIDFromWS:(NSInteger ) userID AndHotelId:(NSString*) hotelId AndLastModified:(NSString *) lastModified AndPercentView:(MBProgressHUD*)percentView {
    
    BOOL result = YES;
    if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
//        [[EngineerCaseViewManagerV2 sharedEngineerCaseViewManagerV2]  getDataEngineeringCategoryWithUserID:[NSNumber numberWithInteger:userID] AndLastModified:lastModified AndPercentView:percentView];
        [[EngineerCaseViewManagerV2 sharedEngineerCaseViewManagerV2] getDataEngineeringCategoryWithUserID:[NSNumber numberWithInteger:userID] lastModified:lastModified AndHotelID:hotelId AndPercentView:percentView];
        result = YES;
    }
    else
    {
        result = NO;
    }
    return result;
}

#pragma mark - ===  Get Engineering Item  ===
#pragma mark
-(BOOL) getDataEngineeringItemWithUserIDFromWS:(NSInteger) userID AndHotelID:(NSInteger) hotelID AndLastModified:(NSString *) lastModified AndPercentView:(MBProgressHUD *)percentView{
    
    BOOL result = YES;
    if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
//        [[EngineerCaseViewManagerV2 sharedEngineerCaseViewManagerV2]  getDataEngineeringCategoryWithUserID:[NSNumber numberWithInteger:userID] AndLastModified:lastModified AndPercentView:percentView];
        [[EngineerCaseViewManagerV2 sharedEngineerCaseViewManagerV2] getDataEngineeringItemWithUserID:[NSNumber numberWithInteger:userID] AndHotelID:[NSNumber numberWithInteger:hotelID] AndLastModified:lastModified];
        result = YES;
    }
    else
    {
        result = NO;
    }
    return result;
}

#pragma mark - ===  Post Engineering Case  ===
#pragma mark
-(BOOL) postEngineeringCaseWithUserID:(NSInteger) userID AndPercentView:(MBProgressHUD *)percentView{
    BOOL status = YES;
    NSMutableArray *engineeringCaseDetailes = [[EngineerCaseViewManagerV2 sharedEngineerCaseViewManagerV2] loadAllEngineerDetailModelByRoomIdUserId:userID];
    
    float currentPercent = 0;
    float stepPercent = [self caculateStepFromCountingObject:(int)[engineeringCaseDetailes count]];
    [self updatePercentView:percentView value:currentPercent description:LOADING_POST_ENGINEERING];
    
    for (EngineeringCaseDetailModelV2 *modelDetail  in engineeringCaseDetailes) {
        //break syncing
        if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL)
        {
            break;
        }
        status = NO;
        NSMutableArray *arrayPhoto = [[EngineerCaseViewManagerV2 sharedEngineerCaseViewManagerV2] loadAlLEngineerImageByDetailID:(int)modelDetail.ecd_id];
        
        NSMutableArray *arrayECModel = [[EngineerCaseViewManagerV2 sharedEngineerCaseViewManagerV2] loadAllEngineerCaseViewModelV2:modelDetail.ecd_id];
        
        BOOL isPostECSuccess = FALSE;
        
        if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]
            == TRUE) {
            
            //remove for change WS post Engineering
            //            for (EngineerImageModelV2 *imageModel in arrayPhoto) {
            //                //break syncing
            //                if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL) {
            //                    break;
            //                }
            //                isPostPhotoSuccess = [[EngineerCaseViewManagerV2 sharedEngineerCaseViewManagerV2] postECAttachPhotoWithUserID:[NSNumber numberWithInteger:modelDetail.ecd_user_id] AndRoomAssignID:[NSNumber numberWithInteger:modelDetail.ecd_room_id] AndPhoto:imageModel.enim_image];
            //            }
            //
            //            if(isPostPhotoSuccess == TRUE || [arrayPhoto count] <= 0) {
            //                for (int i = 0; i < [arrayECModel count]; i++) {
            //                    //break syncing
            //                    if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL)
            //                    {
            //                        break;
            //                    }
            //                    EngineerCaseViewModelV2 *model = [arrayECModel objectAtIndex:i];
            //
            //                    isPostECSuccess = [[EngineerCaseViewManagerV2 sharedEngineerCaseViewManagerV2] postEngineeringCaseWithUserID:[NSNumber numberWithInteger:modelDetail.ecd_user_id] AndRoomAssignID:[NSNumber numberWithInteger:modelDetail.ecd_room_id] AndEngineeringItemID:[NSNumber numberWithInteger:model.ec_item_id] AndReportedTime:modelDetail.ecd_date                                                                                                                    AndRemark:modelDetail.ecd_reMark];
            //
            //                }
            //            }
            
            //apply for new WS post Engineering
            int maxIndexPhoto = (int)[arrayPhoto count] - 1;
            for (int i = 0; i < [arrayECModel count]; i++) {
                EngineerCaseViewModelV2 *model = [arrayECModel objectAtIndex:i];
                
                EngineerImageModelV2 *imageModel = nil;
                if(i <= maxIndexPhoto) {
                    imageModel = [arrayPhoto objectAtIndex:i];
                }
                
                isPostECSuccess = [[EngineerCaseViewManagerV2 sharedEngineerCaseViewManagerV2] postEngineeringCaseWithUserID:[NSNumber numberWithInteger:modelDetail.ecd_user_id] AndRoomAssignID:modelDetail.ecd_room_id AndEngineeringItemID:[NSNumber numberWithInteger:model.ec_item_id] AndReportedTime:modelDetail.ecd_date AndRemark:modelDetail.ecd_reMark AndPhoto: ((imageModel == nil) ? nil : imageModel.enim_image) AndExtension:@"JPEG"];
            }
            
            if(isPostECSuccess == TRUE) {
                [[EngineerCaseViewManagerV2 sharedEngineerCaseViewManagerV2] deleteAllEngineerImageData:(int)modelDetail.ecd_id];
                
                [[EngineerCaseViewManagerV2 sharedEngineerCaseViewManagerV2] deleteAllEngineerCaseViewData:(int)modelDetail.ecd_id];
                
                [[EngineerCaseViewManagerV2 sharedEngineerCaseViewManagerV2] deleteEngineeringCaseDetailModelV2:modelDetail];
                status = YES;
            }
        }
        
        currentPercent += stepPercent;
        [self updatePercentView:percentView value:currentPercent description:LOADING_POST_ENGINEERING];
    }
    
    return status;
}



#pragma mark - ===  Get Floor List Room ===
#pragma mark

-(BOOL)getFloorListFromWSWithFromWS:(NSInteger)hotelID andLastModified:(NSString*)lastModified AndPercentView:(MBProgressHUD *)percentView{
    BOOL result = YES;
    UserModelV2 *_user = [[UserModelV2 alloc] init];
    _user.userHotelsId = (int)hotelID;
    if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        [[UnassignManagerV2 sharedUnassignManager] getFloorListFromWSWith:_user andLastModified:lastModified AndPercentView:percentView];
        result = YES;
    }
    else
    {
        result = NO;
    }
    return result;
    
}

#pragma mark - ===  Get Zone List Room ===
#pragma mark
-(BOOL)getZoneRoomListWithUserFromWS:(NSInteger)hotelID andlastModified:(NSString*)lastModidfied AndPercentView:(MBProgressHUD *)percentView{
    BOOL result = YES;
    UserModelV2 *_user = [[UserModelV2 alloc] init];
    _user.userHotelsId = (int)hotelID;
    if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        [[UnassignManagerV2 sharedUnassignManager] getZoneRoomListWithUser:_user andlastModified:lastModidfied AndPercentView:percentView];;
        result = YES;
    }
    else
    {
        result = NO;
    }
    return result;
    
    
}

#pragma mark - ===  Get Zone List Room ===
#pragma mark
-(BOOL)getRoomRecordDetailListWithUserFromWS:(NSInteger)userId andlastModified:(NSString*)lastModidfied{
    BOOL result = YES;
    if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        [[RoomManagerV2 sharedRoomManager] getRoomRecordDetail:userId andLastModified:nil];
        result = YES;
    }
    else
    {
        result = NO;
    }
    return result;
    
    
}

#pragma mark - ===  Post Unassign Room ===
#pragma mark

-(BOOL)postAssignRoom:(NSInteger)userID AndPercentView:(MBProgressHUD *)percentView{
    
    BOOL status = YES, temp = YES;
    
    //load all room assign model in local db
    NSMutableArray *assigns = [[UnassignManagerV2 sharedUnassignManager] loadAllAssignRoomByUserId:userID];
    
    float currentPercent = 0;
    float stepPercent = [self caculateStepFromCountingObject:(int)[assigns count]];
    [self updatePercentView:percentView value:currentPercent description:LOADING_POST_ASSIGN];
    
    for (AssignRoomModelV2 *assign in assigns) {
        //break syncing
        if([MBProgressHUD getCancelStatus] == PRESSED_CANCEL)
        {
            break;
        }
        temp = [self postAssignRoomWithAssignModel:assign];
        
        if (temp == NO) {
            status = NO;
        }
        currentPercent += stepPercent;
        [self updatePercentView:percentView value:currentPercent description:LOADING_POST_ASSIGN];
    }
    
    return status;
}

-(int)postAssignRoomWithAssignModel:(AssignRoomModelV2 *)assignRoom{
    if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp] == NO) {
        //no network connection
        return NO;
    }
    int responseCode = [[UnassignManagerV2 sharedUnassignManager] postAssignRoom:assignRoom];
    // DungPhan-20150708-Discussion with FM: Delete assign room when resp code is 12025, 12026, 12027, 12028, 12029, 12030, 12031 for not repost when sync again
    if (responseCode == RESPONSE_STATUS_ERROR_REASSIGN_DATE_IN_THE_PAST || responseCode == RESPONSE_STATUS_ERROR_REASSIGN_OUT_OF_STAFF_WORKSHIFT || responseCode == RESPONSE_STATUS_ERROR_REASSIGN_OUT_OF_REMAINED_TIME || responseCode == RESPONSE_STATUS_ERROR_ASSIGNMENT_NOT_EXIST || responseCode == RESPONSE_STATUS_ERROR_STAFF_NOT_ASSIGNED || responseCode == RESPONSE_STATUS_ERROR_NOT_CURRENT_DAY_ROOM_ASSIGNMENT || responseCode == RESPONSE_STATUS_ERROR_END_CLEANING_TIME_EXCEED_CURRENT_DAY_ASSIGNMENT) {
        
        assignRoom.assignRoom_post_status = POST_STATUS_POSTED;
        
        [[UnassignManagerV2 sharedUnassignManager] deleteAssign:assignRoom];
    }
    return responseCode;
}

#pragma mark - ===  Post Room Record Detail Room ===
#pragma mark

-(BOOL)postRoomRecordDetailWithRoomRecordDetail:(RoomRecordDetailModelV2*)roomRecordDetail{
    if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp] == NO) {
        //no network connection
        return NO;
    }
    return [[RoomManagerV2 sharedRoomManager] postRoomRecordDetail:roomRecordDetail];
    
}


-(BOOL)postRoomRoomRecordDetail:(NSInteger)userID{
    
    BOOL status = YES, temp = YES;
    
    //load all room assign model in local db
    NSMutableArray *roomRecordDetails = [[RoomManagerV2 sharedRoomManager] loadAllRoomRecordDetailByCurrentUser:userID];
    
    for (RoomRecordDetailModelV2 *roomRecordDetail in roomRecordDetails) {
        temp = [self postRoomRecordDetailWithRoomRecordDetail:roomRecordDetail];
        
        if (temp == NO) {
            status = NO;
        }
    }
    
    return status;
}



#pragma mark - === Update Must Post Records ===
#pragma mark
-(void) updateMustPostRecords {
    NSInteger userId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    
    NSInteger mustPostRecords = 0;
    
    CheckListManagerV2 *chkListManager = [[CheckListManagerV2 alloc] init];
    CountManagerV2 *countManager = [[CountManagerV2 alloc] init];
    AmenitiesManagerV2 *amenitiesManager = [AmenitiesManagerV2 sharedAmenitiesManager];
    //LaundryManagerV2 *laundryManager = [[LaundryManagerV2 alloc] init];
    
    NSMutableString *alertString = [NSMutableString string];
    NSInteger tempMustPostRecords = 0;
    
    //check list
    tempMustPostRecords = [chkListManager numberOfMustPostChecklistItemRecords:userId];
    mustPostRecords += tempMustPostRecords;
    
    [alertString appendFormat:@"checklist %d ||", (int)tempMustPostRecords];
    
//    //laundry
//    tempMustPostRecords = [laundryManager numberOfMustPostLaundryRecords:userId];
//    mustPostRecords += tempMustPostRecords;
//    
//    [alertString appendFormat:@"laundry %d ||", tempMustPostRecords];
//    
//    //laundry instruction
//    tempMustPostRecords = [laundryManager numberOfMustPostLaundryInstructionRecords:userId];
//    mustPostRecords += tempMustPostRecords;
//    
//    [alertString appendFormat:@"laundry instruction %d ||", tempMustPostRecords];
    
    //minibar
    tempMustPostRecords = [countManager numberOfMustPostMinibarRecords:userId];
    mustPostRecords += tempMustPostRecords;
    
    [alertString appendFormat:@"minibar %d ||", (int)tempMustPostRecords];
    
    //linen
    tempMustPostRecords = [countManager numberOfMustPostLinenRecords:userId];
    mustPostRecords += tempMustPostRecords;
    
    [alertString appendFormat:@"linen %d ||", (int)tempMustPostRecords];
    
    //amenities
    //tempMustPostRecords = [amenitiesManager numberOfMustPostAmenitiesRecords:userId];
    tempMustPostRecords = [amenitiesManager countTotalAmenitiesByUserId:(int)userId];
    mustPostRecords += tempMustPostRecords;
    
    [alertString appendFormat:@"amenities %d ||", (int)tempMustPostRecords];
    
    //engineering case
    tempMustPostRecords = [EngineerCaseViewManagerV2 numberOfECAttachPhotoMustSyn];
    tempMustPostRecords = [EngineerCaseViewManagerV2 numberOfEngineeringCaseItemMustSyn];
    mustPostRecords += tempMustPostRecords;
    
    [alertString appendFormat:@"engineering case %d ||", (int)tempMustPostRecords];
    
    //lost and found
    //tempMustPostRecords = [[LostandFoundManagerV2 sharedLostandFoundManagerV2] numberOfMustPostLaFRecords];
    mustPostRecords += tempMustPostRecords;
    
    [alertString appendFormat:@"lost and found %d ||", (int)tempMustPostRecords];
    
    //reassign
    tempMustPostRecords = [[RoomManagerV2 sharedRoomManager] numberOfMustPostReassignRecordsWithUserId:userId];
    mustPostRecords += tempMustPostRecords;
    
    [alertString appendFormat:@"reassign %d ||", (int)tempMustPostRecords];
    
    //unassign
    tempMustPostRecords = [[UnassignManagerV2 sharedUnassignManager] numberOfAssignRoomMustSyn];
    mustPostRecords += tempMustPostRecords;
    
    [alertString appendFormat:@"unassign %d ||", (int)tempMustPostRecords];
    
    //guest info
    tempMustPostRecords = [[RoomManagerV2 sharedRoomManager] numberOfMustPostGuestInfoRecordsWithUserId:userId];
    mustPostRecords += tempMustPostRecords;
    
    [alertString appendFormat:@"guest info %d ||", (int)tempMustPostRecords];
    
    //Additional Job Records
    tempMustPostRecords = [[AdditionalJobManagerV2 sharedAddionalJobManager] countAddJobDetailRecordsByUserId:(int)userId postStatus:(int)POST_STATUS_SAVED_UNPOSTED];
    mustPostRecords += tempMustPostRecords;
    [alertString appendFormat:@"additional job time records %d ||", (int)tempMustPostRecords];
    
    //Additional Job Remarks
    tempMustPostRecords = [[AdditionalJobManagerV2 sharedAddionalJobManager] countAddJobDetailsByUserId:(int)userId postStatus:(int)POST_STATUS_SAVED_UNPOSTED];
    mustPostRecords += tempMustPostRecords;
    [alertString appendFormat:@"additional job remarks %d ||", (int)tempMustPostRecords];
    
    // Other Activity
    tempMustPostRecords = [[OtherActivityManager sharedOtherActivityManager] countOtherActivityRecordsByUserId:(int)userId postStatus:(int)POST_STATUS_SAVED_UNPOSTED];
    mustPostRecords += tempMustPostRecords;
    [alertString appendFormat:@"other activity %d ||", (int)tempMustPostRecords];
    
    //room assignment
    if ([UserManagerV2 isSupervisor] == YES) {
        //supervisor
        tempMustPostRecords = [[RoomManagerV2 sharedRoomManager] numberOfMustPostRoomAssignmentRecordsWithSupervisorId:userId];
        mustPostRecords += tempMustPostRecords;
        
        
        tempMustPostRecords = [[RoomManagerV2 sharedRoomManager] countRoomRemarkUnPost];
        mustPostRecords += tempMustPostRecords;
        
        
        [alertString appendFormat:@"room inspection %d ||", (int)tempMustPostRecords];
        
    } else {
        
        //maid
        //tempMustPostRecords = [[RoomManagerV2 sharedRoomManager] numberOfRoomAssignmentMustPost];
       
        //Count unpost room assignment of current user
        tempMustPostRecords = [[RoomManagerV2 sharedRoomManager] countUnpostRoomRecordHistoryByUserId:userId];
        mustPostRecords += tempMustPostRecords;
        [alertString appendFormat:@"room assignment %d ||", (int)tempMustPostRecords];
        
        tempMustPostRecords = [[RoomManagerV2 sharedRoomManager] numberOfRoomServiceLaterMustPost];
        mustPostRecords += tempMustPostRecords;
        [alertString appendFormat:@"room assignment service later %d ||", (int)tempMustPostRecords];
    }
    
    if([LogFileManager isLogConsole]) {
        NSLog(@"%@",alertString);
    }
    //    if (mustPostRecords > 0) {
    //        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Show test sync records" message:alertString delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    //        [alert show];
    //    }
    
    if(!isDemoMode)
    {
        HomeViewV2 *currentHomeView = [HomeViewV2 shareHomeView];
        if (currentHomeView != nil) {
            //[currentHomeView updateRecordSyncWithNumber:mustPostRecords];
            [currentHomeView setBadgeNumberTabbarWith:tagOfSyncNowButton number:[NSNumber numberWithInteger:mustPostRecords]];
        }
    }
}


#pragma mark - PostRoomCleaningStatus
-(BOOL) postRoomCleaningtoServerWithUSerID:(NSInteger)userID AndRoomAssigmentModel:(RoomAssignmentModelV2 *)model{
    BOOL temp = NO;
    temp = [[FindManagerV2 sharedFindManagerV2]updateCleaningStatusWithUserID:userID andRoomAssigmentMode:model];
    return temp;
}

#pragma mark -  Progress step loading
//return percentage step
-(float) caculateStepFromCountingObject:(int) countValue
{
    if(countValue == 0 || countValue == 1)
        return 100;
    
    float stepValue = (float)(100 / (float)countValue);
    return stepValue;
}

#pragma mark - Update Percent View

-(void)updatePercentView:(MBProgressHUD*)percentView value:(float)percentageValue description:(NSString *)description
{
    if (percentView) {
        NSString *percentString = [[NSString alloc]initWithFormat:@"%.0f%%",percentageValue];
        percentView.detailsLabelFont = percentView.labelFont;
        percentView.detailsLabelText = percentString;
        
        float nextStepProgress = [SyncManagerV2 getNextStepOperations];
        int maxSyncOperations = [SyncManagerV2 getMaxSyncOperations];
        if(nextStepProgress > 0 && maxSyncOperations > 0)
        {
            double currentOperations = round(percentView.progress/nextStepProgress);
           // percentView.labelText = [NSString stringWithFormat:@"%@ - [%0.f/%d]", description, currentOperations, maxSyncOperations];
            percentView.labelText = [NSString stringWithFormat:@"[%0.f/%d]", currentOperations, maxSyncOperations];
        }
        else
        {
            percentView.labelText = description;
        }
    }
}

#pragma mark - Room Type Inventory WS

- (BOOL)getRoomTypeInventoryWSByServiceType:(NSInteger)serviceType userId:(NSInteger)userId hotelId:(NSInteger)hotelId {
    BOOL result = YES;
    CountManagerV2 *manager = [[CountManagerV2 alloc] init];
    if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        result = [manager getRoomTypeInventoryWSByServiceType:serviceType userId:userId hotelId:hotelId];
    } else {
        result = NO;
    }
    return result;
}
@end
