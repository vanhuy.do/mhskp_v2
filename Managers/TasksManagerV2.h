//
//  TasksManager.h
//  eHouseKeeping
//
//  Created by KhanhNguyen on 6/8/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "eHousekeepingService.h"
#import "ServiceDefines.h"
#import "RoomAssignmentModelV2.h"
//#import "GuestInfoModel.h"


#import "UserManagerV2.h"
#import "RoomManagerV2.h"

@interface TasksManagerV2 : NSObject {
    NSMutableArray *currentAssignments;
}
@property (nonatomic,retain) NSMutableArray *currentAssignments;

//-(void)postCurrentAssignments;
//-(BOOL)updateTask;
//-(void)postRoomAssignmentSuper:(RoomAssignmentModelV2 *) raModel;
//-(BOOL)postRoomAssignment:(RoomAssignmentModelV2 *)raModel;
//-(void) postAllRoomAssignmentSuper;
-(BOOL)postAllRoomAssignment;

+(NSInteger)getCurrentRoomAssignment;
+(void)setCurrentRoomAssignment:(NSInteger) roomAssignmentId;
+(void)resetCurrentRoomAssignment;
@end
