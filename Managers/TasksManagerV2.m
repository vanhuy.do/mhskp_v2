//
//  TasksManager.m
//  eHouseKeeping
//
//  Created by KhanhNguyen on 6/8/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "TasksManagerV2.h"
//#import "ChecklistManager.h"
#import "ehkDefines.h"
#import "RoomModelV2.h"
//#import "CheckListModel.h"
#import "ehkConvert.h"
#import "RoomAssignmentAdapterV2.h"
//#import "RoomRecordModel.h"

#define dateTimeDefaultReturn  @"1900-01-01 00:00:00" //define value of date time return from server : 1900-01-01 00:00:00

@implementation TasksManagerV2
@synthesize currentAssignments;
static NSInteger _currentRoomAssignment = 0;

/*
-(void)postCurrentAssignments
{
    eHousekeepingServiceSoapBinding *binding = [eHousekeepingService eHousekeepingServiceSoapBinding];
    binding.address=[[NSURL alloc] initWithString:@""];
    //binding.logXMLInOut = YES;
    
}
 */
/*
-(BOOL)updateTask
{
    BOOL returnCode = NO;
    
    eHousekeepingServiceSoapBinding *binding = [eHousekeepingService eHousekeepingServiceSoapBinding];
    //binding.logXMLInOut = YES;
    //request for get RoomAssignment list.
    eHousekeepingService_GetRoomAssignmentList *reqRoomAssignmentList=[[eHousekeepingService_GetRoomAssignmentList alloc] init];
    [reqRoomAssignmentList setIntUsrID:[NSNumber numberWithInt:[[UserManager sharedUserManager] currentUser].user_Id]] ;
    
    // request for get room Detail.
    eHousekeepingService_GetRoomDetail *reqGetRoomDetail=[[eHousekeepingService_GetRoomDetail alloc] init];
    
    //request for get Guestinfo.
    eHousekeepingService_GetGuestInfo *reqGetGuestInfo=[[eHousekeepingService_GetGuestInfo alloc] init];
    
    eHousekeepingServiceSoapBindingResponse *response=[binding GetRoomAssignmentListUsingParameters:reqRoomAssignmentList] ;
    
    NSArray *responseHeaders = response.headers;
    for (id header in responseHeaders) {
        
    }
    
    NSArray *responseBodyParts=response.bodyParts;
    
    NSMutableArray *roomAssignmentListSVC;
    // response for service getRoomAssignment.
    for (id bodyPart in responseBodyParts)
    {
        
        // process error request
        if ([bodyPart isKindOfClass:[SOAPFault class]]) 
        {
            
            continue;
        }
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetRoomAssignmentListResponse class]]) 
        {
            eHousekeepingService_GetRoomAssignmentListResponse *body=(eHousekeepingService_GetRoomAssignmentListResponse*)bodyPart;
            if ([body.GetRoomAssignmentListResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]]) {
                returnCode = YES;
            } else {
                if ([body.GetRoomAssignmentListResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_NOROOM_ASSIGNMENT_IS_FOUND]]) {
                    returnCode = YES;
                } else {
                    returnCode = NO;
                }
            }
            roomAssignmentListSVC=body.GetRoomAssignmentListResult.RoomAssignmentList.RoomAssign;
            
            //@: cleaning data by user current login.
            RoomAssignmentModelV2 *roomAssignmentModel_For_Cleaning_Data=[[RoomAssignmentModelV2 alloc] init];
            roomAssignmentModel_For_Cleaning_Data.roomAssignment_UserId=[[UserManager sharedUserManager] currentUser].user_Id;
            NSMutableArray* roomAssign = [[RoomManagerV2 sharedRoomManager] loadRoomByUserID:roomAssignmentModel_For_Cleaning_Data];
            
            [roomAssignmentModel_For_Cleaning_Data release];
            //  cleaning data of RoomAssignment:
            
            // delete data
            for (RoomAssignmentModelV2 *roomAssignment_delete in roomAssign)
            {
                //RoomAssignmentModel* roomAssignment_delete=[[RoomAssignmentModel alloc] init];
                //                RoomAssignmentModel *roomAssignment_delete=(RoomAssignmentModel*)[roomAssign objectAtIndex:i];
                [[RoomManagerV2 sharedRoomManager] delete:roomAssignment_delete];
                
                //[roomAssignment_delete release];
            }
            
            //add data
            for (int i=0; i<[roomAssignmentListSVC count]; i++) 
            {
                
                RoomAssignmentModelV2 *roomAssignmentModel=[[RoomAssignmentModelV2 alloc] init];
                
                
                eHousekeepingService_RoomAssign *itemRoomAssignmentSVC=(eHousekeepingService_RoomAssign*)[roomAssignmentListSVC objectAtIndex:i];
                
                //[roomAssignmentModel setRoomAssignment_Id:[itemRoomAssignmentSVC.raID intValue]];
                roomAssignmentModel.roomAssignment_Id = [itemRoomAssignmentSVC.raID intValue];
                
                //roomAssignmentModel.roomAssignment_AssignedDate=
                
                roomAssignmentModel.roomAssignment_LastModified=itemRoomAssignmentSVC.raLastModified;
                roomAssignmentModel.roomAssignment_Priority=[itemRoomAssignmentSVC.raPriority intValue];
                roomAssignmentModel.roomAssignment_RoomId=[itemRoomAssignmentSVC.raRoomNo intValue];
                roomAssignmentModel.roomAssignment_UserId=[[UserManager sharedUserManager] currentUser].user_Id;
                roomAssignmentModel.roomAssignment_Priority_Sort_Order = [itemRoomAssignmentSVC.raPrioritySortOrder integerValue];
                roomAssignmentModel.roomAssignment_PostStatus=POST_STATUS_UN_CHANGED;
                // insert data to roomAssignment Table.
                [[RoomManagerV2 sharedRoomManager] insert:roomAssignmentModel];
                //[roomAssignmentModel insert]; 
                
                
                RoomRecordModel *rRecordModel=[[RoomRecordModel alloc] init];
                rRecordModel.rrec_Room_Id=roomAssignmentModel.roomAssignment_RoomId;
                rRecordModel.rrec_User_Id=[UserManager sharedUserManager].currentUser.user_Id;
                [[RoomManagerV2 sharedRoomManager] loadRoomRecord:rRecordModel];
                //[rRecordModel load];
                
                if (rRecordModel.rrec_Id==0)
                {
                    
                    
                    rRecordModel.rrec_Cleaning_Start_Time=[ehkConvert DateToStringWithString:itemRoomAssignmentSVC.raCleanStartTm fromFormat:[ehkConvert getyyyy_MM_dd_HH_mm_ss] toFormat:[ehkConvert getyyyyMMddhhmm]];
                    
                    rRecordModel.rrec_Cleaning_End_Time=[ehkConvert DateToStringWithString:itemRoomAssignmentSVC.raCleanEndTm fromFormat:[ehkConvert getyyyy_MM_dd_HH_mm_ss] toFormat:[ehkConvert getyyyyMMddhhmm]];
                    
                    //                         NSLog(@"insert start time:  %@ and end time : %@ ",rRecordModel.rrec_Cleaning_Start_Time,itemRoomAssignmentSVC.raCleanEndTm);
                    [[RoomManagerV2 sharedRoomManager] insertRoomRecord:rRecordModel];
                }
                else
                {
                    rRecordModel.rrec_Cleaning_Start_Time=[ehkConvert DateToStringWithString:itemRoomAssignmentSVC.raCleanStartTm fromFormat:[ehkConvert getyyyy_MM_dd_HH_mm_ss] toFormat:[ehkConvert getyyyyMMddhhmm]];
                    
                    rRecordModel.rrec_Cleaning_End_Time=[ehkConvert DateToStringWithString:itemRoomAssignmentSVC.raCleanEndTm fromFormat:[ehkConvert getyyyy_MM_dd_HH_mm_ss] toFormat:[ehkConvert getyyyyMMddhhmm]];
                    
                    [[RoomManagerV2 sharedRoomManager] updateRoomRecord:rRecordModel];
                    //[rRecordModel update];
                }
                
                //@ insert info to ROom table 
                
                RoomModelV2 *rModel=[[RoomModelV2 alloc] init];
                rModel.room_Id=[itemRoomAssignmentSVC.raRoomNo intValue];
                rModel.room_CleaningStatusId=[itemRoomAssignmentSVC.raCleaningStatusID intValue];
                rModel.room_StatusId=[itemRoomAssignmentSVC.raRoomStatusID intValue];
                rModel.room_Building_Name=itemRoomAssignmentSVC.raBuildingName;
                rModel.room_TypeId=[itemRoomAssignmentSVC.raRoomTypeID intValue];
                rModel.room_VIPStatusId=[itemRoomAssignmentSVC.raVIP intValue];
                rModel.room_LastModified=itemRoomAssignmentSVC.raLastModified;
                rModel.room_GuestReference=itemRoomAssignmentSVC.raGuestPreference;
                rModel.room_AdditionalJob=itemRoomAssignmentSVC.raAddtionalJob;
                rModel.room_ExpectCleaningTime=itemRoomAssignmentSVC.raExpectedCleaningTime;
                //if([rModel insert]==0)
                if ([[RoomManagerV2
                      sharedRoomManager] insertRoomModel:rModel]==0) 
                {
                    [[RoomManagerV2 sharedRoomManager] updateRoomModel:rModel];
                    //[rModel update];
                }
                
                
                //@ insert info to GUest Info to table local.
                GuestInfoModel *gModel=[[GuestInfoModel alloc] init];
                
                gModel.guest_Room_Id=[itemRoomAssignmentSVC.raRoomNo intValue];
                //gModel.guest_Name=itemRoomAssignmentSVC.raGuestName ;
                [[RoomManagerV2 sharedRoomManager] loadByRoomIDGuestInfo:gModel];
                
                //if([gModel insert]==0)
                //if ([[RoomManager sharedRoomManager] insertGuestInfo:gModel] == 0)
                if (gModel.guest_Name!= nil && [gModel.guest_Name isEqualToString:itemRoomAssignmentSVC.raGuestName])
                {
                    [[RoomManagerV2 sharedRoomManager] updateGuestInfo:gModel];
                    //[gModel update];
                } else {
                    gModel.guest_Name = itemRoomAssignmentSVC.raGuestName;
                    [[RoomManagerV2 sharedRoomManager] insertGuestInfo:gModel];
                }
                
                [gModel release];
                [rModel release];
                [roomAssignmentModel release];
                [rRecordModel release];
                //[itemRoomAssignmentSVC release];
                // roomAssignmentModel=nil;
                
            }
        }
    }
    
    
    //    // get req for get room detail.
    //    // exec req.
    //    if(!roomAssignmentListSVC) return;
    //    for (int i=0; i<[roomAssignmentListSVC count]; i++) 
    //    {
    //        eHousekeepingService_RoomAssign *itemRoomAssignmentSVC=(eHousekeepingService_RoomAssign*)[roomAssignmentListSVC objectAtIndex:i];
    //        [reqGetRoomDetail setIntUsrID:[NSNumber numberWithInt:[[UserManager sharedUserManager] currentUser].user_Id]];
    //        [reqGetRoomDetail setIntRoomAssignID:[NSNumber numberWithInt:[itemRoomAssignmentSVC.raID intValue]]];
    //        
    //        response=[binding GetRoomDetailUsingParameters:reqGetRoomDetail] ;
    //        responseBodyParts=response.bodyParts;
    //        for (id bodyPart in responseBodyParts)
    //        {
    //            
    //            // process error request
    //            if ([bodyPart isKindOfClass:[SOAPFault class]]) 
    //            {
    //                
    //                continue;
    //            }
    //            
    //            
    //            if ([bodyPart isKindOfClass:[eHousekeepingService_GetRoomDetailResponse class]]) 
    //            {
    //                RoomModel *roomModel=[[RoomModel alloc] init];
    //                RoomModel *rLoad = [[RoomModel alloc] init];
    //                
    //                eHousekeepingService_GetRoomDetailResponse *body=(eHousekeepingService_GetRoomDetailResponse*)bodyPart;
    //                eHousekeepingService_RoomAssign *RoomInfoModelSVC=body.GetRoomDetailResult.RoomDetail;
    //                
    //                //@ check exist RoomInfo in local database.
    //                rLoad.room_Id=[RoomInfoModelSVC.raRoomNo intValue];
    //                                
    //                [rLoad load];
    //                //--
    //                //mapping here and then insert data to database.
    //                roomModel.room_Id=[RoomInfoModelSVC.raRoomNo intValue];
    //                roomModel.room_VIPStatusId=[itemRoomAssignmentSVC.raVIP intValue];
    //                roomModel.room_TypeId=[itemRoomAssignmentSVC.raRoomTypeID intValue];// current this property removed.
    //                roomModel.room_StatusId=[itemRoomAssignmentSVC.raRoomStatusID intValue];
    //                roomModel.room_CleaningStatusId=[itemRoomAssignmentSVC.raCleaningStatusID intValue];
    //                roomModel.room_ExpectCleaningTime=itemRoomAssignmentSVC.raExpectedCleaningTime;
    //                roomModel.room_LastModified=itemRoomAssignmentSVC.raLastModified;
    //                roomModel.room_GuestReference=itemRoomAssignmentSVC.raGuestPreference;
    //                roomModel.room_AdditionalJob=itemRoomAssignmentSVC.raAddtionalJob;
    //                roomModel.room_Building_Name=itemRoomAssignmentSVC.raBuildingName;
    //                
    //                // check condition for insert or update data.
    //                // insert data .
    //                if (rLoad.room_LastModified != nil) 
    //                {
    //                    [roomModel update];
    //                }
    //                else
    //                {
    //                    [roomModel insert];
    //                }
    //                //
    //                [roomModel release];
    //                [rLoad release];
    //                
    //            }
    //        }
    //        
    //    }
    
    [reqRoomAssignmentList release];
    [reqGetGuestInfo release];
    [reqGetRoomDetail release];
    
    return returnCode;
}
*/
#pragma mark - Update RoomAssignment Supervisor
/*
-(void)postRoomAssignmentSuper:(RoomAssignmentModelV2 *)raModel{
    if (raModel.roomAssignment_PostStatus == POST_STATUS_POSTED) {
        return;
    }
    eHousekeepingServiceSoapBinding *binding = [eHousekeepingService eHousekeepingServiceSoapBinding];
    eHousekeepingService_UpdateRoomAssignmentSuper *request = [[eHousekeepingService_UpdateRoomAssignmentSuper alloc] init];
    [request setIntUsrID:[NSNumber numberWithInt:[UserManager sharedUserManager].currentUser.user_Id]];
    [request setIntRoomAssignID:[NSNumber numberWithInt:raModel.roomAssignment_Id]];
    
    RoomModelV2 *rModel = [[RoomModelV2 alloc] init];
    rModel.room_Id = raModel.roomAssignment_RoomId;
    
    //[rModel load];
    [request setIntRoomStatusID:[NSNumber numberWithInt:rModel.room_StatusId]];
    
    CheckListModel *chkModel = [[CheckListModel alloc] init];
    chkModel.chklist_Id = raModel.roomAssignment_Id;
    [[ChecklistManager sharedChecklistManager] load:chkModel];
    //[chkModel load];
    [request setDtInspectedDt:[ehkConvert DateToStringWithString:chkModel.chklist_Inspect_Date fromFormat:@"yyyyMMdd:HHmm" toFormat:@"yyyy-MM-dd HH:mm:ss"]];
    [request setIntInspectedStatus:[NSNumber numberWithInt:chkModel.chklist_Status]];
    
    eHousekeepingServiceSoapBindingResponse *response = [binding UpdateRoomAssignmentSuperUsingParameters:request];
    NSArray *responseBodyParts = response.bodyParts;
    for (id bodyPart in responseBodyParts) {
        
        if ([bodyPart isKindOfClass:[SOAPFault class]]) {
            // You can get the error like this:
            
        }
        
                 
        if([bodyPart isKindOfClass:[eHousekeepingService_UpdateRoomAssignmentSuper class]]) {
            eHousekeepingService_UpdateRoomAssignmentSuperResponse *body = (eHousekeepingService_UpdateRoomAssignmentSuperResponse *)bodyPart;
            if ([body.UpdateRoomAssignmentSuperResult.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]]) {
                raModel.roomAssignment_PostStatus = POST_STATUS_POSTED;
                [[RoomManagerV2 sharedRoomManager] update:raModel];
                //[raModel update];
            }
        }
    }
    [request release];
    
    [chkModel release];
    [rModel release];
}
*/
#pragma mark - Update RoomAssignment With Normal user
/*
-(BOOL)postRoomAssignment:(RoomAssignmentModelV2 *)raModel
{
    BOOL returnCode = NO;
    //    if (raModel.roomAssignment_PostStatus == POST_STATUS_POSTED || raModel.roomAssignment_PostStatus==POST_STATUS_UN_CHANGED) {
    //        return;
    //    }
    eHousekeepingServiceSoapBinding *binding = [eHousekeepingService eHousekeepingServiceSoapBinding];
    eHousekeepingService_UpdateRoomAssignment *request = [[eHousekeepingService_UpdateRoomAssignment alloc] init];
    
    // get info from roomDetail :
    RoomModelV2 *roomModel=[[RoomModelV2 alloc] init];
    roomModel.room_Id=raModel.roomAssignment_RoomId;
    [[RoomManagerV2 sharedRoomManager] loadRoomModel:roomModel];
    //[roomModel load];
    
    [request setIntUsrID:[NSNumber numberWithInt:[UserManager sharedUserManager].currentUser.user_Id]];
    [request setIntRoomAssignID:[NSNumber numberWithInt:raModel.roomAssignment_Id]];
    [request setIntRoomStatusID:[NSNumber numberWithInt:roomModel.room_StatusId]];
    [request setIntCleaningStatusID:[NSNumber numberWithInt:roomModel.room_CleaningStatusId]];
    [request setIntPriority:[NSString stringWithFormat:@"%d",raModel.roomAssignment_Priority]]; 
    [request setIntPriorityOrder:[NSString stringWithFormat:@"%d",raModel.roomAssignment_Priority_Sort_Order]]; 
    
    //set time to cleaning :
    RoomRecordModel *rrecordModel=[[RoomRecordModel alloc] init];
    rrecordModel.rrec_Room_Id=raModel.roomAssignment_RoomId;
    rrecordModel.rrec_User_Id=[UserManager sharedUserManager].currentUser.user_Id ;
    [[RoomManagerV2 sharedRoomManager] loadRoomRecord:rrecordModel];
    //[rrecordModel load];
    
    [request setStrCleanStartTm:[ehkConvert DateToStringWithString:rrecordModel.rrec_Cleaning_Start_Time fromFormat:[ehkConvert getyyyyMMddhhmm] toFormat:[ehkConvert getyyyy_MM_dd_HH_mm_ss]]];
    if(rrecordModel.rrec_Cleaning_Start_Time==nil) [request setStrCleanStartTm:dateTimeDefaultReturn]; //@ hard code here 
    
    [request setStrCleanEndTm:[ehkConvert DateToStringWithString:rrecordModel.rrec_Cleaning_End_Time fromFormat:[ehkConvert getyyyyMMddhhmm] toFormat:[ehkConvert getyyyy_MM_dd_HH_mm_ss]]];
    if(rrecordModel.rrec_Cleaning_End_Time==nil) [request setStrCleanEndTm:dateTimeDefaultReturn]; // hard code here
    [rrecordModel release];
    //
    eHousekeepingServiceSoapBindingResponse *response = [binding UpdateRoomAssignmentUsingParameters:request];
    NSArray *responseBodyParts = response.bodyParts;
    for (id bodyPart in responseBodyParts) {
        
        if ([bodyPart isKindOfClass:[SOAPFault class]]) 
        {
            // You can get the error like this:
            
        }
        
        
         // UpdateRoomAssignment
                
        
        raModel.roomAssignment_PostStatus = POST_STATUS_POSTED;
        [[RoomManagerV2 sharedRoomManager] update:raModel];
        //[raModel update];
        
        if([bodyPart isKindOfClass:[eHousekeepingService_UpdateRoomAssignment class]]) {
            eHousekeepingService_UpdateRoomAssignmentResponse *body = (eHousekeepingService_UpdateRoomAssignmentResponse *)bodyPart;
            if ([body.UpdateRoomAssignmentResult.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]]) 
            {
                raModel.roomAssignment_PostStatus = POST_STATUS_POSTED;
                [[RoomManagerV2 sharedRoomManager] update:raModel];
                //[raModel update];
                returnCode = YES;
            } else {
                returnCode = NO;
            }
        }
    }
    
    [request release];
    
    [roomModel release];
    return returnCode;
}
 */
//-------

-(void)postAllRoomAssignmentSuper{
    RoomAssignmentAdapterV2 *raAdapter = [RoomAssignmentAdapterV2 createRoomAssignmentAdapter];
    NSMutableArray *raArray = [NSMutableArray arrayWithArray:[raAdapter loadRoomAssignmentData:NO]];
    for (RoomAssignmentModelV2 *raModel in raArray) {
        if (raModel.roomAssignment_PostStatus == POST_STATUS_POSTED) {
            continue;
        }
//        [self postRoomAssignmentSuper:raModel];
    }
}


//-------post All RoomAssignment with normal user.

-(BOOL)postAllRoomAssignment
{
    BOOL returnCode = YES;
    
    RoomAssignmentAdapterV2 *raAdapter = [RoomAssignmentAdapterV2 createRoomAssignmentAdapter];
    [raAdapter openDatabase];
    NSMutableArray *raArray;// = [NSMutableArray array];
    raArray=[raAdapter loadRoomAssignmentData:NO];
    for (RoomAssignmentModelV2 *raModel in raArray) {
        if (raModel.roomAssignment_PostStatus == POST_STATUS_POSTED || raModel.roomAssignment_PostStatus==POST_STATUS_UN_CHANGED) {
            continue;
        }
//        if ([self postRoomAssignment:raModel] == YES) {
//            
//        } else {
//            returnCode = NO;
//        }
    }
    
    return returnCode;
}
#pragma mark - GetSetCurrentRoomAssignment
+(NSInteger)getCurrentRoomAssignment{
    return _currentRoomAssignment;
}

+(void)setCurrentRoomAssignment:(NSInteger)roomAssignmentId{
    _currentRoomAssignment = roomAssignmentId;
}

+(void)resetCurrentRoomAssignment{
    _currentRoomAssignment = 0;
}


@end
