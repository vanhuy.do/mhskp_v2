//
//  UserManagerV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserAdapterV2.h"
#import "UserModelV2.h"
#import "UserLogModelV2.h"
#import "ServiceDefines.h"
#import "AccessRightModel.h"

#define CURRENT_BUILDING_ID @"CURRENT_BUILDING_ID"

enum UserConfigPermission {
    UserConfigPermission_Unknown = -1,
    UserConfigPermission_CommonUser = 0,
    UserConfigPermission_Supervisor,
    UserConfigPermission_Maid
};

@interface UserManagerV2 : NSObject
{
    UserAdapterV2 *adapter;
    UserModelV2 *currentUser;

}
@property (nonatomic, strong) UserAdapterV2 *adapter;
@property (nonatomic, strong) UserModelV2 *currentUser;
@property (nonatomic, strong) AccessRightModel *currentUserAccessRight;
@property (nonatomic, strong) NSString *strKey, *strIV;
//-(void) postUserData;
-(void) updateUserData:(NSString*)user_name userPassword:(NSString*)user_password;
-(BOOL) signIn:(NSString*)userName userPassword:(NSString*)passWord;
//-(UserModelV2*)getCurrentUser;
//-(void)setCurrentUser:(UserModelV2*)user;
+(BOOL)isSupervisor;

//update get current user.
+ (UserManagerV2 *)sharedUserManager;
+ (UserManagerV2 *) updateUserManagerInstance;

//-(UserModelV2*)currentUser;

-(BOOL)checkExistUser:(NSString*)user_Name userPassword:(NSString*)password;
-(NSInteger) insert:(UserModelV2*)userModel;
-(BOOL)checkExistUser:(UserModelV2*)userModel;
-(BOOL) isAuthenticateUser:(UserModelV2*)userModel;
-(userRoles*) authorization:(UserModelV2*)userModel;
-(void) loadUser:(UserModelV2*)userData;
-(NSMutableArray*) loadAllUsers; //return list of UserModelV2
-(NSMutableArray*) loadAttendanceUsersBySupervisorId:(int)userId;
-(BOOL) updateUserModel:(UserModelV2 *) model;
-(UserModelV2*) getUserByUserId:(int)userId;
-(NSString*)getUserLastModifiedDate;
-(int)getBuildingIdByUserId:(int)userId;

#pragma mark - WS Functions
-(void) getUserConfigurationsFromWS:(NSInteger)userId;
-(NSInteger) getStillLoggedInWSForUserId:(NSInteger)userId deviceNumber:(NSString*)deviceNumber deviceType:(NSInteger)deviceType;
-(NSInteger) postLogOutWSForUserId:(NSInteger)userId;

#pragma mark - User Configurations
-(NSString*)getCommonUserConfigValueByKey:(NSString*)configKey;
-(NSInteger)setCommontUserConfig:(NSString*)configKey value:(NSString*)configValue;
- (NSInteger)setRemarkForCommonUserConfig:(NSString*)configKey value:(NSString*)configValue;
-(NSString*)getCurrentUserConfigValueByKey:(NSString*)configKey;
-(NSInteger)setCurrentUserConfig:(NSString*)configKey value:(NSString*)configValue;
-(NSInteger)insertUserConfiguration:(UserConfigModel*)userConfig;
-(BOOL)updateUserConfigValue:(NSString*) configValue ByUserId:(int)userId andConfigKey:(NSString *)configKey;
-(UserConfigModel*) getUserConfigBy:(int)userId andKey:(NSString*)configKey;
-(NSString*) getConfigValueOfCurrentUserByKey:(NSString*) configKey;
-(NSString*) getConfigValueByUserID:(int)userId AndKey:(NSString*) configKey;
-(BOOL)updateConfigValueOfCurrentUser:(NSString*)configValue andConfigKey:(NSString*)configKey;
-(void)getUserDetailsRoutine:(NSString*)userName withHotel:(NSString*)HTID withTypeID: (NSString*) TypeID;
//if userId <= 0  we will delete all config value for parameter "configKey"
//if configkey.length <= 0 we will delete all config value for parameter "userId"
-(BOOL) deleteUserConfigByUserId:(int)userId andKey:(NSString*)configKey;
-(BOOL) isCurrentUserAlreadyConfiged;
-(void) createDefaultConfigurationsOfCurrentUser;

#pragma mark - User log information
/*!
 CRF-1036: Concurrent License Control By Login Status.
 This function will call WS to check whether user login in different device or not
 */
-(BOOL)shouldLogOutUser;

-(NSInteger)insertUserLog:(UserLogModelV2*)userLogModel;
-(BOOL)updateUserLog:(UserLogModelV2*)userLogModel;
-(UserLogModelV2*) getUserLogByUserId:(int)userId;

@end


