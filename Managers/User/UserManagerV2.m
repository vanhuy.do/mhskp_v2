//
//  UserManagerV2.m
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
#import "UserManagerV2.h"
#import "LogFileManager.h"
#import "LogObject.h"
#import "AccessRightAdapter.h"
#import "ehkDefinesV2.h"
#import "ehkDefines.h"
#import "eHousekeepingService.h"
#import "UserConfigModel.h"
#import "DataBaseExtension.h"
#import "UserManagerV2Demo.h"
#import "DbDefinesV2.h"
#import "RoomManagerV2.h"
#import "eHouseKeepingAppDelegate.h"
#import "NetworkCheck.h"
#import "DeviceManager.h"
#import "Web_x0020_Service.h"
#define isDemoMode ([[NSUserDefaults standardUserDefaults] integerForKey:ENABLE_DEMO_MODE] == 1)

static UserManagerV2 *sharedInstance;

@implementation UserManagerV2
@synthesize adapter;
@synthesize currentUser;
@synthesize currentUserAccessRight = _currentUserAccessRight;


/*
 this function post data from database local to server
 */
/*
 -(void) postUserData{
 
 eHousekeepingServiceSoapBinding *binding = [eHousekeepingService eHousekeepingServiceSoapBinding];
 //binding.logXMLInOut = YES;
 eHousekeepingService_AuthenticateUser *reqAuthenticateUser=[[eHousekeepingService_AuthenticateUser alloc] init];
 
 
 //for release resource.
 [reqAuthenticateUser release];
 [binding release];
 }*/

-(AccessRightModel*) currentUserAccessRight
{
    
    if(!isDemoMode && _currentUserAccessRight == nil)
    {
        _currentUserAccessRight = [self loadAccessRightModel:currentUser.userId];
        //Still nothing data, create temporary data
        if(_currentUserAccessRight == nil){
            _currentUserAccessRight = [[AccessRightModel alloc] init];
        }
    }
    return _currentUserAccessRight;
}

-(BOOL)checkExistUser:(NSString*)user_Name userPassword:(NSString*)password
{
    UserModelV2 *user=[[UserModelV2 alloc] init];
    BOOL check=NO;
    user.userName=user_Name;
    user.userPassword=password;
    check=[self checkExistUser:user];
//    [user release];
    
    return check;
}

/*
 this function update data from server to local database
 */
-(void) updateUserData:(NSString*)user_name userPassword:(NSString*)user_password
{
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole])
    {
        [binding setLogXMLInOut:YES];
    }
//    [binding setDefaultTimeout:10];
    
    eHousekeepingService_AuthenticateUser *reqAuthenticateUser = [[eHousekeepingService_AuthenticateUser alloc] init];
    [reqAuthenticateUser setStrUsrName:user_name];
    [reqAuthenticateUser setStrUsrPassword:user_password];
    
    //Deivce type = 0 is iOS device, Device type = 1 is Android device
    [reqAuthenticateUser setIntDeviceType:[NSNumber numberWithInt:0]];
    
    //[CRF-1036 / 20150512] - send identifier for vendor to server
    [reqAuthenticateUser setStrDeviceNo:[DeviceManager identifierForVendor]];
    
    // CFG [20170627/CRF-00001611] - Save device token to web services.
    if (![[[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"] isEqual: @""]) {
        Web_x0020_Service_ElementPostUpdateDeviceTokenRoutine *request = [[Web_x0020_Service_ElementPostUpdateDeviceTokenRoutine alloc] init];
        Web_x0020_Service_PostUpdateDeviceTokenRequest *requestData = [[Web_x0020_Service_PostUpdateDeviceTokenRequest alloc] init];
        [requestData setUserName:user_name];
        [requestData setPassword:user_password];
        [requestData setDeviceToken:[[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"]];
        [requestData setDeviceTokenType:@"1"];
        [request setPostUpdateDeviceTokenRequest:requestData];
        
        Web_x0020_Service_Web_x0020_ServiceSoap12Binding *binding = [Web_x0020_Service Web_x0020_ServiceSoap12Binding];
        if([LogFileManager isLogConsole]){
            [binding setLogXMLInOut:YES];
        }
        Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *response = [binding PostUpdateDeviceTokenRoutine:request];
        
        NSArray *responseBodyParts = response.bodyParts;
        for (id bodyPart in responseBodyParts) {
            if ([bodyPart isKindOfClass:[Web_x0020_Service_ElementPostUpdateDeviceTokenRoutineResponse class]]){
                
                Web_x0020_Service_ElementPostUpdateDeviceTokenRoutineResponse *dataBody = (Web_x0020_Service_ElementPostUpdateDeviceTokenRoutineResponse *)bodyPart;
                if (dataBody) {
                    if ([dataBody.PostUpdateDeviceTokenRoutineResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInt:0]]) {
                    }
                }
            }
        }
    }
    // CFG [20170627/CRF-00001611] - End.
    
    UserModelV2 *model = [[UserModelV2 alloc] init];
    model.userName = user_name;
    [self loadUser:model];
    if (model.userId != 0) {
//        [reqAuthenticateUser setStrLastModified:model.userLastModified];
    }
    
//    [reqAuthenticateUser setStrLastModified:@"2010-10-10"];
    
    LogObject *logObj = [[LogObject alloc] init];
    logObj.dateTime = [NSDate date];
    logObj.log = [NSString stringWithFormat:@"<><updateUserData><UsrName:%@ UsrPassword:******** lastModified:%@>",user_name,model.userLastModified];
    LogFileManager *logManager = [[LogFileManager alloc] init];
    [logManager save:logObj];
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding AuthenticateUserUsingParameters:reqAuthenticateUser];
    
    NSArray *responseBodyParts = response.bodyParts;
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_AuthenticateUserResponse class]]) {
            eHousekeepingService_AuthenticateUserResponse *body = bodyPart;
            eHousekeepingService_UserDetail *userModelSvc = body.AuthenticateUserResult.UserDetail;
            if ([body.AuthenticateUserResult.ResponseStatus.respCode integerValue] == RESPONSE_STATUS_USER_SUCCESSFULLY_AUTHENTICATED) {
                
                //Remove for disccussing with Chris
                /*
                //hold current value of language status because reset database will set IS_LOAD_LANGUAGE to NO
                BOOL isAlreadyLoadLanguage = [[NSUserDefaults standardUserDefaults] boolForKey:IS_LOAD_LANGUAGE];
                //only reset database, not language
                [((eHouseKeepingAppDelegate*)[[UIApplication sharedApplication] delegate]) resetLocalDatabase];
                [[NSUserDefaults standardUserDefaults] setBool:isAlreadyLoadLanguage forKey:IS_LOAD_LANGUAGE];
                */
                
                UserModelV2 *userModel = [[UserModelV2 alloc] init];
                userModel.userId = [userModelSvc.usrID intValue];
                userModel.userName = userModelSvc.usrName;
                userModel.userFullName = userModelSvc.usrFullName;
                userModel.userLang = userModelSvc.usrLang;
                if (userModelSvc.usrPassword) {
                     userModel.userPassword = userModelSvc.usrPassword;
                } else {
                    userModel.userPassword = user_password;
                }
               
                if([LogFileManager isLogConsole]) {
                    UserConfigModel *userConfig = [[UserConfigModel alloc] init];
                    userConfig.userId = userModel.userId;
                    userConfig.value = user_password;
                    NSString *keyConfig = [NSString stringWithFormat:@"%@", DEBUG_USER_COMFIG_PASSWORD];
                    userConfig.key = keyConfig;
                    
                    NSString *valueCompare = [self getConfigValueByUserID:userConfig.userId AndKey:keyConfig];
                    if(valueCompare.length <= 0)
                    {
                        [self insertUserConfiguration:userConfig];
                    }
                    else
                    {
                        [self updateUserConfigValue:userConfig.value ByUserId:userModel.userId andConfigKey:keyConfig];
                    }
                }
                
                userModel.userTitle = userModelSvc.usrTitle;
                userModel.userSupervisorId = [userModelSvc.usrIsSupervisor intValue];
//                userModel.userIsAdmin = [userModelSvc.usrIsAdmin intValue];
                userModel.userDepartmentId = [userModelSvc.usrDeptID intValue];
                userModel.userDepartment = userModelSvc.usrDept;
//                userModel.userLangCode = userModelSvc.usrLangCode;
                userModel.userHotelsId = [userModelSvc.usrHotelID intValue];
                
                //Hao Tran [20141126/Save building ID] - Get Building Id
                NSInteger buildingId = [userModelSvc.usrBuilding integerValue];
                [[NSUserDefaults standardUserDefaults] setInteger:buildingId forKey:[NSString stringWithFormat:@"%@%d", CURRENT_BUILDING_ID, userModel.userId]];
                
//                userModel.userIsActive = userModelSvc.usrIsActive;
                userModel.userLastModified = userModelSvc.usrLastModified;
                userModel.userAllowReorderRoom = [userModelSvc.intAllowReorderRoomAssignment integerValue];
                userModel.userAllowBlockRoom = [userModelSvc.intAllowBlockVacantRoom integerValue];
                
                //check if user is existing in local db ==> update usermodel, otherwise insert
                
                if (model.userId == userModel.userId) {
                    //update
                    [self updateUserModel:userModel];
                }
                else {
                    //insert data
                    [self insert:userModel];
                }
                
                AccessRightModel *accessRightModel = [self loadAccessRightModel:userModel.userId];
                [self getAccessRightFromWS:userModel.userId WithOption:(accessRightModel.userId == 0)];
                [self getSecretsKey:user_name withPassword:user_password];
                //Hao Tran [20130708/Get User Config] - Get User config from WS
                [self getUserConfigurationsFromWS:userModel.userId];
                
            }
        }
    }
}

- (void)getSecretsKey:(NSString*)userName withPassword:(NSString*)password{
    //    NSMutableArray *listResult = [NSMutableArray new];
    Web_x0020_Service_Web_x0020_ServiceSoap12Binding *binding = [Web_x0020_Service Web_x0020_ServiceSoap12Binding];
    if([LogFileManager isLogConsole]){
        [binding setLogXMLInOut:YES];
    }
    
    Web_x0020_Service_ElementGetSecretsRoutine *request = [[Web_x0020_Service_ElementGetSecretsRoutine alloc] init];
    Web_x0020_Service_GetSecretsRequest *requestData = [[Web_x0020_Service_GetSecretsRequest alloc] init];
    [requestData setUserName:userName];
    [requestData setPassword:password];
    [request setGetSecretsRequest:requestData];
    
    
    Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *response = [binding GetSecretsRoutine:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    for (id bodyPart in responseBodyParts) {
        if ([bodyPart isKindOfClass:[Web_x0020_Service_ElementGetSecretsRoutineResponse class]]){
            
            Web_x0020_Service_ElementGetSecretsRoutineResponse *dataBody = (Web_x0020_Service_ElementGetSecretsRoutineResponse *)bodyPart;
            if (dataBody) {
                if ([dataBody.GetSecretsRoutineResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInt:0]]) {
                    NSString *valueKeyAPI = dataBody.GetSecretsRoutineResult.DesExtendedSecretKey;
                    NSString *valueIVAPI = dataBody.GetSecretsRoutineResult.DesExtendedInitializationVector;
                    [UserManagerV2 sharedUserManager].strKey = valueKeyAPI;
                    [UserManagerV2 sharedUserManager].strIV = valueIVAPI;
                }
            }
        }
    }
}

-(void) getAccessRightFromWS:(NSInteger)userId WithOption:(BOOL) isNew
{
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole])
    {
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_GetAccessRights *getAccessRightRequest = [[eHousekeepingService_GetAccessRights alloc] init];
    [getAccessRightRequest setIntUsrID:[NSNumber numberWithInteger:userId]];
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetAccessRightsUsingParameters:getAccessRightRequest];
    
    for (id bodyPart in response.bodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetAccessRightsResponse class]]) {
            eHousekeepingService_GetAccessRightsResponse *body = bodyPart;
            BOOL isAllowedView, isAllowedEdit, isActive;
            AccessRightModel *accessRightModel = [[AccessRightModel alloc] initWithDefaultInfo];
            
            accessRightModel.userId = userId;
            
            for (eHousekeepingService_Rights *right in body.GetAccessRightsResult.AccessRightsList.Rights) {
                isAllowedView = ([right.arView integerValue] == 1);
                isAllowedEdit = ([right.arAdd integerValue] == 1 || [right.arEdit integerValue] == 1);
                isActive = ([right.arAccess integerValue] == 1);
                
                NSInteger value = (isAllowedView?IS_ALLOWED_VIEW:0)|(isAllowedEdit?IS_ALLOWED_EDIT:0)|(isActive?IS_ACTIVE:0);
                if ([ROOM_ASSIGNMENT_ROOM_ASSIGNMENT isEqualToString:right.arModule]) {
                    accessRightModel.roomAssignment = value;
                }
                if ([ROOM_ASSIGNMENT_MANUAL_CREDIT isEqualToString:right.arModule]) {
                    accessRightModel.roomAssignmentManualCredit = value;
                }
                if ([ROOM_ASSIGNMENT_CHECKLIST_REMARKS isEqualToString:right.arModule]) {
                    accessRightModel.roomAssignmentChecklistRemark = value;
                }
                if ([SYSTEM_ENABLE_ENCRYPTED_QRCODE isEqualToString:right.arModule]) {
                    accessRightModel.enableEncryptedQRCode = value;
                }
                
                else if ([ROOM_ASSIGNMENT_COMPLETED_ROOM isEqualToString:right.arModule]) {
                    accessRightModel.completedRoom = value;
                }
                else if ([ROOM_ASSIGNMENT_MANUAL_UPDATE_ROOM_STATUS isEqualToString:right.arModule]) {
                    accessRightModel.manualUpdateRoomStatus = value;
                }
                else if ([ROOM_ASSIGNMENT_RESTRICTION_ASSIGNMENT isEqualToString:right.arModule]) {
                    accessRightModel.restrictionAssignment = value;
                }
                else if ([ROOM_ASSIGNMENT_QR_CODE_SCANNER isEqualToString:right.arModule]) {
                    accessRightModel.QRCodeScanner = value;
                }
                else if ([POSTING_LOST_AND_FOUND isEqualToString:right.arModule]) {
                    accessRightModel.postLostAndFound = value;
                }
                else if ([POSTING_ENGINEERING isEqualToString:right.arModule]) {
                    accessRightModel.postEngineering = value;
                }
                else if ([POSTING_LAUNDRY isEqualToString:right.arModule]) {
                    accessRightModel.postLaundry = value;
                }
                else if ([POSTING_MINIBAR isEqualToString:right.arModule]) {
                    accessRightModel.postMinibar = value;
                }
                else if ([POSTING_LINEN isEqualToString:right.arModule]) {
                    accessRightModel.postLinen = value;
                }
                else if ([POSTING_AMENITIES isEqualToString:right.arModule]) {
                    accessRightModel.postAmenities = value;
                }
                else if ([POSTING_PHYSICAL_CHECK isEqualToString:right.arModule]) {
                    accessRightModel.postPhysicalCheck = value;
                }
                else if ([ACTION_LOST_AND_FOUND isEqualToString:right.arModule]) {
                    accessRightModel.actionLostAndFound = value;
                }
                else if ([ACTION_ENGINEERING isEqualToString:right.arModule]) {
                    accessRightModel.actionEngineering = value;
                }
                else if ([ACTION_LAUNDRY isEqualToString:right.arModule]) {
                    accessRightModel.actionLaundry = value;
                }
                else if ([ACTION_MINIBAR isEqualToString:right.arModule]) {
                    accessRightModel.actionMinibar = value;
                }
                else if ([ACTION_LINEN isEqualToString:right.arModule]) {
                    accessRightModel.actionLinen = value;
                }
                else if ([ACTION_AMENITIES isEqualToString:right.arModule]) {
                    accessRightModel.actionAmenities = value;
                }
                else if ([ACTION_GUIDELINE isEqualToString:right.arModule]) {
                    accessRightModel.actionGuideLine = value;
                }
                else if ([ACTION_CHECKLIST isEqualToString:right.arModule]) {
                    accessRightModel.actionCheckList = value;
                }else if ([MESSAGE_MESSAGE isEqualToString:right.arModule]) {
                    accessRightModel.message = value;
                }else if ([FIND_ATTENDANT_PENDING_ROOM isEqualToString:right.arModule]) {
                    accessRightModel.findAttendantPendingCompleted = value;
                }else if ([FIND_ROOM isEqualToString:right.arModule]) {
                    accessRightModel.findRoom = value;
                }else if ([FIND_INSPECTION isEqualToString:right.arModule]) {
                    accessRightModel.findInspection = value;
                }else if ([SETTING_SETTING isEqualToString:right.arModule]) {
                    accessRightModel.setting = value;
                }else if ([FIND_ROOM_REMARKS_UPDATE isEqualToString:right.arModule]) {
                    accessRightModel.findRoomRemarkUpdate = value;
                }else if ([POSTING_FUNCTION_ACTION_HISTORY isEqualToString:right.arModule]) {
                    accessRightModel.isShowActionHistoryPosting = value;
                }else if ([POSTING_FUNCTION_MINIBAR_NEW isEqualToString:right.arModule]) {
                    accessRightModel.isPostingFunctionMinibarNew = value;
                }else if ([POSTING_FUNCTION_MINIBAR_USED isEqualToString:right.arModule]) {
                    accessRightModel.isPosingFunctionMinibarUsed = value;
                }else if ([ROOM_DETAILS_ACTION_MINIBAR_NEW isEqualToString:right.arModule]) {
                    accessRightModel.isRoomDetailsActionMinibarNew = value;
                }else if ([ROOM_DETAILS_ACTION_MINIBAR_USED isEqualToString:right.arModule]) {
                    accessRightModel.isRoomDetailsActionMinibarUsed = value;
                }else if ([POSTING_FUNCTION_AMENITIES_QR_CODE isEqualToString:right.arModule]) {
                    accessRightModel.isPostingFunctionAmenitiesQRCode = value;
                }else if ([POSTING_FUNCTION_ENGINEERING_QR_CODE isEqualToString:right.arModule]) {
                    accessRightModel.isPostingFunctionEngineeringQRCode = value;
                }else if ([POSTING_FUNCTION_LINEN_QR_CODE isEqualToString:right.arModule]) {
                    accessRightModel.isPostingFunctionLinenQRCode = value;
                }else if ([POSTING_FUNCTION_LOST_AND_FOUND_QR_CODE isEqualToString:right.arModule]) {
                    accessRightModel.isPostingFunctionLostAndFoundQRCode = value;
                }else if ([POSTING_FUNCTION_MINIBAR_QR_CODE isEqualToString:right.arModule]) {
                    accessRightModel.isPostingFunctionMinibarQRCode = value;
                }else if ([POSTING_FUNCTION_PHYSICAL_CHECK_QR_CODE isEqualToString:right.arModule]) {
                    accessRightModel.isPostingFunctionPhysicalCheckQRCode = value;
                }else if ([ADDTIONAL_JOB isEqualToString:right.arModule]){
                    
                    UserConfigModel *userConfig = [[UserConfigModel alloc] init];
                    userConfig.userId = (int)userId;
                    
                    //View Additional Job
                    if(isAllowedView){
                        userConfig.key = USER_CONFIG_IS_ADDJOB_VIEW;
                        userConfig.value = @"true";
                    } else {
                        userConfig.key = USER_CONFIG_IS_ADDJOB_VIEW;
                        userConfig.value = @"false";
                    }
                    
                    UserConfigModel *userConfigExisting = [self getUserConfigBy:(int)userId andKey:USER_CONFIG_IS_ADDJOB_VIEW];
                    if(userConfigExisting){
                        [self updateUserConfigValue:userConfig.value ByUserId:(int)userId andConfigKey:USER_CONFIG_IS_ADDJOB_VIEW];
                    } else {
                        [self insertUserConfiguration:userConfig];
                    }
                    
                    //Active Addtional Job
                    if(isActive){
                        userConfig.key = USER_CONFIG_IS_ADDJOB_ACTIVE;
                        userConfig.value = @"true";
                    } else {
                        userConfig.key = USER_CONFIG_IS_ADDJOB_ACTIVE;
                        userConfig.value = @"false";
                    }
                    
                    userConfigExisting = [self getUserConfigBy:(int)userId andKey:USER_CONFIG_IS_ADDJOB_ACTIVE];
                    if(userConfigExisting){
                        [self updateUserConfigValue:userConfig.value ByUserId:(int)userId andConfigKey:USER_CONFIG_IS_ADDJOB_ACTIVE];
                    } else {
                        [self insertUserConfiguration:userConfig];
                    }
                }
                else if ([ROOM_DETAILS_ACTION_HISTORY isEqualToString:right.arModule]) {
                    
                    UserConfigModel *userConfig = [[UserConfigModel alloc] init];
                    userConfig.userId = (int)userId;
                    
                    if(isAllowedView) {
                        userConfig.key = USER_CONFIG_ROOM_DETAILS_ACTION_HISTORY_VIEW;
                        userConfig.value = @"true";
                    } else {
                        userConfig.key = USER_CONFIG_ROOM_DETAILS_ACTION_HISTORY_VIEW;
                        userConfig.value = @"false";
                    }
                    
                    UserConfigModel *userConfigExisting = [self getUserConfigBy:(int)userId andKey:USER_CONFIG_ROOM_DETAILS_ACTION_HISTORY_VIEW];
                    if(userConfigExisting) {
                        [self updateUserConfigValue:userConfig.value ByUserId:(int)userId andConfigKey:USER_CONFIG_ROOM_DETAILS_ACTION_HISTORY_VIEW];
                    } else {
                        [self insertUserConfiguration:userConfig];
                    }
                    
                    if(isActive){
                        userConfig.key = USER_CONFIG_ROOM_DETAILS_ACTION_HISTORY_ACTIVE;
                        userConfig.value = @"true";
                    } else {
                        userConfig.key = USER_CONFIG_ROOM_DETAILS_ACTION_HISTORY_ACTIVE;
                        userConfig.value = @"false";
                    }
                    
                    userConfigExisting = [self getUserConfigBy:(int)userId andKey:USER_CONFIG_ROOM_DETAILS_ACTION_HISTORY_ACTIVE];
                    if(userConfigExisting) {
                        [self updateUserConfigValue:userConfig.value ByUserId:(int)userId andConfigKey:USER_CONFIG_ROOM_DETAILS_ACTION_HISTORY_ACTIVE];
                    } else {
                        [self insertUserConfiguration:userConfig];
                    }
                }
                else if ([FIND_ACTION_HISTORY isEqualToString:right.arModule]) {
                    
                    UserConfigModel *userConfig = [[UserConfigModel alloc] init];
                    userConfig.userId = (int)userId;
                    
                    if(isAllowedView) {
                        userConfig.key = USER_CONFIG_FIND_ACTION_HISTORY_VIEW;
                        userConfig.value = @"true";
                    } else {
                        userConfig.key = USER_CONFIG_FIND_ACTION_HISTORY_VIEW;
                        userConfig.value = @"false";
                    }
                    
                    UserConfigModel *userConfigExisting = [self getUserConfigBy:(int)userId andKey:USER_CONFIG_FIND_ACTION_HISTORY_VIEW];
                    if(userConfigExisting) {
                        [self updateUserConfigValue:userConfig.value ByUserId:(int)userId andConfigKey:USER_CONFIG_FIND_ACTION_HISTORY_VIEW];
                    } else {
                        [self insertUserConfiguration:userConfig];
                    }
                    
                    if(isActive){
                        userConfig.key = USER_CONFIG_FIND_ACTION_HISTORY_ACTIVE;
                        userConfig.value = @"true";
                    } else {
                        userConfig.key = USER_CONFIG_FIND_ACTION_HISTORY_ACTIVE;
                        userConfig.value = @"false";
                    }
                    
                    userConfigExisting = [self getUserConfigBy:(int)userId andKey:USER_CONFIG_FIND_ACTION_HISTORY_ACTIVE];
                    if(userConfigExisting) {
                        [self updateUserConfigValue:userConfig.value ByUserId:(int)userId andConfigKey:USER_CONFIG_FIND_ACTION_HISTORY_ACTIVE];
                    } else {
                        [self insertUserConfiguration:userConfig];
                    }
                }
            }
            
            if (isNew) {
                [self insertAccessRightModel:accessRightModel];
            }
            else {
                [self updateAccessRightModel:accessRightModel];
            }

            self.currentUserAccessRight = accessRightModel;
        }
    }
}
#pragma mark - User Configurations WS
-(void) getUserConfigurationsFromWS:(NSInteger)userId
{
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    if([LogFileManager isLogConsole]){
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_GetCommonConfigurations *getUserConfigurationsRequest = [[eHousekeepingService_GetCommonConfigurations alloc] init];
    [getUserConfigurationsRequest setIntUsrID:[NSNumber numberWithInteger:userId]];
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetCommonConfigurationsUsingParameters:getUserConfigurationsRequest];
    
    for (id bodyPart in response.bodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetCommonConfigurationsResponse class]]) {
            eHousekeepingService_GetCommonConfigurationsResponse *body = bodyPart;
            eHousekeepingService_CommonConfigurationsList *configList = body.GetCommonConfigurationsResult;
            
            for (eHousekeepingService_WSSettings *currentSetting in configList.CommonConfigurationsList.WSSettings) {
                UserConfigModel *userConfig = [self getUserConfigBy:(int)userId andKey:currentSetting.ccModule];
 
                //Check existing data
                if(userConfig != nil){
                    [self updateUserConfigValue:currentSetting.ccValue ByUserId:(int)userId andConfigKey:currentSetting.ccModule];
                } else {
                    userConfig = [[UserConfigModel alloc] init];
                    userConfig.userId = (int)userId;
                    userConfig.key = currentSetting.ccModule;
                    userConfig.value = currentSetting.ccValue;
                    [self insertUserConfiguration:userConfig];
                }
            }
        }
    }
}

//[20150512 / CRF-1036] - As discuss with Chris, will logout current user if response code different to 1000 (1000 means OK)
-(NSInteger) getStillLoggedInWSForUserId:(NSInteger)userId deviceNumber:(NSString*)deviceNumber deviceType:(NSInteger)deviceType
{
    int result = RESPONSE_STATUS_OK;
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    if([LogFileManager isLogConsole]){
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_GetStillLoggedIn *getStillLoggedIn = [[eHousekeepingService_GetStillLoggedIn alloc] init];
    [getStillLoggedIn setIntUsrID:[NSNumber numberWithInteger:userId]];
    [getStillLoggedIn setStrDeviceNo:deviceNumber];
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetStillLoggedInUsingParameters:getStillLoggedIn];
    
    for (id bodyPart in response.bodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetStillLoggedInResponse class]]) {
            eHousekeepingService_GetStillLoggedInResponse *body = bodyPart;
            eHousekeepingService_ResponseStatus *responseStatus = body.GetStillLoggedInResult;
            
            if (responseStatus) {
                result =  [responseStatus.respCode intValue];
            }
        }
    }
    
    return result;
}

-(NSInteger) postLogOutWSForUserId:(NSInteger)userId
{
    int result = RESPONSE_STATUS_ERROR;
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    if([LogFileManager isLogConsole]){
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_PostLogout *postLogout = [[eHousekeepingService_PostLogout alloc] init];
    [postLogout setIntUsrID:[NSNumber numberWithInteger:userId]];
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding PostLogoutUsingParameters:postLogout];
    
    for (id bodyPart in response.bodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_PostLogoutResponse class]]) {
            eHousekeepingService_PostLogoutResponse *body = bodyPart;
            eHousekeepingService_ResponseStatus *responseStatus = body.PostLogoutResult;
            
            result =  [responseStatus.respCode intValue];
        }
    }
    
    return result;
}

/*
 signin offline then set current user.
 */
-(BOOL) signIn:(NSString*)userName userPassword:(NSString*)passWord
{
    BOOL check=NO;
    UserModelV2 *user=[[UserModelV2 alloc] init];
    user.userName = userName;
    user.userPassword = passWord;
    
    check = [self isAuthenticateUser:user];
    if (check) {
        [self loadUser:user];
        [sharedInstance setCurrentUser:user];
        
    }
    
    BOOL isUserDefaultConfiged = [self isCurrentUserAlreadyConfiged];
    if(!isUserDefaultConfiged){
        [self createDefaultConfigurationsOfCurrentUser];
    }
    
    return check;
}
-(UserModelV2*)getCurrentUser
{
    @synchronized(self) {
		return currentUser;
	}	
}

/*
 -(UserModel*)currentUser
 {
 @synchronized(self) {
 return currentUser;
 }	
 }
 */
+(BOOL)isSupervisor{
    if ( [[self sharedUserManager] currentUser] != nil) {
        if ([[self sharedUserManager] currentUser].userSupervisorId) {
            return YES;
        }
    }
    return NO;
}
////
/*
 static UserManager* sharedUserManagerInstance = nil;
 
 + (UserManager*) sharedUserManager{
 if (sharedUserManagerInstance == nil) {
 sharedUserManagerInstance = [[super allocWithZone:NULL] init];
 }
 return sharedUserManagerInstance;
 }
 + (id)allocWithZone:(NSZone *)zone
 {
 return [[self sharedUserManager] retain];
 }
 */
////

//+ (UserManagerV2 *)sharedUserManager
//{
////    @synchronized (self) {
////		if (sharedInstance == nil) {
////			sharedInstance = [[self alloc] init]; // assignment not done here, see allocWithZone
////		}
////	}
////	
////	return sharedInstance;
//    
//    static dispatch_once_t onceToken;
//    dispatch_once(&onceToken, ^{
//        if([[NSUserDefaults standardUserDefaults] integerForKey:ENABLE_DEMO_MODE] == 1)
//        {
//            sharedInstance = [[UserManagerV2_demo alloc] init];
//        }
//        else{
//            sharedInstance = [[UserManagerV2 alloc] init];
//        }
//        // Do any other initialisation stuff here
//    });
//    return sharedInstance;
//}

+ (UserManagerV2 *)sharedUserManager {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if(isDemoMode) {
            if (sharedInstance == nil || ![sharedInstance isKindOfClass:UserManagerV2Demo.class]) {
                sharedInstance = [[UserManagerV2Demo alloc] init];
            }
        } else {
            if (sharedInstance == nil || [sharedInstance isKindOfClass:UserManagerV2Demo.class]) {
                sharedInstance = [[UserManagerV2 alloc] init];
            }
        }
    });
    return sharedInstance;
}

+ (UserManagerV2 *) updateUserManagerInstance{
    if(isDemoMode) {
        if (sharedInstance == nil || ![sharedInstance isKindOfClass:UserManagerV2Demo.class]) {
            sharedInstance = [[UserManagerV2Demo alloc] init];
        }
    } else {
        if (sharedInstance == nil || [sharedInstance isKindOfClass:UserManagerV2Demo.class]) {
            sharedInstance = [[UserManagerV2 alloc] init];
        }
    }
    return sharedInstance;
}
//+ (id)allocWithZone:(NSZone *)zone
//{
//    @synchronized(self) {
//        if (sharedInstance == nil) {
//            sharedInstance = [super allocWithZone:zone];
//            return sharedInstance;  // assignment and return on first allocation
//        }
//    }
//	
//    return nil; //on subsequent allocation attempts return nil
//}

//- (id)copyWithZone:(NSZone *)zone
//{
//    return self;
//}

//- (id)retain
//{
//    return self;
//}

//- (id)autorelease
//{
//    return self;
//}

//- (NSUInteger)retainCount
//{
//    return NSUIntegerMax;  // This is sooo not zero
//}

- (id)init
{
	@synchronized(self) {
//		[super init];	
		currentUser = [[UserModelV2 alloc] init];
		return self;
	}
}

-(void)dealloc
{
//    [adapter release];
//    [currentUser release];
//    [super dealloc];
    
}
/*
 -(void)setCurrentUser:(UserModel*)user
 {
 
 @synchronized(self) {
 if (currentUser != user) {
 //[currentUser release];
 currentUser = [user retain];
 NSLog(@"%d",currentUser.user_Id);
 }
 }
 
 }*/

#pragma mark -
#pragma mark insert, load data for usermodel

//HaoTran[20130607/ Get last date modified] - Get last user modified date by current user
-(NSString*)getUserLastModifiedDate
{
    UserAdapterV2 *adapter1 = [UserAdapterV2 createUserAdapter];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    NSString *result = nil;
    result = [adapter1 getUserLastModifiedDate];
    [adapter1 close];
    //[adapter1 release];
    adapter1 = nil;
    return result;
}
//HaoTran[20130607/ Get last date modified] - END

//HaoTran[20130607/ Get User by User ID] - Get User by User ID
-(UserModelV2*) getUserByUserId:(int)userId
{
    UserModelV2* result = nil;
    UserAdapterV2 *adapter1 = [UserAdapterV2 createUserAdapter];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    result = [adapter1 getUserByUserId:userId];
    [adapter1 close];
    adapter1 = nil;
    return result;
}
//HaoTran[20130607/ Get User by User ID] - END

-(NSInteger)insert:(UserModelV2*)userModel
{
    /*
     if (adapter == nil) {
     adapter = [[UserAdapter alloc] init];
     [adapter openDatabase];
     [adapter resetSqlCommand];
     return [adapter insertUserData:userModel];
     [adapter close];
     [adapter release];
     adapter = nil;
     } else {
     [adapter openDatabase];
     [adapter resetSqlCommand];
     return [adapter insertUserData:userModel];
     [adapter close];
     }*/
    UserAdapterV2 *adapter1 = [UserAdapterV2 createUserAdapter];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    NSInteger returnValue = [adapter1 insertUserData:userModel];
    [adapter1 close];
//    [adapter1 release];
    return returnValue;
}
-(BOOL)checkExistUser:(UserModelV2*)userModel
{
    /*
     if (adapter == nil) {
     adapter = [[UserAdapter alloc] init];
     [adapter openDatabase];
     [adapter resetSqlCommand];
     BOOL result=NO;
     result=[adapter checkExistUser:userModel];
     [adapter close];
     [adapter release];
     adapter = nil;
     return result;
     } else {
     [adapter openDatabase];
     [adapter resetSqlCommand];
     BOOL result=NO;
     result=[adapter checkExistUser:userModel];
     [adapter close];
     return result;
     }
     */
    UserAdapterV2 *adapter1 = [UserAdapterV2 createUserAdapter];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    BOOL result=NO;
    result=[adapter1 checkExistUser:userModel];
    [adapter1 close];
//    [adapter1 release];
    adapter1 = nil;
    return result;
}
/**/
-(BOOL)isAuthenticateUser:(UserModelV2*)userModel
{
    /*
     if (adapter == nil) {
     adapter = [[UserAdapter alloc] init];
     [adapter openDatabase];
     [adapter resetSqlCommand];
     BOOL result = [adapter isAuthenticate:userModel];
     [adapter close];
     [adapter release];
     adapter = nil;
     return result;
     } else {
     [adapter openDatabase];
     [adapter resetSqlCommand];
     BOOL result = [adapter isAuthenticate:userModel];
     [adapter close];
     return result;
     }*/
    UserAdapterV2 *adapter1 = [UserAdapterV2 createUserAdapter];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    BOOL result = [adapter1 isAuthenticate:userModel];
    [adapter1 close];
//    [adapter1 release];
    adapter1 = nil;
    return result;
}
-(userRoles*)authorization:(UserModelV2*)userModel
{
    /*
     if (adapter == nil) {
     adapter = [[UserAdapter alloc] init];
     [adapter openDatabase];
     [adapter resetSqlCommand];
     userRoles *role = [adapter getRole:userModel];
     [adapter close];
     [adapter release];
     adapter = nil;
     return role;
     } else {
     [adapter openDatabase];
     [adapter resetSqlCommand];
     userRoles *role = [adapter getRole:userModel];
     [adapter close];
     return role;
     }*/
    UserAdapterV2 *adapter1 = [UserAdapterV2 createUserAdapter];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    userRoles *role = [adapter1 getRole:userModel];
    [adapter1 close];
//    [adapter1 release];
    adapter1 = nil;
    return role;
}
-(void) loadUser:(UserModelV2*)userData
{
    /*
     if (adapter == nil) {
     adapter = [[UserAdapter alloc] init];
     [adapter openDatabase];
     [adapter resetSqlCommand];
     [adapter loadUserData:userData];
     [adapter close];
     [adapter release];
     adapter = nil;
     } else {
     [adapter openDatabase];
     [adapter resetSqlCommand];
     [adapter loadUserData:userData];
     [adapter close];
     }*/
    UserAdapterV2 *adapter1 = [UserAdapterV2 createUserAdapter];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    [adapter1 loadUserData:userData];
    [adapter1 close];
//    [adapter1 release];
    adapter1 = nil;
}

-(NSMutableArray*) loadAllUsers
{
    DataTable *tableUsers = [[DataTable alloc] initWithName:[NSString stringWithFormat:@"%@",USER]];
    
    DataRow *rowRef = [[DataRow alloc] init];
    [rowRef addColumnInt:[NSString stringWithFormat:@"%@",user_id]];
    [rowRef addColumnString:[NSString stringWithFormat:@"%@",user_name]];
    [rowRef addColumnString:[NSString stringWithFormat:@"%@",user_password]];
    [rowRef addColumnInt:[NSString stringWithFormat:@"%@",user_role]];
    [rowRef addColumnString:[NSString stringWithFormat:@"%@",user_fullname]];
    [rowRef addColumnString:[NSString stringWithFormat:@"%@",user_language]];
    [rowRef addColumnString:[NSString stringWithFormat:@"%@",user_title]];
    [rowRef addColumnInt:[NSString stringWithFormat:@"%@",user_hotel_id]];
    [rowRef addColumnString:[NSString stringWithFormat:@"%@",user_department]];
    [rowRef addColumnInt:[NSString stringWithFormat:@"%@",user_department_id]];
    [rowRef addColumnInt:[NSString stringWithFormat:@"%@",user_supervisor_id]];
    [rowRef addColumnString:[NSString stringWithFormat:@"%@",user_last_modified]];
    [rowRef addColumnInt:[NSString stringWithFormat:@"%@",allow_block_room]];
    [rowRef addColumnInt:[NSString stringWithFormat:@"%@",allow_reorder_room]];
    
    [tableUsers loadTableDataWithReferenceRow:rowRef conditions:nil];
    
    int countRows = [tableUsers countRows];
    if(countRows > 0)
    {
        NSMutableArray *listUser = [[NSMutableArray alloc] init];
        for (int i = 0; i < countRows; i ++) {
            UserModelV2 *curUser = [[UserModelV2 alloc] init];
            DataRow *curRow = [tableUsers rowWithIndex:i];
            DataColumn *curColumn = nil;
            
            curColumn = [curRow columnWithName:[NSString stringWithFormat:@"%@",user_id]];
            curUser.userId = [curColumn fieldValueInt];
            
            curColumn = [curRow columnWithName:[NSString stringWithFormat:@"%@",user_name]];
            curUser.userName = [curColumn fieldValueString];
            
            curColumn = [curRow columnWithName:[NSString stringWithFormat:@"%@",user_password]];
            curUser.userPassword = [curColumn fieldValueString];
            
            curColumn = [curRow columnWithName:[NSString stringWithFormat:@"%@",user_role]];
            curUser.userRole = [curColumn fieldValueString];
            
            curColumn = [curRow columnWithName:[NSString stringWithFormat:@"%@",user_fullname]];
            curUser.userFullName = [curColumn fieldValueString];
            
            curColumn = [curRow columnWithName:[NSString stringWithFormat:@"%@",user_language]];
            curUser.userLang = [curColumn fieldValueString];
            
            curColumn = [curRow columnWithName:[NSString stringWithFormat:@"%@",user_title]];
            curUser.userTitle = [curColumn fieldValueString];
            
            curColumn = [curRow columnWithName:[NSString stringWithFormat:@"%@",user_hotel_id]];
            curUser.userHotelsId = [curColumn fieldValueInt];
            
            curColumn = [curRow columnWithName:[NSString stringWithFormat:@"%@",user_department]];
            curUser.userDepartment = [curColumn fieldValueString];
            
            curColumn = [curRow columnWithName:[NSString stringWithFormat:@"%@",user_department_id]];
            curUser.userDepartmentId = [curColumn fieldValueInt];
            
            curColumn = [curRow columnWithName:[NSString stringWithFormat:@"%@",user_supervisor_id]];
            curUser.userSupervisorId = [curColumn fieldValueInt];
            
            curColumn = [curRow columnWithName:[NSString stringWithFormat:@"%@",user_last_modified]];
            curUser.userLastModified = [curColumn fieldValueString];
            
            curColumn = [curRow columnWithName:[NSString stringWithFormat:@"%@",allow_block_room]];
            curUser.userAllowBlockRoom = [curColumn fieldValueInt];
            
            curColumn = [curRow columnWithName:[NSString stringWithFormat:@"%@",allow_reorder_room]];
            curUser.userAllowReorderRoom = [curColumn fieldValueInt];
            
            [listUser addObject:curUser];
        }
        
        return  listUser;
    }
    
    return nil;
}

-(NSMutableArray*) loadAttendanceUsersBySupervisorId:(int)userId
{
    DataTable *tableUsers = [[DataTable alloc] initWithName:[NSString stringWithFormat:@"%@",USER]];
    
    DataRow *rowRef = [[DataRow alloc] init];
    [rowRef addColumnInt:[NSString stringWithFormat:@"%@",user_id]];
    [rowRef addColumnString:[NSString stringWithFormat:@"%@",user_name]];
    [rowRef addColumnString:[NSString stringWithFormat:@"%@",user_password]];
    [rowRef addColumnInt:[NSString stringWithFormat:@"%@",user_role]];
    [rowRef addColumnString:[NSString stringWithFormat:@"%@",user_fullname]];
    [rowRef addColumnString:[NSString stringWithFormat:@"%@",user_language]];
    [rowRef addColumnString:[NSString stringWithFormat:@"%@",user_title]];
    [rowRef addColumnInt:[NSString stringWithFormat:@"%@",user_hotel_id]];
    [rowRef addColumnString:[NSString stringWithFormat:@"%@",user_department]];
    [rowRef addColumnInt:[NSString stringWithFormat:@"%@",user_department_id]];
    [rowRef addColumnInt:[NSString stringWithFormat:@"%@",user_supervisor_id]];
    [rowRef addColumnString:[NSString stringWithFormat:@"%@",user_last_modified]];
    [rowRef addColumnInt:[NSString stringWithFormat:@"%@",allow_block_room]];
    [rowRef addColumnInt:[NSString stringWithFormat:@"%@",allow_reorder_room]];
    
    //[tableUsers loadTableDataWithReferenceRow:rowRef conditions:nil];
    NSString *sqlString = [NSString stringWithFormat:
                           @"select a.%@,a.%@,a.%@,a.%@,a.%@,a.%@,a.%@,a.%@,a.%@,a.%@,a.%@,a.%@,a.%@,a.%@ from %@ a, %@ b where a.%@ = b.%@ and b.%@ = %d",
                           //Select
                           user_id,
                           user_name,
                           user_password,
                           user_role,
                           user_fullname,
                           user_language,
                           user_title,
                           user_hotel_id,
                           user_department,
                           user_department_id,
                           user_supervisor_id,
                           user_last_modified,
                           allow_block_room,
                           allow_reorder_room,
                           
                           //From
                           USER,
                           USER_LIST,
                           
                           user_id,
                           usr_id,
                           
                           user_supervisor_id,
                           userId
                           ];
    [tableUsers loadTableDataWithQuery:sqlString referenceRow:rowRef];
    
    int countRows = [tableUsers countRows];
    if(countRows > 0)
    {
        NSMutableArray *listUser = [[NSMutableArray alloc] init];
        for (int i = 0; i < countRows; i ++) {
            UserModelV2 *curUser = [[UserModelV2 alloc] init];
            DataRow *curRow = [tableUsers rowWithIndex:i];
            DataColumn *curColumn = nil;
            
            curColumn = [curRow columnWithName:[NSString stringWithFormat:@"%@",user_id]];
            curUser.userId = [curColumn fieldValueInt];
            
            curColumn = [curRow columnWithName:[NSString stringWithFormat:@"%@",user_name]];
            curUser.userName = [curColumn fieldValueString];
            
            curColumn = [curRow columnWithName:[NSString stringWithFormat:@"%@",user_password]];
            curUser.userPassword = [curColumn fieldValueString];
            
            curColumn = [curRow columnWithName:[NSString stringWithFormat:@"%@",user_role]];
            curUser.userRole = [curColumn fieldValueString];
            
            curColumn = [curRow columnWithName:[NSString stringWithFormat:@"%@",user_fullname]];
            curUser.userFullName = [curColumn fieldValueString];
            
            curColumn = [curRow columnWithName:[NSString stringWithFormat:@"%@",user_language]];
            curUser.userLang = [curColumn fieldValueString];
            
            curColumn = [curRow columnWithName:[NSString stringWithFormat:@"%@",user_title]];
            curUser.userTitle = [curColumn fieldValueString];
            
            curColumn = [curRow columnWithName:[NSString stringWithFormat:@"%@",user_hotel_id]];
            curUser.userHotelsId = [curColumn fieldValueInt];
            
            curColumn = [curRow columnWithName:[NSString stringWithFormat:@"%@",user_department]];
            curUser.userDepartment = [curColumn fieldValueString];
            
            curColumn = [curRow columnWithName:[NSString stringWithFormat:@"%@",user_department_id]];
            curUser.userDepartmentId = [curColumn fieldValueInt];
            
            curColumn = [curRow columnWithName:[NSString stringWithFormat:@"%@",user_supervisor_id]];
            curUser.userSupervisorId = [curColumn fieldValueInt];
            
            curColumn = [curRow columnWithName:[NSString stringWithFormat:@"%@",user_last_modified]];
            curUser.userLastModified = [curColumn fieldValueString];
            
            curColumn = [curRow columnWithName:[NSString stringWithFormat:@"%@",allow_block_room]];
            curUser.userAllowBlockRoom = [curColumn fieldValueInt];
            
            curColumn = [curRow columnWithName:[NSString stringWithFormat:@"%@",allow_reorder_room]];
            curUser.userAllowReorderRoom = [curColumn fieldValueInt];
            
            [listUser addObject:curUser];
        }
        
        return  listUser;
    }
    
    return nil;
}

-(BOOL)updateUserModel:(UserModelV2 *)model {
    //update user model
    UserAdapterV2 *adapter1 = [UserAdapterV2 createUserAdapter];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    [adapter1 updateUserData:model];
    [adapter1 close];
    adapter1 = nil;
    return YES;
}

-(void) insertAccessRightModel:(AccessRightModel *)accessRightModel
{
    AccessRightAdapter *accessRightAdapter = [[AccessRightAdapter alloc] init];
    [accessRightAdapter openDatabase];
    [accessRightAdapter resetSqlCommand];
    [accessRightAdapter insertAccessRight:accessRightModel];
    [accessRightAdapter close];
}

-(void) updateAccessRightModel:(AccessRightModel *)accessRightModel
{
    AccessRightAdapter *accessRightAdapter = [[AccessRightAdapter alloc] init];
    [accessRightAdapter openDatabase];
    [accessRightAdapter resetSqlCommand];
    [accessRightAdapter updateAccessRight:accessRightModel];
    [accessRightAdapter close];
}

-(AccessRightModel *) loadAccessRightModel:(NSInteger)userId
{
    AccessRightAdapter *accessRightAdapter = [[AccessRightAdapter alloc] init];
    [accessRightAdapter openDatabase];
    [accessRightAdapter resetSqlCommand];
    AccessRightModel *accessRightModel = [accessRightAdapter loadAccessRightByUserId:userId];
    [accessRightAdapter close];
    return accessRightModel;
}
-(int)getBuildingIdByUserId:(int)userId
{
    /*
    DataTable *table = [[DataTable alloc] initWithName:@"TableResut"];
    NSString *raRoomNumberField = [NSString stringWithFormat:@"%@",ra_room_id];
    NSString *sqlQuery = [NSString stringWithFormat:@"select %@ from %@ where %@ = %d LIMIT 1", ra_room_id, ROOM_ASSIGNMENT, ra_user_id, userId];
    DataRow *rowRef = [[DataRow alloc] init];
    [rowRef addColumnString:raRoomNumberField];
    [table loadTableDataWithQuery:sqlQuery referenceRow:rowRef];
    
    NSString *roomNumber = nil;
    if([table countRows] > 0){
        roomNumber = [[[table rowWithIndex:0] columnWithName:raRoomNumberField] fieldValueString];
    }
    
    int buildingId = 0;
    if(roomNumber > 0){
        RoomModelV2 *roomModel = [[RoomModelV2 alloc] init];
        roomModel.room_Id = roomNumber;
        [[RoomManagerV2 sharedRoomManager] loadRoomModel:roomModel];
        if(roomModel.room_Building_Id > 0){
            buildingId = roomModel.room_Building_Id;
        } else {
            buildingId = 1;
        }
    } else {
        buildingId = 1;
    }
    
    return buildingId;
    */
    
    NSInteger buildingId = [[NSUserDefaults standardUserDefaults] integerForKey:[NSString stringWithFormat:@"%@%d", CURRENT_BUILDING_ID, [UserManagerV2 sharedUserManager].currentUser.userId]];
    if(buildingId <= 0) {
        return 1;
    }
    
    return (int)buildingId;
    
}


#pragma mark - User Common Configurations

-(NSString*)getCommonUserConfigValueByKey:(NSString*)configKey
{
    return [self getConfigValueByUserID:UserConfigPermission_CommonUser AndKey:configKey];
}

-(NSInteger)setCommontUserConfig:(NSString*)configKey value:(NSString*)configValue
{
    int userId = UserConfigPermission_CommonUser;
    int result = 0;
    
    UserConfigModel *userConfig = nil;
    NSString *valueRecord = [self getCommonUserConfigValueByKey:configKey];
    
    if(valueRecord.length > 0)
    {
        result = [self updateUserConfigValue:configValue ByUserId:userId andConfigKey:configKey] ? 1 : 0 ;
    }
    else
    {
        userConfig = [[UserConfigModel alloc] init];
        userConfig.userId = userId;
        userConfig.key = configKey;
        userConfig.value = configValue;
        result = (int)[self insertUserConfiguration:userConfig];
    }
    
    return result;
}

- (NSInteger)setRemarkForCommonUserConfig:(NSString*)configKey value:(NSString*)configValue {
    int userId = UserConfigPermission_CommonUser;
    int result = 0;
    
    UserConfigModel *userConfig = [self getUserConfigBy:userId andKey:configKey];
    if (userConfig != nil) {
        result = [self updateUserConfigValue:configValue ByUserId:userId andConfigKey:configKey] ? 1 : 0 ;
    } else {
        UserConfigModel *newUserConfig = [[UserConfigModel alloc] init];
        newUserConfig.value = configValue;
        newUserConfig.userId = userId;
        newUserConfig.key = configKey;
        result = (int)[self insertUserConfiguration:newUserConfig];
    }
    
    return result;
}

-(NSString*)getCurrentUserConfigValueByKey:(NSString*)configKey
{
    return  [self getConfigValueOfCurrentUserByKey:configKey];
}

-(NSInteger)setCurrentUserConfig:(NSString*)configKey value:(NSString*)configValue
{
    int userId = [UserManagerV2 sharedUserManager].currentUser.userId;
    int result = 0;
    if(userId > 0)
    {
        UserConfigModel *userConfig = nil;
        NSString *valueRecord = [self getCurrentUserConfigValueByKey:configKey];
 
        if(valueRecord.length > 0 || [valueRecord isEqualToString:@""])
        {
            result = [self updateUserConfigValue:configValue ByUserId:userId andConfigKey:configKey] ? 1 : 0 ;
        }
        else
        {
            userConfig = [[UserConfigModel alloc] init];
            userConfig.userId = userId;
            userConfig.key = configKey;
            userConfig.value = configValue;
            result = (int)[self insertUserConfiguration:userConfig];
        }
    }

    return result;
}

-(NSInteger)insertUserConfiguration:(UserConfigModel*)userConfig
{
    UserAdapterV2 *adapter1 = [UserAdapterV2 createUserAdapter];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    NSInteger returnValue = [adapter1 insertUserConfig:userConfig];
    [adapter1 close];
    return returnValue;
}

-(BOOL)updateUserConfigValue:(NSString*) configValue ByUserId:(int)userId andConfigKey:(NSString *)configKey
{
    UserAdapterV2 *adapter1 = [UserAdapterV2 createUserAdapter];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    int result = [adapter1 updateUserConfigurationValue:configValue ByUserId:userId andConfigKey:configKey];
    [adapter1 close];
    adapter1 = nil;
    return (result > 0);
}

-(BOOL)updateConfigValueOfCurrentUser:(NSString*)configValue andConfigKey:(NSString*)configKey
{
    BOOL result = [self updateUserConfigValue:configValue ByUserId:[UserManagerV2 sharedUserManager].currentUser.userId andConfigKey:configKey];
    return result;
}

-(UserConfigModel*) getUserConfigBy:(int)userId andKey:(NSString*)configKey
{
    if(userId < 0)
        return nil;
    if(configKey.length <= 0)
        return nil;
    
    UserConfigModel* result = nil;
    UserAdapterV2 *adapter1 = [UserAdapterV2 createUserAdapter];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    result = [adapter1 getUserConfigByUserId:userId andKey:configKey];
    [adapter1 close];
    adapter1 = nil;
    return result;
}

-(NSString*) getConfigValueOfCurrentUserByKey:(NSString*) configKey
{
//    [LogFileManager logDebugMode:@"[Item 3] getConfigValueOfCurrentUserByKey Stack: %@",[NSThread callStackSymbols]];
    int currentUserId = [[UserManagerV2 sharedUserManager] currentUser].userId;
    UserConfigModel *userConfig = [self getUserConfigBy:currentUserId andKey:configKey];
    if(userConfig){
        [LogFileManager logDebugMode:@"[Item 3] getConfigValueOfCurrentUserByKey: userConfig.value: %@", userConfig.value];
        return userConfig.value;
    }
    [LogFileManager logDebugMode:@"[Item 3] getConfigValueOfCurrentUserByKey: userConfig.value: nil"];
    return nil;
}

-(NSString*) getConfigValueByUserID:(int)userId AndKey:(NSString*) configKey
{
    UserConfigModel *userConfig = [self getUserConfigBy:userId andKey:configKey];
    if(userConfig){
        return userConfig.value;
    }
    
    return nil;
}

//if userId <= 0  we will delete all config value for parameter "configKey"
//if configkey.length <= 0 we will delete all config value for parameter "userId"
-(BOOL) deleteUserConfigByUserId:(int)userId andKey:(NSString*)configKey
{
    UserAdapterV2 *adapter1 = [UserAdapterV2 createUserAdapter];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    int result = [adapter1 deleteUserConfigurationsByUserId:userId AndKey:configKey];
    [adapter1 close];
    adapter1 = nil;
    return (result > 0);
}

-(BOOL) isCurrentUserAlreadyConfiged
{
    NSString *value = [self getConfigValueOfCurrentUserByKey:USER_CONFIG_IS_ALREADY_CONFIGED];
    BOOL result = NO;
    
    if(value.length > 0){
        if([value caseInsensitiveCompare:@"yes"] == NSOrderedSame
           || [value caseInsensitiveCompare:@"true"] == NSOrderedSame
           || [value caseInsensitiveCompare:@"1"] == NSOrderedSame)
        {
            result = YES;
        }
    }
    
    return result;
}

-(void) createDefaultConfigurationsOfCurrentUser
{
    UserConfigModel *userConfig = [[UserConfigModel alloc] init];
    userConfig.userId = [UserManagerV2 sharedUserManager].currentUser.userId;
    
    //config validate room number
    userConfig.key = USER_CONFIG_IS_VALIDATE_ROOM_NUMBER;
    userConfig.value = @"false";
    [self insertUserConfiguration:userConfig];
    
    //set user configed
    userConfig.key = USER_CONFIG_IS_ALREADY_CONFIGED;
    userConfig.value = @"true";
    [self insertUserConfiguration:userConfig];
    
}

#pragma mark - User log information

/*!
 CRF-1036: Concurrent License Control By Login Status.
 This function will call WS to check whether user login in different device or not
 return YES if need to logout user otherwhile return NO
 */
-(BOOL)shouldLogOutUser
{
    if (![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        return NO;
    }
    
    //Deivce type = 0 is iOS device, Device type = 1 is Android device
    int responseCode = (int)[self getStillLoggedInWSForUserId:[UserManagerV2 sharedUserManager].currentUser.userId deviceNumber:[DeviceManager identifierForVendor] deviceType:0];
    
    if (responseCode == RESPONSE_STATUS_OK) {
        return NO;
    }
    
    return YES;
}

-(NSInteger)insertUserLog:(UserLogModelV2*)userLogModel
{
    UserAdapterV2 *adapter1 = [UserAdapterV2 createUserAdapter];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    NSInteger returnValue = [adapter1 insertUserLogData:userLogModel];
    [adapter1 close];
    return returnValue;
}

-(BOOL)updateUserLog:(UserLogModelV2*)userLogModel
{
    UserAdapterV2 *adapter1 = [UserAdapterV2 createUserAdapter];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    [adapter1 updateUserLogData:userLogModel];
    [adapter1 close];
    adapter1 = nil;
    return YES;
}

-(UserLogModelV2*) getUserLogByUserId:(int)userId
{
    UserLogModelV2* result = nil;
    UserAdapterV2 *adapter1 = [UserAdapterV2 createUserAdapter];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    result = [adapter1 getUserLogByUserId:userId];
    [adapter1 close];
    adapter1 = nil;
    return result;
}

@end

