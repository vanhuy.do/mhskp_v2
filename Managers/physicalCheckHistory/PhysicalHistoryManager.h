//
//  PhysicalHistoryManager.h
//  mHouseKeeping
//
//  Created by Mac User on 26/03/2013.
//
//

#import <Foundation/Foundation.h>
#import "PhysicalHistoryModel.h"

@interface PhysicalHistoryManager : NSObject
//-(NSInteger) insertEngineeringItemModelV2:(EngineeringItemModelV2 *)model{
-(NSInteger) insertPhysicalHistoryModel:(PhysicalHistoryModel *)model;
-(NSMutableArray *) loadAllPhysicalHistoryData;
-(NSMutableArray *) loadAllFloorContainsPhysicalHistoryByUserId:(NSInteger) userId;
-(NSMutableArray *)loadAllPhysicalHistoryByFloorId:(NSInteger)floorID AndUserId:(NSInteger)userId;
-(void) loadAllPhysicalHistory;
-(int) deleteDatabaseAfterSomeDays;
@end
