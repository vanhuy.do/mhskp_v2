//
//  PhysicalHistoryManager.m
//  mHouseKeeping
//
//  Created by Mac User on 26/03/2013.
//
//

#import "PhysicalHistoryManager.h"
#import "PhysicalHistoryAdapter.h"

@implementation PhysicalHistoryManager
-(NSInteger) insertPhysicalHistoryModel:(PhysicalHistoryModel *)model{
    NSInteger returnCode = 0;
    PhysicalHistoryAdapter *adapter = [[PhysicalHistoryAdapter alloc] init];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    returnCode = [adapter insertPhysicalHistoryData:model];
    [adapter close];
    adapter = nil;
    return returnCode;
}
-(NSMutableArray *) loadAllPhysicalHistoryData{
    PhysicalHistoryAdapter *adapter = [[PhysicalHistoryAdapter alloc] init];
    [adapter openDatabase];
    NSMutableArray *array = [adapter loadAllPhysicalHistoryData];
    [adapter close];
    return array;
}
-(NSMutableArray *) loadAllFloorContainsPhysicalHistoryByUserId:(NSInteger) userId{
    PhysicalHistoryAdapter *adapter = [[PhysicalHistoryAdapter alloc] init];
    [adapter openDatabase];
    NSMutableArray *array = [adapter loadAllFloorContainsPhysicalHistoryByUserId:userId];
    [adapter close];
    return array;
}
-(NSMutableArray *)loadAllPhysicalHistoryByFloorId:(NSInteger)floorID AndUserId:(NSInteger)userId{
    PhysicalHistoryAdapter *adapter = [[PhysicalHistoryAdapter alloc] init];
    [adapter openDatabase];
    NSMutableArray *array = [adapter loadAllPhysicalHistoryByFloorId:floorID AndUserId:userId];
    [adapter close];
    return array;
}
-(void) loadAllPhysicalHistory{
    PhysicalHistoryAdapter *adapter = [[PhysicalHistoryAdapter alloc] init];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    [adapter loadAllPhysicalHistory];
    [adapter close];
    
    adapter = nil;
}

-(int) deleteDatabaseAfterSomeDays{
    int returnCode = 0;
    PhysicalHistoryAdapter *adapter = [[PhysicalHistoryAdapter alloc] init];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    returnCode = [adapter deleteDatabaseAfterSomeDays];
    [adapter close];
    adapter = nil;
    return returnCode;
}
@end
