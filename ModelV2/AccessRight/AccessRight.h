//
//  AccessRight.h
//  mHouseKeeping
//
//  Created by Khanh Phan on 2/27/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AccessRight : NSObject

@property (nonatomic, assign) BOOL isAllowedView;
@property (nonatomic, assign) BOOL isALlowedEdit;
@property (nonatomic, assign) BOOL isActive;

@end
