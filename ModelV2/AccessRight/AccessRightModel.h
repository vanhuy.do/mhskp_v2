//
//  AccessRightModel.h
//  mHouseKeeping
//
//  Created by Khanh Phan on 2/25/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ehkLanguageDefines.h"

#define isDemoMode ([[NSUserDefaults standardUserDefaults] integerForKey:ENABLE_DEMO_MODE] == 1)

//MARK: AccessRightModel ONLY APPLY FOR CURRENT USER LOGIN
@interface AccessRightModel : NSObject
{
}
- (instancetype)initWithDefaultInfo;
@property (nonatomic, assign) NSInteger userId;
@property (nonatomic, assign) NSInteger roomAssignment;
@property (nonatomic, assign) NSInteger roomAssignmentManualCredit;
@property (nonatomic, assign) NSInteger roomAssignmentChecklistRemark;
@property (nonatomic, assign) NSInteger completedRoom;
@property (nonatomic, assign) NSInteger manualUpdateRoomStatus;
@property (nonatomic, assign) NSInteger restrictionAssignment;
@property (nonatomic, assign) NSInteger QRCodeScanner;
@property (nonatomic, assign) NSInteger postLostAndFound;
@property (nonatomic, assign) NSInteger postEngineering;
@property (nonatomic, assign) NSInteger postLaundry;
@property (nonatomic, assign) NSInteger postMinibar;
@property (nonatomic, assign) NSInteger postLinen;
@property (nonatomic, assign) NSInteger postAmenities;
@property (nonatomic, assign) NSInteger postPhysicalCheck;
@property (nonatomic, assign) NSInteger actionLostAndFound;
@property (nonatomic, assign) NSInteger actionEngineering;
@property (nonatomic, assign) NSInteger actionLaundry;
@property (nonatomic, assign) NSInteger actionMinibar;
@property (nonatomic, assign) NSInteger actionLinen;
@property (nonatomic, assign) NSInteger actionAmenities;
@property (nonatomic, assign) NSInteger actionCheckList;
@property (nonatomic, assign) NSInteger actionGuideLine;
@property (nonatomic, assign) NSInteger message;
@property (nonatomic, assign) NSInteger findAttendantPendingCompleted;
@property (nonatomic, assign) NSInteger findRoom;
@property (nonatomic, assign) NSInteger findInspection;
@property (nonatomic, assign) NSInteger setting;
@property (nonatomic, assign) NSInteger findRoomRemarkUpdate;
@property (nonatomic, assign) NSInteger enableEncryptedQRCode;
@property (nonatomic, assign) NSInteger isShowActionHistoryPosting;

@property (nonatomic, assign) NSInteger isPostingFunctionMinibarNew;
@property (nonatomic, assign) NSInteger isPosingFunctionMinibarUsed;
@property (nonatomic, assign) NSInteger isRoomDetailsActionMinibarNew;
@property (nonatomic, assign) NSInteger isRoomDetailsActionMinibarUsed;

@property (nonatomic, assign) NSInteger isPostingFunctionAmenitiesQRCode;
@property (nonatomic, assign) NSInteger isPostingFunctionEngineeringQRCode;
@property (nonatomic, assign) NSInteger isPostingFunctionLinenQRCode;
@property (nonatomic, assign) NSInteger isPostingFunctionLostAndFoundQRCode;
@property (nonatomic, assign) NSInteger isPostingFunctionMinibarQRCode;
@property (nonatomic, assign) NSInteger isPostingFunctionPhysicalCheckQRCode;
//This Access right come from load Common Configuration WS
//MARK: value store in table user_configurations with userId, key, value
-(NSInteger) countTimesPauseRoom;
-(BOOL) isFindRoomByFloor;
-(BOOL) hasUnassignRoom;

-(void) setCountTimesPauseRoom:(NSInteger)countTimesPauseRoomValue;
-(void) setIsFindRoomByFloor:(BOOL)isFindRoomByFloorValue;
-(void) setHasUnassignRoom:(BOOL)hasUnassignRoomValue;

//Validate room number config
-(BOOL) isValidateRoom;
-(void) setIsValidateRoomValue:(BOOL)isValidateValue;

//additional job
-(BOOL) isAddJobActive;
-(BOOL) isAddJobAllowedView;

//This access right get from function "GetCommonConfigurationsResponse" WS
//With key USER_CONFIG_ROOM_ASSIGNMENT_FIND_RA_METHOD
-(BOOL) isFindRARoomsToday;

//MESSAGE PERIOD DAYS
-(int) getMessagePeriod;
-(void) setMessagePeriod:(int)messagePeriod;

//ENABLE Guest Info
-(BOOL) isEnableGuestInfo;

/*
****Rating
INI Setting
CHECKLIST_DEFAULT_FULLMARK=0

- When view in checklist, the default mark shown will be N/A
- when submit, all checklist item will be insert with the final value [min value is 0]

CHECKLIST_DEFAULT_FULLMARK=1
- when view in checklist, the defaule mark shown will be the maximun mark set in config [eHousekeeping]
- when submit, all checklist item will be insert with the final value [min value is 0]


****Checkmark
INI Setting

CHECKLIST_DEFAULT_FULLMARK=0

- When view in checklist, the default checkmark will be uncheck
- when submit, all checklist item will be insert with the final value
if checked, then the value is 1
if uncheck, then the value is 0

CHECKLIST_DEFAULT_FULLMARK=1
- When view in checklist, the default checkmark will be uncheck
- when submit, all checklist item will be insert with the final value
if checked, then the value is 0
if uncheck, then the value is 1
 
CHECKLIST_DEFAULT_FULLMARK=2
 - this will indicate that all checklist will default checked
 - checked means pass, unchecked means failed
 
 */
//-(BOOL) isChecklistFullMark;
- (NSInteger)valueForChecklistCheckMark;
- (NSInteger)valueForCheckMarkRate;
-(void) setIsChecklistFullMark:(BOOL)fullMarkValue;

//Only for supervisor to get all room inspections or only get room inspection today
-(void) setEnableRoomMatrix:(BOOL)isEnable;
-(BOOL) isEnableRoomMatrix;

//Check for posting New number depends on Used number or not
//If return YES: Used number change will not affect to New number
//If return NO: Used number change will affect to New number
-(BOOL) isLinenSeperateUsedNew;

//Check whether showing physical check or not
//Return true: show physical check remark
//Return false: Hide physical check remark
-(BOOL) isShowPhyshicalCheckRemark;

//whether get physical check value after input room number or not
//return true: will get physical check remark value
//return false: will not get physical check remark value
-(BOOL) isGetPhysicalCheckRemarkValue;

//CRF-00000516 / Find Function Enhancement - MHSKP-328
//Return true: current Find Room will become Find by Status and show NEW option, Find by Room Number.
//Return false: used old Find Method (no Find Room Number).
-(NSInteger*) isFindRoomNo;

//CRF - 00000797
//1. Room listings for Room Release on above API
//2. GetCommonConfigurations
//3. Screen for Home, Find Module need to add this indicator if RELEASE_ROOM_ENABLE=1
-(BOOL) isEnableReleaseRoom;

-(BOOL)isEnablePanicButton;

/*
 0 = Disabled
 1 = Synchronize New to Used - Don't Overwrite if got Used value
 2 = Synchronize Used to New - Don't Overwrite if got New value.
 3 = Synchronize New to Used - Overwrite if got Used value
 4 = Synchronize Used to New - Overwrite if got New value
 5 = Synchronize New & Used - Don't overwrite if got value
 6 = Synchronize Used & New - Overwrite if got value
 */
-(int)miniBarSynchronizeUsedAndNew;

-(BOOL)isPhysicalCheckRoomStatusFromRoomTable;

//Hao Tran[20141105 / Apply Configuration POSTING_POST_ZEROTH_VALUE] - if 0 follow normal, if 1 post 0
-(BOOL) isPostingZeroQuantity;

//CRF-00000995: New remark type for consolidation remark
//REMARKS_TYPE INI - if 1: new type of remark, if 0: no change (use the old type of remark)
-(BOOL) isRemarkConsolidation;
+ (BOOL)isEnableEncryptedQRCode;
+ (NSString*)getKey;
+ (NSString*)getIV;
/*!
 SYNC_INTERVAL time for supervisor
 */
-(NSInteger)syncSUPInterval;

/*!
 SYNC_INTERVAL time for room attendant
 */
-(NSInteger)syncRAInterval;

/*!
 CRF-1036: Concurrent License Control by Login Status
 */
-(NSInteger)userCheckingLoginInterval;

/* DungPhan - May 22, 2015
 CRF-00001276
 Description: Only "Used" Column is Required for All Minibar / Linen Posting: Client requested the "New" Column to be invisible for Minibar & Linen Posting.
 Accepted Values: 0 = No Change, 1 = New Only, 2 = Used Only
 */
-(int)minibarNewUsedDisplay;
-(int)linenNewUsedDisplay;

/*! CRF-00001230: Clear History of old records posted
 Select to show current posting history config by GetCommonConfiguration API
 Example: API response 2, history will be show 2 days recently and the day before 2 days will be deleted
*/
-(NSInteger)countPostingHistoryDays;

/*! CRF-1274: Checklist fail would result in auto fail inspection
 
 Changes required on WS.
 m-Housekeeping will get new new field from API GetCommonConfiguration
 CHECKLIST_MANDATORYPASS
 
 If it's 1, then mandatory_pass is required.
 
 Rating Checklist
 cliMandatoryPass from GetCheckListItems will return minimun passing rate for the specific rating checklist items.
 If the value entered by user is less than this value, will auto failed the inspection.
 
 Checkmark Checklist - depending on the INI config
 CHECKLIST_DEFAULT_FULLMARK
 if 1 [all items is mark as uncheck] - check means failed
 if 2 [all items is mark as check] - uncheck means failed
 
 Optional: Changed the checklist items color if user failed it, when CHECKLIST_MANDATORYPASS=1
 
 If cli_mandatorypass is 0, this mean the checklist item is normal
 If cli_mandatorypass is > 0, this checklist item is compulsory pass

 */
-(NSInteger)checklistMandatoryPass;

/*
 CRF-1259: Display filter for supervisor home screen
 
 return YES means enabled
 return NO means disabled
 */
-(BOOL) isEnabledSupHomeFilter;


/*
 CRF-1229: Posting History in mHSKP
 
 return YES if enabled by INI
 return NO if disabled by INI
 */
-(BOOL) isEnablePostingHistory;


/*
 CRF-711: "Time Counter" for room cleaning should continue from where it stopped
 This will not effect on posting data to server, only effect on UI when user back to clean room again
 Cleaning start time, pause time, complete time is not change by this INI
 
 return YES if enabled by INI
 return NO if disabled by INI
 */
-(BOOL) continueFromPausedTime;

/*!
 CRF-00001046: 
 In the system, guest profile information no matter in-house guest, arriving guest or departed guest is stored. This is valuable information and could be handy for housekeeping user to quickly view essential information of the guest especially preferences and notes.
 
 New feature in m-Housekeeping to allow user to search guest information on-the-go. User is able to search by room number / last name / guest name. In addition, search is extend to filter only In-House / Due-In / Due-Out criteria, as well as to select a date range of on day / past 1 week / future 1 week / past 1 month / future 1 month for the result.

 */
-(BOOL) isEnableGuestProfile;

/*
  CRF-00001229: Posting History in mHSKP
 */
-(BOOL) isRoomDetailsActionHistoryActive;
-(BOOL) isRoomDetailsActionHistoryAllowedView;
-(BOOL) isFindActionHistoryActive;
-(BOOL) isFindActionHistoryAllowedView;

/*
 CRF-00000711
 Backward compatible for Additional Job
 if 1 - means used the new logic
 if 0 - means used the old logic
 */
-(BOOL) isUseAdditionalJobStatusTable;

/*
 CRF-00001262 > [PO] [Marriott] Guideline, Linen, Amenities, Minibar Based on Room Type
 **for backward compatibility**
 
 LOAD_AMENITIES_BY_ROOMTYPE_ENABLE
 LOAD_LINEN_BY_ROOMTYPE_ENABLE
 LOAD_MINIBAR_BY_ROOMTYPE_ENABLE
 
 return YES mean enable
 return NO mean disable
 */
-(BOOL) isLoadAmenitiesByRoomType;
-(BOOL) isLoadLinenByRoomType;
-(BOOL) isLoadMinibarByRoomType;

/*
 CRF-00001178 > Show Assigned Other Activity in Assignment List
 [OTHER_ACTIVITY]
 ENABLE_OTHER_ACTIVITY
 return YES mean enable Other Activity
 return NO mean disable Other Activity
 */
- (BOOL)isEnableOtherActivity;

- (int)getRoomAssignmentSpecialAssignmentMethod; // CFG [20160914/CRF-00001439]
- (int)getInspectionCrossZoneMethod; // CFG [20161221/CRF-00001559]

- (NSString *) getConfigByKey: (NSString *)sKey; // CFG [20160928/CRF-00001612]

@end
