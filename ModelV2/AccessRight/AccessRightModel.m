//
//  AccessRightModel.m
//  mHouseKeeping
//
//  Created by Khanh Phan on 2/25/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "AccessRightModel.h"
#import "UserManagerV2.h"
#import "ehkDefinesV2.h"

//MARK: AccessRightModel ONLY APPLY FOR CURRENT USER LOGIN
@implementation AccessRightModel
{
}

@synthesize roomAssignment;
@synthesize roomAssignmentManualCredit;
@synthesize roomAssignmentChecklistRemark;
@synthesize enableEncryptedQRCode;
@synthesize message;
@synthesize setting;
@synthesize findRoom;
@synthesize postLinen;
@synthesize actionLinen;
@synthesize postLaundry;
@synthesize postMinibar;
@synthesize actionLaundry;
@synthesize actionMinibar;
@synthesize completedRoom;
@synthesize postAmenities;
@synthesize QRCodeScanner;
@synthesize findInspection;
@synthesize actionAmenities;
@synthesize actionCheckList;
@synthesize actionGuideLine;
@synthesize postEngineering;
@synthesize postLostAndFound;
@synthesize actionEngineering;
@synthesize postPhysicalCheck;
@synthesize actionLostAndFound;
@synthesize manualUpdateRoomStatus;
@synthesize findAttendantPendingCompleted;
@synthesize restrictionAssignment;
@synthesize userId;
@synthesize findRoomRemarkUpdate;
@synthesize isShowActionHistoryPosting;
@synthesize isPosingFunctionMinibarUsed,isPostingFunctionMinibarNew,isRoomDetailsActionMinibarNew,isRoomDetailsActionMinibarUsed;
@synthesize isPostingFunctionLinenQRCode, isPostingFunctionMinibarQRCode, isPostingFunctionAmenitiesQRCode, isPostingFunctionEngineeringQRCode, isPostingFunctionLostAndFoundQRCode, isPostingFunctionPhysicalCheckQRCode;

//Access Rights below come from Common Configuraion WS
//MARK: value store in table user_configurations with userId, key, value
//MARK: AccessRightModel ONLY APPLY FOR CURRENT USER LOGIN

//Count times pause room
-(NSInteger) countTimesPauseRoom
{
    //if countTimesPauseRoom = 0, it's "unlimitedPauseRoom"
    NSString *timesPauseRoom = [[UserManagerV2 sharedUserManager] getConfigValueByUserID:[UserManagerV2 sharedUserManager].currentUser.userId AndKey:USER_CONFIG_ROOM_ASSIGNMENT_PAUSE_ONE_TIME];
    int countTimesPauseRoom = (timesPauseRoom.length <= 0) ? 1 : [timesPauseRoom intValue];
    return countTimesPauseRoom;
}
- (instancetype)initWithDefaultInfo
{
    self = [super init];
    if (self) {
        //CRF1615
        isPostingFunctionMinibarNew = -1;
        isPosingFunctionMinibarUsed = -1;
        isRoomDetailsActionMinibarNew = -1;
        isRoomDetailsActionMinibarUsed = -1;
        //CRF1619
        isPostingFunctionAmenitiesQRCode = -1;
        isPostingFunctionEngineeringQRCode = -1;
        isPostingFunctionLinenQRCode = -1;
        isPostingFunctionLostAndFoundQRCode = -1;
        isPostingFunctionMinibarQRCode = -1;
        isPostingFunctionPhysicalCheckQRCode = -1;
    }
    return self;
}
-(void) setCountTimesPauseRoom:(NSInteger)countTimesPauseRoomValue
{
    NSString *countTimesValue = [[UserManagerV2 sharedUserManager] getConfigValueByUserID:[UserManagerV2 sharedUserManager].currentUser.userId AndKey:USER_CONFIG_ROOM_ASSIGNMENT_PAUSE_ONE_TIME];
    if(countTimesValue.length == 0) {
        UserConfigModel *userConfig = [[UserConfigModel alloc] init];
        userConfig.key = USER_CONFIG_ROOM_ASSIGNMENT_PAUSE_ONE_TIME;
        userConfig.value = [NSString stringWithFormat:@"%d", (int)countTimesPauseRoomValue];
        userConfig.userId = [UserManagerV2 sharedUserManager].currentUser.userId;
        [[UserManagerV2 sharedUserManager] insertUserConfiguration: userConfig];
    } else {
        [[UserManagerV2 sharedUserManager] updateUserConfigValue:[NSString stringWithFormat:@"%d", (int)countTimesPauseRoomValue] ByUserId:[UserManagerV2 sharedUserManager].currentUser.userId andConfigKey:USER_CONFIG_ROOM_ASSIGNMENT_PAUSE_ONE_TIME];
    }
}

//Find room by floor
-(void) setIsFindRoomByFloor:(BOOL)isFindRoomByFloorValue
{
    NSString *isFindRoomByFloor = [[UserManagerV2 sharedUserManager] getConfigValueByUserID:[UserManagerV2 sharedUserManager].currentUser.userId AndKey:USER_CONFIG_ROOM_ASSIGNMENT_FIND_ROOMS_METHOD];
    if(isFindRoomByFloor.length == 0) {
        UserConfigModel *userConfig = [[UserConfigModel alloc] init];
        userConfig.key = USER_CONFIG_ROOM_ASSIGNMENT_FIND_ROOMS_METHOD;
        userConfig.value = [NSString stringWithFormat:@"%d", (isFindRoomByFloorValue ? 1 : 0)];
        userConfig.userId = [UserManagerV2 sharedUserManager].currentUser.userId;
        [[UserManagerV2 sharedUserManager] insertUserConfiguration: userConfig];
    } else {
        [[UserManagerV2 sharedUserManager] updateUserConfigValue:[NSString stringWithFormat:@"%d", (isFindRoomByFloorValue ? 1 : 0)] ByUserId:[UserManagerV2 sharedUserManager].currentUser.userId andConfigKey:USER_CONFIG_ROOM_ASSIGNMENT_FIND_ROOMS_METHOD];
    }
}

-(BOOL) isFindRoomByFloor
{
    NSString *findRoomByFloorValue = [[UserManagerV2 sharedUserManager] getConfigValueByUserID:[UserManagerV2 sharedUserManager].currentUser.userId AndKey:USER_CONFIG_ROOM_ASSIGNMENT_FIND_ROOMS_METHOD];
    BOOL isFindRoomByFloor = (findRoomByFloorValue.length > 0 && [findRoomByFloorValue intValue] == 0);
    return isFindRoomByFloor;
}

//Unassign room
-(void) setHasUnassignRoom:(BOOL)hasUnassignRoomValue
{
    NSString *hasUnassignRoom = [[UserManagerV2 sharedUserManager] getConfigValueByUserID:[UserManagerV2 sharedUserManager].currentUser.userId AndKey:USER_CONFIG_ROOM_ASSIGNMENT_UNASSIGNED_ROOMS_METHOD];
    if(hasUnassignRoom.length == 0) {
        UserConfigModel *userConfig = [[UserConfigModel alloc] init];
        userConfig.key = USER_CONFIG_ROOM_ASSIGNMENT_UNASSIGNED_ROOMS_METHOD;
        userConfig.value = [NSString stringWithFormat:@"%d", (hasUnassignRoomValue ? 1 : 0)];
        userConfig.userId = [UserManagerV2 sharedUserManager].currentUser.userId;
        [[UserManagerV2 sharedUserManager] insertUserConfiguration: userConfig];
    } else {
        [[UserManagerV2 sharedUserManager] updateUserConfigValue:[NSString stringWithFormat:@"%d", (hasUnassignRoomValue ? 1 : 0)] ByUserId:[UserManagerV2 sharedUserManager].currentUser.userId andConfigKey:USER_CONFIG_ROOM_ASSIGNMENT_UNASSIGNED_ROOMS_METHOD];
    }
}

-(BOOL) hasUnassignRoom
{
    NSString *unassignedValue = [[UserManagerV2 sharedUserManager] getConfigValueByUserID:[UserManagerV2 sharedUserManager].currentUser.userId AndKey:USER_CONFIG_ROOM_ASSIGNMENT_UNASSIGNED_ROOMS_METHOD];
    BOOL hasUnassignRoom = (unassignedValue.length > 0 && [unassignedValue intValue] > 0);
    return hasUnassignRoom;
}

//Validate room number config
-(BOOL) isValidateRoom
{
    //NSString *isValidate = [[UserManagerV2 sharedUserManager] getConfigValueByUserID:[UserManagerV2 sharedUserManager].currentUser.userId AndKey:USER_CONFIG_IS_VALIDATE_ROOM_NUMBER];
    
    NSString *isValidate = [[UserManagerV2 sharedUserManager] getCommonUserConfigValueByKey:USER_CONFIG_IS_VALIDATE_ROOM_NUMBER];
    
    if(isValidate.length > 0)
    {
        if([isValidate caseInsensitiveCompare:@"yes"] == NSOrderedSame
           || [isValidate caseInsensitiveCompare:@"true"] == NSOrderedSame
           || [isValidate caseInsensitiveCompare:@"1"] == NSOrderedSame)
        {
            return true;
        }
    }
    
    return NO;
}

-(void) setIsValidateRoomValue:(BOOL)isValidateValue
{
//    NSString *isValidate = [[UserManagerV2 sharedUserManager] getConfigValueByUserID:[UserManagerV2 sharedUserManager].currentUser.userId AndKey:USER_CONFIG_IS_VALIDATE_ROOM_NUMBER];
//    if(isValidate.length == 0) {
//        UserConfigModel *userConfig = [[UserConfigModel alloc] init];
//        userConfig.key = USER_CONFIG_IS_VALIDATE_ROOM_NUMBER;
//        userConfig.value = [NSString stringWithFormat:@"%@", (isValidateValue ? @"true" : @"false")];
//        userConfig.userId = [UserManagerV2 sharedUserManager].currentUser.userId;
//        [[UserManagerV2 sharedUserManager] insertUserConfiguration: userConfig];
//    } else {
//        [[UserManagerV2 sharedUserManager] updateUserConfigValue:[NSString stringWithFormat:@"%@", (isValidateValue ? @"true" : @"false")] ByUserId:[UserManagerV2 sharedUserManager].currentUser.userId andConfigKey:USER_CONFIG_IS_VALIDATE_ROOM_NUMBER];
//    }
    
    //Change to common user
    [[UserManagerV2 sharedUserManager] setCommontUserConfig:USER_CONFIG_IS_VALIDATE_ROOM_NUMBER value:[NSString stringWithFormat:@"%@", (isValidateValue ? @"true" : @"false")]];
    
}

-(BOOL) isAddJobActive
{
    NSString *isActive = [[UserManagerV2 sharedUserManager] getConfigValueByUserID:[UserManagerV2 sharedUserManager].currentUser.userId AndKey:USER_CONFIG_IS_ADDJOB_ACTIVE];
    if(isActive.length > 0)
    {
        if([isActive caseInsensitiveCompare:@"yes"] == NSOrderedSame
           || [isActive caseInsensitiveCompare:@"true"] == NSOrderedSame
           || [isActive caseInsensitiveCompare:@"1"] == NSOrderedSame)
        {
            return YES;
        }
    }
    
    return NO;
}

-(BOOL) isAddJobAllowedView
{
    NSString *isView = [[UserManagerV2 sharedUserManager] getConfigValueByUserID:[UserManagerV2 sharedUserManager].currentUser.userId AndKey:USER_CONFIG_IS_ADDJOB_VIEW];
    if(isView.length > 0)
    {
        if([isView caseInsensitiveCompare:@"yes"] == NSOrderedSame
           || [isView caseInsensitiveCompare:@"true"] == NSOrderedSame
           || [isView caseInsensitiveCompare:@"1"] == NSOrderedSame)
        {
            return YES;
        }
    }
    
    return NO;
}

-(BOOL) isFindRARoomsToday
{
    NSString *valueConfig = [[UserManagerV2 sharedUserManager] getCurrentUserConfigValueByKey:USER_CONFIG_ROOM_ASSIGNMENT_FIND_RA_METHOD];
    int valueInt = 0;
    if(valueConfig.length > 0)
    {
        valueInt = [valueConfig intValue];
    }
    return (valueInt > 0);
}

//MESSAGE PERIOD DAYS
-(int) getMessagePeriod
{
    NSString *valueConfig = [[UserManagerV2 sharedUserManager] getCommonUserConfigValueByKey:USER_CONFIG_MESSAGE_PERIOD];
    int valueInt = 7; //Default message period is 7 days (recent 7 days)
    if(valueConfig.length > 0) {
        valueInt = [valueConfig intValue];
    }
    
    return valueInt;
}

-(void) setMessagePeriod:(int)messagePeriod
{
    //Change to common user
    [[UserManagerV2 sharedUserManager] setCommontUserConfig:USER_CONFIG_MESSAGE_PERIOD value:[NSString stringWithFormat:@"%d", messagePeriod]];
}

//ENABLE Guest Info
-(BOOL) isEnableGuestInfo
{
    NSString *valueConfig = [[UserManagerV2 sharedUserManager] getCurrentUserConfigValueByKey:USER_CONFIG_GUEST_INFO_ENABLE];
    int valueInt = 0;
    if(valueConfig.length > 0)
    {
        valueInt = [valueConfig intValue];
    }
    return (valueInt > 0);
}

//Check list full mark
//-(BOOL) isChecklistFullMark
//{
//    NSString *valueConfig = [[UserManagerV2 sharedUserManager] getCurrentUserConfigValueByKey:USER_CONFIG_CHECKLIST_DEFAULT_FULLMARK];
//    int valueInt = 1;
//    if(valueConfig.length > 0) {
//        valueInt = [valueConfig intValue];
//    }
//    return (valueInt > 0);
//}
- (NSInteger)valueForChecklistCheckMark {
    NSString *valueConfig = [[UserManagerV2 sharedUserManager] getCurrentUserConfigValueByKey:USER_CONFIG_CHECKLIST_DEFAULT_FULLMARK];
    
    if([valueConfig integerValue] == MHChecklistCheckMark_CheckToAdd) {
        return MHChecklistCheckMark_CheckToAdd;
    } else if ([valueConfig integerValue] == MHChecklistCheckMark_CheckToMinus){
        return MHChecklistCheckMark_CheckToMinus;
    }
    return MHChecklistCheckMark_Normal;
}

- (NSInteger)valueForCheckMarkRate {
    NSString *valueConfig = [[UserManagerV2 sharedUserManager] getCurrentUserConfigValueByKey:USER_CONFIG_CHECKLIST_DEFAULT_FULLMARK];
    //CHECKLIST_DEFAULT_FULLMARK = 1 or 2 is fullmark
    if([valueConfig integerValue] >= MHCheckMarkRate_FullMark) {
        return MHCheckMarkRate_FullMark;
    }
    return MHCheckMarkRate_Normal;
}

-(void) setIsChecklistFullMark:(BOOL)fullMarkValue
{
    //Change to common user
    [[UserManagerV2 sharedUserManager] setCommontUserConfig:USER_CONFIG_CHECKLIST_DEFAULT_FULLMARK value:[NSString stringWithFormat:@"%d", fullMarkValue ? 1 : 0]];
}

//Only for supervisor to get all room inspections or only get room inspection today
//If isEnable = YES will not filter rooms today
//If isEnable = NO filter rooms today
-(void) setEnableRoomMatrix:(BOOL)isEnable
{
    //Change to common user
    [[UserManagerV2 sharedUserManager] setCurrentUserConfig:USER_CONFIG_ENABLE_INSPECTION_MATRIX value:[NSString stringWithFormat:@"%d", isEnable ? 1 : 0]];
}

-(BOOL) isEnableRoomMatrix
{
    NSString *valueConfig = [[UserManagerV2 sharedUserManager] getCurrentUserConfigValueByKey:USER_CONFIG_ENABLE_INSPECTION_MATRIX];
    int valueInt = 1; //Default is get today data
    if(valueConfig.length > 0) {
        valueInt = [valueConfig intValue];
    }
    return (valueInt > 0);
}

//Check for posting New number depends on Used number or not
//If return YES: Used number change will not affect to New number
//If return NO: Used number change will affect to New number
-(BOOL) isLinenSeperateUsedNew
{
    NSString *valueConfig = [[UserManagerV2 sharedUserManager] getCurrentUserConfigValueByKey:USER_CONFIG_IS_LINEN_SEPERATE_NEW_AND_USED];
    int valueInt = 1;
    if(valueConfig.length > 0) {
        valueInt = [valueConfig intValue];
    }
    return (valueInt > 0);
}


//Check whether showing physical check or not
//Return true: show physical check remark
//Return false: Hide physical check remark
-(BOOL) isShowPhyshicalCheckRemark
{
    NSString *valueConfig = [[UserManagerV2 sharedUserManager] getCurrentUserConfigValueByKey:USER_CONFIG_PHYSICAL_CHECK_SHOW_REMARKS];
    int valueInt = 0;
    if(valueConfig.length > 0) {
        valueInt = [valueConfig intValue];
    }
    return (valueInt > 0);
}

//whether get physical check value after input room number or not
//return true: will get physical check remark value
//return false: will not get physical check remark value
-(BOOL) isGetPhysicalCheckRemarkValue
{
    NSString *valueConfig = [[UserManagerV2 sharedUserManager] getCurrentUserConfigValueByKey:USER_CONFIG_PHYSICAL_CHECK_APPEND_TO_EXISTING_REMARKS];
    int valueInt = 0;
    if(valueConfig.length > 0) {
        valueInt = [valueConfig intValue];
    }
    return (valueInt > 0);
}
//CRF-00000516 / Find Function Enhancement - MHSKP-328
//Return true: current Find Room will become Find by Status and show NEW option, Find by Room Number.
//Return false: used old Find Method (no Find Room Number).
-(NSInteger*) isFindRoomNo
{
    NSString *valueConfig = [[UserManagerV2 sharedUserManager] getCurrentUserConfigValueByKey:USER_CONFIG_FIND_FUNCTION_DISPLAY_METHOD];
    int valueInt = 0;
    if(valueConfig.length > 0) {
        valueInt = [valueConfig intValue];
    }
    return valueInt;
}

//CRF - 00000797
//1. Room listings for Room Release on above API
//2. GetCommonConfigurations
//3. Screen for Home, Find Module need to add this indicator if RELEASE_ROOM_ENABLE=1
-(BOOL) isEnableReleaseRoom
{
    NSString *valueConfig = [[UserManagerV2 sharedUserManager] getCurrentUserConfigValueByKey:USER_CONFIG_RELEASE_ROOM_ENABLE];
    int valueInt = 0;
    if(valueConfig.length > 0) {
        valueInt = [valueConfig intValue];
    }
    return (valueInt > 0);
}

-(BOOL)isEnablePanicButton{
    
    NSString *valueConfig = [[UserManagerV2 sharedUserManager] getCurrentUserConfigValueByKey:USER_CONFIG_PANIC_BUTTON_ENABLE];
    int valueInt = 0;
    if(valueConfig.length > 0) {
        valueInt = [valueConfig intValue];
    }
    return (valueInt > 0);
}

/*
 CRF-00000482 [v2.1, v2.2.1, v2.2.2] => For Sheraton Towers Singapore. [Priority: m-HK 2]. Target: 10-Sep-2014.
 - Description:-
 1. When RA enter value in "New", the value will copy to "used" field.
 2. This is to prevent user enter wrong field and cause zero posting
 - Technical Description: Should rely on INI control. I remember for Used to automatically update New, there’s an INI, and to break-off there’s an INI, what’s the INI name, let’s utilize this.
 */
-(int)miniBarSynchronizeUsedAndNew{
    NSString *valueConfig = [[UserManagerV2 sharedUserManager] getCurrentUserConfigValueByKey:USER_CONFIG_MINIBAR_SYNCHRONIZE_USED_AND_NEW];
    return [valueConfig intValue];
}

/* DungPhan - May 22, 2015
 CRF-00001276
 Description: Only "Used" Column is Required for All Minibar / Linen Posting: Client requested the "New" Column to be invisible for Minibar & Linen Posting.
 Accepted Values: 0 = No Change, 1 = New Only, 2 = Used Only
 */
-(int)minibarNewUsedDisplay{
    NSString *valueConfig = [[UserManagerV2 sharedUserManager] getCurrentUserConfigValueByKey:USER_CONFIG_MINIBAR_NEW_USED_DISPLAY];
    return [valueConfig intValue];
}

-(int)linenNewUsedDisplay{
    NSString *valueConfig = [[UserManagerV2 sharedUserManager] getCurrentUserConfigValueByKey:USER_CONFIG_LINEN_NEW_USED_DISPLAY];
    return [valueConfig intValue];
}

-(BOOL)isPhysicalCheckRoomStatusFromRoomTable{
    NSString *valueConfig = [[UserManagerV2 sharedUserManager] getCurrentUserConfigValueByKey:USER_CONFIG_PHYSICALCHECK_ROOMSTATUS_FROM_ROOMTABLE];
    int valueInt = 0;
    if(valueConfig.length > 0) {
        valueInt = [valueConfig intValue];
    }
    return (valueInt > 0);
}

-(BOOL) isPostingZeroQuantity
{
    NSString *valueConfig = [[UserManagerV2 sharedUserManager] getCurrentUserConfigValueByKey:USER_CONFIG_POSTING_POST_ZEROTH_VALUE];
    int valueInt = 0;
    if(valueConfig.length > 0) {
        valueInt = [valueConfig intValue];
    }
    return (valueInt > 0);
}

//CRF-00000995: New remark type for consolidation remark
//REMARKS_TYPE INI - if 1: new type of remark, if 0: no change (use the old type of remark)
-(BOOL) isRemarkConsolidation
{
    NSString *valueConfig = [[UserManagerV2 sharedUserManager] getCurrentUserConfigValueByKey:USER_CONFIG_REMARKS_TYPE];
    int valueInt = 0;
    if(valueConfig.length > 0) {
        valueInt = [valueConfig intValue];
    }
    
    return (valueInt > 0);
}
+ (BOOL)isEnableEncryptedQRCode{
    NSString *valueConfig = [[UserManagerV2 sharedUserManager] getCurrentUserConfigValueByKey:@"SYSTEM_ENABLE_ENCRYPTED_QRCODE"];
    int valueInt = 0;
    if(valueConfig.length > 0) {
        valueInt = [valueConfig intValue];
    }
    
    return (valueInt > 0);
}
+ (NSString*)getKey{
    return [UserManagerV2 sharedUserManager].strKey;
    
}
+ (NSString*)getIV{
    return [UserManagerV2 sharedUserManager].strIV;
}
/*!
 SYNC_INTERVAL time for supervisor
 */
-(NSInteger)syncSUPInterval
{
    NSString *valueConfig = [[UserManagerV2 sharedUserManager] getCurrentUserConfigValueByKey:USER_CONFIG_SUP_SYNC_INTERVAL];
    NSInteger syncIntervalTime = [valueConfig integerValue];
    if (syncIntervalTime == NSNotFound) {
        return 180;//Default time
    } else if (syncIntervalTime < 180) {
        return 180;//Default time
    } else {
        return syncIntervalTime;
    }
}

/*!
 SYNC_INTERVAL time for room attendant
 */
-(NSInteger)syncRAInterval
{
    NSString *valueConfig = [[UserManagerV2 sharedUserManager] getCurrentUserConfigValueByKey:USER_CONFIG_RA_SYNC_INTERVAL];
    NSInteger syncIntervalTime = [valueConfig integerValue];
    if (syncIntervalTime == NSNotFound) {
        return 180;//Default time
    } else if (syncIntervalTime < 180) {
        return 180;//Default time
    } else {
        return syncIntervalTime;
    }
}

/*!
 CRF-1036: Concurrent License Control by Login Status
 */
-(NSInteger)userCheckingLoginInterval
{
    NSString *valueConfig = [[UserManagerV2 sharedUserManager] getCurrentUserConfigValueByKey:USER_CONFIG_USER_CHECKING_LOGIN_INTERVAL];
    NSInteger syncIntervalTime = [valueConfig integerValue];
    if (syncIntervalTime == NSNotFound) {
        return 300;//Default time
    } else if (syncIntervalTime < 300) {
        return 300;//Default time
    } else {
        return syncIntervalTime;
    }
}

/*! CRF-00001230: Clear History of old records posted
 Select to show current posting history config by GetCommonConfiguration API
 Example: API response 2, history will be show 2 days recently and the day before 2 days will be deleted
 */
-(NSInteger)countPostingHistoryDays
{
    NSString *valueConfig = [[UserManagerV2 sharedUserManager] getCurrentUserConfigValueByKey:USER_CONFIG_POSTING_HISTORY_DAYS];
    NSInteger deleteHistoryBeforeDay = [valueConfig integerValue];
    if (deleteHistoryBeforeDay == 0 || deleteHistoryBeforeDay == NSNotFound) {
        return 1;//Default time
    } else {
        return deleteHistoryBeforeDay;
    }
}

-(NSInteger)checklistMandatoryPass
{
    NSString *valueConfig = [[UserManagerV2 sharedUserManager] getCurrentUserConfigValueByKey:USER_CONFIG_CHECKLIST_MANDATORYPASS];
    NSInteger intValueConfig = [valueConfig integerValue];
    if (intValueConfig == NSNotFound) {
        return 0;
    } else {
        return intValueConfig;
    }
}

/*
 CRF-1259: Display filter for supervisor home screen
 
 return YES means enabled
 return NO means disabled
 */
-(BOOL) isEnabledSupHomeFilter
{
    NSString *valueConfig = [[UserManagerV2 sharedUserManager] getCurrentUserConfigValueByKey:USER_CONFIG_ENABLE_SUP_HOME_FILTER];
    NSInteger intValueConfig = [valueConfig integerValue];
    if (intValueConfig == NSNotFound) {
        return NO;
    } else {
        return intValueConfig > 0;
    }
}

-(BOOL) isEnablePostingHistory
{
    NSString *valueConfig = [[UserManagerV2 sharedUserManager] getCurrentUserConfigValueByKey:USER_CONFIG_ENABLE_ONLINE_POSTING_HISTORY];
    NSInteger intValueConfig = [valueConfig integerValue];
    if (intValueConfig == NSNotFound) {
        return NO;
    } else {
        return intValueConfig > 0;
    }
}

-(BOOL) continueFromPausedTime
{
    NSString *valueConfig = [[UserManagerV2 sharedUserManager] getCurrentUserConfigValueByKey:USER_CONFIG_CONTINUE_FROM_PAUSE_TIME];
    NSInteger intValueConfig = [valueConfig integerValue];
    if (intValueConfig == NSNotFound) {
        return NO;
    } else {
        return intValueConfig > 0;
    }
}

-(BOOL) isEnableGuestProfile
{
    NSString *valueConfig = [[UserManagerV2 sharedUserManager] getCurrentUserConfigValueByKey:USER_CONFIG_ENABLE_SEARCH_GUEST_PROFILE];
    NSInteger intValueConfig = [valueConfig integerValue];
    if (intValueConfig == NSNotFound) {
        return NO;
    } else {
        return intValueConfig > 0;
    }
}

-(BOOL) isRoomDetailsActionHistoryActive
{
    NSString *isActive = [[UserManagerV2 sharedUserManager] getConfigValueByUserID:[UserManagerV2 sharedUserManager].currentUser.userId AndKey:USER_CONFIG_ROOM_DETAILS_ACTION_HISTORY_ACTIVE];
    if(isActive.length > 0)
    {
        if([isActive caseInsensitiveCompare:@"yes"] == NSOrderedSame
           || [isActive caseInsensitiveCompare:@"true"] == NSOrderedSame
           || [isActive caseInsensitiveCompare:@"1"] == NSOrderedSame)
        {
            return YES;
        }
    }
    
    return NO;
}

-(BOOL) isRoomDetailsActionHistoryAllowedView
{
    NSString *isView = [[UserManagerV2 sharedUserManager] getConfigValueByUserID:[UserManagerV2 sharedUserManager].currentUser.userId AndKey:USER_CONFIG_ROOM_DETAILS_ACTION_HISTORY_VIEW];
    if(isView.length > 0)
    {
        if([isView caseInsensitiveCompare:@"yes"] == NSOrderedSame
           || [isView caseInsensitiveCompare:@"true"] == NSOrderedSame
           || [isView caseInsensitiveCompare:@"1"] == NSOrderedSame)
        {
            return YES;
        }
    }
    
    return NO;
}

-(BOOL) isFindActionHistoryActive
{
    NSString *isActive = [[UserManagerV2 sharedUserManager] getConfigValueByUserID:[UserManagerV2 sharedUserManager].currentUser.userId AndKey:USER_CONFIG_FIND_ACTION_HISTORY_ACTIVE];
    if(isActive.length > 0)
    {
        if([isActive caseInsensitiveCompare:@"yes"] == NSOrderedSame
           || [isActive caseInsensitiveCompare:@"true"] == NSOrderedSame
           || [isActive caseInsensitiveCompare:@"1"] == NSOrderedSame)
        {
            return YES;
        }
    }
    
    return NO;
}

-(BOOL) isFindActionHistoryAllowedView
{
    NSString *isView = [[UserManagerV2 sharedUserManager] getConfigValueByUserID:[UserManagerV2 sharedUserManager].currentUser.userId AndKey:USER_CONFIG_FIND_ACTION_HISTORY_VIEW];
    if(isView.length > 0)
    {
        if([isView caseInsensitiveCompare:@"yes"] == NSOrderedSame
           || [isView caseInsensitiveCompare:@"true"] == NSOrderedSame
           || [isView caseInsensitiveCompare:@"1"] == NSOrderedSame)
        {
            return YES;
        }
    }
    
    return NO;
}

#pragma mark - CRF-00000711
/*
 DungPhan - 20150908: CRF-711: backward compatibility for Additional Job
 if USE_ADDITIONALJOB_STATUS is 0, will use old method
 Where Stop, the Job Status is update to 0
 Where Complete, the Job Status is update to 1
 Hide Job Status button
*/
-(BOOL) isUseAdditionalJobStatusTable {
    
    NSString *valueConfig = [[UserManagerV2 sharedUserManager] getCurrentUserConfigValueByKey:USER_CONFIG_USE_ADDITIONALJOB_STATUS_TABLE];
    NSInteger intValueConfig = [valueConfig integerValue];
    if (intValueConfig == NSNotFound) {
        return NO;
    } else {
        return intValueConfig > 0;
    }
}

#pragma mark - CRF-00001262
/*
 CRF-00001262 > [PO] [Marriott] Guideline, Linen, Amenities, Minibar Based on Room Type
 **for backward compatibility**
 
 LOAD_AMENITIES_BY_ROOMTYPE_ENABLE
 LOAD_LINEN_BY_ROOMTYPE_ENABLE
 LOAD_MINIBAR_BY_ROOMTYPE_ENABLE
 
 return YES mean enable
 return NO mean disable
 */

-(BOOL) isLoadAmenitiesByRoomType {
    
    NSString *valueConfig = [[UserManagerV2 sharedUserManager] getCurrentUserConfigValueByKey:USER_CONFIG_LOAD_AMENITIES_BY_ROOMTYPE_ENABLE];
    NSInteger intValueConfig = [valueConfig integerValue];
    if (intValueConfig == NSNotFound) {
        return NO;
    } else {
        return intValueConfig > 0;
    }
}

-(BOOL) isLoadLinenByRoomType {
    
    NSString *valueConfig = [[UserManagerV2 sharedUserManager] getCurrentUserConfigValueByKey:USER_CONFIG_LOAD_LINEN_BY_ROOMTYPE_ENABLE];
    NSInteger intValueConfig = [valueConfig integerValue];
    if (intValueConfig == NSNotFound) {
        return NO;
    } else {
        return intValueConfig > 0;
    }
}

-(BOOL) isLoadMinibarByRoomType {
    
    NSString *valueConfig = [[UserManagerV2 sharedUserManager] getCurrentUserConfigValueByKey:USER_CONFIG_LOAD_MINIBAR_BY_ROOMTYPE_ENABLE];
    NSInteger intValueConfig = [valueConfig integerValue];
    if (intValueConfig == NSNotFound) {
        return NO;
    } else {
        return intValueConfig > 0;
    }
}

/*
 CRF-00001178 > Show Assigned Other Activity in Assignment List
 [OTHER_ACTIVITY]
 ENABLE_OTHER_ACTIVITY
 return YES mean enable Other Activity
 return NO mean disable Other Activity
 */
- (BOOL)isEnableOtherActivity {
    
    NSString *valueConfig = [[UserManagerV2 sharedUserManager] getCurrentUserConfigValueByKey:USER_CONFIG_ENABLE_OTHER_ACTIVITY];
    NSInteger intValueConfig = [valueConfig integerValue];
    if (intValueConfig == NSNotFound) {
        return NO;
    } else {
        return intValueConfig > 0;
    }
}

// CFG [20160914/CRF-00001439] - Special assignment method.
- (int)getRoomAssignmentSpecialAssignmentMethod {
    @try {
        NSString *valueConfig = [[UserManagerV2 sharedUserManager] getCurrentUserConfigValueByKey:USER_CONFIG_ROOM_ASSIGNMENT_SPECIAL_ASSIGNMENT_METHOD];
        NSInteger intValueConfig = [valueConfig integerValue];
        if (intValueConfig == NSNotFound) {
            return 0;
        } else {
            return (int)intValueConfig;
        }
    }
    @catch (NSException *ex) {
        NSLog(@">AccessRightModel.m>getSpecialAssignmentMethod>%@", ex.reason);
        return 0;
    }
}
// CFG [20160914/CRF-00001439] - End.

// CFG [20161221/CRF-00001557] - Special assignment method.
- (int)getInspectionCrossZoneMethod {
    @try {
        NSString *valueConfig = [[UserManagerV2 sharedUserManager] getCurrentUserConfigValueByKey:USER_CONFIG_INSPECTION_CROSS_ZONE_METHOD];
        NSInteger intValueConfig = [valueConfig integerValue];
        if (intValueConfig == NSNotFound) {
            return 0;
        } else {
            return (int)intValueConfig;
        }
    }
    @catch (NSException *ex) {
        NSLog(@">AccessRightModel.m>getInspectionCrossZoneMethod>%@", ex.reason);
        return 0;
    }
}
// CFG [20161221/CRF-00001557] - End.

// CFG [20160928/CRF-00001612] - Special assignment method.
- (NSString *) getConfigByKey: (NSString *)sKey  {
    @try {
        NSString *sReturn = [[UserManagerV2 sharedUserManager] getCurrentUserConfigValueByKey:sKey];
        return sReturn;
    }
    @catch (NSException *ex) {
        NSLog(@">AccessRightModel.m>getConfigByKey>%@", ex.reason);
        return @"";
    }
}
// CFG [20160928/CRF-00001612] - End.

@end
