//
//  MessageModelV2.h
//  mHouseKeeping
//
//  Created by TMS on 2/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MessageModelV2 : NSObject
{
    
}

@property NSInteger
message_id,
message_group_id,
message_owner_id,
message_kind,
message_count_photo_attached,
message_status;

@property (retain) NSString
*message_from_user_login_name,
*message_from_user_name,
*message_topic,
*message_content,
*message_last_modified;

@end
