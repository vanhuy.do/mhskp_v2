//
//  MessageModelV2.m
//  mHouseKeeping
//
//  Created by TMS on 2/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MessageModelV2.h"

@implementation MessageModelV2

@synthesize message_id,
message_group_id,
message_owner_id,
message_kind,
message_count_photo_attached,
message_status,
message_from_user_login_name,
message_from_user_name,
message_topic,
message_content,
message_last_modified;

@end
