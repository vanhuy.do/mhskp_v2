//
//  MessagePhotoModelV2.h
//  mHouseKeeping
//
//  Created by TMS on 3/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MessagePhotoModelV2 : NSObject
{

}
@property NSInteger message_photo_group_id;
@property (retain) NSData *message_photo;
@property (retain) NSString *message_photo_last_modified;

@end
