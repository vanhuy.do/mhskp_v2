//
//  MessageReceiverModelV2.h
//  mHouseKeeping
//
//  Created by TMS on 3/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MessageReceiverModelV2 : NSObject
{
}
@property NSInteger
message_receiver_owner_id,
message_receiver_id,
message_receiver_group_id;

@property (retain) NSString *message_receiver_last_modified;

@end
