//
//  MessageReceiverModelV2.m
//  mHouseKeeping
//
//  Created by TMS on 3/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MessageReceiverModelV2.h"

@implementation MessageReceiverModelV2

@synthesize message_receiver_owner_id,
message_receiver_id,
message_receiver_group_id,
message_receiver_last_modified;

@end
