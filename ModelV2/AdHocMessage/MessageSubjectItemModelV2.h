//
//  MessageSubjectItemModelV2.h
//  mHouseKeeping
//
//  Created by TMS on 3/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MessageSubjectItemModelV2 : NSObject
{
    NSInteger smiId;
    NSString *smiName;
    NSString *smiNameLang;
    NSInteger smiCategoryId;
    NSData *smiImage;
}
@property NSInteger smiId;
@property (nonatomic,strong) NSString *smiName;
@property (nonatomic,strong) NSString *smiNameLang;
@property NSInteger smiCategoryId;
@property (nonatomic,strong) NSData *smiImage;
@end
