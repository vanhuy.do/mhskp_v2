//
//  MessageSubjectItemModelV2.m
//  mHouseKeeping
//
//  Created by TMS on 3/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MessageSubjectItemModelV2.h"

@implementation MessageSubjectItemModelV2
@synthesize smiId,smiName,smiImage,smiNameLang,smiCategoryId;

- (MessageSubjectItemModelV2*) copy {
    MessageSubjectItemModelV2 *aSubject = [[MessageSubjectItemModelV2 alloc] init];
    aSubject.smiId = self.smiId;
    aSubject.smiName = self.smiName;
    aSubject.smiNameLang = self.smiNameLang;
    aSubject.smiCategoryId = self.smiCategoryId;
    aSubject.smiImage = [[NSData alloc] initWithData:self.smiImage];
    return aSubject;
}

@end
