//
//  MessageSubjectModelV2.h
//  mHouseKeeping
//
//  Created by TMS on 2/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MessageSubjectModelV2 : NSObject
{   
    NSInteger smc;
    NSString *smcName;
    NSString *smcNameLang;
    NSData *smcImage;
    
   
}
//
@property NSInteger smc;
@property (nonatomic,strong) NSString *smcName;
@property (nonatomic,strong) NSString *smcNameLang;
@property (nonatomic,strong) NSData *smcImage;

@end
