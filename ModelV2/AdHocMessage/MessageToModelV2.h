//
//  MessageToModelV2.h
//  mHouseKeeping
//
//  Created by TMS on 2/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MessageToModelV2 : NSObject
{
    NSInteger personId;
    NSString *personName;
    NSString *personTitle;
    BOOL isSelected;
}
@property  BOOL isSelected;

@property  NSInteger personId;
@property (nonatomic,strong) NSString *personName;
@property (nonatomic,strong) NSString *personTitle;
@end
