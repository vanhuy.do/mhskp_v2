//
//  UserDetailsModelV2.h
//  mHouseKeeping
//
//  Created by TMS on 3/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserDetailsModelV2 : NSObject
{
    NSInteger userId;
    NSString *userDetailsId;
    NSString *userDetailsName;
    NSInteger userDetailsHotelId;
    NSString *userDetailsFullName;
    NSString *userDetailsFullNameLang;
    NSString *userDetailsLastModified;
    NSInteger usrSupervisor;
    NSInteger userSupervisorId;
    BOOL isSelected;
}
@property  NSInteger userId;
@property  NSString *userDetailsId;
@property(nonatomic,strong) NSString *userDetailsName;
@property NSInteger userDetailsHotelId;
@property(nonatomic,strong) NSString *userDetailsFullName;
@property(nonatomic,strong) NSString *userDetailsFullNameLang;
@property(nonatomic,strong) NSString *userDetailsLastModified;
@property (nonatomic)  NSInteger usrSupervisor;
@property (nonatomic) NSInteger userSupervisorId;
@property  BOOL isSelected;
@end
