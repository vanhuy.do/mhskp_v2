//
//  UserDetailsModelV2.m
//  mHouseKeeping
//
//  Created by TMS on 3/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UserDetailsModelV2.h"

@implementation UserDetailsModelV2
@synthesize userId;
@synthesize userDetailsId;
@synthesize userDetailsName;
@synthesize userDetailsHotelId;
@synthesize userDetailsFullName;
@synthesize userDetailsFullNameLang;
@synthesize userDetailsLastModified;
@synthesize usrSupervisor;
@synthesize isSelected;
@synthesize userSupervisorId;
@end
