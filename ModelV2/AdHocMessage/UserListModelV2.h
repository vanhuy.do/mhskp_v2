//
//  UserListModelV2.h
//  mHouseKeeping
//
//  Created by TMS on 3/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserListModelV2 : NSObject
{
    /*
     CREATE TABLE 'user_list' (
                              'usr_id' INT NULL ,
                              'usr_name' TEXT NULL DEFAULT NULL ,
                              'usr_hotel_id' INT NULL DEFAULT NULL ,
                              'usr_fullname' TEXT NULL DEFAULT NULL ,
                              'user_fullnamelang' TEXT NULL DEFAULT NULL ,
                              'usr_last_modified' TEXT NULL DEFAULT NULL ,
                              PRIMARY KEY ('usr_id') )
    */
    NSInteger userListId;
    NSString *userListName;
    NSInteger userListHotelId;
    NSString *userListFullName;
    NSString *userListFullNameLang;
    NSString *userListLastModified;
    NSInteger usrSupervisor;
    NSInteger userSupervisorId;
    BOOL isSelected;
}
@property  NSInteger userListId;
@property(nonatomic,strong) NSString *userListName;
@property NSInteger userListHotelId;
@property(nonatomic,strong) NSString *userListFullName;
@property(nonatomic,strong) NSString *userListFullNameLang;
@property(nonatomic,strong) NSString *userListLastModified;
@property (nonatomic)  NSInteger usrSupervisor;
@property (nonatomic) NSInteger userSupervisorId;
@property  BOOL isSelected;
@end
