//
//  UserListModelV2.m
//  mHouseKeeping
//
//  Created by TMS on 3/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UserListModelV2.h"

@implementation UserListModelV2
@synthesize    userListId;
@synthesize userListName;
@synthesize userListHotelId;
@synthesize userListFullName;
@synthesize userListFullNameLang;
@synthesize userListLastModified;
@synthesize usrSupervisor;
@synthesize isSelected;
@synthesize userSupervisorId;
@end
