//
//  AddJobRoomModelV2.h
//  mHouseKeeping

#import <Foundation/Foundation.h>

@interface AddJobCategoryModelV2 : NSObject{
    
}

@property (nonatomic) NSInteger
addjob_category_id,
addjob_category_user_id;

@property (nonatomic, strong) NSString
*addjob_category_name,
*addjob_category_name_lang,
*addjob_category_name_lang2,
*addjob_category_last_modified;

@end
