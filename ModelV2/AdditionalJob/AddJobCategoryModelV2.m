//
//  AddJobItemModelV2.m
//  mHouseKeeping
//

#import "AddJobCategoryModelV2.h"

@implementation AddJobCategoryModelV2

@synthesize addjob_category_id,
addjob_category_name,
addjob_category_name_lang,
addjob_category_name_lang2,
addjob_category_last_modified,
addjob_category_user_id;

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}
@end
