//
//  AdditionalJobModelV2.h
//  mHouseKeeping

#import <Foundation/Foundation.h>

@interface AddJobDetailModelV2 : NSObject{
}

@property (nonatomic) NSInteger
addjob_detail_onday_addjob_id,
addjob_detail_room_id,
addjob_detail_expected_cleaning_time,
addjob_detail_room_assign_id,
addjob_detail_category_id,
addjob_detail_status,
addjob_detail_post_status,
addjob_detail_user_id,
addjob_detail_total_time; //second unit

@property (nonatomic, strong) NSString
*addjob_detail_start_time,
*addjob_detail_stop_time,
*addjob_detail_last_modified,
*addjob_detail_remark,
*addjob_detail_room_number,
*addjob_detail_assigned_date;

@end
