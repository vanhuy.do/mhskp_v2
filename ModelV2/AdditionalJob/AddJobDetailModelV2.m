//
//  AddJobDetailModelV2.m
//  mHouseKeeping
//

#import "AddJobDetailModelV2.h"

@implementation AddJobDetailModelV2

@synthesize addjob_detail_onday_addjob_id,
addjob_detail_room_id,
addjob_detail_expected_cleaning_time,
addjob_detail_room_assign_id,
addjob_detail_category_id,
addjob_detail_status,
addjob_detail_post_status,
addjob_detail_user_id,
addjob_detail_room_number,
addjob_detail_total_time,
addjob_detail_start_time,
addjob_detail_stop_time,
addjob_detail_last_modified,
addjob_detail_remark,
addjob_detail_assigned_date;

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}
@end
