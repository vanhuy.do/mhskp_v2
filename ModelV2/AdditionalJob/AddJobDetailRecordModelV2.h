//
//  AddJobDetailRecordModelV2.h
//  mHouseKeeping

#import <Foundation/Foundation.h>

@interface AddJobDetailRecordModelV2 : NSObject{
}

@property (nonatomic) NSInteger
addjob_detail_record_onday_addjob_id,
addjob_detail_record_post_status,
addjob_detail_record_user_id,
addjob_detail_record_operation;

@property (nonatomic, strong) NSString
*addjob_detail_record_time,
*addjob_detail_record_last_modified;

@end
