//
//  AddJobDetailRecordModelV2.m
//  mHouseKeeping
//

#import "AddJobDetailRecordModelV2.h"

@implementation AddJobDetailRecordModelV2

@synthesize addjob_detail_record_onday_addjob_id,
addjob_detail_record_post_status,
addjob_detail_record_user_id,
addjob_detail_record_operation,
addjob_detail_record_time,
addjob_detail_record_last_modified;

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}
@end
