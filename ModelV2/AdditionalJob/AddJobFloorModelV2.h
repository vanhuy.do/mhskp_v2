//
//  AddJobModelV2.h
//  mHouseKeeping

#import <Foundation/Foundation.h>

@interface AddJobFloorModelV2 : NSObject{
}

@property (nonatomic) NSInteger addjob_floor_id;
@property (nonatomic) NSInteger addjob_floor_search_id;
@property (nonatomic,strong) NSString *addjob_floor_name;
@property (nonatomic,strong) NSString *addjob_floor_name_lang;
@property (nonatomic,strong) NSString *addjob_floor_last_modified;
@property (nonatomic) NSInteger addjob_floor_user_id;
@property (nonatomic) NSInteger addjob_floor_number_of_job;

@end
