//
//  AddJobDetailModelV2.m
//  mHouseKeeping
//

#import "AddJobFloorModelV2.h"

@implementation AddJobFloorModelV2

@synthesize addjob_floor_id,
addjob_floor_search_id,
addjob_floor_name,
addjob_floor_name_lang,
addjob_floor_last_modified,
addjob_floor_user_id,
addjob_floor_number_of_job;

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}
@end
