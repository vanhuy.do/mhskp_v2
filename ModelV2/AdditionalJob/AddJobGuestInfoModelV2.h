//
//  AddJobGuestInfoModelV2.h
//  mHouseKeeping

#import <Foundation/Foundation.h>

@interface AddJobGuestInfoModelV2 : NSObject{
    
}

@property (nonatomic) NSInteger addjob_guestinfo_room_id, addjob_guestinfo_user_id, addjob_guestinfo_post_status;
@property (nonatomic, strong) NSString *addjob_guestinfo_guest_title, *addjob_guestinfo_guest_name, *addjob_guestinfo_lang_pref, *addjob_guestinfo_check_in_date, *addjob_guestinfo_check_out_date, *addjob_guestinfo_pref_desc, *addjob_guestinfo_last_modified, *addjob_guestinfo_housekeeping_name, *addjob_guestinfo_special_service, *addjob_guestinfo_guest_number, *addjob_guestinfo_vip;
@property (nonatomic, strong) NSData *addjob_guestinfo_photo;

@end
