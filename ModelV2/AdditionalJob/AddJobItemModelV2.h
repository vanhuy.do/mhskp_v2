//
//  AddJobItemModelV2.h
//  mHouseKeeping

#import <Foundation/Foundation.h>

@interface AddJobItemModelV2 : NSObject{
    
}

@property (nonatomic) NSInteger addjob_item_id, addjob_item_user_id, addjob_item_onday_addjob_id;

@property (nonatomic, strong) NSString *addjob_item_name, *addjob_item_name_lang, *addjob_item_name_lang2,*addjob_item_last_modified;
@end
