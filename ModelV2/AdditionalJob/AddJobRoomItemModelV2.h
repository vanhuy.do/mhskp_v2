//
//  AddJobRoomItemModelV2.h
//  mHouseKeeping

#import <Foundation/Foundation.h>

@interface AddJobRoomItemModelV2 : NSObject{
    
}

@property (nonatomic) NSInteger
addjob_roomitem_onday_addjob_id,
addjob_roomitem_item_id,
addjob_roomitem_user_id;

@property (nonatomic, strong) NSString *addjob_roomitem_last_modified;

@end
