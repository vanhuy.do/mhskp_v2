//
//  AddJobItemModelV2.m
//  mHouseKeeping
//

#import "AddJobRoomItemModelV2.h"

@implementation AddJobRoomItemModelV2

@synthesize
addjob_roomitem_onday_addjob_id,
addjob_roomitem_item_id,
addjob_roomitem_last_modified,
addjob_roomitem_user_id;

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}
@end
