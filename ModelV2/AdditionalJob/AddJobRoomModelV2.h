//
//  AddJobRoomModelV2.h
//  mHouseKeeping

#import <Foundation/Foundation.h>

@interface AddJobRoomModelV2 : NSObject{
    
}

@property (nonatomic) NSInteger
addjob_room_id,
addjob_room_assign_id,
addjob_room_status_id,
addjob_room_floor_id,
addjob_room_type_id,
addjob_room_number_pending_job,
addjob_room_search_id,
addjob_room_is_due_out,
addjob_room_user_id;

@property (nonatomic, strong) NSString
*addjob_room_remark,
*addjob_room_last_modified,
*addjob_room_number;

@end
