//
//  AddJobItemModelV2.m
//  mHouseKeeping
//

#import "AddJobRoomModelV2.h"

@implementation AddJobRoomModelV2

@synthesize addjob_room_id,
addjob_room_assign_id,
addjob_room_number,
addjob_room_status_id,
addjob_room_floor_id,
addjob_room_type_id,
addjob_room_number_pending_job,
addjob_room_search_id,
addjob_room_is_due_out,
addjob_room_remark,
addjob_room_last_modified,
addjob_room_user_id;

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}
@end
