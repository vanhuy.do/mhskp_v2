//
//  AddJobSearchModelV2.h
//  mHouseKeeping
//
//  Created by vinhnguyen on 9/17/13.
//
//

#import <Foundation/Foundation.h>

@interface AddJobSearchModelV2 : NSObject

@property (nonatomic) NSInteger addjob_search_id;
@property (nonatomic,strong) NSString *addjob_search_tasks_id;
@property (nonatomic,strong) NSString *addjob_search_last_modified;
@property (nonatomic) NSInteger addjob_search_user_id;
@end
