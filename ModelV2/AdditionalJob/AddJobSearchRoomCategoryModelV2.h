//
//  AddJobRoomItemModelV2.h
//  mHouseKeeping

#import <Foundation/Foundation.h>

@interface AddJobSearchRoomCategoryModelV2 : NSObject{
    
}

@property (nonatomic) NSInteger addjob_searchroomcategory_search_id,
addjob_searchroomcategory_room_id,
addjob_searchroomcategory_category_id,
addjob_searchroomcategory_user_id;

@property (nonatomic, strong) NSString *addjob_searchroomcategory_last_modified;

@end
