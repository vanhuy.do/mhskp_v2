//
//  AddJobItemModelV2.m
//  mHouseKeeping
//

#import "AddJobSearchRoomCategoryModelV2.h"

@implementation AddJobSearchRoomCategoryModelV2

@synthesize
addjob_searchroomcategory_search_id,
addjob_searchroomcategory_room_id,
addjob_searchroomcategory_category_id,
addjob_searchroomcategory_user_id,
addjob_searchroomcategory_last_modified;

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}
@end
