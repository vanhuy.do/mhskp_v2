//
//  AddJobStatusModel.h
//  mHouseKeeping
//
//  Created by Gambogo on 7/9/15.
//
//

#import <Foundation/Foundation.h>

@interface AddJobStatusModel : NSObject

@property (nonatomic) NSInteger addjob_status_id;

@property (nonatomic, strong) NSData *addjob_status_icon;
@property (nonatomic, strong) NSString *addjob_status_name, *addjob_status_lang, *addjob_status_last_modified;

@end
