//
//  AddJobStatusModel.m
//  mHouseKeeping
//
//  Created by Gambogo on 7/9/15.
//
//

#import "AddJobStatusModel.h"

@implementation AddJobStatusModel

@synthesize addjob_status_id, addjob_status_icon, addjob_status_lang, addjob_status_last_modified, addjob_status_name;

@end
