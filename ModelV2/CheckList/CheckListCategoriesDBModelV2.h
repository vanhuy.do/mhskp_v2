//
//  CheckListCategoriesDBModelV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 3/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CheckListCategoriesDBModelV2 : NSObject{
    NSInteger chcId;
    NSInteger chcFormId;
    NSString *chcName;
    NSString *chcNameLang;
}
@property (nonatomic, readwrite) NSInteger chcId;
@property (nonatomic, readwrite) NSInteger chcFormId;
@property (nonatomic, strong) NSString *chcName;
@property (nonatomic, strong) NSString *chcNameLang;
@end
