//
//  CheckListContentTypeModelV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CheckListContentTypeModelV2 : NSObject{
    NSInteger chkContentId;
    NSInteger chkTypeId;
    NSString *chkContentName;
    NSString *chkContentNameLang;
    NSString *chkLastModified;
    
}
@property (nonatomic, readwrite)    NSInteger    chkContentId;
@property (nonatomic, readwrite)    NSInteger chkTypeId;
@property (nonatomic, strong)     NSString    *chkContentName;
@property (nonatomic, strong)    NSString      *chkContentNameLang;
@property (nonatomic, strong)    NSString      *chkLastModified;

@end
