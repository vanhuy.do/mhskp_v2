//
//  CheckListDetailContentModelV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CheckListItemCoreDBModelV2.h"
#import "CheckListTypeModelV2.h"

@interface CheckListDetailContentModelV2 : NSObject{
    NSInteger chkDetailContentId;
    NSString *chkDetailContentName;
    NSInteger chkDetailContentRoomType;
    NSInteger chkDetailContentPointInpected;//chkItemPassScore (max score item)
    NSString *chkDetailContentLastUpdate;
    NSInteger chkContentId;
    NSString *chkDetailContentLang;
    NSInteger chkTypeId;
    
    CheckListItemCoreDBModelV2 *itemCoreModel;
    CheckListTypeModelV2 *typeModel;
}

@property (nonatomic, strong) CheckListItemCoreDBModelV2 *itemCoreModel;
@property (nonatomic, strong)  CheckListTypeModelV2 *typeModel;
@property (nonatomic, readwrite)    NSInteger    chkDetailContentId;
@property (nonatomic, strong)     NSString    *chkDetailContentName;
@property (nonatomic, readwrite) NSInteger chkDetailContentRoomType;
@property (nonatomic, readwrite)    NSInteger chkDetailContentPointInpected;
@property (nonatomic, strong)    NSString *chkDetailContentLastUpdate;
@property (nonatomic, readwrite) NSInteger chkContentId;
@property (nonatomic, strong)  NSString *chkDetailContentLang;
@property (nonatomic, readwrite)    NSInteger chkTypeId;
@property (nonatomic, readwrite)    NSInteger chkMandatoryPass;

@end
