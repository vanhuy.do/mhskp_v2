//
//  CheckListItemCoreDBModelV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 3/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CheckListItemCoreDBModelV2 : NSObject{
    NSInteger chkItemCoreId;
    NSInteger chkItemCoreDetailContentId;
    NSInteger chkItemCore;
    NSInteger chkFormScore;
    NSInteger chkPostStatus;
}
@property (nonatomic, readwrite) NSInteger chkItemCoreId;
@property (nonatomic, readwrite) NSInteger chkItemCoreDetailContentId;
@property (nonatomic, readwrite) NSInteger chkItemCore;
@property (nonatomic, readwrite) NSInteger chkFormScore;
@property (nonatomic, readwrite) NSInteger chkPostStatus;

@end
