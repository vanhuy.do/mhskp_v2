//
//  CheckListItemDBModelV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 3/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CheckListItemDBModelV2 : NSObject{
    NSInteger chkItemId;
    NSString *chkItemName;
    NSInteger chkItemRoomType;
    NSInteger chkItemPassScore;
    NSString *chkItemLastUpdate;
    NSInteger chkItemCategoryId;
    NSString *chkItemLang;
 
}
@property (nonatomic, readwrite) NSInteger chkItemId;
@property (nonatomic, strong) NSString *chkItemName;
@property (nonatomic, readwrite) NSInteger chkItemRoomType;
@property (nonatomic, readwrite) NSInteger chkItemPassScore;
@property (nonatomic, strong) NSString *chkItemLastUpdate;
@property (nonatomic, assign) NSInteger chkItemCategoryId;
@property (nonatomic, strong) NSString *chkItemLang;

@end
