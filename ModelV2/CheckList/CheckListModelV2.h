//
//  CheckListModelV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef enum {
    tChkFail = 1,
    tChkPass = 2,
    
}enumChkListStatus;
@interface CheckListModelV2 : NSObject{
    
    NSInteger      chkListId;
    NSInteger      chkListStatus;
    NSInteger chkListUserId;
    NSString *chkListRoomId;
    NSString *chkInspectDate;
    NSInteger chkPostStatus;
    NSInteger chkCore;
    

}

@property (nonatomic, readwrite)    NSInteger     chkListId;
@property (nonatomic, readwrite)    NSInteger     chkListStatus;
@property (nonatomic, readwrite) NSInteger chkListUserId;
@property (nonatomic, strong) NSString *chkListRoomId;
@property (nonatomic, strong) NSString *chkInspectDate;
@property (nonatomic, readwrite) NSInteger chkPostStatus;
@property (nonatomic, readwrite) NSInteger chkCore;



@end
