//
//  CheckListTypeModelV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum{
    tCheckmark = 0,
    tRating = 1,
}enumChkType;
typedef enum{
    tCompleted = 1,
    tNotCompleted = 0,
}enumChkStatus;

@interface CheckListTypeModelV2 : NSObject{
    NSInteger chkTypeId;
    NSString *chkTypeName;
    NSString *chkTypeLangName;
    NSInteger chkType;
    NSInteger chkTypeHotelId;
    CGFloat chkTypePercentageOverall;//chkFormPossiblePoint
    NSInteger chkTypePassingScore;
    NSString *chkTypeLastModified;
}

@property (nonatomic, readwrite)    NSInteger    chkTypeId;
@property (nonatomic, strong)     NSString    *chkTypeName;
@property (nonatomic, strong) NSString *chkTypeLangName;
@property (nonatomic, readwrite) NSInteger chkType;
@property (nonatomic, readwrite) NSInteger chkTypeHotelId;
@property (nonatomic, readwrite)    CGFloat chkTypePercentageOverall;
@property (nonatomic, readwrite) NSInteger chkTypePassingScore;
@property (nonatomic, strong) NSString *chkTypeLastModified;


@end
