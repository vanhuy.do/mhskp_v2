//
//  ChecklistFormScoreModel.h
//
//  Created by ThuongNM.
//  Copyright 2012 __TMS__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ChecklistFormScoreModel : NSObject {

	NSInteger dfId;
	NSInteger dfScore;
	NSInteger dfPostStatus;
	NSInteger dfChecklistId;
	NSInteger dfFormId;
	NSInteger dfUserId;
}

@property (nonatomic, assign) NSInteger dfId;
@property (nonatomic, assign) NSInteger dfScore;
@property (nonatomic, assign) NSInteger dfPostStatus;
@property (nonatomic, assign) NSInteger dfChecklistId;
@property (nonatomic, assign) NSInteger dfFormId;
@property (nonatomic, assign) NSInteger dfUserId;
@property (nonatomic, assign) NSInteger dfIsMandatoryPass;

@end