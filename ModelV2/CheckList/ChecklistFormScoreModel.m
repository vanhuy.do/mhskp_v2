//
//  ChecklistFormScoreModel.m
//
//  Created by ThuongNM.
//  Copyright 2012 TMS. All rights reserved.
//

#import "ChecklistFormScoreModel.h"

@implementation ChecklistFormScoreModel

@synthesize dfId;
@synthesize dfScore;
@synthesize dfPostStatus;
@synthesize dfChecklistId;
@synthesize dfFormId;
@synthesize dfUserId;

@end

