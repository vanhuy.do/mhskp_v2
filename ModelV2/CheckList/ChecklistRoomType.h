//
//  ChecklistRoomType.h
//  mHouseKeeping
//
//  Created by Gambogo on 1/19/15.
//
//

#import <Foundation/Foundation.h>

@interface ChecklistRoomType : NSObject

@property (nonatomic, assign) NSInteger clrtRoomTypeID, clrtChecklistID, userId;
@property (nonatomic, strong) NSString *lastModified;

@end
