//
//  AmenitiesCategoriesModelV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 3/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AmenitiesCategoriesModelV2 : NSObject {
    
	NSInteger amcaId;
	NSString *amcaName;
	NSInteger amcaHotelId;
	NSString *amcaNameLang;
	NSData *amcaImage;
//    NSInteger amcaService;
    NSString *amcaLastModified;
    
}

@property (nonatomic, assign) NSInteger amcaId;
@property (nonatomic, strong) NSString *amcaName;
@property (nonatomic, assign) NSInteger amcaHotelId;
@property (nonatomic, strong) NSString *amcaNameLang;
@property (nonatomic, strong) NSData *amcaImage;
//@property (nonatomic, assign) NSInteger amcaService;
@property (nonatomic, strong) NSString *amcaLastModified;

@end