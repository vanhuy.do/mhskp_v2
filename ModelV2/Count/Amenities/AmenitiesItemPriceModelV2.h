//
//  AmenitiesItemPriceModelV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 3/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AmenitiesItemPriceModelV2 : NSObject {
    
	NSInteger amipId;
	NSInteger amipItemId;
	NSInteger amipServiceId;
	CGFloat amipPrice;
    
}

@property (nonatomic, assign) NSInteger amipId;
@property (nonatomic, assign) NSInteger amipItemId;
@property (nonatomic, assign) NSInteger amipServiceId;
@property (nonatomic, assign) CGFloat amipPrice;

@end