//
//  AmenitiesItemsModelV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 3/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AmenitiesItemsModelV2 : NSObject {
    
	NSInteger itemId;
	NSString *itemName;
	NSString *itemLang;
	NSInteger itemCategoryId;
	NSData *itemImage;
    NSInteger itemRoomType;
    NSString *itemLastModified;
    
    NSInteger itemQty;
}

@property (nonatomic, assign) NSInteger itemId;
@property (nonatomic, strong) NSString *itemName;
@property (nonatomic, strong) NSString *itemLang;
@property (nonatomic, assign) NSInteger itemCategoryId;
@property (nonatomic, strong) NSData *itemImage;
@property (nonatomic, assign) NSInteger itemRoomType;
@property (nonatomic, strong) NSString *itemLastModified;

//@property (nonatomic, assign) NSInteger itemQty;
@property (nonatomic, assign) NSInteger itemUsed;
@property (nonatomic, assign) NSInteger itemNew;

@end