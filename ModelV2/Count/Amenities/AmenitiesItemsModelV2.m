//
//  AmenitiesItemsModelV2.m
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 3/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AmenitiesItemsModelV2.h"

@implementation AmenitiesItemsModelV2
@synthesize itemId;
@synthesize itemName;
@synthesize itemLang;
@synthesize itemCategoryId;
@synthesize itemImage;
@synthesize itemRoomType;
@synthesize itemLastModified;

//@synthesize itemQty;
@synthesize itemNew;
@synthesize itemUsed;

@end
