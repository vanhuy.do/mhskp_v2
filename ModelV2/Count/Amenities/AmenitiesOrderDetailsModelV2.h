//
//  AmenitiesOrderDetailsModelV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 3/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AmenitiesOrderDetailsModelV2 : NSObject {
    
}
@property (nonatomic, assign) NSInteger amdId;
@property (nonatomic, assign) NSInteger amdItemId;
@property (nonatomic, assign) NSInteger amdNewQuantity;
@property (nonatomic, assign) NSInteger amdUsedQuantity;
@property (nonatomic, assign) NSInteger amdCategoryId;
@property (nonatomic, retain) NSString *amdTractionDateTime;
@property (nonatomic, assign) NSInteger amdHotelId;
@property (nonatomic, assign) NSInteger amdRoomAssignId;
@property (nonatomic, retain) NSString *amdRoomNumber;
@property (nonatomic, assign) NSInteger amdUserId;

//@property (nonatomic, assign) NSInteger amdAmenitiesOrderId;
//@property (nonatomic, assign) NSInteger amdQuantity;
//@property (nonatomic, assign) NSInteger amdRoomTypeId;
@end
