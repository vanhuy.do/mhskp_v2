//
//  AmenitiesOrderDetailsModelV2.m
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 3/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AmenitiesOrderDetailsModelV2.h"

@implementation AmenitiesOrderDetailsModelV2

@synthesize
amdId,
amdItemId,
amdNewQuantity,
amdUsedQuantity,
amdCategoryId,
amdTractionDateTime,
amdHotelId,
amdRoomAssignId,
amdRoomNumber,
amdUserId;

@end
