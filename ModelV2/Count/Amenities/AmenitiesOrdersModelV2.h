//
//  AmenitiesOrdersModelV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 3/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AmenitiesOrdersModelV2 : NSObject {
    
	NSInteger amenId;
	NSInteger amenRoomId;
	NSInteger amenUserId;
	NSInteger amenStatus;
	NSString *amenCollectDate;
    NSInteger amenService;
    
}

@property (nonatomic, assign) NSInteger amenId;
@property (nonatomic, assign) NSInteger amenRoomId;
@property (nonatomic, assign) NSInteger amenUserId;
@property (nonatomic, assign) NSInteger amenStatus;
@property (nonatomic, strong) NSString *amenCollectDate;
@property (nonatomic, assign) NSInteger amenService;

@end