//
//  AmenitiesServiceModelV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 3/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AmenitiesServiceModelV2 : NSObject {
    
	NSInteger amsId;
	NSString *amsName;
	NSString *amsNameLang;
	NSString *amsLastModified;
    NSData *amsImage;
    
}

@property (nonatomic, assign) NSInteger amsId;
@property (nonatomic, strong) NSString *amsName;
@property (nonatomic, strong) NSString *amsNameLang;
@property (nonatomic, strong) NSString *amsLastModified;
@property (nonatomic, strong) NSData *amsImage;

@end
