//
//  AmenitiesCategoryModelV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 1/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AmenitiesCategoryModelV2 : NSObject{
    NSInteger amenitiesCategoryId;
    NSData *amenitiesCategoryImage;
    NSString *amenitiesCategoryTitle;
    NSInteger amenitiesLocationId;
    NSString *amenitiesCategoryNameLang;
}
@property (nonatomic, assign) NSInteger amenitiesCategoryId;
@property (nonatomic, strong) NSData *amenitiesCategoryImage;
@property (nonatomic, strong) NSString *amenitiesCategoryTitle;
@property (nonatomic, assign) NSInteger amenitiesLocationId;
@property (nonatomic, strong) NSString *amenitiesCategoryNameLang;

@end
