//
//  AmenitiesCategoryModelV2.m
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 1/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AmenitiesCategoryModelV2.h"

@implementation AmenitiesCategoryModelV2
@synthesize  amenitiesCategoryId;
@synthesize amenitiesCategoryImage;
@synthesize amenitiesCategoryTitle;
@synthesize amenitiesLocationId;
@synthesize amenitiesCategoryNameLang;

@end
