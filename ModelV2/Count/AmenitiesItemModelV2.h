//
//  AmenitiesItemModelV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 1/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AmenitiesItemModelV2 : NSObject {
    NSInteger amenitiesItemId;
    NSData *amenitiesItemImage;
    NSString *amenitiesItemTitle;
    NSString *amenitiesItemLang;
    NSInteger amenitiesItemQuantity;
    NSInteger amenitiesCategoryId;
    
}
@property (nonatomic, assign) NSInteger amenitiesItemId;
@property (nonatomic, strong) NSData *amenitiesItemImage;
@property (nonatomic, strong) NSString *amenitiesItemTitle;
@property (nonatomic, strong) NSString *amenitiesItemLang;
@property (nonatomic, assign) NSInteger amenitiesItemQuantity;
@property (nonatomic, assign) NSInteger amenitiesCategoryId;
///<NSCopying>
@end
