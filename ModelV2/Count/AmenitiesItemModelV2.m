//
//  AmenitiesItemModelV2.m
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 1/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AmenitiesItemModelV2.h"

@implementation AmenitiesItemModelV2
@synthesize amenitiesItemId;
@synthesize amenitiesItemImage;
@synthesize amenitiesItemTitle;
@synthesize amenitiesItemQuantity;
@synthesize amenitiesCategoryId;
@synthesize amenitiesItemLang;

//-(id)copyWithZone:(NSZone *)zone {
//    AmenitiesItemModelV2 *model = [AmenitiesItemModelV2 allocWithZone:zone];
//    model.amenitiesItemId = amenitiesItemId;
//    model.amenitiesItemImage = [amenitiesItemImage copy];
//    model.amenitiesItemTitle = [NSString stringWithFormat:@"%@", amenitiesItemTitle];
//    model.amenitiesItemQuantity = amenitiesItemQuantity;
//    model.amenitiesCategoryId = amenitiesCategoryId;
//    return model;
//}

@end
