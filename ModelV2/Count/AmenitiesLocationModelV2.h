//
//  AmenitiesLocationModelV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 1/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AmenitiesLocationModelV2 : NSObject{
    NSInteger amenitiesLocationId;
    NSData *amenitiesLocationImage;
    NSString *amenitiesLocationTitle;

}
@property (nonatomic, assign) NSInteger amenitiesLocationId;
@property (nonatomic, strong) NSData *amenitiesLocationImage;
@property (nonatomic, strong) NSString *amenitiesLocationTitle;
@end
