//
//  CountCategoryModelV2.h
//  mHouseKeeping
//
//  Created by TMS on 3/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CountCategoryModelV2 : NSObject{
    NSInteger countCategoryId;
    NSInteger countServiceId;
    NSString *countCategoryName;
    NSString *countHotelId;
    NSString *countCategoryNameLang;
    NSData *countCategoryImage;
    NSString *countCategoryLastModified;
}

@property (nonatomic, assign) NSInteger countCategoryId;
@property (nonatomic, assign) NSInteger countServiceId;
@property (nonatomic, strong) NSString *countCategoryName;
@property (nonatomic, strong) NSString *countHotelId;
@property (nonatomic, strong) NSString *countCategoryNameLang;
@property (nonatomic, strong) NSData *countCategoryImage;
@property (nonatomic, strong) NSString *countCategoryLastModified;

@end
