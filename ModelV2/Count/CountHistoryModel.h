//
//  CountHistoryModel.h
//  mHouseKeeping
//
//  Created by Mac User on 28/03/2013.
//
//

#import <Foundation/Foundation.h>

@interface CountHistoryModel : NSObject{
    NSInteger count_id;
    NSString *room_id;
    NSInteger count_item_id;
    NSInteger count_collected;
    NSInteger count_new;
    NSInteger count_service;
    NSString *count_date;
    NSInteger count_user_id;
    NSString *count_item_name;
}
@property (nonatomic) NSInteger count_id;
@property (nonatomic, strong) NSString *room_id;
@property (nonatomic) NSInteger count_item_id;
@property (nonatomic) NSInteger count_collected;
@property (nonatomic) NSInteger count_new;
@property (nonatomic) NSInteger count_service;
@property (nonatomic, strong) NSString *count_date;
@property (nonatomic) NSInteger count_user_id;
@property (nonatomic, strong) NSString *count_item_name;
@end
