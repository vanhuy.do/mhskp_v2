//
//  CountHistoryModel.m
//  mHouseKeeping
//
//  Created by Mac User on 28/03/2013.
//
//

#import "CountHistoryModel.h"

@implementation CountHistoryModel

@synthesize count_collected;
@synthesize count_date;
@synthesize room_id;
@synthesize count_id;
@synthesize count_item_id;
@synthesize count_new;
@synthesize count_service;
@synthesize count_user_id;
@synthesize count_item_name;
@end
