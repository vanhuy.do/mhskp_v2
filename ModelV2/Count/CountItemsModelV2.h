//
//  CountItemsModelV2.h
//  mHouseKeeping
//
//  Created by TMS on 3/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CountItemsModelV2 : NSObject {
    NSInteger countItemId;
    NSString *countItemName;
    CGFloat countUnitPrice;
    NSInteger countItemType;
    NSString *countItemLang;
    NSInteger countCategoryId;
    NSData *countItemImage;
    NSString *countItemLastModified;
    NSInteger countDefaultQuantity; // CFG [20160921/CRF-00001275]
}

@property (nonatomic, assign) NSInteger countItemId;
@property (nonatomic, strong) NSString *countItemName;
@property (nonatomic, assign) CGFloat countUnitPrice;
@property (nonatomic, assign) NSInteger countItemType;
@property (nonatomic, strong) NSString *countItemLang;
@property (nonatomic, assign) NSInteger countCategoryId;
@property (nonatomic, strong) NSData *countItemImage;
@property (nonatomic, strong) NSString *countItemLastModified;
@property (nonatomic, assign) NSInteger countDefaultQuantity; // CFG [20160921/CRF-00001275]

@end
