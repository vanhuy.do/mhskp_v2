//
//  CountItemsModelV2.m
//  mHouseKeeping
//
//  Created by TMS on 3/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CountItemsModelV2.h"

@implementation CountItemsModelV2
@synthesize countCategoryId, countItemId, countItemLang, countItemName, countItemType, countItemImage, countUnitPrice, countItemLastModified, countDefaultQuantity; // CFG [20160921/CRF-00001275]

@end
