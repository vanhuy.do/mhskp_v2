//
//  CountMoreInforModelV2.h
//  mHouseKeeping
//
//  Created by TMS on 3/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CountMoreInforModelV2 : NSObject {
    NSInteger countMoreInforId;
    NSInteger countCategoryId;
    NSInteger countRegularOrExpress;
    NSInteger countMaleOrFemale;
}

@property (nonatomic, assign) NSInteger countMoreInforId;
@property (nonatomic, assign) NSInteger countCategoryId;
@property (nonatomic, assign) NSInteger countRegularOrExpress;
@property (nonatomic, assign) NSInteger countMaleOrFemale;

@end
