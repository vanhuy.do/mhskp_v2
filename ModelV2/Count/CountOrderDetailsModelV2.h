//
//  CountOrderDetailsModelV2.h
//  mHouseKeeping
//
//  Created by TMS on 3/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CountOrderDetailsModelV2 : NSObject {
    NSInteger countOrderDetailId;
    NSInteger countOrderId;
    NSInteger countItemId;
    NSInteger countCollected;
    NSInteger countNew;
    NSInteger countQuantity;
    NSInteger countPostStatus;
    NSString *countCollectedDate;
}
@property (nonatomic, assign) NSInteger countOrderDetailId;
@property (nonatomic, assign) NSInteger countOrderId;
@property (nonatomic, assign) NSInteger countItemId;
@property (nonatomic, assign) NSInteger countCollected;
@property (nonatomic, assign) NSInteger countNew;
@property (nonatomic, assign) NSInteger countQuantity;
@property (nonatomic, assign) NSInteger countPostStatus;
@property (nonatomic, strong) NSString *countCollectedDate;


@end
