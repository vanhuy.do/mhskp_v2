//
//  CountOrdersModelV2.h
//  mHouseKeeping
//
//  Created by TMS on 3/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CountOrdersModelV2 : NSObject {
    
}

@property (nonatomic, assign) NSInteger countOrderId;
@property (nonatomic, assign) NSInteger countRoomAssignId;
@property (nonatomic, assign) NSInteger countUserId;
@property (nonatomic, assign) NSInteger countStatus;
@property (nonatomic, strong) NSString *countCollectDate;
@property (nonatomic, assign) NSInteger countServiceId;
@property (nonatomic, strong) NSString *countRoomNumber;
@property (nonatomic, assign) NSInteger countPostStatus;
@property (nonatomic, assign) NSInteger countOrderWSId;

@end
