//
//  CountServiceModelV2.h
//  mHouseKeeping
//
//  Created by TMS on 3/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    tStatusChecked = 1,
    tStatusNoCheck = 2,
    
}enumCountStatus;

typedef enum {
    cMinibar = 2,
    cLinen = 3,
    cAmenities = 4
}enumCountService;

@interface CountServiceModelV2 : NSObject {
    NSInteger countServiceId;
    NSString *countServiceKind;
    NSData *countServiceImage;
    NSString *countServiceName;
    NSString *countServiceNameLang;
}

@property (nonatomic, assign) NSInteger countServiceId;
@property (nonatomic, strong) NSString *countServiceKind;
@property (nonatomic, strong) NSData *countServiceImage;
@property (nonatomic, strong) NSString *countServiceName;
@property (nonatomic, strong) NSString *countServiceNameLang;

@end
