//
//  InstructionModelV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 1/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface InstructionModelV2 : NSObject{
    NSInteger instructionId;
    NSString *instructionName;
    BOOL  isStatus;
}
@property (readwrite, nonatomic) NSInteger instructionId;
@property (strong, nonatomic) NSString *instructionName;
@property (assign, nonatomic) BOOL isStatus;
@end
