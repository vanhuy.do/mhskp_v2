//
//  LinenCategoryModelV2.h
//  mHouseKeeping
//
//  Created by TMS on 2/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LinenCategoryModelV2 : NSObject
@property (nonatomic, assign) NSInteger linenCategoryID;
@property (nonatomic, strong) NSData *linenCategoryImage;
@property (nonatomic, strong) NSString *linenCategoryTitle;
@end
