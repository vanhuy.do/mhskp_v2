//
//  LinenItemModelV2.h
//  mHouseKeeping
//
//  Created by TMS on 2/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LinenItemModelV2 : NSObject {
    NSInteger linenItemID;
    NSData *linenItemImage;
    NSString *linenItemTitle;
    NSInteger linenItemUsed;
    NSInteger linenItemNew;
}

@property (nonatomic, assign) NSInteger linenItemID;
@property (nonatomic, strong) NSData *linenItemImage;
@property (nonatomic, strong) NSString *linenItemTitle;
@property (nonatomic, assign) NSInteger linenItemUsed;
@property (nonatomic, assign) NSInteger linenItemNew;
@end
