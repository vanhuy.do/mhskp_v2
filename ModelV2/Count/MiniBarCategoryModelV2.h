//
//  MiniBarCategoryModelV2.h
//  mHouseKeeping
//
//  Created by TMS on 2/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MiniBarCategoryModelV2 : NSObject
@property (nonatomic, assign) NSInteger minibarCategoryID;
@property (nonatomic, strong) NSData *minibarCategoryImage;
@property (nonatomic, strong) NSString *minibarCategoryTitle;
@end
