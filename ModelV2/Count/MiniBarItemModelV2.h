//
//  MiniBarItemModelV2.h
//  mHouseKeeping
//
//  Created by TMS on 2/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LinenItemModelV2.h"

@interface MiniBarItemModelV2 : NSObject
@property (nonatomic, assign) NSInteger minibarItemID;
@property (nonatomic, strong) NSData *minibarItemImage;
@property (nonatomic, strong) NSString *minibarItemTitle;
@property (nonatomic, assign) NSInteger minibarItemUsed;
@property (nonatomic, assign) NSInteger minibarItemNew;
@property (nonatomic, strong) NSString *CategoryDescription;

-(void) convertDataFromLinenToMiniBar:(LinenItemModelV2 *) model;
@end
