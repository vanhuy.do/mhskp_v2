//
//  MiniBarItemModelV2.m
//  mHouseKeeping
//
//  Created by TMS on 2/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MiniBarItemModelV2.h"

@implementation MiniBarItemModelV2
@synthesize minibarItemID, minibarItemNew, minibarItemUsed, minibarItemImage, minibarItemTitle;

-(void)convertDataFromLinenToMiniBar:(LinenItemModelV2 *)model {
    self.minibarItemID = model.linenItemID;
    self.minibarItemNew = model.linenItemNew;
    self.minibarItemUsed = model.linenItemUsed;
    self.minibarItemImage = model.linenItemImage;
    self.minibarItemTitle = model.linenItemTitle;
}
@end
