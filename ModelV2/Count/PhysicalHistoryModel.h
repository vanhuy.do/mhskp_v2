//
//  PhysicalHistoryModel.h
//  mHouseKeeping
//
//  Created by Mac User on 26/03/2013.
//
//

#import <Foundation/Foundation.h>

@interface PhysicalHistoryModel : NSObject{
    NSInteger ph_floor_id;
    NSString *ph_room_id;
    NSInteger ph_status;
    BOOL ph_dnd;
    NSString *ph_date;
    NSInteger ph_user_id;
}
@property (nonatomic) NSInteger ph_floor_id;
@property (nonatomic, strong) NSString *ph_room_id;
@property (nonatomic) NSInteger ph_status;
@property (nonatomic) BOOL ph_dnd;
@property (nonatomic, strong) NSString *ph_date;
@property (nonatomic) NSInteger ph_user_id;

@end
