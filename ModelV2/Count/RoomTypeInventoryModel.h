//
//  RoomTypeInventoryModel.h
//  mHouseKeeping
//
//  Created by DungPhan on 02/10/2015.
//

#import <Foundation/Foundation.h>

@interface RoomTypeInventoryModel : NSObject

@property (nonatomic, assign) NSInteger userId;
@property (nonatomic, assign) NSInteger itemId;
@property (nonatomic, assign) NSInteger categoryId;
@property (nonatomic, assign) NSInteger roomTypeId;
@property (nonatomic, assign) NSInteger serviceItem;
@property (nonatomic, assign) NSInteger hotelId;
@property (nonatomic, assign) NSInteger buildingId;
@property (nonatomic, assign) NSInteger sectionId;
@property (nonatomic, assign) NSInteger defaultQuantity; // CFG [20160921/CRF-00001275]

@end
