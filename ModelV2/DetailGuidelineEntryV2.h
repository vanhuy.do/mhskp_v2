//
//  DetailGuidelineEntryV2.h
//  mHouseKeeping
//
//  Created by TMS on 2/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DetailGuidelineEntryV2 : NSObject {
    NSString        *uid;
    NSMutableArray  *arrayImage;
    NSMutableArray  *arrayText;
    NSMutableArray  *arrayName;
    NSString        *textGuide;
}

@property (nonatomic, retain) NSString *uid;
@property (nonatomic, retain) NSMutableArray *arrayImage;
@property (nonatomic, retain) NSMutableArray *arrayText;
@property (nonatomic, retain) NSMutableArray *arrayName;
@property (nonatomic, retain) NSString *textGuide;
@end
