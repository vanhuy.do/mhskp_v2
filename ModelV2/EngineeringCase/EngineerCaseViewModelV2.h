//
//  EngineerCaseViewModelV2.h
//  mHouseKeeping
//
//  Created by ThuongNM on 3/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EngineerCaseViewModelV2 : NSObject { 
    NSInteger ec_id;
    NSInteger ec_category_id;
    NSInteger ec_item_id;
    NSInteger ec_detail_id;    
    NSInteger ec_index;    
}

@property (nonatomic, assign) NSInteger ec_id;
@property (nonatomic, assign) NSInteger ec_category_id;
@property (nonatomic, assign) NSInteger ec_item_id;
@property (nonatomic, assign) NSInteger ec_detail_id;
@property (nonatomic, assign) NSInteger ec_index;    

@end
