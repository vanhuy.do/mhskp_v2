//
//  EngineerImageModelV2.h
//  mHouseKeeping
//
//  Created by ThuongNM on 3/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EngineerImageModelV2 : NSObject {    
    NSInteger enim_id;
    NSData *enim_image;
    NSInteger enim_detail_id;
    NSInteger enim_case_id;
}

@property (nonatomic, assign) NSInteger enim_id;
@property (nonatomic, strong) NSData *enim_image;
@property (nonatomic, assign) NSInteger enim_detail_id;
@property (nonatomic, assign) NSInteger enim_case_id;
@end
