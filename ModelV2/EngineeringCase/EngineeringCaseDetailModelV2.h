//
//  EngineeringCaseDetailModelV2.h
//  mHouseKeeping
//
//  Created by ThuongNM on 3/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EngineeringCaseDetailModelV2 : NSObject {    
    NSInteger ecd_id;
    NSInteger ecd_user_id;
    NSString *ecd_room_id;
    NSInteger ecd_pos_status;
    NSString *ecd_date;
    NSString *ecd_reMark;
}

@property (nonatomic, assign) NSInteger ecd_id;
@property (nonatomic, assign) NSInteger ecd_user_id;
@property (nonatomic, strong) NSString *ecd_room_id;
@property (nonatomic, assign) NSInteger ecd_pos_status;
@property (nonatomic, strong) NSString *ecd_date;
@property (nonatomic, strong) NSString *ecd_reMark;

@end
