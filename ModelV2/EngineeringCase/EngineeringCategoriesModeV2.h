//
//  EngineeringCategoriesModeV2.h
//  mHouseKeeping
//
//  Created by ThuongNM on 3/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EngineeringCategoriesModeV2 : NSObject {
    NSInteger enca_id;
    NSString *enca_name;
    NSString *enca_lang_name;
    NSData *enca_image;
    NSString *enca_last_modified;
}

@property (nonatomic, assign) NSInteger enca_id;
@property (nonatomic, strong) NSString *enca_name;
@property (nonatomic, strong) NSString *enca_lang_name;
@property (nonatomic, strong) NSData *enca_image;
@property (nonatomic, strong) NSString *enca_last_modified;

@end
