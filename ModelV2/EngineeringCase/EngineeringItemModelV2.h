//
//  EngineeringItemModelV2.h
//  mHouseKeeping
//
//  Created by ThuongNM on 3/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EngineeringItemModelV2 : NSObject {
    NSInteger eni_id;
    NSString *eni_name;
    NSString *eni_name_lang;
    NSInteger eni_category_id;
    NSString *eni_last_modified;
    NSString *eni_post_id;
}

@property (nonatomic, assign) NSInteger eni_id;
@property (nonatomic, strong) NSString *eni_name;
@property (nonatomic, strong) NSString *eni_name_lang;
@property (nonatomic, assign) NSInteger eni_category_id;
@property (nonatomic, strong) NSString *eni_last_modified;
@property (strong, nonatomic) NSString *eni_post_id;

@end
