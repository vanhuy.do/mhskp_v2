//
//  EngineeringHistoryModel.h
//  mHouseKeeping
//
//  Created by Mac User on 02/04/2013.
//
//

#import <Foundation/Foundation.h>

@interface EngineeringHistoryModel : NSObject{
    NSInteger eh_id;
    NSInteger eh_user_id;
    NSString *eh_room_id;
    NSInteger eh_item_id;
    NSString *eh_date;
    NSString *eh_item_name;
}
@property (nonatomic, assign) NSInteger eh_id;
@property (nonatomic, assign) NSInteger eh_user_id;
@property (nonatomic, strong) NSString *eh_room_id;
@property (nonatomic, assign) NSInteger eh_item_id;
@property (nonatomic, strong) NSString *eh_date;
@property (nonatomic, strong) NSString *eh_item_name;

@end
