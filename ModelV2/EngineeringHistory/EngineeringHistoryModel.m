//
//  EngineeringHistoryModel.m
//  mHouseKeeping
//
//  Created by Mac User on 02/04/2013.
//
//

#import "EngineeringHistoryModel.h"

@implementation EngineeringHistoryModel

@synthesize eh_id, eh_user_id, eh_room_id, eh_item_id, eh_date;
@synthesize eh_item_name;
@end
