//
//  FindCellObject.h
//  mHouseKeeping
//
//  Created by Dung Phan on 3/3/15.
//
//

#import <Foundation/Foundation.h>

@interface FindCellObject : NSObject

@property (nonatomic, strong) NSString *imageName;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, assign) NSInteger tag;

@end
