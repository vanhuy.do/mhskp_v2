//
//  GuestInfoModelV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GuestInfoModelV2 : NSObject{
//    NSInteger         guestId;
//    NSString    *guestName;
//    NSData    *guestImage;
//    NSInteger guestNumberVIP;
//    BOOL guestStatusVIP;
//    NSString *guestHousekeeperName;
//    NSString    *guestLangPref; 
//    NSString    *guestCheckInTime;
//    NSString    *guestCheckOutTime;
//    NSString    *guestGuestRef;
//    NSString *guestRoomId;
//   
//    NSInteger         guestPostStatus;
//    NSString    *guestLastModified;

}
@property (nonatomic, assign) NSInteger         guestId;
@property (nonatomic, strong) NSString    *guestName;
@property (nonatomic, strong) NSData    *guestImage;
@property (nonatomic, strong) NSString *guestVIPCode;
@property (nonatomic, assign) BOOL guestStatusVIP;
@property (nonatomic, strong) NSString *guestHousekeeperName;
@property (nonatomic, strong) NSString    *guestLangPref;
@property (nonatomic, strong) NSString    *guestCheckInTime;
@property (nonatomic, strong) NSString    *guestCheckOutTime;
@property (nonatomic, strong) NSString *guestNumber;
@property (nonatomic, strong) NSString *guestRoomId;

@property (nonatomic, assign) NSInteger   guestPostStatus;
@property (nonatomic, strong) NSString    *guestLastModified;
@property (nonatomic, assign) NSInteger guestType;

//@property (nonatomic, strong) NSString *guestGuestRef;
@property (nonatomic, strong) NSString *guestTitle;
//@property (nonatomic, strong) NSString *guestSpecialService;
@property (nonatomic, strong) NSString *guestSpecialService;
@property (nonatomic, strong) NSString *guestPreferenceCode;

@end
