//
//  GuestInfoModelV2.m
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GuestInfoModelV2.h"

@implementation GuestInfoModelV2
@synthesize guestId,
guestName,
guestImage,
guestRoomId,
guestLangPref,
guestVIPCode,
guestStatusVIP,
guestPostStatus,
guestCheckInTime,
guestCheckOutTime,
guestLastModified,
guestHousekeeperName,
guestNumber,
guestType,
//guestGuestRef,
guestPreferenceCode,
guestSpecialService,
guestTitle;

@end
