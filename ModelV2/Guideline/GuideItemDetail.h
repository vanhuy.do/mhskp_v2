//
//  GuideItemsV2.h
//  mHouseKeeping
//
//  Created by TMS on 3/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GuideItemDetail : NSObject {

}

@property (nonatomic) int gitem_id;
@property (nonatomic, strong) NSData *gitem_image;

@end
