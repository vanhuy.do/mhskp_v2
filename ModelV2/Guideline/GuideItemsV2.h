//
//  GuideItemsV2.h
//  mHouseKeeping
//
//  Created by TMS on 3/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GuideItemsV2 : NSObject {
    int gitem_id;
    int guide_CategoryId;
    NSString *gitem_Name;
    NSString *gitem_NameLang;
    NSString *gitem_content;
    NSString *gitem_content_lang;
    NSData *gitem_Image;
    NSString *gitem_last_modified;
}

@property (nonatomic) int gitem_id;
@property (nonatomic) int guide_CategoryId;
@property (nonatomic, strong) NSString *gitem_Name;
@property (nonatomic, strong) NSString *gitem_NameLang;
@property (nonatomic, strong) NSString *gitem_content;
@property (nonatomic, strong) NSString *gitem_content_lang;
@property (nonatomic, strong) NSData *gitem_Image;
@property (nonatomic, strong) NSString *gitem_last_modified;

@end
