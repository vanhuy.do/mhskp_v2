//
//  GuideItemsV2.m
//  mHouseKeeping
//
//  Created by TMS on 3/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GuideItemsV2.h"

@implementation GuideItemsV2
@synthesize gitem_id;
@synthesize guide_CategoryId;
@synthesize gitem_Name;
@synthesize gitem_NameLang;
@synthesize gitem_content;
@synthesize gitem_content_lang;
@synthesize gitem_Image;
@synthesize gitem_last_modified;
@end
