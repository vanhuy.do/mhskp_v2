//
//  GuideModelV2.h
//  mHouseKeeping
//
//  Created by Nghia Truong on 3/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GuideModelV2 : NSObject {
    int guide_Id;
    int guide_RoomTypeId;
    NSString *guide_Name;
    NSString *guide_NameLang;
    NSData *guide_Image;
    NSString *guide_LastUpdated;
    int guide_CategoryId;
}

@property (nonatomic) int guide_Id;
@property (nonatomic) int guide_RoomTypeId;
@property (nonatomic, strong) NSString *guide_Name;
@property (nonatomic, strong) NSString *guide_NameLang;
@property (nonatomic, strong) NSData *guide_Image;
@property (nonatomic, strong) NSString *guide_LastUpdated;
@property (nonatomic, assign) int guide_CategoryId;
@end
