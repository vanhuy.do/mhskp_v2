//
//  GuideModelV2.m
//  mHouseKeeping
//
//  Created by Nghia Truong on 3/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GuideModelV2.h"

@implementation GuideModelV2

@synthesize guide_Id;
@synthesize guide_RoomTypeId;
@synthesize guide_Name;
@synthesize guide_NameLang;
@synthesize guide_Image;
@synthesize guide_LastUpdated;
@synthesize guide_CategoryId;

@end
