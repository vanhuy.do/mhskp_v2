//
//  GuidelineImageV2.h
//  mHouseKeeping
//
//  Created by TMS on 2/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GuidelineImageV2 : NSObject
{
    int guidelineImageId;
    NSString* guidelineName;
    UIImage* guidelineImage;
    
}
@property (strong, nonatomic) UIImage* guidelineImage;
@property (strong, nonatomic) NSString* guidelineName;
@property int guidelineImageId;
@end
