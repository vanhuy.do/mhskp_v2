//
//  HotelModelV2.h
//  mHouseKeeping
//
//  Created by TMS TRIBE on 3/20/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HotelModelV2 : NSObject{
    NSInteger hotel_id;
    NSString *hotel_lang;
    NSString *hotel_name_lang;
    NSData *hotel_logo;
    NSString *hotel_last_modified;
}

@property (nonatomic) NSInteger hotel_id;
@property (nonatomic,strong) NSString *hotel_lang;
@property (nonatomic,strong) NSString *hotel_name_lang;
@property (nonatomic,strong) NSData *hotel_logo;
@property (nonatomic,strong) NSString *hotel_last_modified;

@end
