//
//  HousekeeperRecordModelV2.h
//  mHouseKeeping
//
//  Created by Nghia Truong on 3/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HousekeeperRecordModelV2 : NSObject {
    int hkp_UserId;
    int hkp_TotalCleaningTime;
    int hkp_RoomsCleaned;
    NSString *hkp_RecordDate;
    int hkp_Posted;
}

@property (nonatomic) int hkp_UserId;
@property (nonatomic) int hkp_TotalCleaningTime;
@property (nonatomic) int hkp_RoomsCleaned;
@property (nonatomic, strong) NSString *hkp_RecordDate;
@property (nonatomic) int hkp_Posted;

@end
