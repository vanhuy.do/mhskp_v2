//
//  HousekeeperRecordModelV2.m
//  mHouseKeeping
//
//  Created by Nghia Truong on 3/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "HousekeeperRecordModelV2.h"

@implementation HousekeeperRecordModelV2

@synthesize hkp_UserId;
@synthesize hkp_TotalCleaningTime;
@synthesize hkp_RoomsCleaned;
@synthesize hkp_RecordDate;
@synthesize hkp_Posted;

- (id)init {
    self = [super init];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

@end
