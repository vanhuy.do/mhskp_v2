//
//  LanguageModelV2.h
//  mHouseKeeping
//
//  Created by TMS on 3/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LanguageModelV2 : NSObject {
    NSString    *lang_id;
    NSString    *lang_name;
    NSString    *lang_pack;
    NSString    *lang_active;
    NSString    *lang_last_modified;
    NSString    *lang_currency;
    NSString    *lang_decimal_place;
}

@property (nonatomic, strong) NSString  *lang_id;
@property (nonatomic, strong) NSString  *lang_name;
@property (nonatomic, strong) NSString  *lang_pack;
@property (nonatomic, strong) NSString  *lang_active;
@property (nonatomic, strong) NSString  *lang_last_modified;
@property (nonatomic, strong) NSString  *lang_currency;
@property (nonatomic, strong) NSString  *lang_decimal_place;


-(id)initWithLangId:(NSString*)langId langName:(NSString*)langName langPack:(NSString*)langPack langActive:(NSString*)langActive;

@end
