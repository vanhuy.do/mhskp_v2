//
//  LanguageModelV2.m
//  mHouseKeeping
//
//  Created by TMS on 3/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LanguageModelV2.h"

@implementation LanguageModelV2
@synthesize lang_id;
@synthesize lang_name;
@synthesize lang_pack;
@synthesize lang_active;
@synthesize lang_last_modified;
@synthesize lang_currency;
@synthesize lang_decimal_place;


-(id)initWithLangId:(NSString*)langId langName:(NSString *)langName langPack:(NSString *)langPack langActive:(NSString *)langActive
{
    self = [super init];
    self.lang_id = langId;
    self.lang_name  = langName;
    self.lang_pack = langPack;
    self.lang_active = langActive;
    return self;
    
}



//-(void)dealloc
//{
//    [lang_id release];
//    [lang_name release];
//    [lang_pack release];
//    [lang_active release];
//    [lang_last_modified release];
//    [lang_decimal_place release];
//    [lang_currency release];
//    [super dealloc];
//}

@end
