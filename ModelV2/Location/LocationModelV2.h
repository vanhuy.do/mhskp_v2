//
//  LocationModelV2.h
//  mHouseKeeping
//
//  Created by Nghia Truong on 3/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LocationModelV2 : NSObject {
    int location_Id;
    int location_HotelId;
    NSString *location_Code;
    NSString *location_Desc;
    NSString *location_Lang;
    NSString *location_RoomNo;
    NSString *location_Type;
    NSString *location_LastModified;
}

@property (nonatomic) int location_Id;
@property (nonatomic) int location_HotelId;
@property (nonatomic,strong) NSString *location_Code;
@property (nonatomic,strong) NSString *location_Desc;
@property (nonatomic,strong) NSString *location_Lang;
@property (nonatomic,strong) NSString *location_RoomNo;
@property (nonatomic,strong) NSString *location_Type;
@property (nonatomic,strong) NSString *location_LastModified;

@end
