//
//  LocationModelV2.m
//  mHouseKeeping
//
//  Created by Nghia Truong on 3/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LocationModelV2.h"

@implementation LocationModelV2

@synthesize location_Id;
@synthesize location_HotelId;
@synthesize location_Code;
@synthesize location_Desc;
@synthesize location_Lang;
@synthesize location_RoomNo;
@synthesize location_Type;
@synthesize location_LastModified;

- (id)init {
    self = [super init];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

@end
