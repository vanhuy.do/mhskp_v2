//
//  LFCategoryModelV2.h
//  mHouseKeeping
//
//  Created by TMS on 1/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LFItemModelV2.h"

@interface LFCategoryModelV2 : NSObject {
    NSInteger lfcId;
    NSString *lfcName;
    NSString *lfcLang;
    NSData *lfcImage;
    NSString *lfcLastModified;
    
    LFItemModelV2 *lfItem;
    NSData *lfcColor;   
}

@property (nonatomic) NSInteger lfcID;
@property (nonatomic, strong) NSString *lfcName;
@property (nonatomic, strong) NSString *lfcLang;
@property (nonatomic, strong) NSData *lfcImage;
@property (nonatomic, strong) NSString *lfcLastModified;

@property (nonatomic, strong) LFItemModelV2 *lfItem;
@property (nonatomic, strong) NSData *lfcColor;

@end
