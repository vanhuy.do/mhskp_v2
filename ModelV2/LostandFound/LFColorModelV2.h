//
//  LFColorModelV2.h
//  mHouseKeeping
//
//  Created by TMS on 1/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LFColorModelV2 : NSObject {
    NSInteger lfcID;
    NSString *htmlCode;
    NSString *lfc_Name;
    NSString *lfc_lang;
    NSString *lfc_LastModifier;
}

@property (nonatomic) NSInteger lfcID;
@property (nonatomic, strong) NSString *htmlCode;
@property (nonatomic, strong) NSString *lfc_Name;
@property (nonatomic, strong) NSString *lfc_lang;
@property (nonatomic, strong) NSString *lfc_LastModifier;

@end
