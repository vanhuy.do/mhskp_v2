//
//  LFImageModelV2.h
//  mHouseKeeping
//
//  Created by TMS on 3/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    inDataBaseI = 0,
    isNeedCreateI,
    isNeedDeleteI
} LFImageStatus;

@interface LFImageModelV2 : NSObject {
    NSInteger lafImgId;
    NSData *lafImgImage;
    NSInteger lafDetailId;
    LFImageStatus lafStatus;
}

@property (nonatomic) NSInteger lafImgId;
@property (nonatomic, strong) NSData *lafImgImage;
@property (nonatomic) NSInteger lafDetailId;
@property LFImageStatus lafStatus;

@end
