//
//  LFItemModelV2.h
//  mHouseKeeping
//
//  Created by TMS on 1/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LFItemModelV2 : NSObject {
    NSInteger lfitID;
    NSString *lfitName;
    NSString *lfitLang;
    NSInteger lafi_category_id;
    NSData *lfitImage;
    NSString *lfitLastModified;
}

@property (nonatomic)  NSInteger lfitID;
@property (nonatomic, strong) NSString *lfitName;
@property (nonatomic, strong) NSString *lfitLang;
@property (nonatomic) NSInteger lafi_category_id;
@property (nonatomic, strong) NSData *lfitImage;
@property (nonatomic, strong) NSString *lfitLastModified;
@end
