//
//  LostandFoundDetailModelV2.h
//  mHouseKeeping
//
//  Created by TMS on 3/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef enum {
    haveDataBase = 0,
    isUpdate,
    isCreate,
    isDelete
} LostandFoundDetailStatus;

@interface LostandFoundDetailModelV2 : NSObject {
    NSInteger lafDetailId;
    NSInteger lafDetailUserId;
    NSString *lafDetailRoomId;
    NSString *lafDetailDate;
    NSString *lafDetailRemark;
    NSMutableArray *lafArray;
    NSMutableArray *lafPhoto;
    LostandFoundDetailStatus lafDetail_Status;
}

@property LostandFoundDetailStatus lafDetail_Status;
@property (nonatomic) NSInteger lafDetailId;
@property (nonatomic) NSInteger lafDetailUserId;
@property (nonatomic) NSInteger lafRoomAssignmentId;
@property (nonatomic, strong) NSString *lafDetailRoomId;
@property (nonatomic) NSInteger lafDetailPosStatus;
@property (nonatomic, strong) NSString *lafDetailDate;
@property (nonatomic, strong) NSString *lafDetailRemark;
@property (nonatomic, strong) NSMutableArray *lafArray;
@property (nonatomic, strong) NSMutableArray *lafPhoto;

@end
