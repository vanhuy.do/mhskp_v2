//
//  LostandFoundModelV2.h
//  mHouseKeeping
//
//  Created by TMS on 3/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LFColorModelV2.h"
#import "LFItemModelV2.h"
#import "LFCategoryModelV2.h"

typedef enum {
    inDataBase = 0,
    isNeedUpdate,
    isNeedCreate,
    isNeedDelete
} LostandFoundStatus;

@interface LostandFoundModelV2 : NSObject {
    NSInteger lafId;
    NSInteger lafCategoryId;
    NSInteger lafItemId;
    NSInteger lafColorId;
    NSInteger lafLaFDetailId;
    NSInteger laf_item_quantity;
    LostandFoundStatus laf_Status;
    LFCategoryModelV2 *lafCat;
    LFItemModelV2 *lafItem;
    LFColorModelV2 *lafColor;
}

@property (nonatomic) NSInteger lafId;
@property (nonatomic) NSInteger lafCategoryId;
@property (nonatomic) NSInteger lafItemId;
@property (nonatomic) NSInteger lafColorId;
@property (nonatomic) NSInteger lafLaFDetailId;
@property (nonatomic) NSInteger laf_item_quantity;
@property LostandFoundStatus laf_Status;
@property (nonatomic, strong) LFCategoryModelV2 *lafCat;
@property (nonatomic, strong) LFItemModelV2 *lafItem;
@property (nonatomic, strong) LFColorModelV2 *lafColor;
@end
