//
//  LostandFoundModelV2.m
//  mHouseKeeping
//
//  Created by TMS on 3/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LostandFoundModelV2.h"

@implementation LostandFoundModelV2
@synthesize lafId;
@synthesize lafCategoryId;
@synthesize lafItemId;
@synthesize lafColorId;
@synthesize lafLaFDetailId;
@synthesize laf_item_quantity;
@synthesize laf_Status;
@synthesize lafCat, lafItem, lafColor;
@end
