//
//  LostandFoundHistory.h
//  mHouseKeeping
//
//  Created by Quang Nguyen on 4/8/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

//#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface LostandFoundHistoryModel : NSObject{
    NSInteger lafh_id;
    NSString *lafh_room_id;
    NSInteger lafh_item_id;
    NSInteger lafh_color_id;
    NSInteger lafh_quantity;
    NSInteger lafh_user_id;
    NSString *lafh_date;
    NSString *lafh_item_name;
    NSString *lafh_category_name;
    NSString *lafh_color_code;
}
@property (nonatomic, assign) NSInteger lafh_id;
@property (nonatomic, strong) NSString *lafh_room_id;
@property (nonatomic, assign) NSInteger lafh_item_id;
@property (nonatomic, assign) NSInteger lafh_color_id;
@property (nonatomic, assign) NSInteger lafh_quantity;
@property (nonatomic, assign) NSInteger lafh_user_id;
@property (nonatomic, strong) NSString *lafh_date;
@property (nonatomic, strong) NSString *lafh_item_name;
@property (nonatomic, strong) NSString *lafh_category_name;
@property (nonatomic, strong) NSString *lafh_color_code;
@end
