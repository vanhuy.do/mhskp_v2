//
//  LostandFoundHistory.m
//  mHouseKeeping
//
//  Created by Quang Nguyen on 4/8/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "LostandFoundHistoryModel.h"

@implementation LostandFoundHistoryModel
@synthesize lafh_id;
@synthesize lafh_room_id;
@synthesize lafh_item_id;
@synthesize lafh_color_id;
@synthesize lafh_quantity;
@synthesize lafh_user_id;
@synthesize lafh_date;
@synthesize lafh_item_name;
@synthesize lafh_category_name;
@synthesize lafh_color_code;
@end
