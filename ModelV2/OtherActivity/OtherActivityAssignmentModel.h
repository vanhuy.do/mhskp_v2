//
//  OtherActivityAssignmentModel.h
//  mHouseKeeping
//
//  Created by DungPhan on 10/14/15.
//
//

#import <Foundation/Foundation.h>

@interface OtherActivityAssignmentModel : NSObject

@property (nonatomic) int oaa_id;
@property (nonatomic) int oaa_activity_id;
@property (nonatomic) int oaa_activities_id;
@property (nonatomic) int oaa_location_id;
@property (nonatomic) int oaa_status_id;
@property (nonatomic, strong) NSString *oaa_remark;
@property (nonatomic) int oaa_duration;
@property (nonatomic) int oaa_reminder;
@property (nonatomic) int oaa_remind;
@property (nonatomic, strong) NSString *oaa_assign_date;
@property (nonatomic, strong) NSString *oaa_start_time;
@property (nonatomic, strong) NSString *oaa_end_time;
@property (nonatomic, strong) NSString *oaa_user_start_time;
@property (nonatomic, strong) NSString *oaa_user_end_time;
@property (nonatomic, strong) NSString *oaa_user_stop_time;
@property (nonatomic, strong) NSString *oaa_decline_service_time;
@property (nonatomic) int oaa_duration_spent;
@property (nonatomic) int oaa_local_duration;
@property (nonatomic) int oaa_user_id;
@property (nonatomic) int oaa_is_single;
@property (nonatomic) int oaa_deleted;
@property (nonatomic, strong) NSString *oaa_last_modified;

@end
