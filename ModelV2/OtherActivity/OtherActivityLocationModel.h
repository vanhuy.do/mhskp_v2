//
//  OtherActivityLocationModel.h
//  mHouseKeeping
//
//  Created by DungPhan on 10/14/15.
//
//

#import <Foundation/Foundation.h>

@interface OtherActivityLocationModel : NSObject

@property (nonatomic) int oal_id;
@property (nonatomic, strong) NSString *oal_name;
@property (nonatomic, strong) NSString *oal_name_lang;
@property (nonatomic, strong) NSString *oal_code;
@property (nonatomic, strong) NSString *oal_desc;
@property (nonatomic, strong) NSString *oal_desc_lang;
@property (nonatomic) int oal_hotel_id;
@property (nonatomic, strong) NSString *oal_last_modified;

@end
