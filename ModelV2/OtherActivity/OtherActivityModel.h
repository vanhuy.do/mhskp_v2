//
//  OtherActivityModel.h
//  mHouseKeeping
//
//  Created by DungPhan on 10/14/15.
//
//

#import <Foundation/Foundation.h>

@interface OtherActivityModel : NSObject

@property (nonatomic) int oa_id;
@property (nonatomic, strong) NSString *oa_name;
@property (nonatomic, strong) NSString *oa_name_lang;
@property (nonatomic, strong) NSString *oa_code;
@property (nonatomic, strong) NSString *oa_desc;
@property (nonatomic, strong) NSString *oa_desc_lang;
@property (nonatomic, strong) NSString *oa_last_modified;

@end
