//
//  OtherActivityRecordModel.h
//  mHouseKeeping
//
//  Created by DungPhan on 10/14/15.
//
//

#import <Foundation/Foundation.h>

@interface OtherActivityRecordModel : NSObject

@property (nonatomic) int oar_id;
@property (nonatomic) int oar_onday_activity_id;
@property (nonatomic) int oar_user_id;
@property (nonatomic) int oar_operation;
@property (nonatomic) int oar_duration;
@property (nonatomic, strong) NSString *oar_time;
@property (nonatomic, strong) NSString *oar_remark;
@property (nonatomic) int oar_post_status;
@property (nonatomic, strong) NSString *oar_last_modified;

@end
