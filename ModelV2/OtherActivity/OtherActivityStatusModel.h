//
//  OtherActivityStatusModel.h
//  mHouseKeeping
//
//  Created by DungPhan on 10/14/15.
//
//

#import <Foundation/Foundation.h>

@interface OtherActivityStatusModel : NSObject

@property (nonatomic) int oas_id;
@property (nonatomic, strong) NSString *oas_name;
@property (nonatomic, strong) NSString *oas_lang;
@property (nonatomic, strong) NSData *oas_image;
@property (nonatomic, strong) NSString *oas_last_modified;

@end
