//
//  PanicReceiver.h
//  mHouseKeeping
//
//  Created by vinhnguyen on 12/16/13.
//
//

#import <Foundation/Foundation.h>

@interface PanicReceiver : NSObject
{
    
}

@property (strong, nonatomic) NSString
*panic_receivers_phone_number,
*panic_receivers_email,
*panic_receivers_receiver_name;

@property (assign, nonatomic) int
panic_receivers_owner_id,
panic_receivers_receiver_id;

@property (assign, nonatomic) bool
panic_receivers_is_direct_call;

@end
