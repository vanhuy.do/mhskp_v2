//
//  PanicReceiver.m
//  mHouseKeeping
//
//  Created by vinhnguyen on 12/16/13.
//
//

#import "PanicReceiver.h"

@implementation PanicReceiver

@synthesize panic_receivers_owner_id,
panic_receivers_receiver_id,
panic_receivers_phone_number,
panic_receivers_email,
panic_receivers_receiver_name,
panic_receivers_is_direct_call;

@end
