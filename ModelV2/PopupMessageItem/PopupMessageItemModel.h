//
//  PopupMessageItemModel.h
//  mHouseKeeping
//
//  Created by vinhnguyen on 1/13/14.
//
//

#import <Foundation/Foundation.h>

@interface PopupMessageItemModel : NSObject
{
    
}

@property (nonatomic, retain) NSString *popupMessage, *popupDateTime;
@property (nonatomic, assign) NSInteger popupId;

@end
