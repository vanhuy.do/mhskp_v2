//
//  ProfileNoteTypeV2.h
//  mHouseKeeping

#import <Foundation/Foundation.h>

@interface ProfileNoteTypeV2 : NSObject{
    
}

@property (nonatomic) NSInteger profilenote_type_id, profilenote_type_user_id, profilenote_type_guest_id;

@property (nonatomic, strong) NSString *profilenote_type_code, *profilenote_type_description, *profilenote_type_last_modified, *profilenote_type_roomnumber ;

@end
