//
//  ProfileNoteType.m
//  mHouseKeeping
//

#import "ProfileNoteTypeV2.h"

@implementation ProfileNoteTypeV2

@synthesize profilenote_type_code,
profilenote_type_description,
profilenote_type_id,
profilenote_type_guest_id,
profilenote_type_last_modified,
profilenote_type_roomnumber,
profilenote_type_user_id;

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}
@end
