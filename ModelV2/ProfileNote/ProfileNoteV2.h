//
//  ProfileNoteV2.h
//  mHouseKeeping

#import <Foundation/Foundation.h>

@interface ProfileNoteV2 : NSObject{
    
}

@property (nonatomic) NSInteger profilenote_type_id, profilenote_user_id, profilenote_guest_id;

@property (nonatomic, strong) NSString *profilenote_description, *profilenote_last_modified, *profilenote_room_number;

@end
