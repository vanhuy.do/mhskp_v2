//
//  ProfileNote.m
//  mHouseKeeping
//

#import "ProfileNoteV2.h"

@implementation ProfileNoteV2

@synthesize
profilenote_description,
profilenote_type_id,
profilenote_last_modified,
profilenote_user_id,
profilenote_room_number,
profilenote_guest_id;

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}
@end
