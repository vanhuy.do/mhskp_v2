//
//  CleaningStatusModelV2.h
//  mHouseKeeping
//
//  Created by TMS TRIBE on 3/6/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CleaningStatusModelV2 : NSObject{
    int cstat_Id;
    NSString *cstat_Name;
    NSString *cstat_Language;
    NSData *cstat_image;
    NSString *cstat_last_modified;
}
@property (nonatomic) int cstat_Id;
@property (nonatomic,strong) NSString *cstat_Name;
@property (nonatomic,strong) NSString *cstat_Language;
@property (nonatomic,strong) NSData *cstat_image;
@property (nonatomic,strong) NSString *cstat_last_modified;

@end
