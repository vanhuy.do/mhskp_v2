//
//  ReassignRoomAssignmentModel.h
//
//  Created by ThuongNM.
//  Copyright 2012 __TMS__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ReassignRoomAssignmentModel : NSObject {

	NSInteger reassignUserId;
	NSString *reassignRoomId;
//    NSInteger reassign_AssignRoomId;
	NSString *reassignDatetime;
	NSInteger reasignExpectedCleaningTime;
	NSInteger reassignRoomStatus;
	NSInteger reassignCleaningStatus;
	NSInteger reassignHousekeeperId;
	NSInteger reassignRoomType;
	NSInteger reassignFloorId;
	NSInteger reassignGuestProfileId;
	NSInteger reassignHotelId;
	NSInteger reassignRaId;
    NSString *cleaningCredit;
}

@property (nonatomic, assign) NSInteger reassignUserId;
@property (nonatomic, strong) NSString *reassignRoomId;
@property (nonatomic, strong) NSString *reassignDatetime;
@property (nonatomic, assign) NSInteger reasignExpectedCleaningTime;
@property (nonatomic, assign) NSInteger reassignRoomStatus;
@property (nonatomic, assign) NSInteger reassignCleaningStatus;
@property (nonatomic, assign) NSInteger reassignHousekeeperId;
@property (nonatomic, assign) NSInteger reassignRoomType;
@property (nonatomic, assign) NSInteger reassignFloorId;
@property (nonatomic, assign) NSInteger reassignGuestProfileId;
@property (nonatomic, assign) NSInteger reassignHotelId;
@property (nonatomic, assign) NSInteger reassignRaId;
@property (nonatomic, strong) NSString *reassignRemark;
@property (nonatomic, strong) NSString *cleaningCredit;
//@property (nonatomic, assign) NSInteger reass

@end
