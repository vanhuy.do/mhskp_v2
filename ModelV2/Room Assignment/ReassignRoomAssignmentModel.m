//
//  ReassignRoomAssignmentModel.m
//
//  Created by ThuongNM.
//  Copyright 2012 TMS. All rights reserved.
//

#import "ReassignRoomAssignmentModel.h"

@implementation ReassignRoomAssignmentModel

@synthesize reassignUserId;
@synthesize reassignRoomId;
@synthesize reassignDatetime;
@synthesize reasignExpectedCleaningTime;
@synthesize reassignRoomStatus;
@synthesize reassignCleaningStatus;
@synthesize reassignHousekeeperId;
@synthesize reassignRoomType;
@synthesize reassignFloorId;
@synthesize reassignGuestProfileId;
@synthesize reassignHotelId;
@synthesize reassignRaId;
@synthesize reassignRemark;
@synthesize cleaningCredit;

@end

