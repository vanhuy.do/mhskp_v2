//
//  RoomAssignmentModelV2.h
//  mHouseKeeping
//
//  Created by TMS TRIBE on 2/27/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RoomAssignmentModelV2 : NSObject{
    int roomAssignment_Id;
    NSString *roomAssignment_RoomId;
    int roomAssignment_UserId;
    NSString *roomAssignment_AssignedDate;
    NSString *roomAssignment_LastCleaningDate;
    int roomAssignment_Priority;
    int roomAssignment_PostStatus;		
    NSString *roomAssignment_LastModified;
    NSInteger roomAssignment_Priority_Sort_Order;
    NSInteger roomAssignmentHousekeeperId;
    NSInteger roomAssignmentRoomStatusId;
    NSInteger roomAssignmentRoomCleaningStatusId;
    NSInteger roomAssignmentRoomInspectionStatusId;
    NSInteger roomAssignmentRoomExpectedCleaningTime;
    NSInteger roomAssignmentRoomExpectedInspectionTime;
    NSInteger roomAssignmentGuestProfileId;
    NSString *roomAssignmentFirstName;
    NSString *roomAssignmentCleaningCredit;
    NSString *roomAssignmentChecklistRemark;
    NSString *roomAssignmentLastName;
    NSInteger roomAssignmentInspectedScore;
    
    NSString *raGuestArrivalTime;
    NSString *raGuestDepartureTime;
    NSInteger raKindOfRoom;
    NSInteger raIsMockRoom;
}

@property (nonatomic, strong) NSString *roomAssignmentFirstName;
@property (nonatomic, strong) NSString *roomAssignmentCleaningCredit;
@property (nonatomic, strong) NSString *roomAssignmentChecklistRemark;
@property (nonatomic, strong) NSString *roomAssignmentLastName;
@property (nonatomic, readwrite) NSInteger roomAssignmentGuestProfileId;
@property (nonatomic, readwrite) NSInteger roomAssignmentRoomExpectedCleaningTime;
@property (nonatomic, readwrite) NSInteger roomAssignmentRoomExpectedInspectionTime;
@property (nonatomic, readwrite) NSInteger roomAssignmentRoomStatusId;
@property (nonatomic, readwrite) NSInteger roomAssignmentRoomCleaningStatusId;
@property (nonatomic, readwrite) NSInteger roomAssignmentRoomInspectionStatusId;
@property (nonatomic, strong) NSString *roomAssignmentGuestFirstName;
@property (nonatomic, strong) NSString *roomAssignmentGuestLastName;
@property (nonatomic, assign) NSInteger roomAssignmentInspectedScore;
@property (nonatomic, assign) NSInteger roomAssignmentZoneId;
@property int roomAssignment_Id;
@property (nonatomic, strong) NSString *roomAssignment_RoomId;
@property int roomAssignment_UserId;
@property (nonatomic,strong)  NSString *roomAssignment_AssignedDate;
@property (nonatomic,strong)  NSString *roomAssignment_LastCleaningDate;
@property int roomAssignment_Priority;
@property int roomAssignment_PostStatus;		
@property (nonatomic,strong) NSString *roomAssignment_LastModified;
@property (nonatomic, readwrite) NSInteger roomAssignment_Priority_Sort_Order;
@property (nonatomic, readwrite) NSInteger roomAssignmentHousekeeperId;

@property (nonatomic, strong) NSString *raGuestArrivalTime;
@property (nonatomic, strong) NSString *raGuestDepartureTime;
@property (nonatomic, assign) NSInteger raKindOfRoom;
@property (nonatomic, assign) NSInteger raIsMockRoom;
@property (nonatomic, assign) NSInteger raIsReassignedRoom;

@end
