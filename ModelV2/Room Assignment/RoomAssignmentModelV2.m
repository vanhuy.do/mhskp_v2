//
//  RoomAssignmentModelV2.m
//  mHouseKeeping
//
//  Created by TMS TRIBE on 2/27/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "RoomAssignmentModelV2.h"

@implementation RoomAssignmentModelV2

@synthesize  roomAssignment_Id;
@synthesize  roomAssignment_RoomId;
@synthesize  roomAssignment_UserId;
@synthesize  roomAssignment_AssignedDate;
@synthesize roomAssignment_LastCleaningDate;
@synthesize  roomAssignment_Priority;
@synthesize  roomAssignment_PostStatus;		
@synthesize  roomAssignment_LastModified;
@synthesize roomAssignment_Priority_Sort_Order;
@synthesize roomAssignmentHousekeeperId;
@synthesize roomAssignmentRoomStatusId;
@synthesize roomAssignmentRoomCleaningStatusId;
@synthesize roomAssignmentRoomInspectionStatusId;
@synthesize roomAssignmentRoomExpectedCleaningTime;
@synthesize roomAssignmentRoomExpectedInspectionTime;
@synthesize roomAssignmentGuestProfileId;
@synthesize roomAssignmentFirstName;
@synthesize roomAssignmentLastName;
@synthesize roomAssignmentGuestFirstName;
@synthesize roomAssignmentGuestLastName;
@synthesize roomAssignmentInspectedScore;
@synthesize roomAssignmentCleaningCredit;
@synthesize roomAssignmentChecklistRemark;

@synthesize raIsMockRoom, raKindOfRoom, raGuestDepartureTime, raGuestArrivalTime, raIsReassignedRoom;

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

@end

/*
 \ 'ra_id' INT NULL DEFAULT NULL ,
 'ra_room_id' INT NULL DEFAULT NULL ,
 'ra_user_id' INT NULL DEFAULT NULL ,
 'ra_assigned_date' TEXT NULL DEFAULT NULL ,
 'ra_prioprity' INT NULL DEFAULT NULL ,
 'ra_post_status' INT NULL DEFAULT -1 ,
 'ra_last_modified' TEXT NULL DEFAULT NULL ,
 */
