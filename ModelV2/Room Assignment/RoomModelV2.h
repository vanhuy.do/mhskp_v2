//
//  RoomModelV2.h
//  mHouseKeeping
//
//  Created by TMS TRIBE on 2/28/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface RoomModelV2 : NSObject{
    NSString *room_Id;
    int room_HotelId;
    //int room_StatusId;
    int room_TypeId;
    NSString *room_Lang;
    int room_LocationCode;
    NSString *room_Building_Name;
    NSString *room_VIPStatusId;
    NSString *room_GuestReference;
    NSString *room_AdditionalJob;
    int room_PostStatus;		

    NSString *room_LastModified;
    NSString *room_Guest_Name;
    NSString *room_building_namelang;
    
     NSInteger room_Building_Id;

    NSInteger room_is_re_cleaning;
    NSInteger  room_expected_status_id;
    NSInteger  room_zone_id;
       
    NSString *room_remark;
    BOOL room_isReassigned;
    BOOL room_isInpected;
   
    NSInteger room_floor_id;
}

/*property model RoomModel that map with table in DB*/
//@property (nonatomic,readwrite)  int room_building_id;
@property (nonatomic) NSInteger room_Building_Id;
@property (nonatomic,strong) NSString *room_Id;
@property (nonatomic,readwrite) int room_HotelId;
//@property (nonatomic,readwrite) int room_StatusId;
@property (nonatomic,readwrite) int room_TypeId;
@property (nonatomic,strong) NSString *room_Lang;
@property (nonatomic,readwrite) int room_LocationCode;
@property (nonatomic,strong)  NSString *room_Building_Name;
@property (nonatomic,strong) NSString *room_VIPStatusId;
//@property (nonatomic,readwrite) int room_CleaningStatusId;
//@property (nonatomic) int room_inspection_status_id;

//@property (nonatomic,strong) NSString *room_ExpectCleaningTime;
@property (nonatomic,strong) NSString *room_GuestReference;
@property (nonatomic,strong) NSString *room_AdditionalJob;
@property (nonatomic,readwrite) int room_PostStatus;		
@property (nonatomic,strong) NSString *room_LastModified;
@property (nonatomic,strong) NSString *room_Guest_Name;
@property (nonatomic, strong) NSString *room_building_namelang;

@property (nonatomic,strong)  NSString *room_remark;
//@property (nonatomic) int room_Status;
@property (nonatomic) BOOL room_isReassigned;
@property (nonatomic) BOOL room_isInpected;


//@property (nonatomic,strong) NSString *room_expected_inspection_time;
@property (nonatomic) NSInteger room_is_re_cleaning;
@property (nonatomic) NSInteger room_expected_status_id;
@property (nonatomic) NSInteger room_zone_id;
@property (nonatomic) NSInteger room_floor_id;

@end
