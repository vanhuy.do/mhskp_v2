//
//  RoomModelV2.m
//  mHouseKeeping
//
//  Created by TMS TRIBE on 2/28/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "RoomModelV2.h"

@implementation RoomModelV2
@synthesize  room_Id;
@synthesize  room_HotelId;
//@synthesize  room_StatusId;
@synthesize  room_TypeId;
@synthesize  room_Lang;
@synthesize  room_LocationCode;
@synthesize  room_Building_Name;
@synthesize  room_VIPStatusId;
//@synthesize  room_CleaningStatusId;
//@synthesize  room_inspection_status_id;
//@synthesize  room_ExpectCleaningTime;
@synthesize  room_GuestReference;
@synthesize  room_AdditionalJob;
@synthesize  room_PostStatus;		
@synthesize  room_LastModified;
@synthesize  room_remark;
//@synthesize  room_Status;

@synthesize  room_isReassigned;
@synthesize  room_isInpected;
@synthesize  room_Guest_Name;
@synthesize  room_building_namelang;

//@synthesize  room_expected_inspection_time;
@synthesize  room_is_re_cleaning;
@synthesize  room_expected_status_id;
@synthesize  room_zone_id;
@synthesize room_floor_id;
@synthesize room_Building_Id;

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

@end
