//
//  RoomRecordDetailModelV2.h
//  mHouseKeeping
//
//  Created by TMS TRIBE on 6/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RoomRecordDetailModelV2 : NSObject{
    NSInteger recd_Id;
    NSInteger recd_Ra_Id;
    NSInteger recd_User_Id;
    
    NSString *recd_Start_Date;
    NSString *recd_Stop_Date;
    
}

@property (nonatomic,strong) NSString *recd_Start_Date; //yyyy-MM-dd HH:mm:ss
@property (nonatomic,strong) NSString *recd_Stop_Date;

@property (nonatomic) NSInteger recd_Id;
@property (nonatomic) NSInteger recd_Ra_Id;
@property (nonatomic) NSInteger recd_User_Id;
@end
