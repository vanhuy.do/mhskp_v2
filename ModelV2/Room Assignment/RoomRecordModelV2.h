//
//  RoomRecordModelV2.h
//  mHouseKeeping
//
//  Created by TMS TRIBE on 2/29/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RoomRecordModelV2 : NSObject{
    
}

@property (nonatomic) int rrec_Id;
@property (nonatomic) int rrec_room_assignment_id;
@property (nonatomic) int rrec_User_Id;
@property (nonatomic) int rrec_PostStatus;
@property (nonatomic,strong) NSString *rrec_Remark;
@property (nonatomic,strong) NSString *rrec_Cleaning_Date;

//Cleaning property
@property (nonatomic,strong) NSString *rrec_Cleaning_Start_Time;
//@property (nonatomic,strong) NSString *rrec_Cleaning_Stop_Time;
@property (nonatomic,strong) NSString *rrec_Cleaning_End_Time;
@property (nonatomic,strong) NSString *rrec_Last_Cleaning_Duration;
@property (nonatomic) int rrec_Total_Cleaning_Time;
@property (nonatomic, strong) NSString *rrec_cleaning_pause_time;

//Hao Tran - Remove for unneccessary property
//@property (nonatomic,strong) NSString *rrec_Cleaning_Total_Pause_Time;
//@property (nonatomic, strong) NSString *rrec_Inspected_Total_Pause_Time;

//Inspecting property
@property (nonatomic,strong) NSString *rrec_Last_Inspected_Duration;
@property (nonatomic,strong) NSString *rrec_Inspection_Start_Time;
@property (nonatomic,strong) NSString *rrec_Inspection_Stop_Time;
@property (nonatomic,strong) NSString *rrec_Inspection_End_Time;
@property (nonatomic) int rrec_Total_Inspected_Time;

@end
