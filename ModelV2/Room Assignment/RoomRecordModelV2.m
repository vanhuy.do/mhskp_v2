//
//  RoomRecordModelV2.m
//  mHouseKeeping
//
//  Created by TMS TRIBE on 2/29/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "RoomRecordModelV2.h"

@implementation RoomRecordModelV2
@synthesize rrec_Id;
@synthesize rrec_room_assignment_id;
@synthesize rrec_User_Id;

//Cleaning
@synthesize rrec_Cleaning_Start_Time;
//@synthesize rrec_Cleaning_Stop_Time;
@synthesize rrec_Cleaning_End_Time;
@synthesize rrec_Cleaning_Date;

//Inspectingrrec_Cleaning_Stop_Time
@synthesize rrec_Inspection_Start_Time;
@synthesize rrec_Inspection_End_Time;
@synthesize rrec_PostStatus;
@synthesize rrec_Remark;

//Hao Tran - Add properties
@synthesize rrec_Inspection_Stop_Time;
@synthesize rrec_Last_Cleaning_Duration;
@synthesize rrec_Last_Inspected_Duration;
@synthesize rrec_Total_Cleaning_Time;
@synthesize rrec_Total_Inspected_Time;

//Hao Tran - Remove for unneccessary property
//@synthesize rrec_Cleaning_Total_Pause_Time;
//@synthesize rrec_Inspection_Duration;
//@synthesize rrec_Inspected_Total_Pause_Time;
//@synthesize rrec_Cleaning_Duration;

- (id)init {
    self = [super init];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

@end
