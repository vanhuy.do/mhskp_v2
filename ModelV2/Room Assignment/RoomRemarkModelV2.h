//
//  RoomRemarkModelV2.h
//  mHouseKeeping
//
//  Created by TMS TRIBE on 2/29/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RoomRemarkModelV2 : NSObject{
    NSInteger rm_Id;
    NSString *rm_Time;
    int rm_RecordId;
    int rm_PostStatus;
    
    NSString *rm_clean_pause_time;
    NSString *rm_content;
    
}
@property (nonatomic) NSInteger rm_Id;
@property (nonatomic,strong) NSString *rm_Time;
@property (nonatomic) int rm_RecordId;
@property (nonatomic) int rm_PostStatus;

@property (nonatomic,strong) NSString *rm_clean_pause_time;
@property (nonatomic,strong) NSString *rm_content;
@property (nonatomic,strong) NSString *rm_physical_content;
@property (nonatomic,strong) NSString *rm_roomassignment_roomId;
@end
