//
//  RoomRemarkModelV2.m
//  mHouseKeeping
//
//  Created by TMS TRIBE on 2/29/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "RoomRemarkModelV2.h"

@implementation RoomRemarkModelV2
@synthesize rm_Id;
@synthesize rm_Time;
@synthesize rm_RecordId;
@synthesize rm_PostStatus;
@synthesize rm_content;
@synthesize rm_clean_pause_time;
@synthesize rm_physical_content;
@synthesize rm_roomassignment_roomId;
- (id)init {
    self = [super init];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

@end
