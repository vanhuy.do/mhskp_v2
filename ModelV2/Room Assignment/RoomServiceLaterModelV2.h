//
//  RoomServiceLaterModelV2.h
//  mHouseKeeping
//
//  Created by TMS TRIBE on 2/29/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RoomServiceLaterModelV2 : NSObject{
    int rsl_Id;
    NSString *rsl_Time;
    int rsl_RecordId;
}
@property (nonatomic) int rsl_Id;
@property (nonatomic, strong) NSString *rsl_Time;
@property (nonatomic) int rsl_RecordId;

@end
