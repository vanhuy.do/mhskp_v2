//
//  RoomStatusModelV2.h
//  mHouseKeeping
//
//  Created by Nghia Truong on 3/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RoomStatusModelV2 : NSObject {
    int rstat_Id;
    NSString *rstat_Code;
    NSString *rstat_Name;
    NSString *rstat_Lang;
    NSData *rstat_Image;
    NSString *rstat_LastModified;
    int rstat_PhysicalCheck;
    int rstat_FindStatus;
}
@property (nonatomic) int rstat_Id;
@property (nonatomic,strong) NSString *rstat_Code;
@property (nonatomic,strong) NSString *rstat_Name;
@property (nonatomic,strong) NSString *rstat_Lang;
@property (nonatomic,strong) NSData *rstat_Image;
@property (nonatomic,strong) NSString *rstat_LastModified;
@property (nonatomic) int rstat_PhysicalCheck;
@property (nonatomic) int rstat_FindStatus;

@end
