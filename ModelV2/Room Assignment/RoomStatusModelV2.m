//
//  RoomStatusModelV2.m
//  mHouseKeeping
//
//  Created by Nghia Truong on 3/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "RoomStatusModelV2.h"

@implementation RoomStatusModelV2
@synthesize rstat_Id;
@synthesize rstat_Code;
@synthesize rstat_Name;
@synthesize rstat_Lang;
@synthesize rstat_LastModified;
@synthesize rstat_Image;
@synthesize rstat_PhysicalCheck;
@synthesize rstat_FindStatus;

- (id)init {
    self = [super init];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

@end
