//
//  SuperRoomModelV2.h
//  mHouseKeeping
//
//  Created by TMS TRIBE on 2/27/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RoomAssignmentModelV2.h"
#import "HotelModelV2.h"
#import "RoomModelV2.h"
//#import "LocationModel.h"
#import "RoomStatusModelV2.h"
#import "UserModelV2.h"

@interface SuperRoomModelV2 : NSObject{
    //NSMutableArray *roomAssignmentList;
    //SupRoomAssignmentAdapter *adapter;
    // UserModel *userRoomAssignment;
    //RoomStatusModel *roomStatusRoomAssignment;
    RoomAssignmentModelV2 *roomAssignmentModel;
    //LocationModel *locationModel;
    RoomStatusModelV2 *roomStatusModel;
    HotelModelV2 *hotelModel;
    RoomModelV2 *roomModelForGetBuildingName;

}

//@property (nonatomic,retain) NSMutableArray *roomAssignmentList;
//@property (nonatomic,retain) SupRoomAssignmentAdapter *adapter;
@property (nonatomic,retain) UserModelV2 *userRoomAssignment;
//@property (nonatomic,retain) RoomStatusModel *roomStatusRoomAssignment;
@property (nonatomic,retain) RoomAssignmentModelV2 *roomAssignmentModel;
//@property (nonatomic ,retain) LocationModel *locationModel;
@property (nonatomic, retain) RoomStatusModelV2 *roomStatusModel;
@property (nonatomic,retain)  HotelModelV2 *hotelModel;
@property (nonatomic ,retain)  RoomModelV2 *roomModelForGetBuildingName;

@end
