//
//  InspectionStatusModel.h
//
//  Created by ThuongNM.
//  Copyright 2012 __TMS__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface InspectionStatusModel : NSObject {

	NSInteger istatId;
	NSString *istatName;
	NSData *istatImage;
	NSString *istatLang;

}

@property (nonatomic, assign) NSInteger istatId;
@property (nonatomic, strong) NSString *istatName;
@property (nonatomic, strong) NSData *istatImage;
@property (nonatomic, strong) NSString *istatLang;

@end