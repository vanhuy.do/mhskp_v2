//
//  RoomBlocking.h
//  mHouseKeeping


#import <Foundation/Foundation.h>
@interface RoomBlocking : NSObject{
    
}

@property (nonatomic, assign) NSInteger roomblocking_is_blocked;
@property (nonatomic, assign) NSInteger roomblocking_user_id;
@property (nonatomic, strong) NSString *roomblocking_room_number, *roomblocking_remark_physical_check, *roomblocking_reason, *roomblocking_oosdurations;

@end
