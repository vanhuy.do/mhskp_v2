//
//  RoomBlocking.m
//  mHouseKeeping

#import "RoomBlocking.h"

@implementation RoomBlocking

@synthesize roomblocking_is_blocked, roomblocking_reason, roomblocking_remark_physical_check, roomblocking_room_number, roomblocking_user_id;

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

@end
