//
//  RoomRecordModelV2.h
//  mHouseKeeping
//
//  Created by TMS TRIBE on 2/29/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RoomRecordModelV2.h"

//Use same data base structure of Room Record Table
@interface RoomRecordHistory : RoomRecordModelV2{
    
}

@property (nonatomic, assign) int rrec_room_status_id;
@property (nonatomic, assign) int rrec_cleaning_status_id;

@end
