//
//  RoomType.h
//  mHouseKeeping
//
//  Created by Gambogo on 5/18/15.
//
//

#import <Foundation/Foundation.h>

@interface RoomTypeModel : NSObject

@property (nonatomic, assign) NSInteger amsId;
@property (nonatomic, strong) NSString *amsName;
@property (nonatomic, strong) NSString *amsNameLang;
@property (nonatomic, strong) NSString *amsLastModified;
@property (nonatomic, strong) NSData *amsImage;

@end
