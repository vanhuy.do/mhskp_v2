//
//  SettingModelV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ehkDefinesV2.h"

#define DEFAULT_SETTING @"DEFAULT_SETTING"

@interface SettingModelV2 : NSObject{
    NSString *settingsLang;
    NSString *settingsLastSync;
    NSInteger settingsNotificationType;
    NSInteger row_Id;
    NSInteger settingsSynch;
    NSInteger settingsPhoto;
    BOOL settingEnableLog;
    BOOL settingEnableRoomValidate;
    NSInteger settingsLogAmount;
    NSString *settingsWSURL;
    NSInteger settingsBackgroundMode; // CFG [20160927/CRF-00001432]
    NSString *settingsHeartbeatServer; // CFG [20160927/CRF-00001432]
    NSString *settingsHeartbeatPort; // CFG [20160927/CRF-00001432]
}
@property (nonatomic, strong)     NSString *settingsLang;
@property (nonatomic, strong)     NSString *settingsLastSync;
@property (nonatomic ,readwrite) NSInteger settingsNotificationType;
@property (nonatomic,readwrite) NSInteger row_Id;
@property (nonatomic , readwrite) NSInteger settingsSynch;
@property (nonatomic , readwrite) NSInteger settingsPhoto;
@property (nonatomic , readwrite) BOOL settingEnableLog;
@property (nonatomic , readwrite) BOOL settingEnableRoomValidate;
@property (nonatomic, readwrite) NSInteger settingsLogAmount;
@property (nonatomic, strong) NSString *settingsWSURL;
@property (nonatomic, readwrite) NSInteger settingsBackgroundMode; // CFG [20160927/CRF-00001432]
@property (nonatomic, strong) NSString *settingsHeartbeatServer; // CFG [20160927/CRF-00001432]
@property (nonatomic, strong) NSString *settingsHeartbeatPort; // CFG [20160927/CRF-00001432]

+(void) setDefaultSetting;
+(BOOL) isAlreadySetDefaultSetting;

@end
