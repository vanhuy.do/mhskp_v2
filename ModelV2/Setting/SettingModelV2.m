//
//  SettingModelV2.m
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SettingModelV2.h"
#import "SettingAdapterV2.h"

@implementation SettingModelV2

@synthesize settingsLang, settingsPhoto, settingsSynch, settingsLastSync, settingsNotificationType, row_Id, settingEnableLog, settingEnableRoomValidate, settingsBackgroundMode, settingsHeartbeatServer, settingsHeartbeatPort;
@synthesize settingsLogAmount, settingsWSURL;

+(BOOL) isAlreadySetDefaultSetting
{
    BOOL isAlreadyDefault = [[NSUserDefaults standardUserDefaults] boolForKey:DEFAULT_SETTING];
    return isAlreadyDefault;
}

+(void) setDefaultSetting
{
    //Check if already set Default Setting
    if(![SettingModelV2 isAlreadySetDefaultSetting]) {
        
        //Set Enable WS 1 = on or 0 = OFF
        [[NSUserDefaults standardUserDefaults] setInteger:DEFAULT_ENABLE_WS_LOG forKey:ENABLE_LOGWS];
        
        //Retry Times
        [[NSUserDefaults standardUserDefaults] setInteger:DEFAULT_RETRY_TIMES  forKey:RETRY_TIMES];
        
        SettingModelV2 *smodel = [[SettingModelV2 alloc] init];
        SettingAdapterV2 *sadapter = [[SettingAdapterV2 alloc] init];
        [sadapter openDatabase];
        [sadapter loadSettingModel:smodel];
        [sadapter close];
        
        //update new language id
        SettingModelV2 *smodel2 = [[SettingModelV2 alloc] init];
        smodel2.settingsLang = [NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE];
        smodel2.settingsLastSync = smodel.settingsLastSync;
        smodel2.settingsNotificationType = smodel.settingsNotificationType;
        smodel2.settingsPhoto = smodel.settingsPhoto;
        // 1 manual sync, 2 autosync
        smodel2.settingsSynch = DEFAULT_SYNC_SETTING;//smodel.settingsSynch;
        //Local Log WS
        smodel2.settingEnableLog = DEFAULT_LOCAL_LOG;// smodel.settingEnableLog;
        smodel2.settingsLogAmount = smodel.settingsLogAmount;
        //Room Number Validation
        smodel2.settingEnableRoomValidate = DEFAULT_ROOM_VALIDATION; //smodel.settingEnableRoomValidate;
        // CFG [20160927/CRF-00001432] - Load background mode values.
        smodel2.settingsBackgroundMode = smodel.settingsBackgroundMode;
        smodel2.settingsHeartbeatServer = smodel.settingsHeartbeatServer;
        smodel2.settingsHeartbeatPort = smodel.settingsHeartbeatPort;
        // CFG [20160927/CRF-00001432] - End.
        smodel2.row_Id = smodel.row_Id;
        
        NSString *wsURL = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_WEBSERVICE_URL];
        if (wsURL == nil) {
            smodel2.settingsWSURL = eHouseKeepingDefaultWebService;
            [[NSUserDefaults standardUserDefaults] setObject:eHouseKeepingDefaultWebService forKey:KEY_WEBSERVICE_URL];
        }
        else {
            smodel2.settingsWSURL = smodel.settingsWSURL;
        }
        
        [[LanguageManagerV2 sharedLanguageManager] setCurrentLanguage:smodel2.settingsLang];
        
        [sadapter openDatabase];
        [sadapter updateSettingModel:smodel2];
        [sadapter close];
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:DEFAULT_SETTING];
        [[UserManagerV2 sharedUserManager].currentUserAccessRight setIsValidateRoomValue:smodel2.settingEnableRoomValidate];
    }
}

@end
