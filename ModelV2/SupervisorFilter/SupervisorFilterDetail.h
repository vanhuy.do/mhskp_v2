//
//  SupervisorFilterDetail.h
//  mHouseKeeping
//
//  Created by Gambogo on 6/25/15.
//
//

#import <Foundation/Foundation.h>

@interface SupervisorFilterDetail : NSObject

@property (nonatomic, assign) int filter_user_id;
@property (nonatomic, assign) int filter_type;
@property (nonatomic, assign) int filter_id;
@property (nonatomic, strong) NSString *filter_name;
@property (nonatomic, strong) NSString *filter_lang;
@property (nonatomic, assign) int filter_hotel_id;
@property (nonatomic, strong) NSString *filter_room_number;

@end
