//
//  AssignRoomModelV2.h
//  mHouseKeeping
//
//  Created by TMS TRIBE on 3/16/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AssignRoomModelV2 : NSObject{
    NSInteger assignRoomID;
    NSInteger assignRoom_assign_id;
    NSInteger assignRoom_HouseKeeper_Id;
    NSString *assignRoom_Assign_Date;
    NSString *assignRoom_Remark;
    NSInteger assignRoom_post_status;
    NSInteger assignRoom_supervisor_id;
    
}

@property (nonatomic) NSInteger assignRoomID;
@property (nonatomic) NSInteger assignRoom_assign_id;
@property (nonatomic) NSInteger assignRoom_HouseKeeper_Id;
@property (nonatomic,strong) NSString *assignRoom_Assign_Date;
@property (nonatomic,strong) NSString *assignRoom_Remark;
@property (nonatomic) NSInteger assignRoom_post_status;
@property (nonatomic) NSInteger assignRoom_supervisor_id;
@end
