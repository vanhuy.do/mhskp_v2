//
//  AssignRoomModelV2.m
//  mHouseKeeping
//
//  Created by TMS TRIBE on 3/16/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "AssignRoomModelV2.h"

@implementation AssignRoomModelV2

@synthesize assignRoomID;
@synthesize assignRoom_assign_id;
@synthesize assignRoom_HouseKeeper_Id;
@synthesize assignRoom_Assign_Date;
@synthesize assignRoom_Remark;
@synthesize assignRoom_post_status;
@synthesize assignRoom_supervisor_id;
@end
