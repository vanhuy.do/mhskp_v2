//
//  FloorModelV2.h
//  mHouseKeeping
//
//  Created by TMS TRIBE on 3/12/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FloorModelV2 : NSObject{
    NSInteger floor_id;
    NSString *floor_name;
    NSString *floor_name_lang;
    NSInteger floor_building_id;
    NSString *floor_last_modified;
    BOOL isSelected;
}

@property (nonatomic) NSInteger floor_id;
@property (nonatomic,strong) NSString *floor_name;
@property (nonatomic,strong) NSString *floor_name_lang;
@property (nonatomic) NSInteger floor_building_id;
@property (nonatomic,strong) NSString *floor_last_modified;
@property BOOL isSelected;
@end
