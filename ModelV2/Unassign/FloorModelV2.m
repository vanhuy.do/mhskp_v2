//
//  FloorModelV2.m
//  mHouseKeeping
//
//  Created by TMS TRIBE on 3/12/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "FloorModelV2.h"

@implementation FloorModelV2

@synthesize floor_id;
@synthesize floor_name;
@synthesize floor_name_lang;
@synthesize floor_building_id;
@synthesize floor_last_modified;


- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}
@end
