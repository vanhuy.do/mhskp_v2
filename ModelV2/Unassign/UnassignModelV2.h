//
//  UnassignModelV2.h
//  mHouseKeeping
//
//  Created by TMS TRIBE on 5/4/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UnassignModelV2 : NSObject{
    NSInteger  unassignroom_id;
    NSInteger  unassignroom_floor_id;
    NSInteger  unassignroom_zone_id;
    NSInteger  unassignroom_hotel_id;
    NSInteger  unassignroom_room_status_id;
    NSInteger  unassignroom_cleaning_status_id;
    NSString   *unassignroom_guest_first_name;
    NSString   *unassignroom_guest_last_name;
    NSInteger  unassignroom_room_kind;
    NSString *unassignroom_room_id;
    NSString  *unassignroom_last_modified;
    
}

@property (nonatomic) NSInteger  unassignroom_id;
@property (nonatomic) NSInteger  unassignroom_floor_id;
@property (nonatomic) NSInteger  unassignroom_zone_id;
@property (nonatomic) NSInteger  unassignroom_hotel_id;
@property (nonatomic) NSInteger  unassignroom_room_status_id;
@property (nonatomic) NSInteger  unassignroom_cleaning_status_id;
@property (nonatomic,strong) NSString   *unassignroom_guest_first_name;
@property (nonatomic,strong) NSString   *unassignroom_guest_last_name;
@property (nonatomic) NSInteger  unassignroom_room_kind;
@property (nonatomic, strong) NSString *unassignroom_room_id;
@property (nonatomic,strong) NSString  *unassignroom_last_modified;


@end
