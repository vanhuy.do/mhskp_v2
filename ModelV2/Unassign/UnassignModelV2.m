//
//  UnassignModelV2.m
//  mHouseKeeping
//
//  Created by TMS TRIBE on 5/4/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "UnassignModelV2.h"

@implementation UnassignModelV2
@synthesize  unassignroom_id;
@synthesize  unassignroom_floor_id;
@synthesize  unassignroom_zone_id;
@synthesize  unassignroom_hotel_id;
@synthesize  unassignroom_room_status_id;
@synthesize  unassignroom_cleaning_status_id;
@synthesize  unassignroom_guest_first_name;
@synthesize  unassignroom_guest_last_name;
@synthesize  unassignroom_room_kind;
@synthesize  unassignroom_room_id;
@synthesize  unassignroom_last_modified;


- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

@end
