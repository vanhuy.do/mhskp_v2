//
//  UnassignRoomModelV2.h
//  mHouseKeeping
//
//  Created by Giang Le on 2/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UnassignRoomModelV2 : NSObject{
    NSInteger unAssignRoomId;
    NSString *unAssignRoomNo;
    NSString *unAssignRoomStatus;
    NSString *unAssignVipStatus;
    NSString *unAssignGuestname;
}

@property (nonatomic) NSInteger unAssignRoomId;
@property (nonatomic, strong) NSString *unAssignRoomNo;
@property (nonatomic , strong) NSString *unAssignRoomStatus;
@property (nonatomic , strong) NSString *unAssignVipStatus;
@property (nonatomic , strong) NSString *unAssignGuestname;
@end
