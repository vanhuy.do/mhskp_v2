
//
//  UnassignZoneModel.h
//  mHouseKeeping
//
//  Created by Giang Le on 2/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UnassignZoneModelV2 : NSObject{
    
}

@property (nonatomic) NSInteger unAssignZoneId;
@property (strong,nonatomic) NSString *unAssignZoneName;
@property (strong,nonatomic) NSString *unAssignZoneName_Lang;
@property (nonatomic) NSInteger unAssignZone_Floor_Id;
@property (strong,nonatomic) NSString *unAssignZoneLast_Modified;
@end
