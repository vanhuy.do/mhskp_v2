//
//  ZoneModelV2.h
//  mHouseKeeping
//
//  Created by TMS TRIBE on 3/12/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZoneModelV2 : NSObject{
    NSInteger zone_id; 
    NSString *zone_name;
    NSString *zone_name_lang;
    NSInteger zone_floor_id; 
    NSString *zone_last_modified;

}

@property (nonatomic) NSInteger zone_id; 
@property (nonatomic,strong) NSString *zone_name;
@property (nonatomic,strong) NSString *zone_name_lang;
@property (nonatomic) NSInteger zone_floor_id; 
@property (nonatomic,strong) NSString *zone_last_modified;
@end
