//
//  UserModelV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@class UserAdapterV2;

@interface UserConfigModel : NSObject{
    
    int rowId;
    int userId;
    NSString *key;
    NSString *value;
    NSString *lastModified;
}
@property (nonatomic,readwrite) int rowId;
@property (nonatomic,readwrite) int userId;
@property (nonatomic,copy) NSString *key;
@property (nonatomic,copy) NSString *value;
@property (nonatomic,copy) NSString *lastModified;

@end
