//
//  UserModelV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@class UserAdapterV2;

@interface UserLogModelV2 : NSObject{
    
    int userId;
    NSString *userName;
    NSString *userLastLoginDate;
    NSString *userLastLogoutDate;
}

@property (nonatomic,readwrite) int userId;
@property (nonatomic,copy) NSString *userName;
@property (nonatomic,copy) NSString *userLastLoginDate;
@property (nonatomic,copy) NSString *userLastLogoutDate;

@end
