//
//  UserModelV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@class UserAdapterV2;

/**
 M: Management
 A: Agent
 R: Runner
 S: Supervisor
 */
typedef enum
{
    M, A, R, S
} userRoles;

@interface UserModelV2 : NSObject{
    
    int userId;
    NSString *userName;
    NSString *userPassword;
    NSString *userRole;
    NSString *userFullName;
    NSString *userFullNameLang;
    NSString *userLang;
    NSString *userTitle;
    int userHotelsId;
    NSString *userDepartment;
    int userDepartmentId;
    int userSupervisorId;	
    NSString *userLastModified;
    NSInteger userAllowBlockRoom;
    NSInteger userAllowReorderRoom;
}

@property (nonatomic,readwrite) int userId;
@property (nonatomic,copy) NSString *userName;
@property (nonatomic,copy) NSString *userPassword;
@property (nonatomic,copy) NSString *userRole;
@property (nonatomic,copy) NSString *userFullName;
@property (nonatomic,copy) NSString *userFullNameLang;
@property (nonatomic,copy) NSString *userLang;
@property (nonatomic,copy) NSString *userTitle;
@property (nonatomic,readwrite) int userHotelsId;
@property (nonatomic,copy) NSString *userDepartment;
@property (nonatomic,readwrite) int userDepartmentId;
@property (nonatomic,readwrite) int userSupervisorId;	
@property (nonatomic,copy) NSString *userLastModified;
@property (nonatomic, readwrite) NSInteger userAllowBlockRoom;
@property (nonatomic, readwrite) NSInteger userAllowReorderRoom;



@end
