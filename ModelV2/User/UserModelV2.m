//
//  UserModelV2.m
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UserModelV2.h"

@implementation UserModelV2
@synthesize userId, userLang, userName, userRole, userTitle, userFullName,userFullNameLang, userHotelsId, userPassword, userDepartment, userDepartmentId, userLastModified, userSupervisorId;
@synthesize userAllowBlockRoom;
@synthesize userAllowReorderRoom;

@end
