//
//  AFNetworkingUtility.h
//  PTTLibrary
//
//  Created by Admin on 8/14/15.
//  Copyright (c) 2015 FCS Computer Systems. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"

@interface AFNetworkingUtility : NSObject{
    AFHTTPRequestOperationManager *manager;
    NSString *secretKey;
}
+ (AFNetworkingUtility *)instance;
- (void)setHeaderWithValue:(NSString*)value withKey:(NSString*)key;
- (void)requestData:(NSString*)url withParameters:(NSDictionary*)parameters success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
            failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

@end
