//
//  AFNetworkingUtility.m
//  PTTLibrary
//
//  Created by Admin on 8/14/15.
//  Copyright (c) 2015 FCS Computer Systems. All rights reserved.
//

#import "AFNetworkingUtility.h"
#import "macro.h"
#import "GlobalObject.h"
#import "Utility.h"
#import "AppKey.h"

@implementation AFNetworkingUtility

#pragma mark - Singleton

+ (AFNetworkingUtility *)instance
{
    __strong static AFNetworkingUtility *_sharedLocalSystem = nil;
    
    static dispatch_once_t pred = 0;
    dispatch_once(&pred, ^{
        _sharedLocalSystem = [[self alloc] initPrivate];
    });
    return _sharedLocalSystem;
}

- (id) initPrivate {
    self = [super init];
    if(self)
    {
        // Init your data here
        manager = [AFHTTPRequestOperationManager manager];
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        GlobalObject *globalObject = [GlobalObject instance];
        NSString *strKey = [NSString stringWithFormat:@"des=^qry=^usr=^pwd=^dat=^dur=^platform=1^applicationId=1^uid=%@",globalObject.eConnectUserId];
        secretKey = [Utility encryptUseDES:strKey];
        [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [manager.requestSerializer setValue:secretKey forHTTPHeaderField:KEY_SECRET];
        [manager.requestSerializer setValue:globalObject.eConnectUserId forHTTPHeaderField:KEY_USER_ID];
    }
    return self;
}

- (void)setHeaderWithValue:(NSString*)value withKey:(NSString*)key{
    [manager.requestSerializer setValue:value forHTTPHeaderField:key];
}

- (void)setManager:(AFHTTPRequestOperationManager*)man{
    manager = man;
}
- (void)requestData:(NSString*)url withParameters:(NSDictionary*)parameters success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
            failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    url = [NSString stringWithFormat:@"http://%@", url];
    DLog(@"url: %@ \n parameters:\n%@",url, parameters);
    DLog(@"%@",manager.requestSerializer.HTTPRequestHeaders);
    
    [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        DLog(@"JSON: %@", responseObject);
        success(operation, responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DLog(@"Error: %@", error.userInfo);
        failure(operation, error);
    }];
}
@end
