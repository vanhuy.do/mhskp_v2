//
//  AppKey.h
//  PTTLibrary
//
//  Created by Nguyen Minh Duc on 8/11/15.
//  Copyright (c) 2015 FCS Computer Systems. All rights reserved.
//

#ifndef PTTLibrary_AppKey_h
#define PTTLibrary_AppKey_h

// Home

#define NAME                        @"Name"
#define IMAGE_PATH                  @"imagePath"
#define INCOMMING_CALL_MESSAGE      @"incomming_call_message"

#define ROLE_MOBILE_PERMISSION_EDIT @"ROLE_MOBILE_PERMISSION_EDIT"
#define ROLE_MOBILE_PERMISSION_PLAY @"ROLE_MOBILE_PERMISSION_PLAY"
#define ROLE_MOBILE_PERMISSION_VIEW @"ROLE_MOBILE_PERMISSION_VIEW"
#define ROLE_NO_ROLES               @"ROLE_NO_ROLES"

#define ROLE_ADMIN                  2
#define ROLE_USER                   1
#define ROLE_NO_PERMISSION          0
//setting keys

#define SETTING_RECORD_FILE_STORAGE         @"record_file_storage"
#define KEY_INTERNAL                        @"internal"
#define KEY_EXTERNAL                        @"external"

#define KEY_INTERNAL_WIFI                   @"internal_wifi"
#define KEY_STUN                            @"stun"

#define SETTING_CONFIG_PTT_SIP              @"config_ptt_sip"
#define SETTING_CONFIG_PTT_SIP_PORT         @"config_ptt_sip_port"
#define SETTING_CONFIG_PTT_WEB              @"config_ptt_web"
#define SETTING_CONFIG_PTT_WEB_PORT         @"config_ptt_web_port"

#define USER_ID                             @"userId"
#define AUTH_TOKEN                          @"X-Auth-Token"
#define SECRET                              @"secret"
#define PAGE                                @"page"

#endif
