//
//  JsonKey.h
//  PTTLibrary
//
//  Created by Nguyen Minh Duc on 8/19/15.
//  Copyright (c) 2015 FCS Computer Systems. All rights reserved.
//

#ifndef PTTLibrary_JsonKey_h
#define PTTLibrary_JsonKey_h

// Home
#define IS_DEFAULT @"isDefault"

#define NOTIFICATION_TYPE  @"notificationType"
#define KEY_DATA           @"data"
#define KEY_CALLER_ID      @"callerId"
#define KEY_USER_ID        @"userId"
#define KEY_NICK_NAME      @"nickName"
#define KEY_CHANNEL        @"channel"
#define KEY_STATUS         @"status"
#define KEY_CALL_GROUP     @"callGroup"
#define KEY_TO             @"to"
#define KEY_CALLER         @"caller"
#define KEY_GROUP_TYPE     @"groupType"
#define KEY_DEVICE_ID      @"deviceID"
#define KEY_SECRET         @"secret"
#define KEY_GROUP_TYPE     @"groupType"
#define KEY_TOKEN          @"X-Auth-Token"
#define KEY_ACCESS_TOKEN   @"access_token"
#define KEY_CURRENT_USER   @"currentUser"
#define KEY_USER_NAME      @"userName"
#define KEY_ROLE           @"role"
#define KEY_GROUPS         @"groups"
#define KEY_GROUP_ID       @"groupId"
#define KEY_SEQUENCE       @"Sequence"
#define KEY_PAGE           @"page"
#define KEY_FILTER         @"filter"
#define KEY_TOTAL_COUNT    @"totalCount"
#define KEY_USER_ID_A      @"userIdA"
#define KEY_USER_ID_B      @"userIdB"
#define KEY_ID             @"id"
#define KEY_GROUP_NAME     @"groupName"
#define KEY_LANG           @"lang"
#define KEY_GROUP_TYPE     @"groupType"
#define KEY_DEPARTMENT_ID  @"departmentId"
#define KEY_APPLICATION_ID @"applicationId"
#define KEY_ASSIGN         @"assign"
#define KEY_JOINED         @"joined"
#define KEY_START_HISTORY  @"start"
#define KEY_END_HISTORY    @"end"
#endif
