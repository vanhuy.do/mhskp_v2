//
//  ServerPath.h
//  PTTLibrary
//
//  Created by Admin on 8/11/15.
//  Copyright (c) 2015 FCS Computer Systems. All rights reserved.
//

#ifndef PTTLibrary_ServerPath_h
#define PTTLibrary_ServerPath_h

#pragma mark - SERVER INFO
#define DEFAULT_SER_IP        @"210.184.6.231"
//#define DEFAULT_SER_IP        @"14.161.36.97"
#define DEFAULT_SER_SIP_PORT  @"5060"
#define DEFAULT_SER_PTT_PORT  @"8080"
#define DEFAULT_SER_MQTT_PORT @"1883"
//#define SER_ADDRESS           @"/pttportal/api/"
#define SER_ADDRESS           @"/m2talkportal/api/"
//#define SER_ADDRESS           @"/m2talk/api/"

#define SER_IP          @"SER_IP"
#define SET_SER_IP(v)   UsrDfltSetObjKeySync(v,SER_IP)
#define GET_SER_IP      CheckObjNil(UsrDfltStr4Key(SER_IP), DEFAULT_SER_IP)

#define MQTT_PORT          @"MQTT_PORT"
#define SET_MQTT_PORT(v)   UsrDfltSetObjKeySync(v,MQTT_PORT)
#define GET_MQTT_PORT      CheckObjNil(UsrDfltStr4Key(MQTT_PORT), DEFAULT_SER_MQTT_PORT)

#define SER_PTT_PORT        @"SER_PTT_PORT"
#define SET_SER_PTT_PORT(v) UsrDfltSetObjKeySync(v,SER_PTT_PORT)
#define GET_SER_PTT_PORT    CheckObjNil(UsrDfltStr4Key(SER_PTT_PORT), DEFAULT_SER_PTT_PORT)

#define SER_SIP_PORT        @"SER_SIP_PORT"
#define SET_SER_SIP_PORT(v) UsrDfltSetObjKeySync(v,SER_SIP_PORT)
#define GET_SER_SIP_PORT    CheckObjNil(UsrDfltStr4Key(SER_SIP_PORT), DEFAULT_SER_SIP_PORT)

#define SETTING_CONFIG_SERVER_CONNECTION        @"config_server_connection"
#define SET_SETTING_CONFIG_SERVER_CONNECTION(v)   UsrDfltSetBool4KeySync(v,SETTING_CONFIG_SERVER_CONNECTION)
#define GET_SETTING_CONFIG_SERVER_CONNECTION      (UsrDfltGetBool4Key(SETTING_CONFIG_SERVER_CONNECTION) == nil) ? NO : UsrDfltGetBool4Key(SETTING_CONFIG_SERVER_CONNECTION)

#define SETTING_CONFIG_STUN_ADDRESS         @"config_stun_address"
#define SET_SETTING_CONFIG_STUN_ADDRESS(v)   UsrDfltSetObjKeySync(v,SETTING_CONFIG_STUN_ADDRESS)
#define GET_SETTING_CONFIG_STUN_ADDRESS      CheckObjNil(UsrDfltStr4Key(SETTING_CONFIG_STUN_ADDRESS), @"stun.l.google.com:19302")

#pragma mark - API INFO
#define GetAPIAddress(o)                [NSString stringWithFormat:@"%@:%@%@%@",GET_SER_IP, GET_SER_PTT_PORT,SER_ADDRESS, o]

#define API_GET_LIST_CONTACT_BY_CHANNEL GetAPIAddress(@"group/userInGroup")
#define API_GET_ASSIGN_USER             GetAPIAddress(@"group/joinOrUnjoin")
#define API_GET_LIST_CONTACT            GetAPIAddress(@"user/search")
#define API_GET_LIST_CHANNEL            GetAPIAddress(@"group/joinedList")
#define API_GET_LIST_USER_IN_CHANNEL    GetAPIAddress(@"group/usersInGroupChat")
#define API_DELETE_CHANNEL              GetAPIAddress(@"group/delete")
#define API_IS_JOINED_CHANNEL           GetAPIAddress(@"group/isJoinedGroup")
#define API_CHECK_USER                  GetAPIAddress(@"user/currentUser")
#define API_LOGOUT                      GetAPIAddress(@"user/logout")
#define API_GET_CREATE_CHANNEL          GetAPIAddress(@"group/save")
#define API_GET_CREATE_CHANNEL_FAKE     GetAPIAddress(@"group/createFakeGroup")
#define API_GET_LIST_HISTORY            GetAPIAddress(@"callMedia/history")
#define API_GET_LIST_HISTORY_DETAIL     GetAPIAddress(@"callMedia/loadMedias")
#define API_GET_LIST_HISTORY_DETAIL_A_B GetAPIAddress(@"callMedia/mediasOfA_B")
#define API_UPDATE_STATUS               GetAPIAddress(@"user/updateStatus")
#define API_GET_UN_ASSIGN_USER          GetAPIAddress(@"group/unjoin")
#define API_GET_LIST_STATUS_USER        GetAPIAddress(@"user/statusOfUsers")
#define API_DELETE_CHANNEL              GetAPIAddress(@"group/delete")
#define API_CREATE_CHANNEL              GetAPIAddress(@"group/save")


#endif
