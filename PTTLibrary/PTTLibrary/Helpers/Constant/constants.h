/*============================================================================

 FILE:    constants.h

 ============================================================================*/
#ifndef _CONSTANTS_H_H_
#define _CONSTANTS_H_H_

/*============================================================================
 FLAG PRECOMPILE
 ============================================================================*/

/*============================================================================
 TRACE MACRO
 ============================================================================*/



#endif  /* _CONSTANTS_H_H_ */
#pragma mark - GENERAL

#define HTTP_STATUS_SUCCESS 200
#define HTTP_STATUS_FAIL_401 401
#define HTTP_STATUS_FAIL_403 403

#define DEFAULT_GROUP_VALUE 1

#define PING_TIMER 180

#pragma mark - DEFINE CONSTANT

#define APP_NAME @"PTT_LIBRARY"

#define IS_ECHO_MODE YES

#define DISCONNECT_SUCCESS @"DISCONNECT_SUCCESS"
#define MAKE_CALL_SUCCESS @"MAKE_CALL_SUCCESS"
#define MAKE_CALL_INFO @"MAKE_CALL_INFO"
#define LINE_BUSY @"LINE_BUSY"
#define NO_TIME @"--:--"
#define RETRY_AFTER_FAILED_TIMER 1
#define CAN_SPEAK_TIMER 3
#define SOUND_TOUCH    @"beep_touch.wav"
#define SOUND_OK      @"ok.mp3"
#define SOUND_WAIT    @"wait.mp3"
#define SOUND_FAIR_NW @"fairnetwork.mp3"
#define SOUND_BAD_NW  @"badnetwork.mp3"
#define SOUND_NO      @"beep_no.wav"
#define SOUND_FAIL    @"beep_fail.wav"
/**
 * MOVE FROM CALL SCREEN FRAGMENT
 */

