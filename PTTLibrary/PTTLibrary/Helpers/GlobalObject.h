//
//  GlobalObject.h
//  PTTLibrary
//
//  Created by Admin on 8/13/15.
//  Copyright (c) 2015 FCS Computer Systems. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "UserInfoModel.h"
#import "DepartmentModel.h"
#import "GeneralDataModel.h"
typedef enum
{
    STATE_NONE,
    STATE_REQUESTING,
    STATE_REQUESTED_OK ,
    STATE_REQUESTED_FAILED,
    STATE_DISCONNECT_SIP,
    STATE_BAD_NETWORK,
    STATE_FAIR_NETWORK,
    STATE_GOOD_NETWORK,
    STATE_UNKNOWN_NETWORK
} CALL_STATE;

typedef enum
{
    THEME_MCONNECT,
    THEME_MHSKP
} THEME;

@interface GlobalObject : NSObject
+ (GlobalObject *)instance;


@property(nonatomic,strong) GeneralDataModel* generalData;

@property(nonatomic,strong) GroupModel* defaultGroup;

@property (nonatomic, strong) NSString *eConnectUserId;
@property (nonatomic, strong) NSString *currentLanguage;
@property (nonatomic, strong) NSString *groupDefaultId;
@property int groupID, role;
@property (nonatomic, strong) NSString *groupName;
@property (nonatomic, strong) NSString *uri;
@property (nonatomic, strong) NSString *uniqueDeviceId;
@property (nonatomic, strong) NSString *callToUserID;
@property (nonatomic, strong) GroupBaseModel *groupCallToUser;

@property(nonatomic,strong) NSTimer* pingTimer;
@property (nonatomic, strong) NSString *currentCallToUserID, *nextCallToUserID;
@property BOOL isEchoTest;
@property BOOL firstSuccessClick;
@property BOOL isPlayHistory;
@property THEME currentTheme;
- (void)clearData;
@end
