//
//  GlobalObject.m
//  PTTLibrary
//
//  Created by Admin on 8/13/15.
//  Copyright (c) 2015 FCS Computer Systems. All rights reserved.
//

#import "GlobalObject.h"

@implementation GlobalObject

#pragma mark - Singleton

+ (GlobalObject *)instance
{
    __strong static GlobalObject *_sharedLocalSystem = nil;
    
    static dispatch_once_t pred = 0;
    dispatch_once(&pred, ^{
        _sharedLocalSystem = [[self alloc] init];
    });
    return _sharedLocalSystem;
}
- (void)clearData{
    self.generalData = nil;
    self.defaultGroup = nil;
    self.eConnectUserId = nil;
    self.currentLanguage = nil;
    
    self.groupDefaultId = nil;
    self.groupID = -1;
    self.role = -1;
    self.groupName = nil;
    self.uri = nil;
    self.uniqueDeviceId = nil;
    self.callToUserID = nil;
    self.groupCallToUser = nil;
    
    if ([self.pingTimer isValid]) {
        [self.pingTimer invalidate];
    }
    self.pingTimer = nil;
    self.isEchoTest = NO;
    
    self.firstSuccessClick = NO;
    self.isPlayHistory = NO;
    
}
@end
