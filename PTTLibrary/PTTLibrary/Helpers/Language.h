//
//  Language.h
//  PTTLibrary
//
//  Created by phu the cong on 8/11/15.
//  Copyright (c) 2015 FCS Computer Systems. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Language : NSObject
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *languageCode;

- (id)initWithLanguageCode:(NSString *)languageCode name:(NSString *)name;
@end
