//
//  Language.m
//  PTTLibrary
//
//  Created by phu the cong on 8/11/15.
//  Copyright (c) 2015 FCS Computer Systems. All rights reserved.
//

#import "Language.h"

@implementation Language

- (id)initWithLanguageCode:(NSString *)languageCode name:(NSString *)name
{
    if (self = [super init]) {
        self.languageCode = languageCode;
        self.name = name;
    }
    return self;
}

@end
