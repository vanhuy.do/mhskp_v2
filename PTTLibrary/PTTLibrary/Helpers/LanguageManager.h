//
//  LanguageManager.h
//  PTTLibrary
//
//  Created by phu the cong on 8/11/15.
//  Copyright (c) 2015 FCS Computer Systems. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Language;

@interface PTTLanguageManager : NSObject

@property (nonatomic, copy) NSArray *availableLanguages;

+ (PTTLanguageManager *)sharedLanguageManager;
- (void)setLanguage:(Language *)language;
- (NSString *)getTranslationForKey:(NSString *)key;
@end
