//
//  LanguageManager.m
//  PTTLibrary
//
//  Created by phu the cong on 8/11/15.
//  Copyright (c) 2015 FCS Computer Systems. All rights reserved.
//

#import "LanguageManager.h"
#import "Language.h"
#import "Utility.h"
#import "GlobalObject.h"

#define DEFAULTS_KEY_LANGUAGE_CODE @"LanguageCode"

@interface PTTLanguageManager()
{
    NSBundle * languageBundle;
}

@end

@implementation PTTLanguageManager
#pragma mark - Object Lifecycle
+ (PTTLanguageManager *)sharedLanguageManager {
    
    // Create a singleton.
    static dispatch_once_t once;
    static PTTLanguageManager *languageManager;
    dispatch_once(&once, ^ {
        languageManager = [[PTTLanguageManager alloc] init];
    });
    return languageManager;
}

- (id)init {
    
    if (self = [super init]) {
        Language *english       = [[Language alloc] initWithLanguageCode:@"en" name:@"English"];
        Language *english1      = [[Language alloc] initWithLanguageCode:@"en" name:@"eng"];
        Language *french        = [[Language alloc] initWithLanguageCode:@"fr" name:@"French (française)"];
        Language *korean        = [[Language alloc] initWithLanguageCode:@"ko" name:@"Korean (한국어)"];
        Language *sChinese      = [[Language alloc] initWithLanguageCode:@"zh-Hans" name:@"Simplified Chinese (简体中文)"];
        Language *sChinese1     = [[Language alloc] initWithLanguageCode:@"zh-Hans" name:@"chs"];
        Language *tChinese      = [[Language alloc] initWithLanguageCode:@"zh-Hant" name:@"Traditional Chinese (繁体中文)"];
        Language *tChinese1     = [[Language alloc] initWithLanguageCode:@"zh-Hant" name:@"cht"];
        Language *japanese      = [[Language alloc] initWithLanguageCode:@"ja" name:@"Japanese (日本語)"];
        Language *japanese1     = [[Language alloc] initWithLanguageCode:@"ja" name:@"jpn"];
        Language *spanish       = [[Language alloc] initWithLanguageCode:@"es" name:@"Spanish (española)"];
        Language *thailand      = [[Language alloc] initWithLanguageCode:@"th" name:@"Thai (ภาษาไทย)"];
        Language *thailand1     = [[Language alloc] initWithLanguageCode:@"th" name:@"tha"];
        self.availableLanguages = @[english, english1, french, korean, sChinese, sChinese1, tChinese, tChinese1, japanese, japanese1, spanish, thailand, thailand1];
    }
    
    return self;
}

#pragma mark - Methods

- (void)setLanguage:(Language *)language {

    DLog(@"lange first: %@", language.name)
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name = %@",language.name];
    NSArray *filteredArr = [_availableLanguages filteredArrayUsingPredicate:predicate];
    language = [filteredArr firstObject];
    DLog(@"lang found: %@", language.name)
    
    // Get the relevant language bundle.
    NSString *bundlePath = [[Utility getPttLibraryBundle] pathForResource:language.languageCode ofType:@"lproj"];
    languageBundle = [NSBundle bundleWithPath:bundlePath];
}



- (NSString *)getTranslationForKey:(NSString *)key {
    // Get the translated string using the language bundle.
    NSString *translatedString = [languageBundle localizedStringForKey:key value:@"" table:nil];
    
    if (translatedString.length < 1) {
        
        // There is no localizable strings file for the selected language.
        translatedString = NSLocalizedStringWithDefaultValue(key, nil, [NSBundle mainBundle], key, key);
    }
    
    return translatedString;
}

@end
