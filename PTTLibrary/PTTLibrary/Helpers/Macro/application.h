/*=============================================================================

 FILE:    application.h

 =============================================================================*/
#ifndef _APPLICATION_H_H_
#define _APPLICATION_H_H_

/*=============================================================================
 MACRO DEFINITION
 =============================================================================*/
#define MyAPP                     [UIApplication sharedApplication]

/* Orientation */
#define MyAPPOrientation          MyAPP.statusBarOrientation
#define IsScreenPortrait(d)       UIInterfaceOrientationIsPortrait(d)
#define IsScreenLandscape(d)      UIInterfaceOrientationIsLandscape(d)
#define IsMyAppPortrait()         IsScreenPortrait(MyAPPOrientation)
#define IsMyAppLandscape()        IsScreenPortrait(MyAPPOrientation)

#endif  /* _APPLICATION_H_H_ */
