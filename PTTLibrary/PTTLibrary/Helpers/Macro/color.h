/*=============================================================================

 FILE:    color.h

 =============================================================================*/
#ifndef _COLOR_H_H_
#define _COLOR_H_H_

/*=============================================================================
 MACRO DEFINITION
 =============================================================================*/
#define ColorRGBA(r,g,b,a)        [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]
#define ColorRGB(r,g,b)           ColorRGBA(r,g,b,1.0)
#define Color4Img(i)              [UIColor colorWithPatternImage:i]

#define ColorBlack                [UIColor blackColor]
#define ColorWhite                [UIColor whiteColor]
#define ColorBlue                 [UIColor blueColor]
#define ColorGreen                [UIColor greenColor]
#define ColorClear                [UIColor clearColor]
#define ColorDarkGray             [UIColor darkGrayColor]
#define ColorGray                 [UIColor grayColor]
#define ColorLightGray            [UIColor lightGrayColor]

#define ColorShadow               ColorRGBA(0.0,0.0,0.0,0.5)

#define ColorLine                 ColorGray

#endif  /* _COLOR_H_H_ */
