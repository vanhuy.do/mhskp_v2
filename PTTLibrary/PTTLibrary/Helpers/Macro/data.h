/*=============================================================================

 FILE:    data.h

 =============================================================================*/
#ifndef _DATA_H_H_
#define _DATA_H_H_

/*=============================================================================
 MACRO DEFINITION
 =============================================================================*/
#define IsData(o)             IsKindOfClass(o,NSData)

#define DataNew()             [NSMutableData data]
#define Data4Data(o)          [NSMutableData dataWithData:d]
#define Data4File(p)          [NSData dataWithContentsOfFile:p]
#define Data4URL(p)           [NSData dataWithContentsOfURL:u]
#define Data4Path(p)          [NSData dataWithContentsOfURL:u]

#define DataAppend(o,a)       [o appendData:a]

#endif  /* _DATA_H_H_ */