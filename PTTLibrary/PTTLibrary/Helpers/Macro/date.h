/*=============================================================================

 FILE:    date.h

 =============================================================================*/
#ifndef _DATE_H_H_
#define _DATE_H_H_

/*=============================================================================
 MACRO DEFINITION
 =============================================================================*/
#define IsDate(n)             IsKindOfClass(n,NSDate)
#define DateNow               [NSDate date]
#define DateReference         Date4Int(0)

#define DateIntNow            [NSDate timeIntervalSinceReferenceDate]


#define Date2Int(o)           [o timeIntervalSinceReferenceDate]
#define Date4Int(o)           [NSDate dateWithTimeIntervalSinceReferenceDate:o]

#endif  /* _DATE_H_H_ */
