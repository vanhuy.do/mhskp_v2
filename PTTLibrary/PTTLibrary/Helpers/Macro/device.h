/*=============================================================================

 FILE:    device.h

 =============================================================================*/
#ifndef _DEVICE_H_H_
#define _DEVICE_H_H_

/*=============================================================================
 MACRO DEFINITION
 =============================================================================*/
#define Device                [UIDevice currentDevice]

#define CanMakePhoto()        [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]
#define IsiPadDevice          (([Device respondsToSelector:@selector(userInterfaceIdiom)]) && (UIUserInterfaceIdiomPad == [Device userInterfaceIdiom]))

#define CanMakeRecord()   [[AVAudioSession sharedInstance] inputIsAvailable]
#define CanMakePhoto()    [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]
#define isDev(_dev)       (0 == [Device.model rangeOfString:_dev].location)
#define isIPod()          isDev(@"iPod")
#define isIPhone()        isDev(@"iPhone")
#define isIPad()          isDev(@"iPad")

#endif  /* _DEVICE_H_H_ */