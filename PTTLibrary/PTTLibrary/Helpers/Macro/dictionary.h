/*=============================================================================

 FILE:    dictionary.h

 =============================================================================*/
#ifndef _DICTIONARY_H_H_
#define _DICTIONARY_H_H_

/*=============================================================================
 MACRO DEFINITION
 =============================================================================*/
#define IsDictionary(d)         IsKindOfClass(d,NSDictionary)

#define DictNew()               [NSMutableDictionary dictionary]
#define Dict4Dict(d)            [NSMutableDictionary dictionaryWithDictionary:d]
#define Dict4ObjsKeys(_o_...)   [NSMutableDictionary dictionaryWithObjectsAndKeys:_o_,nil]

#define DictSetObj4Key(d,o,k)   [d setObject:o forKey:k]
#define DictObj4Key(d,k)        [d objectForKey:k]

#define DictAllKeys(d)          [d allKeys]
#define DictHasKey(d,k)         ArrContain(DictAllKeys(d),k)

#endif  /* _DICTIONARY_H_H_ */
