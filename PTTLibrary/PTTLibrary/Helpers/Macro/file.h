/*=============================================================================

 FILE:    file.h

 =============================================================================*/
#ifndef _FILE_H_H_
#define _FILE_H_H_

/*=============================================================================
 MACRO DEFINITION
 =============================================================================*/
#define FileManager             [NSFileManager defaultManager]

#define DocPath                 ArrObj4Idx(NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES),0)
#define FilePathInDoc(f)        [DocPath stringByAppendingPathComponent:f]

#define FileExist(f0)           [FileManager fileExistsAtPath:f0]
#define FileRemove(f0)          [FileManager removeItemAtPath:f0 error:nil]
#define FileRename(f0,f1)       [FileManager moveItemAtPath:f0 toPath:f1 error:nil]
#define FileCopy(f0,f1)         [FileManager copyItemAtPath:f0 toPath:f1 error:nil]

#endif  /* _FILE_H_H_ */