/*=============================================================================
 
 FILE:    font.h
 
 =============================================================================*/
#ifndef _FONT_H_H_
#define _FONT_H_H_

/*=============================================================================
 MACRO DEFINITION
 =============================================================================*/
#define FontWithSize(d)       [UIFont systemFontOfSize:d]
#define FontBoldWithSize(d)   [UIFont boldSystemFontOfSize:d]

#define Font14                FontWithSize(14)
#define FontBold14            FontBoldWithSize(14)

#define Font12                FontWithSize(12)
#define FontBold12            FontBoldWithSize(12)


#endif  /* _FONT_H_H_ */
