/*=============================================================================

 FILE:    globals.h

 =============================================================================*/
#ifndef _GLOBALS_H_
#define _GLOBALS_H_

/*=============================================================================
 IMPORT
 =============================================================================*/
#import "constants.h"

/*=============================================================================
 DEFINITIONS
 =============================================================================*/

/*=============================================================================
 GLOBAL VARIABLES
 =============================================================================*/
#define MainBundle                [NSBundle mainBundle]
#define MainInfos                 [MainBundle infoDictionary]
#define AppVersion                DictObj4Key(MainInfos,@"CFBundleVersion")
#define AppName                   DictObj4Key(MainInfos,@"CFBundleDisplayName")

#define CanMakeCall()             [myApp canOpenURL:[NSURL URLWithString:@"tel:113"]]
#define CanMakeEmail()            [myApp canOpenURL:[NSURL URLWithString:@"mailto:foo@example.com"]] 


#define myApp                     [UIApplication sharedApplication]
#define myAppDelegate             ((AppDelegate*)myApp.delegate)
#define AppWindow                 myAppDelegate.window


/*=============================================================================
 GLOBAL MACRO
 =============================================================================*/

#endif  /* _GLOBALS_H_ */
