/*=============================================================================

 FILE:    macro.h

 =============================================================================*/
#ifndef _MACRO_H_H_
#define _MACRO_H_H_

/*=============================================================================
 IMPORT
 =============================================================================*/
#import "application.h"
#import "number.h"
#import "array.h"
#import "dictionary.h"
#import "color.h"
#import "font.h"
#import "data.h"
#import "notification.h"
#import "file.h"
#import "trace.h"
#import "globals.h"
#import "screen.h"
#import "navigation.h"
#import "device.h"
#import "date.h"
#import "logger.h"
#import "userdefault.h"
#import "userSettings.h"

/*=============================================================================
 MACRO DEFINITION
 =============================================================================*/
#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

#define CheckObjNil(o,v) (o == nil ? v : o)
#define ObjRelease(o)       {if(nil!=o) {id d=o; o=nil; [d release];}}
#define ObjSetRetain(o,v)   {ObjRelease(o); o=[v retain];}

/* layer macro */
#define LayerCircle(l) {\
[l setMasksToBounds:YES];\
[l setCornerRadius:l.frame.size.height/2];\
}

#define LayerConer(l) {\
[l setMasksToBounds:YES];\
[l setCornerRadius:5.0];\
}

#define LayerBorder(l) {\
[l setBorderWidth:1.0];\
[l setBorderColor:[[UIColor darkGrayColor] CGColor]];\
}

#define LayerRound(l) {\
LayerConer(l);\
LayerBorder(l);\
}

#define ButtonBorder(b) {                   \
b.layer.masksToBounds = YES;                \
b.layer.cornerRadius  = 5.0;               \
b.layer.borderWidth   = 2.0;                \
b.layer.borderColor   = ColorBlack.CGColor; \
}

#define LayerBorderActive(l) {\
[l setBorderWidth:2.0];\
[l setBorderColor:ColorBorderActive.CGColor];\
}

#define LayerBorderInactive(l) {\
[l setBorderWidth:1.0];\
[l setBorderColor:ColorBorderInactive.CGColor];\
}

#define DrawLine4View(v,f) {            \
CALayer *l        = [CALayer layer];    \
l.frame           = f;                  \
l.contentsGravity  = kCAGravityCenter;  \
l.backgroundColor = ColorLine.CGColor;  \
[v.layer insertSublayer:l atIndex:0];   \
}

#define BottomLine4View(v)  DrawLine4View(v,CGRectMake(0, ViewH(v)-1, ViewW(v), 1))
#define TopLine4View(v)     DrawLine4View(v,CGRectMake(0, 0, ViewW(v), 1))
#define LeftLine4View(v)    DrawLine4View(v,CGRectMake(0, 0, 1, ViewH(v)))
#define RightLine4View(v)   DrawLine4View(v,CGRectMake(ViewW(v), 0, 1, ViewH(v)-1))


#define IsKindOfClass(o,c)  [o isKindOfClass:[c class]]





/*=============================================================================
 Main macro
 =============================================================================*/
#define ActiveMainWindow()      [MainWindow makeKeyAndVisible]

#define NetworkBusy()           myApp.networkActivityIndicatorVisible = YES
#define NetworkFree()           myApp.networkActivityIndicatorVisible = NO

#define CallTarget(t,s,o)         if (((NSObject*)t) && [((NSObject*)t) respondsToSelector:s]) { [((NSObject*)t) performSelector:s withObject:o]; }
#define CallTargetAfter(t,s,o,d)  if (((NSObject*)t) && [((NSObject*)t) respondsToSelector:s]) { [((NSObject*)t) performSelector:s withObject:o afterDelay:d]; }
#define RunAfter(s,o)             CallTargetAfter(self,s,o,0.0)

/*=============================================================================
 UIView macro
 =============================================================================*/
#define RectO(v)                  v.origin
#define RectX(v)                  RectO(v).x
#define RectY(v)                  RectO(v).y
#define RectS(v)                  v.size
#define RectW(v)                  RectS(v).width
#define RectH(v)                  RectS(v).height
#define RectRight(v)              (RectX(v) + RectW(v))
#define RectBottom(v)             (RectY(v) + RectH(v))

#define RectL(v)                  RectX(v)
#define RectR(v)                  (RectX(v)+Rect(v))
#define RectT(v)                  RectY(v)
#define RectB(v)                  (RectY(v)+RectH(v))

#define ViewF(v)                  v.frame
#define ViewO(v)                  RectO(ViewF(v))
#define ViewX(v)                  ViewO(v).x
#define ViewY(v)                  ViewO(v).y
#define ViewS(v)                  RectS(ViewF(v))
#define ViewW(v)                  ViewS(v).width
#define ViewH(v)                  ViewS(v).height
#define ViewRight(v)              (ViewX(v) + ViewW(v))
#define ViewBottom(v)             (ViewY(v) + ViewH(v))

#define ViewL(v)                  ViewO(v).x
#define ViewR(v)                  (ViewX(v) + ViewW(v))
#define ViewT(v)                  ViewO(v).y
#define ViewB(v)                  (ViewY(v) + ViewH(v))

#define BarButtWidth(b)   (b.customView?  ViewW(b.customView) : (b.width? b.width : 33))
#define BarButtHigh(b)    33

#define StartNextInput(v,s,n)    [[v.superview viewWithTag:((v.tag-s +1) %n) + s] becomeFirstResponder]


/*--------------------------------------------------------------------------------------------------
 Ipod Player 
 --------------------------------------------------------------------------------------------------*/
#define iPodPlayer 						[MPMusicPlayerController iPodMusicPlayer]
#define iPodNowItem 					iPodPlayer.nowPlayingItem
#define iPodPlaybackTime				iPodPlayer.currentPlaybackTime
#define iPodVolume						iPodPlayer.volume
#define iPodPlayState					iPodPlayer.playbackState
#define iPodRepeatMode					iPodPlayer.repeatMode
#define iPodShuffleMode					iPodPlayer.shuffleMode
/*  Ipod Player  */

/*--------------------------------------------------------------------------------------------------
 
 --------------------------------------------------------------------------------------------------*/

/*=============================================================================
 Config button
 =============================================================================*/

/*=============================================================================
 CHECK DEVICE
 =============================================================================*/

/*=============================================================================
 Localization
 =============================================================================*/
#define LocStr(key) [[NSBundle mainBundle] localizedStringForKey:(key) value:@"" table:nil]

/*=============================================================================
 Alert
 =============================================================================*/
#define ErrorWithMessage(m,s) {\
    UIAlertView *alert = [[[UIAlertView alloc] initWithTitle:nil \
     message:m \
    delegate:s \
     cancelButtonTitle:@"OK" \
     otherButtonTitles: nil]; \
    [alert show];\
}

#define AnnouceWithMessage(m,s) {\
    UIAlertView *alert = [[[UIAlertView alloc] initWithTitle:nil \
    message:m \
    delegate:s \
    cancelButtonTitle:nil \
    otherButtonTitles: nil]; \
    [alert show];\
}

#define MakeAlert(t,m,s,k,o...) \
    [[UIAlertView alloc] initWithTitle:t \
    message:m \
    delegate:s \
    cancelButtonTitle:k \
    otherButtonTitles:o]; \


#define SAFE_RELEASE(p)			{ if (p) { [(p) release]; (p) = nil;  } }

// Localization
#define getLocalizationString(msg)                 [[PTTLanguageManager sharedLanguageManager] getTranslationForKey:msg]
#define IntToString(number)                         [NSString stringWithFormat:@"%i", number]
#endif  /* _MACRO_H_H_ */
