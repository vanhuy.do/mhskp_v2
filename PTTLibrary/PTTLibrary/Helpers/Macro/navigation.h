/*=============================================================================

 FILE:    navigation.h

 =============================================================================*/
#ifndef _NAVIGATION_H_H_
#define _NAVIGATION_H_H_

/*=============================================================================
 MACRO DEFINITION
 =============================================================================*/
#define IsNavigation(o)                   IsKindOfClass(o,UINavigationController)

#define Nav4Root(c)                       [[UINavigationController alloc] initWithRootViewController:c]

#define Nav2CtrlList(o)                   o.viewControllers
#define Nav2TopCtrl(o)                    o.topViewController
#define Nav2VisiableCtrl(o)               o.visibleViewController
#define Nav2LastCtrl(o)                   [Nav2CtrlList(o) lastObject]

/* Push/Pop */
#define NavPushCtrlAni(n,o,a)             [n pushViewController:o animated:a]
#define NavPushCtrl(n,o)                  NavPushCtrlAni(n,o,YES)

#define NavPopCtrlAni(n,a)                [n popViewControllerAnimated:a]
#define NavPopCtrl(n)                     NavPopCtrlAni(n,YES)

/* Tool bar */
#define NavSetToolBarAnimated(n,s,a)      [n setToolbarHidden:s animated:a]
#define NavShowToolBar(n)                 NavSetToolBarAnimated(n,NO,YES)
#define NavHideToolBar(n)                 NavSetToolBarAnimated(n,YES,YES)

/* Modal view */
#define CtrlPresentModal(c,o)     {\
UINavigationController *n = (UINavigationController*)o;\
if (!IsNavigation(o))   n = Nav4Root(o);\
[s presentViewController:n  animated:YES completion:nil];\
}

#define CtrlDismissModal(c,a)       [c dismissViewControllerAnimated:a completion:nil]


#if 0

#define Pop2Root()            updateNaviCtrl(NaviCtrl,Arr4Objs(CtrlTop))
#define Pop2CtrlAnimated(o,a) popNaviCtrl2Ctrl(NaviCtrl,o)
#define Pop2Ctrl(o)           popNaviCtrl2Ctrl(NaviCtrl,o)

#define Pop2Class(c)  { \
for (id ctrl in NaviCtrl.viewControllers) {\
if ([ctrl isKindOfClass:[c class]]){\
Pop2Ctrl(ctrl);\
break;\
}\
}\
}



#endif


/*=============================================================================
 Main Navigation Controller
 =============================================================================*/
#define AppNavCtrl                  myAppDelegate.navCtrl
#define AppCtrlList                 Nav2CtrlList(AppNavCtrl)
#define AppTopCtrl                  Nav2TopCtrl(AppNavCtrl)
#define AppVisCtrl                  Nav2VisiableCtrl(AppNavCtrl)
#define AppLastCtrl                 Nav2LastCtrl(AppNavCtrl)

#define PopCtrl()                   NavPopCtrl(AppNavCtrl)
#define PushCtrl(c)                 NavPushCtrl(AppNavCtrl,c)

#define ShowToolBar()               NavShowToolBar(AppNavCtrl)
#define HideToolBar()               NavHideToolBar(AppNavCtrl)


#endif  /* _NAVIGATION_H_H_ */
