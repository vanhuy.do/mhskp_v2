/*=============================================================================

 FILE:    screen.h

 =============================================================================*/
#ifndef _SCREEN_H_H_
#define _SCREEN_H_H_

/*=============================================================================
 MACRO DEFINITION
 =============================================================================*/
#define MainScreen             [UIScreen mainScreen]

#define ScrAppFrame             MainScreen.applicationFrame
#define ScrAppScrFrame          CGRectMake(0,0,ScrAppFrame.origin.x + ScrAppFrame.size.width, ScrAppFrame.origin.y + ScrAppFrame.size.height)
#define SCREEN_WIDTH            ScrAppFrame.size.width
#define SCREEN_HEIGHT           ScrAppFrame.size.height
#endif  /* _SCREEN_H_H_ */