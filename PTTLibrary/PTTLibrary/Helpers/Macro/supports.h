/*=============================================================================

 FILE:    supports.h

 =============================================================================*/
#ifndef _SUPPORTS_H_H_
#define _SUPPORTS_H_H_

/*============================================================================
 IMPORT
 =============================================================================*/

/*=============================================================================
 FUNCTION DEFINITION
 =============================================================================*/
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

void makeCallPhonne(NSString *phonenumber);
void makeEmail(NSString *emailAdrr);
void loadFacebook(NSString *nameid);
void loadFoursquare(NSString *nameid);
void loadTwitter(NSString *nameid);

id loadNIB(NSString *file);

//void updateNaviCtrl(UINavigationController* navCtrl, NSArray *newCtrls);
//void popNaviCtrl2Ctrl(UINavigationController* navCtrl, NSObject *ctrl);

NSString    *convertTzTxt(NSString *txtTz);
#if 1
#define date2ISO8601Str(date)   date
#else
NSString    *date2ISO8601Str(NSDate *date);
#endif
NSTimeZone  *timezone4Str(NSString *str);
NSString    *date2Str(NSDate *date, NSString *txtTz);
NSString    *date2DateStr(NSDate *date, NSString *txtTz);
NSString    *date2TimeStr(NSDate *date, NSString *txtTz);

NSDate    *str2Date(NSString *str);
NSString  *duration2Str(NSInteger aInt);
NSString *date2CompStr(NSDate *data);
NSString *reminderTime2Text(int n);

BOOL      IsRegularEmail(NSString *str);
BOOL      IsRegularPhone(NSString *str);

/*=============================================================================
 IMAGE SERVICE
 =============================================================================*/
/*----------------------------------------------------------------------------
 Method:      image4URL
 -----------------------------------------------------------------------------*/
UIImage   *image4URL(NSString *urlStr);
UIImage   *imageContact4URL(NSString *urlStr);
UIImage   *image4Size(UIImage *img, CGSize *size);

/*=============================================================================
 NSSTRING SERVICE
 =============================================================================*/
NSString  *str4RegularWithPlus(NSString *txt);
NSString  *str4Beauty(NSString *txt);

/*=============================================================================
 GPS SERVICE
 =============================================================================*/
NSDictionary  *coordinate4Address(NSString *addr);
NSString      *timezone4Coordinate(NSDictionary *coor);

/*=============================================================================
 Friend
 =============================================================================*/
void sortFriendByName(NSMutableArray *arr);

/*=============================================================================
 COLOR
 =============================================================================*/
UIColor *colorAdjust(UIColor *color, float adjust);
UIColor *colorDisable(UIColor *color);
#define colorHighlighed(c)      colorAdjust(c, 0.75)
#define colorSelected(c)        colorAdjust(c, 1.25)
#define colorDisabled(c)        c

#endif  /* _SUPPORTS_H_H_ */
