/*=============================================================================

 FILE:    supports.m

 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "supports.h"
#import "macro.h"

/*=============================================================================
 FUNCTION DEFINITION
 =============================================================================*/
/*----------------------------------------------------------------------------
 function:    makeCallPhonne
 -----------------------------------------------------------------------------*/
void makeCallPhonne(NSString *phonenumber) {
  /* make command */
  NSString  *cmd = Str4Fmt(@"tel:%@",phonenumber);
  NSURL     *url = [NSURL URLWithString:cmd];
  /* call command */
  if ([myApp canOpenURL:url]) {
    [myApp openURL:url];
  }
  return;
} /* makeCallPhonne */

/*----------------------------------------------------------------------------
 function:    makeEmail
 -----------------------------------------------------------------------------*/
void makeEmail(NSString *emailAdrr) {
  /* make command */
  NSString  *cmd = Str4Fmt(@"mailto:%@",emailAdrr);
  NSURL     *url = [NSURL URLWithString:cmd];
  /* call command */
  if ([myApp canOpenURL:url]) {
    [myApp openURL:url];
  }
  return;
} /* makeEmail */

/*----------------------------------------------------------------------------
 function:    loadFacebook
 -----------------------------------------------------------------------------*/
void loadFacebook(NSString *nameid) {
  /* make command */
  NSString  *cmd = Str4Fmt(@"http://www.facebook.com/profile.php?id=%@",nameid);
  NSURL     *url = [NSURL URLWithString:cmd];
  /* call command */
  if ([myApp canOpenURL:url]) {
    [myApp openURL:url];
  }
  return;
} /* loadFacebook */

/*----------------------------------------------------------------------------
 function:    loadTwitter
 -----------------------------------------------------------------------------*/
void loadTwitter(NSString *nameid) {
  /* make command */
  NSString  *cmd = Str4Fmt(@"http://www.twitter.com/%@",nameid);
  NSURL     *url = [NSURL URLWithString:cmd];
  /* call command */
  if ([myApp canOpenURL:url]) {
    [myApp openURL:url];
  }
  return;
} /* loadTwitter */

/*----------------------------------------------------------------------------
 function:    loadFoursquare
 -----------------------------------------------------------------------------*/
void loadFoursquare(NSString *nameid) {
  /* make command */
  NSString  *cmd = Str4Fmt(@"https://foursquare.com/%@",nameid);
  NSURL     *url = [NSURL URLWithString:cmd];
  /* call command */
  if ([myApp canOpenURL:url]) {
    [myApp openURL:url];
  }
  return;
} /* loadFoursquare */

/*----------------------------------------------------------------------------
 function:    loadNIB
 -----------------------------------------------------------------------------*/
id loadNIB(NSString *file) {
  NSArray *arr  = [MainBundle loadNibNamed:file owner:nil options:nil];
  id      ret   = [arr objectAtIndex:0];
  return ret;
} /* loadNIB */

///*----------------------------------------------------------------------------
// function:    updateNaviCtrl
// -----------------------------------------------------------------------------*/
//void updateNaviCtrl(UINavigationController* navCtrl, NSArray *newCtrls) {
//  funcstart();
//  /* check old Arr in new Arr */
//  for (NSObject *o in navCtrl.viewControllers){
//    if (!ArrContain(newCtrls,o)) {
//      CallTarget(o,@selector(didPopMe),nil);
//    }
//  }
//  /* update control list */
//  [navCtrl setViewControllers:newCtrls animated:YES];
//  funcstop();
//  return;
//} /* updateNaviCtrl */

///*----------------------------------------------------------------------------
// function:    popNaviCtrl2Ctrl
// -----------------------------------------------------------------------------*/
//void popNaviCtrl2Ctrl(UINavigationController* navCtrl, NSObject *ctrl) {
//  funcstart();
//  NSMutableArray *arr = ArrNew();
//  /* make new array */
//  for (int i = 0; i < ArrCount(navCtrl.viewControllers); i++){
//    if (ctrl != ArrObj4Idx(navCtrl.viewControllers,i)){
//      ArrAddObj(arr,ArrObj4Idx(navCtrl.viewControllers,i));
//    } else {
//      ArrAddObj(arr,ctrl);
//      updateNaviCtrl(navCtrl,arr);
//      break;
//    }
//  }
//  funcstop();
//  return;
//} /* popNaviCtrl2Ctrl */

/*----------------------------------------------------------------------------
 Method:      timezone4Str
 -----------------------------------------------------------------------------*/
NSTimeZone *timezone4Str(NSString *str){
  NSTimeZone *tz = nil;
  if (IsKindOfClass(str,NSString) && [str length]) {
    if ([str integerValue]){
      str = Str4Fmt(@"GMT%@",str);
    }
    tz = [NSTimeZone timeZoneWithName:str];
  } else {
    tz = [NSTimeZone localTimeZone];
  }
  return tz;
} /* timezone4Str */

/*----------------------------------------------------------------------------
 Method:      date2Str
 -----------------------------------------------------------------------------*/
NSString *date2Str(NSDate *date, NSString *txtTz){
  return Str4Fmt(@"%@ %@", date2DateStr(date,txtTz), date2TimeStr(date,txtTz));
} /* date2Str */

/*----------------------------------------------------------------------------
 Method:      date2DateStr
 -----------------------------------------------------------------------------*/
NSString *date2DateStr(NSDate *date, NSString *txtTz){
  NSTimeZone      *tz     = timezone4Str(txtTz);
  NSDateFormatter *format = [[NSDateFormatter alloc] init] ;
  [format setTimeZone:tz];
  //[format setDateStyle:NSDateFormatterMediumStyle];
  //[format setTimeStyle:NSDateFormatterNoStyle];
  
#if 1
  //NSString *str = [format dateFormat];
  //mtrace(@"str[%s",[str UTF8String]);
  //str = [NSString stringWithFormat:@"EEEE, %@",str];
  [format setDateFormat:@"EEEE, MMM d"];
#endif
  return [format stringFromDate:date];
} /* date2DateStr */

/*----------------------------------------------------------------------------
 Method:      convertTzTxt
 -----------------------------------------------------------------------------*/
NSString *convertTzTxt(NSString *txtTz){
  NSString      *ret      = nil;
#if 0
  NSDictionary  *tzDict   = [NSTimeZone abbreviationDictionary];
#else
  static NSDictionary  *tzDict   = nil;
  
  if (nil == tzDict){
    tzDict = Dict4ObjsKeys(@"EST", @"America/New_York",
                           @"PST", @"America/Los_Angeles", 
                           @"ICT", @"Asia/Ho_Chi_Minh",    
                           @"EST", @"-0400",               
                           @"EST", @"-0500",               
                           @"PST", @"-0700",               
                           @"PST", @"-0800",               
                           @"ICT", @"+0700",
                           @"EST", @"GMT-0400",               
                           @"EST", @"GMT-0500",               
                           @"PST", @"GMT-0700",               
                           @"PST", @"GMT-0800",
                           @"ICT", @"GMT+0700");
    [tzDict mutableCopy];
  }
#endif
  ret      = (IsString(DictObj4Key(tzDict,txtTz))? DictObj4Key(tzDict,txtTz) : txtTz);
  return ret;
} /* convertTzTxt */

/*----------------------------------------------------------------------------
 Method:      date2TimeStr
 -----------------------------------------------------------------------------*/
NSString *date2TimeStr(NSDate *date, NSString *txtTz){
  NSTimeZone      *tz     = timezone4Str(txtTz);
  NSDateFormatter *format = [[NSDateFormatter alloc] init];
  [format setTimeZone:tz];
  /* only get Time */
  [format setDateStyle:NSDateFormatterNoStyle];
  [format setTimeStyle:NSDateFormatterShortStyle];
  
  NSString *str = StrRemSpace([format dateFormat]);
  [format setDateFormat:str];
  
  
  NSString *timeStr = [format stringFromDate:date];
  
#if 0
  if (0 < StrLen(txtTz)){
    timeStr = [timeStr stringByAppendingFormat:@"(%@)",convertTzTxt(txtTz)];
  }
#endif
  return timeStr;
} /* date2TimeStr */

/*----------------------------------------------------------------------------
 Method:      str2Date
 -----------------------------------------------------------------------------*/
NSDate *str2Date(NSString *str){
  /* standard time string */
  if (1 < ArrCount([str componentsSeparatedByString:@"+"])) {
    NSArray *arr = [str componentsSeparatedByString:@"+"];
    str = [NSString stringWithFormat:@"%@+%@",
           ArrObj4Idx([ArrObj4Idx(arr,0) componentsSeparatedByString:@"."],0),
           [ArrObj4Idx(arr,1) stringByReplacingOccurrencesOfString:@":" withString:@""]];
#if 0
  } else if (1 < ArrCount([str componentsSeparatedByString:@"-"])) {
    NSArray *arr = [str componentsSeparatedByString:@"-"];
    str = [NSString stringWithFormat:@"%@-%@",
           ArrObj4Idx([ArrObj4Idx(arr,0) componentsSeparatedByString:@"."],0),
           [ArrObj4Idx(arr,1) stringByReplacingOccurrencesOfString:@":" withString:@""]];
#endif
  } else {
    str = Str4Fmt(@"%@+0000",ArrObj4Idx([str componentsSeparatedByString:@"."],0));
  }

  /* convert string to date */
  NSDateFormatter *format = [[NSDateFormatter alloc] init];
  [format setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
  [format setDateFormat:@"yyyy-MM-dd HH:mm:ssZ"];
  return [format dateFromString:str];
} /* str2Date */

/*----------------------------------------------------------------------------
 Method:      duration2Str
 -----------------------------------------------------------------------------*/
NSString *duration2Str(NSInteger aInt){
  NSString *ret = @" ";
  
  int hrs = aInt/60;
  int min = aInt%60;
  /* hours */
  if (1 < hrs) {
    ret = Str4Fmt(@"%dhrs",hrs);
  } else if (0 < hrs) {
    ret = Str4Fmt(@"%dhr",hrs);
  }
  
  /* space */
  if (0 < min && 0 < hrs){
    ret = [ret stringByAppendingString:@" "];
  }

  /* minutes */
  if (1 < min) {
    ret = [ret stringByAppendingFormat:@"%dmins",min];
  } else if (0 < min) {
    ret = [ret stringByAppendingFormat:@"%dmin",min];
  }

  return ret;
} /* duration2Str */

/*----------------------------------------------------------------------------
 Method:      date2CompStr
 -----------------------------------------------------------------------------*/
NSString *date2CompStr(NSDate *date) {
  NSString *ret = nil;
  NSCalendar  *cal  = [NSCalendar currentCalendar];
  NSInteger   opt   = (   NSCalendarUnitYear  | NSCalendarUnitMonth   | NSCalendarUnitDay
                       |  NSCalendarUnitHour  | NSCalendarUnitMinute  );
  NSDateComponents *comp = [cal components:opt fromDate:date toDate:[NSDate date] options:0];
  /* parce comp */
  if        (1 < comp.year){
    ret = Str4Fmt(@"%d years ago", comp.year);
  } else if (1 == comp.year) {
    ret = Str4Fmt(@"%d year ago", comp.year);
  }  else if (1 < comp.month) {
    ret = Str4Fmt(@"%d months ago", comp.month);
  } else if (1 == comp.month) {
    ret = Str4Fmt(@"%d month ago", comp.month);
  }  else if (1 < comp.day) {
    ret = Str4Fmt(@"%d days ago", comp.day);
  } else if (1 == comp.day) {
    ret = Str4Fmt(@"%d day ago", comp.day);
  }  else if (1 < comp.hour) {
    ret = Str4Fmt(@"%d hours ago", comp.hour);
  } else if (1 == comp.hour) {
    ret = Str4Fmt(@"%d hour ago", comp.hour);
  }  else if (1 < comp.minute) {
    ret = Str4Fmt(@"%d minutes ago", comp.minute);
  } else if (1 == comp.minute) {
    ret = Str4Fmt(@"%d minute ago", comp.minute);
  } else {
    ret = @"right now!";
  }
  return ret;
} /* date2CompStr */

/*=============================================================================
 IMAGE SERVICE
 =============================================================================*/
/*----------------------------------------------------------------------------
 Method:      image4URL
 -----------------------------------------------------------------------------*/
UIImage   *image4URL(NSString *urlStr){
  UIImage *img = nil;
  
  if (IsKindOfClass(urlStr,NSString) && [urlStr length]) {
    /* get data from urlStr */
    NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:urlStr]];
    /* check to create image */
    if (nil != data){
      img = [UIImage imageWithData:data];
    }
  }

  return img;
} /* image4URL */

/*----------------------------------------------------------------------------
 Method:      image4Size
 -----------------------------------------------------------------------------*/
UIImage   *image4Size(UIImage *img, CGSize *size){
  UIImage *ret = nil;
  /* check device to make real size */
  //size->width   = 2*size->width;
  //size->height  = 2*size->height;
  /* check to make scale */
  if (img.size.width != size->width || img.size.height != size->height) {
    /* calcule scale and fit for scale image */
    CGFloat scale = img.size.width/size->width;
    if (scale < img.size.height/size->height) {
      scale = img.size.height/size->height;
    }
    CGFloat w = img.size.width/scale;
    CGFloat h = img.size.height/scale;
    CGFloat x = (size->width - w)/2.0;
    CGFloat y = (size->height -h)/2.0;
    
    /* make the scale */
    UIGraphicsBeginImageContext(*size);
    [img drawInRect:CGRectMake(x,y,w,h)];
    ret = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
  } else {
    ret = img;
  }
  return ret;
} /* image4Size */

/*=============================================================================
 NSSTRING SERVICE
 =============================================================================*/
/*----------------------------------------------------------------------------
 Method:      str4Beauty
 -----------------------------------------------------------------------------*/
NSString  *str4Beauty(NSString *txt) {
  NSMutableString *str  = [NSMutableString string];
  NSArray         *arr  = [txt componentsSeparatedByString:@" "];
  for (NSString *s in arr) {
    if (0 < [s length]) {
      [str appendFormat:@" %@",s];
    }
  }
  if (0 < [str length]) {
    NSRange r = {0,1};
    [str deleteCharactersInRange:r];
  }
  return str;
} /* str4Beauty */

/*----------------------------------------------------------------------------
 Method:      str4RegularWithPlus
 -----------------------------------------------------------------------------*/
NSString  *str4RegularWithPlus(NSString *txt) {
  NSMutableString *str  = [NSMutableString string];
  NSArray         *arr  = [txt componentsSeparatedByString:@" "];
  for (NSString *s in arr) {
    if (0 < [s length]) {
      [str appendFormat:@"+%@",s];
    }
  }
  if (0 < [str length]) {
    NSRange r = {0,1};
    [str deleteCharactersInRange:r];
  }
  return str;
} /* str4RegularWithPlus */

/*=============================================================================
 GPS SERVICE
 =============================================================================*/
/*----------------------------------------------------------------------------
 Method:      coordinate4Address
 -----------------------------------------------------------------------------*/
NSDictionary  *coordinate4Address(NSString *addr) {
  NSDictionary  *ret    = nil;
  
#if 1
#if 0
  NSString *strURL = [Str4Fmt(@"http://maps.googleapis.com/maps/api/geocode/json?address=%@&sensor=true",addr)
                      stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
  NSString *locStr = [NSString stringWithContentsOfURL:[NSURL URLWithString:strURL]] encoding:NSUTF8StringEncoding error:nil];
  NSDictionary *dict = [locStr JSONValue];
  /* check result */
  if (isString(DictObj4Key(dict,@"status")) && strIsEqual(DictObj4Key(dict,@"status"),@"OK")){
    NSArray *data = DictObj4Key(dict,@"results");
    for (NSDictionary *d in data){
      NSArray *a = DictObj4Key(d, @"types");
      if (isArray(a)){
        if (!ArrContain(a, @"route")    && 
            !ArrContain(a, @"political")  ) {
          NSDictionary *g   = DictObj4Key(d, @"geometry");
          NSDictionary *l   = DictObj4Key(g, @"location");
          NSNumber     *lt  = DictObj4Key(l, @"lat");
          NSNumber     *ln  = DictObj4Key(l, @"lng");
          if (IsNumber(lt) && IsNumber(ln)){
            ret = Dict4ObjsKeys(lt, @"latitude",
                                ln, @"longitude");
            break;
          }
        }      
      }
    }
  }

#else
  NSString  *url = Str4AddPercent(Str4Fmt(@"http://maps.google.com/maps/geo?q=%@&output=csv", addr));
  etrace(@"url[%s]",[url UTF8String]);
  NSString  *loc = [NSString stringWithContentsOfURL:Str2URL(url) encoding:NSUTF8StringEncoding error:nil];
  NSArray   *arr = [loc componentsSeparatedByString:@","];

  etrace(@"loc[%s]",[loc UTF8String]);
  if (4 == ArrCount(arr)){
    if (200 == Str2Int(ArrObj4Idx(arr,0))) {
      ret = Dict4ObjsKeys(Num4Float(Str2Float(ArrObj4Idx(arr,2))), @"latitude",
                          Num4Float(Str2Float(ArrObj4Idx(arr,3))), @"longitude");
    } else {
      etrace(@"Error locationString[%s]",[loc UTF8String]);
    }
  } else {
    etrace(@"Error locationString[%s]",[loc UTF8String]);
  }
#endif
#endif
  return ret;
} /* coordinate4Address */


/*----------------------------------------------------------------------------
 Method:      coordinate4Address
 -----------------------------------------------------------------------------*/
NSString  *timezone4Coordinate(NSDictionary *coor) {
  NSString      *ret    = nil;
  NSString      *urlStr = [[NSString stringWithFormat:@"http://www.earthtools.org/timezone/%@/%@", 
                                    DictObj4Key(coor, @"latitude"),
                                    DictObj4Key(coor, @"longitude")]
                                   stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
  NSString      *txStr  = [NSString stringWithContentsOfURL:[NSURL URLWithString:urlStr] encoding:NSUTF8StringEncoding error:nil];
  NSArray       *arr1   = [txStr componentsSeparatedByString:@"<isotime>"];
  if (2 == ArrCount(arr1)) {
    NSArray     *arr2   = [ArrObj4Idx(arr1,1) componentsSeparatedByString:@"</isotime>"];
    if (2 == ArrCount(arr2)) {
      NSString  *str    = ArrObj4Idx(arr2,0);
      str = [str substringFromIndex:StrLen(str)-5];
      if (![str hasPrefix:@"+"] && ![str hasPrefix:@"-"]) {
        ret = Str4Fmt(@"GMT+%@",str);
      } else {
        ret = Str4Fmt(@"GMT%@",str);
      }
    }
  }
  
  return ret;
} /* coordinate4Address */

/*=============================================================================
 COLOR
 =============================================================================*/
/*----------------------------------------------------------------------------
 Method:      colorAdjust
 -----------------------------------------------------------------------------*/
UIColor *colorAdjust(UIColor *color, float adjust) {
  UIColor     *ret = nil;
  CGColorRef  cg  = color.CGColor;
  size_t      n     = CGColorGetNumberOfComponents(cg);
  float       *comp = (float*)CGColorGetComponents (cg);
  if (4 == n){
    ret = ColorRGB(comp[0]*adjust, comp[1]*adjust, comp[2]*adjust);
  } else {
    ret = ColorRGB(comp[0]*adjust, comp[0]*adjust, comp[0]*adjust);
  }
  return ret;
} /* colorAdjust */

/*----------------------------------------------------------------------------
 Method:      colorDisable
 -----------------------------------------------------------------------------*/
UIColor *colorDisable(UIColor *color) {
  float       adjust = 1.0;
  UIColor     *ret = nil;
  CGColorRef  cg  = color.CGColor;
  size_t      n     = CGColorGetNumberOfComponents(cg);
  float       *comp = (float*)CGColorGetComponents (cg);
  if (4 == n){
    ret = ColorRGBA(comp[0]*adjust, comp[1]*adjust, comp[2]*adjust, 0.18);
  } else {
    ret = ColorRGBA(comp[0]*adjust, comp[0]*adjust, comp[0]*adjust, 0.18);
  }
  return ret;
} /* colorAdjust */

/*----------------------------------------------------------------------------
 Method:      IsRegularEmail
 -----------------------------------------------------------------------------*/
BOOL IsRegularEmail(NSString *str) {
  BOOL ret = NO;
  
  NSString *email = [str lowercaseString];
  NSString *emailRegEx =
  @"(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"
  @"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
  @"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
  @"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"
  @"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
  @"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
  @"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
  
  NSPredicate *regExPredicate =
  [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
  ret = [regExPredicate evaluateWithObject:email];
  return ret;
} /* IsRegularEmail */

/*----------------------------------------------------------------------------
 Method:      IsRegularPhone
 -----------------------------------------------------------------------------*/
BOOL      IsRegularPhone(NSString *str) {
  BOOL ret = NO;
  NSDataDetector  *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypePhoneNumber
                                                              error:nil];
  NSUInteger      match     = [detector numberOfMatchesInString:str
                                                        options:0
                                                          range:NSMakeRange(0, [str length])];
  ret = (StrLen(str) == match);
  return ret;
} /* IsRegularPhone */
