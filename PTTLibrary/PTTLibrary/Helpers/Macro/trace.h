/*============================================================================

 FILE:    trace.h

 ============================================================================*/
#ifndef _TRACE_H_
#define _TRACE_H_

/*============================================================================
 FLAG PRECOMPILE
 ============================================================================*/


#ifndef NS_BLOCK_ASSERTIONS
#define TRACING_ENABLE
#else
#undef  TRACING_ENABLE
#endif

#undef  TRACING_ENABLE
#define TRACING_ENABLE

#ifdef TRACING_ENABLE
#define TRACE_TO_FILE
#define LOW_TRACING_ENABLE
#define MIDLE_TRACING_ENABLE
#define HIGH_TRACING_ENABLE
#define ERROR_TRACING_ENABLE
#define FATAL_TRACING_ENABLE
#define FUNC_TRACING_ENABLE       // remove comment to turn on the Debug NSLog statement

#define DATA_TRACING_ENABLE
#define REQUEST_TRACING_ENABLE
//#define ALL_REQUEST_TRACING_ENABLE

#endif

/*============================================================================
 TRACE MACRO
 ============================================================================*/
/*----------------------------------------------------------------------------
 Macro: __FILENAME__
 ----------------------------------------------------------------------------*/
#define __LOG_FILENAME__      @"logfile.txt"
#define __FILENAME__          [@__FILE__ lastPathComponent]


#import <Foundation/Foundation.h>




void printToFile(NSString *str);

/*----------------------------------------------------------------------------
 Macro: debug
 ----------------------------------------------------------------------------*/
#ifndef TRACE_TO_FILE
#define debug(_l_,_f_,...) NSLog(@"@%@:%@(%d)%s-" _f_,_l_,__FILENAME__,__LINE__,__FUNCTION__, ## __VA_ARGS__);
#else
#define debug(_l_,_f_,...) {\
NSString *str = [NSString stringWithFormat: @"@%@:%@(%d)%s-" _f_,_l_,__FILENAME__,__LINE__,__FUNCTION__, ## __VA_ARGS__];\
NSLog(@"%@",str);\
printToFile(str);\
}
#endif

/*----------------------------------------------------------------------------
 Macro: trace
 ----------------------------------------------------------------------------*/
#ifdef TRACING_ENABLE
#ifdef TRACE_TO_FILE
  #define trace(_level_,_args_...) debug(_level_,_args_)
#else
  #define trace(_level_,_args_...) debug(_level_,_args_)
#endif
#else
  #define trace(...)
#endif /*TRACING_ENABLE */

/*----------------------------------------------------------------------------
 Macro: ltrace
 ----------------------------------------------------------------------------*/
#ifdef LOW_TRACING_ENABLE
#define ltrace(_args_...) trace(@"LOW",_args_)
#else
#define ltrace(...)
#endif /*LOW_TRACING_ENABLE */

/*----------------------------------------------------------------------------
 Macro: mtrace
 ----------------------------------------------------------------------------*/
#ifdef MIDLE_TRACING_ENABLE
#define mtrace(_args_...) trace(@"MID",_args_)
#else
#define mtrace(...)
#endif /*MIDLE_TRACING_ENABLE */

/*----------------------------------------------------------------------------
 Macro: htrace
 ----------------------------------------------------------------------------*/
#ifdef HIGH_TRACING_ENABLE
#define htrace(_args_...) trace(@"HIGH",_args_)
#else
#define htrace(...)
#endif /*HIGH_TRACING_ENABLE */

/*----------------------------------------------------------------------------
 Macro: etrace
 ----------------------------------------------------------------------------*/
#ifdef ERROR_TRACING_ENABLE
#define etrace(_args_...) trace(@"ERROR",_args_)
#else
#define etrace(...)
#endif /*ERROR_TRACING_ENABLE */

/*----------------------------------------------------------------------------
 Macro: ftrace
 ----------------------------------------------------------------------------*/
#ifdef FATAL_TRACING_ENABLE
#define ftrace(_args_...) trace(@"FATAL",_args_)
#else
#define ftrace(...)
#endif /*FATAL_TRACING_ENABLE */

/*----------------------------------------------------------------------------
 Macro: rtrace
 ----------------------------------------------------------------------------*/
#ifdef REQUEST_TRACING_ENABLE
#define rtrace(r) {\
NSString *errorStr = [NSString stringWithFormat:@"request [url: %@, post: %@] - response[%@]",\
[r url],\
[[NSString alloc] initWithData:[r postBody] encoding:NSUTF8StringEncoding],\
r.responseString];\
etrace(@"[%s]",[errorStr UTF8String]);\
}
#else
#define rtrace(...)
#endif /*REQUEST_TRACING_ENABLE */


/*----------------------------------------------------------------------------
 Macro: allrtrace
 ----------------------------------------------------------------------------*/
#ifdef ALL_REQUEST_TRACING_ENABLE
#define allrtrace(r) {\
NSString *errorStr = [NSString stringWithFormat:@"request [url: %@, post: %@] - response[%@]",\
[r url],\
[[NSString alloc] initWithData:[r postBody] encoding:NSUTF8StringEncoding],\
r.responseString];\
mtrace(@"[%s]",[errorStr UTF8String]);\
}
#else
#define allrtrace(...)
#endif /* ALL_REQUEST_TRACING_ENABLE */

/*----------------------------------------------------------------------------
 Macro: dtrace
 ----------------------------------------------------------------------------*/
#ifdef DATA_TRACING_ENABLE
#define dtrace(k) mtrace(@"[%s]",[[k description] UTF8String]);
#else
#define dtrace(...)
#endif /*DATA_TRACING_ENABLE */

/*----------------------------------------------------------------------------
 Macro: funcstart
 ----------------------------------------------------------------------------*/
#ifdef FUNC_TRACING_ENABLE
#define funcstart()       ltrace(@"START")
#define funcstop()        ltrace(@"STOP")
#else
#define funcstart(...)
#define funcstop(...)
#endif


/*============================================================================
 TRACE_TO_FILE
 =============================================================================*/
#if 0
#ifdef TRACE_TO_FILE

#define Debug2File(s)  [[Trace2File currentInstance] trace2File:s]
/*----------------------------------------------------------------------------
 Interface:   Trace2File
 -----------------------------------------------------------------------------*/
@interface Trace2File : NSObject {
  NSThread *myThread;
  
} /* Trace2File */

/*----------------------------------------------------------------------------
 Variable property
 -----------------------------------------------------------------------------*/
@property (nonatomic,retain) NSThread *myThread;

/*============================================================================
 METHODS
 =============================================================================*/
+(id) currentInstance;
-(void) trace2File:(NSString*)str;

@end /* Trace2File */
#endif /* TRACE_TO_FILE */

#endif  /* _TRACE_H_ */
#endif
