/*=============================================================================

 FILE:    trace.m

 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "trace.h"

/*=============================================================================
 FUNCTION DEFINITION
 =============================================================================*/
/*----------------------------------------------------------------------------
 function:    printToFile
 -----------------------------------------------------------------------------*/
void printToFile(NSString *str) {
  /* check to create log file */\
  NSArray       *myPathList = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
  NSString      *myPath     = [[myPathList  objectAtIndex:0] stringByAppendingPathComponent:__LOG_FILENAME__];
  NSFileManager *fm         = [NSFileManager defaultManager];
  NSDictionary  *fileAttr   = [fm attributesOfItemAtPath:myPath error:nil];
  /* check file size to clean */
  if (    (nil != fileAttr)
      &&  (1024*1024 < [fileAttr fileSize])) {
    NSLog(@"\n ================== MOVE LOG FILE ================== \n");
    [fm removeItemAtPath:myPath error:nil];
  }
  if(![fm fileExistsAtPath:myPath]){
    [fm createFileAtPath:myPath contents:nil attributes:nil];
  }
  NSFileHandle *fd0 = [NSFileHandle fileHandleForUpdatingAtPath:myPath];
  [fd0 seekToEndOfFile];
#if 0
  NSFileHandle *fd1 = [[NSFileHandle alloc] initWithFileDescriptor:[fd0 fileDescriptor] closeOnDealloc:YES];
//  [fd1 autorelease];
  [NSThread detachNewThreadSelector:@selector(writeData:) toTarget:fd1 withObject:[str dataUsingEncoding:NSUTF8StringEncoding]];
#else
  [NSThread detachNewThreadSelector:@selector(writeData:) toTarget:fd0 withObject:[str dataUsingEncoding:NSUTF8StringEncoding]];
#endif
  return;
} /* printToFile */


#if 0
/*============================================================================
 PROJECT: ScrapBooks
 FILE:    iObject.m
 AUTHOR:  Mai Trung Tin
 =============================================================================*/
#ifdef TRACE_TO_FILE

/*============================================================================
 IMPORT
 =============================================================================*/
#import "Trace2File.h"

/*============================================================================
 DEFINITION
 =============================================================================*/
static Trace2File *_staticObj = nil;
/*============================================================================
 MACRO
 =============================================================================*/

/*============================================================================
 IMPLMENTATION
 =============================================================================*/
/*----------------------------------------------------------------------------
 Interface:   Trace2File
 -----------------------------------------------------------------------------*/
@implementation Trace2File

/*============================================================================
 SYNTHESIZE
 =============================================================================*/
@synthesize myThread;

/*============================================================================
 METHODS
 =============================================================================*/
/*----------------------------------------------------------------------------
 Methods:     currentInstance
 ----------------------------------------------------------------------------*/
+(id) currentInstance {
  funcstart();
  /* check to create */
  if (nil == _staticObj){
    _staticObj = [[Trace2File alloc] init];
  }
  funcstop();
  return _staticObj;
} /* currentInstance */

/*----------------------------------------------------------------------------
 Method:      init
 -----------------------------------------------------------------------------*/
-(id)   init {
  /* call super */
  if (nil != (self = [super init])){
    self.myThread = [[NSThread alloc] init];
  }
  return self;
} /* init */

/*----------------------------------------------------------------------------
 Method:      dealloc
 -----------------------------------------------------------------------------*/
- (void)dealloc {
  funcstart();
  /* release */
  self.myThread = nil;
  /* cal super */
  [super dealloc];
  funcstop();
  return;
} /* dealloc */
@end /* iObject */
#endif /* TRACE_TO_FILE */
#endif