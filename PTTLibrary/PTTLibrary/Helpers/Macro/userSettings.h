/*============================================================================

 FILE:    userSettings.h

 ============================================================================*/
#ifndef _USER_SETTINGS_H_
#define _USER_SETTINGS_H_

/*============================================================================
 KEY LIST
 ============================================================================*/

/* Last run status */
#define MY_APP_RUNNING                  @"MY_APP_RUNNING"
#define UsrDfltSetAppRunning()          UsrDfltSetObjKey(Num4Bool(YES),MY_APP_RUNNING)
#define UsrDfltGetAppRunning()          UsrDfltObj4Key(MY_APP_RUNNING)
#define UsrDfltClearAppRunning()        UsrDfltCleanKey(MY_APP_RUNNING)
#define IsMyAppCrashed()                IsNumber(UsrDfltGetAppRunning())

/*============================================================================
 SETTINGs
 ============================================================================*/
/* SETTINGS READY */
#define SETTINGS_READY                  @"SETTINGS_READY"
#define SettingsReadySet()              {UsrDfltSetBool4Key(YES,SETTINGS_READY); UsrDfltSync();}
#define SettingsReadyGet()              UsrDfltGetBool4Key(SETTINGS_READY)
#define IsSettingsReady()               (YES == SettingsReadyGet())

/* SPEAKER */
#define SETTINGS_SPEAKER                @"SETTINGS_SPEAKER"
#define SettingsSpeakerSet(v)           {UsrDfltSetBool4Key(v,SETTINGS_SPEAKER); UsrDfltSync();}
#define SettingsSpeakerGet()            UsrDfltGetBool4Key(SETTINGS_SPEAKER)
#define IsSettingsSpeakerOn()           (YES == SettingsSpeakerGet())

/* VOICE QUALITY */
#define SETTINGS_VOICE_QUALITY          @"SETTINGS_VOICE_QUALITY"
#define SettingsVoiceQualitySet(v)      {UsrDfltSetInt4Key(v,SETTINGS_VOICE_QUALITY); UsrDfltSync();}
#define SettingsVoiceQualityGet()       UsrDfltGetInt4Key(SETTINGS_VOICE_QUALITY)

/* VOICE LENGTH */
#define SETTINGS_VOICE_LENGTH           @"SETTINGS_VOICE_LENGTH"
#define SettingsVoiceLengthSet(v)       {UsrDfltSetInt4Key(v,SETTINGS_VOICE_LENGTH); UsrDfltSync();}
#define SettingsVoiceLengthGet()        UsrDfltGetInt4Key(SETTINGS_VOICE_LENGTH)

#endif