/*============================================================================

 FILE:    userdefault.h

 ============================================================================*/
#ifndef _USER_DEFAULT_H_
#define _USER_DEFAULT_H_

/*============================================================================
 FUNCTION DEIFNITION
 ============================================================================*/
#define UsrDflt                         [NSUserDefaults standardUserDefaults]
#define UsrDfltSync()                   [UsrDflt synchronize]

#define UsrDfltCleanKey(k)              [UsrDflt removeObjectForKey:k]
#define UsrDfltSetObjKey(o,k)           [UsrDflt setObject:o forKey:k]
#define UsrDfltSetObjKeySync(o,k)       {UsrDfltSetObjKey(o,k); UsrDfltSync();}

#define UsrDfltObj4Key(k)               [UsrDflt objectForKey:k]
#define UsrDfltDict4Key(k)              [UsrDflt dictionaryForKey:k]
#define UsrDfltStr4Key(k)              [UsrDflt stringForKey:k]
#define UsrDfltArr4Key(k)              [UsrDflt arrayForKey:k]

#define UsrDfltRmKey(k)                 [UsrDflt removeObjectForKey:k]




#define UsrDfltSetBool4Key(o,k)         [UsrDflt setBool:o forKey:k]
#define UsrDfltSetBool4KeySync(o,k)       {UsrDfltSetBool4Key(o,k); UsrDfltSync();}
#define UsrDfltGetBool4Key(k)           [UsrDflt boolForKey:k]

#define UsrDfltSetInt4Key(o,k)         [UsrDflt setInteger:o forKey:k]
#define UsrDfltSetInt4KeySync(o,k)     {UsrDfltSetInt4Key(o,k); UsrDfltSync();}
#define UsrDfltGetInt4Key(k)           [UsrDflt integerForKey:k]

#if 0
– arrayForKey:

– dataForKey:
– dictionaryForKey:
– floatForKey:

– objectForKey:
– stringArrayForKey:
– stringForKey:
– doubleForKey:
– URLForKey:

– setBool:forKey:
– setFloat:forKey:

– setObject:forKey:
– setDouble:forKey:
– setURL:forKey:
#endif



/* SERVER */

#endif  /* _USER_DEFAULT_H_ */
