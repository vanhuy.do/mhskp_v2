/*============================================================================

 FILE:    userkeys.h

 ============================================================================*/
#ifndef _USER_KEY_H_
#define _USER_KEY_H_

/*============================================================================
 KEY LIST
 ============================================================================*/
#define MY_APP_RUNNING              @"MY_APP_RUNNING"

/* Last run status */
#define UsrDfltSetAppRunning()          UsrDfltSetObjKey(Num4Bool(YES),MY_APP_RUNNING)
#define UsrDfltGetAppRunning()          UsrDfltObj4Key(MY_APP_RUNNING)
#define UsrDfltClearAppRunning()        UsrDfltCleanKey(MY_APP_RUNNING)
#define IsMyAppCrashed()                IsNumber(UsrDfltGetAppRunning())

#endif  /* _USER_KEY_H_ */
