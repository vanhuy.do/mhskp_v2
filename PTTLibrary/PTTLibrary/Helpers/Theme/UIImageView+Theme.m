//
//  UIImageView+Theme.m
//  PTTLibrary
//
//  Created by Admin on 9/7/15.
//  Copyright (c) 2015 FCS Computer Systems. All rights reserved.
//

#import "UIImageView+Theme.h"
#import "GlobalObject.h"
#import "UIImage+PTT.h"

@implementation UIImageView (Theme)
- (void)awakeFromNib {
    if ([GlobalObject instance].currentTheme == THEME_MHSKP ) {
        if (self.accessibilityLabel) {
            DLog(@"%@", self.accessibilityLabel);
            self.image = [UIImage PTTImageNamed:self.accessibilityLabel];
        }
        
        if (self.accessibilityHint) {
                        DLog(@"%@", self.accessibilityHint);
            self.highlightedImage = [UIImage PTTImageNamed:self.accessibilityHint];
        }
    }
}
@end
