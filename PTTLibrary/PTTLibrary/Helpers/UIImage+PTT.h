//
//  UIImage+Custom.h
//  MyLibrary
//
//  Created by Admin on 7/30/15.
//
//

#import <UIKit/UIKit.h>

@interface UIImage (Custom)
+ (UIImage *)PTTImageNamed:(NSString *)name;
@end
