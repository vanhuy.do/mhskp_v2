//
//  UIImage+Custom.m
//  MyLibrary
//
//  Created by Admin on 7/30/15.
//
//

#import "UIImage+PTT.h"
#import "Utility.h"

@implementation UIImage (Custom)
+ (UIImage *)PTTImageNamed:(NSString *)name{
    NSString *filePath = [[Utility getPttLibraryBundle] pathForResource:name ofType:@"png"];
    return [UIImage imageWithContentsOfFile:filePath];
}
@end
