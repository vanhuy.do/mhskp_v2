//
//  Utility.h
//  PTTLibrary
//
//  Created by Admin on 7/31/15.
//  Copyright (c) 2015 Tuan Nguyen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "MQTTManager.h"

@interface Utility : NSObject

//+ (MainViewController*)getMainViewController;
+ (NSBundle*) getPttLibraryBundle;
+ (NSString*)getDeviceUUID;
+ (NSMutableDictionary*)parseJsonString:(NSString*)JSON;
/**
 *  Parse NSDictionary or Array to JSON string
 *
 *  @param object only support NSDictionary or Array
 *
 *  @return String format JSON
 */
+ (NSString*)parseObjectToJson:(id)object;
+ (NSString *) encryptUseDES:(NSString *)plainText;
+ (NSString *)decryptUseDES:(NSString *)cipherText;
+ (NSString *)base64StringFromFileAtPath:(NSString*)filePath;
+ (NSData*)dataFrom64String:(NSString*)stringEncodedWithBase64;
@end
