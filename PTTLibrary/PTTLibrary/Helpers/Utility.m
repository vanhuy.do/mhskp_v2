//
//  Utility.m
//  PTTLibrary
//
//  Created by Admin on 7/31/15.
//  Copyright (c) 2015 Tuan Nguyen. All rights reserved.
//

#import "Utility.h"
#import "Base64.h"
#import "CommonCrypto/CommonDigest.h"
#import "CommonCrypto/CommonCryptor.h"
#import <Foundation/NSUUID.h>
#include <CoreFoundation/CoreFoundation.h>
#include <Security/Security.h>

@implementation Utility
const NSString *key = @"fcs@2oi5";
const NSString *iv = @"fcs@ptt.";
///**
// *  get Main ViewController
// *
// *  @return UIViewController of main viewcontroller
// */
//+ (MainViewController*)getMainViewController{
//    
//    
//    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Storyboard" bundle:[NSBundle bundleForClass:[self class]]];
//    MainViewController *viewController = [sb instantiateViewControllerWithIdentifier:@"MainViewController"];
//    return viewController;
//}

/**
 *  get Bundle of Library
 *
 *  @return NSBundle of library
 */
+ (NSBundle*) getPttLibraryBundle{
    return [NSBundle bundleForClass:[self class]];
}

/**
 *  Get UUID of device
 *
 *  @return NSString of UUID
 */
+ (NSString*)getDeviceUUID{
    
    NSUUID *deviceId;
    if(TARGET_IPHONE_SIMULATOR)
        return @"E621E1F8-C36C-495A-93FC-0C247A3E6E5F";
    else
        deviceId = [UIDevice currentDevice].identifierForVendor;
    return deviceId.UUIDString;
}

+ (NSMutableDictionary*)parseJsonString:(NSString*)JSON{
    NSString *newString = [NSString stringWithFormat:@"%@",JSON];
    
    NSLog(@"response: %@", JSON);
    NSData* data = [newString dataUsingEncoding:NSUTF8StringEncoding];
    NSError* error;
    //    id jsonObjects = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    
    if (error){
        NSLog(@"error is %@", [error localizedDescription]);
        return nil;
    }
    return [dic mutableCopy];
}
+ (NSString*)parseObjectToJson:(id)object{
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:object
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        return jsonString;
    }
    return @"";
}
+(NSString *) encryptUseDES:(NSString *)plainText
{
    NSData* ivData = [iv dataUsingEncoding: NSUTF8StringEncoding];
    Byte *ivBytes = (Byte *)[ivData bytes];
    NSString *ciphertext = nil;
    NSData *textData = [plainText dataUsingEncoding:NSUTF8StringEncoding];
    NSUInteger dataLength = [textData length];
    unsigned char buffer[1024];
    memset(buffer, 0, sizeof(char));
    size_t numBytesEncrypted = 0;
    CCCryptorStatus cryptStatus = CCCrypt(kCCEncrypt, kCCAlgorithmDES,
                                          kCCOptionPKCS7Padding,
                                          [key UTF8String], kCCKeySizeDES,
                                          ivBytes,
                                          [textData bytes], dataLength,
                                          buffer, 1024,
                                          &numBytesEncrypted);
    if (cryptStatus == kCCSuccess) {
        NSData *data = [NSData dataWithBytes:buffer length:(NSUInteger)numBytesEncrypted];
        if ([data respondsToSelector:@selector(base64EncodedStringWithOptions:)]) {
            ciphertext = [data base64EncodedStringWithOptions:kNilOptions];  // iOS 7+
        } else {
            ciphertext = [data base64Encoding];                              // pre iOS7
        }
    }
    return ciphertext;
}

+(NSString *)decryptUseDES:(NSString *)cipherText
{
    NSData* ivData = [iv dataUsingEncoding: NSUTF8StringEncoding];
    Byte *ivBytes = (Byte *)[ivData bytes];
    NSString *plaintext = nil;
    NSData *cipherdata;
    if ([NSData instancesRespondToSelector:@selector(initWithBase64EncodedString:options:)]) {
        cipherdata = [[NSData alloc] initWithBase64EncodedString:cipherText options:0];  // iOS 7+
    } else {
        cipherdata = [[NSData alloc] initWithBase64Encoding:cipherText];                           // pre iOS7
    }
    
//    NSData *cipherdata = [[NSData alloc]
//                          initWithBase64EncodedString:cipherText options:0];//[Base64 decode:cipherText];
    unsigned char buffer[1024];
    memset(buffer, 0, sizeof(char));
    size_t numBytesDecrypted = 0;
    CCCryptorStatus cryptStatus = CCCrypt(kCCDecrypt, kCCAlgorithmDES,
                                          kCCOptionPKCS7Padding,
                                          [key UTF8String], kCCKeySizeDES,
                                          ivBytes,
                                          [cipherdata bytes], [cipherdata length],
                                          buffer, 1024,
                                          &numBytesDecrypted);
    if(cryptStatus == kCCSuccess) {
        NSData *plaindata = [NSData dataWithBytes:buffer length:(NSUInteger)numBytesDecrypted];
        plaintext = [[NSString alloc]initWithData:plaindata encoding:NSUTF8StringEncoding];
    }
    return plaintext;
}
+ (NSString *)base64StringFromFileAtPath:(NSString*)filePath {
    NSData * dataFromFile = [NSData dataWithContentsOfFile:filePath];
    return [dataFromFile base64Encoding];
}

+ (NSData*)dataFrom64String:(NSString*)stringEncodedWithBase64 {
    NSData *dataFromBase64 = [[NSData alloc] initWithBase64Encoding:stringEncodedWithBase64];
    return dataFromBase64;
}
@end
