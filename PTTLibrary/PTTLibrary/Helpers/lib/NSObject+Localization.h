//
//  NSObject+Localization.h
//  PTTLibrary
//
//  Created by phu the cong on 8/12/15.
//  Copyright (c) 2015 FCS Computer Systems. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (Localization)
- (void)localizeFromNib;

@end
