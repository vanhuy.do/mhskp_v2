//
//  NSObject+Localization.m
//  PTTLibrary
//
//  Created by phu the cong on 8/12/15.
//  Copyright (c) 2015 FCS Computer Systems. All rights reserved.
//

#import "NSObject+Localization.h"
#import <objc/runtime.h>

@implementation NSObject (Localization)
//By default do nothing when localizing
- (void)localizeFromNib {
}


#pragma mark - Method swizzling

+ (void)load {
    Method awakeFromNibOriginal = class_getInstanceMethod(self, @selector(awakeFromNib));
    Method awakeFromNibCustom = class_getInstanceMethod(self, @selector(awakeFromNibCustom));
    
    //Swizzle methods
    method_exchangeImplementations(awakeFromNibOriginal, awakeFromNibCustom);
}

- (void)awakeFromNibCustom {
    //Call standard methods
    [self awakeFromNibCustom];
    
    //Localize
    [self localizeFromNib];
}
@end
