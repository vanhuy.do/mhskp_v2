//
//  UIBarButtonItem+Localization.m
//  PTTLibrary
//
//  Created by phu the cong on 8/12/15.
//  Copyright (c) 2015 FCS Computer Systems. All rights reserved.
//

#import "UIBarButtonItem+Localization.h"

@implementation UIBarButtonItem (Localization)
- (void)localizeFromNib {
    //Replace text with localizable version
    if (self.title.length > 0) {
        self.title = [[PTTLanguageManager sharedLanguageManager] getTranslationForKey:self.title];
    }
}
@end
