//
//  UIButton+Localization.m
//  PTTLibrary
//
//  Created by phu the cong on 8/12/15.
//  Copyright (c) 2015 FCS Computer Systems. All rights reserved.
//

#import "UIButton+Localization.h"

@implementation UIButton (Localization)
- (void)localizeFromNib {
    //Replace text with localizable version
    NSArray *states = @[@(UIControlStateNormal), @(UIControlStateHighlighted), @(UIControlStateDisabled), @(UIControlStateSelected), @(UIControlStateApplication)];
    for (NSNumber *state in states) {
        NSString *title = [self titleForState:state.integerValue];
        if (title.length > 0) {
            [self setTitle:[[PTTLanguageManager sharedLanguageManager] getTranslationForKey:title] forState:state.integerValue];
        }
    }
}
@end
