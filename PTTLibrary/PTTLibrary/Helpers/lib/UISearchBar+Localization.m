//
//  UISearchBar+Localization.m
//  PTTLibrary
//
//  Created by phu the cong on 8/12/15.
//  Copyright (c) 2015 FCS Computer Systems. All rights reserved.
//

#import "UISearchBar+Localization.h"

@implementation UISearchBar (Localization)
- (void)localizeFromNib {
    //Replace text with localizable version
    if (self.text.length > 0) {
        self.text = [[PTTLanguageManager sharedLanguageManager] getTranslationForKey:self.text];
    }
    if (self.placeholder.length > 0) {
        self.placeholder = [[PTTLanguageManager sharedLanguageManager] getTranslationForKey:self.placeholder];
    }
    if (self.prompt.length > 0) {
        self.prompt = [[PTTLanguageManager sharedLanguageManager] getTranslationForKey:self.prompt];
    }
}
@end
