//
//  MQTTBase.h
//  PTTLibrary
//
//  Created by Admin on 8/20/15.
//  Copyright (c) 2015 FCS Computer Systems. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "MQTTClient.h" // MQTT
#import "MQTTAsync.h"  // MQTT
//
typedef enum
{
    MQTT_STATE_DISCONNECTED = 1,
    MQTT_STATE_CONNECTING,
    MQTT_STATE_CONNECTED,
    MQTT_STATE_RECONNECTED,
} MQTT_STATE;

@interface MQTTBase : NSObject{
    NSString* mqttClientID, *mqttAddress;
}

@property (unsafe_unretained,nonatomic) MQTTAsync mqttClient;
@property (strong, nonatomic) NSMutableArray *mqttArrayChannel;
@property int connectState;

- (void)reConnectMQTT;
void mqttConnectionSucceeded(void* context, MQTTAsync_successData* response);
void mqttConnectionFailed(void* context, MQTTAsync_failureData* response);
void mqttConnectionLost(void* context, char* cause);
void mqttSubscriptionSucceeded(void* context, MQTTAsync_successData* response);
void mqttSubscriptionFailed(void* context, MQTTAsync_failureData* response);
int mqttMessageArrived(void* context, char* topicName, int topicLen, MQTTAsync_message* message);
void mqttUnsubscriptionSucceeded(void* context, MQTTAsync_successData* response);
void mqttUnsubscriptionFailed(void* context, MQTTAsync_failureData* response);
void mqttPublishSucceeded(void* context, MQTTAsync_successData* response);
void mqttPublishFailed(void* context, MQTTAsync_failureData* response);
void mqttDisconnectionSucceeded(void* context, MQTTAsync_successData* response);
void mqttDisconnectionFailed(void* context, MQTTAsync_failureData* response);
void mqttDestroy(void* context);
+ (NSString*)checkErrorChar:(NSString*)textContent;
@end
