//
//  MQTTBase.m
//  PTTLibrary
//
//  Created by Admin on 8/20/15.
//  Copyright (c) 2015 FCS Computer Systems. All rights reserved.
//

#import "MQTTBase.h"
#import "macro.h"
#import "MQTTConstant.h"
#import "Utility.h"
#import "ServerPath.h"
#import "AppKey.h"
#import "GlobalObject.h"
#import "LanguageManager.h"
#import "MQTTHandleMessage.h"


@implementation MQTTBase

#pragma mark - MQTT Cal#pragma mark - MQTT Callback
- (void)reConnectMQTT{
    if ([MQTTManager instance].mqttClient==NULL) {
        [[MQTTManager instance] registerMQTT];
        return;
    }
    [MQTTManager instance].connectState = MQTT_STATE_RECONNECTED;
    mqttDestroy((__bridge void*)[MQTTManager instance]);
    
    
}
void mqttConnectionSucceeded(void* context, MQTTAsync_successData* response)
{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        DLog("MQTT connection to broker succeeded.\n");
        NotifPost(MQTT_CONNECT_SUCCESS);
        //send disconnect in case login duplicate
        [MQTTHandleMessage sendDisconnectMessage];
        MQTTManager *mqttManager = [MQTTManager instance];
        [mqttManager subscribeChannel];
        mqttManager.connectState = MQTT_STATE_CONNECTED;
    });
    
}

void mqttConnectionFailed(void* context, MQTTAsync_failureData* response)
{
    NotifPost(MQTT_CONNECT_FAIL);
    [MQTTManager instance].connectState = MQTT_STATE_DISCONNECTED;
    DLog("MQTT connection to broker failed.\n");
    mqttDestroy(context);
}

void mqttConnectionLost(void* context, char* cause)
{
    NotifPost(MQTT_CONNECTION_LOST);
    [MQTTManager instance].connectState = MQTT_STATE_DISCONNECTED;
    DLog("MQTT connection was lost with cause: %s\n", cause);
    [[MQTTManager instance] reConnectMQTT];
}

void mqttSubscriptionSucceeded(void* context, MQTTAsync_successData* response)
{
//    NotifPost(MQTT_SUBSCRIBE_SUCCESS);
    dispatch_async(dispatch_get_main_queue(), ^{
        //NSString *textContent = [NSString stringWithFormat:@"%s", response->alt.pub.message.payload];
        //DLog(@"MQTT subscription succeeded to topic %@", textContent);
    });
}

void mqttSubscriptionFailed(void* context, MQTTAsync_failureData* response)
{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        NotifPost(MQTT_SUBSCRIBE_FAIL);
        DLog("MQTT subscription failed to topic");
    });
}
+ (NSString*)checkErrorChar:(NSString*)textContent{
    if ([textContent rangeOfString:@"}"].location != NSNotFound) {
        
        int resultIndex = textContent.length;
        for (int i=textContent.length-1; i >= 0; i--) {
            char character = [textContent characterAtIndex:i];
            if (character == '}') {
                resultIndex = i;
                break;
            }
        }
        if (resultIndex != textContent.length -1) {
            textContent = [textContent substringToIndex:resultIndex+1];
        }
        
    }
    return textContent;
}
int mqttMessageArrived(void* context, char* topicName, int topicLen, MQTTAsync_message* message)
{
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString *textContent = [NSString stringWithFormat:@"%s", message->payload];
        int lenMsg = message->payloadlen;
        if (lenMsg < textContent.length) {
            textContent = [textContent substringToIndex:lenMsg];
        }
        NotifPost2Obj(MQTT_MESSAGE_ARRIVED, textContent);
//        DLog("MQTT message arrived from topic: %s with body: %@\n", topicName, textContent);
        NSArray *listItems = [textContent componentsSeparatedByString:@"|"];
        if (listItems && listItems.count > 1 && [[listItems objectAtIndex:0] isEqualToString:SIP_MSG]) {
            [MQTTHandleMessage handleSIPMessage:listItems];
        }else{
            [MQTTHandleMessage handleMessage:textContent];
        }
    });
    return true;
}


void mqttUnsubscriptionSucceeded(void* context, MQTTAsync_successData* response)
{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        NotifPost(MQTT_UNSUBSCRIBE_SUCCESS);
        DLog("MQTT unsubscription succeeded.\n");
    });
}

void mqttUnsubscriptionFailed(void* context, MQTTAsync_failureData* response)
{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        NotifPost(MQTT_UNSUBSCRIBE_FAIL);
        DLog("MQTT unsubscription failed.\n");
    });
}

void mqttPublishSucceeded(void* context, MQTTAsync_successData* response)
{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        NotifPost(MQTT_PUBLISH_SUCCESS);
        DLog("MQTT publish message succeeded.\n");
    });
}

void mqttPublishFailed(void* context, MQTTAsync_failureData* response)
{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        NotifPost(MQTT_PUBLISH_FAIL);
        DLog("MQTT publish message failed.\n");
        [[MQTTManager instance] reConnectMQTT];
        
    });
}

void mqttDisconnectionSucceeded(void* context, MQTTAsync_successData* response)
{
    NotifPost(MQTT_DISCONNECT_SUCCESS);
    DLog("MQTT disconnection succeeded.\n");
    mqttDestroy(context);
}

void mqttDisconnectionFailed(void* context, MQTTAsync_failureData* response)
{
    NotifPost(MQTT_DISCONNECT_FAIL);
    DLog("MQTT disconnection failed.\n");
    mqttDestroy(context);
}
void mqttDestroy(void* context)
{
    dispatch_async(dispatch_get_main_queue(), ^{
        BOOL isReconnect = [MQTTManager instance].connectState == MQTT_STATE_RECONNECTED ? YES : NO;
        DLog("MQTT handler destroyed.\n");
        // When MQTT start, MQTT Manager will handle one mqttClient
        MQTTManager* strongSelf = (__bridge __weak MQTTManager*)context;
        if (!strongSelf) {
            return;
        }
        strongSelf.connectState = MQTT_STATE_DISCONNECTED;
        MQTTAsync mqttClient = strongSelf.mqttClient;
        // Remove mqttClient when destroy
        MQTTAsync_destroy(&mqttClient);
        strongSelf.mqttClient = NULL;
        if (isReconnect) {
            [[MQTTManager instance] registerMQTT];
        }
    });
}



@end
