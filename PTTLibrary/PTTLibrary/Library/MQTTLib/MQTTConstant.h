//
//  MQTTConstant.h
//  PTTLibrary
//
//  Created by Admin on 8/11/15.
//  Copyright (c) 2015 FCS Computer Systems. All rights reserved.
//

#ifndef PTTLibrary_MQTTConstant_h
#define PTTLibrary_MQTTConstant_h

#define NOTIFY_UPDATE_CHANNEL       2
#define NOTIFY_CHANGE_STATUS        3
#define NOTIFY_INVITE_CALL          4
#define NOTIFY_JOIN_CALL            5
#define NOTIFY_UNJON_CALL           6
#define NOTIFY_ASSIGN               7
#define NOTIFY_UNASSIGN             8
#define NOTIFY_DELETEGROUP          9
#define NOTIFY_USER_IN_LIST         10
#define NOTIFY_USER_LOGIN_DUPLICATE 11

#define MQTT_CONNECT_SUCCESS            @"MQTT_CONNECT_SUCCESS"
#define MQTT_CONNECT_FAIL               @"MQTT_CONNECT_FAIL"
#define MQTT_SUBSCRIBE_SUCCESS          @"MQTT_SUBSCRIBE_SUCCESS"
#define MQTT_SUBSCRIBE_FAIL             @"MQTT_SUBSCRIBE_FAIL"
#define MQTT_PUBLISH_SUCCESS            @"MQTT_PUBLISH_SUCCESS"
#define MQTT_PUBLISH_FAIL               @"MQTT_PUBLISH_FAIL"
#define MQTT_CONNECTION_LOST            @"MQTT_CONNECTION_LOST"
#define MQTT_MESSAGE_ARRIVED            @"MQTT_MESSAGE_ARRIVED"
#define MQTT_UNSUBSCRIBE_SUCCESS        @"MQTT_UNSUBSCRIBE_SUCCESS"
#define MQTT_UNSUBSCRIBE_FAIL           @"MQTT_UNSUBSCRIBE_FAIL"
#define MQTT_DISCONNECT_SUCCESS         @"MQTT_DISCONNECT_SUCCESS"
#define MQTT_DISCONNECT_FAIL            @"MQTT_DISCONNECT_FAIL"
#define MQTT_NOTIFY_CALL_ACCEPT         @"5"
#define MQTT_NOTIFY_CALL_INVITE         @"4"
#define MQTT_NOTIFY_CALL_JOIN           @"3"
#define SIP_MSG                         @"SIP_MSG"

#define MQTT_UPDATE_LIST_GROUP          @"MQTT_UPDATE_LIST_GROUP"
#define MQTT_UPDATE_LIST_CONTACT        @"MQTT_UPDATE_LIST_CONTACT"
#define MQTT_UPDATE_LIST_USER_INCALL    @"MQTT_UPDATE_LIST_USER_INCALL"
#define MQTT_MAKE_CALL_USER_USER        @"MQTT_MAKE_CALL_USER_USER"
#define MQTT_UNASSIGN_USER              @"MQTT_UNASSIGN_USER"
#define MQTT_LOGIN_DUPLICATE            @"MQTT_LOGIN_DUPLICATE"
#define MQTT_GROUP_DELETE               @"MQTT_GROUP_DELETE"
#define MQTT_CALL_MESSAGE               @"MQTT_CALL_MESSAGE"
#define MQTT_MAKE_CALL                  @"MQTT_MAKE_CALL"
#define MQTT_SHOW_MESSAGE               @"MQTT_SHOW_MESSAGE"
#define PTT_STATE                       @"PTT_STATE"



#define SIP_SERVER_MQTT_TOPIC           @"PTT_SIPMESSAGE"
#define MSG_REQUEST_PERMISSION_TO_SPEAK @"REQUEST"
#define MSG_PERMISSION_SPEAK_GRANTED    @"OK"
#define MSG_PERMISSION_SPEAK_DECLINED   @"WAIT"
#define MSG_SESSION_END                 @"END"
#define MSG_OK                          @"OK"
#define MSG_SESSION_TIMEOUT             @"TIMEOUT"
#define MSG_SESSION_PING                @"PING"
#define MSG_SESSION_DISCONNECT          @"DISCONNECT"
#define MSG_SESSION_END_CALL            @"END_CALL"
#define MSG_SESSION_RESET               @"RESET"
#define MSG_SESSION_END_CALL            @"END_CALL"
#define MSG_SESSION_NO_JOIN_CALL        @"NO_JOIN_CALL"
#define ECHO_RECORD                     @"ECHO_RECORD"
#define ECHO_PLAY                       @"ECHO_PLAY"
#define MESSAGE_PART_COUNT              4
#endif
