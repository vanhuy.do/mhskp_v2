//
//  MQTTHandleMessage.h
//  PTTLibrary
//
//  Created by Admin on 8/20/15.
//  Copyright (c) 2015 FCS Computer Systems. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MQTTHandleMessage : NSObject

/**
 *  Handle SIP message from MQTT
 *
 *  @param arrayMessage info of message
 */
+ (void)handleSIPMessage:(NSString*)message;

/**
 *  Handle nomal message from MQTT
 *
 *  @param message info of message
 */
+ (void)handleMessage:(NSString*)message;


+ (void)handleUpdateChannel:(NSMutableDictionary*)jsonObj;
+ (void)handleChangeStatus:(NSMutableDictionary*)jsonObj;
+ (void)notifyUserForIncomingCall:(NSString*)strCaller withCallerId:(NSString*)strCallerID withCallGroup:(NSString*)strCallGroup withTo:(NSString*)strTo isSound:(BOOL)isSound isViberate:(BOOL)isViberate isCallUser:(BOOL)isCallUser withUserInfo:(NSDictionary*)dataInfo;
+ (void)handleCallInvite:(NSMutableDictionary*)jsonObj;
+ (void)handleCallJoin:(NSMutableDictionary*)jsonObj;
+ (void)handleUnassign;
+ (void)handleDeleteGroup:(NSMutableDictionary*)jsonObj;
+ (void)handleUserList:(NSMutableDictionary*)jsonObj;
+ (void)handleLoginDuplicate:(NSMutableDictionary*)jsonObj;

+ (void)showNotifycation:(NSString*)strBody withData:(NSDictionary*)data isSound:(BOOL)isSound isVibrate:(BOOL)isVibrate;
+ (void)sendDisconnectMessage;

@end
