//
//  MQTTHandleMessage.m
//  PTTLibrary
//
//  Created by Admin on 8/20/15.
//  Copyright (c) 2015 FCS Computer Systems. All rights reserved.
//

#import "MQTTHandleMessage.h"

#import "macro.h"
#import "MQTTConstant.h"
#import "Utility.h"
#import "ServerPath.h"
#import "AppKey.h"
#import "GlobalObject.h"
#import "LanguageManager.h"
#import <AudioToolbox/AudioServices.h>
#import "SettingConstants.h"
#import "MQTTMessageUtils.h"
#import "CallInfo.h"
#import "HandleCall.h"

@implementation MQTTHandleMessage


#pragma mark - HANDLE MESSAGE ARRIVE

+ (void)handleSIPMessage:(NSString*)message{
    //
    NSArray *split = [MQTTMessageUtils getMessageParts:message];
    NSString *action = [MQTTMessageUtils getAction:split];
    NSString *status = [MQTTMessageUtils getStatus:split];
    
    if ([action isEqualToString:MSG_JOIN]) {
        if ([status isEqualToString:STATUS_OK]) {
            HandleCall *handleCall = [HandleCall instance];
            NSString *strUserId = [NSString stringWithFormat:@"%@", [MQTTMessageUtils getUserId:split]];
            NSString *callToUser = [NSString stringWithFormat:@"%@", [GlobalObject instance].callToUserID];
            if ([GlobalObject instance].callToUserID != nil && [callToUser isEqualToString:strUserId] && ![strUserId isEqualToString:[GlobalObject instance].generalData.currentUser.userId]){
                NSDictionary *info = [[NSDictionary alloc] initWithObjects:@[[MQTTMessageUtils getGroupId:split], split[split.count - 1]] forKeys:@[KEY_GROUP_ID, KEY_GROUP_NAME]];
                NotifPost2Obj(MAKE_CALL_INFO, info);
                return;
            }
            
            if (handleCall.mCall.state == CALL_STATE_INVITED) {
                handleCall.mCall.sequence = [split[split.count - 2] intValue];
                handleCall.mCall.groupId = [[MQTTMessageUtils getGroupId:split] intValue];
                handleCall.mCall.groupName = split[split.count - 1];
                handleCall.mCall.userId = [MQTTMessageUtils getUserId:split];
                handleCall.mCall.state = CALL_STATE_JOINED;
                handleCall.mCall.audio_seq = [split[split.count - 2] intValue];
                handleCall.mCall.pre_audio_seq = 0;
                [handleCall removeAllSound];
                NotifPost(MAKE_CALL_SUCCESS);
            }
        } else {
            NotifPost(NOTI_HIDE_DIALOG);
            NotifPost(LINE_BUSY);
        }
    } else if ([action isEqualToString:MSG_END_CALL]) {
        [[HandleCall instance] stopRrecord];
    } else if ([action isEqualToString:MSG_REQUEST]) {
        if ([status isEqualToString:STATUS_OK]) {
            NotifPost2Obj(MSG_OK_TO_TALK, split[split.count - 1]);
            
        } else if ([status isEqualToString:STATUS_SPEAKER_BUSY]) {
            NotifPost(MSG_WAIT_FOR_SPEAKER);
            
        }
    } else if ([action isEqualToString:MSG_UNJOIN]){
        if ([status isEqualToString:STATUS_OK]){
            NotifPost(MSG_UNJOIN_SUCCESS);
            NotifPost(DISCONNECT_SUCCESS);
        } else {
            NotifPost(MSG_UNJOIN_FAILED);
            NotifPost(DISCONNECT_SUCCESS);
        }
    } else if ([action isEqualToString:MSG_AUDIO]){
        [[HandleCall instance] handleAudioReceived:split[4] withS:split[split.count - 1] withSSEQ:split[6] withMaxSEQ:split[7]];
    } else if ([action isEqualToString:MSG_NEW_SEQ]){
        
        if (split.count > 2)
            [[HandleCall instance] handleNewSeqReceived:split];
        else {
        }
    } else if ([action isEqualToString:MSG_ECHO]){
        if ([status isEqualToString:STATUS_OK]){
            NotifPost(MSG_ECHO_OK);
        } else {
            
        }
    } else {
        // do not implement
    }
    NotifPost(MSG_PRESENT_CALL_STATE);
    
//    NotifPost2Obj(MQTT_CALL_MESSAGE, arrayMessage);
    
}
+ (void)handleMessage:(NSString*)message{
    NSMutableDictionary *jsonObj = [Utility parseJsonString:message];
    if (jsonObj != nil) {
        
        NSString *notificationTypeStr = [jsonObj objectForKey:NOTIFICATION_TYPE];
        
        switch (notificationTypeStr.integerValue) {
            case NOTIFY_UPDATE_CHANNEL:{
                [[self class] handleUpdateChannel:jsonObj];
                break;
            }case NOTIFY_CHANGE_STATUS:{
                
                [[self class] handleChangeStatus:jsonObj];
                break;
            }case NOTIFY_INVITE_CALL:{
                [[self class] handleCallInvite:jsonObj];
                break;
            }case NOTIFY_JOIN_CALL:
            case NOTIFY_UNJON_CALL:{
                [[self class] handleCallJoin:jsonObj];
                break;
            }case NOTIFY_UNASSIGN:{
                [[self class] handleUnassign];
                break;
            }case NOTIFY_DELETEGROUP:{
                [[self class] handleDeleteGroup:jsonObj];
                break;
            }case NOTIFY_USER_IN_LIST:{
                [[self class] handleUserList:jsonObj];
                break;
            }case NOTIFY_USER_LOGIN_DUPLICATE:{
                [[self class] handleLoginDuplicate:jsonObj];
                break;
            }
            default:
                break;
        }
    }
    
}


+ (void)handleUpdateChannel:(NSMutableDictionary*)jsonObj{
    NotifPost2Obj(MQTT_UPDATE_LIST_GROUP,jsonObj);
    //TODO: update flow when unassign group if user in call screen go back previous creen
    
}
+ (void)handleChangeStatus:(NSMutableDictionary*)jsonObj{
    if ([jsonObj objectForKey:KEY_DATA] == nil) {
        return;
    }
    NSMutableDictionary *jsonData   = [jsonObj objectForKey:KEY_DATA];
    NSString* strUserId             = [jsonData objectForKey:KEY_USER_ID];
    NSString* strStatus             = [jsonData objectForKey:KEY_STATUS];
    NSMutableDictionary *resultData = [[NSMutableDictionary alloc] init];
//    NSString *strChannel            = [jsonObj objectForKey:KEY_CHANNEL];
    
    [resultData setObject:strUserId forKey:KEY_USER_ID];
    [resultData setObject:strStatus forKey:KEY_STATUS];
    NotifPost2Obj(MQTT_UPDATE_LIST_CONTACT, resultData);
}
+ (void)notifyUserForIncomingCall:(NSString*)strCaller withCallerId:(NSString*)strCallerID withCallGroup:(NSString*)strCallGroup withTo:(NSString*)strTo isSound:(BOOL)isSound isViberate:(BOOL)isViberate isCallUser:(BOOL)isCallUser withUserInfo:(NSDictionary*)dataInfo{
    NSString *bodyMessage = [NSString stringWithFormat:@"%@ %@", [[PTTLanguageManager sharedLanguageManager] getTranslationForKey:INCOMMING_CALL_MESSAGE], strCaller];
    if (isCallUser) {
        [GlobalObject instance].nextCallToUserID = strCallerID;
        [self showNotifycation:bodyMessage withData:dataInfo isSound:GET_SETTING_NOTIFY_CALL_USER_SOUND isVibrate:GET_SETTING_NOTIFY_CALL_USER_VIBRATE];
    }else{
        [GlobalObject instance].nextCallToUserID = @"-1";
        [self showNotifycation:bodyMessage withData:dataInfo isSound:GET_SETTING_NOTIFY_CALL_GROUP_SOUND isVibrate:GET_SETTING_NOTIFY_CALL_GROUP_VIBRATE];
    }
}
+ (void)handleCallInvite:(NSMutableDictionary*)jsonObj{
    if ([jsonObj objectForKey:KEY_DATA] == nil) {
        return;
    }
    NSMutableDictionary *jsonData    = [jsonObj objectForKey:KEY_DATA];
    NSString *strTo = [jsonData objectForKey:KEY_TO];
    NSString *strCaller = [jsonData objectForKey:KEY_CALLER];
    NSString *strCallGroup = [jsonData objectForKey:KEY_CALL_GROUP];
    
    if ([jsonData objectForKey:KEY_GROUP_TYPE]) {
        NSString *currentId = [NSString stringWithFormat:@"%@", [GlobalObject instance].generalData.currentUser.userId];
        NSString *callerID = [NSString stringWithFormat:@"%@", jsonData[@"callerId"]];
        if ([jsonData objectForKey:KEY_TO] && strTo.integerValue != [GlobalObject instance].groupID && ![callerID isEqualToString:currentId]) {
            [[self class] notifyUserForIncomingCall:strCaller withCallerId:callerID withCallGroup:strCallGroup withTo:strTo isSound:YES isViberate:YES isCallUser:NO withUserInfo:jsonData];
        }
    }else{
        NSString *strCallerId = [NSString stringWithFormat:@"%@", [jsonData objectForKey:KEY_CALLER_ID]];
        if ([jsonData objectForKey:KEY_CALLER_ID]
            && ![strCallerId isEqualToString:[GlobalObject instance].generalData.currentUser.userId]
            && [jsonData objectForKey:KEY_TO]
            && strTo.integerValue != [GlobalObject instance].groupID) {
            [[MQTTManager instance] subscribeChannel:[NSString stringWithFormat:@"g%@", strTo]];
            [[self class] notifyUserForIncomingCall:strCaller withCallerId:strCallerId withCallGroup:strCallGroup withTo:strTo isSound:YES isViberate:YES isCallUser:YES withUserInfo:jsonData];
        }else{
            NSMutableDictionary *dictMessage = [[NSMutableDictionary alloc] init];
            [dictMessage setObject:MQTT_NOTIFY_CALL_ACCEPT forKey:NOTIFICATION_TYPE];
            [dictMessage setObject:[NSString stringWithFormat:@"u%@", strCallerId] forKey:KEY_CHANNEL];
            NSMutableDictionary *dictDataMessage = [[NSMutableDictionary alloc] init];
            [dictDataMessage setObject:[GlobalObject instance].generalData.currentUser.nickName forKey:KEY_NICK_NAME];
            [dictDataMessage setObject:[GlobalObject instance].generalData.currentUser.userId forKey:KEY_USER_ID];
            [dictMessage setObject:dictDataMessage forKey:KEY_DATA];
            NSString* messagepublish = [Utility parseObjectToJson:dictMessage];
            NSString *channelPublish = [NSString stringWithFormat:@"u%@", strCallerId];
            [[MQTTManager instance] publishMessage:channelPublish withMessage:messagepublish];
        }
    }
}
+ (void)handleCallJoin:(NSMutableDictionary*)jsonObj{
    NotifPost(MQTT_UPDATE_LIST_USER_INCALL);
    if ([jsonObj objectForKey:KEY_DATA]) {
        NSMutableDictionary *jsonData   = [jsonObj objectForKey:KEY_DATA];
        NSString* strUserId             = [jsonData objectForKey:KEY_USER_ID];
        NSString* channelInfo           = [jsonObj objectForKey:KEY_CHANNEL];
//        NSMutableDictionary *resultData = [[NSMutableDictionary alloc] init];
//        [resultData setObject:strUserId forKey:KEY_USER_ID];
//        [resultData setObject:channelInfo forKey:KEY_CHANNEL];
        NSString *callToUserID = [GlobalObject instance].callToUserID;
        NSString *channelTemp = [NSString stringWithFormat:@"u%@", [GlobalObject instance].generalData.currentUser.userId];
        if (callToUserID != nil && callToUserID.integerValue == strUserId.integerValue && [channelInfo isEqualToString:channelTemp]) {
            
            NotifPost(MQTT_MAKE_CALL_USER_USER);
        }
        
    }
}
+ (void)handleUnassign{
    NotifPost(MQTT_UNASSIGN_USER);
}
+ (void)handleDeleteGroup:(NSMutableDictionary*)jsonObj{
    NotifPost2Obj(MQTT_GROUP_DELETE, jsonObj);
}
+ (void)handleUserList:(NSMutableDictionary*)jsonObj{
    NotifPost2Obj(MQTT_UPDATE_LIST_USER_INCALL, jsonObj);
}
+ (void)handleLoginDuplicate:(NSMutableDictionary*)jsonObj{
    NSString *deviceID = [jsonObj objectForKey:KEY_DEVICE_ID];
    if ([deviceID isEqualToString:[GlobalObject instance].uniqueDeviceId]) {
        return;
    }
    NotifPost(MQTT_LOGIN_DUPLICATE);
}

+ (void)showNotifycation:(NSString*)strBody withData:(NSDictionary*)data isSound:(BOOL)isSound isVibrate:(BOOL)isVibrate{
//    AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);
//    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
    UILocalNotification* localNotification = [[UILocalNotification alloc] init];
    localNotification.fireDate = [NSDate date];
    localNotification.alertBody = strBody;
    localNotification.timeZone = [NSTimeZone defaultTimeZone];
    if (isSound) {
        NotifPost(@"PLAY_CALL_SOUND");
//        localNotification.soundName = @"sound.caf";
    }
    localNotification.alertAction = @"answer ...";
    localNotification.userInfo = data;
    localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
    
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
}


+ (void)sendDisconnectMessage{
    NSMutableDictionary *dictSendDisconnect = [[NSMutableDictionary alloc] init];
    [dictSendDisconnect setObject:[NSNumber numberWithInt:NOTIFY_USER_LOGIN_DUPLICATE] forKey:NOTIFICATION_TYPE];
    [dictSendDisconnect setObject:[Utility getDeviceUUID] forKey:KEY_DEVICE_ID];
    
    NSString* messagepublish = [Utility parseObjectToJson:dictSendDisconnect];
    NSString *channelPublish = [NSString stringWithFormat:@"u%@", [GlobalObject instance].generalData.currentUser.userId];
    
    [[MQTTManager instance] publishMessage:channelPublish withMessage:messagepublish];
}
@end
