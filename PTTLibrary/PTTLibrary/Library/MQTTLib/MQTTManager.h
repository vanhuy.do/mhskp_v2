//
//  MQTTManager.h
//  MQTTExample
//
//  Created by Admin on 7/28/15.
//  Copyright (c) 2015 Tuan Nguyen. All rights reserved.
//

#import <Foundation/Foundation.h>
@class MQTTClient;
@interface MQTTManager : NSObject{

}
@property (nonatomic, strong) MQTTClient *clientMQTT;
@property (strong, nonatomic) NSMutableArray *mqttArrayChannel;
+ (MQTTManager *)instance;
//@property (unsafe_unretained,nonatomic) MQTTAsync mqttClient;
//@property (strong, nonatomic) NSMutableArray *mqttArrayChannel;
//@property int connectState;
/**
 *  register MQTT with default address
 *
 */
- (void)registerMQTT;

/**
 *  Subcribe all channel
 */
- (void)subscribeChannel;

/**
 *  subcribe a channel
 *
 *  @param channel channel name
 */
- (void)subscribeChannel:(NSString*)channel;

/**
 *  unsubcribe a channel
 *
 *  @param channel channel name
 */
- (void)unsubscribeChannel:(NSString*)channel;

/**
 *  publish a message
 *
 *  @param channel    channel want to publish
 *  @param messageStr message of publish
 */
- (void)publishMessage:(NSString*)channel withMessage:(NSString*)messageStr;



/**
 *  Send message in call
 *
 *  @param messageType type of message
 */
- (void)sendMessageInCall:(NSString*)messageType;

/**
 *  send notify when end message
 */
- (void)notifyGroupOnSendingEndMessage;

/**
 *  Send request talk
 */
- (void)sendRequestTalk;

/**
 *  send request end talk
 */
- (void)sendEndTalk;

/**
 *  Send request call to another user
 *
 *  @param groupName  Group Name
 *  @param groupID    Group ID
 *  @param userCallID call to userID
 */
- (void)sendInviteCall:(NSString*)groupName withGroupID:(NSString*)groupID withUserCall:(NSString*)userCallID;
/**
 *  Unregister MQTT when destroy
 */
- (void)unregisterMQTT;

/**
 *  Ping MQTT every time
 */
- (void)pingToMQTT;

- (void)unsubscribeChannel;
- (void)mqttReleaseSpeaker;
- (void)mqttRequestSpeaker;
@end
