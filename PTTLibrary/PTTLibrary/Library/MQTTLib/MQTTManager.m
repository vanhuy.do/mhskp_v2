//
//  MQTTManager.m
//  MQTTExample
//
//  Created by Admin on 7/28/15.
//  Copyright (c) 2015 Tuan Nguyen. All rights reserved.
//

#import "MQTTManager.h"
#import "macro.h"
#import "MQTTConstant.h"
#import "Utility.h"
#import "ServerPath.h"
#import "AppKey.h"
#import "GlobalObject.h"
#import "LanguageManager.h"
#import "AFNetworking.h"
#import "MQTTHandleMessage.h"
#import "MQTTKit/MQTTKit.h"
#import "HandleCall.h"

@implementation MQTTManager


#define QOS 0
#define CALL_TYPE_GROUP 0
#define CALL_TYPE_USER  1
#pragma mark - Synthesize Variable

#pragma mark - Singleton

+ (MQTTManager *)instance
{
    __strong static MQTTManager *_sharedLocalSystem = nil;
    
    static dispatch_once_t pred = 0;
    dispatch_once(&pred, ^{
        _sharedLocalSystem = [[self alloc] init];
    });
    return _sharedLocalSystem;
}
#pragma mark - public function

/**
 *  register MQTT with default address
 *
 *  @param address  address of MQTT server
 *  @param clientID client id when register
 */
- (void)registerMQTT{
    
    UIApplication *application = [UIApplication sharedApplication];
    if (self.clientMQTT && !self.clientMQTT.connected) {
        [self.clientMQTT destroyMQTT];
        self.clientMQTT = nil;
    }
    NSString *clientID = [self getClientID];
    self.clientMQTT = [[MQTTClient alloc] initWithClientId:clientID];
    self.clientMQTT.keepAlive = 60;
    
//    __weak MQTTManager *weakSelf = self;
    [self.clientMQTT setMessageHandler:^(MQTTMessage *message) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSString* textContent = [[NSString alloc] initWithData:message.payload encoding:NSUTF8StringEncoding];
            NSInteger lenMsg = message.payload.length;
            if (lenMsg < textContent.length) {
                textContent = [textContent substringToIndex:lenMsg];
            }
            //[weakSelf sendNotification:[NSString stringWithFormat:@"received:%@", textContent]];
            DLog(@"received :%@", textContent);
            NotifPost2Obj(MQTT_MESSAGE_ARRIVED, textContent);
            //DLog("MQTT message arrived from topic: %s with body: %@\n", topicName, textContent);
            NSArray *listItems = [textContent componentsSeparatedByString:@"|"];
            if (listItems && listItems.count > 1) {
                [MQTTHandleMessage handleSIPMessage:textContent];
            }else{
                [MQTTHandleMessage handleMessage:textContent];
            }
        });
        
    }];
    
    // connect the MQTT client
    NSString *mqttPort = [NSString stringWithFormat:@"%@", GET_MQTT_PORT];
    NSString *strPort = [[NSString alloc] initWithString:mqttPort];
    [self.clientMQTT connectToHost:GET_SER_IP toPort:strPort completionHandler:^(MQTTConnectionReturnCode code) {
        if (code == ConnectionAccepted) {
            DLog("MQTT connection to broker succeeded.\n");
            NotifPost(MQTT_CONNECT_SUCCESS);
            
            // The client is connected when this completion handler is called
            DLog(@"client is connected with id %@", clientID);
//            [MQTTHandleMessage sendDisconnectMessage];
            // Subscribe to the topic
            [self subscribeChannel];
            
            //send disconnect in case login duplicate
//
        } else {
            DLog(@"Failed to connect to server...");
            NotifPost(MQTT_CONNECT_FAIL);
            DLog("MQTT connection to broker failed.\n");
            
            [self.clientMQTT destroyMQTT];
            [self registerMQTT];
        }
    }];
    
//    [self.clientMQTT disconnectWithCompletionHandler:^(NSUInteger code) {
//        // The client is disconnected when this completion handler is called
//        DLog(@"MQTT client is disconnected");
//        NotifPost(MQTT_CONNECTION_LOST);
//        [[MQTTManager instance] registerMQTT];
//    }];
    
    [application setKeepAliveTimeout:600 handler:^{
        if (!self.clientMQTT.connected) {
            [self registerMQTT];
        }
    }];
    
}

/**
 *  Unregister MQTT
 */
- (void)unregisterMQTT
{
    [self.clientMQTT destroyMQTT];
    self.clientMQTT = nil;
}
/**
 *  Subcribe all channel
 */
- (void)subscribeChannel{
    
    if (self.clientMQTT == nil || self.clientMQTT.connected == NO) {
        [[MQTTManager instance] registerMQTT];
        return;
    }
    
    if (!self.mqttArrayChannel.count) { DLog("You need to write a subscription topic.\n"); return; }
    
    [self.clientMQTT subscribes:self.mqttArrayChannel withCompletionHandler:^(NSArray *grantedQos) {
        // The client is effectively subscribed to the topic when this completion handler is called
        NSLog(@"subscribed to topic %@", self.mqttArrayChannel);
        BOOL res = [self.clientMQTT enableBackgrounding];
        
        if (!res) {
            NSLog(@"Failed to enable background socket...");
        }
        NotifPost(MQTT_SUBSCRIBE_SUCCESS);
        
    }];
}
/**
 *  subcribe a channel
 *
 *  @param channel channel name
 */
- (void)subscribeChannel:(NSString*)channel{
    if (channel == nil) {
        return;
    }
    if (self.clientMQTT == nil || self.clientMQTT.connected == NO) {
        [[MQTTManager instance] registerMQTT];
        return;
    }
    
    if ([self.mqttArrayChannel containsObject:channel]) {
        return;
    }
    [self.mqttArrayChannel addObject:channel];
    
    [self.clientMQTT subscribe:channel withCompletionHandler:^(NSArray *grantedQos) {
        // The client is effectively subscribed to the topic when this completion handler is called
        NSLog(@"subscribed to topic %@", self.mqttArrayChannel);
        BOOL res = [self.clientMQTT enableBackgrounding];
        
        if (!res) {
            NSLog(@"Failed to enable background socket...");
        }
        NotifPost(MQTT_SUBSCRIBE_SUCCESS);
        /*
         dispatch_async(dispatch_get_main_queue(), ^{
         NotifPost(MQTT_SUBSCRIBE_FAIL);
         DLog("MQTT subscription failed to topic");
         });
         */
    }];

    
}
- (void)unsubscribeChannel{
    
    if (self.clientMQTT == nil || self.clientMQTT.connected == NO) {
        return;
    }
    
    if (!self.mqttArrayChannel.count) { DLog("You need to write a subscription topic.\n"); return; }
    [self.clientMQTT unsubscribes:self.mqttArrayChannel withCompletionHandler:^{
        NotifPost(MQTT_UNSUBSCRIBE_SUCCESS);
        DLog("MQTT unsubscription succeeded.\n");
        
        /*
         dispatch_async(dispatch_get_main_queue(), ^{
         NotifPost(MQTT_UNSUBSCRIBE_FAIL);
         DLog("MQTT unsubscription failed.\n");
         });
         */
    }];
    
}
/**
 *  unsubcribe a channel
 *
 *  @param channel channel name
 */
- (void)unsubscribeChannel:(NSString*)channel{
    
    if (self.clientMQTT == nil || self.clientMQTT.connected == NO) {
        return;
    }
    
    if (!self.mqttArrayChannel.count) { DLog("You need to write a subscription topic.\n"); return; }
    [self.clientMQTT unsubscribe:channel withCompletionHandler:^{
        
        NotifPost(MQTT_UNSUBSCRIBE_SUCCESS);
        DLog("MQTT unsubscription succeeded.\n");
        
        /*
        dispatch_async(dispatch_get_main_queue(), ^{
            NotifPost(MQTT_UNSUBSCRIBE_FAIL);
            DLog("MQTT unsubscription failed.\n");
        });
         */
        
    }];
    
}

/**
 *  publish a message
 *
 *  @param channel    channel want to publish
 *  @param messageStr message of publish
 */
- (void)publishMessage:(NSString*)channel withMessage:(NSString*)messageStr{
    DLog(@"Publish Message : %@",messageStr);
    if (self.clientMQTT == nil || self.clientMQTT.connected == NO) {
        [[MQTTManager instance] registerMQTT];
        return;
    }
    if (!channel.length) { DLog("You need to write a publish topic.\n"); return; }
    if (!messageStr.length) { DLog("You need to to write a message to be published.\n"); return; }
    [self.clientMQTT publishString:messageStr toTopic:channel withQos:QOS retain:YES completionHandler:^(int mid) {
        
        NotifPost(MQTT_PUBLISH_SUCCESS);
        DLog("MQTT publish message succeeded.\n");
        /*
         NotifPost(MQTT_PUBLISH_FAIL);
         dispatch_async(dispatch_get_main_queue(), ^{
         
         DLog("MQTT publish message failed.\n");
         [[MQTTManager instance] reConnectMQTT];
         
         });
         */
    }];
    
}
- (void)mqttReleaseSpeaker{
    
    HandleCall *handleCall = [HandleCall instance];
    if (handleCall.mCall.recordId) {
        NSString *message = [MQTTMessageUtils createReleaseSpeakerMessage:handleCall.mCall.groupId withGroupId:handleCall.mCall.groupId withUserId:handleCall.mCall.userId withDeviceId:handleCall.mCall.deviceID withRecordId:handleCall.mCall.recordId];
        [self publishMessage:PTT_SIP_TOPIC withMessage:message];
    }
}
- (void)sendMessageInCall:(NSString*)messageType{
    HandleCall *handleCall = [HandleCall instance];
    NSString *requestSpeaker = [MQTTMessageUtils createRequestSpeakerMessage:handleCall.mCall.groupId withGroupId:handleCall.mCall.groupId withUserId:handleCall.mCall.userId withDeviceId:handleCall.mCall.deviceID];
    [self publishMessage:PTT_SIP_TOPIC withMessage:requestSpeaker];
}
- (void)sendMessageEndToGroup{
    GlobalObject *globalObject = [GlobalObject instance];
    
        NSString *message = [NSString stringWithFormat:@"%@|%d|%@|%@|%@", SIP_MSG, globalObject.groupID, globalObject.generalData.currentUser.userId, MSG_SESSION_END_CALL, globalObject.uniqueDeviceId];
    NSString *stringChannel = [NSString stringWithFormat:@"g%d", globalObject.groupID];
    [self publishMessage:stringChannel withMessage:message];
}
- (void)sendRequestTalk{
    if ([GlobalObject instance].isEchoTest) {
        [[HandleCall instance] startRecord];
//        [self sendMessageInCall:ECHO_RECORD];
    }else{
        [self mqttRequestSpeaker];
    }
    
}
- (void)mqttRequestSpeaker{
    HandleCall *handleCall = [HandleCall instance];
    NSString *requestSpeaker = [MQTTMessageUtils createRequestSpeakerMessage:handleCall.mCall.groupId withGroupId:handleCall.mCall.groupId withUserId:handleCall.mCall.userId withDeviceId:handleCall.mCall.deviceID];
    [self publishMessage:PTT_SIP_TOPIC withMessage:requestSpeaker];
}
- (void)sendEndEchoTalk{
    [self sendMessageInCall:ECHO_PLAY];
}
- (void)sendEndTalk{
    if ([GlobalObject instance].isEchoTest) {
        [self performSelector:@selector(sendEndEchoTalk) withObject:nil afterDelay:2.0];
    }else{
        [[HandleCall instance] muteMicrophone];
        [self performSelector:@selector(sendEndMessageTalk) withObject:nil afterDelay:2.0];
        
    }
}
- (void)sendEndMessageTalk{
    [self sendMessageInCall:MSG_SESSION_END];
    [self sendMessageEndToGroup];
}
- (void)sendInviteCall:(NSString*)groupName withGroupID:(NSString*)groupID withUserCall:(NSString*)userCallID{

    NSMutableDictionary *dictMessage = [[NSMutableDictionary alloc] init];
    [dictMessage setObject:MQTT_NOTIFY_CALL_INVITE forKey:NOTIFICATION_TYPE];
    [dictMessage setObject:groupName forKey:KEY_CHANNEL];
    NSMutableDictionary *dictDataMessage = [[NSMutableDictionary alloc] init];
    [dictDataMessage setObject:groupID forKey:KEY_TO];
    [dictDataMessage setObject:[GlobalObject instance].generalData.currentUser.userName forKey:KEY_CALLER];
    [dictDataMessage setObject:groupName forKey:KEY_CALL_GROUP];
    [dictDataMessage setObject:[GlobalObject instance].generalData.currentUser.userId forKey:KEY_CALLER_ID];
    [dictDataMessage setObject:MQTT_NOTIFY_CALL_JOIN forKey:NOTIFICATION_TYPE];
    [dictMessage setObject:dictDataMessage forKey:KEY_DATA];

    NSString* messagepublish = [Utility parseObjectToJson:dictMessage];
    NSString *channelPublish = [NSString stringWithFormat:@"u%@", userCallID];
    
    [[MQTTManager instance] publishMessage:channelPublish withMessage:messagepublish];

}

- (void)notifyGroupOnSendingEndMessage{
    NSString *message = [NSString stringWithFormat:@"%@|%d|%@|%@|%@", SIP_MSG, [GlobalObject instance].groupID, [GlobalObject instance].generalData.currentUser.userId, MSG_SESSION_END_CALL, [GlobalObject instance].uniqueDeviceId];
    [self publishMessage:[NSString stringWithFormat:@"g%d", [GlobalObject instance].groupID] withMessage:message];
}

- (void)pingToMQTT{
    GlobalObject *glbObject=  [GlobalObject instance];
    if (glbObject.uniqueDeviceId != nil
        && glbObject.uniqueDeviceId.length > 0){
//        && mActiveCall != null && mActiveCall.isActive() && !mActiveCall.isAfterEnded()) {
        NSString *message = [NSString stringWithFormat:@"PTT_STATE1|ping|%d|%@|%@", glbObject.groupID, glbObject.generalData.currentUser.userId, glbObject.uniqueDeviceId];
        DLog(@"Ping Server: %@", message);
        [self publishMessage:PTT_STATE withMessage:message];
    }
}
#pragma mark - MQTT private fuction

/**
 *  get client ID
 *
 *  @return NSString client ID
 */
- (NSString*)getClientID{
    
    int timeNowMilisecond = Date2Int(DateNow);
    NSString *result = [NSString stringWithFormat:@"%d%@", timeNowMilisecond, [Utility getDeviceUUID]];
    result = [result substringWithRange:NSMakeRange(0, 23)];
    return result;
}

@end
