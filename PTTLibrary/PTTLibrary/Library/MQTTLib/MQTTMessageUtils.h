//
//  MQTTMessageUtils.h
//  PTTLibrary
//
//  Created by Nguyen Minh Tuan on 6/3/16.
//  Copyright © 2016 FCS Computer Systems. All rights reserved.
//

#import <Foundation/Foundation.h>

#define PTT_SIP_TOPIC @"PTT_STATE1"
#define STATUS_OK @"200"
#define MSG_JOIN @"join"
#define MSG_UNJOIN @"unjoin"
#define MSG_END_CALL @"endcall"
#define MSG_REQUEST @"request"
#define MSG_OK_TO_TALK @"okToTalk"
#define MSG_WAIT_FOR_SPEAKER @"waitForSpeaker"
#define MSG_JOIN_SUCCESS @"joinGroupSucceed"
#define MSG_UNJOIN_SUCCESS @"unjoinGroupSucceed"
#define MSG_UNJOIN_FAILED @"unjoinFailed"
#define MSG_AUDIO @"audio"
#define MSG_NEW_SEQ @"newseq"
#define MSG_ECHO @"echo"
#define MSG_ECHO_OK @"echoOK"
#define MSG_PRESENT_CALL_STATE @"presentCallState"
#define MSG_HANDLE_NEW_SEQ_RECEIVED @"handleNewSeqReceived"
#define MSG_HANDLE_AUDIO_RECEIVED @"handleAudioReceived"
#define NOTI_HIDE_DIALOG @"hideConnectingDialog"
#define NOTI_SHOW_DIALOG @"showConnectingDialog"
#define STATUS_JOINED_ALREADY @"201"
#define STATUS_SPEAKER_BUSY @"202"
#define INVALID_SESSION -1
#define JOIN_CLIENT @"PTT_STATE1|join|%d|%@|%@"
#define REQUEST_SPEAKER_CLIENT @"PTT_STATE1|request|%d|%d|%@|%@"
#define RELEASE_SPEAKER_CLIENT @"PTT_STATE1|endcall|%d|%d|%@|%@|%@"
#define UNJOIN_CLIENT @"PTT_STATE1|unjoin|%d|%d|%@|%@"
#define AUDIO @"PTT_STATE1|audio|%d|%d|%@|%@|%d|%d|%@"
#define DOWNLOAD @"PTT_STATE1|download|%d|%d|%@|%@|%d|%d"
#define UPLOAD @"PTT_STATE1|upload|%d|%d|%@|%@|%ld|%ld|%d|%@|%@"
#define ECHO @"PTT_STATE1|echo|%@"
#define UPLOAD_PART @"PTT_STATE1|upload_partial|%d|%d|%@|%@|%ld|%ld|%d|%@|%d|%d|%@"

@interface MQTTMessageUtils : NSObject

+ (NSString*)createJoinMessage:(int)groupId withUserId:(NSString*)userId withDeviceId:(NSString*)deviceId;
+ (NSString*)createRequestSpeakerMessage:(int)sessionId withGroupId:(int)groupId withUserId:(NSString*)userId withDeviceId:(NSString*)deviceId;
+ (NSString*)createReleaseSpeakerMessage:(int)sessionId withGroupId:(int)groupId withUserId:(NSString*)userId withDeviceId:(NSString*)deviceId withRecordId:(NSString*)recordId;
+ (NSString*)createUnjoinMessage:(int)sessionId withGroupId:(int)groupId withUserId:(NSString*)userId withDeviceId:(NSString*)deviceId;
+ (NSString*)createDownloadMessage:(int)sessionId withGroupId:(int)groupId withUserId:(NSString*)userId withDeviceId:(NSString*)deviceId withSEQ:(int)seq withTimeout:(int)timeout;
+ (NSString*)createUploadMessage:(int)sessionId withGroupId:(int)groupId withUserId:(NSString*)userId withDeviceId:(NSString*)deviceId withStartTime:(long)startTime withEndTime:(long)endTime withSize:(int)sizeKb withRecordId:(NSString*)recordId withAudio:(NSString*)audio;
+ (NSString*)createUploadMessage:(int)sessionId withGroupId:(int)groupId withUserId:(NSString*)userId withDeviceId:(NSString*)deviceId withStartTime:(long)startTime withEndTime:(long)endTime withSize:(int)sizeKb withRecordId:(NSString*)recordId withOffset:(int)offset withNumOfParts:(int)numOfParts withAudio:(NSString*)audio;
+ (NSDictionary*)createUploadParams:(int)sessionId withGroupId:(int)groupId withUserId:(NSString*)userId withDeviceId:(NSString*)deviceId withStartTime:(long)startTime withEndTime:(long)endTime withSize:(int)sizeKb withRecordId:(NSString*)recordId withAudio:(NSString*)audio;
+ (NSArray*)getMessageParts:(NSString*) response;
+ (NSString*)getAction:(NSArray*)parts;
+ (NSString*)getStatus:(NSArray*)parts;
+ (NSString*)getSessionId:(NSArray*)parts;
+ (NSString*)getDeviceId:(NSArray*)parts;
+ (NSString*)getGroupId:(NSArray*)parts;
+ (NSString*)getUserId:(NSArray*)parts;
+ (NSString*)getStatusByString:(NSString*)response;
+ (int)getSessionIdByString:(NSString*)response;
+ (NSString*)parseJsonObject:(int)notiType withChannel:(int)channel withCallGroup:(NSString*)callGroup withCallId:(int)callId;
+ (NSString*)createAudioMessage:(int)sessionId withGroupId:(int)groupId withUserId:(NSString*)userId withDeviceId:(NSString*)deviceId withSEQ:(int)seq withMaxSEQ:(int)maxseq withMessagePayload:(NSString*)messagePayLoad;
+ (NSString*)parseAudioMessage:(NSString*)data;
+ (NSString*)createEchoMessage:(NSString*) userId;

@end
