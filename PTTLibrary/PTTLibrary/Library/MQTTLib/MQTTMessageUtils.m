//
//  MQTTMessageUtils.m
//  PTTLibrary
//
//  Created by Nguyen Minh Tuan on 6/3/16.
//  Copyright © 2016 FCS Computer Systems. All rights reserved.
//

#import "MQTTMessageUtils.h"
#import "Utility.h"

@implementation MQTTMessageUtils



+ (NSString*)createJoinMessage:(int)groupId withUserId:(NSString*)userId withDeviceId:(NSString*)deviceId{
    return [NSString stringWithFormat:JOIN_CLIENT, groupId, userId, deviceId];
}

+ (NSString*)createRequestSpeakerMessage:(int)sessionId withGroupId:(int)groupId withUserId:(NSString*)userId withDeviceId:(NSString*)deviceId{
    return [NSString stringWithFormat:REQUEST_SPEAKER_CLIENT, sessionId, groupId, userId, deviceId];
}

+ (NSString*)createReleaseSpeakerMessage:(int)sessionId withGroupId:(int)groupId withUserId:(NSString*)userId withDeviceId:(NSString*)deviceId withRecordId:(NSString*)recordId{
    return [NSString stringWithFormat:RELEASE_SPEAKER_CLIENT, sessionId, groupId, userId, deviceId, recordId];
}

+ (NSString*)createUnjoinMessage:(int)sessionId withGroupId:(int)groupId withUserId:(NSString*)userId withDeviceId:(NSString*)deviceId{
    return [NSString stringWithFormat:UNJOIN_CLIENT, sessionId, groupId, userId, deviceId];
}

+ (NSString*)createDownloadMessage:(int)sessionId withGroupId:(int)groupId withUserId:(NSString*)userId withDeviceId:(NSString*)deviceId withSEQ:(int)seq withTimeout:(int)timeout{
    return [NSString stringWithFormat:DOWNLOAD, sessionId, groupId, userId, deviceId, seq, timeout];
}

+ (NSString*)createUploadMessage:(int)sessionId withGroupId:(int)groupId withUserId:(NSString*)userId withDeviceId:(NSString*)deviceId withStartTime:(long)startTime withEndTime:(long)endTime withSize:(int)sizeKb withRecordId:(NSString*)recordId withAudio:(NSString*)audio{
    return [NSString stringWithFormat:UPLOAD, sessionId, groupId, userId, deviceId, startTime, endTime, sizeKb, recordId, audio];
}

+ (NSString*)createUploadMessage:(int)sessionId withGroupId:(int)groupId withUserId:(NSString*)userId withDeviceId:(NSString*)deviceId withStartTime:(long)startTime withEndTime:(long)endTime withSize:(int)sizeKb withRecordId:(NSString*)recordId withOffset:(int)offset withNumOfParts:(int)numOfParts withAudio:(NSString*)audio{
    return [NSString stringWithFormat:UPLOAD_PART, sessionId, groupId, userId, deviceId, startTime, endTime, sizeKb, recordId, offset, numOfParts, audio];
}

+ (NSDictionary*)createUploadParams:(int)sessionId withGroupId:(int)groupId withUserId:(NSString*)userId withDeviceId:(NSString*)deviceId withStartTime:(long)startTime withEndTime:(long)endTime withSize:(int)sizeKb withRecordId:(NSString*)recordId withAudio:(NSString*)audio{
    
    
    return [[NSDictionary alloc] initWithObjects:@[[NSNumber numberWithInt:groupId],
                                                   deviceId,
                                                   audio,
                                                   [NSNumber numberWithLong:startTime],
                                                   [NSNumber numberWithLong:endTime],
                                                   [NSNumber numberWithInt:sizeKb],
                                                   recordId,
                                                   userId]
                                         forKeys:@[@"groupId",
                                                   @"deviceId",
                                                   @"audio",
                                                   @"startTime",
                                                   @"endTime",
                                                   @"sizeKb",
                                                   @"recordId",
                                                   @"userId"]];
    
}

+ (NSArray*)getMessageParts:(NSString*) response {
    return [response componentsSeparatedByString:@"|"];
}

+ (NSString*)getAction:(NSArray*)parts {
    if (parts.count >= 2) {
        return parts[1];
    }
    return @"";
}

+ (NSString*)getStatus:(NSArray*)parts{
    if (parts.count >= 3) {
        return parts[2];
    }
    return @"";
}

+ (NSString*)getSessionId:(NSArray*)parts{
    if (parts.count >= 4) {
        return parts[3];
    }
    return @"";
}

+ (NSString*)getDeviceId:(NSArray*)parts {
    if (parts.count >= 7) {
        return parts[6];
    }
    return @"";
}

+ (NSString*)getGroupId:(NSArray*)parts {
    if (parts.count >= 5) {
        return parts[4];
    }
    return @"";
}

+ (NSString*)getUserId:(NSArray*)parts {
    if (parts.count >= 6) {
        return parts[5];
    }
    return @"";
}

+ (NSString*)getStatusByString:(NSString*)response {
    NSArray *parts = [MQTTMessageUtils getMessageParts:response];
    return [MQTTMessageUtils getStatus:parts];
}


+ (int)getSessionIdByString:(NSString*)response {
    NSArray *parts = [MQTTMessageUtils getMessageParts:response];
    NSString *sesId = [MQTTMessageUtils getSessionId:parts];
    int sessionId = INVALID_SESSION;
    //TODO: need check sesId is an integer
    sessionId = [sesId intValue];
    return sessionId;
}
+ (NSString*)getUserId{
    return @"";
}
+ (NSString*)parseJsonObject:(int)notiType withChannel:(int)channel withCallGroup:(NSString*)callGroup withCallId:(int)callId{
    NSString *message = @"";
    NSDictionary *data = [[NSDictionary alloc] initWithObjects:@[[NSNumber numberWithInt:channel],
                                                                 [[self class] getUserId],
                                                                 @1,
                                                                 callGroup,
                                                                 [NSNumber numberWithInt:callId]]
                                                       forKeys:@[@"to",
                                                                 @"caller",
                                                                 @"groupType",
                                                                 @"callGroup",
                                                                 @"callerId"]];
    NSDictionary *object = [[NSDictionary alloc] initWithObjects:@[[NSNumber numberWithInt:notiType],
                                                                   [NSString stringWithFormat:@"g%d", channel],
                                                                   data]
                                                         forKeys:@[@"notificationType",
                                                                   @"channel",
                                                                   @"data"]];
    message = [Utility parseObjectToJson:object];
    return message;
    
}

+ (NSString*)createAudioMessage:(int)sessionId withGroupId:(int)groupId withUserId:(NSString*)userId withDeviceId:(NSString*)deviceId withSEQ:(int)seq withMaxSEQ:(int)maxseq withMessagePayload:(NSString*)messagePayLoad {
    return [NSString stringWithFormat:AUDIO, sessionId, groupId, userId, deviceId, seq, maxseq, messagePayLoad];
}

+ (NSString*)parseAudioMessage:(NSString*)data {
    NSDictionary *jsonObject = [Utility parseJsonString:data];
    return jsonObject[@"audio"];
    
}

+ (NSString*)createEchoMessage:(NSString*) userId {
    return [NSString stringWithFormat:ECHO, userId];
}
@end
