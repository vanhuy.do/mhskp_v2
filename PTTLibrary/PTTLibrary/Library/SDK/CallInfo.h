//
//  CallInfo.h
//  PTTLibrary
//
//  Created by Nguyen Minh Tuan on 6/6/16.
//  Copyright © 2016 FCS Computer Systems. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef enum
{
    CALL_STATE_NONE = 0,
    CALL_STATE_INVITED = 1,
    CALL_STATE_JOINED = 2,
    CALL_STATE_HOLD = 5
} CALL_STATE_RF;
@interface CallInfo : NSObject
@property int sessionId, groupId, sequence, state, stateBeforeHolding, audio_seq, pre_audio_seq;
@property (nonatomic, strong) NSString *userId, *groupName, *deviceID, *recordId;
- (instancetype)init:(int)sessionId withGroupId:(int)groupId withSequence:(int)sequence withState:(int)state;
- (void)hold;
- (void)resume;
- (BOOL)isHeld;
@end
