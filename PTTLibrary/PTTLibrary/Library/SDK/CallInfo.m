//
//  CallInfo.m
//  PTTLibrary
//
//  Created by Nguyen Minh Tuan on 6/6/16.
//  Copyright © 2016 FCS Computer Systems. All rights reserved.
//

#import "CallInfo.h"

@implementation CallInfo
- (instancetype)init:(int)sessionId withGroupId:(int)groupId withSequence:(int)sequence withState:(int)state{
    self = [super init];
    if (self) {
        self.sessionId = sessionId;
        self.groupId = groupId;
        self.sequence = sequence;
        self.state = state;
        self.audio_seq = 0;
        self.pre_audio_seq = 0;
    }
    return self;
}
- (void)hold{
    self.stateBeforeHolding = self.state;
    self.state = CALL_STATE_HOLD;
}
- (void)resume{
    self.state = self.stateBeforeHolding;
}
- (BOOL)isHeld{
    return self.state == CALL_STATE_HOLD;
}
@end
