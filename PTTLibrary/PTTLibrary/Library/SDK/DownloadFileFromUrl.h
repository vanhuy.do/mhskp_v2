//
//  DownloadFileFromUrl.h
//  PTTLibrary
//
//  Created by Nguyen Minh Tuan on 6/9/16.
//  Copyright © 2016 FCS Computer Systems. All rights reserved.
//

#import <Foundation/Foundation.h>
//#define URL_FORMAT "http://%s:%s%s"

@interface DownloadFileFromUrl : NSOperation
- (instancetype)init:(NSString*)_server withPort:(NSString*)_port withDir:(NSString*)_dir withTalker:(NSString*)_talker withUrl:(NSString*)_url withSEQ:(int)_seq withMaxSEQ:(int)_maxSeq;
@property (nonatomic, strong) NSBlockOperation *blockCompletionOperation;
@end
