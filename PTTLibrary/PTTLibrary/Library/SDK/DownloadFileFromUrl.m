//
//  DownloadFileFromUrl.m
//  PTTLibrary
//
//  Created by Nguyen Minh Tuan on 6/9/16.
//  Copyright © 2016 FCS Computer Systems. All rights reserved.
//

#import "DownloadFileFromUrl.h"

@implementation DownloadFileFromUrl{
    
    NSString* receiveDir;
    NSString* server;
    NSString* port;
    NSString* talker;
    NSString* link;
    int seq, maxSeq;
    
}
@synthesize blockCompletionOperation;
- (instancetype)init:(NSString*)_server withPort:(NSString*)_port withDir:(NSString*)_dir withTalker:(NSString*)_talker withUrl:(NSString*)_url withSEQ:(int)_seq withMaxSEQ:(int)_maxSeq{
    self = [super init];
    if (self) {
        receiveDir = _dir;
        server = _server;
        port = _port;
        talker = _talker;
        link = _url;
        seq = _seq;
        maxSeq = _maxSeq;
        blockCompletionOperation = [NSBlockOperation blockOperationWithBlock:^{
            //This is the completion block that will get called when the custom operation work is completed.
            NSLog(@"Do Something here. Probably alert the user that the work is complete");
        }];
    }
    return self;
}
@end
