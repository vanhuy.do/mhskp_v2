//
//  HandleCall.h
//  PTTLibrary
//
//  Created by Nguyen Minh Tuan on 6/6/16.
//  Copyright © 2016 FCS Computer Systems. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import "GlobalObject.h"
#import "MQTTMessageUtils.h"
#import "CallInfo.h"
#import "SoundPlayerService.h"

@interface HandleCall : NSObject
//@property (strong, nonatomic) NSThread *currentThreat;
@property (nonatomic) BOOL isRequestCall;
//@property int callState;
@property (nonatomic, strong) CallInfo *mCall;

/**
 * The callback for register pjsip account.
 */
typedef void (^RegisterCallBack)(BOOL success);


/**
 * Get the singleton XCPjsua.
 */
+ (HandleCall *)instance;

/**
 * Make VoIP call.
 *
 * @param destUri the uri of the receiver, something like "sip:192.168.43.106:5080"
 */
- (void)makeCallTo:(int)groupID;

/**
 * End ongoing VoIP calls
 */
- (void)endCall;

/**
 * End ongoing VoIP calls
 */
- (void)setRoutingType:(AVAudioSessionPortOverride)portOverride;
/**
 *  Unregister Pjsua when destroy or log out
 */
- (void)unregisterData;
- (void)removeAllSound;
- (void)shutdownSound;
- (void)restartSound;
- (void)setPJSipConfAudioId:(int)info;
- (void)muteMicrophone;
- (void)unmuteMicrophone;
- (void)startRecord;
- (void)stopRrecord;
- (void)handleNewSeqReceived:(NSArray*)split;
- (void)handleAudioReceived:(NSString*) talker withS:(NSString*)s withSSEQ:(NSString*)sseq withMaxSEQ:(NSString*)smaxSeq;
- (void)stopEchoRecord;
@end
