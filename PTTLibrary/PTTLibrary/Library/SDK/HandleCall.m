//
//  HandleCall.m
//  PTTLibrary
//
//  Created by Nguyen Minh Tuan on 6/6/16.
//  Copyright © 2016 FCS Computer Systems. All rights reserved.
//

#import "HandleCall.h"
#import "MQTTManager.h"
#import "VoiceRecordManager.h"
#import "Utility.h"
#import "DownloadFileFromUrl.h"
#import "ServerPath.h"
#import <PTTLibrary/AFNetworking.h>
#define PART_MAX_LENTH 6144.0f // 6 *1024
#define DOCUMENTS_FOLDER [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"]
#define URL_FORMAT @"http://%@:%@%@"
@implementation HandleCall{
    MQTTManager *mqttManager;
    GlobalObject *globalObject;
    VoiceRecordManager *voiceRecord;
    int mStateOfCall;
    int mSignalState;
    int playSoundWithin;
    NSOperationQueue *downloadAudioQueue;
    SoundPlayerService *audioService;
}
@synthesize mCall;
+ (HandleCall *)instance
{
    static HandleCall *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[HandleCall alloc] init];
        sharedInstance.isRequestCall = false;
        [sharedInstance initInfo];
        
    });
    return sharedInstance;
}
- (void)removeAllSound{
    [audioService removeAllSoundInQueue];
}
- (void)initInfo{
    mqttManager = [MQTTManager instance];
    globalObject = [GlobalObject instance];
    voiceRecord = [VoiceRecordManager instance];
    mStateOfCall = STATE_NONE;
    mSignalState = STATE_UNKNOWN_NETWORK;
    self.mCall = [[CallInfo alloc] init:-1 withGroupId:-1 withSequence:-1 withState:CALL_STATE_NONE];
    playSoundWithin = 5 * 60;
    downloadAudioQueue = [NSOperationQueue new];
    downloadAudioQueue.maxConcurrentOperationCount = 1;
    audioService = [SoundPlayerService instance];
//    NotifReg(self, @selector(pauseSound), @"PAUSE_SOUND");
    NotifReg(self, @selector(resumeSound), @"RESUME_SOUND");
}

- (void)pauseSound{
    [self.mCall hold];
    [audioService pauseAudio];
}
- (void)resumeSound{
    [self.mCall resume];
    [audioService playAudio];
}
- (void)setPJSipConfAudioId:(int)info{
//    pjsipConfAudioId = info;
}
- (void)muteMicrophone {
    //set mute microphone
}

- (void)unmuteMicrophone {
    //set unmute microphone
}

#pragma mark Private method

- (void)removeUnuseCodec{
    
}
- (void)unregisterData{
    //release all data
    [mqttManager unregisterMQTT];
}
- (void)shutdownSound{
    //shutdown sound
}
- (void)restartSound{
    //restart sound
}
- (void)startRecord{
    [voiceRecord startRecord];
}
- (void)stopEchoRecord{
    [voiceRecord stopRecord];
    [audioService addSound:[NSURL fileURLWithPath:voiceRecord.recorderFilePath]];
    [audioService playAudio];
}
- (void)stopRrecord{
    [voiceRecord stopRecord];
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
    dispatch_async(queue, ^{
        NSString *base64String = [Utility base64StringFromFileAtPath:voiceRecord.recorderFilePath];
        int fileSize = (int)([[[NSFileManager defaultManager] attributesOfItemAtPath:voiceRecord.recorderFilePath error:nil] fileSize]);
        dispatch_sync(dispatch_get_main_queue(), ^{
            NSInteger len = base64String.length;
            float parts = (len + PART_MAX_LENTH - 1 )/PART_MAX_LENTH ;
            int nparts = (int)parts;
            int offset = 0;
            int i = 0;
            while (i < nparts) {
                int toOffset = MIN(offset+PART_MAX_LENTH, len) == offset+PART_MAX_LENTH ? PART_MAX_LENTH : ((int)len - (int)offset);
                
                NSString *base64 = [base64String substringWithRange:NSMakeRange(offset, toOffset)];
                NSString *message = [MQTTMessageUtils createUploadMessage:[self.mCall.recordId intValue] withGroupId:self.mCall.groupId withUserId:self.mCall.userId withDeviceId:self.mCall.deviceID withStartTime:voiceRecord.startTime withEndTime:voiceRecord.endTime withSize:fileSize withRecordId:self.mCall.recordId withOffset:i withNumOfParts:nparts withAudio:base64];
                [[MQTTManager instance] publishMessage:PTT_SIP_TOPIC withMessage:message];
                offset += PART_MAX_LENTH;
                i++;
            }
        });
    });
}
- (void)makeCallTo:(int)groupID
{
    // stop recording if any
//    observer.stopWatching();
    [voiceRecord stopRecord];
    // stop playing sound and remove sound list if any
    // receivedMP4FileObserver.stopWatching();
//    emptySoundQueue();
    
    mStateOfCall = STATE_NONE;
    mSignalState = STATE_UNKNOWN_NETWORK;
//    if (mReleaseTalkCountDown != null)
//        mReleaseTalkCountDown.cancel();
//    firstSuccessClick = false;
    NotifPost(NOTI_SHOW_DIALOG);
    self.mCall.state = CALL_STATE_INVITED;
//    mConnectionCheckTimer.start();
//    mLastOperation = MAKE_CALL;
//    mCall.setState(CallInfo.CALL_STATE_INVITED);
    NSString *uuid = [[NSUUID UUID] UUIDString];
    NSString * timestamp = [NSString stringWithFormat:@"%d",(int)[[NSDate date] timeIntervalSince1970]];
    NSString *uniqueDeviceId = [NSString stringWithFormat:@"%@_%@",uuid,timestamp];
    [HandleCall instance].mCall.deviceID = uniqueDeviceId;
    [GlobalObject instance].uniqueDeviceId = uniqueDeviceId;
    NSString *message = [MQTTMessageUtils createJoinMessage:groupID withUserId:globalObject.generalData.currentUser.userId withDeviceId:globalObject.uniqueDeviceId];
    [mqttManager publishMessage:PTT_SIP_TOPIC withMessage:message];
}

- (void)endCall
{
    mCall.state = CALL_STATE_NONE;
    NSString *message = [MQTTMessageUtils createUnjoinMessage:globalObject.groupID withGroupId:globalObject.groupID withUserId:globalObject.generalData.currentUser.userId withDeviceId:globalObject.uniqueDeviceId];
    [mqttManager publishMessage:[NSString stringWithFormat:@"%d",globalObject.groupID] withMessage:message];
}


- (BOOL)isHeadsetPluggedIn
{
    AVAudioSessionRouteDescription *route = [[AVAudioSession sharedInstance] currentRoute];
    
    BOOL headphonesLocated = NO;
    for( AVAudioSessionPortDescription *portDescription in route.outputs )
    {
        headphonesLocated |= ( [portDescription.portType isEqualToString:AVAudioSessionPortHeadphones] );
    }
    return headphonesLocated;
}

#pragma mark -- Set routing type

- (void)setRoutingType:(AVAudioSessionPortOverride)portOverride{
    BOOL success;
    AVAudioSession *session = [AVAudioSession sharedInstance];
    NSError *error = nil;
    
    success = [session overrideOutputAudioPort:portOverride error:&error];
    if (!success) DLog(@"AVAudioSession error overrideOutputAudioPort: %@", [error localizedDescription]);
    
    success = [session setActive:YES error:&error];
    if (!success) DLog(@"AVAudioSession error setActive: %@", [error localizedDescription]);
}
- (void)handleNewSeqReceived:(NSArray*)split{
//    if (mCall.isHeld || voiceRecord.isRecording) {
//        DLog(@"handleNewSeqReceived isHeld :%d seq on server %@", mCall.audio_seq, split[6]);
//    }else{
//        int group = [split[3] intValue];
//        if (group == mCall.groupId) { // check for group
//            NSString *new_seq = split[6];
//            int seq = [new_seq intValue];
//            [self publishDownloadAudioMsg:seq];
//        }
//    }
    NotifPost(@"CHECK_CALL");
    int group = [split[3] intValue];
    if (group == mCall.groupId) { // check for group
        NSString *new_seq = split[6];
        int seq = [new_seq intValue];
        [self publishDownloadAudioMsg:seq];
    }
}
- (void)handleAudioReceived:(NSString*)talker withS:(NSString*)s withSSEQ:(NSString*)sseq withMaxSEQ:(NSString*)smaxSeq{
    
    int seq = [sseq intValue];
    int maxSeq = [smaxSeq intValue];
    DLog(@"handleAudioReceived seq : %d , audio_seq : %d , max: %d ", seq, mCall.audio_seq, maxSeq);
    if (mCall.audio_seq > seq || mCall.pre_audio_seq >= seq) {
        DLog("audio file sent by wrong order");
//        continueQueue();
        return;
    }
    if ([talker isEqualToString:mCall.userId]) {
        DLog(@"Same talker %@", talker);
        if (mCall.audio_seq < seq) {
            mCall.pre_audio_seq = mCall.audio_seq;
            mCall.audio_seq = seq;
            [self sendDownloadAudioMsg:(mCall.audio_seq + 1)];
        }
        return;
    }
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    NSString *url = [NSString stringWithFormat:URL_FORMAT, GET_SER_IP, DEFAULT_SER_PTT_PORT, s];
    NSURL *URL = [NSURL URLWithString:url];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    NSString *fileName = [NSString stringWithFormat:@"%ld_%@_%@_%@.mp4", [self getMilisecond], sseq, smaxSeq, talker];
    NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request progress:nil destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
        NSURL *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
        return [documentsDirectoryURL URLByAppendingPathComponent:fileName];
    } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
        NSString *fileURL = filePath.path;
        NSArray *split = [fileURL componentsSeparatedByString:@"_"];
        NSString *maxSeq = [split objectAtIndex:split.count-2];
        NSString *seq = [split objectAtIndex:split.count-3];
        [self setAudioSeq:[seq intValue] withMax:[maxSeq intValue]];
        if (![mCall isHeld]) {
            if (mStateOfCall == STATE_REQUESTING || mStateOfCall == STATE_REQUESTED_OK) {
                [audioService addSound:filePath];
            } else {
                [audioService addSound:filePath];
                [audioService playAudio];
            }
        }else if([mCall isHeld]){
            DLog(@"handleAudioReceived isHeld :%d seq on server %@", mCall.audio_seq, smaxSeq);
            [audioService addSound:filePath];
        }
        
        
    }];
    [downloadTask resume];
    
    /*
    taskQueue.add(new DownloadFileFromUrl(PreferencesUtil.getServerPTTURL(this),
                                          PreferencesUtil.getServerPTTPort(this),
                                          receiveDir,
                                          talker,s, seq, maxSeq, this));
    continueQueue();
     */
}
- (void)setAudioSeq:(int)seq withMax:(int)maxSeq{
    if(seq > mCall.audio_seq) {
        mCall.pre_audio_seq = mCall.audio_seq;
        mCall.audio_seq = seq;
        [self publishDownloadAudioMsg:maxSeq];
    }else{
//        continueQueue();
    }
}
- (long)getMilisecond{
    return ([NSDate timeIntervalSinceReferenceDate] * 1000);
}

- (void)publishDownloadAudioMsg:(int)seq{
    if (mCall.audio_seq < seq) {
        [self sendDownloadAudioMsg:mCall.audio_seq + 1];
    }
}
- (void)sendDownloadAudioMsg:(int)seq{
    NSString *payload = [MQTTMessageUtils createDownloadMessage:mCall.groupId withGroupId:mCall.groupId withUserId:mCall.userId withDeviceId:mCall.deviceID withSEQ:seq withTimeout:playSoundWithin];
    [mqttManager publishMessage:PTT_SIP_TOPIC withMessage:payload];
}
@end