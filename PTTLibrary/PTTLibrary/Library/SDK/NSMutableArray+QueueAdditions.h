//
//  NSMutableArray+QueueAdditions.h
//  PTTLibrary
//
//  Created by Nguyen Minh Tuan on 6/7/16.
//  Copyright © 2016 FCS Computer Systems. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableArray (QueueAdditions)
- (id) dequeue;
- (void) enqueue:(id)obj;
@end
