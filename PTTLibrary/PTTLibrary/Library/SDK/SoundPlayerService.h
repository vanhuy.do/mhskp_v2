//
//  SoundPlayerService.h
//  PTTLibrary
//
//  Created by Nguyen Minh Tuan on 6/9/16.
//  Copyright © 2016 FCS Computer Systems. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSMutableArray+QueueAdditions.h"
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>

@interface SoundPlayerService : NSObject<AVAudioPlayerDelegate, AVAudioSessionDelegate>
@property (nonatomic, strong) AVQueuePlayer *player;
@property (nonatomic, strong) id timeObserver;
+ (SoundPlayerService *)instance;
- (void)initData;
- (void)playAudio;
- (void)pauseAudio;
- (BOOL)isPlaying;
- (void)addSound:(NSURL*)url;
- (void)removeAllSoundInQueue;
@end
