//
//  SoundPlayerService.m
//  PTTLibrary
//
//  Created by Nguyen Minh Tuan on 6/9/16.
//  Copyright © 2016 FCS Computer Systems. All rights reserved.
//

#import "SoundPlayerService.h"
#import "HandleCall.h"
@implementation SoundPlayerService{
    NSString *currentItemURL;
}
+ (SoundPlayerService *)instance
{
    static SoundPlayerService *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[SoundPlayerService alloc] init];
        [sharedInstance initData];
    });
    return sharedInstance;
}
- (void)initData{
    // Set AVAudioSession
    NSError *sessionError = nil;
    [[AVAudioSession sharedInstance] setDelegate:self];
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayAndRecord error:&sessionError];
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
    // Change the default output audio route
    UInt32 doChangeDefaultRoute = 1;
    AudioSessionSetProperty(kAudioSessionProperty_OverrideCategoryDefaultToSpeaker, sizeof(doChangeDefaultRoute), &doChangeDefaultRoute);
    
    NSBundle *bundle = [NSBundle bundleForClass:[self class]];
    NSArray *queue = @[[AVPlayerItem playerItemWithURL:[bundle URLForResource:@"point1sec" withExtension:@"mp3"]]];
    
    self.player = [[AVQueuePlayer alloc] initWithItems:queue];
    self.player.actionAtItemEnd = AVPlayerActionAtItemEndAdvance;
    
    [self.player addObserver:self
                  forKeyPath:@"currentItem"
                     options:NSKeyValueObservingOptionNew
                     context:nil];
    
    void (^observerBlock)(CMTime time) = ^(CMTime time) {
//        NSString *timeString = [NSString stringWithFormat:@"%02.2f", (float)time.value / (float)time.timescale];
//        if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateActive) {
//            //update time
//            DLog(@"App is forcerounded. Time is: %@", timeString);
//        } else {
//            DLog(@"App is backgrounded. Time is: %@", timeString);
//        }
    };
    
    self.timeObserver = [self.player addPeriodicTimeObserverForInterval:CMTimeMake(10, 1000)
                                                                  queue:dispatch_get_main_queue()
                                                             usingBlock:observerBlock];
    [self.player play];
}
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"currentItem"])
    {
        AVPlayerItem *item = change[@"new"];
        if (item && ![item isKindOfClass:[NSNull class]]) {
            AVAsset *currentPlayerAsset = item.asset;
            currentItemURL = [[(AVURLAsset *)currentPlayerAsset URL] path];
        }else if(currentItemURL){
            if ([[NSFileManager defaultManager] fileExistsAtPath:currentItemURL]) {
                [[NSFileManager defaultManager] removeItemAtPath: currentItemURL error: nil];
            }
            
        }
        DLog(@"currentItem status change : %@ ", item);
//        self.lblMusicName.text = ((AVURLAsset*)item.asset).URL.pathComponents.lastObject;
//        NSLog(@"New music name: %@", self.lblMusicName.text);
    }
}
- (void)playAudio{
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
    [self.player play];
}

- (void)addSound:(NSURL*)url{
    id newItem = [AVPlayerItem playerItemWithURL:url];
    id lastItem = [self.player.items lastObject];
    if(newItem)
        [self.player insertItem:newItem afterItem:lastItem];
    else{
        DLog(@"New Item is nil");
    }
}

- (void)pauseAudio{
    if ([self isPlaying]) {
        [self.player pause];
    }
}
- (BOOL)isPlaying{
    if ((self.player.rate != 0) && (self.player.error == nil)) {
        return YES;
    }
    return NO;
}

#pragma mark - DELEGATE SESSION AUDIO
/* something has caused your audio session to be interrupted */
- (void)beginInterruption{
    [[[HandleCall instance] mCall] hold];
    DLog(@"beginInterruption");
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
    [self pauseAudio];
}
/* Currently the only flag is AVAudioSessionInterruptionFlags_ShouldResume. */
/* the interruption is over */
- (void)endInterruptionWithFlags:(NSUInteger)flags NS_AVAILABLE_IOS(4_0){
    [[[HandleCall instance] mCall] resume];
    DLog(@"endInterruptionWithFlags: %lu", (unsigned long)flags);
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [self playAudio];
    });
    
}

/* endInterruptionWithFlags: will be called instead if implemented. */
- (void)endInterruption{
    
    DLog(@"endInterruption");
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
//    [self playAudio];
}

/* notification for input become available or unavailable */
- (void)inputIsAvailableChanged:(BOOL)isInputAvailable{
    DLog(@"inputIsAvailableChanged: %@", (isInputAvailable?@"YES":@"NO"));
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
}
- (void)removeAllSoundInQueue{
    [self.player removeAllItems];
}
@end
