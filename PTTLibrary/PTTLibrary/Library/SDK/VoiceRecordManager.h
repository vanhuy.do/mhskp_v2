//
//  VoiceRecordManager.h
//  VoiceRecord
//
//  Created by Nguyen Minh Tuan on 5/23/16.
//
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>

@interface VoiceRecordManager : NSObject{
    NSMutableDictionary *recordSetting;
    NSURL *recordFileURL;
    AVAudioRecorder *recorder;
}
@property (nonatomic, strong) NSString *recorderFilePath;
@property long startTime, endTime;
+ (VoiceRecordManager *)instance;
- (void)startRecord;
- (void)stopRecord;
- (void)playRecord;
- (void)setRecordDelegate:(id)delegate;
- (BOOL)isRecording;
- (NSURL*)getRecordUrl;
@end
