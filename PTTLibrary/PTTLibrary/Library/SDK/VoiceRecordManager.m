//
//  VoiceRecordManager.m
//  VoiceRecord
//
//  Created by Nguyen Minh Tuan on 5/23/16.
//
//

#import "VoiceRecordManager.h"
#define DOCUMENTS_FOLDER [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"]
#define TIME_RECORD 60.0
#define SAMPLE_RATE 32000.0f
#define BIT_DEPT 16

@implementation VoiceRecordManager
@synthesize recorderFilePath;
@synthesize startTime, endTime;
+ (VoiceRecordManager *)instance
{
    __strong static VoiceRecordManager *_sharedLocalSystem = nil;
    
    static dispatch_once_t pred = 0;
    dispatch_once(&pred, ^{
        _sharedLocalSystem = [[self alloc] init];
        [_sharedLocalSystem initSetting];
    });
    return _sharedLocalSystem;
}

#pragma mark - PRIVATE METHOD
- (void)initSetting{
    recorderFilePath = [NSString stringWithFormat:@"%@/MySound.mp4", DOCUMENTS_FOLDER];
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    NSError *err = nil;
    [audioSession setCategory :AVAudioSessionCategoryPlayAndRecord error:&err];
    if(err){
        NSLog(@"audioSession: %@ %ld %@", [err domain], (long)[err code], [[err userInfo] description]);
        return;
    }
    [audioSession setActive:YES error:&err];
    err = nil;
    if(err){
        NSLog(@"audioSession: %@ %ld %@", [err domain], (long)[err code], [[err userInfo] description]);
        return;
    }
    
    recordSetting = [[NSMutableDictionary alloc] init];
    
    // We can use kAudioFormatAppleIMA4 (4:1 compression) or kAudioFormatLinearPCM for nocompression
    [recordSetting setValue :[NSNumber numberWithInt:kAudioFormatMPEG4AAC] forKey:AVFormatIDKey];
    
    // We can use 44100, 32000, 24000, 16000 or 12000 depending on sound quality
    [recordSetting setValue:[NSNumber numberWithFloat:SAMPLE_RATE] forKey:AVSampleRateKey];
    
    // We can use 2(if using additional h/w) or 1 (iPhone only has one microphone)
    [recordSetting setValue:[NSNumber numberWithInt: 1] forKey:AVNumberOfChannelsKey];
    
    // These settings are used if we are using kAudioFormatLinearPCM format
    [recordSetting setValue :[NSNumber numberWithInt:BIT_DEPT] forKey:AVLinearPCMBitDepthKey];
    //[recordSetting setValue :[NSNumber numberWithBool:NO] forKey:AVLinearPCMIsBigEndianKey];
    //[recordSetting setValue :[NSNumber numberWithBool:NO] forKey:AVLinearPCMIsFloatKey];
    
    
    
    // Create a new dated file
    //NSDate *now = [NSDate dateWithTimeIntervalSinceNow:0];
    //	NSString *caldate = [now description];
    //	recorderFilePath = [[NSString stringWithFormat:@"%@/%@.caf", DOCUMENTS_FOLDER, caldate] retain];
    
    DLog(@"recorderFilePath: %@",recorderFilePath);
    
    recordFileURL = [NSURL fileURLWithPath:recorderFilePath];
    err = nil;
    recorder = [[ AVAudioRecorder alloc] initWithURL:recordFileURL settings:recordSetting error:&err];
    if(!recorder){
        NSLog(@"recorder: %@ %ld %@", [err domain], (long)[err code], [[err userInfo] description]);
        UIAlertView *alert =
        [[UIAlertView alloc] initWithTitle: @"Warning"
                                   message: [err localizedDescription]
                                  delegate: nil
                         cancelButtonTitle:@"OK"
                         otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    //prepare to record
    recorder.meteringEnabled = YES;
    
    BOOL audioHWAvailable = [AVAudioSession sharedInstance].inputIsAvailable;
    if (! audioHWAvailable) {
        UIAlertView *cantRecordAlert =
        [[UIAlertView alloc] initWithTitle: @"Warning"
                                   message: @"Audio input hardware not available"
                                  delegate: nil
                         cancelButtonTitle:@"OK"
                         otherButtonTitles:nil];
        [cantRecordAlert show];
        return;
    }
}

#pragma mark - PUBLIC METHOD
- (void)setRecordDelegate:(id)delegate{
    [recorder setDelegate:delegate];
    
}
- (void)startRecord{
    
    [[AVAudioSession sharedInstance] setCategory: AVAudioSessionCategoryPlayAndRecord error: nil];
    //check if recording must be stop first
    if ([recorder isRecording]) {
        [recorder stop];
    }
    
    //remove old file record
    NSError *err = nil;
    NSData *audioData = [NSData dataWithContentsOfFile:[recordFileURL path] options: 0 error:&err];
    if(audioData)
    {
        NSFileManager *fm = [NSFileManager defaultManager];
        [fm removeItemAtPath:[recordFileURL path] error:&err];
    }
    //start record
    startTime = ([[NSDate date] timeIntervalSince1970] * 1000);
    [recorder prepareToRecord];
    [recorder recordForDuration:(NSTimeInterval) TIME_RECORD];
}
- (void)stopRecord{
    if (recorder == nil || !recorder.isRecording) {
        return;
    }
    endTime = ([[NSDate date] timeIntervalSince1970] * 1000);
    [recorder stop];
    unsigned long long fileSize = [[[NSFileManager defaultManager] attributesOfItemAtPath:recorderFilePath error:nil] fileSize];
    DLog(@"file size: %llu", fileSize);
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
}
- (void)playRecord{
    NSError *error;
    AVAudioPlayer *backgroundMusicPlayer = [[AVAudioPlayer alloc]
                                            initWithContentsOfURL:recordFileURL error:&error];
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
    [backgroundMusicPlayer prepareToPlay];
    [backgroundMusicPlayer play];
}
- (BOOL)isRecording{
    return recorder != nil && recorder.recording;
}

@end
