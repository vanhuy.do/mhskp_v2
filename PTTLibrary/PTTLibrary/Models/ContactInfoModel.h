//
//  ContactInfoModel.h
//  PTTLibrary
//
//  Created by phu the cong on 8/20/15.
//  Copyright (c) 2015 FCS Computer Systems. All rights reserved.
//

#import "JSONModel.h"
#import "PTTBaseJSONModel.h"
typedef enum
{
    Available,
    DoNotDisturb,
    OnCall ,
    Offline
} UserStatus;

@interface ContactInfoModel : PTTBaseJSONModel
@property (strong,nonatomic ) NSString   * nickName;
@property (strong,nonatomic ) NSString   * userId;
@property (strong,nonatomic ) NSString   * userName;
@property (assign, nonatomic) UserStatus status;
@property (assign, nonatomic) BOOL       joined;
@property (assign, nonatomic) BOOL       originalState;

@end
