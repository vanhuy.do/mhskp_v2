//
//  ContactInfoModel.m
//  PTTLibrary
//
//  Created by phu the cong on 8/20/15.
//  Copyright (c) 2015 FCS Computer Systems. All rights reserved.
//

#import "ContactInfoModel.h"

@implementation ContactInfoModel{
}

-(id)initWithDictionary:(NSDictionary *)dict error:(NSError *__autoreleasing *)err
{
    if (self = [super  init]) {
        self.nickName = [dict objectForKey:KEY_NICK_NAME];
        self.userId = [dict objectForKey:KEY_USER_ID];
        self.status = [[dict objectForKey:KEY_STATUS] intValue];
        self.userName = [dict objectForKey:KEY_USER_NAME];
        id join = [dict objectForKey:KEY_JOINED];
        if ([join isKindOfClass:[NSNumber class]]) {
            self.joined = [join boolValue];
            self.originalState = self.joined;
        }
    }
    return self;
}
@end
