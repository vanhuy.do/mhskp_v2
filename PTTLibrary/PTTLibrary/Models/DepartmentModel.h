//
//  DepartmentModel.h
//  PTTLibrary
//
//  Created by Nguyen Minh Duc on 8/19/15.
//  Copyright (c) 2015 FCS Computer Systems. All rights reserved.
//

#import "JSONModel.h"
#import "PTTBaseJSONModel.h"
@protocol DepartmentModel
@end

@interface DepartmentModel : PTTBaseJSONModel

@property (nonatomic       ) int      id;
@property (nonatomic       ) int      departmentId;
@property (strong,nonatomic) NSString * departmentName;
@end
