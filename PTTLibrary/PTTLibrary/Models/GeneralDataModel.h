//
//  GeneralDataModel.h
//  PTTLibrary
//
//  Created by Nguyen Minh Duc on 8/19/15.
//  Copyright (c) 2015 FCS Computer Systems. All rights reserved.
//

#import "JSONModel.h"
#import "UserInfoModel.h"
#import "DepartmentModel.h"
#import "PTTBaseJSONModel.h"

@interface GeneralDataModel : PTTBaseJSONModel

@property (nonatomic,strong) NSString      *access_token, *request_time_out;
@property (nonatomic,strong) UserInfoModel *currentUser;
@property (nonatomic,strong) NSArray<DepartmentModel> * departments;
@end
