//
//  GroupBaseModel.h
//  PTTLibrary
//
//  Created by Admin on 8/21/15.
//  Copyright (c) 2015 FCS Computer Systems. All rights reserved.
//
#import "JSONModel.h"
#import "PTTBaseJSONModel.h"
@protocol GroupBaseModel
@end

@interface GroupBaseModel : PTTBaseJSONModel

@property (nonatomic       ) int      countUser;
@property (strong,nonatomic) NSString * groupName;
@property (nonatomic       ) int      groupId;
@end
