//
//  GroupModel.h
//  PTTLibrary
//
//  Created by Nguyen Minh Duc on 8/19/15.
//  Copyright (c) 2015 FCS Computer Systems. All rights reserved.
//

#import "GroupBaseModel.h"

@protocol GroupModel
@end

@interface GroupModel : GroupBaseModel


@property (nonatomic       ) int      applicationId;
@property (strong,nonatomic) NSString * applicationName;
@property (nonatomic       ) int      departmentId;
@property (strong,nonatomic) NSString * departmentName;
@property (strong,nonatomic) NSString * groupType;
@property (nonatomic       ) int      isDefault;

+ (GroupModel*) findDefaultGroup:(NSArray*)groups;
/**
 *  Get list channel for MQTT subcribe
 *
 *  @param groups list of group
 *
 *  @return list string of channel
 */
+ (NSArray*)getListChannel:(NSArray*)groups;
@end
