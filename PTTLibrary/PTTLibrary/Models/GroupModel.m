//
//  GroupModel.m
//  PTTLibrary
//
//  Created by Nguyen Minh Duc on 8/19/15.
//  Copyright (c) 2015 FCS Computer Systems. All rights reserved.
//

#import "GroupModel.h"

@implementation GroupModel

+ (GroupModel*) findDefaultGroup:(NSArray*)groups{
    for (GroupModel* group in groups) {
        if(group.isDefault == DEFAULT_GROUP_VALUE){
            return group;
        }
    }
    return nil;
}

+ (NSArray*)getListChannel:(NSArray*)groups{
    NSMutableArray *result = [[NSMutableArray alloc] init];
    for (GroupModel* group in groups) {
        NSString *strChannel =[NSString stringWithFormat:@"g%d", group.groupId];
        [result addObject:strChannel];
    }
    return result;
}
+(BOOL)propertyIsOptional:(NSString*)propertyName
{
    return YES;
}
@end
