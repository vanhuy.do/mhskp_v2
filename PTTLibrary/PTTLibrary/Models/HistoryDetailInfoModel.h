//
//  HistoryDetailInfoModel.h
//  PTTLibrary
//
//  Created by Admin on 8/28/15.
//  Copyright (c) 2015 FCS Computer Systems. All rights reserved.
//

#import "JSONModel.h"
#import "PTTBaseJSONModel.h"
@interface HistoryDetailInfoModel : PTTBaseJSONModel{
    NSString *stringStartTime, *stringDuration;
    int duration;
    float currentDuration;
    BOOL isPlayed;
}
@property (nonatomic       ) int      callMediaId;
@property (strong,nonatomic) NSString * endDate;
@property (strong,nonatomic) NSString * startDate;
@property (strong,nonatomic) NSString * mediaUrl;
@property (strong,nonatomic) NSString * name;
@property (nonatomic       ) int      sizeKb;
@property (nonatomic       ) int      userId;
@property (strong,nonatomic) NSString * userName;
- (NSString*)getStartTime;
- (NSString*)getDuration;
- (int)duration;
- (float)currentDuration;
- (void)setCurrentDuration:(float)currentData;
- (void)setPlay:(BOOL)value;
- (BOOL)getPlay;
@end
