//
//  HistoryDetailInfoModel.m
//  PTTLibrary
//
//  Created by Admin on 8/28/15.
//  Copyright (c) 2015 FCS Computer Systems. All rights reserved.
//

#import "HistoryDetailInfoModel.h"

@implementation HistoryDetailInfoModel
- (NSString*)getStartTime{
    if(!stringStartTime){
        NSArray *arrayDate = [self.startDate componentsSeparatedByString:@" "];
        stringStartTime = (arrayDate.count > 1) ? [arrayDate objectAtIndex:1] : @"";
    }
    return stringStartTime;
}
- (NSString*)getDuration{
    if (!stringDuration) {
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"dd-MM-yyyy HH:mm:ss"];
        NSDate *tempStart = [dateFormat dateFromString:self.startDate];
        NSDate *tempEnd = [dateFormat dateFromString:self.endDate];
        NSTimeInterval diff = [tempEnd timeIntervalSinceDate:tempStart];
        duration = (int)diff;
        stringDuration = [NSString stringWithFormat:@"00:00:%d", duration];
    }
    float currentTimeNow = duration - currentDuration;
    DLog(@"%f", currentTimeNow);
    if (currentTimeNow < 0.9) {
        currentDuration = 0;
        currentTimeNow = 0;
        isPlayed = NO;
    }
    stringDuration = [NSString stringWithFormat:@"00:00:%d", (int)currentTimeNow];
    return stringDuration;
}
- (int)duration{
    return duration;
}
- (float)currentDuration{
    return currentDuration;
}
- (void)setCurrentDuration:(float)currentData{
    currentDuration = currentData;
}
- (void)setPlay:(BOOL)value{
    isPlayed = value;
}
- (BOOL)getPlay{
    return isPlayed;
}
@end
