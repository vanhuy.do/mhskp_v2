//
//  HistoryDetailListInfoModel.h
//  PTTLibrary
//
//  Created by Admin on 8/28/15.
//  Copyright (c) 2015 FCS Computer Systems. All rights reserved.
//

#import "JSONModel.h"
#import "PTTBaseJSONModel.h"
@class HistoryDetailInfoModel;
@interface HistoryDetailListInfoModel : PTTBaseJSONModel
@property (strong,nonatomic) NSString       *startDate;
@property (strong,nonatomic) NSDate         *date;
@property (strong,nonatomic) NSMutableArray *arrayHistoryDetail;
- (void)addHistoryObject:(HistoryDetailInfoModel*)object;
@end
