//
//  HistoryDetailListInfoModel.m
//  PTTLibrary
//
//  Created by Admin on 8/28/15.
//  Copyright (c) 2015 FCS Computer Systems. All rights reserved.
//

#import "HistoryDetailListInfoModel.h"
#import "HistoryDetailInfoModel.h"
@implementation HistoryDetailListInfoModel
@synthesize startDate;
@synthesize date;
@synthesize arrayHistoryDetail;
- (void)addHistoryObject:(HistoryDetailInfoModel*)object{
    if (!arrayHistoryDetail) {
        arrayHistoryDetail = [[NSMutableArray alloc] init];
        
//        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
//        [dateFormat setDateFormat:@"dd-MM-yyyy"];
//        date = [dateFormat dateFromString:object.startDate];
//        startDate = [dateFormat stringFromDate:date];
        NSArray *arrayDate = [object.startDate componentsSeparatedByString:@" "];
        startDate = [arrayDate objectAtIndex:0];

    }
    [arrayHistoryDetail insertObject:object atIndex:0];
}
@end
