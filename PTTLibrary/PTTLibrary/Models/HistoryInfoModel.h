//
//  HistoryInfoModel.h
//  PTTLibrary
//
//  Created by Admin on 8/27/15.
//  Copyright (c) 2015 FCS Computer Systems. All rights reserved.
//

#import "JSONModel.h"
#import "PTTBaseJSONModel.h"
@interface HistoryInfoModel : PTTBaseJSONModel
@property (nonatomic       ) int      groupId;
@property (nonatomic       ) int      groupType;
@property (strong,nonatomic) NSString * lastedCall;
@property (strong,nonatomic) NSString * name;
@end
