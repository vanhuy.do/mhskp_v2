//
//  UserInCallModel.h
//  PTTLibrary
//
//  Created by Admin on 8/25/15.
//  Copyright (c) 2015 FCS Computer Systems. All rights reserved.
//

#import "JSONModel.h"
#import "PTTBaseJSONModel.h"
@interface UserInCallModel : PTTBaseJSONModel

@property (strong,nonatomic) NSString * nickName;
@property (nonatomic       ) int      state;
@property (nonatomic       ) int      userId;
@property (strong,nonatomic) NSString * userName;
@end
