//
//  UserInfoModel.h
//  PTTLibrary
//
//  Created by Nguyen Minh Duc on 8/19/15.
//  Copyright (c) 2015 FCS Computer Systems. All rights reserved.
//

#import "JSONModel.h"
#import "GroupModel.h"
#import "PTTBaseJSONModel.h"
@class PropertyModel;
@interface UserInfoModel : PTTBaseJSONModel

@property (strong, nonatomic) NSMutableArray<GroupModel> *groups;
@property (strong, nonatomic) NSString *nickName;
@property (strong, nonatomic) NSString *role;
@property (strong, nonatomic) NSString *userId;
@property (strong, nonatomic) NSString *userName;
//@property (strong, nonatomic) NSArray<DepartmentModel>* departments;

@end
