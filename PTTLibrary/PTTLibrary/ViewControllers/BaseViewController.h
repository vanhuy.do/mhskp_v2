//
//  BaseViewController.h
//  PTTLibrary
//
//  Created by Admin on 7/31/15.
//  Copyright (c) 2015 Tuan Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "AFNetworkingUtility.h"
#import "GlobalObject.h"
#import "ServerPath.h"
#import "SettingConstants.h"
#import "MBProgressHUD1.h"
#import "Utility.h"

@class HomeViewController;

@interface BaseViewController : UIViewController{
    HomeViewController *homeView;
    MBProgressHUD1 *hudLoading;
    NSTimer *timerMakeCall;
    __weak IBOutlet UIButton *buttonPTT;
}
@property(nonatomic, strong) NSString *selectedLanguageName;
//- (void)audioRouteChangeListenerCallback:(NSNotification*)notification;

- (BOOL)isHeadsetPluggedIn;

- (void) startPingPTT;

-(void)pingPTTServer: (id) sender;

- (void)showMessageWithTitle:(NSString*)title withMessage:(NSString*)message success:(void (^)())success;
- (void)showMakeCallLoading;
- (void)stopMakeCallLoading;
- (IBAction)exit:(id)sender;
- (void)showLoading;
- (void)hideLoading;
@end
