//
//  BaseViewController.m
//  PTTLibrary
//
//  Created by Admin on 7/31/15.
//  Copyright (c) 2015 Tuan Nguyen. All rights reserved.
//

#import "BaseViewController.h"
#import "Language.h"
#import "MQTTManager.h"
#import "Utility.h"
#import "HomeViewController.h"
#import "UIAlertView+Blocks.h"
#import "UIImage+PTT.h"
#import "HandleCall.h"

#define COLOR_NAVIGATION_BAR [UIColor colorWithRed:99/255.0f green:202/255.0f blue:206/255.0f alpha:1.0f]
@interface BaseViewController ()
{
    UITapGestureRecognizer *tap;
}

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupNavigationBar];
    tap = [[UITapGestureRecognizer alloc]
           initWithTarget:self
           action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    [tap setEnabled:NO];
    NotifReg(self, @selector(keyboardDidShow:), UIKeyboardDidShowNotification);
    NotifReg(self, @selector(keyboardDidHide:), UIKeyboardDidHideNotification);
}
- (void)dealloc{
    NotifUnregAll(self);
}
- (void)showMessageWithTitle:(NSString*)title withMessage:(NSString*)message success:(void (^)())success {
    [UIAlertView showWithTitle:title
                       message:message
             cancelButtonTitle:getLocalizationString(@"Ok")
             otherButtonTitles:nil
                      tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                          success();
                      }];
}
+ (UIImage *)imageWithColor:(UIColor *)color
{
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}
-(void)setupNavigationBar
{
    if ([GlobalObject instance].currentTheme == THEME_MHSKP) {
        UIImage *imageBackground;
        if ([[[UIDevice currentDevice] systemVersion] floatValue] < 7) {
            
            imageBackground = [UIImage PTTImageNamed: @"NavigationBar"];
            [imageBackground drawInRect: CGRectMake(0, 0, 320, 44)];
        }else{
            imageBackground = [[self class] imageWithColor:COLOR_NAVIGATION_BAR];
            [imageBackground drawInRect:CGRectMake(0, 0, 320, 44)];
        }
        [[UINavigationBar appearance] setBackgroundImage:imageBackground forBarMetrics:UIBarMetricsDefault];
    }else{
        NSDictionary *attributes = nil;
        if ([[[UIDevice currentDevice] systemVersion] floatValue] < 7) {
            attributes  = @{
                            UITextAttributeTextColor: [UIColor greenColor],
                            UITextAttributeTextShadowColor: [UIColor redColor],
                            UITextAttributeTextShadowOffset: [NSValue valueWithUIOffset:UIOffsetMake(0.0f, -1.0f)],
                            UITextAttributeFont: [UIFont fontWithName:@"Helvetica" size:20.0f]
                            };
            [self.navigationController.navigationBar setBackgroundColor:[UIColor colorWithRed:103/255.0 green:162/255.0 blue:223/255.0 alpha:1.0]];
            
        }else
        {
            [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:103/255.0 green:162/255.0 blue:223/255.0 alpha:1.0]];
            NSShadow *shadow = [[NSShadow alloc] init];
            shadow.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
            shadow.shadowOffset = CGSizeMake(0.0f, -1.0f);
            attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                          [UIColor whiteColor], NSForegroundColorAttributeName,
                          [UIFont boldSystemFontOfSize:18.0], NSFontAttributeName,
                          shadow, NSShadowAttributeName,
                          nil];
        }
        [[UINavigationBar appearance] setTitleTextAttributes: attributes];
        
    }
    [self setupNavigationBarItems];
    
}


-(void)setupNavigationBarItems
{
    
}

-(void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if([self isHeadsetPluggedIn]){
        [[HandleCall instance] setRoutingType:AVAudioSessionPortOverrideNone];
    }
    else{
        [[HandleCall instance] setRoutingType:AVAudioSessionPortOverrideSpeaker];
    }
    if ([GlobalObject instance].currentTheme == THEME_MHSKP) {
        [buttonPTT setBackgroundImage:[UIImage PTTImageNamed:@"m_house_64x64"] forState:UIControlStateNormal];
    }
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
}
- (void)audioRouteChangeListenerCallback:(NSNotification*)notification
{
}

-(void)setSelectedLanguageName:(NSString *)selectedLanguageName
{
    _selectedLanguageName =selectedLanguageName;
    [[PTTLanguageManager sharedLanguageManager] setLanguage:[[Language alloc] initWithLanguageCode:@"" name:selectedLanguageName]];
    
}

- (IBAction)exit:(id)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Private Methods

- (void)dismissKeyboard {
    [self.view endEditing:YES];
}

- (void)keyboardDidShow: (NSNotification *) notif{
    // Do something here
    [tap setEnabled:YES];
}

- (void)keyboardDidHide: (NSNotification *) notif{
    // Do something here
    [tap setEnabled:NO];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */



#pragma mark -- Detech Headphone

- (BOOL)isHeadsetPluggedIn
{
    AVAudioSessionRouteDescription *route = [[AVAudioSession sharedInstance] currentRoute];
    
    BOOL headphonesLocated = NO;
    for( AVAudioSessionPortDescription *portDescription in route.outputs )
    {
        headphonesLocated |= ( [portDescription.portType isEqualToString:AVAudioSessionPortHeadphones] );
    }
    return headphonesLocated;
}

#pragma mark -- Ping PTT
- (void) startPingPTT{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [[NSRunLoop mainRunLoop] addTimer:[GlobalObject instance].pingTimer forMode:NSDefaultRunLoopMode];
        });
    });
}
-(void)remoteControlReceivedWithEvent:(UIEvent *)event {
    
    if (event.type == UIEventTypeRemoteControl) {
        NSLog(@"%ld",(long)event.subtype);
        switch(event.subtype) {
            case UIEventSubtypeRemoteControlTogglePlayPause:{
                if ([HandleCall instance].mCall.groupName) {
                    if (!homeView) {
                        for (UIViewController *retVal in self.navigationController.viewControllers) {
                            if ([retVal isKindOfClass:[HomeViewController class]]) {
                                homeView = (HomeViewController*)retVal;
                                break;
                            }
                        }
                    }
                    if (homeView) {
                        if (homeView.StateOfCall == STATE_REQUESTED_OK) {
                            [homeView handleEndTalk];
                        }else{
                            [homeView handleTalking];
                        }
                    }
                    
                }
                
                
                //                if(![XCPjsua sharedXCPjsua].isRequestCall){
                //                    [_mqttManager sendRequestTalk];
                //                }
                //                else{
                //                    [_mqttManager sendEndTalk];
                //                }
                //                [XCPjsua sharedXCPjsua].isRequestCall = ![XCPjsua sharedXCPjsua].isRequestCall;
            }
                break;
            case UIEventSubtypeRemoteControlPlay:
                
                break;
            case UIEventSubtypeRemoteControlPause:
                
                break;
            case UIEventSubtypeRemoteControlStop:
                
                break;
            default:
                break;
                
        }
    }
//    else{
//        [super remoteControlReceivedWithEvent:event];
//    }
}
- (void)pingPTTServer: (id) sender
{
    [[MQTTManager instance] pingToMQTT];
}

#pragma mark - show loading
- (void)showMakeCallLoading{
    hudLoading = [MBProgressHUD1 showHUDAddedTo:self.view animated:YES];
    hudLoading.mode = MBProgressHUDModeDeterminateHorizontalBar1;
    
    hudLoading.labelText = getLocalizationString(@"connecting");
    hudLoading.progress = 0;
    [self makeCallCountdown];
}
- (void)makeCallCountdown{
    if (!timerMakeCall) {
        timerMakeCall = [NSTimer scheduledTimerWithTimeInterval:0.04f
                                                         target:self
                                                       selector:@selector(updateMakeCallProgress:)
                                                       userInfo:nil
                                                        repeats:YES];
    }
}
- (void)updateMakeCallProgress:(NSTimer *)timer{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        hudLoading.progress += timer.timeInterval/[GET_SETTING_TIME_OUT_CONNECTING_SERVER intValue];
    });
    if (hudLoading.progress >= 1.0) {
        [hudLoading hide:YES];
        [self stopMakeCallLoading];
        [UIAlertView showWithTitle:@""
                           message:@"Make Call Fail"
                 cancelButtonTitle:@"OK"
                 otherButtonTitles:nil
                          tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                          }];
    }
}
- (void)stopMakeCallLoading{
    if ([timerMakeCall isValid]) {
        [timerMakeCall invalidate];
    }
    timerMakeCall = nil;
    if (hudLoading) {
        [MBProgressHUD1 hideAllHUDsForView:self.view animated:YES];
    }
}
- (void)showLoading{
    [MBProgressHUD1 showHUDAddedTo:self.view animated:YES];
//    dispatch_async(dispatch_get_main_queue(), ^{
//        [MBProgressHUD1 showHUDAddedTo:self.view animated:YES];
//    });
}
- (void)hideLoading{
    [MBProgressHUD1 hideHUDForView:self.view animated:YES];
//    dispatch_async(dispatch_get_main_queue(), ^{
//        [MBProgressHUD1 hideHUDForView:self.view animated:YES];
//    });
}
@end
