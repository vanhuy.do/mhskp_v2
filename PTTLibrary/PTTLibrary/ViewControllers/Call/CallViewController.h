//
//  CallViewController.h
//  PTTLibrary
//
//  Created by Nguyen Minh Duc on 8/10/15.
//  Copyright (c) 2015 FCS Computer Systems. All rights reserved.
//

#import "BaseViewController.h"
#import "HomeViewController.h"

@interface CallViewController : BaseViewController{
    
    __weak IBOutlet UIButton *buttonBig;
    __weak IBOutlet UICollectionView *collectionUserView;
    NSMutableArray *arrayListUser;
    __weak IBOutlet UILabel *labelNumberOfUser;
    __weak IBOutlet UILabel *labelStatusOfTalk;
    __weak IBOutlet UILabel *labelCountDown;
    HomeViewController *homeVC;
    NSTimer *mReleaseTalkCountDown;
    int currentTimeTalk;
}

//@property ( nonatomic) NSInteger callType;
@property (nonatomic, strong) NSString *titleViewController;
- (IBAction)makeRequestCall:(id)sender;

- (IBAction)endRequestCall:(id)sender;
- (void)presentCallState:(int)callState;
- (void)ReleaseTalkCountDownStop;
- (void)ReleaseTalkCountDownStart;
- (void)updateCollapseTextView:(NSString*)text;
@end
