//
//  CallViewController.m
//  PTTLibrary
//
//  Created by Nguyen Minh Duc on 8/10/15.
//  Copyright (c) 2015 FCS Computer Systems. All rights reserved.
//

#import "CallViewController.h"
#import "MQTTManager.h"
#import "ServerPath.h"
#import "UserCollectionViewCell.h"
#import "UIImage+PTT.h"
#import "UserInCallModel.h"
#import "MarqueeLabel.h"
#import "HandleCall.h"
#import "CallInfo.h"

@import MediaPlayer;
#define USER_CALL 1
#define GROUP_CALL 2
#define STATE_OFFLINE -1
#define STATTE_ON_CALL 2

@interface CallViewController (){
    MQTTManager *_mqttManager;
}

@property (weak, nonatomic) IBOutlet UITextField *idTextbox;


- (IBAction)makeCall:(id)sender;

- (IBAction)endCall:(id)sender;
@end

@implementation CallViewController
#pragma mark - VIEWCONTROLLER LIFECYCLE

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = self.titleViewController;
    _mqttManager = [MQTTManager instance];
    [buttonBig setBackgroundImage:[UIImage PTTImageNamed:[self getBigButtonName:@"push_to_talk_button"]] forState:UIControlStateNormal];
    [self setBackgroundTalkButton:YES];
    self.title = self.titleViewController;
    arrayListUser = [[NSMutableArray alloc] init];
    if ([GlobalObject instance].isEchoTest == YES) {
        [self initEchoUser];
    }else
        [self requestListUserInCall];
    [collectionUserView registerNib:[UINib nibWithNibName:@"UserCollectionViewCell" bundle:[Utility getPttLibraryBundle]] forCellWithReuseIdentifier:@"userIdentifier"];
    for (UIViewController *retVal in self.navigationController.viewControllers) {
        if ([retVal isKindOfClass:[HomeViewController class]]) {
            homeVC = (HomeViewController*)retVal;
            break;
        }
    }
    if (self.titleViewController != nil && self.titleViewController.length > 10) {
        [self setNavigationTitle:self.titleViewController];
    }
    
}
- (void)setNavigationTitle:(NSString*)title{
    CGRect frame = CGRectMake(0, 0, 480, 44);
    MarqueeLabel *scrollyLabel = [[MarqueeLabel alloc] initWithFrame:frame rate:20.0f andFadeLength:15.0f];
    [scrollyLabel setMarqueeType:MLContinuous];
    scrollyLabel.text = title;
    scrollyLabel.font = [UIFont boldSystemFontOfSize: 17.0f];
    scrollyLabel.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    scrollyLabel.backgroundColor = [UIColor clearColor];
    scrollyLabel.textColor = [UIColor whiteColor];
    self.navigationItem.titleView = scrollyLabel;
}
- (void)setBackgroundTalkButton:(BOOL)state{
    
    if (state) {
        [buttonBig setBackgroundImage:[UIImage PTTImageNamed:[self getBigButtonName:@"push_to_talk_button_press"]] forState:UIControlStateHighlighted];
        [buttonBig setBackgroundImage:[UIImage PTTImageNamed:[self getBigButtonName:@"push_to_talk_button_press"]] forState:UIControlStateSelected];
    }else{
        [buttonBig setBackgroundImage:[UIImage PTTImageNamed:[self getBigButtonName:@"push_to_talk_button_presson_fail"]] forState:UIControlStateHighlighted];
        [buttonBig setBackgroundImage:[UIImage PTTImageNamed:[self getBigButtonName:@"push_to_talk_button_presson_fail"]] forState:UIControlStateSelected];
    }
}
- (void)initEchoUser{
    GlobalObject *glbObject = [GlobalObject instance];
    UserInCallModel *userInfo = [[UserInCallModel alloc] init];
    userInfo.userName = glbObject.generalData.currentUser.userName;
    userInfo.nickName = glbObject.generalData.currentUser.nickName;
    userInfo.userId = glbObject.generalData.currentUser.userId.intValue;
    userInfo.state = 1;
    [arrayListUser addObject:userInfo];
    labelNumberOfUser.text = [NSString stringWithFormat:@"%lu", (unsigned long)arrayListUser.count];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];

    NotifReg(self, @selector(handleUpdateUserCalling:), MQTT_UPDATE_LIST_CONTACT);
    NotifReg(self, @selector(handleUpdateListUser), MQTT_UPDATE_LIST_USER_INCALL);
    NotifReg(self, @selector(handleDeleteGroup:), MQTT_GROUP_DELETE);
}

- (void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [homeVC handleEndTalk];
    NotifUnreg(self, MQTT_UPDATE_LIST_CONTACT);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)dealloc{
    arrayListUser = nil;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - SUPPORT FUNCTION
- (NSString*)getBigButtonName:(NSString*)name{
    if ([GlobalObject instance].currentTheme == THEME_MHSKP) {
        return [NSString stringWithFormat:@"%@_mhskp", name];
    }
    return name;
}
- (void)ReleaseTalkCountDownStart{
    if (!mReleaseTalkCountDown) {
        mReleaseTalkCountDown = [NSTimer scheduledTimerWithTimeInterval:1.0f
                                                                 target:self
                                                               selector:@selector(handleUpdateTime:)
                                                               userInfo:nil
                                                                repeats:YES];
        currentTimeTalk = [[GlobalObject instance].generalData.request_time_out intValue];
    }
}
- (void)handleUpdateTime:(NSTimer *)timer{
    
    currentTimeTalk--;
    if (currentTimeTalk == 0) {
        [self ReleaseTalkCountDownStop];
    }
    [self updateCollapseTextView:[NSString stringWithFormat:@"00:%d", currentTimeTalk]];
}
- (void)ReleaseTalkCountDownStop{
    if ([mReleaseTalkCountDown isValid]) {
        [mReleaseTalkCountDown invalidate];
    }
    mReleaseTalkCountDown = nil;
    currentTimeTalk = 0;
    [self releaseTalkButton];
}
- (void)releaseTalkButton{
    [self updateCollapseTextView:NO_TIME];
    homeVC.StateOfCall = STATE_NONE;
    [self presentCallState:homeVC.StateOfCall];
}
- (void)requestListUserInCall{
//    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSMutableDictionary *parms =[NSMutableDictionary dictionary];
    [parms setObject:[NSNumber numberWithInt:[GlobalObject instance].groupID] forKey:KEY_GROUP_ID];
    
    [[AFNetworkingUtility instance] requestData:API_GET_LIST_USER_IN_CHANNEL withParameters:parms success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        [MBProgressHUD hideHUDForView:self.view animated:YES];
        DLog(@"%@", responseObject);
        id data = [responseObject objectForKey:KEY_DATA];
        if ([data isKindOfClass:[NSArray class]]) {
            [arrayListUser removeAllObjects];
	    BOOL isHaveMe = NO;
            for (NSDictionary *userDict in data) {
                NSError* err = nil;
                UserInCallModel *userInfo = [[UserInCallModel alloc] initWithDictionary:userDict error:&err];
                if (err == nil) {
                    UserInfoModel *currentUser = [GlobalObject instance].generalData.currentUser;
                    if (userInfo.userId == currentUser.userId.intValue) isHaveMe = YES;
                    [arrayListUser addObject:userInfo];
                }
                
            }
            if (!isHaveMe) {
                UserInCallModel *userInfo = [[UserInCallModel alloc] init];
                UserInfoModel *currentUser = [GlobalObject instance].generalData.currentUser;
                userInfo.userId = currentUser.userId.intValue;
                userInfo.nickName = currentUser.nickName;
                userInfo.userName = currentUser.userName;
                userInfo.state = 1;
                [arrayListUser addObject:userInfo];
            }
            labelNumberOfUser.text = [NSString stringWithFormat:@"%lu", (unsigned long)arrayListUser.count];
            [collectionUserView reloadData];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
}

- (void)presentCallState:(int)callState{
    [self setBackgroundTalkButton:YES];
    switch (callState) {
        case STATE_NONE:
        case STATE_GOOD_NETWORK:
        case STATE_UNKNOWN_NETWORK:{
            labelStatusOfTalk.text = @"";
            break;
        }case STATE_REQUESTING:{
            labelStatusOfTalk.text = getLocalizationString(@"Requesting");
            break;
        }case STATE_REQUESTED_FAILED:{
            labelStatusOfTalk.text = getLocalizationString(@"Some_one_talking");
            [self setBackgroundTalkButton:NO];
            break;
        }case STATE_REQUESTED_OK:{
            labelStatusOfTalk.text = getLocalizationString(@"Speak_now");
            break;
        }case STATE_DISCONNECT_SIP:{
            labelStatusOfTalk.text = getLocalizationString(@"Connection_lost");
            [self setBackgroundTalkButton:NO];
            break;
        }case STATE_BAD_NETWORK:{
            labelStatusOfTalk.text = getLocalizationString(@"Bad_network");
            break;
        }case STATE_FAIR_NETWORK:{
            labelStatusOfTalk.text = getLocalizationString(@"Fair_network");
            break;
        }
        default:{
            labelStatusOfTalk.text = getLocalizationString(@"Wait_seconds");
            break;
        }
    }
}
#pragma mark -- Private Method

-(void)callWithGroup:(NSString*)groupId{
    GlobalObject *globalObject = [GlobalObject instance];
    globalObject.uri = [NSString stringWithFormat:@"sip:%@@%@:%@",groupId, GET_SER_IP, GET_SER_SIP_PORT];

//    char *callUri = (char*)[globalObject.uri UTF8String];
    globalObject.groupID = [groupId intValue];
    globalObject.groupName = groupId;
    [[HandleCall instance] makeCallTo:globalObject.groupID];
    if([self.idTextbox.text integerValue] == 0){
        [GlobalObject instance].isEchoTest = YES;
    }
    else{
        [GlobalObject instance].isEchoTest = NO;
    }
    
}

#pragma mark -- Action events

- (IBAction)makeCall:(id)sender {
//    if(self.callType == USER_CALL){
//        // Get Fake Group
////        int test = -1;
//        NSDictionary* inputDict = [[NSDictionary alloc] initWithObjectsAndKeys:[NSNumber numberWithInt:[[GlobalObject instance].generalData.currentUser.userId integerValue]],@"userIdA",[NSNumber numberWithInt:[self.idTextbox.text integerValue]],@"userIdB",nil];
////        NSDictionary* inputDict = [[NSDictionary alloc] initWithObjectsAndKeys:[NSNumber numberWithInt:1],@"groupId",nil];
//        [[AFNetworkingUtility instance] requestData:API_GET_CREATE_CHANNEL_FAKE withParameters:inputDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
//            DLog(@"%@", responseObject);
////            success(nil);
//        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {\
////            success([NSString stringWithFormat:@"Error code %d",[operation.response statusCode]]);
//        }];
//        
//    }
//    else{
//        [self callWithGroup:self.idTextbox.text];
//    }

}

- (IBAction)makeRequestCall:(id)sender {
    [homeVC handleTalking];
}
- (void)updateCollapseTextView:(NSString*)text{
    labelCountDown.text = text;
}
- (IBAction)endRequestCall:(id)sender {

    [homeVC handleEndTalk];
    
}
- (IBAction)endCall:(id)sender {
    [[HandleCall instance] endCall];
}

#pragma mark -- Headset Clicked

#pragma mark - HANDLE MQTT MESSAGE
- (void)handleUpdateListUser{
    [self requestListUserInCall];
}
- (void)handleUpdateUserCalling:(id)notification{
    NSDictionary *userInfo = [notification object];
    int userID = [[userInfo objectForKey:KEY_USER_ID] intValue];
    int status = [[userInfo objectForKey:KEY_STATUS] intValue];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userId = %d",userID];
    NSArray *filters = [arrayListUser filteredArrayUsingPredicate:predicate];
    UserInCallModel *objectUser = [filters firstObject];
    if (objectUser == nil) {
        return;
    }
    objectUser.state = status;
    [arrayListUser removeObject:objectUser];
    [arrayListUser insertObject:objectUser atIndex:0];
    [collectionUserView reloadData];
}
- (void)handleDeleteGroup:(id)notification{
    NSDictionary *jsonObj = [notification object];
    
    if ([jsonObj objectForKey:KEY_CHANNEL]) {
        NSString *strGroupName = [jsonObj objectForKey:KEY_CHANNEL];
        NSString *strGroupID   = [strGroupName substringFromIndex:1];
        int groupID            = [[strGroupID stringByTrimmingCharactersInSet:NSMutableCharacterSet.whitespaceCharacterSet] intValue];
        if (groupID == [GlobalObject instance].groupID) {
            [GlobalObject instance].uri = nil;
            [HandleCall instance].mCall.state = CALL_STATE_NONE;
            [[HandleCall instance] endCall];
            [self.navigationController popViewControllerAnimated:YES];
            
        }
    }

}
#pragma mark - COLLECTION DATASOURCE + DELEGATE
/**
 *  Calculate number of sections
 *
 *  @param collectionView view
 *
 *  @return number of sections
 */
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

// Every section has to have every cell filled, as we need to add empty cells as well to correct the spacing
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return arrayListUser.count;
}

// And now the most important one
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UserCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"userIdentifier" forIndexPath:indexPath];
    // Convert rows and columns
    UserInCallModel *userInfo = [arrayListUser objectAtIndex:indexPath.row];
    cell.labelUserName.text = userInfo.userName;
    cell.layer.borderColor = [UIColor clearColor].CGColor;
    cell.layer.borderWidth = 1.0f;
    cell.layer.cornerRadius = 4.0f;
    if (userInfo.state == STATE_OFFLINE) {
        cell.layer.borderColor = [UIColor redColor].CGColor;
        cell.layer.borderWidth = 1.0f;
    }else if (userInfo.state == STATTE_ON_CALL){
        cell.layer.borderColor = [UIColor greenColor].CGColor;
        cell.layer.borderWidth = 1.0f;
    }
    cell.labelNickName.text = userInfo.nickName;
    return cell;
}
@end
