//
//  ChatGroupFooterView.h
//  PTTLibrary
//
//  Created by phu the cong on 8/24/15.
//  Copyright (c) 2015 FCS Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ChatGroupFooterViewDelegate <NSObject>

-(void)createNewGroup:(id)sender;

@end

@interface ChatGroupFooterView : UITableViewHeaderFooterView

@property(nonatomic, weak) id<ChatGroupFooterViewDelegate>delegate;

@end
