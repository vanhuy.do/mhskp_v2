//
//  ChatGroupFooterView.m
//  PTTLibrary
//
//  Created by phu the cong on 8/24/15.
//  Copyright (c) 2015 FCS Computer Systems. All rights reserved.
//

#import "ChatGroupFooterView.h"

@implementation ChatGroupFooterView

- (IBAction)createGroup:(id)sender {
    if ([self.delegate respondsToSelector:@selector(createNewGroup:)]) {
        [self.delegate createNewGroup:sender];
    }
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
