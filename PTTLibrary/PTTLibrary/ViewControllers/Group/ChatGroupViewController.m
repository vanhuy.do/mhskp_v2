//
//  ChatGroupViewController.m
//  PTTLibrary
//
//  Created by phu the cong on 8/21/15.
//  Copyright (c) 2015 FCS Computer Systems. All rights reserved.
//

#import "ChatGroupViewController.h"
#import "ChatGroupTableViewCell.h"
#import "Utility.h"
#import "ChatGroupFooterView.h"
#import "GroupBaseModel.h"
#import "HomeViewController.h"
#import "CreateGroupViewController.h"
#import "Utility.h"
#import "MGSwipeButton.h"
#import "UIAlertView+Blocks.h"

@interface ChatGroupViewController ()<UITableViewDataSource, UITableViewDelegate, MGSwipeTableCellDelegate, ChatGroupTableViewCellDelegate, ChatGroupFooterViewDelegate, RefreshingDelegate>
{
    NSMutableArray *listGroup;
    NSIndexPath *indexPathOpenning;
}

@property (weak, nonatomic) IBOutlet UITableView *groupChatTableView;

@end

@implementation ChatGroupViewController

#pragma mark - VIEWCONOLLER LIFECYCLE
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.title = getLocalizationString(@"Chat Group");
    NotifReg(self, @selector(updateListGroup:), MQTT_UPDATE_LIST_GROUP);
    NotifReg(self, @selector(deleteGroup:), MQTT_GROUP_DELETE);
    listGroup = [[NSMutableArray alloc] init];
    _groupChatTableView.layer.cornerRadius =  6;
    [_groupChatTableView registerNib:[UINib nibWithNibName:@"ChatGroupTableViewCell" bundle:[Utility getPttLibraryBundle]] forCellReuseIdentifier:@"ChatGroupTableViewCellId"];
    [_groupChatTableView registerNib:[UINib nibWithNibName:@"ChatGroupFooterView" bundle:[Utility getPttLibraryBundle]] forHeaderFooterViewReuseIdentifier:@"ChatGroupFooterViewId"];
    _groupChatTableView.delegate = self;
    _groupChatTableView.dataSource = self;
    [self loadListGroup];
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [self.groupChatTableView addSubview:refreshControl];
}


-(void)deleteGroup:(NSNotification*)notify
{
    id jsonObj = notify.object;
    if ([jsonObj isKindOfClass:[NSDictionary class]]) {
        NSString *channel = [jsonObj objectForKey:KEY_CHANNEL];
        NSString *strGroupID   = [channel substringFromIndex:1];
        int groupID            = [[strGroupID stringByTrimmingCharactersInSet:NSMutableCharacterSet.whitespaceCharacterSet] intValue];
        for (GroupBaseModel *group in listGroup) {
            if (group.groupId == groupID) {
                [listGroup removeObject:group];
                [_groupChatTableView reloadData];
                return;
            }
        }

    }
    
}

-(void)updateListGroup:(NSNotification*)notify
{
    [self requestDataWithStart:^{
        
    } finished:^{
        [listGroup removeAllObjects];
    }];
}

- (void)refresh:(UIRefreshControl *)refreshControl {
    // Do your job, when done:
    
    [self requestDataWithStart:^{
        
    } finished:^{
        [listGroup removeAllObjects];
        [refreshControl endRefreshing];
    }];
    
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [_groupChatTableView reloadData];
}
#pragma mark - SUPPORT FUNCTION

-(void)loadListGroup
{
    [self requestDataWithStart:^{
        [self showLoading];
    } finished:^{
        [listGroup removeAllObjects];
        [self hideLoading];
    }];
}

- (void)requestDataWithStart:(void(^)(void))start finished:(void(^)(void))end{
    start();
    NSMutableDictionary *parms =[NSMutableDictionary dictionary];
    [parms setObject:[[[[GlobalObject instance] generalData] currentUser] userId] forKey:KEY_USER_ID];
    [[AFNetworkingUtility instance] requestData:API_GET_LIST_CHANNEL withParameters:parms success:^(AFHTTPRequestOperation *operation, id responseObject) {
        end();
        id data = [responseObject objectForKey:KEY_DATA];
        if ([data isKindOfClass:[NSArray class]]) {
            [[GlobalObject instance].generalData.currentUser.groups removeAllObjects];
            for (NSDictionary *userDict in data) {
                NSError* err = nil;
                GroupModel *group = [[GroupModel alloc] initWithDictionary:userDict error:&err];
                [listGroup addObject:group];
                [[GlobalObject instance].generalData.currentUser.groups addObject:group];
            }
            //Update listening channels;
//            NSMutableArray *channels = [NSMutableArray arrayWithArray:[GroupModel getListChannel:[GlobalObject instance].generalData.currentUser.groups]] ;
//            [channels addObject:[NSString stringWithFormat:@"u%@", [[[[GlobalObject instance] generalData] currentUser] userId]]];
//            
//            [[MQTTManager instance] unsubscribeChannel];
//            [MQTTManager instance].mqttArrayChannel = channels;
//            [[MQTTManager instance] subscribeChannel];

            [_groupChatTableView reloadData];
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        end();
    }];
}



#pragma mark-UITableViewDataSource, UITableViewDelegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [listGroup count];
}

-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if ([[GlobalObject instance] role]== ROLE_ADMIN) {
        ChatGroupFooterView *footerView =[tableView dequeueReusableHeaderFooterViewWithIdentifier:@"ChatGroupFooterViewId"];
        footerView.delegate = self;
        return footerView;
    }
    return nil;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ChatGroupTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ChatGroupTableViewCellId"];

    if ([[GlobalObject instance] role]== ROLE_ADMIN) {
        cell.swipeBackgroundColor = [UIColor clearColor];
        cell.rightButtons = [self createRightButtons:2];
        cell.delegate = self;
        cell.ownDelegate = self;
    }
    
    GroupBaseModel *group = [listGroup objectAtIndex:indexPath.row];
    cell.lbGroupName.text = group.groupName;
    cell.lbNumberOfGroup.text = [NSString stringWithFormat:@"%d",group.countUser];
    [cell.lbGroupStatus setHidden:YES];
    if (group.groupId == [[GlobalObject instance] groupID]) {
        [cell.lbGroupStatus setHidden:NO];
    }
     return cell;
}

-(NSArray *) createRightButtons: (int) number
{
    NSMutableArray * result = [NSMutableArray array];
    NSString* titles[2] = {getLocalizationString(@"Delete"), getLocalizationString(@"Edit")};
    UIColor * colors[2] = {[UIColor redColor], [UIColor orangeColor]};
    for (int i = 0; i < number; ++i)
    {
        MGSwipeButton * button = [MGSwipeButton buttonWithTitle:titles[i] backgroundColor:colors[i] callback:^BOOL(MGSwipeTableCell * sender){
            DLog(@"Convenience callback received (right).");
            BOOL autoHide = i != 0;
            return autoHide; //Don't autohide in delete button to improve delete expansion animation
        }];
        [result addObject:button];
    }
    return result;
}

#pragma mark - MGSwipeTableCellDelegate


-(BOOL) swipeTableCell:(MGSwipeTableCell*) cell tappedButtonAtIndex:(NSInteger) index direction:(MGSwipeDirection)direction fromExpansion:(BOOL) fromExpansion
{
    GroupModel *selectedGroup = [listGroup objectAtIndex:[[_groupChatTableView indexPathForCell:cell] row]] ;
    if (direction == MGSwipeDirectionRightToLeft) {
        //delete button
        switch (index) {
            case 0:
            {
                DLog(@"delete button");
                [UIAlertView showWithTitle:@""
                                   message:getLocalizationString(@"Do you want to delete this group?")
                         cancelButtonTitle:@"Ok"
                         otherButtonTitles:@[getLocalizationString(@"Cancel")]
                                  tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                                      if (buttonIndex == [alertView cancelButtonIndex]) {
                                          [MBProgressHUD1 showHUDAddedTo:self.view animated:YES];
                                          NSMutableDictionary *parms =[NSMutableDictionary dictionary];
                                          [parms setObject:[NSNumber numberWithInt:selectedGroup.groupId] forKey:KEY_ID];
                                          [[AFNetworkingUtility instance] requestData:API_DELETE_CHANNEL withParameters:parms success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                              [self hideLoading];
                                              [listGroup removeObject:selectedGroup];
                                              [_groupChatTableView reloadData];
                                              
                                              
                                          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                              [self hideLoading];
                                          }];

                                      } else {
                                          
                                      } }];
                
            }
                break;
            case 1:
            {
                DLog(@"edit button");
                CreateGroupViewController *createNewGroupController = [[CreateGroupViewController alloc] initWithNibName:@"CreateGroupViewController" bundle:[Utility getPttLibraryBundle]group:selectedGroup];
                createNewGroupController.delegate = self;
                [self.navigationController pushViewController:createNewGroupController animated:YES];
                
            }
                break;
            default:
                break;
        }

        return NO; //Don't autohide to improve delete expansion animation
    }
    
    return YES;
}

-(void)performActionsOnCell:(ChatGroupTableViewCell *)cell
{
    [cell showSwipe:MGSwipeDirectionRightToLeft animated:YES];
}



-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if ([[GlobalObject instance] role]== ROLE_ADMIN) {
        return 47;
    }
    return 0.01;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.01;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    for (UIViewController *retVal in self.navigationController.viewControllers) {
        if ([retVal isKindOfClass:[HomeViewController class]]) {
            GroupBaseModel *group = [listGroup objectAtIndex:indexPath.row];
            HomeViewController *homeViewController = (HomeViewController*)retVal;
            [GlobalObject instance].nextCallToUserID = @"-1";
            [homeViewController handleCallWithGroupName:group.groupName withGroupID:group.groupId];
            break;
        }
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


#pragma mark-ChatGroupFooterViewDelegate

-(void)createNewGroup:(id)sender
{
    CreateGroupViewController *createNewGroupController = [[CreateGroupViewController alloc] initWithNibName:@"CreateGroupViewController" bundle:[Utility getPttLibraryBundle]];
    createNewGroupController.delegate = self;
    [self.navigationController pushViewController:createNewGroupController animated:YES];
}


-(void)needToRefresh
{
    [self loadListGroup];
}


@end
