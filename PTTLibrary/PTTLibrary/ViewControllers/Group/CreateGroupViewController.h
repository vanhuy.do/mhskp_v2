//
//  CreateGroupViewController.h
//  PTTLibrary
//
//  Created by phu the cong on 8/25/15.
//  Copyright (c) 2015 FCS Computer Systems. All rights reserved.
//

#import <PTTLibrary/PTTLibrary.h>
#import "UserViewController.h"
#import "BaseViewController.h"

@interface CreateGroupViewController : BaseViewController

@property(weak, nonatomic) id<RefreshingDelegate>delegate;
-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil group:(GroupModel*)group;

@end
