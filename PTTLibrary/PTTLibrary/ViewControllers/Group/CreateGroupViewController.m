//
//  CreateGroupViewController.m
//  PTTLibrary
//
//  Created by phu the cong on 8/25/15.
//  Copyright (c) 2015 FCS Computer Systems. All rights reserved.
//

#import "CreateGroupViewController.h"
#import "AsignedUserTableViewCell.h"
#import "DepartmentsViewController.h"
#import "ContactInfoModel.h"
#import "UIAlertView+Blocks.h"


@interface CreateGroupViewController ()<UITableViewDataSource, UITableViewDelegate,DepartmentDelegate, RefreshingDelegate>
{
    int currentPage;
    BOOL needToLoadMore;
    DepartmentsViewController *departmentsController;
    GroupModel *selectedGroup;
}
@property (weak, nonatomic) IBOutlet UIView *firstView;
@property (weak, nonatomic) IBOutlet UIView *secondView;
@property (weak, nonatomic) IBOutlet UITableView *asignedUsersTableView;
@property (weak, nonatomic) IBOutlet UITextField *lbGroupName;

@property (weak, nonatomic) IBOutlet UILabel *lbDepartment;
@property (weak, nonatomic) IBOutlet UIButton *btnDepartment;

@property (strong, nonatomic) NSMutableArray* listAsignedUser;
@property (strong, nonatomic) DepartmentModel *assignedDepartment;
@property (assign, nonatomic) BOOL updatedData;


@end

@implementation CreateGroupViewController

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil group:(GroupModel*)group{
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        selectedGroup = group;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    NotifReg(self, @selector(updateListAssignedUser:), MQTT_UPDATE_LIST_GROUP);
//    NotifReg(self, @selector(deleteGroup:), MQTT_GROUP_DELETE);

    self.navigationItem.title = getLocalizationString(@"Add Group");
    if (selectedGroup) {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:getLocalizationString(@"Assign User") style:UIBarButtonItemStylePlain target:self action:@selector(assignUser:)];
        [self setupFormWithGroup:selectedGroup];
        
        currentPage= 1;
        needToLoadMore = NO;
        [self loadListUserInGroup];
        
    }else{
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(createNewGroup:)];
        _assignedDepartment = [[[[GlobalObject instance] generalData] departments] firstObject];
        _lbDepartment.text = [_assignedDepartment departmentName];

        
    }
    departmentsController = [[DepartmentsViewController alloc] initWithNibName:@"DepartmentsViewController" bundle:[Utility getPttLibraryBundle]];
    departmentsController.delegate = self;
    _listAsignedUser = [[NSMutableArray alloc] init];
    _firstView.layer.cornerRadius = 6;
    _secondView.layer.cornerRadius = 6;
    [_asignedUsersTableView setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];
    [_asignedUsersTableView registerNib:[UINib nibWithNibName:@"AsignedUserTableViewCell" bundle:[Utility getPttLibraryBundle]] forCellReuseIdentifier:@"AsignedUserTableViewCellId"];
    _asignedUsersTableView.delegate =self;
    _asignedUsersTableView.dataSource = self;
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [_asignedUsersTableView addSubview:refreshControl];
    
}


//-(void)deleteGroup:(NSNotification*)notify
//{
//    id jsonObj = notify.object;
//    if ([jsonObj isKindOfClass:[NSDictionary class]]) {
//        NSString *channel = [jsonObj objectForKey:KEY_CHANNEL];
//        NSString *strGroupID   = [channel substringFromIndex:1];
//        int groupID            = [[strGroupID stringByTrimmingCharactersInSet:NSMutableCharacterSet.whitespaceCharacterSet] integerValue];
//        if (groupID == selectedGroup.groupId) {
//            [UIAlertView showWithTitle:@""
//                               message:@"This group has been deleted"
//                     cancelButtonTitle:@"OK"
//                     otherButtonTitles:nil
//                              tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
//                                  if (buttonIndex == [alertView cancelButtonIndex]) {
//                                      [self.navigationController popViewControllerAnimated:YES];
//                                  }
//                                 }];
//
//        }
//        
//    }
//    
//}

-(void)updateListAssignedUser:(NSNotification*)notify
{
    id jsonObj = notify.object;
    if ([jsonObj isKindOfClass:[NSDictionary class]]) {
        NSString *channel = [jsonObj objectForKey:KEY_CHANNEL];
        NSString *strGroupID   = [channel substringFromIndex:1];
        int groupID            = [[strGroupID stringByTrimmingCharactersInSet:NSMutableCharacterSet.whitespaceCharacterSet] intValue];
        id data = [notify.object objectForKey:KEY_DATA];
        if (groupID == selectedGroup.groupId) {
            NSArray *unjoins = [data objectForKey:@"unjoins"];
            if ([unjoins containsObject:[NSNumber numberWithInt:[[GlobalObject instance].generalData.currentUser.userId intValue]]]) {
                [UIAlertView showWithTitle:@""
                                   message:@"This user does not exist in this Group!"
                         cancelButtonTitle:@"OK"
                         otherButtonTitles:nil
                                  tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                                      if (buttonIndex == [alertView cancelButtonIndex]) {
                                          [self.navigationController popToRootViewControllerAnimated:YES];
                                      }
                                  }];
            }else{
                [self refresh:nil];
            }
        }
    }
}


#pragma mark-SETTER
-(void)setUpdatedData:(BOOL)updatedData
{
    _updatedData = updatedData;
    if (_updatedData && [self.delegate respondsToSelector:@selector(needToRefresh)]) {
        [self.delegate needToRefresh];
    }
}


- (void)refresh:(UIRefreshControl *)refreshControl {
    
    currentPage = 1;
    needToLoadMore = NO;
    [self requestListUserInGroupWithStart:^{
        
    } finished:^{
        [_listAsignedUser removeAllObjects];
        [refreshControl endRefreshing];
    }];
}

-(void)loadListUserInGroup
{
    [self requestListUserInGroupWithStart:^{
        [self showLoading];
    } finished:^{
        [self hideLoading];
    }];
}

-(void)requestListUserInGroupWithStart:(void(^)(void))start finished:(void(^)(void))end
{
    start();
    NSMutableDictionary *parms =[NSMutableDictionary dictionary];
    [parms setObject:[NSNumber numberWithInt:selectedGroup.groupId] forKey:KEY_GROUP_ID];
    [parms setObject:[NSNumber numberWithBool:currentPage] forKey:KEY_PAGE];
    [[AFNetworkingUtility instance] requestData:API_GET_LIST_CONTACT_BY_CHANNEL withParameters:parms success:^(AFHTTPRequestOperation *operation, id responseObject) {
        end();
        id data = [responseObject objectForKey:KEY_DATA];
        if ([data isKindOfClass:[NSArray class]]) {
            int totalCount =  [[responseObject objectForKey:KEY_TOTAL_COUNT] intValue];
            if (([data count] + [_listAsignedUser count]) < totalCount) {
                needToLoadMore = YES;
            }
            NSError *err;
            for (id user in data) {
                ContactInfoModel *contact = [[ContactInfoModel alloc] initWithDictionary:user error:&err];
                [_listAsignedUser addObject:contact];
            }
//            [_listAsignedUser sortUsingComparator:^NSComparisonResult(ContactInfoModel *obj1, ContactInfoModel *obj2) {
//                return [obj1.nickName compare:obj2.nickName];
//            }];
            [_asignedUsersTableView reloadData];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        end();
    }];
}

-(void)setupFormWithGroup:(GroupModel*)group
{
    _lbGroupName.enabled = NO;
    _btnDepartment.enabled = NO;
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:getLocalizationString(@"Assign User") style:UIBarButtonItemStylePlain target:self action:@selector(assignUser:)];
    if (group) {
        _lbGroupName.text = group.groupName;
        _lbDepartment.text = group.departmentName;
    }

}
-(void)createNewGroup:(id)sender
{

    if ([_lbGroupName.text length] == 0) {
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    [MBProgressHUD1 showHUDAddedTo:self.view animated:YES];
    NSMutableDictionary *parms =[NSMutableDictionary dictionary];
    [parms setObject:_lbGroupName.text forKey:KEY_GROUP_NAME];
    [parms setObject:[NSNumber numberWithInt:1] forKey:KEY_GROUP_TYPE];
    [parms setObject:[NSNumber numberWithInt:_assignedDepartment.departmentId]  forKey:KEY_DEPARTMENT_ID];
    [parms setObject:@"en" forKey:KEY_LANG];
    [parms setObject:[NSNumber numberWithInt:1] forKey:KEY_ASSIGN];
    [[AFNetworkingUtility instance] requestData:API_CREATE_CHANNEL withParameters:parms success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self hideLoading];
        NSError *error;
        selectedGroup = [[GroupModel alloc] initWithDictionary:[responseObject objectForKey:KEY_DATA] error:&error];
        _lbGroupName.enabled = NO;
        _lbGroupName.layer.borderColor = [UIColor orangeColor].CGColor;
        _lbGroupName.layer.borderWidth = 2;
        _lbGroupName.layer.cornerRadius = 2;
        _btnDepartment.enabled = NO;
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:getLocalizationString(@"Assign User") style:UIBarButtonItemStylePlain target:self action:@selector(assignUser:)];
        if (selectedGroup) {
//            [[GlobalObject instance].generalData.currentUser.groups addObject:selectedGroup];
//            [[MQTTManager instance] subscribeChannel:[NSString stringWithFormat:@"g%d",selectedGroup.groupId]];
            _lbGroupName.text = selectedGroup.groupName;
            _lbDepartment.text = selectedGroup.departmentName;
        }
        self.updatedData = YES;
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self hideLoading];
    }];
}

-(void)assignUser:(id)sender
{
    UserViewController *assignUserController = [[UserViewController alloc] initWithNibName:@"UserViewController" bundle:[Utility getPttLibraryBundle] group:selectedGroup];
    assignUserController.delegate = self;
    [self.navigationController pushViewController:assignUserController animated:YES];
}

#pragma mark-RefreshingDelegate

-(void)needToRefresh
{
    [_listAsignedUser removeAllObjects];
    [self loadListUserInGroup];
    //have changes data and need to refresh
    self.updatedData = YES;
}

- (IBAction)showListDepartment:(id)sender {
    [self.navigationController.view addSubview:departmentsController.view];
    [departmentsController viewWillAppear:NO];
}


#pragma mark - UIScrollView Delegate
//Load more
- (void)scrollViewDidScroll:(UIScrollView *)scrollView;
{
    if (_asignedUsersTableView.contentOffset.y >= (_asignedUsersTableView.contentSize.height - _asignedUsersTableView.bounds.size.height) && needToLoadMore) {
        currentPage ++;
        needToLoadMore = NO;
        [self loadListUserInGroup];
    }
}

#pragma mark-UITableViewDataSource, UITableViewDelegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_listAsignedUser count];
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AsignedUserTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AsignedUserTableViewCellId"];
    ContactInfoModel *contact = [_listAsignedUser objectAtIndex:indexPath.row];
    cell.lbName.text = contact.nickName;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark-Department Delegate

-(void)selectedDepartment:(DepartmentModel *)department
{
    _assignedDepartment = department;
    _lbDepartment.text = department.departmentName;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
