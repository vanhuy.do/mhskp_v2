//
//  DepartmentsViewController.h
//  PTTLibrary
//
//  Created by phu the cong on 8/25/15.
//  Copyright (c) 2015 FCS Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DepartmentModel;

@protocol DepartmentDelegate <NSObject>

-(void)selectedDepartment:(DepartmentModel*)department;

@end


@interface DepartmentsViewController : UIViewController

@property(nonatomic, weak) id<DepartmentDelegate> delegate;

@end
