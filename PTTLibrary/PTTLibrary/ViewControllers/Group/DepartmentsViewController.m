//
//  DepartmentsViewController.m
//  PTTLibrary
//
//  Created by phu the cong on 8/25/15.
//  Copyright (c) 2015 FCS Computer Systems. All rights reserved.
//

#import "DepartmentsViewController.h"
#import "DepartmentTableViewCell.h"
#import "Utility.h"
#import "GlobalObject.h"

@interface DepartmentsViewController ()<UITableViewDelegate, UITableViewDataSource>
{
    NSInteger selectedIndex;
}
@property (weak, nonatomic) IBOutlet UITableView *selectionTableView;
@property (strong, nonatomic) NSMutableArray *listDepartment;

@end

@implementation DepartmentsViewController

-(void)viewDidLoad
{
    [_selectionTableView registerNib:[UINib nibWithNibName:@"DepartmentTableViewCell" bundle:[Utility getPttLibraryBundle]] forCellReuseIdentifier:@"DepartmentTableViewCellId"];
    selectedIndex = 0;
    self.view.alpha = 0;
    _selectionTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    _listDepartment = [NSMutableArray arrayWithArray:[[[GlobalObject instance] generalData] departments]];
    _selectionTableView.delegate = self;
    _selectionTableView.dataSource = self;
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [_selectionTableView reloadData];
    [self slideIn];
}


#pragma mark - Custom ActionSheet Methods

- (void)slideOut {
    [UIView beginAnimations:@"removeFromSuperviewWithAnimation" context:nil];
    
    // Set delegate and selector to remove from superview when animation completes
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:context:)];
    
    // Move this view to bottom of superview
    self.view.alpha = 0;
    
    [UIView commitAnimations];
}

- (void)animationDidStop:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context {
    if ([animationID isEqualToString:@"removeFromSuperviewWithAnimation"]) {
        [self.view removeFromSuperview];
    }
}

- (void)slideIn {
    
    // set up an animation for the transition between the views
    [UIView animateWithDuration:0.3 animations:^{
        self.view.alpha =1;
    } completion:^(BOOL finished) {
        
    }];
}



#pragma mark - UITableView Deleagte and DataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_listDepartment count];
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DepartmentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DepartmentTableViewCellId"];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    DepartmentModel *department = [_listDepartment objectAtIndex:indexPath.row];
    cell.selectionImageView.highlighted = selectedIndex == indexPath.row;
    cell.lbDepartmentName.text = department.departmentName;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self slideOut];
    if (selectedIndex == indexPath.row) {
        return;
    }
    selectedIndex = indexPath.row;
    if ([self.delegate respondsToSelector:@selector(selectedDepartment:)]) {
        [self.delegate selectedDepartment:[_listDepartment objectAtIndex:selectedIndex]];
    }
}





@end
