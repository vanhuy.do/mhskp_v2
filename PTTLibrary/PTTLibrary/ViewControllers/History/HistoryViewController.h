//
//  HistoryViewController.h
//  PTTLibrary
//
//  Created by Admin on 8/27/15.
//  Copyright (c) 2015 FCS Computer Systems. All rights reserved.
//

#import "BaseViewController.h"


@interface HistoryViewController : BaseViewController<UITableViewDelegate, UITableViewDataSource>{
    
    __weak IBOutlet UITableView *tableviewHistory;
    NSMutableArray *arrayHistory;
    int currentPage;
    BOOL needToLoadMore;
}

@end
