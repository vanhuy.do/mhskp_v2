//
//  HistoryViewController.m
//  PTTLibrary
//
//  Created by Admin on 8/27/15.
//  Copyright (c) 2015 FCS Computer Systems. All rights reserved.
//

#import "HistoryViewController.h"
#import "HistoryTableViewCell.h"

#import "HistoryInfoModel.h"
#import "UIImage+PTT.h"
#import "HistoryDetailViewController.h"

@interface HistoryViewController ()

@end

@implementation HistoryViewController

typedef enum
{
    TYPE_USER,
    TYPE_GROUP
} GROUP_TYPE;

#pragma mark - VIEWCONTROLLER LIFECYCLE
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = getLocalizationString(@"History_Log");
    [tableviewHistory registerNib:[UINib nibWithNibName:@"HistoryTableViewCell" bundle:[Utility getPttLibraryBundle]] forCellReuseIdentifier:@"HistoryTableViewCell"];
    arrayHistory = [[NSMutableArray alloc] init];
    currentPage = 1;
    needToLoadMore = NO;
    [self requestData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)dealloc{
    [arrayHistory removeAllObjects];
    arrayHistory = nil;
}

#pragma mark - SUPPORT FUNCTION
- (void)requestData{
    [MBProgressHUD1 showHUDAddedTo:self.view animated:YES];
    NSMutableDictionary *parms =[NSMutableDictionary dictionary];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSDate *now = [NSDate date];
    NSString *toDate = [dateFormat stringFromDate:now];
    int Hour = -[GET_SETTING_RECORD_FILE_MAX_TIME intValue];
    now = [now dateByAddingTimeInterval:60*60*Hour];
    NSString *fromDate = [dateFormat stringFromDate:now];
    [parms setObject:fromDate forKey:KEY_START_HISTORY];
    [parms setObject:toDate forKey:KEY_END_HISTORY];
    [parms setObject:[NSNumber numberWithInt:currentPage] forKey:KEY_PAGE];
    NSMutableDictionary *filter = [[NSMutableDictionary alloc] init];
    [filter setObject:[[[[GlobalObject instance] generalData] currentUser] userId] forKey:KEY_USER_ID];
    [parms setObject:filter forKey:KEY_FILTER];
    [[AFNetworkingUtility instance] requestData:API_GET_LIST_HISTORY withParameters:parms success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self hideLoading];
        DLog(@"%@", responseObject);
        id data = [responseObject objectForKey:KEY_DATA];
        int totalCount =  [[responseObject objectForKey:KEY_TOTAL_COUNT] intValue];
        if ([data isKindOfClass:[NSArray class]]) {
            if (([data count] + [arrayHistory count]) < totalCount) {
                needToLoadMore = YES;
            }
            for (NSDictionary *historyDict in data) {
                NSError* err = nil;
                HistoryInfoModel *history = [[HistoryInfoModel alloc] initWithDictionary:historyDict error:&err];
                [arrayHistory addObject:history];
                
            }
            [tableviewHistory reloadData];
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self hideLoading];
    }];
}
#pragma mark - UIScrollView Delegate
//Load more
- (void)scrollViewDidScroll:(UIScrollView *)scrollView;
{
    if (tableviewHistory.contentOffset.y >= (tableviewHistory.contentSize.height - tableviewHistory.bounds.size.height) && needToLoadMore) {
        currentPage ++;
        needToLoadMore = NO;
        [self requestData];
    }
}
#pragma mark - UITABLEVIEW DATASOUCE + DELEGATE
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arrayHistory.count;
}

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    HistoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HistoryTableViewCell"];
    HistoryInfoModel *history = [arrayHistory objectAtIndex:indexPath.row];
    cell.labelName.text = history.name;
    cell.labelTime.text = history.lastedCall;
    cell.imageviewIcon.image = [UIImage PTTImageNamed:@"ic_people"];
    if (history.groupType == TYPE_USER) {
        cell.imageviewIcon.image = [UIImage PTTImageNamed:@"avatar_general"];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    HistoryInfoModel *history = [arrayHistory objectAtIndex:indexPath.row];
    HistoryDetailViewController *historyDetailController = [[HistoryDetailViewController alloc] initWithNibName:@"HistoryDetailViewController" bundle:[Utility getPttLibraryBundle]];
    historyDetailController.isHistoryOfUser = NO;
    historyDetailController.groupID = history.groupId;
    historyDetailController.titleViewController = history.name;
    [self.navigationController pushViewController:historyDetailController animated:YES];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}
@end
