//
//  HistoryDetailViewController.h
//  PTTLibrary
//
//  Created by Admin on 8/27/15.
//  Copyright (c) 2015 FCS Computer Systems. All rights reserved.
//

#import "HistoryDetailBaseTableViewCell.h"
#import "BaseViewController.h"

@class HomeViewController;
@interface HistoryDetailViewController : BaseViewController<UITableViewDelegate, UITableViewDataSource, HistoryDetailDelegate>{
    
    
    NSMutableArray *arrayHistoryInfo;
    int currentPage;
    BOOL needToLoadMore;
    MBProgressHUD1 *hudDownload;
    AVQueuePlayer *audioPlayer;

}
@property (weak, nonatomic) IBOutlet UITableView *tableviewHistoryInfo;
@property (nonatomic, strong) NSString *titleViewController;
@property (nonatomic, strong)     HistoryDetailInfoModel *currentInfoData;
@property BOOL isHistoryOfUser;
@property int groupID;
@property int userIDA, userIDB;
@end
