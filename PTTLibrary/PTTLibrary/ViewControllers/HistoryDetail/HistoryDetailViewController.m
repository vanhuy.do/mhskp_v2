//
//  HistoryDetailViewController.m
//  PTTLibrary
//
//  Created by Admin on 8/27/15.
//  Copyright (c) 2015 FCS Computer Systems. All rights reserved.
//

#import "HistoryDetailViewController.h"
#import "HistoryDetailLeftTableViewCell.h"
#import "HistoryDetailRightTableViewCell.h"
#import "HistoryDetailInfoModel.h"
#import "HistoryDetailListInfoModel.h"
#import "UIImage+PTT.h"
#import "UIAlertView+Blocks.h"
#import "SettingConstants.h"

@interface HistoryDetailViewController ()

@end

@implementation HistoryDetailViewController
@synthesize currentInfoData;
@synthesize tableviewHistoryInfo;
#pragma mark - VIEWCONTROLLER LIFECYCLE

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = self.titleViewController;
    arrayHistoryInfo = [[NSMutableArray alloc] init];
    [tableviewHistoryInfo registerNib:[UINib nibWithNibName:@"HistoryDetailLeftTableViewCell" bundle:[Utility getPttLibraryBundle]] forCellReuseIdentifier:@"HistoryDetailLeftTableViewCell"];
    [tableviewHistoryInfo registerNib:[UINib nibWithNibName:@"HistoryDetailRightTableViewCell" bundle:[Utility getPttLibraryBundle]] forCellReuseIdentifier:@"HistoryDetailRightTableViewCell"];
    [self reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [audioPlayer pause];
    audioPlayer = nil;
}
- (void)dealloc{
    
}
#pragma mark - SUPPORT FUNCTION
- (void)playAudio{
    __weak typeof(self) weakSelf = self;
    [audioPlayer addPeriodicTimeObserverForInterval:CMTimeMake(1, 2)
                                              queue:dispatch_get_main_queue()
                                         usingBlock:^(CMTime time)
     {
         dispatch_async(dispatch_get_main_queue(), ^{
             NSTimeInterval currentTime = CMTimeGetSeconds(time);
             NSLog(@" Capturing Time :%f ",currentTime);
             [weakSelf.currentInfoData setCurrentDuration:currentTime];
             [weakSelf.tableviewHistoryInfo reloadData];
         });
         
         
     }];
    [audioPlayer play];
}
- (void)playSoundWithPath:(NSString*)path{
    if (audioPlayer == nil) {
        AVPlayerItem *playerItem = [AVPlayerItem playerItemWithURL:[NSURL fileURLWithPath:path]];
        audioPlayer = [AVQueuePlayer queuePlayerWithItems:[NSArray arrayWithObject:playerItem]];
        [self playAudio];
        return;
    }
    AVPlayerItem *item = [audioPlayer.items firstObject];
    if (item) {
        AVAsset *currentPlayerAsset = item.asset;
        NSURL *url = [(AVURLAsset *)currentPlayerAsset URL];
        
        AVAsset *avAsset = [AVAsset assetWithURL:[NSURL fileURLWithPath:path]];
        NSURL *urlPath = [(AVURLAsset *)avAsset URL];
        if ([urlPath isEqual:url]) {
            [self playAudio];
            return;
        }
    }
    [audioPlayer pause];
    [audioPlayer removeAllItems];
    AVAsset *avAsset = [AVAsset assetWithURL:[NSURL fileURLWithPath:path]];
    AVPlayerItem *avPlayerItem =[[AVPlayerItem alloc]initWithAsset:avAsset];
    [audioPlayer insertItem:avPlayerItem afterItem:nil];
    [self playAudio];
}

- (void)reloadData{
    [MBProgressHUD1 showHUDAddedTo:self.view animated:YES];
    NSMutableDictionary *parms =[NSMutableDictionary dictionary];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSDate *now = [NSDate date];
    NSString *toDate = [dateFormat stringFromDate:now];
    int Hour = -[GET_SETTING_RECORD_FILE_MAX_TIME intValue];
    now = [now dateByAddingTimeInterval:60*60*Hour];
    NSString *fromDate = [dateFormat stringFromDate:now];
    [parms setObject:fromDate forKey:KEY_START_HISTORY];
    [parms setObject:toDate forKey:KEY_END_HISTORY];
    [parms setObject:[NSNumber numberWithInt:currentPage] forKey:KEY_PAGE];
    NSString *url = API_GET_LIST_HISTORY_DETAIL;
    if (self.isHistoryOfUser) {
        url = API_GET_LIST_HISTORY_DETAIL_A_B;
        [parms setObject:[NSNumber numberWithInt:self.userIDA] forKey:KEY_USER_ID_A];
        [parms setObject:[NSNumber numberWithInt:self.userIDB] forKey:KEY_USER_ID_B];
    }else{
        NSMutableDictionary *filter = [[NSMutableDictionary alloc] init];
        [filter setObject:[NSNumber numberWithInt:self.groupID] forKey:KEY_GROUP_ID];
        [parms setObject:filter forKey:KEY_FILTER];
    }
    [[AFNetworkingUtility instance] requestData:url withParameters:parms success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self hideLoading];
        DLog(@"%@", responseObject);
        id data = [responseObject objectForKey:KEY_DATA];
        int totalCount =  [[responseObject objectForKey:KEY_TOTAL_COUNT] intValue];
        if ([data isKindOfClass:[NSArray class]]) {
            if (([data count] + [arrayHistoryInfo count]) < totalCount) {
                needToLoadMore = YES;
            }
            
            for (NSDictionary *historyDict in data) {
                NSError* err = nil;
                HistoryDetailInfoModel *history = [[HistoryDetailInfoModel alloc] initWithDictionary:historyDict error:&err];
                NSArray *arrayDate = [history.startDate componentsSeparatedByString:@" "];
                NSString *startDate = [arrayDate objectAtIndex:0];
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"startDate = %@",startDate];
                NSArray *filters = [arrayHistoryInfo filteredArrayUsingPredicate:predicate];
                HistoryDetailListInfoModel *object;
                if ([filters count] == 0) {
                    object = [[HistoryDetailListInfoModel alloc] init];
                    [arrayHistoryInfo insertObject:object atIndex:0];
                }else{
                    object = [filters firstObject];
                }
                [object addHistoryObject:history];
                
            }
            [tableviewHistoryInfo reloadData];
            HistoryDetailListInfoModel *object = [arrayHistoryInfo lastObject];
            NSIndexPath *scrollIndexPath = [NSIndexPath indexPathForRow:(object.arrayHistoryDetail.count - 1) inSection:(arrayHistoryInfo.count - 1)];
            [tableviewHistoryInfo scrollToRowAtIndexPath:scrollIndexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self hideLoading];
    }];
}
#pragma mark - UITABLEVIEW DATASOURCE + DELEGATE
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    HistoryDetailListInfoModel *historyList = [arrayHistoryInfo objectAtIndex:section];
    return historyList.arrayHistoryDetail.count;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return arrayHistoryInfo.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 30;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    HistoryDetailListInfoModel *historyList = [arrayHistoryInfo objectAtIndex:section];
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(tableviewHistoryInfo.frame), 30)];
    headerView.backgroundColor = ColorClear;
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:headerView.bounds];
    imageView.image = [UIImage PTTImageNamed:@"bg_main_activity"];
    [headerView addSubview:imageView];
    UILabel *content = [[UILabel alloc] initWithFrame:CGRectMake(13, 0, CGRectGetWidth(headerView.frame), CGRectGetHeight(headerView.frame))];
    content.text = historyList.startDate;
    content.textColor = ColorWhite;
    content.backgroundColor = ColorClear;
    [headerView addSubview:content];
    return headerView;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 80;
}
// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    HistoryDetailListInfoModel *historyList = [arrayHistoryInfo objectAtIndex:indexPath.section];
    HistoryDetailInfoModel *historyInfo = [historyList.arrayHistoryDetail objectAtIndex:indexPath.row];
    HistoryDetailBaseTableViewCell *cell;
    if ([historyInfo.userName isEqualToString:[[[[GlobalObject instance] generalData] currentUser] userName]]) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"HistoryDetailRightTableViewCell"];
    }else{
        cell = [tableView dequeueReusableCellWithIdentifier:@"HistoryDetailLeftTableViewCell"];
    }
    cell.delegate = self;
    [cell setData:historyInfo];
    
    return cell;
}
#pragma mark - delegate of cell
- (void)seekingMedia:(HistoryDetailInfoModel*)infoData withValue:(float)value{
//    AVPlayerItem *item = audioPlayer.currentItem;
    
    int32_t timeScale = audioPlayer.currentItem.asset.duration.timescale;
    CMTime time = CMTimeMakeWithSeconds(value, timeScale);
    [audioPlayer seekToTime:time toleranceBefore:kCMTimeZero toleranceAfter:kCMTimeZero];
    
}
- (void)didClickPlay:(HistoryDetailInfoModel *)infoData{
    currentInfoData = infoData;
    for (HistoryDetailListInfoModel *retVal in arrayHistoryInfo) {
        for (HistoryDetailInfoModel *retVal1 in retVal.arrayHistoryDetail) {
            if (retVal1 != infoData) {
                if (infoData.getPlay) {
                    [audioPlayer pause];
                }
                [retVal1 setPlay:NO];
            }
        }
    }
    [tableviewHistoryInfo reloadData];
    if (infoData.getPlay) {

        NSString *path = [[self getRecordFolder] stringByAppendingPathComponent:[NSString stringWithFormat:@"%d|%@.wav", infoData.userId, infoData.startDate]];
        if ([[NSFileManager defaultManager] fileExistsAtPath:path]) {
            [self playSoundWithPath:path];
        }else{
            [self downloadAndPlayFile:infoData];
        }
    }else{
        [audioPlayer pause];
        
    }
}
- (void)showDownloading{
    hudDownload = [MBProgressHUD1 showHUDAddedTo:self.view animated:YES];
    hudDownload.mode = MBProgressHUDModeDeterminateHorizontalBar1;
    
    hudDownload.labelText = getLocalizationString(@"Downloading");
    hudDownload.progress = 0;
}
- (NSString*)getRecordFolder{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"RecordFile"];
    if (![[NSFileManager defaultManager] isExecutableFileAtPath:path]) {
        NSError * error = nil;
        [[NSFileManager defaultManager] createDirectoryAtPath:path
                                  withIntermediateDirectories:YES
                                                   attributes:nil
                                                        error:&error];
        if (error != nil) {
            NSLog(@"error creating directory: %@", error);
            //..
        }
    }
    return path;
}
- (void)downloadAndPlayFile:(HistoryDetailInfoModel *)infoData{
    [self showDownloading];
    NSString *urlOfMedia = [NSString stringWithFormat:@"http://%@:%@%@",GET_SER_IP, GET_SER_PTT_PORT, infoData.mediaUrl];
    NSURL *URL = [NSURL URLWithString:urlOfMedia];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request] ;
    
    NSString *path = [[self getRecordFolder] stringByAppendingPathComponent:[NSString stringWithFormat:@"%d|%@.wav", infoData.userId, infoData.startDate]];
    
    operation.outputStream = [NSOutputStream outputStreamToFileAtPath:path append:NO];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        DLog(@"Successfully downloaded file to %@", path);
        [self cleanHistoryFile:path];
        [self playSoundWithPath:path];
        [MBProgressHUD1 hideAllHUDsForView:self.view animated:YES];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DLog(@"Error: %@", error);
        [MBProgressHUD1 hideAllHUDsForView:self.view animated:YES];
        
        [UIAlertView showWithTitle:nil
                           message:getLocalizationString(@"download_fail")
                 cancelButtonTitle:getLocalizationString(@"Ok")
                 otherButtonTitles:nil
                          tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                              
                }];
    }];
    [operation setDownloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead) {
        dispatch_async(dispatch_get_main_queue(), ^{
            float progress = (float)totalBytesRead/(float)totalBytesExpectedToRead;
            hudDownload.progress = progress;
        });

    }];
    [operation start];
}
- (unsigned long long int)folderSize:(NSString *)folderPath {
    NSArray *filesArray = [[NSFileManager defaultManager] subpathsOfDirectoryAtPath:folderPath error:nil];
    NSEnumerator *filesEnumerator = [filesArray objectEnumerator];
    NSString *fileName;
    unsigned long long int fileSize = 0;
    
    while (fileName = [filesEnumerator nextObject]) {
        NSString *fileInFolder = [folderPath stringByAppendingPathComponent:fileName];
        NSArray *arrFileInfo = [fileName componentsSeparatedByString:@"|"];
        NSString *strDateFile = arrFileInfo.count > 1 ? [arrFileInfo objectAtIndex:1] : @"";
        if (strDateFile.length > 4) {
            strDateFile = [strDateFile substringToIndex:strDateFile.length-4];
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat: @"dd-MM-yyyy HH:mm:ss"];
            NSTimeInterval timeOfFile = [[dateFormat dateFromString:strDateFile] timeIntervalSinceNow];
            if (-timeOfFile >= [GET_SETTING_RECORD_FILE_MAX_TIME intValue]*3600) {
                [self deleteFileAtPath:fileInFolder];
                continue;
            }
        }
        unsigned long long fileSizeCurrent = [[[NSFileManager defaultManager] attributesOfItemAtPath:fileInFolder error:nil] fileSize];
//        NSDictionary *fileDictionary = [[NSFileManager defaultManager] attributesOfFileSystemForPath:fileInFolder error:nil];
        fileSize += fileSizeCurrent;
    }
    
    return fileSize;
}
- (void)deleteFileAtPath:(NSString*)filePath{
    NSError *error;
    BOOL success = [[NSFileManager defaultManager] removeItemAtPath:filePath error:&error];
    if (!success) {
        NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
    }
}
- (void)deleteAllFileRecord:(NSString*)Exfile{

    NSString *pathFolder = [self getRecordFolder];
    NSError * error;
    NSArray * directoryContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:pathFolder error:&error];
    
    for (NSString *file in directoryContents) {
        if (![file isEqualToString:Exfile]) {
            NSString *fileInFolder = [[self getRecordFolder] stringByAppendingPathComponent:file];
            [self deleteFileAtPath:fileInFolder];
        }
    }
}
- (void)cleanHistoryFile:(NSString*)path{
    NSString *pathFolder = [self getRecordFolder];
    
    float sizefolder =  [self folderSize:pathFolder]/1048576;
    if (sizefolder >= [GET_SETTING_RECORD_FILE_MAX_SIZE intValue]) {
        [self deleteAllFileRecord:path];
    }
}
@end
