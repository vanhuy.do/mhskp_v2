//
//  HomeViewController.h
//  PTTLibrary
//
//  Created by Nguyen Minh Duc on 8/10/15.
//  Copyright (c) 2015 FCS Computer Systems. All rights reserved.
//

#import <PTTLibrary/PTTLibrary.h>
#import "HomeMenuTableViewCell.h"
#import "DepartmentModel.h"
#import <AVFoundation/AVFoundation.h>
#import "BaseViewController.h"

@protocol PttDelegate <NSObject>

-(void)exitPtt;

@end

@class MQTTManager;

@interface HomeViewController : BaseViewController<AVAudioSessionDelegate>{
    MQTTManager *mqttManager;
    BOOL isPlayBeep;
    BOOL callDefaultGroup;
}
@property (nonatomic, strong) AVQueuePlayer *audioPlayer;
@property (nonatomic, strong) AVQueuePlayer *audioCallPlayer;
@property NSTimeInterval currentTimeTouch;
@property int StateOfCall;
//@property (nonatomic, strong) NSString *userID, *userName;
@property BOOL isLogged;
@property(nonatomic,strong) NSTimer* mCanTalkCountDown;
@property (nonatomic, weak) id<PttDelegate>delegate;


- (void)initBasicInfo:(NSString*)userid withUserName:(NSString*)username withLanguage:(NSString*)language;

- (void)checkLogin:(void (^)(NSString* errMsg))success;
//- (void)callWithGroup:(NSString*)groupId;
- (void)handleCallWithGroupName:(NSString*)groupName withGroupID:(int)groupID;
- (void)CanTalkCountDownStart;
- (void)CanTalkCountDownStop;
- (void)playSound:(NSString*)fileName isPlay:(BOOL)isPlay;
//- (void)playSoundWithPath:(NSString*)path;
- (void)handleTalking;
- (void)handleEndTalk;
- (void)exitPushtoTalk;
- (void)releaseWhenLogout;
@end
