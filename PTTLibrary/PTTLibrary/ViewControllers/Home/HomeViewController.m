//
//  HomeViewController.m
//  PTTLibrary
//
//  Created by Nguyen Minh Duc on 8/10/15.
//  Copyright (c) 2015 FCS Computer Systems. All rights reserved.
//

#import "HomeViewController.h"
#import "Language.h"
#import "LanguageManager.h"
#import "CallViewController.h"
#import "GlobalObject.h"
#import "SettingViewController.h"
#import "AFNetworkingUtility.h"
#import "UserViewController.h"
#import "ChatGroupViewController.h"
#import <AudioToolbox/AudioToolbox.h>
#import "UIImage+PTT.h"
#import "UIAlertView+Blocks.h"
#import "HistoryViewController.h"
#import <CoreTelephony/CTCallCenter.h>
#import <CoreTelephony/CTCall.h>
#import "SettingConstants.h"
#import "MQTTManager.h"
#import "MQTTKit/MQTTKit.h"
#import "HandleCall.h"
#import <MediaPlayer/MediaPlayer.h>

// Xib Name
#define HOME_MENU_TABLE_CELL_NAME  @"HomeMenuTableViewCell"

// Menu Index
#define HISTORY_LOG_INDEX 0
#define USER_INDEX 1
#define CHAT_GROUP_INDEX 2
#define EXIT_PTT_INDEX 3

@interface HomeViewController () <UITableViewDelegate,UITableViewDataSource>{
    NSArray *_homeMenuArray;
    __weak IBOutlet UITableView *menuTableview;
    HandleCall *handleCall;
}

@end

@implementation HomeViewController
@synthesize StateOfCall;
@synthesize mCanTalkCountDown;
@synthesize currentTimeTouch;
@synthesize audioPlayer;
//@synthesize userID, userName;

#pragma mark - Viewcontroller lifecycle
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    isPlayBeep = GET_SETTING_NOTIFY_BEEP;
}
- (void)viewDidLoad {

    [super viewDidLoad];
    
    
    handleCall = [HandleCall instance];
    mqttManager = [MQTTManager instance];
    [self stopAllMedia];
    // Do any additional setup after loading the view from its nib.
    self.title = [GlobalObject instance].generalData.currentUser.nickName;
    menuTableview.layer.cornerRadius = 6;
    _homeMenuArray = @[
                       @{NAME: getLocalizationString(@"History Logs"), IMAGE_PATH: @""},
                       @{NAME: getLocalizationString(@"User"), IMAGE_PATH: @""},
                       @{NAME: getLocalizationString(@"Chat Group"), IMAGE_PATH: @""},
                       @{NAME: getLocalizationString(@"Exit Push-to-Talk"), IMAGE_PATH: @""}
                       ];
//    NotifReg(self, @selector(handleCallMessage:), MQTT_CALL_MESSAGE);
    NotifReg(self, @selector(handleMsgOkToTalk:), MSG_OK_TO_TALK);
    
    NotifReg(self, @selector(handleLoginDuplicate:), MQTT_LOGIN_DUPLICATE);
    NotifReg(self, @selector(handleDisconnectSuccess:), DISCONNECT_SUCCESS);
    NotifReg(self, @selector(handleMakeCallSuccess:), MAKE_CALL_SUCCESS);
    NotifReg(self, @selector(handleMakeCallWithInfoSuccess:), MAKE_CALL_INFO);
    
    NotifReg(self, @selector(handlemakeCallFromDelegate:), MQTT_MAKE_CALL);
    NotifReg(self, @selector(handleUpdateListChannel:), MQTT_UPDATE_LIST_GROUP);
    NotifReg(self, @selector(deleteGroup:), MQTT_GROUP_DELETE);
    NotifReg(self, @selector(exitPushtoTalk), MQTT_UNASSIGN_USER);
    NotifReg(self, @selector(subcriblesuccess), MQTT_SUBSCRIBE_SUCCESS);
    NotifReg(self, @selector(audioRouteChangeListenerCallback:), AVAudioSessionRouteChangeNotification);
    NotifReg(self, @selector(shutdownSound), @"SHUTDOWN");
    NotifReg(self, @selector(restartSound), @"RESTART");
    NotifReg(self, @selector(playCallSound), @"PLAY_CALL_SOUND");
    NotifReg(self, @selector(stopCallSound), @"STOP_CALL_SOUND");
    
    [self handleInternetConnection];
}
- (void)subcriblesuccess{
    if([GlobalObject instance].defaultGroup && !callDefaultGroup){
        [self handleCallWithGroupName:[GlobalObject instance].defaultGroup.groupName withGroupID:[GlobalObject instance].defaultGroup.groupId];
    }
    callDefaultGroup = YES;
}
- (void)handleInternetConnection{
    NSURL *baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@", GET_SER_IP]];
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:baseURL];
    
    NSOperationQueue *operationQueue = manager.operationQueue;
    [manager.reachabilityManager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        switch (status) {
            case AFNetworkReachabilityStatusReachableViaWWAN:
            case AFNetworkReachabilityStatusReachableViaWiFi:
                if(![MQTTManager instance].clientMQTT.connected){
                    [self startMQTT];
                }
                [self shutdownSound];
                [self restartSound];
                [operationQueue setSuspended:NO];
                break;
            case AFNetworkReachabilityStatusNotReachable:
            default:
                [[MQTTManager instance].clientMQTT destroyMQTT];
                [operationQueue setSuspended:YES];
                break;
        }
    }];
    
    [manager.reachabilityManager startMonitoring];
}
- (void)handleUpdateListChannel:(NSNotification*)notification{
    //Update listening channels
    id data = [notification.object objectForKey:KEY_DATA];
    if ([data isKindOfClass:[NSDictionary class]]) {
        id groupId = [data objectForKey:KEY_GROUP_ID];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"groupId = %@",groupId];
        NSArray *filter = [[GlobalObject instance].generalData.currentUser.groups filteredArrayUsingPredicate:predicate];
        if ([filter count]) {
            NSArray *unjoins = [data objectForKey:@"unjoins"];
            if ([unjoins containsObject:[NSNumber numberWithInt:[[GlobalObject instance].generalData.currentUser.userId intValue]]]) {
                [[GlobalObject instance].generalData.currentUser.groups removeObject:[filter firstObject]];
                [[MQTTManager instance] unsubscribeChannel:[@"g" stringByAppendingString:[groupId stringValue]]];
            }
        }else
        {
            GroupModel *newGroup = [[GroupModel alloc] init];
            newGroup.groupId = [groupId intValue];
            [[GlobalObject instance].generalData.currentUser.groups addObject:newGroup];
            [[MQTTManager instance] subscribeChannel:[@"g" stringByAppendingString:[groupId stringValue]]];
        }
    }
}

-(void)deleteGroup:(NSNotification*)notify
{
    id jsonObj = notify.object;
    if ([jsonObj isKindOfClass:[NSDictionary class]]) {
        NSString *channel = [jsonObj objectForKey:KEY_CHANNEL];
        NSString *strGroupID   = [channel substringFromIndex:1];
        int groupID            = [[strGroupID stringByTrimmingCharactersInSet:NSMutableCharacterSet.whitespaceCharacterSet] intValue];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"groupId = %d",groupID];
        NSArray *filter = [[GlobalObject instance].generalData.currentUser.groups filteredArrayUsingPredicate:predicate];
        [[GlobalObject instance].generalData.currentUser.groups removeObject:[filter firstObject]];
        [[MQTTManager instance] unsubscribeChannel:[NSString stringWithFormat:@"g%d",groupID]];
    }
}

// Override method
-(void)setupNavigationBarItems
{
    
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)dealloc{
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark -- Private Methods

- (void)checkLogin:(void (^)(NSString* errMsg))success{
    [[AFNetworkingUtility instance] requestData:API_CHECK_USER withParameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        DLog(@"%@", responseObject);
        if ([responseObject objectForKey:KEY_DATA] == nil || [[responseObject objectForKey:KEY_DATA] objectForKey:KEY_CURRENT_USER] == nil) {
            success(@"Can't get user info");
            return;
        }
        NSError* err = nil;
        NSDictionary *responseData = [responseObject objectForKey:KEY_DATA];
        GlobalObject * globalObject = [GlobalObject instance];
        globalObject.generalData = [[GeneralDataModel alloc] initWithDictionary:responseData error:&err];
        
        globalObject.generalData.departments  = (NSArray<DepartmentModel>*)[globalObject.generalData.departments sortedArrayUsingComparator:^NSComparisonResult(DepartmentModel *obj1, DepartmentModel *obj2) {
            return [obj1.departmentName compare:obj2.departmentName];
        }];
        // Set Token in Header for other request
        [[AFNetworkingUtility instance] setHeaderWithValue:globalObject.generalData.access_token withKey:KEY_TOKEN];
        [self setRole:globalObject.generalData.currentUser.role];
        
        // Get joined groups
        NSMutableArray *arrayChannel = [[NSMutableArray alloc] init];
        [arrayChannel addObject:[NSString stringWithFormat:@"u%@", globalObject.generalData.currentUser.userId]];
        [arrayChannel addObject:@"PING_IOS"];
        globalObject.defaultGroup = [GroupModel findDefaultGroup:globalObject.generalData.currentUser.groups];
        
        [arrayChannel addObjectsFromArray:[GroupModel getListChannel:globalObject.generalData.currentUser.groups]];
        [MQTTManager instance].mqttArrayChannel = arrayChannel;
        
        // Check permission
        if ([GlobalObject instance].role == ROLE_NO_PERMISSION) {
            //This user do not have permission to login PTT
            success(getLocalizationString(@"Not_Have_Login_Permisstion"));
            return;
        }
        //TODO: must check open 1 instance PTT
//        int result = [self startPjSip];
//        if (result != 0) {
//            success(getLocalizationString(@"ptt_is_exists"));
//            return;
//        }
        globalObject.pingTimer = [NSTimer timerWithTimeInterval:PING_TIMER
                                                           target:self
                                                         selector:@selector(pingPTTServer:)
                                                         userInfo:nil
                                                          repeats:YES];
        self.isLogged = YES;
        success(nil);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        switch ([operation.response statusCode]) {
            case HTTP_STATUS_FAIL_401:
                success(getLocalizationString(@"Not_Have_Login_Permisstion"));
                break;
            case HTTP_STATUS_FAIL_403:
                success(getLocalizationString(@"Not_Have_Login_Permisstion"));
                break;
            default:
                success([NSString stringWithFormat:@"Error code %ld",(long)[operation.response statusCode]]);
                break;
        }
    }];
    
}

- (void)logout:(void (^)(NSString* errMsg))success{
    [[AFNetworkingUtility instance] requestData:API_LOGOUT withParameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        DLog(@"%@", responseObject);
        success(nil);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        success(nil);
    }];
    
}
- (void)setRole:(NSString*)role{
    BOOL isPlay = NO;
    BOOL isEdit = NO;
    BOOL isView = NO;
    BOOL isNotPermision = NO;
    NSArray *arrayRole = [role componentsSeparatedByString:@"|"];
    for (NSString *retval in arrayRole) {
        if ([retval isEqualToString:ROLE_MOBILE_PERMISSION_EDIT]) {
            isEdit = YES;
        }else if([retval isEqualToString:ROLE_MOBILE_PERMISSION_PLAY]){
            isPlay = YES;
        }else if([retval isEqualToString:ROLE_MOBILE_PERMISSION_VIEW]){
            isView = YES;
        }else if([retval isEqualToString:ROLE_NO_ROLES]){
            isNotPermision = YES;
        }
    }
    if (isEdit) {
        [GlobalObject instance].role = ROLE_ADMIN;
    }else if(isView){
        [GlobalObject instance].role = ROLE_USER;
    }else if(isNotPermision){
        [GlobalObject instance].role = ROLE_NO_PERMISSION;
    }
    [GlobalObject instance].isPlayHistory = isPlay;
}

- (void)releaseWhenLogout{

    NotifUnregAll(self);
//    [mqttManager.clientMQTT destroyMQTT];
    [[UIApplication sharedApplication] endReceivingRemoteControlEvents];
    [self resignFirstResponder];
    [[AFNetworkingUtility instance] setHeaderWithValue:nil withKey:KEY_TOKEN];
    self.isLogged = NO;
    [handleCall endCall];
    [handleCall unregisterData];
    [[GlobalObject instance] clearData];
    [self logout:^(NSString *errMsg) {
        NotifPost(@"EXIT_PTT");
//        if ([self.delegate respondsToSelector:@selector(exitPtt)]) {
//            [self.delegate exitPtt];
//        }
    }];
}

#pragma mark -- Home Menu Table
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return  [_homeMenuArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *homeMenuTableIdentifier = HOME_MENU_TABLE_CELL_NAME;
    
    HomeMenuTableViewCell *cell = (HomeMenuTableViewCell *)[tableView dequeueReusableCellWithIdentifier:homeMenuTableIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[Utility getPttLibraryBundle] loadNibNamed:HOME_MENU_TABLE_CELL_NAME owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    cell.menuNameLable.text = [_homeMenuArray[indexPath.row] objectForKey:NAME];
    [cell.bgImage setHidden:NO];
    [cell.contentView setBackgroundColor:[UIColor clearColor]];
    switch (indexPath.row) {
        case 0:
            [cell.menuImage setImage:[UIImage PTTImageNamed:@"ic_action_alarm"]];
            break;
        case 1:
            [cell.menuImage setImage:[UIImage PTTImageNamed:@"ic_action_contacts"]];
            break;
        case 2:
            [cell.menuImage setImage:[UIImage PTTImageNamed:@"ic_action_channels"]];
            break;
        case 3:
        {
            [cell.bgImage setHidden:YES];
            [cell.menuImage setImage:[UIImage PTTImageNamed:@"ic_action_exit" ]];
            [cell.contentView setBackgroundColor:[UIColor orangeColor]];
        }
            break;
            
        default:
            break;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (indexPath.row) {
        case USER_INDEX:{
            UserViewController *userVC = [[UserViewController alloc] initWithNibName:@"UserViewController" bundle:[Utility getPttLibraryBundle]];
            userVC.isUserList = YES;
            [self.navigationController pushViewController:userVC animated:YES];
        }
            break;
            
        case CHAT_GROUP_INDEX:{
            ChatGroupViewController *groupCallVC = [[ChatGroupViewController alloc] initWithNibName:@"ChatGroupViewController" bundle:[Utility getPttLibraryBundle]];
            [self.navigationController pushViewController:groupCallVC animated:YES];
        }
            break;
    
        case HISTORY_LOG_INDEX:
        {
            HistoryViewController *historyController = [[HistoryViewController alloc] initWithNibName:@"HistoryViewController" bundle:[Utility getPttLibraryBundle]];
            [self.navigationController pushViewController:historyController animated:YES];
        }
            break;
        case EXIT_PTT_INDEX:{
            [UIAlertView showWithTitle:getLocalizationString(@"Confirm")
                               message:getLocalizationString(@"Do you want to exit?")
                     cancelButtonTitle:getLocalizationString(@"Ok")
                     otherButtonTitles:@[getLocalizationString(@"Cancel")]
                              tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                                  if (buttonIndex == [alertView cancelButtonIndex]) {
                                      [MBProgressHUD1 showHUDAddedTo:self.view animated:YES];
                                      [self exitPushtoTalk];
                                  } else {
                                      
                                  } }];

        }
            break;
            
        default:
            break;
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}
- (void)exitPushtoTalk{
    callDefaultGroup = NO;
    [self releaseWhenLogout];
    
    [self dismissViewControllerAnimated:YES completion:^{
        [MBProgressHUD1 hideAllHUDsForView:self.view animated:YES];
    }];
    
}
#pragma mark - Support Function
/**
 *  Init basic information of user
 */
- (void)initBasicInfo:(NSString*)userid withUserName:(NSString*)username withLanguage:(NSString*)language{
//    self.userID = userid;
//    self.userName = username;
    self.selectedLanguageName = language;
    [GlobalObject instance].eConnectUserId = userid;
//    [GlobalObject instance].generalData.currentUser.userName = username;
}



- (NSString*)createSendInviteMessage{
    NSMutableDictionary *dictMessage = [[NSMutableDictionary alloc] init];
    [dictMessage setObject:[NSNumber numberWithInteger:4] forKey:NOTIFICATION_TYPE];
    NSString *groupID = [NSString stringWithFormat:@"g%d", [GlobalObject instance].groupID];
    [dictMessage setObject:groupID forKey:KEY_CHANNEL];
    
    NSMutableDictionary *dictDataMessage = [[NSMutableDictionary alloc] init];
    
    [dictDataMessage setObject:[GlobalObject instance].groupName forKey:KEY_CALL_GROUP];
    [dictDataMessage setObject:[GlobalObject instance].generalData.currentUser.userId forKey:KEY_CALLER_ID];
    [dictDataMessage setObject:[NSNumber numberWithInteger:[GlobalObject instance].groupID] forKey:KEY_TO];
    [dictDataMessage setObject:[GlobalObject instance].generalData.currentUser.userName forKey:KEY_CALLER];
    [dictDataMessage setObject:@"1" forKey:KEY_GROUP_TYPE];
    
    [dictMessage setObject:dictDataMessage forKey:KEY_DATA];
    
    NSString *message = [Utility parseObjectToJson:dictMessage];
    return message;
}
- (void)playSound:(NSString*)fileName isPlay:(BOOL)isPlay{
    if (!isPlay) {
        return;
    }
    NSString *path = [[Utility getPttLibraryBundle]
                      pathForResource:fileName ofType:nil];
    [audioPlayer pause];
    [audioPlayer removeAllItems];
    AVAsset *avAsset = [AVAsset assetWithURL:[NSURL fileURLWithPath:path]];
    AVPlayerItem *avPlayerItem =[[AVPlayerItem alloc]initWithAsset:avAsset];
    [audioPlayer insertItem:avPlayerItem afterItem:nil];
    [audioPlayer play];
}

- (void)playCallSound{
    
    NSString *path = [[Utility getPttLibraryBundle] pathForResource:@"sound" ofType:@"caf"];
    [audioPlayer pause];
    [audioPlayer removeAllItems];
    AVAsset *avAsset = [AVAsset assetWithURL:[NSURL fileURLWithPath:path]];
    AVPlayerItem *avPlayerItem =[[AVPlayerItem alloc]initWithAsset:avAsset];
    [audioPlayer insertItem:avPlayerItem afterItem:nil];
    [audioPlayer play];
    AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
}
- (void)stopCallSound{
    [self playSound:@"point1sec.mp3" isPlay:YES];
}
- (NSURL *)urlOfCurrentlyPlayingInPlayer:(AVPlayer *)player{
    // get current asset
    AVAsset *currentPlayerAsset = player.currentItem.asset;
    // make sure the current asset is an AVURLAsset
    if (![currentPlayerAsset isKindOfClass:AVURLAsset.class]) return nil;
    // return the NSURL
    return [(AVURLAsset *)currentPlayerAsset URL];
}


- (void)CanTalkCountDownStart{
    if (!mCanTalkCountDown) {
        mCanTalkCountDown = [NSTimer scheduledTimerWithTimeInterval:3.0f
                                                             target:self
                                                           selector:@selector(requestTalkFail:)
                                                           userInfo:nil
                                                            repeats:YES];
    }
}
- (void)requestTalkFail:(NSTimer *)timer{
    if (StateOfCall == STATE_REQUESTING) {
        StateOfCall = STATE_NONE;
        [self playSound:SOUND_FAIL isPlay:isPlayBeep];
        [self presentStateOnCallScreen];
    }
}
- (void)CanTalkCountDownStop{
    if ([mCanTalkCountDown isValid]) {
        [mCanTalkCountDown invalidate];
    }
    mCanTalkCountDown = nil;
}
//- (void)callWithGroup:(NSString*)groupId{
//    GlobalObject *globalObject = [GlobalObject instance];
//    NSString *uri = [NSString stringWithFormat:@"sip:%@@%@:%@",groupId, GET_SER_IP, GET_SER_SIP_PORT];
//    if ([uri isEqualToString:globalObject.uri]) {
//        return;
//    }
//    globalObject.uri = uri;
//    [self startPingPTT];
//    char *callUri = (char*)[globalObject.uri UTF8String];
//    globalObject.groupID = [groupId integerValue];
//    globalObject.groupName = groupId;
//    [[XCPjsua sharedXCPjsua] makeCallTo:callUri];
//    if(groupId.integerValue == 0){
//        [GlobalObject instance].isEchoTest = YES;
//    }
//    else{
//        [GlobalObject instance].isEchoTest = NO;
//    }
//    
//}

#pragma mark - HANDLE CALL
- (void)handleTalking{
    
    NSTimeInterval newTimeTouch = [[NSDate date] timeIntervalSince1970];
    if (newTimeTouch - currentTimeTouch < (self.StateOfCall == STATE_REQUESTED_FAILED ? RETRY_AFTER_FAILED_TIMER : CAN_SPEAK_TIMER)) {
        CallViewController *callVC = nil;
        for (UIViewController *retVal in self.navigationController.viewControllers) {
            if ([retVal isKindOfClass:[CallViewController class]]) {
                callVC = (CallViewController*)retVal;
                break;
            }
        }
        [callVC presentCallState:1000];
        [self playSound:SOUND_NO isPlay:isPlayBeep];
        return;
    }
    currentTimeTouch = newTimeTouch;
    if ([GlobalObject instance].isEchoTest) {
        [self playSound:SOUND_TOUCH isPlay:isPlayBeep];
        [[HandleCall instance] startRecord];
        self.StateOfCall = STATE_NONE;
        
    }else{
        self.StateOfCall = STATE_REQUESTING;
        [[MQTTManager instance] sendRequestTalk];
        [self CanTalkCountDownStart];
    }
}
- (void)handleEndTalk{
    CallViewController *callVC = nil;
    for (UIViewController *retVal in self.navigationController.viewControllers) {
        if ([retVal isKindOfClass:[CallViewController class]]) {
            callVC = (CallViewController*)retVal;
            break;
        }
    }
    if (![[GlobalObject instance] isEchoTest]) {
        if (self.StateOfCall == STATE_REQUESTED_OK) {
            
            self.currentTimeTouch = [[NSDate date] timeIntervalSince1970];
            self.StateOfCall = STATE_NONE;
            if (callVC) {
                [callVC updateCollapseTextView:NO_TIME];
                [callVC ReleaseTalkCountDownStop];
                [callVC presentCallState:self.StateOfCall];
            }
            [self playSound:SOUND_TOUCH isPlay:isPlayBeep];
            [[MQTTManager instance] mqttReleaseSpeaker];
        } else {
            [self CanTalkCountDownStop];
            self.StateOfCall = STATE_NONE;
            if (callVC) {
                [callVC presentCallState:self.StateOfCall];
            }
        }
    }else{
        [self playSound:SOUND_TOUCH isPlay:isPlayBeep];
        [handleCall stopEchoRecord];
        StateOfCall = STATE_NONE;
//        [[MQTTManager instance] mqttReleaseSpeaker];
    }
    
}
- (void)handlemakeCallFromDelegate:(id)notification{
    NSDictionary *userInfoData = [notification object];
    [self handleCallWithGroupName:[userInfoData objectForKey:@"callGroup"] withGroupID:[[userInfoData objectForKey:@"to"] intValue]];
}
- (void)handleMakeCallWithInfoSuccess:(id)notification{
    NSDictionary *info = [notification object];
//    NSDictionary *info = [[NSDictionary alloc] initWithObjects:@[[MQTTMessageUtils getGroupId:split], split[split.count - 1]] forKeys:@[KEY_GROUP_ID, KEY_GROUP_NAME]];
    [self handleCallWithGroupName:info[KEY_GROUP_NAME] withGroupID:[info[KEY_GROUP_ID] intValue]];
}
- (void)handleMakeCallSuccess:(id)notification{
    GlobalObject *glObject = [GlobalObject instance];
    if (glObject.nextCallToUserID) {
        glObject.currentCallToUserID = glObject.nextCallToUserID;
    }
    [self showCallScreen:[GlobalObject instance].groupName];
    
}

- (void)handleDisconnectSuccess:(id)notification{
    
    GlobalObject *globalObject = [GlobalObject instance];
    if (globalObject.generalData == nil) {
        return;
    }
    [self handleCallWithGroupName:globalObject.groupName withGroupID:globalObject.groupID];
}
- (void)shutdownSound{
    [handleCall shutdownSound];
}
- (void)restartSound{
    [handleCall restartSound];
}
- (void)showCallScreen:(NSString*)groupName{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self pingPTTServer:nil];
        [self startPingPTT];
        BaseViewController *lastViewController = self.navigationController.viewControllers.lastObject;
        
        [lastViewController stopMakeCallLoading];
        if ([lastViewController isKindOfClass:[CallViewController class]]) {
            [lastViewController.navigationController popViewControllerAnimated:NO];
        }
        lastViewController = self.navigationController.viewControllers.lastObject;
        CallViewController *userCallVC = [[CallViewController alloc] initWithNibName:@"CallViewController" bundle:[Utility getPttLibraryBundle]];
        //    userCallVC.callType = 1;
        userCallVC.titleViewController = groupName;
        [lastViewController.navigationController pushViewController:userCallVC animated:YES];

    });
}
- (void)makeCallAgain{
    [handleCall endCall];
}
- (void)handleCallWithGroupName:(NSString*)groupName withGroupID:(int)groupID{
    dispatch_async(dispatch_get_main_queue(), ^{
        BaseViewController *lastVC = self.navigationController.viewControllers.lastObject;
        [lastVC showMakeCallLoading];
    });
//    NSString *uri = [NSString stringWithFormat:@"sip:%d@%@:%@",groupID, GET_SER_IP, GET_SER_SIP_PORT];
    int callState = handleCall.mCall.state;
//    // a call is still waiting to be confirmed
//    if (callState >= 1 && callState < 5) {
//        return;
//    }
    
    if (callState == CALL_STATE_JOINED && groupID == handleCall.mCall.groupId) {
        [self showCallScreen:groupName];
    }else if (groupID != handleCall.mCall.groupId && callState == CALL_STATE_JOINED){
        [handleCall endCall];
        if (groupID == 0) {
            handleCall.mCall.groupName = groupName;
            [self showCallScreen:groupName];
        }else{
            [handleCall makeCallTo:groupID];
        }
        
    }else{
        if (groupID == 0) {
            handleCall.mCall.groupName = groupName;
            [self showCallScreen:groupName];
        }else{
            [handleCall makeCallTo:groupID];
        }
        
    }
    [self setCurrentCallInfoWithGroupName:groupName withGroupID:groupID];
}

/**
 *  set current call info
 *
 *  @param groupName group name of call
 *  @param groupID   group id of call
 *  @param uri       uri of call
 */
- (void)setCurrentCallInfoWithGroupName:(NSString*)groupName withGroupID:(int)groupID{
    GlobalObject *globalObject = [GlobalObject instance];
    globalObject.groupName = groupName;
    globalObject.groupID = groupID;
//    globalObject.uri = uri;
    globalObject.isEchoTest = (groupID == 0);
}

#pragma mark - HANDLE SIP MESSAGE
- (void)handleLoginDuplicate:(id)notification{
    [self exitPushtoTalk];
}
- (void)handleMsgOkToTalk:(id)notification{
    CallViewController *callVC = nil;
    for (UIViewController *retVal in self.navigationController.viewControllers) {
        if ([retVal isKindOfClass:[CallViewController class]]) {
            callVC = (CallViewController*)retVal;
            break;
        }
    }
    NSString *recordId  = [notification object];
    if (callVC) {
        [self handleMessageOK:callVC withRecordId:recordId];
    }
    
}
/*
- (void)handleCallMessage:(id)notification{
    NSArray *arrayMessage  = [notification object];
    NSString *messageType  = @"";
    NSString *groupIdTemp  = @"";
    NSString *uniqueIdTemp = @"";
    CallViewController *callVC = nil;
    for (UIViewController *retVal in self.navigationController.viewControllers) {
        if ([retVal isKindOfClass:[CallViewController class]]) {
            callVC = (CallViewController*)retVal;
            break;
        }
    }
    
    if (arrayMessage.count == MESSAGE_PART_COUNT) {
        messageType = arrayMessage[MESSAGE_PART_COUNT - 1];
    }else if (arrayMessage.count == MESSAGE_PART_COUNT + 1){
        messageType = arrayMessage[MESSAGE_PART_COUNT - 1];
        groupIdTemp     = arrayMessage[1];
        uniqueIdTemp    = arrayMessage[MESSAGE_PART_COUNT];
        if ([messageType isEqualToString:MSG_SESSION_END_CALL] && ![uniqueIdTemp isEqualToString:[GlobalObject instance].uniqueDeviceId]) {
            [self playSound:SOUND_TOUCH isPlay:isPlayBeep];
            if (callVC) {
                [callVC ReleaseTalkCountDownStop];
            }
        }else if([messageType isEqualToString:MSG_SESSION_DISCONNECT] && [uniqueIdTemp isEqualToString:[GlobalObject instance].uniqueDeviceId]){
            [self exitPushtoTalk];
        }
    }
    if (messageType.length == 0) {
        return;
    }
    
    if ([messageType isEqualToString:MSG_PERMISSION_SPEAK_GRANTED]) {
        [self handleMessageOK:callVC];
    }else if ([messageType isEqualToString:MSG_PERMISSION_SPEAK_DECLINED]){
        [self handleMessageWait];
    }else if ([messageType isEqualToString:MSG_SESSION_END]){
        [self handleMessageEnd:callVC];
    }else if ([messageType isEqualToString:MSG_SESSION_PING]){
        [mqttManager sendMessageInCall:MSG_OK];
    }
//    else if ([messageType isEqualToString:MSG_OK]){
//        if (mActiveCall != null && mActiveCall.isActive() && !mActiveCall.isAfterEnded()) {
//            [mqttManager sendMessageInCall:MSG_OK];
//        }
//    }
    else if ([messageType isEqualToString:MSG_SESSION_RESET]){
        [self exitPushtoTalk];
    }else if ([messageType isEqualToString:MSG_SESSION_NO_JOIN_CALL]){
//        notifyCallerNotJoinCall();
    }
    
    [self presentStateOnCallScreen];
}
 */
- (void)presentStateOnCallScreen{
//    int stateToPresent = StateOfCall == STATE_NONE ? mSignalState : mStateOfCall;
    for (UIViewController *retVal in self.navigationController.viewControllers) {
        if ([retVal isKindOfClass:[CallViewController class]]) {
            CallViewController *callVC = (CallViewController*)retVal;
            [callVC presentCallState:StateOfCall];
            break;
        }
    }
}

- (void)handleMessageEnd:(CallViewController*)callVC{
//    mHeadsetButtonDownCount = 0;
    if (callVC) {
        [callVC ReleaseTalkCountDownStop];
    }
    //don't care, reset the state
    StateOfCall = STATE_NONE;
    [mqttManager notifyGroupOnSendingEndMessage];
}
- (void)handleMessageWait{
    // wait, someone else is talking
//    mHeadsetButtonDownCount = 0;
    [self CanTalkCountDownStop];
    if (StateOfCall == STATE_REQUESTING) {
        // we have to wait
        StateOfCall = STATE_REQUESTED_FAILED;
        if([self isHeadsetPluggedIn]){
            [self playSound:SOUND_WAIT isPlay:GET_SETTING_NOTIFY_HEADSET_VOICE];
        }
        else{
            [self playSound:SOUND_FAIL isPlay:isPlayBeep];
        }
    } else {
        // never mind it
        StateOfCall = STATE_NONE;
    }
}
- (void)handleMessageOK:(CallViewController*)callVC withRecordId:(NSString*)recordId{
    [HandleCall instance].mCall.recordId = recordId;
//    if (![GlobalObject instance].firstSuccessClick) {
//        [GlobalObject instance].firstSuccessClick = true;
//        NSString *message = [self createSendInviteMessage];
//        NSString *groupID = [NSString stringWithFormat:@"g%d", [GlobalObject instance].groupID];
//        [mqttManager publishMessage:groupID withMessage:message];
//    }
    //cancel countdown
    [self CanTalkCountDownStop];
    // check if we are still requesting
    
    
    if (StateOfCall == STATE_REQUESTING) {
        
        //we're good to talk
        StateOfCall = STATE_REQUESTED_OK;
        if([self isHeadsetPluggedIn]){
            [self playSound:SOUND_OK isPlay:GET_SETTING_NOTIFY_HEADSET_VOICE];
        }
        else{
            [self playSound:SOUND_TOUCH isPlay:isPlayBeep];
        }
        
        [[HandleCall instance] startRecord];
        [callVC ReleaseTalkCountDownStart];
    } else if (StateOfCall == STATE_REQUESTED_OK) {
        /**
         * A bug from SIP server
         * leave it here
         */
    } else if (StateOfCall == STATE_NONE) { // the count down timer is up, but we receive a bit later.
//        mHeadsetButtonDownCount = 0;
        [mqttManager mqttReleaseSpeaker];
        [callVC ReleaseTalkCountDownStop];
    } else {
        StateOfCall = STATE_NONE;
        
        if (callVC) {
            [callVC ReleaseTalkCountDownStop];
        }
    }
    
}
#pragma mark - MQTT
/**
 *  Start MQTT
 */
- (void)startMQTT{
    [mqttManager registerMQTT];
}

#pragma mark - STOP ALL MEDIA AND HANDLE CLICK ON HEADSET

- (void)stopAllMedia{
    
    [self startRemoteControlEvents];
    [self playEmptySound];
    
}
//- (void)viewWillAppear:(BOOL)animated{
//    if([self isHeadsetPluggedIn]){
//        [[XCPjsua sharedXCPjsua] setRoutingType:AVAudioSessionPortOverrideNone];
//    }
//    else{
//        [[XCPjsua sharedXCPjsua] setRoutingType:AVAudioSessionPortOverrideSpeaker];
//    }
//}
- (void)playEmptySound
{
    //play .1 sec empty sound
    NSBundle *bundle = [NSBundle bundleForClass:[self class]];
    NSString *filepath = [bundle pathForResource:@"point1sec" ofType:@"mp3"];
    if ([[NSFileManager defaultManager]fileExistsAtPath:filepath]) {
        AVPlayerItem *playerItem = [AVPlayerItem playerItemWithURL:[NSURL fileURLWithPath:filepath]];
        audioPlayer = [AVQueuePlayer queuePlayerWithItems:[NSArray arrayWithObject:playerItem]];
        [audioPlayer play];
    }
}
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object
                        change:(NSDictionary *)change context:(void *)context {
    if (object == self.audioPlayer && [keyPath isEqualToString:@"status"]) {
        if (self.audioPlayer.status == AVPlayerStatusReadyToPlay) {
            AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);
        }
    } 
}
-(void) startRemoteControlEvents {
    [AVAudioSession sharedInstance];
    UIApplication *application = [UIApplication sharedApplication];
    if ([application respondsToSelector:@selector(beginReceivingRemoteControlEvents)]) {
        [application beginReceivingRemoteControlEvents];
    }
    
    [self becomeFirstResponder];
    
//    MPRemoteCommandCenter *commandCenter = [MPRemoteCommandCenter sharedCommandCenter];
//    [commandCenter.playCommand addTargetUsingBlock:^(MPRemoteCommandEvent *event) {
//        // Begin playing the current track.
//        DLog(@"check");
//    }];
}

-(void) endRemoteControlEvents {
    [[UIApplication sharedApplication] endReceivingRemoteControlEvents];
    [self resignFirstResponder];
}

-(BOOL)canBecomeFirstResponder{
    return YES;
}
// If the user pulls out he headphone jack, stop playing.
- (void)audioRouteChangeListenerCallback:(NSNotification*)notification
{
    NSDictionary *interuptionDict = notification.userInfo;
    
    NSInteger routeChangeReason = [[interuptionDict valueForKey:AVAudioSessionRouteChangeReasonKey] integerValue];
    
    switch (routeChangeReason) {
            
        case AVAudioSessionRouteChangeReasonNewDeviceAvailable:{
            DLog(@"AVAudioSessionRouteChangeReasonNewDeviceAvailable");
            DLog(@"Headphone/Line plugged in");
            [[HandleCall instance] setRoutingType:AVAudioSessionPortOverrideNone];
        }
            break;
            
        case AVAudioSessionRouteChangeReasonOldDeviceUnavailable:{
            DLog(@"AVAudioSessionRouteChangeReasonOldDeviceUnavailable");
            DLog(@"Headphone/Line was pulled. Stopping player....");
            [[HandleCall instance] setRoutingType:AVAudioSessionPortOverrideSpeaker];
        }
            break;
            
        case AVAudioSessionRouteChangeReasonCategoryChange:
            // called at start - also when other audio wants to play
            NSLog(@"AVAudioSessionRouteChangeReasonCategoryChange");
            break;
        default:
        {
            if([self isHeadsetPluggedIn]){
                [[HandleCall instance] setRoutingType:AVAudioSessionPortOverrideNone];
            }
            else{
                [[HandleCall instance] setRoutingType:AVAudioSessionPortOverrideSpeaker];
            }
            break;
        }
    }
}
@end
