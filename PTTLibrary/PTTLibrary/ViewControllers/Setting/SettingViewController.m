//
//  SettingViewController.m
//  PTTLibrary
//
//  Created by Nguyen Minh Duc on 8/12/15.
//  Copyright (c) 2015 FCS Computer Systems. All rights reserved.
//

#import "SettingViewController.h"
#import "NMRangeSlider.h"
#import "M13Checkbox.h"
#import "RadioButton.h"

@interface SettingViewController ()<RangeSliderDelegate>
{
    
    __weak IBOutlet M13Checkbox *callUserSound;
    __weak IBOutlet M13Checkbox *callUserVibrate;
    
    __weak IBOutlet M13Checkbox *callGroupSound;
    __weak IBOutlet M13Checkbox *callGroupVibrate;
    __weak IBOutlet M13Checkbox *pushedBeep;
    __weak IBOutlet M13Checkbox *headsetVoice;
    __weak IBOutlet UITextField *serverConnectionTimeOut;
    __weak IBOutlet UITextField *callConnectionTimeOut;
    __weak IBOutlet RadioButton *recordFileStorage;
    __weak IBOutlet RadioButton *recordFileExternal;
    
    __weak IBOutlet UITextField *recordFileMaxTime;
    __weak IBOutlet UITextField *recordFileMaSize;
    __weak IBOutlet RadioButton *serverConnection;
    
    __weak IBOutlet RadioButton *stun;
    __weak IBOutlet UITextField *stunAddress;
    __weak IBOutlet UITextField *sipServerAddress;
    __weak IBOutlet UITextField *sipPort;
    __weak IBOutlet UITextField *webAddress;
    __weak IBOutlet UITextField *webPort;
    __weak IBOutlet UITextField *mqttPort;
    
}
@property (weak, nonatomic) IBOutlet UILabel *lowerLabel;
@property (weak, nonatomic) IBOutlet UILabel *upperLabel;
@property (weak, nonatomic) IBOutlet NMRangeSlider *rangeSlider;
@property (weak, nonatomic) IBOutlet UILabel *badNetwork;
@property (weak, nonatomic) IBOutlet UILabel *fairNetwork;
@property (weak, nonatomic) IBOutlet UIView *notificationView;
@property (weak, nonatomic) IBOutlet UIView *timeoutView;
@property (weak, nonatomic) IBOutlet UIView *signalView;
@property (weak, nonatomic) IBOutlet UIView *recordView;
@property (weak, nonatomic) IBOutlet UIView *configurationView;

- (IBAction)didClickCallUserSound:(UIButton *)sender;
- (IBAction)didClickCallUserVibrate:(UIButton *)sender;
- (IBAction)didClickCallGroupVibrate:(UIButton *)sender;
- (IBAction)didClickPushedBeep:(UIButton *)sender;
- (IBAction)didClickHeadsetVoice:(UIButton *)sender;


@end

@implementation SettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Setting";
    // Do any additional setup after loading the view from its nib.
    [self loadSettingValues];
    [self configureSlider];
    [self configureSectionViews];
    DLog(@"setting - %@",recordFileStorage.titleLabel.text);
    [recordFileStorage setTitle:[[PTTLanguageManager sharedLanguageManager] getTranslationForKey:recordFileStorage.titleLabel.text] forState:UIControlStateNormal];
    [recordFileExternal setTitle:[[PTTLanguageManager sharedLanguageManager] getTranslationForKey:recordFileExternal.titleLabel.text] forState:UIControlStateNormal];
    [serverConnection setTitle:[[PTTLanguageManager sharedLanguageManager] getTranslationForKey:serverConnection.titleLabel.text] forState:UIControlStateNormal];
    [stun setTitle:[[PTTLanguageManager sharedLanguageManager] getTranslationForKey:stun.titleLabel.text] forState:UIControlStateNormal];
    [sipServerAddress addTarget:self
                       action:@selector(textFieldDidChange:)
             forControlEvents:UIControlEventEditingChanged];
    [webAddress addTarget:self
                         action:@selector(textFieldDidChange:)
               forControlEvents:UIControlEventEditingChanged];
    
}

-(void)textFieldDidChange:(UITextField*)textField
{
    if (textField == sipServerAddress) {
        webAddress.text = textField.text;
        return;
    }
    sipServerAddress.text = textField.text;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) loadSettingValues
{
    callUserSound.checkState     = GET_SETTING_NOTIFY_CALL_USER_SOUND;
    callUserVibrate.checkState   = GET_SETTING_NOTIFY_CALL_USER_VIBRATE;
    callGroupSound.checkState    = GET_SETTING_NOTIFY_CALL_GROUP_SOUND;
    callGroupVibrate.checkState  = GET_SETTING_NOTIFY_CALL_GROUP_VIBRATE;
    pushedBeep.checkState        = GET_SETTING_NOTIFY_BEEP;
    headsetVoice.checkState      = GET_SETTING_NOTIFY_HEADSET_VOICE;
    serverConnectionTimeOut.text = GET_SETTING_TIME_OUT_CONNECTING_SERVER;
    callConnectionTimeOut.text   = GET_SETTING_TIME_OUT_CONNECTING_CALL;
//    recordFileStorage.selected   = UsrDfltGetBool4Key(SETTING_RECORD_FILE_STORAGE);
    recordFileMaxTime.text       = GET_SETTING_RECORD_FILE_MAX_TIME;
    recordFileMaSize.text        = GET_SETTING_RECORD_FILE_MAX_SIZE;
    serverConnection.selected    = GET_SETTING_CONFIG_SERVER_CONNECTION;
    stunAddress.text             = GET_SETTING_CONFIG_STUN_ADDRESS;
    sipServerAddress.text        = GET_SER_IP;
    sipPort.text                 = GET_SER_SIP_PORT;
    webAddress.text              = GET_SER_IP;
    webPort.text                 = GET_SER_PTT_PORT;
    mqttPort.text                = GET_MQTT_PORT;
}

-(void) configureSectionViews
{
    self.notificationView.layer.cornerRadius = 6;
    self.timeoutView.layer.cornerRadius = 6;
    self.signalView.layer.cornerRadius = 6;
    self.recordView.layer.cornerRadius = 6;
    self.configurationView.layer.cornerRadius = 6;
}

- (void) configureSlider
{
    self.rangeSlider.minimumValue = 0;
    self.rangeSlider.maximumValue = 100;
    self.rangeSlider.upperValue   = GET_SETTING_SIGNAL_BAD;
    self.rangeSlider.lowerValue   = GET_SETTING_SIGNAL_FAIR;
    self.rangeSlider.delegate     = self;
    [self updateSlider];
}

- (void) updateSlider
{
    // You get get the center point of the slider handles and use this to arrange other subviews
    
    CGPoint lowerCenter;
    lowerCenter.x = (self.rangeSlider.lowerCenter.x + self.rangeSlider.frame.origin.x);
    lowerCenter.y = (self.rangeSlider.center.y - 30.0f);
    
    self.lowerLabel.center = lowerCenter;
    self.lowerLabel.text   = [NSString stringWithFormat:@"%d", (int)self.rangeSlider.lowerValue];
    
    CGPoint badNetworkCenter;
    badNetworkCenter.x     = (self.rangeSlider.lowerCenter.x + self.rangeSlider.frame.origin.x);
    badNetworkCenter.y     = (self.rangeSlider.center.y + 30.0f);
    self.badNetwork.center = badNetworkCenter;
    
    CGPoint upperCenter;
    upperCenter.x          = (self.rangeSlider.upperCenter.x + self.rangeSlider.frame.origin.x);
    upperCenter.y          = (self.rangeSlider.center.y - 30.0f);
    self.upperLabel.center = upperCenter;
    self.upperLabel.text   = [NSString stringWithFormat:@"%d", (int)self.rangeSlider.upperValue];
    
    CGPoint fairNetworkCenter;
    fairNetworkCenter.x     = (self.rangeSlider.upperCenter.x + self.rangeSlider.frame.origin.x);
    fairNetworkCenter.y     = (self.rangeSlider.center.y + 30.0f);
    self.fairNetwork.center = fairNetworkCenter;
    
}

-(void)updateFrame
{
    [self updateSlider];
    if (self.rangeSlider.lowerValue == self.rangeSlider.minimumRange && self.rangeSlider.upperValue == self.rangeSlider.maximumValue) {
        [self.upperLabel setHidden:YES];
        [self.lowerLabel setHidden:YES];
        [self.badNetwork setHidden:YES];
        [self.fairNetwork setHidden:YES];
    }else{
        [self.upperLabel setHidden:NO];
        [self.lowerLabel setHidden:NO];
        [self.badNetwork setHidden:NO];
        [self.fairNetwork setHidden:NO];
    }

}

- (IBAction)save:(id)sender {
    SET_SETTING_NOTIFY_CALL_USER_SOUND(callUserSound.checkState);
    SET_SETTING_NOTIFY_CALL_USER_VIBRATE(callUserVibrate.checkState);
    SET_SETTING_NOTIFY_CALL_GROUP_SOUND(callGroupSound.checkState);
    SET_SETTING_NOTIFY_CALL_GROUP_VIBRATE(callGroupVibrate.checkState);
    SET_SETTING_NOTIFY_BEEP(pushedBeep.checkState);
    SET_SETTING_NOTIFY_HEADSET_VOICE(headsetVoice.checkState);
    SET_SETTING_TIME_OUT_CONNECTING_SERVER(serverConnectionTimeOut.text);
    SET_SETTING_TIME_OUT_CONNECTING_CALL(callConnectionTimeOut.text);
    SET_SETTING_SIGNAL_BAD(_rangeSlider.upperValue);
    SET_SETTING_SIGNAL_FAIR(_rangeSlider.lowerValue);
    SET_SETTING_RECORD_FILE_MAX_TIME(recordFileMaxTime.text);
    SET_SETTING_RECORD_FILE_MAX_SIZE(recordFileMaSize.text);
    SET_SETTING_CONFIG_SERVER_CONNECTION(serverConnection.selected);
    SET_SETTING_CONFIG_STUN_ADDRESS(stunAddress.text);
    SET_SER_IP(sipServerAddress.text);
    SET_SER_IP(webAddress.text);
    SET_SER_PTT_PORT(webPort.text);
    SET_SER_SIP_PORT(sipPort.text);
    SET_MQTT_PORT(mqttPort.text);
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - ACTION
- (IBAction)cancel:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)didClickCallUserSound:(UIButton *)sender {
    callUserSound.checkState = !callUserSound.checkState;
}

- (IBAction)didClickCallUserVibrate:(UIButton *)sender {
    callUserVibrate.checkState = !callUserVibrate.checkState;
}

- (IBAction)didClickCallGroupVibrate:(UIButton *)sender {
    callGroupVibrate.checkState = !callGroupVibrate.checkState;
}

- (IBAction)didClickPushedBeep:(UIButton *)sender {
    pushedBeep.checkState = !pushedBeep.checkState;
}

- (IBAction)didClickHeadsetVoice:(UIButton *)sender {
    headsetVoice.checkState = !headsetVoice.checkState;
}

- (IBAction)didClickCallGroupSound:(UIButton *)sender {
    callGroupSound.checkState = !callGroupSound.checkState;
}
@end
