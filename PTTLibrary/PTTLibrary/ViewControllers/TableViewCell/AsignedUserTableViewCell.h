//
//  AsignedUserTableViewCell.h
//  PTTLibrary
//
//  Created by phu the cong on 8/25/15.
//  Copyright (c) 2015 FCS Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AsignedUserTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbName;
@property (weak, nonatomic) IBOutlet UIButton *joinedState;

@end
