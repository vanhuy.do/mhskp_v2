//
//  AsignedUserTableViewCell.m
//  PTTLibrary
//
//  Created by phu the cong on 8/25/15.
//  Copyright (c) 2015 FCS Computer Systems. All rights reserved.
//

#import "AsignedUserTableViewCell.h"

@implementation AsignedUserTableViewCell

- (void)awakeFromNib {
    // Initialization code
    self.layer.cornerRadius = 6;
    self.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
