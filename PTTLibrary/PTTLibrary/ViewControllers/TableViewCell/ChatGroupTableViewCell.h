//
//  ChatGroupTableViewCell.h
//  PTTLibrary
//
//  Created by phu the cong on 8/21/15.
//  Copyright (c) 2015 FCS Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MGSwipeTableCell.h"

@class ChatGroupTableViewCell;

@protocol ChatGroupTableViewCellDelegate <NSObject>

-(void)performActionsOnCell:(ChatGroupTableViewCell*)cell;

@end

@interface ChatGroupTableViewCell : MGSwipeTableCell
@property (weak, nonatomic) IBOutlet UILabel *lbGroupName;
@property (weak, nonatomic) IBOutlet UILabel *lbNumberOfGroup;
@property (weak, nonatomic) IBOutlet UILabel *lbGroupStatus;

@property(weak, nonatomic) id<ChatGroupTableViewCellDelegate>ownDelegate;

@end
