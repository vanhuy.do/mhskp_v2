//
//  ChatGroupTableViewCell.m
//  PTTLibrary
//
//  Created by phu the cong on 8/21/15.
//  Copyright (c) 2015 FCS Computer Systems. All rights reserved.
//

#import "ChatGroupTableViewCell.h"

@implementation ChatGroupTableViewCell

- (void)awakeFromNib {
    // Initialization code
    self.layer.cornerRadius = 6;
    self.clipsToBounds = YES;
    self.backgroundColor = [UIColor clearColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)performActionsOnGroup:(id)sender {
    if ([self.ownDelegate respondsToSelector:@selector(performActionsOnCell:)]) {
        [self.ownDelegate performActionsOnCell:self];
    }
}

@end
