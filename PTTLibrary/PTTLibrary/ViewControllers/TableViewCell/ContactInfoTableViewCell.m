//
//  ContactInfoTableViewCell.m
//  PTTLibrary
//
//  Created by phu the cong on 8/20/15.
//  Copyright (c) 2015 FCS Computer Systems. All rights reserved.
//

#import "ContactInfoTableViewCell.h"

@implementation ContactInfoTableViewCell

- (void)awakeFromNib {
    // Initialization code
    self.layer.cornerRadius = 6;
    self.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
