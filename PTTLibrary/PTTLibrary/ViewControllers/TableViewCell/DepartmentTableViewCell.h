//
//  DepartmentTableViewCell.h
//  PTTLibrary
//
//  Created by phu the cong on 8/25/15.
//  Copyright (c) 2015 FCS Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DepartmentTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbDepartmentName;
@property (weak, nonatomic) IBOutlet UIImageView *selectionImageView;

@end
