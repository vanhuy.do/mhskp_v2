//
//  HistoryDetailBaseTableViewCell.h
//  PTTLibrary
//
//  Created by Admin on 8/28/15.
//  Copyright (c) 2015 FCS Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HistoryDetailInfoModel;
@protocol HistoryDetailDelegate;
@interface HistoryDetailBaseTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelUserName;
@property (weak, nonatomic) IBOutlet UIButton *buttonPlay;
@property (weak, nonatomic) IBOutlet UISlider *sliderMedia;
@property (weak, nonatomic) IBOutlet UILabel *labelDuration;
@property (weak, nonatomic) IBOutlet UILabel *labelTime;
@property (weak, nonatomic) IBOutlet UILabel *labelNickName;
@property (weak, nonatomic) HistoryDetailInfoModel *infoData;
@property (weak, nonatomic) id<HistoryDetailDelegate> delegate;
- (IBAction)changeSliderValue:(UISlider *)sender;
- (void)setData:(HistoryDetailInfoModel *)infoData;
- (IBAction)didClickShowNickName:(UIButton *)sender;
- (IBAction)didClickPlay:(UIButton *)sender;
@end

@protocol HistoryDetailDelegate <NSObject>

- (void)didClickPlay:(HistoryDetailInfoModel *)infoData;
- (void)seekingMedia:(HistoryDetailInfoModel*)infoData withValue:(float)value;
//- (void)downloadComplete;
//- (void)updateProgress:(float)status;

@end