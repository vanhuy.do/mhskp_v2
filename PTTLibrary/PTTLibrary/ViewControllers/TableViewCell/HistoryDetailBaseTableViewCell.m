//
//  HistoryDetailBaseTableViewCell.m
//  PTTLibrary
//
//  Created by Admin on 8/28/15.
//  Copyright (c) 2015 FCS Computer Systems. All rights reserved.
//

#import "HistoryDetailBaseTableViewCell.h"
#import "HistoryDetailInfoModel.h"

@implementation HistoryDetailBaseTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)changeSliderValue:(UISlider *)sender {
    [self.infoData setCurrentDuration:sender.value];
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(seekingMedia:withValue:)])
    {
        [self.delegate seekingMedia:self.infoData withValue:sender.value];
    }
}

- (void)setData:(HistoryDetailInfoModel *)infoData{
    self.infoData                 = infoData;
    self.labelUserName.text       = infoData.userName;
    self.labelTime.text           = infoData.getStartTime;
    self.labelDuration.text       = infoData.getDuration;
    self.sliderMedia.minimumValue = 0;
    self.sliderMedia.maximumValue = infoData.duration;
    self.sliderMedia.value        = infoData.currentDuration;
    self.buttonPlay.selected      = infoData.getPlay;
    self.sliderMedia.enabled      = infoData.getPlay;
    self.labelNickName.text       = infoData.name;
}

- (IBAction)didClickShowNickName:(UIButton *)sender {
    [self.labelNickName setAlpha:1.0f];
    [UIView animateWithDuration:1.0f animations:^{
        [self.labelNickName setAlpha:0.0f];
    } completion:^(BOOL finished) {
        [self.labelNickName setAlpha:0.0f];
    }];
}

- (IBAction)didClickPlay:(UIButton *)sender {
    sender.selected = !sender.selected;
    [self.infoData setPlay:sender.selected];
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(didClickPlay:)])
    {
        [self.delegate didClickPlay:self.infoData];
    }
}
@end
