//
//  HistoryDetailRightTableViewCell.m
//  PTTLibrary
//
//  Created by Admin on 8/27/15.
//  Copyright (c) 2015 FCS Computer Systems. All rights reserved.
//

#import "HistoryDetailRightTableViewCell.h"
#import "HistoryDetailInfoModel.h"
#import "UIImage+PTT.h"
@implementation HistoryDetailRightTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [self.sliderMedia setThumbImage:[UIImage PTTImageNamed:@"play_point"] forState:UIControlStateNormal];
    [self.buttonPlay setBackgroundImage:[UIImage PTTImageNamed:@"bt_play"] forState:UIControlStateNormal];
    [self.buttonPlay setBackgroundImage:[UIImage PTTImageNamed:@"btn_pause"] forState:UIControlStateSelected];
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
@end
