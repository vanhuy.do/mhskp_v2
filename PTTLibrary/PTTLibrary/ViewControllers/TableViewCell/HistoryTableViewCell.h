//
//  HistoryTableViewCell.h
//  PTTLibrary
//
//  Created by Admin on 8/27/15.
//  Copyright (c) 2015 FCS Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HistoryTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageType;
@property (weak, nonatomic) IBOutlet UILabel *labelName;
@property (weak, nonatomic) IBOutlet UILabel *labelTime;
@property (weak, nonatomic) IBOutlet UIImageView *imageviewIcon;

@end
