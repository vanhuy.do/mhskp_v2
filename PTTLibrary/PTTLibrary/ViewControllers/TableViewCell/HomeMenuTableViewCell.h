//
//  HomeMenuTableViewCell.h
//  PTTLibrary
//
//  Created by Nguyen Minh Duc on 8/11/15.
//  Copyright (c) 2015 FCS Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeMenuTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *menuImage;
@property (weak, nonatomic) IBOutlet UILabel *menuNameLable;
@property (weak, nonatomic) IBOutlet UIImageView *bgImage;

@end
