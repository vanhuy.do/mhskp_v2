//
//  HomeMenuTableViewCell.m
//  PTTLibrary
//
//  Created by Nguyen Minh Duc on 8/11/15.
//  Copyright (c) 2015 FCS Computer Systems. All rights reserved.
//

#import "HomeMenuTableViewCell.h"

@implementation HomeMenuTableViewCell

- (void)awakeFromNib {
    // Initialization code
    self.layer.cornerRadius = 6;
    [self setClipsToBounds:YES];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
