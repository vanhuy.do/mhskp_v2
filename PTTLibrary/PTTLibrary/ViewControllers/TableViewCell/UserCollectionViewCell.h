//
//  UserCollectionViewCell.h
//  PTTLibrary
//
//  Created by Admin on 8/25/15.
//  Copyright (c) 2015 FCS Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelUserName;
@property (weak, nonatomic) IBOutlet UILabel *labelNickName;
- (IBAction)didClickShowNickName:(UIButton *)sender;

@end
