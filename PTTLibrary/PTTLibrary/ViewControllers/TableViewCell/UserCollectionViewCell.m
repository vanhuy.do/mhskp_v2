//
//  UserCollectionViewCell.m
//  PTTLibrary
//
//  Created by Admin on 8/25/15.
//  Copyright (c) 2015 FCS Computer Systems. All rights reserved.
//

#import "UserCollectionViewCell.h"

@implementation UserCollectionViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (IBAction)didClickShowNickName:(UIButton *)sender {
    [self.labelNickName setAlpha:1.0f];
    [UIView animateWithDuration:1.5f animations:^{
        [self.labelNickName setAlpha:0.0f];
    } completion:^(BOOL finished) {
        [self.labelNickName setAlpha:0.0f];
    }];
}
@end
