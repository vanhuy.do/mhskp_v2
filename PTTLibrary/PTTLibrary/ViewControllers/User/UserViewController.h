//
//  UserViewController.h
//  PTTLibrary
//
//  Created by Admin on 8/14/15.
//  Copyright (c) 2015 FCS Computer Systems. All rights reserved.
//

#import "BaseViewController.h"

@class ContactInfoModel;

@protocol RefreshingDelegate <NSObject>

-(void)needToRefresh;

@end

@interface UserViewController : BaseViewController<UITableViewDelegate, UITableViewDataSource>{
    
    __weak IBOutlet UITableView *tbvContent;
    NSMutableArray *listUser;
    NSTimer* contactingUser;
    MBProgressHUD1 *hud;
    NSString *userNameCall;
}
@property BOOL isUserList;
@property(nonatomic, weak) id<RefreshingDelegate> delegate;

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil group:(GroupModel*)group;
- (void)contactingUserCountdownStop;
@end
