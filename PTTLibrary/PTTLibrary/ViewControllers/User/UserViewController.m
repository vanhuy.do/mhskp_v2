//
//  UserViewController.m
//  PTTLibrary
//
//  Created by Admin on 8/14/15.
//  Copyright (c) 2015 FCS Computer Systems. All rights reserved.
//

#import "UserViewController.h"
#import "ServerPath.h"
#import "AFNetworkingUtility.h"
#import "ContactInfoModel.h"
#import "ContactInfoTableViewCell.h"
#import "CallViewController.h"
#import "UserViewController.h"
#import "AsignedUserTableViewCell.h"
#import "UIAlertView+Blocks.h"
#import "UIImage+PTT.h"

#define SizeOfPage 25

@interface UserViewController ()<UITextFieldDelegate>
{
    int currentPage;
    BOOL needToLoadMore;
    NSString *searchingString;
    GroupModel *selectedGroup;
    BOOL typingFiltered;
    NSArray *filteredArray;
}
@property (weak, nonatomic) IBOutlet UITextField *searchingText;

@end

@implementation UserViewController


#pragma mark - VIEWCONTROLLER LIFECYCLE

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil group:(GroupModel*)group
{
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        selectedGroup = group;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    typingFiltered = NO;
    listUser = [[NSMutableArray alloc] init];
    if (selectedGroup) {
        self.navigationItem.title = getLocalizationString(@"Assign User");
        NotifReg(self, @selector(updateListAssignedUser:), MQTT_UPDATE_LIST_GROUP);
//        NotifReg(self, @selector(deleteGroup:), MQTT_GROUP_DELETE);
        [_searchingText addTarget:self
                                   action:@selector(textFieldDidChange:)
                         forControlEvents:UIControlEventEditingChanged];
        [tbvContent registerNib:[UINib nibWithNibName:@"AsignedUserTableViewCell" bundle:[Utility getPttLibraryBundle]] forCellReuseIdentifier:@"AsignedUserTableViewCellId"];
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(assignUsers:)];
    }else{
        self.navigationItem.title = getLocalizationString(@"User");
        [tbvContent registerNib:[UINib nibWithNibName:@"ContactInfoTableViewCell" bundle:[Utility getPttLibraryBundle]] forCellReuseIdentifier:@"ContactInfoTableViewCellId"];
        if (IS_ECHO_MODE) {
            [listUser addObject:[self echouser]];
        }
    }
    currentPage = 1;
    needToLoadMore = NO;
    tbvContent.layer.cornerRadius = 6;
    self.searchingText.delegate = self;
    searchingString = @"";
    [self loadListContact];
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [tbvContent addSubview:refreshControl];
}

//-(void)deleteGroup:(NSNotification*)notify
//{
//    id jsonObj = notify.object;
//    if ([jsonObj isKindOfClass:[NSDictionary class]]) {
//        NSString *channel = [jsonObj objectForKey:KEY_CHANNEL];
//        NSString *strGroupID   = [channel substringFromIndex:1];
//        int groupID            = [[strGroupID stringByTrimmingCharactersInSet:NSMutableCharacterSet.whitespaceCharacterSet] integerValue];
//        if (groupID == selectedGroup.groupId) {
//            [UIAlertView showWithTitle:@""
//                               message:@"This group has been deleted"
//                     cancelButtonTitle:@"OK"
//                     otherButtonTitles:nil
//                              tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
//                                  if (buttonIndex == [alertView cancelButtonIndex]) {
//                                      [self.navigationController popViewControllerAnimated:YES];
//                                  }
//                              }];
//            
//        }
//        
//    }
//    
//}

-(void)updateListAssignedUser:(NSNotification*)notify
{
    id jsonObj = notify.object;
    if ([jsonObj isKindOfClass:[NSDictionary class]]) {
        NSString *channel = [jsonObj objectForKey:KEY_CHANNEL];
        NSString *strGroupID   = [channel substringFromIndex:1];
        int groupID            = [[strGroupID stringByTrimmingCharactersInSet:NSMutableCharacterSet.whitespaceCharacterSet] intValue];
        id data = [notify.object objectForKey:KEY_DATA];
        if (groupID == selectedGroup.groupId) {
            NSArray *unjoins = [data objectForKey:@"unjoins"];
            if ([unjoins containsObject:[NSNumber numberWithInt:[[GlobalObject instance].generalData.currentUser.userId intValue]]]) {
                [UIAlertView showWithTitle:@""
                                   message:@"This user does not exist in this Group!"
                         cancelButtonTitle:@"OK"
                         otherButtonTitles:nil
                                  tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                                      if (buttonIndex == [alertView cancelButtonIndex]) {
                                          [self.navigationController popToRootViewControllerAnimated:YES];
                                      }
                                  }];
            }else{
                [self refresh:nil];
            }
        }
    }
}

- (void)refresh:(UIRefreshControl *)refreshControl {
    currentPage = 1;
    needToLoadMore = NO;
   
    [self requestDataWithStart:^{
        
    } finished:^{
        [listUser removeAllObjects];
        if (!selectedGroup) {
            [listUser addObject:[self echouser]];
        }
        [refreshControl endRefreshing];
    }];
    
}

-(void)assignUsers:(id)sender
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.joined != self.originalState"];
    NSArray *changedStates = [listUser filteredArrayUsingPredicate:predicate];
    if ([changedStates count] == 0) {
        return;
    }
    NSMutableArray *unjoinUsers = [NSMutableArray array];
    NSMutableArray *joinUsers = [NSMutableArray array];
    for (ContactInfoModel *contactUser in changedStates) {
        if (contactUser.originalState) {
            [unjoinUsers addObject:contactUser.userId];
        }else{
            [joinUsers addObject:contactUser.userId];
        }
    }
    [MBProgressHUD1 showHUDAddedTo:self.view animated:YES];
    NSMutableDictionary *parms =[NSMutableDictionary dictionary];
    [parms setObject:[NSNumber numberWithInt:selectedGroup.groupId]  forKey:KEY_GROUP_ID];
    [parms setObject:unjoinUsers forKey:@"unjoinedIds"];
    [parms setObject:joinUsers forKey:@"joinedIds"];
    [[AFNetworkingUtility instance] requestData:API_GET_ASSIGN_USER withParameters:parms success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self hideLoading];
        [self.navigationController popViewControllerAnimated:YES];
        if ([self.delegate respondsToSelector:@selector(needToRefresh)]) {
            [self.delegate needToRefresh];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self hideLoading];
    }];
}


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (!selectedGroup) {
        NotifReg(self, @selector(updateListUser:), MQTT_UPDATE_LIST_CONTACT);
        NotifReg(self, @selector(handleMakeCallUserToUser), MQTT_MAKE_CALL_USER_USER);
    }
    if (listUser && listUser.count > 0) {
        [self sortWhenActiveCallUser];
        [tbvContent reloadData];
    }
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    if (!selectedGroup) {
        NotifUnreg(self, MQTT_UPDATE_LIST_CONTACT);
        NotifUnreg(self, MQTT_MAKE_CALL_USER_USER);
    }
    
}

-(ContactInfoModel*)echouser{
    ContactInfoModel *echoUser = [[ContactInfoModel alloc] init];
    echoUser.userId = @"0";
    echoUser.nickName = getLocalizationString(@"Echo");
    echoUser.userName = @"Echo";
    echoUser.status = 0;
    return echoUser;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - SUPPORT FUNCTION


-(void)loadListContact
{
    [self requestDataWithStart:^{
        [self showLoading];
    } finished:^{
        [self hideLoading];
    }];
}

- (void)requestDataWithStart:(void (^)(void))start finished:(void(^)(void))end{
    start();
    NSMutableDictionary *parms =[NSMutableDictionary dictionary];
    
    if (selectedGroup) {
        //assign users.
        [parms setObject: [NSNumber numberWithInt:selectedGroup.groupId] forKey:KEY_GROUP_ID];
    }else
    {
        //contact users.
        NSMutableDictionary *filter = [NSMutableDictionary dictionary];
        [filter setObject:searchingString forKey:KEY_NICK_NAME];
        [parms setObject:[NSNumber numberWithInt:currentPage] forKey:KEY_PAGE];
        [parms setObject:filter forKey:KEY_FILTER];
        
    }
  
    [[AFNetworkingUtility instance] requestData:API_GET_LIST_CONTACT withParameters:parms success:^(AFHTTPRequestOperation *operation, id responseObject) {
        end();
        id data = [responseObject objectForKey:KEY_DATA];
        int totalCount =  [[responseObject objectForKey:KEY_TOTAL_COUNT] intValue] + (IS_ECHO_MODE&&!selectedGroup?1:0);
        if ([data isKindOfClass:[NSArray class]]) {
            if (([data count] + [listUser count]) < totalCount) {
                needToLoadMore = YES;
            }
            NSString *currentUserID = [[[[GlobalObject instance] generalData] currentUser] userId];
            for (NSDictionary *userDict in data) {
                NSError* err = nil;
                ContactInfoModel *user = [[ContactInfoModel alloc] initWithDictionary:userDict error:&err];
                if (self.isUserList && [[NSString stringWithFormat:@"%@", user.userId] isEqualToString:currentUserID]) {
                    continue;
                }
                [listUser addObject:user];
            }
            [self sortWhenActiveCallUser];
            [tbvContent reloadData];
        }

        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        end();
    }];
}
- (void)sortWhenActiveCallUser{
    NSString *currentUserCall = [GlobalObject instance].currentCallToUserID;
    if (self.isUserList && currentUserCall) {
        int indexFound = -1;
        for (int i=0; i<listUser.count; i++) {
            ContactInfoModel *user = [listUser objectAtIndex:i];
            if (user.userId.intValue == currentUserCall.intValue) {
                indexFound = i;
                break;
            }
        }
        if (indexFound != -1) {
            [listUser exchangeObjectAtIndex:indexFound withObjectAtIndex:1];
        }
    }
}
#pragma mark - UIScrollView Delegate
//Load more
- (void)scrollViewDidScroll:(UIScrollView *)scrollView;
{
    if (tbvContent.contentOffset.y >= (tbvContent.contentSize.height - tbvContent.bounds.size.height) && needToLoadMore) {
        currentPage ++;
        needToLoadMore = NO;
        [self loadListContact];
    }
}



#pragma mark - UITABLEVIEW DATASOURCE + DELEGATE

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (typingFiltered) {
        return [filteredArray count];
    }
    return listUser.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (selectedGroup) {
        AsignedUserTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AsignedUserTableViewCellId"];
        ContactInfoModel *user = nil;
        if (typingFiltered) {
            user = [filteredArray objectAtIndex:indexPath.row];
        }else{
            user = [listUser objectAtIndex:indexPath.row];
        }
        cell.lbName.text = user.nickName;
        cell.joinedState.selected = user.joined;
        return cell;
        
    }else{
        
        ContactInfoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ContactInfoTableViewCellId"];
        ContactInfoModel *user = [listUser objectAtIndex:indexPath.row];
        cell.lbName.text = user.nickName;
        switch (user.status) {
            case Available:
            {
                [cell.statusImageView setImage:[UIImage PTTImageNamed:@"ic_navigation_check"]];
                cell.lbStatus.text = getLocalizationString(@"Available");
            }
                break;
            case OnCall:
            {
                [cell.statusImageView setImage:[UIImage PTTImageNamed:@"ic_status_solo"]];
                cell.lbStatus.text = getLocalizationString(@"On Call");
            }
                break;
            case Offline:
            {
                [cell.statusImageView setImage:[UIImage PTTImageNamed:@"ic_offline"]];
                cell.lbStatus.text = getLocalizationString(@"Offline");
            }
                break;
            default:
                break;
        }
        cell.lbActive.hidden = YES;
        if ([user.userId intValue] == [[GlobalObject instance].currentCallToUserID intValue]) {
            cell.lbActive.hidden = NO;
        }
        return cell;
    }
    return nil;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (selectedGroup) {
        ContactInfoModel *user = nil;
        if (typingFiltered) {
            user = [filteredArray objectAtIndex:indexPath.row];
        }else{
            user = [listUser objectAtIndex:indexPath.row];
        }
        user.joined = !user.joined;
        AsignedUserTableViewCell *cell = (AsignedUserTableViewCell*)[tableView cellForRowAtIndexPath:indexPath];
        cell.joinedState.selected = user.joined;
        
    }else{
        if (IS_ECHO_MODE && indexPath.row == 0) {
            // call into echo
            for (UIViewController *retVal in self.navigationController.viewControllers) {
                if ([retVal isKindOfClass:[HomeViewController class]]) {
                    HomeViewController *homeViewController = (HomeViewController*)retVal;
                    [homeViewController handleCallWithGroupName:getLocalizationString(@"Echo") withGroupID:0];
                }
            }
            [GlobalObject instance].callToUserID = nil;
            
        }else{
            //create fake group
            [self createFakeGroup:[listUser objectAtIndex:indexPath.row]];
        }
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}
- (void)createFakeGroup:(ContactInfoModel*)objectData{
    [MBProgressHUD1 showHUDAddedTo:self.view animated:YES];
    NSDictionary* parms = [[NSDictionary alloc] initWithObjectsAndKeys:[GlobalObject instance].generalData.currentUser.userId, KEY_USER_ID_A,objectData.userId,KEY_USER_ID_B,nil];
    
    
    [[AFNetworkingUtility instance] requestData:API_GET_CREATE_CHANNEL_FAKE withParameters:parms success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self hideLoading];
        id data = [responseObject objectForKey:KEY_DATA];
        if ([data isKindOfClass:[NSDictionary class]]) {
            NSError* err = nil;
            GroupBaseModel *group = [[GroupBaseModel alloc] initWithDictionary:data error:&err];
            if (err == nil) {
                [GlobalObject instance].callToUserID = objectData.userId;
                [GlobalObject instance].nextCallToUserID = objectData.userId;
                [[MQTTManager instance] subscribeChannel:[NSString stringWithFormat:@"g%d", group.groupId]];
                [[MQTTManager instance] sendInviteCall:group.groupName withGroupID:[NSString stringWithFormat:@"%d", group.groupId] withUserCall:objectData.userId];
                [GlobalObject instance].groupCallToUser = group;
            }
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [self showContactLoading:objectData.nickName];
        });
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self hideLoading];
    }];
}
- (void)showContactLoading:(NSString*)name{
    hud = [MBProgressHUD1 showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeDeterminateHorizontalBar1;
    
    hud.labelText = [NSString stringWithFormat:@"%@ %@ ...", getLocalizationString(@"contacting_user"), name];
    hud.progress = 0;
    [self contactingUserCountdown];
    userNameCall = name;
}
- (void)contactingUserCountdown{
    if (!contactingUser) {
        contactingUser = [NSTimer scheduledTimerWithTimeInterval:0.04f
                                                             target:self
                                                           selector:@selector(updateDataProgress:)
                                                           userInfo:nil
                                                            repeats:YES];
    }
}
- (void)updateDataProgress:(NSTimer *)timer{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        hud.progress += timer.timeInterval/[GET_SETTING_TIME_OUT_CONNECTING_CALL intValue];
    });
    if (hud.progress >= 1.0) {
        [hud hide:YES];
        [self contactingUserCountdownStop];
        [UIAlertView showWithTitle:@""
                           message:[NSString stringWithFormat:@"%@ %@", getLocalizationString(@"no_response_from"), userNameCall]
                 cancelButtonTitle:@"OK"
                 otherButtonTitles:nil
                          tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                          }];
    }
}
- (void)contactingUserCountdownStop{
    if ([contactingUser isValid]) {
        [contactingUser invalidate];
    }
    contactingUser = nil;
    if (hud) {
        [hud hide:YES];
    }
}
#pragma mark-UITextFieldDelegate
// search via WS
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    if (selectedGroup) {
        return YES;
    }
    currentPage = 1;
    [listUser removeAllObjects];
    if (IS_ECHO_MODE) {
        [listUser addObject:[self echouser]];
    }
    searchingString = textField.text;
    [self loadListContact];
    return YES;
}
// search when typing
-(void)textFieldDidChange:(UITextField*)textField
{
    if (selectedGroup) {
        if ([textField.text length] == 0) {
            typingFiltered = NO;
        }else{
            typingFiltered = YES;
            NSPredicate *predicate =[NSPredicate predicateWithFormat:@"nickName contains[cd] %@",textField.text];
            filteredArray = [listUser filteredArrayUsingPredicate:predicate];
        }
        filteredArray = [filteredArray sortedArrayUsingComparator:^NSComparisonResult(ContactInfoModel *obj1, ContactInfoModel *obj2) {
            return [obj1.nickName compare:obj2.nickName];
        }];
         [tbvContent reloadData];
    }
}


#pragma mark - MQTT MESSAGE
- (void)updateListUser:(id)notification{
    NSDictionary *statusInfo = [notification object];
    NSString *userID = [NSString stringWithFormat:@"%@", [statusInfo objectForKey:KEY_USER_ID]];
    NSString *status = [statusInfo objectForKey:KEY_STATUS];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userId = %@",userID];
    NSArray *filters = [listUser filteredArrayUsingPredicate:predicate];
    ContactInfoModel *objectUser = [filters firstObject];
    if(objectUser != nil){
        objectUser.status = status.intValue;
    }
    [tbvContent reloadData];
}
- (void)handleMakeCallUserToUser{
    [hud hide:NO];
    for (UIViewController *retVal in self.navigationController.viewControllers) {
        if ([retVal isKindOfClass:[HomeViewController class]]) {
            HomeViewController *homeViewController = (HomeViewController*)retVal;
            GroupBaseModel *groupInfo = [GlobalObject instance].groupCallToUser;
            [homeViewController handleCallWithGroupName:groupInfo.groupName withGroupID:groupInfo.groupId];
            [self contactingUserCountdownStop];
            break;
        }
    }
}


@end
