//
//  AmenitiesCartViewV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AmenitiesCategoryModelV2.h"
#import "AmenitiesItemModelV2.h"
#import "SectionAmenitiesCartViewV2.h"
#import "SectionAmenitiesCartInfoV2.h"
#import "AmenitiesItemCellV2.h"
#import "PickerViewV2.h"

@protocol AmenitiesCartViewV2Delegate <NSObject>

@optional
-(void) saveCartItem;

@end

@interface AmenitiesCartViewV2 : UIViewController<UITableViewDataSource, UITableViewDelegate, AmenitiesItemCellV2Delegate, PickerViewV2Delegate>{
    __unsafe_unretained id<AmenitiesCartViewV2Delegate> delegate;
    NSMutableArray *dataAmenitiesCart;
    NSIndexPath *indexpath;
    NSInteger amenitiesCartServiceId;
}
@property (strong, nonatomic) IBOutlet UILabel *lblGrandTotalAmenities;
@property (strong, nonatomic) AmenitiesCategoryModelV2 *amenitiesCategory;
@property (strong, nonatomic) AmenitiesItemModelV2 *amenitiesItem;
@property (strong, nonatomic) NSMutableArray *dataAmenitiesCart;
@property (strong, nonatomic) IBOutlet UITableView *tbvAmenitiesCart;
@property (strong, nonatomic) NSIndexPath *indexpath;
@property (strong, nonatomic) NSString *titleSectionAmenitiesLocation;
@property (strong, nonatomic) IBOutlet UILabel *lblGrandTotal;
@property (readwrite, nonatomic) NSInteger countGrandTotal;
@property (assign, nonatomic) NSInteger amenitiesCartServiceId;
@property (assign, nonatomic) id<AmenitiesCartViewV2Delegate> delegate;
@property (strong, nonatomic) NSString *msgbtnYes;
@property (strong, nonatomic) NSString *msgbtnNo;
@property (strong, nonatomic) NSString *msgSaveCount;
@property (strong, nonatomic) NSString *msgDiscardCount;
@property (assign, nonatomic) BOOL statusChooseQty;

-(void)saveAmenitiesCart;
-(void)setCaptionView;
@end
