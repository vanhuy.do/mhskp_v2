//
//  AmenitiesCartViewV2.m
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AmenitiesCartViewV2.h"
#import "AmenitiesManagerV2.h"
#import "AmenitiesOrdersModelV2.h"
#import "RoomManagerV2.h"
#import "UserManagerV2.h"
#import "MBProgressHUD.h"
#import "CountServiceModelV2.h"
#import "CustomAlertViewV2.h"
#import "CommonVariable.h"
#import "TasksManagerV2.h"
#import "MyNavigationBarV2.h"

#define tagAlertBack 10
#define tagAlertNo 11


@interface AmenitiesCartViewV2 (PrivateMethods)

#define tagTopbarView 1234

-(void) loadTopbarView;
-(void) showTopbarView;
-(void) hideTopbarView;
-(void) setHiddenTopbarView;
-(void) adjustShowForViews;
-(void) adjustRemoveForViews;
-(void) addButtonHandleShowHideTopbar;
-(void) topbarTapped:(UIButton *) sender;
-(TopbarViewV2 *) getTopBarView;
-(void) loadOtherAmenities;

@end

@implementation AmenitiesCartViewV2
@synthesize lblGrandTotalAmenities;
@synthesize amenitiesCategory;
@synthesize tbvAmenitiesCart;
@synthesize dataAmenitiesCart;
@synthesize amenitiesItem;
@synthesize indexpath;
@synthesize titleSectionAmenitiesLocation;
@synthesize lblGrandTotal;
@synthesize countGrandTotal;
@synthesize amenitiesCartServiceId;
@synthesize delegate;
@synthesize msgDiscardCount;
@synthesize msgSaveCount;
@synthesize msgbtnYes;
@synthesize msgbtnNo;
@synthesize statusChooseQty;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - Load Amenities Cart
-(void) addAmenitiestoSectionWithStatus:(BOOL) status AmenitiesItem: (AmenitiesItemsModelV2 *) item Quantity:(NSInteger) quantity AndSection:(SectionAmenitiesCartInfoV2 *) sectionAmenitiesCartInfo
{
    
    AmenitiesManagerV2 *manager = [[AmenitiesManagerV2 alloc] init];
 
    if (status == NO) {
        
        //load room type
        AmenitiesServiceModelV2 *roomType = [[AmenitiesServiceModelV2 alloc] init];
        roomType.amsId = item.itemRoomType;
        [manager loadAmenitiesServiceModelByPrimaryKey:roomType];
        
        sectionAmenitiesCartInfo.amenitiesService = roomType;
        
        //load category
        AmenitiesCategoriesModelV2 *amentitesCategory = [[AmenitiesCategoriesModelV2 alloc] init];
        amentitesCategory.amcaId = item.itemCategoryId;
        [manager loadAmenitiesCategoriesModelByPrimaryKey:amentitesCategory];
        
        AmenitiesCategoryModelV2 *amenitiesCategoryGUI = [[AmenitiesCategoryModelV2 alloc] init];
        amenitiesCategoryGUI.amenitiesCategoryId = amentitesCategory.amcaId;
        amenitiesCategoryGUI.amenitiesCategoryImage = UIImagePNGRepresentation([UIImage imageWithData:amentitesCategory.amcaImage]);
        amenitiesCategoryGUI.amenitiesCategoryTitle = amentitesCategory.amcaName;
        amenitiesCategoryGUI.amenitiesCategoryNameLang = amentitesCategory.amcaNameLang;
        
        sectionAmenitiesCartInfo.amenitiesCategory = amenitiesCategoryGUI;
        
        //insert item
        AmenitiesItemModelV2 *modelAmenitiesItem = [[AmenitiesItemModelV2 alloc] init];
        modelAmenitiesItem.amenitiesCategoryId = item.itemCategoryId;
        modelAmenitiesItem.amenitiesItemId = item.itemId;
        modelAmenitiesItem.amenitiesItemImage = UIImagePNGRepresentation([UIImage imageWithData:item.itemImage]);
        modelAmenitiesItem.amenitiesItemTitle = item.itemName;
        modelAmenitiesItem.amenitiesItemLang = item.itemLang;
        modelAmenitiesItem.amenitiesItemQuantity = quantity;
        
        [sectionAmenitiesCartInfo insertObjectToNextIndex:modelAmenitiesItem];
    }
    else
    {
        //insert item
        AmenitiesItemModelV2 *modelAmenitiesItem = [[AmenitiesItemModelV2 alloc] init];
        modelAmenitiesItem.amenitiesCategoryId = item.itemCategoryId;
        modelAmenitiesItem.amenitiesItemId = item.itemId;
        modelAmenitiesItem.amenitiesItemImage = UIImagePNGRepresentation([UIImage imageWithData:item.itemImage]);
        modelAmenitiesItem.amenitiesItemTitle = item.itemName;
        modelAmenitiesItem.amenitiesItemLang = item.itemLang;
        modelAmenitiesItem.amenitiesItemQuantity = quantity;
        
        [sectionAmenitiesCartInfo insertObjectToNextIndex:modelAmenitiesItem];
    }
    
}
-(void)loadOtherAmenities { 
    //enable flag to announce, is loading data
    [[HomeViewV2 shareHomeView] waitingLoadingData];

    AmenitiesManagerV2 *manager = [[AmenitiesManagerV2 alloc] init];
    dataAmenitiesCart = [NSMutableArray array];
    SectionAmenitiesCartInfoV2 *sectionAmenitiesCartInfo = nil;
    
    //load room type
    NSMutableArray *arrayRoomType = [manager loadAllAmenitiesService];
    
    for (AmenitiesServiceModelV2 *roomType in arrayRoomType) {
        
        //Count Order
        AmenitiesOrdersModelV2 *amenitiesOrder = [[AmenitiesOrdersModelV2 alloc] init];
        amenitiesOrder.amenRoomId = [TasksManagerV2 getCurrentRoomAssignment];
        amenitiesOrder.amenUserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
        amenitiesOrder.amenService = roomType.amsId;
        amenitiesOrder.amenStatus = tStatusChecked;
        amenitiesOrder.amenCollectDate = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd"];
        
        
        BOOL isCheckAmenitiesOrder = [manager isCheckAmenitiesOrder:amenitiesOrder];
        
        if (isCheckAmenitiesOrder == TRUE) {
            
            [manager loadAmenitiesOrdersModelByRoomIdUserIdAndServiceId:amenitiesOrder];
            
            //load All Amenities Order Detail By Order Id
            NSMutableArray *arrayOrderDetail = [manager loadAllAmenitiesOrderDetailsByOrderId:amenitiesOrder.amenId];
            
            
            for (AmenitiesOrderDetailsModelV2 *orderDetail in arrayOrderDetail) {
                
                //load item of order detail
                AmenitiesItemsModelV2 *item = [[AmenitiesItemsModelV2 alloc] init];
                item.itemId = orderDetail.amdItemId;
                item.itemCategoryId = orderDetail.amdCategoryId;
                item.itemRoomType = orderDetail.amdRoomTypeId;
                [manager loadAmenitiesItemsModelByPrimaryKey:item];
                    
                    
                //check item in dataAmenitiesCart
                BOOL isSection = NO;
                for (NSInteger indexSection=0; indexSection < dataAmenitiesCart.count; indexSection ++) {
                   
                    SectionAmenitiesCartInfoV2 *sectionCart = [dataAmenitiesCart objectAtIndex:indexSection];
    
                    if (sectionCart.amenitiesService.amsId == item.itemRoomType && sectionCart.amenitiesCategory.amenitiesCategoryId == item.itemCategoryId) {
                        
                        [self addAmenitiestoSectionWithStatus:YES AmenitiesItem:item Quantity:orderDetail.amdQuantity AndSection:sectionCart];
                        isSection = YES;
                        break;
                    }
                }
                
                if (isSection == NO) {
                    //new section
                    sectionAmenitiesCartInfo = [[SectionAmenitiesCartInfoV2  alloc] init];
                    [self addAmenitiestoSectionWithStatus:NO AmenitiesItem:item Quantity:orderDetail.amdQuantity AndSection:sectionAmenitiesCartInfo];
                    //insert section to cart
                    if (sectionAmenitiesCartInfo.rowHeights.count > 0) {
                        [dataAmenitiesCart addObject:sectionAmenitiesCartInfo];
                    }
                }
            }
                
        }
                
    }
    
    //insert item in amenities cart temp
    //load amenities item temp
    NSMutableArray *arrayOrderTemp = [manager loadAllAmenitiesItemOrderTempModel];
    
    for (AmenitiesItemsOrderTempModel *temp in arrayOrderTemp) {
        
        BOOL isCheckTemp = NO;
        BOOL isCheckSection = YES;
        SectionAmenitiesCartInfoV2 *sectionCart;
        
        //check amenities in dataAmenitiesCart
        for (NSInteger indexSection=0; indexSection < dataAmenitiesCart.count; indexSection ++) {
            
            sectionCart = [dataAmenitiesCart objectAtIndex:indexSection];
            
            
            for (AmenitiesItemModelV2 *item in sectionCart.rowHeights) {
                
                if (sectionCart.amenitiesService.amsId == temp.itemRoomtypeId && sectionCart.amenitiesCategory.amenitiesCategoryId == temp.itemCategoryId) {
                    isCheckSection = NO;
                }
                
                if (sectionCart.amenitiesService.amsId == temp.itemRoomtypeId && sectionCart.amenitiesCategory.amenitiesCategoryId == temp.itemId && item.amenitiesItemId == temp.itemId) {
                    
                    item.amenitiesItemQuantity += temp.itemQuantity;
                    isCheckTemp = YES;
                    break;
                }
                
            }
        }
            
            if (isCheckSection == YES) {
                
                //convert order temp to item
                AmenitiesItemsModelV2 *item = [[AmenitiesItemsModelV2 alloc] init];
                item.itemId = temp.itemId;
                item.itemCategoryId = temp.itemCategoryId;
                item.itemRoomType = temp.itemRoomtypeId;
                item.itemImage = temp.itemImage;
                item.itemLang = temp.itemLang;
                item.itemLastModified = temp.itemLastModified;
                item.itemName = temp.itemName;

                //new section
                sectionAmenitiesCartInfo = [[SectionAmenitiesCartInfoV2  alloc] init];
                [self addAmenitiestoSectionWithStatus:NO AmenitiesItem:item Quantity:temp.itemQuantity AndSection:sectionAmenitiesCartInfo];
                
                //insert section to cart
                if (sectionAmenitiesCartInfo.rowHeights.count > 0) {
                    [dataAmenitiesCart addObject:sectionAmenitiesCartInfo];
                }

            } else {
                if (isCheckTemp == NO) {
                    
                    //convert order temp to item
                    AmenitiesItemsModelV2 *item = [[AmenitiesItemsModelV2 alloc] init];
                    item.itemId = temp.itemId;
                    item.itemCategoryId = temp.itemCategoryId;
                    item.itemRoomType = temp.itemRoomtypeId;
                    item.itemImage = temp.itemImage;
                    item.itemLang = temp.itemLang;
                    item.itemLastModified = temp.itemLastModified;
                    item.itemName = temp.itemName;
                    [self addAmenitiestoSectionWithStatus:YES AmenitiesItem:item Quantity:temp.itemQuantity AndSection:sectionCart];
                }
            }
            
            
        
    }

    [tbvAmenitiesCart setDelegate:self];
    [tbvAmenitiesCart setDataSource:self];
    
    [tbvAmenitiesCart reloadData];
    
    
    //end loading data
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
}

#pragma mark - View lifecycle
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //Move to center vertical
    CGRect titleViewBounds = self.navigationController.navigationBar.topItem.titleView.bounds;
    self.navigationController.navigationBar.topItem.titleView.bounds = CGRectMake(titleViewBounds.origin.x, Y_TOPBAR_TITLE, titleViewBounds.size.width, titleViewBounds.size.height);
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //[self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:imgBackground]]];
    
    [self setTitle:[[LanguageManagerV2 sharedLanguageManager] getCart]];
    [lblGrandTotalAmenities setText:[[LanguageManagerV2 sharedLanguageManager] getGrandTotal]];
    [tbvAmenitiesCart reloadData];
    
    //[self loadOtherAmenities];

    for (NSInteger indexSection=0; indexSection < dataAmenitiesCart.count; indexSection ++) {
        SectionAmenitiesCartInfoV2 *sectionCart = [dataAmenitiesCart objectAtIndex:indexSection];
        for (NSInteger indexRow=0; indexRow < sectionCart.rowHeights.count; indexRow++) {
            AmenitiesItemModelV2 *model = [sectionCart.rowHeights objectAtIndex:indexRow];
            countGrandTotal += model.amenitiesItemQuantity;
        }
    }
    [lblGrandTotal setText:[NSString stringWithFormat:@"%d",countGrandTotal]];
    
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *img = [UIImage imageNamed:@"btn_back.png"];
    [button setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK] forState:UIControlStateNormal];
    [button setBackgroundImage:img forState:UIControlStateNormal];
    [button addTarget:self action:@selector(backBarPressed) forControlEvents:UIControlEventTouchUpInside];
    [button setFrame:CGRectMake(0, 0,WIDTH_BACK_BUTTON , HEIGHT_BACK_BUTTON)];
    [button.titleLabel setFont:[UIFont boldSystemFontOfSize:FONT_BACK_BUTTON]];
    [button setContentEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
    UIView *backButtonView = [[UIView alloc] initWithFrame:button.frame];
    backButtonView.bounds = CGRectMake(backButtonView.bounds.origin.x, Y_BACK_BUTTON, backButtonView.bounds.size.width, backButtonView.bounds.size.height);
    [backButtonView addSubview:button];
    
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButtonView];
    self.navigationItem.leftBarButtonItem = barButtonItem;
//    [barButtonItem release];
    
    [self performSelector:@selector(loadTopbarView)];
   

}

#pragma mark - Click back
-(void)backBarPressed {
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - === Handle Check Save Data ===
#pragma mark
-(BOOL) checkDataAmenitiesOrderCartTemp
{
    BOOL isCheckData = NO;
    AmenitiesManagerV2 *manager = [[AmenitiesManagerV2 alloc] init];
    NSMutableArray *array = [manager loadAllAmenitiesItemOrderTempModel];
    if (array.count > 0) {
        isCheckData = YES;
    }
    else
    {
        isCheckData = NO;
    }
    return isCheckData;
}
-(BOOL) checkSaveDataWithTagEvent:(NSInteger) tagOfEvent {
    
    BOOL isCheckOrder = [self checkDataAmenitiesOrderCartTemp];
    if (tagOfEvent == tagOfMessageButton) {
        if (isCheckOrder == NO) {
            return NO;
        }
        else
        {
            //Show alert please save before leaving
            UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:msgSaveCount delegate:self cancelButtonTitle:msgbtnYes otherButtonTitles:msgbtnNo, nil]; 
            alert.delegate = self;
            alert.tag = tagAlertBack;
            [alert show];
            
            return YES;

        }
    }
    else
    {
        //Show alert complete room
        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_complete_room_to_leave] delegate:self cancelButtonTitle:msgbtnYes otherButtonTitles: nil]; 
        alert.delegate = self;
        [alert show];
        
        return YES;

    }
    
}

#pragma mark - UIAlertView Delegate Methods
-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    switch (alertView.tag) {
        case tagAlertBack:
        {
            switch (buttonIndex)
            {
                case 0:
                {
                    //YES: save minibar
                    [self saveAmenitiesCart];
                    statusChooseQty = NO;
                    
                    //if tag is message
                    if ([[NSUserDefaults standardUserDefaults] integerForKey:TAG_OF_EVENT] == tagOfMessageButton) {
                        [[HomeViewV2 shareHomeView] handleTabbarEventWithTagOfButton:[[NSUserDefaults standardUserDefaults] integerForKey:TAG_OF_EVENT]];
                    }
                    
                }
                    break;
                    
                case 1:
                {
                    //NO: show popup discard
                    UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:msgDiscardCount delegate:self cancelButtonTitle:msgbtnYes otherButtonTitles:msgbtnNo, nil]; 
                    alert.delegate = self;
                    alert.tag = tagAlertNo;
                    [alert show];
                    
                }
                    break;
                    
                default:
                    break;
            }
        }
            break;
            
        case tagAlertNo:
        {
            switch (buttonIndex)
            {
                case 0:
                {
                    //YES 
                    
                    //clear cart temp
                    AmenitiesManagerV2 *manager = [[AmenitiesManagerV2 alloc] init];
                    [manager deleteAllAmenitiesItemsOrderTemp];
                    
                    //if tag is message
                    if ([[NSUserDefaults standardUserDefaults] integerForKey:TAG_OF_EVENT] == tagOfMessageButton) {

                        [[HomeViewV2 shareHomeView] handleTabbarEventWithTagOfButton:[[NSUserDefaults standardUserDefaults] integerForKey:TAG_OF_EVENT]];
                    }
                    else{
                        //back to count menu, no save
                        NSArray *arrayController = [self.navigationController viewControllers];
                        for (UIViewController *viewController in arrayController) {
                            if ([viewController isKindOfClass:[CountViewV2 class]]) {
                                [self.navigationController popToViewController:viewController animated:YES];
                                break;
                            }
                        }
                    }
                                   
                }
                    break;
                    
                case 1:
                {
                    //NO: cart screen
                    //do no thing
                    
                }
                    break;
                    
                default:
                    break;
            }
            
        }
            break;
            
        default:
            break;
    }
}


-(void)viewWillAppear:(BOOL)animated {
    //show wifi view
    [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationShowWifiView] object:nil];
    [super viewWillAppear:animated];
    [self setCaptionView];
    [self loadOtherAmenities];
    [tbvAmenitiesCart reloadData];
}

- (void)viewDidUnload
{
//    [self setTbvAmenitiesCart:nil];
//    [self setLblGrandTotal:nil];
//    [self setLblGrandTotalAmenities:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - UITableView Delegate methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return dataAmenitiesCart.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    SectionAmenitiesCartInfoV2 *sectioninfo = [dataAmenitiesCart objectAtIndex:section];
    NSInteger numberRowInSection = sectioninfo.rowHeights.count;
    return numberRowInSection;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 60;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 70;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    SectionAmenitiesCartInfoV2 *sectioninfo = [dataAmenitiesCart objectAtIndex:section];
    
    if (sectioninfo.headerView == nil) {
        NSData *imgdataArrow = UIImagePNGRepresentation([UIImage imageNamed:imgRightRowOpen]);
        
        //set language name amenities category cart
        NSString *titleSection = @"";
        if ([[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString: (NSString *)ENGLISH_LANGUAGE] == NO) {
            titleSection = [NSString stringWithFormat:@"%@ - %@", sectioninfo.amenitiesService.amsNameLang, sectioninfo.amenitiesCategory.amenitiesCategoryNameLang];
        }
        else{
            titleSection = [NSString stringWithFormat:@"%@ - %@", sectioninfo.amenitiesService.amsName, sectioninfo.amenitiesCategory.amenitiesCategoryTitle];
        }
        
        SectionAmenitiesCartViewV2 *sviewcart = [[SectionAmenitiesCartViewV2 alloc] initWithSection:sectioninfo.amenitiesCategory.amenitiesCategoryId contentImg:sectioninfo.amenitiesService.amsImage contentName:titleSection AndArrow:imgdataArrow];    
        sectioninfo.headerView = sviewcart;
    }
    
    return sectioninfo.headerView;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifyCell = @"IdentifyCell";
    AmenitiesItemCellV2 *cell = nil;
    cell = [tableView dequeueReusableCellWithIdentifier:identifyCell];
    if (cell == nil) {
        NSArray *array = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([AmenitiesItemCellV2 class]) owner:self options:nil];
        cell = [array objectAtIndex:0];
    }
    
    SectionAmenitiesCartInfoV2 *sectioninfo = [dataAmenitiesCart objectAtIndex:indexPath.section];
    AmenitiesItemModelV2 *model = [sectioninfo.rowHeights objectAtIndex:indexPath.row];
    
    
    [cell.imgAmenitiesItemCell setImage:[UIImage imageWithData:model.amenitiesItemImage]];
    //set language name amenities item
    if ([[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString: (NSString *)ENGLISH_LANGUAGE] == NO){
        [cell.lblAmenitiesItemCell setText:model.amenitiesItemLang];
    }
    else{
        [cell.lblAmenitiesItemCell setText:model.amenitiesItemTitle];
    }
    //set language quantity
    [cell.lblQuantityAmenitiesItemCell setText:[[LanguageManagerV2 sharedLanguageManager] getQuantity]];
    [cell.btnQuantityAmenitiesItemCell setTitle:[NSString stringWithFormat:@"%d",model.amenitiesItemQuantity ] forState:UIControlStateNormal];
    
    [cell setDelegate:self];
    [cell setIndexpath:indexPath];
    return  cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

#pragma mark - Picker View Delegate
-(void)btnAmenitiesItemCellV2PressedWithIndexpath:(NSIndexPath *)indexpath1 sender:(UIButton *)button{
    statusChooseQty = YES;
    NSMutableArray *array = [[NSMutableArray alloc] init];
    for (NSInteger index = 0; index <= 10; index++) {
        [array addObject:[NSString stringWithFormat:@"%d", index]];
    }
    
    PickerViewV2 *picker = [[PickerViewV2 alloc] initPickerViewV2WithDatas:array AndSelectedData:button.titleLabel.text AndIndex:indexpath1];
    [picker showPickerViewV2];
    [picker setDelegate:self];
}

-(void)didChoosePickerViewV2WithData:(NSString *)data AndIndex:(NSIndexPath *)index{
    countGrandTotal = 0;
    AmenitiesManagerV2 *manager = [[AmenitiesManagerV2 alloc] init];
    SectionAmenitiesCartInfoV2 *sectioninfo = [dataAmenitiesCart objectAtIndex:index.section];
    AmenitiesItemModelV2 *model = [sectioninfo.rowHeights objectAtIndex:index.row];
    model.amenitiesItemQuantity = [data integerValue];
    
    for (NSInteger indexSection=0; indexSection < dataAmenitiesCart.count; indexSection ++) {
        SectionAmenitiesCartInfoV2 *sectionCart = [dataAmenitiesCart objectAtIndex:indexSection];
        for (NSInteger indexRow=0; indexRow < sectionCart.rowHeights.count; indexRow++) {
            AmenitiesItemModelV2 *model = [sectionCart.rowHeights objectAtIndex:indexRow];
            countGrandTotal += model.amenitiesItemQuantity;
            
            //load amenities cart temp from database
            AmenitiesItemsOrderTempModel *orderTemp = [[AmenitiesItemsOrderTempModel alloc] init];
            orderTemp.itemId = model.amenitiesItemId;
            orderTemp.itemCategoryId = sectionCart.amenitiesCategory.amenitiesCategoryId;
            orderTemp.itemRoomtypeId = sectionCart.amenitiesService.amsId;
            orderTemp.itemImage = model.amenitiesItemImage;
            orderTemp.itemLang = model.amenitiesItemLang;
            orderTemp.itemName = model.amenitiesItemTitle;
            [manager loadAmenitiesItemsOrderTempModelByPrimaryKey:orderTemp];
            
            //load amenities order detail
            AmenitiesOrderDetailsModelV2 *orderDetail = [[AmenitiesOrderDetailsModelV2 alloc] init];
            orderDetail.amdItemId = model.amenitiesItemId;
            orderDetail.amdCategoryId = sectionCart.amenitiesCategory.amenitiesCategoryId;
            orderDetail.amdRoomTypeId = sectionCart.amenitiesService.amsId;
            [manager loadAmenitiesOrderDetailsModelByPrimaryKey:orderDetail];
            
            if (model.amenitiesItemQuantity > 0) {
                
                NSInteger sumItem = orderTemp.itemQuantity + orderDetail.amdQuantity;
                if (sumItem != model.amenitiesItemQuantity) {
                    
                    //remove order
                    [manager deleteAmenitiesOrderDetailsModel:orderDetail];
                    
                    //update to amenities cart temp
                    orderTemp.itemQuantity = model.amenitiesItemQuantity;
                    
                    
                    BOOL result = [manager insertAmenitiesItemsOrderTempModel:orderTemp];                    
                    if (result == NO) {
                        [manager updateAmenitiesItemsOrderTempModel:orderTemp];
                    }
                }

            }
            else
            {
                //remove order
                [manager deleteAmenitiesOrderDetailsModel:orderDetail];
                
                //remove temp
                [manager deleteAmenitiesItemsOrderTempModel:orderTemp];
            }
        }
    }
    
    [lblGrandTotal setText:[NSString stringWithFormat:@"%d",countGrandTotal]];
    
    [tbvAmenitiesCart reloadRowsAtIndexPaths:[NSArray arrayWithObject:index] withRowAnimation:UITableViewRowAnimationFade];

}

#pragma mark - Save Cart Minibar
-(void) saveData{
    BOOL isCheckOrder = [self checkDataAmenitiesOrderCartTemp];
    if (isCheckOrder == YES) {
        [self saveAmenitiesCart];
        statusChooseQty = NO;
    }
}

-(void)saveAmenitiesCart
{
    // @ show loading data .
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getRaSavingData]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES]; 
    
    ///Save amenities cart
    AmenitiesManagerV2 *manager = [[AmenitiesManagerV2 alloc] init];
    SyncManagerV2 *synManager = [[SyncManagerV2 alloc] init];
    NSInteger userId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    NSInteger roomAssignId = [TasksManagerV2 getCurrentRoomAssignment];
    
    NSMutableArray *arrayRoomType = [manager loadAllAmenitiesService];
    
    for (AmenitiesServiceModelV2 *roomType in arrayRoomType) {
        
        NSMutableArray *arrayAmenitiesOrderTemp = [manager loadAllAmenitiesItemOrderTempByRoomType:roomType.amsId];
        
        if (arrayAmenitiesOrderTemp.count > 0) {
            for (AmenitiesItemsOrderTempModel *orderTemp in arrayAmenitiesOrderTemp) {
                
                //load Order in amenities
                AmenitiesOrdersModelV2 *amenitiesOrder = [[AmenitiesOrdersModelV2 alloc] init];
                amenitiesOrder.amenRoomId = [TasksManagerV2 getCurrentRoomAssignment];
                amenitiesOrder.amenUserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
                amenitiesOrder.amenService = roomType.amsId;
                amenitiesOrder.amenStatus = tStatusChecked;
                amenitiesOrder.amenCollectDate = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd"];
                
                
                BOOL isCheckAmenitiesOrder = [manager isCheckAmenitiesOrder:amenitiesOrder];
                
                if (isCheckAmenitiesOrder == FALSE) {
                    [manager insertAmenitiesOrdersModel:amenitiesOrder];
                }
                
                //load amenities order
                [manager loadAmenitiesOrdersModelByRoomIdUserIdAndServiceId:amenitiesOrder];
                
                //load amenities order detail
                AmenitiesOrderDetailsModelV2 *orderDetail = [[AmenitiesOrderDetailsModelV2 alloc] init];
                orderDetail.amdItemId = orderTemp.itemId;
                orderDetail.amdRoomTypeId = orderTemp.itemRoomtypeId;
                orderDetail.amdCategoryId = orderTemp.itemCategoryId;
                
                AmenitiesOrderDetailsModelV2 *amenitiesOrderDetailModel = [manager loadAmenitiesOrderDetailsByItemId:orderDetail AndOrderId:amenitiesOrder.amenId];
                
                if (amenitiesOrderDetailModel.amdId != 0) {
                    
                    //update Order detail in amenities
                    amenitiesOrderDetailModel.amdQuantity = orderTemp.itemQuantity;
                    
                    [manager updateAmenitiesOrderDetailsModel:amenitiesOrderDetailModel];
                }
                else
                {
                    //insert Order detail in amenities
                    AmenitiesOrderDetailsModelV2 *amenitiesOrderDetailModel = [[AmenitiesOrderDetailsModelV2 alloc] init];
                    amenitiesOrderDetailModel.amdAmenitiesOrderId = amenitiesOrder.amenId;
                    amenitiesOrderDetailModel.amdItemId = orderTemp.itemId;
                    amenitiesOrderDetailModel.amdQuantity = orderTemp.itemQuantity;
                    amenitiesOrderDetailModel.amdPostStatus = POST_STATUS_SAVED_UNPOSTED;
                    amenitiesOrderDetailModel.amdCategoryId = orderTemp.itemCategoryId;
                    amenitiesOrderDetailModel.amdRoomTypeId = orderTemp.itemRoomtypeId;
                    
                    [manager insertAmenitiesOrderDetailsModel:amenitiesOrderDetailModel];
                    
                }
                
                //--------------------------post to server WS-----------------
                AmenitiesOrderDetailsModelV2 *orderDetailPost = [[AmenitiesOrderDetailsModelV2 alloc] init];
                orderDetailPost.amdItemId = orderTemp.itemId;
                orderDetailPost.amdRoomTypeId = orderTemp.itemRoomtypeId;
                orderDetailPost.amdCategoryId = orderTemp.itemCategoryId;
                
                AmenitiesOrderDetailsModelV2 *amenitiesOrderDetailPost = [manager loadAmenitiesOrderDetailsByItemId:orderDetailPost AndOrderId:amenitiesOrder.amenId];
                
                BOOL statusPost = [synManager postAmenitiesItemWithUserId:userId RoomAssignId:roomAssignId AndAmenitiesItem:amenitiesOrderDetailPost WithDate:amenitiesOrder.amenCollectDate];

                if (statusPost == YES) {
                    //post success, clear database
                    [manager deleteAmenitiesOrderDetailsModel:amenitiesOrderDetailPost];
                    
                }
                [manager deleteAmenitiesItemsOrderTempModel:orderTemp];
                
                [tbvAmenitiesCart reloadData];
                //--------------------------End------------------------------
            }
            
        }
        
    }
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getMsgSaveMessage]];
    //hide saving data.
    [self performSelector:@selector(hiddenHUDAfterSaved:) withObject:HUD afterDelay:0.5];
    
    if ([delegate respondsToSelector:@selector(saveCartItem)]) {
        [delegate saveCartItem];
    }
    
}

#pragma mark - Set Caption View
-(void)setCaptionView
{
    msgbtnNo = [[LanguageManagerV2 sharedLanguageManager] getNo];
    msgbtnYes = [[LanguageManagerV2 sharedLanguageManager] getYes];
    msgSaveCount = [[LanguageManagerV2 sharedLanguageManager] getMsgAlertSaveCounts];
    msgDiscardCount = [[LanguageManagerV2 sharedLanguageManager] getMsgAlertDiscardCounts];
    
    //refresh topview
    TopbarViewV2 *topview = [self getTopBarView];
    [topview refresh:[[SuperRoomModelV2 alloc] init]];
}

#pragma didden after save.
-(void) hiddenHUDAfterSaved:(MBProgressHUD *)HUD{
    [HUD hide:YES];
    [HUD removeFromSuperview];
//    [HUD release];
}


- (void)dealloc {
//    [tbvAmenitiesCart release];
//    [lblGrandTotal release];
//    [lblGrandTotalAmenities release];
//    [super dealloc];
}

#pragma mark - Handle Topbar Methods
-(void)loadTopbarView {
    TopbarViewV2 *topbar = [[TopbarViewV2 alloc] initViewWithSuperModel:[[CommonVariable sharedCommonVariable] superRoomModel]];
    [topbar setTag:tagTopbarView];
    [self.view addSubview:topbar];
    [topbar setHidden:YES];
    
    [topbar setAlpha:0.0];
    [self addButtonHandleShowHideTopbar];
}

-(void)showTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationWillStartSelector:@selector(setHiddenTopbarView)];
    
    f = topbar.frame;
    f.origin.y = 0;
    [topbar setFrame:f];
    [topbar setAlpha:1.0];
    
    [self adjustShowForViews];
    
    [UIView commitAnimations];
}

-(void)hideTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDidStopSelector:@selector(setHiddenTopbarView)];
    [UIView setAnimationDelegate:self];
    
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    [topbar setAlpha:0.0];
    
    [self adjustRemoveForViews];
    
    [UIView commitAnimations];
}

-(void)setHiddenTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    [topbar setHidden:!topbar.hidden];
}

-(TopbarViewV2 *)getTopBarView {
    return (TopbarViewV2 *)[self.view viewWithTag:tagTopbarView];
}

-(void)addButtonHandleShowHideTopbar {
    UIButton *titleBarButton = [[UIButton alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
    
    titleBarButton.titleLabel.backgroundColor = [UIColor clearColor];
    titleBarButton.titleLabel.font = FONT_BUTTON_TOPBAR;
    titleBarButton.titleLabel.textAlignment = UITextAlignmentCenter;
    titleBarButton.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
    [titleBarButton.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
    [titleBarButton.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
    [titleBarButton addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    [titleBarButton setTitle:[self title] forState:UIControlStateNormal];
    self.navigationItem.titleView = titleBarButton;
}

-(void)topbarTapped:(UIButton *)sender {
    TopbarViewV2 *topbar = [self getTopBarView];
    [sender setEnabled:NO];
    if (topbar.isHidden) {
        [self showTopbarView];
    } else {
        [self hideTopbarView];
    }
    [self performSelector:@selector(topbarButtonEnable:) withObject:sender afterDelay:0.5];
}

-(void) topbarButtonEnable:(UIButton *) topbarBtn {
    [topbarBtn setEnabled:YES];
}

-(void)adjustShowForViews {
    //adjust all views when show 
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    CGRect ftbv = tbvAmenitiesCart.frame;
    [tbvAmenitiesCart setFrame:CGRectMake(0, f.size.height, 320, ftbv.size.height - f.size.height)];
}

-(void)adjustRemoveForViews { 
    //adjust all views when hide
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    CGRect ftbv = tbvAmenitiesCart.frame;
    [tbvAmenitiesCart setFrame:CGRectMake(0, 0, 320, ftbv.size.height + f.size.height)];
}

@end
