//
//  AmenitiesCategoryCellV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 1/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AmenitiesCategoryCellV2 : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *imgAmenitiesCategoryCell;
@property (strong, nonatomic) IBOutlet UILabel *lblAmenitiesCategoryCell;
@property (strong, nonatomic) IBOutlet UIImageView *imgRowAmenitiesCategoryCell;

@end
