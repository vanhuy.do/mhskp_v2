//
//  AmenitiesCategoryCellV2.m
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 1/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AmenitiesCategoryCellV2.h"

@implementation AmenitiesCategoryCellV2
@synthesize imgAmenitiesCategoryCell, imgRowAmenitiesCategoryCell, lblAmenitiesCategoryCell;

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
