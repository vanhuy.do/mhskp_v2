//
//  AmenitiesCategoryViewV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 1/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AmenitiesCategoryCellV2.h"
#import "AmenitiesCategoryModelV2.h"
#import "AmenitiesLocationModelV2.h"
#import "AmenitiesLocationViewV2.h"
#import "AmenitiesItemViewV2.h"
#import "AmenitiesItemModelV2.h"
#import "MBProgressHUD.h"

@interface AmenitiesCategoryViewV2 : UIViewController<UITableViewDataSource,UITableViewDelegate>{
    NSMutableArray *datas;
    NSInteger serviceId;
}

@property (nonatomic, assign) NSInteger serviceId;
@property (strong, nonatomic) IBOutlet UITableView *tbvAmenitiesCategory;
@property (nonatomic, strong) IBOutlet AmenitiesCategoryCellV2 *amenitiesCategoryCell;
@property (strong, nonatomic) NSMutableArray *datas;
@property (strong, nonatomic) AmenitiesCategoryModelV2 *amenitiesCategoryModel;

-(void) loadDataAmenitiesCategory;
@end
