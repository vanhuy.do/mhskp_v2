//
//  AmenitiesItemCellV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 1/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AmenitiesItemCellV2Delegate <NSObject>

@optional
-(void) btnAmenitiesItemCellV2PressedWithIndexpath:(NSIndexPath *) indexpath sender:(UIButton *) button;

@end

@interface AmenitiesItemCellV2 : UITableViewCell{
    __unsafe_unretained id<AmenitiesItemCellV2Delegate> delegate;
    NSIndexPath *indexpath;
}
@property (strong, nonatomic) NSIndexPath *indexpath;
@property (assign, nonatomic) id<AmenitiesItemCellV2Delegate> delegate;
@property (strong, nonatomic) IBOutlet UIImageView *imgAmenitiesItemCell;
@property (strong, nonatomic) IBOutlet UILabel *lblAmenitiesItemCell;
@property (strong, nonatomic) IBOutlet UILabel *lblQuantityAmenitiesItemCell;
@property (strong, nonatomic) IBOutlet UIButton *btnQuantityAmenitiesItemCell;
- (IBAction)btnQuantityAmenitiesItemCellPressed:(id)sender;

@end
