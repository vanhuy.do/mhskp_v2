//
//  AmenitiesItemCellV2.m
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 1/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AmenitiesItemCellV2.h"

@implementation AmenitiesItemCellV2
@synthesize btnQuantityAmenitiesItemCell;
@synthesize imgAmenitiesItemCell;
@synthesize lblAmenitiesItemCell;
@synthesize lblQuantityAmenitiesItemCell;
@synthesize delegate;
@synthesize indexpath;


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc {
//    [imgAmenitiesItemCell release];
//    [lblAmenitiesItemCell release];
//    [lblQuantityAmenitiesItemCell release];
//    [btnQuantityAmenitiesItemCell release];
//    [super dealloc];
}
- (IBAction)btnQuantityAmenitiesItemCellPressed:(id)sender {
    if([delegate respondsToSelector:@selector(btnAmenitiesItemCellV2PressedWithIndexpath:sender:)])
    {
        [delegate btnAmenitiesItemCellV2PressedWithIndexpath:indexpath sender:sender];
    }
}
@end
