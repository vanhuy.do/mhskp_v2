//
//  AmenitiesItemViewV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 1/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AmenitiesCategoryViewV2.h"
#import "AmenitiesCategoryModelV2.h"
#import "AmenitiesItemCellV2.h"
#import "PickerViewV2.h"
#import "AmenitiesCartViewV2.h"


@interface AmenitiesItemViewV2 : UIViewController <UITableViewDataSource, UITableViewDelegate, AmenitiesItemCellV2Delegate, PickerViewV2Delegate, AmenitiesCartViewV2Delegate>
{
    NSMutableArray *datas;
    NSInteger categoriesId;
    NSInteger amenitiesServiceId;
    NSMutableArray *cartdatas;
}

@property (nonatomic, strong) NSMutableArray *cartdatas;
@property (strong, nonatomic) IBOutlet UIImageView *imgAmenitiesItem;
@property (strong, nonatomic) IBOutlet UILabel *lblAmenitiesItem;
@property (strong, nonatomic) IBOutlet UITableView *tbvAmenitiesItem;
@property (strong, nonatomic) AmenitiesCategoryModelV2 *amenitiesCategoryWithItem;
@property (strong, nonatomic) NSMutableArray *datas;
@property (strong, nonatomic) IBOutlet UIButton *btnCart;
@property (strong, nonatomic) NSMutableArray *datasAmenities;
@property (assign, nonatomic) NSInteger categoriesId;
@property (assign, nonatomic) NSInteger amenitiesServiceId;
@property (strong, nonatomic) IBOutlet UIButton *btnAddtoCart;
@property (assign, nonatomic) BOOL statusChooseQty;
@property (assign, nonatomic) BOOL statusSave;
@property (assign, nonatomic) NSInteger sumAmenitiesCartQty;
@property (strong, nonatomic) NSString *msgbtnYes;
@property (strong, nonatomic) NSString *msgbtnNo;
@property (strong, nonatomic) NSString *msgSaveCount;
@property (strong, nonatomic) NSString *msgDiscardCount;
@property (strong, nonatomic) IBOutlet UIView *vHeaderAmenities;
@property (assign, nonatomic) BOOL statusAddToCart;

- (IBAction)btnAddtoCartAmenitiesItem:(id)sender;
-(void) handleBtnAddToCartPressed;
- (void) loadDataAmenitiesItem;
- (void) saveAmenitiesCart;
- (void) setCaptionView;

@end
