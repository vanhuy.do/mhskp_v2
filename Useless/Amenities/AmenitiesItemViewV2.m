//
//  AmenitiesItemViewV2.m
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 1/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AmenitiesItemViewV2.h"
#import "AmenitiesManagerV2.h"
#import "MBProgressHUD.h"
#import "CountViewV2.h"
#import "AmenitiesOrdersModelV2.h"
#import "RoomManagerV2.h"
#import "UserManagerV2.h"
#import "CustomAlertViewV2.h"
#import "CommonVariable.h"
#import "TasksManagerV2.h"
#import "MyNavigationBarV2.h"

#define tagAlertBack 10
#define tagAlertNo 11
#define tagAlertBottom 12

@interface AmenitiesItemViewV2(privatemethods)

-(void) btnCartPressed;
-(NSInteger) loadOrtherDataAmenitiesItem;

#define tagTopbarView 1234

-(void) loadTopbarView;
-(void) showTopbarView;
-(void) hideTopbarView;
-(void) setHiddenTopbarView;
-(void) adjustShowForViews;
-(void) adjustRemoveForViews;
-(void) addButtonHandleShowHideTopbar;
-(void) topbarTapped:(UIButton *) sender;
-(TopbarViewV2 *) getTopBarView;

@end

@implementation AmenitiesItemViewV2
@synthesize imgAmenitiesItem;
@synthesize lblAmenitiesItem;
@synthesize tbvAmenitiesItem;
@synthesize amenitiesCategoryWithItem;
@synthesize datas;
@synthesize btnCart;
@synthesize datasAmenities;
@synthesize statusChooseQty;
@synthesize categoriesId;
@synthesize amenitiesServiceId;
@synthesize btnAddtoCart;
@synthesize sumAmenitiesCartQty;
@synthesize cartdatas;
@synthesize msgbtnNo;
@synthesize msgbtnYes;
@synthesize msgSaveCount;
@synthesize msgDiscardCount;
@synthesize vHeaderAmenities;
@synthesize statusSave;
@synthesize statusAddToCart;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

-(void)viewWillAppear:(BOOL)animated {
    //hide wifi view
    [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationHideWifiView] object:nil];
    [super viewWillAppear:animated];

    [self setCaptionView];
    
    [self loadDataAmenitiesItem];
    
    [self handleBtnAddToCartPressed];
    
    //hide wifi view
    [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationHideWifiView] object:nil];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    //Move to center vertical
    CGRect titleViewBounds = self.navigationController.navigationBar.topItem.titleView.bounds;
    self.navigationController.navigationBar.topItem.titleView.bounds = CGRectMake(titleViewBounds.origin.x, Y_TOPBAR_TITLE, titleViewBounds.size.width, titleViewBounds.size.height);
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //[self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:imgBackground]]];
    
    statusSave = NO;
    
    btnCart = [[UIButton alloc]initWithFrame:CGRectMake(236, 6, 60, 32)];
    [btnCart setBackgroundImage:[UIImage imageNamed:imgTopCart] forState:UIControlStateNormal];
    [btnCart setTitle:[NSString stringWithFormat:@"%d", sumAmenitiesCartQty] forState:UIControlStateNormal];   
    [btnCart setTitleEdgeInsets:UIEdgeInsetsMake(0, 32, 0, 0)];
    [btnCart.titleLabel setFont:[UIFont fontWithName:@"Arial-BoldMT" size:15]];
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithCustomView:btnCart];
    self.navigationItem.rightBarButtonItem = rightButton;    
    
    [btnCart addTarget:self action:@selector(btnCartPressed) forControlEvents:UIControlEventTouchUpInside];
    
    [imgAmenitiesItem setImage:[UIImage imageWithData:amenitiesCategoryWithItem.amenitiesCategoryImage]];
    
    //set language name amenities category 
    if ([[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString: (NSString *)ENGLISH_LANGUAGE] == NO) {
        [lblAmenitiesItem setText:amenitiesCategoryWithItem.amenitiesCategoryNameLang];
    }
    else{
        [lblAmenitiesItem setText:amenitiesCategoryWithItem.amenitiesCategoryTitle];
    }
        
    [self loadDataAmenitiesItem];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *img = [UIImage imageNamed:@"btn_back.png"];
    [button setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK] forState:UIControlStateNormal];
    [button setBackgroundImage:img forState:UIControlStateNormal];
    [button addTarget:self action:@selector(backBarPressed) forControlEvents:UIControlEventTouchUpInside];
    [button setFrame:CGRectMake(0, 0,WIDTH_BACK_BUTTON , HEIGHT_BACK_BUTTON)];
    [button.titleLabel setFont:[UIFont boldSystemFontOfSize:FONT_BACK_BUTTON]];
    [button setContentEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
    UIView *backButtonView = [[UIView alloc] initWithFrame:button.frame];
    backButtonView.bounds = CGRectMake(backButtonView.bounds.origin.x, Y_BACK_BUTTON, backButtonView.bounds.size.width, backButtonView.bounds.size.height);
    [backButtonView addSubview:button];
    
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButtonView];
    self.navigationItem.leftBarButtonItem = barButtonItem;

    [self performSelector:@selector(loadTopbarView)];
}

#pragma mark - Click back
-(void)backBarPressed {
    //clear previous tag of event
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:TAG_OF_EVENT];
    
    if(statusChooseQty == NO)
    {
        [self.navigationController popViewControllerAnimated:YES];        
    }
    else
    {
        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_confirm_add_to_cart] delegate:self cancelButtonTitle:msgbtnYes otherButtonTitles:msgbtnNo, nil]; 
        alert.delegate = self;
        alert.tag = tagAlertBack;
        [alert show];             
    }
}

#pragma mark - === Handle Check Save Data ===
#pragma mark
-(BOOL) checkDataAmenitiesOrderCartTemp
{
    BOOL isCheckData = NO;
    AmenitiesManagerV2 *manager = [[AmenitiesManagerV2 alloc] init];
    NSMutableArray *array = [manager loadAllAmenitiesItemOrderTempModel];
    if (array.count > 0) {
        isCheckData = YES;
    }
    else
    {
        isCheckData = NO;
    }
    return isCheckData;
}
-(BOOL) checkSaveDataWithTagEvent:(NSInteger) tagOfEvent {
    
    BOOL isCheckOrder = [self checkDataAmenitiesOrderCartTemp];
    if (tagOfEvent == tagOfMessageButton) {
        if (isCheckOrder == NO) {
            return NO;
        }
        else
        {
            //Show alert please save before leaving
            UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:[[LanguageManagerV2 sharedLanguageManager] getMsgAlertSaveCounts] delegate:self cancelButtonTitle:msgbtnYes otherButtonTitles:msgbtnNo, nil]; 
            alert.delegate = self;
            alert.tag = tagAlertBottom;
            [alert show];
            
            return YES;

        }
    }
    else
    {
        //Show alert complete room
        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_complete_room_to_leave] delegate:self cancelButtonTitle:msgbtnYes otherButtonTitles: nil]; 
        alert.delegate = self;
        [alert show];
        
        return YES;

    }
    
}

#pragma mark - UIAlertView Delegate Methods
-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    switch (alertView.tag) {
        case tagAlertBack:
        {
            switch (buttonIndex)
            {
                case 0:
                {
                    //YES: add to cart, no save
                    [self btnAddtoCartAmenitiesItem:btnCart];
                    
                }
                    break;
                    
                case 1:
                {
                    //NO: show popup discard
                    UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:msgDiscardCount delegate:self cancelButtonTitle:msgbtnYes otherButtonTitles:msgbtnNo, nil]; 
                    alert.delegate = self;
                    alert.tag = tagAlertNo;
                    [alert show];  
                    
                }
                    break;
                    
                default:
                    break;
            }
        }
            break;
            
        case tagAlertBottom:
        {
            switch (buttonIndex)
            {
                case 0:
                {
                    //YES: save data
                    [self saveAmenitiesCart];
                    statusChooseQty = NO;
                    
                    //if tag is message
                    if ([[NSUserDefaults standardUserDefaults] integerForKey:TAG_OF_EVENT] == tagOfMessageButton) {
                        [[HomeViewV2 shareHomeView] handleTabbarEventWithTagOfButton:[[NSUserDefaults standardUserDefaults] integerForKey:TAG_OF_EVENT]];
                    }
                    
                }
                    break;
                    
                case 1:
                {
                    //NO: show popup discard
                    UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:msgDiscardCount delegate:self cancelButtonTitle:msgbtnYes otherButtonTitles:msgbtnNo, nil]; 
                    alert.delegate = self;
                    alert.tag = tagAlertNo;
                    [alert show];  
                    
                    
                }
                    break;
                    
                default:
                    break;
            }
        }
            break;
            
        case tagAlertNo:
        {
            switch (buttonIndex)
            {
                case 0:
                {
                    //YES
                    //clear cart temp
                    AmenitiesManagerV2 *manager = [[AmenitiesManagerV2 alloc] init];
                    [manager deleteAllAmenitiesItemsOrderTemp];
                    
                    //if tag is message
                    if ([[NSUserDefaults standardUserDefaults] integerForKey:TAG_OF_EVENT] == tagOfMessageButton) {
                        
                        [[HomeViewV2 shareHomeView] handleTabbarEventWithTagOfButton:[[NSUserDefaults standardUserDefaults] integerForKey:TAG_OF_EVENT]];
                    }
                    else{
                        //back to count menu, no save
                        NSArray *arrayController = [self.navigationController viewControllers];
                        for (UIViewController *viewController in arrayController) {
                            if ([viewController isKindOfClass:[CountViewV2 class]]) {
                                [self.navigationController popToViewController:viewController animated:YES];
                                break;
                            }
                        }
                    }

                }
                    break;
                    
                case 1:
                {
                    //NO: cart screen
                    //[self btnAddtoCartAmenitiesItem:nil];
                    //[self btnCartPressed];
                    
                }
                    break;
                    
                default:
                    break;
            }
            
        }
            break;
            
        default:
            break;
    }
}



- (void)viewDidUnload
{
//    [self setImgAmenitiesItem:nil];
//    [self setLblAmenitiesItem:nil];
//    [self setTbvAmenitiesItem:nil];
//    [self setBtnAddtoCart:nil];
//    [self setVHeaderAmenities:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Load Amenities Item
-(NSInteger) loadOrtherDataAmenitiesItem{
    
    NSInteger count = 0;
    
    AmenitiesManagerV2 *manager = [[AmenitiesManagerV2 alloc] init];
    
    NSMutableArray *arrayAmenitiesService = [manager loadAllAmenitiesService];
    
    for (AmenitiesServiceModelV2 *amenitiesService in arrayAmenitiesService) {
        //Count Order
        AmenitiesOrdersModelV2 *amenitiesOrder = [[AmenitiesOrdersModelV2 alloc] init];
        amenitiesOrder.amenRoomId = [TasksManagerV2 getCurrentRoomAssignment];
        amenitiesOrder.amenUserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
        
        amenitiesOrder.amenStatus = tStatusChecked;
        amenitiesOrder.amenCollectDate = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd"];
        amenitiesOrder.amenService = amenitiesService.amsId;
        
        BOOL isCheckAmenitiesOrder = [manager isCheckAmenitiesOrder:amenitiesOrder];
        
        if (isCheckAmenitiesOrder == TRUE) {
            [manager loadAmenitiesOrdersModelByRoomIdUserIdAndServiceId:amenitiesOrder];
        }
        
        NSMutableArray *arrayAmenitiesOrderDetail = [manager loadAllAmenitiesOrderDetailsByOrderId:amenitiesOrder.amenId];
        for (AmenitiesOrderDetailsModelV2 *modelAmenitiesOrderDetail in arrayAmenitiesOrderDetail) {
            count += modelAmenitiesOrderDetail.amdQuantity;
        }
    }
        
    return count;
}

-(void)loadDataAmenitiesItem{
    //enable flag to announce, is loading data
    [[HomeViewV2 shareHomeView] waitingLoadingData];

    AmenitiesManagerV2 *manager = [[AmenitiesManagerV2 alloc] init];
    
    datasAmenities = [[NSMutableArray alloc] init];
    datas = [[NSMutableArray alloc] init];
    
    SectionAmenitiesCartInfoV2 *sectionAmenitiesCartInfo = [[SectionAmenitiesCartInfoV2  alloc] init];
    sectionAmenitiesCartInfo.amenitiesCategory = amenitiesCategoryWithItem;
    
    //Count Order
    AmenitiesOrdersModelV2 *amenitiesOrder = [[AmenitiesOrdersModelV2 alloc] init];
    amenitiesOrder.amenRoomId = [TasksManagerV2 getCurrentRoomAssignment];
    amenitiesOrder.amenUserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    amenitiesOrder.amenService = amenitiesServiceId;
    amenitiesOrder.amenStatus = tStatusChecked;
    amenitiesOrder.amenCollectDate = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd"];
    
    
    BOOL isCheckAmenitiesOrder = [manager isCheckAmenitiesOrder:amenitiesOrder];
    
    if (isCheckAmenitiesOrder == TRUE) {
        [manager loadAmenitiesOrdersModelByRoomIdUserIdAndServiceId:amenitiesOrder];
    }
    
    NSMutableArray *array = [manager loadAllAmenitiesItemsByCategoriesId:categoriesId AndRoomTypeId:amenitiesServiceId];
    

    for (AmenitiesItemsModelV2 *amenitiesItemsModel in array) {
        
        AmenitiesOrderDetailsModelV2 *orderDetail = [[AmenitiesOrderDetailsModelV2 alloc] init];
        orderDetail.amdId = amenitiesItemsModel.itemId;
        orderDetail.amdRoomTypeId = amenitiesServiceId;
        orderDetail.amdCategoryId = amenitiesItemsModel.itemCategoryId;
        AmenitiesOrderDetailsModelV2 *amenitiesOrderDetail = [manager loadAmenitiesOrderDetailsByItemId:orderDetail AndOrderId:amenitiesOrder.amenId];
        
        sumAmenitiesCartQty += amenitiesOrderDetail.amdQuantity;


        AmenitiesItemModelV2 *model = [[AmenitiesItemModelV2 alloc] init];
        model.amenitiesCategoryId = amenitiesItemsModel.itemCategoryId;
        model.amenitiesItemId = amenitiesItemsModel.itemId;
        model.amenitiesItemImage = UIImagePNGRepresentation([UIImage imageWithData:amenitiesItemsModel.itemImage]);
        model.amenitiesItemTitle = amenitiesItemsModel.itemName;
        model.amenitiesItemQuantity = 0;
        model.amenitiesItemLang = amenitiesItemsModel.itemLang;
        [datas addObject:model];
        [sectionAmenitiesCartInfo insertObjectToNextIndex:model];
        
    }
    
    [datasAmenities addObject:sectionAmenitiesCartInfo];
    
    statusAddToCart = YES;
    [tbvAmenitiesItem reloadData];
    
    
    //end loading data
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
    
}

#pragma mark - UITableView Delegate methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return  datas.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 70;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifyCell = @"IdentifyCell";
    AmenitiesItemCellV2 *cell = nil;
    cell = (AmenitiesItemCellV2 *)[tableView dequeueReusableCellWithIdentifier:identifyCell];
    if(cell == nil)
    {
        NSArray *array = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([AmenitiesItemCellV2 class]) owner:self options:nil];
        cell = [array objectAtIndex:0];
    }
    AmenitiesItemModelV2 *model = [datas objectAtIndex:indexPath.row];
    [cell.imgAmenitiesItemCell setImage:[UIImage imageWithData:model.amenitiesItemImage]];
    //set language name amenities item
    if ([[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString: (NSString *)ENGLISH_LANGUAGE] == NO) {
        [cell.lblAmenitiesItemCell setText:model.amenitiesItemLang];
    }
    else{
        [cell.lblAmenitiesItemCell setText:model.amenitiesItemTitle];
    }
    [cell.lblQuantityAmenitiesItemCell setText:[[LanguageManagerV2 sharedLanguageManager] getQuantity]];
    
    if (statusAddToCart == YES) {
        [cell.btnQuantityAmenitiesItemCell setTitle:[NSString stringWithFormat:@"%d",0] forState:UIControlStateNormal];
    }
    else
    {
        [cell.btnQuantityAmenitiesItemCell setTitle:[NSString stringWithFormat:@"%d",model.amenitiesItemQuantity] forState:UIControlStateNormal];
    }
    [cell setDelegate:self];
    [cell setIndexpath:indexPath];
    
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

-(void)btnAmenitiesItemCellV2PressedWithIndexpath:(NSIndexPath *)indexpath sender:(UIButton *)button{
    statusAddToCart = NO;
    //statusChooseQty = YES;
    NSMutableArray *array = [[NSMutableArray alloc] init];
    for (NSInteger index = 0; index <= 10; index++) {
        [array addObject:[NSString stringWithFormat:@"%d", index]];
    }
    
    PickerViewV2 *picker = [[PickerViewV2 alloc] initPickerViewV2WithDatas:array AndSelectedData:button.titleLabel.text AndIndex:indexpath];
    [picker showPickerViewV2];
    [picker setDelegate:self];
}

-(void)didChoosePickerViewV2WithData:(NSString *)data AndIndex:(NSIndexPath *)index{
    AmenitiesItemModelV2 *model = [datas objectAtIndex:index.row];
    if ([data integerValue] > 0) {
        statusChooseQty = YES;
    }
    model.amenitiesItemQuantity = [data integerValue];
    [tbvAmenitiesItem reloadRowsAtIndexPaths:[NSArray arrayWithObject:index] withRowAnimation:UITableViewRowAnimationFade];
}

- (void)dealloc {
    
}

#pragma mark - Add to Cart Press
-(void)handleBtnAddToCartPressed
{
    AmenitiesManagerV2 *manager = [[AmenitiesManagerV2 alloc] init];
    
    for (NSInteger indexSection=0; indexSection < datasAmenities.count; indexSection ++) {
        SectionAmenitiesCartInfoV2 *sectionInfo = [datasAmenities objectAtIndex:indexSection];
        for (NSInteger indexRow=0; indexRow < sectionInfo.rowHeights.count; indexRow++) {
            AmenitiesItemModelV2 *model = [sectionInfo.rowHeights objectAtIndex:indexRow];
            if (model.amenitiesItemQuantity > 0) {
                
                //load amenities cart temp from database
                AmenitiesItemsOrderTempModel *orderTemp = [[AmenitiesItemsOrderTempModel alloc] init];
                orderTemp.itemId = model.amenitiesItemId;
                orderTemp.itemCategoryId = model.amenitiesCategoryId;
                orderTemp.itemRoomtypeId = amenitiesServiceId;
                [manager loadAmenitiesItemsOrderTempModelByPrimaryKey:orderTemp];
                
                if (orderTemp.itemName == nil) {
                    
                    //insert to amenities cart temp
                    AmenitiesItemsOrderTempModel *orderTempDB = [[AmenitiesItemsOrderTempModel alloc] init];
                    orderTempDB.itemId = model.amenitiesItemId;
                    orderTempDB.itemCategoryId = model.amenitiesCategoryId;
                    orderTempDB.itemImage = model.amenitiesItemImage;
                    orderTempDB.itemLang = model.amenitiesItemLang;
                    orderTempDB.itemName = model.amenitiesItemTitle;
                    orderTempDB.itemQuantity = model.amenitiesItemQuantity;
                    orderTempDB.itemRoomtypeId = amenitiesServiceId;
                    
                    [manager insertAmenitiesItemsOrderTempModel:orderTempDB];
                }
                else
                {
                    //update to amenities cart temp
                    orderTemp.itemQuantity = model.amenitiesItemQuantity + orderTemp.itemQuantity;
                    
                    [manager updateAmenitiesItemsOrderTempModel:orderTemp];

                }
               
                model.amenitiesItemQuantity = 0;
            }
        }
    }

    
    NSInteger count = 0;
    NSMutableArray *arrayOrderTemp = [manager loadAllAmenitiesItemOrderTempModel];
    
        
    NSMutableArray *arrayRoomType = [manager loadAllAmenitiesService];
    for (AmenitiesServiceModelV2 *roomType in arrayRoomType) {
        
        AmenitiesOrdersModelV2 *amenitiesOrder = [[AmenitiesOrdersModelV2 alloc] init];
        amenitiesOrder.amenRoomId = [TasksManagerV2 getCurrentRoomAssignment];
        amenitiesOrder.amenUserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
        amenitiesOrder.amenService = roomType.amsId;
        amenitiesOrder.amenStatus = tStatusChecked;
        amenitiesOrder.amenCollectDate = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd"];
        
        
        BOOL isCheckAmenitiesOrder = [manager isCheckAmenitiesOrder:amenitiesOrder];
        
        if (isCheckAmenitiesOrder == TRUE) {
            
            [manager loadAmenitiesOrdersModelByRoomIdUserIdAndServiceId:amenitiesOrder];
            NSMutableArray *arrayOrderDetail = [manager loadAllAmenitiesOrderDetailsByOrderId:amenitiesOrder.amenId];
            
            for (AmenitiesOrderDetailsModelV2 *orderDetail in arrayOrderDetail) {
                count += orderDetail.amdQuantity;
            }

        }

    }

    for (AmenitiesItemsOrderTempModel *temp in arrayOrderTemp) {
        count += temp.itemQuantity;
    }

    
    [btnCart setTitle:[NSString stringWithFormat:@"%d",count] forState:UIControlStateNormal];

}

- (IBAction)btnAddtoCartAmenitiesItem:(id)sender {
    statusSave = YES;
    statusChooseQty = NO;
    statusAddToCart = YES;
    [self handleBtnAddToCartPressed];
    [tbvAmenitiesItem reloadData];
}

#pragma mark - Cart Press
-(void)btnCartPressed{
    AmenitiesCartViewV2 *amenitiesCart = [[AmenitiesCartViewV2 alloc] initWithNibName:NSStringFromClass([AmenitiesCartViewV2 class]) bundle:nil];
    
    amenitiesCart.delegate = self;
        
    amenitiesCart.dataAmenitiesCart = cartdatas;
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK] style:UIBarButtonItemStyleBordered target:nil action:nil];
    self.navigationItem.backBarButtonItem = backButton;
    
    [amenitiesCart setTitleSectionAmenitiesLocation:self.title];
    
    amenitiesCart.amenitiesCartServiceId = amenitiesServiceId;
    
    amenitiesCart.amenitiesCategory = amenitiesCategoryWithItem;
    
    if (statusSave == NO && statusChooseQty == NO) {
        amenitiesCart.statusChooseQty = NO;
    }
    else
    {
        amenitiesCart.statusChooseQty = YES;
    }

    
    [self.navigationController pushViewController:amenitiesCart animated:YES];
}

#pragma mark - Save Amenities Cart
-(void) saveData{
    
    if (statusSave == YES) {
        [self saveAmenitiesCart];
        [self btnAddtoCartAmenitiesItem:btnCart];
        
    }
    statusChooseQty = NO;
    statusSave = NO;
}

-(void)saveAmenitiesCart
{
    // @ show loading data .
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getRaSavingData]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES]; 
    
    ///Save amenities cart
    AmenitiesManagerV2 *manager = [[AmenitiesManagerV2 alloc] init];
    SyncManagerV2 *synManager = [[SyncManagerV2 alloc] init];
    NSInteger userId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    NSInteger roomAssignId = [TasksManagerV2 getCurrentRoomAssignment];
    
    NSMutableArray *arrayRoomType = [manager loadAllAmenitiesService];
    
    for (AmenitiesServiceModelV2 *roomType in arrayRoomType) {
        
        NSMutableArray *arrayAmenitiesOrderTemp = [manager loadAllAmenitiesItemOrderTempByRoomType:roomType.amsId];
        
        if (arrayAmenitiesOrderTemp.count > 0) {
            for (AmenitiesItemsOrderTempModel *orderTemp in arrayAmenitiesOrderTemp) {
                
                //Update Order in amenities
                AmenitiesOrdersModelV2 *amenitiesOrder = [[AmenitiesOrdersModelV2 alloc] init];
                amenitiesOrder.amenRoomId = [TasksManagerV2 getCurrentRoomAssignment];
                amenitiesOrder.amenUserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
                amenitiesOrder.amenService = roomType.amsId;
                amenitiesOrder.amenStatus = tStatusChecked;
                amenitiesOrder.amenCollectDate = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd"];
                
                
                BOOL isCheckAmenitiesOrder = [manager isCheckAmenitiesOrder:amenitiesOrder];
                
                if (isCheckAmenitiesOrder == FALSE) {
                    [manager insertAmenitiesOrdersModel:amenitiesOrder];
                }
                
                //load amenities order
                [manager loadAmenitiesOrdersModelByRoomIdUserIdAndServiceId:amenitiesOrder];
                
                //load amenities order detail
                AmenitiesOrderDetailsModelV2 *orderDetail = [[AmenitiesOrderDetailsModelV2 alloc] init];
                orderDetail.amdItemId = orderTemp.itemId;
                orderDetail.amdRoomTypeId = orderTemp.itemRoomtypeId;
                orderDetail.amdCategoryId = orderTemp.itemCategoryId;
                AmenitiesOrderDetailsModelV2 *amenitiesOrderDetailModel = [manager loadAmenitiesOrderDetailsByItemId:orderDetail AndOrderId:amenitiesOrder.amenId];
                
                if (amenitiesOrderDetailModel.amdId != 0) {
                    
                    //update Order detail in amenities
                    amenitiesOrderDetailModel.amdQuantity += orderTemp.itemQuantity;
                    
                    [manager updateAmenitiesOrderDetailsModel:amenitiesOrderDetailModel];
                }
                else
                {
                    //insert Order detail in amenities
                    AmenitiesOrderDetailsModelV2 *amenitiesOrderDetailModel = [[AmenitiesOrderDetailsModelV2 alloc] init];
                    amenitiesOrderDetailModel.amdAmenitiesOrderId = amenitiesOrder.amenId;
                    amenitiesOrderDetailModel.amdItemId = orderTemp.itemId;
                    amenitiesOrderDetailModel.amdQuantity = orderTemp.itemQuantity;
                    amenitiesOrderDetailModel.amdPostStatus = POST_STATUS_SAVED_UNPOSTED;
                    amenitiesOrderDetailModel.amdCategoryId = orderTemp.itemCategoryId;
                    amenitiesOrderDetailModel.amdRoomTypeId = orderTemp.itemRoomtypeId;
                    
                    [manager insertAmenitiesOrderDetailsModel:amenitiesOrderDetailModel];
                    
                }
                
                //--------------------Post WS----------------
                AmenitiesOrderDetailsModelV2 *orderDetailPost = [[AmenitiesOrderDetailsModelV2 alloc] init];
                orderDetailPost.amdItemId = orderTemp.itemId;
                orderDetailPost.amdRoomTypeId = orderTemp.itemRoomtypeId;
                orderDetailPost.amdCategoryId = orderTemp.itemCategoryId;
                
                AmenitiesOrderDetailsModelV2 *amenitiesOrderDetailPost = [manager loadAmenitiesOrderDetailsByItemId:orderDetailPost AndOrderId:amenitiesOrder.amenId];
                
                BOOL statusPost = [synManager postAmenitiesItemWithUserId:userId RoomAssignId:roomAssignId AndAmenitiesItem:amenitiesOrderDetailPost WithDate:amenitiesOrder.amenCollectDate];
                
                if (statusPost == YES) {
                    //post success, clear database
                    [manager deleteAmenitiesOrderDetailsModel:amenitiesOrderDetailPost];
                }
                
                [manager deleteAmenitiesItemsOrderTempModel:orderTemp];
                //--------------------End-------------------
            }
            
        }
        
    }
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getMsgSaveMessage]];
    //hide saving data.
    [self performSelector:@selector(hiddenHUDAfterSaved:) withObject:HUD afterDelay:0.5];

}


#pragma didden after save.
-(void) hiddenHUDAfterSaved:(MBProgressHUD *)HUD{
    [HUD hide:YES];
    [HUD removeFromSuperview];
//    [HUD release];
}

#pragma mark -  Amenities Cart View delegate method
-(void)saveCartItem{
    statusChooseQty = NO;
}

#pragma mark - set caption view
-(void)setCaptionView
{
    [btnAddtoCart setTitle:[[LanguageManagerV2 sharedLanguageManager] getAddToCart] forState:UIControlStateNormal];
    [self setTitle:[[LanguageManagerV2 sharedLanguageManager] getAmenities]];
    
    msgbtnNo = [[LanguageManagerV2 sharedLanguageManager] getNo];
    msgbtnYes = [[LanguageManagerV2 sharedLanguageManager] getYes];
    msgSaveCount = [[LanguageManagerV2 sharedLanguageManager] getMsgAlertSaveCounts];
    msgDiscardCount = [[LanguageManagerV2 sharedLanguageManager] getMsgAlertDiscardCounts];
    
    //refresh topview
    TopbarViewV2 *topview = [self getTopBarView];
    [topview refresh:[[SuperRoomModelV2 alloc] init]];
}

#pragma mark - Handle Topbar Methods
-(void)loadTopbarView {
    TopbarViewV2 *topbar = [[TopbarViewV2 alloc] initViewWithSuperModel:[[CommonVariable sharedCommonVariable] superRoomModel]];
    [topbar setTag:tagTopbarView];
    [self.view addSubview:topbar];
    [topbar setHidden:YES];
    
    [topbar setAlpha:0.0];
    [self addButtonHandleShowHideTopbar];
}

-(void)showTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationWillStartSelector:@selector(setHiddenTopbarView)];
    
    f = topbar.frame;
    f.origin.y = 0;
    [topbar setFrame:f];
    [topbar setAlpha:1.0];
    
    [self adjustShowForViews];
    
    [UIView commitAnimations];
}

-(void)hideTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDidStopSelector:@selector(setHiddenTopbarView)];
    [UIView setAnimationDelegate:self];
    
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    [topbar setAlpha:0.0];
    
    [self adjustRemoveForViews];
    
    [UIView commitAnimations];
}

-(void)setHiddenTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    [topbar setHidden:!topbar.hidden];
}

-(TopbarViewV2 *)getTopBarView {
    return (TopbarViewV2 *)[self.view viewWithTag:tagTopbarView];
}

-(void)addButtonHandleShowHideTopbar {
    UIButton *titleBarButton = [[UIButton alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
    
    titleBarButton.titleLabel.backgroundColor = [UIColor clearColor];
    titleBarButton.titleLabel.font = FONT_BUTTON_TOPBAR;
    titleBarButton.titleLabel.textAlignment = UITextAlignmentCenter;
    titleBarButton.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
    [titleBarButton.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
    [titleBarButton.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
    [titleBarButton addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    [titleBarButton setTitle:[self title] forState:UIControlStateNormal];
    self.navigationItem.titleView = titleBarButton;
}

-(void)topbarTapped:(UIButton *)sender {
    TopbarViewV2 *topbar = [self getTopBarView];
    [sender setEnabled:NO];
    if (topbar.isHidden) {
        [self showTopbarView];
    } else {
        [self hideTopbarView];
    }
    [self performSelector:@selector(topbarButtonEnable:) withObject:sender afterDelay:0.5];
}

-(void) topbarButtonEnable:(UIButton *) topbarBtn {
    [topbarBtn setEnabled:YES];
}

-(void)adjustShowForViews {
    //adjust all views when show 
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    CGRect fheader = vHeaderAmenities.frame;
    [vHeaderAmenities setFrame:CGRectMake(0, f.size.height, 320, fheader.size.height)];
    
    CGRect ftbv = tbvAmenitiesItem.frame;
    [tbvAmenitiesItem setFrame:CGRectMake(0, f.size.height + fheader.size.height, 320, ftbv.size.height - f.size.height)];
}

-(void)adjustRemoveForViews { 
    //adjust all views when hide
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    CGRect fheader = vHeaderAmenities.frame;
    [vHeaderAmenities setFrame:CGRectMake(0, 0, 320, fheader.size.height)];
    
    CGRect ftbv = tbvAmenitiesItem.frame;
    [tbvAmenitiesItem setFrame:CGRectMake(0, fheader.size.height, 320, ftbv.size.height + f.size.height)];
}

@end
