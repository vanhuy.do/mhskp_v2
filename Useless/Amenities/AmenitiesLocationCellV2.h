//
//  AmenitiesLocationCellV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 1/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AmenitiesLocationCellV2 : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imgAmenitiesLocationCell;
@property (strong, nonatomic) IBOutlet UILabel *lblAmenitiesLocationCell;
@property (strong, nonatomic) IBOutlet UIImageView *imgRowAmenitiesLocationCell;

@end
