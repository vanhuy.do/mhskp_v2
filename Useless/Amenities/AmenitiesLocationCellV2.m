//
//  AmenitiesLocationCellV2.m
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 1/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AmenitiesLocationCellV2.h"

@implementation AmenitiesLocationCellV2
@synthesize imgAmenitiesLocationCell;
@synthesize lblAmenitiesLocationCell;
@synthesize imgRowAmenitiesLocationCell;


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc {
//    [imgAmenitiesLocationCell release];
//    [lblAmenitiesLocationCell release];
//    [imgRowAmenitiesLocationCell release];
//    [super dealloc];
}
@end
