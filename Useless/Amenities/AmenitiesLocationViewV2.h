//
//  AmenitiesLocationViewV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 1/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AmenitiesLocationCellV2.h"
#import "AmenitiesLocationModelV2.h"
#import "AmenitiesCategoryViewV2.h"
#import "CustomAlertViewV2.h"


@interface AmenitiesLocationViewV2 : UIViewController<UITableViewDataSource,UITableViewDelegate>{
    NSMutableArray *datas;
}
@property (strong, nonatomic) IBOutlet UITableView *tbvAmenitiesLocation;
@property (strong, nonatomic) IBOutlet AmenitiesLocationCellV2 *amenitiesLocationCell;
@property (strong, nonatomic) IBOutlet NSMutableArray *datas;
@property (strong, nonatomic) NSString *msgbtnYes;
@property (strong, nonatomic) NSString *msgbtnNo;
@property (strong, nonatomic) NSString *msgSaveCount;
@property (strong, nonatomic) NSString *msgDiscardCount;

- (void) loadDataAmenitiesLocation;
- (void) setCaptionView;
@end
