//
//  AmenitiesLocationViewV2.m
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 1/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AmenitiesLocationViewV2.h"
#import "AmenitiesManagerV2.h"
#import "MBProgressHUD.h"
#import "TasksManagerV2.h"
#import "MyNavigationBarV2.h"
#import "ehkDefines.h"

#define imgExcutive @"Executive_Floor.png"
#define imgRegular @"Regular_Floor.png"
#define tagAlertBack 10
#define tagAlertNo 11

@interface AmenitiesLocationViewV2 (PrivateMethods)

-(void) saveAmenitiesCart;
-(BOOL) checkDataAmenitiesOrderCartTemp;
-(void) backBarPressed;

#define tagTopbarView 1234

-(void) loadTopbarView;
-(void) showTopbarView;
-(void) hideTopbarView;
-(void) setHiddenTopbarView;
-(void) adjustShowForViews;
-(void) adjustRemoveForViews;
-(void) addButtonHandleShowHideTopbar;
-(void) topbarTapped:(UIButton *) sender;
-(TopbarViewV2 *) getTopBarView;

@end

@implementation AmenitiesLocationViewV2
@synthesize tbvAmenitiesLocation;
@synthesize amenitiesLocationCell;
@synthesize datas;
@synthesize msgbtnNo, msgbtnYes, msgSaveCount, msgDiscardCount;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - Load data amenities room type from service
-(void) loadDataAmenitiesFormService:(MBProgressHUD *) HUD {
    //enable flag to announce, is loading data
    [[HomeViewV2 shareHomeView] waitingLoadingData];
    
    
    SyncManagerV2 *synManager = [[SyncManagerV2 alloc] init];
    
    NSInteger hotelId = [[[UserManagerV2 sharedUserManager] currentUser] userHotelsId];
    
    //-----------WS------------
    [synManager getRoomTypeListWithHotelId:hotelId];
    //-----------end-----------
    
    [self loadDataAmenitiesLocation];
    
    
    //hide saving data.
    [self performSelector:@selector(hiddenHUDAfterSaved:) withObject:HUD afterDelay:0.5];
    
    //end loading data
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
}


#pragma mark - View lifecycle
-(void)viewWillAppear:(BOOL)animated {
    
    [self setCaptionView];
    
    //Move to center vertical
    CGRect titleViewBounds = self.navigationController.navigationBar.topItem.titleView.bounds;
    self.navigationController.navigationBar.topItem.titleView.bounds = CGRectMake(titleViewBounds.origin.x, Y_TOPBAR_TITLE, titleViewBounds.size.width, titleViewBounds.size.height);
    
    //show wifi view
    [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationShowWifiView] object:nil];
    [super viewWillAppear:animated];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //[self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:imgBackground]]];
    
    MBProgressHUD	*HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getRaLoadingData]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES];

    //hide saving data.
    [self performSelector:@selector(loadDataAmenitiesFormService:) withObject:HUD afterDelay:0.1];

    [self setTitle:[[LanguageManagerV2 sharedLanguageManager] getAmenities]];

    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *img = [UIImage imageNamed:@"btn_back.png"];
    
    [button setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK] forState:UIControlStateNormal];
    [button setBackgroundImage:img forState:UIControlStateNormal];
    [button addTarget:self action:@selector(backBarPressed) forControlEvents:UIControlEventTouchUpInside];
    [button setFrame:CGRectMake(0, 0,WIDTH_BACK_BUTTON , HEIGHT_BACK_BUTTON)];
    [button.titleLabel setFont:[UIFont boldSystemFontOfSize:FONT_BACK_BUTTON]];
    [button setContentEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
    UIView *backButtonView = [[UIView alloc] initWithFrame:button.frame];
    backButtonView.bounds = CGRectMake(backButtonView.bounds.origin.x, Y_BACK_BUTTON, backButtonView.bounds.size.width, backButtonView.bounds.size.height);
    [backButtonView addSubview:button];
    
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButtonView];
    self.navigationItem.leftBarButtonItem = barButtonItem;
    
    [self performSelector:@selector(loadTopbarView)];
}

#pragma mark - Click back
-(BOOL) checkDataAmenitiesOrderCartTemp
{
    BOOL isCheckData = NO;
    AmenitiesManagerV2 *manager = [[AmenitiesManagerV2 alloc] init];
    NSMutableArray *array = [manager loadAllAmenitiesItemOrderTempModel];
    if (array.count > 0) {
        isCheckData = YES;
    }
    else
    {
        isCheckData = NO;
    }
    return isCheckData;
}

-(void)backBarPressed {
    //clear previous tag of event
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:TAG_OF_EVENT];
    
    BOOL isCheckData = [self checkDataAmenitiesOrderCartTemp];
    if(isCheckData == NO)
    {
        [self.navigationController popViewControllerAnimated:YES];        
    }
    else
    {
        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:@"Alert!" message:msgSaveCount delegate:self cancelButtonTitle:msgbtnYes otherButtonTitles:msgbtnNo, nil]; 
            alert.delegate = self;
            alert.tag = tagAlertBack;
            [alert show];             
    }
}

#pragma mark - === Handle Check Save Data ===
#pragma mark
-(BOOL) checkSaveDataWithTagEvent:(NSInteger) tagOfEvent {
    
    BOOL isCheckData = [self checkDataAmenitiesOrderCartTemp];
    if (tagOfEvent == tagOfMessageButton) {
        if (isCheckData == NO) {
            return NO;
        }
        else
        {
            //Show alert please save before leaving
            UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:[[LanguageManagerV2 sharedLanguageManager] getMsgAlertSaveCounts] delegate:self cancelButtonTitle:msgbtnYes otherButtonTitles:msgbtnNo, nil]; 
            alert.delegate = self;
            alert.tag = tagAlertBack;
            [alert show];
            
            return YES;

        }
    }
    else
    {
        //Show alert complete room
        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_complete_room_to_leave] delegate:self cancelButtonTitle:msgbtnYes otherButtonTitles: nil]; 
        alert.delegate = self;
        [alert show];
        
        return YES;

    }
    
}

#pragma mark - UIAlertView Delegate Methods
-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    switch (alertView.tag) {
        case tagAlertBack:
        {
            switch (buttonIndex)
            {
                case 0:
                {
                    //YES: save
                    [self saveAmenitiesCart];
                    
                    //if tag is message
                    if ([[NSUserDefaults standardUserDefaults] integerForKey:TAG_OF_EVENT] == tagOfMessageButton) {
                        [[HomeViewV2 shareHomeView] handleTabbarEventWithTagOfButton:[[NSUserDefaults standardUserDefaults] integerForKey:TAG_OF_EVENT]];
                    }
                    else{
                        //back to count menu
                        [self.navigationController popViewControllerAnimated:YES];
                    }
                }
                    break;
                    
                case 1:
                {
                    //NO: show popup discard
                    UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:msgDiscardCount delegate:self cancelButtonTitle:msgbtnYes otherButtonTitles:msgbtnNo, nil]; 
                    alert.delegate = self;
                    alert.tag = tagAlertNo;
                    [alert show];  
                }
                    break;
                    
                default:
                    break;
            }
        }
            break;
            
        case tagAlertNo:
        {
            switch (buttonIndex)
            {
                case 0:
                {
                    //YES: back to count menu, no save, delete all order in cart temp
                    //clear cart temp
                    AmenitiesManagerV2 *manager = [[AmenitiesManagerV2 alloc] init];
                    [manager deleteAllAmenitiesItemsOrderTemp];
                    
                    //if tag is message
                    if ([[NSUserDefaults standardUserDefaults] integerForKey:TAG_OF_EVENT] == tagOfMessageButton) {
                        [[HomeViewV2 shareHomeView] handleTabbarEventWithTagOfButton:[[NSUserDefaults standardUserDefaults] integerForKey:TAG_OF_EVENT]];
                    }
                    else{
                        [self.navigationController popViewControllerAnimated:YES];  
                    }
                    
                }
                    break;
                    
                case 1:
                {
                    //NO:go to cart screen
                    //AmenitiesCartViewV2 *amenitiesCart = [[AmenitiesCartViewV2 alloc] initWithNibName:NSStringFromClass([AmenitiesCartViewV2 class]) bundle:nil];
                    //[self.navigationController pushViewController:amenitiesCart animated:YES];

                }
                    break;
                    
                default:
                    break;
            }
            
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - save amenities cart
-(void) saveData{
    BOOL isCheckData = [self checkDataAmenitiesOrderCartTemp];
    if (isCheckData == YES) {
        [self saveAmenitiesCart];
    }
}
-(void) saveAmenitiesCart
{
    // @ show loading data .
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getRaSavingData]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES]; 
    
    ///Save amenities cart
    AmenitiesManagerV2 *manager = [[AmenitiesManagerV2 alloc] init];
    SyncManagerV2 *synManager = [[SyncManagerV2 alloc] init];
    NSInteger userId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    NSInteger roomAssignId = [TasksManagerV2 getCurrentRoomAssignment];
    
    NSMutableArray *arrayRoomType = [manager loadAllAmenitiesService];
   
    for (AmenitiesServiceModelV2 *roomType in arrayRoomType) {
    
         NSMutableArray *arrayAmenitiesOrderTemp = [manager loadAllAmenitiesItemOrderTempByRoomType:roomType.amsId];
        
        if (arrayAmenitiesOrderTemp.count > 0) {
            for (AmenitiesItemsOrderTempModel *orderTemp in arrayAmenitiesOrderTemp) {
                
                //Update Order in amenities
                AmenitiesOrdersModelV2 *amenitiesOrder = [[AmenitiesOrdersModelV2 alloc] init];
                amenitiesOrder.amenRoomId = [TasksManagerV2 getCurrentRoomAssignment];
                amenitiesOrder.amenUserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
                amenitiesOrder.amenService = roomType.amsId;
                amenitiesOrder.amenStatus = tStatusChecked;
                amenitiesOrder.amenCollectDate = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd"];
                
                
                BOOL isCheckAmenitiesOrder = [manager isCheckAmenitiesOrder:amenitiesOrder];
                
                if (isCheckAmenitiesOrder == FALSE) {
                    [manager insertAmenitiesOrdersModel:amenitiesOrder];
                }
                
                //load amenities order
                [manager loadAmenitiesOrdersModelByRoomIdUserIdAndServiceId:amenitiesOrder];
                
                //load amenities order detail
                AmenitiesOrderDetailsModelV2 *orderDetail = [[AmenitiesOrderDetailsModelV2 alloc] init];
                orderDetail.amdItemId = orderTemp.itemId;
                orderDetail.amdRoomTypeId = orderTemp.itemRoomtypeId;
                orderDetail.amdCategoryId = orderTemp.itemCategoryId;
                AmenitiesOrderDetailsModelV2 *amenitiesOrderDetailModel = [manager loadAmenitiesOrderDetailsByItemId:orderDetail AndOrderId:amenitiesOrder.amenId];
                
                if (amenitiesOrderDetailModel.amdId != 0) {
                    
                    //update Order detail in amenities
                    amenitiesOrderDetailModel.amdQuantity = orderTemp.itemQuantity;
                    
                    [manager updateAmenitiesOrderDetailsModel:amenitiesOrderDetailModel];
                }
                else
                {
                    //insert Order detail in amenities
                    AmenitiesOrderDetailsModelV2 *amenitiesOrderDetailModel = [[AmenitiesOrderDetailsModelV2 alloc] init];
                    amenitiesOrderDetailModel.amdAmenitiesOrderId = amenitiesOrder.amenId;
                    amenitiesOrderDetailModel.amdItemId = orderTemp.itemId;
                    amenitiesOrderDetailModel.amdQuantity = orderTemp.itemQuantity;
                    amenitiesOrderDetailModel.amdPostStatus = POST_STATUS_SAVED_UNPOSTED;
                    amenitiesOrderDetailModel.amdCategoryId = orderTemp.itemCategoryId;
                    amenitiesOrderDetailModel.amdRoomTypeId = orderTemp.itemRoomtypeId;
                    
                    [manager insertAmenitiesOrderDetailsModel:amenitiesOrderDetailModel];
                    
                }
                
                //-----------------------Post WS--------------------
                AmenitiesOrderDetailsModelV2 *orderDetailPost = [[AmenitiesOrderDetailsModelV2 alloc] init];
                orderDetailPost.amdItemId = orderTemp.itemId;
                orderDetailPost.amdRoomTypeId = orderTemp.itemRoomtypeId;
                orderDetailPost.amdCategoryId = orderTemp.itemCategoryId;
                AmenitiesOrderDetailsModelV2 *amenitiesOrderDetailPost = [manager loadAmenitiesOrderDetailsByItemId:orderDetailPost AndOrderId:amenitiesOrder.amenId];
                
                ///-------------------Post WSLog-------
                NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
                NSString *messageValue = [NSString stringWithFormat:@"Amenities Location Post WSLog:Time:%@, UserId:%d,RoomAssignId:%d, AmenitiesItemId:%d", time, userId, roomAssignId, orderDetailPost.amdItemId];
                int switchStatus = [[NSUserDefaults standardUserDefaults] integerForKey:ENABLE_LOGWS];
                if (switchStatus == 1) {
                    [synManager postWSLogAmenities:userId Message:messageValue MessageType:1];
                }
               
                ///-------------------End Post WSLog-------
                
                BOOL statusPost = [synManager postAmenitiesItemWithUserId:userId RoomAssignId:roomAssignId AndAmenitiesItem:amenitiesOrderDetailPost WithDate:amenitiesOrder.amenCollectDate];
                
                if (statusPost == YES) {
                    //post success, clear database
                    [manager deleteAmenitiesOrderDetailsModel:amenitiesOrderDetailPost];
                    
                }
                [manager deleteAmenitiesItemsOrderTempModel:orderTemp];
                //-----------------------End-------------------
                
            }

        }
                            
    }
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getMsgSaveMessage]];   
    //hide saving data.
    [self performSelector:@selector(hiddenHUDAfterSaved:) withObject:HUD afterDelay:0.5];
}

- (void)viewDidUnload
{
//    [self setTbvAmenitiesLocation:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Load Amenities Location
-(void)loadDataAmenitiesLocation{
    
    AmenitiesManagerV2 *manager = [[AmenitiesManagerV2 alloc] init];
    
    
    datas = [[NSMutableArray alloc] init];
    NSMutableArray *array = [manager loadAllAmenitiesService];
    
    for (AmenitiesServiceModelV2 *amenitiesServiceModel in array) {
        
        AmenitiesLocationModelV2 *model = [[AmenitiesLocationModelV2 alloc] init];
        model.amenitiesLocationId = amenitiesServiceModel.amsId;
        model.amenitiesLocationImage = UIImagePNGRepresentation([UIImage imageWithData:amenitiesServiceModel.amsImage]);
        //set language name amenities service
        if ([[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString: (NSString *)ENGLISH_LANGUAGE] == NO) {
            model.amenitiesLocationTitle = amenitiesServiceModel.amsNameLang;
        }
        else{
            model.amenitiesLocationTitle = amenitiesServiceModel.amsName;
        }
        [datas addObject:model];
    }
    
    [tbvAmenitiesLocation reloadData];
}

#pragma mark - UITableView Delegate methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return  1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return  datas.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifyCell = @"IdentifyCell";
    AmenitiesLocationCellV2 *cell = nil;
    cell = [tableView dequeueReusableCellWithIdentifier:identifyCell];
    if(cell == nil)
    {
        NSArray *array = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([AmenitiesLocationCellV2 class]) owner:self options:nil];
        cell = [array objectAtIndex:0];
    }
    AmenitiesLocationModelV2 *model = [datas objectAtIndex:indexPath.row];
    [cell.imgAmenitiesLocationCell setImage:[UIImage imageWithData:model.amenitiesLocationImage]];
    [cell.lblAmenitiesLocationCell setText:model.amenitiesLocationTitle];
    [cell.imgRowAmenitiesLocationCell setImage:[UIImage imageNamed:imgRowGray]];
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
    return  cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    AmenitiesCategoryViewV2 *amenitiesCategory = [[AmenitiesCategoryViewV2 alloc] initWithNibName:NSStringFromClass([AmenitiesCategoryViewV2 class]) bundle:nil ];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK] style:UIBarButtonItemStyleBordered target:nil action:nil];
    self.navigationItem.backBarButtonItem = backButton;
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [cell setSelected:NO animated:YES];
    
    AmenitiesLocationModelV2 *amenitiesLocation = [datas objectAtIndex:indexPath.row];
    
    [amenitiesCategory setTitle:amenitiesLocation.amenitiesLocationTitle];
    
    //set location id for category view
    amenitiesCategory.serviceId = amenitiesLocation.amenitiesLocationId;
    
    [self.navigationController pushViewController:amenitiesCategory animated:YES];
    
}
#pragma didden after save.
-(void) hiddenHUDAfterSaved:(MBProgressHUD *)HUD{
    [HUD hide:YES];
    [HUD removeFromSuperview];
//    [HUD release];
}
- (void)dealloc {
//    [tbvAmenitiesLocation release];
//    [super dealloc];
}

#pragma mark - set caption view
-(void)setCaptionView
{

    [self setTitle:[[LanguageManagerV2 sharedLanguageManager] getAmenities]];
    
    msgbtnNo = [[LanguageManagerV2 sharedLanguageManager] getNo];
    msgbtnYes = [[LanguageManagerV2 sharedLanguageManager] getYes];
    msgSaveCount = [[LanguageManagerV2 sharedLanguageManager] getMsgAlertSaveCounts];
    msgDiscardCount = [[LanguageManagerV2 sharedLanguageManager] getMsgAlertDiscardCounts];
    
    //refresh topview
    TopbarViewV2 *topview = [self getTopBarView];
    [topview refresh:[[SuperRoomModelV2 alloc] init]];
}

#pragma mark - Handle Topbar Methods
-(void)loadTopbarView {
    TopbarViewV2 *topbar = [[TopbarViewV2 alloc] initViewWithSuperModel:[[CommonVariable sharedCommonVariable] superRoomModel]];
    [topbar setTag:tagTopbarView];
    [self.view addSubview:topbar];
    [topbar setHidden:YES];
    
    [topbar setAlpha:0.0];
    [self addButtonHandleShowHideTopbar];
}

-(void)showTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationWillStartSelector:@selector(setHiddenTopbarView)];
    
    f = topbar.frame;
    f.origin.y = 0;
    [topbar setFrame:f];
    [topbar setAlpha:1.0];
    
    [self adjustShowForViews];
    
    [UIView commitAnimations];
}

-(void)hideTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDidStopSelector:@selector(setHiddenTopbarView)];
    [UIView setAnimationDelegate:self];
    
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    [topbar setAlpha:0.0];
    
    [self adjustRemoveForViews];
    
    [UIView commitAnimations];
}

-(void)setHiddenTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    [topbar setHidden:!topbar.hidden];
}

-(TopbarViewV2 *)getTopBarView {
    return (TopbarViewV2 *)[self.view viewWithTag:tagTopbarView];
}

-(void)addButtonHandleShowHideTopbar {
    UIButton *titleBarButton = [[UIButton alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
    
    titleBarButton.titleLabel.backgroundColor = [UIColor clearColor];
    titleBarButton.titleLabel.font = FONT_BUTTON_TOPBAR;
    titleBarButton.titleLabel.textAlignment = UITextAlignmentCenter;
    titleBarButton.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
    [titleBarButton.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
    [titleBarButton.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
    [titleBarButton addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    [titleBarButton setTitle:[self title] forState:UIControlStateNormal];
    self.navigationItem.titleView = titleBarButton;
}

-(void)topbarTapped:(UIButton *)sender {
    TopbarViewV2 *topbar = [self getTopBarView];
    [sender setEnabled:NO];
    if (topbar.isHidden) {
        [self showTopbarView];
    } else {
        [self hideTopbarView];
    }
    [self performSelector:@selector(topbarButtonEnable:) withObject:sender afterDelay:0.5];
}

-(void) topbarButtonEnable:(UIButton *) topbarBtn {
    [topbarBtn setEnabled:YES];
}

-(void)adjustShowForViews {
    //adjust all views when show 
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    CGRect ftbv = tbvAmenitiesLocation.frame;
    [tbvAmenitiesLocation setFrame:CGRectMake(0, f.size.height, 320, ftbv.size.height - f.size.height)];
}

-(void)adjustRemoveForViews { 
    //adjust all views when hide
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    CGRect ftbv = tbvAmenitiesLocation.frame;
    [tbvAmenitiesLocation setFrame:CGRectMake(0, 0, 320, ftbv.size.height + f.size.height)];
}


@end
