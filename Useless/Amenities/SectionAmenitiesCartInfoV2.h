//
//  SectionAmenitiesCartInfoV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AmenitiesCategoryModelV2.h"
#import "AmenitiesServiceModelV2.h"

@class SectionAmenitiesCartViewV2;

@interface SectionAmenitiesCartInfoV2 : NSObject{
    
}
@property (nonatomic, strong) AmenitiesCategoryModelV2 *amenitiesCategory;
@property (nonatomic, strong) AmenitiesServiceModelV2 *amenitiesService;
@property (assign) BOOL open;
@property (strong) SectionAmenitiesCartViewV2* headerView;

@property (nonatomic,strong,readonly) NSMutableArray *rowHeights;

- (NSUInteger)countOfRowHeights;
- (id)objectInRowHeightsAtIndex:(NSUInteger)idx;
- (void)insertObject:(id)anObject inRowHeightsAtIndex:(NSUInteger)idx;
- (void)removeObjectFromRowHeightsAtIndex:(NSUInteger)idx;
- (void)replaceObjectInRowHeightsAtIndex:(NSUInteger)idx withObject:(id)anObject;
- (void)getRowHeights:(id __unsafe_unretained [])buffer range:(NSRange)inRange;
- (void)insertRowHeights:(NSArray *)rowHeightArray atIndexes:(NSIndexSet *)indexes;
- (void)removeRowHeightsAtIndexes:(NSIndexSet *)indexes;
- (void)replaceRowHeightsAtIndexes:(NSIndexSet *)indexes withRowHeights:(NSArray *)rowHeightArray;
-(void) insertObjectToNextIndex:(id)anObject;

@end
