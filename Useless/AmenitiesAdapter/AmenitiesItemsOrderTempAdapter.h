//
//  AmenitiesItemsOrderTempAdapter.h
//
//  Created by Thuong Nguyen.
//  Copyright 2012 __TMS__. All rights reserved.
//

#import "DatabaseAdapter.h"
#import "AmenitiesItemsOrderTempModel.h"
@interface AmenitiesItemsOrderTempAdapter : DatabaseAdapter

-(BOOL) insertAmenitiesItemsOrderTempModel:(AmenitiesItemsOrderTempModel *)model;

-(AmenitiesItemsOrderTempModel *) loadAmenitiesItemsOrderTempModelByPrimaryKey:(AmenitiesItemsOrderTempModel *) model;

-(BOOL) updateAmenitiesItemsOrderTempModel:(AmenitiesItemsOrderTempModel *) model;

-(BOOL) deleteAmenitiesItemsOrderTempModel:(AmenitiesItemsOrderTempModel *) model;

-(NSMutableArray *) loadAllAmenitiesItemOrderTempModel;

-(NSMutableArray *) loadAmenitiesItemOrderTempSameCategoryId:(NSInteger) categoryId AndRoomTypeId:(NSInteger) roomTypeId;

-(BOOL) deleteAllAmenitiesItemsOrderTemp;

-(NSMutableArray *) loadAllAmenitiesItemOrderTempByRoomType:(NSInteger) roomType;

@end