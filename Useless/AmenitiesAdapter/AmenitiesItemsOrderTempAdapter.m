//
//  AmenitiesItemsOrderTempAdapter.h
//
//  Created by Thuong Nguyen.
//  Copyright 2012 __TMS__. All rights reserved.
//

#import "AmenitiesItemsOrderTempAdapter.h"
#import "DbDefinesV2.h"
#import "ehkDefines.h"

@implementation AmenitiesItemsOrderTempAdapter

- (id)init
{
	self = [super init];
	if (self) {
		// Initialization code here.
	}
	return self;

}

-(BOOL)insertAmenitiesItemsOrderTempModel:(AmenitiesItemsOrderTempModel *)model {
	NSString *sqlString = [NSString stringWithFormat:@"INSERT INTO %@ ( %@, %@, %@, %@, %@, %@, %@, %@) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?)" ,AMENITIES_ITEMS_ORDER_TEMP ,item_idtemp, item_nametemp, item_langtemp, item_category_idtemp, item_imagetemp, item_roomtype_idtemp, item_last_modifiedtemp, item_quantitytemp ];

	const char *sql = [sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
	if(sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {
		sqlite3_bind_int(sqlStament, 1, model.itemId);
		sqlite3_bind_text(sqlStament, 2, [model.itemName UTF8String], -1, SQLITE_TRANSIENT);
		sqlite3_bind_text(sqlStament, 3, [model.itemLang UTF8String], -1, SQLITE_TRANSIENT);
		sqlite3_bind_int(sqlStament, 4, model.itemCategoryId);
		sqlite3_bind_blob(sqlStament, 5, [model.itemImage bytes], [model.itemImage length], NULL);
		sqlite3_bind_int(sqlStament, 6, model.itemRoomtypeId);
		sqlite3_bind_text(sqlStament, 7, [model.itemLastModified UTF8String], -1, SQLITE_TRANSIENT);
		sqlite3_bind_int(sqlStament, 8, model.itemQuantity);
	}
    
	NSInteger status = sqlite3_step(sqlStament);
	if (status == SQLITE_DONE) {
		sqlite3_last_insert_rowid(database);
		sqlite3_reset(sqlStament);
		return YES;
	}
    else{
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return NO;
    }
	return  NO;
}

-(BOOL)updateAmenitiesItemsOrderTempModel:(AmenitiesItemsOrderTempModel *)model {
	NSString *sqlString = [NSString stringWithFormat:@"UPDATE %@ SET %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ? WHERE  %@ = ? AND %@ = ? AND %@ = ?", AMENITIES_ITEMS_ORDER_TEMP, item_nametemp, item_langtemp, item_imagetemp, item_last_modifiedtemp, item_quantitytemp, item_idtemp, item_category_idtemp, item_roomtype_idtemp];
	const char *sql = [sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
	if(sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {
		sqlite3_bind_text(sqlStament, 1, [model.itemName UTF8String], -1, SQLITE_TRANSIENT);
		sqlite3_bind_text(sqlStament, 2, [model.itemLang UTF8String], -1, SQLITE_TRANSIENT);
		sqlite3_bind_blob(sqlStament, 3, [model.itemImage bytes], [model.itemImage length], NULL);
		sqlite3_bind_text(sqlStament, 4, [model.itemLastModified UTF8String], -1, SQLITE_TRANSIENT);
		sqlite3_bind_int(sqlStament, 5, model.itemQuantity);
		sqlite3_bind_int(sqlStament, 6, model.itemId);
		sqlite3_bind_int(sqlStament, 7, model.itemCategoryId);
		sqlite3_bind_int(sqlStament, 8, model.itemRoomtypeId);
	}
    
	NSInteger status = sqlite3_step(sqlStament);
	if (status == SQLITE_DONE) {
		return YES;
	}
    else{
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return NO;
    }
	return  NO;
}

-(BOOL)deleteAmenitiesItemsOrderTempModel:(AmenitiesItemsOrderTempModel *)model {
	NSString *sqlString = [NSString stringWithFormat:@"DELETE FROM %@ WHERE %@ = ? AND %@ = ? AND %@ = ?", AMENITIES_ITEMS_ORDER_TEMP, item_idtemp, item_category_idtemp, item_roomtype_idtemp];
	const char *sql = [sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
	if(sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {
		sqlite3_bind_int(sqlStament, 1, model.itemId);
		sqlite3_bind_int(sqlStament, 2, model.itemCategoryId);
		sqlite3_bind_int(sqlStament, 3, model.itemRoomtypeId);
	}
    
	NSInteger status = sqlite3_step(sqlStament);
	if (status == SQLITE_DONE) {
		return YES;
	}
    else{
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return  NO;
    }
	return  NO;
}

-(AmenitiesItemsOrderTempModel *)loadAmenitiesItemsOrderTempModelByPrimaryKey:(AmenitiesItemsOrderTempModel *)model {
	NSString *sqlString = [NSString stringWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@, %@ FROM %@ WHERE  %@ = ? AND %@ = ? AND %@ = ?" , item_idtemp, item_nametemp, item_langtemp, item_category_idtemp, item_imagetemp, item_roomtype_idtemp, item_last_modifiedtemp, item_quantitytemp, AMENITIES_ITEMS_ORDER_TEMP, item_idtemp, item_category_idtemp, item_roomtype_idtemp];
	const char *sql = [sqlString UTF8String];
	
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSInteger status;
    status = sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL);
	if(status == SQLITE_OK) {
		sqlite3_bind_int(sqlStament, 1, model.itemId);
		sqlite3_bind_int(sqlStament, 2, model.itemCategoryId);
		sqlite3_bind_int(sqlStament, 3, model.itemRoomtypeId);
        
        NSInteger status = sqlite3_step(sqlStament);
		while (status == SQLITE_ROW) {
			model.itemId = sqlite3_column_int(sqlStament, 0);
			if (sqlite3_column_text(sqlStament, 1)) {
				model.itemName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
			}
			if (sqlite3_column_text(sqlStament, 2)) {
				model.itemLang = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
			}
			model.itemCategoryId = sqlite3_column_int(sqlStament, 3);
			if (sqlite3_column_bytes(sqlStament, 4) > 0) {
				model.itemImage = [NSData dataWithBytes:sqlite3_column_blob(sqlStament, 4) length:sqlite3_column_bytes(sqlStament, 4)];
			}			
            model.itemRoomtypeId = sqlite3_column_int(sqlStament, 5);
			if (sqlite3_column_text(sqlStament, 6)) {
				model.itemLastModified = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 6)];
			}
			model.itemQuantity = sqlite3_column_int(sqlStament, 7);
			return model;
		}
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
	}
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
	return model;
}

-(NSMutableArray *)loadAllAmenitiesItemOrderTempModel
{
    NSMutableArray *array = [NSMutableArray array];
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@, %@ FROM %@",item_idtemp, item_nametemp, item_langtemp, item_category_idtemp, item_imagetemp, item_roomtype_idtemp, item_last_modifiedtemp, item_quantitytemp, AMENITIES_ITEMS_ORDER_TEMP];
    const char *sql =[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        
        status = sqlite3_step(sqlStament);
        while(status == SQLITE_ROW)
        {
            AmenitiesItemsOrderTempModel *model = [[AmenitiesItemsOrderTempModel alloc] init];
            
            model.itemId = sqlite3_column_int(sqlStament, 0);
            if (sqlite3_column_text(self.sqlStament, 1)) {
                model.itemName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(self.sqlStament, 1)];
            }
            if (sqlite3_column_text(self.sqlStament, 2)) {
                model.itemLang = [NSString stringWithUTF8String:(char *)sqlite3_column_text(self.sqlStament, 2)];
            }
            model.itemCategoryId = sqlite3_column_int(sqlStament, 3);
            
            if (sqlite3_column_bytes(sqlStament, 4) > 0) {
				model.itemImage = [NSData dataWithBytes:sqlite3_column_blob(sqlStament, 4) length:sqlite3_column_bytes(sqlStament, 4)];
			}	
            
            model.itemRoomtypeId = sqlite3_column_int(sqlStament, 5);
			if (sqlite3_column_text(sqlStament, 6)) {
				model.itemLastModified = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 6)];
			}
			model.itemQuantity = sqlite3_column_int(sqlStament, 7);
            
            [array addObject:model];
            
            status = sqlite3_step(sqlStament);
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                [array removeAllObjects];
                goto RE_ACCESS_DB;
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    return array;
}

-(NSMutableArray *)loadAmenitiesItemOrderTempSameCategoryId:(NSInteger)categoryId AndRoomTypeId:(NSInteger)roomTypeId
{
    NSMutableArray *array = [NSMutableArray array];
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ? AND %@ = ?",item_idtemp, item_nametemp, item_langtemp, item_category_idtemp, item_imagetemp, item_roomtype_idtemp, item_last_modifiedtemp, item_quantitytemp, AMENITIES_ITEMS_ORDER_TEMP, item_category_idtemp, item_roomtype_idtemp];
    const char *sql =[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        
        sqlite3_bind_int(sqlStament, 1, categoryId);
        sqlite3_bind_int(sqlStament, 2, roomTypeId);
        
        NSInteger status = sqlite3_step(sqlStament);
        while(status  == SQLITE_ROW)
        {
            AmenitiesItemsOrderTempModel *model = [[AmenitiesItemsOrderTempModel alloc] init];
            
            model.itemId = sqlite3_column_int(sqlStament, 0);
            if (sqlite3_column_text(self.sqlStament, 1)) {
                model.itemName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(self.sqlStament, 1)];
            }
            if (sqlite3_column_text(self.sqlStament, 2)) {
                model.itemLang = [NSString stringWithUTF8String:(char *)sqlite3_column_text(self.sqlStament, 2)];
            }
            model.itemCategoryId = sqlite3_column_int(sqlStament, 3);
            
            if (sqlite3_column_bytes(sqlStament, 4) > 0) {
				model.itemImage = [NSData dataWithBytes:sqlite3_column_blob(sqlStament, 4) length:sqlite3_column_bytes(sqlStament, 4)];
			}	
            
            model.itemRoomtypeId = sqlite3_column_int(sqlStament, 5);
			if (sqlite3_column_text(sqlStament, 6)) {
				model.itemLastModified = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 6)];
			}
			model.itemQuantity = sqlite3_column_int(sqlStament, 7);
            
            [array addObject:model];
            
            status = sqlite3_step(sqlStament);
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                [array removeAllObjects];
                goto RE_ACCESS_DB;
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    return array;

}

-(BOOL)deleteAllAmenitiesItemsOrderTemp
{
    NSString *sqlString = [NSString stringWithFormat:@"DELETE FROM %@", AMENITIES_ITEMS_ORDER_TEMP];
	const char *sql = [sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
	if(sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {

	}
    
	NSInteger status = sqlite3_step(sqlStament);
	if (status == SQLITE_DONE) {
		return YES;
	}
    else{
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }
	return  NO;
}

-(NSMutableArray *)loadAllAmenitiesItemOrderTempByRoomType:(NSInteger)roomType
{
    NSMutableArray *array = [NSMutableArray array];
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ?",item_idtemp, item_nametemp, item_langtemp, item_category_idtemp, item_imagetemp, item_roomtype_idtemp, item_last_modifiedtemp, item_quantitytemp, AMENITIES_ITEMS_ORDER_TEMP, item_roomtype_idtemp];
    const char *sql =[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {
        
        sqlite3_bind_int(sqlStament, 1, roomType);
        
        status = sqlite3_step(sqlStament);
        while(status == SQLITE_ROW)
        {
            AmenitiesItemsOrderTempModel *model = [[AmenitiesItemsOrderTempModel alloc] init];
            
            model.itemId = sqlite3_column_int(sqlStament, 0);
            if (sqlite3_column_text(self.sqlStament, 1)) {
                model.itemName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(self.sqlStament, 1)];
            }
            if (sqlite3_column_text(self.sqlStament, 2)) {
                model.itemLang = [NSString stringWithUTF8String:(char *)sqlite3_column_text(self.sqlStament, 2)];
            }
            model.itemCategoryId = sqlite3_column_int(sqlStament, 3);
            
            if (sqlite3_column_bytes(sqlStament, 4) > 0) {
				model.itemImage = [NSData dataWithBytes:sqlite3_column_blob(sqlStament, 4) length:sqlite3_column_bytes(sqlStament, 4)];
			}	
            
            model.itemRoomtypeId = sqlite3_column_int(sqlStament, 5);
			if (sqlite3_column_text(sqlStament, 6)) {
				model.itemLastModified = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 6)];
			}
			model.itemQuantity = sqlite3_column_int(sqlStament, 7);
            
            [array addObject:model];
            
            status = sqlite3_step(sqlStament);
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                [array removeAllObjects];
                goto RE_ACCESS_DB;
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    return array;

}

@end