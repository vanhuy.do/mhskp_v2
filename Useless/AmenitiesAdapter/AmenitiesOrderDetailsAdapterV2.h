//
//  AmenitiesOrderDetailsAdapterV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 3/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "DatabaseAdapterV2.h"
#import "AmenitiesOrderDetailsModelV2.h"

@interface AmenitiesOrderDetailsAdapterV2 : DatabaseAdapterV2

-(BOOL) insertAmenitiesOrderDetailsModel:(AmenitiesOrderDetailsModelV2 *)model;

-(AmenitiesOrderDetailsModelV2 *) loadAmenitiesOrderDetailsModelByPrimaryKey:(AmenitiesOrderDetailsModelV2 *) model;

-(BOOL) updateAmenitiesOrderDetailsModel:(AmenitiesOrderDetailsModelV2 *) model;

-(BOOL) deleteAmenitiesOrderDetailsModel:(AmenitiesOrderDetailsModelV2 *) model;

-(NSMutableArray *) loadAllAmenitiesOrderDetailByOrderId:(NSInteger) amenitiesOrderId;

-(AmenitiesOrderDetailsModelV2 *) loadAmenitiesOrderDetailsByItemId:(NSInteger) amenitiesItemId;

-(AmenitiesOrderDetailsModelV2 *) loadAmenitiesOrderDetailsByItemId:(AmenitiesOrderDetailsModelV2 *) amenitiesItemId AndOrderId: (NSInteger) orderId;

@end