//
//  AmenitiesOrderDetailsAdapterV2.m
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 3/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AmenitiesOrderDetailsAdapterV2.h"
#import "DbDefinesV2.h"
#import "ehkDefines.h"

@implementation AmenitiesOrderDetailsAdapterV2

- (id)init
{
	self = [super init];
	if (self) {
		// Initialization code here.
	}
	return self;
    
}

-(BOOL)insertAmenitiesOrderDetailsModel:(AmenitiesOrderDetailsModelV2 *)model {
	NSString *sqlString = [NSString stringWithFormat:@"INSERT INTO %@ (%@, %@, %@, %@, %@, %@) VALUES (?, ?, ?, ?, ?, ?)" , AMENITIES_ORDER_DETAILS, amd_amenities_order_id, amd_item_id, amd_quantity, amd_post_status , amd_roomtype_id, amd_category_id];
    
	const char *sql = [sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
	if(sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {

		sqlite3_bind_int(sqlStament, 1, model.amdAmenitiesOrderId);
		sqlite3_bind_int(sqlStament, 2, model.amdItemId);
		sqlite3_bind_int(sqlStament, 3, model.amdNewQuantity);
        sqlite3_bind_int(sqlStament, 4, model.amdUsedQuantity);
		sqlite3_bind_int(sqlStament, 5, model.amdPostStatus);
        sqlite3_bind_int(sqlStament, 6, model.amdPostStatus);
		sqlite3_bind_int(sqlStament, 7, model.amdCategoryId);
	}
    
	NSInteger status = sqlite3_step(sqlStament);
	if (status == SQLITE_DONE) {
		sqlite3_last_insert_rowid(database);
		sqlite3_reset(sqlStament);
		return YES;
	}
    else{
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return NO;
    }
	return  NO;
}

-(AmenitiesOrderDetailsModelV2 *)loadAmenitiesOrderDetailsModelByPrimaryKey:(AmenitiesOrderDetailsModelV2 *)model {
	NSString *sqlString = [NSString stringWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@ FROM %@ WHERE  %@ = ? AND %@ = ? AND %@ = ?" , amd_id, amd_amenities_order_id, amd_item_id, amd_quantity, amd_post_status, amd_roomtype_id, amd_category_id, AMENITIES_ORDER_DETAILS, amd_item_id, amd_roomtype_id, amd_category_id];
	const char *sql = [sqlString UTF8String];
	
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSInteger status;
    status = sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL);
	if(status == SQLITE_OK) {
		sqlite3_bind_int(sqlStament, 1, model.amdItemId);
        sqlite3_bind_int(sqlStament, 2, model.amdRoomTypeId);
        sqlite3_bind_int(sqlStament, 3, model.amdCategoryId);
        
        status = sqlite3_step(sqlStament);
		while (status == SQLITE_ROW) {
			model.amdId = sqlite3_column_int(sqlStament, 0);
			model.amdAmenitiesOrderId = sqlite3_column_int(sqlStament, 1);
			model.amdItemId = sqlite3_column_int(sqlStament, 2);
			model.amdNewQuantity = sqlite3_column_int(sqlStament, 3);
            model.amdUsedQuantity = sqlite3_column_int(sqlStament, 4);
			model.amdPostStatus = sqlite3_column_int(sqlStament, 5);
            model.amdRoomTypeId = sqlite3_column_int(sqlStament, 6);
            model.amdCategoryId = sqlite3_column_int(sqlStament, 7);
			return model;
		}
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
	}
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
	return model;
}

-(BOOL)updateAmenitiesOrderDetailsModel:(AmenitiesOrderDetailsModelV2 *)model {
	NSString *sqlString = [NSString stringWithFormat:@"UPDATE %@ SET %@ = ?, %@ = ?, %@ = ?, %@ = ? WHERE  %@ = ? AND %@ = ? AND %@ = ?", AMENITIES_ORDER_DETAILS, amd_amenities_order_id, amd_item_id, amd_quantity, amd_post_status, amd_item_id, amd_roomtype_id, amd_category_id];
	const char *sql = [sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
	if(sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {
		sqlite3_bind_int(sqlStament, 1, model.amdAmenitiesOrderId);
		sqlite3_bind_int(sqlStament, 2, model.amdItemId);
		sqlite3_bind_int(sqlStament, 3, model.amdNewQuantity);
        sqlite3_bind_int(sqlStament, 4, model.amdUsedQuantity);
		sqlite3_bind_int(sqlStament, 5, model.amdPostStatus);
		sqlite3_bind_int(sqlStament, 6, model.amdItemId);
        sqlite3_bind_int(sqlStament, 7, model.amdRoomTypeId);
        sqlite3_bind_int(sqlStament, 8, model.amdCategoryId);
	}
    
	NSInteger status = sqlite3_step(sqlStament);
	if (status == SQLITE_DONE) {
		return YES;
	}
    else{
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return NO;
    }
	return  NO;
}

-(BOOL)deleteAmenitiesOrderDetailsModel:(AmenitiesOrderDetailsModelV2 *)model {
	NSString *sqlString = [NSString stringWithFormat:@"DELETE FROM %@ WHERE %@ = ? AND %@ = ? AND %@ = ?", AMENITIES_ORDER_DETAILS, amd_item_id, amd_roomtype_id, amd_category_id];
	const char *sql = [sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
	if(sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {
		sqlite3_bind_int(sqlStament, 1, model.amdItemId);
        sqlite3_bind_int(sqlStament, 2, model.amdRoomTypeId);
        sqlite3_bind_int(sqlStament, 3, model.amdCategoryId);
	}
    
	NSInteger status = sqlite3_step(sqlStament);
	if (status == SQLITE_DONE) {
		return YES;
	}
    else{
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return NO;
    }
	return  NO;
}

-(NSMutableArray *) loadAllAmenitiesOrderDetailByOrderId:(NSInteger) amenitiesOrderId {
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@ ,%@, %@, %@, %@, %@ FROM %@ WHERE %@ = ?", 
                           amd_id,
                           amd_amenities_order_id,
                           amd_item_id,
                           amd_quantity,
                           amd_post_status,
                           amd_roomtype_id,
                           amd_category_id,
                           AMENITIES_ORDER_DETAILS,
                           amd_amenities_order_id
                           ];
    
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
        
    NSMutableArray *array = [NSMutableArray array];
    
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        
        sqlite3_bind_int (self.sqlStament, 1, amenitiesOrderId);
        
        status = sqlite3_step(sqlStament);
        while(status == SQLITE_ROW) 
        {
            AmenitiesOrderDetailsModelV2 *model = [[AmenitiesOrderDetailsModelV2 alloc] init];
            model.amdId = sqlite3_column_int(sqlStament, 0);
            model.amdAmenitiesOrderId = sqlite3_column_int(sqlStament, 1);
            model.amdItemId = sqlite3_column_int(sqlStament, 2);
            model.amdNewQuantity = sqlite3_column_int(sqlStament, 3);
            model.amdUsedQuantity = sqlite3_column_int(sqlStament, 4);
            model.amdPostStatus = sqlite3_column_int(sqlStament, 5);
            model.amdRoomTypeId = sqlite3_column_int(sqlStament, 6);
            model.amdCategoryId = sqlite3_column_int(sqlStament, 7);
            
            [array addObject:model];
            
            status = sqlite3_step(sqlStament);
        }
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                [array removeAllObjects];
                goto RE_ACCESS_DB;
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    return array;
}

-(AmenitiesOrderDetailsModelV2 *) loadAmenitiesOrderDetailsByItemId:(NSInteger) amenitiesItemId;
{
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@ ,%@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ?", 
                           amd_id,
                           amd_amenities_order_id,
                           amd_item_id,
                           amd_quantity,
                           amd_post_status,
                           amd_roomtype_id,
                           amd_category_id,
                           AMENITIES_ORDER_DETAILS,
                           amd_item_id
                           ];
    
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    AmenitiesOrderDetailsModelV2 *model = [[AmenitiesOrderDetailsModelV2 alloc] init];
    
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        
        sqlite3_bind_int (self.sqlStament, 1, amenitiesItemId);
        
        status = sqlite3_step(sqlStament);
        while(status == SQLITE_ROW) 
        {
            model.amdId = sqlite3_column_int(sqlStament, 0);
            model.amdAmenitiesOrderId = sqlite3_column_int(sqlStament, 1);
            model.amdItemId = sqlite3_column_int(sqlStament, 2);
            model.amdNewQuantity = sqlite3_column_int(sqlStament, 3);
            model.amdUsedQuantity = sqlite3_column_int(sqlStament, 4);
            model.amdPostStatus = sqlite3_column_int(sqlStament, 5);
            model.amdRoomTypeId = sqlite3_column_int(sqlStament, 6);
            model.amdCategoryId = sqlite3_column_int(sqlStament, 7);
            
            return model;
        }
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    return model;
    
}

-(AmenitiesOrderDetailsModelV2 *) loadAmenitiesOrderDetailsByItemId:(AmenitiesOrderDetailsModelV2 *) amenitiesItemId AndOrderId: (NSInteger) orderId
{
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@ ,%@, %@, %@, %@, %@  FROM %@ WHERE %@ = ? AND %@ = ? AND %@ = ? AND %@ = ?", 
                           amd_id,
                           amd_amenities_order_id,
                           amd_item_id,
                           amd_quantity,
                           amd_post_status,
                           amd_roomtype_id,
                           amd_category_id,
                           AMENITIES_ORDER_DETAILS,
                           amd_item_id,
                           amd_roomtype_id,
                           amd_category_id,
                           amd_amenities_order_id
                           ];
    
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        
        sqlite3_bind_int (self.sqlStament, 1, amenitiesItemId.amdItemId);
        sqlite3_bind_int (self.sqlStament, 2, amenitiesItemId.amdRoomTypeId);
        sqlite3_bind_int (self.sqlStament, 3, amenitiesItemId.amdCategoryId);
        sqlite3_bind_int (self.sqlStament, 4, orderId);
        
        status = sqlite3_step(sqlStament);
        while(status == SQLITE_ROW) 
        {
            amenitiesItemId.amdId = sqlite3_column_int(sqlStament, 0);
            amenitiesItemId.amdAmenitiesOrderId = sqlite3_column_int(sqlStament, 1);
            amenitiesItemId.amdItemId = sqlite3_column_int(sqlStament, 2);
            amenitiesItemId.amdNewQuantity = sqlite3_column_int(sqlStament, 3);
            amenitiesItemId.amdUsedQuantity = sqlite3_column_int(sqlStament, 4);
            amenitiesItemId.amdPostStatus = sqlite3_column_int(sqlStament, 5);
            amenitiesItemId.amdRoomTypeId = sqlite3_column_int(sqlStament, 6);
            amenitiesItemId.amdCategoryId = sqlite3_column_int(sqlStament, 7);
            
            return amenitiesItemId;
        }
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    return amenitiesItemId;
}



@end
