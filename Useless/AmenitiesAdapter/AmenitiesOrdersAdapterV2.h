//
//  AmenitiesOrdersAdapterV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 3/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "DatabaseAdapterV2.h"
#import "AmenitiesOrdersModelV2.h"

@interface AmenitiesOrdersAdapterV2 : DatabaseAdapterV2

-(BOOL) insertAmenitiesOrdersModel:(AmenitiesOrdersModelV2 *)model;

-(AmenitiesOrdersModelV2 *) loadAmenitiesOrdersModelByPrimaryKey:(AmenitiesOrdersModelV2 *) model;

-(BOOL) updateAmenitiesOrdersModel:(AmenitiesOrdersModelV2 *) model;

-(BOOL) deleteAmenitiesOrdersModel:(AmenitiesOrdersModelV2 *) model;

-(AmenitiesOrdersModelV2 *) loadAmenitiesOrdersModelByRoomIdUserIdAndServiceId:(AmenitiesOrdersModelV2 *) model;

-(NSMutableArray *) loadAllAmenitiesOrdersModelByRoomId:(NSInteger) roomId UserId:(NSInteger) userId;

-(NSMutableArray *) loadAllAmenitiesOrdersModelByUserId:(NSInteger) userId;

@end