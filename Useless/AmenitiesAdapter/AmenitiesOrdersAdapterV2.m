//
//  AmenitiesOrdersAdapterV2.m
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 3/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AmenitiesOrdersAdapterV2.h"
#import "DbDefinesV2.h"
#import "ehkConvert.h"
#import "ehkDefines.h"

@implementation AmenitiesOrdersAdapterV2

- (id)init
{
	self = [super init];
	if (self) {
		// Initialization code here.
	}
	return self;
    
}

-(BOOL)insertAmenitiesOrdersModel:(AmenitiesOrdersModelV2 *)model {
	NSString *sqlString = [NSString stringWithFormat:@"INSERT INTO %@ ( %@, %@, %@, %@, %@) VALUES (?, ?, ?, ?, ?)", AMENITIES_ORDERS, amen_room_id, amen_user_id, amen_status, amen_collect_date,amen_service ];
    
	const char *sql = [sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
	if(sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {
    
		sqlite3_bind_int(sqlStament, 1, model.amenRoomId);
		sqlite3_bind_int(sqlStament, 2, model.amenUserId);
		sqlite3_bind_int(sqlStament, 3, model.amenStatus);
		sqlite3_bind_text(sqlStament, 4, [model.amenCollectDate UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_int(sqlStament, 5, model.amenService);

	}
    
	NSInteger status = sqlite3_step(sqlStament);
	if (status == SQLITE_DONE) {
		sqlite3_last_insert_rowid(database);
		sqlite3_reset(sqlStament);
		return YES;
	}
    else{
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return NO;
    }
	return  NO;
}

-(AmenitiesOrdersModelV2 *)loadAmenitiesOrdersModelByPrimaryKey:(AmenitiesOrdersModelV2 *)model {
	NSString *sqlString = [NSString stringWithFormat:@"SELECT %@, %@, %@, %@, %@, %@ FROM %@ WHERE  %@ = ?" , amen_id, amen_room_id, amen_user_id, amen_status, amen_collect_date,amen_service, AMENITIES_ORDERS, amen_id];
	const char *sql = [sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
	
    NSInteger status;
    status = sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL);
	if(status == SQLITE_OK) {
		sqlite3_bind_int(sqlStament, 1, model.amenId);
        
        status = sqlite3_step(sqlStament);
		while (status == SQLITE_ROW) {
			model.amenId = sqlite3_column_int(sqlStament, 0);
			model.amenRoomId = sqlite3_column_int(sqlStament, 1);
			model.amenUserId = sqlite3_column_int(sqlStament, 2);
			model.amenStatus = sqlite3_column_int(sqlStament, 3);
			if (sqlite3_column_text(sqlStament, 4)) {
				model.amenCollectDate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
			}
            model.amenService = sqlite3_column_int(sqlStament, 5);
			return model;
		}
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
	}
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
	return model;
}

-(BOOL)updateAmenitiesOrdersModel:(AmenitiesOrdersModelV2 *)model {
	NSString *sqlString = [NSString stringWithFormat:@"UPDATE %@ SET %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ? WHERE  %@ = ?", AMENITIES_ORDERS, amen_room_id, amen_user_id, amen_status, amen_collect_date,amen_service, amen_id];
	const char *sql = [sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
	if(sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {
		sqlite3_bind_int(sqlStament, 1, model.amenRoomId);
		sqlite3_bind_int(sqlStament, 2, model.amenUserId);
		sqlite3_bind_int(sqlStament, 3, model.amenStatus);
		sqlite3_bind_text(sqlStament, 4, [model.amenCollectDate UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_int(sqlStament, 5, model.amenService);
		sqlite3_bind_int(sqlStament, 6, model.amenId);
        
	}
    
	NSInteger status = sqlite3_step(sqlStament);
	if (status == SQLITE_DONE) {
		return YES;
	}
    else{
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return  NO;
    }
	return  NO;
}

-(BOOL)deleteAmenitiesOrdersModel:(AmenitiesOrdersModelV2 *)model {
	NSString *sqlString = [NSString stringWithFormat:@"DELETE FROM %@ WHERE %@ = ?", AMENITIES_ORDERS, amen_id];
	const char *sql = [sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
        
	if(sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {
		sqlite3_bind_int(sqlStament, 1, model.amenId);
	}
    
	NSInteger status = sqlite3_step(sqlStament);
	if (status == SQLITE_DONE) {
		return YES;
	}
    else{
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return  NO;
    }
	return  NO;
}

-(AmenitiesOrdersModelV2 *) loadAmenitiesOrdersModelByRoomIdUserIdAndServiceId:(AmenitiesOrdersModelV2 *) model
{
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@ ,%@, %@, %@ , %@ FROM %@ WHERE %@ = ? AND %@ = ? AND %@ = ? AND %@ = ?", 
                           amen_id,
                           amen_room_id,
                           amen_user_id,
                           amen_status,
                           amen_collect_date,
                           amen_service,
                           AMENITIES_ORDERS,
                           amen_room_id,
                           amen_user_id,
                           amen_collect_date,
                           amen_service
                           ];
    
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        
        sqlite3_bind_int (self.sqlStament, 1, model.amenRoomId);
        sqlite3_bind_int (self.sqlStament, 2, model.amenUserId);
        sqlite3_bind_text(self.sqlStament, 3, [model.amenCollectDate UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_int (self.sqlStament, 4, model.amenService);
        
        status = sqlite3_step(sqlStament);
        while(status == SQLITE_ROW) 
        {
            model.amenId = sqlite3_column_int(sqlStament, 0);
            model.amenRoomId = sqlite3_column_int(sqlStament, 1);
            model.amenUserId = sqlite3_column_int(sqlStament, 2);
            model.amenStatus = sqlite3_column_int(sqlStament, 3);
            if (sqlite3_column_text(sqlStament, 4)) {
                model.amenCollectDate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
            }
            model.amenService = sqlite3_column_int(sqlStament, 5);
            return model;
        }
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    return model;
    
}

-(NSMutableArray *)loadAllAmenitiesOrdersModelByRoomId:(NSInteger)roomId UserId:(NSInteger)userId {
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@ ,%@, %@, %@ , %@ FROM %@ WHERE %@ = ? AND %@ = ? AND %@ = ?", 
                           amen_id,
                           amen_room_id,
                           amen_user_id,
                           amen_status,
                           amen_collect_date,
                           amen_service,
                           AMENITIES_ORDERS,
                           amen_room_id,
                           amen_user_id,
                           amen_collect_date
                           ];
    
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSMutableArray *array = [NSMutableArray array];
    
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    
    if(status == SQLITE_OK) {
        
        sqlite3_bind_int (self.sqlStament, 1, roomId);
        sqlite3_bind_int (self.sqlStament, 2, userId);
        sqlite3_bind_text(self.sqlStament, 3, [[ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd"] UTF8String], -1, SQLITE_TRANSIENT);
        
        NSInteger status = sqlite3_step(sqlStament);
        while(status == SQLITE_ROW) 
        {
            AmenitiesOrdersModelV2 *model = [[AmenitiesOrdersModelV2 alloc] init];
            model.amenId = sqlite3_column_int(sqlStament, 0);
            model.amenRoomId = sqlite3_column_int(sqlStament, 1);
            model.amenUserId = sqlite3_column_int(sqlStament, 2);
            model.amenStatus = sqlite3_column_int(sqlStament, 3);
            if (sqlite3_column_text(sqlStament, 4)) {
                model.amenCollectDate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
            }
            model.amenService = sqlite3_column_int(sqlStament, 5);
            [array addObject:model];
            
            status = sqlite3_step(sqlStament);
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                [array removeAllObjects];
                goto RE_ACCESS_DB;
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    return array;
}

-(NSMutableArray *)loadAllAmenitiesOrdersModelByUserId:(NSInteger)userId{
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@ ,%@, %@, %@, %@ FROM %@ WHERE %@ = ?", 
                           amen_id,
                           amen_room_id,
                           amen_user_id,
                           amen_status,
                           amen_collect_date,
                           amen_service,
                           AMENITIES_ORDERS,
                           amen_user_id
                           ];
    
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSMutableArray *array = [NSMutableArray array];
    
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        
        sqlite3_bind_int (self.sqlStament, 1, userId);
        
        status = sqlite3_step(sqlStament);
        while(status == SQLITE_ROW) 
        {
            AmenitiesOrdersModelV2 *model = [[AmenitiesOrdersModelV2 alloc] init];
            model.amenId = sqlite3_column_int(sqlStament, 0);
            model.amenRoomId = sqlite3_column_int(sqlStament, 1);
            model.amenUserId = sqlite3_column_int(sqlStament, 2);
            model.amenStatus = sqlite3_column_int(sqlStament, 3);
            if (sqlite3_column_text(sqlStament, 4)) {
                model.amenCollectDate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
            }
            model.amenService = sqlite3_column_int(sqlStament, 5);
            [array addObject:model];
            
            status = sqlite3_step(sqlStament);
        }
        
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                [array removeAllObjects];
                goto RE_ACCESS_DB;
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    return array;

}


@end