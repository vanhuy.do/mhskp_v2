//
//  AmenitiesItemsOrderTempModel.h
//
//  Created by ThuongNM.
//  Copyright 2012 __TMS__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AmenitiesItemsOrderTempModel : NSObject {

}

@property (nonatomic, assign) NSInteger itemId;
@property (nonatomic, strong) NSString *itemName;
@property (nonatomic, strong) NSString *itemLang;
@property (nonatomic, assign) NSInteger itemCategoryId;
@property (nonatomic, strong) NSData *itemImage;
@property (nonatomic, assign) NSInteger itemRoomtypeId;
@property (nonatomic, strong) NSString *itemLastModified;

//@property (nonatomic, assign) NSInteger itemQuantity;
@property (nonatomic, assign) NSInteger itemNewQuantity;
@property (nonatomic, assign) NSInteger itemUsedQuantity;

@end