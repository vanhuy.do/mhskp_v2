//
//  AmenitiesItemsOrderTempModel.m
//
//  Created by ThuongNM.
//  Copyright 2012 TMS. All rights reserved.
//

#import "AmenitiesItemsOrderTempModel.h"

@implementation AmenitiesItemsOrderTempModel

@synthesize itemId;
@synthesize itemName;
@synthesize itemLang;
@synthesize itemCategoryId;
@synthesize itemImage;
@synthesize itemRoomtypeId;
@synthesize itemLastModified;
//@synthesize itemQuantity;
@synthesize itemNewQuantity;
@synthesize itemUsedQuantity;

@end

