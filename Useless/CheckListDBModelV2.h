//
//  CheckListDBModelV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 3/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CheckListDBModelV2 : NSObject{
    NSInteger      chkListId;
    NSInteger      chkListStatus;
    NSInteger chkListUserId;
    NSInteger chkListRoomId;
    NSString *chkInspectDate;
    NSInteger chkPostStatus;
    NSInteger chkCore;
    
}
@property (nonatomic, readwrite)    NSInteger     chkListId;
@property (nonatomic, readwrite)    NSInteger     chkListStatus;
@property (nonatomic, readwrite) NSInteger chkListUserId;
@property (nonatomic, readwrite) NSInteger chkListRoomId;
@property (nonatomic, strong) NSString *chkInspectDate;
@property (nonatomic, readwrite) NSInteger chkPostStatus;
@property (nonatomic, readwrite) NSInteger chkCore;


@end
