//
//  CheckListDetailContentCell.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CheckListDetailContentCellDelegate <NSObject>

@optional
-(void) btnDetailContentRatingPressedWithIndexPath:(NSIndexPath *)indexpath AndSender:(UIButton *) sender;

@end

@interface CheckListDetailContentCell : UITableViewCell{
    UIImageView *imgCheck;
    UILabel *lblDetailContentName;
    UILabel *lblDetailContentPointInpected;
    UIButton *btnDetailContentRating;
    UIButton *btnDetailContentCheck;
    BOOL isChecked;
    NSIndexPath *indexpath;
    id<CheckListDetailContentCellDelegate> delegate;
}

@property (nonatomic, assign) id<CheckListDetailContentCellDelegate> delegate;
@property (nonatomic, strong) NSIndexPath *indexpath;
@property (nonatomic, readwrite) BOOL isChecked;
@property (retain, nonatomic) IBOutlet UIButton *btnDetailContentRating;
@property (retain, nonatomic) IBOutlet UIButton *btnDetailContentCheck;
@property (nonatomic, retain) IBOutlet UIImageView *imgCheck;
@property (nonatomic, retain) IBOutlet UILabel *lblDetailContentName;
@property (nonatomic,retain) IBOutlet UILabel *lblDetailContentPointInpected;
- (IBAction)btnRatingPressed:(id)sender;
- (IBAction)btnCheckPressed:(id)sender;
-(void) setCheckStatus:(BOOL)checkStatus;

@end
