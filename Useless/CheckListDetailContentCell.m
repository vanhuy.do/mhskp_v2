//
//  CheckListDetailContentCell.m
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CheckListDetailContentCell.h"

@implementation CheckListDetailContentCell
@synthesize btnDetailContentRating;
@synthesize btnDetailContentCheck;
@synthesize imgCheck,lblDetailContentName,lblDetailContentPointInpected;
@synthesize isChecked;
@synthesize indexpath;
@synthesize delegate;

//- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
//{
//    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
//    if (self) {
//        // Custom initialization
//    }
//    return self;
//}
//
//- (void)didReceiveMemoryWarning
//{
//    // Releases the view if it doesn't have a superview.
//    [super didReceiveMemoryWarning];
//    
//    // Release any cached data, images, etc that aren't in use.
//}
//
//#pragma mark - View lifecycle
//
//- (void)viewDidLoad
//{
//    [super viewDidLoad];
//    // Do any additional setup after loading the view from its nib.
//}
//
//- (void)viewDidUnload
//{
//    [super viewDidUnload];
//    // Release any retained subviews of the main view.
//    // e.g. self.myOutlet = nil;
//}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(void)dealloc{
    [btnDetailContentRating release];
    [btnDetailContentCheck release];
    [imgCheck release];
    [lblDetailContentName release];
    [lblDetailContentPointInpected release];
}
- (IBAction)btnRatingPressed:(id)sender {
    if ([delegate respondsToSelector:@selector(btnDetailContentRatingPressedWithIndexPath:AndSender:)]) {
        [delegate btnDetailContentRatingPressedWithIndexPath:indexpath AndSender:sender];
    }
}

- (IBAction)btnCheckPressed:(id)sender {
    if(isChecked == NO){
        isChecked = YES;
        [btnDetailContentCheck setImage:[UIImage imageNamed:imgChecked] forState:UIControlStateNormal];    
    }
    else
    {
        isChecked = NO;
        [btnDetailContentCheck setImage:[UIImage imageNamed:imgUnCheck] forState:UIControlStateNormal];
    }
}

-(void)setCheckStatus:(BOOL)checkStatus {
    isChecked = checkStatus;
    if(isChecked == YES){
        [btnDetailContentCheck setImage:[UIImage imageNamed:imgChecked] forState:UIControlStateNormal];    
    }
    else
    {
        [btnDetailContentCheck setImage:[UIImage imageNamed:imgUnCheck] forState:UIControlStateNormal];
    }
}

@end
