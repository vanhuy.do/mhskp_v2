//
//  CheckListDetailForMaidViewV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 5/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CheckListViewV2.h"
#import "CheckListTypeModelV2.h"
#import "CheckListContentTypeModelV2.h"
#import "CheckListDetailContentModelV2.h"
#import "SectionInfoV2.h"
#import "SectionCheckListContentTypeViewV2.h"
#import "CheckListDetailContentCellV2.h"
#import "CheckListItemCoreDBModelV2.h"

@interface CheckListDetailForMaidViewV2 : UIViewController <UITableViewDataSource, UITableViewDelegate, SectionCheckListContentTypeViewV2Delegate, CheckListDetailContentCellV2Delegate, UIAlertViewDelegate>
{
    UILabel *lblPointsTotal;
    UILabel *lblPointsPossible;
    UILabel *lblTotal;
    UILabel *lblReceived;
    UILabel *lblPossible;
    UILabel *lblPointsReceived;
    
    UITableView *tbvchkDetail;
    UIImageView *imgType;
    UILabel *lblContentName;
    UIView *vHeaderChkListDetail;
    
    NSMutableArray *sectionInfoArray;
    CheckListModelV2 *chkList;
    CheckListTypeModelV2 *chkListTypeV2;
    CheckListItemCoreDBModelV2 *chkListItemScoreView;
    NSInteger totalReceived;
    CGFloat totalPoint;
    NSInteger totalPossible;
}
@property (strong, nonatomic) IBOutlet UILabel *lblPointsTotal;
@property (strong, nonatomic) IBOutlet UILabel *lblPointsPossible;
@property (strong, nonatomic) IBOutlet UILabel *lblTotal;
@property (strong, nonatomic) IBOutlet UILabel *lblReceived;
@property (strong, nonatomic) IBOutlet UILabel *lblPossible;
@property (strong, nonatomic) IBOutlet UILabel *lblPointsReceived;

@property (strong, nonatomic) IBOutlet UITableView *tbvchkDetail;
@property (strong, nonatomic) IBOutlet UIImageView *imgType;
@property (strong, nonatomic) IBOutlet UILabel *lblContentName;
@property (strong, nonatomic) IBOutlet UIView *vHeaderChkListDetail;
@property (nonatomic, strong) NSMutableArray *sectionInfoArray;
@property (nonatomic, strong) CheckListModelV2 *chkList;
@property (nonatomic, strong) CheckListTypeModelV2 *chkListTypeV2;
@property (nonatomic, strong) CheckListItemCoreDBModelV2 *chkListItemScoreView;
@property (nonatomic, readwrite) NSInteger totalReceived;
@property (nonatomic, readwrite) CGFloat totalPoint;
@property (nonatomic, assign) NSInteger totalPossible;
@property (nonatomic, strong) NSString *roomNo;

@end
