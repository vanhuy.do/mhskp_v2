//
//  CheckListDetailForMaidViewV2.m
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 5/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CheckListDetailForMaidViewV2.h"
#import "TopbarViewV2.h"
//#import "HomeViewV2.h"
#import "MyNavigationBarV2.h"
#import "DeviceManager.h"
#import "ehkDefinesV2.h"

@interface CheckListDetailForMaidViewV2(PrivateMethods)

-(void) backBarPressed;
-(void)loadSectionDatas;
-(void)calculatorScore;

#define tagTopbarView 1234

-(void) loadTopbarView;
-(void) showTopbarView;
-(void) hideTopbarView;
-(void) setHiddenTopbarView;
-(void) adjustShowForViews;
-(void) adjustRemoveForViews;
-(void) addButtonHandleShowHideTopbar;
-(void) topbarTapped:(UIButton *) sender;
-(TopbarViewV2 *) getTopBarView;

@end

@implementation CheckListDetailForMaidViewV2
@synthesize imgType, tbvchkDetail, lblTotal, lblPossible, lblReceived, lblContentName, lblPointsTotal, lblPointsPossible, lblPointsReceived, vHeaderChkListDetail, chkList, chkListTypeV2, chkListItemScoreView, totalPoint, totalReceived, sectionInfoArray, totalPossible;
@synthesize roomNo;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

//load data for checklist detail
-(void) loadCheckListItemScoreForMaid:(MBProgressHUD *) HUD {
    //enable flag to announce, is loading data
    [[HomeViewV2 shareHomeView] waitingLoadingData];

//Hao Tran remove for new ws checklist
//    //get data checklist category, checklist item
//    SyncManagerV2 *synManager = [[SyncManagerV2 alloc] init];
//    CheckListManagerV2 *manager = [[CheckListManagerV2 alloc] init];
//    NSInteger userId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
//    NSInteger hotelId = [[[UserManagerV2 sharedUserManager] currentUser] userHotelsId];
//    //check data checklist category in database
//    CheckListContentTypeModelV2 *chkListContentType = [manager loadCheckListCategoriesLatest];
//    
//    //get data checklist category WS
//    if (chkListContentType.chkContentId == 0) {
//        [synManager getAllCheckListCategoriesWithUserId:userId AndHotelId:hotelId];
//    }
    
    //get data checklist item score for maid
    [self loadSectionDatas];
    
    //hide saving data.
    [self performSelector:@selector(hiddenHUDAfterSaved:) withObject:HUD afterDelay:0.5];
    
    
    //end loading data
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //Resize content views of current VC to fit with Navigation bar and tabbar
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    //set data for section checklist category
    if(chkListTypeV2 !=nil)
    {
        if (chkListTypeV2.chkType == tCheckmark) {
            [imgType setImage:[UIImage imageNamed:imgCheckMark]];
        } else {
            [imgType setImage:[UIImage imageNamed:imgRating]];
        }
        //set language name checklist form
        if ([[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString: (NSString *)ENGLISH_LANGUAGE] == NO) {
            [lblContentName setText:chkListTypeV2.chkTypeLangName];
        }
        else{
            [lblContentName setText:chkListTypeV2.chkTypeName];
        }
    }
    
    [tbvchkDetail setSeparatorColor:[UIColor clearColor]];
    [tbvchkDetail setBackgroundView:nil];
    [tbvchkDetail setBackgroundColor:[UIColor clearColor]];
    [tbvchkDetail setOpaque:YES];
    
    //set title bar
    NSString *chkListTitle = [NSString stringWithFormat:@"%@ - %@%@",[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_strchecklist],[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOM_NO],[RoomManagerV2 getCurrentRoomNo]];
    [self setTitle:chkListTitle];
    
    //back button navigation
    self.navigationItem.leftBarButtonItem = [MyNavigationBarV2 createBackButtonWithTarget:self selector:@selector(backBarPressed) forControlEvents:UIControlEventTouchUpInside];
    
    //load topbar view
    [self performSelector:@selector(loadTopbarView)];
    
    //load data for checklist detail
    MBProgressHUD	*HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getRaLoadingData]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES];
    
    [self performSelector:@selector(loadCheckListItemScoreForMaid:) withObject:HUD afterDelay:0.1];
    
    
    [lblReceived setText:[[LanguageManagerV2 sharedLanguageManager] getPointsReceived]];
    [lblPossible setText:[[LanguageManagerV2 sharedLanguageManager] getPointsPossible]];
    
    if (chkListTypeV2.chkType == tCheckmark) {
        
        [lblTotal setText:[[LanguageManagerV2 sharedLanguageManager] getPointsTotal]];
        
    } else {
        
        [lblTotal setText:[[LanguageManagerV2 sharedLanguageManager] getRoomStandards]];
        
    }
}

#pragma mark - Back Button
-(void)backBarPressed{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - === Handle Check Save Data ===
#pragma mark
-(BOOL) checkSaveDataWithTagEvent:(NSInteger) tagOfEvent {
    return NO;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - UITableView Delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return sectionInfoArray.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    SectionInfoV2 *sectioninfo = [sectionInfoArray objectAtIndex:section];
    NSInteger numberRowInSection = sectioninfo.rowHeights.count;
    return sectioninfo.open ? numberRowInSection : 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 50;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    //config datas for cell
    SectionInfoV2 *sectionInfo = [sectionInfoArray objectAtIndex:indexPath.section];
    CheckListDetailContentModelV2 *model = [sectionInfo.rowHeights objectAtIndex:indexPath.row];
    
    //set language name checklist item
    NSString *stringDisplay = nil;
    if ([[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString: (NSString *)ENGLISH_LANGUAGE] == NO) {
        stringDisplay = model.chkDetailContentLang;
    }
    else{
        stringDisplay = model.chkDetailContentName;
    }
    
    //Calculate the expected size based on the font and linebreak mode of your label
    CGSize maximumLabelSize = CGSizeMake(150, 9999);
    //Load CheckList Form
    CheckListTypeModelV2 *chkForm = model.typeModel;
    
    if (chkForm.chkType == tCheckmark) {
        maximumLabelSize = CGSizeMake(210, 9999);
    }
    
    CGSize expectedLabelSize = [stringDisplay sizeWithFont:[UIFont fontWithName:@"Arial-BoldMT" size:14.0] 
                                         constrainedToSize:maximumLabelSize 
                                             lineBreakMode:UILineBreakModeWordWrap]; 
    
    //adjust the label the the new height
    if (expectedLabelSize.height < 55) {
        return 55;
    } else
        return expectedLabelSize.height;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    SectionInfoV2 *sectioninfo = [sectionInfoArray objectAtIndex:section];
    //set language name checklist category
    NSString *nameChkContentType = @"";
    if ([[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString: (NSString *)ENGLISH_LANGUAGE] == NO) {
        nameChkContentType = sectioninfo.chkContentType.chkContentNameLang;
    }
    else{
        nameChkContentType = sectioninfo.chkContentType.chkContentName;
    }
    if (sectioninfo.headerView == nil) {
        SectionCheckListContentTypeViewV2 *sview = [[SectionCheckListContentTypeViewV2 alloc] initWithSection:section contentName:nameChkContentType AndStatusArrow:sectioninfo.open];
        
        sectioninfo.headerView = sview;
        sview.delegate = self;
    }
    
    return sectioninfo.headerView;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifyCell = @"CellIndetify";
    CheckListDetailContentCellV2 *cell = nil;
    
    cell = (CheckListDetailContentCellV2 *)[tableView dequeueReusableCellWithIdentifier:identifyCell];
    
    if (cell == nil) {
        NSArray *array = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([CheckListDetailContentCellV2 class]) owner:self options:nil];
        cell = [array objectAtIndex:0];
        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
            [cell.btnDetailContentRating setBackgroundImage:[UIImage imageNamed:imgRatingBoxFlat] forState:UIControlStateNormal];
            [cell.btnDetailContentRating setBackgroundImage:[UIImage imageNamed:imgRatingBoxFlat] forState:UIControlStateHighlighted];
        }
    }
    
    //config datas for cell
    SectionInfoV2 *sectionInfo = [sectionInfoArray objectAtIndex:indexPath.section];
    CheckListDetailContentModelV2 *model = [sectionInfo.rowHeights objectAtIndex:indexPath.row];
    
    
    //Load CheckList Item Score
    CheckListItemCoreDBModelV2 *chkListItemScore = model.itemCoreModel;
    
    //Load CheckList Form
    CheckListTypeModelV2 *chkForm = model.typeModel;
    
    //set user interraction
    [cell setUserInteractionEnabled:NO];

    
    //set language name checklist item
    if ([[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString: (NSString *)ENGLISH_LANGUAGE] == NO) {
        cell.lblDetailContentName.text = model.chkDetailContentLang;
    }
    else{
        cell.lblDetailContentName.text = model.chkDetailContentName;
    }
    
    if (model.chkDetailContentPointInpected >= 0) {
        cell.lblDetailContentPointInpected.text = [NSString stringWithFormat:@"%d",model.chkDetailContentPointInpected];
    }
    
    if(chkForm.chkType == tCheckmark) {
        [cell.imgCheck setHidden:YES];
        [cell.btnDetailContentRating setHidden:YES];
        [cell.btnDetailContentCheck setHidden:NO];
        [cell.lblDetailContentPointInpected setHidden:YES];
        
        [cell setCheckStatus:chkListItemScore.chkItemCore <= 0 ? NO : YES];
        
    } else {
        [cell.btnDetailContentRating setHidden:NO];
        
        [cell.btnDetailContentRating setTitle:(chkListItemScore.chkItemCore < 0 ? @"NA" : [NSString stringWithFormat:@"%d", chkListItemScore.chkItemCore]) forState:UIControlStateNormal];
        [cell.btnDetailContentCheck setHidden:YES];
        [cell.lblDetailContentPointInpected setHidden:NO];
        
        [cell.imgCheck setHidden:NO];
        cell.lblDetailContentName.textColor = [UIColor colorWithRed:6.0/255.0 green:62.0/255.0 blue:127.0/255.0 alpha:1.0];       }
    
    //handle re-location contents cell
    //Calculate the expected size based on the font and linebreak mode of your label
    CGSize maximumLabelSize = CGSizeMake(150, 9999);
    if (chkForm.chkType == tCheckmark) {
        maximumLabelSize = CGSizeMake(210, 9999);
    }
    
    CGSize expectedLabelSize = [cell.lblDetailContentName.text sizeWithFont:[UIFont fontWithName:@"Arial-BoldMT" size:14.0] 
                                                          constrainedToSize:maximumLabelSize 
                                                              lineBreakMode:UILineBreakModeWordWrap]; 
    CGRect frameContentName = cell.lblDetailContentName.frame;
    
    if (expectedLabelSize.height < 55) {
        expectedLabelSize.height = 55;
    }
    
    frameContentName.size.height = expectedLabelSize.height;
    if (chkForm.chkType == tCheckmark) {
        frameContentName.size.width = 210;
    }
    [cell.lblDetailContentName setFrame:frameContentName];
    
    //image check
    CGRect frame = cell.imgCheck.frame;
    frame.origin.y = expectedLabelSize.height / 2 - frame.size.height / 2;
    [cell.imgCheck setFrame:frame];
    
    //label detail content point inspected
    frame = cell.lblDetailContentPointInpected.frame;
    frame.origin.y = expectedLabelSize.height / 2 - frame.size.height / 2;
    [cell.lblDetailContentPointInpected setFrame:frame];
    
    //btn detail content check
    frame = cell.btnDetailContentCheck.frame;
    frame.origin.y = expectedLabelSize.height / 2 - frame.size.height / 2;
    [cell.btnDetailContentCheck setFrame:frame];
    
    //btn detail content rating
    frame = cell.btnDetailContentRating.frame;
    frame.origin.y = expectedLabelSize.height / 2 - frame.size.height / 2;
    [cell.btnDetailContentRating setFrame:frame];
    
    //label line
    frame = cell.lblLine.frame;
    frame.origin.y = expectedLabelSize.height - 1;
    [cell.lblLine setFrame:frame];
    
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell setBackgroundColor:[UIColor clearColor]];
    [cell setIndexpath:indexPath];
    cell.delegate = self;
    
    return cell;
}

#pragma mark - Section Header Delegate
-(void)sectionHeaderView:(SectionCheckListContentTypeViewV2 *)sectionheaderView sectionOpened:(NSInteger)section {
    
    SectionInfoV2 *sectionInfo = [sectionInfoArray objectAtIndex:section];
    sectionInfo.open = YES;
    
    NSInteger countOfRowsToInsert = [sectionInfo.rowHeights count];
    if (countOfRowsToInsert == 0) {
        return;
    }
    NSMutableArray *indexPathsToInsert = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i < countOfRowsToInsert; i++) {
        [indexPathsToInsert addObject:[NSIndexPath indexPathForRow:i
                                                         inSection:section]];
    }
    
    UITableViewRowAnimation insertAnimation;
    insertAnimation = UITableViewRowAnimationAutomatic;
    
    [self.tbvchkDetail beginUpdates];
    
    [self.tbvchkDetail insertRowsAtIndexPaths:indexPathsToInsert
                             withRowAnimation:insertAnimation];
    [self.tbvchkDetail endUpdates];
    
    [self.tbvchkDetail scrollToRowAtIndexPath:[indexPathsToInsert objectAtIndex:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

-(void)sectionHeaderView:(SectionCheckListContentTypeViewV2 *)sectionheaderView sectionClosed:(NSInteger)section {
    SectionInfoV2 *sectionInfo = [sectionInfoArray
                                  objectAtIndex:section];
    
    sectionInfo.open = NO;
    NSInteger countOfRowsToDelete = [self.tbvchkDetail
                                     numberOfRowsInSection:section];
    
    if (countOfRowsToDelete > 0) {
        NSMutableArray *indexPathsToDelete = [[NSMutableArray alloc] init];
        for (NSInteger i = 0; i < countOfRowsToDelete; i++) {
            [indexPathsToDelete addObject:[NSIndexPath indexPathForRow:i
                                                             inSection:section]];
        }
        
        [tbvchkDetail beginUpdates];
        [self.tbvchkDetail deleteRowsAtIndexPaths:indexPathsToDelete
                                 withRowAnimation:UITableViewRowAnimationAutomatic];
        [tbvchkDetail endUpdates];
    }
}

#pragma mark - Loading data
-(void)loadSectionDatas {
    
    CheckListManagerV2 *manager = [[CheckListManagerV2 alloc] init];
    
    //load checklist items have the same checklist form id
    NSMutableArray *arraychkItemByFormId = [manager loadCheckListItemByChkFormId:chkListTypeV2.chkTypeId];
    NSMutableArray *arraychkItemByCategoryId = [[NSMutableArray alloc] init];
    
    sectionInfoArray = [[NSMutableArray alloc] init];
    
    //load checklist items have the same checklist categories id
    for (CheckListDetailContentModelV2 *chkItems in arraychkItemByFormId) {
        
        if (arraychkItemByCategoryId.count != 0) {
            
            CheckListDetailContentModelV2 *chkItemInArray  = [arraychkItemByCategoryId objectAtIndex:0];
            
            if (chkItems.chkContentId != chkItemInArray.chkContentId) {
                
                //array checklist item has the same category id
                arraychkItemByCategoryId = [[NSMutableArray alloc] init];
                arraychkItemByCategoryId = [manager loadCheckListItemByChkCategoriesId:chkItems.chkContentId formId:chkListTypeV2.chkTypeId];
                
                //Load CheckList Categories
                CheckListContentTypeModelV2 *chkCategory = [[CheckListContentTypeModelV2 alloc] init];
                CheckListDetailContentModelV2 *chkItemByCategory = [arraychkItemByCategoryId objectAtIndex:0];
                chkCategory.chkContentId = chkItemByCategory.chkContentId;
                [manager loadCheckListCategories:chkCategory];
                
                //set checklist category for section
                SectionInfoV2 *sectionInfo = [[SectionInfoV2 alloc] init];
                sectionInfo.chkContentType = chkCategory;
                
                //load checklist item
                for (CheckListDetailContentModelV2 *chkDetailContentModel in arraychkItemByCategoryId) {
                    
                    //load checklist form score
                    ChecklistFormScoreModel *chkFormScore = [manager loadCheckListFormScoreByChkListId:chkList.chkListId AndChkFormId:chkDetailContentModel.chkTypeId AndChkUserId:[[[UserManagerV2 sharedUserManager] currentUser] userId]];
                    
                    //load checklist item score
                    CheckListItemCoreDBModelV2 *chklistItemScoreDB = [manager loadCheckListItemScoreByItemId:chkDetailContentModel.chkDetailContentId AndFormScoreId:chkFormScore.dfId];
                    
                    if (chklistItemScoreDB.chkItemCoreDetailContentId == 0) {
                        //insert checklist item score
                        chklistItemScoreDB.chkItemCoreDetailContentId = chkDetailContentModel.chkDetailContentId;
                        chklistItemScoreDB.chkItemCore = -1;
                        chklistItemScoreDB.chkFormScore = chkFormScore.dfId;
                        chklistItemScoreDB.chkPostStatus = POST_STATUS_UN_CHANGED;
                        [manager insertCheckListItemCore:chklistItemScoreDB];
                        
                        //load checklist item score
                        [manager loadCheckListItemScoreByItemId:chklistItemScoreDB.chkItemCoreDetailContentId AndFormScoreId:chkFormScore.dfId];
                        
                    }
                    
                    //set checklist item score for checklist item
                    chkDetailContentModel.itemCoreModel = chklistItemScoreDB;
                    
                    //load checklist form
                    CheckListTypeModelV2 *chkForm = [[CheckListTypeModelV2 alloc] init];
                    chkForm.chkTypeId = chkDetailContentModel.chkTypeId;
                    [manager loadCheckListForm:chkForm];
                    
                    //set checklist form for checklist item;
                    chkDetailContentModel.typeModel = chkForm;
                    
                    [sectionInfo insertObjectToNextIndex:chkDetailContentModel];
                }
                
                [sectionInfoArray addObject:sectionInfo];
                
            }
            
        }
        else
        {
            arraychkItemByCategoryId = [[NSMutableArray alloc] init];
            arraychkItemByCategoryId = [manager loadCheckListItemByChkCategoriesId:chkItems.chkContentId formId:chkListTypeV2.chkTypeId];
            
            //Load CheckList Categories
            CheckListContentTypeModelV2 *chkCategory = [[CheckListContentTypeModelV2 alloc] init];
            CheckListDetailContentModelV2 *chkItemByCategory = [arraychkItemByCategoryId objectAtIndex:0];
            chkCategory.chkContentId = chkItemByCategory.chkContentId;
            [manager loadCheckListCategories:chkCategory];
            
            //set checklist category for section
            SectionInfoV2 *sectionInfo = [[SectionInfoV2 alloc] init];
            sectionInfo.chkContentType = chkCategory;
            
            //load checklist item
            for (CheckListDetailContentModelV2 *chkDetailContentModel in arraychkItemByCategoryId) {
                
                //load checklist form score
                ChecklistFormScoreModel *chkFormScore = [manager loadCheckListFormScoreByChkListId:chkList.chkListId AndChkFormId:chkDetailContentModel.chkTypeId AndChkUserId:[[[UserManagerV2 sharedUserManager] currentUser] userId]];
                
                //load checklist item score
                CheckListItemCoreDBModelV2 *chklistItemScoreDB = [manager loadCheckListItemScoreByItemId:chkDetailContentModel.chkDetailContentId AndFormScoreId:chkFormScore.dfId];
                
                if (chklistItemScoreDB.chkItemCoreDetailContentId == 0) {
                    //insert checklist item score
                    chklistItemScoreDB.chkItemCoreDetailContentId = chkDetailContentModel.chkDetailContentId;
                    chklistItemScoreDB.chkItemCore = -1;
                    chklistItemScoreDB.chkFormScore = chkFormScore.dfId;
                    chklistItemScoreDB.chkPostStatus = POST_STATUS_UN_CHANGED;
                    [manager insertCheckListItemCore:chklistItemScoreDB];
                    
                    //load checklist item score
                    [manager loadCheckListItemScoreByItemId:chklistItemScoreDB.chkItemCoreDetailContentId AndFormScoreId:chkFormScore.dfId];
                }
                
                //set checklist item score for checklist item
                chkDetailContentModel.itemCoreModel = chklistItemScoreDB;
                
                //load checklist form
                CheckListTypeModelV2 *chkForm = [[CheckListTypeModelV2 alloc] init];
                chkForm.chkTypeId = chkDetailContentModel.chkTypeId;
                [manager loadCheckListForm:chkForm];
                
                //set checklist form for checklist item;
                chkDetailContentModel.typeModel = chkForm;
                
                [sectionInfo insertObjectToNextIndex:chkDetailContentModel];
                
            }
            
            [sectionInfoArray addObject:sectionInfo];
        }
        
    }
    
    [self calculatorScore];
    
    //reload table views
    [tbvchkDetail reloadData];
}

#pragma mark - Calculator Score
-(void)calculatorScore
{
    totalPossible = 0;
    totalReceived = 0;
    
    for (NSInteger indexSection=0; indexSection < sectionInfoArray.count; indexSection ++) {
        SectionInfoV2 *sectionInfo = [sectionInfoArray objectAtIndex:indexSection];
        
        for (NSInteger indexRow=0; indexRow < sectionInfo.rowHeights.count; indexRow++) {
            CheckListDetailContentModelV2 *model = [sectionInfo.rowHeights objectAtIndex:indexRow];
            
            //Load CheckList Item Score
            CheckListItemCoreDBModelV2 *chkListItemScore = model.itemCoreModel;
            
            if (chkListItemScore.chkItemCore != -1) {
                totalReceived += chkListItemScore.chkItemCore;
            }
            
            if (chkListTypeV2.chkType == tCheckmark) {
                totalPossible += 1;
            } 
            else {
                totalPossible += model.chkDetailContentPointInpected;
            }
        }
    }
    
    [lblPointsReceived setText:[NSString stringWithFormat:@"%d",totalReceived]];
    
    if (totalReceived > 0) {
        [lblPointsPossible setText:[NSString stringWithFormat:@"%d",totalPossible]];
        totalPoint = (1.0 * totalReceived / totalPossible)*100.0;
        [lblPointsTotal setText:[NSString stringWithFormat:@"%.0f %%",totalPoint]];
    }
}

#pragma didden after save.
-(void) hiddenHUDAfterSaved:(MBProgressHUD *)HUD{
    [HUD hide:YES];
    [HUD removeFromSuperview];
}

#pragma mark - Handle Topbar Methods
-(void)loadTopbarView {
    TopbarViewV2 *topbar = [[TopbarViewV2 alloc] initViewWithSuperModel:[[CommonVariable sharedCommonVariable] superRoomModel]];
    [topbar setTag:tagTopbarView];
    [self.view addSubview:topbar];
    [topbar setHidden:YES];
    
    [topbar setAlpha:0.0];
    [self addButtonHandleShowHideTopbar];
}

-(void)showTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationWillStartSelector:@selector(setHiddenTopbarView)];
    
    f = topbar.frame;
    f.origin.y = 0;
    [topbar setFrame:f];
    [topbar setAlpha:1.0];
    
    [self adjustShowForViews];
    
    [UIView commitAnimations];
}

-(void)hideTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDidStopSelector:@selector(setHiddenTopbarView)];
    [UIView setAnimationDelegate:self];
    
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    [topbar setAlpha:0.0];
    
    [self adjustRemoveForViews];
    
    [UIView commitAnimations];
}

-(void)setHiddenTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    [topbar setHidden:!topbar.hidden];
}

-(TopbarViewV2 *)getTopBarView {
    return (TopbarViewV2 *)[self.view viewWithTag:tagTopbarView];
}

-(void)addButtonHandleShowHideTopbar {
    UIButton *titleBarButton = [[UIButton alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
    
    titleBarButton.titleLabel.backgroundColor = [UIColor clearColor];
    titleBarButton.titleLabel.font = FONT_BUTTON_TOPBAR;
    titleBarButton.titleLabel.textAlignment = UITextAlignmentCenter;
    titleBarButton.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
    [titleBarButton.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
    [titleBarButton.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
    [titleBarButton.titleLabel setMinimumFontSize:10];
    [titleBarButton.titleLabel setAdjustsFontSizeToFitWidth:YES];
    [titleBarButton addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    [titleBarButton setTitle:[self title] forState:UIControlStateNormal];
    self.navigationItem.titleView = titleBarButton;
    
    //Move title to center vertical
    CGRect titleViewBounds = self.navigationItem.titleView.bounds;
    int moveLeftTitle = 0;
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        moveLeftTitle = 10;
    }
    self.navigationItem.titleView.bounds = CGRectMake(titleViewBounds.origin.x + moveLeftTitle, Y_TOPBAR_TITLE, titleViewBounds.size.width, titleViewBounds.size.height);
}

-(void)topbarTapped:(UIButton *)sender {
    TopbarViewV2 *topbar = [self getTopBarView];
    [sender setEnabled:NO];
    if (topbar.isHidden) {
        [self showTopbarView];
    } else {
        [self hideTopbarView];
    }
    [self performSelector:@selector(topbarButtonEnable:) withObject:sender afterDelay:0.5];
}

-(void) topbarButtonEnable:(UIButton *) topbarBtn {
    [topbarBtn setEnabled:YES];
}

-(void)adjustShowForViews {
    //adjust all views when show 
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    CGRect fvChklistDetail = vHeaderChkListDetail.frame;
    
    [vHeaderChkListDetail setFrame:CGRectMake(0, f.size.height, 320, fvChklistDetail.size.height)];
    CGRect ftbv = tbvchkDetail.frame;
    [tbvchkDetail setFrame:CGRectMake(0, f.size.height + fvChklistDetail.size.height, 320, ftbv.size.height - f.size.height)];
}

-(void)adjustRemoveForViews { 
    //adjust all views when hide
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    CGRect fvChklistDetail = vHeaderChkListDetail.frame;
    
    [vHeaderChkListDetail setFrame:CGRectMake(0, 0, 320, fvChklistDetail.size.height)];
    CGRect ftbv = tbvchkDetail.frame;
    [tbvchkDetail setFrame:CGRectMake(0, fvChklistDetail.size.height, 320, ftbv.size.height + f.size.height)];
}



@end
