//
//  CheckListForMaidViewV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 5/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MenuCheckListCellV2.h"
#import "CheckListModelV2.h"
#import "CheckListTypeModelV2.h"
#import "CheckListDetailViewV2.h"
#import "CheckListScoreCellV2.h"
#import "MBProgressHUD.h"
#import "SyncManagerV2.h"
#import "CheckListItemDBModelV2.h"
#import "CheckListDetailForMaidViewV2.h"

@interface CheckListForMaidViewV2 : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    UIImageView *imgViewChecListScore;
    UITableView *tbvScoreCheckList;
    UITableView *tbvChecklist;
    UILabel *lblProcessAudit;
    UILabel *lblShortForm;
    UILabel *lblLongForm;
    UILabel *lblOveral;
    UILabel *lblCheckListStatus;
    UIImageView *imgChkStatus;
    UILabel *lblResultProcessAudit;
    UILabel *lblResultShortForm;
    UILabel *lblResultLongForm;
    UILabel *lblResultOverall;
    NSMutableArray *datas;
    CheckListModelV2 *chkModel;
    CGFloat totalChkListScore;
    NSInteger totalhkListFormScore;
    NSInteger totalPossible;
    CGFloat totalPercentFormScore;    
    CGFloat totalOveral;

}
@property (strong, nonatomic) IBOutlet UIImageView *imgViewChecListScore;
@property (strong, nonatomic) IBOutlet UITableView *tbvScoreCheckList;
@property (strong, nonatomic) IBOutlet UITableView *tbvChecklist;
@property (strong, nonatomic) IBOutlet UILabel *lblProcessAudit;
@property (strong, nonatomic) IBOutlet UILabel *lblShortForm;
@property (strong, nonatomic) IBOutlet UILabel *lblLongForm;
@property (strong, nonatomic) IBOutlet UILabel *lblOveral;
@property (strong, nonatomic) IBOutlet UILabel *lblCheckListStatus;
@property (strong, nonatomic) IBOutlet UIImageView *imgChkStatus;
@property (strong, nonatomic) IBOutlet UILabel *lblResultProcessAudit;
@property (strong, nonatomic) IBOutlet UILabel *lblResultShortForm;
@property (strong, nonatomic) IBOutlet UILabel *lblResultLongForm;
@property (strong, nonatomic) IBOutlet UILabel *lblResultOverall;
@property (nonatomic, strong) NSMutableArray *datas;
@property (readwrite, nonatomic) NSInteger row;
@property (nonatomic, strong) CheckListModelV2 *chkModel;
@property  (readwrite, nonatomic) CGFloat totalChkListScore;
@property (readwrite, nonatomic) NSInteger totalhkListFormScore;
@property (readwrite, nonatomic) NSInteger totalPossible;
@property (readwrite, nonatomic)CGFloat totalPercentFormScore;
@property (readwrite, nonatomic) CGFloat totalOveral;
@property (nonatomic, strong) NSString *roomNo;

@end
