//
//  CheckListForMaidViewV2.m
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 5/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CheckListForMaidViewV2.h"
#import "TopbarViewV2.h"
#import "ehkConvert.h"
#import "LanguageManagerV2.h"
//#import "HomeViewV2.h"
#import "TasksManagerV2.h"
#import "MyNavigationBarV2.h"
#import "DeviceManager.h"

#define imgStatusPass @"pass_icon.png"
#define imgStatusFail @"fail_icon.png"

@interface CheckListForMaidViewV2 (PrivateMethods)

-(void)backBarPressed;
-(void)loadDataCheckList;
-(void)calculatorCheckListScoreForMaid;

#define tagTopbarView 1234

-(void) loadTopbarView;
-(void) showTopbarView;
-(void) hideTopbarView;
-(void) setHiddenTopbarView;
-(void) adjustShowForViews;
-(void) adjustRemoveForViews;
-(void) addButtonHandleShowHideTopbar;
-(void) topbarTapped:(UIButton *) sender;
-(TopbarViewV2 *) getTopBarView;
-(void) saveData;

@end

@implementation CheckListForMaidViewV2
@synthesize imgChkStatus, imgViewChecListScore, tbvChecklist, tbvScoreCheckList, lblOveral, lblLongForm, lblShortForm, lblProcessAudit, lblResultOverall, lblResultLongForm, lblCheckListStatus, lblResultShortForm, lblResultProcessAudit, datas, row, chkModel, totalChkListScore, totalhkListFormScore, totalOveral, totalPossible, totalPercentFormScore;
@synthesize roomNo;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

//load checklist item score for maid
-(void) loadDataCheckListItemScoreForMaidService:(MBProgressHUD *) HUD
{
    //enable flag to announce, is loading data
    [[HomeViewV2 shareHomeView] waitingLoadingData];
    
    
    //get checklist item score for maid
    SyncManagerV2 *synManager = [[SyncManagerV2 alloc] init];
    NSInteger userId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    NSInteger roomAssignId = [TasksManagerV2 getCurrentRoomAssignment];
    NSInteger hotelId = [[[UserManagerV2 sharedUserManager] currentUser] userHotelsId];
    NSString *dateLastModified = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd"];
    
    //load List CheckList FormScore by CheckListId
    CheckListManagerV2 *manager = [[CheckListManagerV2 alloc] init];
    chkModel = [[CheckListModelV2 alloc] init];
    chkModel.chkListId  = roomAssignId;
    chkModel.chkListUserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    
    if ([[[UserManagerV2 sharedUserManager] currentUser] userSupervisorId] == 0) {
        return;
    }
    
    [manager loadCheckListData:chkModel];
    
    //if data checklist have not in database
    //insert checklist 
    if (chkModel.chkListRoomId.length <= 0) {
        
        NSMutableArray *arrayChkList;
        NSMutableArray *arrayRoomModel = [[NSMutableArray alloc] init];
        
        //load all room assign
        NSMutableArray *arrayRoomAssign = nil;
        [[RoomManagerV2 sharedRoomManager] getInspectedRoom];
        arrayRoomAssign = [NSMutableArray arrayWithArray:[[RoomManagerV2 sharedRoomManager] roomAssignmentList]];
        
        //insert checklist by all room
        for (NSDictionary *roomAssign in arrayRoomAssign) {
            
            //load room model
            RoomModelV2 *roomModel = [[RoomModelV2 alloc] init];
            roomModel.room_Id = [roomAssign objectForKey:kRoomNo];
            
            [[RoomManagerV2 sharedRoomManager] loadRoomModel:roomModel];
                        
            //load data of checkList by userID and roomId
            CheckListModelV2 *chkModelWS = [[CheckListModelV2 alloc] init];
            chkModelWS.chkListId  = [[roomAssign objectForKey:kRoomAssignmentID] integerValue];
            chkModelWS.chkListUserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
            [manager loadCheckListData:chkModelWS];
            
            if (chkModelWS.chkListRoomId.length <= 0) {
                
                //Insert CheckList
                chkModelWS = [[CheckListModelV2 alloc] init];
                chkModelWS.chkListId = [[roomAssign objectForKey:kRoomAssignmentID] integerValue];
                chkModelWS.chkListRoomId  = roomModel.room_Id;
                chkModelWS.chkListUserId = userId;
                
                int inspectionStatusId = [[roomAssign objectForKey:KInspection_Status_id] integerValue];
                if (inspectionStatusId == ENUM_INSPECTION_COMPLETED_PASS) {
                    chkModelWS.chkListStatus = tChkPass;
                }
                else if (inspectionStatusId == ENUM_INSPECTION_COMPLETED_FAIL) {
                    chkModelWS.chkListStatus = tChkFail;
                }
                else {
                    chkModelWS.chkListStatus = tNotCompleted;
                }
                
                chkModelWS.chkCore = -1;
                [manager insertCheckList:chkModelWS];
                
            }
            
            //load all room model
            [arrayRoomModel addObject:roomModel];
            
        }
        
        //Get all checklist from database
        arrayChkList = [manager loadAllCheckListByCurrentUserData];
        
//        //--------WS----------
//        //Get checklist form score from WS
//        [synManager getCheckListRoomTypeWithUserId:userId HotelId:hotelId AndArrayRoomModel:arrayRoomModel AndArrayChkList:arrayChkList];
        
        //Get checkList form from WS
        [synManager getAllCheckListFormWithUserId:userId AndHotelId:hotelId];
        //-------end----------
        
    }
    
//Hao Tran remove for change WS checklist
//    //check data checklist categories in database
//    CheckListContentTypeModelV2 *chkListCategories = [manager loadCheckListCategoriesLatest];
//    
//    //get data checklist category WS
//    if (chkListCategories.chkContentId == 0) {
//        [synManager getAllCheckListCategoriesWithUserId:userId AndHotelId:hotelId];
//    }
//    //check data checklist item in database
//    CheckListDetailContentModelV2 *chkListDetailContent = [manager loadCheckListItemLatest];
//    
//    //get data checklist item WS
//    if (chkListDetailContent.chkDetailContentId == 0) {
//        [synManager getAllCheckListItemWithUserId:userId AndHotelId:hotelId];
//    }

    //Insert CheckList Form Score
    NSMutableArray *arrayChkFrom = [manager loadAllCheckListForm];
    
    for (CheckListTypeModelV2 *chkTypeModel in arrayChkFrom) {
        ChecklistFormScoreModel *chkFormScore = [manager loadCheckListFormScoreByChkListId:chkModel.chkListId AndChkFormId:chkTypeModel.chkTypeId AndChkUserId:[UserManagerV2 sharedUserManager].currentUser.userId];
        
        if(chkFormScore.dfId == 0) {
            chkFormScore.dfFormId = chkTypeModel.chkTypeId;
            chkFormScore.dfChecklistId = chkModel.chkListId;
            chkFormScore.dfUserId = [UserManagerV2 sharedUserManager].currentUser.userId;
            chkFormScore.dfScore = -1;
            chkFormScore.dfPostStatus = POST_STATUS_SAVED_UNPOSTED;
            [manager insertCheckListFormScore:chkFormScore];
        }
    }
    
    //get checklist item score for maid WS
    [synManager getCheckListItemScoreForMaidWithUserId:userId DutyAssignId:roomAssignId AndLastModified:dateLastModified];
    
    //load data for checklist view
    [self loadDataCheckList];
    
    //hide saving data.
    [self performSelector:@selector(hiddenHUDAfterSaved:) withObject:HUD afterDelay:0.5];
    
    
    //end loading data
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
}

#pragma mark - View lifecycle
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //show wifi view
    if(!isDemoMode){
        [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationShowWifiView] object:nil];
    }
    [tbvChecklist reloadData];
    [tbvScoreCheckList reloadData];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //Resize content views of current VC to fit with Navigation bar and tabbar
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    //set title bar
    NSString *chkListTitle = [NSString stringWithFormat:@"%@ - %@%@",[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_strchecklist],[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOM_NO],[RoomManagerV2 getCurrentRoomNo]];
    [self setTitle:chkListTitle];
    
    //back button navigation
    self.navigationItem.leftBarButtonItem = [MyNavigationBarV2 createBackButtonWithTarget:self selector:@selector(backBarPressed) forControlEvents:UIControlEventTouchUpInside];
    
    //load topbar view
    [self performSelector:@selector(loadTopbarView)];
    
    //get data from service
    MBProgressHUD	*HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getRaLoadingData]];
    [HUD show:YES];
    [self.tabBarController.view addSubview:HUD];
    
    [self performSelector:@selector(loadDataCheckListItemScoreForMaidService:) withObject:HUD afterDelay:0.5];
}

#pragma mark - Back Button
-(void)backBarPressed{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - === Handle Check Save Data ===
#pragma mark
-(BOOL) checkSaveDataWithTagEvent:(NSInteger) tagOfEvent {
    return NO;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - UITableView Delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView == tbvScoreCheckList) {
        //table score checklist will has row checklist status
        NSInteger height = (datas.count + 2) * 30;
        [tbvChecklist setFrame:CGRectMake(0, 0, 320, 387 - height)];
        [tbvScoreCheckList setFrame:CGRectMake(0, 387 - height, 320, height)];
        [imgViewChecListScore setFrame:CGRectMake(0, 387 - height, 320, height)];
        return datas.count + 2;
    }
    return datas.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(tableView == tbvScoreCheckList)
    {
        return 30;
    }
    return 56;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView == tbvScoreCheckList)
    {
        //table score
        static NSString *identifyScoreCell = @"IdentifyScoreCell";
        CheckListScoreCellV2 *cell = nil;
        cell = [tableView dequeueReusableCellWithIdentifier:identifyScoreCell];
        if (cell == nil) {
            NSArray *array = [[NSBundle mainBundle] loadNibNamed:@"CheckListScoreCellV2" owner:self options:nil];
            cell = [array objectAtIndex:0];
        }
        
        //config data
        row = indexPath.row;
        [cell.lblLineCheckList setHidden:YES];
        
        //when table score checklist has no checklist status
        if (row == datas.count) {
            
            [cell.lblNameScoreCheckList setText:[NSString stringWithFormat:@"%@",[[LanguageManagerV2 sharedLanguageManager] getOverral]]];
            
            [cell.lblScoreCheckList setText:[NSString stringWithFormat:@"%d %%",chkModel.chkCore < 0 ? 0 : chkModel.chkCore]];
            
            return cell;
        }
        
        //set checklist status when score >= 90
        if (row == datas.count + 1) {
            
            [cell.lblNameScoreCheckList setText:[NSString stringWithFormat:@"%@",[[LanguageManagerV2 sharedLanguageManager] getChecklistStatus]]];
            
//#warning fix checkList : change inspection status image
//            if (chkModel.chkCore >= 90) {
//                [cell.imgStatusCheckList setImage:[UIImage imageNamed:imgStatusPass]];
//            }
//            else
//            {
//                [cell.imgStatusCheckList setImage:[UIImage imageNamed:imgStatusFail]];
//            }
            
            if (chkModel.chkListStatus == tChkPass) {
                [cell.imgStatusCheckList setImage:[UIImage imageNamed:imgStatusPass]];
            }
            else {
                [cell.imgStatusCheckList setImage:[UIImage imageNamed:imgStatusFail]];
            }
//#warning end
            
            [cell.lblScoreCheckList setHidden:YES];
            [cell.imgStatusCheckList setHidden:NO];
            
            return cell;
        }
        
        [cell.imgStatusCheckList setHidden:YES];
        [cell.lblScoreCheckList setHidden:NO];
        
        //set line for group checklist has same kind (group checkmark, group rating)
        CheckListTypeModelV2 *model = [datas objectAtIndex:indexPath.row];
        if (indexPath.row <= datas.count) {
            if (datas.count > indexPath.row + 1) {
                CheckListTypeModelV2 *nextModel = [datas objectAtIndex:indexPath.row + 1];
                if (model.chkType == tCheckmark && nextModel.chkType == tRating) {
                    [cell.lblLineCheckList setHidden:NO];
                }
                else{
                    [cell.lblLineCheckList setHidden:YES];
                }
            }
        }
        
        //set line for checklist status
        if (row == datas.count - 1) {
            [cell.lblLineCheckList setHidden:NO];
        }
        
        //----Load CheckList Form Score and set score on view
        CheckListManagerV2 *chkManager = [[CheckListManagerV2 alloc] init];
        ChecklistFormScoreModel *chkListFormScore = [chkManager loadCheckListFormScoreByChkListId:chkModel.chkListId AndChkFormId:model.chkTypeId AndChkUserId:[UserManagerV2 sharedUserManager].currentUser.userId];
        
        //set language name checklist form
        if ([[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString: (NSString *)ENGLISH_LANGUAGE] == NO) {
            [cell.lblNameScoreCheckList setText:[NSString stringWithFormat:@"%@ %%:", model.chkTypeLangName]];
        }
        else{
            [cell.lblNameScoreCheckList setText:[NSString stringWithFormat:@"%@ %%:", model.chkTypeName]];
        }
        
        [cell.lblScoreCheckList setText:[NSString stringWithFormat:@"%d %%", chkListFormScore.dfScore < 0 ? 0 : chkListFormScore.dfScore]];
        
        return cell;
    }
    
    //table menu checklist
    static NSString *cellIndentify = @"cellIndentify";
    MenuCheckListCellV2 *cell = nil;
    cell = [tableView dequeueReusableCellWithIdentifier:cellIndentify];
    if(cell == nil)
    {
        NSArray *array = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([MenuCheckListCellV2 class]) owner:self options:nil];
        cell = [array objectAtIndex:0];
    }
    
    CheckListTypeModelV2 *model = [datas objectAtIndex:indexPath.row];
    
    //----Load CheckList Form Score and set score on view
    CheckListManagerV2 *chkManager = [[CheckListManagerV2 alloc] init];
    ChecklistFormScoreModel *chkListFormScore = [chkManager loadCheckListFormScoreByChkListId:chkModel.chkListId AndChkFormId:model.chkTypeId AndChkUserId:[UserManagerV2 sharedUserManager].currentUser.userId];
    
    //set status of checklist when checked
    // if (chkListFormScore.dfScore >= 0) {
    if (chkListFormScore.dfScore > 0) {
        [cell.imgStatusCheckList setHidden:NO];
    } else {
        [cell.imgStatusCheckList setHidden:YES];
    }
    
    //set checklist is checkmark or rating
    if (model.chkType == tCheckmark) {
        [cell.imgCheckList setImage:[UIImage imageNamed:imgCheckMark]];
    }else{
        [cell.imgCheckList setImage:[UIImage imageNamed:imgRating] ];
    }
    
    //set language name checklist
    if ([[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString: (NSString *)ENGLISH_LANGUAGE] == NO) {
        [cell.lblNameChecklist setText:model.chkTypeLangName];
    }
    else{
        [cell.lblNameChecklist setText:model.chkTypeName];
    }
    
    return cell;

}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //load checklist form row
    CheckListDetailForMaidViewV2 *chkDetailV2 = [[CheckListDetailForMaidViewV2 alloc]initWithNibName:NSStringFromClass([CheckListDetailForMaidViewV2 class]) bundle:nil];
    
    CheckListTypeModelV2 *model = [datas objectAtIndex:indexPath.row];
    chkDetailV2.chkListTypeV2 = model;
    
    //get checklist for checklist item
    chkDetailV2.chkList = chkModel;
    
    //button back
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK] style:UIBarButtonItemStyleBordered target:nil action:nil];
    self.navigationItem.backBarButtonItem = backButton;
    
    [self.navigationController pushViewController:chkDetailV2 animated:YES];
}

#pragma mark - Load Data for CheckList
-(void)loadDataCheckList {    
    CheckListManagerV2 *manager = [[CheckListManagerV2 alloc] init];
    datas = [[NSMutableArray alloc] init];
    
    //Load List CheckList FormScore by CheckListId
    chkModel = [[CheckListModelV2 alloc] init];
    chkModel.chkListId  = [TasksManagerV2 getCurrentRoomAssignment];
    chkModel.chkListUserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    [manager loadCheckListData:chkModel];

    // Son Nguyen Edit for just get all check list from
    self.datas = [manager loadAllCheckListForm];

//    NSMutableArray *arrayChkFormScore = [manager loadChecklistFormScoreByChkListId:chkModel.chkListId];
//
//    for (ChecklistFormScoreModel *formScoreModel in arrayChkFormScore) {
//        
//        //load Checklist Form by CheckList FormScore
//        CheckListTypeModelV2 *chkListType = [[CheckListTypeModelV2 alloc] init];
//        chkListType.chkTypeId = formScoreModel.dfFormId;
//        [manager loadCheckListForm:chkListType];
//        
//        [self.datas addObject:chkListType];
//    }
    
    //update score for checklist if checklist have no score
//    if (chkModel.chkCore == -1) {
//        [self calculatorCheckListScoreForMaid];
//    }

    [self calculatorCheckListScoreForMaid];
    
//#warning fix checkilist : why update whem just view checklist without modification
    //Update Score in CheckList Form Score
//    NSMutableArray *arrayChkListForm = [manager loadAllCheckListForm];
//    for(CheckListTypeModelV2 *chkTypeModelV2 in arrayChkListForm) {
//        if(chkTypeModelV2.chkTypeId != 0) {
//            ChecklistFormScoreModel *chkFormScore = [manager loadCheckListFormScoreByChkListId:chkModel.chkListId  AndChkFormId:chkTypeModelV2.chkTypeId AndChkUserId:[UserManagerV2 sharedUserManager].currentUser.userId];
//            chkFormScore.dfScore = totalPercentFormScore;
//            chkFormScore.dfPostStatus = POST_STATUS_SAVED_UNPOSTED;
//            [manager updateCheckListFormScore:chkFormScore];
//        }
//    }
//#warning end
    
    //set height for table score checklist and table checklist
    NSInteger height = (datas.count + 2) * 30;
    [tbvChecklist setFrame:CGRectMake(0, 0, 320, 387 - height)];
    [tbvScoreCheckList setFrame:CGRectMake(0, 387 - height, 320, height)];
    [imgViewChecListScore setFrame:CGRectMake(0, 387 - height, 320, height)];
    
    [tbvScoreCheckList reloadData];
    [tbvChecklist reloadData];
    
}

#pragma mark - Calculator Score
-(void)calculatorCheckListScoreForMaid {
    totalChkListScore = 0;
    CheckListManagerV2 *manager = [[CheckListManagerV2 alloc] init];
    
    //add score for checklist
    NSMutableArray *arrayChkFormScore = [manager loadChecklistFormScoreByChkListId:chkModel.chkListId AndChkUserId:[UserManagerV2 sharedUserManager].currentUser.userId];
    
    for (ChecklistFormScoreModel *formScoreModel in arrayChkFormScore) {        
        totalhkListFormScore = 0;
        totalPossible = 0;
        totalPercentFormScore = 0;
        
        //load checklist form
        CheckListTypeModelV2 *chkListForm = [[CheckListTypeModelV2 alloc] init];
        chkListForm.chkTypeId = formScoreModel.dfFormId;
        [manager loadCheckListForm:chkListForm];
        
        //set total possible
        totalPossible = chkListForm.chkTypePercentageOverall;

        //load checklist item score
        NSMutableArray *arrayItemScore = [manager loadCheckListItemCoreByChkListFormScoreIdData:formScoreModel.dfId];
        
        for (CheckListItemCoreDBModelV2 *itemScoreModel in arrayItemScore) {            
            if (itemScoreModel.chkItemCore != -1) {
                totalhkListFormScore += itemScoreModel.chkItemCore;
            }
        }

        //update score for checklist form
        totalPercentFormScore = roundf((1.0 * totalhkListFormScore / totalPossible)*100.0);
        formScoreModel.dfScore = totalPercentFormScore;
        [manager updateCheckListFormScore:formScoreModel];
        
        //set score for checklist
        totalChkListScore += totalPercentFormScore;
    }
    
    //total score of checklist (Overal)
    if (datas.count == 0) {
        totalOveral = 0;
    } else {
        totalOveral = (totalChkListScore * 1.0);
//        totalOveral = (totalChkListScore * 1.0 / datas.count);
    }

    //set room inspected status
    RoomAssignmentModelV2 *rAssign = [[RoomAssignmentModelV2 alloc] init];
    rAssign.roomAssignment_Id = [TasksManagerV2 getCurrentRoomAssignment];
    rAssign.roomAssignment_UserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    [[RoomManagerV2 sharedRoomManager] load:rAssign];
    
//#warning fix CheckiList
//    if (rAssign.roomAssignmentRoomInspectionStatusId == ENUM_INSPECTION_COMPLETED_PASS && totalOveral < 90) {
//        totalOveral = 90;
//    }
//    if (rAssign.roomAssignmentRoomInspectionStatusId == ENUM_INSPECTION_COMPLETED_FAIL && totalOveral >= 90) {
//        totalOveral = 0;
//    }
    
    //update score checklist to database
    //set status checklist
//    if (totalOveral >= 90) {
//        chkModel.chkListStatus = tChkPass;
//    } else {
//        chkModel.chkListStatus = tChkFail;
//    }

    //set score checklist
    chkModel.chkCore = totalOveral;
    [manager updateCheckList:chkModel];
    
    //load checklist
//    [manager loadCheckListData:chkModel];
//#warning end
}

#pragma didden after save.
-(void) hiddenHUDAfterSaved:(MBProgressHUD *)HUD{
    [HUD hide:YES];
    [HUD removeFromSuperview];
}

#pragma mark - Handle Topbar Methods
-(void)loadTopbarView {
    TopbarViewV2 *topbar = [[TopbarViewV2 alloc] initViewWithSuperModel:[[CommonVariable sharedCommonVariable] superRoomModel]];
    [topbar setTag:tagTopbarView];
    [self.view addSubview:topbar];
    [topbar setHidden:YES];
    
    [topbar setAlpha:0.0];
    [self addButtonHandleShowHideTopbar];
}

-(void)showTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationWillStartSelector:@selector(setHiddenTopbarView)];
    
    f = topbar.frame;
    f.origin.y = 0;
    [topbar setFrame:f];
    [topbar setAlpha:1.0];
    
    [self adjustShowForViews];
    
    [UIView commitAnimations];
}

-(void)hideTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDidStopSelector:@selector(setHiddenTopbarView)];
    [UIView setAnimationDelegate:self];
    
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    [topbar setAlpha:0.0];
    
    [self adjustRemoveForViews];
    
    [UIView commitAnimations];
}

-(void)setHiddenTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    [topbar setHidden:!topbar.hidden];
}

-(TopbarViewV2 *)getTopBarView {
    return (TopbarViewV2 *)[self.view viewWithTag:tagTopbarView];
}

-(void)addButtonHandleShowHideTopbar {
    UIButton *titleBarButton = [[UIButton alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
    
    titleBarButton.titleLabel.backgroundColor = [UIColor clearColor];
    titleBarButton.titleLabel.font = FONT_BUTTON_TOPBAR;
    titleBarButton.titleLabel.textAlignment = UITextAlignmentCenter;
    titleBarButton.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
    [titleBarButton.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
    [titleBarButton.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
    [titleBarButton.titleLabel setMinimumFontSize:10];
    [titleBarButton.titleLabel setAdjustsFontSizeToFitWidth:YES];
    [titleBarButton addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    [titleBarButton setTitle:[self title] forState:UIControlStateNormal];
    self.navigationItem.titleView = titleBarButton;
    
    //Move title to center vertical
    CGRect titleViewBounds = self.navigationItem.titleView.bounds;
    int moveLeftTitle = 0;
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        moveLeftTitle = 10;
    }
    self.navigationItem.titleView.bounds = CGRectMake(titleViewBounds.origin.x + moveLeftTitle, Y_TOPBAR_TITLE, titleViewBounds.size.width, titleViewBounds.size.height);
    
}

-(void)topbarTapped:(UIButton *)sender {
    TopbarViewV2 *topbar = [self getTopBarView];
    [sender setEnabled:NO];
    if (topbar.isHidden) {
        [self showTopbarView];
    } else {
        [self hideTopbarView];
    }
    [self performSelector:@selector(topbarButtonEnable:) withObject:sender afterDelay:0.5];
}

-(void) topbarButtonEnable:(UIButton *) topbarBtn {
    [topbarBtn setEnabled:YES];
}

-(void)adjustShowForViews {
    //adjust all views when show 
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    NSInteger height = (datas.count + 2) * 30;
    
    [tbvChecklist setFrame:CGRectMake(0, f.size.height, 320, 387 - height - f.size.height)];
}

-(void)adjustRemoveForViews { 
    //adjust all views when hide
    NSInteger height = (datas.count + 2) * 30;
    
    [tbvChecklist setFrame:CGRectMake(0, 0, 320, 387 - height)];
}


@end
