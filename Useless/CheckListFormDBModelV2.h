//
//  CheckListFormDBModelV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 3/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CheckListFormDBModelV2 : NSObject{
    NSInteger chkFormId;
    NSString *chkFormName;
    NSString *chkFormLangName;
    NSInteger chkFormKind;
    NSInteger chkFormHotelId;
    NSInteger chkFormPossiblePoint;
    NSInteger chkFormChecklistId;
    
}
@property (nonatomic,readwrite) NSInteger chkFormId;
@property (nonatomic, strong) NSString *chkFormName;
@property (nonatomic, strong) NSString *chkFormLangName;
@property (nonatomic, readwrite) NSInteger chkFormKind;
@property (nonatomic, readwrite) NSInteger chkFormHotelId;
@property (nonatomic, readwrite) NSInteger chkFormPossiblePoint;
@property (nonatomic, readwrite) NSInteger chkFormChecklistId;


@end
