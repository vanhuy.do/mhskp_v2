//
//  GuideManagerV2.h
//  mHouseKeeping
//
//  Created by Nghia Truong on 3/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GuideModelV2.h"
#import "GuideAdapterV2.h"
#import "GuideImageModelV2.h"
#import "GuideImageAdapterV2.h"

@interface GuideManagerV2 : NSObject {
    
}

#pragma mark - Guide Model
-(int) insertGuideData:(GuideModelV2 *) guide;
-(int) updateGuideData:(GuideModelV2 *) guide;
-(int) deleteGuideData:(GuideModelV2 *) guide;
-(GuideModelV2 *) loadGuideData:(GuideModelV2 *) guide;
-(GuideModelV2 *) loadGuideDataByRoomSectionId:(GuideModelV2 *) guide;
-(GuideModelV2 *) loadGuideDataByRoomTypeId:(GuideModelV2 *) guide;
-(NSMutableArray *) loadAllGuide;
-(NSMutableArray *) loadAllGuideByRoomSectionId:(int) roomSectionId;
-(NSMutableArray *) loadAllGuideByRoomTypeId:(int) roomTypeId;

#pragma mark - Guide Image Model
-(int) insertGuideImageData:(GuideImageModelV2 *) image;
-(int) updateGuideImageData:(GuideImageModelV2 *) image;
-(int) deleteGuideImageData:(GuideImageModelV2 *) image;
-(GuideImageModelV2 *) loadGuideImageData:(GuideImageModelV2 *) image;
-(GuideImageModelV2 *) loadGuideImageDataByGuideId:(GuideImageModelV2 *) image;
-(NSMutableArray *) loadAllGuideImage;
-(NSMutableArray *) loadAllGuideImageByGuideId:(int) guideId;

@end
