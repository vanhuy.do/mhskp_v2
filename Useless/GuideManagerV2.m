//
//  GuideManagerV2.m
//  mHouseKeeping
//
//  Created by Nghia Truong on 3/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GuideManagerV2.h"

@implementation GuideManagerV2

#pragma mark - Guide Model
-(int) insertGuideData:(GuideModelV2 *) guide {
    GuideAdapterV2 *guideAdapter = [[GuideAdapterV2 alloc] init];
    [guideAdapter openDatabase];
    [guideAdapter resetSqlCommand];
    int result = [guideAdapter insertGuideData:guide];
    [guideAdapter close];
    [guideAdapter release];
    guideAdapter = nil;
    
    return result;
}

-(int) updateGuideData:(GuideModelV2 *) guide {
    GuideAdapterV2 *guideAdapter = [[GuideAdapterV2 alloc] init];
    [guideAdapter openDatabase];
    [guideAdapter resetSqlCommand];
    int result = [guideAdapter updateGuideData:guide];
    [guideAdapter close];
    [guideAdapter release];
    guideAdapter = nil;
    
    return result;
}

-(int) deleteGuideData:(GuideModelV2 *) guide {
    GuideAdapterV2 *guideAdapter = [[GuideAdapterV2 alloc] init];
    [guideAdapter openDatabase];
    [guideAdapter resetSqlCommand];
    int result = [guideAdapter deleteGuideData:guide];
    [guideAdapter close];
    [guideAdapter release];
    guideAdapter = nil;
    
    return result;
}

-(GuideModelV2 *) loadGuideData:(GuideModelV2 *) guide {
    GuideAdapterV2 *guideAdapter = [[GuideAdapterV2 alloc] init];
    [guideAdapter openDatabase];
    [guideAdapter resetSqlCommand]; 
    [guideAdapter loadGuideData:guide];
    [guideAdapter close];
    [guideAdapter release];
    guideAdapter = nil;
    
    return guide;
}

-(GuideModelV2 *) loadGuideDataByRoomSectionId:(GuideModelV2 *) guide {
    GuideAdapterV2 *guideAdapter = [[GuideAdapterV2 alloc] init];
    [guideAdapter openDatabase];
    [guideAdapter resetSqlCommand]; 
    [guideAdapter loadGuideDataByRoomSectionId:guide];
    [guideAdapter close];
    [guideAdapter release];
    guideAdapter = nil;
    
    return guide;
}

-(GuideModelV2 *) loadGuideDataByRoomTypeId:(GuideModelV2 *) guide {
    GuideAdapterV2 *guideAdapter = [[GuideAdapterV2 alloc] init];
    [guideAdapter openDatabase];
    [guideAdapter resetSqlCommand]; 
    [guideAdapter loadGuideDataByRoomTypeId:guide];
    [guideAdapter close];
    [guideAdapter release];
    guideAdapter = nil;
    
    return guide;
}

-(NSMutableArray *) loadAllGuide {
    GuideAdapterV2 *guideAdapter = [[GuideAdapterV2 alloc] init];
    [guideAdapter openDatabase];
    [guideAdapter resetSqlCommand];    
    NSMutableArray * results = [guideAdapter loadAllGuide];
    [guideAdapter close];
    [guideAdapter release];
    guideAdapter = nil;
    
    return results;
}

-(NSMutableArray *) loadAllGuideByRoomSectionId:(int) roomSectionId {
    GuideAdapterV2 *guideAdapter = [[GuideAdapterV2 alloc] init];
    [guideAdapter openDatabase];
    [guideAdapter resetSqlCommand];    
    NSMutableArray * results = [guideAdapter loadAllGuideByRoomSectionId:roomSectionId];
    [guideAdapter close];
    [guideAdapter release];
    guideAdapter = nil;
    
    return results;
}

-(NSMutableArray *) loadAllGuideByRoomTypeId:(int) roomTypeId {
    GuideAdapterV2 *guideAdapter = [[GuideAdapterV2 alloc] init];
    [guideAdapter openDatabase];
    [guideAdapter resetSqlCommand];    
    NSMutableArray * results = [guideAdapter loadAllGuideByRoomTypeId:roomTypeId];
    [guideAdapter close];
    [guideAdapter release];
    guideAdapter = nil;
    
    return results;
}

#pragma mark - Guide Image Model
-(int) insertGuideImageData:(GuideImageModelV2 *) image {
    GuideImageAdapterV2 *imageAdapter = [[GuideImageAdapterV2 alloc] init];
    [imageAdapter openDatabase];
    [imageAdapter resetSqlCommand];
    int result = [imageAdapter insertGuideImageData:image];
    [imageAdapter close];
    [imageAdapter release];
    imageAdapter = nil;
    
    return result;
}

-(int) updateGuideImageData:(GuideImageModelV2 *) image {
    GuideImageAdapterV2 *imageAdapter = [[GuideImageAdapterV2 alloc] init];
    [imageAdapter openDatabase];
    [imageAdapter resetSqlCommand];
    int result = [imageAdapter updateGuideImageData:image];
    [imageAdapter close];
    [imageAdapter release];
    imageAdapter = nil;
    
    return result;
}

-(int) deleteGuideImageData:(GuideImageModelV2 *) image {
    GuideImageAdapterV2 *imageAdapter = [[GuideImageAdapterV2 alloc] init];
    [imageAdapter openDatabase];
    [imageAdapter resetSqlCommand];
    int result = [imageAdapter deleteGuideImageData:image];
    [imageAdapter close];
    [imageAdapter release];
    imageAdapter = nil;
    
    return result;
}

-(GuideImageModelV2 *) loadGuideImageData:(GuideImageModelV2 *) image {
    GuideImageAdapterV2 *imageAdapter = [[GuideImageAdapterV2 alloc] init];
    [imageAdapter openDatabase];
    [imageAdapter resetSqlCommand]; 
    [imageAdapter loadGuideImageData:image];
    [imageAdapter close];
    [imageAdapter release];
    imageAdapter = nil;
    
    return image;
}

-(GuideImageModelV2 *) loadGuideImageDataByGuideId:(GuideImageModelV2 *) image {
    GuideImageAdapterV2 *imageAdapter = [[GuideImageAdapterV2 alloc] init];
    [imageAdapter openDatabase];
    [imageAdapter resetSqlCommand]; 
    [imageAdapter loadGuideImageDataByGuideId:image];
    [imageAdapter close];
    [imageAdapter release];
    imageAdapter = nil;
    
    return image;
}

-(NSMutableArray *) loadAllGuideImage {
    GuideImageAdapterV2 *imageAdapter = [[GuideImageAdapterV2 alloc] init];
    [imageAdapter openDatabase];
    [imageAdapter resetSqlCommand];    
    NSMutableArray * results = [imageAdapter loadAllGuideImage];
    [imageAdapter close];
    [imageAdapter release];
    imageAdapter = nil;
    
    return results;
}

-(NSMutableArray *) loadAllGuideImageByGuideId:(int) guideId {
    GuideImageAdapterV2 *imageAdapter = [[GuideImageAdapterV2 alloc] init];
    [imageAdapter openDatabase];
    [imageAdapter resetSqlCommand];    
    NSMutableArray * results = [imageAdapter loadAllGuideImageByGuideId:guideId];
    [imageAdapter close];
    [imageAdapter release];
    imageAdapter = nil;
    
    return results;
}

@end
