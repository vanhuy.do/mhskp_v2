//
//  InstructionCellV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 1/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol InstructionCellV2Delegate <NSObject>

@optional
-(void) btnInstructionCellPressedWithIndexPath:(NSIndexPath *)indexpath AndSender:(UIButton *) sender;


@end

@interface InstructionCellV2 : UITableViewCell {
    NSIndexPath *indexpath;
    __unsafe_unretained id <InstructionCellV2Delegate> delegate;
}

@property (nonatomic, strong) NSIndexPath *indexpath;
@property (strong, nonatomic) IBOutlet UIButton *btnInstruction;
@property (strong, nonatomic) IBOutlet UILabel *lblInstruction;
@property (assign, nonatomic) id<InstructionCellV2Delegate> delegate;

- (IBAction)btnInstructionPressed:(id)sender;

@end
