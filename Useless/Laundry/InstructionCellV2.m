//
//  InstructionCellV2.m
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 1/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "InstructionCellV2.h"

@implementation InstructionCellV2
@synthesize lblInstruction;
@synthesize btnInstruction;
@synthesize indexpath;
@synthesize delegate;

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


- (IBAction)btnInstructionPressed:(id)sender {
    if([delegate respondsToSelector:@selector(btnInstructionCellPressedWithIndexPath:AndSender:)]){
        [delegate btnInstructionCellPressedWithIndexPath:indexpath AndSender:sender];
        
    }
}
@end
