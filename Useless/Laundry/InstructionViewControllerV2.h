//
//  InstructionViewControllerV2.h
//  mHouseKeeping
//
//  Created by TMS on 5/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InstructionViewV2.h"

@interface InstructionViewControllerV2 : UIViewController {
}

@property (nonatomic, strong) InstructionViewV2 *instructionView;

-(void) setCaptionsView;

@end
