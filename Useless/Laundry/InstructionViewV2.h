//
//  InstructionViewV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 1/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InstructionCellV2.h"
#import "InstructionModelV2.h"
#import "LaundryItemViewV2.h"
#import "RemarkViewV2.h"
#import "MBProgressHUD.h"
#import "LanguageManagerV2.h"
#import "LaundryManagerV2.h"
@interface InstructionViewV2 : UIView <UITableViewDataSource, UITableViewDelegate, InstructionCellV2Delegate, RemarkViewV2Delegate, UITextViewDelegate, MBProgressHUDDelegate> {
    NSMutableArray *datas;
    __unsafe_unretained UIViewController *parentViewController;
    MBProgressHUD *HUD;
    NSString * remarkText;
    UITextView *tvinstruction;
    LaudryOrderModelV2 *instructionLaundryOrder;
    UITextView *txtRemark;
    UILabel *lblTitleInstruction;
    BOOL statusBtnInstructionPress;
}

@property (strong, nonatomic) MBProgressHUD *HUD;
@property (assign, nonatomic) UIViewController *parentViewController;
@property (strong, nonatomic) NSMutableArray *datas;
@property (strong, nonatomic) UITableView *tbvInstruction;
@property (strong, nonatomic) UILabel *lblInstructionTitle;
@property (strong, nonatomic) UILabel *lblRemark;
@property (strong, nonatomic) UIButton *btnIntructionView;
@property (strong, nonatomic) UIButton *btnRemark;
@property (strong, nonatomic) NSString * remarkText;
@property (strong, nonatomic) LaudryOrderModelV2 *instructionLaundryOrder;
@property (strong, nonatomic) IBOutletCollection(UITextView) NSArray *tvInstruction;
@property (assign, nonatomic) BOOL statusBtnInstructionPress;

- (IBAction) btnIntructionView:(id)sender;
-(void) loadDataInstruction;
-(void) saveData;
-(BOOL) checkInstructionObjectIsInInstructionOrderArrayWithInstructionModel:(LaundryIntructionModelV2 *)laundryInstructionModel inInstructionOrderArray:(NSMutableArray *)instructionOrders;
-(BOOL) isChangeInInstructionView;
-(void) setCaptionsView;
@end
