//
//  InstructionViewV2.m
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 1/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "InstructionViewV2.h"
#import "RemarkViewV2.h"
#import "RoomManagerV2.h"
#import "NetworkCheck.h"
#import "CustomAlertViewV2.h"
#define imgYesButton @"yes_bt.png"
#define imgNoButton @"no_bt.png"
#define imgBackgroundInstructionTitle @"top_gray.png"
#define imgRowGray @"gray_arrow_36x36.png"

@interface InstructionViewV2(privateMethods)

-(IBAction)btnRemark:(id)sender;

@end
@implementation InstructionViewV2
@synthesize tvInstruction;
@synthesize tbvInstruction;
@synthesize lblInstructionTitle;
@synthesize btnIntructionView;
@synthesize datas,btnRemark;
@synthesize lblRemark,remarkText;
@synthesize parentViewController;
@synthesize HUD,instructionLaundryOrder;
@synthesize statusBtnInstructionPress;

-(id)init {
    self = [super init];
    if (self) {
        //init controls
        UIImageView *imgInstructionTitle = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 35)];
        imgInstructionTitle.image = [UIImage imageNamed:imgBackgroundInstructionTitle];
        lblTitleInstruction = [[UILabel alloc] initWithFrame:CGRectMake(109, 10, 102, 21)];
        lblTitleInstruction.text = [[LanguageManagerV2 sharedLanguageManager] getInstruction];
        [lblTitleInstruction setFont:[UIFont fontWithName:@"Arial-BoldMT" size:17]];
        lblTitleInstruction.textAlignment = UITextAlignmentCenter;
        lblTitleInstruction.textColor = [UIColor colorWithRed:255 green:255 blue:255 alpha:1];
        [lblTitleInstruction setBackgroundColor:[UIColor clearColor]];
        
        UITableView *tbvInstructions = [[UITableView alloc] initWithFrame:CGRectMake(0, 45, 320, 277)];
        [tbvInstructions setBackgroundColor:[UIColor clearColor]];
        [tbvInstructions setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        [tbvInstructions setDelegate:self];
        [tbvInstructions setDataSource:self];
        self.tbvInstruction = tbvInstructions;
        
        UIButton *buttonInstruction = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [buttonInstruction setFrame:CGRectMake(20, 256, 280, 85)];
        [buttonInstruction setEnabled:NO];

        txtRemark = [[UITextView alloc] initWithFrame:CGRectMake(25, 260, 80, 73)];
        [txtRemark setText:[[LanguageManagerV2 sharedLanguageManager] getRemark]];
        [txtRemark setUserInteractionEnabled:NO];
        [txtRemark setTextColor:[UIColor colorWithRed:6.0/255 green:62.0/255 blue:127.0/255 alpha:1.0]];
        [txtRemark setFont:[UIFont fontWithName:@"Arial-BoldMT" size:15.0]];
        [txtRemark setEditable:NO];
        [txtRemark setBackgroundColor:[UIColor clearColor]];
        
        tvinstruction = [[UITextView alloc] initWithFrame:CGRectMake(25, 260, 250, 75)];

        [tvinstruction setDelegate:self];
//        [tvinstruction setEditable:NO];
        [tvinstruction setBackgroundColor:[UIColor clearColor]];
                
//        lblRemark=[[UILabel alloc] initWithFrame:CGRectMake(29, 255, 100, 30)];
//        [lblRemark setFont:[UIFont fontWithName:@"Arial-BoldMT" size:17]];
//    
//        lblRemark.textAlignment = UITextAlignmentCenter;
//        lblRemark.textColor = [UIColor blackColor];
//        [lblRemark setBackgroundColor:[UIColor clearColor]];
//        [lblRemark setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_remark_title]];
        
        btnRemark = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [btnRemark setFrame:CGRectMake(20, 256, 280, 85)];
        [btnRemark setAlpha:0.03];
        [btnRemark addTarget:self action:@selector(btnRemark:) 
                    forControlEvents:UIControlEventTouchUpInside];
        
        UIImageView *imgrow = [[UIImageView alloc] initWithFrame:CGRectMake(275, 259, 20, 20)];
        imgrow.image = [UIImage imageNamed:imgRowGray];
        
        [self addSubview:imgInstructionTitle];
        [self addSubview:lblTitleInstruction];
        [self addSubview:tbvInstructions];
        [self addSubview:buttonInstruction];
        [self addSubview:imgrow];
        [self addSubview:btnRemark];
        [self addSubview:txtRemark];
        [self addSubview:tvinstruction];
        [self setBackgroundColor:[UIColor colorWithWhite:0.3 alpha:0.4]];
    }
    
    return self;
}

-(void)loadDataInstruction{
    //enable flag to announce, is loading data
    [[HomeViewV2 shareHomeView] waitingLoadingData];
    
    
    if (!datas) {
        datas = [[NSMutableArray alloc] init];
    }
    
    NSMutableArray * laundryInstructionArray = nil;
    
    //get all instruction data to check if it had data, does not need to get data from service
    laundryInstructionArray = [[LaundryManagerV2 sharedLaundryManagerV2]  loadAllLaundryInstructionsModelV2];
    
    // check internet connection
    if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp] == NO) {
        //show no network
        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:imgYesButton], [UIImage imageNamed:imgNoButton], nil] title:[[LanguageManagerV2 sharedLanguageManager]getAlertTitle] message:[[LanguageManagerV2 sharedLanguageManager] getNoWifi] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles: nil];   
        
        [alert show];
        [HUD hide:YES];
        return;
    } else {
        if (laundryInstructionArray.count == 0) {
            //get lastestLastModified
            NSString *lastModified = [[LaundryManagerV2 sharedLaundryManagerV2] getTheLatestLastModifier];
            //get Instruction from WS
            [[LaundryManagerV2 sharedLaundryManagerV2]  getAllLaundryInstructionFromWSByUserId:[NSNumber numberWithInt:[[UserManagerV2 sharedUserManager] currentUser].userId] andModifiedDate:lastModified];
        }
    }
    
    if (laundryInstructionArray.count == 0) {
        laundryInstructionArray = [[LaundryManagerV2 sharedLaundryManagerV2]  loadAllLaundryInstructionsModelV2];
    }
    
    //get laundry orderModel
    NSInteger userID = [[UserManagerV2 sharedUserManager] currentUser].userId;
    NSDateFormatter *datetimeFormater = [[NSDateFormatter alloc] init];
    [datetimeFormater setDateFormat:@"yyyy-MM-dd"];
    NSString *presentTime = [datetimeFormater stringFromDate:[NSDate date]];
    NSInteger roomID = [TasksManagerV2 getCurrentRoomAssignment];
    
    self.instructionLaundryOrder = [[LaudryOrderModelV2 alloc] init];
    instructionLaundryOrder.laundryOrderRoomId = roomID;
    instructionLaundryOrder.laundryOrderUserId = userID;
    instructionLaundryOrder.laundryOrderCollectDate = presentTime;
    
    [[LaundryManagerV2 sharedLaundryManagerV2] loadLaundryOrdersModelByRoomIdUserIdAndCollectDate:instructionLaundryOrder];
    
    if (instructionLaundryOrder.laundryOrderId == 0) {
        //laundry order is not existing, insert laundry order
        instructionLaundryOrder = [[LaudryOrderModelV2 alloc] init];
        instructionLaundryOrder.laundryOrderCollectDate = presentTime;
        instructionLaundryOrder.lromStatus = createStatus;
        instructionLaundryOrder.laundryOrderUserId = userID;
        instructionLaundryOrder.laundryOrderRoomId = roomID;
        instructionLaundryOrder.laundryOrderStatus = tStatusChecked;
        
    } else {
        instructionLaundryOrder.lromStatus = inlocalDB;
        
        tvinstruction.text = instructionLaundryOrder.laundryOrderRemark;
        if ([tvinstruction.text isEqualToString:@""]) {
            [txtRemark setHidden:NO];
        } else
            [txtRemark setHidden:YES];
        
        remarkText = tvinstruction.text;
        
        NSMutableArray * checkedInstructionArray = nil;
        
        checkedInstructionArray = [[LaundryManagerV2 sharedLaundryManagerV2]  loadAllLaundryInstructionsOrderModelV2ID:instructionLaundryOrder.laundryOrderId];
        
        if ([checkedInstructionArray count] > 0) {
            if(!instructionLaundryOrder.laundryOrderInstructions)
            {
                instructionLaundryOrder.laundryOrderInstructions = [[NSMutableArray alloc] init];     
            }
            
            for(int i = 0; i < [laundryInstructionArray count]; i++){
                
                LaundryIntructionModelV2 * laundryInstructionModel = [laundryInstructionArray objectAtIndex:i];
                if([checkedInstructionArray count] > 0)
                {
                    for(int j = 0; j < [checkedInstructionArray count]; j++)
                    {
                        LaundryInstructionsOrderModelV2 * intructionOrder = [checkedInstructionArray objectAtIndex:j];
                        if(intructionOrder.instructionId == laundryInstructionModel.instructionId)
                        {
                            laundryInstructionModel.isChecked = YES;
                        }
                    }
                }
            }
            
            [instructionLaundryOrder.laundryOrderInstructions addObjectsFromArray:checkedInstructionArray];
        }
    } 
    
    [datas addObjectsFromArray:laundryInstructionArray];
    
    [tbvInstruction reloadData];
    
    //end loading data
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
}

#pragma mark - UITableView Delegate methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return datas.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 48;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *IdentifyCell = @"IdentifyCell";
    InstructionCellV2 *cell = nil;
    cell = [tableView dequeueReusableCellWithIdentifier:IdentifyCell];
    if (cell == nil) {
        NSArray *array = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([InstructionCellV2 class]) owner:self options:nil];
        cell = [array objectAtIndex:0];
    }
    //add data
    LaundryIntructionModelV2 *model = [datas objectAtIndex:indexPath.row];
    if ([[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString:(NSString *)ENGLISH_LANGUAGE]) {
        [cell.lblInstruction setText:model.instructionName];
    }else
    {
        [cell.lblInstruction setText:model.instructionNameLang];
    }
    
    if(model.isChecked == YES)
    {
        [cell.btnInstruction setBackgroundImage:[UIImage imageNamed:imginstructionOn] forState:UIControlStateNormal];
    }
    else
    {
        [cell.btnInstruction setBackgroundImage:[UIImage imageNamed:imgInstructionOff] forState:UIControlStateNormal];   
    }
    [cell setDelegate:self];
    [cell setIndexpath:indexPath];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

-(void)btnInstructionCellPressedWithIndexPath:(NSIndexPath *)indexpath AndSender:(UIButton *)sender{
    LaundryIntructionModelV2 *model = [datas objectAtIndex:indexpath.row];
    if(model.isChecked == NO)
    {
        statusBtnInstructionPress = YES;
        [sender setBackgroundImage:[UIImage imageNamed:imginstructionOn] forState:UIControlStateNormal];
        model.isChecked = YES;
    }
    else
    {
        statusBtnInstructionPress = NO;
        [sender setBackgroundImage:[UIImage imageNamed:imgInstructionOff] forState:UIControlStateNormal];
        model.isChecked = NO;
    }
}

- (IBAction)btnIntructionView:(id)sender {
}

-(IBAction)btnRemark:(id)sender{
    RemarkViewV2 * remark = [[RemarkViewV2 alloc] init];
    remark.delegate = self;
    [parentViewController.navigationController pushViewController:remark animated:YES];
    remark.txvRemark.text = remarkText;
    [remark.navigationItem setHidesBackButton:YES animated:NO];
}

-(void)saveData{ 
    NSInteger userId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    NSInteger total = 0;
    CGFloat subTotal = 0;
    
    //load all laundry service
    NSMutableArray *arrayService = [[LaundryManagerV2 sharedLaundryManagerV2] loadAllLaundryServicesModelV2];
    
    //load all laundry order
    NSMutableArray *arrayLaundry = [[LaundryManagerV2 sharedLaundryManagerV2] loadAllLaundryOrderByUserId:userId];
    
    //get total and subTotal
    for (LaudryOrderModelV2 *laundryOrderModel in arrayLaundry) {
        //load all laundry order detail by order id
        NSMutableArray *arrayPostLaundry = [[LaundryManagerV2 sharedLaundryManagerV2] loadAllLaundryOrderDetailsByOrderId:laundryOrderModel.laundryOrderId];
        
        for (LaundryOrderDetailModelV2 *orderDetail in arrayPostLaundry) {
            //calculator total and subTotal
            total += orderDetail.lod_quantity;
            subTotal += orderDetail.lod_price * orderDetail.lod_quantity;
        }
    }

    //delete laundry instruction order in database
    if ([instructionLaundryOrder.laundryOrderInstructions count] > 0 && instructionLaundryOrder.lromStatus == inlocalDB) {
        for (LaundryInstructionsOrderModelV2 *deleteInstructionOrderModel in instructionLaundryOrder.laundryOrderInstructions) {
            [[LaundryManagerV2 sharedLaundryManagerV2] deleteLaundryInstructionsOrderModelV2:deleteInstructionOrderModel];
        }
        [instructionLaundryOrder.laundryOrderInstructions removeAllObjects];
    }
    
    //laundry instruction order exist in local database
    if (instructionLaundryOrder.lromStatus == inlocalDB) {
        if (![instructionLaundryOrder.laundryOrderRemark isEqualToString:remarkText]) {
            instructionLaundryOrder.laundryOrderRemark = remarkText;
            //update laundry order when change remark
            [[LaundryManagerV2 sharedLaundryManagerV2] updateLaudryOrderModelV2:instructionLaundryOrder];  
            }
        for(LaundryIntructionModelV2 *createLaundryInstruction in datas) {
            if (createLaundryInstruction.isChecked == YES) {
                //insert laundry instruction order
                LaundryInstructionsOrderModelV2 *createLaundryInstructionOrderModel=[[LaundryInstructionsOrderModelV2 alloc] init];
                createLaundryInstructionOrderModel.instructionId = createLaundryInstruction.instructionId;
                createLaundryInstructionOrderModel.laundryOrderId = instructionLaundryOrder.laundryOrderId;
                [[LaundryManagerV2 sharedLaundryManagerV2] insertLaundryInstructionsOrderModelV2:createLaundryInstructionOrderModel];
                
                //Post instruction to WS
                for (LaundryServicesModelV2 *service in arrayService) {
                    //get laundry order id
                    NSInteger orderId = [[LaundryManagerV2 sharedLaundryManagerV2] postLaundryOrder:instructionLaundryOrder andsubQuantity:total andServiceID:service.lservices_id andHotel:[[[UserManagerV2 sharedUserManager] currentUser] userHotelsId] andsubTotal:subTotal];
                    
                    NSInteger laundryInstructionId = [[LaundryManagerV2 sharedLaundryManagerV2] postLaundryInstructionOrder:createLaundryInstructionOrderModel andLaundryOrder:orderId anduser:userId];
                    if (laundryInstructionId != 0) {
                        //post success, delete laundry instruction order in database
                        [[LaundryManagerV2 sharedLaundryManagerV2] deleteLaundryInstructionsOrderModelV2:createLaundryInstructionOrderModel];
                    }
                }
            }
        }
    }
    else if (instructionLaundryOrder.lromStatus == createStatus) {
        instructionLaundryOrder.laundryOrderRemark = remarkText;
        //insert laundry order
       int laundryOrder_Id = [[LaundryManagerV2 sharedLaundryManagerV2] insertLaudryOrderModelV2:instructionLaundryOrder];
        instructionLaundryOrder.lromStatus = inlocalDB;
        for(LaundryIntructionModelV2 *create_laundry_instruction in datas) {
            if (create_laundry_instruction.isChecked == YES) {
                //insert laundry instruction order
                LaundryInstructionsOrderModelV2 *create_Laundry_Instruction_Order_Model=[[LaundryInstructionsOrderModelV2 alloc] init];
                create_Laundry_Instruction_Order_Model.instructionId=create_laundry_instruction.instructionId;
                create_Laundry_Instruction_Order_Model.laundryOrderId=laundryOrder_Id;
                [[LaundryManagerV2 sharedLaundryManagerV2] insertLaundryInstructionsOrderModelV2:create_Laundry_Instruction_Order_Model];
                
                //Post instruction to WS
                for (LaundryServicesModelV2 *service in arrayService) {
                    //get laundry order id
                    NSInteger orderId = [[LaundryManagerV2 sharedLaundryManagerV2] postLaundryOrder:instructionLaundryOrder andsubQuantity:total andServiceID:service.lservices_id andHotel:[[[UserManagerV2 sharedUserManager] currentUser] userHotelsId] andsubTotal:subTotal];
                    
                    NSInteger laundryInstructionId = [[LaundryManagerV2 sharedLaundryManagerV2] postLaundryInstructionOrder:create_Laundry_Instruction_Order_Model andLaundryOrder:orderId anduser:userId];
                    if (laundryInstructionId != 0 ) {
                        //post success, delete laundry instruction order in database
                        [[LaundryManagerV2 sharedLaundryManagerV2] deleteLaundryInstructionsOrderModelV2:create_Laundry_Instruction_Order_Model];
                    }
                }
            }
            
        }
    }
}

#pragma mark - remark Delegate
-(void) remarkViewV2DoneWithText:(NSString *)text andController:(UIViewController *)controllerView 
{
    if ([text isEqualToString:@""]) {
        [txtRemark setHidden:NO];
    } else
        [txtRemark setHidden:YES];
    [tvinstruction setText:[NSString stringWithFormat:@"%@",text]];
    self.remarkText = text;
}

#pragma mark - UITextView Delegate methods
-(BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    [self btnRemark:nil];
    return NO;
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if([text isEqualToString:@"\n"]){
        [textView resignFirstResponder];
        return  NO;
    }
    return  YES;
}

#pragma mark- hidden after save.
-(void)hudWasHidden
{
    if (HUD != nil){
		[HUD removeFromSuperview];
    }
}

#pragma mark-check LaundryInstructionModel In LaundryIntructionOrderModel Array
-(BOOL)checkInstructionObjectIsInInstructionOrderArrayWithInstructionModel:(LaundryIntructionModelV2 *)laundryInstructionModel inInstructionOrderArray:(NSMutableArray *)instructionOrders
{
    BOOL isExist=NO;
    for (LaundryInstructionsOrderModelV2 *instructionModel in instructionOrders ) {
        if (laundryInstructionModel.instructionId == instructionModel.instructionId) {
            isExist = YES;
            break;
        }else
        {
            continue;
        }
    }
    return isExist;

}

-(void)pressBackBar{
  //check there is something need to be save
    if ([self isChangeInInstructionView]) {
        //show alert to save data
    }else
    {
        // exit to backBar.
    }
}

-(BOOL)isChangeInInstructionView
{
    BOOL isNeedToSaveInDatabase = NO;
    NSMutableArray *selectedArray = [[NSMutableArray alloc] init];
    for (LaundryIntructionModelV2 * lr_instruction_model in datas) {
        if (lr_instruction_model.isChecked) {
            LaundryIntructionModelV2 *seleted_lr_instruction_model = lr_instruction_model;
            [selectedArray addObject:seleted_lr_instruction_model];
        }
    }
    //check instruction_lr_order_object.lromStatus==inlocalDB
    if (instructionLaundryOrder.lromStatus == inlocalDB) {
        if ([selectedArray count] == [instructionLaundryOrder.laundryOrderInstructions count] && [selectedArray count]>0) {
            //check all member of selectedArray is the same as in local database
            for (LaundryIntructionModelV2 *lr_instruction_model in selectedArray) {
                if ([self checkInstructionObjectIsInInstructionOrderArrayWithInstructionModel:lr_instruction_model inInstructionOrderArray:instructionLaundryOrder.laundryOrderInstructions]) {
                    
                }
                else{
                    isNeedToSaveInDatabase=YES;
                }
            }
        }
        if (![remarkText isEqualToString:instructionLaundryOrder.laundryOrderRemark]) {
            isNeedToSaveInDatabase=YES;
        }
    }
    else if (instructionLaundryOrder.lromStatus == createStatus){
        if ([selectedArray count] > 0) {
            isNeedToSaveInDatabase = YES;
        }
    }
    
    if (statusBtnInstructionPress == YES) {
        isNeedToSaveInDatabase = YES;
    }
    
    return isNeedToSaveInDatabase;
}

#pragma mark - === Set Captions ===
#pragma mark
-(void)setCaptionsView {
    [txtRemark setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_remark_title]];
    [lblTitleInstruction setText: [[LanguageManagerV2 sharedLanguageManager] getInstruction]];
}

@end
