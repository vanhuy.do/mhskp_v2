//
//  LaundryCartCellV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol LaundryCartCellV2Delegate <NSObject>

@optional
-(void) btnQtyLaundryCartCellPressedWithIndexpath:(NSIndexPath *) indexpath sender:(UIButton *) button;

@end


@interface LaundryCartCellV2 : UITableViewCell{
     id<LaundryCartCellV2Delegate> delegate;
    NSIndexPath *indexpath;
}
@property (strong, nonatomic) id<LaundryCartCellV2Delegate> delegate;
@property (strong, nonatomic) NSIndexPath *indexpath;
@property (strong, nonatomic) IBOutlet UIImageView *imgLaundryCartCell;
@property (strong, nonatomic) IBOutlet UILabel *lblNameLaundryCartCell;
@property (strong, nonatomic) IBOutlet UILabel *lblQtyLaundryCartCell;
@property (strong, nonatomic) IBOutlet UIButton *btnQtyLaundryCartCell;
@property (strong, nonatomic) IBOutlet UILabel *lblSubtotalCostLaundry;
@property (strong, nonatomic) IBOutlet UIButton *btnSubtotalCostLaundry;
- (IBAction)btnQtyLaundryCartCellPressed:(id)sender;
@end
