//
//  LaundryCartCellV2.m
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LaundryCartCellV2.h"

@implementation LaundryCartCellV2
@synthesize imgLaundryCartCell;
@synthesize lblNameLaundryCartCell;
@synthesize lblQtyLaundryCartCell;
@synthesize btnQtyLaundryCartCell;
@synthesize lblSubtotalCostLaundry;
@synthesize btnSubtotalCostLaundry;
@synthesize indexpath;
@synthesize  delegate;

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)btnQtyLaundryCartCellPressed:(id)sender {
    if([delegate respondsToSelector:@selector(btnQtyLaundryCartCellPressedWithIndexpath:sender:)])
    {
        [delegate btnQtyLaundryCartCellPressedWithIndexpath:indexpath sender:sender];
    }
}
@end
