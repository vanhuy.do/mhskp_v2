//
//  LaundryCartViewV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LaundryCartCellV2.h"
#import "SectionLaundryInfoV2.h"
#import "LaundryItemModelV2.h"
#import "SectionLaundryCartInfoV2.h"
#import "SectionLaundryCartViewV2.h"
#import "PickerViewV2.h"
#import "LaundryManagerV2.h"
#import "LaudryOrderModelV2.h"
#import "TasksManagerV2.h"


@protocol LaundryCartViewV2Delegate <NSObject>
@optional
-(void)transferCartFromCartView:(LaudryOrderModelV2 *)lr_Order_Model;
@end
@interface LaundryCartViewV2 : UIViewController<UITableViewDataSource,UITableViewDelegate,LaundryCartCellV2Delegate, SectionLaundryCartViewV2Delegate, PickerViewV2Delegate>{
    
    int total;
    CGFloat subTotal;
    CGFloat subTotalCost;
    CGFloat firstItem;
    CGFloat secondItem;
    BOOL flag;
    BOOL statusChooseSave;
    LaundryCategoryModelV2 * laundryCatagory;
    LaudryOrderModelV2 *laundry_order_Object;
    __unsafe_unretained id <LaundryCartViewV2Delegate>delegate;
}
@property (nonatomic)int total;
@property (nonatomic) CGFloat subTotal;
@property (nonatomic) CGFloat subTotalCost;
@property (nonatomic) CGFloat firstItem;
@property (nonatomic) CGFloat secondItem;
@property (nonatomic,assign)  id <LaundryCartViewV2Delegate> delegate;
@property (strong,nonatomic)LaudryOrderModelV2 *laundry_order_Object;
@property (strong,  nonatomic)LaundryCategoryModelV2 * laundryCatagory;
@property (strong,  nonatomic) IBOutlet LaundryCartCellV2 *laundryCartCell;
@property (strong, nonatomic) IBOutlet UITableView *tbvLaundryCart;
@property (strong, nonatomic) SectionLaundryCartInfoV2 *sectionLaundryCartInfo;
@property (strong, nonatomic) NSString *titleSectionLaundryCart;
@property (nonatomic, strong) NSMutableArray *dataLaundryCarts;
@property (strong, nonatomic) IBOutlet UILabel *lblSubtotal;
@property (strong, nonatomic) IBOutlet UILabel *lblPriceSubtotal;
@property (strong, nonatomic) IBOutlet UILabel *lblServiceCharge;
@property (strong, nonatomic) IBOutlet UILabel *lblPriceServiceCharge;
@property (strong, nonatomic) IBOutlet UILabel *lblExpressCharge;
@property (strong, nonatomic) IBOutlet UILabel *lblPriceExpressCharge;
@property (strong, nonatomic) IBOutlet UILabel *lblTotal;
@property (strong, nonatomic) IBOutlet UILabel *lblNumberTotal;
@property (strong, nonatomic) IBOutlet UILabel *lblTotalCost;
@property (strong, nonatomic) IBOutlet UILabel *lblPriceTotalCost;
@property (assign, nonatomic) NSIndexPath *rowIndexPath;
@property (strong, nonatomic) NSString *msgbtnYes;
@property (strong, nonatomic) NSString *msgbtnNo;
@property (strong, nonatomic) NSString *msgSaveCount;
@property (strong, nonatomic) NSString *msgDiscardCount;
@property (assign, nonatomic) BOOL statusSave;
@property (assign, nonatomic) BOOL statusChooseSave;
@property (assign, nonatomic) NSInteger laundryService;

-(void) setCaptionView;
//-(void) setCategory:(tCategoryLaundry) tcate AndGender:(tGender) gender;
-(void)loadDataForCartView;
@end
