 //
//  LaundryCartViewV2.m
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LaundryCartViewV2.h"
#import "LaundryItemPriceModelV2.h"
#import "LaundryOrderDetailModelV2.h"
#import "LaudryOrderModelV2.h"
#import "UserManagerV2.h"
#import "RoomManagerV2.h"
#import "MBProgressHUD.h"
#import "CustomAlertViewV2.h"
#import "MyNavigationBarV2.h"
#import "DeviceManager.h"

#define imgSectionCartExpress @"laundry_cart_express.png"
#define imgSectionCartRegular @"laundry_cart_regular.png"
#define tagSaveLeaving 10
#define tagAlertNo 11
#define tagAlertBottomBar 12
#define tagAlertDispatch 13
#define tagAlertDisCard 14
#define tagTopbarView 1234
#define imgYesButton @"yes_bt.png"
#define imgNoButton @"no_bt.png"
@interface LaundryCartViewV2(privatemethods)

-(void) loadTopbarView;
-(void) showTopbarView;
-(void) hideTopbarView;
-(void) setHiddenTopbarView;
-(void) adjustShowForViews;
-(void) adjustRemoveForViews;
-(void) addButtonHandleShowHideTopbar;
-(void) topbarTapped:(UIButton *) sender;
-(TopbarViewV2 *) getTopBarView;
-(void)saveLaundryCart:(BOOL) postServer;
@end


@implementation LaundryCartViewV2
@synthesize laundryCartCell;
@synthesize dataLaundryCarts;
@synthesize lblSubtotal;
@synthesize lblPriceSubtotal;
@synthesize lblServiceCharge;
@synthesize lblPriceServiceCharge;
@synthesize lblExpressCharge;
@synthesize lblPriceExpressCharge;
@synthesize lblTotal;
@synthesize lblNumberTotal;
@synthesize lblTotalCost;
@synthesize lblPriceTotalCost;
@synthesize tbvLaundryCart;
@synthesize sectionLaundryCartInfo;
@synthesize titleSectionLaundryCart;
@synthesize rowIndexPath,laundryCatagory;
@synthesize msgbtnNo,msgbtnYes,msgSaveCount,msgDiscardCount,statusSave;
@synthesize statusChooseSave;
@synthesize laundryService;
@synthesize laundry_order_Object,delegate;
@synthesize total,subTotal,subTotalCost,firstItem,secondItem;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

-(void)viewWillAppear:(BOOL)animated{
    [self setCaptionView];
    
    //show wifi view
    [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationShowWifiView] object:nil];
    [super viewWillAppear:animated];

    [self loadDataForCartView];
    [tbvLaundryCart reloadData];
}

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    //Resize content views of current VC to fit with Navigation bar and tabbar
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    // Do any additional setup after loading the view from its nib.
    //[self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:imgBackground]]];

    //[self loadDataForCartView];
    
    statusChooseSave=NO;
    //back button navigation
    self.navigationItem.leftBarButtonItem = [MyNavigationBarV2 createBackButtonWithTarget:self selector:@selector(backBarPressed) forControlEvents:UIControlEventTouchUpInside];

    [self performSelector:@selector(loadTopbarView)];

    
}

- (void)viewDidUnload
{
 
    [self setTbvLaundryCart:nil];
    [self setLblSubtotal:nil];
    [self setLblPriceSubtotal:nil];
    [self setLblServiceCharge:nil];
    [self setLblPriceServiceCharge:nil];
    [self setLblExpressCharge:nil];
    [self setLblPriceExpressCharge:nil];
    [self setLblTotal:nil];
    [self setLblNumberTotal:nil];
    [self setLblTotalCost:nil];
    [self setLblPriceTotalCost:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    [self performSelector:@selector(loadTopbarView)];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}



#pragma mark - UITableView Delegate methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return dataLaundryCarts.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    SectionLaundryCartInfoV2 *sectioninfo = [dataLaundryCarts objectAtIndex:section];
    NSInteger numberRowInSection = sectioninfo.rowHeights.count;
    return numberRowInSection;
    //return sectioninfo.open ? numberRowInSection : 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 60;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    SectionLaundryCartInfoV2 *sectioninfo = [dataLaundryCarts objectAtIndex:section];

    if (sectioninfo.headerView == nil) {
    
        NSString *titleSection;
        NSData *imgSectionCart;
        
        //set language for laundry category name
        NSString *nameLaundryCategory = @"";
        if ([[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString: (NSString *)ENGLISH_LANGUAGE] == NO)
        {
            nameLaundryCategory = sectioninfo.laundryCategory.laundryCategoryNameLang;
        }
        else{
            nameLaundryCategory = sectioninfo.laundryCategory.laundryCategoryName;
        }
            
       if(sectioninfo.laundryService.lservices_id == 2)
        {
            imgSectionCart = UIImagePNGRepresentation([UIImage imageNamed:imgSectionCartExpress]);
            titleSectionLaundryCart = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_express];
             titleSection = [NSString stringWithFormat:@"%@ - %@",titleSectionLaundryCart,nameLaundryCategory];
            
        }
        else
        {
            imgSectionCart = UIImagePNGRepresentation([UIImage imageNamed:imgSectionCartRegular]);
            titleSectionLaundryCart = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_regular];
            titleSection = [NSString stringWithFormat:@"%@ - %@",titleSectionLaundryCart,nameLaundryCategory];
        }
        
        
        SectionLaundryCartViewV2 *sviewcart = [[SectionLaundryCartViewV2 alloc] initWithSection:sectioninfo.laundryCategory.laundryCategoryId contentImg:imgSectionCart contentName:titleSection];
        
        sectioninfo.headerView = sviewcart;
        [sviewcart setDelegate:self];
    }
    return sectioninfo.headerView;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
   
    rowIndexPath = indexPath;
    
    static NSString *identifyCell = @"IdentifyCell";
    LaundryCartCellV2 *cell = nil;
    cell = [tableView dequeueReusableCellWithIdentifier:identifyCell];
    if(cell == nil)
    {
        NSArray *array = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([LaundryCartCellV2 class]) owner:self options:nil];
        cell = [array objectAtIndex:0];
    }
    //config datas for cell
  
    SectionLaundryCartInfoV2 *sectionInfo = [dataLaundryCarts objectAtIndex:indexPath.section];
    LaundryItemModelV2 *model = [sectionInfo.rowHeights objectAtIndex:indexPath.row];
    
    [cell.imgLaundryCartCell setImage:[UIImage imageWithData:model.laundryItemImg]];
    
    //set language name laundry item
    if ([[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString: (NSString *)ENGLISH_LANGUAGE] == NO){
        cell.lblNameLaundryCartCell.text = model.laundryItemLang;

    }
    else{
        cell.lblNameLaundryCartCell.text = model.laundryItemName;
    }
    
    //set language for quantity
    [cell.lblQtyLaundryCartCell setText:[[LanguageManagerV2 sharedLanguageManager] getQuantity]];
    [cell.btnQtyLaundryCartCell setTitle:[NSString stringWithFormat:@"%d",model.laundryItem_Quantity] forState:UIControlStateNormal];

    //set language for subtotal cost
    [cell.lblSubtotalCostLaundry setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_subtotal_cost]];
    [cell.btnSubtotalCostLaundry setTitle:[NSString stringWithFormat:@"$ %.0f",model.laundryItem_Price] forState:UIControlStateNormal];
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell setBackgroundColor:[UIColor clearColor]];
    [cell setDelegate:self];
    [cell setIndexpath:indexPath];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}


-(void)btnQtyLaundryCartCellPressedWithIndexpath:(NSIndexPath *)indexpath sender:(UIButton *)button{
    
    statusChooseSave = YES;
    NSMutableArray *array = [NSMutableArray array];
    for (NSInteger index =0; index <= 10; index++) {
        [array addObject:[NSString stringWithFormat:@"%d", index]];
    }
    SectionLaundryCartInfoV2 *sectionInfo = [dataLaundryCarts objectAtIndex:indexpath.section];
    LaundryItemModelV2 *model = [sectionInfo.rowHeights objectAtIndex:indexpath.row];
    if (model.laundryItem_Quantity>10) {
        [array addObject:[NSString stringWithFormat:@"%d", model.laundryItem_Quantity]];
    }
    PickerViewV2 *picker = [[PickerViewV2 alloc] initPickerViewV2WithDatas:array AndSelectedData:button.titleLabel.text AndIndex:indexpath];
    [picker showPickerViewV2];
    [picker setDelegate:self];
    
}

-(void)didChoosePickerViewV2WithData:(NSString *)data AndIndex:(NSIndexPath *)index{
    total = 0;
    subTotal = 0;
    LaundryManagerV2 *manager = [[LaundryManagerV2 alloc] init];
    SectionLaundryCartInfoV2 *sectionInfo = [dataLaundryCarts objectAtIndex:index.section];
    LaundryItemModelV2 *model = [sectionInfo.rowHeights objectAtIndex:index.row];
    model.laundryItem_Quantity = [data integerValue];
    
    for (NSInteger indexSection=0; indexSection < dataLaundryCarts.count; indexSection ++) {
        SectionLaundryCartInfoV2 *sectionCart = [dataLaundryCarts objectAtIndex:indexSection];
        for (NSInteger indexRow=0; indexRow < sectionCart.rowHeights.count; indexRow++) {
            LaundryItemModelV2 *model = [sectionCart.rowHeights objectAtIndex:indexRow];
            
            //load laundry cart temp from database
            LaundryOrderDetailModelV2 *orderTemp = [[LaundryOrderDetailModelV2 alloc] init];
            orderTemp.lod_item_id = model.laundryItemId;
            orderTemp.lod_category_id = sectionCart.laundryCategory.laundryCategoryId;
            orderTemp.lod_service_id = sectionCart.laundryService.lservices_id;
            [manager loadLaundryOrderDetailsTemp:orderTemp];            

            
            //load laundry order detail
            LaundryOrderDetailModelV2 *orderDetail = [[LaundryOrderDetailModelV2 alloc] init];
            orderDetail.lod_item_id = model.laundryItemId;
            orderDetail.lod_category_id = sectionCart.laundryCategory.laundryCategoryId;
            orderDetail.lod_service_id = sectionCart.laundryService.lservices_id;
            [manager loadLaundryOrderDetails:orderDetail];
            
            if (model.laundryItem_Quantity > 0) {
                
                NSInteger sumItem = orderTemp.lod_quantity + orderDetail.lod_quantity;
                if (sumItem > 0) {
                    //ordertemp + orderdetail did have ==> can edit
                    if (sumItem != model.laundryItem_Quantity) {
                        
                        //remove order
                        [manager deleteLaundryOrderDetailModelV2:orderDetail];
                        
                        //update to laundry cart temp
                        orderTemp.lod_quantity = model.laundryItem_Quantity;
                        
                                                
                        BOOL result = [manager insertLaundryOrderDetailTemp:orderTemp];   
                        
                        if (result == NO) {
                            [manager updateLaundryOrderDetailTemp:orderTemp];
                        }
                        
                        //load item price for laundry item
                        if (model.laundryItem_Price == 0 || orderTemp.lod_price == 0) {
                            LaundryItemPriceModelV2 *laundryItemPrice = nil;
                            laundryItemPrice = [manager loadLaundryItemPriceModelByLaundryItemId:model.laundryItemId andLaundryCategoryId:sectionInfo.laundryCategory.laundryCategoryId];
                            orderTemp.lod_price = laundryItemPrice.lip_price;
                        }
                        
                        model.laundryItem_Price = model.laundryItem_Quantity * orderTemp.lod_price;
                                            
                    }
                } else {
                    if (sumItem == 0) {
                        //sumItem == 0 ==> orderDetail & orderTemp does not contain this item
                        //insert this item to orderTemp
                        orderTemp.lod_quantity = model.laundryItem_Quantity;
                        orderTemp.lod_price = model.laundryItem_Price;
                        [manager insertLaundryOrderDetailTemp:orderTemp];
                    }
                }
                //set total & subtotal
                total += model.laundryItem_Quantity;
                subTotal += model.laundryItem_Price;
            }
            else
            {
                //remove order
                [manager deleteLaundryOrderDetailModelV2:orderDetail];
                
                //remove temp
                [manager deleteLaundryOrderDetailTemp:orderTemp];
            }
        }
    }
    
    [lblNumberTotal setText:[NSString stringWithFormat:@"%d",total]];
    [lblPriceSubtotal setText:[NSString stringWithFormat:@"$%.0f",subTotal]];
    [lblPriceTotalCost setText:[NSString stringWithFormat:@"$%.0f",subTotal]];

    [tbvLaundryCart reloadData];

    
}

#pragma mark - Click back
-(void)backBarPressed {
    
     [self.navigationController popViewControllerAnimated:YES];

}

#pragma mark - === Handle Check Save Data ===
#pragma mark
-(BOOL) isCheckLaundryOrderTemp
{
    NSMutableArray *array = [[LaundryManagerV2 sharedLaundryManagerV2] loadAllLaundryOrderDetailModelV2Temp];
    if (array.count > 0) {
        return YES;
    }
    else
    {
        return NO;
    }
}
-(BOOL) checkSaveDataWithTagEvent:(NSInteger) tagOfEvent {
    
    BOOL isCheckOrder = [self isCheckLaundryOrderTemp];
    if (tagOfEvent == tagOfMessageButton) {
        if (isCheckOrder == NO) {
            return NO;
        }
        else
        {
            //Show alert please save before leaving
            UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:msgSaveCount delegate:self cancelButtonTitle:msgbtnYes otherButtonTitles:msgbtnNo, nil]; 
            alert.delegate = self;
            alert.tag = tagAlertBottomBar;
            [alert show];
            
            return YES;

        }
    }
    else
    {
        //Show alert complete room
        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"],  nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_complete_room_to_leave] delegate:self cancelButtonTitle:msgbtnYes otherButtonTitles: nil]; 
        alert.delegate = self;
        [alert show];
        
        return YES;

    }
    
}

#pragma mark - UIAlertView Delegate Methods
-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    switch (alertView.tag) {
        case tagAlertBottomBar:
        {
            switch (buttonIndex)
            {
                case 0:
                {
                    //YES: show alert dispatch for pickup
                    UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:imgYesButton], [UIImage imageNamed:imgNoButton], nil] title:[[LanguageManagerV2 sharedLanguageManager]getAlertTitle] message:[[LanguageManagerV2 sharedLanguageManager] getMsg_Dispatch_This_For_PickUp] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles:  [[LanguageManagerV2 sharedLanguageManager] getNo], nil];   
                    [alert setTag:tagAlertDispatch];
                    [alert show];      
                }
                    break;
                case 1:
                {
                    //NO: show alert discart all input
                    UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:imgYesButton], [UIImage imageNamed:imgNoButton], nil] title:[[LanguageManagerV2 sharedLanguageManager]getAlertTitle] message:[[LanguageManagerV2 sharedLanguageManager] getMsgAlertDiscardCounts] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles:  [[LanguageManagerV2 sharedLanguageManager] getNo], nil];   
                    [alert setTag:tagAlertDisCard];
                    [alert show];                    
                    
                }
                default:
                    break;
            }
        }
            
            break;
        case tagAlertDispatch:
        {
            switch (buttonIndex)
            {
                case 0:
                {
                    //YES: in dispatch for pickup will save data & post to server
                    [self saveLaundryCart:YES];
                    statusSave = YES;
                    
                    //if tag is message
                    if ([[NSUserDefaults standardUserDefaults] integerForKey:TAG_OF_EVENT] == tagOfMessageButton) {
                        [[HomeViewV2 shareHomeView] handleTabbarEventWithTagOfButton:[[NSUserDefaults standardUserDefaults] integerForKey:TAG_OF_EVENT]];
                    }
                }
                    break;
                case 1:
                {
                    //NO: in dispatch for pickup will save data but post to server
                    [self saveLaundryCart:NO];
                    statusSave = YES;
                    
                    //if tag is message
                    if ([[NSUserDefaults standardUserDefaults] integerForKey:TAG_OF_EVENT] == tagOfMessageButton) {
                        [[HomeViewV2 shareHomeView] handleTabbarEventWithTagOfButton:[[NSUserDefaults standardUserDefaults] integerForKey:TAG_OF_EVENT]];
                    }
                }
                default:
                    break;
            }
        }
            
            break;
        case tagAlertDisCard:
        {
            switch (buttonIndex)
            {
                case 0:
                {
                    //YES: in discard all data will go out, clear cart temp
                    [[LaundryManagerV2 sharedLaundryManagerV2] deleteAllLaundryOrderDetailTempData];
                    
                    //if tag is message
                    if ([[NSUserDefaults standardUserDefaults] integerForKey:TAG_OF_EVENT] == tagOfMessageButton) {
                        [[HomeViewV2 shareHomeView] handleTabbarEventWithTagOfButton:[[NSUserDefaults standardUserDefaults] integerForKey:TAG_OF_EVENT]];
                    }
                    else{
                        //back to count menu, no save
                        NSArray *arrayController = [self.navigationController viewControllers];
                        for (UIViewController *viewController in arrayController) {
                            if ([viewController isKindOfClass:[CountViewV2 class]]) {
                                [self.navigationController popToViewController:viewController animated:YES];
                                break;
                            }
                        }
                    }

                }
                    break;
                case 1:
                {
                    //NO: in discart all data will go cart screen
                    //do nothing.
                    
                }
                default:
                    break;
            }
        }
            
            break;
            
        default:
            break;
    }
}

#pragma mark - Save Laundry
-(void)saveData{
    
    statusChooseSave = NO;
    statusSave = YES;
    BOOL isCheckOrder = [self isCheckLaundryOrderTemp];
    if (isCheckOrder == YES) {
        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:imgYesButton], [UIImage imageNamed:imgNoButton], nil] title:[[LanguageManagerV2 sharedLanguageManager]getAlertTitle] message:[[LanguageManagerV2 sharedLanguageManager] getMsg_Dispatch_This_For_PickUp] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles:  [[LanguageManagerV2 sharedLanguageManager] getNo], nil];   
        [alert setTag:tagAlertDispatch];
        [alert show];

    }    
}

-(void)saveLaundryCart:(BOOL) postServer
{
    // @ show loading data .
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getRaSavingData]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES]; 
    
    ///Save laundry cart
    LaundryManagerV2 *manager = [[LaundryManagerV2 alloc] init];
    SyncManagerV2 *synManager = [[SyncManagerV2 alloc] init];
    NSInteger userId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    NSInteger hotelId = [[[UserManagerV2 sharedUserManager] currentUser] userHotelsId];
    NSInteger roomAssignId = [TasksManagerV2 getCurrentRoomAssignment];
    
    //load order in laundry
    LaudryOrderModelV2 *laundryOrder = [[LaudryOrderModelV2 alloc] init];
    laundryOrder.laundryOrderUserId = userId;
    laundryOrder.laundryOrderRoomId = roomAssignId;
    laundryOrder.laundryOrderStatus = tStatusChecked;
    laundryOrder.laundryOrderCollectDate = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd"]; 
    
    BOOL isCheckLaundryOrder = [manager isCheckLaundryOrder:laundryOrder];
    
    if (isCheckLaundryOrder == FALSE) {
        [manager insertLaudryOrderModelV2:laundryOrder];
    }
    
    //load laundry order
    [manager loadLaundryOrdersModelByRoomIdUserIdAndCollectDate:laundryOrder];
    
    
    //load all laundry service
    NSMutableArray *arrayService = [manager loadAllLaundryServicesModelV2];
    
    for (LaundryServicesModelV2 *service in arrayService) {
        
        //--------------------get laundry order Id--------------------
        int orderIdWS = 0;
        if (postServer == YES) {
            orderIdWS = [synManager postLaundryOrder:laundryOrder andsubQuantity:total andServiceID:service.lservices_id andHotel:hotelId andsubTotal:subTotal];
        }
        //--------------------WS--------------------------------------
        
        //load all laundry order detail temp by service id
        NSMutableArray *arrayLaundryOrderDetailTemp = [manager loadAllLaundryOrderDetailsTempByServiceId:service.lservices_id];
        
        if (arrayLaundryOrderDetailTemp.count > 0) {
            
            for (LaundryOrderDetailModelV2 *orderTemp in arrayLaundryOrderDetailTemp) {
                
                //load laundry order detail by order id
                LaundryOrderDetailModelV2 *orderDetail = [[LaundryOrderDetailModelV2 alloc] init];
                orderDetail.lod_item_id = orderTemp.lod_item_id;
                orderDetail.lod_service_id = orderTemp.lod_service_id;
                orderDetail.lod_category_id = orderTemp.lod_category_id;
                orderDetail.lod_laundry_order_id = laundryOrder.laundryOrderId;
                
                [manager loadLaundryOrderDetailsByItemId:orderDetail AndOrderId:laundryOrder.laundryOrderId];

                if (orderDetail.lod_id != 0) {
                    
                    //update Order detail in laundry
                    orderDetail.lod_quantity = orderTemp.lod_quantity + orderDetail.lod_quantity;
                    
                    [manager updateLaundryOrderDetailModelV2:orderDetail];
                }
                else
                {
                    //insert Order detail in laundry
                    LaundryOrderDetailModelV2 *laundryOrderDetailModel = [[LaundryOrderDetailModelV2 alloc] init];
                    laundryOrderDetailModel.lod_item_id = orderTemp.lod_item_id;
                    laundryOrderDetailModel.lod_service_id = orderTemp.lod_service_id;
                    laundryOrderDetailModel.lod_category_id = orderTemp.lod_category_id;
                    laundryOrderDetailModel.lod_post_status = POST_STATUS_SAVED_UNPOSTED;
                    laundryOrderDetailModel.lod_quantity = orderTemp.lod_quantity;
                    laundryOrderDetailModel.lod_laundry_order_id = laundryOrder.laundryOrderId;
                    
                    [manager insertLaundryOrderDetailModelV2:laundryOrderDetailModel];
                    
                }
                
                //--------------------------post to server WS-----------------
                if (postServer == YES) {
                    
                    LaundryOrderDetailModelV2 *orderDetailPost = [[LaundryOrderDetailModelV2 alloc] init];
                    orderDetailPost.lod_item_id = orderTemp.lod_item_id;
                    orderDetailPost.lod_service_id = orderTemp.lod_service_id;
                    orderDetailPost.lod_category_id = orderTemp.lod_category_id;
                    orderDetailPost.lod_laundry_order_id = laundryOrder.laundryOrderId;
                    
                    [manager loadLaundryOrderDetailsByItemId:orderDetailPost AndOrderId:laundryOrder.laundryOrderId];
                    BOOL statusPost = [synManager postLaundryItem:orderDetailPost andLaundryOrder:orderIdWS anduser:userId andTransactionTime:laundryOrder.laundryOrderCollectDate];
                    
                    if (statusPost == YES) {
                        //post success, clear database
                        [manager deleteLaundryOrderDetailModelV2:orderDetailPost];
                        
                    }

                }
                
                //delete laundry order detail temp
                [manager deleteLaundryOrderDetailTemp:orderTemp];
                
                [tbvLaundryCart reloadData];
                //--------------------------End------------------------------
            }
        }
        
    }
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getMsgSaveMessage]];
    //hide saving data.
    [self performSelector:@selector(hiddenHUDAfterSaved:) withObject:HUD afterDelay:0.5];

}

#pragma mark - Load Laundry Cart
-(void) addLaundrytoSectionWithStatus:(BOOL) status LaundryItem: (LaundryOrderDetailModelV2 *) item AndSection:(SectionLaundryCartInfoV2 *) sectionLaundryCart
{
    
    LaundryManagerV2 *manager = [[LaundryManagerV2 alloc] init];
    
    if (status == NO) {
        
        //load service
        LaundryServicesModelV2 *service = [[LaundryServicesModelV2 alloc] init];
        service.lservices_id = item.lod_service_id;
        [manager loadLaundryServiceModelV2:service];
        
        sectionLaundryCart.laundryService = service;
        
        //load category
        LaundryCategoryModelV2 *laundryCategory = [[LaundryCategoryModelV2 alloc] init];
        laundryCategory.laundryCategoryId = item.lod_category_id;
        [manager loadLaundryCategoryModelV2ByLaundryCategory:laundryCategory];
        
        sectionLaundryCart.laundryCategory = laundryCategory;
        
        //load laundry item
        LaundryItemModelV2 *modelLaundryItem = [[LaundryItemModelV2 alloc] init];
        modelLaundryItem.laundryItemId = item.lod_item_id;
        modelLaundryItem = [manager loadLaundryItemByItemID:modelLaundryItem];
        
        //load item price for laundry item
        if (item.lod_price == 0) {
            LaundryItemPriceModelV2 *laundryItemPrice = nil;
            laundryItemPrice = [manager loadLaundryItemPriceModelByLaundryItemId:item.lod_item_id andLaundryCategoryId:item.lod_category_id];
            item.lod_price = laundryItemPrice.lip_price;
        }
        
        //insert item
        modelLaundryItem.laundryItem_Quantity = item.lod_quantity;
        modelLaundryItem.laundryItem_Price = item.lod_price * item.lod_quantity;
        
        [sectionLaundryCart insertObjectToNextIndex:modelLaundryItem];
        
        //set total & subtotal
        total += modelLaundryItem.laundryItem_Quantity;
        subTotal += modelLaundryItem.laundryItem_Price;
    }
    else
    {
        //insert item
        //load laundry item
        LaundryItemModelV2 *modelLaundryItem = [[LaundryItemModelV2 alloc] init];
        modelLaundryItem.laundryItemId = item.lod_item_id;
        modelLaundryItem = [manager loadLaundryItemByItemID:modelLaundryItem];
        
        //load item price for laundry item
        if (item.lod_price == 0) {
            LaundryItemPriceModelV2 *laundryItemPrice = nil;
            laundryItemPrice = [manager loadLaundryItemPriceModelByLaundryItemId:item.lod_item_id andLaundryCategoryId:item.lod_category_id];
            item.lod_price = laundryItemPrice.lip_price;
        }
        
        //insert item
        modelLaundryItem.laundryItem_Quantity = item.lod_quantity;
        modelLaundryItem.laundryItem_Price = item.lod_price * item.lod_quantity;;
        
        [sectionLaundryCart insertObjectToNextIndex:modelLaundryItem];
        
        //set total & subtotal
        total += modelLaundryItem.laundryItem_Quantity;
        subTotal += modelLaundryItem.laundryItem_Price;
    }
    
}

-(void)loadDataForCartView
{
    //enable flag to announce, is loading data
    [[HomeViewV2 shareHomeView] waitingLoadingData];
    
    
    MBProgressHUD	*HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getRaLoadingData]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES];
    
    total = 0;
    subTotal = 0;

    LaundryManagerV2 *manager = [[LaundryManagerV2 alloc] init];
    dataLaundryCarts = [NSMutableArray array];
//    SectionLaundryCartInfoV2 *sectionLaundryCartInfo = nil;
    
    //Laundry Order
    LaudryOrderModelV2 *laundryOrder = [[LaudryOrderModelV2 alloc] init];
    laundryOrder.laundryOrderRoomId = [TasksManagerV2 getCurrentRoomAssignment];
    laundryOrder.laundryOrderUserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    laundryOrder.laundryOrderStatus = tStatusChecked;
    laundryOrder.laundryOrderCollectDate = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd"];
    
    //load service
    NSMutableArray *arrayService = [manager loadAllLaundryServicesModelV2];
    
    for (LaundryServicesModelV2 *service in arrayService) {
        
        BOOL isCheckLaundryOrder = [manager isCheckLaundryOrder:laundryOrder];
        
        if (isCheckLaundryOrder == TRUE) {
            
            [manager loadLaundryOrdersModelByRoomIdUserIdAndCollectDate:laundryOrder];
            
            //load All llaundry Order Detail By Order Id
            NSMutableArray *arrayOrderDetail = [manager loadAllLaundryOrderDetailsByServiceId:service.lservices_id AndOrderId:laundryOrder.laundryOrderId];
            
            
            for (LaundryOrderDetailModelV2 *orderDetail in arrayOrderDetail) {
                
                //check item in dataLaundryCart
                BOOL isSection = NO;
                for (NSInteger indexSection=0; indexSection < dataLaundryCarts.count; indexSection ++) {
                    
                    SectionLaundryCartInfoV2 *sectionCart = [dataLaundryCarts objectAtIndex:indexSection];
                    
                    if (sectionCart.laundryService.lservices_id == orderDetail.lod_service_id && sectionCart.laundryCategory.laundryCategoryId == orderDetail.lod_category_id) {
                        
                        [self addLaundrytoSectionWithStatus:YES LaundryItem:orderDetail AndSection:sectionLaundryCartInfo];
                        isSection = YES;
                        break;
                    }
                }
                
                if (isSection == NO) {
                    //new section
                    sectionLaundryCartInfo = [[SectionLaundryCartInfoV2  alloc] init];
                    [self addLaundrytoSectionWithStatus:NO LaundryItem:orderDetail AndSection:sectionLaundryCartInfo];
                    //insert section to cart
                    if (sectionLaundryCartInfo.rowHeights.count > 0) {
                        [dataLaundryCarts addObject:sectionLaundryCartInfo];
                    }
                }
            }
            
        }
        
    }
    
    //insert item in laundry cart temp
    //load laundry item temp
    NSMutableArray *arrayOrderTemp = [manager loadAllLaundryOrderDetailModelV2Temp];
    
    for (LaundryOrderDetailModelV2 *temp in arrayOrderTemp) {
        
        BOOL isCheckTemp = NO;
        BOOL isCheckSection = YES;
        SectionLaundryCartInfoV2 *sectionCart;
        SectionLaundryCartInfoV2 *sectionIsFind;
        
        //check laundry in dataLaundryCart
        for (NSInteger indexSection=0; indexSection < dataLaundryCarts.count; indexSection ++) {
            
            sectionCart = [dataLaundryCarts objectAtIndex:indexSection];
            
            for (LaundryItemModelV2 *item in sectionCart.rowHeights) {
                
                if (sectionCart.laundryService.lservices_id == temp.lod_service_id && sectionCart.laundryCategory.laundryCategoryId == temp.lod_category_id) {
                    isCheckSection = NO;
                    sectionIsFind = sectionCart;
                }
                
                if (sectionCart.laundryService.lservices_id == temp.lod_service_id && sectionCart.laundryCategory.laundryCategoryId == temp.lod_category_id && item.laundryItemId == temp.lod_item_id) {
                    
                    item.laundryItem_Quantity += temp.lod_quantity;
                    
                    //update total
                    total += temp.lod_quantity;
                    //update subtotal
                    subTotal += temp.lod_quantity * temp.lod_price;
                    isCheckTemp = YES;
                    goto outOfCheckSectionCart;
                }
                
            }
            
            if (isCheckSection == NO) {
                goto outOfCheckSectionCart;
            }

        }
        
        //label for check out check section
    outOfCheckSectionCart:
        
        if (isCheckSection == YES) {
            
            //new section
            sectionLaundryCartInfo = [[SectionLaundryCartInfoV2  alloc] init];
            [self addLaundrytoSectionWithStatus:NO LaundryItem:temp AndSection:sectionLaundryCartInfo];
            
            //insert section to cart
            if (sectionLaundryCartInfo.rowHeights.count > 0) {
                [dataLaundryCarts addObject:sectionLaundryCartInfo];
            }
            
        } else {
            if (isCheckTemp == NO) {
                
                [self addLaundrytoSectionWithStatus:YES LaundryItem:temp AndSection:sectionIsFind];
            }
        }
        
        
        
    }
    
    [lblNumberTotal setText:[NSString stringWithFormat:@"%d",total]];
    [lblPriceSubtotal setText:[NSString stringWithFormat:@"$%.0f",subTotal]];
    [lblPriceTotalCost setText:[NSString stringWithFormat:@"$%.0f",subTotal]];

    
    [tbvLaundryCart setDelegate:self];
    [tbvLaundryCart setDataSource:self];
    [tbvLaundryCart reloadData];

    [HUD hide:YES];
    [HUD removeFromSuperview];
    
    //end loading data
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
}

#pragma mark - set caption view
-(void)setCaptionView{
    [self setTitle:[[LanguageManagerV2 sharedLanguageManager] getCart]];
    
    msgbtnNo = [[LanguageManagerV2 sharedLanguageManager] getNo];
    msgbtnYes = [[LanguageManagerV2 sharedLanguageManager] getYes];
    msgSaveCount = [[LanguageManagerV2 sharedLanguageManager] getMsgAlertSaveCounts];
    msgDiscardCount = [[LanguageManagerV2 sharedLanguageManager] getMsgAlertDiscardCounts];

    [lblSubtotal setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_subtotal]];
    [lblServiceCharge setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_service_charge]];
    [lblExpressCharge setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_express_charge]];
    [lblTotal setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_total]];
    [lblTotalCost setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_total_cost]];
    
    //refresh topview
    TopbarViewV2 *topview = [self getTopBarView];
    [topview refresh:[[SuperRoomModelV2 alloc] init]];
}

#pragma didden after save.
-(void) hiddenHUDAfterSaved:(MBProgressHUD *)HUD{
    [HUD hide:YES];
    [HUD removeFromSuperview];
    // [HUD release];
}

#pragma mark - Handle Topbar Methods
-(void)loadTopbarView {
    TopbarViewV2 *topbar = [[TopbarViewV2 alloc] initViewWithSuperModel:[[CommonVariable sharedCommonVariable] superRoomModel]];
    [topbar setTag:tagTopbarView];
    
    [self.view addSubview:topbar];
    [topbar setHidden:YES];
    [self addButtonHandleShowHideTopbar];
}

-(void)showTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationWillStartSelector:@selector(setHiddenTopbarView)];
    
    f = topbar.frame;
    f.origin.y = 0;
    [topbar setFrame:f];
    
    [self adjustShowForViews];
    
    [UIView commitAnimations];
}

-(void)hideTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDidStopSelector:@selector(setHiddenTopbarView)];
    [UIView setAnimationDelegate:self];
    
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    
    [self adjustRemoveForViews];
    
    [UIView commitAnimations];
}

-(void)setHiddenTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    [topbar setHidden:!topbar.hidden];
}

-(TopbarViewV2 *)getTopBarView {
    return (TopbarViewV2 *)[self.view viewWithTag:tagTopbarView];
}

-(void)addButtonHandleShowHideTopbar {
    UIButton *titleBarButton = [[UIButton alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
    
    titleBarButton.titleLabel.backgroundColor = [UIColor clearColor];
    titleBarButton.titleLabel.font = FONT_BUTTON_TOPBAR;
    titleBarButton.titleLabel.textAlignment = UITextAlignmentCenter;
    titleBarButton.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
    [titleBarButton.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
    [titleBarButton.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
    [titleBarButton addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    [titleBarButton setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_CART] forState:UIControlStateNormal];
    self.navigationItem.titleView = titleBarButton;
    
    //Move title to center vertical
    CGRect titleViewBounds = self.navigationItem.titleView.bounds;
    int moveLeftTitle = 0;
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        moveLeftTitle = 10;
    }
    self.navigationItem.titleView.bounds = CGRectMake(titleViewBounds.origin.x + moveLeftTitle, Y_TOPBAR_TITLE, titleViewBounds.size.width, titleViewBounds.size.height);
}

-(void)topbarTapped:(UIButton *)sender {
    TopbarViewV2 *topbar = [self getTopBarView];
    if (topbar.isHidden) {
        [self showTopbarView];
    } else {
        [self hideTopbarView];
    }
}

-(void)adjustShowForViews {
    //adjust all views when show 
    [tbvLaundryCart setFrame:CGRectMake(0, 60, 320, 320)];
    
}

-(void)adjustRemoveForViews { 
    //adjust all views when hide
    [tbvLaundryCart setFrame:CGRectMake(0,0, 320,387)];
    
}

@end
