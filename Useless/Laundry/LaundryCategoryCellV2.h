//
//  LaundryCategoryCellV2.h
//  mHouseKeeping
//
//  Created by TMS on 2/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LaundryCategoryCellV2 : UITableViewCell{
    UIImageView *imgLaundryCategory;
    UILabel *lblLaundryCategory;
}
@property (strong,nonatomic) IBOutlet UIImageView *imgLaundryCategory;
@property (strong,nonatomic) IBOutlet UILabel *lblLaundryCategory;
@end
