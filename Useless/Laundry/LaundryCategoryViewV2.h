//
//  LaundryCategoryViewV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 1/8/12.
//  Copyright (c) 2012 __MyCompanyName__.. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LaundryCategoryCellV2.h"
#import "LaundryItemViewV2.h"
#import "InstructionViewV2.h"
#import "LaundryServicesModelV2.h"
#import "MBProgressHUD.h"
@interface LaundryCategoryViewV2 : UIViewController<UITableViewDataSource,UITableViewDelegate>{
    UITableView *tbvLaundryCategory;
    tServiceLaundry tService;
    tGenderLaundry tGender;
    InstructionViewV2 * instruction;
    LaundryServicesModelV2 *serviceModel;
    int total;
    CGFloat subTotal;
    NSMutableArray * datasLaundryCart;
}
@property (nonatomic)int total;
@property (nonatomic) CGFloat subTotal;
@property(nonatomic,strong)NSMutableArray * datasLaundryCart;
@property (strong,nonatomic) LaundryServicesModelV2 *serviceModel;
@property (strong, nonatomic) InstructionViewV2 * instruction;
@property (strong, nonatomic) IBOutlet UITableView *tbvLaundryCategory;
@property (strong, nonatomic) IBOutlet LaundryCategoryCellV2 *laundryCategoryCell;
@property (nonatomic, assign) tServiceLaundry tService;
@property (nonatomic, assign) tGenderLaundry tGender;
@property (strong, nonatomic) IBOutlet UIButton *btnInstruction;
-(void) loadLaundryCategoryFromWS;
- (IBAction)btnInstructionPressed:(id)sender;

@end
