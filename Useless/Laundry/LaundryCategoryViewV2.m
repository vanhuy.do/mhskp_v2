//
//  LaundryCategoryViewV2.m
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 1/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LaundryCategoryViewV2.h"
#import "CustomAlertViewV2.h"
#import "InstructionViewControllerV2.h"
#import "NetworkCheck.h"
#import "MyNavigationBarV2.h"
#import "DeviceManager.h"

#define imgMale @"male.png"
#define imgFemale @"female.png"

@interface LaundryCategoryViewV2 (PrivateMethods)

-(void)saveLaundryCart:(BOOL) postServer;

#define tagTopbarView 1234
#define tagSaveLeaving 12
#define tagDiscardAll 1002
#define tagDispatchforpickup 1022
#define imgYesButton @"yes_bt.png"
#define imgNoButton @"no_bt.png"

-(void) loadTopbarView;
-(void) showTopbarView;
-(void) hideTopbarView;
-(void) setHiddenTopbarView;
-(void) adjustShowForViews;
-(void) adjustRemoveForViews;
-(void) addButtonHandleShowHideTopbar;
-(void) topbarTapped:(UIButton *) sender;
-(TopbarViewV2 *) getTopBarView;

@end

@implementation LaundryCategoryViewV2
@synthesize laundryCategoryCell,tbvLaundryCategory,serviceModel;
@synthesize tService,instruction;
@synthesize tGender;
@synthesize btnInstruction;
@synthesize total, subTotal, datasLaundryCart;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        tService = tRegular;
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
   
    //Resize content views of current VC to fit with Navigation bar and tabbar
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    //[self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:imgBackground]]];
    
    [btnInstruction setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_INSTRUCTION] forState:UIControlStateNormal];
    
    
    [self performSelectorOnMainThread:@selector(loadTopbarView) withObject:nil waitUntilDone:YES];
    
    //load laundry Category from WS
//    [self performSelector:@selector(loadLaundryCategoryFromWS) withObject:nil afterDelay:0.1];
    [self performSelectorOnMainThread:@selector(loadLaundryCategoryFromWS) withObject:nil waitUntilDone:NO];
}

-(void)viewWillAppear:(BOOL)animated {
    //show wifi view
    [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationShowWifiView] object:nil];
    [super viewWillAppear:animated];
    
    //refresh topview
    TopbarViewV2 *topview = [self getTopBarView];
    [topview refresh:[[SuperRoomModelV2 alloc] init]];
}

- (void)viewDidUnload
{
    laundryCategoryCell = nil;
    [self setLaundryCategoryCell:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark UITableView Delegate Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 2;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 97;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *IdentifyCell = @"IdentifyCell";
    LaundryCategoryCellV2 *cell = nil;
    cell = [tableView dequeueReusableCellWithIdentifier:IdentifyCell];
    if(cell == nil){
        NSArray *array = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([LaundryCategoryCellV2 class]) owner:self options:nil];
        cell = [array objectAtIndex:0];
    }
    switch (indexPath.row) {
        case 0:
        {
            [cell.imgLaundryCategory setImage:[UIImage imageNamed:imgMale]];
            [cell.lblLaundryCategory setText:[[LanguageManagerV2 sharedLanguageManager] getMale]];
        }
            break;
        case 1:   {
            [cell.imgLaundryCategory setImage:[UIImage imageNamed:imgFemale]];
            [cell.lblLaundryCategory setText:[[LanguageManagerV2 sharedLanguageManager] getFemale]];
        }
        default:
            break;
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    LaundryItemViewV2 *laundryItemView = [[LaundryItemViewV2 alloc] initWithNibName:NSStringFromClass([LaundryItemViewV2 class]) bundle:nil ];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK] style:UIBarButtonItemStyleBordered target:nil action:nil];
    self.navigationItem.backBarButtonItem = backButton;
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [cell setSelected:NO animated:YES];
    
    switch (indexPath.row) {
        case 0:
        {
           
            tGender= tMale;
            
            NSString *titleString = nil;

            if (serviceModel.lservices_id == tRegular) {

                titleString = [NSString stringWithFormat:@"%@ - %@", [[LanguageManagerV2 sharedLanguageManager] getRegular], [[LanguageManagerV2 sharedLanguageManager] getMale]];
            } else {
                titleString = [NSString stringWithFormat:@"%@ - %@", [[LanguageManagerV2 sharedLanguageManager] getExpress], [[LanguageManagerV2 sharedLanguageManager] getMale]];
            }
            
            [laundryItemView setTitle:titleString];
        }
            break;
        case 1:
        {
            
            tGender = tFemale;
            
            NSString *titleString = nil;
            
            if (serviceModel.lservices_id == tRegular) {
                titleString = [NSString stringWithFormat:@"%@ - %@",[[LanguageManagerV2 sharedLanguageManager] getRegular],[[LanguageManagerV2 sharedLanguageManager] getFemale]];
            }
            else{
                titleString = [NSString stringWithFormat:@"%@ - %@",[[LanguageManagerV2 sharedLanguageManager] getExpress],[[LanguageManagerV2 sharedLanguageManager] getFemale]];
            }
            
            [laundryItemView setTitle:titleString];
        }
            break;
        default:
            break;
    }
    [laundryItemView setService:tService AndGender:tGender];
    [laundryItemView setLr_service_model:self.serviceModel];
    [self.navigationController pushViewController:laundryItemView animated:YES];
   // [laundryItemView release];
    
}

#pragma mark - Intruction Press
- (IBAction)btnInstructionPressed:(id)sender{
    //push instruction view controller
    InstructionViewControllerV2 *instructionView = [[InstructionViewControllerV2 alloc] initWithNibName:@"InstructionViewControllerV2" bundle:nil];
    [self.navigationController pushViewController:instructionView animated:YES];
}

#pragma mark - Load Laundry Cart
-(void) addLaundrytoSectionWithStatus:(BOOL) status LaundryItem: (LaundryOrderDetailModelV2 *) item AndSection:(SectionLaundryCartInfoV2 *) sectionLaundryCart
{
    
    LaundryManagerV2 *manager = [[LaundryManagerV2 alloc] init];
    
    if (status == NO) {
        
        //load service
        LaundryServicesModelV2 *service = [[LaundryServicesModelV2 alloc] init];
        service.lservices_id = item.lod_service_id;
        [manager loadLaundryServiceModelV2:service];
        
        sectionLaundryCart.laundryService = service;
        
        //load category
        LaundryCategoryModelV2 *laundryCategory = [[LaundryCategoryModelV2 alloc] init];
        laundryCategory.laundryCategoryId = item.lod_category_id;
        [manager loadLaundryCategoryModelV2ByLaundryCategory:laundryCategory];
        
        sectionLaundryCart.laundryCategory = laundryCategory;
        
        //load laundry item
        LaundryItemModelV2 *modelLaundryItem = [[LaundryItemModelV2 alloc] init];
        modelLaundryItem.laundryItemId = item.lod_item_id;
        modelLaundryItem = [manager loadLaundryItemByItemID:modelLaundryItem];
        
        //load item price for laundry item
        if (item.lod_price == 0) {
            LaundryItemPriceModelV2 *laundryItemPrice = nil;
            laundryItemPrice = [manager loadLaundryItemPriceModelByLaundryItemId:item.lod_item_id andLaundryCategoryId:item.lod_category_id];
            item.lod_price = laundryItemPrice.lip_price;
        }
        
        //insert item
        modelLaundryItem.laundryItem_Quantity = item.lod_quantity;
        modelLaundryItem.laundryItem_Price = item.lod_price * item.lod_quantity;
        
        [sectionLaundryCart insertObjectToNextIndex:modelLaundryItem];
        
        //set total & subtotal
        total += modelLaundryItem.laundryItem_Quantity;
        subTotal += modelLaundryItem.laundryItem_Price;
    }
    else
    {
        //insert item
        //load laundry item
        LaundryItemModelV2 *modelLaundryItem = [[LaundryItemModelV2 alloc] init];
        modelLaundryItem.laundryItemId = item.lod_item_id;
        modelLaundryItem = [manager loadLaundryItemByItemID:modelLaundryItem];
        
        //load item price for laundry item
        if (item.lod_price == 0) {
            LaundryItemPriceModelV2 *laundryItemPrice = nil;
            laundryItemPrice = [manager loadLaundryItemPriceModelByLaundryItemId:item.lod_item_id andLaundryCategoryId:item.lod_category_id];
            item.lod_price = laundryItemPrice.lip_price;
        }
        
        //insert item
        modelLaundryItem.laundryItem_Quantity = item.lod_quantity;
        modelLaundryItem.laundryItem_Price = item.lod_price * item.lod_quantity;;
        
        [sectionLaundryCart insertObjectToNextIndex:modelLaundryItem];
        
        //set total & subtotal
        total += modelLaundryItem.laundryItem_Quantity;
        subTotal += modelLaundryItem.laundryItem_Price;
    }
    
}

-(void)loadDataForCartView
{
    
    LaundryManagerV2 *manager = [[LaundryManagerV2 alloc] init];
    datasLaundryCart = [NSMutableArray array];
    SectionLaundryCartInfoV2 *sectionLaundryCartInfo = nil;
    
    //load service
    NSMutableArray *arrayService = [manager loadAllLaundryServicesModelV2];
    
    //Laundry Order
    LaudryOrderModelV2 *laundryOrder = [[LaudryOrderModelV2 alloc] init];
    laundryOrder.laundryOrderRoomId = [TasksManagerV2 getCurrentRoomAssignment];
    laundryOrder.laundryOrderUserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    laundryOrder.laundryOrderStatus = tStatusChecked;
    laundryOrder.laundryOrderCollectDate = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd"];
    
    
    BOOL isCheckLaundryOrder = [manager isCheckLaundryOrder:laundryOrder];
    
    for (LaundryServicesModelV2 *service in arrayService) {
        
        if (isCheckLaundryOrder == TRUE) {
            
            [manager loadLaundryOrdersModelByRoomIdUserIdAndCollectDate:laundryOrder];
            
            //load All llaundry Order Detail By Order Id
            NSMutableArray *arrayOrderDetail = [manager loadAllLaundryOrderDetailsByOrderId:laundryOrder.laundryOrderId];
            
            
            for (LaundryOrderDetailModelV2 *orderDetail in arrayOrderDetail) {
                
                //check item in dataLaundryCart
                BOOL isSection = NO;
                for (NSInteger indexSection=0; indexSection < datasLaundryCart.count; indexSection ++) {
                    
                    SectionLaundryCartInfoV2 *sectionCart = [datasLaundryCart objectAtIndex:indexSection];
                    
                    if (sectionCart.laundryService.lservices_id == orderDetail.lod_service_id && sectionCart.laundryCategory.laundryCategoryId == orderDetail.lod_category_id) {
                        
                        [self addLaundrytoSectionWithStatus:YES LaundryItem:orderDetail AndSection:sectionLaundryCartInfo];
                        isSection = YES;
                        break;
                    }
                }
                
                if (isSection == NO) {
                    //new section
                    sectionLaundryCartInfo = [[SectionLaundryCartInfoV2  alloc] init];
                    [self addLaundrytoSectionWithStatus:NO LaundryItem:orderDetail AndSection:sectionLaundryCartInfo];
                    //insert section to cart
                    if (sectionLaundryCartInfo.rowHeights.count > 0) {
                        [datasLaundryCart addObject:sectionLaundryCartInfo];
                    }
                }
            }
            
        }
        
    }
    
    //insert item in laundry cart temp
    //load laundry item temp
    NSMutableArray *arrayOrderTemp = [manager loadAllLaundryOrderDetailModelV2Temp];
    
    for (LaundryOrderDetailModelV2 *temp in arrayOrderTemp) {
        
        BOOL isCheckTemp = NO;
        BOOL isCheckSection = YES;
        SectionLaundryCartInfoV2 *sectionCart;
        SectionLaundryCartInfoV2 *sectionIsFind;
        
        //check laundry in dataLaundryCart
        for (NSInteger indexSection=0; indexSection < datasLaundryCart.count; indexSection ++) {
            
            sectionCart = [datasLaundryCart objectAtIndex:indexSection];
            
            for (LaundryItemModelV2 *item in sectionCart.rowHeights) {
                
                if (sectionCart.laundryService.lservices_id == temp.lod_service_id && sectionCart.laundryCategory.laundryCategoryId == temp.lod_category_id) {
                    isCheckSection = NO;
                    sectionIsFind = sectionCart;
                }
                
                if (sectionCart.laundryService.lservices_id == temp.lod_service_id && sectionCart.laundryCategory.laundryCategoryId == temp.lod_category_id && item.laundryItemId == temp.lod_item_id) {
                    
                    item.laundryItem_Quantity += temp.lod_quantity;
                    
                    //update total
                    total += temp.lod_quantity;
                    //update subtotal
                    subTotal += temp.lod_quantity * temp.lod_price;
                    
                    isCheckTemp = YES;
                    goto outOfCheckSectionCart;
                }
                
            }
            
            if (isCheckSection == NO) {
                goto outOfCheckSectionCart;
            }
            
        }
        
        //label for check out check section
    outOfCheckSectionCart:
        
        if (isCheckSection == YES) {
            
            //new section
            sectionLaundryCartInfo = [[SectionLaundryCartInfoV2  alloc] init];
            [self addLaundrytoSectionWithStatus:NO LaundryItem:temp AndSection:sectionLaundryCartInfo];
            
            //insert section to cart
            if (sectionLaundryCartInfo.rowHeights.count > 0) {
                [datasLaundryCart addObject:sectionLaundryCartInfo];
            }
            
        } else {
            if (isCheckTemp == NO) {
                
                [self addLaundrytoSectionWithStatus:YES LaundryItem:temp AndSection:sectionIsFind];
            }
        }
        
        
        
    }
    
}

#pragma mark - === Handle Check Save Data ===
#pragma mark
-(BOOL) isCheckLaundryOrderTemp
{
    NSMutableArray *array = [[LaundryManagerV2 sharedLaundryManagerV2] loadAllLaundryOrderDetailModelV2Temp];
    if (array.count > 0) {
        return YES;
    }
    else
    {
        return NO;
    }
}
-(BOOL) checkSaveDataWithTagEvent:(NSInteger) tagOfEvent {
    
    BOOL isCheckOrder = [self isCheckLaundryOrderTemp];
    if (tagOfEvent == tagOfMessageButton) {
        if (isCheckOrder == NO) {
            return NO;
        }
        else
        {
            //Show alert please save before leaving
            UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:[[LanguageManagerV2 sharedLanguageManager] getMsgAlertSaveCounts] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles:[[LanguageManagerV2 sharedLanguageManager] getNo], nil]; 
            alert.delegate = self;
            alert.tag = tagSaveLeaving;
            [alert show];
            
            return YES;
            
        }
    }
    else
    {
        //Show alert complete room
        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_complete_room_to_leave] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles: nil]; 
        alert.delegate = self;
        [alert show];
        
        return YES;
        
    }
    
}

#pragma mark - UIAlertView Delegate Methods
-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    switch (alertView.tag) {
        case tagSaveLeaving:
        {
            switch (buttonIndex)
            {
                case 0:
                {
                    //YES: show alert dispatch for pickup
                    UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:imgYesButton], [UIImage imageNamed:imgNoButton], nil] title:[[LanguageManagerV2 sharedLanguageManager]getAlertTitle] message:[[LanguageManagerV2 sharedLanguageManager] getMsg_Dispatch_This_For_PickUp] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles:  [[LanguageManagerV2 sharedLanguageManager] getNo], nil];   
                    [alert setTag:tagDispatchforpickup];
                    [alert show];      
                }
                    break;
                case 1:
                {
                    //NO: show alert discart all input
                    UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:imgYesButton], [UIImage imageNamed:imgNoButton], nil] title:[[LanguageManagerV2 sharedLanguageManager]getAlertTitle] message:[[LanguageManagerV2 sharedLanguageManager] getMsgAlertDiscardCounts] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles:  [[LanguageManagerV2 sharedLanguageManager] getNo], nil];   
                    [alert setTag:tagDiscardAll];
                    [alert show];                    
                    
                }
                default:
                    break;
            }
        }
            
            break;
        case tagDispatchforpickup:
        {
            switch (buttonIndex)
            {
                case 0:
                {
                    //YES: in dispatch for pickup will save data and post to server
                    [self saveLaundryCart:YES];
                    
                    //if tag is message
                    if ([[NSUserDefaults standardUserDefaults] integerForKey:TAG_OF_EVENT] == tagOfMessageButton) {
                        [[HomeViewV2 shareHomeView] handleTabbarEventWithTagOfButton:[[NSUserDefaults standardUserDefaults] integerForKey:TAG_OF_EVENT]];
                    }
                    
                }
                    break;
                case 1:
                {
                    //NO: in dispatch for pickup will save data but not post to server
                    [self saveLaundryCart:NO];
                    
                    //if tag is message
                    if ([[NSUserDefaults standardUserDefaults] integerForKey:TAG_OF_EVENT] == tagOfMessageButton) {
                        [[HomeViewV2 shareHomeView] handleTabbarEventWithTagOfButton:[[NSUserDefaults standardUserDefaults] integerForKey:TAG_OF_EVENT]];
                    }
                    
                }
                default:
                    break;
            }
        }
            
            break;
        case tagDiscardAll:
        {
            switch (buttonIndex)
            {
                case 0:
                {
                    //YES: in discard all data will go out, clear cart temp
                    [[LaundryManagerV2 sharedLaundryManagerV2] deleteAllLaundryOrderDetailTempData];
                    
                    //if tag is message
                    if ([[NSUserDefaults standardUserDefaults] integerForKey:TAG_OF_EVENT] == tagOfMessageButton) {
                        [[HomeViewV2 shareHomeView] handleTabbarEventWithTagOfButton:[[NSUserDefaults standardUserDefaults] integerForKey:TAG_OF_EVENT]];
                    }
                    else{
                        [self.navigationController popViewControllerAnimated:YES];
                    }
                }
                    break;
                case 1:
                {
                    //NO: in discart all data will go cart screen
                    //LaundryCartViewV2 *laundryCartView = [[LaundryCartViewV2 alloc] initWithNibName:NSStringFromClass([LaundryCartViewV2 class]) bundle:nil];
                    //[self.navigationController pushViewController:laundryCartView animated:YES];
                    
                }
                default:
                    break;
            }
        }
            
            break;
            
        default:
            break;
    }
}

#pragma mark - Save Laundry
-(void)saveData{
    BOOL isCheckOrder = [self isCheckLaundryOrderTemp];
    if (isCheckOrder == YES) {
        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:imgYesButton], [UIImage imageNamed:imgNoButton], nil] title:[[LanguageManagerV2 sharedLanguageManager]getAlertTitle] message:[[LanguageManagerV2 sharedLanguageManager] getMsg_Dispatch_This_For_PickUp] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles:  [[LanguageManagerV2 sharedLanguageManager] getNo], nil];   
        [alert setTag:tagDispatchforpickup];
        [alert show];

    }
}

-(void)saveLaundryCart:(BOOL) postServer{
    
    // @ show loading data .
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getRaSavingData]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES]; 
    
    ///Save laundry cart
    LaundryManagerV2 *manager = [[LaundryManagerV2 alloc] init];
    SyncManagerV2 *synManager = [[SyncManagerV2 alloc] init];
    NSInteger userId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    NSInteger hotelId = [[[UserManagerV2 sharedUserManager] currentUser] userHotelsId];
    NSInteger roomAssignId = [TasksManagerV2 getCurrentRoomAssignment];
    
    //load order in laundry
    LaudryOrderModelV2 *laundryOrder = [[LaudryOrderModelV2 alloc] init];
    laundryOrder.laundryOrderUserId = userId;
    laundryOrder.laundryOrderRoomId = roomAssignId;
    laundryOrder.laundryOrderStatus = tStatusChecked;
    laundryOrder.laundryOrderCollectDate = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd"]; 
    
    BOOL isCheckLaundryOrder = [manager isCheckLaundryOrder:laundryOrder];
    
    if (isCheckLaundryOrder == FALSE) {
        [manager insertLaudryOrderModelV2:laundryOrder];
    }
    
    //load laundry order
    [manager loadLaundryOrdersModelByRoomIdUserIdAndCollectDate:laundryOrder];
    
    
    //load all laundry service
    NSMutableArray *arrayService = [manager loadAllLaundryServicesModelV2];
    
    for (LaundryServicesModelV2 *service in arrayService) {
        
        //--------------------get laundry order Id--------------------
        int orderIdWS = 0;
        if (postServer == YES) {
            
            //load laundry item to get total & subtotal
            if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
                [self loadDataForCartView];
                orderIdWS = [synManager postLaundryOrder:laundryOrder andsubQuantity:total andServiceID:service.lservices_id andHotel:hotelId andsubTotal:subTotal];
            }
            
        }
        //--------------------WS--------------------------------------
        
        //load all laundry order detail temp by service id
        NSMutableArray *arrayLaundryOrderDetailTemp = [manager loadAllLaundryOrderDetailsTempByServiceId:service.lservices_id];
        
        if (arrayLaundryOrderDetailTemp.count > 0) {
            
            for (LaundryOrderDetailModelV2 *orderTemp in arrayLaundryOrderDetailTemp) {
                
                //load laundry order detail by order id
                LaundryOrderDetailModelV2 *orderDetail = [[LaundryOrderDetailModelV2 alloc] init];
                orderDetail.lod_item_id = orderTemp.lod_item_id;
                orderDetail.lod_service_id = orderTemp.lod_service_id;
                orderDetail.lod_category_id = orderTemp.lod_category_id;
                
                [manager loadLaundryOrderDetailsByItemId:orderDetail AndOrderId:laundryOrder.laundryOrderId];
                
                if (orderDetail.lod_id != 0) {
                    
                    //update Order detail in laundry
                    orderDetail.lod_quantity = orderTemp.lod_quantity + orderDetail.lod_quantity;
                    
                    [manager updateLaundryOrderDetailModelV2:orderDetail];
                }
                else
                {
                    //insert Order detail in laundry
                    LaundryOrderDetailModelV2 *laundryOrderDetailModel = [[LaundryOrderDetailModelV2 alloc] init];
                    laundryOrderDetailModel.lod_item_id = orderTemp.lod_item_id;
                    laundryOrderDetailModel.lod_service_id = orderTemp.lod_service_id;
                    laundryOrderDetailModel.lod_category_id = orderTemp.lod_category_id;
                    laundryOrderDetailModel.lod_post_status = POST_STATUS_SAVED_UNPOSTED;
                    laundryOrderDetailModel.lod_quantity = orderTemp.lod_quantity;
                    laundryOrderDetailModel.lod_laundry_order_id = laundryOrder.laundryOrderId;
                    
                    [manager insertLaundryOrderDetailModelV2:laundryOrderDetailModel];
                    
                }
                
                //--------------------------post to server WS-----------------
                if (postServer == YES) {
                    
                    LaundryOrderDetailModelV2 *orderDetailPost = [[LaundryOrderDetailModelV2 alloc] init];
                    orderDetailPost.lod_item_id = orderTemp.lod_item_id;
                    orderDetailPost.lod_service_id = orderTemp.lod_service_id;
                    orderDetailPost.lod_category_id = orderTemp.lod_category_id;
                    orderDetailPost.lod_laundry_order_id = laundryOrder.laundryOrderId;
                    
                    [manager loadLaundryOrderDetailsByItemId:orderDetailPost AndOrderId:laundryOrder.laundryOrderId];
                    
                    BOOL statusPost = [synManager postLaundryItem:orderDetailPost andLaundryOrder:orderIdWS anduser:userId andTransactionTime:laundryOrder.laundryOrderCollectDate];
                    if (statusPost == YES) {
                        //post success, clear database
                        [manager deleteLaundryOrderDetailModelV2:orderDetailPost];
                        
                    }
                }
                
                //delete laundry order detail temp
                [manager deleteLaundryOrderDetailTemp:orderTemp];
                //--------------------------End------------------------------
            }
            
        }
        
    }
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getMsgSaveMessage]];
    //hide saving data.
    [self performSelector:@selector(hiddenHUDAfterSaved:) withObject:HUD afterDelay:0.5];
    
}


#pragma mark - Get Laundry Category From WS
-(void)loadLaundryCategoryFromWS
{
    //enable flag to announce, is loading data
    [[HomeViewV2 shareHomeView] waitingLoadingData];
    
    MBProgressHUD	*HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getRaLoadingData]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES];
    
    SyncManagerV2 *synManager = [[SyncManagerV2 alloc] init];
    int userID=[[UserManagerV2 sharedUserManager] currentUser].userId ;
    int hotelID=[[UserManagerV2 sharedUserManager] currentUser].userHotelsId;
    
    //-----------------get laundry category WS--------------
    NSString *lastModifiedCategory = [[LaundryManagerV2 sharedLaundryManagerV2] getTheLatestModifiedInLaundryCatergory];
    BOOL result = [synManager getAllLaundryCategoryByUserId:[NSNumber numberWithInt:userID] atHotel:[NSNumber numberWithInt:hotelID] andlastDate:lastModifiedCategory];
    
    if (result == NO) {
        //show alert no network
        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:[[LanguageManagerV2 sharedLanguageManager] getNoWifi] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles: nil]; 
        alert.tag = 1;
        [alert show];
    }
    //-----------------end----------------------------------
    
    [tbvLaundryCategory reloadData];
    [HUD hide:YES];
    [HUD removeFromSuperview];
    
    //end loading data
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
}

#pragma mark- hidden after save.
-(void) hiddenHUDAfterSaved:(MBProgressHUD *)HUD{
    [HUD hide:YES];
    [HUD removeFromSuperview];
    // [HUD release];
}

#pragma mark - Handle Topbar Methods
-(void)loadTopbarView {
    TopbarViewV2 *topbar = [[TopbarViewV2 alloc] initViewWithSuperModel:[[CommonVariable sharedCommonVariable] superRoomModel]];
    [topbar setTag:tagTopbarView];
    
    [self.view addSubview:topbar];
    [topbar setHidden:YES];
    [self addButtonHandleShowHideTopbar];
}

-(void)showTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationWillStartSelector:@selector(setHiddenTopbarView)];
    
    f = topbar.frame;
    f.origin.y = 0;
    [topbar setFrame:f];
    
    [self adjustShowForViews];
    
    [UIView commitAnimations];
}

-(void)hideTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDidStopSelector:@selector(setHiddenTopbarView)];
    [UIView setAnimationDelegate:self];
    
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    
    [self adjustRemoveForViews];
    
    [UIView commitAnimations];
}

-(void)setHiddenTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    [topbar setHidden:!topbar.hidden];
}

-(TopbarViewV2 *)getTopBarView {
    return (TopbarViewV2 *)[self.view viewWithTag:tagTopbarView];
}

-(void)addButtonHandleShowHideTopbar {
    UIButton *titleBarButton = [[UIButton alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
    
    titleBarButton.titleLabel.backgroundColor = [UIColor clearColor];
    titleBarButton.titleLabel.font = FONT_BUTTON_TOPBAR;
    titleBarButton.titleLabel.textAlignment = UITextAlignmentCenter;
    titleBarButton.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
    [titleBarButton.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
    [titleBarButton.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
    [titleBarButton addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    NSString *titleString = nil;
    
    if (tService == tRegular) {
        titleString = [NSString stringWithFormat:@"%@ - %@", [[LanguageManagerV2 sharedLanguageManager] getLaundry], [[LanguageManagerV2 sharedLanguageManager] getRegular]];
    } else {
        titleString = [NSString stringWithFormat:@"%@ - %@", [[LanguageManagerV2 sharedLanguageManager] getLaundry], [[LanguageManagerV2 sharedLanguageManager] getExpress]];
    }
    [titleBarButton setTitle:titleString forState:UIControlStateNormal];
    self.navigationItem.titleView = titleBarButton;
    
    //Move title to center vertical
    CGRect titleViewBounds = self.navigationItem.titleView.bounds;
    int moveLeftTitle = 0;
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        moveLeftTitle = 10;
    }
    self.navigationItem.titleView.bounds = CGRectMake(titleViewBounds.origin.x + moveLeftTitle, Y_TOPBAR_TITLE, titleViewBounds.size.width, titleViewBounds.size.height);
    
    //back button navigation
    self.navigationItem.leftBarButtonItem = [MyNavigationBarV2 createBackButtonWithTarget:self selector:@selector(backBarPressed) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - === Back ===
#pragma mark
-(void)backBarPressed {
//    //clear tag of event
//    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:TAG_OF_EVENT];
//    if ([instructionView isChangeInInstructionView] == YES) {
//        //show alert to save data
//        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:[[LanguageManagerV2 sharedLanguageManager] getMsgAlertSaveCounts] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles:[[LanguageManagerV2 sharedLanguageManager] getNo], nil]; 
//        alert.delegate = self;
//        alert.tag = tagAlertBack;
//        [alert show];
//        
//    } else // pop to previous view
    [self.navigationController popViewControllerAnimated:NO];
}


-(void)topbarTapped:(UIButton *)sender {
    TopbarViewV2 *topbar = [self getTopBarView];
    if (topbar.isHidden) {
        [self showTopbarView];
    } else {
        [self hideTopbarView];
    }
}

-(void)adjustShowForViews {
    //adjust all views when show 
    [tbvLaundryCategory setFrame:CGRectMake(0, 60, 320, 320)];
    
}

-(void)adjustRemoveForViews { 
    //adjust all views when hide
    [tbvLaundryCategory setFrame:CGRectMake(0,0, 320, 350)];
    
}


@end
