//
//  LaundryCellV2.h
//  mHouseKeeping
//
//  Created by TMS on 2/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LaundryCellV2 : UITableViewCell{
    UIImageView *imgLaundryCell;
    UILabel *lblLaundryCell;
}

@property (nonatomic, strong) IBOutlet UIImageView *imgLaundryCell;
@property (nonatomic, strong) IBOutlet UILabel *lblLaundryCell;

@end
