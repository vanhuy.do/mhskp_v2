//
//  LaundryItemCellV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 1/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol LaundryItemCellV2Delegate <NSObject>

@optional
- (void) btnQtyLaundryItemCellPressedWithIndexPath:(NSIndexPath *)indexpath
AndSender:(UIButton *) sender;

@end

@interface LaundryItemCellV2 : UITableViewCell{
    __unsafe_unretained id<LaundryItemCellV2Delegate> delegate;
    NSIndexPath *indexpath;
    
}
@property (assign, nonatomic) id<LaundryItemCellV2Delegate> delegate;
@property (strong, nonatomic) NSIndexPath *indexpath;
@property (strong, nonatomic) IBOutlet UIImageView *imgLaundryItemCell;
@property (strong, nonatomic) IBOutlet UILabel *lblNameLaundryItemCell;
@property (strong, nonatomic) IBOutlet UILabel *lblQtyLaundryItemCell;
@property (strong, nonatomic) IBOutlet UIButton *btnQtyLaundryItemCell;
@property (strong, nonatomic) IBOutlet UILabel *lblSeparationItemCell;


- (IBAction)btnQtyLaundryItemCellPressed:(id)sender;

@end
