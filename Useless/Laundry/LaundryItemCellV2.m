//
//  LaundryItemCellV2.m
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 1/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LaundryItemCellV2.h"

@implementation LaundryItemCellV2
@synthesize imgLaundryItemCell;
@synthesize lblSeparationItemCell;
@synthesize lblNameLaundryItemCell;
@synthesize lblQtyLaundryItemCell;
@synthesize btnQtyLaundryItemCell;
@synthesize delegate;
@synthesize indexpath;
//- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
//{
//    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
//    if (self) {
//        // Custom initialization
//    }
//    return self;
//}
//
//- (void)didReceiveMemoryWarning
//{
//    // Releases the view if it doesn't have a superview.
//    [super didReceiveMemoryWarning];
//    
//    // Release any cached data, images, etc that aren't in use.
//}
//
//#pragma mark - View lifecycle
//
//- (void)viewDidLoad
//{
//    [super viewDidLoad];
//    // Do any additional setup after loading the view from its nib.
//}
//
//- (void)viewDidUnload
//{
//    [super viewDidUnload];
//    // Release any retained subviews of the main view.
//    // e.g. self.myOutlet = nil;
//}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc {
//    [imgLaundryItemCell release];
//    [lblNameLaundryItemCell release];
//    [lblQtyLaundryItemCell release];
//    [btnQtyLaundryItemCell release];
   // [super dealloc];
}
- (IBAction)btnQtyLaundryItemCellPressed:(id)sender {
    if ([delegate respondsToSelector:@selector(btnQtyLaundryItemCellPressedWithIndexPath:AndSender:)]) {
        [delegate btnQtyLaundryItemCellPressedWithIndexPath:indexpath AndSender:sender];
    }

}
@end
