//
//  LaundryItemViewV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 1/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LaundryCategoryModelV2.h"
#import "LaundryItemModelV2.h"
#import "SectionLaundryCategoryViewV2.h"
#import "SectionLaundryInfoV2.h"
#import "LaundryItemModelV2.h"
#import "LaundryItemCellV2.h"
#import "PickerViewV2.h"
#import "InstructionViewV2.h"
#import "LaundryCartViewV2.h"
#import "SectionLaundryCartInfoV2.h"
#import "LaundryManagerV2.h"
#import "MBProgressHUD.h"
#import "LaundryItemTempAdapterV2.h"
#import "LaudryOrderModelV2.h"
@class InstructionViewV2;

typedef enum {
    tRegular = 1,
    tExpress= 2,
}tServiceLaundry;

typedef enum {
    tFemale = 1,
    tMale = 2,
}tGenderLaundry;

@interface UILabel (BPExtensions)
- (void)sizeToFitFixedWidth: (NSInteger)fixedWith;

@end


@interface LaundryItemViewV2 : UIViewController<UITableViewDataSource,UITableViewDelegate, SectionLaundryItemViewV2Delegate, LaundryItemCellV2Delegate, PickerViewV2Delegate, LaundryCartViewV2Delegate>{
    tServiceLaundry tService;
    tGenderLaundry tgender;
    NSMutableArray *datas;
    InstructionViewV2 *instruction;
    BOOL statusChooseSave;
    NSMutableArray * datasLaundryCart;
    UIButton * button;
    LaundryCategoryModelV2 *categoryObj;
    //add laundry Oder
    LaudryOrderModelV2 *lrOrderObject;
    NSMutableDictionary *lr_Item_Not_In_Cart;
    LaundryServicesModelV2 *lr_service_model;
    int item_Total;
    int total;
    CGFloat subTotal;
}

@property (nonatomic)int total;
@property (nonatomic) CGFloat subTotal;
@property (nonatomic)  int item_Total;
@property (nonatomic,strong)LaundryServicesModelV2 *lr_service_model;
@property (nonatomic,strong)NSMutableDictionary *lr_Item_Not_In_Cart;
@property (nonatomic, strong) LaudryOrderModelV2 *lrOrderObject;
@property(nonatomic,strong) LaundryCategoryModelV2 *categoryObj;
@property(nonatomic,strong)NSMutableArray * datasLaundryCart;
@property (nonatomic, strong) InstructionViewV2 *instruction;
@property (strong, nonatomic) UIButton *btnCart;
@property (strong, nonatomic) UIButton * button;;
@property (nonatomic, assign) tGenderLaundry tgender;
@property (nonatomic, assign) tServiceLaundry tService;
@property (strong, nonatomic) IBOutlet UITableView *tbvLaundryCategory;
@property (nonatomic, retain) NSMutableArray *datas;
@property (assign, nonatomic) BOOL statusSave;
@property (strong, nonatomic) NSString *msgbtnYes;
@property (strong, nonatomic) NSString *msgbtnNo;
@property (strong, nonatomic) NSString *msgSaveCount;
@property (strong, nonatomic) NSString *msgDiscardCount;
@property (assign, nonatomic) BOOL statusAddToCart;
@property (strong, nonatomic) IBOutlet UIButton *btnInstruction;
@property (strong, nonatomic) IBOutlet UIButton *btnLaundryAddToCart;

- (IBAction)btnInstructionPressed:(id)sender;
-(void) setService:(tServiceLaundry) tservice AndGender:(tGenderLaundry) gender;
- (IBAction)btnLaundryAddtoCartPressed:(id)sender;
-(void)loadDataLaundryItemFromDatabase;
-(void)setCaptionView;
@end
