//
//  LaundryItemViewV2.m
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 1/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LaundryItemViewV2.h"
#import "LaundryManagerV2.h"
#import "MBProgressHUD.h"
#import "RoomManagerV2.h"
#import "CustomAlertViewV2.h"
#import "UserManagerV2.h"
#import "ehkConvert.h"
#import "RoomManagerV2.h"
#import "TasksManagerV2.h"
#import "NetworkCheck.h"
#import "InstructionViewControllerV2.h"
#import "MyNavigationBarV2.h"

#define tagAlertBack 10
#define tagAlertNo 11
#define tagAlertBottomBar 12
#define tagAlertDispatch 13
#define tagAlertDisCard 14
#define tagTopbarView 1234
#define tagPostedStatus 1
#define tagNoPostedStatus 0
#define imgYesButton @"yes_bt.png"
#define imgNoButton @"no_bt.png"
@interface LaundryItemViewV2(privatemethods)
-(void) btnCartPressed;
-(void)saveLaundryCart:(BOOL) postServer;
-(void) handleBtnAddToCartPressed;

-(void) loadTopbarView;
-(void) showTopbarView;
-(void) hideTopbarView;
-(void) setHiddenTopbarView;
-(void) adjustShowForViews;
-(void) adjustRemoveForViews;
-(void) addButtonHandleShowHideTopbar;
-(void) topbarTapped:(UIButton *) sender;
-(TopbarViewV2 *) getTopBarView;

@end

@implementation UILabel (BPExtensions)

- (void)sizeToFitFixedWidth: (NSInteger)fixedWith {
	self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, fixedWith, 0);
	self.lineBreakMode = UILineBreakModeWordWrap;
	self.numberOfLines = 0;
	[self sizeToFit];
}

@end

@implementation LaundryItemViewV2
@synthesize lr_Item_Not_In_Cart,lr_service_model,item_Total;
@synthesize tbvLaundryCategory;
@synthesize  datas,categoryObj;
@synthesize lrOrderObject;
@synthesize tgender, tService,button;
@synthesize btnCart,msgSaveCount,msgbtnNo,msgbtnYes,msgDiscardCount;
@synthesize instruction,statusSave,datasLaundryCart;
@synthesize statusAddToCart;
@synthesize btnInstruction;
@synthesize btnLaundryAddToCart;
@synthesize total, subTotal;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle
-(void)viewWillAppear:(BOOL)animated {
    //hide wifi view
    [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationHideWifiView] object:nil];
    [super viewWillAppear:animated];
    //Move to center vertical
    CGRect titleViewBounds = self.navigationController.navigationBar.topItem.titleView.bounds;
    self.navigationController.navigationBar.topItem.titleView.bounds = CGRectMake(titleViewBounds.origin.x, Y_TOPBAR_TITLE, titleViewBounds.size.width, titleViewBounds.size.height);
    [self handleBtnAddToCartPressed];
    [self setCaptionView];
    [tbvLaundryCategory reloadData];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
//    [self performSelector:@selector(loadDataLaundryItemFromDatabase) withObject:nil afterDelay:0.5];
    [self performSelectorOnMainThread:@selector(loadDataLaundryItemFromDatabase) withObject:nil waitUntilDone:NO];

    [btnInstruction setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_INSTRUCTION] forState:UIControlStateNormal];
    [btnLaundryAddToCart setTitle:[[LanguageManagerV2 sharedLanguageManager] getAddToCart] forState:UIControlStateNormal];
    
    btnCart = [[UIButton alloc]initWithFrame:CGRectMake(236, 6, 60, 32)];
    
    [btnCart setBackgroundImage:[UIImage imageNamed:imgTopCart] forState:UIControlStateNormal];
    [btnCart setTitleEdgeInsets:UIEdgeInsetsMake(0, 32, 0, 0)];
    [btnCart.titleLabel setFont:[UIFont fontWithName:@"Arial-BoldMT" size:15]];
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithCustomView:btnCart];
    self.navigationItem.rightBarButtonItem = rightButton;
    [btnCart addTarget:self action:@selector(btnCartPressed) forControlEvents:UIControlEventTouchUpInside];
    
    button = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *img = [UIImage imageNamed:@"btn_back.png"];
    [button setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK] forState:UIControlStateNormal];
    [button setBackgroundImage:img forState:UIControlStateNormal];
    [button addTarget:self action:@selector(backBarPressed) forControlEvents:UIControlEventTouchUpInside];
    [button setFrame:CGRectMake(0, 0,WIDTH_BACK_BUTTON , HEIGHT_BACK_BUTTON)];
    [button.titleLabel setFont:[UIFont boldSystemFontOfSize:FONT_BACK_BUTTON]];
    [button setContentEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
    UIView *backButtonView = [[UIView alloc] initWithFrame:button.frame];
    backButtonView.bounds = CGRectMake(backButtonView.bounds.origin.x, Y_BACK_BUTTON, backButtonView.bounds.size.width, backButtonView.bounds.size.height);
    [backButtonView addSubview:button];
    
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButtonView];
    self.navigationItem.leftBarButtonItem = barButtonItem;
   // [barButtonItem release];
//    [self performSelector:@selector(loadTopbarView)];
    [self performSelectorOnMainThread:@selector(loadTopbarView) withObject:nil waitUntilDone:YES];
    statusSave = NO;
    
}

- (void)viewDidUnload
{
    [self setTbvLaundryCategory:nil];
    [self setBtnInstruction:nil];
    [self setBtnLaundryAddToCart:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark UITableView Delegate methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return datas.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    SectionLaundryInfoV2 *sectioninfo = [datas objectAtIndex:section];
    NSInteger numberRowInSection = sectioninfo.rowHeights.count;
    return sectioninfo.open ? numberRowInSection : 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 60;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    SectionLaundryInfoV2 *sectionInfo = [datas objectAtIndex:indexPath.section];
    LaundryItemModelV2 *model = [sectionInfo.rowHeights objectAtIndex:indexPath.row];
    CGSize t = [model.laundryItemName sizeWithFont:[UIFont systemFontOfSize:17] constrainedToSize:CGSizeMake(128, 1000) lineBreakMode:UILineBreakModeWordWrap];
    return t.height + 49;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    SectionLaundryInfoV2 *sectioninfo = [datas objectAtIndex:section];
    
    //set language for laundry category
    NSString *nameLaundryCategory = @"";
    if ([[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString: (NSString *)ENGLISH_LANGUAGE] == NO) {
        nameLaundryCategory = sectioninfo.laundryCategory.laundryCategoryNameLang;
    }
    else{
        nameLaundryCategory = sectioninfo.laundryCategory.laundryCategoryName;
    }
    
    //show laundry category in section
    if (sectioninfo.headerView == nil) {
        SectionLaundryCategoryViewV2 *sview = [[SectionLaundryCategoryViewV2 alloc] initWithSection:section contentImg:sectioninfo.laundryCategory.laundryCategoryImg contentName:nameLaundryCategory AndStatusArrow:sectioninfo.open];
        sectioninfo.headerView = sview;
        [sview setDelegate:self];
    }
    return sectioninfo.headerView;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifyCell = @"CellIndetify";
    LaundryItemCellV2 *cell = nil;
    
    cell = (LaundryItemCellV2 *)[tableView dequeueReusableCellWithIdentifier:identifyCell];
    
    if (cell == nil) {
        NSArray *array = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([LaundryItemCellV2 class]) owner:self options:nil];
        cell = [array objectAtIndex:0];
    }
    
    //config datas for cell
    SectionLaundryInfoV2 *sectionInfo = [datas objectAtIndex:indexPath.section];
    LaundryItemModelV2 *model = [sectionInfo.rowHeights objectAtIndex:indexPath.row];
    [cell.imgLaundryItemCell setImage:[UIImage imageWithData:model.laundryItemImg]];
    
    //set language for laundry item name
    if ([[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString: (NSString *)ENGLISH_LANGUAGE] == NO) {
        cell.lblNameLaundryItemCell.text = model.laundryItemLang;

    }
    else{
        cell.lblNameLaundryItemCell.text = model.laundryItemName;
    }
       
    [cell.lblNameLaundryItemCell  setLineBreakMode:UILineBreakModeWordWrap];
    [cell.lblNameLaundryItemCell sizeToFitFixedWidth:128];
    CGSize t = [model.laundryItemName sizeWithFont:[UIFont systemFontOfSize:17] constrainedToSize:CGSizeMake(128, 1000) lineBreakMode:UILineBreakModeWordWrap];
    [cell.lblSeparationItemCell setFrame:CGRectMake(7,48+t.height ,340 , 1)];
    if (statusAddToCart == YES) {
        [cell.btnQtyLaundryItemCell setTitle:[NSString stringWithFormat:@"%d",0] forState:UIControlStateNormal];

    }
    else
    {
        
        [cell.btnQtyLaundryItemCell setTitle:[NSString stringWithFormat:@"%d",model.laundryItem_Quantity] forState:UIControlStateNormal];

    }
    
    [cell.lblQtyLaundryItemCell setText:[[LanguageManagerV2 sharedLanguageManager] getQuantity]];
        
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell setBackgroundColor:[UIColor clearColor]];
    [cell setDelegate:self];
    [cell setIndexpath:indexPath];
    return cell;
}


#pragma mark - Section Header Delegate

-(void)sectionHeaderView:(SectionLaundryCategoryViewV2 *)sectionheaderView sectionOpened:(NSInteger)section {
    SectionLaundryInfoV2 *sectionInfo = [datas objectAtIndex:section];
    sectionInfo.open = YES;
    
    NSInteger countOfRowsToInsert = [sectionInfo.rowHeights count];
    if (countOfRowsToInsert == 0) {
        return;
    }
    NSMutableArray *indexPathsToInsert = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i < countOfRowsToInsert; i++) {
        [indexPathsToInsert addObject:[NSIndexPath indexPathForRow:i
                                                         inSection:section]];
    }
    
    UITableViewRowAnimation insertAnimation;
    insertAnimation = UITableViewRowAnimationTop;
    
    [self.tbvLaundryCategory beginUpdates];
    
    [self.tbvLaundryCategory insertRowsAtIndexPaths:indexPathsToInsert
                                   withRowAnimation:insertAnimation];
    [self.tbvLaundryCategory endUpdates];
    
    [self.tbvLaundryCategory scrollToRowAtIndexPath:[indexPathsToInsert objectAtIndex:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

-(void)sectionHeaderView:(SectionLaundryCategoryViewV2 *)sectionheaderView sectionClosed:(NSInteger)section {
    SectionLaundryInfoV2 *sectionInfo = [datas
                                         objectAtIndex:section];
    
    sectionInfo.open = NO;
    NSInteger countOfRowsToDelete = [self.tbvLaundryCategory
                                     numberOfRowsInSection:section];
    
    if (countOfRowsToDelete > 0) {
        NSMutableArray *indexPathsToDelete = [[NSMutableArray alloc] init];
        for (NSInteger i = 0; i < countOfRowsToDelete; i++) {
            [indexPathsToDelete addObject:[NSIndexPath indexPathForRow:i
                                                             inSection:section]];
        }
        
        [tbvLaundryCategory beginUpdates];
        [self.tbvLaundryCategory deleteRowsAtIndexPaths:indexPathsToDelete
                                       withRowAnimation:UITableViewRowAnimationTop];
        [tbvLaundryCategory endUpdates];
        
       // [indexPathsToDelete release];
    }
}

#pragma mark - LaundryItemCellV2 Delegate Methods
-(void)btnQtyLaundryItemCellPressedWithIndexPath:(NSIndexPath *)indexpath AndSender:(UIButton *)sender{
    //statusAddToCart = NO;
    NSMutableArray *array =[NSMutableArray array];
    for (NSInteger index = 0; index <= 10; index++) {
        [array addObject:[NSString stringWithFormat:@"%d", index]];
    }

    PickerViewV2 *picker = [[PickerViewV2 alloc] initPickerViewV2WithDatas:array AndSelectedData:sender.titleLabel.text AndIndex:indexpath];
    picker.delegate = self;
    [picker showPickerViewV2];   
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

-(void)didChoosePickerViewV2WithData:(NSString *)data AndIndex:(NSIndexPath *)index{
    SectionLaundryInfoV2 *sectionInfo = [datas objectAtIndex:index.section];
    LaundryItemModelV2 *model = [sectionInfo.rowHeights objectAtIndex:index.row];
    if ([data integerValue] > 0) {
        statusAddToCart = NO;
    }
    model.laundryItem_Quantity = [data integerValue];
        
    [tbvLaundryCategory reloadRowsAtIndexPaths:[NSArray arrayWithObject:index] withRowAnimation:UITableViewRowAnimationFade];
}

#pragma mark - Load Data Laundry Item From Database
-(void)setService:(tServiceLaundry)tservice AndGender:(tGenderLaundry)gender
{
    tService = tservice;
    tgender = gender;
}

-(void)loadDataLaundryItemFromDatabase
{
    //enable flag to announce, is loading data
    [[HomeViewV2 shareHomeView] waitingLoadingData];

    
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getRaLoadingData]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES]; 
    
    SyncManagerV2 *synManager = [[SyncManagerV2 alloc] init];
    datas=[[NSMutableArray alloc] init];
    int userID=[[UserManagerV2 sharedUserManager] currentUser].userId ;
    NSMutableArray *categoryArray;
    
    //--------------------------Get Laundry Item WS----------------------------
    NSString *lastModifiedItem = [[LaundryManagerV2 sharedLaundryManagerV2] getTheLatestModifiedInLaundryItem];
    BOOL resultItem = [synManager getAllLaundryItemByUserId:[NSNumber numberWithInt:userID ] andmodifiedDate:lastModifiedItem];
    
    NSString *lastModifiedItemPrice = [[LaundryManagerV2 sharedLaundryManagerV2] getTheLatestModifiedInLaundryItemPrice];
    BOOL resultItemPrice = [synManager getAllLaundryItemPriceByUserId:[NSNumber numberWithInt:userID] andmodifiedDate:lastModifiedItemPrice];
    
    if (resultItem == NO || resultItemPrice == NO) {
        //show alert no network
        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:[[LanguageManagerV2 sharedLanguageManager] getNoWifi] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles: nil]; 
        alert.tag = 1;
        [alert show];
        
    }
    //--------------------------End--------------------------------------------
    
    //load laundry category from local database
    categoryArray=[[LaundryManagerV2 sharedLaundryManagerV2] loadAllLaundryCategoryModelV2];
    
    for (int i=0; i<[categoryArray count]; i++) {
        
        categoryObj=[categoryArray objectAtIndex:i];
        
        //load all laundry item price by laundry category id
        NSMutableArray *itemPriceArray=[[LaundryManagerV2 sharedLaundryManagerV2] loadAllLaundryItemPriceModelV2ID:categoryObj.laundryCategoryId];
        
        SectionLaundryInfoV2 *section1=[[SectionLaundryInfoV2 alloc] init];
        section1.laundryCategory = categoryObj;
        
        if ([itemPriceArray count] > 0) {
            for (LaundryItemPriceModelV2 *ipModel in itemPriceArray) {
                
                //load laundry item by laundry item id and gender
                LaundryItemModelV2 *itemObj=[[LaundryManagerV2 sharedLaundryManagerV2] loadLaundryItemByItemId:ipModel.lip_item_id andGender:tgender];
                
                itemObj.laundryItem_Price=ipModel.lip_price;
                if (itemObj.laundryItemId != 0 && itemObj.laundryItemName) {
                    [section1 insertObjectToNextIndex:itemObj];
                }
                else
                {
                    continue;
                }
                
            }
        }
        
        [datas addObject:section1];    
        
    }
    
    statusAddToCart = YES;
    [tbvLaundryCategory reloadData];
    [HUD hide:YES];
    [HUD removeFromSuperview];
    
    
    //end loading data
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
}

#pragma mark - Add to cart Press
-(void) handleBtnAddToCartPressed
{
    LaundryManagerV2 *manager = [[LaundryManagerV2 alloc] init];
    
    for (NSInteger indexSection=0; indexSection < datas.count; indexSection ++) {
        SectionLaundryInfoV2 *sectionInfo = [datas objectAtIndex:indexSection];
        for (NSInteger indexRow=0; indexRow < sectionInfo.rowHeights.count; indexRow++) {
            LaundryItemModelV2 *model = [sectionInfo.rowHeights objectAtIndex:indexRow];
            if (model.laundryItem_Quantity > 0) {
                
                //load laundry cart temp from database
                LaundryOrderDetailModelV2 *orderTemp = [[LaundryOrderDetailModelV2 alloc] init];
                orderTemp.lod_item_id = model.laundryItemId;
                orderTemp.lod_category_id = sectionInfo.laundryCategory.laundryCategoryId;
                orderTemp.lod_service_id = tService;
                [manager loadLaundryOrderDetailsTemp:orderTemp];
                
                if (orderTemp.lod_quantity == 0) {
                    
                    //insert to laundry item cart temp
                    LaundryOrderDetailModelV2 *orderTempDB = [[LaundryOrderDetailModelV2 alloc] init];
                    orderTempDB.lod_laundry_order_id = 0;
                    orderTempDB.lod_item_id = model.laundryItemId;
                    orderTempDB.lod_quantity = model.laundryItem_Quantity;
                    orderTempDB.lod_service_id = tService;
                    orderTempDB.lod_category_id = sectionInfo.laundryCategory.laundryCategoryId;
                    orderTempDB.lod_price = model.laundryItem_Price;
                    
                    [manager insertLaundryOrderDetailTemp:orderTempDB];
                }
                else
                {
                    //update to laundry item cart temp
                    orderTemp.lod_quantity = model.laundryItem_Quantity + orderTemp.lod_quantity;
                    
                    [manager updateLaundryOrderDetailTemp:orderTemp];
                    
                }
                
                model.laundryItem_Quantity = 0;
            }
        }
    }
    
    
    NSInteger count = 0;
    //load all laundry cart temp
    NSMutableArray *arrayOrderTemp = [manager loadAllLaundryOrderDetailModelV2Temp];
    
    //load all laundry service
    NSMutableArray *arrayLaundryService = [manager loadAllLaundryServicesModelV2];
    
    //load laundry order 
    LaudryOrderModelV2 *laundryOrder = [[LaudryOrderModelV2 alloc] init];
    laundryOrder.laundryOrderRoomId = [TasksManagerV2 getCurrentRoomAssignment];
    laundryOrder.laundryOrderUserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    laundryOrder.laundryOrderStatus = tStatusChecked;
    laundryOrder.laundryOrderCollectDate = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd"];
    
    for (LaundryServicesModelV2 *laundryService in arrayLaundryService) {
        
        BOOL isCheckLaundryOrder = [manager isCheckLaundryOrder:laundryOrder];
        
        if (isCheckLaundryOrder == TRUE) {
            
            [manager loadLaundryOrdersModelByRoomIdUserIdAndCollectDate:laundryOrder];
            
            NSMutableArray *arrayOrderDetail = [manager loadAllLaundryOrderDetailsByServiceId:laundryService.lservices_id AndOrderId:laundryOrder.laundryOrderId];
            
            for (LaundryOrderDetailModelV2 *orderDetail in arrayOrderDetail) {
                count += orderDetail.lod_quantity;
            }
            
        }
        
    }
    
    for (LaundryOrderDetailModelV2 *temp in arrayOrderTemp) {
        count += temp.lod_quantity;
    }
    
    
    [btnCart setTitle:[NSString stringWithFormat:@"%d",count] forState:UIControlStateNormal];
    
}

- (IBAction)btnLaundryAddtoCartPressed:(id)sender {
    
    statusSave = YES;
    statusChooseSave = YES;
    statusAddToCart = YES;
    [self handleBtnAddToCartPressed];
    [tbvLaundryCategory reloadData];
    
}

#pragma mark - Intruction Press
- (IBAction)btnInstructionPressed:(id)sender{
    //push instruction view controller
    InstructionViewControllerV2 *instructionView = [[InstructionViewControllerV2 alloc] initWithNibName:@"InstructionViewControllerV2" bundle:nil];
    [self.navigationController pushViewController:instructionView animated:YES];
}

#pragma mark - Cart Press
-(void) btnCartPressed {
    //clear all data on view before go to cart
    for (NSInteger indexSection=0; indexSection < datas.count; indexSection ++) {
        SectionLaundryInfoV2 *sectionInfo = [datas objectAtIndex:indexSection];
        for (NSInteger indexRow=0; indexRow < sectionInfo.rowHeights.count; indexRow++) {
            LaundryItemModelV2 *model = [sectionInfo.rowHeights objectAtIndex:indexRow];
            if (model.laundryItem_Quantity > 0) {
                model.laundryItem_Quantity = 0;
                statusAddToCart = YES;
            }
        }
    }
    
    LaundryCartViewV2 *laundryCartView = [[LaundryCartViewV2 alloc] initWithNibName:NSStringFromClass([LaundryCartViewV2 class]) bundle:nil];
    
    laundryCartView.dataLaundryCarts = datasLaundryCart;
    
    if (tService == tRegular) {
        laundryCartView.titleSectionLaundryCart = [[LanguageManagerV2 sharedLanguageManager] getRegular];
    } 
    else {
        laundryCartView.titleSectionLaundryCart = [[LanguageManagerV2 sharedLanguageManager] getExpress];
    }
    

    laundryCartView.laundry_order_Object = lrOrderObject;
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK] style:UIBarButtonItemStyleBordered target:nil action:nil];
    self.navigationItem.backBarButtonItem=backButton;
        
    if (statusSave == NO && statusChooseSave == NO) {
        laundryCartView.statusChooseSave = NO;
    }
    else
    {
        laundryCartView.statusChooseSave = YES;
    }
    
    laundryCartView.delegate=self;
    
    [self.navigationController pushViewController:laundryCartView animated:YES];
    
}

#pragma mark - Load Laundry Cart
-(void) addLaundrytoSectionWithStatus:(BOOL) status LaundryItem: (LaundryOrderDetailModelV2 *) item AndSection:(SectionLaundryCartInfoV2 *) sectionLaundryCart
{
    
    LaundryManagerV2 *manager = [[LaundryManagerV2 alloc] init];
    
    if (status == NO) {
        
        //load service
        LaundryServicesModelV2 *service = [[LaundryServicesModelV2 alloc] init];
        service.lservices_id = item.lod_service_id;
        [manager loadLaundryServiceModelV2:service];
        
        sectionLaundryCart.laundryService = service;
        
        //load category
        LaundryCategoryModelV2 *laundryCategory = [[LaundryCategoryModelV2 alloc] init];
        laundryCategory.laundryCategoryId = item.lod_category_id;
        [manager loadLaundryCategoryModelV2ByLaundryCategory:laundryCategory];
        
        sectionLaundryCart.laundryCategory = laundryCategory;
        
        //load laundry item
        LaundryItemModelV2 *modelLaundryItem = [[LaundryItemModelV2 alloc] init];
        modelLaundryItem.laundryItemId = item.lod_item_id;
        modelLaundryItem = [manager loadLaundryItemByItemID:modelLaundryItem];
        
        //load item price for laundry item
        if (item.lod_price == 0) {
            LaundryItemPriceModelV2 *laundryItemPrice = nil;
            laundryItemPrice = [manager loadLaundryItemPriceModelByLaundryItemId:item.lod_item_id andLaundryCategoryId:item.lod_category_id];
            item.lod_price = laundryItemPrice.lip_price;
        }
        
        
        //insert item
        modelLaundryItem.laundryItem_Quantity = item.lod_quantity;
        modelLaundryItem.laundryItem_Price = item.lod_price * item.lod_quantity;
        
        [sectionLaundryCart insertObjectToNextIndex:modelLaundryItem];
        
        //set total & subtotal
        total += modelLaundryItem.laundryItem_Quantity;
        subTotal += modelLaundryItem.laundryItem_Price;
    }
    else
    {
        //insert item
        //load laundry item
        LaundryItemModelV2 *modelLaundryItem = [[LaundryItemModelV2 alloc] init];
        modelLaundryItem.laundryItemId = item.lod_item_id;
        modelLaundryItem = [manager loadLaundryItemByItemID:modelLaundryItem];
        
        //load item price for laundry item
        if (item.lod_price == 0) {
            LaundryItemPriceModelV2 *laundryItemPrice = nil;
            laundryItemPrice = [manager loadLaundryItemPriceModelByLaundryItemId:item.lod_item_id andLaundryCategoryId:item.lod_category_id];
            item.lod_price = laundryItemPrice.lip_price;
        }
        
        //insert item
        modelLaundryItem.laundryItem_Quantity = item.lod_quantity;
        modelLaundryItem.laundryItem_Price = item.lod_price * item.lod_quantity;;
        
        [sectionLaundryCart insertObjectToNextIndex:modelLaundryItem];
        
        //set total & subtotal
        total += modelLaundryItem.laundryItem_Quantity;
        subTotal += modelLaundryItem.laundryItem_Price;
    }
    
}

-(void)loadDataForCartView
{
    
    LaundryManagerV2 *manager = [[LaundryManagerV2 alloc] init];
    datasLaundryCart = [NSMutableArray array];
    SectionLaundryCartInfoV2 *sectionLaundryCartInfo = nil;
    
    //load service
    NSMutableArray *arrayService = [manager loadAllLaundryServicesModelV2];
    
    //Laundry Order
    LaudryOrderModelV2 *laundryOrder = [[LaudryOrderModelV2 alloc] init];
    laundryOrder.laundryOrderRoomId = [TasksManagerV2 getCurrentRoomAssignment];
    laundryOrder.laundryOrderUserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    laundryOrder.laundryOrderStatus = tStatusChecked;
    laundryOrder.laundryOrderCollectDate = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd"];
    
    BOOL isCheckLaundryOrder = [manager isCheckLaundryOrder:laundryOrder];
    
    for (LaundryServicesModelV2 *service in arrayService) {
        
        if (isCheckLaundryOrder == TRUE) {
            
            [manager loadLaundryOrdersModelByRoomIdUserIdAndCollectDate:laundryOrder];
            
            //load All llaundry Order Detail By Order Id
            NSMutableArray *arrayOrderDetail = [manager loadAllLaundryOrderDetailsByOrderId:laundryOrder.laundryOrderId];
            
            
            for (LaundryOrderDetailModelV2 *orderDetail in arrayOrderDetail) {
                
                //check item in dataLaundryCart
                BOOL isSection = NO;
                for (NSInteger indexSection=0; indexSection < datasLaundryCart.count; indexSection ++) {
                    
                    SectionLaundryCartInfoV2 *sectionCart = [datasLaundryCart objectAtIndex:indexSection];
                    
                    if (sectionCart.laundryService.lservices_id == orderDetail.lod_service_id && sectionCart.laundryCategory.laundryCategoryId == orderDetail.lod_category_id) {
                        
                        [self addLaundrytoSectionWithStatus:YES LaundryItem:orderDetail AndSection:sectionLaundryCartInfo];
                        isSection = YES;
                        break;
                    }
                }
                
                if (isSection == NO) {
                    //new section
                    sectionLaundryCartInfo = [[SectionLaundryCartInfoV2  alloc] init];
                    [self addLaundrytoSectionWithStatus:NO LaundryItem:orderDetail AndSection:sectionLaundryCartInfo];
                    //insert section to cart
                    if (sectionLaundryCartInfo.rowHeights.count > 0) {
                        [datasLaundryCart addObject:sectionLaundryCartInfo];
                    }
                }
            }
            
        }
        
    }
    
    //insert item in laundry cart temp
    //load laundry item temp
    NSMutableArray *arrayOrderTemp = [manager loadAllLaundryOrderDetailModelV2Temp];
    
    for (LaundryOrderDetailModelV2 *temp in arrayOrderTemp) {
        
        BOOL isCheckTemp = NO;
        BOOL isCheckSection = YES;
        SectionLaundryCartInfoV2 *sectionCart;
        SectionLaundryCartInfoV2 *sectionIsFind;
        
        //check laundry in dataLaundryCart
        for (NSInteger indexSection=0; indexSection < datasLaundryCart.count; indexSection ++) {
            
            sectionCart = [datasLaundryCart objectAtIndex:indexSection];
            
            for (LaundryItemModelV2 *item in sectionCart.rowHeights) {
                
                if (sectionCart.laundryService.lservices_id == temp.lod_service_id && sectionCart.laundryCategory.laundryCategoryId == temp.lod_category_id) {
                    isCheckSection = NO;
                    sectionIsFind = sectionCart;
                }
                
                if (sectionCart.laundryService.lservices_id == temp.lod_service_id && sectionCart.laundryCategory.laundryCategoryId == temp.lod_category_id && item.laundryItemId == temp.lod_item_id) {
                    
                    item.laundryItem_Quantity += temp.lod_quantity;
                    
                    //update total
                    total += temp.lod_quantity;
                    //update subtotal
                    subTotal += temp.lod_quantity * temp.lod_price;
                    
                    isCheckTemp = YES;
                    goto outOfCheckSectionCart;
                }
                
            }
            
            if (isCheckSection == NO) {
                goto outOfCheckSectionCart;
            }
            
        }
        
        //label for check out check section
    outOfCheckSectionCart:
        
        if (isCheckSection == YES) {
            
            //new section
            sectionLaundryCartInfo = [[SectionLaundryCartInfoV2  alloc] init];
            [self addLaundrytoSectionWithStatus:NO LaundryItem:temp AndSection:sectionLaundryCartInfo];
            
            //insert section to cart
            if (sectionLaundryCartInfo.rowHeights.count > 0) {
                [datasLaundryCart addObject:sectionLaundryCartInfo];
            }
            
        } else {
            if (isCheckTemp == NO) {
                
                [self addLaundrytoSectionWithStatus:YES LaundryItem:temp AndSection:sectionIsFind];
            }
        }
        
        
        
    }
    
}


#pragma mark - Click back
-(void)backBarPressed {
    //clear previous tag of event
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:TAG_OF_EVENT];
    
    if(statusAddToCart == YES)
    {
        
        [self.navigationController popViewControllerAnimated:YES];
        
    }
    else
    {
        //show alert add to cart before leaving
        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:    msgSaveCount = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_confirm_add_to_cart] delegate:self cancelButtonTitle:msgbtnYes otherButtonTitles:msgbtnNo, nil]; 
        alert.delegate = self;
        alert.tag = tagAlertBack;
        [alert show];
    }
}

#pragma mark - === Handle Check Save Data ===
#pragma mark
-(BOOL) isCheckLaundryOrderTemp
{
    NSMutableArray *array = [[LaundryManagerV2 sharedLaundryManagerV2] loadAllLaundryOrderDetailModelV2Temp];
    if (array.count > 0) {
        return YES;
    }
    else
    {
        return NO;
    }
}
-(BOOL) checkSaveDataWithTagEvent:(NSInteger) tagOfEvent {
    
    BOOL isCheckOrder = [self isCheckLaundryOrderTemp];
    if (tagOfEvent == tagOfMessageButton) {
        if (isCheckOrder == NO) {
            return NO;
        }
        else
        {
            //Show alert please save before leaving
            UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:msgSaveCount delegate:self cancelButtonTitle:msgbtnYes otherButtonTitles:msgbtnNo, nil]; 
            alert.delegate = self;
            alert.tag = tagAlertBottomBar;
            [alert show];
            
            return YES;
        }
    }
    else
    {
        //Show alert complete room
        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_complete_room_to_leave] delegate:self cancelButtonTitle:msgbtnYes otherButtonTitles: nil]; 
        alert.delegate = self;
        [alert show];
        
        return YES;
    }
    
}

#pragma mark - UIAlertView Delegate Methods
-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    switch (alertView.tag) {
        case tagAlertBack:
        {
            switch (buttonIndex)
            {
                case 0:
                {
                    //YES: add to cart
                    [self btnLaundryAddtoCartPressed:btnCart];
                    
                }
                    break;
                    
                case 1:
                {
                    //NO: show popup discard
                    UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:msgDiscardCount delegate:self cancelButtonTitle:msgbtnYes otherButtonTitles:msgbtnNo, nil]; 
                    alert.delegate = self;
                    alert.tag = tagAlertDisCard;
                    [alert show];
                    
                }
                    break;
                    
                default:
                    break;
            }
        }
            break;
            
        case tagAlertBottomBar:
        {
            switch (buttonIndex)
            {
                case 0:
                {
                    //YES: show alert dispatch for pickup
                    UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ios_laundry_please_dispatch_for_pickup] delegate:self cancelButtonTitle:msgbtnYes otherButtonTitles:msgbtnNo, nil]; 
                    alert.delegate = self;
                    alert.tag = tagAlertDispatch;
                    [alert show];
                    
                }
                    break;
                    
                case 1:
                {
                    
                    //NO: show alert discart all inputs
                    UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:msgDiscardCount delegate:self cancelButtonTitle:msgbtnYes otherButtonTitles:msgbtnNo, nil]; 
                    alert.delegate = self;
                    alert.tag = tagAlertDisCard;
                    [alert show];
                    
                }
                    break;
                    
                default:
                    break;
            }
            
        }
            break;
            
        case tagAlertDispatch:
        {
            switch (buttonIndex)
            {
                case 0:
                {
                    //YES: in dispatch for pickup will save and post to server
                    [self saveLaundryCart:YES];
                    statusSave = YES;
                    statusAddToCart = YES;
                    
                    //if tag is message
                    if ([[NSUserDefaults standardUserDefaults] integerForKey:TAG_OF_EVENT] == tagOfMessageButton) {
                        [[HomeViewV2 shareHomeView] handleTabbarEventWithTagOfButton:[[NSUserDefaults standardUserDefaults] integerForKey:TAG_OF_EVENT]];
                    }
                    
                }
                    break;
                    
                case 1:
                {
                    //NO: in dispatch for pickup will save but do not post server
                    [self saveLaundryCart:NO];
                    statusSave = YES;
                    statusAddToCart = YES;
                    
                    //if tag is message
                    if ([[NSUserDefaults standardUserDefaults] integerForKey:TAG_OF_EVENT] == tagOfMessageButton) {
                        [[HomeViewV2 shareHomeView] handleTabbarEventWithTagOfButton:[[NSUserDefaults standardUserDefaults] integerForKey:TAG_OF_EVENT]];
                    }
                    
                }
                    break;
                    
                default:
                    break;
            }
            
        }
            break;
            
        case tagAlertDisCard:
        {
            switch (buttonIndex)
            {
                case 0:
                {
                    //YES: in discart all input will go out, clear cart temp
                    [[LaundryManagerV2 sharedLaundryManagerV2] deleteAllLaundryOrderDetailTempData];
                    
                    //if tag is message
                    if ([[NSUserDefaults standardUserDefaults] integerForKey:TAG_OF_EVENT] == tagOfMessageButton) {
                        [[HomeViewV2 shareHomeView] handleTabbarEventWithTagOfButton:[[NSUserDefaults standardUserDefaults] integerForKey:TAG_OF_EVENT]];
                    }
                    else{
                        //back to count menu, no save
                        NSArray *arrayController = [self.navigationController viewControllers];
                        for (UIViewController *viewController in arrayController) {
                            if ([viewController isKindOfClass:[CountViewV2 class]]) {
                                [self.navigationController popToViewController:viewController animated:YES];
                                break;
                            }
                        }
                    }
                    
                    
                }
                    break;
                    
                case 1:
                {
                    //NO: in discart all input will go cart screen
                    //[self btnLaundryAddtoCartPressed:nil];
                    //[self btnCartPressed];
                    
                }
                    break;
                    
                default:
                    break;
            }
            
        }
            break; 
            
        default:
            break;
    }
}

#pragma mark - Save Laundry
-(void)saveData{
    BOOL isCheckOrder = [self isCheckLaundryOrderTemp];
    if (isCheckOrder == YES) {
        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:imgYesButton], [UIImage imageNamed:imgNoButton], nil] title:[[LanguageManagerV2 sharedLanguageManager]getAlertTitle] message:[[LanguageManagerV2 sharedLanguageManager] getMsg_Dispatch_This_For_PickUp] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles:  [[LanguageManagerV2 sharedLanguageManager] getNo], nil];   
        [alert setTag:tagAlertDispatch];
        [alert show];

    }
    statusSave = YES;
    statusAddToCart = YES;
    statusChooseSave = NO;
}

-(void)saveLaundryCart:(BOOL) postServer
{
    // @ show loading data .
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getRaSavingData]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES]; 
    
    ///Save laundry cart
    LaundryManagerV2 *manager = [[LaundryManagerV2 alloc] init];
    SyncManagerV2 *synManager = [[SyncManagerV2 alloc] init];
    NSInteger userId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    NSInteger hotelId = [[[UserManagerV2 sharedUserManager] currentUser] userHotelsId];
    NSInteger roomAssignId = [TasksManagerV2 getCurrentRoomAssignment];
    
    //load order in laundry
    LaudryOrderModelV2 *laundryOrder = [[LaudryOrderModelV2 alloc] init];
    laundryOrder.laundryOrderUserId = userId;
    laundryOrder.laundryOrderRoomId = roomAssignId;
    laundryOrder.laundryOrderStatus = tStatusChecked;
    laundryOrder.laundryOrderCollectDate = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd"]; 
    
    BOOL isCheckLaundryOrder = [manager isCheckLaundryOrder:laundryOrder];
    
    if (isCheckLaundryOrder == FALSE) {
        [manager insertLaudryOrderModelV2:laundryOrder];
    }
    
    //load laundry order
    [manager loadLaundryOrdersModelByRoomIdUserIdAndCollectDate:laundryOrder];
    
    
    //load all laundry service
    NSMutableArray *arrayService = [manager loadAllLaundryServicesModelV2];
    
    for (LaundryServicesModelV2 *service in arrayService) {
        
        //--------------------get laundry order Id--------------------
        int orderIdWS = 0;
        if (postServer == YES) {
            //load laundry item to get total & subtotal
            if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
                [self loadDataForCartView];
                orderIdWS = [synManager postLaundryOrder:laundryOrder andsubQuantity:total andServiceID:service.lservices_id andHotel:hotelId andsubTotal:subTotal];
            }
            
        }
        //--------------------WS--------------------------------------
        
        //load all laundry order detail temp by service id
        NSMutableArray *arrayLaundryOrderDetailTemp = [manager loadAllLaundryOrderDetailsTempByServiceId:service.lservices_id];
        
        if (arrayLaundryOrderDetailTemp.count > 0) {
            
            for (LaundryOrderDetailModelV2 *orderTemp in arrayLaundryOrderDetailTemp) {
                
                //load laundry order detail by order id
                LaundryOrderDetailModelV2 *orderDetail = [[LaundryOrderDetailModelV2 alloc] init];
                orderDetail.lod_item_id = orderTemp.lod_item_id;
                orderDetail.lod_service_id = orderTemp.lod_service_id;
                orderDetail.lod_category_id = orderTemp.lod_category_id;
                
                [manager loadLaundryOrderDetailsByItemId:orderDetail AndOrderId:laundryOrder.laundryOrderId];
                
                if (orderDetail.lod_id != 0) {
                    
                    //update Order detail in laundry
                    orderDetail.lod_quantity = orderTemp.lod_quantity + orderDetail.lod_quantity;
                    
                    [manager updateLaundryOrderDetailModelV2:orderDetail];
                }
                else
                {
                    //insert Order detail in laundry
                    LaundryOrderDetailModelV2 *laundryOrderDetailModel = [[LaundryOrderDetailModelV2 alloc] init];
                    laundryOrderDetailModel.lod_item_id = orderTemp.lod_item_id;
                    laundryOrderDetailModel.lod_service_id = orderTemp.lod_service_id;
                    laundryOrderDetailModel.lod_category_id = orderTemp.lod_category_id;
                    laundryOrderDetailModel.lod_post_status = POST_STATUS_SAVED_UNPOSTED;
                    laundryOrderDetailModel.lod_quantity = orderTemp.lod_quantity;
                    laundryOrderDetailModel.lod_laundry_order_id = laundryOrder.laundryOrderId;
                    
                    [manager insertLaundryOrderDetailModelV2:laundryOrderDetailModel];
                    
                }
                
                //--------------------------post to server WS-----------------
                if (postServer == YES) {
                    
                    LaundryOrderDetailModelV2 *orderDetailPost = [[LaundryOrderDetailModelV2 alloc] init];
                    orderDetailPost.lod_item_id = orderTemp.lod_item_id;
                    orderDetailPost.lod_service_id = orderTemp.lod_service_id;
                    orderDetailPost.lod_category_id = orderTemp.lod_category_id;
                    
                    [manager loadLaundryOrderDetailsByItemId:orderDetailPost AndOrderId:laundryOrder.laundryOrderId];
                    
                    BOOL statusPost = [synManager postLaundryItem:orderDetailPost andLaundryOrder:orderIdWS anduser:userId andTransactionTime:laundryOrder.laundryOrderCollectDate];
                    
                    if (statusPost == YES) {
                        //post success, clear database
                        [manager deleteLaundryOrderDetailModelV2:orderDetailPost];
                        
                    }

                }
                //delete laundry order detail temp
                [manager deleteLaundryOrderDetailTemp:orderTemp];
                
                [tbvLaundryCategory reloadData];
                //--------------------------End------------------------------
            }
            
        }
        
    }
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getMsgSaveMessage]];
    //hide saving data.
    [self performSelector:@selector(hiddenHUDAfterSaved:) withObject:HUD afterDelay:0.5];
}

#pragma mark- hidden after save.
-(void) hiddenHUDAfterSaved:(MBProgressHUD *)HUD{
    [HUD hide:YES];
    [HUD removeFromSuperview];
    // [HUD release];
}

#pragma mark - set caption view
-(void)setCaptionView
{
    
    msgbtnNo = [[LanguageManagerV2 sharedLanguageManager] getNo];
    msgbtnYes = [[LanguageManagerV2 sharedLanguageManager] getYes];
    msgSaveCount = [[LanguageManagerV2 sharedLanguageManager] getMsgAlertSaveCounts];
    msgDiscardCount = [[LanguageManagerV2 sharedLanguageManager] getMsgAlertDiscardCounts];
    
    //refresh topview
    TopbarViewV2 *topview = [self getTopBarView];
    [topview refresh:[[SuperRoomModelV2 alloc] init]];
}

-(BOOL)checkLaundryOrderDetailExistInArrayWithLaundryOrderDetail:(LaundryItemModelV2 *)lr_Item_Model_Exist inArray:(NSMutableArray *)lr_order_detail_Array
{
    BOOL isExist=NO;
    for (LaundryOrderDetailModelV2 *lr_Order_Detail_Model in lr_order_detail_Array) {
        if (lr_Order_Detail_Model.lod_item_id==lr_Item_Model_Exist.laundryItemId ) {
            isExist=YES;
        }else
        {
            continue;
        }
    }
    return isExist;
}


#pragma mark - Handle Topbar Methods
-(void)loadTopbarView {
    TopbarViewV2 *topbar = [[TopbarViewV2 alloc] initViewWithSuperModel:[[CommonVariable sharedCommonVariable] superRoomModel]];
    [topbar setTag:tagTopbarView];
    
    [self.view addSubview:topbar];
    [topbar setHidden:YES];
    [self addButtonHandleShowHideTopbar];
}

-(void)showTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationWillStartSelector:@selector(setHiddenTopbarView)];
    
    f = topbar.frame;
    f.origin.y = 0;
    [topbar setFrame:f];
    
    [self adjustShowForViews];
    
    [UIView commitAnimations];
}

-(void)hideTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDidStopSelector:@selector(setHiddenTopbarView)];
    [UIView setAnimationDelegate:self];
    
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    
    [self adjustRemoveForViews];
    
    [UIView commitAnimations];
}

-(void)setHiddenTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    [topbar setHidden:!topbar.hidden];
}

-(TopbarViewV2 *)getTopBarView {
    return (TopbarViewV2 *)[self.view viewWithTag:tagTopbarView];
}

-(void)addButtonHandleShowHideTopbar {
    UIButton *titleBarButton = [[UIButton alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
    
    titleBarButton.titleLabel.backgroundColor = [UIColor clearColor];
    titleBarButton.titleLabel.font = FONT_BUTTON_TOPBAR;
    titleBarButton.titleLabel.textAlignment = UITextAlignmentCenter;
    titleBarButton.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
    [titleBarButton.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
    [titleBarButton.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
    [titleBarButton addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    NSString *titleString = nil;
    
    if (tService == tRegular) {
        titleString = [NSString stringWithFormat:@"%@ - %@",[[LanguageManagerV2 sharedLanguageManager] getRegular], tgender == tFemale ? [[LanguageManagerV2 sharedLanguageManager] getFemale] : [[LanguageManagerV2 sharedLanguageManager] getMale]];
    }
    else{
        titleString = [NSString stringWithFormat:@"%@ - %@",[[LanguageManagerV2 sharedLanguageManager] getExpress], tgender == tFemale ? [[LanguageManagerV2 sharedLanguageManager] getFemale] : [[LanguageManagerV2 sharedLanguageManager] getMale]];
    }
    [titleBarButton setTitle:titleString forState:UIControlStateNormal];
    self.navigationItem.titleView = titleBarButton;
}

-(void)topbarTapped:(UIButton *)sender {
    TopbarViewV2 *topbar = [self getTopBarView];
    if (topbar.isHidden) {
        [self showTopbarView];
    } else {
        [self hideTopbarView];
    }
}

-(void)adjustShowForViews {
    //adjust all views when show 
    [tbvLaundryCategory setFrame:CGRectMake(0, 60, 320, 320)];
    
}

-(void)adjustRemoveForViews { 
    //adjust all views when hide
    [tbvLaundryCategory setFrame:CGRectMake(0,0, 320, 350)];
    
}

#pragma Mark- Process Delegate from LaundryCartViewV2
-(void)transferCartFromCartView:(LaudryOrderModelV2 *)lr_Order_Model
{
    
    if (lr_Order_Model) {
        self.lrOrderObject=lr_Order_Model;
    }
   
}
-(void)getAllItemNumber
{
    item_Total=0;
    for (int i=0; i<[self.lrOrderObject.laundryOrderDetails count]; i++) {
        LaundryOrderDetailModelV2 *lr_order_detail_model=[lrOrderObject.laundryOrderDetails objectAtIndex:i];
        item_Total+=lr_order_detail_model.lod_quantity;
    }
    [btnCart setTitle:[NSString stringWithFormat:@"%d",self.item_Total] forState:UIControlStateNormal];
}
@end
