//
//  LaundryViewV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 1/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LaundryCellV2.h"
#import "LaundryCategoryViewV2.h"
#import "InstructionViewControllerV2.h"
#import "LaundryCartViewV2.h"
#import "MBProgressHUD.h"
#import "CustomAlertViewV2.h"
#import "AccessRight.h"

@interface LaundryViewV2 : UIViewController<UITableViewDataSource,UITableViewDelegate>{
    InstructionViewV2 *instruction;
    NSMutableArray *laundryServiceArray;
    int total;
    CGFloat subTotal;
    NSMutableArray * datasLaundryCart;
    AccessRight *QRCodeScanner;
    AccessRight *postingLaundry;
}
@property (nonatomic)int total;
@property (nonatomic) CGFloat subTotal;
@property(nonatomic,strong)NSMutableArray * datasLaundryCart;
@property (strong,nonatomic)  NSMutableArray *laundryServiceArray;
@property (strong, nonatomic) IBOutlet UITableView *tbvLaundryCell;
@property (strong, nonatomic) IBOutlet LaundryCellV2 *laundryCell;
@property (strong, nonatomic) IBOutlet UIButton *btnIntruction;

-(IBAction)btnIntructionPressed:(id)sender;
-(void)loadDataLaundryServiceFromLocalDatabase;
@end
