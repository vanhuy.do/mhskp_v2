//
//  LaundryViewV2.m
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 1/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LaundryViewV2.h"
#import "LaundryManagerV2.h"
#import "CommonVariable.h"
#import "LanguageManager.h"
#import "NetworkCheck.h"
#import "TasksManagerV2.h"
#import "CommonVariable.h"
#import "LanguageManagerV2.h"
#import "MyNavigationBarV2.h"
#import "DeviceManager.h"

#define imgRegular @"regular_bt.png"
#define imgExpress @"express_bt.png"
#define tagSaveLeaving 12
#define tagDiscardAll 1002
#define tagDispatchforpickup 1022
#define imgYesButton @"yes_bt.png"
#define imgNoButton @"no_bt.png"
@interface LaundryViewV2 (PrivateMethods)

-(void)saveLaundryCart:(BOOL) postServer;

#define tagTopbarView 1234

-(void) loadTopbarView;
-(void) showTopbarView;
-(void) hideTopbarView;
-(void) setHiddenTopbarView;
-(void) adjustShowForViews;
-(void) adjustRemoveForViews;
-(void) addButtonHandleShowHideTopbar;
-(void) topbarTapped:(UIButton *) sender;
-(TopbarViewV2 *) getTopBarView;

@end

@implementation LaundryViewV2
@synthesize btnIntruction;
@synthesize tbvLaundryCell, laundryCell, laundryServiceArray;
@synthesize total, subTotal, datasLaundryCart;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //Resize content views of current VC to fit with Navigation bar and tabbar
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    //[self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:imgBackground]]];
    
    [self setTitle:[[LanguageManagerV2 sharedLanguageManager] getLaundry]];
    [btnIntruction setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_INSTRUCTION] forState:UIControlStateNormal];
    
    //back button navigation
    self.navigationItem.leftBarButtonItem = [MyNavigationBarV2 createBackButtonWithTarget:self selector:@selector(backBarPressed) forControlEvents:UIControlEventTouchUpInside];
    
    [self performSelector:@selector(loadTopbarView)];
    
    //load data laundry services
//    [self performSelector:@selector(loadDataLaundryServiceFromLocalDatabase) withObject:nil afterDelay:0.1];
    [self performSelectorOnMainThread:@selector(loadDataLaundryServiceFromLocalDatabase) withObject:nil waitUntilDone:NO];
}

-(void)viewWillAppear:(BOOL)animated {
    //show wifi view
    [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationShowWifiView] object:nil];
    [super viewWillAppear:animated];
    
    //refresh topview
    TopbarViewV2 *topview = [self getTopBarView];
    [topview refresh:[[SuperRoomModelV2 alloc] init]];
}

- (void)viewDidUnload
{
    [self setTbvLaundryCell:nil];
    [self setBtnIntruction:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


#pragma mark - UITableView Delegate Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [laundryServiceArray count];
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 97;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *IdentifyCell = @"IdentifyCell";
    LaundryCellV2 *cell = nil;
    laundryCell = [tableView dequeueReusableCellWithIdentifier:IdentifyCell];
    if(cell == nil){
        NSArray *array = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([LaundryCellV2 class]) owner:self options:nil];
        cell = [array objectAtIndex:0];
    }
    LaundryServicesModelV2 *laundryServiceObj;
    if ([laundryServiceArray count]>0) {
         laundryServiceObj=[laundryServiceArray objectAtIndex:indexPath.row];
    }

    //set language for laundry name service
    if ([[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString:(NSString *)SIMPLIFIED_LANGUAGE]) {
        [cell.lblLaundryCell setText:laundryServiceObj.lservices_name_lang];
    }
    else{
        [cell.lblLaundryCell setText:laundryServiceObj.lservices_name];
    }
    
    switch (laundryServiceObj.lservices_id) {
        case 1:
        {
            [cell.imgLaundryCell setImage:[UIImage imageNamed:imgRegular]];

        }
            break;
        case 2:
        {
            [cell.imgLaundryCell setImage:[UIImage imageNamed:imgExpress]];
           
        }
        default:
            break;
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    LaundryCategoryViewV2 *laundryCategory = [[LaundryCategoryViewV2 alloc]initWithNibName:NSStringFromClass([LaundryCategoryViewV2 class]) bundle:nil];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK] style:UIBarButtonItemStyleBordered target:nil action:nil];
    self.navigationItem.backBarButtonItem = backButton;
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [cell setSelected:NO animated:YES];
    
    LaundryServicesModelV2 *laundryServiceObj;
    if ([laundryServiceArray count]>0) {
        laundryServiceObj=[laundryServiceArray objectAtIndex:indexPath.row];
    }

    switch (laundryServiceObj.lservices_id) {
        case 1:
        {
            [laundryCategory setTService:tRegular];
            [laundryCategory.navigationItem setTitle:laundryServiceObj.lservices_name];
                    }
            break;
        case 2:
        {
            [laundryCategory setTService:tExpress];
            [laundryCategory.navigationItem setTitle:laundryServiceObj.lservices_name];
        }
            break;
        default:
            break;
    }
    [laundryCategory setServiceModel:laundryServiceObj];
    [self.navigationController pushViewController:laundryCategory animated:YES];
}

#pragma mark - Intruction Press
- (IBAction)btnIntructionPressed:(id)sender{
    
    //push instruction view controller
    InstructionViewControllerV2 *instructionView = [[InstructionViewControllerV2 alloc] initWithNibName:@"InstructionViewControllerV2" bundle:nil];
    [self.navigationController pushViewController:instructionView animated:YES];
    
}

#pragma mark - Load Laundry Service From Database
-(void)loadDataLaundryServiceFromLocalDatabase
{
    //enable flag to announce, is loading data
    [[HomeViewV2 shareHomeView] waitingLoadingData];
    
    
    /// initialise the order_object in here 
    //get from database and insert into the global variable
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getRaLoadingData]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES];
    
    SyncManagerV2 *synManager = [[SyncManagerV2 alloc] init];
    NSInteger userId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    NSInteger hotelId = [[[UserManagerV2 sharedUserManager] currentUser] userHotelsId];
    
    //------Get data laundry service from WS----------
    NSString *latestModified = [[LaundryManagerV2 sharedLaundryManagerV2] getLastModifierForLaundryService];
    BOOL result = [synManager getAllLaundryServiceByUserID:[NSNumber numberWithInt:userId] atLocation:[NSNumber numberWithInt:hotelId] andDate:latestModified];
    
    if (result == NO) {
        //show alert no network
        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:[[LanguageManagerV2 sharedLanguageManager] getNoWifi] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles: nil]; 
        alert.tag = 1;
        [alert show];
    }
    //-------------------End----------------------------
    
    //load laundry service from local database
    self.laundryServiceArray = [[LaundryManagerV2 sharedLaundryManagerV2] loadAllLaundryServicesModelV2];
    
    [tbvLaundryCell reloadData];
    
    [HUD hide:YES];
    [HUD removeFromSuperview];

    //end loading data
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
}

#pragma mark - Load Laundry Cart
-(void) addLaundrytoSectionWithStatus:(BOOL) status LaundryItem: (LaundryOrderDetailModelV2 *) item AndSection:(SectionLaundryCartInfoV2 *) sectionLaundryCart
{
    
    LaundryManagerV2 *manager = [[LaundryManagerV2 alloc] init];
    
    if (status == NO) {
        
        //load service
        LaundryServicesModelV2 *service = [[LaundryServicesModelV2 alloc] init];
        service.lservices_id = item.lod_service_id;
        [manager loadLaundryServiceModelV2:service];
        
        sectionLaundryCart.laundryService = service;
        
        //load category
        LaundryCategoryModelV2 *laundryCategory = [[LaundryCategoryModelV2 alloc] init];
        laundryCategory.laundryCategoryId = item.lod_category_id;
        [manager loadLaundryCategoryModelV2ByLaundryCategory:laundryCategory];
        
        sectionLaundryCart.laundryCategory = laundryCategory;
        
        //load laundry item
        LaundryItemModelV2 *modelLaundryItem = [[LaundryItemModelV2 alloc] init];
        modelLaundryItem.laundryItemId = item.lod_item_id;
        modelLaundryItem = [manager loadLaundryItemByItemID:modelLaundryItem];
        
        //load item price for laundry item
        if (item.lod_price == 0) {
            LaundryItemPriceModelV2 *laundryItemPrice = nil;
            laundryItemPrice = [manager loadLaundryItemPriceModelByLaundryItemId:item.lod_item_id andLaundryCategoryId:item.lod_category_id];
            item.lod_price = laundryItemPrice.lip_price;
        }

        //insert item
        modelLaundryItem.laundryItem_Quantity = item.lod_quantity;
        modelLaundryItem.laundryItem_Price = item.lod_price * item.lod_quantity;
        
        [sectionLaundryCart insertObjectToNextIndex:modelLaundryItem];
        
        //set total & subtotal
        total += modelLaundryItem.laundryItem_Quantity;
        subTotal += modelLaundryItem.laundryItem_Price;
    }
    else
    {
        //insert item
        //load laundry item
        LaundryItemModelV2 *modelLaundryItem = [[LaundryItemModelV2 alloc] init];
        modelLaundryItem.laundryItemId = item.lod_item_id;
        modelLaundryItem = [manager loadLaundryItemByItemID:modelLaundryItem];
        
        //load item price for laundry item
        if (item.lod_price == 0) {
            LaundryItemPriceModelV2 *laundryItemPrice = nil;
            laundryItemPrice = [manager loadLaundryItemPriceModelByLaundryItemId:item.lod_item_id andLaundryCategoryId:item.lod_category_id];
            item.lod_price = laundryItemPrice.lip_price;
        }

        //insert item
        modelLaundryItem.laundryItem_Quantity = item.lod_quantity;
        modelLaundryItem.laundryItem_Price = item.lod_price * item.lod_quantity;;
        
        [sectionLaundryCart insertObjectToNextIndex:modelLaundryItem];
        
        //set total & subtotal
        total += modelLaundryItem.laundryItem_Quantity;
        subTotal += modelLaundryItem.laundryItem_Price;
    }
    
}

-(void)loadDataForCartView
{
    
    LaundryManagerV2 *manager = [[LaundryManagerV2 alloc] init];
    datasLaundryCart = [NSMutableArray array];
    SectionLaundryCartInfoV2 *sectionLaundryCartInfo = nil;
    
    //load service
    NSMutableArray *arrayService = [manager loadAllLaundryServicesModelV2];
    
    //Laundry Order
    LaudryOrderModelV2 *laundryOrder = [[LaudryOrderModelV2 alloc] init];
    laundryOrder.laundryOrderRoomId = [TasksManagerV2 getCurrentRoomAssignment];
    laundryOrder.laundryOrderUserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    laundryOrder.laundryOrderStatus = tStatusChecked;
    laundryOrder.laundryOrderCollectDate = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd"];
    
    
    BOOL isCheckLaundryOrder = [manager isCheckLaundryOrder:laundryOrder];
    
    for (LaundryServicesModelV2 *service in arrayService) {
        
        if (isCheckLaundryOrder == TRUE) {
            
            [manager loadLaundryOrdersModelByRoomIdUserIdAndCollectDate:laundryOrder];
            
            //load All llaundry Order Detail By Order Id
            NSMutableArray *arrayOrderDetail = [manager loadAllLaundryOrderDetailsByOrderId:laundryOrder.laundryOrderId];
            
            
            for (LaundryOrderDetailModelV2 *orderDetail in arrayOrderDetail) {
                
                //check item in dataLaundryCart
                BOOL isSection = NO;
                for (NSInteger indexSection=0; indexSection < datasLaundryCart.count; indexSection ++) {
                    
                    SectionLaundryCartInfoV2 *sectionCart = [datasLaundryCart objectAtIndex:indexSection];
                    
                    if (sectionCart.laundryService.lservices_id == orderDetail.lod_service_id && sectionCart.laundryCategory.laundryCategoryId == orderDetail.lod_category_id) {
                        
                        [self addLaundrytoSectionWithStatus:YES LaundryItem:orderDetail AndSection:sectionLaundryCartInfo];
                        isSection = YES;
                        break;
                    }
                }
                
                if (isSection == NO) {
                    //new section
                    sectionLaundryCartInfo = [[SectionLaundryCartInfoV2  alloc] init];
                    [self addLaundrytoSectionWithStatus:NO LaundryItem:orderDetail AndSection:sectionLaundryCartInfo];
                    //insert section to cart
                    if (sectionLaundryCartInfo.rowHeights.count > 0) {
                        [datasLaundryCart addObject:sectionLaundryCartInfo];
                    }
                }
            }
            
        }
        
    }
    
    //insert item in laundry cart temp
    //load laundry item temp
    NSMutableArray *arrayOrderTemp = [manager loadAllLaundryOrderDetailModelV2Temp];
    
    for (LaundryOrderDetailModelV2 *temp in arrayOrderTemp) {
        
        BOOL isCheckTemp = NO;
        BOOL isCheckSection = YES;
        SectionLaundryCartInfoV2 *sectionCart;
        SectionLaundryCartInfoV2 *sectionIsFind;
        
        //check laundry in dataLaundryCart
        for (NSInteger indexSection=0; indexSection < datasLaundryCart.count; indexSection ++) {
            
            sectionCart = [datasLaundryCart objectAtIndex:indexSection];
            
            for (LaundryItemModelV2 *item in sectionCart.rowHeights) {
                
                if (sectionCart.laundryService.lservices_id == temp.lod_service_id && sectionCart.laundryCategory.laundryCategoryId == temp.lod_category_id) {
                    isCheckSection = NO;
                    sectionIsFind = sectionCart;
                }
                
                if (sectionCart.laundryService.lservices_id == temp.lod_service_id && sectionCart.laundryCategory.laundryCategoryId == temp.lod_category_id && item.laundryItemId == temp.lod_item_id) {
                    
                    item.laundryItem_Quantity += temp.lod_quantity;
                    
                    //update total
                    total += temp.lod_quantity;
                    //update subtotal
                    subTotal += temp.lod_quantity * temp.lod_price;
                    
                    isCheckTemp = YES;
                    goto outOfCheckSectionCart;
                }
                
            }
            
            if (isCheckSection == NO) {
                goto outOfCheckSectionCart;
            }
            
        }
        
        //label for check out check section
    outOfCheckSectionCart:
        
        if (isCheckSection == YES) {
            
            //new section
            sectionLaundryCartInfo = [[SectionLaundryCartInfoV2  alloc] init];
            [self addLaundrytoSectionWithStatus:NO LaundryItem:temp AndSection:sectionLaundryCartInfo];
            
            //insert section to cart
            if (sectionLaundryCartInfo.rowHeights.count > 0) {
                [datasLaundryCart addObject:sectionLaundryCartInfo];
            }
            
        } else {
            if (isCheckTemp == NO) {
                
                [self addLaundrytoSectionWithStatus:YES LaundryItem:temp AndSection:sectionIsFind];
            }
        }
        
        
        
    }
    
}

#pragma mark - Click back
-(BOOL) isCheckLaundryOrderTemp
{
    NSMutableArray *array = [[LaundryManagerV2 sharedLaundryManagerV2] loadAllLaundryOrderDetailModelV2Temp];
    if (array.count > 0) {
        return YES;
    }
    else
    {
        return NO;
    }
}
-(void)backBarPressed
{
    //clear previous tag of event
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:TAG_OF_EVENT];
    
    // check data in laundry order temp
    BOOL isCheckOrder = [self isCheckLaundryOrderTemp];
    if (isCheckOrder == NO) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        // show message to confirm save before leaving
        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:[[LanguageManagerV2 sharedLanguageManager] getMsgAlertSaveCounts] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles:[[LanguageManagerV2 sharedLanguageManager] getNo], nil]; 
        alert.delegate = self;
        alert.tag = tagSaveLeaving;
        [alert show];
        
    }
    
}

#pragma mark - === Handle Check Save Data ===
#pragma mark
-(BOOL) checkSaveDataWithTagEvent:(NSInteger) tagOfEvent {
    
    BOOL isCheckOrder = [self isCheckLaundryOrderTemp];
    if (tagOfEvent == tagOfMessageButton) {
        if (isCheckOrder == NO) {
            return NO;
        }
        else
        {
            //Show alert please save before leaving
            UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:[[LanguageManagerV2 sharedLanguageManager] getMsgAlertSaveCounts] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles:[[LanguageManagerV2 sharedLanguageManager] getNo], nil]; 
            alert.delegate = self;
            alert.tag = tagSaveLeaving;
            [alert show];
            
            return YES;

        }
    }
    else
    {
        //Show alert complete room
        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_complete_room_to_leave] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles:nil]; 
        alert.delegate = self;
        [alert show];
        
        return YES;

    }
    
}

#pragma mark - UIAlertView Delegate Methods
-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    switch (alertView.tag) {
        case tagSaveLeaving:
        {
            switch (buttonIndex)
            {
                case 0:
                {
                    //YES: show alert dispatch for pickup
                    UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:imgYesButton], [UIImage imageNamed:imgNoButton], nil] title:[[LanguageManagerV2 sharedLanguageManager]getAlertTitle] message:[[LanguageManagerV2 sharedLanguageManager] getMsg_Dispatch_This_For_PickUp] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles:  [[LanguageManagerV2 sharedLanguageManager] getNo], nil];   
                    [alert setTag:tagDispatchforpickup];
                    [alert show];      
                }
                    break;
                case 1:
                {
                    //NO: show alert discart all input
                    UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:imgYesButton], [UIImage imageNamed:imgNoButton], nil] title:[[LanguageManagerV2 sharedLanguageManager]getAlertTitle] message:[[LanguageManagerV2 sharedLanguageManager] getMsgAlertDiscardCounts] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles:  [[LanguageManagerV2 sharedLanguageManager] getNo], nil];   
                    [alert setTag:tagDiscardAll];
                    [alert show];                    
                    
                }
                default:
                    break;
            }
        }
            
            break;
        case tagDispatchforpickup:
        {
            switch (buttonIndex)
            {
                case 0:
                {
                    //YES: in dispatch for pickup will save data and post to server
                    [self saveLaundryCart:YES];
                    
                    //if tag is message
                    if ([[NSUserDefaults standardUserDefaults] integerForKey:TAG_OF_EVENT] == tagOfMessageButton) {
                        [[HomeViewV2 shareHomeView] handleTabbarEventWithTagOfButton:[[NSUserDefaults standardUserDefaults] integerForKey:TAG_OF_EVENT]];
                    }
                    else{
                        [self.navigationController popViewControllerAnimated:YES];
                    }

                }
                    break;
                case 1:
                {
                    //NO: in dispatch for pickup will save data but not post to server
                    [self saveLaundryCart:NO];
                    
                    //if tag is message
                    if ([[NSUserDefaults standardUserDefaults] integerForKey:TAG_OF_EVENT] == tagOfMessageButton) {
                        [[HomeViewV2 shareHomeView] handleTabbarEventWithTagOfButton:[[NSUserDefaults standardUserDefaults] integerForKey:TAG_OF_EVENT]];
                    }
                    else{
                        [self.navigationController popViewControllerAnimated:YES];
                    }

                }
                default:
                    break;
            }
        }
            
            break;
        case tagDiscardAll:
        {
            switch (buttonIndex)
            {
                case 0:
                {
                    //YES: in discard all data will go out, clear cart temp
                    [[LaundryManagerV2 sharedLaundryManagerV2] deleteAllLaundryOrderDetailTempData];
                    
                    //if tag is message
                    if ([[NSUserDefaults standardUserDefaults] integerForKey:TAG_OF_EVENT] == tagOfMessageButton) {
                        [[HomeViewV2 shareHomeView] handleTabbarEventWithTagOfButton:[[NSUserDefaults standardUserDefaults] integerForKey:TAG_OF_EVENT]];
                    }
                    else{
                        [self.navigationController popViewControllerAnimated:YES];
                    }
                }
                    break;
                case 1:
                {
                    //NO: in discart all data will go cart screen
                    //LaundryCartViewV2 *laundryCartView = [[LaundryCartViewV2 alloc] initWithNibName:NSStringFromClass([LaundryCartViewV2 class]) bundle:nil];
                    //[self.navigationController pushViewController:laundryCartView animated:YES];
                    
                }
                default:
                    break;
            }
        }
            
            break;
            
        default:
            break;
    }
}

#pragma mark - Save Laundry
-(void)saveData{
    BOOL isCheckOrder = [self isCheckLaundryOrderTemp];
    if (isCheckOrder == YES) {
        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:imgYesButton], [UIImage imageNamed:imgNoButton], nil] title:[[LanguageManagerV2 sharedLanguageManager]getAlertTitle] message:[[LanguageManagerV2 sharedLanguageManager] getMsg_Dispatch_This_For_PickUp] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles:  [[LanguageManagerV2 sharedLanguageManager] getNo], nil];   
        [alert setTag:tagDispatchforpickup];
        [alert show];
    }
}

#pragma mark - Load access right
-(void) loadAccessRights
{
    AccessRightModel *currentUserAccessRight = [[UserManagerV2 sharedUserManager] currentUserAccessRight];
    QRCodeScanner = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.QRCodeScanner];
    postingLaundry = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.postLaundry];
}

-(void)saveLaundryCart:(BOOL) postServer{
    
    // @ show loading data .
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getRaSavingData]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES]; 
    
    ///Save laundry cart
    LaundryManagerV2 *manager = [[LaundryManagerV2 alloc] init];
    SyncManagerV2 *synManager = [[SyncManagerV2 alloc] init];
    NSInteger userId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    NSInteger hotelId = [[[UserManagerV2 sharedUserManager] currentUser] userHotelsId];
    NSInteger roomAssignId = [TasksManagerV2 getCurrentRoomAssignment];
    
    //load order in laundry
    LaudryOrderModelV2 *laundryOrder = [[LaudryOrderModelV2 alloc] init];
    laundryOrder.laundryOrderUserId = userId;
    laundryOrder.laundryOrderRoomId = roomAssignId;
    laundryOrder.laundryOrderStatus = tStatusChecked;
    laundryOrder.laundryOrderCollectDate = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd"]; 
    
    BOOL isCheckLaundryOrder = [manager isCheckLaundryOrder:laundryOrder];
    
    if (isCheckLaundryOrder == FALSE) {
        [manager insertLaudryOrderModelV2:laundryOrder];
    }
    
    //load laundry order
    [manager loadLaundryOrdersModelByRoomIdUserIdAndCollectDate:laundryOrder];
    
    
    //load all laundry service
    NSMutableArray *arrayService = [manager loadAllLaundryServicesModelV2];
    
    //--------------Post WSLog--
    NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *messageValue = [NSString stringWithFormat:@"Laundry Post WSLog: Time:%@, UserId:%d, HotelId:%d, RoomAssignId:%d", time, userId, hotelId, roomAssignId];
    int switchStatus = [[NSUserDefaults standardUserDefaults] integerForKey:ENABLE_LOGWS];
    if (switchStatus == 1) {
        [synManager postWSLogLaundry:userId Message:messageValue MessageType:1];
    }
    //----------End Post WSLog--
    
    for (LaundryServicesModelV2 *service in arrayService) {
        
        //--------------------get laundry order Id--------------------
        int orderIdWS = 0;
        if (postServer == YES) {
            
            //load laundry item to get total & subtotal
            if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
                [self loadDataForCartView];
                orderIdWS = [synManager postLaundryOrder:laundryOrder andsubQuantity:total andServiceID:service.lservices_id andHotel:hotelId andsubTotal:subTotal];
            }
            
        }
        //--------------------WS--------------------------------------
        
        //load all laundry order detail temp by service id
        NSMutableArray *arrayLaundryOrderDetailTemp = [manager loadAllLaundryOrderDetailsTempByServiceId:service.lservices_id];
        
        if (arrayLaundryOrderDetailTemp.count > 0) {
            
            for (LaundryOrderDetailModelV2 *orderTemp in arrayLaundryOrderDetailTemp) {
                
                //load laundry order detail by order id
                LaundryOrderDetailModelV2 *orderDetail = [[LaundryOrderDetailModelV2 alloc] init];
                orderDetail.lod_item_id = orderTemp.lod_item_id;
                orderDetail.lod_service_id = orderTemp.lod_service_id;
                orderDetail.lod_category_id = orderTemp.lod_category_id;
                
                [manager loadLaundryOrderDetailsByItemId:orderDetail AndOrderId:laundryOrder.laundryOrderId];
                
                if (orderDetail.lod_id != 0) {
                    
                    //update Order detail in laundry
                    orderDetail.lod_quantity = orderTemp.lod_quantity + orderDetail.lod_quantity;
                    
                    [manager updateLaundryOrderDetailModelV2:orderDetail];
                }
                else
                {
                    //insert Order detail in laundry
                    LaundryOrderDetailModelV2 *laundryOrderDetailModel = [[LaundryOrderDetailModelV2 alloc] init];
                    laundryOrderDetailModel.lod_item_id = orderTemp.lod_item_id;
                    laundryOrderDetailModel.lod_service_id = orderTemp.lod_service_id;
                    laundryOrderDetailModel.lod_category_id = orderTemp.lod_category_id;
                    laundryOrderDetailModel.lod_post_status = POST_STATUS_SAVED_UNPOSTED;
                    laundryOrderDetailModel.lod_quantity = orderTemp.lod_quantity;
                    laundryOrderDetailModel.lod_laundry_order_id = laundryOrder.laundryOrderId;
                    
                    [manager insertLaundryOrderDetailModelV2:laundryOrderDetailModel];
                    
                }
                
                //--------------------------post to server WS-----------------
                if (postServer == YES) {
                    
                    LaundryOrderDetailModelV2 *orderDetailPost = [[LaundryOrderDetailModelV2 alloc] init];
                    orderDetailPost.lod_item_id = orderTemp.lod_item_id;
                    orderDetailPost.lod_service_id = orderTemp.lod_service_id;
                    orderDetailPost.lod_category_id = orderTemp.lod_category_id;
                    orderDetailPost.lod_laundry_order_id = laundryOrder.laundryOrderId;
                    
                    [manager loadLaundryOrderDetailsByItemId:orderDetailPost AndOrderId:laundryOrder.laundryOrderId];

                     BOOL statusPost = [synManager postLaundryItem:orderDetailPost andLaundryOrder:orderIdWS anduser:userId andTransactionTime:laundryOrder.laundryOrderCollectDate];
                    if (statusPost == YES) {
                        //post success, clear database
                        [manager deleteLaundryOrderDetailModelV2:orderDetailPost];
                        
                    }
                }
                
                //delete laundry order detail temp
                [manager deleteLaundryOrderDetailTemp:orderTemp];
                
                [tbvLaundryCell reloadData];
                //--------------------------End------------------------------
            }
            
        }
        
    }
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getMsgSaveMessage]];
    //hide saving data.
    [self performSelector:@selector(hiddenHUDAfterSaved:) withObject:HUD afterDelay:0.5];
    
}

#pragma mark MBProgressHUDDelegate methods
-(void) hiddenHUDAfterSaved:(MBProgressHUD *)HUD{
    [HUD hide:YES];
    [HUD removeFromSuperview];
    // [HUD release];
}

#pragma mark - Handle Topbar Methods
-(void)loadTopbarView {
    TopbarViewV2 *topbar = [[TopbarViewV2 alloc] initViewWithSuperModel:[[CommonVariable sharedCommonVariable] superRoomModel]];
    [topbar setTag:tagTopbarView];
    
    [self.view addSubview:topbar];
    [topbar setHidden:YES];
    [topbar setAlpha:0.0];
    [self addButtonHandleShowHideTopbar];
}

-(void)showTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationWillStartSelector:@selector(setHiddenTopbarView)];
    
    f = topbar.frame;
    f.origin.y = 0;
    [topbar setFrame:f];
    [topbar setAlpha:1.0];
    
    [self adjustShowForViews];
    
    [UIView commitAnimations];
}

-(void)hideTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDidStopSelector:@selector(setHiddenTopbarView)];
    [UIView setAnimationDelegate:self];
    
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    [topbar setAlpha:0.0];
    
    [self adjustRemoveForViews];
    
    [UIView commitAnimations];
}

-(void)setHiddenTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    [topbar setHidden:!topbar.hidden];
}

-(TopbarViewV2 *)getTopBarView {
    return (TopbarViewV2 *)[self.view viewWithTag:tagTopbarView];
}

-(void)addButtonHandleShowHideTopbar {
    UIButton *titleBarButton = [[UIButton alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
    
    titleBarButton.titleLabel.backgroundColor = [UIColor clearColor];
    titleBarButton.titleLabel.font = FONT_BUTTON_TOPBAR;
    titleBarButton.titleLabel.textAlignment = UITextAlignmentCenter;
    titleBarButton.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
    [titleBarButton.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
    [titleBarButton.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
    [titleBarButton addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    [titleBarButton setTitle:[[LanguageManagerV2 sharedLanguageManager] getLaundry] forState:UIControlStateNormal];
    self.navigationItem.titleView = titleBarButton;
    
    //Move title to center vertical
    CGRect titleViewBounds = self.navigationItem.titleView.bounds;
    int moveLeftTitle = 0;
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        moveLeftTitle = 10;
    }
    self.navigationItem.titleView.bounds = CGRectMake(titleViewBounds.origin.x + moveLeftTitle, Y_TOPBAR_TITLE, titleViewBounds.size.width, titleViewBounds.size.height);
}

-(void)topbarTapped:(UIButton *)sender {
    TopbarViewV2 *topbar = [self getTopBarView];
    [sender setEnabled:NO];
    if (topbar.isHidden) {
        [self showTopbarView];
    } else {
        [self hideTopbarView];
    }
    [self performSelector:@selector(topbarButtonEnable:) withObject:sender afterDelay:0.5];
}

-(void) topbarButtonEnable:(UIButton *) topbarBtn {
    [topbarBtn setEnabled:YES];
}

-(void)adjustShowForViews {
    //adjust all views when show 
    [tbvLaundryCell setFrame:CGRectMake(0, 60, 320, 320)];
    
}

-(void)adjustRemoveForViews { 
    //adjust all views when hide
    [tbvLaundryCell setFrame:CGRectMake(0,0, 320, 350)];
}

@end
