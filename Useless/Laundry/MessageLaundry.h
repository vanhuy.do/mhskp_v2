//
//  MessageLaundry.h
//  iGuest
//
//  Created by ThuongNM on 1/13/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol MessageLaundryDelegate <NSObject>

@optional
-(void) alertAdvancedOKButtonPressed;
-(void) alertAdvancedCancelButtonPressed;

@end

@interface MessageLaundry : UIAlertView <UIAlertViewDelegate>{
  

    UIView * view;
    UIButton * btnOK;
    UIButton *btnCancel;
    UILabel *lblTitle;
    UILabel *lblContent;
    UIPopoverController *popoverReserStatus;
    MessageLaundry *MessageLaundry;
    __unsafe_unretained id<MessageLaundryDelegate> delegate;
}
@property(nonatomic,assign)id<MessageLaundryDelegate> delegate;
- (id)alertAdvancedSearchInitWithFrame:(CGRect)frame title:(NSString *)titletext content:(NSString *)contenttext;
-(void) setCaptionsView;
@end

