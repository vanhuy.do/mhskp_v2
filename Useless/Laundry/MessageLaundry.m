//
//  AlertAdvancedSearch.m
//  iGuest
//
//  Created by ThuongNM on 1/13/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "MessageLaundry.h"

@interface MessageLaundry(privateMethods)

-(IBAction)btnSearch:(id)sender;
-(IBAction)btnCancel:(id)sender;
@end

@implementation MessageLaundry

@synthesize delegate;

- (id)alertAdvancedSearchInitWithFrame:(CGRect)frame title:(NSString *)titletext content:(NSString *)contenttext {
    if (self) {
                    
        lblTitle=[[UILabel alloc] initWithFrame:CGRectMake(130,3,100, 30)];
       
        lblTitle.textColor=[UIColor colorWithRed:255/255.0 green:255/255.0 
                                            blue:255/255.0 alpha:1.0];
        lblTitle.backgroundColor = [UIColor clearColor];
        [lblTitle setTextAlignment:UITextAlignmentLeft];
        
        lblTitle.text=titletext;
        
        lblContent=[[UILabel alloc] initWithFrame:CGRectMake(50,35,250, 30)];
        
        lblContent.textColor=[UIColor colorWithRed:255/255.0 green:255/255.0 
                                            blue:255/255.0 alpha:1.0];
        lblContent.backgroundColor = [UIColor clearColor];
      //  [lblContent setTextAlignment:UITextAlignmentCenter];
        lblContent.text=contenttext;
        

        
        btnOK=[[UIButton alloc] initWithFrame:CGRectMake(15,83,120,40)];
        [btnOK.titleLabel setTextAlignment:UITextAlignmentCenter];  
        [btnOK setBackgroundImage:[UIImage imageNamed:@"yes_bt.png"] 
                             forState:UIControlStateNormal];  
        [btnOK setTitle:[[LanguageManagerV2 sharedLanguageManager] getOK] forState:UIControlStateNormal];
        btnOK.titleLabel.textColor=[UIColor whiteColor];
     
        [btnOK setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btnOK addTarget:self action:@selector(btnOk:) 
            forControlEvents:UIControlEventTouchUpInside];
        
        btnCancel=[[UIButton alloc] initWithFrame:CGRectMake(160,83,120,40)];
        [btnCancel.titleLabel setTextAlignment:UITextAlignmentCenter];  
        [btnCancel setBackgroundImage:[UIImage imageNamed:
                                       @"no_bt.png"] forState:UIControlStateNormal];
        [btnCancel setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_CANCEL] forState:UIControlStateNormal];
        btnCancel.titleLabel.textColor=[UIColor whiteColor];        [btnCancel setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btnCancel addTarget:self action:@selector(btnCancel:) 
            forControlEvents:UIControlEventTouchUpInside];
        [btnCancel setTitleEdgeInsets:UIEdgeInsetsMake(30, 35, 30, 12)];
          // [btnCancel setTitleEdgeInsets:ui];
        [self setCaptionsView];
        [self addSubview:lblTitle];
        [self addSubview:lblContent];
        [self addSubview:btnCancel];
        [self addSubview:btnOK];
            }
    
    return self;
}
- (void)setFrame:(CGRect)rect {         
    [super setFrame:CGRectMake(10,80, 
                               305, 150)]; 
}


-(IBAction)btnOk:(id)sender{
    if ([delegate respondsToSelector:@selector(alertAdvancedOKButtonPressed)]) {
            [super dismissWithClickedButtonIndex:-1 animated:YES];
            [delegate alertAdvancedOKButtonPressed];
                }    
}

-(IBAction)btnCancel:(id)sender{
    if ([delegate respondsToSelector:@selector(alertAdvancedCancelButtonPressed)]) {
       [super dismissWithClickedButtonIndex:-1 animated:YES];
        [delegate alertAdvancedCancelButtonPressed];
    }    
}

-(void) setCaptionsView {
    }

@end
