//
//  SectionLaundryCartViewV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SectionLaundryCartViewV2Delegate <NSObject>

@end

@interface SectionLaundryCartViewV2 : UIView{
    UIImageView *imgSectionLaundryCart;
    UILabel *lblSectionLaundryCart;
   __unsafe_unretained id<SectionLaundryCartViewV2Delegate> delegate;
}
@property (nonatomic, assign) id<SectionLaundryCartViewV2Delegate> delegate; 
@property (nonatomic, assign) NSInteger section;
@property (strong, nonatomic) IBOutlet UIImageView *imgSectionLaundryCart;
@property (strong, nonatomic) IBOutlet UILabel *lblSectionLaundryCart;
-(id) initWithSection:(NSInteger) sectionIndex contentImg:(NSData *)imgName contentName:(NSString *)content;
@end

