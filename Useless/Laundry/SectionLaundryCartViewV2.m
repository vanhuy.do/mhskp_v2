//
//  SectionLaundryCartViewV2.m
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SectionLaundryCartViewV2.h"
#define imgBackgroundCart @"laundry_cart_bt.png"

@implementation SectionLaundryCartViewV2
@synthesize imgSectionLaundryCart,lblSectionLaundryCart,section;
@synthesize delegate;
-(id) init{
    self = [super init];
    if (self) {
        [self setFrame:CGRectMake(0, 0, 320, 60)];
        [self setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:imgBackgroundCart]]];
        
        imgSectionLaundryCart = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 60)];
        [imgSectionLaundryCart setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:imgBackgroundCart]]];
        [self addSubview:imgSectionLaundryCart];
        
        lblSectionLaundryCart = [[UILabel alloc] initWithFrame:CGRectMake(53, 12, 200, 30)];
        [lblSectionLaundryCart setFont:[UIFont fontWithName:@"Arial-BoldMT" size:17]];
        [lblSectionLaundryCart setBackgroundColor:[UIColor clearColor]];
        [lblSectionLaundryCart setTextColor:[UIColor colorWithRed:6 green:62 blue:127 alpha:1]];
        [self addSubview:lblSectionLaundryCart];
        
        
    }
    return self;
}

-(id)initWithSection:(NSInteger)sectionIndex contentImg:(NSData *)imgName contentName:(NSString *)content{
    self = [self init];
    if(self){
        section = sectionIndex;
        [imgSectionLaundryCart setImage:[UIImage imageWithData:imgName]];
        lblSectionLaundryCart.text = content;
    }
    return self;
    
}
#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
}
*/

//- (void)viewDidUnload
//{
//    [super viewDidUnload];
//    // Release any retained subviews of the main view.
//    // e.g. self.myOutlet = nil;
//}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
