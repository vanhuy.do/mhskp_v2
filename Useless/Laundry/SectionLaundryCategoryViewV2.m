//
//  SectionLaundryCategoryViewV2.m
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 1/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SectionLaundryCategoryViewV2.h"
#define imgBackgroundSectionLaundry @"big_bt_640x111.png"

@implementation SectionLaundryCategoryViewV2
@synthesize  imgLaundryCategory, imgArrowSection, lblNameLaundryCategory,section,delegate;

- (id) init {
    self = [super init];
    if(self){
        [self setFrame:CGRectMake(0, 0, 320, 50)];
        
        imgLaundryCategory = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 64, 60)];
        
        lblNameLaundryCategory = [[UILabel alloc] initWithFrame:CGRectMake(100, 10, 196, 39)];
        [lblNameLaundryCategory setBackgroundColor:[UIColor clearColor]];
        [lblNameLaundryCategory setTextColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:1]];
        [lblNameLaundryCategory setFont:[UIFont fontWithName:@"Arial-BoldMT" size:17]];
        [self setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:imgBackgroundSectionLaundry]]];
        if(isToggle == YES)
        {
            imgArrowSection = [[UIImageView alloc]initWithImage:[UIImage imageNamed:imgRightRowOpen]];
        }
        else
        {
            imgArrowSection = [[UIImageView alloc]initWithImage:[UIImage imageNamed:imgRightRow]];
        }
        
        [imgArrowSection setFrame:CGRectMake(288, 16, 25, 25)];
        btnToggle = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnToggle setFrame:CGRectMake(0, 0, 320, 50)];
        [btnToggle addTarget:self action:@selector(toggleOpen:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:imgLaundryCategory];
        [self addSubview:lblNameLaundryCategory];
        [self addSubview:imgArrowSection];
        [self addSubview:btnToggle];
        
    }
    return self;
}
-(id)initWithSection:(NSInteger)sectionIndex contentImg:(NSData *)imgName contentName:(NSString *)content AndStatusArrow:(BOOL)isOpened{
    self = [self init];
    if(self){
        section = sectionIndex;
        [imgLaundryCategory setImage:[UIImage imageWithData:imgName]];
        lblNameLaundryCategory.text = content;
        isToggle = isOpened;
        
    }
    return self;
    
}

-(void)toggleOpen:(id)sender {
    
    [self toggleOpenWithUserAction:YES];
}

-(void)toggleOpenWithUserAction:(BOOL)userAction{
    if (userAction) {
        if (isToggle == NO) {
            if ([delegate respondsToSelector:@selector(sectionHeaderView:sectionOpened:)]) {
                isToggle = YES;
                [delegate sectionHeaderView:self sectionOpened:section];
                
                [imgArrowSection setImage:[UIImage imageNamed:imgRightRowOpen]];
            }
        }else {
            if ([delegate respondsToSelector:@selector(sectionHeaderView:sectionClosed:)]) {
                isToggle = NO;
                [imgArrowSection setImage:[UIImage imageNamed:imgRightRow]];
                [delegate sectionHeaderView:self sectionClosed:section];
            }
        }
    }

}
#pragma mark - View lifecycle

/*
 // Implement loadView to create a view hierarchy programmatically, without using a nib.
 - (void)loadView
 {
 }
 */

/*
 // Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
 - (void)viewDidLoad
 {
 [super viewDidLoad];
 }
 */



- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
