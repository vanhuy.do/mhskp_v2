//
//  SectionLaundryItemViewV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 1/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SectionLaundryItemViewV2Delegate;

@interface SectionLaundryItemViewV2 : UIView{
    UIImageView *imgLaundryCategory;
    UILabel *lblNameLaundryCategory;
    UIImageView *imgArrowSection;
    UIButton *btnToggle;
    BOOL isToggle;
    NSInteger section;
}
@property (nonatomic, assign) NSInteger section;
@property (strong, nonatomic) IBOutlet UIImageView *imgLaundryCategory;
@property (strong, nonatomic) IBOutlet UILabel *lblNameLaundryCategory;
@property (strong, nonatomic) UIImageView *imgArrowSection;
-(id) initWithSection:(NSInteger) sectionIndex contentImg:(NSData *)imgName contentName:(NSString *)content AndStatusArrow:(BOOL)isOpened;
@end
