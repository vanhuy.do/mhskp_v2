//
//  SectionLaundryItemViewV2.m
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 1/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SectionLaundryItemViewV2.h"

@implementation SectionLaundryItemViewV2
@synthesize  imgLaundryCategory, imgArrowSection, lblNameLaundryCategory,section;

- (id) init {
    self = [super init];
    if(self){
        [self setFrame:CGRectMake(0, 0, 320, 50)];
        lblNameLaundryCategory = [[UILabel alloc] initWithFrame:CGRectMake(20, 4, 196, 39)];
        [lblNameLaundryCategory setBackgroundColor:[UIColor clearColor]];
        [lblNameLaundryCategory setTextColor:[UIColor colorWithRed:255 green:255 blue:255 alpha:1]];
        [lblNameLaundryCategory setFont:[UIFont fontWithName:@"Arial-BoldMT" size:15]];
        [self setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:imgBackgroundSection]]];
        if(isToggle == YES)
        {
            imgArrowSection = [[UIImageView alloc]initWithImage:[UIImage imageNamed:imgRightRowOpen]];
        }
        else
        {
            imgArrowSection = [[UIImageView alloc]initWithImage:[UIImage imageNamed:imgRightRow]];
        }
        
        [imgArrowSection setFrame:CGRectMake(288, 12, 25, 25)];
        btnToggle = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnToggle setFrame:CGRectMake(0, 0, 320, 50)];
        [btnToggle addTarget:self action:@selector(toggleOpen:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:lblNameLaundryCategory];
        [self addSubview:imgArrowSection];
        [self addSubview:btnToggle];

    }
    return self;
}
-(id)initWithSection:(NSInteger)sectionIndex contentImg:(NSData *)imgName contentName:(NSString *)content AndStatusArrow:(BOOL)isOpened{
    self = [self init];
    if(self){
        section = sectionIndex;
        [imgLaundryCategory setImage:[UIImage imageWithData:imgName]];
        lblNameLaundryCategory.text = content;
        isToggle = isOpened;
        
    }
    return self;

}
#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
}
*/



- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
