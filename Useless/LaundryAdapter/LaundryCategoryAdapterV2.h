//
//  LaundryCategoryAdapterV2.h
//  mHouseKeeping
//
//  Created by ThuongNM on 3/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DatabaseAdapterV2.h"
#import "LaundryCategoryModelV2.h"
@interface LaundryCategoryAdapterV2 : DatabaseAdapterV2{
    
}

-(int)insertLaundryCategoryData:(LaundryCategoryModelV2*) LaundryCategoryModel;
-(int)updateLaundryCategoryData:(LaundryCategoryModelV2*) LaundryCategoryModel;
-(int)deleteLaundryCategoryData:(LaundryCategoryModelV2*) LaundryCategoryModel;
-(NSMutableArray*)loadAllLaundryCategoryModelV2;
-(LaundryCategoryModelV2*)loadLaundryCategoryID:(LaundryCategoryModelV2 *)model;
-(NSString *)getTheLatestLastModifierInLaundryCatergory;
@end
