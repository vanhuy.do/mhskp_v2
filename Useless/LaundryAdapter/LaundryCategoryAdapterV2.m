//
//  LaundryCategoryAdapterV2.m
//  mHouseKeeping
//
//  Created by ThuongNM on 3/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LaundryCategoryAdapterV2.h"
#import "DbDefinesV2.h"
#import "ehkDefines.h"

@implementation LaundryCategoryAdapterV2

-(int)insertLaundryCategoryData:(LaundryCategoryModelV2*) LaundryCategoryModel{

    NSString *sqlString = [[NSString alloc] initWithFormat:@"INSERT INTO %@ (%@,%@,%@,%@,%@ ) %@",LAUNDRY_CATEGORIES,ldryca_id,ldryca_name,ldryca_name_lang,ldryca_image,ldryca_last_modified,@"VALUES(?,?,?,?,?)"];     
    
    const char *sql =[sqlString UTF8String];
 
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSInteger status1=sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status1 == SQLITE_OK) {
        
        sqlite3_bind_int(self.sqlStament, 1,LaundryCategoryModel.laundryCategoryId);
        sqlite3_bind_text(self.sqlStament, 2, [LaundryCategoryModel.laundryCategoryName UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(self.sqlStament, 3, [LaundryCategoryModel.laundryCategoryNameLang UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_blob(self.sqlStament, 4,[LaundryCategoryModel.laundryCategoryImg bytes],[LaundryCategoryModel.laundryCategoryImg length], NULL);
        sqlite3_bind_text(self.sqlStament, 5, [LaundryCategoryModel.laundryCategoryLastModified UTF8String], -1, SQLITE_TRANSIENT);
    
    }
    
    NSInteger status = sqlite3_step(self.sqlStament);
    if (status == SQLITE_DONE) {
        sqlite3_last_insert_rowid(self.database);
        sqlite3_reset(self.sqlStament);
        return 1;
    }
    else{
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return 0;
    }
    return  0;
}

-(int)updateLaundryCategoryData:(LaundryCategoryModelV2*) LaundryCategoryModel{
    
    NSInteger returnInt = 0;
    if(self.sqlStament == nil) {
        NSString *sqlString = [[NSString alloc] initWithFormat:@"UPDATE %@ SET  %@ = ?,%@= ?,%@=?,%@= ?,%@=? WHERE %@ = ?",LAUNDRY_CATEGORIES,ldryca_name,ldryca_name_lang,ldryca_image,ldryca_last_modified,ldryca_id];
        const char *sql = [sqlString UTF8String];

        NSInteger reAccessCount = 0;
        RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
            reAccessCount ++;
        }
        
        NSInteger prepare = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
		if(prepare != SQLITE_OK)
        {
        }
        
        sqlite3_bind_text(self.sqlStament, 1, [LaundryCategoryModel.laundryCategoryName UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(self.sqlStament, 2, [LaundryCategoryModel.laundryCategoryNameLang UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_blob(self.sqlStament, 3, [LaundryCategoryModel.laundryCategoryImg bytes], [LaundryCategoryModel.laundryCategoryImg length], NULL);
        sqlite3_bind_text(self.sqlStament, 4, [LaundryCategoryModel.laundryCategoryLastModified UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_int(self.sqlStament, 5,LaundryCategoryModel.laundryCategoryId);
        
        NSInteger step = sqlite3_step(self.sqlStament);
        if(SQLITE_DONE != step)
        {
            if (step == SQLITE_BUSY) {
                if (reAccessCount <= REACCESS_MAX_TIMES) {
                    goto  RE_ACCESS_DB;
                }
            }
            return returnInt;
        }
        else
        { 
            return 1;
        } 
    }
    return returnInt;
}

-(int)deleteLaundryCategoryData:(LaundryCategoryModelV2*) LaundryCategoryModel{
    
    NSInteger result = 1;
    if(self.sqlStament == nil) {
        NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"DELETE FROM %@ WHERE %@ = ?",LAUNDRY_CATEGORIES, ldryca_id];
		const char *sql = [sqlString UTF8String];

        NSInteger reAccessCount = 0;
        RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
            reAccessCount ++;
        }
        
		if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
			result = 0;
        
        sqlite3_bind_int(self.sqlStament, 1,LaundryCategoryModel.laundryCategoryId);	
        
        NSInteger status = sqlite3_step(self.sqlStament);
        if (SQLITE_DONE != status) {
            if (status == SQLITE_BUSY) {
                if (reAccessCount <= REACCESS_MAX_TIMES) {
                    goto RE_ACCESS_DB;
                }
            }
            result = 0;
        }
	}
	
    return result;
}

-(NSMutableArray*)loadAllLaundryCategoryModelV2{
    
    NSMutableArray * array=[NSMutableArray array];
    NSMutableString * sqlString=[[NSMutableString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@ FROM %@",       
                                 ldryca_id,
                                 ldryca_name,
                                 ldryca_name_lang,
                                 ldryca_image,
                                 ldryca_last_modified,
                                 LAUNDRY_CATEGORIES];
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
        
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {     
        
        status = sqlite3_step(sqlStament);
        while(status == SQLITE_ROW) {
            
            LaundryCategoryModelV2 *model =[[LaundryCategoryModelV2 alloc] init];
            
            if(sqlite3_column_int(sqlStament, 0)){
               model.laundryCategoryId= sqlite3_column_int(sqlStament, 0);
            }
            if(sqlite3_column_text(sqlStament, 1)){
                model.laundryCategoryName=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
                
            }
            if(sqlite3_column_text(sqlStament, 2)){
                model.laundryCategoryNameLang=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament,2)];
            }
            if(sqlite3_column_blob(sqlStament,3)){
                NSData *image = [NSData dataWithBytes:sqlite3_column_blob(sqlStament, 3) length:sqlite3_column_bytes(sqlStament, 3)];
                [model setLaundryCategoryImg:image];

            }
            if(sqlite3_column_text(sqlStament, 4)){
                model.laundryCategoryLastModified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
            }            
            
            [array addObject:model];
            status = sqlite3_step(sqlStament);
        }
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                [array removeAllObjects];
                goto RE_ACCESS_DB;
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    return  array;
}

-(LaundryCategoryModelV2*)loadLaundryCategoryID:(LaundryCategoryModelV2 *)model{
    
    NSMutableString * sqlString=[[NSMutableString alloc] initWithFormat:@"SELECT %@,%@,%@,%@,%@ FROM %@ Where %@=? ",ldryca_id,ldryca_name,ldryca_name_lang,ldryca_image,ldryca_last_modified,LAUNDRY_CATEGORIES,ldryca_id];
    const char *sql=[sqlString UTF8String];

    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {     
         sqlite3_bind_int(self.sqlStament, 1,model.laundryCategoryId);
        
        status = sqlite3_step(sqlStament);
        while(status == SQLITE_ROW) {
        
            if(sqlite3_column_int(sqlStament, 0)){
                model.laundryCategoryId= sqlite3_column_int(sqlStament, 0);
            }
            if(sqlite3_column_text(sqlStament, 1)){
                model.laundryCategoryName=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
                
            }
            if(sqlite3_column_text(sqlStament, 2)){
                model.laundryCategoryNameLang=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament,2)];
            }
            if(sqlite3_column_blob(sqlStament,3)){
                NSData *image = [[NSData alloc] initWithBytes:sqlite3_column_blob(sqlStament, 3) length:sqlite3_column_bytes(sqlStament, 3)];
                [model setLaundryCategoryImg:image];

            }
            if(sqlite3_column_text(sqlStament, 4)){
                model.laundryCategoryLastModified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
            }       
            return model;
        }
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    return  model;
}

-(NSString *)getTheLatestLastModifierInLaundryCatergory
{
    NSMutableString * sqlString=[[NSMutableString alloc] initWithFormat:@"SELECT MAX(%@) FROM %@ ORDER BY %@ ASC",ldryca_last_modified,LAUNDRY_CATEGORIES,ldryca_last_modified];
    const char *sql=[sqlString UTF8String];

    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSString *lastModifier;
    
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {  
        
        status = sqlite3_step(sqlStament);
        while(status == SQLITE_ROW)
        {
            if(sqlite3_column_text(sqlStament, 0)){
                lastModifier=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 0)];
            }
            return lastModifier;
        }
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    return lastModifier;
}

@end
