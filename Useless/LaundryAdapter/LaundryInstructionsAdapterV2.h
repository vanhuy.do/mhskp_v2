//
//  LaundryInstructionsAdapterV2.h
//  mHouseKeeping
//
//  Created by ThuongNM on 3/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LaundryIntructionModelV2.h"
#import "DatabaseAdapterV2.h"
@interface LaundryInstructionsAdapterV2 : DatabaseAdapterV2{
    
}
-(int)insertLaundryIntructionData:(LaundryIntructionModelV2*) LaundryIntructionModel;
-(int)updateLaundryIntructionData:(LaundryIntructionModelV2*) LaundryIntructionModel;
-(int)deleteLaundryIntructionData:(LaundryIntructionModelV2*) LaundryIntructionModel;
-(LaundryIntructionModelV2*)loadLaundryIntructionModelV2:(LaundryIntructionModelV2*) laundryIntructionModel;
-(NSMutableArray*)loadAllLaundryIntructionModelV2;
-(NSString *)getTheLatestLastModifier;
@end
