//
//  LaundryInstructionsAdapterV2.m
//  mHouseKeeping
//
//  Created by ThuongNM on 3/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LaundryInstructionsAdapterV2.h"
#import "DbDefinesV2.h"
#import "ehkDefines.h"

@implementation LaundryInstructionsAdapterV2

-(int)insertLaundryIntructionData:(LaundryIntructionModelV2 *) LaundryIntructionModel{
    NSString *sqlString = [[NSString alloc] initWithFormat:@"INSERT INTO %@ (%@, %@, %@,%@ ) %@",LAUNDRY_INSTRUCTIONS,li_id,li_name,li_name_lang,li_last_modified,@"VALUES(?,?,?,?)"];     
    
    const char *sql =[sqlString UTF8String];

    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSInteger status1=sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status1 == SQLITE_OK) {
        
        sqlite3_bind_int(self.sqlStament, 1,LaundryIntructionModel.instructionId);
        sqlite3_bind_text(self.sqlStament, 2, [LaundryIntructionModel.instructionName UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(self.sqlStament, 3, [LaundryIntructionModel.instructionNameLang UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(self.sqlStament,4, [LaundryIntructionModel.instructionLastModified UTF8String], -1, SQLITE_TRANSIENT);
   }
    
    NSInteger status = sqlite3_step(self.sqlStament);
    if (status == SQLITE_DONE) {
        sqlite3_last_insert_rowid(self.database);
        sqlite3_reset(self.sqlStament);
        return 1;
    }
    else{
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return 0;
    }
    return  0;
}

-(int)updateLaundryIntructionData:(LaundryIntructionModelV2*) LaundryIntructionModel{
    
    if(self.sqlStament == nil) {
        NSString *sqlString = [[NSString alloc] initWithFormat:@"UPDATE %@ SET %@= ?,%@= ? ,%@= ? WHERE %@ = ?",LAUNDRY_INSTRUCTIONS,li_name,li_name_lang,li_last_modified,li_id];
        const char *sql = [sqlString UTF8String];

        NSInteger reAccessCount = 0;
        RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
            reAccessCount ++;
        }
            
        NSInteger prepare = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
		if(prepare != SQLITE_OK)
        {
        }
        
        sqlite3_bind_text(self.sqlStament, 1, [LaundryIntructionModel.instructionName UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(self.sqlStament, 2, [LaundryIntructionModel.instructionNameLang UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(self.sqlStament, 3, [LaundryIntructionModel.instructionLastModified UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_int(self.sqlStament,4,LaundryIntructionModel.instructionId);
        
        NSInteger returnInt = 0;
        NSInteger step = sqlite3_step(self.sqlStament);
        if(SQLITE_DONE != step)
        {
            if (step == SQLITE_BUSY) {
                if (reAccessCount <= REACCESS_MAX_TIMES) {
                    goto RE_ACCESS_DB;
                }
            }
            return returnInt;
        }
        else
        { 
            return 1;
        }   

    }
    return 0;
}

-(int)deleteLaundryIntructionData:(LaundryIntructionModelV2*) LaundryIntructionModel{
    
    NSInteger result = 1;
    if(self.sqlStament == nil) {
        NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"DELETE FROM %@ WHERE %@ = ?",LAUNDRY_INSTRUCTIONS,li_id];
		const char *sql = [sqlString UTF8String];

        NSInteger reAccessCount = 0;
        RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
            reAccessCount ++;
        }
        
		if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
        {
            
        }
//			result = 0;
        
        sqlite3_bind_int(self.sqlStament, 1,LaundryIntructionModel.instructionId);	
        
        NSInteger status = sqlite3_step(self.sqlStament);
        if (SQLITE_DONE != status) {
            if (status == SQLITE_BUSY) {
                if (reAccessCount <= REACCESS_MAX_TIMES) {
                    goto RE_ACCESS_DB;
                }
            }
            result = 0;
        }
        else{
            result = 1;
        }
	}
    return result;
}

-(NSMutableArray*)loadAllLaundryIntructionModelV2{
    
    NSMutableArray * array = [NSMutableArray array];
    
    NSMutableString * sqlString = [[NSMutableString alloc] initWithFormat:@"SELECT %@, %@, %@, %@ FROM %@ ",
                                 li_id,
                                 li_name,
                                 li_name_lang,
                                 li_last_modified,
                                 LAUNDRY_INSTRUCTIONS
                                 ];
    
    const char *sql = [sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {     
        
        status = sqlite3_step(sqlStament);
        while(status == SQLITE_ROW) {
            
            LaundryIntructionModelV2 *model = [[LaundryIntructionModelV2 alloc] init];
            
            model.instructionId= sqlite3_column_int(sqlStament, 0);
            
            if(sqlite3_column_text(sqlStament, 1)){
                model.instructionName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
            }
            
            if(sqlite3_column_text(sqlStament, 2)){
                model.instructionNameLang = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
            }
            
            if(sqlite3_column_text(sqlStament, 3)){
                model.instructionLastModified = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
            }
            
            [array addObject:model];
            status = sqlite3_step(sqlStament);
        }
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                [array removeAllObjects];
                goto RE_ACCESS_DB;
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    return array;
}

-(LaundryIntructionModelV2*)loadLaundryIntructionModelV2:(LaundryIntructionModelV2*) laundryIntructionModel{
    
    NSMutableString * sqlString=[[NSMutableString alloc] initWithFormat:@"SELECT %@,%@,%@,%@ FROM %@ WHERE %@ =?",li_id,li_name,li_name_lang,li_last_modified,LAUNDRY_INSTRUCTIONS,li_id];
    const char *sql=[sqlString UTF8String];

    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {     
         sqlite3_bind_int(self.sqlStament, 1,laundryIntructionModel.instructionId);
        
        NSInteger status = sqlite3_step(sqlStament);
        while(status == SQLITE_ROW) {
            
            laundryIntructionModel =[[LaundryIntructionModelV2 alloc] init];
            if(sqlite3_column_int(sqlStament, 0)){
                laundryIntructionModel.instructionId= sqlite3_column_int(sqlStament, 0);
            }
            if(sqlite3_column_text(sqlStament, 1)){
                laundryIntructionModel.instructionName=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
                
            }
            if(sqlite3_column_text(sqlStament, 2)){
                laundryIntructionModel.instructionNameLang=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
            }
            if(sqlite3_column_text(sqlStament, 3)){
                laundryIntructionModel.instructionLastModified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
                
            }
            return laundryIntructionModel;
        }
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    return  laundryIntructionModel;
}

-(NSString *)getTheLatestLastModifier
{
    NSMutableString * sqlString=[[NSMutableString alloc] initWithFormat:@"SELECT MAX(%@) FROM %@ ORDER BY %@ ASC",li_last_modified,LAUNDRY_INSTRUCTIONS,li_last_modified];
    const char *sql=[sqlString UTF8String];

    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSString *lastModifier;
    
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {  
        
        status = sqlite3_step(sqlStament);
         while(status == SQLITE_ROW)
         {
             if(sqlite3_column_text(sqlStament, 0)){
                 lastModifier=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 0)];
             }
             return lastModifier;
         }
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    return lastModifier;
}

@end
