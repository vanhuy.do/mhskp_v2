//
//  LaundryInstructionsOrderAdapterV2.h
//  mHouseKeeping
//
//  Created by ThuongNM on 3/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DatabaseAdapterV2.h"
#import "LaundryInstructionsOrderModelV2.h"
@interface LaundryInstructionsOrderAdapterV2 : DatabaseAdapterV2{
    
}
-(int)insertLaundryInstructionsOrderData:(LaundryInstructionsOrderModelV2*) LaundryInstructionsOrderModel;
-(int)updateLaundryInstructionsOrderData:(LaundryInstructionsOrderModelV2*) LaundryInstructionsOrderModel;
-(int)deleteLaundryInstructionsOrderData:(LaundryInstructionsOrderModelV2*) LaundryInstructionsOrderModel;
-(NSMutableArray*)loadAllLaundryIntructionOrderModelV2;
-(NSMutableArray*)loadAllLaundryIntructionOrderID:(int)ID; 

@end
