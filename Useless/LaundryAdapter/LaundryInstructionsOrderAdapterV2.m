//
//  LaundryInstructionsOrderAdapterV2.m
//  mHouseKeeping
//
//  Created by ThuongNM on 3/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LaundryInstructionsOrderAdapterV2.h"
#import "ehkDefines.h"

@implementation LaundryInstructionsOrderAdapterV2

-(int)insertLaundryInstructionsOrderData:(LaundryInstructionsOrderModelV2*) LaundryInstructionsOrderModel{
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"INSERT INTO %@ (%@, %@ ) %@",LAUNDRY_INSTRUCTIONS_ORDER,lio_intruction_id,lio_order_id,@"VALUES(?,?)"];     
    
    const char *sql =[sqlString UTF8String];
 
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
        
    NSInteger status1=sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status1 == SQLITE_OK) {
        
        sqlite3_bind_int(self.sqlStament, 1,LaundryInstructionsOrderModel.instructionId);
        sqlite3_bind_int(self.sqlStament, 2,LaundryInstructionsOrderModel.laundryOrderId);
    }
    
    NSInteger status = sqlite3_step(self.sqlStament);
    if (status == SQLITE_DONE) {
        sqlite3_last_insert_rowid(self.database);
        sqlite3_reset(self.sqlStament);
        return 1;
    }
    else{
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return 0;
    }
    return  0;
}

-(int)updateLaundryInstructionsOrderData:(LaundryInstructionsOrderModelV2*) LaundryInstructionsOrderModel{
    
    if(self.sqlStament == nil) {
        NSString *sqlString = [[NSString alloc] initWithFormat:@"UPDATE %@ SET %@ = ?, %@ = ? WHERE %@ = ?",LAUNDRY_INSTRUCTIONS_ORDER,lio_intruction_id,lio_order_id,lod_id];
        const char *sql = [sqlString UTF8String];

        NSInteger reAccessCount = 0;
        RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
            reAccessCount ++;
        }
        
        NSInteger prepare = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
		if(prepare != SQLITE_OK)
        {
        }
        
        sqlite3_bind_int(self.sqlStament, 1,LaundryInstructionsOrderModel.laundryOrderId);
        sqlite3_bind_int(self.sqlStament, 2,LaundryInstructionsOrderModel.instructionOrderId);

        NSInteger returnInt = 0;
        NSInteger step = sqlite3_step(self.sqlStament);
        if(SQLITE_DONE != step)
        {
            if (step == SQLITE_BUSY) {
                if (reAccessCount <= REACCESS_MAX_TIMES) {
                    goto RE_ACCESS_DB;
                }
            }
            return returnInt;
        }
        else
        { 
            return 1;
        }
    }
    return 0;
}

-(int)deleteLaundryInstructionsOrderData:(LaundryInstructionsOrderModelV2*) LaundryInstructionsOrderModel{
    
    NSInteger result = 1;
    if(self.sqlStament == nil) {
        NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"DELETE FROM %@ WHERE %@ = ?",LAUNDRY_INSTRUCTIONS_ORDER,lio_order_id];
		const char *sql = [sqlString UTF8String];
        
        NSInteger reAccessCount = 0;
        RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
            reAccessCount ++;
        }

		if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
        {
            
        }
//			result = 0;
        
        sqlite3_bind_int(self.sqlStament, 1,LaundryInstructionsOrderModel.laundryOrderId);	
        
        NSInteger status = sqlite3_step(self.sqlStament);
        if (SQLITE_DONE != status) {
            if (status == SQLITE_BUSY) {
                if (reAccessCount <= REACCESS_MAX_TIMES) {
                    goto RE_ACCESS_DB;
                }
            }
            result = 0;
        }
        else{
            result = 1;
        }
	}
    return result;
}

-(NSMutableArray*)loadAllLaundryIntructionOrderModelV2{
    
    NSMutableArray * array=[NSMutableArray array];
    NSMutableString * sqlString=[[NSMutableString alloc] initWithFormat:@"SELECT %@,%@,%@ FROM %@ ",lio_id,lio_intruction_id,lio_order_id ,LAUNDRY_INSTRUCTIONS_ORDER];
    const char *sql=[sqlString UTF8String];

    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    
    if(status == SQLITE_OK) {     
        
        status = sqlite3_step(sqlStament);
        while(status == SQLITE_ROW) {
            
            LaundryInstructionsOrderModelV2 *model =[[LaundryInstructionsOrderModelV2 alloc] init];
            if(sqlite3_column_int(sqlStament, 0)){
                model.instructionOrderId= sqlite3_column_int(sqlStament, 0);
            }
            if(sqlite3_column_int(sqlStament, 1)){
                model.instructionId= sqlite3_column_int(sqlStament, 1);
            }
            if(sqlite3_column_int(sqlStament, 2)){
                model.laundryOrderId= sqlite3_column_int(sqlStament, 2);
            }
            
            [array addObject:model];
            status = sqlite3_step(sqlStament);
        }
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                [array removeAllObjects];
                goto RE_ACCESS_DB;
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    return  array;
}

-(NSMutableArray*)loadAllLaundryIntructionOrderID:(int)ID{
    
    NSMutableArray * array=[NSMutableArray array];
    NSMutableString * sqlString=[[NSMutableString alloc] initWithFormat:@"SELECT %@,%@,%@ FROM %@ WHERE %@= %d ",lio_id,lio_intruction_id,lio_order_id ,LAUNDRY_INSTRUCTIONS_ORDER,lio_order_id,ID];
    const char *sql=[sqlString UTF8String];

    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSInteger status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) ;
    if(status == SQLITE_OK) {     
        
        NSInteger status = sqlite3_step(sqlStament);
        while(status == SQLITE_ROW) {
            
            LaundryInstructionsOrderModelV2 *model =[[LaundryInstructionsOrderModelV2 alloc] init];
            if(sqlite3_column_int(sqlStament, 0)){
                model.instructionOrderId= sqlite3_column_int(sqlStament, 0);
            }
            if(sqlite3_column_int(sqlStament, 1)){
                model.instructionId= sqlite3_column_int(sqlStament, 1);
            }
            if(sqlite3_column_int(sqlStament, 2)){
                model.laundryOrderId= sqlite3_column_int(sqlStament, 2);
            }
            
            [array addObject:model];
            status = sqlite3_step(sqlStament);
        }
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                [array removeAllObjects];
                goto RE_ACCESS_DB;
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    return  array;
}

@end
