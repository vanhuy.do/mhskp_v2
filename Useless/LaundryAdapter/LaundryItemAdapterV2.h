//
//  LaundryItemAdapterV2.h
//  mHouseKeeping
//
//  Created by ThuongNM on 3/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LaundryItemModelV2.h"
#import "DatabaseAdapterV2.h"
@interface LaundryItemAdapterV2 : DatabaseAdapterV2{
    
}
-(int)insertLaundryItemData:(LaundryItemModelV2*) LaundryItemModel;
-(int)updateLaundryItemData:(LaundryItemModelV2*) LaundryItemModel;
-(int)deleteLaundryItemData:(LaundryItemModelV2*) LaundryItemModel;
-(NSMutableArray*)loadAllLaundryItemModelV2;
-(LaundryItemModelV2*)loadLaundryItemByLRItem:(LaundryItemModelV2*)model;

-(LaundryItemModelV2*)loadLaundryItemByUserID:(int)ID tgender:(int)_tgender;
-(NSString *)getTheLatestLastModifierInLaundryItem;
@end
