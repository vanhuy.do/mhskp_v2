//
//  LaundryItemAdapterV2.m
//  mHouseKeeping
//
//  Created by ThuongNM on 3/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LaundryItemAdapterV2.h"
#import"DbDefinesV2.h"
#import "ehkDefines.h"

@implementation LaundryItemAdapterV2

-(int)insertLaundryItemData:(LaundryItemModelV2*) LaundryItemModel{
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"INSERT INTO %@ (%@, %@, %@, %@ , %@, %@) %@",LAUNDRY_ITEMS,ldry_Id,ldry_Name,ldry_Lang,ldry_image,ldry_male_or_female,ldry_item_last_modifier,@"VALUES(?,?,?,?,?,?)"];     
    
    const char *sql =[sqlString UTF8String];

    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSInteger status1=sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status1 == SQLITE_OK) {
        
        sqlite3_bind_int(self.sqlStament, 1,LaundryItemModel.laundryItemId);
        sqlite3_bind_text(self.sqlStament, 2, [LaundryItemModel.laundryItemName UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(self.sqlStament, 3, [LaundryItemModel.laundryItemLang UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_blob(self.sqlStament,4, [LaundryItemModel.laundryItemImg bytes], [LaundryItemModel.laundryItemImg length], NULL);
        sqlite3_bind_int(self.sqlStament,5,LaundryItemModel.laundryItem_male_or_female);
        sqlite3_bind_text(self.sqlStament, 6, [LaundryItemModel.laundry_last_modified UTF8String], -1, SQLITE_TRANSIENT);
    }
    
    NSInteger status = sqlite3_step(self.sqlStament);
    if (status == SQLITE_DONE) {
        sqlite3_last_insert_rowid(self.database);
        sqlite3_reset(self.sqlStament);
        return 1;
    }
    else{
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return 0;
    }
    return  0;
}

-(int)updateLaundryItemData:(LaundryItemModelV2*) LaundryItemModel{
    
    if(self.sqlStament == nil) {
        NSString *sqlString = [[NSString alloc] initWithFormat:@"UPDATE %@ SET %@ = ?, %@ = ?,%@= ?,%@=?,%@= ? WHERE %@ = ?",LAUNDRY_ITEMS,ldry_Name,ldry_Lang,ldry_image,ldry_male_or_female,ldry_item_last_modifier,ldry_Id];
        const char *sql = [sqlString UTF8String];
       
        NSInteger reAccessCount = 0;
        RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
            reAccessCount ++;
        }
        
        NSInteger prepare = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
		if(prepare != SQLITE_OK)
        {
        }
        
        sqlite3_bind_text(self.sqlStament, 1, [LaundryItemModel.laundryItemName UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(self.sqlStament, 2, [LaundryItemModel.laundryItemLang UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_blob(self.sqlStament, 3, [LaundryItemModel.laundryItemImg bytes], [LaundryItemModel.laundryItemImg length], NULL);
        sqlite3_bind_int(self.sqlStament, 4,LaundryItemModel.laundryItem_male_or_female);
        sqlite3_bind_text(self.sqlStament, 5, [LaundryItemModel.laundry_last_modified UTF8String], -1, SQLITE_TRANSIENT);
         sqlite3_bind_int(self.sqlStament, 6,LaundryItemModel.laundryItemId);
        
        NSInteger returnInt = 0;
        NSInteger step = sqlite3_step(self.sqlStament);
        if(SQLITE_DONE != step)
        {
            if (step == SQLITE_BUSY) {
                if (reAccessCount <= REACCESS_MAX_TIMES) {
                    goto RE_ACCESS_DB;
                }
            }
            return returnInt;
        }
        else
        { 
            return 1;
        } 
    }
    return 0;
}

-(int)deleteLaundryItemData:(LaundryItemModelV2*) LaundryItemModel{
    NSInteger result = 1;
    if(self.sqlStament == nil) {
        NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"DELETE FROM %@ WHERE %@ = ?",LAUNDRY_ITEMS, ldry_Id];
		const char *sql = [sqlString UTF8String];
     
        NSInteger reAccessCount = 0;
        RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
            reAccessCount ++;
        }
        
		if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
			result = 0;
        
        sqlite3_bind_int(self.sqlStament, 1,LaundryItemModel.laundryItemId);	
        
        NSInteger status = sqlite3_step(self.sqlStament);
        if (SQLITE_DONE != status) {
            if (status == SQLITE_BUSY) {
                if (reAccessCount <= REACCESS_MAX_TIMES) {
                    goto RE_ACCESS_DB;
                }
            }
            result = 0;
        }
	}
    return result;
}

-(NSMutableArray*)loadAllLaundryItemModelV2{
    
    NSMutableArray * array=[NSMutableArray array];
    NSMutableString * sqlString=[[NSMutableString alloc] initWithFormat:@"SELECT %@,%@,%@,%@,%@,%@ FROM %@ ",ldry_Id,ldry_Name,ldry_Lang,ldry_image,ldry_male_or_female,ldry_item_last_modifier,LAUNDRY_ITEMS];
    const char *sql=[sqlString UTF8String];
  
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {     
        
        status = sqlite3_step(sqlStament);
        while(status == SQLITE_ROW) {
            
            LaundryItemModelV2 *model =[[LaundryItemModelV2 alloc] init];
            
            if(sqlite3_column_int(sqlStament, 0)){
                model.laundryItemId= sqlite3_column_int(sqlStament, 0);
            }
            if(sqlite3_column_text(sqlStament, 1)){
                model.laundryItemName=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
                
            }
            if(sqlite3_column_text(sqlStament, 2)){
                model.laundryItemLang=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
            }
            if(sqlite3_column_blob(sqlStament,3)){
                NSData *image = [[NSData alloc] initWithBytes:sqlite3_column_blob(sqlStament, 3) length:sqlite3_column_bytes(sqlStament, 3)];
                [model setLaundryItemImg:image];
                
            }
            if(sqlite3_column_int(sqlStament, 4)){
                model.laundryItem_male_or_female=sqlite3_column_int(sqlStament,4);
                ;
            }
            if(sqlite3_column_text(sqlStament, 5)){
                model.laundry_last_modified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
                
            }
            
            [array addObject:model];
            status =sqlite3_step(sqlStament);
        }
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                [array removeAllObjects];
                goto RE_ACCESS_DB;
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    return  array;
}

-(LaundryItemModelV2*)loadLaundryItemByLRItem:(LaundryItemModelV2*)model
{
    NSMutableString * sqlString=[[NSMutableString alloc] initWithFormat:@"SELECT %@,%@,%@,%@,%@,%@ FROM %@ WHERE %@= ? ",ldry_Id,ldry_Name,ldry_Lang,ldry_image,ldry_male_or_female,ldry_item_last_modifier,LAUNDRY_ITEMS,ldry_Id];
    const char *sql=[sqlString UTF8String];

    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
        
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {     
        sqlite3_bind_int(self.sqlStament, 1,model.laundryItemId);
        
        status = sqlite3_step(sqlStament);
        while( status == SQLITE_ROW) {
            
            if(sqlite3_column_int(sqlStament, 0)){
                model.laundryItemId= sqlite3_column_int(sqlStament, 0);
            }
            if(sqlite3_column_text(sqlStament, 1)){
                model.laundryItemName=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
                
            }
            if(sqlite3_column_text(sqlStament, 2)){
                model.laundryItemLang=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
            }
            if(sqlite3_column_blob(sqlStament,3)){
                NSData *image = [[NSData alloc] initWithBytes:sqlite3_column_blob(sqlStament, 3) length:sqlite3_column_bytes(sqlStament, 3)];
                [model setLaundryItemImg:image];
        
            }
            if(sqlite3_column_int(sqlStament, 4)){
                model.laundryItem_male_or_female=sqlite3_column_int(sqlStament,4);
                ;
            }
            if(sqlite3_column_text(sqlStament, 5)){
                model.laundry_last_modified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
                
            }
            return model;
        }
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    return  model;
}

-(LaundryItemModelV2*)loadLaundryItemByUserID:(int)ID tgender:(int)_tgender{ 
    
    NSMutableString * sqlString=[[NSMutableString alloc] initWithFormat:@"SELECT %@,%@,%@,%@,%@,%@ FROM %@ WhERE %@= ? AND %@= ?",ldry_Id,ldry_Name,ldry_Lang,ldry_image,ldry_male_or_female,ldry_item_last_modifier,LAUNDRY_ITEMS,ldry_Id,ldry_male_or_female];
    const char *sql=[sqlString UTF8String];

    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
     LaundryItemModelV2 *model =[[LaundryItemModelV2 alloc] init];
    
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {     
        sqlite3_bind_int(self.sqlStament, 1,ID);
        sqlite3_bind_int(self.sqlStament, 2,_tgender);
        
        status = sqlite3_step(sqlStament);
        while(status == SQLITE_ROW) {
            if(sqlite3_column_int(sqlStament, 0)){
                model.laundryItemId= sqlite3_column_int(sqlStament, 0);
            }
            if(sqlite3_column_text(sqlStament, 1)){
                model.laundryItemName=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
                
            }
            if(sqlite3_column_text(sqlStament, 2)){
                model.laundryItemLang=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
            }
            if(sqlite3_column_blob(sqlStament,3)){
                NSData *image = [[NSData alloc] initWithBytes:sqlite3_column_blob(sqlStament, 3) length:sqlite3_column_bytes(sqlStament, 3)];
                [model setLaundryItemImg:image];
                
            }
            if(sqlite3_column_int(sqlStament, 4)){
                model.laundryItem_male_or_female=sqlite3_column_int(sqlStament,4);
                ;
            }
            if(sqlite3_column_text(sqlStament, 5)){
                model.laundry_last_modified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
            }
            return model;
        }
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    return  model;
}

-(NSString *)getTheLatestLastModifierInLaundryItem
{
    NSMutableString * sqlString=[[NSMutableString alloc] initWithFormat:@"SELECT MAX(%@) FROM %@ ORDER BY %@ ASC",ldry_item_last_modifier,LAUNDRY_ITEMS,ldry_item_last_modifier];
    const char *sql=[sqlString UTF8String];

    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSString *lastModifier;
    
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {  
        
        status = sqlite3_step(sqlStament);
        while(status == SQLITE_ROW)
        {
            if(sqlite3_column_text(sqlStament, 0)){
                lastModifier=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 0)];
            }
            return lastModifier;
        }
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    return lastModifier;
}

@end
