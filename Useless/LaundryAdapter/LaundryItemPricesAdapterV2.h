//
//  LaundryItemPricesAdapterV2.h
//  mHouseKeeping
//
//  Created by Bien Do on 3/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LaundryItemPriceModelV2.h"
#import "DatabaseAdapterV2.h"

@interface LaundryItemPricesAdapterV2 : DatabaseAdapter{
    
}
-(int)insertLaundryItemPriceData:(LaundryItemPriceModelV2*) LaundryItemPriceModel;
-(int)updateLaundryItemPriceData:(LaundryItemPriceModelV2*) LaundryItemPriceModel;
-(int)deleteLaundryItemPriceData:(LaundryItemPriceModelV2*) LaundryItemPriceModel;
-(LaundryItemPriceModelV2 *)loadLaundryItemPriceModelV2ByLaundryIPModel:(LaundryItemPriceModelV2*)model;
-(NSMutableArray*)loadAllLaundryItemPriceModelV2;
-(NSMutableArray*)loadAllLaundryItemPriceByCategoryID:(int)ID;
-(LaundryItemPriceModelV2 *)loadLaundryItemPriceModelV2ByLaundryItemID:(int)itemID;
-(NSString *)getTheLatestLastModifierInLaundryItemPrice;

-(LaundryItemPriceModelV2 *) loadLaundryItemPriceModelV2ByLaundryItemId:(NSInteger)itemId andLaundryCategoryId:(NSInteger) categoryId;
@end
