//
//  LaundryItemPricesAdapterV2.m
//  mHouseKeeping
//
//  Created by Bien Do on 3/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LaundryItemPricesAdapterV2.h"
#import "ehkDefines.h"

@implementation LaundryItemPricesAdapterV2

-(int)insertLaundryItemPriceData:(LaundryItemPriceModelV2*) LaundryItemPriceModel
{
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"INSERT INTO %@ (%@, %@, %@, %@, %@) %@",LAUNDRY_ITEMS_PRICE,lip_id,lip_item_id,lip_category_id,lip_price,lip_last_modified,@"VALUES(?,?,?,?,?)"];     
    
    const char *sql =[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }

    NSInteger status1=sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status1 == SQLITE_OK) {
        
        sqlite3_bind_int(self.sqlStament, 1,LaundryItemPriceModel.lip_id);
        sqlite3_bind_int(self.sqlStament, 2,LaundryItemPriceModel.lip_item_id);
        sqlite3_bind_int(self.sqlStament, 3,LaundryItemPriceModel.lip_category_id);
        sqlite3_bind_double(self.sqlStament, 4,LaundryItemPriceModel.lip_price);
         sqlite3_bind_text(self.sqlStament,5, [LaundryItemPriceModel.lip_last_modified UTF8String], -1, SQLITE_TRANSIENT);
    }
    
    NSInteger status = sqlite3_step(self.sqlStament);
    if (status == SQLITE_DONE) {
        sqlite3_last_insert_rowid(self.database);
        sqlite3_reset(self.sqlStament);
        return 1;
    }
    else{
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return 0;
    }
    return  0;
}

-(int)updateLaundryItemPriceData:(LaundryItemPriceModelV2*) LaundryItemPriceModel{
    
    NSInteger returnInt = 0;
    if(self.sqlStament == nil) {
        NSString *sqlString = [[NSString alloc] initWithFormat:@"UPDATE %@ SET %@ = ?, %@ = ?,%@= ?,%@=? WHERE %@ = ?",LAUNDRY_ITEMS_PRICE,lip_item_id,lip_category_id,lip_price,lip_last_modified,lip_id];
        const char *sql = [sqlString UTF8String];
      
        NSInteger reAccessCount = 0;
        RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
            reAccessCount ++;
        }
        
        NSInteger prepare = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
		if(prepare != SQLITE_OK)
        {
        }
        sqlite3_bind_int(self.sqlStament, 1,LaundryItemPriceModel.lip_item_id);
        sqlite3_bind_int(self.sqlStament, 2,LaundryItemPriceModel.lip_category_id);
        sqlite3_bind_double(self.sqlStament, 3,LaundryItemPriceModel.lip_price);
        sqlite3_bind_text(self.sqlStament,4, [LaundryItemPriceModel.lip_last_modified UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_int(self.sqlStament,5,LaundryItemPriceModel.lip_id);
        
        NSInteger step = sqlite3_step(self.sqlStament);
        if(SQLITE_DONE != step)
        {
            if (step == SQLITE_BUSY) {
                if (reAccessCount <= REACCESS_MAX_TIMES) {
                    goto RE_ACCESS_DB;
                }
            }
            return returnInt;
        }
        else
        { 
            return 1;
        } 
    }
    return returnInt;
}

-(int)deleteLaundryItemPriceData:(LaundryItemPriceModelV2*) LaundryItemPriceModel{
    NSInteger result = 1;
    if(self.sqlStament == nil) {
        NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"DELETE FROM %@ WHERE %@ = ?",LAUNDRY_ITEMS_PRICE,lip_id];
		const char *sql = [sqlString UTF8String];
      
        NSInteger reAccessCount = 0;
        RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
            reAccessCount ++;
        }
        
		if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
			result = 0;
        
        sqlite3_bind_int(self.sqlStament, 1,LaundryItemPriceModel.lip_id);	
        
        NSInteger status = sqlite3_step(self.sqlStament);
        if (SQLITE_DONE != status) {
            if (status == SQLITE_BUSY) {
                if (reAccessCount <= REACCESS_MAX_TIMES) {
                    goto RE_ACCESS_DB;
                }
            }
            result = 0;
        }
        
        return result;

	}
	return result;
}

-(LaundryItemPriceModelV2 *)loadLaundryItemPriceModelV2ByLaundryItemId:(NSInteger)itemId andLaundryCategoryId:(NSInteger)categoryId {
    NSMutableString * sqlString=[[NSMutableString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ? AND %@ = ?",
                                 lip_id, 
                                 lip_item_id, 
                                 lip_category_id,
                                 lip_price,
                                 lip_last_modified,
                                 LAUNDRY_ITEMS_PRICE,
                                 lip_item_id,
                                 lip_category_id
                                 ];
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    LaundryItemPriceModelV2 *model = [[LaundryItemPriceModelV2 alloc] init];
    
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {     
        
        sqlite3_bind_int(self.sqlStament, 1, itemId);
        
        sqlite3_bind_int(sqlStament, 2, categoryId);
        
        NSInteger status = sqlite3_step(sqlStament);
        while(status == SQLITE_ROW) {
            
            model.lip_id = sqlite3_column_int(sqlStament, 0);
            
            model.lip_item_id = sqlite3_column_int(sqlStament, 1);
                
            model.lip_category_id = sqlite3_column_int(sqlStament, 2);

            model.lip_price = sqlite3_column_double(sqlStament, 3);
            
            if(sqlite3_column_text(sqlStament, 4)) {
                model.lip_last_modified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
            }
            
            return model;
        }
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    return model;
}

-(LaundryItemPriceModelV2 *)loadLaundryItemPriceModelV2ByLaundryIPModel:(LaundryItemPriceModelV2*)model
{
    NSMutableString * sqlString=[[NSMutableString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ? ",
                                 lip_id, 
                                 lip_item_id, 
                                 lip_category_id,
                                 lip_price,
                                 lip_last_modified,
                                 LAUNDRY_ITEMS_PRICE,
                                 lip_id
                                 ];
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {     
         sqlite3_bind_int(self.sqlStament, 1,model.lip_id);
        
        status = sqlite3_step(sqlStament);
        while(status == SQLITE_ROW) {
            
         
            if(sqlite3_column_int(sqlStament, 0)){
                model.lip_id= sqlite3_column_int(sqlStament, 0);
            }
            if(sqlite3_column_int(sqlStament, 1)){
                model.lip_item_id=sqlite3_column_int(sqlStament, 1);
                
            }
            if(sqlite3_column_int(sqlStament, 2)){
                model.lip_category_id= sqlite3_column_int(sqlStament, 2);
            }
            if(sqlite3_column_double(sqlStament, 3)){
                model.lip_price=sqlite3_column_double(sqlStament,3);
            }

            if(sqlite3_column_text(sqlStament, 4)){
                model.lip_last_modified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
                
            }
            return model;
        }
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    return model;


}
-(LaundryItemPriceModelV2 *)loadLaundryItemPriceModelV2ByLaundryItemID:(int)itemID
{
    NSMutableString * sqlString=[[NSMutableString alloc] initWithFormat:@"SELECT %@,%@,%@,%@,%@ FROM %@ WHERE %@=? ",lip_id,lip_item_id,lip_category_id,lip_price,lip_last_modified,LAUNDRY_ITEMS_PRICE,lip_item_id];
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {     
        sqlite3_bind_int(self.sqlStament, 1,itemID);
        
        status = sqlite3_step(sqlStament);
        while(status == SQLITE_ROW) {
            LaundryItemPriceModelV2 *model=[[LaundryItemPriceModelV2 alloc] init];
            
            if(sqlite3_column_int(sqlStament, 0)){
                model.lip_id= sqlite3_column_int(sqlStament, 0);
            }
            if(sqlite3_column_int(sqlStament, 1)){
                model.lip_item_id=sqlite3_column_int(sqlStament, 1);
                
            }
            if(sqlite3_column_int(sqlStament, 2)){
                model.lip_category_id= sqlite3_column_int(sqlStament, 2);
            }
            if(sqlite3_column_double(sqlStament, 3)){
                model.lip_price=sqlite3_column_double(sqlStament,3);
            }
            
            if(sqlite3_column_text(sqlStament, 4)){
                model.lip_last_modified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
                
            }
             return model;
            
        }
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    return nil;
    
    
}

-(NSMutableArray*)loadAllLaundryItemPriceModelV2{
    NSMutableArray * array=[NSMutableArray array];
    NSMutableString * sqlString=[[NSMutableString alloc] initWithFormat:@"SELECT %@,%@,%@,%@,%@ FROM %@ ",lip_id,lip_item_id,lip_category_id,lip_price,lip_last_modified,LAUNDRY_ITEMS_PRICE];
    const char *sql=[sqlString UTF8String];

    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {     
        
        status = sqlite3_step(sqlStament);
        while(status == SQLITE_ROW) {
            
            LaundryItemPriceModelV2 *model =[[LaundryItemPriceModelV2 alloc] init];
            if(sqlite3_column_int(sqlStament, 0)){
                model.lip_id= sqlite3_column_int(sqlStament, 0);
            }
            if(sqlite3_column_int(sqlStament, 1)){
                model.lip_item_id=sqlite3_column_int(sqlStament, 1);
                
            }
            if(sqlite3_column_int(sqlStament, 2)){
                model.lip_category_id= sqlite3_column_int(sqlStament, 2);
            }
            if(sqlite3_column_double(sqlStament, 3)){
                model.lip_price=sqlite3_column_double(sqlStament,3);
            }
            
            if(sqlite3_column_text(sqlStament, 4)){
                model.lip_last_modified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
                
            }
            [array addObject:model];

            status = sqlite3_step(sqlStament);
        }
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                [array removeAllObjects];
                goto RE_ACCESS_DB;
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    return  array;
    

}
-(NSMutableArray*)loadAllLaundryItemPriceByCategoryID:(int)ID{
    
    NSMutableArray * array=[NSMutableArray array];
    NSMutableString * sqlString=[[NSMutableString alloc] initWithFormat:@"SELECT %@,%@,%@,%@,%@ FROM %@ WHERE %@=%d",lip_id,lip_item_id,lip_category_id,lip_price,lip_last_modified,LAUNDRY_ITEMS_PRICE,lip_category_id,ID];
    const char *sql=[sqlString UTF8String];
   
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {     
        
        status = sqlite3_step(sqlStament);
        while(status == SQLITE_ROW) {
            
            LaundryItemPriceModelV2 *model =[[LaundryItemPriceModelV2 alloc] init];
            if(sqlite3_column_int(sqlStament, 0)){
                model.lip_id= sqlite3_column_int(sqlStament, 0);
            }
            if(sqlite3_column_int(sqlStament, 1)){
                model.lip_item_id=sqlite3_column_int(sqlStament, 1);
                
            }
            if(sqlite3_column_int(sqlStament, 2)){
                model.lip_category_id= sqlite3_column_int(sqlStament, 2);
            }
            if(sqlite3_column_double(sqlStament, 3)){
                model.lip_price=sqlite3_column_double(sqlStament,3);
            }
            
            if(sqlite3_column_text(sqlStament, 4)){
                model.lip_last_modified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
                
            }
            [array addObject:model];
            
            status = sqlite3_step(sqlStament);
        }
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                [array removeAllObjects];
                goto RE_ACCESS_DB;
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    return  array;
    

}
-(NSString *)getTheLatestLastModifierInLaundryItemPrice
{
    NSMutableString * sqlString=[[NSMutableString alloc] initWithFormat:@"SELECT MAX(%@) FROM %@ ORDER BY %@ ASC",lip_last_modified,LAUNDRY_ITEMS_PRICE,lip_last_modified];
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
        
    NSString *lastModifier;
    
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {  
        
        status = sqlite3_step(sqlStament);
        while(status == SQLITE_ROW)
        {
            if(sqlite3_column_text(sqlStament, 0)){
                lastModifier=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 0)];
            }
            return lastModifier;
            
        }
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    return lastModifier;
}

@end
