//
//  LaundryItemTempAdapterV2.h
//  mHouseKeeping
//
//  Created by TMS on 5/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LaundryOrderDetailModelV2.h"
#import "DatabaseAdapterV2.h"

@interface LaundryItemTempAdapterV2  : DatabaseAdapterV2
{}
-(int)insertLaundryOrderDetailTempData:(LaundryOrderDetailModelV2*) LaundryOrderDetailModel;
-(int)updateLaundryOrderDetailTempData:(LaundryOrderDetailModelV2*) LaundryOrderDetailModel;
-(int)deleteLaundryOrderDetailTempData:(LaundryOrderDetailModelV2*) LaundryOrderDetailModel;
-(NSMutableArray*)loadAllLaundryOrderDetailModelV2Temp;
-(NSMutableArray*)loadAllLaundryOrderDetailIDTemp:(int)ID; 
-(LaundryOrderDetailModelV2 *) loadLaundryOrderDetailsTemp:(LaundryOrderDetailModelV2 *) laundryOrderDetail;
-(NSMutableArray *) loadAllLaundryOrderDetailsTempByServiceId: (NSInteger) serviceId;
-(int)deleteAllLaundryOrderDetailTempData;
@end
