//
//  LaundryOrderAdapterV2.h
//  mHouseKeeping
//
//  Created by Bien Do on 3/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DatabaseAdapterV2.h"
#import "LaudryOrderModelV2.h"

@interface LaundryOrderAdapterV2 : DatabaseAdapterV2{
    
}
-(int)insertLaudryOrderData:(LaudryOrderModelV2*) LaudryOrderModel;
-(int)updateLaudryOrderData:(LaudryOrderModelV2*) LaudryOrderModel;
-(int)deleteLaudryOrderData:(LaudryOrderModelV2*) LaudryOrderModel;
-(NSMutableArray*)loadAllLaudryOrderV2;
-(NSMutableArray*)loadAllLaudryOrderID:(int)ID; 
-(LaudryOrderModelV2 *)loadLaundryOrderModelByRoomIdUserIdAndCollectDate:(LaudryOrderModelV2 *)model;
-(LaudryOrderModelV2 *)loadLaundryOrderModelByUserID:(int)userID Location:(int)roomID andTime :(NSString *)dateCreate;
-(NSMutableArray*)loadAllLaundryOrderByUserId:(NSInteger) userId;
@end
