//
//  LaundryOrderAdapterV2.m
//  mHouseKeeping
//
//  Created by Bien Do on 3/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LaundryOrderAdapterV2.h"
#import "DbDefinesV2.h"
#import "ehkConvert.h"
#import "ehkDefines.h"

@implementation LaundryOrderAdapterV2

-(int)insertLaudryOrderData:(LaudryOrderModelV2*) LaudryOrderModel{
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"INSERT INTO %@ ( %@, %@, %@,%@,%@) %@",LAUNDRY_ORDER,lo_room_id,lo_user_id,lo_status,lo_collect_date,lo_remark,@"VALUES(?,?,?,?,?)"];     
    
    const char *sql =[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSInteger status1=sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status1 == SQLITE_OK) {
        
       
        sqlite3_bind_int(self.sqlStament, 1,LaudryOrderModel.laundryOrderRoomId);
        sqlite3_bind_int(self.sqlStament, 2,LaudryOrderModel.laundryOrderUserId);
        sqlite3_bind_int(self.sqlStament, 3,LaudryOrderModel.laundryOrderStatus);
        sqlite3_bind_text(self.sqlStament, 4, [LaudryOrderModel.laundryOrderCollectDate UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(self.sqlStament, 5, [LaudryOrderModel.laundryOrderRemark UTF8String], -1, SQLITE_TRANSIENT);
    }
    
    NSInteger status = sqlite3_step(self.sqlStament);
    if (status == SQLITE_DONE) {
        sqlite3_reset(self.sqlStament);
        return sqlite3_last_insert_rowid(database);
    }else
    {
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return  0;
    }
}

-(int)updateLaudryOrderData:(LaudryOrderModelV2*) LaudryOrderModel{
    
    if(self.sqlStament == nil) {
        NSString *sqlString = [[NSString alloc] initWithFormat:@"UPDATE %@ SET %@ = ?, %@ = ?,%@= ?,%@=?,%@ = ? WHERE %@ = ?",LAUNDRY_ORDER,lo_room_id,lo_user_id,lo_status,lo_collect_date,lo_remark,lo_room_id];
        const char *sql = [sqlString UTF8String];
        
        NSInteger reAccessCount = 0;
        RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
            reAccessCount ++;
        }
        
        NSInteger prepare = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
		if(prepare != SQLITE_OK)
        {
        }
        
        sqlite3_bind_int(self.sqlStament, 1,LaudryOrderModel.laundryOrderRoomId);
        sqlite3_bind_int(self.sqlStament, 2,LaudryOrderModel.laundryOrderUserId);
        sqlite3_bind_int(self.sqlStament, 3,LaudryOrderModel.laundryOrderStatus);
        sqlite3_bind_text(self.sqlStament, 4, [LaudryOrderModel.laundryOrderCollectDate UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(self.sqlStament, 5, [LaudryOrderModel.laundryOrderRemark UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_int (sqlStament, 6,LaudryOrderModel.laundryOrderRoomId);
        
        NSInteger returnInt = 0;
        NSInteger step = sqlite3_step(self.sqlStament);
        if(SQLITE_DONE != step)
        {
            if (step == SQLITE_BUSY) {
                if (reAccessCount <= REACCESS_MAX_TIMES) {
                    goto RE_ACCESS_DB;
                }
            }
            return returnInt;
        }
        else
        { 
            return 1;
        } 

    }
    return 0;      
}

-(int)deleteLaudryOrderData:(LaudryOrderModelV2*) LaudryOrderModel{
 
    NSInteger result = 1;
    if(self.sqlStament == nil) {
        NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"DELETE FROM %@ WHERE %@ = ?",LAUNDRY_ORDER,lo_id];
		const char *sql = [sqlString UTF8String];

        NSInteger reAccessCount = 0;
        RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
            reAccessCount ++;
        }
        
		if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
        {
            
        }
//			result = 0;
        
        sqlite3_bind_int(self.sqlStament, 1,LaudryOrderModel.laundryOrderId);	
        
        NSInteger status = sqlite3_step(self.sqlStament);
        if (SQLITE_DONE != status) {
            if (status == SQLITE_BUSY) {
                if (reAccessCount <= REACCESS_MAX_TIMES) {
                    goto RE_ACCESS_DB;
                }
            }
            result = 0;
        }
        else{
            result = 1;
        }
	}
	return result;
}

-(NSMutableArray*)loadAllLaudryOrderV2{
    
    
    NSMutableArray * array=[NSMutableArray array];
    NSMutableString * sqlString=[[NSMutableString alloc] initWithFormat:@"SELECT %@,%@,%@,%@,%@,%@ FROM %@ ",lo_id,lo_room_id,lo_user_id,lo_status,lo_collect_date,lo_remark,LAUNDRY_ORDER];
    const char *sql=[sqlString UTF8String];
   
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {     
        
        status = sqlite3_step(self.sqlStament);
        while(status == SQLITE_ROW) {
            
            LaudryOrderModelV2 *model =[[LaudryOrderModelV2 alloc] init];
            if(sqlite3_column_int(sqlStament, 0)){
                model.laundryOrderId= sqlite3_column_int(sqlStament, 0);
            }
            if(sqlite3_column_int(sqlStament, 1)){
                model.laundryOrderRoomId= sqlite3_column_int(sqlStament, 1);
            }
            if(sqlite3_column_int(sqlStament, 2)){
                model.laundryOrderUserId= sqlite3_column_int(sqlStament, 2);
            }
            if(sqlite3_column_int(sqlStament, 3)){
                model.laundryOrderStatus= sqlite3_column_int(sqlStament, 3);
            }
            if(sqlite3_column_text(sqlStament, 4)){
                model.laundryOrderCollectDate=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
            }
            if(sqlite3_column_text(sqlStament, 5)){
                model.laundryOrderRemark=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament,5)];
            }

            [array addObject:model];
            
            status = sqlite3_step(self.sqlStament);

        }
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                [array removeAllObjects];
                goto RE_ACCESS_DB;
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    return  array;
}

-(NSMutableArray*)loadAllLaudryOrderID:(int)ID{
    NSMutableArray * array=[NSMutableArray array];
    NSMutableString * sqlString=[[NSMutableString alloc] initWithFormat:@"SELECT %@,%@,%@,%@,%@,%@ FROM %@ WHERE %@=%d",lo_id,lo_room_id,lo_user_id,lo_status,lo_collect_date,lo_remark,LAUNDRY_ORDER,lo_id,ID];
    const char *sql=[sqlString UTF8String];

    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {     
        
        status = sqlite3_step(sqlStament);
        while(status == SQLITE_ROW) {
            
            LaudryOrderModelV2 *model =[[LaudryOrderModelV2 alloc] init];
            if(sqlite3_column_int(sqlStament, 0)){
                model.laundryOrderId= sqlite3_column_int(sqlStament, 0);
            }
            if(sqlite3_column_int(sqlStament, 1)){
                model.laundryOrderRoomId= sqlite3_column_int(sqlStament, 1);
            }
            if(sqlite3_column_int(sqlStament, 2)){
                model.laundryOrderUserId= sqlite3_column_int(sqlStament, 2);
            }
            if(sqlite3_column_int(sqlStament, 3)){
                model.laundryOrderStatus= sqlite3_column_int(sqlStament, 3);
            }
            if(sqlite3_column_text(sqlStament, 4)){
                model.laundryOrderCollectDate=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
            }
            if(sqlite3_column_text(sqlStament, 5)){
                model.laundryOrderRemark=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament,5)];
            }
            
            [array addObject:model];
            
            status = sqlite3_step(sqlStament);

        }
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                [array removeAllObjects];
                goto RE_ACCESS_DB;
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    return  array;
    

}

-(LaudryOrderModelV2 *)loadLaundryOrderModelByRoomIdUserIdAndCollectDate:(LaudryOrderModelV2 *)model
{
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@ ,%@,%@ ,%@, %@ FROM %@ WHERE %@ = ? AND %@ = ? AND %@ = ?", 
                           lo_id,
                           lo_room_id,
                           lo_user_id,
                           lo_status,
                           lo_remark,
                           lo_collect_date,
                           LAUNDRY_ORDER,
                           lo_room_id,
                           lo_user_id,
                           lo_collect_date
                           ];
    
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
        
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        
        sqlite3_bind_int (self.sqlStament, 1, model.laundryOrderRoomId);
        sqlite3_bind_int (self.sqlStament, 2, model.laundryOrderUserId);
        sqlite3_bind_text(self.sqlStament, 3, [model.laundryOrderCollectDate UTF8String], -1, SQLITE_TRANSIENT);

        status = sqlite3_step(sqlStament);
        while(status == SQLITE_ROW) 
        {
            model.laundryOrderId = sqlite3_column_int(sqlStament, 0);
            model.laundryOrderRoomId = sqlite3_column_int(sqlStament, 1);
            model.laundryOrderUserId = sqlite3_column_int(sqlStament, 2);
            model.laundryOrderStatus = sqlite3_column_int(sqlStament, 3);
            if (sqlite3_column_text(sqlStament, 4))
            {   
                model.laundryOrderRemark=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
            }
            
            if (sqlite3_column_text(sqlStament, 5)) {
                model.laundryOrderCollectDate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
            }
            
            return model;
        
        }
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    return model;
    
}

-(LaudryOrderModelV2 *)loadLaundryOrderModelByUserID:(int)userID Location:(int)roomID andTime :(NSString *)dateCreate
{
  
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@ ,%@,%@ ,%@, %@ FROM %@ WHERE %@ = ? AND %@ = ? ", 
                           lo_id,
                           lo_room_id,
                           lo_user_id,
                           lo_status,
                           lo_remark,
                           lo_collect_date,
                           LAUNDRY_ORDER,
                           lo_user_id,
                           lo_room_id
                           ];
    
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
        
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        
        sqlite3_bind_int (self.sqlStament, 1, userID);
        sqlite3_bind_int (self.sqlStament, 2, roomID);

        status = sqlite3_step(sqlStament);
        while(status == SQLITE_ROW) 
        {
            LaudryOrderModelV2 *model=[[LaudryOrderModelV2 alloc] init];
    
            model.laundryOrderId = sqlite3_column_int(sqlStament, 0);
            model.laundryOrderRoomId = sqlite3_column_int(sqlStament, 1);
            model.laundryOrderUserId = sqlite3_column_int(sqlStament, 2);
            model.laundryOrderStatus = sqlite3_column_int(sqlStament, 3);
            
            if (sqlite3_column_text(sqlStament, 4))
            {   
                model.laundryOrderRemark=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
            }
            if (sqlite3_column_text(sqlStament, 5)) {
                model.laundryOrderCollectDate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 5)];
            }
            return model;
        }
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    return nil;
    
}

-(NSMutableArray *)loadAllLaundryOrderByUserId:(NSInteger)userId{
    NSMutableArray * array=[NSMutableArray array];
    
    NSMutableString * sqlString = [[NSMutableString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ?",
                                 lo_id,
                                 lo_room_id,
                                 lo_user_id,
                                 lo_status,
                                 lo_collect_date,
                                 lo_remark,
                                 LAUNDRY_ORDER,
                                 lo_user_id
                                 ];
    
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {  
        
        sqlite3_bind_int (self.sqlStament, 1, userId);

        status = sqlite3_step(sqlStament);
        while(status == SQLITE_ROW) {
            
            LaudryOrderModelV2 *model =[[LaudryOrderModelV2 alloc] init];
            
            if(sqlite3_column_int(sqlStament, 0)){
                model.laundryOrderId= sqlite3_column_int(sqlStament, 0);
            }
            if(sqlite3_column_int(sqlStament, 1)){
                model.laundryOrderRoomId= sqlite3_column_int(sqlStament, 1);
            }
            if(sqlite3_column_int(sqlStament, 2)){
                model.laundryOrderUserId= sqlite3_column_int(sqlStament, 2);
            }
            if(sqlite3_column_int(sqlStament, 3)){
                model.laundryOrderStatus= sqlite3_column_int(sqlStament, 3);
            }
            if(sqlite3_column_text(sqlStament, 4)){
                model.laundryOrderCollectDate=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
            }
            if(sqlite3_column_text(sqlStament, 5)){
                model.laundryOrderRemark=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament,5)];
            }
            
            [array addObject:model];
            
            status = sqlite3_step(sqlStament);
        }
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                [array removeAllObjects];
                goto RE_ACCESS_DB;
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    return  array;

}

@end
