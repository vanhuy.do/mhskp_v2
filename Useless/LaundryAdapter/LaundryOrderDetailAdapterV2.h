//
//  LaundryOrderDetailAdapterV2.h
//  mHouseKeeping
//
//  Created by Bien Do on 3/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LaundryOrderDetailModelV2.h"
#import "DatabaseAdapterV2.h"
@interface LaundryOrderDetailAdapterV2 : DatabaseAdapterV2{
    
}
-(int)insertLaundryOrderDetailData:(LaundryOrderDetailModelV2*) LaundryOrderDetailModel;
-(int)updateLaundryOrderDetailData:(LaundryOrderDetailModelV2*) LaundryOrderDetailModel;
-(int)deleteLaundryOrderDetailData:(LaundryOrderDetailModelV2*) LaundryOrderDetailModel;
-(NSMutableArray*)loadAllLaundryOrderDetailModelV2;
-(NSMutableArray*)loadAllLaundryOrderDetailID:(int)ID; 
-(LaundryOrderDetailModelV2 *) loadLaundryOrderDetailsByItemId:(NSInteger) laundryItemId;
-(LaundryOrderDetailModelV2 *) loadLaundryOrderDetails:(LaundryOrderDetailModelV2 *) laundryOrderDetail;
-(NSMutableArray *) loadAllLaundryOrderDetailsByOrderId: (NSInteger) orderId;
-(LaundryOrderDetailModelV2 *) loadLaundryOrderDetailsByItemId:(LaundryOrderDetailModelV2 *) laundryItemId AndOrderId:(NSInteger) orderId;
-(NSMutableArray *) loadAllLaundryOrderDetailsByServiceId:(NSInteger) serviceId AndOrderId: (NSInteger) orderId;
@end
