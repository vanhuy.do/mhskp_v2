//
//  LaundryOrderDetailAdapterV2.m
//  mHouseKeeping
//
//  Created by Bien Do on 3/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LaundryOrderDetailAdapterV2.h"
#import "DbDefinesV2.h"
#import "ehkDefines.h"

@implementation LaundryOrderDetailAdapterV2

-(int)insertLaundryOrderDetailData:(LaundryOrderDetailModelV2*) LaundryOrderDetailModel{
   
    NSString *sqlString = [[NSString alloc] initWithFormat:@"INSERT INTO %@ (%@,%@,%@,%@,%@,%@) %@",    
                           LAUNDRY_ORDER_DETAILS,
                           lod_laundry_order_id,
                           lod_item_id,
                           lod_quantity,
                           lod_post_status,
                           lod_service_id,
                           lod_category_id,
                           @"VALUES(?,?,?,?,?,?)"
                           ];     
    
    const char *sql =[sqlString UTF8String];
  
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSInteger status1=sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status1 == SQLITE_OK) {
        
        sqlite3_bind_int(self.sqlStament, 1,LaundryOrderDetailModel.lod_laundry_order_id);
        sqlite3_bind_int(self.sqlStament, 2,LaundryOrderDetailModel.lod_item_id);
        sqlite3_bind_int(self.sqlStament, 3,LaundryOrderDetailModel.lod_quantity);
        sqlite3_bind_int(self.sqlStament, 4,LaundryOrderDetailModel.lod_post_status);
        sqlite3_bind_int(self.sqlStament, 5,LaundryOrderDetailModel.lod_service_id);
        sqlite3_bind_int(self.sqlStament, 6,LaundryOrderDetailModel.lod_category_id);
    }
    
    NSInteger status = sqlite3_step(self.sqlStament);
    if (status == SQLITE_DONE) {
        sqlite3_last_insert_rowid(self.database);
        sqlite3_reset(self.sqlStament);
        return 1;
    }
    else{
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return  0;
    }
    return 0;
}

-(int)updateLaundryOrderDetailData:(LaundryOrderDetailModelV2*) LaundryOrderDetailModel{
    
    
    if(self.sqlStament == nil) {
        NSString *sqlString = [[NSString alloc] initWithFormat:@"UPDATE %@ SET %@ = ?, %@ = ?, %@ = ? WHERE %@ = ? AND %@ = ? AND %@ = ?",
                               LAUNDRY_ORDER_DETAILS,
                               lod_laundry_order_id,
                               lod_quantity,
                               lod_post_status,
                               lod_item_id,
                               lod_service_id,
                               lod_category_id
                               ];
        const char *sql = [sqlString UTF8String];
        
        NSInteger reAccessCount = 0;
        RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
            reAccessCount ++;
        }
        
        NSInteger prepare = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
		if(prepare != SQLITE_OK)
        {
        }
       
        sqlite3_bind_int(self.sqlStament,1,LaundryOrderDetailModel.lod_laundry_order_id);
        sqlite3_bind_int(self.sqlStament, 2,LaundryOrderDetailModel.lod_quantity);
        sqlite3_bind_int(self.sqlStament, 3,LaundryOrderDetailModel.lod_post_status);
        
        sqlite3_bind_int(self.sqlStament, 4,LaundryOrderDetailModel.lod_item_id);
        sqlite3_bind_int(self.sqlStament, 5,LaundryOrderDetailModel.lod_service_id);
        sqlite3_bind_int(sqlStament, 6, LaundryOrderDetailModel.lod_category_id);
        
        NSInteger returnInt = 0;
        NSInteger step = sqlite3_step(self.sqlStament);
        if(SQLITE_DONE != step)
        {
            if (step == SQLITE_BUSY) {
                if (reAccessCount <= REACCESS_MAX_TIMES) {
                    goto RE_ACCESS_DB;
                }
            }
            return returnInt;
        }
        else
        { 
            return 1;
        } 

    }
    return 0;
}

-(int)deleteLaundryOrderDetailData:(LaundryOrderDetailModelV2*) LaundryOrderDetailModel{
    
    
    NSInteger result = 1;
    if(self.sqlStament == nil) {
        NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"DELETE FROM %@ WHERE %@ = ?",LAUNDRY_ORDER_DETAILS,lod_id];
		const char *sql = [sqlString UTF8String];
       
        NSInteger reAccessCount = 0;
        RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
            reAccessCount ++;
        }
        
		if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
        {
//			result = 0;
        }
        sqlite3_bind_int(self.sqlStament, 1,LaundryOrderDetailModel.lod_id);	
        
        NSInteger status = sqlite3_step(self.sqlStament);
        if (SQLITE_DONE != status) {
            if (status == SQLITE_BUSY) {
                if (reAccessCount <= REACCESS_MAX_TIMES) {
                    goto RE_ACCESS_DB;
                }
            }
            result = 0;
        }
        else{
            result = 1;
        }
	}
	return result;
}

-(NSMutableArray*)loadAllLaundryOrderDetailModelV2{
    
    
    NSMutableArray * array=[NSMutableArray array];
    NSMutableString * sqlString=[[NSMutableString alloc] initWithFormat:@"SELECT %@,%@,%@,%@,%@,%@ ,%@ FROM %@ ",
                                 lod_id,
                                 lod_laundry_order_id,
                                 lod_item_id,
                                 lod_quantity,
                                 lod_post_status,
                                 lod_service_id,
                                 lod_category_id,
                                 LAUNDRY_ORDER_DETAILS
                                 ];
    const char *sql=[sqlString UTF8String];

    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {     
        
        status = sqlite3_step(sqlStament);
        while(status == SQLITE_ROW) {
            
            LaundryOrderDetailModelV2 *model =[[LaundryOrderDetailModelV2 alloc] init];
            if(sqlite3_column_int(sqlStament, 0)){
                model.lod_id= sqlite3_column_int(sqlStament, 0);
            }
            if(sqlite3_column_int(sqlStament, 1)){
                model.lod_laundry_order_id= sqlite3_column_int(sqlStament, 1);
            }
            if(sqlite3_column_int(sqlStament, 2)){
                model.lod_item_id= sqlite3_column_int(sqlStament, 2);
            }
            if(sqlite3_column_int(sqlStament, 3)){
                model.lod_quantity= sqlite3_column_int(sqlStament, 3);
            }
            if(sqlite3_column_int(sqlStament, 4)){
                    model.lod_post_status= sqlite3_column_int(sqlStament, 4);
                }
            if(sqlite3_column_int(sqlStament, 5)){
                model.lod_service_id= sqlite3_column_int(sqlStament, 5);
            }
            if(sqlite3_column_int(sqlStament, 6)){
                model.lod_category_id= sqlite3_column_int(sqlStament, 6);
            }
            [array addObject:model];
            
            status = sqlite3_step(sqlStament);
        }
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                [array removeAllObjects];
                goto RE_ACCESS_DB;  
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;  
        }
    }
    
    return  array;
}

-(NSMutableArray*)loadAllLaundryOrderDetailID:(int)ID{
    
    
    NSMutableArray * array=[NSMutableArray array];
    NSMutableString * sqlString=[[NSMutableString alloc] initWithFormat:@"SELECT %@,%@,%@,%@,%@,%@ FROM %@ WHERE %@=%d",
                                 lod_id,
                                 lod_laundry_order_id,
                                 lod_item_id,
                                 lod_quantity,
                                 lod_post_status,
                                 lod_service_id,
                                 lod_category_id,
                                 LAUNDRY_ORDER_DETAILS,
                                 lod_laundry_order_id,
                                 ID
                                 ];
    const char *sql=[sqlString UTF8String];

    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
        
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {     
        
        status = sqlite3_step(sqlStament);
        while(status == SQLITE_ROW) {
            
            LaundryOrderDetailModelV2 *model =[[LaundryOrderDetailModelV2 alloc] init];
            if(sqlite3_column_int(sqlStament, 0)){
                model.lod_id= sqlite3_column_int(sqlStament, 0);
            }
            if(sqlite3_column_int(sqlStament, 1)){
                model.lod_laundry_order_id= sqlite3_column_int(sqlStament, 1);
            }
            if(sqlite3_column_int(sqlStament, 2)){
                model.lod_item_id= sqlite3_column_int(sqlStament, 2);
            }
            if(sqlite3_column_int(sqlStament, 3)){
                model.lod_quantity= sqlite3_column_int(sqlStament, 3);
            }
            if(sqlite3_column_int(sqlStament, 4)){
                model.lod_post_status= sqlite3_column_int(sqlStament, 4);
            }
            if(sqlite3_column_int(sqlStament, 5)){
                model.lod_service_id= sqlite3_column_int(sqlStament,5);
            }
            if(sqlite3_column_int(sqlStament, 6)){
                model.lod_category_id= sqlite3_column_int(sqlStament, 6);
            }
            
            [array addObject:model];
            status = sqlite3_step(sqlStament);
        }
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                [array removeAllObjects];
                goto RE_ACCESS_DB;
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    return  array;
}

-(LaundryOrderDetailModelV2 *) loadLaundryOrderDetailsByItemId:(NSInteger) laundryItemId{
   
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@ ,%@, %@, %@, %@, %@ FROM %@ WHERE %@ = ?", 
                          lod_id,
                           lod_laundry_order_id,
                           lod_item_id,
                           lod_quantity,
                           lod_post_status,
                           lod_service_id,
                           LAUNDRY_ORDER_DETAILS,
                           lod_item_id
                           ];
    
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    LaundryOrderDetailModelV2 *model = [[LaundryOrderDetailModelV2 alloc] init];
    
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        
        sqlite3_bind_int (self.sqlStament, 1, laundryItemId);
        
        status = sqlite3_step(sqlStament);
        while(status == SQLITE_ROW) 
        {
            model.lod_id = sqlite3_column_int(sqlStament, 0);
            model.lod_laundry_order_id = sqlite3_column_int(sqlStament, 1);
            model.lod_item_id = sqlite3_column_int(sqlStament, 2);
            model.lod_quantity = sqlite3_column_int(sqlStament, 3);
            model.lod_post_status = sqlite3_column_int(sqlStament, 4);
            model.lod_service_id = sqlite3_column_int(sqlStament, 5);
            
            return model;
        }
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    return model;
}

-(LaundryOrderDetailModelV2 *)loadLaundryOrderDetails:(LaundryOrderDetailModelV2 *)model
{
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@ ,%@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ? AND %@ = ? AND %@ = ?", 
                           lod_id,
                           lod_laundry_order_id,
                           lod_item_id,
                           lod_quantity,
                           lod_post_status,
                           lod_service_id,
                           lod_category_id,
                           LAUNDRY_ORDER_DETAILS,
                           lod_item_id,
                           lod_service_id,
                           lod_category_id
                           ];
    
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        
        sqlite3_bind_int (self.sqlStament, 1, model.lod_item_id);
        sqlite3_bind_int (self.sqlStament, 2, model.lod_service_id);
        sqlite3_bind_int (self.sqlStament, 3, model.lod_category_id);
        
         status = sqlite3_step(sqlStament);
        while(status == SQLITE_ROW) 
        {
            model.lod_id = sqlite3_column_int(sqlStament, 0);
            model.lod_laundry_order_id = sqlite3_column_int(sqlStament, 1);
            model.lod_item_id = sqlite3_column_int(sqlStament, 2);
            model.lod_quantity = sqlite3_column_int(sqlStament, 3);
            model.lod_post_status = sqlite3_column_int(sqlStament, 4);
            model.lod_service_id = sqlite3_column_int(sqlStament, 5);
            model.lod_category_id = sqlite3_column_int(sqlStament, 6);
            
            return model;
        }
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    return model;
}

-(NSMutableArray *) loadAllLaundryOrderDetailsByOrderId: (NSInteger) orderId{
    
    NSMutableArray * array=[NSMutableArray array];
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@ ,%@, %@, %@, %@ FROM %@ WHERE %@ = ?", 
                           lod_id,
                           lod_laundry_order_id,
                           lod_item_id,
                           lod_quantity,
                           lod_post_status,
                           lod_service_id,
                           LAUNDRY_ORDER_DETAILS,
                           lod_laundry_order_id
                           ];
    
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
       
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        
        sqlite3_bind_int (self.sqlStament, 1, orderId);
        
        status = sqlite3_step(sqlStament);
        while(status == SQLITE_ROW) 
        {
            LaundryOrderDetailModelV2 *model = [[LaundryOrderDetailModelV2 alloc] init];

            model.lod_id = sqlite3_column_int(sqlStament, 0);
            model.lod_laundry_order_id = sqlite3_column_int(sqlStament, 1);
            model.lod_item_id = sqlite3_column_int(sqlStament, 2);
            model.lod_quantity = sqlite3_column_int(sqlStament, 3);
            model.lod_post_status = sqlite3_column_int(sqlStament, 4);
            model.lod_service_id = sqlite3_column_int(sqlStament, 5);
            
            [array addObject:model];
            status = sqlite3_step(sqlStament);
        }
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                [array removeAllObjects];
                goto RE_ACCESS_DB;
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    return array;
}

-(LaundryOrderDetailModelV2 *)loadLaundryOrderDetailsByItemId:(LaundryOrderDetailModelV2 *)laundryItemId AndOrderId:(NSInteger)orderId
{
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@ ,%@, %@, %@, %@, %@ FROM %@ WHERE %@ = ? AND %@ = ? AND %@ = ? AND %@ = ?", 
                           lod_id,
                           lod_laundry_order_id,
                           lod_item_id,
                           lod_quantity,
                           lod_post_status,
                           lod_service_id,
                           LAUNDRY_ORDER_DETAILS,
                           lod_item_id,
                           lod_service_id,
                           lod_category_id,
                           lod_laundry_order_id
                           ];
    
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        
        sqlite3_bind_int (self.sqlStament, 1, laundryItemId.lod_item_id);
        sqlite3_bind_int (self.sqlStament, 2, laundryItemId.lod_service_id);
        sqlite3_bind_int (self.sqlStament, 3, laundryItemId.lod_category_id);
        sqlite3_bind_int (self.sqlStament, 4, orderId);
        
        status = sqlite3_step(sqlStament);
        while(status == SQLITE_ROW) 
        {
            laundryItemId.lod_id = sqlite3_column_int(sqlStament, 0);
            laundryItemId.lod_laundry_order_id = sqlite3_column_int(sqlStament, 1);
            laundryItemId.lod_item_id = sqlite3_column_int(sqlStament, 2);
            laundryItemId.lod_quantity = sqlite3_column_int(sqlStament, 3);
            laundryItemId.lod_post_status = sqlite3_column_int(sqlStament, 4);
            laundryItemId.lod_service_id = sqlite3_column_int(sqlStament, 5);
            
            return laundryItemId;
        }
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    return laundryItemId;
}

-(NSMutableArray *)loadAllLaundryOrderDetailsByServiceId:(NSInteger) serviceId AndOrderId :(NSInteger)orderId
{
    NSMutableArray * array=[NSMutableArray array];
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@ ,%@, %@, %@, %@, %@ FROM %@ WHERE %@ = ? AND %@ = ?", 
                           lod_id,
                           lod_laundry_order_id,
                           lod_item_id,
                           lod_quantity,
                           lod_post_status,
                           lod_service_id,
                           lod_category_id,
                           LAUNDRY_ORDER_DETAILS,
                           lod_laundry_order_id,
                           lod_service_id
                           ];
    
    const char *sql=[sqlString UTF8String];
    
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {
        
        sqlite3_bind_int (self.sqlStament, 1, orderId);
        sqlite3_bind_int (self.sqlStament, 2, serviceId);
        
        status = sqlite3_step(sqlStament);
        while(status == SQLITE_ROW) 
        {
            LaundryOrderDetailModelV2 *model = [[LaundryOrderDetailModelV2 alloc] init];
            
            model.lod_id = sqlite3_column_int(sqlStament, 0);
            model.lod_laundry_order_id = sqlite3_column_int(sqlStament, 1);
            model.lod_item_id = sqlite3_column_int(sqlStament, 2);
            model.lod_quantity = sqlite3_column_int(sqlStament, 3);
            model.lod_post_status = sqlite3_column_int(sqlStament, 4);
            model.lod_service_id = sqlite3_column_int(sqlStament, 5);
            model.lod_category_id = sqlite3_column_int(sqlStament, 6);

            [array addObject:model];
            
            status = sqlite3_step(sqlStament);
        }
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                [array removeAllObjects];
                goto RE_ACCESS_DB;
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    return array;
}

@end
