//
//  LaundryServiceAdapterV2.h
//  mHouseKeeping
//
//  Created by Bien Do on 3/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DatabaseAdapterV2.h"
#import "LaundryServicesModelV2.h"
@interface LaundryServiceAdapterV2 : DatabaseAdapterV2{
    
}
-(int)insertLaundryServicesData:(LaundryServicesModelV2*) LaundryServicesModel;
-(int)updateLaundryServicesData:(LaundryServicesModelV2*) LaundryServicesModel;
-(int)deleteLaundryServicesData:(LaundryServicesModelV2*) LaundryServicesModel;
-(LaundryServicesModelV2 *)loadLaundryServiceModelV2:(LaundryServicesModelV2 *)LaundryServicesModel;
-(NSMutableArray*)loadAllLaundryServicesModelV2;
-(NSString*)getLastModifier;
//-(NSMutableArray*)loadAllLaundryServicesID:(int)ID; 
@end
