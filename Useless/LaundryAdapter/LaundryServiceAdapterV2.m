//
//  LaundryServiceAdapterV2.m
//  mHouseKeeping
//
//  Created by Bien Do on 3/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LaundryServiceAdapterV2.h"
#import "DbDefinesV2.h"
#import "ehkDefines.h"

@implementation LaundryServiceAdapterV2

-(int)insertLaundryServicesData:(LaundryServicesModelV2*) LaundryServicesModel{
    
    NSString *sqlString = [[NSString alloc] initWithFormat:@"INSERT INTO %@ (%@, %@, %@, %@) %@",LAUNDRY_SERVICE,lservice_id,lservice_name,lservice_name_lang,lservice_last_modifed,@"VALUES(?,?,?,?)"];     
    
    const char *sql =[sqlString UTF8String];

    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSInteger status1=sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status1 == SQLITE_OK) {
        
        sqlite3_bind_int(self.sqlStament, 1,LaundryServicesModel.lservices_id);
        sqlite3_bind_text(self.sqlStament, 2, [LaundryServicesModel.lservices_name UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(self.sqlStament, 3, [LaundryServicesModel.lservices_name_lang UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(self.sqlStament, 4, [LaundryServicesModel.lservices_last_modifed UTF8String], -1, SQLITE_TRANSIENT);
    }
    
    NSInteger status = sqlite3_step(self.sqlStament);
    if (status == SQLITE_DONE) {
        sqlite3_last_insert_rowid(self.database);
        sqlite3_reset(self.sqlStament);
        return 1;
    }
    else{
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
        return 0;
    }
    return  0;
}

-(int)updateLaundryServicesData:(LaundryServicesModelV2*) LaundryServicesModel{
    
    NSInteger returnInt = 0;
    if(self.sqlStament == nil) {
        NSString *sqlString = [[NSString alloc] initWithFormat:@"UPDATE %@ SET %@ = ?, %@ = ?,%@= ?,%@=? WHERE %@ = ?",LAUNDRY_SERVICE,lservice_id,lservice_name,lservice_name_lang,lservice_last_modifed,lservice_id];
        const char *sql = [sqlString UTF8String];
      
        NSInteger reAccessCount = 0;
        RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
            reAccessCount ++;
        }
        
        NSInteger prepare = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
		if(prepare != SQLITE_OK)
        {
        }
        
        sqlite3_bind_int(self.sqlStament, 1,LaundryServicesModel.lservices_id);
        sqlite3_bind_text(self.sqlStament, 2, [LaundryServicesModel.lservices_name UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(self.sqlStament, 3, [LaundryServicesModel.lservices_name_lang UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(self.sqlStament, 4, [LaundryServicesModel.lservices_last_modifed UTF8String], -1, SQLITE_TRANSIENT);
        
        NSInteger step = sqlite3_step(self.sqlStament);
        if(SQLITE_DONE != step)
        {
            if (step == SQLITE_BUSY) {
                if (reAccessCount <= REACCESS_MAX_TIMES) {
                    goto RE_ACCESS_DB;
                }
            }
            returnInt = 0;
        }
        else
        { 
            returnInt = 1;
        } 
    }
    return returnInt;
}

-(int)deleteLaundryServicesData:(LaundryServicesModelV2*) LaundryServicesModel{
    
    NSInteger result = 1;
    if(self.sqlStament == nil) {
        NSMutableString *sqlString = [[NSMutableString alloc] initWithFormat:@"DELETE FROM %@ WHERE %@ = ?",LAUNDRY_SERVICE,lservice_id];
		const char *sql = [sqlString UTF8String];
        
        NSInteger reAccessCount = 0;
        RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
            reAccessCount ++;
        }
        
		if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
			result = 0;
        
        sqlite3_bind_int(self.sqlStament, 1,LaundryServicesModel.lservices_id);	
        NSInteger status = sqlite3_step(self.sqlStament);
        if (SQLITE_DONE != status) {
            if (status == SQLITE_BUSY) {
                if (reAccessCount <= REACCESS_MAX_TIMES) {
                    goto RE_ACCESS_DB;
                }
            }
            result = 0;
        }
	}
    return result;
}

-(NSMutableArray*)loadAllLaundryServicesModelV2{
    
    NSMutableArray * array=[NSMutableArray array];
    NSMutableString * sqlString=[[NSMutableString alloc] initWithFormat:@"SELECT %@,%@,%@,%@ FROM %@ ",lservice_id,lservice_name,lservice_name_lang,lservice_last_modifed,LAUNDRY_SERVICE];
    const char *sql=[sqlString UTF8String];
   
    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSInteger status;
    
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {     
        
        status = sqlite3_step(sqlStament);
        while(status == SQLITE_ROW) {
            
            LaundryServicesModelV2 *model =[[LaundryServicesModelV2 alloc] init];
            if(sqlite3_column_int(sqlStament, 0)){
                model.lservices_id= sqlite3_column_int(sqlStament, 0);
            }
            if(sqlite3_column_text(sqlStament, 1)){
                model.lservices_name=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
                
            }
            if(sqlite3_column_text(sqlStament, 2)){
                model.lservices_name_lang=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
            }
            if(sqlite3_column_text(sqlStament, 3)){
                model.lservices_last_modifed=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament,3)];
            }
            
            [array addObject:model];
            status = sqlite3_step(sqlStament);
        }
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                [array removeAllObjects];
                goto RE_ACCESS_DB;
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            [array removeAllObjects];
            goto RE_ACCESS_DB;
        }
    }
    
    return  array;
}

-(LaundryServicesModelV2 *)loadLaundryServiceModelV2:(LaundryServicesModelV2 *)LaundryServicesModel
{
    NSMutableString * sqlString=[[NSMutableString alloc] initWithFormat:@"SELECT %@,%@,%@,%@ FROM %@ WHERE %@= ?",lservice_id,lservice_name,lservice_name_lang,lservice_last_modifed,LAUNDRY_SERVICE,lservice_id];
    const char *sql=[sqlString UTF8String];

    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {     
        
        sqlite3_bind_int(self.sqlStament, 1,LaundryServicesModel.lservices_id);
        status = sqlite3_step(sqlStament);
        while(status == SQLITE_ROW) {
           
            if(sqlite3_column_int(sqlStament, 0)){
                LaundryServicesModel.lservices_id= sqlite3_column_int(sqlStament, 0);
            }
            if(sqlite3_column_text(sqlStament, 1)){
                LaundryServicesModel.lservices_name=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
                
            }
            if(sqlite3_column_text(sqlStament, 2)){
                LaundryServicesModel.lservices_name_lang=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
            }
            if(sqlite3_column_text(sqlStament, 3)){
                LaundryServicesModel.lservices_last_modifed=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament,3)];
            }

            return LaundryServicesModel;
        }
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    return  LaundryServicesModel;
}

-(NSString*)getLastModifier
{
    NSMutableString * sqlString=[[NSMutableString alloc] initWithFormat:@"SELECT MAX(%@) FROM %@ ORDER BY %@ ASC",lservice_last_modifed,LAUNDRY_SERVICE,lservice_last_modifed];
    const char *sql=[sqlString UTF8String];

    NSInteger reAccessCount = 0;
    RE_ACCESS_DB:{
        if (reAccessCount > 0) {
            [self close];
            usleep(0.1);
            [self openDatabase];
        }
        reAccessCount ++;
    }
    
    NSString *lastModified;
    
    NSInteger status;
    status = sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL);
    if(status == SQLITE_OK) {     
    
        status = sqlite3_step(sqlStament);
        while(status == SQLITE_ROW) {
            
            if(sqlite3_column_text(sqlStament, 0)){
                lastModified=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 0)];
            }
            return lastModified;
        }
        if (status == SQLITE_BUSY) {
            if (reAccessCount <= REACCESS_MAX_TIMES) {
                goto RE_ACCESS_DB;
            }
        }
    }
    
    if (status == SQLITE_BUSY) {
        if (reAccessCount <= REACCESS_MAX_TIMES) {
            goto RE_ACCESS_DB;
        }
    }
    
    return  lastModified;
}

@end
