//
//  LaundryCategoryCell.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 1/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LaundryCategoryCell : UITableViewCell{
    UIImageView *imgLaundryCategory;
    UILabel *lblLaundryCategory;
}
@property (retain,nonatomic) IBOutlet UIImageView *imgLaundryCategory;
@property (retain,nonatomic) IBOutlet UILabel *lblLaundryCategory;
@end
