//
//  LaundryCell.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 1/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LaundryCell : UITableViewCell{
    UIImageView *imgLaundryCell;
    UILabel *lblLaundryCell;
}
@property (nonatomic, retain) IBOutlet UIImageView *imgLaundryCell;
@property (nonatomic, retain) IBOutlet UILabel *lblLaundryCell;
@end
