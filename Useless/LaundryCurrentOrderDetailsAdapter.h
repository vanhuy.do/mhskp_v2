//
//  LaundryCurrentOrderDetailsAdapter.h
//
//  Created by Thuong Nguyen.
//  Copyright 2012 __TMS__. All rights reserved.
//

#import "DatabaseAdapter.h"
#import "LaundryCurrentOrderDetailsModel.h"
@interface LaundryCurrentOrderDetailsAdapter : DatabaseAdapterV2

-(BOOL) insertLaundryCurrentOrderDetailsModel:(LaundryCurrentOrderDetailsModel *)model;

-(LaundryCurrentOrderDetailsModel *) loadLaundryCurrentOrderDetailsModelByPrimaryKey:(LaundryCurrentOrderDetailsModel *) model;

-(BOOL) updateLaundryCurrentOrderDetailsModel:(LaundryCurrentOrderDetailsModel *) model;

-(BOOL) deleteLaundryCurrentOrderDetailsModel:(LaundryCurrentOrderDetailsModel *) model;
@end