//
//  LaundryCurrentOrderDetailsAdapter.h
//
//  Created by Thuong Nguyen.
//  Copyright 2012 __TMS__. All rights reserved.
//

#import "LaundryCurrentOrderDetailsAdapter.h"
#import "DbDefinesV2.h"

@implementation LaundryCurrentOrderDetailsAdapter

- (id)init
{
	self = [super init];
	if (self) {
		// Initialization code here.
	}
	return self;

}

-(BOOL)insertLaundryCurrentOrderDetailsModel:(LaundryCurrentOrderDetailsModel *)model {
	NSString *sqlString = [NSString stringWithFormat:@"INSERT INTO LAUNDRY_CURRENT_ORDER_DETAILS ( %@, %@, %@, %@, %@, %@) VALUES ( ?, ?, ?, ?, ?, ?)" , lod_id, lod_laundry_order_id, lod_item_id, lod_quantity, lod_post_status, lod_service_id ];

	const char *sql = [sqlString UTF8String];
	if(sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {
		sqlite3_bind_int(sqlStament, 1, model.lodId);
		sqlite3_bind_int(sqlStament, 2, model.lodLaundryOrderId);
		sqlite3_bind_int(sqlStament, 3, model.lodItemId);
		sqlite3_bind_int(sqlStament, 4, model.lodQuantity);
		sqlite3_bind_int(sqlStament, 5, model.lodPostStatus);
		sqlite3_bind_int(sqlStament, 6, model.lodServiceId);
	}
	NSInteger status = sqlite3_step(sqlStament);
	if (status == SQLITE_DONE) {
		sqlite3_last_insert_rowid(database);
		sqlite3_reset(sqlStament);
		return YES;
	}
	return  NO;
}

-(BOOL)updateLaundryCurrentOrderDetailsModel:(LaundryCurrentOrderDetailsModel *)model {
	NSString *sqlString = [NSString stringWithFormat:@"UPDATE %@ SET %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ? WHERE  %@ = ?", LAUNDRY_CURRENT_ORDER_DETAILS, lod_laundry_order_id, lod_item_id, lod_quantity, lod_post_status, lod_service_id, lod_id];
	const char *sql = [sqlString UTF8String];
	if(sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {
		sqlite3_bind_int(sqlStament, 1, model.lodLaundryOrderId);
		sqlite3_bind_int(sqlStament, 2, model.lodItemId);
		sqlite3_bind_int(sqlStament, 3, model.lodQuantity);
		sqlite3_bind_int(sqlStament, 4, model.lodPostStatus);
		sqlite3_bind_int(sqlStament, 5, model.lodServiceId);
		sqlite3_bind_int(sqlStament, 6, model.lodId);
	}
	NSInteger status = sqlite3_step(sqlStament);
	if (status == SQLITE_DONE) {
		return YES;
	}
	return  NO;
}

-(BOOL)deleteLaundryCurrentOrderDetailsModel:(LaundryCurrentOrderDetailsModel *)model {
	NSString *sqlString = [NSString stringWithFormat:@"DELETE FROM %@ WHERE %@ = ?", LAUNDRY_CURRENT_ORDER_DETAILS, lod_id];
	const char *sql = [sqlString UTF8String];
	if(sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {
		sqlite3_bind_int(sqlStament, 1, model.lodId);
	}
	NSInteger status = sqlite3_step(sqlStament);
	if (status == SQLITE_DONE) {
		return YES;
	}
	return  NO;
}

-(LaundryCurrentOrderDetailsModel *)loadLaundryCurrentOrderDetailsModelByPrimaryKey:(LaundryCurrentOrderDetailsModel *)model {
	NSString *sqlString = [NSString stringWithFormat:@"SELECT %@, %@, %@, %@, %@, %@ FROM %@ WHERE  %@ = ?" , lod_id, lod_laundry_order_id, lod_item_id, lod_quantity, lod_post_status, lod_service_id, LAUNDRY_CURRENT_ORDER_DETAILS, lod_id];
	const char *sql = [sqlString UTF8String];
	
	if(sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {
		sqlite3_bind_int(sqlStament, 1, model.lodId);
		while (sqlite3_step(sqlStament) == SQLITE_ROW) {
			model.lodId = sqlite3_column_int(sqlStament, 0);
			model.lodLaundryOrderId = sqlite3_column_int(sqlStament, 1);
			model.lodItemId = sqlite3_column_int(sqlStament, 2);
			model.lodQuantity = sqlite3_column_int(sqlStament, 3);
			model.lodPostStatus = sqlite3_column_int(sqlStament, 4);
			model.lodServiceId = sqlite3_column_int(sqlStament, 5);
			return model;
		}
	}
	return model;
}

@end