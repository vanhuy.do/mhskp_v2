//
//  LaundryCurrentOrderDetailsModel.h
//
//  Created by ThuongNM.
//  Copyright 2012 __TMS__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LaundryCurrentOrderDetailsModel : NSObject {

	NSInteger lodId;
	NSInteger lodLaundryOrderId;
	NSInteger lodItemId;
	NSInteger lodQuantity;
	NSInteger lodPostStatus;
	NSInteger lodServiceId;

}

@property (nonatomic, assign) NSInteger lodId;
@property (nonatomic, assign) NSInteger lodLaundryOrderId;
@property (nonatomic, assign) NSInteger lodItemId;
@property (nonatomic, assign) NSInteger lodQuantity;
@property (nonatomic, assign) NSInteger lodPostStatus;
@property (nonatomic, assign) NSInteger lodServiceId;

@end