//
//  LaundryCurrentOrderDetailsModel.m
//
//  Created by ThuongNM.
//  Copyright 2012 TMS. All rights reserved.
//

#import "LaundryCurrentOrderDetailsModel.h"

@implementation LaundryCurrentOrderDetailsModel

@synthesize lodId;
@synthesize lodLaundryOrderId;
@synthesize lodItemId;
@synthesize lodQuantity;
@synthesize lodPostStatus;
@synthesize lodServiceId;

@end

