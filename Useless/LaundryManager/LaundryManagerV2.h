//
//  LaundryManagerV2.h
//  mHouseKeeping
//
//  Created by ThuongNM on 3/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LaundryCategoryModelV2.h"
#import "LaundryItemModelV2.h"
#import "LaundryServicesModelV2.h"
#import "LaundryOrderDetailModelV2.h"
#import "LaudryOrderModelV2.h"
#import "LaundryItemPriceModelV2.h"
#import "LaundryIntructionModelV2.h"
#import "LaundryInstructionsOrderModelV2.h"
#import "LaundryItemTempAdapterV2.h"
@interface LaundryManagerV2 : NSObject{
    
}
+ (LaundryManagerV2*) sharedLaundryManagerV2;

////////////////////LaundryService//////////////////////////////////////////////////
-(NSInteger) insertLaundryServicesModelV2:(LaundryServicesModelV2 *)model;
-(NSInteger) updateLaundryServicesModelV2:(LaundryServicesModelV2 *)model;
-(NSInteger) deleteLaundryServicesModelV2:(LaundryServicesModelV2*)model;
-(NSMutableArray*)loadAllLaundryServicesModelV2;
-(LaundryServicesModelV2 *)loadLaundryServiceModelV2:(LaundryServicesModelV2 *)LaundryServicesModel;
-(NSString*)getLastModifierForLaundryService;

//////////////////LaundryCategory//////////////////////////////////////////////////
-(NSInteger) insertLaundryCategoryModelV2:(LaundryCategoryModelV2 *)model;
-(NSInteger) updateLaundryCategoryModelV2:(LaundryCategoryModelV2 *)model;
-(NSInteger) deleteLaundryCategoryModelV2:(LaundryCategoryModelV2*)model;
-(NSMutableArray*)loadAllLaundryCategoryModelV2;
-(LaundryCategoryModelV2*)loadLaundryCategoryModelV2ByLaundryCategory:(LaundryCategoryModelV2 *)model;
-(NSString *)getTheLatestModifiedInLaundryCatergory;

///////////////////LaundryItem/////////////////////////////////////////////////////
-(NSInteger) insertLaundryItemModelV2:(LaundryItemModelV2 *)model;
-(NSInteger) updateLaundryItemModelV2:(LaundryItemModelV2 *)model;
-(NSInteger) deleteLaundryItemModelV2:(LaundryItemModelV2*)model;
-(NSMutableArray*)loadAllLaundryItemModelV2;
-(LaundryItemModelV2*)loadLaundryItemByItemID:(LaundryItemModelV2 *)model;
-(LaundryItemModelV2*)loadLaundryItemByItemId:(NSInteger)itemId andGender:(NSInteger)genderType;
-(NSString *)getTheLatestModifiedInLaundryItem;

///////////////////////LaundryOrderDetail///////////////////////////////////////////
-(NSInteger) insertLaundryOrderDetailModelV2:(LaundryOrderDetailModelV2 *)model;
-(NSInteger) updateLaundryOrderDetailModelV2:(LaundryOrderDetailModelV2 *)model;
-(NSInteger) deleteLaundryOrderDetailModelV2:(LaundryOrderDetailModelV2*)model;
-(NSMutableArray*)loadAllLaundryOrderDetailModelV2;
-(NSMutableArray*)loadAllLaundryOrderDetailModelV2ID:(int)ID;
-(LaundryOrderDetailModelV2 *) loadLaundryOrderDetailsByItemId:(NSInteger) laundryItemId;
-(NSMutableArray *) loadAllLaundryOrderDetailsByOrderId: (NSInteger) orderId;
-(LaundryOrderDetailModelV2 *) loadLaundryOrderDetails:(LaundryOrderDetailModelV2 *) laundryOrderDetail;
-(LaundryOrderDetailModelV2 *) loadLaundryOrderDetailsByItemId:(LaundryOrderDetailModelV2 *) laundryItemId AndOrderId:(NSInteger) orderId;
-(NSMutableArray *)loadAllLaundryOrderDetailsByServiceId:(NSInteger) serviceId AndOrderId :(NSInteger)orderId;

////////////////////////LaundryOrder////////////////////////////////////////////////
-(NSInteger) insertLaudryOrderModelV2:(LaudryOrderModelV2 *)model;
-(NSInteger) updateLaudryOrderModelV2:(LaudryOrderModelV2 *)model;
-(NSInteger) deleteLaudryOrderModelV2:(LaudryOrderModelV2*)model;
-(NSMutableArray*)loadLaudryOrderModelV2;
-(NSMutableArray*)loadAllLaudryOrderModelV2ID:(int)ID;
-(LaudryOrderModelV2 *)loadLaundryOrdersModelByRoomIdUserIdAndCollectDate:(LaudryOrderModelV2 *)model;
-(LaudryOrderModelV2 *)loadLaundryOrderModelByUserID:(int)userID Location:(int)roomID andTime :(NSString *)dateCreate;
-(BOOL)isCheckLaundryOrder:(LaudryOrderModelV2 *)model;
-(NSString *)getTheLatestLastModifier;
-(NSMutableArray *)loadAllLaundryOrderByUserId:(NSInteger)userId;

/////////////////////////LaundryItemPrice///////////////////////////////////////////
-(NSInteger) insertLaundryItemPriceModelV2:(LaundryItemPriceModelV2 *)model;
-(NSInteger) updateLaundryItemPriceModelV2:(LaundryItemPriceModelV2 *)model;
-(NSInteger) deleteLaundryItemPriceModelV2:(LaundryItemPriceModelV2*)model;
-(LaundryItemPriceModelV2 *)loadLaundryItemPriceModelV2ByLaundryItemID:(int)itemID;
-(LaundryItemPriceModelV2 *)loadLaundryItemPriceModelByLIPModel:(LaundryItemPriceModelV2*)model;
-(NSMutableArray*)loadAllLaundryItemPriceModelV2;
-(NSMutableArray*)loadAllLaundryItemPriceModelV2ID:(int)ID;
-(NSString *)getTheLatestModifiedInLaundryItemPrice;
-(LaundryItemPriceModelV2 *) loadLaundryItemPriceModelByLaundryItemId:(NSInteger) itemId andLaundryCategoryId:(NSInteger) categoryId;

////////////////////////LaundryItemTemp///////////////////////////////////////////
-(int)insertLaundryOrderDetailTemp:(LaundryOrderDetailModelV2*) LaundryOrderDetailModel;
-(int)updateLaundryOrderDetailTemp:(LaundryOrderDetailModelV2*) LaundryOrderDetailModel;
-(int)deleteLaundryOrderDetailTemp:(LaundryOrderDetailModelV2*) LaundryOrderDetailModel;
-(NSMutableArray*)loadAllLaundryOrderDetailModelV2Temp;
-(NSMutableArray*)loadAllLaundryOrderDetailIDTemp:(int)ID; 
-(LaundryOrderDetailModelV2 *) loadLaundryOrderDetailsTemp:(LaundryOrderDetailModelV2 *) laundryOrderDetail;
-(NSMutableArray *) loadAllLaundryOrderDetailsTempByServiceId: (NSInteger) serviceId;
-(int)deleteAllLaundryOrderDetailTempData;

////////////////////////////LaundryInstruction///////////////////////////////////
-(NSInteger) insertLaundryInstructionsModelV2:(LaundryIntructionModelV2 *)model;
-(NSInteger) updateLaundryInstructionsModelV2:(LaundryIntructionModelV2 *)model;
-(NSInteger) deleteLaundryInstructionsModelV2:(LaundryIntructionModelV2*)model;
-(NSMutableArray*)loadAllLaundryInstructionsModelV2;

///////////////////////////LaundryInstructionOrder////////////////////////////////
-(NSInteger) insertLaundryInstructionsOrderModelV2:(LaundryInstructionsOrderModelV2 *)model;
-(NSInteger) updateLaundryInstructionsOrderModelV2:(LaundryInstructionsOrderModelV2 *)model;
-(NSInteger) deleteLaundryInstructionsOrderModelV2:(LaundryInstructionsOrderModelV2*)model;
-(NSMutableArray*)loadLaundryInstructionsOrderModelV2;
-(NSMutableArray*)loadAllLaundryInstructionsOrderModelV2ID:(int)ID;

//////////////////////////Laundry WS///////////////////////////////////////////////
-(void)getAllLaundryServiceFromWSByUserID:(NSNumber *)userId atLocation:(NSNumber*)hotelId andDate:(NSString*)lastModified;
-(void)getAllLaundryCategoryFromWSByUserId:(NSNumber *)userID atHotel:(NSNumber *)userHotelID andlastDate: (NSString *)lastModified;
-(void)getAllLaundryItemFromWSByUserId :(NSNumber *)userID andmodifiedDate:(NSString *)modifiedDate;
-(void)getAllLaundryItemPriceFromWSByUserId : (NSNumber *)userID andmodifiedDate:(NSString *)modifiedDate;
-(int)postLaundryOrder:(LaudryOrderModelV2*)lr_Order_Model andsubQuantity:(int)subQuantity andServiceID:(int)serviceID andHotel: (int)HotelID andsubTotal:(CGFloat) subTotal;
-(BOOL)postLaundryItem:(LaundryOrderDetailModelV2*)lr_Order_Detail_Model andLaundryOrder: (int)laundry_Order_Id anduser:(int)userId andTransactionTime:(NSString *)transaction_time;
-(NSInteger) numberOfMustPostLaundryRecords:(NSInteger) userId;

//////////////////////////Laundry Instruction WS////////////////////////////////////////
-(void)getAllLaundryInstructionFromWSByUserId : (NSNumber *)userID andModifiedDate:(NSString *)modifiedDate;
-(int)postLaundryInstructionOrder:(LaundryInstructionsOrderModelV2*)lr_Instruction_Order_Model andLaundryOrder: (int)laundry_Order_Id anduser:(int)userId;
-(NSInteger) numberOfMustPostLaundryInstructionRecords:(NSInteger) userId;

///-------------Post WSLog----
- (BOOL) postWSLog:(NSInteger)userId Message:(NSString *)messageValue MessageType:(NSInteger)type;

@end
