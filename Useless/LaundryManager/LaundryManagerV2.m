//
//  LaundryManagerV2.m
//  mHouseKeeping
//
//  Created by ThuongNM on 3/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LaundryManagerV2.h"
#import "LaundryCategoryAdapterV2.h"
#import "LaundryItemAdapterV2.h"
#import "LaundryOrderAdapterV2.h"
#import "LaundryOrderDetailAdapterV2.h"
#import "LaundryServiceAdapterV2.h"
#import "LaundryItemPricesAdapterV2.h"
#import "LaundryInstructionsAdapterV2.h"
#import "LaundryInstructionsOrderAdapterV2.h"
#import "LogFileManager.h"
#import "LogObject.h"

@implementation LaundryManagerV2
static LaundryManagerV2* sharedLaundryManagerV2Instance = nil;

+ (LaundryManagerV2*) sharedLaundryManagerV2
{
    //	if (sharedLostandFoundManagerV2Instance == nil) {
    //        sharedLostandFoundManagerV2Instance = [[super allocWithZone:NULL] init];
    //    }
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedLaundryManagerV2Instance = [[LaundryManagerV2 alloc] init];
        
    });
    return sharedLaundryManagerV2Instance;
}
#pragma mark- LaundryService
-(NSInteger) insertLaundryServicesModelV2:(LaundryServicesModelV2 *)model{
    
    NSInteger returnCode = 0;
    LaundryServiceAdapterV2 *adapter1 = [[LaundryServiceAdapterV2 alloc] init];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    returnCode = [adapter1 insertLaundryServicesData:model];
    [adapter1 close];
    //[adapter1 release];
    adapter1 = nil;
    return returnCode; 
}
-(NSInteger) updateLaundryServicesModelV2:(LaundryServicesModelV2 *)model{
    
    NSInteger returnCode = 0;
    LaundryServiceAdapterV2 *adapter1 = [[LaundryServiceAdapterV2 alloc] init];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    returnCode = [adapter1 updateLaundryServicesData:model];
    [adapter1 close];
    //[adapter1 release];
    adapter1 = nil;
    return returnCode; 
}
-(NSInteger) deleteLaundryServicesModelV2:(LaundryServicesModelV2*)model{
    
    NSInteger returnCode = 0;
    LaundryServiceAdapterV2 *adapter1 = [[LaundryServiceAdapterV2 alloc] init];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    returnCode = [adapter1 deleteLaundryServicesData:model];
    [adapter1 close];
    //[adapter1 release];
    adapter1 = nil;
    return returnCode; 
}
-(NSMutableArray*)loadAllLaundryServicesModelV2{
    LaundryServiceAdapterV2 *adapter1 = [[LaundryServiceAdapterV2 alloc] init];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    NSMutableArray *array = [adapter1 loadAllLaundryServicesModelV2 ];
    [adapter1 close];
    //[adapter1 release];
    adapter1 = nil;
    return array; 
    
}
-(LaundryServicesModelV2 *)loadLaundryServiceModelV2:(LaundryServicesModelV2 *)LaundryServicesModel
{
    LaundryServiceAdapterV2 *adapter1 = [[LaundryServiceAdapterV2 alloc] init];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    LaundryServicesModelV2 *laundryServices_Object = [adapter1 loadLaundryServiceModelV2:LaundryServicesModel ];
    [adapter1 close];
    return laundryServices_Object; 
    
}
-(NSString*)getLastModifierForLaundryService
{
    LaundryServiceAdapterV2 *adapter1 = [[LaundryServiceAdapterV2 alloc] init];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    NSString *latestModified = [adapter1 getLastModifier ];
    [adapter1 close];  
    return latestModified; 
}

#pragma mark- LaundryCategory
-(NSInteger) insertLaundryCategoryModelV2:(LaundryCategoryModelV2 *)model{
    
    NSInteger returnCode = 0;
    LaundryCategoryAdapterV2 *adapter1 = [[LaundryCategoryAdapterV2 alloc] init];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    returnCode = [adapter1 insertLaundryCategoryData:model];
    [adapter1 close];
    //[adapter1 release];
    adapter1 = nil;
    return returnCode; 
}
-(NSInteger) updateLaundryCategoryModelV2:(LaundryCategoryModelV2 *)model{
    
    NSInteger returnCode = 0;
    LaundryCategoryAdapterV2 *adapter1 = [[LaundryCategoryAdapterV2 alloc] init];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    returnCode = [adapter1 updateLaundryCategoryData:model];
    [adapter1 close];
    //[adapter1 release];
    adapter1 = nil;
    return returnCode; 
    
}
-(NSInteger) deleteLaundryCategoryModelV2:(LaundryCategoryModelV2*)model{
    NSInteger returnCode = 0;
    LaundryCategoryAdapterV2 *adapter1 = [[LaundryCategoryAdapterV2 alloc] init];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    returnCode = [adapter1 deleteLaundryCategoryData:model];
    [adapter1 close];
   // [adapter1 release];
    adapter1 = nil;
    return returnCode; 
    
}
-(NSMutableArray*)loadAllLaundryCategoryModelV2{
    LaundryCategoryAdapterV2 *adapter1 = [[LaundryCategoryAdapterV2 alloc] init];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    NSMutableArray *array = [adapter1 loadAllLaundryCategoryModelV2];
    [adapter1 close];
    //[adapter1 release];
    adapter1 = nil;
    return array; 
    
}
-(NSString *)getTheLatestModifiedInLaundryCatergory
{
    LaundryCategoryAdapterV2 *adapter1 = [[LaundryCategoryAdapterV2 alloc] init];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    NSString *lastModifier=[adapter1 getTheLatestLastModifierInLaundryCatergory];
    [adapter1 close];
    return lastModifier;
}
-(LaundryCategoryModelV2*)loadLaundryCategoryModelV2ByLaundryCategory:(LaundryCategoryModelV2 *)model{
    LaundryCategoryAdapterV2 *adapter1 = [[LaundryCategoryAdapterV2 alloc] init];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    model = [adapter1 loadLaundryCategoryID:model];
    [adapter1 close];
    //[adapter1 release];
    adapter1 = nil;
    return model; 
}

#pragma mark- LaundryItem
-(NSInteger) insertLaundryItemModelV2:(LaundryItemModelV2 *)model{
    
    NSInteger returnCode = 0;
    LaundryItemAdapterV2 *adapter1 = [[LaundryItemAdapterV2 alloc] init];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    returnCode = [adapter1 insertLaundryItemData:model];
    [adapter1 close];
    //[adapter1 release];
    adapter1 = nil;
    return returnCode; 
}
-(NSInteger) updateLaundryItemModelV2:(LaundryItemModelV2 *)model{
    NSInteger returnCode = 0;
    LaundryItemAdapterV2 *adapter1 = [[LaundryItemAdapterV2 alloc] init];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    returnCode = [adapter1 updateLaundryItemData:model];
    [adapter1 close];
    //[adapter1 release];
    adapter1 = nil;
    return returnCode; 
}
-(NSInteger) deleteLaundryItemModelV2:(LaundryItemModelV2*)model{
    NSInteger returnCode = 0;
    LaundryItemAdapterV2 *adapter1 = [[LaundryItemAdapterV2 alloc] init];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    returnCode = [adapter1 deleteLaundryItemData:model];
    [adapter1 close];
    //[adapter1 release];
    adapter1 = nil;
    return returnCode; 
}

-(NSMutableArray*)loadAllLaundryItemModelV2{
    LaundryItemAdapterV2 *adapter1 = [[LaundryItemAdapterV2 alloc] init];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    NSMutableArray *array = [adapter1 loadAllLaundryItemModelV2];
    [adapter1 close];
    //[adapter1 release];
    adapter1 = nil;
    return array; 
}
-(LaundryItemModelV2*)loadLaundryItemByItemID:(LaundryItemModelV2 *)model{
    
    LaundryItemAdapterV2 *adapter1 = [[LaundryItemAdapterV2 alloc] init];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    model = [adapter1 loadLaundryItemByLRItem:model];
    [adapter1 close];
    //[adapter1 release];
    adapter1 = nil;
    return model; 
}
-(NSString *)getTheLatestModifiedInLaundryItem
{
    LaundryItemAdapterV2 *adapter1 = [[LaundryItemAdapterV2 alloc] init];    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    NSString *lastModifier=[adapter1 getTheLatestLastModifierInLaundryItem];
    [adapter1 close];
    return lastModifier;
}

-(LaundryItemModelV2*)loadLaundryItemByItemId:(NSInteger)itemId andGender:(NSInteger)genderType{
    LaundryItemAdapterV2 *adapter1 = [[LaundryItemAdapterV2 alloc] init];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    LaundryItemModelV2 *model = [adapter1 loadLaundryItemByUserID:itemId tgender:genderType];
    [adapter1 close];
    //[adapter1 release];
    return model; 
}

#pragma mark- LaundryOrderDetail
-(NSInteger) insertLaundryOrderDetailModelV2:(LaundryOrderDetailModelV2 *)model{
    
    NSInteger returnCode = 0;
    LaundryOrderDetailAdapterV2 *adapter1 = [[LaundryOrderDetailAdapterV2 alloc] init];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    returnCode = [adapter1 insertLaundryOrderDetailData:model];
    [adapter1 close];
    //[adapter1 release];
    adapter1 = nil;
    return returnCode; 
}
-(NSInteger) updateLaundryOrderDetailModelV2:(LaundryOrderDetailModelV2 *)model{
    
    NSInteger returnCode = 0;
    LaundryOrderDetailAdapterV2 *adapter1 = [[LaundryOrderDetailAdapterV2 alloc] init];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    returnCode = [adapter1 updateLaundryOrderDetailData:model];
    [adapter1 close];
    //[adapter1 release];
    adapter1 = nil;
    return returnCode;
}
-(NSInteger) deleteLaundryOrderDetailModelV2:(LaundryOrderDetailModelV2*)model{
    
    NSInteger returnCode = 0;
    LaundryOrderDetailAdapterV2 *adapter1 = [[LaundryOrderDetailAdapterV2 alloc] init];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    returnCode = [adapter1 deleteLaundryOrderDetailData:model];
    [adapter1 close];
    //[adapter1 release];
    adapter1 = nil;
    return returnCode;
}
-(NSMutableArray*)loadAllLaundryOrderDetailModelV2{
    LaundryOrderDetailAdapterV2 *adapter1 = [[LaundryOrderDetailAdapterV2 alloc] init];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    NSMutableArray *array = [adapter1 loadAllLaundryOrderDetailModelV2];
    [adapter1 close];
    //[adapter1 release];
    adapter1 = nil;
    return array; 
    
}
-(NSMutableArray*)loadAllLaundryOrderDetailModelV2ID:(int)ID{
    LaundryOrderDetailAdapterV2 *adapter1 = [[LaundryOrderDetailAdapterV2 alloc] init];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    NSMutableArray *array = [adapter1 loadAllLaundryOrderDetailID:ID];
    [adapter1 close];
    //[adapter1 release];
    adapter1 = nil;
    return array; 
}
-(LaundryOrderDetailModelV2 *) loadLaundryOrderDetailsByItemId:(NSInteger) laundryItemId{
    
    LaundryOrderDetailAdapterV2 *adapter = [[LaundryOrderDetailAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    LaundryOrderDetailModelV2 *model = [adapter loadLaundryOrderDetailsByItemId:laundryItemId];
    [adapter close];
    return model;
}
-(NSMutableArray *) loadAllLaundryOrderDetailsByOrderId: (NSInteger) orderId{
    LaundryOrderDetailAdapterV2 *adapter = [[LaundryOrderDetailAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    NSMutableArray *array = [adapter loadAllLaundryOrderDetailsByOrderId:orderId];
    [adapter close];
    return array;
}

-(LaundryOrderDetailModelV2 *)loadLaundryOrderDetails:(LaundryOrderDetailModelV2 *)laundryOrderDetail
{
    LaundryOrderDetailAdapterV2 *adapter = [[LaundryOrderDetailAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    [adapter loadLaundryOrderDetails:laundryOrderDetail];
    [adapter close];
    return laundryOrderDetail;
}

-(LaundryOrderDetailModelV2 *)loadLaundryOrderDetailsByItemId:(LaundryOrderDetailModelV2 *)laundryItemId AndOrderId:(NSInteger)orderId
{
    LaundryOrderDetailAdapterV2 *adapter = [[LaundryOrderDetailAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    LaundryOrderDetailModelV2 *model = [adapter loadLaundryOrderDetailsByItemId:laundryItemId AndOrderId:orderId];
    [adapter close];
    return model;
}

-(NSMutableArray *)loadAllLaundryOrderDetailsByServiceId:(NSInteger)serviceId AndOrderId:(NSInteger)orderId
{
    LaundryOrderDetailAdapterV2 *adapter = [[LaundryOrderDetailAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    NSMutableArray *array = [adapter loadAllLaundryOrderDetailsByServiceId:serviceId AndOrderId:orderId];
    [adapter close];
    return array;
}
#pragma mark- LaundryOrder
-(NSInteger) insertLaudryOrderModelV2:(LaudryOrderModelV2 *)model{
    
    NSInteger returnCode = 0;
    LaundryOrderAdapterV2 *adapter1 = [[LaundryOrderAdapterV2 alloc] init];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    returnCode = [adapter1 insertLaudryOrderData:model];
    [adapter1 close];
    //[adapter1 release];
    adapter1 = nil;
    return returnCode;

}
-(NSInteger) updateLaudryOrderModelV2:(LaudryOrderModelV2 *)model{
    
    NSInteger returnCode = 0;
    LaundryOrderAdapterV2 *adapter1 = [[LaundryOrderAdapterV2 alloc] init];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    returnCode = [adapter1 updateLaudryOrderData:model];
    [adapter1 close];
    //[adapter1 release];
    adapter1 = nil;
    return returnCode;
}
-(NSInteger) deleteLaudryOrderModelV2:(LaudryOrderModelV2*)model{
 
    NSInteger returnCode = 0;
    LaundryOrderAdapterV2 *adapter1 = [[LaundryOrderAdapterV2 alloc] init];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    returnCode = [adapter1 deleteLaudryOrderData:model];
    [adapter1 close];
    //[adapter1 release];
    adapter1 = nil;
    return returnCode;
}
-(NSMutableArray*)loadLaudryOrderModelV2{
    LaundryOrderAdapterV2 *adapter1 = [[LaundryOrderAdapterV2 alloc] init];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    NSMutableArray *array = [adapter1 loadAllLaudryOrderV2];
    [adapter1 close];
    //[adapter1 release];
    adapter1 = nil;
    return array; 
}
-(NSMutableArray*)loadAllLaudryOrderModelV2ID:(int)ID{
    LaundryOrderAdapterV2 *adapter1 = [[LaundryOrderAdapterV2 alloc] init];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    NSMutableArray *array = [adapter1 loadAllLaudryOrderID:ID];
    [adapter1 close];
    //[adapter1 release];
    adapter1 = nil;
    return array; 
}
-(LaudryOrderModelV2 *)loadLaundryOrderModelByUserID:(int)userID Location:(int)roomID andTime :(NSString *)dateCreate

{
    LaundryOrderAdapterV2 *adapter = [[LaundryOrderAdapterV2 alloc] init];
    
    [adapter openDatabase];
    LaudryOrderModelV2* model=[adapter loadLaundryOrderModelByUserID:userID Location:roomID andTime:dateCreate];
    [adapter close];
    return model;

}

-(LaudryOrderModelV2 *)loadLaundryOrdersModelByRoomIdUserIdAndCollectDate:(LaudryOrderModelV2 *)model
{
    LaundryOrderAdapterV2 *adapter = [[LaundryOrderAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter loadLaundryOrderModelByRoomIdUserIdAndCollectDate:model];
    [adapter close];
    return model;
}

-(BOOL)isCheckLaundryOrder:(LaudryOrderModelV2 *)model
{
    LaundryOrderAdapterV2 *adapter = [[LaundryOrderAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter loadLaundryOrderModelByRoomIdUserIdAndCollectDate:model];
    [adapter close];
    if (model.laundryOrderId != 0) {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

-(NSMutableArray *)loadAllLaundryOrderByUserId:(NSInteger)userId{
    LaundryOrderAdapterV2 *adapter1 = [[LaundryOrderAdapterV2 alloc] init];
    [adapter1 openDatabase];
    NSMutableArray *array = [adapter1 loadAllLaundryOrderByUserId:userId];
    [adapter1 close];
    return array; 
}

#pragma mark- LaundryItemPrice
-(NSInteger) insertLaundryItemPriceModelV2:(LaundryItemPriceModelV2 *)model{
    
    NSInteger returnCode = 0;
    LaundryItemPricesAdapterV2 *adapter1 = [[LaundryItemPricesAdapterV2 alloc] init];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    returnCode = [adapter1 insertLaundryItemPriceData:model];
    [adapter1 close];
    //[adapter1 release];
    adapter1 = nil;
    return returnCode; 
}
-(NSInteger) updateLaundryItemPriceModelV2:(LaundryItemPriceModelV2 *)model{
    
    NSInteger returnCode = 0;
    LaundryItemPricesAdapterV2 *adapter1 = [[LaundryItemPricesAdapterV2 alloc] init];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    returnCode = [adapter1 updateLaundryItemPriceData:model];
    [adapter1 close];
    //[adapter1 release];
    adapter1 = nil;
    return returnCode; 
}
-(NSInteger) deleteLaundryItemPriceModelV2:(LaundryItemPriceModelV2*)model{
    
    
    NSInteger returnCode = 0;
    LaundryItemPricesAdapterV2 *adapter1 = [[LaundryItemPricesAdapterV2 alloc] init];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    returnCode = [adapter1 deleteLaundryItemPriceData:model];
    [adapter1 close];
    //[adapter1 release];
    adapter1 = nil;
    return returnCode; 
}
-(LaundryItemPriceModelV2 *)loadLaundryItemPriceModelV2ByLaundryItemID:(int)itemID
{
    LaundryItemPricesAdapterV2 *adapter1 = [[LaundryItemPricesAdapterV2 alloc] init];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    LaundryItemPriceModelV2* object = [adapter1 loadLaundryItemPriceModelV2ByLaundryItemID:itemID];
    [adapter1 close];
    //[adapter1 release];
    adapter1 = nil;
    return object;

}
-(LaundryItemPriceModelV2 *)loadLaundryItemPriceModelByLIPModel:(LaundryItemPriceModelV2*)model
{

    LaundryItemPricesAdapterV2 *adapter1 = [[LaundryItemPricesAdapterV2 alloc] init];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    LaundryItemPriceModelV2* object = [adapter1 loadLaundryItemPriceModelV2ByLaundryIPModel:model];
    [adapter1 close];
    //[adapter1 release];
    adapter1 = nil;
    return object;

}
-(NSMutableArray*)loadAllLaundryItemPriceModelV2{
    LaundryItemPricesAdapterV2 *adapter1 = [[LaundryItemPricesAdapterV2 alloc] init];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    NSMutableArray *array = [adapter1 loadAllLaundryItemPriceModelV2];
    [adapter1 close];
    //[adapter1 release];
    adapter1 = nil;
    return array; 
}
-(NSMutableArray*)loadAllLaundryItemPriceModelV2ID:(int)ID{
    LaundryItemPricesAdapterV2 *adapter1 = [[LaundryItemPricesAdapterV2 alloc] init];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    NSMutableArray *array = [adapter1 loadAllLaundryItemPriceByCategoryID:ID];
    [adapter1 close];
    //[adapter1 release];
    adapter1 = nil;
    return array; 
}
-(NSString *)getTheLatestModifiedInLaundryItemPrice
{
    LaundryItemPricesAdapterV2 *adapter1 = [[LaundryItemPricesAdapterV2 alloc] init];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    NSString *lastModifier=[adapter1 getTheLatestLastModifierInLaundryItemPrice];
    [adapter1 close];
    return lastModifier;
}

-(LaundryItemPriceModelV2 *)loadLaundryItemPriceModelByLaundryItemId:(NSInteger)itemId andLaundryCategoryId:(NSInteger)categoryId {
    LaundryItemPricesAdapterV2 *adapter = [[LaundryItemPricesAdapterV2 alloc] init];
    [adapter openDatabase];
    LaundryItemPriceModelV2 *model = [adapter loadLaundryItemPriceModelV2ByLaundryItemId:itemId andLaundryCategoryId:categoryId];
    [adapter close];
    return model;
}

#pragma mark - Laundry Item Temp
-(int)insertLaundryOrderDetailTemp:(LaundryOrderDetailModelV2*) LaundryOrderDetailModel{
    LaundryItemTempAdapterV2 *adapter = [[LaundryItemTempAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    int result = [adapter insertLaundryOrderDetailTempData:LaundryOrderDetailModel];
    [adapter close];
    return result;
}
-(int)updateLaundryOrderDetailTemp:(LaundryOrderDetailModelV2*) LaundryOrderDetailModel{
    LaundryItemTempAdapterV2 *adapter = [[LaundryItemTempAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    int result = [adapter updateLaundryOrderDetailTempData:LaundryOrderDetailModel];
    [adapter close];
    return result;
}
-(int)deleteLaundryOrderDetailTemp:(LaundryOrderDetailModelV2*) LaundryOrderDetailModel{
    LaundryItemTempAdapterV2 *adapter = [[LaundryItemTempAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    int result = [adapter deleteLaundryOrderDetailTempData:LaundryOrderDetailModel];
    [adapter close];
    return result;
}
-(NSMutableArray*)loadAllLaundryOrderDetailModelV2Temp{
    LaundryItemTempAdapterV2 *adapter = [[LaundryItemTempAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    NSMutableArray *result = [adapter loadAllLaundryOrderDetailModelV2Temp];
    [adapter close];
    return result;

}
-(NSMutableArray*)loadAllLaundryOrderDetailIDTemp:(int)ID{
    LaundryItemTempAdapterV2 *adapter = [[LaundryItemTempAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    NSMutableArray *result = [adapter loadAllLaundryOrderDetailIDTemp:ID];
    [adapter close];
    return result;
}


-(LaundryOrderDetailModelV2 *) loadLaundryOrderDetailsTemp:(LaundryOrderDetailModelV2 *) laundryOrderDetail{
    LaundryItemTempAdapterV2 *adapter = [[LaundryItemTempAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter loadLaundryOrderDetailsTemp:laundryOrderDetail];
    [adapter close];
    return laundryOrderDetail;
}
-(NSMutableArray *)loadAllLaundryOrderDetailsTempByServiceId:(NSInteger)serviceId{
    LaundryItemTempAdapterV2 *adapter = [[LaundryItemTempAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    NSMutableArray *array = [adapter loadAllLaundryOrderDetailsTempByServiceId:serviceId];
    [adapter close];
    return array;

}

-(int)deleteAllLaundryOrderDetailTempData{
    LaundryItemTempAdapterV2 *adapter = [[LaundryItemTempAdapterV2 alloc] init];
    [adapter openDatabase];
    [adapter resetSqlCommand];
    int result = [adapter deleteAllLaundryOrderDetailTempData];
    [adapter close];
    return result;
}

#pragma mark- LaundryInstructions
-(NSInteger) insertLaundryInstructionsModelV2:(LaundryIntructionModelV2 *)model{
    
    NSInteger returnCode = 0;
    LaundryInstructionsAdapterV2 *adapter1 = [[LaundryInstructionsAdapterV2 alloc] init];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    returnCode =[adapter1 insertLaundryIntructionData:model];
    [adapter1 close];
    //[adapter1 release];
    adapter1 = nil;
    return returnCode;
}
-(NSInteger) updateLaundryInstructionsModelV2:(LaundryIntructionModelV2 *)model{
    
    NSInteger returnCode = 0;
    LaundryInstructionsAdapterV2 *adapter1 = [[LaundryInstructionsAdapterV2 alloc] init];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    returnCode =[adapter1 updateLaundryIntructionData:model];
    [adapter1 close];
    //[adapter1 release];
    adapter1 = nil;
    return returnCode;
}
-(NSInteger) deleteLaundryInstructionsModelV2:(LaundryIntructionModelV2*)model{
    
    NSInteger returnCode = 0;
    LaundryInstructionsAdapterV2 *adapter1 = [[LaundryInstructionsAdapterV2 alloc] init];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    returnCode =[adapter1 deleteLaundryIntructionData:model];
    [adapter1 close];
    //[adapter1 release];
    adapter1 = nil;
    return returnCode;
}
-(NSMutableArray*)loadAllLaundryInstructionsModelV2{
    
    NSMutableArray * array = nil;
    LaundryInstructionsAdapterV2 *adapter1 = [[LaundryInstructionsAdapterV2 alloc] init];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    array = [adapter1 loadAllLaundryIntructionModelV2];
    [adapter1 close];
    adapter1 = nil;
    return array; 
}
-(LaundryIntructionModelV2 *)loadLaundryInstructionsModelV2:(LaundryIntructionModelV2 *)laundryInstructionModel

{
    LaundryInstructionsAdapterV2 *adapter1 = [[LaundryInstructionsAdapterV2 alloc] init];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    [adapter1 loadLaundryIntructionModelV2:laundryInstructionModel];
    [adapter1 close];
    return laundryInstructionModel;
}

-(NSString *)getTheLatestLastModifier
{
    LaundryInstructionsAdapterV2 *adapter1 = [[LaundryInstructionsAdapterV2 alloc] init];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    NSString *lastModifier=[adapter1 getTheLatestLastModifier];
    [adapter1 close];
    return lastModifier;
}
#pragma mark- LaundryInstructionsOrder
-(NSInteger) insertLaundryInstructionsOrderModelV2:(LaundryInstructionsOrderModelV2 *)model{
    
    NSInteger returnCode = 0;
    LaundryInstructionsOrderAdapterV2 *adapter1 = [[LaundryInstructionsOrderAdapterV2 alloc] init];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    returnCode =[adapter1 insertLaundryInstructionsOrderData:model];
    [adapter1 close];
    //[adapter1 release];
    adapter1 = nil;
    return returnCode;

}
-(NSInteger) updateLaundryInstructionsOrderModelV2:(LaundryInstructionsOrderModelV2 *)model{
    
    NSInteger returnCode = 0;
    LaundryInstructionsOrderAdapterV2 *adapter1 = [[LaundryInstructionsOrderAdapterV2 alloc] init];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    returnCode =[adapter1 updateLaundryInstructionsOrderData:model];
    [adapter1 close];
    //[adapter1 release];
    adapter1 = nil;
    return returnCode;

}
-(NSInteger) deleteLaundryInstructionsOrderModelV2:(LaundryInstructionsOrderModelV2*)model{
    NSInteger returnCode = 0;
    LaundryInstructionsOrderAdapterV2 *adapter1 = [[LaundryInstructionsOrderAdapterV2 alloc] init];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    returnCode =[adapter1 deleteLaundryInstructionsOrderData:model];
    [adapter1 close];
    //[adapter1 release];
    adapter1 = nil;
    return returnCode;

}
-(NSMutableArray*)loadLaundryInstructionsOrderModelV2{
    LaundryInstructionsOrderAdapterV2 *adapter1 = [[LaundryInstructionsOrderAdapterV2 alloc] init];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    NSMutableArray *array = [adapter1 loadAllLaundryIntructionOrderModelV2];
    [adapter1 close];
    //[adapter1 release];
    adapter1 = nil;
    return array; 

}
-(NSMutableArray*)loadAllLaundryInstructionsOrderModelV2ID:(int)ID{
    LaundryInstructionsOrderAdapterV2 *adapter1 = [[LaundryInstructionsOrderAdapterV2 alloc] init];
    [adapter1 openDatabase];
    [adapter1 resetSqlCommand];
    NSMutableArray *array = [adapter1 loadAllLaundryIntructionOrderID:ID];
    [adapter1 close];
    //[adapter1 release];
    adapter1 = nil;
    return array;
}

#pragma mark - Laundry WS
#pragma mark -==get Laundry Service===
-(void)getAllLaundryServiceFromWSByUserID:(NSNumber *)userId atLocation:(NSNumber*)hotelId andDate:(NSString*)lastModified
{
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    if([LogFileManager isLogConsole]) {
        [binding setLogXMLInOut:YES];
    }
    
    eHousekeepingService_GetLaundryServiceList *request = [[eHousekeepingService_GetLaundryServiceList alloc] init];
    
    if (userId) {
        [request setIntUsrID:userId];
    }
    if (hotelId) {
        [request setIntHotelID:hotelId];
    }
    if (lastModified) {
        [request setStrLastModified:lastModified];
    }
    
//    LogObject *logObj = [[LogObject alloc] init];
//    logObj.dateTime = [NSDate date];
//    logObj.log = [NSString stringWithFormat:@"<%i><getAllLaundryServiceFromWSByUserID><intUsrID:%i intHotelID:%i lastModifier:%@>",userId.intValue,userId.intValue,hotelId.intValue,lastModified];
//    LogFileManager *logManager = [[LogFileManager alloc] init];
//    [logManager save:logObj];
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetLaundryServiceListUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    for (id bodyPart in responseBodyParts ) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetLaundryServiceListResponse class]]) {
            
            eHousekeepingService_GetLaundryServiceListResponse *dataBody = bodyPart;
            
            if (dataBody != nil) {
                if ([dataBody.GetLaundryServiceListResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]]) {
                    
                    NSMutableArray *arrayLRService = dataBody.GetLaundryServiceListResult.LaundrySrvList.LaundryList;
                    for (eHousekeepingService_LaundryList *lrServiceObject in arrayLRService) {
                        LaundryServicesModelV2 *modelObject = [[LaundryServicesModelV2 alloc] init];
                        modelObject.lservices_id = [lrServiceObject.ldryLstID integerValue];
                        modelObject.lservices_name = lrServiceObject.ldryLstName;
                        modelObject.lservices_name_lang = lrServiceObject.ldryLstLang;
                        modelObject.lservices_last_modifed = lrServiceObject.ldryLstLastModified;
                        LaundryServicesModelV2 *compareOject = [[LaundryServicesModelV2 alloc] init];
                        compareOject.lservices_id = modelObject.lservices_id;
                        int result = [self insertLaundryServicesModelV2:modelObject];
                        if (result == 0) {
                            [self loadLaundryServiceModelV2:compareOject];
                            if ([compareOject.lservices_last_modifed compare:modelObject.lservices_last_modifed] == NSOrderedAscending) {
                                
                                [self updateLaundryServicesModelV2:modelObject];
                            }
                            
                        }
                        
                    }
                    
                    
                }
            }
            
        }
    }
    
}

#pragma mark -==get Laundry Category===
-(void)getAllLaundryCategoryFromWSByUserId:(NSNumber *)userID atHotel:(NSNumber *)userHotelID andlastDate: (NSString *)lastModified
{
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    eHousekeepingService_GetLaundryCategoryList *request = [[eHousekeepingService_GetLaundryCategoryList alloc] init];
    
    if (userID) {
        [request setIntUsrID:userID];
    }
    if (userHotelID) {
        [request setIntHotelID:userHotelID];
    }
    if (lastModified) {
        [request setStrLastModified:lastModified];
    }
    
//    LogObject *logObj = [[LogObject alloc] init];
//    logObj.dateTime = [NSDate date];
//    logObj.log = [NSString stringWithFormat:@"<%i><getAllLaundryCategoryFromWSByUserId><intUsrID:%i intHotelID:%i lastModifier:%@>",userID.intValue,userID.intValue,userHotelID.intValue,lastModified];
//    LogFileManager *logManager = [[LogFileManager alloc] init];
//    [logManager save:logObj];
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetLaundryCategoryListUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    for (id bodyPart in responseBodyParts ) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetLaundryCategoryListResponse class]]) {
            eHousekeepingService_GetLaundryCategoryListResponse *dataBody = bodyPart;
            if (dataBody != nil) {
                if ([dataBody.GetLaundryCategoryListResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]]) {
                    
                    NSMutableArray *arrayLRCategory=dataBody.GetLaundryCategoryListResult.LaundryTypList.LaundryTypeList;
                    for (eHousekeepingService_LaundryTypeList *lrCategoryObject in arrayLRCategory) {
                        LaundryCategoryModelV2 *modelObject = [[LaundryCategoryModelV2 alloc] init];
                        modelObject.laundryCategoryId = [lrCategoryObject.ldryCatID integerValue];
                        modelObject.laundryCategoryName = lrCategoryObject.ldryCatName;
                        modelObject.laundryCategoryNameLang = lrCategoryObject.ldryCatLang;
                        if (lrCategoryObject.ldryCatPicture == nil) {
                            modelObject.laundryCategoryImg  = UIImagePNGRepresentation([UIImage imageNamed:IMAGE_NO_IMAGE_AVAILABLE]);
                        } else
                            modelObject.laundryCategoryImg = lrCategoryObject.ldryCatPicture;
                        modelObject.laundryCategoryLastModified = lrCategoryObject.ldryTypLastModified;
                        LaundryCategoryModelV2 *compareOject = [[LaundryCategoryModelV2 alloc] init];
                        compareOject.laundryCategoryId = modelObject.laundryCategoryId;
                        int result = [self insertLaundryCategoryModelV2:modelObject];
                        if (result == 0) {
                            [self loadLaundryCategoryModelV2ByLaundryCategory:compareOject];
                            if ([compareOject.laundryCategoryLastModified compare:modelObject.laundryCategoryLastModified] == NSOrderedAscending) {
                                
                                [self updateLaundryCategoryModelV2:modelObject];
                            }
                            
                        }
                        
                    }
                    
                    
                }
            }
        }
    }
    
}

#pragma mark -==get Laundry Item===
-(void)getAllLaundryItemFromWSByUserId :(NSNumber *)userID andmodifiedDate:(NSString *)modifiedDate
{
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
//    [binding setDefaultTimeout:60];
    
    eHousekeepingService_GetLaundryItemList *request = [[eHousekeepingService_GetLaundryItemList alloc] init];
    
    if (userID) {
        [request setIntUsrID:userID];
    }
    
    if (modifiedDate) {
        [request setStrLastModified:modifiedDate];
    }
//    [binding setDefaultTimeout:60];
    
//    LogObject *logObj = [[LogObject alloc] init];
//    logObj.dateTime = [NSDate date];
//    logObj.log = [NSString stringWithFormat:@"<%i><getAllLaundryItemFromWSByUserId><intUsrID:%i lastModifier:%@>",userID.intValue,userID.intValue,modifiedDate];
//    LogFileManager *logManager = [[LogFileManager alloc] init];
//    [logManager save:logObj];
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetLaundryItemListUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    for (id bodyPart in responseBodyParts ) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetLaundryItemListResponse class]]) {
            eHousekeepingService_GetLaundryItemListResponse *dataBody = bodyPart;
            if (dataBody != nil) {
                if ([dataBody.GetLaundryItemListResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]]) {
                    
                    NSMutableArray *arrayLRItem = dataBody.GetLaundryItemListResult.LaundryItmList.LaundryItemList;
                    for (eHousekeepingService_LaundryItemList *lrItemObject in arrayLRItem) {
                        LaundryItemModelV2 *modelObject = [[LaundryItemModelV2 alloc] init];
                        modelObject.laundryItemId = [lrItemObject.ldryItmID integerValue];
                        modelObject.laundryItemName = lrItemObject.ldryItmName;
                        modelObject.laundryItemLang = lrItemObject.ldryItmLang;
                        if (lrItemObject.ldryPicture == nil) {
                            modelObject.laundryItemImg = UIImagePNGRepresentation([UIImage imageNamed:IMAGE_NO_IMAGE_AVAILABLE]);
                        } else
                            modelObject.laundryItemImg = lrItemObject.ldryPicture;
                        modelObject.laundryItem_male_or_female = [lrItemObject.ldryItmGender integerValue];
                        modelObject.laundry_last_modified = lrItemObject.ldryItmLastModified;
                        LaundryItemModelV2 *compareOject = [[LaundryItemModelV2 alloc] init];
                        compareOject.laundryItemId = modelObject.laundryItemId;
                        int result = [self insertLaundryItemModelV2:modelObject];
                        if (result == 0) {
                            [self loadLaundryItemByItemID:compareOject];
                            if ([compareOject.laundry_last_modified compare:modelObject.laundry_last_modified] == NSOrderedAscending) {
                                
                                [self updateLaundryItemModelV2:modelObject];
                            }
                            
                        }
                        
                    }
                    
                }
            }
            
        }
    }
    
    
}

#pragma mark -==get Laundry Item Price===
-(void)getAllLaundryItemPriceFromWSByUserId : (NSNumber *)userID andmodifiedDate:(NSString *)modifiedDate
{
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    eHousekeepingService_GetLaundryItemPriceList *request = [[eHousekeepingService_GetLaundryItemPriceList alloc] init];
    
    if (userID) {
        [request setIntUsrID:userID];
    }
    
    if (modifiedDate) {
        [request setStrLastModified:modifiedDate];
    }
    
//    LogObject *logObj = [[LogObject alloc] init];
//    logObj.dateTime = [NSDate date];
//    logObj.log = [NSString stringWithFormat:@"<%i><getAllLaundryItemPriceFromWSByUserId><intUsrID:%i lastModifier:%@>",userID.intValue,userID.intValue,modifiedDate];
//    LogFileManager *logManager = [[LogFileManager alloc] init];
//    [logManager save:logObj];
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetLaundryItemPriceListUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    for (id bodyPart in responseBodyParts ) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetLaundryItemPriceListResponse class]]) {
            eHousekeepingService_GetLaundryItemPriceListResponse *dataBody = bodyPart;
            if (dataBody != nil) {
                if ([dataBody.GetLaundryItemPriceListResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]]) {
                    
                    NSMutableArray *arrayLRItemPrice = dataBody.GetLaundryItemPriceListResult.LdryItmPriceList.LaundryItmPriceList;
                    for (eHousekeepingService_LaundryItmPriceList *lrItemPriceObject in arrayLRItemPrice) {
                        
                        LaundryItemPriceModelV2 *modelObject = [[LaundryItemPriceModelV2 alloc] init];
                        modelObject.lip_id = [lrItemPriceObject.ldryPriID integerValue];
                        modelObject.lip_item_id = [lrItemPriceObject.ldryPriID integerValue];
                        modelObject.lip_category_id = [lrItemPriceObject.ldryLaundryCategoryID integerValue];
                        modelObject.lip_price = [lrItemPriceObject.ldryPrice floatValue];
                        modelObject.lip_last_modified = lrItemPriceObject.ldryLastModified;
                        LaundryItemPriceModelV2 *compareOject = [[LaundryItemPriceModelV2 alloc] init];
                        compareOject.lip_id = modelObject.lip_id;
                        
                        int result = [self insertLaundryItemPriceModelV2:modelObject];
                        
                        if (result == 0) {
                            
                            [self loadLaundryItemPriceModelByLIPModel:compareOject];
                            if ([compareOject.lip_last_modified compare:modelObject.lip_last_modified] == NSOrderedAscending) {
                                
                                [self updateLaundryItemPriceModelV2:modelObject];
                            }
                            
                        }
                        
                    }
                    
                }
            }
            
        }
    }
    
}

#pragma mark -==post Laundry Order===
-(int)postLaundryOrder:(LaudryOrderModelV2*)lr_Order_Model andsubQuantity:(int)subQuantity andServiceID:(int)serviceID andHotel: (int)HotelID andsubTotal:(CGFloat) subTotal
{
    int laundry_order_ID = 0;
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole])
    {
        [binding setLogXMLInOut:YES];
    }
    eHousekeepingService_PostLaundryOrder *request = [[eHousekeepingService_PostLaundryOrder alloc] init];
    
    if (lr_Order_Model) {
        [request setIntUsrID:[NSNumber numberWithInt:lr_Order_Model.laundryOrderUserId]];
        [request setIntRoomAssignID:[NSNumber numberWithInt:lr_Order_Model.laundryOrderRoomId]];
        [request setIntLaundryServiceID:[NSNumber numberWithInt:serviceID]];
        [request setIntTotalQuantity:[NSNumber numberWithInt:subQuantity]];
        [request setStrTransactionTime:lr_Order_Model.laundryOrderCollectDate];
        [request setDblServiceCharge:[NSNumber numberWithInt:0]];
        [request setIntHotelID:[NSNumber numberWithInt:HotelID]];
        [request setIntTotalQuantity:[NSNumber numberWithFloat:subTotal]];
        
    }
    
//    LogObject *logObj = [[LogObject alloc] init];
//    logObj.dateTime = [NSDate date];
//    logObj.log = [NSString stringWithFormat:@"<%i><getAllLaundryItemPriceFromWSByUserId><intUsrID:%i intRoomAssignID:%i intLaundryServiceID:%i intTotalQuantity:%i transactionTime:%@ DblServiceCharge:%i intHotelID:%i intTotalQuantity:%i>",lr_Order_Model.laundryOrderUserId,lr_Order_Model.laundryOrderRoomId,serviceID,subQuantity,lr_Order_Model.laundryOrderCollectDate,0,HotelID,subTotal];
//    LogFileManager *logManager = [[LogFileManager alloc] init];
//    [logManager save:logObj];
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding PostLaundryOrderUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    for (id bodyPart in responseBodyParts ) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_PostLaundryOrderResponse class]]) {
            eHousekeepingService_PostLaundryOrderResponse *dataBody=bodyPart;
            if (dataBody != nil) {
                if ([dataBody.PostLaundryOrderResult.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_NEW_RECORD_ADDED]]) {
                    
                    //get orderId from WS
                    NSInteger orderIdWS = [dataBody.PostLaundryOrderResult.respMsg integerValue];  
                    laundry_order_ID = orderIdWS;
                }
                else
                {
                    laundry_order_ID=0;
                }
            }
            
        }
    }
    
    return laundry_order_ID;
}

#pragma mark -==get Laundry Order===
-(BOOL)postLaundryItem:(LaundryOrderDetailModelV2*)lr_Order_Detail_Model andLaundryOrder: (int)laundry_Order_Id anduser:(int)userId andTransactionTime:(NSString *)transaction_time
{
    BOOL result = NO;
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    eHousekeepingService_PostLaundryItem *request = [[eHousekeepingService_PostLaundryItem alloc] init];
    
    if (lr_Order_Detail_Model) {
        [request setIntUsrID:[NSNumber numberWithInt:userId]];
        [request setIntLaundryOrderID:[NSNumber numberWithInt:laundry_Order_Id]];
        [request setIntLaundryItemID:[NSNumber numberWithInt:lr_Order_Detail_Model.lod_item_id]];
        [request setIntLaundryCategoryID:[NSNumber numberWithInt:lr_Order_Detail_Model.lod_category_id]];
        [request setIntLaundryServiceID:[NSNumber numberWithInt:lr_Order_Detail_Model.lod_service_id]];
        [request setIntQuantity:[NSNumber numberWithInt:lr_Order_Detail_Model.lod_quantity]];
        [request setStrTransactionTime:transaction_time];
    }
    
    LogObject *logObj = [[LogObject alloc] init];
    logObj.dateTime = [NSDate date];
    logObj.log = [NSString stringWithFormat:@"<%i><postLaundryItem><intUsrID:%i intLaundryOrderID:%i intLaundryItemID:%i intLaundryCategoryID:%i ntLaundryServiceID:%i ntQuantity:%i transactionTim:%@>",userId,userId,laundry_Order_Id,lr_Order_Detail_Model.lod_item_id,lr_Order_Detail_Model.lod_category_id,lr_Order_Detail_Model.lod_service_id,lr_Order_Detail_Model.lod_quantity,transaction_time];
    LogFileManager *logManager = [[LogFileManager alloc] init];
    [logManager save:logObj];
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding PostLaundryItemUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    for (id bodyPart in responseBodyParts ) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_PostLaundryItemResponse class]]) {
            eHousekeepingService_PostLaundryItemResponse *dataBody=bodyPart;
            if (dataBody != nil) {
                if ([dataBody.PostLaundryItemResult.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_NEW_RECORD_ADDED]]) { 
                    lr_Order_Detail_Model.lod_post_status = POST_STATUS_POSTED;
                    [self updateLaundryOrderDetailModelV2:lr_Order_Detail_Model];
                    result = YES;
                }
                else
                {
                    result = NO;
                }
            }
            
        }
    }
    
    return result;
}

-(NSInteger)numberOfMustPostLaundryRecords:(NSInteger)userId{
    NSMutableArray *arrayLaundry = [self loadAllLaundryOrderByUserId:userId];
    NSInteger countPostLaundry = 0;
    for (LaudryOrderModelV2 *laundryModel in arrayLaundry) {
        NSMutableArray *arrayPostLaundry = [self loadAllLaundryOrderDetailsByOrderId:laundryModel.laundryOrderId];
        countPostLaundry += arrayPostLaundry.count;
    }
    return countPostLaundry;
}


#pragma mark - Laundry Intruction
#pragma mark - === get Laundry Instruction ===
-(void)getAllLaundryInstructionFromWSByUserId : (NSNumber *)userID andModifiedDate:(NSString *)modifiedDate
{
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
//    [binding setDefaultTimeout:60];
    
    eHousekeepingService_GetLaundrySpecialInstructionList *request = [[eHousekeepingService_GetLaundrySpecialInstructionList alloc] init];
    
    if (userID) {
        [request setIntUsrID:userID];
    }
    
    if (modifiedDate) {
        [request setStrLastModified:modifiedDate];
    }
    
//    LogObject *logObj = [[LogObject alloc] init];
//    logObj.dateTime = [NSDate date];
//    logObj.log = [NSString stringWithFormat:@"<%i><getAllLaundryInstructionFromWSByUserId><intUsrID:%i lastModified:%@>",userID.intValue,userID.intValue,modifiedDate];
//    LogFileManager *logManager = [[LogFileManager alloc] init];
//    [logManager save:logObj];
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding GetLaundrySpecialInstructionListUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    for (id bodyPart in responseBodyParts ) {
        
        if ([bodyPart isKindOfClass:[eHousekeepingService_GetLaundrySpecialInstructionListResponse class]]) {
            
            eHousekeepingService_GetLaundrySpecialInstructionListResponse *dataBody=bodyPart;
            
            if (dataBody != nil) {
                
                if ([dataBody.GetLaundrySpecialInstructionListResult.ResponseStatus.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]]) {
                    
                    NSMutableArray *arrayLRInstruction=dataBody.GetLaundrySpecialInstructionListResult.LdryInstructionList.LaundryInstructionList;
                    
                    for (eHousekeepingService_LaundryInstructionList *lrInstructionObject in arrayLRInstruction) {
                        
                        LaundryIntructionModelV2 *modelObject = [[LaundryIntructionModelV2 alloc] init];
                        modelObject.instructionId = [lrInstructionObject.ldrySpcInstructionID integerValue];
                        modelObject.instructionName = lrInstructionObject.ldrySpcInstructionName;
                        modelObject.instructionNameLang = lrInstructionObject.ldrySpcInstructionLang;
                        modelObject.instructionLastModified = lrInstructionObject.ldrySpcLastModified;
                        LaundryIntructionModelV2 *compareOject = [[LaundryIntructionModelV2 alloc] init];
                        compareOject.instructionId = modelObject.instructionId;
                        
                        NSInteger result= [self insertLaundryInstructionsModelV2:modelObject];
                        
                        if (result == 0) {
                            
                            [self loadLaundryInstructionsModelV2:compareOject];
                            
                            if ([compareOject.instructionLastModified isEqualToString:modelObject.instructionLastModified] == NO) {
                                
                                [self updateLaundryInstructionsModelV2:modelObject];
                                
                            }   
                        }   
                    }
                }
            }
        }
    }
}

#pragma mark -==post Laundry Intruction Order===
-(int)postLaundryInstructionOrder:(LaundryInstructionsOrderModelV2*)lr_Instruction_Order_Model andLaundryOrder: (int)laundry_Order_Id anduser:(int)userId 
{
    int laundry_instruction_order_ID = 0;
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    eHousekeepingService_PostLaundrySpecialInstruction *request = [[eHousekeepingService_PostLaundrySpecialInstruction alloc] init];
    
    if (lr_Instruction_Order_Model) {
        [request setIntUsrID:[NSNumber numberWithInt:userId]];
        [request setIntLaundryOrderID:[NSNumber numberWithInt:laundry_Order_Id]];
        [request setIntLaundrySpcialInstructionID:[NSNumber numberWithInt:lr_Instruction_Order_Model.instructionId]];
    }
    
    LogObject *logObj = [[LogObject alloc] init];
    logObj.dateTime = [NSDate date];
    logObj.log = [NSString stringWithFormat:@"<%i><postLaundryInstructionOrder><intUsrID:%i intLaundryOrderID:%i intLaundrySpcialInstructionID:%i>",userId,userId,laundry_Order_Id,lr_Instruction_Order_Model.instructionId];
    LogFileManager *logManager = [[LogFileManager alloc] init];
    [logManager save:logObj];
    
    eHousekeepingServiceSoap12BindingResponse *response = [binding PostLaundrySpecialInstructionUsingParameters:request];
    
    NSArray *responseBodyParts = response.bodyParts;
    for (id bodyPart in responseBodyParts ) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_PostLaundrySpecialInstructionResponse class]]) {
            eHousekeepingService_PostLaundrySpecialInstructionResponse *dataBody=bodyPart;
            if (dataBody != nil) {
                if ([dataBody.PostLaundrySpecialInstructionResult.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_NEW_RECORD_ADDED]]) {
                    NSString *orderIdWS = dataBody.PostLaundrySpecialInstructionResult.respMsg;
                    NSArray* arrayMsgOrderId = [orderIdWS componentsSeparatedByString: @":"];
                    NSString* orderId = [arrayMsgOrderId objectAtIndex: 1];
                    
                    laundry_instruction_order_ID = [[orderId stringByReplacingOccurrencesOfString:@" " withString:@""] integerValue];
                }
                else{
                    laundry_instruction_order_ID = 0;
                }
            }
            
        }
    }
    
    return laundry_instruction_order_ID;
}

-(NSInteger)numberOfMustPostLaundryInstructionRecords:(NSInteger)userId
{
    NSMutableArray *arrayLaundry = [self loadAllLaundryOrderByUserId:userId];
    NSInteger countPostLaundryInstruction = 0;
    for (LaudryOrderModelV2 *laundryModel in arrayLaundry) {
        NSMutableArray *arrayPostLaundryInstruction = [self loadAllLaundryInstructionsOrderModelV2ID:laundryModel.laundryOrderId];
        countPostLaundryInstruction += arrayPostLaundryInstruction.count;
    }
    return countPostLaundryInstruction;
}

#pragma mark - Post WSLog
- (BOOL) postWSLog:(NSInteger)userId Message:(NSString *)messageValue MessageType:(NSInteger)type{
    
    eHousekeepingServiceSoap12Binding *binding = [eHousekeepingService eHousekeepingServiceSoap12Binding];
    
    //Allow write log or not
    if([LogFileManager isLogConsole])
    {
        [binding setLogXMLInOut:YES];
    }
    eHousekeepingService_PostWSLog *request = [[eHousekeepingService_PostWSLog alloc] init];
    request.intUsrID = [NSNumber numberWithInt:userId];
    request.strMessage = messageValue;
    request.intMessageType = [NSNumber numberWithInt:type];
    eHousekeepingServiceSoap12BindingResponse *respone = [binding PostWSLogUsingParameters:request];
    BOOL isPostSuccess = NO;
    for (id bodyPart in respone.bodyParts) {
        if ([bodyPart isKindOfClass:[eHousekeepingService_PostWSLogResponse class]]) {
            eHousekeepingService_PostWSLogResponse *dataBody = (eHousekeepingService_PostWSLogResponse *)bodyPart;
            if (dataBody != nil) {
                if ([dataBody.PostWSLogResult.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]]) {
                    isPostSuccess = YES;
                }
            }
        }
    }
    return isPostSuccess;
}

@end
