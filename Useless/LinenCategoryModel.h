//
//  LinenCategoryModel.h
//  mHouseKeeping
//
//  Created by TMS on 2/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LinenCategoryModel : NSObject

@property (nonatomic, assign) NSInteger linenCategoryID;
@property (nonatomic, retain) NSData *linenCategoryImage;
@property (nonatomic, retain) NSString *linenCategoryTitle;

@end
