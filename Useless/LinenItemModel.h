//
//  LinenItemModel.h
//  mHouseKeeping
//
//  Created by TMS on 2/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LinenItemModel : NSObject

@property (nonatomic, assign) NSInteger linenItemID;
@property (nonatomic, strong) NSData *linenItemImage;
@property (nonatomic, strong) NSString *linenItemTitle;
@property (nonatomic, assign) NSInteger linenItemQuantity;

@end
