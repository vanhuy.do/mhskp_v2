//
//  LoginEngine.h
//  eHouseKeeping
//
//  Created by chinh bx on 6/13/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserAdapterV2.h"


@interface LoginEngine : NSObject {
    UserModelV2* returnUserModel;
}
+(void)getUserThenInsertToDB;
-(BOOL)isAuthenticate:(NSString*)user_name:(NSString*)user_password;
-(userRoles*)getRole:(NSString*)user_name:(NSString*)user_password;
@end
