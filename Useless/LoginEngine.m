//
//  LoginEngine.m
//  eHouseKeeping
//
//  Created by chinh bx on 6/13/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "LoginEngine.h"
#import "UserModelV2.h"
#import "UserAdapterV2.h"
#import "UserManagerV2.h"

@implementation LoginEngine

+(void)getUserThenInsertToDB
{
    UserManagerV2 *manager = [[UserManagerV2 alloc] init];
    
    UserModelV2 *userModel=[[UserModelV2 alloc] init] ;
    // cheating info for sample data.
    userModel.userId=165;
    userModel.userName=@"a";
    userModel.userFullName=@"Wong Siu Ming";
    userModel.userFullNameLang = @"";
    userModel.userLang=@"English";
    userModel.userPassword=@"a";
    userModel.userDepartmentId=1;
    userModel.userDepartment=@"HouseKeeper";
    userModel.userRole=@"H";
    userModel.userSupervisorId=0;
    userModel.userLastModified=@"2011-06-02 12:16:55";
    userModel.userHotelsId=1;
    userModel.userTitle = @"Mrs.";
    //---end of cheating.
    //NSLog(@"%d",[userModel insert]);
    [manager insert:userModel];
    [userModel release];
    
    UserModelV2 *superuserModel=[[UserModelV2 alloc] init] ;
    // cheating info for sample data.
    superuserModel.userId=166;
    superuserModel.userName=@"s";
    superuserModel.userFullName=@"Wang Lee Hom";
    userModel.userFullNameLang = @"";
    superuserModel.userLang=@"eHouseKeeping SuperUser";
    superuserModel.userPassword=@"s";
    superuserModel.userDepartmentId=1;
    superuserModel.userDepartment=@"Supervisor";
    superuserModel.userRole=@"S";
    superuserModel.userSupervisorId=1;
    superuserModel.userLastModified=@"2011-06-02 12:16:55";
    superuserModel.userHotelsId=1;
    superuserModel.userTitle = @"Mr.";
    
    //[superuserModel insert];
    [manager insert:superuserModel];
    
    [superuserModel release];
    
    [manager release];
}

-(BOOL)isAuthenticate:(NSString*)user_name:(NSString*)user_password
{
    /*
    BOOL result;
    //UserModel *userModel=nil;
    UserModel *userModel=[[UserModel alloc] init];
    //if(userModel==nil)
    // cheating user login
    userModel.user_Name=user_name;
    userModel.user_Password=user_password;
    
    //result=  [userModel isAuthenticateUser];
    if(result)
    {
       
       //[userModel loadUser:userModel];
        UserManager *set1=[UserManager sharedUserManager];
        [set1 setCurrentUser:userModel];
    
        
    }
    [userModel release];
    //[userModel release];
     
    if (result) 
    {   
         return YES;
    }
    else
    {
        
        return NO;
    }
     */
    return YES;
}

-(userRoles*)getRole:(NSString*)user_name:(NSString*)user_password
{
//    UserModel *userModel=[[[UserModel alloc] init] autorelease];
//    userModel.user_Name=user_name;
//    userModel.user_Password=user_password;
//    userRoles* aUserRoles;// = [userModel authorization];
//    //[userModel release];
//    return aUserRoles;
    return nil;
}
 
-(void)dealloc
{
    [super dealloc];
    [returnUserModel release];
}

@end
