//
//  MenuCheckListCell.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuCheckListCell : UITableViewCell{
    UIImageView *imgStatusCheckList;
    UIImageView *imgCheckList;
    UILabel *lblNameCheckList;
    UIImageView *imgActionCheckList;
}
@property (nonatomic, retain) IBOutlet UIImageView *imgStatusCheckList;
@property (nonatomic, retain) IBOutlet UIImageView *imgCheckList;
@property (nonatomic, retain) IBOutlet UILabel *lblNameChecklist;
@property (nonatomic, retain) IBOutlet UIImageView *imgActionCheckList;
@end
