//
//  MenuCountCell.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuCountCell : UITableViewCell{
    UIImageView *imageView;
    UILabel *label;
}
@property (nonatomic,retain) IBOutlet UIImageView *imageView;
@property (nonatomic,retain) IBOutlet UILabel *label;
@end
