//
//  ReAssignRoomViewControllerV2.h
//  mHouseKeeping
//
//  Created by TMS on 5/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RemarkViewV2.h"
#import "RoomModelV2.h"
#import "RoomAssignmentModelV2.h"

typedef enum {
    ROOM_IS_CLEANING = YES,
    ROOM_IS_COMPLETED = NO
}ENUM_IS_ROOM_CLEANING;

#define kRecleaningTimeValue     @"recleaning time value"

@interface ReAssignRoomViewControllerV2 : UIViewController <MBProgressHUDDelegate, UIPickerViewDelegate, UIPickerViewDataSource, RemarkViewV2Delegate, UIAlertViewDelegate> {
    __unsafe_unretained IBOutlet UILabel *lblRoomNo;
    __unsafe_unretained IBOutlet UIImageView *imgRoomStatus;
    __unsafe_unretained IBOutlet UILabel *lblRoomStatus;
    __unsafe_unretained IBOutlet UILabel *lblGuestName;
    __unsafe_unretained IBOutlet UILabel *lblReassignTo;
    __unsafe_unretained IBOutlet UILabel *lblReassignDateTo;
    __unsafe_unretained IBOutlet UILabel *lblReassignTimeAt;
    __unsafe_unretained IBOutlet UILabel *lblRecleaningTime;
    __unsafe_unretained IBOutlet UIView *controlsView;
    __unsafe_unretained IBOutlet UILabel *lblRemark;
    __unsafe_unretained IBOutlet UILabel *lblRemarkValue;
    __unsafe_unretained IBOutlet UIButton *btnReassignTo;
    __unsafe_unretained IBOutlet UIButton *btnReassignDateTo;
    __unsafe_unretained IBOutlet UIButton *btnReassignTimeAt;
    __unsafe_unretained IBOutlet UIButton *btnRecleaningTime;
    __unsafe_unretained IBOutlet UILabel *lblMins;
    __unsafe_unretained IBOutlet UINavigationBar *navigationBarPicker;
    __unsafe_unretained IBOutlet UIBarButtonItem *btnCancel;
    __unsafe_unretained IBOutlet UIBarButtonItem *btnDone;
    __unsafe_unretained IBOutlet UIDatePicker *pickerTime;
    __unsafe_unretained IBOutlet UIPickerView *pickerReassignTo;
    __unsafe_unretained IBOutlet UIPickerView *pickerRecleaningTime;
    __unsafe_unretained IBOutlet UIDatePicker *pickerDate;
    IBOutlet UIView *chooseDataView;
    __unsafe_unretained IBOutlet UIButton *btnRemarkArrow;
    
    __unsafe_unretained RoomModelV2 *roomModel;
    
    UIButton *btnBack;
    
    MBProgressHUD *HUD;
    // Choosen User HK 
    UserListModelV2 *userChoosen;
    //store all user list
    NSMutableArray *userlist;
    
    //room assignment model
    RoomAssignmentModelV2 *ramodel;
    
    NSDate *selectedDate;
    NSDate *selectedTime;
    
    //recleaning time data
    NSMutableArray *recleaningTimes;
    
    BOOL isAlreadyLayoutSubviews;
}

@property (nonatomic, strong) NSMutableArray *recleaningTimes;
@property (nonatomic, strong) UIView *chooseDataView;
@property (nonatomic, strong) RoomAssignmentModelV2 *ramodel;
@property (nonatomic, strong) NSMutableArray *userlist;
@property (nonatomic, readwrite) BOOL roomIsCleaning;
@property (nonatomic, readwrite) BOOL isSaved;
@property (nonatomic, assign) RoomModelV2 *roomModel;
@property(nonatomic,retain)
UserListModelV2 *userChoosen;
- (IBAction)btnRemarkPressed:(id)sender;
- (IBAction)btnReassignToPressed:(id)sender;
- (IBAction)btnReassignDateToPressed:(id)sender;
- (IBAction)btnReassignTimeAtPressed:(id)sender;
- (IBAction)btnRecleaningTimePressed:(id)sender;
- (IBAction)btnCancelPressed:(id)sender;
- (IBAction)btnDonePressed:(id)sender;


-(void) setCaptionsView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil roomCleaningStatus:(ENUM_IS_ROOM_CLEANING) isRoomCleaning roomModel:(RoomModelV2 *) currentRoomModel;

@end
