//
//  RoomDetailView.m
//  EHouseKeeping
//
//  Created by tms on 5/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "RoomDetailAssignmentViewV2.h"
#import "sqlite3.h"
#import "MBProgressHUD.h"
#import "NetworkCheck.h"
//#import "RoomAssignmentModel.h"
//#import "TasksManager.h"
#import "RoomManagerV2.h"
#import "LanguageManager.h"
#import "RoomInfoAssignmentV2.h"
#import "RemarkViewV2.h"
#import "GuidelineViewV2.h"
#import "CountPopupReminderViewV2.h"
#import "CheckListManagerV2.h"
#import "TasksManagerV2.h"
#import "LogFileManager.h"

#pragma mark constant
#pragma mark

#define tagCleaningStatus 12
#define tagLabel 1234
#define tagText 5678
#define tagImage 9101
#define tagAccessoryType 9999

#define tagAlertRoomStatusChange 200
#define tagAlertBlockRoom 300
#define tagAlertCleaningStatusCompleted 400
#define kTagAlertDoneWithRoomCleaning 500
#define kTagAlertCancelCleaningStatus 600
#define kTagAlertCleaningStatusChangeToStop 700
#define tagAlertCancelCleaningLessThan1Minute 19

#define xOfSccessory 280
#define space2ElementCell 10

#define IMAGE_PAUSE_ON  @"phuse_bt_on_299x58.png"
#define IMAGE_PAUSE_GRAY  @"phuse_bt_on_299x58.png"
#define IMAGE_PAUSE @"phuse_bt_299x58.png"

#define IMAGE_TIMER_PLAY @"timer_play_274x58.png"
#define IMAGE_TIMER_PAUSE @"timer_phuse_274x58.png"
#define IMAGE_TIMER_GRAY @"timer_gray_274x58.png"
#define IMAGE_TIMER_OVER @"timer_red_274x58.png"

#define IMAGE_COMPLETE @"click_bt_299x58.png"
#define IMAGE_COMPLETE_GRAY @"click_bt_gray_299x58.png"

#define IMAGE_COMPLETE_ON @"click_bt_on_299x58.png"

#define IMAGE_PLAY @"play_bt_299x58.png"
#define IMAGE_PLAY_GRAY @"play_bt_gray_299x58.png"
#define IMAGE_PLAY_ON @"play_bt_on_299x58.png"

#define IMAGE_STOP_ON   @"stop_bt_on_299x58.png"
#define IMAGE_STOP_GRAY @"stop_bt_gray_299x58.png"
#define IMAGE_STOP      @"stop_bt_299x58.png"

#define IMAGE_BT_GRAY @"bt_gray_299x58.png"
#define IMAGE_BT_ON @"bt_on_299x58.png"

#define kTimeFormat @"HH:mm:ss"
#define kTime12HoursFormat @"hh:mm a"

@implementation RoomDetailAssignmentViewV2
@synthesize tableView,expectedCleaningTime,guestPref,additioJob,cleaningStatus,scrollView,listButtonView;
@synthesize btnCount,btnEngineeringAssitance,btnGuideLine,btnLaundry,btnLostAnDFound, dataModel,indexPathCleaaningStatus;
@synthesize roomStatus,remark,estimatedInspectionTime,actualInspectionTime,inspectionStatus;
@synthesize reassignBtn;
@synthesize superController;
@synthesize isPass;
@synthesize isInspected;
@synthesize isCompleted;
@synthesize delegate;
@synthesize roomRecord;
@synthesize roomRemarkModel;
@synthesize msgbtnOk;
@synthesize msgSaveRoomDetail;
@synthesize isSaved;
@synthesize userCanInteraction;
@synthesize raDataModel;
@synthesize countTimeStatus;
@synthesize isFindByRoomView;
@synthesize declineServiceDate;

int hours, minutes, seconds;
int secondsLeft;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self == [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        self.title = @"Room Details";
    }
    
    return self;
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - Reload data when sync done
-(RoomRemarkModelV2*)getLatestRoomRemark{
    NSMutableArray *listRoomRemark = [[RoomManagerV2 sharedRoomManager] loadAllRoomRemarkByRecordId:self.roomRecord.rrec_Id];
    
    RoomRemarkModelV2 *roomRemark;
    for (int index = 0; index < [listRoomRemark count]; index++) {
       roomRemark  =(RoomRemarkModelV2*) [listRoomRemark objectAtIndex:index];
    }
    return roomRemark;
}

#pragma mark - View lifecycle
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    //set captions view
    [self setCaptionsView];
    
    if(animated == NO){

    }
    
    if(!animated && ![CommonVariable sharedCommonVariable].isBackFromChk){
        [[RoomManagerV2 sharedRoomManager] loadRoomModel:self.dataModel]; // load room data
    }
    
    if(animated == YES){
      
        isMustResetTimer = NO;

    }
    
    if(self.raDataModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_PAUSE){
//        RoomRecordDetailModelV2 *_roomRecordDetail = [[RoomRecordDetailModelV2 alloc] init];
//        _roomRecordDetail.recd_Ra_Id = [TasksManagerV2 getCurrentRoomAssignment];
//        _roomRecordDetail.recd_User_Id = [UserManagerV2 sharedUserManager].currentUser.userId;
//        [[RoomManagerV2 sharedRoomManager] loadRoomRecordDataDetailByRoomAssignmentIdAndUserId:_roomRecordDetail];
//        
        
//        NSTimeInterval timeDiff = [[timeFomarter dateFromString:_roomRecordDetail.recd_Stop_Date] timeIntervalSinceDate:
//                                   [timeFomarter dateFromString:_roomRecordDetail.recd_Start_Date]];            

//        timeLeftCleaningStatus-= (int)timeDiff;
      //  isMustResetTimer = YES;
    }
    
    if(self.raDataModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_PAUSE){        
        [countTimeButton setBackgroundImage:[UIImage imageNamed:IMAGE_PLAY_ON] forState:UIControlStateNormal];
        [timerButon setBackgroundImage:[UIImage imageNamed:IMAGE_TIMER_PAUSE] forState:UIControlStateNormal];
        [finishTimeButton setBackgroundImage:[UIImage imageNamed:IMAGE_COMPLETE_ON] forState:UIControlStateNormal];
        [finishTimeButton setUserInteractionEnabled:YES];
        countTimeStatus = STOP; // pause is STOP
    }  
    
    [self refreshLayoutButtons];
    
    if(animated == NO){
        // assign data for compare property edited:
        dataModelCompare = [[RoomModelV2 alloc] init];
        raDataModelCompare.roomAssignmentRoomCleaningStatusId = raDataModel.roomAssignmentRoomCleaningStatusId;
        raDataModelCompare.roomAssignmentRoomStatusId = raDataModel.roomAssignmentRoomStatusId;

    }
    
     [self.tableView reloadData];
}

/***************************************/
// layout buttons
-(void)refreshLayoutButtons{
    //[btnAction setHidden:YES];
    
    
    CGRect f = timerButon.frame;
    
    if (countTimeStatus == START || countTimeStatus == FINISH) {
      
        [timerButon setFrame:CGRectMake(f.origin.x, btnAction.frame.origin.y + btnAction.frame.size.height + 4, f.size.width, f.size.height)];
        [finishTimeButton setUserInteractionEnabled:YES];
        [finishTimeButton setBackgroundImage:[UIImage imageNamed:IMAGE_COMPLETE_ON] forState:UIControlStateNormal];
        [btnAction setUserInteractionEnabled:YES];
        [btnAction setBackgroundImage:[UIImage imageNamed:IMAGE_BT_ON] forState:UIControlStateNormal];

    } else {
        [btnAction setUserInteractionEnabled:NO];
        [finishTimeButton setUserInteractionEnabled:NO];
        [finishTimeButton setBackgroundImage:[UIImage imageNamed:IMAGE_COMPLETE_GRAY] forState:UIControlStateNormal];
        [btnAction setBackgroundImage:[UIImage imageNamed:IMAGE_BT_GRAY] forState:UIControlStateNormal];
        
         [timerButon setFrame:CGRectMake(f.origin.x, btnAction.frame.origin.y + btnAction.frame.size.height + 4, f.size.width, f.size.height)];
        //[timerButon setFrame:CGRectMake(f.origin.x, btnAction.frame.origin.y, f.size.width, f.size.height)];
    
    }
  
    [lockButton setHidden:YES];
    [checkListButton setHidden:YES];
    
    //Hao Tran - Remove for can't build
    /*
    // in case cleaning status is pause
    // layout view in case : cleaning status is pause
    if(self.raDataModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_PAUSE){
        
        if([self.roomRecord.rrec_Cleaning_Duration intValue] > 0){
            //timeLeftCleaningStatus -=   [self.roomRecord.rrec_Cleaning_Duration intValue];            
            
            [countTimeButton setBackgroundImage:[UIImage imageNamed:IMAGE_PLAY_ON] forState:UIControlStateNormal];
            [timerButon setBackgroundImage:[UIImage imageNamed:IMAGE_TIMER_PAUSE] forState:UIControlStateNormal];
        }        
        
        countTimeStatus = STOP;
    }
    */
    
    if(self.raDataModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_DECLINE_SERVICE){
        [finishTimeButton setBackgroundImage:[UIImage imageNamed:IMAGE_COMPLETE_GRAY] forState:UIControlStateNormal];
        [countTimeButton setBackgroundImage:[UIImage imageNamed:IMAGE_PLAY_ON] forState:UIControlStateNormal];
        [timerButon setBackgroundImage:[UIImage imageNamed:IMAGE_TIMER_GRAY] forState:UIControlStateNormal];
        
        [countTimeButton setUserInteractionEnabled:NO];
        [finishTimeButton setUserInteractionEnabled:NO];
        
        [btnAction setUserInteractionEnabled:NO];
        [btnAction setBackgroundImage:[UIImage imageNamed:IMAGE_BT_GRAY] forState:UIControlStateNormal];
    }
    
    if(self.raDataModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_PENDING_FIRST_TIME){
        [finishTimeButton setBackgroundImage:[UIImage imageNamed:IMAGE_COMPLETE_GRAY] forState:UIControlStateNormal];
    }

    [self displayTimerButton];
}

-(void)layoutButton{
    //Hao Tran - Remove for can't build
    /*
    [timerButon setBackgroundImage:[UIImage imageNamed:IMAGE_TIMER_GRAY] forState:UIControlStateNormal];
    
    CGRect f = timerButon.frame;
    [timerButon setFrame:CGRectMake(f.origin.x, btnAction.frame.origin.y, f.size.width, f.size.height)];
    
    [lockButton setHidden:YES];
    [checkListButton setHidden:YES];
    
    // layout view in case : cleaning status is pause
    if(self.raDataModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_PAUSE){
        
        if([self.roomRecord.rrec_Cleaning_Duration intValue] > 0){
            timeLeftCleaningStatus -=   [self.roomRecord.rrec_Cleaning_Duration intValue]; 
            
        }
    }
    
    [self displayTimerButton];
     */
}


/***************************************/

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.expectedCleaningTime= [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_EXPECTED_CLEANING_TIME];
    self.guestPref= [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_GUEST_REFERENCE];
    self.additioJob= [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ADDITIONAL_JOB];
    self.cleaningStatus= [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_CLEANING_STATUS];
    self.roomStatus = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_room_status];
    self.remark = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_remark_title];
    self.estimatedInspectionTime = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_estimated_inspection_time];
    self.actualInspectionTime = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_actual_inspection_time];
    self.inspectionStatus = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_room_inspectionstatus];
    self.tableView.backgroundColor=[UIColor clearColor];
    
    [self refreshLayoutButtons];   
    
    countTimeStatus = NOT_START;
    if (finishTimeButton) {
        [finishTimeButton setUserInteractionEnabled:NO];
    }
    
    timeFomarter = [[NSDateFormatter alloc] init];
    [timeFomarter setDateFormat:[ehkConvert getyyyy_MM_dd_HH_mm_ss]];
    
    durationTime = 0;   
    timeLeftCleaningStatus = raDataModel.roomAssignmentRoomExpectedCleaningTime*60;
    
    isSaved = YES;
    
    if(self.roomRecord == nil){
        self.roomRecord = [[RoomRecordModelV2 alloc] init];
        self.roomRecord.rrec_User_Id = [[UserManagerV2 sharedUserManager] currentUser].userId;
        self.roomRecord.rrec_room_assignment_id = [TasksManagerV2 getCurrentRoomAssignment];
        self.roomRecord = [[RoomManagerV2 sharedRoomManager] loadRoomRecordDataByRoomIdAndUserId:self.roomRecord];
        if(self.roomRecord.rrec_Id <=0) {            
            self.roomRecord.rrec_PostStatus = POST_STATUS_SAVED_UNPOSTED;
            [[RoomManagerV2 sharedRoomManager] insertRoomRecordData:self.roomRecord];
            self.roomRecord = [[RoomManagerV2 sharedRoomManager] loadRoomRecordDataByRoomIdAndUserId:self.roomRecord];
        }
    }
    
    if(roomRecordDetailModel == nil){
        roomRecordDetailModel = [[RoomRecordDetailModelV2 alloc] init];
    }
    roomRecordDetailModel.recd_Ra_Id = [TasksManagerV2 getCurrentRoomAssignment];
    roomRecordDetailModel.recd_User_Id = [UserManagerV2 sharedUserManager].currentUser.userId;

    [[RoomManagerV2 sharedRoomManager] loadRoomRecordDataDetailByRoomAssignmentIdAndUserId:roomRecordDetailModel ];

    durationTime = 0;
    timeLeftCleaningStatus = raDataModel.roomAssignmentRoomExpectedCleaningTime*60;
    timeLeftCleaningStatus -= durationTime;
    
    if(self.raDataModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_PAUSE){
        RoomRecordDetailModelV2 *_roomRecordDetail = [[RoomRecordDetailModelV2 alloc] init];
        _roomRecordDetail.recd_Ra_Id = [TasksManagerV2 getCurrentRoomAssignment];
        _roomRecordDetail.recd_User_Id = [UserManagerV2 sharedUserManager].currentUser.userId;
        [[RoomManagerV2 sharedRoomManager] loadRoomRecordDataDetailByRoomAssignmentIdAndUserId:_roomRecordDetail];
        
        
//        NSTimeInterval timeDiff = [[timeFomarter dateFromString:_roomRecordDetail.recd_Stop_Date] timeIntervalSinceDate:
//                                   [timeFomarter dateFromString:_roomRecordDetail.recd_Start_Date]];
        
        //timeLeftCleaningStatus-= (int)timeDiff;
        isMustResetTimer = YES;
    }
    
    count = 0;
}

#pragma didden after save.

-(void) hiddenHUDAfterSaved:(MBProgressHUD *)HUD{
    [HUD hide:YES];
    [HUD removeFromSuperview];
}


- (void)viewDidUnload
{
    [self setReassignBtn:nil];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark
#pragma mark delegate/datasource tableview

- (void)addAccessoryButton:(UITableViewCell *)cell  {
    /*****************************************/
    UIImageView *accessoryButton = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 24, 24)];
    [accessoryButton setBackgroundColor:[UIColor clearColor]];
    [accessoryButton setImage:[UIImage imageNamed:@"gray_arrow_36x36.png"]];
    [accessoryButton setTag:tagAccessoryType];
    [cell setAccessoryView:accessoryButton];
    cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
    /*****************************************/
}

-(NSInteger)tableView:(UITableView *)atableView numberOfRowsInSection:(NSInteger)section
{
    if(raDataModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_COMPLETED){
        return 6;
    } else {
        return 5;
    }
}

/****************************** implement cell *************************************/
//@ add Additional to cell
- (void)setupAdditionalCell:(UITableViewCell *)cell isNew:(BOOL)isNew
{
    CGRect additionalJobLabelRect = CGRectMake(10, 1, 150 , 30);    
    // display label control in table view
    
    UILabel *actualCleaningTimeLabel;
    
    if (isNew == TRUE) {
        actualCleaningTimeLabel = [[UILabel alloc] initWithFrame:additionalJobLabelRect];
    } else {
        actualCleaningTimeLabel = (UILabel *)[cell.contentView viewWithTag:tagLabel];
    }
    
    actualCleaningTimeLabel.textAlignment = UITextAlignmentLeft;
    actualCleaningTimeLabel.font = [UIFont boldSystemFontOfSize:12];
    actualCleaningTimeLabel.text = self.additioJob;
    actualCleaningTimeLabel.tag = tagLabel;
    
    UIImage *img=[UIImage imageNamed:@"tab_small.png"];
    actualCleaningTimeLabel.textColor=[UIColor colorWithPatternImage:img];
    
    [actualCleaningTimeLabel setBackgroundColor:[UIColor clearColor]];
   
    
    
    //label 2 of this cell
    NSString *tmp = dataModel.room_AdditionalJob;
    CGSize t = [tmp sizeWithFont:[UIFont boldSystemFontOfSize:12] constrainedToSize:CGSizeMake(135, 300) lineBreakMode:UILineBreakModeWordWrap];
    CGRect additionalJobValueRect = CGRectMake(160, 1, 135 , 30);

    UILabel *additionalJobValue;
    
    if (isNew == TRUE) {
        additionalJobValue=[[UILabel alloc] initWithFrame:additionalJobValueRect];
    } else {
        additionalJobValue = (UILabel *)[cell.contentView viewWithTag:tagText];
        [additionalJobValue setFrame:CGRectMake(160, 1, 135 , 30)];
    }
    
    additionalJobValue.textAlignment = UITextAlignmentRight;
    additionalJobValue.font = [UIFont boldSystemFontOfSize:12];
    additionalJobValue.lineBreakMode = UILineBreakModeWordWrap;
    int tmprow = floor(t.height/20) + 1;
    additionalJobValue.numberOfLines = tmprow;
    additionalJobValue.tag = tagText;
    
    //set additionalJobValue from dataModel
    [additionalJobValue setText:dataModel.room_AdditionalJob];    
    [additionalJobValue setBackgroundColor:[UIColor clearColor]];
    UIImageView* imageView = (UIImageView *)[cell.contentView viewWithTag:tagImage];
    [imageView removeFromSuperview];
    
    
    if (isNew == TRUE) {        
        [cell.contentView addSubview:actualCleaningTimeLabel];
        [cell.contentView addSubview:additionalJobValue];
       
    }
    cell.accessoryView = nil;
    cell.accessoryType = UITableViewCellAccessoryNone;
}

// display time with format : ##hour ##minute
-(NSString*) timeDisplay:(NSInteger)time{
    NSInteger hoursInt = time/3600;
    NSInteger minutesInt = (time - (hoursInt*3600)) / 60;
    
    // setup hour string
    NSString *hoursString = [NSString stringWithFormat:@"%d",hoursInt];
    NSString *minutesString = [NSString stringWithFormat:@"%d",minutesInt];
    
    if([hoursString length]==1){
        hoursString = [NSString stringWithFormat:@"%.2d %@",[hoursString integerValue],[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_strhour]];
        if(hoursInt == 0){
            hoursString = @"";
        }
    } else {
        hoursString = [NSString stringWithFormat:@"%@ %@",hoursString,[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_strhour]];
    }
    
    // setup minute string
    if([minutesString length]==1){
        minutesString = [NSString stringWithFormat:@"%.2d %@",[minutesString integerValue],[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_strmins]];
        if(minutesInt == 0){
            minutesString = @"";
        }

    }else{
        minutesString = [NSString stringWithFormat:@"%@ %@",minutesString,[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_strmins]];
    }
    
    
    return [NSString stringWithFormat:@"%@ %@ ",hoursString,minutesString];
}


// display time with format : ##hour ##minute ## seconds
// display time with format : ##hour ##minute ## seconds
-(NSString*) timeDisplayWithSecond:(NSInteger)time{
    NSInteger hoursInt = time/3600;
    NSInteger minutesInt = (time - (hoursInt*3600)) / 60;
   
    // setup hour string
    NSString *hoursString = [NSString stringWithFormat:@"%d",hoursInt];
    NSString *minutesString = [NSString stringWithFormat:@"%d",minutesInt];

    
    if(hoursInt <= 1){
        hoursString = [NSString stringWithFormat:@"%.2d %@",[hoursString integerValue],[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_strhour]];
    } else {
        hoursString = [NSString stringWithFormat:@"%@ %@",hoursString,[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_strhour]];
    }
    
    // setup minute string
    if(minutesInt <= 1){
        minutesString = [NSString stringWithFormat:@"%.2d %@",[minutesString integerValue],[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_strmins]];
        
    }else{
        minutesString = [NSString stringWithFormat:@"%@ %@",minutesString,[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_strmins]];
    }
    
      return [NSString stringWithFormat:@"%@ %@",hoursString,minutesString ];
}

//@ add ExpectedCleaning label to cell
- (void)setupExpectedCleaningCell:(UITableViewCell *)cell isNew:(BOOL)isNew
{
    CGRect expectedCleaningLabelRect = CGRectMake(10, 1, 200 , 30);    
    // display label control in table view
    UILabel *expectedCleaningLabel;
    if (isNew == TRUE) {
        expectedCleaningLabel = [[UILabel alloc] initWithFrame:expectedCleaningLabelRect];
    } else {
        expectedCleaningLabel = (UILabel *)[cell.contentView viewWithTag:tagLabel];
    }
    
    expectedCleaningLabel.textAlignment = UITextAlignmentLeft;
    expectedCleaningLabel.font = [UIFont boldSystemFontOfSize:12];
    expectedCleaningLabel.text = self.expectedCleaningTime;
    expectedCleaningLabel.tag = tagLabel;
    UIImage *img=[UIImage imageNamed:@"tab_small.png"];
    expectedCleaningLabel.textColor=[UIColor colorWithPatternImage:img];
    
    [expectedCleaningLabel setBackgroundColor:[UIColor clearColor]];
    if (isNew == TRUE) {
        [cell.contentView addSubview:expectedCleaningLabel];        
    }
    
    //label 2 of this cell
    CGRect expectedCleaningValueRect = CGRectMake(160, 1, 135 , 30); 
    
    UILabel *expectedCleaningTimeValue; 
    if (isNew == TRUE) {
        expectedCleaningTimeValue=[[UILabel alloc] initWithFrame:expectedCleaningValueRect];   
    } else {
        expectedCleaningTimeValue = (UILabel *)[cell.contentView viewWithTag:tagText];
    }
    
    expectedCleaningTimeValue.textAlignment = UITextAlignmentRight;
    expectedCleaningTimeValue.font = [UIFont boldSystemFontOfSize:12];
    expectedCleaningTimeValue.tag = tagText;
    // set expectedCleaningTimeValue from dataModel
    [expectedCleaningTimeValue setText:[self timeDisplay:60*raDataModel.roomAssignmentRoomExpectedCleaningTime]];
    
    [expectedCleaningTimeValue setBackgroundColor:[UIColor clearColor]];    
    UIImageView* imageView = (UIImageView *)[cell.contentView viewWithTag:tagImage];
    [imageView removeFromSuperview];
   
    if (isNew == TRUE) {
        [cell.contentView addSubview:expectedCleaningTimeValue];        
    }
}



//@ config actual cleaning time row 
- (void)setupActualCleaningTimeCell:(UITableViewCell *)cell isNew:(BOOL)isNew
{
    CGRect actualCleaningTimeLabelRect = CGRectMake(10, 1, 150 , 30);    
    // display label control in table view
    
    UILabel *actualCleaningLabel;
    
    if (isNew == TRUE) {
        actualCleaningLabel = [[UILabel alloc] initWithFrame:actualCleaningTimeLabelRect];
    } else {
        actualCleaningLabel = (UILabel *)[cell.contentView viewWithTag:tagLabel];
    }
    
    actualCleaningLabel.textAlignment = UITextAlignmentLeft;
    actualCleaningLabel.font = [UIFont boldSystemFontOfSize:12];
    actualCleaningLabel.text = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_actual_cleaning_time];
    actualCleaningLabel.tag = tagLabel;
    
    UIImage *img=[UIImage imageNamed:@"tab_small.png"];
    actualCleaningLabel.textColor=[UIColor colorWithPatternImage:img];
    
    [actualCleaningLabel setBackgroundColor:[UIColor clearColor]];
    if (isNew == TRUE) {
        [cell.contentView addSubview:actualCleaningLabel];
        
    }
    
    
    //label 2 of this cell
    
    
    CGRect actualCleaningValueRect = CGRectMake(160, 1, 135 , 30);
    UILabel *actualCleaningValue;
    
    if (isNew == TRUE) {
        actualCleaningValue=[[UILabel alloc] initWithFrame:actualCleaningValueRect];
    } else {
        actualCleaningValue = (UILabel *)[cell.contentView viewWithTag:tagText];
        [actualCleaningValue setFrame:actualCleaningValueRect];
    }
    
    actualCleaningValue.textAlignment = UITextAlignmentRight;
    actualCleaningValue.font = [UIFont boldSystemFontOfSize:12];
    actualCleaningValue.lineBreakMode = UILineBreakModeWordWrap;
    actualCleaningValue.tag = tagText;
    [actualCleaningValue setTextColor:[UIColor greenColor]];
    [actualCleaningValue setBackgroundColor:[UIColor clearColor]];
    
    //Hao Tran - Remove for can't build
    /*
    if([self.roomRecord.rrec_Cleaning_Duration intValue] > 0){
        //durationTime = [self.roomRecord.rrec_Cleaning_Duration intValue];
    }        
    
    [actualCleaningValue setText:[self timeDisplayWithSecond:[self.roomRecord.rrec_Cleaning_Duration intValue]]];
    
    if(timeLeftCleaningStatus<0){
        [actualCleaningValue setTextColor:[UIColor redColor]];
    }

    UIImageView* imageView = (UIImageView *)[cell.contentView viewWithTag:tagImage];
    [imageView removeFromSuperview];
    
    if (isNew == TRUE) {
        [cell.contentView addSubview:actualCleaningValue];
    }
     */
}

//@ config cleaning status cell
- (void)setupCleaningStatusCell:(UITableViewCell *)cell isNew:(BOOL)isNew {
    CGRect roomStatusLabelRect = CGRectMake(10, 1, 150 , 30);    
    // display label control in table view
    
    UILabel *cleaningStatusLabel;
    
    if (isNew == TRUE) {
        cleaningStatusLabel = [[UILabel alloc] initWithFrame:roomStatusLabelRect];
    } else {
        cleaningStatusLabel = (UILabel *)[cell.contentView viewWithTag:tagLabel];
    }
    cleaningStatusLabel.textAlignment = UITextAlignmentLeft;
    cleaningStatusLabel.font = [UIFont boldSystemFontOfSize:12];
    cleaningStatusLabel.text = self.cleaningStatus;
    cleaningStatusLabel.tag = tagLabel;
    UIImage *img=[UIImage imageNamed:@"tab_small.png"];
    cleaningStatusLabel.textColor=[UIColor colorWithPatternImage:img];
    
    [cleaningStatusLabel setBackgroundColor:[UIColor clearColor]];
    if (isNew == TRUE) {
        [cell.contentView addSubview:cleaningStatusLabel];
    }
    
    //label 2 of this cell
    CGRect cleaningStatusValueRect = CGRectMake(190, 10, 105 , 30); 
    UILabel *cleanigStatusValue;
    
    // align  text in 2 case: user and super user.
    if (isNew == TRUE) {
        cleanigStatusValue=[[UILabel alloc] initWithFrame:cleaningStatusValueRect]; 
    } else {
        cleanigStatusValue = (UILabel *)[cell.contentView viewWithTag:tagText];
        [cleanigStatusValue setFrame:cleaningStatusValueRect];
    }        
       
    cleanigStatusValue.textAlignment = UITextAlignmentRight;
    cleanigStatusValue.font = [UIFont boldSystemFontOfSize:12];
    
    //set cleaningStatusValue from dataModel
    CleaningStatusModelV2 *cleaningStatusModel = [[CleaningStatusModelV2 alloc] init];
    cleaningStatusModel.cstat_Id = raDataModel.roomAssignmentRoomCleaningStatusId;
    [[RoomManagerV2 sharedRoomManager] loadCleaningStatus:cleaningStatusModel];

    if ([[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString:[NSString stringWithFormat:@"%@",ENGLISH_LANGUAGE]]) {
        [cleanigStatusValue setText: cleaningStatusModel.cstat_Name];
        } else {
        [cleanigStatusValue setText: cleaningStatusModel.cstat_Language];
    }
    cleanigStatusValue.tag=tagText;
       
    [self addAccessoryButton: cell];
   /*****************************************/    
    
    UIImageView *cleaningStatusImageView;
    if (isNew == TRUE) {
        cleaningStatusImageView = [[UIImageView alloc] initWithFrame:CGRectMake(160, 1, 30, 30)];
    } else {
        cleaningStatusImageView = (UIImageView *)[cell.contentView viewWithTag:tagImage];
        [cleaningStatusImageView setFrame:CGRectMake(160, 1, 30, 30)];
    }
    if(raDataModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_PENDING_SECOND_TIME){
        [cleaningStatusImageView setFrame:CGRectMake(160, 1, 60, 31)];
    }else{
        [cleaningStatusImageView setFrame:CGRectMake(160, 1, 31, 31)];
    }
    cleaningStatusImageView.tag = tagImage;
    
    RoomServiceLaterModelV2 *roomServiceLater = [[RoomServiceLaterModelV2 alloc] init];
    roomServiceLater.rsl_RecordId = self.roomRecord.rrec_Id;
    if(raDataModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_SERVICE_LATER){
        // get latest room service later
        [[RoomManagerV2 sharedRoomManager] loadRoomServiceLaterDataByRecordId:roomServiceLater];
    }
    
    [cleaningStatusImageView setImage:[UIImage imageWithData:cleaningStatusModel.cstat_image]];
        
    if(self.raDataModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_SERVICE_LATER) {
        NSDateFormatter *_timeFomarter = [[NSDateFormatter alloc] init];
        [_timeFomarter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];

        NSString *assignDate = @"";
        if([roomServiceLater.rsl_Time length] > 0) {
            assignDate = roomServiceLater.rsl_Time;
        } else {
            assignDate = raDataModel.roomAssignment_AssignedDate;
        }
        
        NSDate *_serviceLaterDate = [_timeFomarter dateFromString:assignDate];
        
        [_timeFomarter setDateFormat:@"HH:mm"];
        NSString *_strServiceLaterDate = [_timeFomarter stringFromDate:_serviceLaterDate];
        if(_strServiceLaterDate == nil) _strServiceLaterDate = @"";
        
        if ([[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString:(NSString *)ENGLISH_LANGUAGE]) {
            [cleanigStatusValue setText: [NSString stringWithFormat:@"%@ %@",cleaningStatusModel.cstat_Name,_strServiceLaterDate]];            
        } else{
            [cleanigStatusValue setText: [NSString stringWithFormat:@"%@ %@",cleaningStatusModel.cstat_Language,_strServiceLaterDate]];       
        }
    } else if(self.raDataModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_DECLINE_SERVICE) {
        if(declineServiceDate != nil && [declineServiceDate length] > 0) {
            if ([[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString:(NSString *)ENGLISH_LANGUAGE]) {
                [cleanigStatusValue setText: [NSString stringWithFormat:@"%@ %@",cleaningStatusModel.cstat_Name, declineServiceDate]];       
            } else {
                [cleanigStatusValue setText: [NSString stringWithFormat:@"%@ %@",cleaningStatusModel.cstat_Language,  declineServiceDate]];
            }
        } else {
            if ([[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString:(NSString *)ENGLISH_LANGUAGE]) {
                [cleanigStatusValue setText:cleaningStatusModel.cstat_Name];
            } else{
                [cleanigStatusValue setText:cleaningStatusModel.cstat_Language];
            }
        }
    } else{
        if ([[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString:(NSString *)ENGLISH_LANGUAGE]) {
            [cleanigStatusValue setText: cleaningStatusModel.cstat_Name];
        } else{
            [cleanigStatusValue setText: cleaningStatusModel.cstat_Language];
        }
    }
    
    /********* resize frame of image and label*************/
    CGSize size = [cleanigStatusValue.text sizeWithFont:cleaningStatusLabel.font];
    CGFloat x = xOfSccessory - size.width-space2ElementCell;
    [cleanigStatusValue setFrame:CGRectMake(x, 10, size.width, size.height)];
    [cleaningStatusImageView setFrame:CGRectMake(x-cleaningStatusImageView.frame.size.width, cleaningStatusImageView.frame.origin.y, cleaningStatusImageView.frame.size.width, cleaningStatusImageView.frame.size.height)];
    /********* end resize frame of image and label*************/

        
    [cleanigStatusValue setBackgroundColor:[UIColor clearColor]];
    if (isNew == TRUE) {
        [cell.contentView addSubview:cleanigStatusValue];
        [cell.contentView addSubview:cleaningStatusImageView];
    }
}

// @ config room status cell
- (void)roomStatusCell:(UITableViewCell *)cell isNew:(BOOL)isNew {
    CGRect roomStatusLabelRect = CGRectMake(10, 1, 150 , 30);    
    // display label control in table view
    
    UILabel *roomStatusLabel;
    
    if (isNew == TRUE) {
        roomStatusLabel = [[UILabel alloc] initWithFrame:roomStatusLabelRect];
    } else {
        roomStatusLabel = (UILabel *)[cell.contentView viewWithTag:tagLabel];
    }
    roomStatusLabel.textAlignment = UITextAlignmentLeft;
    roomStatusLabel.font = [UIFont boldSystemFontOfSize:12];
    roomStatusLabel.text = self.roomStatus;
    roomStatusLabel.tag = tagLabel;
    UIImage *img=[UIImage imageNamed:@"tab_small.png"];
    roomStatusLabel.textColor=[UIColor colorWithPatternImage:img];
    
    [roomStatusLabel setBackgroundColor:[UIColor clearColor]];
    if (isNew == TRUE) {
        [cell.contentView addSubview:roomStatusLabel];
        
    }
    
    //label 2 of this cell
    CGRect roomStatusValueRect = CGRectMake(190, 10, 105 , 30); 
    UILabel *roomStatusValue;
    // align  text in 2 case: user and super user.
    
        if (isNew == TRUE) {
            roomStatusValue=[[UILabel alloc] initWithFrame:roomStatusValueRect]; 
        } else {
            roomStatusValue = (UILabel *)[cell.contentView viewWithTag:tagText];
            [roomStatusValue setFrame:roomStatusValueRect];
        }        
  
    
    roomStatusValue.textAlignment = UITextAlignmentRight;
    roomStatusValue.font = [UIFont boldSystemFontOfSize:FONT_SIZE_ROOM_STATUS];
    
    //set cleaningStatusValue from dataModel
    RoomStatusModelV2 *roomStatusModel=[[RoomStatusModelV2 alloc] init];
    roomStatusModel.rstat_Id = raDataModel.roomAssignmentRoomStatusId;
    
    [[RoomManagerV2 sharedRoomManager] loadRoomStatusData:roomStatusModel];

    if ([[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString:[NSString stringWithFormat:@"%@",ENGLISH_LANGUAGE]]) {
        [roomStatusValue setText:roomStatusModel.rstat_Name];

    } else {
        [roomStatusValue setText:roomStatusModel.rstat_Lang];
    }    
    [self addAccessoryButton:cell];


    roomStatusValue.tag=tagText;
    UIImageView *imageRoomStatusView ;
    imageRoomStatusView = (UIImageView *)[cell.contentView viewWithTag:tagImage];
    if (imageRoomStatusView == nil) {
        imageRoomStatusView = [[UIImageView alloc] initWithFrame:CGRectMake(160, 1, 30, 30)];
        imageRoomStatusView.tag = tagImage;
        [cell.contentView addSubview:imageRoomStatusView];
    }
    
    [imageRoomStatusView setImage:[UIImage imageWithData:roomStatusModel.rstat_Image]];
    [roomStatusValue setText: roomStatusModel.rstat_Name];

    
    /********* resize frame of image and label*************/
    CGSize size = [roomStatusModel.rstat_Name sizeWithFont:roomStatusValue.font];
    CGFloat x = xOfSccessory - size.width-space2ElementCell;
    [roomStatusValue setFrame:CGRectMake(x, 5, size.width, size.height)];
    [imageRoomStatusView setFrame:CGRectMake(x-imageRoomStatusView.frame.size.width, imageRoomStatusView.frame.origin.y, imageRoomStatusView.frame.size.width, imageRoomStatusView.frame.size.height)];
    /********* end resize frame of image and label*************/

    
    [roomStatusValue setBackgroundColor:[UIColor clearColor]];
    if (isNew == TRUE) {
        [cell.contentView addSubview:roomStatusValue];
        [cell.contentView addSubview:imageRoomStatusView];
    }
}

//@ config room remark cell
- (void)setupRemarkCell:(UITableViewCell *)cell isNew:(BOOL)isNew
{
    CGRect remarkLabelRect = CGRectMake(10, 1, 150 , 30);    
    // display label control in table view
    
    UILabel *remarkLabel;
    
    if (isNew == TRUE) {
        remarkLabel = [[UILabel alloc] initWithFrame:remarkLabelRect];
    } else {
        remarkLabel = (UILabel *)[cell.contentView viewWithTag:tagLabel];
    }
    remarkLabel.textAlignment = UITextAlignmentLeft;
    remarkLabel.font = [UIFont boldSystemFontOfSize:12];
    remarkLabel.text = self.remark;
    remarkLabel.tag = tagLabel;
    UIImage *img=[UIImage imageNamed:@"tab_small.png"];
    remarkLabel.textColor=[UIColor colorWithPatternImage:img];
    
    [remarkLabel setBackgroundColor:[UIColor clearColor]];
    if (isNew == TRUE) {
        [cell.contentView addSubview:remarkLabel];        
    }
    
    //label 2 of this cell
    CGRect remarkValueRect = CGRectMake(160, 1, 105 , 30);
    UILabel *remarkValue;
    // align  text in 2 case: user and super user.
    
        if (isNew == TRUE) {
            remarkValue=[[UILabel alloc] initWithFrame:remarkValueRect]; 
        } else {
            remarkValue = (UILabel *)[cell.contentView viewWithTag:tagText];
        }        
   
    
    remarkValue.textAlignment = UITextAlignmentRight;
    remarkValue.font = [UIFont boldSystemFontOfSize:12];
    remarkValue.text = roomRecord.rrec_Remark;
    if ([[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString:[NSString stringWithFormat:@"%@",ENGLISH_LANGUAGE]]) {
    }
    [self addAccessoryButton:cell];

    remarkValue.tag=tagText;
    [remarkValue setBackgroundColor:[UIColor clearColor]];
    

    UIImageView* imageView = (UIImageView *)[cell.contentView viewWithTag:tagImage];
    [imageView removeFromSuperview];

    if (isNew == TRUE) {
        [cell.contentView addSubview:remarkValue];
        
    }
}


/****************************** implement cell *************************************/

-(UITableViewCell*)tableView:(UITableView *)atableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //NSLog(@"bugggggg %d",dataModel.room_CleaningStatusId);
    // init layout for tableview
    BOOL isNew = FALSE;
    
    static NSString *CellTableIdentifier = @"Cell";    
    static NSString *CellCleaningStatus = @"CleaningStatus";
    
    UITableViewCell *cell;
    if (indexPath.row == 3) {
        cell = [atableView dequeueReusableCellWithIdentifier:CellCleaningStatus];
    } else
        cell = [atableView dequeueReusableCellWithIdentifier:CellTableIdentifier];
    
    if(cell == nil){
        if (indexPath.row == 3) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellCleaningStatus] ;
        } else {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellTableIdentifier] ;
        }
        isNew = TRUE;
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    /**********************************************************/
        if(self.raDataModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_COMPLETED){
            /********* room is completed *********/
            /********* room is not inspected******/
            switch (indexPath.row) {
                case 0:{   
                    [self setupExpectedCleaningCell:cell isNew:isNew];
                } 
                    break;
                    
                case 1:{
                    [self setupActualCleaningTimeCell:cell isNew:isNew];
                }
                    break;
                    
                case 2:{
                    [self setupAdditionalCell:cell isNew:isNew];
                } 
                    break;
                    
                case 3:{ 
                    [self setupCleaningStatusCell:cell isNew:isNew]; 
                } 
                    break;
                    
                case 4:{
                    [self roomStatusCell:cell isNew:isNew];
                } 
                    break;
                    
                case 5:{
                    [self setupRemarkCell:cell isNew:isNew];
                } 
                    break;
                
            /********* end room is not inspected******/
          }
        } else {
            /********* room is not completed *********/
            switch (indexPath.row) {
                case 0:{   
                    [self setupExpectedCleaningCell:cell isNew:isNew];
                }
                    break;
                case 1:{
                    [self setupAdditionalCell:cell isNew:isNew];
                }
                    break;
                case 2:{
                    [self setupCleaningStatusCell:cell isNew:isNew];
                } break;
                    
                case 3:{
                    [self roomStatusCell:cell isNew:isNew];
                }break;
                    
                case 4:{
                    [self setupRemarkCell:cell isNew:isNew];
                } break;
            }
            /********* end room is not completed *********/
        }
     
  
    /**********************************************************/
    [cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
    
    //set user interaction for find feature
    if (userCanInteraction == NO) {
        if(indexPath.row == 2) {
            if(raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OD) {
                [cell setUserInteractionEnabled:TRUE];
            } else {
                [cell setUserInteractionEnabled:FALSE];
            }
        } else
            [cell setUserInteractionEnabled:FALSE];
    } else {
        [cell setUserInteractionEnabled:YES];
    }
    
    return cell;
}

-(void)tableView:(UITableView *)atableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger section = indexPath.section;
    if(section != 0) return;
    
    if(countTimeStatus == STOP) return;
        
    /********* room is not completed *********/
    if(indexPath.row == 2)  [self showCleaningStatus];
    if(indexPath.row == 3)  [self showRoomStatus];
    if(indexPath.row == 4) {
        if(self.roomRemarkModel == nil) {
            self.roomRemarkModel = [[RoomRemarkModelV2 alloc] init];
            self.roomRemarkModel.rm_clean_pause_time = @"0";
        }
      [self showRemark];   
    }
    /********* end room is not completed *********/
  
     UITableViewCell *_cell = [atableView cellForRowAtIndexPath:indexPath];
     [_cell setSelected:NO animated:NO];
    /**********************************************************/
}

-(void)showRoomStatus{    
    if(self.raDataModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_COMPLETED) return;
    [delegate showPopupRoomStatus:self];
}

- (void)showCleaningStatus {
    if(self.raDataModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_COMPLETED) return;
    [delegate showPopupCleaningStatus:self];
}

-(void)showRemark{
    if(self.raDataModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_COMPLETED) return;
    if(countTimeStatus == NOT_START ) return;
    if(self.raDataModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_PENDING_FIRST_TIME ||
       self.self.raDataModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_PENDING_SECOND_TIME) return;

    // load last remark :
    RemarkViewV2 *remarkView = [[RemarkViewV2 alloc] initWithNibName:
                                NSStringFromClass([RemarkViewV2 class]) bundle:nil];
    remarkView.textinRemark = roomRemarkModel.rm_content;
    
    [remarkView setDelegate:self];
    [self.superController.navigationController pushViewController:remarkView animated:YES];
}

-(void)showPauseRemark{
    if(self.raDataModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_COMPLETED) return;
    if(countTimeStatus == NOT_START || self.raDataModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_START) return;
    if(self.raDataModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_PENDING_FIRST_TIME ||
       self.raDataModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_PENDING_SECOND_TIME) return;
    
    RemarkViewV2 *remarkView = [[RemarkViewV2 alloc] initWithNibName:
                                NSStringFromClass([RemarkViewV2 class]) bundle:nil];
    [remarkView setDelegate:self];
    [self.superController.navigationController pushViewController:remarkView animated:YES];
}


-(NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    return indexPath;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 33.0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 150.0;
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    //set user interaction for find feature
    if (userCanInteraction == NO) {
        [listButtonView setUserInteractionEnabled:NO];
    } else {
        [listButtonView setUserInteractionEnabled:YES];
    }
    
    return self.listButtonView;
}

#pragma mark - 
#pragma mark Save data
-(void)saveData{
    isSaved = YES;
    if(raDataModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_START){
        isSaved = NO;
    }
   
    // @ show loading data .
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getMsgSaveMessage]];
    [self.view.superview addSubview:HUD];
    [HUD show:YES];  

    [self saveDataWhenChangeStatus]; // save DB
    
//    raDataModelCompare.roomAssignmentRoomCleaningStatusId = raDataModel.roomAssignmentRoomCleaningStatusId;
//    raDataModelCompare.roomAssignmentRoomStatusId = raDataModel.roomAssignmentRoomStatusId;
//    
//    dataModel.room_PostStatus = POST_STATUS_SAVED_UNPOSTED;
//    [[RoomManagerV2 sharedRoomManager] updateRoomModel:dataModel];
//    
//        
//    self.raDataModel.roomAssignment_PostStatus = POST_STATUS_SAVED_UNPOSTED;    
//    [[RoomManagerV2 sharedRoomManager] update:self.raDataModel];

    // Post Server
    SyncManagerV2 *synManager = [[SyncManagerV2 alloc] init];

    if (isFindByRoomView == FALSE) { // for Maid
        [synManager postRoomAssignmentWithUserID:[[UserManagerV2 sharedUserManager] currentUser].userId 
                                      AndRaModel:self.raDataModel];
    } else { // for Supervisor
        [synManager postRoomCleaningtoServerWithUSerID:[[UserManagerV2 sharedUserManager] currentUser].userId AndRoomAssigmentModel:self.raDataModel];
    }
    
    //hide saving data.
    [self performSelector:@selector(hiddenHUDAfterSaved:) withObject:HUD afterDelay:0.5];
}

-(void)saveDataWhenChangeStatus{
    self.raDataModel.roomAssignment_PostStatus = POST_STATUS_SAVED_UNPOSTED;    
    [[RoomManagerV2 sharedRoomManager] update:self.raDataModel];
    
    dataModel.room_PostStatus = POST_STATUS_SAVED_UNPOSTED;
    [[RoomManagerV2 sharedRoomManager] updateRoomModel:dataModel];
}

-(BOOL) isNotSavedAndExit
{
    if([self checkIsSave]){
        
        if(self.raDataModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_COMPLETED
           || self.raDataModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_PAUSE){
            // when not post to server
            if(self.raDataModel.roomAssignment_PostStatus == POST_STATUS_SAVED_UNPOSTED){
                UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ALERT_SAVEROOMDETAIL] delegate:nil cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_OK] otherButtonTitles:nil];
                [alert show];
                
                return YES;
                
            }

        }        
        return NO;
    } else{
        
        if(!superController.isRoomDetailsActive){
            //show please complete the room
            UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_complete_room_to_leave] delegate:nil cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_OK] otherButtonTitles:nil];
            [alert show];
            
            return YES;
        }
        
        if(raDataModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_SERVICE_LATER ||
           raDataModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_DECLINE_SERVICE){
            UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ALERT_SAVEROOMDETAIL] delegate:nil cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_OK] otherButtonTitles:nil];
            [alert show];
            return YES;

        }
               // in case room is stop room must save
        if(countTimeStatus == STOP){
            
            [self insertRoomRecordDetail];
            [self saveDataWhenChangeStatus];
            return NO;
        }
        
        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObject:[UIImage imageNamed:@"yes_bt.png"]] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:msgSaveRoomDetail delegate:self cancelButtonTitle:msgbtnOk otherButtonTitles:nil, nil]; 
        alert.delegate = self;
        alert.tag = 10000;
        [alert show];
        return YES;

    }
}


-(BOOL)checkIsSave{
    return isSaved;
}

//..... is rooom inspected
-(BOOL)isRoomInspected{
    if(raDataModel.roomAssignmentRoomInspectionStatusId == ENUM_INSPECTION_COMPLETED_PASS||
       raDataModel.roomAssignmentRoomInspectionStatusId == ENUM_INSPECTION_COMPLETED_FAIL){
        return YES;
    } else{
        return  NO;
    }
}



#pragma mark -
#pragma mark ButtonCLG Methods
- (IBAction)butCheckListPressed:(id)sender {
    CheckListViewV2 *checklistView = [[CheckListViewV2 alloc] initWithNibName:NSStringFromClass([CheckListViewV2 class]) bundle:nil];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK] style:UIBarButtonItemStyleBordered target:nil action:nil];
    self.superController.navigationItem.backBarButtonItem = backButton;
    
    [self.superController.navigationController pushViewController:checklistView animated:YES];
}

- (IBAction)butActionPressed:(id)sender {
    NSDictionary *dic = [[LanguageManagerV2 sharedLanguageManager] getCurrentDictionaryLanguage];
    NSString *action, *c, *g, *cancel, *ec;
    action = [dic valueForKey:[NSString stringWithFormat:@"%@", L_RA_SELECT_ACTION]];
    
    c = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_Counts];
//#warning modify for conrad
//    g = [dic valueForKey:[NSString stringWithFormat:@"%@", L_SetupStandard]];
    g = [dic valueForKey:[NSString stringWithFormat:@"%@", L_Guideline]];
    ec = [dic valueForKey:[NSString stringWithFormat:@"%@", L_ENGINEERING_CASE]];
    
    cancel = [dic valueForKey:[NSString stringWithFormat:@"%@", L_CANCEL]];
    
    if (action == nil) {
        action = @"Action";
    }
   
    if (c == nil) {
        c= @"Posting";
    }
    
    if (g == nil) {
        g = @"Guideline";
//#warning modify for Conrad
//        g = @"Setup Standard";
    }
    if (ec == nil) {
        ec = @"Engineering";
    }
    
    if(raDataModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_COMPLETED){
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:action delegate:self cancelButtonTitle:cancel destructiveButtonTitle:nil otherButtonTitles:c, ec, g, nil];
        [actionSheet showInView:self.view.superview.superview];
    } 
    else if (raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OOO || raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OOS) 
    {
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:action delegate:self cancelButtonTitle:cancel destructiveButtonTitle:nil otherButtonTitles: c, ec, g, nil];
        [actionSheet showInView:self.view.superview.superview];
    } 
    else {
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:action delegate:self cancelButtonTitle:cancel destructiveButtonTitle:nil otherButtonTitles:c, ec, g, nil];
        [actionSheet showInView:self.view.superview.superview];
    }
}


-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return TRUE;
}

#pragma mark - UIActionSheet Delegate Methods
-(void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex{
        //maid
        if(self.raDataModel.roomAssignmentRoomCleaningStatusId== ENUM_CLEANING_STATUS_COMPLETED){
            switch (buttonIndex) {
                    
                case 0: // Posting
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"AddCLGWithSelectedIndex" object:@"1"];
                    
                    break;
                case 1:
                    [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationAddEngineeringView] object:nil];
                    break;
                case 2: // Guideline
                    [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@",notificationGuidelineView] object:nil];
                    break;
                    
                default:
                    break;
            }

        }
        else
        {
            switch (buttonIndex) {
                    
                case 0: // Posting
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"AddCLGWithSelectedIndex" object:@"1"];
                    
                    break;
                case 1:
                    [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationAddEngineeringView] object:nil];
                    break;
                case 2: // Guideline
                    [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@",notificationGuidelineView] object:nil];
                    break;
                    
                default:
                    break;
            }

        }
        
    
    
}

#pragma mark - set captions view
-(void)setCaptionsView {
    NSDictionary *dic = [[LanguageManagerV2 sharedLanguageManager] getCurrentDictionaryLanguage];
    NSString *action = [dic valueForKey:[NSString stringWithFormat:@"%@", L_BTN_ACTION]];
    if (action == nil) {
        action = @"Action";
    }
    [btnAction setTitle:action forState:UIControlStateNormal];
    
    self.expectedCleaningTime = [dic valueForKey:[NSString stringWithFormat:@"%@", L_EXPECTED_CLEANING_TIME]];
    self.guestPref = [dic valueForKey:[NSString stringWithFormat:@"%@", L_GUEST_REFERENCE]];
    self.additioJob = [dic valueForKey:[NSString stringWithFormat:@"%@", L_ADDITIONAL_JOB]];
    self.cleaningStatus = [dic valueForKey:[NSString stringWithFormat:@"%@", L_CLEANING_STATUS]];
    
    
    msgbtnOk = [[LanguageManagerV2 sharedLanguageManager] getOK];
    msgSaveRoomDetail = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_complete_room_to_leave];

}

#pragma mark
#pragma mark Timer counting 

-(void)showAlert{
    
    AlertAdvancedSearch *alertView=[[AlertAdvancedSearch alloc] init];
    [alertView alertAdvancedSearchInitWithFrame:CGRectZero title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] content:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_msgalert_confirmblockroom]];
   
    alertView.delegate = self;
    [alertView show];

}

-(void)showAlertWithTitle:(NSString*)title content:(NSString*)content andtag:(NSInteger)tag{
    
    
    NSString *msgbtnNo = [[LanguageManagerV2 sharedLanguageManager] getNo];
    NSString *msgbtnYes = [[LanguageManagerV2 sharedLanguageManager] getYes];

    
    UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:content delegate:self cancelButtonTitle:msgbtnYes otherButtonTitles:msgbtnNo, nil]; 
    alert.delegate = self;
    alert.tag = tag;
    [alert show];
    
     
}

#pragma mark
#pragma mark handle room record detail
-(BOOL)isDurationLessMin{
    if((durationTime) < INTERVAL_START_STOP_RECORD_DETAIL) {
        return YES;
    } else{
        return NO;
    }
}

-(BOOL)isMaxRoomRecordDetail{
    RoomRecordDetailModelV2 *roomRecordDetail = [[RoomRecordDetailModelV2 alloc] init];
    roomRecordDetail.recd_Ra_Id = [TasksManagerV2 getCurrentRoomAssignment];
    roomRecordDetail.recd_User_Id = [UserManagerV2 sharedUserManager].currentUser.userId;
    [[RoomManagerV2 sharedRoomManager] loadRoomRecordDataDetailByRoomAssignmentIdAndUserId:roomRecordDetail];
    if([[RoomManagerV2 sharedRoomManager] numberOfRoomRecordDetails:roomRecordDetail] >=MAX_ROOM_RECORD_DETAIL - 1){
        return YES;
    } else{
        return NO;
    }
}

-(void)insertRoomRecordDetail{
    roomRecordDetailModel.recd_Ra_Id = [TasksManagerV2 getCurrentRoomAssignment];
    roomRecordDetailModel.recd_User_Id = [UserManagerV2 sharedUserManager].currentUser.userId;
    [[RoomManagerV2 sharedRoomManager] insertRoomRecordDetailData:roomRecordDetailModel];            

}

-(void)updateRoomRecordDetail{
    NSDate *now = [[NSDate alloc] init];
    roomRecordDetailModel.recd_Ra_Id = [TasksManagerV2 getCurrentRoomAssignment];
    roomRecordDetailModel.recd_User_Id = [UserManagerV2 sharedUserManager].currentUser.userId;
    
    roomRecordDetailModel.recd_Stop_Date = [timeFomarter stringFromDate:now];
    [[RoomManagerV2 sharedRoomManager] updateRoomRecordDetailData:roomRecordDetailModel];
}

// start and pause timer
- (IBAction)startPauseCountTimerDidSelect:(id)sender {

    if(raDataModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_SERVICE_LATER && countTimeStatus >=0){
        [self.superController.navigationController popViewControllerAnimated:YES];
        return;
    }
    
    if(raDataModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_DECLINE_SERVICE && countTimeStatus >=0){
        [self.superController.navigationController popViewControllerAnimated:YES];
        return;
    }

    
    if (countTimeStatus == STOP || countTimeStatus == NOT_START) {
        //check app is syncing
        if ([[HomeViewV2 shareHomeView] isSyncing] == YES) {
            CustomAlertViewV2 *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:nil message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_please_wait_syncing_progress] delegate:nil cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_OK] otherButtonTitles:nil];
            [alert show];
            return;
        }
    }

    
    isSaved = NO;
    if(timer == nil){
        timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self 
                                               selector:@selector(updateTimer) userInfo:nil repeats:YES];
         }
    switch (countTimeStatus) {
        case STOP:{
            
            // transition from pause to start.
            // start
            if([LogFileManager isLogConsole])
            {
                NSLog(@"isMustResetTimer %d" ,isMustResetTimer);
            }
            
            if(isMustResetTimer){
                isMustResetTimer = NO;
                durationTime = 0;
                timeLeftCleaningStatus = raDataModel.roomAssignmentRoomExpectedCleaningTime*60;
            }
          
            
            NSDate *now = [[NSDate alloc] init];            
            NSTimeInterval timeDiff = [now timeIntervalSinceDate:[timeFomarter dateFromString:roomRecordDetailModel.recd_Stop_Date]];
            countTimeStatus = START;
            raDataModel.roomAssignmentRoomCleaningStatusId = ENUM_CLEANING_STATUS_START;
            if([[RoomManagerV2 sharedRoomManager] updateRoomHaveBeenRemove:[UserManagerV2 sharedUserManager].currentUser.userId AndRaModel:raDataModel]== RESPONSE_STATUS_ROOM_HAVE_BEEN_DELETED){
                UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Error- This room is removed" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                [alert show];
                isSaved = YES;
                countTimeStatus=STOP;
                return;
                
            }
            
            if(timeDiff < 60) {
                
               // raDataModel.roomAssignmentRoomCleaningStatusId = ENUM_CLEANING_STATUS_START;  
                
                [self refreshLayoutButtons]; 
                [countTimeButton setBackgroundImage:[UIImage imageNamed:IMAGE_STOP_ON] forState:UIControlStateNormal];
                [countTimeButton setBackgroundImage:[UIImage imageNamed:IMAGE_STOP_ON] forState:UIControlStateHighlighted];
                [countTimeButton setBackgroundImage:[UIImage imageNamed:IMAGE_STOP_GRAY] forState:UIControlStateDisabled];
                
                [timerButon setBackgroundImage:[UIImage imageNamed:IMAGE_TIMER_PLAY] forState:UIControlStateNormal];
                [tableView reloadData];

                return;
            } 
            
               
            timeLeftCleaningStatus = raDataModel.roomAssignmentRoomExpectedCleaningTime*60;

            [self insertRoomRecordDetail]; 
            
            // assign new value for this state
            roomRecordDetailModel = [[RoomRecordDetailModelV2 alloc] init];
            roomRecordDetailModel.recd_Ra_Id = [TasksManagerV2 getCurrentRoomAssignment];
            roomRecordDetailModel.recd_User_Id = [UserManagerV2 sharedUserManager].currentUser.userId;
            roomRecordDetailModel.recd_Start_Date = [timeFomarter stringFromDate:now];
            
            roomRecord.rrec_Cleaning_Start_Time = [timeFomarter stringFromDate:now];  //... restart time
            roomRecord.rrec_PostStatus = POST_STATUS_SAVED_UNPOSTED;
           
//            int DurationEx = durationTime + [roomRecord.rrec_Cleaning_Duration intValue];
//            roomRecord.rrec_Cleaning_Duration = [NSString stringWithFormat:@"%d",DurationEx];
            //[[RoomManagerV2 sharedRoomManager] updateRoomRecordData:roomRecord];
            
          //  raDataModel.roomAssignmentRoomCleaningStatusId = ENUM_CLEANING_STATUS_START;            
            [self saveDataWhenChangeStatus];
            
            durationTime = 0;  // reset counter
            
            /************layout display***************/
            [self refreshLayoutButtons];     
            [finishTimeButton setUserInteractionEnabled:YES];
            [countTimeButton setBackgroundImage:[UIImage imageNamed:IMAGE_STOP_ON] forState:UIControlStateNormal];
            [countTimeButton setBackgroundImage:[UIImage imageNamed:IMAGE_STOP_ON] forState:UIControlStateHighlighted];
            [countTimeButton setBackgroundImage:[UIImage imageNamed:IMAGE_STOP_GRAY] forState:UIControlStateDisabled];
            
            [timerButon setBackgroundImage:[UIImage imageNamed:IMAGE_TIMER_PLAY] forState:UIControlStateNormal];
            
             
            
            [tableView reloadData];

        }
            break;
         
        case START:{
            if([self isDurationLessMin]){
                isMustResetTimer = FALSE;
            }else
            {
                isMustResetTimer = TRUE;
            }
//                
//                CustomAlertViewV2 *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_room_detail_cancel_cleaning] message:nil delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_YES] otherButtonTitles:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_NO], nil];
//                alert.tag = tagAlertCancelCleaningLessThan1Minute;
//                [alert show];
//                return;  
//            }
            [self showRemark];

            
            if([self isMaxRoomRecordDetail]){
                // change room ro completed:
                [self finishCountTimeDidSelect:nil];
                return;
            }
            
            // transition from Start to Stop.
             countTimeStatus = STOP;
            [CommonVariable sharedCommonVariable].roomIsCompleted = 2;
           
            
            NSDate *now = [[NSDate alloc] init];
            
            roomRecordDetailModel.recd_Ra_Id = [TasksManagerV2 getCurrentRoomAssignment];
            roomRecordDetailModel.recd_User_Id = [UserManagerV2 sharedUserManager].currentUser.userId;
            roomRecordDetailModel.recd_Stop_Date = [timeFomarter stringFromDate:now];
           
            raDataModel.roomAssignmentRoomCleaningStatusId = ENUM_CLEANING_STATUS_PAUSE;
            self.roomRecord.rrec_PostStatus = POST_STATUS_SAVED_UNPOSTED;
            
            isSaved = YES;
            
            if([[RoomManagerV2 sharedRoomManager] updateRoomHaveBeenRemove:[UserManagerV2 sharedUserManager].currentUser.userId AndRaModel:raDataModel]== RESPONSE_STATUS_ROOM_HAVE_BEEN_DELETED) {
                UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Error- This room is removed" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                [alert show];
                
                return;
            }
            
            [self saveDataWhenChangeStatus];
            [self refreshLayoutButtons];
            [timerButon setBackgroundImage:[UIImage imageNamed:IMAGE_TIMER_PAUSE] forState:UIControlStateNormal]; 
            
            [countTimeButton setBackgroundImage:[UIImage imageNamed:IMAGE_PLAY_ON] forState:UIControlStateNormal];
            [countTimeButton setBackgroundImage:[UIImage imageNamed:IMAGE_PLAY_ON] forState:UIControlStateHighlighted];
            
            [tableView reloadData];

        } break;
            
        case NOT_START:{
                       
            // transition from not start to pause.
            countTimeStatus = START;
            raDataModel.roomAssignmentRoomCleaningStatusId = ENUM_CLEANING_STATUS_START;
            if([[RoomManagerV2 sharedRoomManager] updateRoomHaveBeenRemove:[UserManagerV2 sharedUserManager].currentUser.userId AndRaModel:raDataModel]== RESPONSE_STATUS_ROOM_HAVE_BEEN_DELETED
               ){
                UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Error- This room is removed" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                [alert show];
                isSaved = YES;
                countTimeStatus=STOP;
                
                return;
                
            }

        
            [CommonVariable sharedCommonVariable].roomIsCompleted = 0;
            NSDate *now = [[NSDate alloc] init];
            
            if(![self.roomRecord.rrec_Cleaning_Start_Time isEqualToString:@""]){
                self.roomRecord.rrec_Cleaning_Start_Time = [timeFomarter stringFromDate:now];  //... start time
            }
            
            // start room record detail
           
            roomRecordDetailModel.recd_Ra_Id = [TasksManagerV2 getCurrentRoomAssignment];
            roomRecordDetailModel.recd_User_Id = [UserManagerV2 sharedUserManager].currentUser.userId;
            roomRecordDetailModel.recd_Start_Date = [timeFomarter stringFromDate:now];

          
            
            [self saveDataWhenChangeStatus];            
            [self refreshLayoutButtons];
            
            [finishTimeButton setUserInteractionEnabled:YES];
            [finishTimeButton setBackgroundImage:[UIImage imageNamed:IMAGE_COMPLETE_ON] forState:UIControlStateNormal];
            //[countTimeButton setBackgroundImage:[UIImage imageNamed:IMAGE_PAUSE] forState:UIControlStateNormal];
            [countTimeButton setBackgroundImage:[UIImage imageNamed:IMAGE_STOP_ON] forState:UIControlStateNormal];
            [countTimeButton setBackgroundImage:[UIImage imageNamed:IMAGE_STOP_ON] forState:UIControlStateHighlighted];
            [countTimeButton setBackgroundImage:[UIImage imageNamed:IMAGE_STOP_GRAY] forState:UIControlStateDisabled];
            
            [timerButon setBackgroundImage:[UIImage imageNamed:IMAGE_TIMER_PLAY] forState:UIControlStateNormal];
            [btnAction setHidden:NO];
            CGRect f = timerButon.frame;
            [timerButon setFrame:CGRectMake(f.origin.x, btnAction.frame.origin.y + btnAction.frame.size.height +5 ,f.size.width, f.size.height)];
            // show check list :
            
            [tableView reloadData];
        } break;
            
        case FINISH:{
            
        } break;
         
        case PAUSE:{
            
        } break;
            
        
    }
}

// display time with expected time
- (NSString *)countTimeSpendForTask:(NSInteger)expectedTimeForTask  {
    NSString *secondsString;
    NSString *minutesString;
    NSString *hoursString;
    NSInteger secondsInt;
    NSInteger minutesInt;
    NSInteger hoursInt;
    NSString *timeDisplay;
    if (expectedTimeForTask < 0){
        hoursInt = -expectedTimeForTask/3600;
        minutesInt =-((expectedTimeForTask + (hoursInt*3600)) / 60);
        secondsInt =-(expectedTimeForTask % 60);
        
        hoursString = [NSString stringWithFormat:@"%d",hoursInt];
        minutesString = [NSString stringWithFormat:@"%d",minutesInt];
        secondsString = [NSString stringWithFormat:@"%d",secondsInt];
        
        if([hoursString length]==1){
            hoursString = [NSString stringWithFormat:@"0%@",hoursString];
        }
        
        if([minutesString length]==1){
            minutesString = [NSString stringWithFormat:@"0%@",minutesString];
        }
        
        if([secondsString length]==1){
            secondsString = [NSString stringWithFormat:@"0%@",secondsString];
        }
        
        timeDisplay = [NSString stringWithFormat:@"- %@:%@:%@",hoursString,minutesString,secondsString];
        [timerButon setBackgroundImage:[UIImage imageNamed:@"timer_red_274x58.png"] forState:UIControlStateNormal];
    } else {
        hoursInt = expectedTimeForTask/3600;
        minutesInt = (expectedTimeForTask - (hoursInt*3600)) / 60;
        secondsInt = expectedTimeForTask % 60;
        
        hoursString = [NSString stringWithFormat:@"%d",hoursInt];
        minutesString = [NSString stringWithFormat:@"%d",minutesInt];
        secondsString = [NSString stringWithFormat:@"%d",secondsInt];
        
        if([hoursString length]==1){
            hoursString = [NSString stringWithFormat:@"0%@",hoursString];
        }
        
        if([minutesString length]==1){
            minutesString = [NSString stringWithFormat:@"0%@",minutesString];
        }
        
        if([secondsString length]==1){
            secondsString = [NSString stringWithFormat:@"0%@",secondsString];
        }
        timeDisplay = [NSString stringWithFormat:@"%@:%@:%@",hoursString,minutesString,secondsString];
    }
  return timeDisplay;
}

- (void)displayTimerButton {
    [timerButon setTitle:[self countTimeSpendForTask: timeLeftCleaningStatus] forState:UIControlStateNormal];
}

-(void)updateTimer{
   // NSLog(@"durationTime - %d timeLeft - %d",durationTime,timeLeftCleaningStatus);
//    NSLog(@" updateTimer %i ",count);
//    count++;

    if(countTimeStatus == PAUSE || countTimeStatus == FINISH || countTimeStatus == STOP) return;
    timeLeftCleaningStatus -= 1;  
    durationTime +=1;
    [self displayTimerButton];
}

- (void)completeCleaningRoom {
    [self confirmCompleteCleaningStatus];
}

//@ this function used when complete task in room: cleaning , inspection
- (IBAction)finishCountTimeDidSelect:(id)sender {    
    countReminder = [[CountPopupReminderViewV2 alloc] initWithRoomId:[TasksManagerV2 getCurrentRoomAssignment]];
    [countReminder setDelegate:self];
    countReminder.tag = tagAlertCleaningStatusCompleted;
    if(![countReminder show]){
        [self showAlertWithTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] content:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_are_you_done_with_the_room_cleaning]
                          andtag:kTagAlertDoneWithRoomCleaning];
    }

}

//@ confirm block room
- (IBAction)blockRoomDidSelect:(id)sender {
    [self showAlertWithTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] content:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_msgalert_confirmblockroom] andtag:tagAlertBlockRoom];
}

//@ confirm reassign room
- (IBAction)reassignDidSelect:(id)sender {
    self.dataModel.room_is_re_cleaning = YES;
    raDataModel.roomAssignmentRoomCleaningStatusId = ENUM_CLEANING_STATUS_PENDING_SECOND_TIME;
    [reassignBtn setBackgroundImage:[UIImage imageNamed:@"bt_gray_299x58"] forState:UIControlStateNormal];
    [reassignBtn setUserInteractionEnabled:NO];
}



//@ get room status code
-(NSString*)roomStatusCode{
    RoomStatusModelV2 *_roomStatus = [[RoomStatusModelV2 alloc] init];
    _roomStatus.rstat_Id = raDataModel.roomAssignmentRoomStatusId;
    [[RoomManagerV2 sharedRoomManager] loadRoomStatusData:_roomStatus];
    return _roomStatus.rstat_Code;}


#pragma mark
#pragma mark  Alert delegate

//@ room status is change
-(void)roomStatusDidChange{
    RoomStatusModelV2 *roomStatusModel = [[RoomStatusModelV2 alloc] init];
    roomStatusModel.rstat_Id = roomStatusIDSelected;
    [[RoomManagerV2 sharedRoomManager] loadRoomStatusData:roomStatusModel];
    
    
    raDataModel.roomAssignmentRoomStatusId = roomStatusIDSelected;
    
    
    if(raDataModel.roomAssignmentRoomStatusId  == ENUM_ROOM_STATUS_OC || raDataModel.roomAssignmentRoomStatusId  == ENUM_ROOM_STATUS_VC || raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OOS || raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OOO){
        [self confirmCompleteCleaningStatus];
        
        if(raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OOO || raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OOS){
            
            [lockButton setUserInteractionEnabled:NO];
            [lockButton setImage:[UIImage imageNamed:@"block_bt_gray_299x58.png"] forState:UIControlStateNormal];    
            [self refreshLayoutButtons];
            
            //show action button
            [btnAction setHidden:NO];    

        }
    } else{
      [tableView reloadData];  
    }
    
}

-(void) roomBlockDidChange{
    self.raDataModel.roomAssignmentRoomStatusId = ENUM_ROOM_STATUS_OOO;
    [lockButton setUserInteractionEnabled:NO];
    [lockButton setImage:[UIImage imageNamed:@"block_bt_gray_299x58.png"] forState:UIControlStateNormal];    
    [self refreshLayoutButtons];
    
    //show action button
    [btnAction setHidden:NO];    
    [tableView reloadData];
}

-(void) alertAdvancedOKButtonPressed:(AlertAdvancedSearch *)alertView{
    isSaved = NO;
    
    if(alertView.tag == tagAlertRoomStatusChange){
        [self roomStatusDidChange];
    }
    
    if(alertView.tag == tagAlertBlockRoom){
        [self roomBlockDidChange];
    }
    
   
}



-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    if(buttonIndex == 1) return;
    isSaved = NO;
    
    if(alertView.tag == tagAlertRoomStatusChange){
        
        [self roomStatusDidChange];
    }
    
    if(alertView.tag == tagAlertBlockRoom){
        [self roomBlockDidChange];
    }
    
    if(alertView.tag == kTagAlertDoneWithRoomCleaning){
        [self confirmCompleteCleaningStatus];        
    }
    
    if(alertView.tag == kTagAlertCancelCleaningStatus){
        // transition from Start to Stop.
        countTimeStatus = STOP;
        isSaved = YES;
        
        NSDate *now = [[NSDate alloc] init];
        RoomRecordDetailModelV2 *roomRecordDetail = [[RoomRecordDetailModelV2 alloc] init];
        roomRecordDetail.recd_Ra_Id = [TasksManagerV2 getCurrentRoomAssignment];
        roomRecordDetail.recd_User_Id = [UserManagerV2 sharedUserManager].currentUser.userId;
        [[RoomManagerV2 sharedRoomManager] loadRoomRecordDataDetailByRoomAssignmentIdAndUserId:roomRecordDetail];
        
        roomRecordDetail.recd_Stop_Date = [timeFomarter stringFromDate:now];
        [[RoomManagerV2 sharedRoomManager] updateRoomRecordDetailData:roomRecordDetail];
        
        
        raDataModel.roomAssignmentRoomCleaningStatusId = ENUM_CLEANING_STATUS_PAUSE;
        [self saveDataWhenChangeStatus];
        
        
        //[countTimeButton setBackgroundImage:[UIImage imageNamed:IMAGE_PLAY] forState:UIControlStateNormal];
        [timerButon setBackgroundImage:[UIImage imageNamed:IMAGE_TIMER_PAUSE] forState:UIControlStateNormal]; 
        [self refreshLayoutButtons];
        
        [countTimeButton setBackgroundImage:[UIImage imageNamed:IMAGE_PLAY_ON] forState:UIControlStateNormal];
        [countTimeButton setBackgroundImage:[UIImage imageNamed:IMAGE_PLAY] forState:UIControlStateHighlighted];


    }
    
    if(alertView.tag == tagAlertCancelCleaningLessThan1Minute){
        // transition from Start to Stop.
        countTimeStatus = STOP;
        raDataModel.roomAssignmentRoomCleaningStatusId = ENUM_CLEANING_STATUS_PAUSE;
        
        [self saveDataWhenChangeStatus];
        
        [timerButon setBackgroundImage:[UIImage imageNamed:IMAGE_TIMER_PAUSE] forState:UIControlStateNormal]; 
        [self refreshLayoutButtons];
        
        [countTimeButton setBackgroundImage:[UIImage imageNamed:IMAGE_PLAY_ON] forState:UIControlStateNormal];
        [countTimeButton setBackgroundImage:[UIImage imageNamed:IMAGE_PLAY] forState:UIControlStateHighlighted];
        [self.superController.navigationController popViewControllerAnimated:YES];

    }
    
  }

-(void) alertAdvancedCancelButtonPressed:(AlertAdvancedSearch *)alertView{    
 }

#pragma mark
#pragma mark  Cleaning Status + Room Status + remark delegate

-(void)cleaningStatusDidSelect:(NSInteger)cleaningStatusId {
    isSaved = NO;
    
    if(cleaningStatusId == ENUM_CLEANING_STATUS_SERVICE_LATER) {
        self.roomRecord.rrec_Cleaning_Start_Time = @"";  //... start time
    }
    
    if(cleaningStatusId != ENUM_CLEANING_STATUS_COMPLETED) {
        raDataModel.roomAssignmentRoomCleaningStatusId = cleaningStatusId;
        
        if(cleaningStatusId == ENUM_CLEANING_STATUS_DND) {
            RoomAssignmentModelV2 *raModel = [[RoomAssignmentModelV2 alloc] init];
            raModel.roomAssignment_RoomId = [RoomManagerV2 getCurrentRoomNo];
            raModel.roomAssignment_UserId = [[UserManagerV2 sharedUserManager] currentUser].userId;
            [[RoomManagerV2 sharedRoomManager] loadRoomAsignmentByRoomIdAndUserID:raModel];            
            raModel.roomAssignment_Priority_Sort_Order = [[RoomManagerV2 sharedRoomManager] loadMaxPrioprity] + 1;
            
            [[RoomManagerV2 sharedRoomManager] update:raModel]; 
            
            // layout View             
            [finishTimeButton setBackgroundImage:[UIImage imageNamed:IMAGE_COMPLETE] forState:UIControlStateNormal];
            [countTimeButton setBackgroundImage:[UIImage imageNamed:IMAGE_PLAY_ON] forState:UIControlStateNormal];
            [timerButon setBackgroundImage:[UIImage imageNamed:IMAGE_TIMER_GRAY] forState:UIControlStateNormal];
            
            [countTimeButton setUserInteractionEnabled:NO];
            [finishTimeButton setUserInteractionEnabled:NO];
            [btnAction setUserInteractionEnabled:NO];
            [btnAction setBackgroundImage:[UIImage imageNamed:IMAGE_BT_GRAY] forState:UIControlStateNormal];
            countTimeStatus = STOP;
            durationTime = 0;     
            
            raModel.roomAssignment_Id = raDataModel.roomAssignment_Id;
            raModel.roomAssignment_UserId = raDataModel.roomAssignment_UserId;
            [[RoomManagerV2 sharedRoomManager] load:raModel];
            //update assign date for DND to 11:59:00
            NSDateFormatter *format = [[NSDateFormatter alloc] init];
            [format setDateFormat:@"yyyy-MM-dd"];
            NSDate *now = [NSDate date];
            NSString *dndAssignDate = [NSString stringWithFormat:@"%@ 23:58:00", [format stringFromDate:now]];
            raModel.roomAssignment_AssignedDate = dndAssignDate;
            
            raDataModel.roomAssignment_AssignedDate = dndAssignDate;
            
            [[RoomManagerV2 sharedRoomManager] update:raModel];
        }
        
        if(cleaningStatusId == ENUM_CLEANING_STATUS_DECLINE_SERVICE) {
            [finishTimeButton setBackgroundImage:[UIImage imageNamed:IMAGE_COMPLETE] forState:UIControlStateNormal];
            [countTimeButton setBackgroundImage:[UIImage imageNamed:IMAGE_PLAY_ON] forState:UIControlStateNormal];
            [timerButon setBackgroundImage:[UIImage imageNamed:IMAGE_TIMER_GRAY] forState:UIControlStateNormal];
            
            [countTimeButton setUserInteractionEnabled:YES];
            [finishTimeButton setUserInteractionEnabled:NO];
            [btnAction setUserInteractionEnabled:NO];
            [btnAction setBackgroundImage:[UIImage imageNamed:IMAGE_BT_GRAY] forState:UIControlStateNormal];
            countTimeStatus = STOP;
            timeLeftCleaningStatus = raDataModel.roomAssignmentRoomExpectedCleaningTime*60;
            durationTime = 0;            
            
            RoomAssignmentModelV2 *raModel = [[RoomAssignmentModelV2 alloc] init];
            raModel.roomAssignment_Id = raDataModel.roomAssignment_Id;
            raModel.roomAssignment_UserId = raDataModel.roomAssignment_UserId;
            [[RoomManagerV2 sharedRoomManager] load:raModel];
            
            //update assign date for Decline Service to 11:59:00
            NSDateFormatter *format = [[NSDateFormatter alloc] init];
            [format setDateFormat:@"yyyy-MM-dd"];
            NSDate *now = [NSDate date];
            NSString *declineServiceAssignDate = [NSString stringWithFormat:@"%@ 23:59:00", [format stringFromDate:now]];
            raModel.roomAssignment_AssignedDate = declineServiceAssignDate;
            
            raDataModel.roomAssignment_AssignedDate = declineServiceAssignDate;
            
            [[RoomManagerV2 sharedRoomManager] update:raModel];
            
            // set date on UI
            NSDateFormatter *_timeFomarter = [[NSDateFormatter alloc] init];
            [_timeFomarter setDateFormat:@"HH:mm"];            
            self.declineServiceDate = [_timeFomarter stringFromDate:now];
        }        
              
        if(cleaningStatusId == ENUM_CLEANING_STATUS_START) {
            [self startPauseCountTimerDidSelect:nil];
            return;
        }
        
        if(cleaningStatusId == ENUM_CLEANING_STATUS_PAUSE) {
            // transition from start to pause.
            countTimeStatus = PAUSE;
            
            self.roomRemarkModel = [[RoomRemarkModelV2 alloc] init];
            NSDate *now = [[NSDate alloc] init];
            self.roomRemarkModel.rm_clean_pause_time = [timeFomarter stringFromDate:now]; // store start time of pause 
            
            //raDataModel.roomAssignmentRoomCleaningStatusId = ENUM_CLEANING_STATUS_PAUSE;
            
            [countTimeButton setBackgroundImage:[UIImage imageNamed:IMAGE_PLAY_ON] forState:UIControlStateNormal];
            [timerButon setBackgroundImage:[UIImage imageNamed:IMAGE_TIMER_PAUSE] forState:UIControlStateNormal];
            
            [self refreshLayoutButtons];
            [self showRemark];
            
            [tableView reloadData];
            return;
        }
        
        [tableView reloadData];
    } else {
        [self finishCountTimeDidSelect:nil];
    }
}

-(void)cleaningStatusDidSelect:(NSInteger)cleaningStatusId andDate:(NSDate*)date{
    isSaved = NO;

    NSDateFormatter *_timeFomarter = [[NSDateFormatter alloc] init];
    [_timeFomarter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
   
    // insert new service later 
    RoomServiceLaterModelV2 *roomServicelater = [[RoomServiceLaterModelV2 alloc] init];
    roomServicelater.rsl_RecordId = self.roomRecord.rrec_Id;
    roomServicelater.rsl_Time = [_timeFomarter stringFromDate:date];    
    [[RoomManagerV2 sharedRoomManager] insertRoomServiceLaterData:roomServicelater];
    
    raDataModel.roomAssignmentRoomCleaningStatusId = cleaningStatusId;
    raDataModel.roomAssignment_AssignedDate = [_timeFomarter stringFromDate:date];    
    [self saveDataWhenChangeStatus];
    
    [finishTimeButton setBackgroundImage:[UIImage imageNamed:IMAGE_COMPLETE] forState:UIControlStateNormal];
    [countTimeButton setBackgroundImage:[UIImage imageNamed:IMAGE_PLAY_ON] forState:UIControlStateNormal];
    [timerButon setBackgroundImage:[UIImage imageNamed:IMAGE_TIMER_GRAY] forState:UIControlStateNormal];    
    [countTimeButton setUserInteractionEnabled:YES];
    [finishTimeButton setUserInteractionEnabled:NO];
    [btnAction setUserInteractionEnabled:NO];
    [btnAction setBackgroundImage:[UIImage imageNamed:IMAGE_BT_GRAY] forState:UIControlStateNormal];
    countTimeStatus = STOP;
    timeLeftCleaningStatus = raDataModel.roomAssignmentRoomExpectedCleaningTime*60;
    durationTime = 0;            
        
    [tableView reloadData];
}

#pragma mark
#pragma mark Count popup reminder
- (void)confirmCompleteCleaningStatus {
    isSaved = YES;
    [CommonVariable sharedCommonVariable].roomIsCompleted = 1;
    raDataModel.roomAssignmentRoomCleaningStatusId = ENUM_CLEANING_STATUS_COMPLETED;
    
    
    
    if(roomStatusIDSelected == ENUM_ROOM_STATUS_OC_SO){
        raDataModel.roomAssignmentRoomStatusId = roomStatusIDSelected;
    }
    
    if(raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VD){
        raDataModel.roomAssignmentRoomStatusId = ENUM_ROOM_STATUS_VC;
     }   
    if(raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OD){
        raDataModel.roomAssignmentRoomStatusId = ENUM_ROOM_STATUS_OC;
    }
    
    // update duration time
    NSDate *now = [[NSDate alloc] init];   
    self.roomRecord.rrec_Cleaning_End_Time = [timeFomarter stringFromDate:now];  //... end time    
    self.roomRecord.rrec_PostStatus = POST_STATUS_SAVED_UNPOSTED;
    
    //Hao Tran - Remove for can't build
    /*
    durationTime += [self.roomRecord.rrec_Cleaning_Duration intValue];    
    self.roomRecord.rrec_Cleaning_Duration = [NSString stringWithFormat:@"%d",durationTime];
    
    NSTimeInterval timeDifference = [now timeIntervalSinceDate:
                                     [timeFomarter dateFromString:self.roomRecord.rrec_Cleaning_Start_Time]];
    
    NSInteger _totalPauseTime = (NSInteger)timeDifference - durationTime;
    self.roomRecord.rrec_Cleaning_Total_Pause_Time =[NSString stringWithFormat:@"%d",_totalPauseTime] ;  
    [[RoomManagerV2 sharedRoomManager] updateRoomRecordData:self.roomRecord];
    [self saveDataWhenChangeStatus];
     */
    
    /********* hidden / show button: *********/
    CGRect frameActionButton = btnAction.frame;
        
    [btnAction setFrame:CGRectMake(frameActionButton.origin.x, countTimeButton.frame.origin.y,
                                       frameActionButton.size.width, frameActionButton.size.height)];
    [countTimeButton setHidden:YES];
    [finishTimeButton setHidden:YES];
    [timerButon setHidden:YES];    
    
    //Always hide lockButton because this view can not block room.
    if(self.raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VD || 
       self.raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VC) {
        [lockButton setHidden:NO];
    } else{
        [lockButton setHidden:YES];
    }        
     CGFloat y = btnAction.frame.origin.y + btnAction.frame.size.height + 4;
    [lockButton setFrame:CGRectMake(lockButton.frame.origin.x, y, 
                                        lockButton.frame.size.width, 
                                        lockButton.frame.size.height)];

       
    countTimeStatus = FINISH ;
    //roomDetailStatus = ENUM_MAID_NOT_INSPECTED_COMPLETED;
    
    [tableView reloadData];

}

-(void)yesCountReminder{
    isSaved = NO;
    [countReminder hide];
    [self confirmCompleteCleaningStatus];
}

-(void)noCountReminder{
    if(countTimeStatus == NOT_START){
        [self startPauseCountTimerDidSelect:nil];
    }
    [countReminder hide];
     [[NSNotificationCenter defaultCenter] postNotificationName:@"AddCLGWithSelectedIndex" object:@"1"];
}


-(void)roomStatusDidSelect:(NSInteger)roomStatusID{
    roomStatusIDSelected = roomStatusID;
    
    if(roomStatusID == ENUM_ROOM_STATUS_OC || roomStatusID == ENUM_ROOM_STATUS_VC || roomStatusID == ENUM_ROOM_STATUS_OC_SO){
        // [self completeCleaningRoom];
      
        [self finishCountTimeDidSelect:nil];
        
    } else{
        isSaved = NO;
        
        [self showAlertWithTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] 
                         content:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_room_confirmchangeroomstatus] andtag:tagAlertRoomStatusChange];

    }
    

}

-(void)remarkViewV2DoneWithText:(NSString *)text andController:(UIViewController *)controllerView{
    //isSaved = NO;
    [controllerView.navigationController popViewControllerAnimated:YES];
    NSIndexPath *indexpath = nil;
    UITableViewCell *cell = nil;
    
    if(self.raDataModel.roomAssignmentRoomStatusId != ENUM_CLEANING_STATUS_COMPLETED){
        indexpath = [NSIndexPath indexPathForRow:4 inSection:0];
    }   
    
    roomRecord.rrec_Remark = text;
    
    [[RoomManagerV2 sharedRoomManager] updateRoomRecordData:roomRecord];
    
    cell = [tableView cellForRowAtIndexPath:indexpath];
    UILabel *label = (UILabel*)[cell.contentView viewWithTag:tagText];
    [label setText:text];
    
        
//    if (countTimeStatus == START) {
//        roomRemarkModel.rm_clean_pause_time = [NSString stringWithString:@"0"];
//    }
//    
//    if(self.roomRemarkModel.rm_Id == 0){
//        // save remark data
//        NSInteger countRoomRemark = [[RoomManagerV2 sharedRoomManager] 
//                                     countRoomRemarkData:self.roomRecord.rrec_Id];
//        countRoomRemark ++;
//        self.roomRemarkModel.rm_Id = countRoomRemark;
//        self.roomRemarkModel.rm_RecordId = self.roomRecord.rrec_Id;
//        self.roomRemarkModel.rm_content  = text;
//        [[RoomManagerV2 sharedRoomManager] insertRoomRemarkData:self.roomRemarkModel];        
//        self.roomRecord.rrec_Cleaning_Duration = [NSString stringWithFormat:@"%d",durationTime];
//        
//        NSDate *now = [[NSDate alloc] init];
//        self.roomRecord.rrec_Cleaning_End_Time = [timeFomarter stringFromDate:now];
//        NSTimeInterval timeDifference = [now timeIntervalSinceDate:
//                                         [timeFomarter dateFromString:self.roomRecord.rrec_Cleaning_Start_Time]];
//        
//        NSInteger _totalPauseTime = (NSInteger)timeDifference - durationTime;
//        self.roomRecord.rrec_Cleaning_Total_Pause_Time =[NSString stringWithFormat:@"%d",_totalPauseTime] ;            
//        [[RoomManagerV2 sharedRoomManager] updateRoomRecordData:self.roomRecord];
//
//    } else{
//        [[RoomManagerV2 sharedRoomManager] updateRoomRemarkData:self.roomRemarkModel];
//    }
}

-(void) setDurationTime:(NSInteger)time
{
    durationTime += time;
    timeLeftCleaningStatus -= time;
}

@end
