//
//  RoomDetailView.h
//  EHouseKeeping
//
//  Created by tms on 5/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RoomAdapterV2.h"
#import "RoomModelV2.h"
#import "CleaningStatusViewV2.h"
#import "UserManagerV2.h"
#import "CleaningStatusModelV2.h"
#import "CheckListViewV2.h"
#import "AlertAdvancedSearch.h"
#import "RemarkViewV2.h"
#import "CountPopupReminderViewV2.h"
#import "CustomAlertViewV2.h"

@class RoomInfoCompletedFailedV2;


@interface RoomDetailCompletedFailedViewV2 : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate, UIActionSheetDelegate,AlertAdvancedSearchDelegate,RemarkViewV2Delegate,CountPopupReminderViewV2Delegate>{
    
    IBOutlet  UITableView *tableView;
    //variable for label column in table
    NSString *expectedCleaningTime;
    NSString *guestPref;
    NSString *additioJob;
    NSString *cleaningStatus;
    NSString *roomStatus;
    NSString *remark;
    NSString  *estimatedInspectionTime;
    NSString *actualInspectionTime;
    NSString *inspectionStatus;
    NSString *msgbtnOk;
    NSString *msgSaveRoomDetail;
    // variable for value column in table
    IBOutlet UIScrollView *scrollView;
    IBOutlet UIView *listButtonView;
    
    IBOutlet UIButton *btnAction;
    IBOutlet UIButton *btnLostAnDFound;
    IBOutlet UIButton *btnEngineeringAssitance;
    IBOutlet UIButton *btnCount;
    IBOutlet UIButton *btnLaundry;
    IBOutlet UIButton *btnGuideLine;
    
    IBOutlet UIButton *lockButton;
    IBOutlet UIButton *checkListButton;
    IBOutlet UIButton *countTimeButton;
    IBOutlet UIButton *finishTimeButton;
    
    
    RoomModelV2 *dataModel;
    RoomModelV2 *dataModelCompare;
    
    RoomAssignmentModelV2 *raDataModel;
    
    
    RoomRecordModelV2 *roomRecord;
    RoomRemarkModelV2 *roomRemarkModel;
    
    NSIndexPath *indexPathCleaaningStatus;
        
    NSTimer *timer;
    NSInteger timeLeftCleaningStatus;
    NSInteger timeLeftInspectionStatus;
    IBOutlet UILabel *countDownTimerLabel;
    IBOutlet UIButton *timerButon;
    
    enum ENUM_COUNT_TIME countTimeStatus;
    
    __unsafe_unretained RoomInfoCompletedFailedV2*  superController;
    BOOL  isPass;
    BOOL isInspected;
    BOOL isCompleted;
    BOOL isLock;
    
    NSDateFormatter *timeFomarter;
    // roomDetail status
    enum ENUM_ROOMDETAIL_STATUS roomDetailStatus;
    //__unsafe_unretained id<RoomDetailViewV2Delegate> delegate;
    NSInteger roomStatusIDSelected;
    
    CountPopupReminderViewV2 *countReminder;
    UIButton *reassignBtn;
}


- (IBAction)butCheckListPressed:(id)sender;
- (IBAction)butActionPressed:(id)sender;

@property (nonatomic, strong) IBOutlet UIButton *reassignBtn;

@property (nonatomic, assign) RoomInfoCompletedFailedV2 *superController;

@property (nonatomic,strong) IBOutlet  UITableView *tableView;
@property (nonatomic,strong) IBOutlet UIScrollView *scrollView;
@property (nonatomic,strong) NSString *expectedCleaningTime;
@property (nonatomic,strong) NSString *guestPref;
@property (nonatomic,strong) NSString  *additioJob;
@property (nonatomic,strong) NSString *cleaningStatus;
@property (nonatomic,strong) NSString *roomStatus;
@property (nonatomic,strong) NSString *remark;
@property (nonatomic,strong) NSString *estimatedInspectionTime;
@property (nonatomic,strong) NSString *actualInspectionTime;
@property (nonatomic,strong) NSString *inspectionStatus;

@property (nonatomic,strong) NSString *msgbtnOk;
@property (nonatomic,strong) NSString *msgSaveRoomDetail;

// define buttons.
@property (nonatomic,strong) IBOutlet UIButton *btnLostAnDFound;
@property (nonatomic,strong) IBOutlet  UIButton *btnEngineeringAssitance;
@property (nonatomic,strong) IBOutlet UIButton *btnCount;
@property (nonatomic,strong) IBOutlet UIButton *btnLaundry;
@property (nonatomic,strong) IBOutlet UIButton *btnGuideLine;

@property (nonatomic,strong)  IBOutlet UIView *listButtonView;
@property (nonatomic,strong) RoomModelV2 *dataModel;

@property (nonatomic, strong) RoomRecordModelV2 *roomRecord;
@property (nonatomic, strong) RoomRemarkModelV2 *roomRemarkModel;
@property (nonatomic, strong) RoomAssignmentModelV2 *raDataModel;
//@property (nonatomic,retain) CleaningStatusViewV2 *cleaningStatusView;

@property (nonatomic,strong) NSIndexPath *indexPathCleaaningStatus;
@property (nonatomic) BOOL isPass;
@property (nonatomic) BOOL isInspected;
@property (nonatomic) BOOL isCompleted;

//@property (assign) id<RoomDetailViewV2Delegate> delegate;

- (void) setCaptionsView;
- (void) updateTimer;
- (void) displayTimerButton ;
- (void)showRemark;
- (void) cleaningStatusDidSelect:(NSInteger)cleaningStatusId;
- (void) cleaningStatusDidSelect:(NSInteger)cleaningStatusId andDate:(NSDate*)date;
- (void) roomStatusDidSelect:(NSInteger)roomStatusID;

-(void)saveData;
-(void)showAlertWithTitle:(NSString*)title content:(NSString*)content andtag:(NSInteger)tag;
- (IBAction)finishCountTimeDidSelect:(id)sender ;
- (void)displayButtons;
- (IBAction)reassignDidSelect:(id)sender;
- (BOOL) isNotSavedAndExit;
//- (BOOL)checkIsSave;
- (void)confirmCompleteCleaningStatus;

@end
