//
//  RoomDetailView.h
//  EHouseKeeping
//
//  Created by tms on 5/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RoomAdapterV2.h"
#import "RoomModelV2.h"
#import "CleaningStatusViewV2.h"
#import "UserManagerV2.h"
#import "CleaningStatusModelV2.h"
#import "CheckListViewV2.h"
#import "AlertAdvancedSearch.h"
#import "RemarkViewV2.h"
#import "CountPopupReminderViewV2.h"
#import "CustomAlertViewV2.h"

@class RoomInfoInspectedPassedV2;


@interface RoomDetailInspectedPassedViewV2 : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate, UIActionSheetDelegate,AlertAdvancedSearchDelegate,RemarkViewV2Delegate,CountPopupReminderViewV2Delegate>{
    
    __unsafe_unretained IBOutlet  UITableView *tableView;
    //variable for label column in table
    NSString *expectedCleaningTime;
    NSString *guestPref;
    NSString *additioJob;
    NSString *cleaningStatus;
    NSString *roomStatus;
    NSString *remark;
    NSString  *estimatedInspectionTime;
    NSString *actualInspectionTime;
    NSString *inspectionStatus;
    NSString *msgbtnOk;
    NSString *msgSaveRoomDetail;
    // variable for value column in table
    IBOutlet UIView *listButtonView;
    
    __unsafe_unretained IBOutlet UIButton *btnAction;
    
    __unsafe_unretained IBOutlet UIButton *lockButton;
    __unsafe_unretained IBOutlet UIButton *checkListButton;
    __unsafe_unretained IBOutlet UIButton *countTimeButton;
    __unsafe_unretained IBOutlet UIButton *finishTimeButton;
    
    
    RoomModelV2 *dataModel;
    RoomAssignmentModelV2 *dataModelCompare;
    
    RoomAssignmentModelV2 *raModel;
    
    RoomRecordModelV2 *roomRecord;
    RoomRemarkModelV2 *roomRemarkModel;
    
    NSIndexPath *indexPathCleaaningStatus;
        
    NSTimer *timer;
    NSInteger timeLeftCleaningStatus;
    NSInteger timeLeftInspectionStatus;
    __unsafe_unretained IBOutlet UIButton *timerButon;
    
    enum ENUM_COUNT_TIME countTimeStatus;
    
    __unsafe_unretained RoomInfoInspectedPassedV2*  superController;
    BOOL  isPass;
    BOOL isInspected;
    BOOL isCompleted;
    BOOL isLock;
    
    NSDateFormatter *timeFomarter;
    // roomDetail status
    enum ENUM_ROOMDETAIL_STATUS roomDetailStatus;
    
    NSInteger roomStatusIDSelected;
    
    CountPopupReminderViewV2 *countReminder;
    __unsafe_unretained IBOutlet UIButton *reassignBtn;
}


- (IBAction)butCheckListPressed:(id)sender;
- (IBAction)butActionPressed:(id)sender;
- (IBAction)blockRoomDidSelect:(id)sender;
- (IBAction)startPauseCountTimerDidSelect:(id)sender;

@property (nonatomic, strong) RoomAssignmentModelV2 *raModel;
@property (nonatomic, assign) UIButton *reassignBtn;

@property (assign) RoomInfoInspectedPassedV2 *superController;

@property (nonatomic, assign) UITableView *tableView;
@property (nonatomic, strong) NSString *expectedCleaningTime;
@property (nonatomic, strong) NSString *guestPref;
@property (nonatomic, strong) NSString  *additioJob;
@property (nonatomic, strong) NSString *cleaningStatus;
@property (nonatomic, strong) NSString *roomStatus;
@property (nonatomic, strong) NSString *remark;
@property (nonatomic, strong) NSString *estimatedInspectionTime;
@property (nonatomic, strong) NSString *actualInspectionTime;
@property (nonatomic, strong) NSString *inspectionStatus;

@property (nonatomic, strong) NSString *msgbtnOk;
@property (nonatomic, strong) NSString *msgSaveRoomDetail;

@property (nonatomic, strong) UIView *listButtonView;
@property (nonatomic, strong) RoomModelV2 *dataModel;

@property (nonatomic, strong) RoomRecordModelV2 *roomRecord;
@property (nonatomic, strong) RoomRemarkModelV2 *roomRemarkModel;


@property (nonatomic, strong) NSIndexPath *indexPathCleaaningStatus;
@property (nonatomic) BOOL isPass;
@property (nonatomic) BOOL isInspected;
@property (nonatomic) BOOL isCompleted;


- (void) setCaptionsView;
- (void) updateTimer;
- (void) displayTimerButton ;

- (void) cleaningStatusDidSelect:(NSInteger)cleaningStatusId;
- (void) cleaningStatusDidSelect:(NSInteger)cleaningStatusId andDate:(NSDate*)date;
- (void) roomStatusDidSelect:(NSInteger)roomStatusID;

-(void)saveData;
-(void)showAlertWithTitle:(NSString*)title content:(NSString*)content andtag:(NSInteger)tag;
- (IBAction)finishCountTimeDidSelect:(id)sender ;
- (void)displayButtons;
- (IBAction)reassignDidSelect:(id)sender;
- (BOOL) isNotSavedAndExit;
- (BOOL)checkIsSave;
- (void)confirmCompleteCleaningStatus;
-(void) setRoomAssignmentModel:(RoomAssignmentModelV2 *) model;

@end
