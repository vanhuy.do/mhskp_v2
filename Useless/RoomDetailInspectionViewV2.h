//
//  RoomDetailView.h
//  EHouseKeeping
//
//  Created by tms on 5/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RoomAdapterV2.h"
#import "RoomModelV2.h"
#import "CleaningStatusViewV2.h"
#import "UserManagerV2.h"
#import "CleaningStatusModelV2.h"
#import "CheckListViewV2.h"
#import "AlertAdvancedSearch.h"
#import "RemarkViewV2.h"
#import "CountPopupReminderViewV2.h"
#import "CustomAlertViewV2.h"
#import "ReAssignRoomViewControllerV2.h"

@class RoomInfoInspectionV2;

@protocol RoomDetailInspectionViewV2Delegate <NSObject>
@optional
- (void)showPopupCleaningStatus:(UIViewController*)controller;
- (void)showPopupRoomStatus:(UIViewController*)controller;
- (void)showPopUpRemark:(UIViewController*)controller;

@end

@interface RoomDetailInspectionViewV2 : UIViewController <UITableViewDelegate, UITableViewDataSource,UITextFieldDelegate, UIActionSheetDelegate, AlertAdvancedSearchDelegate, RemarkViewV2Delegate,CountPopupReminderViewV2Delegate>{
    
    __unsafe_unretained IBOutlet  UITableView *tableView;
    //variable for label column in table
    NSString *expectedCleaningTime;
    NSString *guestPref;
    NSString *additioJob;
    NSString *cleaningStatus;
    NSString *roomStatus;
    NSString *remark;
    NSString *estimatedInspectionTime;
    NSString *actualInspectionTime;
    NSString *inspectionStatus;
    NSString *msgbtnOk;
    NSString *msgSaveRoomDetail;
    
    __unsafe_unretained IBOutlet UIButton *btnAction;
    
    __unsafe_unretained IBOutlet UIButton *lockButton;
    __unsafe_unretained IBOutlet UIButton *checkListButton;
    __unsafe_unretained IBOutlet UIButton *countTimeButton;
    __unsafe_unretained IBOutlet UIButton *finishTimeButton;
    __unsafe_unretained IBOutlet UIButton *reassignBtn;
    IBOutlet UIView *listButtonView;
    
    RoomModelV2 *dataModel;
    RoomAssignmentModelV2 *dataModelCompare;
    
    RoomAssignmentModelV2 *raModel;
    
    RoomRecordModelV2 *roomRecord;
//    RoomRemarkModelV2 *roomRemarkModel;
    RoomRecordDetailModelV2 *roomRecordDetail;
    
    NSIndexPath *indexPathCleaningStatus;
        
    NSTimer *timer;
    NSInteger timeLeftInspectionStatus;

    __unsafe_unretained IBOutlet UIButton *timerButon;
    
    enum ENUM_COUNT_TIME countTimeStatus;
    
    __unsafe_unretained RoomInfoInspectionV2 *superController;
    BOOL  isPass;
    BOOL isInspected;
    BOOL isCompleted;
    BOOL isLock;
    
    NSDateFormatter *timeFomarter;
    // roomDetail status
    enum ENUM_ROOMDETAIL_STATUS roomDetailStatus;
    __unsafe_unretained id<RoomDetailInspectionViewV2Delegate> delegate;
    NSInteger roomStatusIDSelected;
    
    CountPopupReminderViewV2 *countReminder;
    NSInteger durationTime;
    BOOL isSaved;
    
    
}


- (IBAction)butCheckListPressed:(id)sender;
- (IBAction)butActionPressed:(id)sender;
- (IBAction)blockRoomDidSelect:(id)sender;
- (IBAction)startPauseCountTimerDidSelect:(id)sender;

@property (nonatomic, assign) IBOutlet UIButton *reassignBtn;

@property (strong, nonatomic) IBOutlet UIView *listButtonView;
@property (assign) RoomInfoInspectionV2 *superController;

@property (nonatomic, assign) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSString *expectedCleaningTime;
@property (nonatomic, strong) NSString *guestPref;
@property (nonatomic, strong) NSString  *additioJob;
@property (nonatomic, strong) NSString *cleaningStatus;
@property (nonatomic, strong) NSString *roomStatus;
@property (nonatomic, strong) NSString *remark;
@property (nonatomic, strong) NSString *estimatedInspectionTime;
@property (nonatomic, strong) NSString *actualInspectionTime;
@property (nonatomic, strong) NSString *inspectionStatus;
@property (nonatomic, strong) RoomAssignmentModelV2 *raModel;
@property (nonatomic, strong) NSString *msgbtnOk;
@property (nonatomic, strong) NSString *msgSaveRoomDetail;

@property (nonatomic, strong) RoomModelV2 *dataModel;

@property (nonatomic, strong) RoomRecordModelV2 *roomRecord;
//@property (nonatomic, strong) RoomRemarkModelV2 *roomRemarkModel;
@property (nonatomic, strong) RoomRecordDetailModelV2 *roomRecordDetail;

@property (nonatomic, strong) NSIndexPath *indexPathCleaningStatus;
@property (nonatomic) BOOL isPass;
@property (nonatomic) BOOL isInspected;
@property (nonatomic) BOOL isCompleted;

@property (assign) id<RoomDetailInspectionViewV2Delegate> delegate;
@property (assign) BOOL isSaved;


- (void) setCaptionsView;
- (void) updateTimer;
- (void) displayTimerButton;
- (void) showRoomStatus;
- (void) showRemark;
- (void) roomStatusDidSelect:(NSInteger)roomStatusID;

-(void) saveData;
-(void) showAlertWithTitle:(NSString*)title content:(NSString*)content andtag:(NSInteger)tag;
- (IBAction) finishCountTimeDidSelect:(id)sender ;

- (IBAction) reassignDidSelect:(id)sender;
- (BOOL) isNotSavedAndExit;
- (BOOL) checkIsSave;
- (void) confirmCompleteCleaningStatus;

-(void) layoutButton;
-(void) refreshLayoutButtons;

-(BOOL) isRoomInspected;
-(NSString*) timeDisplayWithSecond:(NSInteger)time;

-(void) startInspection;

-(void) setRoomAssignmentModel:(RoomAssignmentModelV2 *) model;

-(enum ENUM_ROOMDETAIL_STATUS) currentCountTimeStatusInspection;
@end
