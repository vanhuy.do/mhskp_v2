//
//  ReAssignRoomViewControllerV2.m
//  mHouseKeeping
//
//  Created by TMS on 5/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ReAssignRoomViewControllerV2.h"
#import "TopbarViewV2.h"
#import "CommonVariable.h"
#import "ehkDefines.h"
#import "LanguageManagerV2.h"
#import "NetworkCheck.h"
#import "UserListModelV2.h"
#import "CustomAlertViewV2.h"
#import "MyNavigationBarV2.h"
#import "DeviceManager.h"

@interface ReAssignRoomViewControllerV2 ()

#define tagTopbarView 1234
#define COLOR_HIGHLIGHT [UIColor colorWithRed:50.0/255 green:79.0/255 blue:133.0/255 alpha:1.0]

-(void) loadTopbarView;
-(void) showTopbarView;
-(void) hideTopbarView;
-(void) setHiddenTopbarView;
-(void) adjustShowForViews;
-(void) adjustRemoveForViews;
-(void) addButtonHandleShowHideTopbar;
-(void) topbarTapped:(UIButton *) sender;
-(TopbarViewV2 *) getTopBarView;

-(void) backBarPressed;
-(void) addBackButton;

-(void) showData;
-(void) setReassignDateToDataWithRoomAssignmentModel:(RoomAssignmentModelV2 *) model;
-(void) loadRecleaningTimes;

-(void) hideChooseDataView;
-(void) showChooseDataView;
-(void) removeChooseDataView;
-(void) hideAllPickerViews;

-(void) saveReassign;

@end

@implementation ReAssignRoomViewControllerV2
@synthesize roomIsCleaning;
@synthesize isSaved;
@synthesize roomModel;
@synthesize ramodel;
@synthesize userlist;
@synthesize chooseDataView;
@synthesize recleaningTimes;
@synthesize userChoosen;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil roomCleaningStatus:(ENUM_IS_ROOM_CLEANING) isRoomCleaning roomModel:(RoomModelV2 *) currentRoomModel{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        roomIsCleaning = isRoomCleaning;
        
        //init save status
        if (roomIsCleaning == ROOM_IS_CLEANING) {
            isSaved = NO;
        } else {
            isSaved = YES;
        }
        
        self.roomModel = currentRoomModel;
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if ([btnRemarkArrow isHighlighted] == YES) {
        [btnRemarkArrow setHighlighted:NO];
    }
}

//Call after set frames serveral subviews of this view
-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0") && !isAlreadyLayoutSubviews){
        isAlreadyLayoutSubviews = YES;
        
        CGRect framePickerReassignTo = pickerReassignTo.frame;
        UIToolbar *blurView = [[UIToolbar alloc] initWithFrame:framePickerReassignTo];
        blurView.translucent = YES;
        [self.view insertSubview:blurView belowSubview:pickerReassignTo];
        
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    isAlreadyLayoutSubviews = NO;
    
    // Do any additional setup after loading the view from its nib.
    
    //load topbar view
    [self loadTopbarView];
    
    //show top bar view
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationWillStartSelector:@selector(setHiddenTopbarView)];
    
    f = topbar.frame;
    f.origin.y = 0;
    [topbar setFrame:f];
    [topbar setAlpha:1.0];
    
    [self adjustShowForViews];
    
    [UIView commitAnimations];
    
    //add back button
    [self addBackButton];
    
    //show data
    [self performSelector:@selector(showData) withObject:nil afterDelay:0.3];
    
    //add label mins to btn recleaning time
    CGRect frame = lblMins.frame;
    frame.origin.x = 51;
    frame.origin.y = 5;
    [lblMins setFrame:frame];
    [btnRecleaningTime addSubview:lblMins];
    
    //set hight light color for buttons
    [btnReassignDateTo setTitleColor:COLOR_HIGHLIGHT forState:UIControlStateHighlighted];
    [btnReassignTimeAt setTitleColor:COLOR_HIGHLIGHT forState:UIControlStateHighlighted];
    [btnReassignTo setTitleColor:COLOR_HIGHLIGHT forState:UIControlStateHighlighted];
    [btnRecleaningTime setTitleColor:COLOR_HIGHLIGHT forState:UIControlStateHighlighted];
}

- (void)viewDidUnload
{
    btnReassignTo = nil;
    btnReassignDateTo = nil;
    btnReassignTimeAt = nil;
    btnRecleaningTime = nil;
    lblMins = nil;
    navigationBarPicker = nil;
    btnCancel = nil;
    btnDone = nil;
    pickerTime = nil;
    pickerReassignTo = nil;
    pickerRecleaningTime = nil;
    pickerDate = nil;
    chooseDataView = nil;
    lblRemark = nil;
    lblRemarkValue = nil;
    btnRemarkArrow = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - === Handle Topbar Methods ===
#pragma mark

-(void)loadTopbarView {
    TopbarViewV2 *topbar = [[TopbarViewV2 alloc] initViewWithSuperModel:[[CommonVariable sharedCommonVariable] superRoomModel]];
    [topbar setTag:tagTopbarView];
    [self.view addSubview:topbar];
    [topbar setHidden:YES];
    
    [topbar setAlpha:0.0];
    [self addButtonHandleShowHideTopbar];
}

-(void)showTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationWillStartSelector:@selector(setHiddenTopbarView)];
    
    f = topbar.frame;
    f.origin.y = 0;
    [topbar setFrame:f];
    [topbar setAlpha:1.0];
    
    [self adjustShowForViews];
    
    [UIView commitAnimations];
}

-(void)hideTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDidStopSelector:@selector(setHiddenTopbarView)];
    [UIView setAnimationDelegate:self];
    
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    [topbar setAlpha:0.0];
    
    [self adjustRemoveForViews];
    
    [UIView commitAnimations];
}

-(void)setHiddenTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    [topbar setHidden:!topbar.hidden];
}

-(TopbarViewV2 *)getTopBarView {
    return (TopbarViewV2 *)[self.view viewWithTag:tagTopbarView];
}

-(void)addButtonHandleShowHideTopbar {
    UIButton *titleBarButton = [[UIButton alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
    
    titleBarButton.titleLabel.backgroundColor = [UIColor clearColor];
    titleBarButton.titleLabel.font = FONT_BUTTON_TOPBAR;
    titleBarButton.titleLabel.textAlignment = UITextAlignmentCenter;
    titleBarButton.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
    [titleBarButton.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
    [titleBarButton.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
    [titleBarButton addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    [titleBarButton setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_reassign_room] forState:UIControlStateNormal];
    self.navigationItem.titleView = titleBarButton;
}

-(void)topbarTapped:(UIButton *)sender {
    TopbarViewV2 *topbar = [self getTopBarView];
    [sender setEnabled:NO];
    if (topbar.isHidden) {
        [self showTopbarView];
    } else {
        [self hideTopbarView];
    }
    [self performSelector:@selector(topbarButtonEnable:) withObject:sender afterDelay:0.5];
}

-(void) topbarButtonEnable:(UIButton *) topbarBtn {
    [topbarBtn setEnabled:YES];
}

-(void)adjustShowForViews {
    //adjust all views when show 
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    CGRect ftbv = [controlsView frame];
    [controlsView setFrame:CGRectMake(0, f.size.height, 320, ftbv.size.height - f.size.height)];
}

-(void)adjustRemoveForViews { 
    //adjust all views when hide
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    CGRect ftbv = controlsView.frame;
    [controlsView setFrame:CGRectMake(0, 0, 320, ftbv.size.height + f.size.height)];
}

#pragma mark - === Handle Button Methods ===
#pragma mark

- (IBAction)btnRemarkPressed:(id)sender {
    [btnRemarkArrow setHighlighted:YES];
    [btnRemarkArrow setSelected:YES];
    
    RemarkViewV2 *remarkView = [[RemarkViewV2 alloc] initWithNibName:@"RemarkViewV2" bundle:nil];
    remarkView.delegate = self;
    [self.navigationController pushViewController:remarkView animated:YES];
    [remarkView.txvRemark setText:lblRemarkValue.text];
    
}

- (IBAction)btnReassignToPressed:(id)sender {
    [self hideAllPickerViews];
    
    [pickerReassignTo setHidden:NO];
    [pickerReassignTo reloadAllComponents];
    [navigationBarPicker.topItem setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_reassign_to]];
    
    [self showChooseDataView];
}

- (IBAction)btnReassignDateToPressed:(id)sender {
    [self hideAllPickerViews];
    
    [pickerDate setHidden:NO];
    [navigationBarPicker.topItem setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_reassign_date_to]];
    
    [self showChooseDataView];
}

- (IBAction)btnReassignTimeAtPressed:(id)sender {
    [self hideAllPickerViews];
    
    [pickerTime setHidden:NO];
    [navigationBarPicker.topItem setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_reassign_time_at]];
    
    [self showChooseDataView];
}

- (IBAction)btnRecleaningTimePressed:(id)sender {
    [self hideAllPickerViews];
    
    [pickerRecleaningTime reloadAllComponents];
    [pickerRecleaningTime setHidden:NO];
    [navigationBarPicker.topItem setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_recleaning_time]];
    
    [self showChooseDataView];
}

- (IBAction)btnCancelPressed:(id)sender {
    [self hideChooseDataView];
}

- (IBAction)btnDonePressed:(id)sender {
    if ([pickerReassignTo isHidden] == NO) {
        
        if (userlist.count == 0) {
            return;
        }
      
        NSInteger rowSelected = [pickerReassignTo selectedRowInComponent:0];
        UserListModelV2 *user = [userlist objectAtIndex:rowSelected];
        userChoosen = user;
        if ([[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString:(NSString *) ENGLISH_LANGUAGE] == YES) {
            [btnReassignTo setTitle:user.userListFullName forState:UIControlStateNormal];
        } else {
            [btnReassignTo setTitle:user.userListFullNameLang forState:UIControlStateNormal];
        }
    }
    
    if ([pickerDate isHidden] == NO) {
        selectedDate = [pickerDate date];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"dd MMM yyyy"];
        [btnReassignDateTo setTitle:[formatter stringFromDate:selectedDate] forState:UIControlStateNormal];
    }
    
    if ([pickerTime isHidden] == NO) {
        selectedTime = [pickerTime date];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"HH:mm"];
        
        NSString *timeSelected = [formatter stringFromDate:selectedTime];
        
        [btnReassignTimeAt setTitle:timeSelected forState:UIControlStateNormal];
    }
    
    if ([pickerRecleaningTime isHidden] == NO) {
        NSInteger rowSelected = [pickerRecleaningTime selectedRowInComponent:0];
        NSDictionary *dictionary = [recleaningTimes objectAtIndex:rowSelected];
        [btnRecleaningTime setTitle:[dictionary objectForKey:kRecleaningTimeValue] forState:UIControlStateNormal];
    }
    
    [self hideChooseDataView];
}

#pragma mark - === Set Captions ===
#pragma mark
//set captions in view
-(void) setCaptionsView {
    //set caption for topview
    TopbarViewV2 *topbar = [self getTopBarView];
    [topbar setCaptionsView];
    
    //set caption for title view
    UIButton *button = (UIButton *)self.navigationItem.titleView;
    [button setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_reassign_room] forState:UIControlStateNormal];
    
    //set caption remark label
    [lblRemark setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_remark_title]];
    
    //set caption re-assign to label
    [lblReassignTo setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_reassign_to]];
    
    //set caption re-assign date to
    [lblReassignDateTo setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_reassign_date_to]];
    
    //set caption re-assign time at
    [lblReassignTimeAt setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_reassign_time_at]];
    
    //set caption re-cleaning time
    [lblRecleaningTime setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_recleaning_time]];
    
    //set caption back button
    [btnBack setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK] forState:UIControlStateNormal];
    
    //set caption lblmin
    [lblMins setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_strmins]];
    
    //set caption room label
    [lblRoomNo setText:[NSString stringWithFormat:@"%@ %@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_room_no_assign_room], roomModel.room_Id]];
}

#pragma mark - === Save Data ===
#pragma mark
-(void) saveData {
    //show alert
    UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_please_confirm_to_assign_the_room] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles:[[LanguageManagerV2 sharedLanguageManager] getNo],  nil]; 
    alert.delegate = self;
    [alert show];
    
}

-(void)saveReassign {
    //show HUD
    HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_RA_SAVING_DATA]];
    [HUD setDelegate:self];
    [HUD show:YES];
    [self.tabBarController.view addSubview:HUD];
    
    //set flag did save
    isSaved = YES;
    
    //save room assignment
    
    //get assign date & time
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSString *dateString = [dateFormat stringFromDate:selectedDate];
    
    NSDateFormatter *timeFormat = [[NSDateFormatter alloc] init];
    [timeFormat setDateFormat:@"HH:mm:ss"];
    NSString *timeString = [timeFormat stringFromDate:selectedTime];
    
    NSString *assignDateString = [NSString stringWithFormat:@"%@ %@", dateString, timeString];
//    NSDateFormatter *dateformatter = [[NSDateFormatter alloc] init];
//    [dateformatter setDateFormat:@"dd MMM yyyy HH:mm a"];
//    NSDate *assignDate = [dateformatter dateFromString:assignDateString];
//    [dateformatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
//    assignDateString = [dateformatter stringFromDate:assignDate];
    
    //change assign date
    ramodel.roomAssignment_AssignedDate = assignDateString;
    
    //load user id of username
//    UserListModelV2 *userList = [[AdHocMessageManagerV2 sharedAdHocMessageManagerV2] loadUserListByUserFullName:btnReassignTo.titleLabel.text];
    
    //change housekeeper id
    ramodel.roomAssignmentHousekeeperId = userChoosen.userListId;
    
    //change post status room asssignment
    ramodel.roomAssignment_PostStatus = POST_STATUS_SAVED_UNPOSTED;
    
    //not set remark yet
    
    //change expect cleaning time
    ramodel.roomAssignmentRoomExpectedCleaningTime = [btnRecleaningTime.titleLabel.text integerValue];
    
    //update db
    //    [[RoomManagerV2 sharedRoomManager] update:ramodel];
    ReassignRoomAssignmentModel *remodel = [[ReassignRoomAssignmentModel alloc] init];
    remodel.reassignRaId = ramodel.roomAssignment_Id;
    remodel.reassignUserId = ramodel.roomAssignment_UserId;
    remodel.reassignRoomId = ramodel.roomAssignment_RoomId;
    remodel.reasignExpectedCleaningTime = ramodel.roomAssignmentRoomExpectedCleaningTime;
    remodel.reassignRoomStatus = ramodel.roomAssignmentRoomStatusId;
    remodel.reassignCleaningStatus = ramodel.roomAssignmentRoomCleaningStatusId;
    
    //An-rework
    RoomManagerV2 *roommgr = [[RoomManagerV2 alloc] init];
    RoomModelV2 *model = [[RoomModelV2 alloc] init];
    model.room_Id = ramodel.roomAssignment_RoomId;
    model.room_is_re_cleaning = 1;
    
    
    
//    NSDateFormatter *formatter;
//    formatter  = [[NSDateFormatter alloc] init];
//    [dateformatter setDateFormat:@"dd-MM-yyyy HH:mm:ss"];
    
    
    RoomAssignmentModelV2 *roomassign = [[RoomAssignmentModelV2 alloc] init];
    roomassign.roomAssignment_Id = ramodel.roomAssignment_Id;
    roomassign.roomAssignment_UserId = ramodel.roomAssignment_UserId;
    roomassign.roomAssignment_RoomId = ramodel.roomAssignment_RoomId;
    roomassign.roomAssignmentRoomCleaningStatusId = ENUM_CLEANING_STATUS_PENDING_SECOND_TIME;
    roomassign.roomAssignment_AssignedDate = assignDateString;
    roomassign.roomAssignment_Priority = [roommgr loadMinPrioprity] - 1; 
    
        [roommgr reassignRoom: model andRoomAssignmentModelV2:roomassign];
    remodel.reassignDatetime =assignDateString;
    
//    [formatter Release];
    remodel.reassignHousekeeperId = ramodel.roomAssignmentHousekeeperId;
    remodel.reassignRoomType = roomModel.room_TypeId;
    remodel.reassignFloorId = roomModel.room_floor_id;
    remodel.reassignGuestProfileId = ramodel.roomAssignmentGuestProfileId;
    remodel.reassignHotelId = roomModel.room_HotelId;
    //An Rework
    ReassignRoomAssignmentModel* dbRoomAssign = nil;
   dbRoomAssign = [[RoomManagerV2 sharedRoomManager] loadReassignRoomAssignmentModelByPrimaryKey:remodel];
    BOOL isInserted = NO;
    if(dbRoomAssign==nil){
     isInserted = [[RoomManagerV2 sharedRoomManager] insertReassignRoomAssignmentModel:remodel];
    }
    else if (isInserted == NO) {
        [[RoomManagerV2 sharedRoomManager] updateReassignRoomAssignmentModel:remodel];
    }
    
    //post to server
    SyncManagerV2 *syncmanager = [[SyncManagerV2 alloc] init];
    if([syncmanager postReAssignRoomWithReAssignModel:remodel])
    {
        
    }
    
    //hide HUD
    [HUD hide:YES];
}

#pragma mark - === Handle Check Save Data ===
#pragma mark
-(BOOL) checkSaveDataWithTagEvent:(NSInteger) tagOfEvent {
    
    if (roomIsCleaning == ROOM_IS_COMPLETED) {
        //check saved?
        if (isSaved == NO) {
            UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ALERT_SAVECOUNTS] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_YES] otherButtonTitles:[[LanguageManagerV2 sharedLanguageManager] getNo], nil]; 
            [alert show];
            
            //need to save before leaving
            return YES;
        }
        
        //NO
        return NO; //no need to save before leaving
    } else {
        //YES
        //need to save before leaving
        
        //check saved?
        if (isSaved == NO) {
            UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ALERT_SAVECOUNTS] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_YES] otherButtonTitles:[[LanguageManagerV2 sharedLanguageManager] getNo], nil]; 
            [alert show];
        } else {
        
            UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_complete_room_to_leave] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_YES] otherButtonTitles:[[LanguageManagerV2 sharedLanguageManager] getNo], nil]; 
            [alert show];
        }
        
        return YES;
    }
}

#pragma mark - === Handle Back Button ===
#pragma mark

-(void)backBarPressed {
    //check saved?
    if (isSaved == NO) {
        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ALERT_SAVECOUNTS] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_YES] otherButtonTitles:[[LanguageManagerV2 sharedLanguageManager] getNo], nil]; 
        [alert show];
    } else
        [self.navigationController popViewControllerAnimated:YES];
}

-(void)addBackButton {
    //Back button
    btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
    
    UIImage *img = [UIImage imageNamed:@"btn_back.png"];
    [btnBack setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK] forState:UIControlStateNormal];
    [btnBack setBackgroundImage:img forState:UIControlStateNormal];
    [btnBack addTarget:self action:@selector(backBarPressed) forControlEvents:UIControlEventTouchUpInside];
    [btnBack setFrame:CGRectMake(0, 0,WIDTH_BACK_BUTTON , HEIGHT_BACK_BUTTON)];
    [btnBack.titleLabel setFont:[UIFont boldSystemFontOfSize:FONT_BACK_BUTTON]];
    [btnBack setContentEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
    UIView *backButtonView = [[UIView alloc] initWithFrame:btnBack.frame];
    backButtonView.bounds = CGRectMake(backButtonView.bounds.origin.x, Y_BACK_BUTTON, backButtonView.bounds.size.width, backButtonView.bounds.size.height);
    [backButtonView addSubview:btnBack];
    
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButtonView];
    self.navigationItem.leftBarButtonItem = barButtonItem;
}

#pragma mark - === HUD Delegate Methods ===
#pragma mark
-(void)hudWasHidden {
    [HUD removeFromSuperview];
    HUD = nil;
}

#pragma mark - === Show Data ===
#pragma mark
-(void)showData {
    //show HUD when loading data
    HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_RA_LOADING_DATA]];
    [HUD show:YES];
    [HUD setDelegate:self];
    [self.tabBarController.view addSubview:HUD];
    
    //show room no
    [lblRoomNo setText:[NSString stringWithFormat:@"%@ %@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_room_no_assign_room], roomModel.room_Id]];
    
    //load current room assignment
    RoomAssignmentModelV2 *roomassign = [[RoomAssignmentModelV2 alloc] init];
    roomassign.roomAssignment_Id = [TasksManagerV2 getCurrentRoomAssignment];
    roomassign.roomAssignment_UserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    [[RoomManagerV2 sharedRoomManager] load:roomassign];
    self.ramodel = roomassign;
    
    //load room status
    RoomStatusModelV2 *roomStatus = [[RoomStatusModelV2 alloc] init];
    roomStatus.rstat_Id = ramodel.roomAssignmentRoomStatusId;
    [[RoomManagerV2 sharedRoomManager] loadRoomStatusData:roomStatus];
    
    //show room status
    [lblRoomStatus setText:[[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString:(NSString *) ENGLISH_LANGUAGE] ? roomStatus.rstat_Name : roomStatus.rstat_Lang];
    
    //show image room status
    [imgRoomStatus setImage:[UIImage imageWithData:roomStatus.rstat_Image]];
    
    //show guest name
    if (roomModel.room_Guest_Name != nil) {
        [lblGuestName setText:[NSString stringWithFormat:@"- %@", roomModel.room_Guest_Name]];
    } else {
        //load Guest name
        GuestInfoModelV2 *gmodel = [[GuestInfoModelV2 alloc] init];
        gmodel.guestId = ramodel.roomAssignment_Id;
        [[RoomManagerV2 sharedRoomManager] loadGuestInfo:gmodel];
        [lblGuestName setText:[NSString stringWithFormat:@"- %@", gmodel.guestName]];
    }
    
    
    //load user list
    self.userlist = [[AdHocMessageManagerV2 sharedAdHocMessageManagerV2] loadAllUserListBySupervisorId:ramodel.roomAssignment_UserId];
    //get user list if data does not have
    if (self.userlist.count == 0) {
        if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp] == YES) {
            [[AdHocMessageManagerV2 sharedAdHocMessageManagerV2] getUserListWith:ramodel.roomAssignment_UserId andHotelID:[[[UserManagerV2 sharedUserManager] currentUser] userHotelsId] andLastModified:nil AndPercentView:HUD];
        }
        
        self.userlist = nil;
        self.userlist = [[AdHocMessageManagerV2 sharedAdHocMessageManagerV2] loadAllUserListBySupervisorId:ramodel.roomAssignment_UserId];
        
    }
    //show user
    for (UserListModelV2 *userlistmodel in self.userlist) {
        if (userlistmodel.userListId == ramodel.roomAssignmentHousekeeperId) {
            int indexofmodel = [self.userlist indexOfObject:userlistmodel];
            self.userChoosen = userlistmodel;
            [pickerReassignTo selectRow:indexofmodel inComponent: 0 animated:NO];
            if ([[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString:(NSString *) ENGLISH_LANGUAGE])  {
                [btnReassignTo setTitle:userlistmodel.userListFullName forState:UIControlStateNormal];
            } else {
                [btnReassignTo setTitle:userlistmodel.userListFullNameLang forState:UIControlStateNormal];
            }
           
          
        }
    }
    
    
    //show reassign date to
    NSDateFormatter *dateformater = [[NSDateFormatter alloc] init];
    selectedDate = [NSDate date];
    selectedTime = [NSDate date];
    
    [dateformater setDateFormat:@"dd MMM yyyy"];
    [btnReassignDateTo setTitle:[dateformater stringFromDate:selectedDate] forState:UIControlStateNormal];
    
    //show reassign time
    [dateformater setDateFormat:@"HH:mm"];
    [btnReassignTimeAt setTitle:[dateformater stringFromDate:selectedTime] forState:UIControlStateNormal];
    
    //show recleaning time
    [btnRecleaningTime setTitle:@"15" forState:UIControlStateNormal];
    
    //load data for pickers
    [self setReassignDateToDataWithRoomAssignmentModel:ramodel];
    
    [self loadRecleaningTimes];
    
    [pickerReassignTo reloadAllComponents];
    
    //hide HUD
    [HUD hide:YES];
}

#pragma mark - === Picker View Datasource methods ===
#pragma mark
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (pickerView == pickerReassignTo) {
        //return number of userlist
        
        return [self.userlist count];
    }
    
    if (pickerView == pickerRecleaningTime) {
        //return number of recleaning times
        return [self.recleaningTimes count];
    }
    
    //default is 0
    return 0;
}

-(UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 300, 37)];

    if (pickerView == pickerReassignTo) {
        UserListModelV2 *user = [userlist objectAtIndex:row];
        
        if ([[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString:(NSString *)ENGLISH_LANGUAGE] == YES) {
            [label setText: user.userListFullName];
        } else 
            [label setText: user.userListFullNameLang];
    }
    
    if (pickerView == pickerRecleaningTime) {
        NSDictionary *dictionary = [recleaningTimes objectAtIndex:row];
        [label setText: [dictionary objectForKey:kRecleaningTimeValue]];
    }
    
    [label setFont:[UIFont boldSystemFontOfSize:18]];
    label.textAlignment = UITextAlignmentCenter;
    label.backgroundColor = [UIColor clearColor];
    return label;
}

#pragma mark - === Picker View Delegate methods ===
#pragma mark
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
}


#pragma mark - === Remark Delgate Methods ===
#pragma mark
-(void)remarkViewV2DoneWithText:(NSString *)text andController:(UIViewController *)controllerView {
    [lblRemarkValue setText:text];
    
    //adjust label topleft
    //[lblRemarkValue sizeToFitFixedWidth:193];
    CGRect frame = lblRemarkValue.frame;
    if (frame.size.height > 73) {
        frame.size.height = 73;
        [lblRemarkValue setFrame:frame];
        [lblRemarkValue setLineBreakMode:UILineBreakModeTailTruncation];
        [lblRemarkValue setNumberOfLines:4];
    }
}

#pragma mark - === Set Reassign Date Picker data ===
#pragma mark
-(void)setReassignDateToDataWithRoomAssignmentModel:(RoomAssignmentModelV2 *)model {
    [pickerDate setMinimumDate:[NSDate date]];
    [pickerDate setMaximumDate:[NSDate dateWithTimeIntervalSinceNow:24 * 60 * 60]];
}

#pragma mark - === Set Recleaning Time data ===
#pragma mark
-(void)loadRecleaningTimes {
    if (recleaningTimes == nil) {
        self.recleaningTimes = [NSMutableArray array];
    }
    
    NSMutableDictionary *cleaningtime = [NSMutableDictionary dictionary];
    [cleaningtime setObject:@"15" forKey:kRecleaningTimeValue];
    [recleaningTimes addObject:cleaningtime];
    
    cleaningtime = [NSMutableDictionary dictionary];
    [cleaningtime setObject:@"20" forKey:kRecleaningTimeValue];
    [recleaningTimes addObject:cleaningtime];
    
    cleaningtime = [NSMutableDictionary dictionary];
    [cleaningtime setObject:@"25" forKey:kRecleaningTimeValue];
    [recleaningTimes addObject:cleaningtime];
    
    cleaningtime = [NSMutableDictionary dictionary];
    [cleaningtime setObject:@"30" forKey:kRecleaningTimeValue];
    [recleaningTimes addObject:cleaningtime];
    
    [pickerRecleaningTime reloadAllComponents];
}

#pragma mark - === Show/Hide Choose Data View ===
#pragma mark
-(void)showChooseDataView {
    
    //Move to center vertical
    CGRect titleViewBounds = self.navigationController.navigationBar.topItem.titleView.bounds;
    self.navigationController.navigationBar.topItem.titleView.bounds = CGRectMake(titleViewBounds.origin.x, Y_TOPBAR_TITLE, titleViewBounds.size.width, titleViewBounds.size.height);
    
    CGRect frame = chooseDataView.frame;
    frame.origin.y = 480;
    [chooseDataView setFrame:frame];
    
    [self.tabBarController.view addSubview:chooseDataView];
    
    [UIView beginAnimations:@"begin Animation" context:nil];
    [UIView setAnimationDuration:0.5];
    
    frame.origin.y = 0;
    [chooseDataView setFrame:frame];
    
    [UIView commitAnimations];
}

-(void)hideChooseDataView {
    CGRect frame = chooseDataView.frame;
    
    [UIView beginAnimations:@"begin Animation" context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDidStopSelector:@selector(removeChooseDataView)];
    
    frame.origin.y = 480;
    [chooseDataView setFrame:frame];
    
    [UIView commitAnimations];
}

-(void)removeChooseDataView {
    [chooseDataView removeFromSuperview];
}

-(void)hideAllPickerViews {
    [pickerDate setHidden:YES];
    [pickerReassignTo setHidden:YES];
    [pickerRecleaningTime setHidden:YES];
    [pickerTime setHidden:YES];
}

#pragma mark - === UIAlertDelegate Methods ===
#pragma mark
-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    switch (buttonIndex) {
        case 0:
            //YES
            [self saveReassign];
            break;
            
        case 1:
            //NO
            [self.navigationController popViewControllerAnimated:YES];
            break;
            
        default:
            break;
    }
}















@end
