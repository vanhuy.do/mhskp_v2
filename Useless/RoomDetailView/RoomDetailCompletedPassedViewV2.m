//
//  RoomDetailView.m
//  EHouseKeeping
//
//  Created by tms on 5/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "RoomDetailCompletedPassedViewV2.h"
#import "sqlite3.h"
#import "MBProgressHUD.h"
#import "NetworkCheck.h"
//#import "RoomAssignmentModel.h"
//#import "TasksManager.h"
#import "RoomManagerV2.h"
#import "LanguageManager.h"
#import "RoomInfoCompletedPassedV2.h"
#import "RemarkViewV2.h"
#import "GuidelineViewV2.h"
#import "CountPopupReminderViewV2.h"
#import "CheckListManagerV2.h"
#import "TasksManagerV2.h"
#import "CheckListForMaidViewV2.h"

#define tagCleaningStatus 12
#define tagLabel 1234
#define tagText 5678
#define tagImage 9101

#define tagAlertRoomStatusChange 200
#define tagAlertBlockRoom 300
#define tagAlertCleaningStatusCompleted 400
#define kTagAlertDoneWithRoomCleaning 500

#define xOfSccessory 280
#define space2ElementCell 10


#define IMAGE_PAUSE_ON  @"phuse_bt_on_299x58.png"
#define IMAGE_PAUSE_GRAY  @"phuse_bt_on_299x58.png"
#define IMAGE_PAUSE @"phuse_bt_299x58.png"

#define IMAGE_TIMER_PLAY @"timer_play_274x58.png"
#define IMAGE_TIMER_PAUSE @"timer_phuse_274x58.png"
#define IMAGE_TIMER_GRAY @"timer_gray_274x58.png"
#define IMAGE_TIMER_OVER @"timer_red_274x58.png"

#define IMAGE_COMPLETE @"click_bt_299x58.png"
#define IMAGE_COMPLETE_GRAY @"click_bt_gray_299x58.png"
#define IMAGE_COMPLETE_ON @"click_bt_on_299x58.png"

#define IMAGE_PLAY @"play_bt_299x58.png"
#define IMAGE_PLAY_GRAY @"play_bt_gray_299x58.png"
#define IMAGE_PLAY_ON @"play_bt_on_299x58.png"

#define kTimeFormat @"HH:mm:ss"
#define kTime12HoursFormat @"hh:mm a"

@interface RoomDetailCompletedPassedViewV2 (PrivateMethods)

-(NSString*) timeDisplayWithSecond:(NSInteger)time;
@end


@implementation RoomDetailCompletedPassedViewV2
@synthesize tableView,expectedCleaningTime,guestPref,additioJob,cleaningStatus,scrollView,listButtonView;
@synthesize btnCount,btnEngineeringAssitance,btnGuideLine,btnLaundry,btnLostAnDFound, dataModel,indexPathCleaaningStatus;
@synthesize roomStatus,remark,estimatedInspectionTime,actualInspectionTime,inspectionStatus;
@synthesize reassignBtn;
@synthesize superController;
@synthesize isPass;
@synthesize isInspected;
@synthesize isCompleted;
//@synthesize delegate;
@synthesize roomRecord;
@synthesize roomRemarkModel;
@synthesize msgbtnOk;
@synthesize msgSaveRoomDetail;
@synthesize raDataModel;
int hours, minutes, seconds;
int secondsLeft;



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if (self == [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        self.title = @"Room Details";
    }
    return self;
}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}


#pragma mark - View lifecycle
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //set captions view
    [self setCaptionsView];
    
    if(animated == NO){
        //
        
        if(self.roomRecord == nil){
            self.roomRecord = [[RoomRecordModelV2 alloc] init];
            self.roomRecord.rrec_User_Id = [[UserManagerV2 sharedUserManager] currentUser].userId;
            self.roomRecord.rrec_room_assignment_id = [TasksManagerV2 getCurrentRoomAssignment] ;
            
            self.roomRecord = [[RoomManagerV2 sharedRoomManager] loadRoomRecordDataByRoomIdAndUserId:self.roomRecord];
            //            if(self.roomRecord.rrec_Id <=0){
            //                self.roomRecord.rrec_PostStatus = POST_STATUS_SAVED_UNPOSTED;
            //                [[RoomManagerV2 sharedRoomManager] insertRoomRecordData:self.roomRecord];
            //                self.roomRecord = [[RoomManagerV2 sharedRoomManager] loadRoomRecordDataByRoomIdAndUserId:self.roomRecord];
            //            }
        }
    }
    
    if(!animated && ![CommonVariable sharedCommonVariable].isBackFromChk){
        [[RoomManagerV2 sharedRoomManager] loadRoomModel:self.dataModel]; // load room data
    }
    
    [self.tableView reloadData];
}



/***************************************/
// layout buttons
-(void)displayButtons{
    /********* room is completed *********/
    if(self.raDataModel.roomAssignmentRoomInspectionStatusId == ENUM_INSPECTION_COMPLETED_FAIL ||
       self.raDataModel.roomAssignmentRoomInspectionStatusId == ENUM_INSPECTION_COMPLETED_PASS){
        /********* room is pass *********/
        [lockButton setHidden:YES];
        [countTimeButton setHidden:YES];
        [timerButon setHidden:YES];
        [finishTimeButton setHidden:YES];
        [btnAction setHidden:YES];
        
        roomDetailStatus = ENUM_MAID_INSPECTED_COMPLETED; //  roomDetail Status
        
        if (raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VI || raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VC) {
            if ([[[UserManagerV2 sharedUserManager] currentUser] userAllowBlockRoom] == 0) {
                [lockButton setImage:[UIImage imageNamed:IMAGE_BLOCK_OFF] forState:UIControlStateNormal];
                [lockButton setUserInteractionEnabled:NO];
                [lockButton setHidden:NO];
            } else {
                [lockButton setHidden:YES];
            }
        }
        
    }
    else {
        [countTimeButton setHidden:YES];
        [timerButon setHidden:YES];
        [finishTimeButton setHidden:YES];
        [checkListButton setHidden:YES];
        
        
        [btnAction setFrame:CGRectMake(countTimeButton.frame.origin.x, countTimeButton.frame.origin.y, btnAction.frame.size.width, btnAction.frame.size.height)];
        
        [lockButton setFrame:CGRectMake(btnAction.frame.origin.x, btnAction.frame.origin.y + btnAction.frame.size.height + 4, checkListButton.frame.size.width, checkListButton.frame.size.height)];
        roomDetailStatus = ENUM_MAID_NOT_INSPECTED_COMPLETED;  //  roomDetail Status
    }
}



/***************************************/

- (void)viewDidLoad
{
    [super viewDidLoad];
    //set frame of this view .
    
    self.expectedCleaningTime= [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_EXPECTED_CLEANING_TIME];
    self.guestPref= [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_GUEST_REFERENCE];
    self.additioJob= [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ADDITIONAL_JOB];
    self.cleaningStatus= [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_CLEANING_STATUS];
    self.roomStatus = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_room_status];
    self.remark = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_remark_title];
    self.estimatedInspectionTime = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_estimated_inspection_time];
    self.actualInspectionTime = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_actual_inspection_time];
    self.inspectionStatus = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_room_inspectionstatus];
    self.tableView.backgroundColor=[UIColor clearColor];
    
    //@ close cleaning status view
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(closeCleaningStatusView) name:@"closeCleaningStatusView" object:nil];
    
    //@ notification saving cleaning status of this view.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(saveCurrentCleaningStatus:) name:[NSString stringWithFormat:@"%@", notificationSaveCleaningStatus] object:nil];
    
    //add Observer to catch notification reload all data when sync done
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadDataWhenSyncDone) name:[NSString stringWithFormat: @"%@", notificationReloadDataAllViewWhenSyncDone] object:nil];
    
    [self displayButtons];
    
    
    timeLeftCleaningStatus = raDataModel.roomAssignmentRoomExpectedCleaningTime*60;
    timeLeftInspectionStatus = raDataModel.roomAssignmentRoomExpectedInspectionTime*60;
    
    [timerButon setBackgroundImage:[UIImage imageNamed:IMAGE_TIMER_GRAY] forState:UIControlStateNormal];
    [self displayTimerButton];
    
    countTimeStatus = NOT_START;
    [finishTimeButton setUserInteractionEnabled:NO];
    
    timeFomarter = [[NSDateFormatter alloc] init];
    [timeFomarter setDateFormat:[ehkConvert getyyyy_MM_dd_HH_mm_ss]];
    
    // assign data for compare property edited:
    /* bxchinh
     dataModelCompare = [[RoomModelV2 alloc] init];
     dataModelCompare.room_CleaningStatusId = dataModel.room_CleaningStatusId;
     dataModelCompare.room_StatusId = dataModel.room_StatusId;
     
     */
}

#pragma didden after save.

-(void) hiddenHUDAfterSaved:(MBProgressHUD *)HUD{
    [HUD hide:YES];
    [HUD removeFromSuperview];
}

- (void)viewDidUnload
{
    [self setReassignBtn:nil];
    //    [timerButon release];
    timerButon = nil;
    //    [countDownTimerLabel release];
    countDownTimerLabel = nil;
    //    [finishTimeButton release];
    finishTimeButton = nil;
    //    [countTimeButton release];
    countTimeButton = nil;
    //    [checkListButton release];
    checkListButton = nil;
    //    [lockButton release];
    lockButton = nil;
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark
#pragma mark delegate/datasource tableview

- (void)addAccessoryButton:(UITableViewCell *)cell  {
    /*****************************************/
    UIImageView *accessoryButton = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 24, 24)];
    [accessoryButton setBackgroundColor:[UIColor clearColor]];
    [accessoryButton setImage:[UIImage imageNamed:@"gray_arrow_36x36.png"]];
    [cell setAccessoryView:accessoryButton];
    cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
    /*****************************************/
}

-(NSInteger)tableView:(UITableView *)atableView numberOfRowsInSection:(NSInteger)section {
    return 7;
}

/****************************** implement cell *************************************/
- (void)setupAdditionalCell:(UITableViewCell *)cell isNew:(BOOL)isNew
{
    CGRect additionalJobLabelRect = CGRectMake(10, 1, 150 , 30);
    // display label control in table view
    
    UILabel *actualCleaningTimeLabel;
    
    if (isNew == TRUE) {
        actualCleaningTimeLabel = [[UILabel alloc] initWithFrame:additionalJobLabelRect];
    } else {
        actualCleaningTimeLabel = (UILabel *)[cell.contentView viewWithTag:tagLabel];
    }
    
    actualCleaningTimeLabel.textAlignment = UITextAlignmentLeft;
    actualCleaningTimeLabel.font = [UIFont boldSystemFontOfSize:12];
    actualCleaningTimeLabel.text = self.additioJob;
    actualCleaningTimeLabel.tag = tagLabel;
    
    UIImage *img=[UIImage imageNamed:@"tab_small.png"];
    actualCleaningTimeLabel.textColor=[UIColor colorWithPatternImage:img];
    
    [actualCleaningTimeLabel setBackgroundColor:[UIColor clearColor]];
    if (isNew == TRUE) {
        [cell.contentView addSubview:actualCleaningTimeLabel];
        
    }
    
    
    //label 2 of this cell
    NSString *tmp = dataModel.room_AdditionalJob;
    CGSize t = [tmp sizeWithFont:[UIFont boldSystemFontOfSize:12] constrainedToSize:CGSizeMake(135, 300) lineBreakMode:UILineBreakModeWordWrap];
    CGRect additionalJobValueRect;
    if (t.height > 20) {
        additionalJobValueRect = CGRectMake(160, 9, 135 , t.height);
    }
    else{
        additionalJobValueRect = CGRectMake(160, 1, 135 , 30);
    }
    UILabel *additionalJobValue;
    
    if (isNew == TRUE) {
        additionalJobValue=[[UILabel alloc] initWithFrame:additionalJobValueRect];
    } else {
        additionalJobValue = (UILabel *)[cell.contentView viewWithTag:tagText];
    }
    
    additionalJobValue.textAlignment = UITextAlignmentRight;
    additionalJobValue.font = [UIFont boldSystemFontOfSize:12];
    additionalJobValue.lineBreakMode = UILineBreakModeWordWrap;
    int tmprow = floor(t.height/20) + 1;
    additionalJobValue.numberOfLines = tmprow;
    additionalJobValue.tag = tagText;
    
    //set additionalJobValue from dataModel
    [additionalJobValue setText:dataModel.room_AdditionalJob];
    
    [additionalJobValue setBackgroundColor:[UIColor clearColor]];
    
    UIImageView* imageView = (UIImageView *)[cell.contentView viewWithTag:tagImage];
    [imageView removeFromSuperview];
    
    if (isNew == TRUE) {
        [cell.contentView addSubview:additionalJobValue];
        
    }
}

// display time with format : ##hour ##minute
-(NSString*) timeDisplay:(NSInteger)time{
    NSInteger hoursInt = time/3600;
    NSInteger minutesInt = (time - (hoursInt*3600)) / 60;
    
    // setup hour string
    NSString *hoursString = [NSString stringWithFormat:@"%d",hoursInt];
    NSString *minutesString = [NSString stringWithFormat:@"%d",minutesInt];
    
    if([hoursString length]==1){
        hoursString = [NSString stringWithFormat:@"%.2d %@",[hoursString integerValue],[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_strhour]];
        if(hoursInt == 0){
            hoursString = @"";
        }
    } else {
        hoursString = [NSString stringWithFormat:@"%@ %@",hoursString,[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_strmins]];
    }
    
    // setup minute string
    if([minutesString length]==1){
        minutesString = [NSString stringWithFormat:@"%.2d %@",[minutesString integerValue],[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_strmins]];
        if(minutesInt == 0){
            minutesString = @"";
        }
        
    }else{
        minutesString = [NSString stringWithFormat:@"%@ %@",minutesString,[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_strmins]];
    }
    
    return [NSString stringWithFormat:@"%@ %@ ",hoursString,minutesString];
}


- (void)setupExpectedCleaningCell:(UITableViewCell *)cell isNew:(BOOL)isNew
{
    CGRect expectedCleaningLabelRect = CGRectMake(10, 1, 200 , 30);
    // display label control in table view
    UILabel *expectedCleaningLabel;
    if (isNew == TRUE) {
        expectedCleaningLabel = [[UILabel alloc] initWithFrame:expectedCleaningLabelRect];
    } else {
        expectedCleaningLabel = (UILabel *)[cell.contentView viewWithTag:tagLabel];
    }
    
    expectedCleaningLabel.textAlignment = UITextAlignmentLeft;
    expectedCleaningLabel.font = [UIFont boldSystemFontOfSize:12];
    expectedCleaningLabel.text = self.expectedCleaningTime;
    expectedCleaningLabel.tag = tagLabel;
    UIImage *img=[UIImage imageNamed:@"tab_small.png"];
    expectedCleaningLabel.textColor=[UIColor colorWithPatternImage:img];
    
    [expectedCleaningLabel setBackgroundColor:[UIColor clearColor]];
    if (isNew == TRUE) {
        [cell.contentView addSubview:expectedCleaningLabel];
        
    }
    
    //label 2 of this cell
    CGRect expectedCleaningValueRect = CGRectMake(160, 1, 135 , 30);
    
    UILabel *expectedCleaningTimeValue;
    if (isNew == TRUE) {
        expectedCleaningTimeValue=[[UILabel alloc] initWithFrame:expectedCleaningValueRect];
    } else {
        expectedCleaningTimeValue = (UILabel *)[cell.contentView viewWithTag:tagText];
    }
    
    expectedCleaningTimeValue.textAlignment = UITextAlignmentRight;
    expectedCleaningTimeValue.font = [UIFont boldSystemFontOfSize:12];
    expectedCleaningTimeValue.tag = tagText;
    // set expectedCleaningTimeValue from dataModel
    [expectedCleaningTimeValue setText:[self timeDisplay:60*raDataModel.roomAssignmentRoomExpectedCleaningTime]];
    
    [expectedCleaningTimeValue setBackgroundColor:[UIColor clearColor]];
    
    UIImageView* imageView = (UIImageView *)[cell.contentView viewWithTag:tagImage];
    [imageView removeFromSuperview];
    
    if (isNew == TRUE) {
        [cell.contentView addSubview:expectedCleaningTimeValue];
        
    }
}

// display time with format : ##hour ##minute ## seconds
-(NSString*) timeDisplayWithSecond:(NSInteger)time{
    NSInteger hoursInt = time/3600;
    NSInteger minutesInt = (time - (hoursInt*3600)) / 60;
    
    // setup hour string
    NSString *hoursString = [NSString stringWithFormat:@"%d",hoursInt];
    NSString *minutesString = [NSString stringWithFormat:@"%d",minutesInt];
    
    if(hoursInt <= 1){
        hoursString = [NSString stringWithFormat:@"%.2d %@",[hoursString integerValue],[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_strhour]];
        if(hoursInt == 0){
            hoursString = @"";
        }
    } else {
        hoursString = [NSString stringWithFormat:@"%@ %@",hoursString,[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_strhour]];
    }
    
    // setup minute string
    if(minutesInt <= 1){
        minutesString = [NSString stringWithFormat:@"%.2d %@",[minutesString integerValue],[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_strhour]];
        if(minutesInt == 0){
            minutesString = @"";
        }
        
    }else{
        minutesString = [NSString stringWithFormat:@"%@ %@",minutesString,[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_strmins]];
    }
    
    return [NSString stringWithFormat:@"%@ %@",hoursString,minutesString];
}


- (void)actualTimeOfTaskStatus:(UILabel *)label  {
    /*******************************************/
    // get time pause
    NSMutableArray *listRoomRemark = [[RoomManagerV2 sharedRoomManager] loadAllRoomRemarkByRecordId:self.roomRecord.rrec_Id];
    
    NSInteger timePause = 0;
    for (int index = 0; index < [listRoomRemark count]; index++) {
        RoomRemarkModelV2 *roomRemark =(RoomRemarkModelV2*) [listRoomRemark objectAtIndex:index];
        timePause += [roomRemark.rm_clean_pause_time intValue];
    }
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:[ehkConvert getyyyy_MM_dd_HH_mm_ss]];
    
    NSDate* startDate;
    NSDate* endDate;
    
    startDate = [timeFomarter dateFromString:self.roomRecord.rrec_Cleaning_Start_Time];
    endDate = [timeFomarter dateFromString:self.roomRecord.rrec_Cleaning_End_Time];
    
    
    NSTimeInterval timeDifference = [endDate timeIntervalSinceDate:startDate];
    
    // get time spend for pause:
    NSInteger secondDiffrence = (NSInteger)timeDifference;
    secondDiffrence = secondDiffrence - timePause;
    
    NSString *minutesString;
    NSString *hoursString;
    
    NSInteger minutesInt;
    NSInteger hoursInt;
    NSString *timeDisplay = @"";
    
    
    hoursInt = secondDiffrence/3600;
    minutesInt = (secondDiffrence - (hoursInt*3600)) / 60;
    
    hoursString = [NSString stringWithFormat:@"%d %@",hoursInt,[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_strhour]];
    minutesString = [NSString stringWithFormat:@"%d %@",minutesInt,[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_strmins]];
    
    
    if([hoursString length]==1){
        hoursString = [NSString stringWithFormat:@"%@ %@",hoursString,[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_strhour]];
    }
    
    if([minutesString length]==1){
        minutesString = [NSString stringWithFormat:@"%@ %@",minutesString,[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_strmins]];
    }
    
    
    timeDisplay = [NSString stringWithFormat:@"%@ %@",hoursString,minutesString];
    
    
    /*******************************************/
    
    [label setText:timeDisplay];
    [label setBackgroundColor:[UIColor clearColor]];
}

- (void)actualTimeOfinspection:(UILabel *)label  {
    /*******************************************/
    // get time pause
    NSMutableArray *listRoomRemark = [[RoomManagerV2 sharedRoomManager] loadAllRoomRemarkByRecordId:self.roomRecord.rrec_Id];
    
    NSInteger timePause = 0;
    for (int index = 0; index < [listRoomRemark count]; index++) {
        RoomRemarkModelV2 *roomRemark =(RoomRemarkModelV2*) [listRoomRemark objectAtIndex:index];
        timePause += [roomRemark.rm_clean_pause_time intValue];
    }
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:[ehkConvert getyyyy_MM_dd_HH_mm_ss]];
    
    NSDate* startDate;
    NSDate* endDate;
    startDate = [timeFomarter dateFromString:self.roomRecord.rrec_Inspection_Start_Time];
    endDate = [timeFomarter dateFromString:self.roomRecord.rrec_Inspection_End_Time];
    
    NSTimeInterval timeDifference = [endDate timeIntervalSinceDate:startDate];
    
    // get time spend for pause:
    NSInteger secondDiffrence = (NSInteger)timeDifference;
    secondDiffrence = secondDiffrence - timePause;
    
    NSString *minutesString;
    NSString *hoursString;
    
    NSInteger minutesInt;
    NSInteger hoursInt;
    NSString *timeDisplay = @"";
    
    
    hoursInt = secondDiffrence/3600;
    minutesInt = (secondDiffrence - (hoursInt*3600)) / 60;
    
    hoursString = [NSString stringWithFormat:@"%d %@",hoursInt,[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_strhour]];
    minutesString = [NSString stringWithFormat:@"%d %@",minutesInt,[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_strmins]];
    
    
    if([hoursString length]==1){
        hoursString = [NSString stringWithFormat:@"%@ %@",hoursString,[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_strhour]];
    }
    
    if([minutesString length]==1){
        minutesString = [NSString stringWithFormat:@"%@ %@",minutesString,[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_strmins]];
    }
    
    timeDisplay = [NSString stringWithFormat:@"%@ %@",hoursString,minutesString];
    
    
    /*******************************************/
    
    [label setText:timeDisplay];
    [label setBackgroundColor:[UIColor clearColor]];
}
//*********************** actual cleaning  time by supervisor *******************************************
- (void)actualCleaningTimeBySuperVisor:(UILabel *)label  {
    
    //Hao Tran - Remove for unneccessary function
    /*
     // get time pause
     
     NSInteger timePause = 0;
     timePause = [self.roomRecord.rrec_Cleaning_Total_Pause_Time intValue];
     
     NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
     [dateFormatter setDateFormat:[ehkConvert getyyyy_MM_dd_HH_mm_ss]];
     
     NSDate* startDate;
     NSDate* endDate;
     
     startDate = [timeFomarter dateFromString:self.roomRecord.rrec_Cleaning_Start_Time];
     endDate = [timeFomarter dateFromString:self.roomRecord.rrec_Cleaning_End_Time];
     
     
     
     NSTimeInterval timeDifference = [endDate timeIntervalSinceDate:startDate];
     
     // get time spend for pause:
     NSInteger secondDiffrence = (NSInteger)timeDifference;
     secondDiffrence = secondDiffrence - timePause;
     
     NSString *minutesString;
     NSString *hoursString;
     
     NSInteger minutesInt;
     NSInteger hoursInt;
     NSString *timeDisplay = @"";
     
     
     hoursInt = secondDiffrence/3600;
     minutesInt = (secondDiffrence - (hoursInt*3600)) / 60;
     
     hoursString = [NSString stringWithFormat:@"%d %@",hoursInt,[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_strhour]];
     minutesString = [NSString stringWithFormat:@"%d %@",minutesInt,[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_strmins]];
     
     
     if([hoursString length]==1){
     hoursString = [NSString stringWithFormat:@"%@ %@",hoursString,[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_strhour]];
     }
     
     if([minutesString length]==1){
     minutesString = [NSString stringWithFormat:@"%@ %@",minutesString,[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_strmins]];
     }
     
     
     timeDisplay = [NSString stringWithFormat:@"%@ %@",hoursString,minutesString];
     
     [label setText:timeDisplay];
     [label setBackgroundColor:[UIColor clearColor]];
     */
}
//*******************************************************************************************************

- (void)setupActualCleaningTimeCell:(UITableViewCell *)cell isNew:(BOOL)isNew
{
    CGRect actualCleaningTimeLabelRect = CGRectMake(10, 1, 150 , 30);
    // display label control in table view
    
    UILabel *actualCleaningLabel;
    
    if (isNew == TRUE) {
        actualCleaningLabel = [[UILabel alloc] initWithFrame:actualCleaningTimeLabelRect];
    } else {
        actualCleaningLabel = (UILabel *)[cell.contentView viewWithTag:tagLabel];
    }
    
    actualCleaningLabel.textAlignment = UITextAlignmentLeft;
    actualCleaningLabel.font = [UIFont boldSystemFontOfSize:12];
    actualCleaningLabel.text = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_actual_cleaning_time];
    actualCleaningLabel.tag = tagLabel;
    
    UIImage *img=[UIImage imageNamed:@"tab_small.png"];
    actualCleaningLabel.textColor=[UIColor colorWithPatternImage:img];
    
    [actualCleaningLabel setBackgroundColor:[UIColor clearColor]];
    if (isNew == TRUE) {
        [cell.contentView addSubview:actualCleaningLabel];
        
    }
    
    
    //label 2 of this cell
    NSString *tmp = dataModel.room_AdditionalJob;
    CGSize t = [tmp sizeWithFont:[UIFont boldSystemFontOfSize:12] constrainedToSize:CGSizeMake(135, 300) lineBreakMode:UILineBreakModeWordWrap];
    CGRect actualCleaningValueRect;
    if (t.height > 20) {
        actualCleaningValueRect = CGRectMake(160, 9, 135 , t.height);
    }
    else{
        actualCleaningValueRect = CGRectMake(160, 1, 135 , 30);
    }
    UILabel *actualCleaningValue;
    
    if (isNew == TRUE) {
        actualCleaningValue=[[UILabel alloc] initWithFrame:actualCleaningValueRect];
    } else {
        actualCleaningValue = (UILabel *)[cell.contentView viewWithTag:tagText];
    }
    
    actualCleaningValue.textAlignment = UITextAlignmentRight;
    actualCleaningValue.font = [UIFont boldSystemFontOfSize:12];
    actualCleaningValue.lineBreakMode = UILineBreakModeWordWrap;
    int tmprow = floor(t.height/20) + 1;
    actualCleaningValue.numberOfLines = tmprow;
    actualCleaningValue.tag = tagText;
    [actualCleaningValue setTextColor:[UIColor greenColor]];
    
    
    [self actualTimeOfTaskStatus: actualCleaningValue];
    
    if(timeLeftCleaningStatus<0){
        [actualCleaningValue setTextColor:[UIColor redColor]];
    }
    /////////////////////////////////////////////////////
    UIImageView* imageView = (UIImageView *)[cell.contentView viewWithTag:tagImage];
    [imageView removeFromSuperview];
    if (isNew == TRUE) {
        [cell.contentView addSubview:actualCleaningValue];
    }
}


- (void)setupCleaningStatusCell:(UITableViewCell *)cell isNew:(BOOL)isNew
{
    
    CGRect roomStatusLabelRect = CGRectMake(10, 1, 150 , 30);
    // display label control in table view
    
    UILabel *cleaningStatusLabel;
    
    if (isNew == TRUE) {
        cleaningStatusLabel = [[UILabel alloc] initWithFrame:roomStatusLabelRect];
    } else {
        cleaningStatusLabel = (UILabel *)[cell.contentView viewWithTag:tagLabel];
    }
    cleaningStatusLabel.textAlignment = UITextAlignmentLeft;
    cleaningStatusLabel.font = [UIFont boldSystemFontOfSize:12];
    cleaningStatusLabel.text = self.cleaningStatus;
    cleaningStatusLabel.tag = tagLabel;
    UIImage *img=[UIImage imageNamed:@"tab_small.png"];
    cleaningStatusLabel.textColor=[UIColor colorWithPatternImage:img];
    
    [cleaningStatusLabel setBackgroundColor:[UIColor clearColor]];
    if (isNew == TRUE) {
        [cell.contentView addSubview:cleaningStatusLabel];
        
    }
    
    //label 2 of this cell
    CGRect cleaningStatusValueRect = CGRectMake(190, 10, 105 , 30);
    UILabel *cleanigStatusValue;
    
    // align  text in 2 case: user and super user.
    
    if (isNew == TRUE) {
        cleanigStatusValue=[[UILabel alloc] initWithFrame:cleaningStatusValueRect];
    } else {
        cleanigStatusValue = (UILabel *)[cell.contentView viewWithTag:tagText];
    }
    
    
    
    cleanigStatusValue.textAlignment = UITextAlignmentRight;
    cleanigStatusValue.font = [UIFont boldSystemFontOfSize:12];
    
    //set cleaningStatusValue from dataModel
    CleaningStatusModelV2 *cleaningStatusModel=[[CleaningStatusModelV2 alloc] init];
    cleaningStatusModel.cstat_Id=raDataModel.roomAssignmentRoomCleaningStatusId;
    [[RoomManagerV2 sharedRoomManager] loadCleaningStatus:cleaningStatusModel];
    
    if ([[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString:[NSString stringWithFormat:@"%@",ENGLISH_LANGUAGE]]) {
        [cleanigStatusValue setText: cleaningStatusModel.cstat_Name];
    } else {
        [cleanigStatusValue setText: cleaningStatusModel.cstat_Language];
    }
    cleanigStatusValue.tag=tagText;
    
    [self addAccessoryButton: cell];
    /*****************************************/
    
    UIImageView *cleaningStatusImageView;
    if (isNew == TRUE) {
        cleaningStatusImageView = [[UIImageView alloc] initWithFrame:CGRectMake(160, 1, 30, 30)];
    } else {
        cleaningStatusImageView = (UIImageView *)[cell.contentView viewWithTag:tagImage];
    }
    cleaningStatusImageView.tag = tagImage;
    
    
    [cleaningStatusImageView setImage:[UIImage imageWithData:cleaningStatusModel.cstat_image]];
    [cleanigStatusValue setText: cleaningStatusModel.cstat_Name];
    
    /********* resize frame of image and label*************/
    CGSize size = [cleanigStatusValue.text sizeWithFont:cleaningStatusLabel.font];
    CGFloat x = xOfSccessory - size.width-space2ElementCell;
    [cleanigStatusValue setFrame:CGRectMake(x, cleanigStatusValue.frame.origin.y, size.width, size.height)];
    [cleaningStatusImageView setFrame:CGRectMake(x-cleaningStatusImageView.frame.size.width, cleaningStatusImageView.frame.origin.y, cleaningStatusImageView.frame.size.width, cleaningStatusImageView.frame.size.height)];
    /********* end resize frame of image and label*************/
    
    [cleanigStatusValue setBackgroundColor:[UIColor clearColor]];
    if (isNew == TRUE) {
        [cell.contentView addSubview:cleanigStatusValue];
        [cell.contentView addSubview:cleaningStatusImageView];
    }
    
}

- (void)roomStatusCell:(UITableViewCell *)cell isNew:(BOOL)isNew
{
    CGRect roomStatusLabelRect = CGRectMake(10, 1, 150 , 30);
    // display label control in table view
    
    UILabel *roomStatusLabel;
    
    if (isNew == TRUE) {
        roomStatusLabel = [[UILabel alloc] initWithFrame:roomStatusLabelRect];
    } else {
        roomStatusLabel = (UILabel *)[cell.contentView viewWithTag:tagLabel];
    }
    roomStatusLabel.textAlignment = UITextAlignmentLeft;
    roomStatusLabel.font = [UIFont boldSystemFontOfSize:12];
    roomStatusLabel.text = self.roomStatus;
    roomStatusLabel.tag = tagLabel;
    UIImage *img=[UIImage imageNamed:@"tab_small.png"];
    roomStatusLabel.textColor=[UIColor colorWithPatternImage:img];
    
    [roomStatusLabel setBackgroundColor:[UIColor clearColor]];
    if (isNew == TRUE) {
        [cell.contentView addSubview:roomStatusLabel];
        
    }
    
    //label 2 of this cell
    CGRect roomStatusValueRect = CGRectMake(190, 5, 105 , 30);
    UILabel *roomStatusValue;
    // align  text in 2 case: user and super user.
    
    if (isNew == TRUE) {
        roomStatusValue=[[UILabel alloc] initWithFrame:roomStatusValueRect];
    } else {
        roomStatusValue = (UILabel *)[cell.contentView viewWithTag:tagText];
    }
    
    
    roomStatusValue.textAlignment = UITextAlignmentRight;
    roomStatusValue.font = [UIFont boldSystemFontOfSize:FONT_SIZE_ROOM_STATUS];
    
    //set cleaningStatusValue from dataModel
    //
    RoomStatusModelV2 *roomStatusModel=[[RoomStatusModelV2 alloc] init];
    RoomModelV2 *roomModel=[[RoomModelV2 alloc] init];
    
    roomModel.room_Id=[RoomManagerV2 getCurrentRoomNo] ;
    [[RoomManagerV2 sharedRoomManager] loadRoomModel:roomModel];
    //[roomModel load];
    roomStatusModel.rstat_Id = raDataModel.roomAssignmentRoomStatusId;
    
    [[RoomManagerV2 sharedRoomManager] loadRoomStatusData:roomStatusModel];
    
    if ([[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString:[NSString stringWithFormat:@"%@",ENGLISH_LANGUAGE]]) {
        [roomStatusValue setText:roomStatusModel.rstat_Name];
        
    } else {
        [roomStatusValue setText:roomStatusModel.rstat_Lang];
    }
    
    [self addAccessoryButton:cell];
    
    
    roomStatusValue.tag=tagText;
    UIImageView *imageRoomStatusView ;
    imageRoomStatusView = (UIImageView *)[cell.contentView viewWithTag:tagImage];
    if (imageRoomStatusView == nil) {
        imageRoomStatusView = [[UIImageView alloc] initWithFrame:CGRectMake(160, 1, 30, 30)];
        imageRoomStatusView.tag = tagImage;
        
    }
    
    [imageRoomStatusView setImage:[UIImage imageWithData:roomStatusModel.rstat_Image]];
    [roomStatusValue setText: roomStatusModel.rstat_Name];
    
    
    /********* resize frame of image and label*************/
    CGSize size = [roomStatusModel.rstat_Name sizeWithFont:roomStatusValue.font];
    CGFloat x = xOfSccessory - size.width-space2ElementCell;
    [roomStatusValue setFrame:CGRectMake(x, roomStatusValue.frame.origin.y, size.width, size.height)];
    [imageRoomStatusView setFrame:CGRectMake(x-imageRoomStatusView.frame.size.width, imageRoomStatusView.frame.origin.y, imageRoomStatusView.frame.size.width, imageRoomStatusView.frame.size.height)];
    /********* end resize frame of image and label*************/
    
    
    [roomStatusValue setBackgroundColor:[UIColor clearColor]];
    if (isNew == TRUE) {
        [cell.contentView addSubview:roomStatusValue];
        [cell.contentView addSubview:imageRoomStatusView];
    }
}

- (void)setupRemarkCell:(UITableViewCell *)cell isNew:(BOOL)isNew
{
    CGRect remarkLabelRect = CGRectMake(10, 1, 150 , 30);
    // display label control in table view
    
    UILabel *remarkLabel;
    
    if (isNew == TRUE) {
        remarkLabel = [[UILabel alloc] initWithFrame:remarkLabelRect];
    } else {
        remarkLabel = (UILabel *)[cell.contentView viewWithTag:tagLabel];
    }
    remarkLabel.textAlignment = UITextAlignmentLeft;
    remarkLabel.font = [UIFont boldSystemFontOfSize:12];
    remarkLabel.text = self.remark;
    remarkLabel.tag = tagLabel;
    UIImage *img=[UIImage imageNamed:@"tab_small.png"];
    remarkLabel.textColor=[UIColor colorWithPatternImage:img];
    
    [remarkLabel setBackgroundColor:[UIColor clearColor]];
    if (isNew == TRUE) {
        [cell.contentView addSubview:remarkLabel];
        
    }
    
    //label 2 of this cell
    CGRect remarkValueRect = CGRectMake(160, 1, 105 , 30);
    UILabel *remarkValue;
    // align  text in 2 case: user and super user.
    
    if (isNew == TRUE) {
        remarkValue=[[UILabel alloc] initWithFrame:remarkValueRect];
    } else {
        remarkValue = (UILabel *)[cell.contentView viewWithTag:tagText];
    }
    
    remarkValue.textAlignment = UITextAlignmentRight;
    remarkValue.font = [UIFont boldSystemFontOfSize:12];
    
    [self addAccessoryButton:cell];
    
    remarkValue.tag=tagText;
    [remarkValue setBackgroundColor:[UIColor clearColor]];
    
    UIImageView* imageView = (UIImageView *)[cell.contentView viewWithTag:tagImage];
    [imageView removeFromSuperview];
    
    if (isNew == TRUE) {
        [cell.contentView addSubview:remarkValue];
        
    }
}


- (void)setupEstimatedInspectionTimeCell:(UITableViewCell *)cell isNew:(BOOL)isNew
{
    CGRect expectedCleaningLabelRect = CGRectMake(10, 1, 200 , 30);
    // display label control in table view
    UILabel *expectedCleaningLabel;
    UIImageView *imageRoomStatusView;
    
    if (isNew == TRUE) {
        expectedCleaningLabel = [[UILabel alloc] initWithFrame:expectedCleaningLabelRect];
        
        imageRoomStatusView = [[UIImageView alloc] initWithFrame:CGRectMake(170, 1, 30, 30)];
        [imageRoomStatusView setTag:tagImage];
    } else {
        expectedCleaningLabel = (UILabel *)[cell.contentView viewWithTag:tagLabel];
        imageRoomStatusView = (UIImageView*)[cell.contentView viewWithTag:tagImage ];
    }
    
    expectedCleaningLabel.textAlignment = UITextAlignmentLeft;
    expectedCleaningLabel.font = [UIFont boldSystemFontOfSize:12];
    expectedCleaningLabel.text = self.estimatedInspectionTime;
    expectedCleaningLabel.tag = tagLabel;
    UIImage *img=[UIImage imageNamed:@"tab_small.png"];
    expectedCleaningLabel.textColor=[UIColor colorWithPatternImage:img];
    
    [expectedCleaningLabel setBackgroundColor:[UIColor clearColor]];
    if (isNew == TRUE) {
        [cell.contentView addSubview:expectedCleaningLabel];
    }
    
    //label 2 of this cell
    CGRect expectedCleaningValueRect = CGRectMake(170, 1, 120 , 30);
    
    UILabel *expectedCleaningTimeValue;
    if (isNew == TRUE) {
        expectedCleaningTimeValue=[[UILabel alloc] initWithFrame:expectedCleaningValueRect];
    } else {
        expectedCleaningTimeValue = (UILabel *)[cell.contentView viewWithTag:tagText];
        [expectedCleaningTimeValue setFrame:expectedCleaningValueRect];
    }
    
    expectedCleaningTimeValue.textAlignment = UITextAlignmentRight;
    expectedCleaningTimeValue.font = [UIFont boldSystemFontOfSize:12];
    expectedCleaningTimeValue.tag = tagText;
    [expectedCleaningTimeValue setText:[self timeDisplay:self.raDataModel.roomAssignmentRoomExpectedCleaningTime*60]];
    [expectedCleaningTimeValue setBackgroundColor:[UIColor clearColor]];
    
    
    CleaningStatusModelV2 *cleaningStatusModel=[[CleaningStatusModelV2 alloc] init];
    cleaningStatusModel.cstat_Id=dataModel.room_expected_status_id;
    [[RoomManagerV2 sharedRoomManager] loadCleaningStatus:cleaningStatusModel];
    
    if (isNew == TRUE) {
        [cell.contentView addSubview:expectedCleaningTimeValue];
        
    }
}

- (void)setupActualInspectionTimeCell:(UITableViewCell *)cell isNew:(BOOL)isNew
{
    CGRect actualInspectionTimeLabelRect = CGRectMake(10, 1, 200 , 30);
    // display label control in table view
    UILabel *actualInspectiionTimeLabel;
    if (isNew == TRUE) {
        actualInspectiionTimeLabel = [[UILabel alloc] initWithFrame:actualInspectionTimeLabelRect];
    } else {
        actualInspectiionTimeLabel = (UILabel *)[cell.contentView viewWithTag:tagLabel];
    }
    
    actualInspectiionTimeLabel.textAlignment = UITextAlignmentLeft;
    actualInspectiionTimeLabel.font = [UIFont boldSystemFontOfSize:12];
    actualInspectiionTimeLabel.text = self.actualInspectionTime;
    actualInspectiionTimeLabel.tag = tagLabel;
    UIImage *img=[UIImage imageNamed:@"tab_small.png"];
    actualInspectiionTimeLabel.textColor=[UIColor colorWithPatternImage:img];
    
    [actualInspectiionTimeLabel setBackgroundColor:[UIColor clearColor]];
    if (isNew == TRUE) {
        [cell.contentView addSubview:actualInspectiionTimeLabel];
        
    }
    
    //label 2 of this cell
    /*************************************************************/
    
    CGRect actualInspectionValueRect = CGRectMake(160, 1, 135 , 30);
    
    UILabel *actualInspectionTimeValue;
    if (isNew == TRUE) {
        actualInspectionTimeValue=[[UILabel alloc] initWithFrame:actualInspectionValueRect];
    } else {
        actualInspectionTimeValue = (UILabel *)[cell.contentView viewWithTag:tagText];
        [actualInspectionTimeValue setFrame:actualInspectionValueRect];
    }
    
    actualInspectionTimeValue.textAlignment = UITextAlignmentRight;
    actualInspectionTimeValue.font = [UIFont boldSystemFontOfSize:12];
    actualInspectionTimeValue.tag = tagText;
    [actualInspectionTimeValue setTextColor:[UIColor greenColor]];
    [actualInspectionTimeValue setBackgroundColor:[UIColor clearColor]];
    
    [self actualTimeOfinspection: actualInspectionTimeValue];
    if(timeLeftInspectionStatus<0){
        [actualInspectionTimeValue setTextColor:[UIColor redColor]];
    }
    
    if (isNew == TRUE) {
        [cell.contentView addSubview:actualInspectionTimeValue];
    }
    
    UIImageView* imageView = (UIImageView *)[cell.contentView viewWithTag:tagImage];
    [imageView removeFromSuperview];
    
    [cell setAccessoryType:UITableViewCellAccessoryNone];
}


- (void)setupInspectionStatusCell:(UITableViewCell *)cell isNew:(BOOL)isNew
{
    CGRect roomStatusLabelRect = CGRectMake(10, 1, 150 , 30);
    // display label control in table view
    
    UILabel *roomStatusLabel;
    
    if (isNew == TRUE) {
        roomStatusLabel = [[UILabel alloc] initWithFrame:roomStatusLabelRect];
    } else {
        roomStatusLabel = (UILabel *)[cell.contentView viewWithTag:tagLabel];
    }
    roomStatusLabel.textAlignment = UITextAlignmentLeft;
    roomStatusLabel.font = [UIFont boldSystemFontOfSize:12];
    roomStatusLabel.text = self.inspectionStatus;
    roomStatusLabel.tag = tagLabel;
    UIImage *img=[UIImage imageNamed:@"tab_small.png"];
    roomStatusLabel.textColor=[UIColor colorWithPatternImage:img];
    
    [roomStatusLabel setBackgroundColor:[UIColor clearColor]];
    if (isNew == TRUE) {
        [cell.contentView addSubview:roomStatusLabel];
        
    }
    
    //label 2 of this cell
    CGRect cleaningStatusValueRect = CGRectMake(160, 10, 105 , 30);
    UILabel *inspectionStatusValue;
    UIImageView *imageRoomStatusView;
    
    if (isNew == TRUE) {
        inspectionStatusValue=[[UILabel alloc] initWithFrame:cleaningStatusValueRect];
        imageRoomStatusView = [[UIImageView alloc] initWithFrame:CGRectMake(170, 1, 30, 30)];
        [imageRoomStatusView setTag:tagImage];
    } else {
        inspectionStatusValue = (UILabel *)[cell.contentView viewWithTag:tagText];
        imageRoomStatusView = (UIImageView*)[cell.contentView viewWithTag:tagImage];
    }
    
    if (imageRoomStatusView == nil) {
        imageRoomStatusView = [[UIImageView alloc] initWithFrame:CGRectMake(170, 1, 30, 30)];
        [imageRoomStatusView setTag:tagImage];
        [cell.contentView addSubview:imageRoomStatusView];
    }
    
    inspectionStatusValue.textAlignment = UITextAlignmentRight;
    inspectionStatusValue.font = [UIFont boldSystemFontOfSize:12];
    inspectionStatusValue.tag=tagText;
    [inspectionStatusValue setBackgroundColor:[UIColor clearColor]];
    
    
    InspectionStatusModel *inspectionStatusModel=[[InspectionStatusModel alloc] init];
    inspectionStatusModel.istatId=raDataModel.roomAssignmentRoomInspectionStatusId;
    [[RoomManagerV2 sharedRoomManager] loadInspectionStatusModelByPrimaryKey:inspectionStatusModel];
    if ([[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString:[NSString stringWithFormat:@"%@",ENGLISH_LANGUAGE]]) {
        [inspectionStatusValue setText: [NSString stringWithFormat:@"%@",inspectionStatusModel.istatName ]];
        [imageRoomStatusView setImage:[UIImage imageWithData:inspectionStatusModel.istatImage]];
    } else {
        [inspectionStatusValue setText: [NSString stringWithFormat:@"%@",inspectionStatusModel.istatName ]];
    }
    
    
    [self addAccessoryButton:cell];
    
    /********* resize frame of image and label*************/
    CGSize size = [inspectionStatusValue.text sizeWithFont:roomStatusLabel.font];
    CGFloat x = xOfSccessory - size.width-space2ElementCell;
    [inspectionStatusValue setFrame:CGRectMake(x, 10, size.width, size.height)];
    [imageRoomStatusView setFrame:CGRectMake(x-imageRoomStatusView.frame.size.width, imageRoomStatusView.frame.origin.y, 30, 30)];
    /********* end resize frame of image and label*************/
    
    if (isNew == TRUE) {
        [cell.contentView addSubview:inspectionStatusValue];
        [cell.contentView addSubview:imageRoomStatusView];
        
    }
}



/****************************** implement cell *************************************/

-(UITableViewCell*)tableView:(UITableView *)atableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //NSLog(@"bugggggg %d",dataModel.room_CleaningStatusId);
    // init layout for tableview
    BOOL isNew = FALSE;
    
    static NSString *CellTableIdentifier = @"Cell";
    static NSString *CellCleaningStatus = @"CleaningStatus";
    
    UITableViewCell *cell;
    if (indexPath.row == 3) {
        cell = [atableView dequeueReusableCellWithIdentifier:CellCleaningStatus];
    } else
        cell = [atableView dequeueReusableCellWithIdentifier:CellTableIdentifier];
    
    if(cell == nil){
        if (indexPath.row == 3) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellCleaningStatus] ;
        } else {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellTableIdentifier] ;
        }
        isNew = TRUE;
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    /**********************************************************/
    if(self.raDataModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_COMPLETED){
        /********* room is completed *********/
        if(self.raDataModel.roomAssignmentRoomInspectionStatusId == ENUM_INSPECTION_COMPLETED_FAIL ||
           self.raDataModel.roomAssignmentRoomInspectionStatusId == ENUM_INSPECTION_COMPLETED_PASS){
            /********* room is inspected *********/
            switch (indexPath.row) {
                case 0:{
                    [self setupExpectedCleaningCell:cell isNew:isNew];
                } break;
                case 1:{
                    [self setupActualCleaningTimeCell:cell isNew:isNew];
                }break;
                case 2:{[self roomStatusCell:cell isNew:isNew];
                } break;
                    
                case 3:{
                    [self setupRemarkCell:cell isNew:isNew];
                } break;
                    
                case 4:{
                    [self setupEstimatedInspectionTimeCell:cell isNew:isNew];
                } break;
                    
                case 5:{
                    [self setupActualInspectionTimeCell:cell isNew:isNew];
                } break;
                    
                case 6:{
                    [self setupInspectionStatusCell:cell isNew:isNew];
                }break;
            }
            /********* end room note inspected******/
        } else{
            /********* room is not inspecte******/
            switch (indexPath.row) {
                case 0:{
                    [self setupExpectedCleaningCell:cell isNew:isNew];
                } break;
                case 1:{
                    [self setupActualCleaningTimeCell:cell isNew:isNew];
                }break;
                case 2:{[self setupAdditionalCell:cell isNew:isNew];
                } break;
                    
                case 3:{
                    [self setupCleaningStatusCell:cell isNew:isNew];
                } break;
                    
                case 4:{
                    [self roomStatusCell:cell isNew:isNew];
                } break;
                    
                case 5:{
                    [self setupRemarkCell:cell isNew:isNew];
                } break;
            }
            /********* end room is not inspecte******/
        }
    } else {
        /********* room is not completed *********/
        switch (indexPath.row) {
            case 0:{
                [self setupExpectedCleaningCell:cell isNew:isNew];
            }
                break;
            case 1:{
                [self setupAdditionalCell:cell isNew:isNew];
            }
                break;
            case 2:{
                [self setupCleaningStatusCell:cell isNew:isNew];
            } break;
                
            case 3:{
                [self roomStatusCell:cell isNew:isNew];
            }break;
                
            case 4:{
                [self setupRemarkCell:cell isNew:isNew];
            } break;
        }
        /********* end room is not completed *********/
    }
    
    /**********************************************************/
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger section = indexPath.section;
    if(section!=0) return;
    
    /**********************************************************/
    if(self.raDataModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_COMPLETED){
        /********* room is completed *********/
        if(self.self.raDataModel.roomAssignmentRoomInspectionStatusId == ENUM_INSPECTION_COMPLETED_FAIL ||
           self.self.raDataModel.roomAssignmentRoomInspectionStatusId == ENUM_INSPECTION_COMPLETED_PASS ){
            /********* room is inspected *********/
            //if(indexPath.row == 2)  [self showRoomStatus];
            if(indexPath.row == 3)  [self showRemark];
            /********* end room note inspected******/
        } else{
            /********* room is not inspecte******/
            //if(indexPath.row == 3)  [self showCleaningStatus];
            //if(indexPath.row == 4)  [self showRoomStatus];
            if(indexPath.row == 5)  [self showRemark];
            /********* end room is not inspecte******/
        }
    } else {
        /********* room is not completed *********/
        //if(indexPath.row == 2)  [self showCleaningStatus];
        //if(indexPath.row == 3)  [self showRoomStatus];
        if(indexPath.row == 4)  [self showRemark];
        /********* end room is not completed *********/
    }
    /**********************************************************/
}

//-(void)showRoomStatus{
//    if(raDataModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_COMPLETED) return;
//    [delegate showPopupRoomStatus:self];
//}
//
//- (void)showCleaningStatus {
//    if(raDataModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_COMPLETED) return;
//    [delegate showPopupCleaningStatus:self];
//}

-(void)showRemark{
    if(raDataModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_COMPLETED) return;
    if(countTimeStatus == NOT_START || self.raDataModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_START) return;
    if(self.raDataModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_PENDING_FIRST_TIME ||
       self.raDataModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_PENDING_SECOND_TIME) return;
    
    
    RemarkViewV2 *remarkView = [[RemarkViewV2 alloc] initWithNibName:
                                NSStringFromClass([RemarkViewV2 class]) bundle:nil];
    [remarkView setDelegate:self];
    [self.superController.navigationController pushViewController:remarkView animated:YES];
}

-(NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    return indexPath;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 33.0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 170.0;
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return self.listButtonView;
}

#pragma mark -
#pragma mark Save data
-(void)saveData{
    
    // @ show loading data .
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getMsgSaveMessage]];
    [self.view.superview addSubview:HUD];
    [HUD show:YES];
    
    /*bxchinh
     dataModelCompare.room_CleaningStatusId = dataModel.room_CleaningStatusId;
     dataModelCompare.room_StatusId = dataModel.room_StatusId;
     */
    
    dataModel.room_PostStatus = POST_STATUS_SAVED_UNPOSTED;
    [[RoomManagerV2 sharedRoomManager] updateRoomModel:dataModel];
    
    
    RoomAssignmentModelV2 *raModel = [[RoomAssignmentModelV2 alloc] init];
    raModel.roomAssignment_RoomId = [RoomManagerV2 getCurrentRoomNo];
    raModel.roomAssignment_UserId = [[UserManagerV2 sharedUserManager] currentUser].userId;
    
    [[RoomManagerV2 sharedRoomManager] loadRoomAsignmentByRoomIdAndUserID:raModel];
    raModel.roomAssignment_PostStatus = POST_STATUS_SAVED_UNPOSTED;
    [[RoomManagerV2 sharedRoomManager] update:raModel];
    
    SyncManagerV2 *synManager = [[SyncManagerV2 alloc] init];
    [synManager postRoomAssignmentWithUserID:[[UserManagerV2 sharedUserManager] currentUser].userId
                                  AndRaModel:raModel];
    
    
    
    //hide saving data.
    [self performSelector:@selector(hiddenHUDAfterSaved:) withObject:HUD afterDelay:0.5];
    
}


-(BOOL) isNotSavedAndExit
{
    return NO;
}


//-(BOOL)checkIsSave{
//    /* bxchinh
//    if(dataModelCompare.room_CleaningStatusId != dataModel.room_CleaningStatusId||
//       dataModelCompare.room_StatusId != dataModel.room_StatusId){
//        return NO;
//    } else{
//        return YES;
//    }
//     */
//}

#pragma mark -
#pragma mark ButtonCLG Methods
- (IBAction)butCheckListPressed:(id)sender {
    //selected home view
    //    [[HomeViewV2 shareHomeView] setSelectedButton:tagOfCheckListButton];
    
    CheckListForMaidViewV2 *checklistView = [[CheckListForMaidViewV2 alloc] initWithNibName:NSStringFromClass([CheckListForMaidViewV2 class]) bundle:nil];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK] style:UIBarButtonItemStyleBordered target:nil action:nil];
    self.superController.navigationItem.backBarButtonItem = backButton;
    
    [self.superController.navigationController pushViewController:checklistView animated:YES];
}

- (IBAction)butActionPressed:(id)sender {
    NSDictionary *dic = [[LanguageManagerV2 sharedLanguageManager] getCurrentDictionaryLanguage];
    NSString *action, *ec, *c, *g, *cancel;
    action = [dic valueForKey:[NSString stringWithFormat:@"%@", L_RA_SELECT_ACTION]];
    ec = [dic valueForKey:[NSString stringWithFormat:@"%@", L_ENGINEERING_CASE]];
    c = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_Counts];
    g = [dic valueForKey:[NSString stringWithFormat:@"%@", L_Guideline]];
    cancel = [dic valueForKey:[NSString stringWithFormat:@"%@", L_CANCEL]];
    
    if (action == nil) {
        action = @"Action";
    }
    if (c == nil) {
        c= @"Posting";
    }
    
    if (g == nil) {
        g = @"Guideline";
    }
    if( self.raDataModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_COMPLETED){
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:action delegate:self cancelButtonTitle:cancel destructiveButtonTitle:nil otherButtonTitles: g, nil];
        [actionSheet showInView:self.view.superview.superview];
        
        
    } else{
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:action delegate:self cancelButtonTitle:cancel destructiveButtonTitle:nil otherButtonTitles:c, g, nil];
        [actionSheet showInView:self.view.superview.superview];
    }
}

- (IBAction)butLaundryPressed:(id)sender {
    NSString *temp = [[NSString alloc] initWithFormat:@"%d",[sender tag]];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"AddCLGWithSelectedIndex" object:temp];
}

- (IBAction)butCountPressed:(id)sender {
    NSString *temp = [[NSString alloc] initWithFormat:@"%d",[sender tag]];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"AddCLGWithSelectedIndex" object:temp];
}

- (IBAction)butGuidePressed:(id)sender {
    NSString *temp = [[NSString alloc] initWithFormat:@"%d",[sender tag]];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"AddCLGWithSelectedIndex" object:temp];
}
-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return TRUE;
}

#pragma mark - UIActionSheet Delegate Methods
-(void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex{
    //maid
    if(self.raDataModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_COMPLETED){
        switch (buttonIndex) {
                
            case 0: // Guideline
                [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@",notificationGuidelineView] object:nil];
                break;
                
            default:
                break;
        }
        
    }
    else
    {
        switch (buttonIndex) {
                
            case 0: // Posting
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"AddCLGWithSelectedIndex" object:@"1"];
                
                break;
            case 1: // Guideline
                [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@",notificationGuidelineView] object:nil];
                break;
                
            default:
                break;
        }
        
    }
}

#pragma mark - set captions view
-(void)setCaptionsView {
    NSDictionary *dic = [[LanguageManagerV2 sharedLanguageManager] getCurrentDictionaryLanguage];
    NSString *action = [dic valueForKey:[NSString stringWithFormat:@"%@", L_BTN_ACTION]];
    if (action == nil) {
        action = @"Action";
    }
    [btnAction setTitle:action forState:UIControlStateNormal];
    
    self.expectedCleaningTime = [dic valueForKey:[NSString stringWithFormat:@"%@", L_EXPECTED_CLEANING_TIME]];
    self.guestPref = [dic valueForKey:[NSString stringWithFormat:@"%@", L_GUEST_REFERENCE]];
    self.additioJob = [dic valueForKey:[NSString stringWithFormat:@"%@", L_ADDITIONAL_JOB]];
    self.cleaningStatus = [dic valueForKey:[NSString stringWithFormat:@"%@", L_CLEANING_STATUS]];
    
    
    msgbtnOk = [[LanguageManagerV2 sharedLanguageManager] getOK];
    msgSaveRoomDetail = [[LanguageManagerV2 sharedLanguageManager] getMsgAlertSaveRoomDetail];
    
}

#pragma mark
#pragma mark Timer counting

-(void)showAlert{
    
    AlertAdvancedSearch *alertView=[[AlertAdvancedSearch alloc] init];
    [alertView alertAdvancedSearchInitWithFrame:CGRectZero title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] content:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_msgalert_confirmblockroom]];
    
    alertView.delegate = self;
    [alertView show];
    
}

-(void)showAlertWithTitle:(NSString*)title content:(NSString*)content andtag:(NSInteger)tag{
    
    
    NSString *msgbtnNo = [[LanguageManagerV2 sharedLanguageManager] getNo];
    NSString *msgbtnYes = [[LanguageManagerV2 sharedLanguageManager] getYes];
    
    
    UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:content delegate:self cancelButtonTitle:msgbtnYes otherButtonTitles:msgbtnNo, nil];
    alert.delegate = self;
    alert.tag = tag;
    [alert show];
    
    
}

// start and pause timer
- (IBAction)startPauseCountTimerDidSelect:(id)sender {
    
    if(timer == nil){
        timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self
                                               selector:@selector(updateTimer) userInfo:nil repeats:YES];
    }
    
    switch (countTimeStatus) {
        case PAUSE:{
            // transition from pause to start.
            countTimeStatus = START;
            self.raDataModel.roomAssignmentRoomCleaningStatusId = ENUM_CLEANING_STATUS_START;
            
            
            // calculate time diffrence:
            NSDate* startDate = [timeFomarter dateFromString:self.roomRemarkModel.rm_clean_pause_time];
            
            NSDate *now = [[NSDate alloc] init];
            NSDate* endDate = [timeFomarter dateFromString:[timeFomarter stringFromDate:now]];
            
            NSTimeInterval timeDifference = [endDate timeIntervalSinceDate:startDate];
            NSInteger secondDiffrence = (NSInteger)timeDifference;
            
            self.roomRemarkModel.rm_clean_pause_time  = [NSString stringWithFormat:@"%d",secondDiffrence];
            
            // save room remark
            [[RoomManagerV2 sharedRoomManager] updateRoomRemarkData:self.roomRemarkModel];
            
            
            [countTimeButton setBackgroundImage:[UIImage imageNamed:IMAGE_PAUSE] forState:UIControlStateNormal];
            [timerButon setBackgroundImage:[UIImage imageNamed:IMAGE_TIMER_PLAY] forState:UIControlStateNormal];
            [tableView reloadData];
            
        }
            break;
            
        case START:{
            // transition from start to pause.
            countTimeStatus = PAUSE;
            
            self.roomRemarkModel = [[RoomRemarkModelV2 alloc] init];
            NSDate *now = [[NSDate alloc] init];
            self.roomRemarkModel.rm_clean_pause_time = [timeFomarter stringFromDate:now]; // store start time of pause
            
            self.raDataModel.roomAssignmentRoomCleaningStatusId = ENUM_CLEANING_STATUS_PAUSE;
            
            [countTimeButton setBackgroundImage:[UIImage imageNamed:IMAGE_PLAY] forState:UIControlStateNormal];
            [timerButon setBackgroundImage:[UIImage imageNamed:IMAGE_TIMER_PAUSE] forState:UIControlStateNormal];
            
            [self showRemark];
            [tableView reloadData];
            
        } break;
            
        case NOT_START:{
            // transition from not start to pause.
            countTimeStatus = START;
            NSDate *now = [[NSDate alloc] init];
            self.roomRecord.rrec_Cleaning_Start_Time = [timeFomarter stringFromDate:now];  //... start time
            
            
            self.raDataModel.roomAssignmentRoomCleaningStatusId = ENUM_CLEANING_STATUS_START;
            
            [finishTimeButton setUserInteractionEnabled:YES];
            [finishTimeButton setBackgroundImage:[UIImage imageNamed:IMAGE_COMPLETE_ON] forState:UIControlStateNormal];
            [countTimeButton setBackgroundImage:[UIImage imageNamed:IMAGE_PAUSE] forState:UIControlStateNormal];
            [timerButon setBackgroundImage:[UIImage imageNamed:IMAGE_TIMER_PLAY] forState:UIControlStateNormal];
            [btnAction setHidden:NO];
            CGRect f = timerButon.frame;
            [timerButon setFrame:CGRectMake(f.origin.x, btnAction.frame.origin.y + btnAction.frame.size.height +5 ,
                                            f.size.width, f.size.height)];
            // show check list :
            [tableView reloadData];
        } break;
            
        case FINISH:{
            
        } break;
            
        case STOP:{
            
        } break;
            
    }
    
}

// display time with expected time
- (NSString *)countTimeSpendForTask:(NSInteger)expectedTimeForTask  {
    NSString *secondsString;
    NSString *minutesString;
    NSString *hoursString;
    NSInteger secondsInt;
    NSInteger minutesInt;
    NSInteger hoursInt;
    NSString *timeDisplay;
    if (expectedTimeForTask < 0){
        hoursInt = -expectedTimeForTask/3600;
        minutesInt =-((expectedTimeForTask - (hoursInt*3600)) / 60);
        secondsInt =-(expectedTimeForTask % 60);
        
        hoursString = [NSString stringWithFormat:@"%d",hoursInt];
        minutesString = [NSString stringWithFormat:@"%d",minutesInt];
        secondsString = [NSString stringWithFormat:@"%d",secondsInt];
        
        if([hoursString length]==1){
            hoursString = [NSString stringWithFormat:@"0%@",hoursString];
        }
        
        if([minutesString length]==1){
            minutesString = [NSString stringWithFormat:@"0%@",minutesString];
        }
        
        if([secondsString length]==1){
            secondsString = [NSString stringWithFormat:@"0%@",secondsString];
        }
        
        timeDisplay = [NSString stringWithFormat:@"- %@:%@:%@",hoursString,minutesString,secondsString];
        [timerButon setBackgroundImage:[UIImage imageNamed:@"timer_red_274x58.png"] forState:UIControlStateNormal];
    } else {
        
        hoursInt = expectedTimeForTask/3600;
        minutesInt = (expectedTimeForTask - (hoursInt*3600)) / 60;
        secondsInt = expectedTimeForTask % 60;
        
        hoursString = [NSString stringWithFormat:@"%d",hoursInt];
        minutesString = [NSString stringWithFormat:@"%d",minutesInt];
        secondsString = [NSString stringWithFormat:@"%d",secondsInt];
        
        if([hoursString length]==1){
            hoursString = [NSString stringWithFormat:@"0%@",hoursString];
        }
        
        if([minutesString length]==1){
            minutesString = [NSString stringWithFormat:@"0%@",minutesString];
        }
        
        if([secondsString length]==1){
            secondsString = [NSString stringWithFormat:@"0%@",secondsString];
        }
        timeDisplay = [NSString stringWithFormat:@"%@:%@:%@",hoursString,minutesString,secondsString];
    }
    return timeDisplay;
}

- (void)displayTimerButton {
    [timerButon setTitle:[self countTimeSpendForTask: timeLeftCleaningStatus] forState:UIControlStateNormal];
    
}

-(void)updateTimer{
    if(countTimeStatus == PAUSE || countTimeStatus == FINISH) return;
    timeLeftCleaningStatus -= 1;
    [self displayTimerButton];
}

- (void)completeCleaningRoom {
    countReminder = [[CountPopupReminderViewV2 alloc] initWithRoomId:[TasksManagerV2 getCurrentRoomAssignment]];
    [countReminder setDelegate:self];
    countReminder.tag = tagAlertCleaningStatusCompleted;
    if(![countReminder show]){
        [self showAlertWithTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] content:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_are_you_done_with_the_room_cleaning]
                          andtag:kTagAlertDoneWithRoomCleaning];
    }
}

// this function used when complete task in room: cleaning , inspection
- (IBAction)finishCountTimeDidSelect:(id)sender {
    [self completeCleaningRoom];
}

// confirm block room
- (IBAction)blockRoomDidSelect:(id)sender {
    [self showAlertWithTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] content:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_msgalert_confirmblockroom] andtag:tagAlertBlockRoom];
}

// confirm reassign room
- (IBAction)reassignDidSelect:(id)sender {
    self.dataModel.room_is_re_cleaning = YES;
    self.raDataModel.roomAssignmentRoomCleaningStatusId = ENUM_CLEANING_STATUS_PENDING_SECOND_TIME;
    [reassignBtn setBackgroundImage:[UIImage imageNamed:@"bt_gray_299x58"] forState:UIControlStateNormal];
    [reassignBtn setUserInteractionEnabled:NO];
}

#pragma mark
#pragma mark  Alert delegate

// room status is change
-(void)roomStatusDidChange{
    RoomStatusModelV2 *roomStatusModel = [[RoomStatusModelV2 alloc] init];
    roomStatusModel.rstat_Id = roomStatusIDSelected;
    [[RoomManagerV2 sharedRoomManager] loadRoomStatusData:roomStatusModel];
    
    NSIndexPath *indexpath = nil;
    UITableViewCell *cell = nil;
    
    if(roomDetailStatus==ENUM_MAID_NOT_COMPLETED) {
        indexpath = [NSIndexPath indexPathForRow:3 inSection:0];
        
    }
    
    if(roomDetailStatus==ENUM_MAID_NOT_INSPECTED_COMPLETED) {
        indexpath = [NSIndexPath indexPathForRow:4 inSection:0];
    }
    
    if(roomDetailStatus == ENUM_MAID_INSPECTED_COMPLETED){
        indexpath = [NSIndexPath indexPathForRow:3 inSection:0];
    }
    
    if(roomDetailStatus == ENUM_SUPER_NOTCOMPLETED){
        indexpath = [NSIndexPath indexPathForRow:3 inSection:0];
    }
    
    
    cell = [tableView cellForRowAtIndexPath:indexpath];
    UIImageView *imgView = (UIImageView*)[cell.contentView viewWithTag:tagImage];
    UILabel *label = (UILabel*)[cell.contentView viewWithTag:tagText];
    
    [imgView setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@.png",roomStatusModel.rstat_Name]]];
    [label setText:roomStatusModel.rstat_Name];
    
    /********* resize frame of image and label*************/
    CGSize size = [roomStatusModel.rstat_Name sizeWithFont:label.font];
    CGFloat x = xOfSccessory - size.width-space2ElementCell;
    [label setFrame:CGRectMake(x, label.frame.origin.y, size.width, size.height)];
    [imgView setFrame:CGRectMake(x-imgView.frame.size.width, imgView.frame.origin.y, imgView.frame.size.width, imgView.frame.size.height)];
    /********* end resize frame of image and label*************/
    raDataModel.roomAssignmentRoomStatusId = roomStatusModel.rstat_Id;
    
    if(raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OD||
       raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VD){
        raDataModel.roomAssignmentRoomStatusId = ENUM_INSPECTION_COMPLETED_FAIL;
    } else{
        raDataModel.roomAssignmentRoomInspectionStatusId = ENUM_INSPECTION_COMPLETED_PASS;
    }
    
    [tableView reloadData];
}

-(void) roomBlockDidChange{
    self.raDataModel.roomAssignmentRoomStatusId = ENUM_ROOM_STATUS_OOO;
    [lockButton setUserInteractionEnabled:NO];
    [lockButton setImage:[UIImage imageNamed:@"block_bt_gray_299x58.png"] forState:UIControlStateNormal];
    
    [self displayButtons];
    
    [tableView reloadData];
}

-(void) alertAdvancedOKButtonPressed:(AlertAdvancedSearch *)alertView{
    if(alertView.tag == tagAlertRoomStatusChange){
        [self roomStatusDidChange];
    }
    
    if(alertView.tag == tagAlertBlockRoom){
        [self roomBlockDidChange];
    }
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    if(buttonIndex == 1) return;
    if(alertView.tag == tagAlertRoomStatusChange){
        [self roomStatusDidChange];
    }
    
    if(alertView.tag == tagAlertBlockRoom){
        [self roomBlockDidChange];
    }
    
    if(alertView.tag == kTagAlertDoneWithRoomCleaning){
        [self confirmCompleteCleaningStatus];
    }
}

-(void) alertAdvancedCancelButtonPressed:(AlertAdvancedSearch *)alertView{
}

#pragma mark
#pragma mark  Cleaning Status + Room Status + remark delegate

-(void)cleaningStatusDidSelect:(NSInteger)cleaningStatusId{
    if(cleaningStatusId == ENUM_CLEANING_STATUS_SERVICE_LATER){
        self.roomRecord.rrec_Cleaning_Start_Time = @"";  //... start time
    }
    
    CleaningStatusModelV2 *cleaningStatusModel = [[CleaningStatusModelV2 alloc] init];
    cleaningStatusModel.cstat_Id = cleaningStatusId;
    [[RoomManagerV2 sharedRoomManager] loadCleaningStatus:cleaningStatusModel];
    
    
    
    // cleaning status != completed
    if(cleaningStatusModel.cstat_Id != ENUM_CLEANING_STATUS_COMPLETED){
        raDataModel.roomAssignmentRoomCleaningStatusId = cleaningStatusModel.cstat_Id;
        
        if(raDataModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_DND){
            RoomAssignmentModelV2 *raModel = [[RoomAssignmentModelV2 alloc] init];
            raModel.roomAssignment_RoomId = [RoomManagerV2 getCurrentRoomNo];
            raModel.roomAssignment_UserId = [[UserManagerV2 sharedUserManager] currentUser].userId;
            [[RoomManagerV2 sharedRoomManager] loadRoomAsignmentByRoomIdAndUserID:raModel];
            raModel.roomAssignment_Priority_Sort_Order = [[RoomManagerV2 sharedRoomManager] loadMaxPrioprity] + 1;
            [[RoomManagerV2 sharedRoomManager] update:raModel];
        }
        
        if(raDataModel.roomAssignmentRoomStatusId == ENUM_CLEANING_STATUS_START){
            [self startPauseCountTimerDidSelect:nil];
            return;
        }
        
        if(raDataModel.roomAssignmentRoomStatusId == ENUM_CLEANING_STATUS_PAUSE){
            // transition from start to pause.
            countTimeStatus = PAUSE;
            
            self.roomRemarkModel = [[RoomRemarkModelV2 alloc] init];
            NSDate *now = [[NSDate alloc] init];
            self.roomRemarkModel.rm_clean_pause_time = [timeFomarter stringFromDate:now]; // store start time of pause
            
            self.raDataModel.roomAssignmentRoomStatusId = ENUM_CLEANING_STATUS_PAUSE;
            
            [countTimeButton setBackgroundImage:[UIImage imageNamed:IMAGE_PLAY] forState:UIControlStateNormal];
            [timerButon setBackgroundImage:[UIImage imageNamed:IMAGE_TIMER_PAUSE] forState:UIControlStateNormal];
            
            [self showRemark];
            [tableView reloadData];
            return;
        }
        
        
        [tableView reloadData];
    }  else{
        [self completeCleaningRoom];
    }
    
    
}


-(void)cleaningStatusDidSelect:(NSInteger)cleaningStatusId andDate:(NSDate*)date{
    CleaningStatusModelV2 *cleaningStatusModel = [[CleaningStatusModelV2 alloc] init];
    cleaningStatusModel.cstat_Id = cleaningStatusId;
    [[RoomManagerV2 sharedRoomManager] loadCleaningStatus:cleaningStatusModel];
    
    
    NSDateFormatter *_timeFomarter = [[NSDateFormatter alloc] init];
    [_timeFomarter setDateFormat:kTime12HoursFormat];
    
    // insert new service later
    RoomServiceLaterModelV2 *roomServicelater = [[RoomServiceLaterModelV2 alloc] init];
    roomServicelater.rsl_RecordId = self.roomRecord.rrec_Id;
    roomServicelater.rsl_Time = [_timeFomarter stringFromDate:date];
    
    [[RoomManagerV2 sharedRoomManager] insertRoomServiceLaterData:roomServicelater];
    
    
    raDataModel.roomAssignmentRoomCleaningStatusId = cleaningStatusModel.cstat_Id;
    
    [tableView reloadData];
}

#pragma mark
#pragma mark Count popup reminder
- (void)confirmCompleteCleaningStatus {
    raDataModel.roomAssignmentRoomCleaningStatusId = ENUM_CLEANING_STATUS_COMPLETED;
    
    if(raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VD){
        raDataModel.roomAssignmentRoomStatusId = ENUM_ROOM_STATUS_VC;
    }
    if(raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OD){
        raDataModel.roomAssignmentRoomStatusId = ENUM_ROOM_STATUS_OC;
    }
    
    
    
    /********* set status of this screen************/
    countTimeStatus = FINISH ;
    roomDetailStatus = ENUM_MAID_NOT_INSPECTED_COMPLETED;
    self.isInspected = NO;
    self.isCompleted = YES;
    
    if(self.raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VD){
        self.raDataModel.roomAssignmentRoomStatusId = ENUM_ROOM_STATUS_VC;
    } else{
        self.raDataModel.roomAssignmentRoomStatusId = ENUM_ROOM_STATUS_OC;
    }
    
    
    /**************************/
    NSDate *now = [[NSDate alloc] init];
    self.roomRecord.rrec_Cleaning_End_Time = [timeFomarter stringFromDate:now];  //... end time
    
    
    [[RoomManagerV2 sharedRoomManager] updateRoomRecordData:self.roomRecord];
    
    /********* set status of this screen************/
    
    /********* hidden / show button: *********/
    CGRect frameActionButton = btnAction.frame;
    
    [btnAction setFrame:CGRectMake(frameActionButton.origin.x, countTimeButton.frame.origin.y,
                                   frameActionButton.size.width, frameActionButton.size.height)];
    [countTimeButton setHidden:YES];
    [finishTimeButton setHidden:YES];
    [timerButon setHidden:YES];
    
    if(self.raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VD ||
       self.raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VC) {
        [lockButton setHidden:NO];
    } else{
        [lockButton setHidden:YES];
    }
    
    CGFloat y = btnAction.frame.origin.y + btnAction.frame.size.height + 4;
    [lockButton setFrame:CGRectMake(lockButton.frame.origin.x, y,
                                    lockButton.frame.size.width,
                                    lockButton.frame.size.height)];
    
    
    // reload layout  of tableview
    [tableView reloadData];
    
}

-(void)yesCountReminder{
    [countReminder hide];
    [self confirmCompleteCleaningStatus];
}

-(void)noCountReminder{
    [countReminder hide];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"AddCLGWithSelectedIndex" object:@"1"];
}


-(void)roomStatusDidSelect:(NSInteger)roomStatusID{
    roomStatusIDSelected = roomStatusID;
    [self showAlertWithTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert]
                     content:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_room_confirmchangeroomstatus] andtag:tagAlertRoomStatusChange];
    
}

-(void)remarkViewV2DoneWithText:(NSString *)text andController:(UIViewController *)controllerView{
    [controllerView.navigationController popViewControllerAnimated:YES];
    NSIndexPath *indexpath = nil;
    UITableViewCell *cell = nil;
    
    if(roomDetailStatus==ENUM_MAID_NOT_COMPLETED || roomDetailStatus == ENUM_SUPER_NOTCOMPLETED) {
        indexpath = [NSIndexPath indexPathForRow:4 inSection:0];
    }
    
    
    cell = [tableView cellForRowAtIndexPath:indexpath];
    UILabel *label = (UILabel*)[cell.contentView viewWithTag:tagText];
    [label setText:text];
    
    // save remark data
    NSInteger countRoomRemark = [[RoomManagerV2 sharedRoomManager] countRoomRemarkData:self.roomRecord.rrec_Id];
    countRoomRemark ++;
    
    self.roomRemarkModel.rm_Id = countRoomRemark;
    self.roomRemarkModel.rm_RecordId = self.roomRecord.rrec_Id;
    self.roomRemarkModel.rm_content  = text;
    [[RoomManagerV2 sharedRoomManager] insertRoomRemarkData:self.roomRemarkModel];
}


@end
