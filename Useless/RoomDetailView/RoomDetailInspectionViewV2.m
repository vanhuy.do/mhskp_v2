//
//  RoomDetailView.m
//  EHouseKeeping
//
//  Created by tms on 5/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "RoomDetailInspectionViewV2.h"
#import "sqlite3.h"
#import "MBProgressHUD.h"
#import "NetworkCheck.h"
#import "RoomManagerV2.h"
#import "LanguageManager.h"
#import "RoomInfoInspectionV2.h"
#import "RemarkViewV2.h"
#import "GuidelineViewV2.h"
#import "CountPopupReminderViewV2.h"
#import "CheckListManagerV2.h"
#import "CheckListForMaidViewV2.h"
#import "LogFileManager.h"

#define tagCleaningStatus 12
#define tagLabel 1234
#define tagText 5678
#define tagImage 9101

#define tagAlertRoomStatusChange 200
#define tagAlertBlockRoom 300
#define tagAlertCleaningStatusCompleted 400
#define kTagAlertDoneWithRoomCleaning 500
#define tagAlertCancelCleaningLessThan1Minute 19

#define xOfSccessory 280
#define space2ElementCell 10

#define indexOfRoomStatusRow 5
#define indexOfRemarkRow    6


#define IMAGE_PAUSE_ON  @"phuse_bt_on_299x58.png"
#define IMAGE_PAUSE_GRAY  @"phuse_bt_on_299x58.png"
#define IMAGE_PAUSE @"phuse_bt_299x58.png"

#define IMAGE_STOP_ON   @"stop_bt_on_299x58.png"
#define IMAGE_STOP_GRAY @"stop_bt_gray_299x58.png"
#define IMAGE_STOP      @"stop_bt_299x58.png"

#define IMAGE_TIMER_PLAY @"timer_play_274x58.png"
#define IMAGE_TIMER_PAUSE @"timer_phuse_274x58.png"
#define IMAGE_TIMER_GRAY @"timer_gray_274x58.png"
#define IMAGE_TIMER_OVER @"timer_red_274x58.png"

#define IMAGE_COMPLETE @"click_bt_299x58.png"
#define IMAGE_COMPLETE_GRAY @"click_bt_gray_299x58.png"
#define IMAGE_COMPLETE_ON @"click_bt_on_299x58.png"

#define IMAGE_PLAY @"play_bt_299x58.png"
#define IMAGE_PLAY_GRAY @"play_bt_gray_299x58.png"
#define IMAGE_PLAY_ON @"play_bt_on_299x58.png"

#define kTimeFormat @"HH:mm:ss"
#define kTime12HoursFormat @"hh:mm a"

@interface RoomDetailInspectionViewV2 ()

-(void) lastCleaningDateCell:(UITableViewCell *)cell;
-(STATUS_UPDATE_RECORD_DETAIL) canUpdateStartRecord;
-(void) updateStartRecord;
-(void) updateStopRecord;
-(void) updateRecordDurationWithRecordDetail:(RoomRecordDetailModelV2 *) recordDetail;
-(STATUS_UPDATE_RECORD_DETAIL) canUpdateStopRecord;
-(void) updateCorrectTimeLeftAndDuration;
-(void) raFullNameCell:(UITableViewCell *) cell;

@end

@implementation RoomDetailInspectionViewV2
@synthesize tableView, expectedCleaningTime, guestPref, additioJob, cleaningStatus;
@synthesize dataModel, indexPathCleaningStatus;
@synthesize roomStatus, remark, estimatedInspectionTime, actualInspectionTime, inspectionStatus;
@synthesize reassignBtn;
@synthesize listButtonView;
@synthesize superController;
@synthesize isPass;
@synthesize isInspected;
@synthesize isCompleted;
@synthesize delegate;
@synthesize roomRecord;
//@synthesize roomRemarkModel;
@synthesize msgbtnOk;
@synthesize msgSaveRoomDetail;
@synthesize isSaved;
@synthesize raModel;
@synthesize roomRecordDetail;

int hours, minutes, seconds;
int secondsLeft;



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if (self == [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        
    }
    return self;
}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    //set captions view
    [self setCaptionsView];
    
    
    if(!animated && ![CommonVariable sharedCommonVariable].isBackFromChk) {
        if (raModel.roomAssignmentRoomInspectionStatusId != 0) {
            
        } else
            [[RoomManagerV2 sharedRoomManager] loadRoomModel:self.dataModel]; // load room data
    }
    
    [self refreshLayoutButtons];
    
    
    [self.tableView reloadData];
}

-(void)refreshLayoutButtons{
    
    CGRect f = timerButon.frame;
    if (countTimeStatus == START || countTimeStatus == FINISH) {
        
        [timerButon setFrame:CGRectMake(f.origin.x, btnAction.frame.origin.y + btnAction.frame.size.height + 4, f.size.width, f.size.height)];
        
        if (countTimeStatus == FINISH) {
            [btnAction setHidden:YES];
        } else {
            [btnAction setHidden:NO];
        }
        
    } else {
        
        [btnAction setHidden:YES];
        
        [timerButon setFrame:CGRectMake(f.origin.x, btnAction.frame.origin.y, f.size.width, f.size.height)];
        
    }
    
    [lockButton setHidden:YES];
    [checkListButton setHidden:YES];
    
    //check list pass
    if(raModel.roomAssignmentRoomInspectionStatusId == ENUM_INSPECTION_COMPLETED_PASS){
        [reassignBtn setHidden:YES];
        [checkListButton setHidden:NO];
    }
    
    // check list fail
    if(raModel.roomAssignmentRoomInspectionStatusId == ENUM_INSPECTION_COMPLETED_FAIL){
        [reassignBtn setHidden:NO];
        [checkListButton setHidden:NO];
        
        //check if room did reassign ==> gray out it and user can not interaction
        ReassignRoomAssignmentModel *remodel = [[ReassignRoomAssignmentModel alloc] init];
        remodel.reassignRaId = raModel.roomAssignment_Id;
        remodel.reassignUserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
        [[RoomManagerV2 sharedRoomManager] loadReassignRoomAssignmentModelByPrimaryKey:remodel];
        if ([remodel.reassignRoomId isEqualToString:dataModel.room_Id]) {
            [reassignBtn setBackgroundImage:[UIImage imageNamed:@"bt_gray_299x58"] forState:UIControlStateNormal];
            [reassignBtn setUserInteractionEnabled:NO];
            [reassignBtn setHidden:NO];
        } else {
            [reassignBtn setHidden:NO];
            [reassignBtn setUserInteractionEnabled:YES];
            [reassignBtn setBackgroundImage:[UIImage imageNamed:@"bt_299x58"] forState:UIControlStateNormal];
        }
    }
    
    roomDetailStatus = ENUM_SUPER_NOTCOMPLETED; //roomDetail Status
    
    //bring complete button to top
    [listButtonView bringSubviewToFront:finishTimeButton];
    
    [self displayTimerButton];
}

-(void)layoutButton{
    [timerButon setBackgroundImage:[UIImage imageNamed:IMAGE_TIMER_GRAY] forState:UIControlStateNormal];
    [lockButton setHidden:YES];
    [checkListButton setHidden:YES];
    roomDetailStatus = ENUM_SUPER_NOTCOMPLETED; //roomDetail Status
    [btnAction setHidden:YES];
    
    [self displayTimerButton];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.expectedCleaningTime= [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_EXPECTED_CLEANING_TIME];
    self.guestPref= [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_GUEST_REFERENCE];
    self.additioJob= [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ADDITIONAL_JOB];
    self.cleaningStatus= [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_CLEANING_STATUS];
    self.roomStatus = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_room_status];
    self.remark = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_remark_title];
    self.estimatedInspectionTime = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_estimated_inspection_time];
    self.actualInspectionTime = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_actual_inspection_time];
    self.inspectionStatus = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_inspection_status];
    self.tableView.backgroundColor = [UIColor clearColor];
    
    timeLeftInspectionStatus = raModel.roomAssignmentRoomExpectedInspectionTime * 60;
    
    countTimeStatus = NOT_START;
    [finishTimeButton setUserInteractionEnabled:NO];
    [finishTimeButton setBackgroundImage:[UIImage imageNamed:IMAGE_COMPLETE_GRAY] forState:UIControlStateNormal];
    
    timeFomarter = [[NSDateFormatter alloc] init];
    [timeFomarter setDateFormat:[ehkConvert getyyyy_MM_dd_HH_mm_ss]];
    
    // assign data for compare property edited:
    dataModelCompare = [[RoomAssignmentModelV2 alloc] init];
    dataModelCompare.roomAssignmentRoomCleaningStatusId = raModel.roomAssignmentRoomCleaningStatusId;
    dataModelCompare.roomAssignmentRoomStatusId = raModel.roomAssignmentRoomStatusId;
    
    durationTime = 0;
    isSaved = YES;
    
    if(roomRecord == nil){
        roomRecord = [[RoomRecordModelV2 alloc] init];
        roomRecord.rrec_User_Id = [[UserManagerV2 sharedUserManager] currentUser].userId;
        roomRecord.rrec_room_assignment_id = [TasksManagerV2 getCurrentRoomAssignment] ;
        
        [[RoomManagerV2 sharedRoomManager] loadRoomRecordDataByRoomIdAndUserId:roomRecord];
        if(roomRecord.rrec_Id <=0){
            roomRecord.rrec_PostStatus = POST_STATUS_UN_CHANGED;
            [[RoomManagerV2 sharedRoomManager] insertRoomRecordData:roomRecord];
            roomRecord = [[RoomManagerV2 sharedRoomManager] loadRoomRecordDataByRoomIdAndUserId:roomRecord];
        }
        
    }
    
    timeLeftInspectionStatus = raModel.roomAssignmentRoomExpectedInspectionTime * 60;
    
    [self layoutButton];
    
}

#pragma didden after save.

-(void) hiddenHUDAfterSaved:(MBProgressHUD *)HUD{
    [HUD hide:YES];
    [HUD removeFromSuperview];
}


- (void)viewDidUnload
{
    [self setListButtonView:nil];
    [self setReassignBtn:nil];
    timerButon = nil;
    finishTimeButton = nil;
    countTimeButton = nil;
    checkListButton = nil;
    lockButton = nil;
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - === Last Cleaning Date ===
#pragma mark
-(void)lastCleaningDateCell:(UITableViewCell *)cell {
    UILabel *label = (UILabel *)[cell.contentView viewWithTag:tagLabel];
    if (label == nil) {
        label = [[UILabel alloc] initWithFrame:CGRectMake(10, 1, 150, 30)];
        [cell.contentView addSubview:label];
        label.textAlignment = UITextAlignmentLeft;
        label.font = [UIFont boldSystemFontOfSize:12];
        label.tag = tagLabel;
        UIImage *img = [UIImage imageNamed:@"tab_small.png"];
        label.textColor = [UIColor colorWithPatternImage:img];
        [label setBackgroundColor:[UIColor clearColor]];
    }
    
    [label setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_last_cleaning_date]];
    
    //remove image if cell contains image
    UIImageView* imageView = (UIImageView *)[cell.contentView viewWithTag:tagImage];
    [imageView removeFromSuperview];
    
    UILabel *value = (UILabel *)[cell.contentView viewWithTag:tagText];
    if (value == nil) {
        value = [[UILabel alloc] initWithFrame:CGRectMake(160, 1, 135 , 30)];
        value.textAlignment = UITextAlignmentRight;
        value.font = [UIFont boldSystemFontOfSize:12];
        value.lineBreakMode = UILineBreakModeWordWrap;
        value.tag = tagText;
        [value setTextColor:[UIColor blackColor]];
        [value setBackgroundColor:[UIColor clearColor]];
        [cell.contentView addSubview:value];
    }
    
    NSDate *now = [NSDate date];
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"dd / MM / yyyy"];
    NSString *nowString = [format stringFromDate:now];
    if([LogFileManager isLogConsole])
    {
        NSLog(@"last cleaning date %@", roomRecord.rrec_Cleaning_Date);
    }
    if ([nowString isEqualToString:roomRecord.rrec_Cleaning_Date]) {
        [value setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_today]];
    } else {
        [value setText:roomRecord.rrec_Cleaning_Date];
    }
    
    
}

#pragma mark - === RA Full Name Cell ===
#pragma mark
-(void)raFullNameCell:(UITableViewCell *)cell {
    UILabel *label = (UILabel *)[cell.contentView viewWithTag:tagLabel];
    if (label == nil) {
        label = [[UILabel alloc] initWithFrame:CGRectMake(10, 1, 150, 30)];
        [cell.contentView addSubview:label];
        label.textAlignment = UITextAlignmentLeft;
        label.font = [UIFont boldSystemFontOfSize:12];
        label.tag = tagLabel;
        UIImage *img = [UIImage imageNamed:@"tab_small.png"];
        label.textColor = [UIColor colorWithPatternImage:img];
        [label setBackgroundColor:[UIColor clearColor]];
    }
    
    [label setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_room_attendant_name]];
    
    //remove image if cell contains image
    UIImageView* imageView = (UIImageView *)[cell.contentView viewWithTag:tagImage];
    [imageView removeFromSuperview];
    
    UILabel *value = (UILabel *)[cell.contentView viewWithTag:tagText];
    if (value == nil) {
        value = [[UILabel alloc] initWithFrame:CGRectMake(160, 1, 135 , 30)];
        value.textAlignment = UITextAlignmentRight;
        value.font = [UIFont boldSystemFontOfSize:12];
        value.lineBreakMode = UILineBreakModeWordWrap;
        value.tag = tagText;
        [value setTextColor:[UIColor blackColor]];
        [value setBackgroundColor:[UIColor clearColor]];
        [cell.contentView addSubview:value];
    }
    
    NSMutableString *raFullName = [NSMutableString string];
    if (raModel.roomAssignmentFirstName != nil) {
        [raFullName appendString:raModel.roomAssignmentFirstName];
    }
    if (raModel.roomAssignmentLastName != nil) {
        [raFullName appendFormat:@" %@", raModel.roomAssignmentLastName];
    }
    
    [value setText:raFullName];
}

#pragma mark - === Table View Setup ===
#pragma mark

- (void)addAccessoryButton:(UITableViewCell *)cell  {
    /*****************************************/
    UIImageView *accessoryButton = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 24, 24)];
    [accessoryButton setBackgroundColor:[UIColor clearColor]];
    [accessoryButton setImage:[UIImage imageNamed:@"gray_arrow_36x36.png"]];
    [cell setAccessoryView:accessoryButton];
    
    /*****************************************/
}

/****************************** implement cell *************************************/
- (void)setupAdditionalCell:(UITableViewCell *)cell isNew:(BOOL)isNew
{
    CGRect additionalJobLabelRect = CGRectMake(10, 1, 150 , 30);
    // display label control in table view
    
    UILabel *actualCleaningTimeLabel;
    
    if (isNew == TRUE) {
        actualCleaningTimeLabel = [[UILabel alloc] initWithFrame:additionalJobLabelRect];
    } else {
        actualCleaningTimeLabel = (UILabel *)[cell.contentView viewWithTag:tagLabel];
    }
    
    actualCleaningTimeLabel.textAlignment = UITextAlignmentLeft;
    actualCleaningTimeLabel.font = [UIFont boldSystemFontOfSize:12];
    actualCleaningTimeLabel.text = self.additioJob;
    actualCleaningTimeLabel.tag = tagLabel;
    
    UIImage *img=[UIImage imageNamed:@"tab_small.png"];
    actualCleaningTimeLabel.textColor=[UIColor colorWithPatternImage:img];
    
    [actualCleaningTimeLabel setBackgroundColor:[UIColor clearColor]];
    if (isNew == TRUE) {
        [cell.contentView addSubview:actualCleaningTimeLabel];
        
    }
    
    
    //label 2 of this cell
    NSString *tmp = dataModel.room_AdditionalJob;
    CGSize t = [tmp sizeWithFont:[UIFont boldSystemFontOfSize:12] constrainedToSize:CGSizeMake(135, 300) lineBreakMode:UILineBreakModeWordWrap];
    CGRect additionalJobValueRect;
    if (t.height > 20) {
        additionalJobValueRect = CGRectMake(160, 9, 135 , t.height);
    }
    else {
        additionalJobValueRect = CGRectMake(160, 1, 135 , 30);
    }
    UILabel *additionalJobValue;
    
    if (isNew == TRUE) {
        additionalJobValue = [[UILabel alloc] initWithFrame:additionalJobValueRect];
    } else {
        additionalJobValue = (UILabel *)[cell.contentView viewWithTag:tagText];
    }
    
    [additionalJobValue setFrame:additionalJobValueRect];
    
    additionalJobValue.textAlignment = UITextAlignmentRight;
    additionalJobValue.font = [UIFont boldSystemFontOfSize:12];
    additionalJobValue.lineBreakMode = UILineBreakModeWordWrap;
    int tmprow = floor(t.height/20) + 1;
    additionalJobValue.numberOfLines = tmprow;
    additionalJobValue.tag = tagText;
    
    //set additionalJobValue from dataModel
    [additionalJobValue setText:dataModel.room_AdditionalJob];
    
    [additionalJobValue setBackgroundColor:[UIColor clearColor]];
    
    //remove image if cell contains image
    UIImageView* imageView = (UIImageView *)[cell.contentView viewWithTag:tagImage];
    [imageView removeFromSuperview];
    
    if (isNew == TRUE) {
        [cell.contentView addSubview:additionalJobValue];
    }
}

// display time with format : ##hour ##minute
-(NSString*) timeDisplay:(NSInteger)time{
    if (time <= 0) {
        return [NSString stringWithFormat:@"0 %@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_strmins]];
    }
    
    NSInteger hoursInt = time/3600;
    NSInteger minutesInt = (time - (hoursInt*3600)) / 60;
    
    // setup hour string
    NSString *hoursString = [NSString stringWithFormat:@"%d",hoursInt];
    NSString *minutesString = [NSString stringWithFormat:@"%d",minutesInt];
    
    if([hoursString length]==1){
        hoursString = [NSString stringWithFormat:@"%.2d %@",[hoursString integerValue], [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_strhour]];
        if(hoursInt == 0){
            hoursString = @"";
        }
    } else {
        hoursString = [NSString stringWithFormat:@"%@ %@",hoursString, [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_strhour]];
    }
    
    // setup minute string
    if([minutesString length]==1){
        minutesString = [NSString stringWithFormat:@"%.2d %@",[minutesString integerValue], [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_strmins]];
        if(minutesInt == 0){
            minutesString = @"";
        }
        
    }else{
        minutesString = [NSString stringWithFormat:@"%@ %@",minutesString, [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_strmins]];
    }
    
    return [NSString stringWithFormat:@"%@ %@", hoursString, minutesString];
}

// display time with format : ##hour ##minute ## seconds
-(NSString*) timeDisplayWithSecond:(NSInteger)time{
    if (time <= 0) {
        return [NSString stringWithFormat:@"0 %@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_strmins]];
    }
    
    NSInteger hoursInt = time / 3600;
    NSInteger minutesInt = (time - (hoursInt * 3600)) / 60;
    NSInteger secondInt = time % 60;
    // setup hour string
    NSString *hoursString = [NSString stringWithFormat:@"%d", hoursInt];
    NSString *minutesString = [NSString stringWithFormat:@"%d", minutesInt];
    NSString *secondsString = [NSString stringWithFormat:@"%d", secondInt];
    
    if(hoursInt <= 1){
        hoursString = [NSString stringWithFormat:@"%d %@",[hoursString integerValue], [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_strhour]];
        if(hoursInt == 0){
            hoursString = @"";
        }
    } else {
        hoursString = [NSString stringWithFormat:@"%@ %@",hoursString, [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_strhour]];
    }
    
    // setup minute string
    if(minutesInt <= 1){
        minutesString = [NSString stringWithFormat:@"%d %@",[minutesString integerValue], [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_strmins]];
        if(minutesInt == 0){
            minutesString = @"";
        }
        
    }else{
        minutesString = [NSString stringWithFormat:@"%@ %@",minutesString, [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_strmins]];
    }
    
    if(secondInt <= 1){
        secondsString = [NSString stringWithFormat:@"%@ second",secondsString];
    } else{
        secondsString = [NSString stringWithFormat:@"%@ seconds",secondsString];
    }
    return [NSString stringWithFormat:@"%@ %@", hoursString, minutesString];
}

- (void)setupExpectedCleaningCell:(UITableViewCell *)cell isNew:(BOOL)isNew
{
    CGRect expectedCleaningLabelRect = CGRectMake(10, 1, 200 , 30);
    // display label control in table view
    UILabel *expectedCleaningLabel;
    if (isNew == TRUE) {
        expectedCleaningLabel = [[UILabel alloc] initWithFrame:expectedCleaningLabelRect];
    } else {
        expectedCleaningLabel = (UILabel *)[cell.contentView viewWithTag:tagLabel];
    }
    
    expectedCleaningLabel.textAlignment = UITextAlignmentLeft;
    expectedCleaningLabel.font = [UIFont boldSystemFontOfSize:12];
    expectedCleaningLabel.text = self.expectedCleaningTime;
    expectedCleaningLabel.tag = tagLabel;
    UIImage *img=[UIImage imageNamed:@"tab_small.png"];
    expectedCleaningLabel.textColor=[UIColor colorWithPatternImage:img];
    
    [expectedCleaningLabel setBackgroundColor:[UIColor clearColor]];
    if (isNew == TRUE) {
        [cell.contentView addSubview:expectedCleaningLabel];
    }
    
    //label 2 of this cell
    CGRect expectedCleaningValueRect = CGRectMake(160, 1, 135 , 30);
    
    UILabel *expectedCleaningTimeValue;
    if (isNew == TRUE) {
        expectedCleaningTimeValue=[[UILabel alloc] initWithFrame:expectedCleaningValueRect];
    } else {
        expectedCleaningTimeValue = (UILabel *)[cell.contentView viewWithTag:tagText];
    }
    
    expectedCleaningTimeValue.textAlignment = UITextAlignmentRight;
    expectedCleaningTimeValue.font = [UIFont boldSystemFontOfSize:12];
    expectedCleaningTimeValue.tag = tagText;
    // set expectedCleaningTimeValue from dataModel
    [expectedCleaningTimeValue setText:[self timeDisplay:60 * raModel.roomAssignmentRoomExpectedCleaningTime]];
    
    [expectedCleaningTimeValue setBackgroundColor:[UIColor clearColor]];
    
    UIImageView* imageView = (UIImageView *)[cell.contentView viewWithTag:tagImage];
    [imageView removeFromSuperview];
    
    if (isNew == TRUE) {
        [cell.contentView addSubview:expectedCleaningTimeValue];
    }
}

- (void)actualTimeOfTaskStatus:(UILabel *)label  {
    
    //Hao Tran - Remove unneccessary changed
    /*
     //get cleaning time from room record
     NSInteger secondDifference = [roomRecord.rrec_Cleaning_Duration integerValue];
     
     NSMutableString *timeString = [NSMutableString string];
     NSInteger minutesInt;
     NSInteger hoursInt;
     
     
     hoursInt = secondDifference / 3600;
     minutesInt = (secondDifference - (hoursInt * 3600)) / 60;
     
     
     if (hoursInt != 0) {
     [timeString appendFormat:@"%.2d %@", hoursInt, [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_strhour]];
     }
     
     if (minutesInt != 0) {
     [timeString appendFormat:@" %.2d %@", minutesInt, [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_strmins]];
     }
     
     if (minutesInt == 0 && hoursInt == 0) {
     [timeString appendFormat:@"%d %@", minutesInt, [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_strmins]];
     }
     
     [label setText:timeString];
     [label setBackgroundColor:[UIColor clearColor]];
     */
}

- (void)actualCleaningTimeBySuperVisor:(UILabel *)label  {
    
    //Hao Tran - Remove for unneccessary changed
    /*
     //get cleaning time from room record
     NSInteger secondDifference = [roomRecord.rrec_Cleaning_Duration integerValue];
     
     NSMutableString *timeString = [NSMutableString string];
     NSInteger minutesInt;
     NSInteger hoursInt;
     hoursInt = secondDifference / 3600;
     minutesInt = (secondDifference - (hoursInt * 3600)) / 60;
     
     if (hoursInt != 0) {
     [timeString appendFormat:@"%.2d %@", hoursInt, [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_strhour]];
     }
     
     if (minutesInt != 0) {
     [timeString appendFormat:@" %.2d %@", minutesInt, [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_strmins]];
     }
     
     if (minutesInt == 0 && hoursInt == 0) {
     [timeString appendFormat:@"%d %@", minutesInt, [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_strmins]];
     }
     
     
     [label setText:timeString];
     [label setBackgroundColor:[UIColor clearColor]];
     
     //set color
     
     NSInteger expectionCleaningTime = raModel.roomAssignmentRoomExpectedCleaningTime * 60;
     if (expectionCleaningTime > secondDifference) {
     [label setTextColor:[UIColor greenColor]];
     } else
     [label setTextColor:[UIColor redColor]];
     */
}


- (void)setupActualCleaningTimeCell:(UITableViewCell *)cell isNew:(BOOL)isNew
{
    CGRect actualCleaningTimeLabelRect = CGRectMake(10, 1, 150 , 30);
    // display label control in table view
    
    UILabel *actualCleaningLabel;
    
    if (isNew == TRUE) {
        actualCleaningLabel = [[UILabel alloc] initWithFrame:actualCleaningTimeLabelRect];
    } else {
        actualCleaningLabel = (UILabel *)[cell.contentView viewWithTag:tagLabel];
    }
    
    actualCleaningLabel.textAlignment = UITextAlignmentLeft;
    actualCleaningLabel.font = [UIFont boldSystemFontOfSize:12];
    actualCleaningLabel.text = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_actual_cleaning_time];
    actualCleaningLabel.tag = tagLabel;
    
    UIImage *img=[UIImage imageNamed:@"tab_small.png"];
    actualCleaningLabel.textColor=[UIColor colorWithPatternImage:img];
    
    [actualCleaningLabel setBackgroundColor:[UIColor clearColor]];
    if (isNew == TRUE) {
        [cell.contentView addSubview:actualCleaningLabel];
    }
    
    
    //label 2 of this cell
    NSString *tmp = dataModel.room_AdditionalJob;
    CGSize t = [tmp sizeWithFont:[UIFont boldSystemFontOfSize:12] constrainedToSize:CGSizeMake(135, 300) lineBreakMode:UILineBreakModeWordWrap];
    CGRect actualCleaningValueRect;
    if (t.height > 20) {
        actualCleaningValueRect = CGRectMake(160, 9, 135 , t.height);
    }
    else{
        actualCleaningValueRect = CGRectMake(160, 1, 135 , 30);
    }
    
    UILabel *actualCleaningValue;
    
    if (isNew == TRUE) {
        actualCleaningValue=[[UILabel alloc] initWithFrame:actualCleaningValueRect];
    } else {
        actualCleaningValue = (UILabel *)[cell.contentView viewWithTag:tagText];
    }
    
    actualCleaningValue.textAlignment = UITextAlignmentRight;
    actualCleaningValue.font = [UIFont boldSystemFontOfSize:12];
    actualCleaningValue.lineBreakMode = UILineBreakModeWordWrap;
    int tmprow = floor(t.height/20) + 1;
    actualCleaningValue.numberOfLines = tmprow;
    actualCleaningValue.tag = tagText;
    [actualCleaningValue setTextColor:[UIColor greenColor]];
    
    [self actualCleaningTimeBySuperVisor:actualCleaningValue];
    
    /////////////////////////////////////////////////////
    
    //remove image view if cell contain image
    UIImageView* imageView = (UIImageView *)[cell.contentView viewWithTag:tagImage];
    [imageView removeFromSuperview];
    
    if (isNew == TRUE) {
        [cell.contentView addSubview:actualCleaningValue];
    }
    
}


- (void)setupCleaningStatusCell:(UITableViewCell *)cell isNew:(BOOL)isNew
{
    CGRect roomStatusLabelRect = CGRectMake(10, 1, 150 , 30);
    // display label control in table view
    
    UILabel *cleaningStatusLabel;
    
    if (isNew == TRUE) {
        cleaningStatusLabel = [[UILabel alloc] initWithFrame:roomStatusLabelRect];
    } else {
        cleaningStatusLabel = (UILabel *)[cell.contentView viewWithTag:tagLabel];
    }
    cleaningStatusLabel.textAlignment = UITextAlignmentLeft;
    cleaningStatusLabel.font = [UIFont boldSystemFontOfSize:12];
    cleaningStatusLabel.text = self.cleaningStatus;
    cleaningStatusLabel.tag = tagLabel;
    UIImage *img=[UIImage imageNamed:@"tab_small.png"];
    cleaningStatusLabel.textColor=[UIColor colorWithPatternImage:img];
    
    [cleaningStatusLabel setBackgroundColor:[UIColor clearColor]];
    if (isNew == TRUE) {
        [cell.contentView addSubview:cleaningStatusLabel];
        
    }
    
    //label 2 of this cell
    CGRect cleaningStatusValueRectSuperVisor = CGRectMake(190, 10, 105 , 30);
    UILabel *cleanigStatusValue;
    
    // align  text in 2 case: user and super user.
    if (isNew == TRUE) {
        cleanigStatusValue=[[UILabel alloc] initWithFrame:cleaningStatusValueRectSuperVisor];
    } else {
        cleanigStatusValue = (UILabel *)[cell.contentView viewWithTag:tagText];
    }
    
    
    cleanigStatusValue.textAlignment = UITextAlignmentRight;
    cleanigStatusValue.font = [UIFont boldSystemFontOfSize:12];
    
    //set cleaningStatusValue from dataModel
    CleaningStatusModelV2 *cleaningStatusModel=[[CleaningStatusModelV2 alloc] init];
    cleaningStatusModel.cstat_Id = raModel.roomAssignmentRoomCleaningStatusId;
    [[RoomManagerV2 sharedRoomManager] loadCleaningStatus:cleaningStatusModel];
    
    if ([[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString:[NSString stringWithFormat:@"%@",ENGLISH_LANGUAGE]]) {
        [cleanigStatusValue setText: cleaningStatusModel.cstat_Name];
    } else {
        [cleanigStatusValue setText: cleaningStatusModel.cstat_Language];
    }
    cleanigStatusValue.tag=tagText;
    
    [self addAccessoryButton: cell];
    /*****************************************/
    
    UIImageView *cleaningStatusImageView;
    if (isNew == TRUE) {
        cleaningStatusImageView = [[UIImageView alloc] initWithFrame:CGRectMake(160, 1, 30, 30)];
        [cell.contentView addSubview:cleaningStatusImageView];
    } else {
        cleaningStatusImageView = (UIImageView *)[cell.contentView viewWithTag:tagImage];
    }
    if(raModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_PENDING_SECOND_TIME){
        [cleaningStatusImageView setFrame:CGRectMake(160, 1, 31, 31)];
    }else{
        [cleaningStatusImageView setFrame:CGRectMake(160, 1, 31, 31)];
    }
    cleaningStatusImageView.tag = tagImage;
    
    
    RoomServiceLaterModelV2 *roomServiceLater = [[RoomServiceLaterModelV2 alloc] init];
    roomServiceLater.rsl_RecordId = roomRecord.rrec_Id;
    if(raModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_SERVICE_LATER){
        // get latest room service later
        [[RoomManagerV2 sharedRoomManager] loadRoomServiceLaterDataByRecordId:roomServiceLater];
    }
    
    [cleaningStatusImageView setImage:[UIImage imageWithData:cleaningStatusModel.cstat_image]];
    [cleanigStatusValue setText: cleaningStatusModel.cstat_Language];
    
    
    /********* resize frame of image and label*************/
    CGSize size = [cleanigStatusValue.text sizeWithFont:cleaningStatusLabel.font];
    CGFloat x = xOfSccessory - size.width-space2ElementCell;
    [cleanigStatusValue setFrame:CGRectMake(x, cleanigStatusValue.frame.origin.y, size.width, size.height)];
    [cleaningStatusImageView setFrame:CGRectMake(x-cleaningStatusImageView.frame.size.width, cleaningStatusImageView.frame.origin.y, cleaningStatusImageView.frame.size.width, cleaningStatusImageView.frame.size.height)];
    /********* end resize frame of image and label*************/
    
    
    
    [cleanigStatusValue setBackgroundColor:[UIColor clearColor]];
    if (isNew == TRUE) {
        [cell.contentView addSubview:cleanigStatusValue];
        [cell.contentView addSubview:cleaningStatusImageView];
    }
    
}

- (void)roomStatusCell:(UITableViewCell *)cell isNew:(BOOL)isNew
{
    CGRect roomStatusLabelRect = CGRectMake(10, 1, 150 , 30);
    // display label control in table view
    
    UILabel *roomStatusLabel;
    
    if (isNew == TRUE) {
        roomStatusLabel = [[UILabel alloc] initWithFrame:roomStatusLabelRect];
    } else {
        roomStatusLabel = (UILabel *)[cell.contentView viewWithTag:tagLabel];
    }
    roomStatusLabel.textAlignment = UITextAlignmentLeft;
    roomStatusLabel.font = [UIFont boldSystemFontOfSize:12];
    roomStatusLabel.text = self.roomStatus;
    roomStatusLabel.tag = tagLabel;
    UIImage *img=[UIImage imageNamed:@"tab_small.png"];
    roomStatusLabel.textColor=[UIColor colorWithPatternImage:img];
    
    [roomStatusLabel setBackgroundColor:[UIColor clearColor]];
    if (isNew == TRUE) {
        [cell.contentView addSubview:roomStatusLabel];
        
    }
    
    //label 2 of this cell
    CGRect cleaningStatusValueRectSuperVisor = CGRectMake(190, 1, 105 , 30);
    UILabel *roomStatusValue;
    // align  text in 2 case: user and super user.
    if (isNew == TRUE) {
        roomStatusValue=[[UILabel alloc] initWithFrame:cleaningStatusValueRectSuperVisor];
    } else {
        roomStatusValue = (UILabel *)[cell.contentView viewWithTag:tagText];
    }
    
    roomStatusValue.textAlignment = UITextAlignmentRight;
    roomStatusValue.font = [UIFont boldSystemFontOfSize:FONT_SIZE_ROOM_STATUS];
    
    //set room status value from dataModel
    RoomStatusModelV2 *roomStatusModel=[[RoomStatusModelV2 alloc] init];
    roomStatusModel.rstat_Id = raModel.roomAssignmentRoomStatusId;
    
    [[RoomManagerV2 sharedRoomManager] loadRoomStatusData:roomStatusModel];
    
    if ([[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString:[NSString stringWithFormat:@"%@",ENGLISH_LANGUAGE]]) {
        [roomStatusValue setText:roomStatusModel.rstat_Name];
        
    } else {
        [roomStatusValue setText:roomStatusModel.rstat_Lang];
    }
    
    [self addAccessoryButton:cell];
    
    
    roomStatusValue.tag=tagText;
    UIImageView *imageRoomStatusView ;
    imageRoomStatusView = (UIImageView *)[cell.contentView viewWithTag:tagImage];
    if (imageRoomStatusView == nil) {
        imageRoomStatusView = [[UIImageView alloc] initWithFrame:CGRectMake(160, 1, 30, 30)];
        imageRoomStatusView.tag = tagImage;
        [cell.contentView addSubview:imageRoomStatusView];
    }
    
    [imageRoomStatusView setImage:[UIImage imageWithData:roomStatusModel.rstat_Image]];
    [roomStatusValue setText: roomStatusModel.rstat_Name];
    
    
    /********* resize frame of image and label*************/
    CGSize size = [roomStatusModel.rstat_Name sizeWithFont:roomStatusValue.font];
    CGFloat x = xOfSccessory - size.width-space2ElementCell;
    [roomStatusValue setFrame:CGRectMake(x, roomStatusValue.frame.origin.y, size.width, 30)];
    [imageRoomStatusView setFrame:CGRectMake(x-imageRoomStatusView.frame.size.width, imageRoomStatusView.frame.origin.y, imageRoomStatusView.frame.size.width, imageRoomStatusView.frame.size.height)];
    /********* end resize frame of image and label*************/
    
    
    [roomStatusValue setBackgroundColor:[UIColor clearColor]];
    if (isNew == TRUE) {
        [cell.contentView addSubview:roomStatusValue];
        [cell.contentView addSubview:imageRoomStatusView];
    }
}

- (void)setupRemarkCell:(UITableViewCell *)cell isNew:(BOOL)isNew
{
    CGRect remarkLabelRect = CGRectMake(10, 1, 150 , 30);
    // display label control in table view
    
    UILabel *remarkLabel;
    
    if (isNew == TRUE) {
        remarkLabel = [[UILabel alloc] initWithFrame:remarkLabelRect];
    } else {
        remarkLabel = (UILabel *)[cell.contentView viewWithTag:tagLabel];
    }
    remarkLabel.textAlignment = UITextAlignmentLeft;
    remarkLabel.font = [UIFont boldSystemFontOfSize:12];
    remarkLabel.text = self.remark;
    remarkLabel.tag = tagLabel;
    UIImage *img=[UIImage imageNamed:@"tab_small.png"];
    remarkLabel.textColor=[UIColor colorWithPatternImage:img];
    
    [remarkLabel setBackgroundColor:[UIColor clearColor]];
    if (isNew == TRUE) {
        [cell.contentView addSubview:remarkLabel];
        
    }
    
    //label 2 of this cell
    CGRect remarkValueRectSuperVisor = CGRectMake(170, 1, 103 , 30);
    UILabel *remarkValue;
    // align  text in 2 case: user and super user.
    if (isNew == TRUE) {
        remarkValue=[[UILabel alloc] initWithFrame:remarkValueRectSuperVisor];
    } else {
        remarkValue = (UILabel *)[cell.contentView viewWithTag:tagText];
        [remarkValue setFrame:remarkValueRectSuperVisor];
    }
    
    remarkValue.textAlignment = UITextAlignmentRight;
    remarkValue.font = [UIFont boldSystemFontOfSize:12];
    
    
    [self addAccessoryButton:cell];
    
    remarkValue.tag = tagText;
    [remarkValue setBackgroundColor:[UIColor clearColor]];
    [remarkValue setText:roomRecord.rrec_Remark == nil ? @"" : roomRecord.rrec_Remark];
    
    //remove image if cell contains image
    UIImageView* imageView = (UIImageView *)[cell.contentView viewWithTag:tagImage];
    [imageView removeFromSuperview];
    
    if (isNew == TRUE) {
        [cell.contentView addSubview:remarkValue];
        
    }
}


- (void)setupEstimatedInspectionTimeCell:(UITableViewCell *)cell isNew:(BOOL)isNew
{
    CGRect expectedCleaningLabelRect = CGRectMake(10, 1, 200 , 30);
    // display label control in table view
    UILabel *expectedCleaningLabel;
    UIImageView *imageRoomStatusView;
    
    imageRoomStatusView = (UIImageView*)[cell.contentView viewWithTag:tagImage];
    if (imageRoomStatusView == nil) {
        //remove image if cell contains image
        [imageRoomStatusView removeFromSuperview];
    }
    
    if (isNew == TRUE) {
        
        expectedCleaningLabel = [[UILabel alloc] initWithFrame:expectedCleaningLabelRect];
        
    } else {
        
        expectedCleaningLabel = (UILabel *)[cell.contentView viewWithTag:tagLabel];
        [expectedCleaningLabel setFrame:expectedCleaningLabelRect];
    }
    
    expectedCleaningLabel.textAlignment = UITextAlignmentLeft;
    expectedCleaningLabel.font = [UIFont boldSystemFontOfSize:12];
    expectedCleaningLabel.text = self.estimatedInspectionTime;
    expectedCleaningLabel.tag = tagLabel;
    UIImage *img=[UIImage imageNamed:@"tab_small.png"];
    expectedCleaningLabel.textColor=[UIColor colorWithPatternImage:img];
    
    [expectedCleaningLabel setBackgroundColor:[UIColor clearColor]];
    if (isNew == TRUE) {
        [cell.contentView addSubview:expectedCleaningLabel];
    }
    
    //label 2 of this cell
    CGRect expectedCleaningValueRect = CGRectMake(160, 1, 135, 30);
    
    UILabel *expectedCleaningTimeValue;
    if (isNew == TRUE) {
        expectedCleaningTimeValue=[[UILabel alloc] initWithFrame:expectedCleaningValueRect];
    } else {
        expectedCleaningTimeValue = (UILabel *)[cell.contentView viewWithTag:tagText];
        [expectedCleaningTimeValue setFrame:expectedCleaningValueRect];
    }
    
    expectedCleaningTimeValue.textAlignment = UITextAlignmentRight;
    expectedCleaningTimeValue.font = [UIFont boldSystemFontOfSize:12];
    expectedCleaningTimeValue.tag = tagText;
    [expectedCleaningTimeValue setText:[self timeDisplay: raModel.roomAssignmentRoomExpectedInspectionTime * 60]];
    [expectedCleaningTimeValue setBackgroundColor:[UIColor clearColor]];
    
    if (isNew == TRUE) {
        
        [cell.contentView addSubview:expectedCleaningTimeValue];
        
    }
}

- (void)setupActualInspectionTimeCell:(UITableViewCell *)cell isNew:(BOOL)isNew
{
    //Hao Tran - Remove for unneccessary change
    /*
     CGRect actualInspectionTimeLabelRect = CGRectMake(10, 1, 200 , 30);
     // display label control in table view
     UILabel *actualInspectiionTimeLabel;
     if (isNew == TRUE) {
     actualInspectiionTimeLabel = [[UILabel alloc] initWithFrame:actualInspectionTimeLabelRect];
     } else {
     actualInspectiionTimeLabel = (UILabel *)[cell.contentView viewWithTag:tagLabel];
     }
     
     actualInspectiionTimeLabel.textAlignment = UITextAlignmentLeft;
     actualInspectiionTimeLabel.font = [UIFont boldSystemFontOfSize:12];
     actualInspectiionTimeLabel.text = self.actualInspectionTime;
     actualInspectiionTimeLabel.tag = tagLabel;
     UIImage *img=[UIImage imageNamed:@"tab_small.png"];
     actualInspectiionTimeLabel.textColor=[UIColor colorWithPatternImage:img];
     
     [actualInspectiionTimeLabel setBackgroundColor:[UIColor clearColor]];
     if (isNew == TRUE) {
     [cell.contentView addSubview:actualInspectiionTimeLabel];
     
     }
     
     CGRect actualInspectionValueRect = CGRectMake(160, 1, 135 , 30);
     
     UILabel *actualInspectionTimeValue;
     if (isNew == TRUE) {
     actualInspectionTimeValue=[[UILabel alloc] initWithFrame:actualInspectionValueRect];
     } else {
     actualInspectionTimeValue = (UILabel *)[cell.contentView viewWithTag:tagText];
     [actualInspectionTimeValue setFrame:actualInspectionValueRect];
     }
     
     actualInspectionTimeValue.textAlignment = UITextAlignmentRight;
     actualInspectionTimeValue.font = [UIFont boldSystemFontOfSize:12];
     actualInspectionTimeValue.tag = tagText;
     [actualInspectionTimeValue setTextColor:[UIColor greenColor]];
     [actualInspectionTimeValue setBackgroundColor:[UIColor clearColor]];
     
     
     if([roomRecord.rrec_Inspection_Duration integerValue] > 0){
     
     timeLeftInspectionStatus = raModel.roomAssignmentRoomExpectedInspectionTime * 60 - [roomRecord.rrec_Inspection_Duration integerValue];
     durationTime = [roomRecord.rrec_Inspection_Duration integerValue];
     }
     
     
     [actualInspectionTimeValue setText:[self timeDisplayWithSecond:durationTime]];
     if(timeLeftInspectionStatus < 0){
     [actualInspectionTimeValue setTextColor:[UIColor redColor]];
     }
     
     if (isNew == TRUE) {
     [cell.contentView addSubview:actualInspectionTimeValue];
     
     }
     
     //remove image if cell contains image
     UIImageView* imageView = (UIImageView *)[cell.contentView viewWithTag:tagImage];
     [imageView removeFromSuperview];
     */
}

#pragma mark - === UITableView Datasource Methods ===
#pragma mark

-(NSInteger)tableView:(UITableView *)atableView numberOfRowsInSection:(NSInteger)section
{
    
    //add + 1 row for RA Full Name row by CR 5.0
    if([self isRoomInspected] && (raModel.roomAssignmentRoomStatusId != ENUM_ROOM_STATUS_OPU) ){
        return 8 + 1 + 1;
    } else{
        return 7 + 1 + 1;
    }
}

- (void)setupInspectionStatusCell:(UITableViewCell *)cell isNew:(BOOL)isNew
{
    CGRect roomStatusLabelRect = CGRectMake(10, 1, 150 , 30);
    // display label control in table view
    
    UILabel *roomStatusLabel;
    
    if (isNew == TRUE) {
        roomStatusLabel = [[UILabel alloc] initWithFrame:roomStatusLabelRect];
    } else {
        roomStatusLabel = (UILabel *)[cell.contentView viewWithTag:tagLabel];
    }
    roomStatusLabel.textAlignment = UITextAlignmentLeft;
    roomStatusLabel.font = [UIFont boldSystemFontOfSize:12];
    roomStatusLabel.text = self.inspectionStatus;
    roomStatusLabel.tag = tagLabel;
    UIImage *img=[UIImage imageNamed:@"tab_small.png"];
    roomStatusLabel.textColor=[UIColor colorWithPatternImage:img];
    
    [roomStatusLabel setBackgroundColor:[UIColor clearColor]];
    if (isNew == TRUE) {
        [cell.contentView addSubview:roomStatusLabel];
        
    }
    
    //label 2 of this cell
    CGRect cleaningStatusValueRect = CGRectMake(160, 10, 105 , 30);
    UILabel *inspectionStatusValue;
    UIImageView *imageRoomStatusView;
    
    if (isNew == TRUE) {
        inspectionStatusValue=[[UILabel alloc] initWithFrame:cleaningStatusValueRect];
        imageRoomStatusView = [[UIImageView alloc] initWithFrame:CGRectMake(170, 1, 30, 30)];
        [imageRoomStatusView setTag:tagImage];
    } else {
        inspectionStatusValue = (UILabel *)[cell.contentView viewWithTag:tagText];
        imageRoomStatusView = (UIImageView*)[cell.contentView viewWithTag:tagImage];
    }
    
    if (imageRoomStatusView == nil) {
        imageRoomStatusView = [[UIImageView alloc] initWithFrame:CGRectMake(170, 1, 30, 30)];
        [imageRoomStatusView setTag:tagImage];
        [cell.contentView addSubview:imageRoomStatusView];
    }
    
    inspectionStatusValue.textAlignment = UITextAlignmentRight;
    inspectionStatusValue.font = [UIFont boldSystemFontOfSize:12];
    inspectionStatusValue.tag = tagText;
    [inspectionStatusValue setBackgroundColor:[UIColor clearColor]];
    [inspectionStatusValue setTextColor:[UIColor blackColor]];
    
    InspectionStatusModel *inspectionStatusModel=[[InspectionStatusModel alloc] init];
    inspectionStatusModel.istatId = raModel.roomAssignmentRoomInspectionStatusId;
    [[RoomManagerV2 sharedRoomManager] loadInspectionStatusModelByPrimaryKey:inspectionStatusModel];
    
    if ([[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString:[NSString stringWithFormat:@"%@",ENGLISH_LANGUAGE]]) {
        [inspectionStatusValue setText: [NSString stringWithFormat:@"%@", inspectionStatusModel.istatName ]];
    } else {
        [inspectionStatusValue setText: [NSString stringWithFormat:@"%@", inspectionStatusModel.istatLang ]];
    }
    
    //set inspection image
    [imageRoomStatusView setImage:[UIImage imageWithData:inspectionStatusModel.istatImage]];
    
    /********* resize frame of image and label*************/
    CGSize size = [inspectionStatusValue.text sizeWithFont:roomStatusLabel.font];
    CGFloat x = xOfSccessory - size.width - space2ElementCell + 24;
    [inspectionStatusValue setFrame:CGRectMake(x, 10, size.width, size.height)];
    
    [imageRoomStatusView setFrame:CGRectMake(x-imageRoomStatusView.frame.size.width, imageRoomStatusView.frame.origin.y, 30, 30)];
    
    /********* end resize frame of image and label*************/
    if (isNew == TRUE) {
        [cell.contentView addSubview:inspectionStatusValue];
        [cell.contentView addSubview:imageRoomStatusView];
        
    }
    
}


/****************************** implement cell *************************************/

-(UITableViewCell*)tableView:(UITableView *)atableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // init layout for tableview
    BOOL isNew = FALSE;
    
    static NSString *CellTableIdentifier = @"Cell";
    static NSString *CellCleaningStatus = @"CleaningStatus";
    
    UITableViewCell *cell;
    if (indexPath.row == indexOfRoomStatusRow) {
        cell = [atableView dequeueReusableCellWithIdentifier:CellCleaningStatus];
    } else
        cell = [atableView dequeueReusableCellWithIdentifier:CellTableIdentifier];
    
    if(cell == nil){
        if (indexPath.row == indexOfRoomStatusRow) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellCleaningStatus] ;
        } else {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellTableIdentifier] ;
        }
        isNew = TRUE;
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell setAccessoryView:nil];
    
    /******************************/
    
    if([self isRoomInspected] && (raModel.roomAssignmentRoomStatusId != ENUM_ROOM_STATUS_OPU) ){
        switch (indexPath.row) {
            case 0:
            {
                [self raFullNameCell:cell];
            }
                break;
                
            case 1:
            {
                [self lastCleaningDateCell:cell];
            }
                break;
                
            case 2:{
                [self setupExpectedCleaningCell:cell isNew:isNew];
            }
                break;
                
            case 3:{
                [self setupActualCleaningTimeCell:cell isNew:isNew];
            }
                break;
                
            case 4:{
                [self setupAdditionalCell:cell isNew:isNew];
            }
                break;
                
            case 5:{
                [self roomStatusCell:cell isNew:isNew];
            }
                break;
                
            case 6:{
                [self setupRemarkCell:cell isNew:isNew];
            }
                break;
                
            case 7:{
                [self setupEstimatedInspectionTimeCell:cell isNew:isNew];
            }
                break;
                
            case 8:{
                [self setupActualInspectionTimeCell:cell isNew:isNew];
            }
                break;
                
            case 9:{
                [self setupInspectionStatusCell:cell isNew:isNew];
            }
                break;
        }
        
    } else {
        switch (indexPath.row) {
            case 0:
            {
                [self raFullNameCell:cell];
            }
                break;
                
            case 1:
            {
                [self lastCleaningDateCell:cell];
            }
                break;
                
            case 2:
            {
                [self setupExpectedCleaningCell:cell isNew:isNew];
            }
                break;
                
            case 3:{
                [self setupActualCleaningTimeCell:cell isNew:isNew];
            }
                break;
                
            case 4:{
                [self setupAdditionalCell:cell isNew:isNew];
            }
                break;
                
            case 5:{
                [self roomStatusCell:cell isNew:isNew];
            }
                break;
                
            case 6:{
                [self setupRemarkCell:cell isNew:isNew];
            }
                break;
                
            case 7:{
                [self setupEstimatedInspectionTimeCell:cell isNew:isNew];
            }
                break;
                
            case 8:{
                [self setupInspectionStatusCell:cell isNew:isNew];
            }
                break;
        }
    }
    
    
    
    /**********************************************************/
    [cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
    return cell;
}

-(NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    return indexPath;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 33.0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 170.0;
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return listButtonView;
}

#pragma mark - === UITableView Delegate methods ===
#pragma mark

-(void)tableView:(UITableView *)atableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [atableView cellForRowAtIndexPath:indexPath];
    [cell setSelected:NO animated:NO];
    
    //check if not start or pausing can not go action
    if (countTimeStatus == PAUSE || countTimeStatus == NOT_START) {
        return;
    }
    
    //allow input remark anyway
    if(indexPath.row == indexOfRemarkRow)  [self showRemark];
    
    if(indexPath.row == indexOfRoomStatusRow)  [self showRoomStatus];
    
    /**********************************************************/
}

#pragma mark - === Save data ===
#pragma mark

-(void)saveData{
    isSaved = YES;
    // @ show loading data .
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_RA_SAVING_DATA]];
    [self.view.superview addSubview:HUD];
    [HUD show:YES];
    
    dataModelCompare.roomAssignmentRoomCleaningStatusId = raModel.roomAssignmentRoomCleaningStatusId;
    dataModelCompare.roomAssignmentRoomStatusId = raModel.roomAssignmentRoomStatusId;
    
    dataModel.room_PostStatus = POST_STATUS_SAVED_UNPOSTED;
    [[RoomManagerV2 sharedRoomManager] updateRoomModel:dataModel];
    
    raModel.roomAssignment_PostStatus = POST_STATUS_SAVED_UNPOSTED;
    [[RoomManagerV2 sharedRoomManager] update:raModel];
    
    SyncManagerV2 *synManager = [[SyncManagerV2 alloc] init];
    [synManager postRoomInspectionWithUserID:[[UserManagerV2 sharedUserManager] currentUser].userId AndRaModel:raModel];
    
    //    [synManager postRoomAssignmentWithUserID:[[[UserManagerV2 sharedUserManager] currentUser] userId] AndRaModel:raModel];
    
    //change save message
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_MSG_SAVE_MESSAGE]];
    
    //hide saving data.
    [self performSelector:@selector(hiddenHUDAfterSaved:) withObject:HUD afterDelay:0.5];
    
}


-(BOOL) isNotSavedAndExit
{
    if([self checkIsSave]) {
        
        return NO;
        
    } else {
        
        if (countTimeStatus == PAUSE || countTimeStatus == FINISH) {
            //completed inspection
            UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObject:[UIImage imageNamed:@"yes_bt.png"]] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:msgSaveRoomDetail delegate:self cancelButtonTitle:msgbtnOk otherButtonTitles:nil, nil];
            
            alert.delegate = self;
            
            alert.tag = 10000;
            
            [alert show];
        } else {
            //show please complete the room
            UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_complete_room_to_leave] delegate:nil cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_OK] otherButtonTitles:nil];
            [alert show];
        }
        
        return YES;
        
    }
}


-(BOOL)checkIsSave{
    return isSaved;
}

#pragma mark -
#pragma mark ButtonCLG Methods
- (IBAction)butCheckListPressed:(id)sender {
    //check app is syncing
    if ([[HomeViewV2 shareHomeView] isSyncing] == YES) {
        CustomAlertViewV2 *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:nil message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_please_wait_syncing_progress] delegate:nil cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_OK] otherButtonTitles:nil];
        [alert show];
        
        //set selected button in tabbar
        [[HomeViewV2 shareHomeView] setSelectedButton:tagOfHomeButton];
        return;
    }
    
    if (countTimeStatus == PAUSE || countTimeStatus == NOT_START) {
        [self startInspection];
    }
    
    //selected checklist view
    //    [[HomeViewV2 shareHomeView] setSelectedButton:tagOfCheckListButton];
    
    if ([self checkIsSave] == YES && (raModel.roomAssignmentRoomInspectionStatusId == ENUM_INSPECTION_COMPLETED_FAIL || raModel.roomAssignmentRoomInspectionStatusId == ENUM_INSPECTION_COMPLETED_PASS)) {
        CheckListForMaidViewV2 *checklistView = [[CheckListForMaidViewV2 alloc] initWithNibName:NSStringFromClass([CheckListForMaidViewV2 class]) bundle:nil];
        
        UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK] style:UIBarButtonItemStyleBordered target:nil action:nil];
        self.superController.navigationItem.backBarButtonItem = backButton;
        
        [self.superController.navigationController pushViewController:checklistView animated:YES];
        return;
    }
    
    CheckListViewV2 *checklistView = [[CheckListViewV2 alloc] initWithNibName:NSStringFromClass([CheckListViewV2 class]) bundle:nil];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK] style:UIBarButtonItemStyleBordered target:nil action:nil];
    self.superController.navigationItem.backBarButtonItem = backButton;
    
    [self.superController.navigationController pushViewController:checklistView animated:YES];
}

- (IBAction)butActionPressed:(id)sender {
    
    //check if not start or pausing can not go action
    if (countTimeStatus == PAUSE || countTimeStatus == NOT_START) {
        return;
    }
    
    NSDictionary *dic = [[LanguageManagerV2 sharedLanguageManager] getCurrentDictionaryLanguage];
    NSString *action, *lf, *ec, *c, *g, *cancel;
    action = [dic valueForKey:[NSString stringWithFormat:@"%@", L_RA_SELECT_ACTION]];
    lf = [dic valueForKey:[NSString stringWithFormat:@"%@", L_LOST_FOUND]];
    ec = [dic valueForKey:[NSString stringWithFormat:@"%@", L_ENGINEERING_CASE]];
    c = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_Counts];
    g = [dic valueForKey:[NSString stringWithFormat:@"%@", L_Guideline]];
    cancel = [dic valueForKey:[NSString stringWithFormat:@"%@", L_CANCEL]];
    
    if (action == nil) {
        action = @"Action";
    }
    if (lf == nil) {
        lf = @"Lost & Found";
    }
    if (c == nil) {
        c= @"Posting";
    }
    
    if (g == nil) {
        g = @"Guideline";
    }
    if ([UserManagerV2 isSupervisor]) {
        //#warning modify for Conrad
        //        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:action delegate:self cancelButtonTitle:cancel destructiveButtonTitle:nil otherButtonTitles:lf, ec, nil];
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:action delegate:self cancelButtonTitle:cancel destructiveButtonTitle:nil otherButtonTitles:c, ec, g, nil];
        [actionSheet showInView:self.view.superview.superview];
        
    }
    else {
        
        if( raModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_COMPLETED){
            //            UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:action delegate:self cancelButtonTitle:cancel destructiveButtonTitle:nil otherButtonTitles: g, nil];
            UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:action delegate:self cancelButtonTitle:cancel destructiveButtonTitle:nil otherButtonTitles: c, ec, g, nil];
            [actionSheet showInView:self.view.superview.superview];
            
            
        } else{
            UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:action delegate:self cancelButtonTitle:cancel destructiveButtonTitle:nil otherButtonTitles:c, g, nil];
            [actionSheet showInView:self.view.superview.superview];
            
        }
        
    }
    
    
}


-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return TRUE;
}

#pragma mark - UIActionSheet Delegate Methods
-(void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex{
    
    
    //    if ([UserManagerV2 isSupervisor]) {
    //        //supervisor
    //        switch (buttonIndex) {
    //#warning modify for Conrad
    ////            case 0: // Lost & Found
    ////                [superController addLostAndFoundView];
    ////
    ////                //hide wifi view
    ////                [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationHideWifiView] object:nil];
    ////                break;
    ////            case 1: // Engineering
    ////                [superController addEngineeringView];
    ////                break;
    //
    //            case 0: // Engineering
    //                [superController addEngineeringView];
    //                break;
    //
    //            default:
    //                break;
    //        }
    //
    //
    //    } else {
    //        //maid
    //        if(raModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_COMPLETED){
    //            switch (buttonIndex) {
    //
    //                case 0: // Guideline
    //                    [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@",notificationGuidelineView] object:nil];
    //                    break;
    //
    //                default:
    //                    break;
    //            }
    //
    //        }
    //        else
    //        {
    //            switch (buttonIndex) {
    //
    //                case 0: // Posting
    //
    //                    [[NSNotificationCenter defaultCenter] postNotificationName:@"AddCLGWithSelectedIndex" object:@"1"];
    //
    //                    break;
    //                case 1: // Guideline
    //                    [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@",notificationGuidelineView] object:nil];
    //                    break;
    //
    //                default:
    //                    break;
    //            }
    //
    //        }
    //
    //    }
    
    switch (buttonIndex) {
            
        case 0: // Posting
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"AddCLGWithSelectedIndex" object:@"1"];
            
            break;
        case 1:
            [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationAddEngineeringView] object:nil];
            break;
        case 2: // Guideline
            [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@",notificationGuidelineView] object:nil];
            break;
            
        default:
            break;
    }
    
}

#pragma mark - set captions view
-(void)setCaptionsView {
    [btnAction setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BTN_ACTION] forState:UIControlStateNormal];
    
    self.expectedCleaningTime = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId: L_EXPECTED_CLEANING_TIME];
    self.guestPref = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId: L_GUEST_REFERENCE];
    self.additioJob = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId: L_ADDITIONAL_JOB];
    self.cleaningStatus = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId: L_CLEANING_STATUS];
    
    
    msgbtnOk = [[LanguageManagerV2 sharedLanguageManager] getOK];
    msgSaveRoomDetail = [[LanguageManagerV2 sharedLanguageManager] getMsgAlertSaveRoomDetail];
    
    [reassignBtn setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_room_reaassignRoom] forState:UIControlStateNormal];
    [checkListButton setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_strchecklist] forState:UIControlStateNormal];
}

#pragma mark
#pragma mark Timer counting

-(void)showAlert{
    
    AlertAdvancedSearch *alertView=[[AlertAdvancedSearch alloc] init];
    [alertView alertAdvancedSearchInitWithFrame:CGRectZero title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] content:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_msgalert_confirmblockroom]];
    
    alertView.delegate = self;
    [alertView show];
    
}

-(void)showAlertWithTitle:(NSString*)title content:(NSString*)content andtag:(NSInteger)tag{
    
    
    NSString *msgbtnNo = [[LanguageManagerV2 sharedLanguageManager] getNo];
    NSString *msgbtnYes = [[LanguageManagerV2 sharedLanguageManager] getYes];
    
    
    UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:content delegate:self cancelButtonTitle:msgbtnYes otherButtonTitles:msgbtnNo, nil];
    alert.delegate = self;
    alert.tag = tag;
    [alert show];
    
    
}

// start and pause timer
- (IBAction)startPauseCountTimerDidSelect:(id)sender {
    NSInteger countTimeStatusInt = countTimeStatus;
    
    if (countTimeStatusInt == PAUSE || countTimeStatusInt == NOT_START) {
        //check app is syncing
        if ([[HomeViewV2 shareHomeView] isSyncing] == YES) {
            CustomAlertViewV2 *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:nil message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_please_wait_syncing_progress] delegate:nil cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_OK] otherButtonTitles:nil];
            [alert show];
            
            //set selected button in tabbar
            [[HomeViewV2 shareHomeView] setSelectedButton:tagOfHomeButton];
            return;
        }
    }
    
    isSaved = NO;
    if(timer == nil){
        timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTimer) userInfo:nil repeats:YES];
    }
    
    
    switch (countTimeStatusInt) {
        case PAUSE:{
            // transition from pause to start.
            
            //reset time left inspection time by CR 5.0
            timeLeftInspectionStatus = raModel.roomAssignmentRoomExpectedInspectionTime * 60;
            [self displayTimerButton];
            
            countTimeStatus = START;
            raModel.roomAssignmentRoomInspectionStatusId = ENUM_INSPECTION_STARTED;
            
            [finishTimeButton setUserInteractionEnabled:YES];
            [finishTimeButton setBackgroundImage:[UIImage imageNamed:IMAGE_COMPLETE_ON] forState:UIControlStateNormal];
            [countTimeButton setBackgroundImage:[UIImage imageNamed:IMAGE_STOP] forState:UIControlStateNormal];
            [countTimeButton setBackgroundImage:[UIImage imageNamed:IMAGE_STOP_ON] forState:UIControlStateHighlighted];
            [countTimeButton setBackgroundImage:[UIImage imageNamed:IMAGE_STOP_GRAY] forState:UIControlStateDisabled];
            
            [timerButon setBackgroundImage:[UIImage imageNamed:IMAGE_TIMER_PLAY] forState:UIControlStateNormal];
            
            CGRect f = timerButon.frame;
            [btnAction setHidden:NO];
            [timerButon setFrame:CGRectMake(f.origin.x, btnAction.frame.origin.y + btnAction.frame.size.height + 4, f.size.width, f.size.height)];
            
            //check can update start
            STATUS_UPDATE_RECORD_DETAIL statusUpdateStart = [self canUpdateStartRecord];
            if (statusUpdateStart == CAN_START_RECORD_DETAIL) {
                [self updateStartRecord];
            } else {
                if (statusUpdateStart == CONTINUE_COUNT_INSPECTION) {
                    NSDateFormatter *f = [[NSDateFormatter alloc] init];
                    [f setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                    NSDate *startDate = [f dateFromString:roomRecordDetail.recd_Start_Date];
                    NSDate *stopDate = [f dateFromString:roomRecordDetail.recd_Stop_Date];
                    //reset stop date
                    roomRecordDetail.recd_Stop_Date = @"";
                    NSTimeInterval timeInterval = [stopDate timeIntervalSinceDate:startDate];
                    timeLeftInspectionStatus = raModel.roomAssignmentRoomExpectedInspectionTime * 60 - (NSInteger) timeInterval;
                    durationTime = (NSInteger) timeInterval;
                    
                    //update start date by add pause time
                    NSDate *now = [NSDate date];
                    startDate = [startDate dateByAddingTimeInterval:[now timeIntervalSinceDate:stopDate]];
                    roomRecordDetail.recd_Start_Date = [f stringFromDate:startDate];
                    
                    [self displayTimerButton];
                }
            }
            
            [tableView reloadData];
            
        }
            break;
            
        case START:{
            // transition from start to pause.
            
            //check can update stop record
            [self showRemark];
            if ([self canUpdateStopRecord] == CAN_START_RECORD_DETAIL) {
                
            } else {
                return;
            }
            
            countTimeStatus = PAUSE;
            [CommonVariable sharedCommonVariable].roomIsCompleted = 2;
            
            raModel.roomAssignmentRoomInspectionStatusId = ENUM_INSPECTION_PAUSE;
            
            [countTimeButton setBackgroundImage:[UIImage imageNamed:IMAGE_PLAY] forState:UIControlStateNormal];
            [countTimeButton setBackgroundImage:[UIImage imageNamed:IMAGE_PLAY_ON] forState:UIControlStateHighlighted];
            
            [timerButon setBackgroundImage:[UIImage imageNamed:IMAGE_TIMER_PAUSE] forState:UIControlStateNormal];
            
            //update stop record
            [self updateStopRecord];
            
            
            [self refreshLayoutButtons];
            
            [tableView reloadData];
            
        } break;
            
        case NOT_START:{
            // transition from not start to start.
            
            roomRecord.rrec_PostStatus = POST_STATUS_SAVED_UNPOSTED;
            
            countTimeStatus = START;
            
            [CommonVariable sharedCommonVariable].roomIsCompleted = 0;
            //only update start time if roomrecord does not contain start time
            if ([roomRecord.rrec_Inspection_Start_Time isEqualToString:@"1900-01-01 00:00:00"]) {
                NSDate *now = [[NSDate alloc] init];
                roomRecord.rrec_Inspection_Start_Time = [timeFomarter stringFromDate:now];  //... start time
            }
            
            raModel.roomAssignmentRoomInspectionStatusId = ENUM_INSPECTION_STARTED;
            
            if(raModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VC){
                raModel.roomAssignmentRoomStatusId = ENUM_ROOM_STATUS_VPU;
            } else if(raModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OC){
                raModel.roomAssignmentRoomStatusId = ENUM_ROOM_STATUS_OPU;
            } else if(raModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OPU){
                raModel.roomAssignmentRoomStatusId = ENUM_ROOM_STATUS_OPU;
            }else if(raModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VPU){
                raModel.roomAssignmentRoomStatusId = ENUM_ROOM_STATUS_VPU;
            }
            
            
            [finishTimeButton setUserInteractionEnabled:YES];
            [finishTimeButton setBackgroundImage:[UIImage imageNamed:IMAGE_COMPLETE_ON] forState:UIControlStateNormal];
            [countTimeButton setBackgroundImage:[UIImage imageNamed:IMAGE_STOP] forState:UIControlStateNormal];
            [countTimeButton setBackgroundImage:[UIImage imageNamed:IMAGE_STOP_ON] forState:UIControlStateHighlighted];
            [countTimeButton setBackgroundImage:[UIImage imageNamed:IMAGE_STOP_GRAY] forState:UIControlStateDisabled];
            [timerButon setBackgroundImage:[UIImage imageNamed:IMAGE_TIMER_PLAY] forState:UIControlStateNormal];
            [btnAction setHidden:NO];
            CGRect f = timerButon.frame;
            [timerButon setFrame:CGRectMake(f.origin.x, btnAction.frame.origin.y + btnAction.frame.size.height +5 ,
                                            f.size.width, f.size.height)];
            // show check list :
            //disable show checklist when start room
            //            [self butCheckListPressed:nil];
            
            //update room record detail start time
            if ([self canUpdateStartRecord] == CAN_START_RECORD_DETAIL) {
                [self updateStartRecord];
            }
            
            
            [tableView reloadData];
        } break;
            
        case FINISH:{
            
        } break;
            
    }
    
}

// display time with expected time
- (NSString *)countTimeSpendForTask:(NSInteger)expectedTimeForTask  {
    NSString *secondsString;
    NSString *minutesString;
    NSString *hoursString;
    NSInteger secondsInt;
    NSInteger minutesInt;
    NSInteger hoursInt;
    NSString *timeDisplay;
    if (expectedTimeForTask < 0){
        hoursInt = - expectedTimeForTask/3600;
        minutesInt = - ((expectedTimeForTask + (hoursInt*3600)) / 60);
        secondsInt = - (expectedTimeForTask % 60);
        
        hoursString = [NSString stringWithFormat:@"%.2d", hoursInt];
        minutesString = [NSString stringWithFormat:@"%.2d", minutesInt];
        secondsString = [NSString stringWithFormat:@"%.2d", secondsInt];
        
        
        timeDisplay = [NSString stringWithFormat:@"- %@:%@:%@", hoursString, minutesString, secondsString];
        [timerButon setBackgroundImage:[UIImage imageNamed:@"timer_red_274x58.png"] forState:UIControlStateNormal];
    } else {
        
        hoursInt = expectedTimeForTask/3600;
        minutesInt = (expectedTimeForTask - (hoursInt*3600)) / 60;
        secondsInt = expectedTimeForTask % 60;
        
        hoursString = [NSString stringWithFormat:@"%.2d",hoursInt];
        minutesString = [NSString stringWithFormat:@"%.2d",minutesInt];
        secondsString = [NSString stringWithFormat:@"%.2d",secondsInt];
        
        timeDisplay = [NSString stringWithFormat:@"%@:%@:%@", hoursString, minutesString, secondsString];
    }
    return timeDisplay;
}

- (void)displayTimerButton {
    
    [timerButon setTitle:[self countTimeSpendForTask: timeLeftInspectionStatus] forState:UIControlStateNormal];
    
    if (countTimeStatus == PAUSE) {
        [timerButon setBackgroundImage:[UIImage imageNamed:IMAGE_TIMER_PAUSE] forState:UIControlStateNormal];
    }
    
}

-(void)updateTimer{
    if(countTimeStatus == PAUSE || countTimeStatus == FINISH) return;
    
    timeLeftInspectionStatus -= 1;
    durationTime += 1;
    
    //handle update correct time left because timer is delay
    [self updateCorrectTimeLeftAndDuration];
    
    [self performSelectorOnMainThread:@selector(displayTimerButton) withObject:nil waitUntilDone:NO];
}

//update correct time left and duration base on room record detail
-(void)updateCorrectTimeLeftAndDuration {
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    [f setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *startDate = [f dateFromString:roomRecordDetail.recd_Start_Date];
    NSDate *stopDate = nil;
    if (roomRecordDetail.recd_Stop_Date == nil || [roomRecordDetail.recd_Stop_Date isEqualToString:@""]) {
        stopDate = [NSDate date];
    } else {
        stopDate = [f dateFromString:roomRecordDetail.recd_Stop_Date];
    }
    
    NSInteger timeDifference = [stopDate timeIntervalSinceDate:startDate];
    
    timeLeftInspectionStatus = raModel.roomAssignmentRoomExpectedInspectionTime * 60 - timeDifference;
    durationTime = timeDifference;
}

- (void)completeCleaningRoom {
    [self confirmCompleteCleaningStatus];
}

// this function used when complete task in room: cleaning , inspection
- (IBAction)finishCountTimeDidSelect:(id)sender {
    [self confirmCompleteCleaningStatus];
}

// confirm block room
- (IBAction)blockRoomDidSelect:(id)sender {
    [self showAlertWithTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] content:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_msgalert_confirmblockroom] andtag:tagAlertBlockRoom];
}

// confirm reassign room
- (IBAction)reassignDidSelect:(id)sender {
    //update ramodel
    raModel.roomAssignment_PostStatus = POST_STATUS_SAVED_UNPOSTED;
    [[RoomManagerV2 sharedRoomManager] update:raModel];
    
    self.dataModel.room_is_re_cleaning = YES;
    //    raModel.roomAssignmentRoomCleaningStatusId = ENUM_CLEANING_STATUS_PENDING_SECOND_TIME;
    [reassignBtn setBackgroundImage:[UIImage imageNamed:@"bt_gray_299x58"] forState:UIControlStateNormal];
    [reassignBtn setUserInteractionEnabled:NO];
    
    ReAssignRoomViewControllerV2 *reassignView = [[ReAssignRoomViewControllerV2 alloc] initWithNibName:@"ReAssignRoomViewControllerV2" bundle:nil roomCleaningStatus:ROOM_IS_CLEANING roomModel:dataModel];
    [superController.navigationController pushViewController:reassignView animated:YES];
}

#pragma mark
#pragma mark Check status if this room detail

//..... is rooom inspected
-(BOOL)isRoomInspected{
    if(raModel.roomAssignmentRoomInspectionStatusId == ENUM_INSPECTION_COMPLETED_PASS||
       raModel.roomAssignmentRoomInspectionStatusId == ENUM_INSPECTION_COMPLETED_FAIL){
        return YES;
    } else{
        return  NO;
    }
}

#pragma mark - === Room Status ===
#pragma mark
-(void)roomStatusDidSelect:(NSInteger)roomStatusID{
    isSaved = NO;
    roomStatusIDSelected = roomStatusID;
    [self showAlertWithTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert]
                     content:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_room_confirmchangeroomstatus] andtag:tagAlertRoomStatusChange];
    
}

-(void)showRoomStatus{
    if(raModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OOO || raModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OOS) return;
    [delegate showPopupRoomStatus:self];
}

// room status is change
-(void)roomStatusDidChange{
    isSaved = NO;
    
    raModel.roomAssignmentRoomStatusId = roomStatusIDSelected;
    
    //complete the room when room status is OOO | OOS
    if (raModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OOO || raModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OOS) {
        [self completeCleaningRoom];
    } else {
        if( [CommonVariable sharedCommonVariable].roomIsCompleted ==1){
            [self confirmCompleteCleaningStatus];
        }
    }
    
    
    
    [tableView reloadData];
}

#pragma mark
#pragma mark  Alert delegate

-(void) roomBlockDidChange{
    isSaved = NO;
    raModel.roomAssignmentRoomStatusId = ENUM_ROOM_STATUS_OOO;
    [lockButton setUserInteractionEnabled:NO];
    [lockButton setImage:[UIImage imageNamed:@"block_bt_gray_299x58.png"] forState:UIControlStateNormal];
    
    [self refreshLayoutButtons];
    
    [tableView reloadData];
}

-(void) alertAdvancedOKButtonPressed:(AlertAdvancedSearch *)alertView{
    isSaved = NO;
    
    if(alertView.tag == tagAlertRoomStatusChange){
        [self roomStatusDidChange];
    }
    
    if(alertView.tag == tagAlertBlockRoom){
        [self roomBlockDidChange];
    }
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    switch (alertView.tag) {
        case tagAlertCancelCleaningLessThan1Minute:
        {
            switch (buttonIndex) {
                case 0:
                {
                    //YES
                    //edit by workflow stop.20120620.1.pdf
                    [superController.navigationController popViewControllerAnimated:YES];
                    
                    //                    //reset time left cleaning
                    //                    timeLeftInspectionStatus = raModel.roomAssignmentRoomExpectedInspectionTime * 60;
                    //                    //reset duration cleaning
                    //                    durationTime = 0;
                    //
                    //                    countTimeStatus = PAUSE;
                    //
                    //                    //refresh timer button
                    //                    [self displayTimerButton];
                    //
                    //                    //change inspection status to PAUSE
                    //                    raModel.roomAssignmentRoomInspectionStatusId = ENUM_INSPECTION_PAUSE;
                    //
                    //                    [countTimeButton setBackgroundImage:[UIImage imageNamed:IMAGE_PLAY] forState:UIControlStateNormal];
                    //                    [countTimeButton setBackgroundImage:[UIImage imageNamed:IMAGE_PLAY_ON] forState:UIControlStateHighlighted];
                    //
                    //
                    //                    roomRecordDetail.recd_Start_Date = nil;
                    //
                    //                    [tableView reloadData];
                }
                    break;
                    
                case 1:
                {
                    //NO
                    //continue countdown time cleaning
                }
                    break;
                    
                default:
                    break;
            }
            
            return;
        }
            break;
            
        default:
            break;
    }
    
    if(buttonIndex == 1) return;
    isSaved = NO;
    
    if(alertView.tag == tagAlertRoomStatusChange){
        
        [self roomStatusDidChange];
    }
    
    if(alertView.tag == tagAlertBlockRoom){
        [self roomBlockDidChange];
    }
    
    if(alertView.tag == kTagAlertDoneWithRoomCleaning){
        [self confirmCompleteCleaningStatus];
    }
    
}

-(void) alertAdvancedCancelButtonPressed:(AlertAdvancedSearch *)alertView{
}

#pragma mark
#pragma mark Count popup reminder
- (void)confirmCompleteCleaningStatus {
    isSaved = NO;
    [CommonVariable sharedCommonVariable].roomIsCompleted = 1;
    
    //change status of checklist
    CheckListManagerV2 *chkManager = [[CheckListManagerV2 alloc] init];
    CheckListModelV2 *chkModel = [[CheckListModelV2 alloc] init];
    chkModel.chkListId = [TasksManagerV2 getCurrentRoomAssignment];
    chkModel.chkListUserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    [chkManager loadCheckListData:chkModel];
    
    if(chkModel.chkListStatus) {
        self.isCompleted = NO;
        
        //#warning fix later: remove below line
        goto INSPECTION_ROOM_WITHOUT_CHECKLIST;
        //#warning end
        
        /********* set status of this screen************/
        
        if(chkModel.chkListStatus == tChkFail) {
            //check if have checklist but user change room status again
            
            //change to pass or block room + pass
            if (raModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OI || raModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VI || raModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OOO || raModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OOS) {
                
                goto INSPECTION_ROOM_WITHOUT_CHECKLIST; //for clear checklist and inspection the room without checklist
            }
            
            //change to fail
            if (raModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OD || raModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VD) {
                
                goto INSPECTION_ROOM_WITHOUT_CHECKLIST; //for clear checklist and inspection the room without checklist
            }
            
            
            raModel.roomAssignmentRoomInspectionStatusId = ENUM_INSPECTION_COMPLETED_FAIL;
        } else{
            raModel.roomAssignmentRoomInspectionStatusId = ENUM_INSPECTION_COMPLETED_PASS;
        }
        
        
        // checklist pass
        if(raModel.roomAssignmentRoomInspectionStatusId == ENUM_INSPECTION_COMPLETED_PASS){
            [countTimeButton setHidden:YES];
            [finishTimeButton setHidden:YES];
            [btnAction setHidden:YES];
            [timerButon setHidden:YES];
            [reassignBtn setHidden:YES];
            
            if(raModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VPU){
                [lockButton setHidden:NO];
                raModel.roomAssignmentRoomStatusId = ENUM_ROOM_STATUS_VI;
                superController.roomStatusView.isBlock = YES;
            }
            
            if(raModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OPU ){
                [lockButton setHidden:YES];
                raModel.roomAssignmentRoomStatusId = ENUM_ROOM_STATUS_OI;
            }
            
            [checkListButton setHidden:NO];
        }
        // check list fail
        if(raModel.roomAssignmentRoomInspectionStatusId == ENUM_INSPECTION_COMPLETED_FAIL){
            
            if(raModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VPU){
                [lockButton setHidden:YES];
                raModel.roomAssignmentRoomStatusId = ENUM_ROOM_STATUS_VD;
            } else{
                raModel.roomAssignmentRoomStatusId = ENUM_ROOM_STATUS_OD;
            }
            
            [countTimeButton setHidden:YES];
            [finishTimeButton setHidden:YES];
            [btnAction setHidden:YES];
            [timerButon setHidden:YES];
            [lockButton setHidden:YES];
            
            [reassignBtn setHidden:NO];
            [checkListButton setHidden:NO];
        }
        
        //update total pause time
        NSDate *now = [[NSDate alloc] init];
        roomRecord.rrec_Inspection_End_Time = [timeFomarter stringFromDate:now];  //... end time
        //Son Nguyen update change when finished button press update total duration inspection
        [self updateStopRecord];
        //[[RoomManagerV2 sharedRoomManager] updateRoomRecordData:roomRecord];
        
        //enable finish
        countTimeStatus = FINISH;
        
        //update stop record detail
        if (countTimeStatus == START) {
            [self updateStopRecord];
        }
        
        //[[RoomManagerV2 sharedRoomManager] updateRoomRecordData:roomRecord];
        
        // reload layout  of tableview
        [tableView reloadData];
        
        
    } else {
        //inspection room without checklist
    INSPECTION_ROOM_WITHOUT_CHECKLIST:
        {
            //            //check can stop and auto complete the room
            //            if ([self canUpdateStopRecord] == CAN_NOT_START_RECORD_DETAIL) {
            //                return;
            //            }
            
            //check input room status
            if (raModel.roomAssignmentRoomStatusId != ENUM_ROOM_STATUS_OI && raModel.roomAssignmentRoomStatusId != ENUM_ROOM_STATUS_VI && raModel.roomAssignmentRoomStatusId != ENUM_ROOM_STATUS_OD && raModel.roomAssignmentRoomStatusId != ENUM_ROOM_STATUS_VD && raModel.roomAssignmentRoomStatusId != ENUM_ROOM_STATUS_OOO && raModel.roomAssignmentRoomStatusId != ENUM_ROOM_STATUS_OOS) {
                
                //check input checklist
                CheckListManagerV2 *chkManager = [[CheckListManagerV2 alloc] init];
                if ([chkManager checkChecklistStatusWithRoomAssigmentId:raModel.roomAssignment_Id] == NO) {
                    //show alert and return
                    CustomAlertViewV2 *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:nil message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_room_detail_input_checklist_room_status] delegate:nil cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_OK] otherButtonTitles:nil];
                    [alert show];
                    
                    return;
                }
            }
            
            //update room record end time
            NSDate *now = [[NSDate alloc] init];
            roomRecord.rrec_Inspection_End_Time = [timeFomarter stringFromDate:now];  //... end time
            //[[RoomManagerV2 sharedRoomManager] updateRoomRecordData:roomRecord];
            [self updateStopRecord];
            //enable finish
            countTimeStatus = FINISH;
            
            //update stop record detail
            if (countTimeStatus == START) {
                [self updateStopRecord];
            }
            
            //implement complete the room without checklist for CR 5.0
            if (raModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OD || raModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VD) {
                //inspection fail without checklist
                //clear checklist by CR 5.0
                CheckListManagerV2 *chkManager = [[CheckListManagerV2 alloc] init];
                [chkManager clearDataChecklistWithRoomAssignmentId:raModel.roomAssignment_Id andInspectionStatus:ENUM_INSPECTION_COMPLETED_FAIL];
                
                //manual set inspection status to fail
                raModel.roomAssignmentRoomInspectionStatusId = ENUM_INSPECTION_COMPLETED_FAIL;
            }
            
            if (raModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OI || raModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VI || raModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OOO || raModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OOS) {
                //inspection pass without checklist
                //clear checklist by CR 5.0
                CheckListManagerV2 *chkManager = [[CheckListManagerV2 alloc] init];
                [chkManager clearDataChecklistWithRoomAssignmentId:raModel.roomAssignment_Id andInspectionStatus:ENUM_INSPECTION_COMPLETED_PASS];
                
                //manual set inspection status to fail
                raModel.roomAssignmentRoomInspectionStatusId = ENUM_INSPECTION_COMPLETED_PASS;
            }
            
            //refresh action buttons
            // checklist pass
            if(raModel.roomAssignmentRoomInspectionStatusId == ENUM_INSPECTION_COMPLETED_PASS){
                [countTimeButton setHidden:YES];
                [finishTimeButton setHidden:YES];
                [btnAction setHidden:YES];
                [timerButon setHidden:YES];
                [reassignBtn setHidden:YES];
                
                [checkListButton setHidden:NO];
            }
            // check list fail
            if(raModel.roomAssignmentRoomInspectionStatusId == ENUM_INSPECTION_COMPLETED_FAIL){
                
                [countTimeButton setHidden:YES];
                [finishTimeButton setHidden:YES];
                [btnAction setHidden:YES];
                [timerButon setHidden:YES];
                [lockButton setHidden:YES];
                
                [reassignBtn setHidden:NO];
                [checkListButton setHidden:NO];
            }
            
            [tableView reloadData];
            
            
        }
    }
}

-(void)yesCountReminder{
    isSaved = NO;
    [countReminder hide];
    [self confirmCompleteCleaningStatus];
}

-(void)noCountReminder{
    [countReminder hide];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"AddCLGWithSelectedIndex" object:@"1"];
}

#pragma mark - === Remark ===
#pragma mark
-(void)showRemark{
    RemarkViewV2 *remarkView = [[RemarkViewV2 alloc] initWithNibName:
                                NSStringFromClass([RemarkViewV2 class]) bundle:nil];
    [remarkView setDelegate:self];
    [superController.navigationController pushViewController:remarkView animated:YES];
    
    [remarkView.txvRemark setText:roomRecord.rrec_Remark != nil ? roomRecord.rrec_Remark : @""];
}

-(void)remarkViewV2DoneWithText:(NSString *)text andController:(UIViewController *)controllerView{
    isSaved = NO;
    
    [controllerView.navigationController popViewControllerAnimated:YES];
    
    //update remark
    roomRecord.rrec_Remark = text;
    
    //update view
    NSIndexPath *indexpath = nil;
    indexpath = [NSIndexPath indexPathForRow:indexOfRemarkRow inSection:0];
    [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexpath, nil] withRowAnimation:UITableViewRowAnimationFade];
}

#pragma mark - === Start Inspection ===
#pragma mark
-(void)startInspection {
    if (countTimeStatus == NOT_START || countTimeStatus == PAUSE) {
        [self startPauseCountTimerDidSelect:nil];
    }
}

#pragma mark - === Set Room Assignment data ===
#pragma mark
-(void)setRoomAssignmentModel:(RoomAssignmentModelV2 *)model {
    self.raModel = model;
}

#pragma mark - === Current count time status inspection ===
#pragma mark
-(enum ENUM_ROOMDETAIL_STATUS)currentCountTimeStatusInspection {
    return countTimeStatus;
}

#pragma mark - === Handle Record Detail ===
#pragma mark
-(STATUS_UPDATE_RECORD_DETAIL)canUpdateStartRecord {
    
    //Hao Tran - Remove for uneccessary changed
    /*
     //init room record detail if needed
     if (roomRecordDetail == nil) {
     roomRecordDetail = [[RoomRecordDetailModelV2 alloc] init];
     roomRecordDetail.recd_Ra_Id = raModel.roomAssignment_Id;
     roomRecordDetail.recd_User_Id = raModel.roomAssignment_UserId;
     return CAN_START_RECORD_DETAIL;
     }
     
     //current number of record details in current room assignment
     NSMutableArray *recordDetails = [[RoomManagerV2 sharedRoomManager] loadAllRoomRecordDetailsByRoomAssignmentId:raModel.roomAssignment_Id andUserId:raModel.roomAssignment_UserId];
     NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
     formatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
     
     if (recordDetails.count > MAX_ROOM_RECORD_DETAIL) {
     return CAN_NOT_START_RECORD_DETAIL;
     }
     
     //check last record did stopped over 1 minute?
     if (recordDetails.count > 0) {
     RoomRecordDetailModelV2 *lastRecordDetail = [recordDetails objectAtIndex:recordDetails.count - 1];
     
     NSDate *startDate = [formatter dateFromString:lastRecordDetail.recd_Start_Date];
     NSDate *stopDate = [formatter dateFromString:lastRecordDetail.recd_Stop_Date];
     NSDate *now = [NSDate date];
     NSTimeInterval timeInterval = [now timeIntervalSinceDate:stopDate];
     if (timeInterval > 60) {
     //over 1 minute
     
     return CAN_START_RECORD_DETAIL;
     } else {
     //less than 1 minute
     
     //delete last record
     [[RoomManagerV2 sharedRoomManager] deleteRoomRecordDetailData:lastRecordDetail];
     
     //subtract duration in room record
     NSInteger duration = [roomRecord.rrec_Inspection_Duration integerValue];
     duration -= (NSInteger)[stopDate timeIntervalSinceDate:startDate];
     roomRecord.rrec_Inspection_Duration = [NSString stringWithFormat:@"%d", duration];
     [[RoomManagerV2 sharedRoomManager] updateRoomRecordData:roomRecord];
     
     return CONTINUE_COUNT_INSPECTION;
     }
     }
     
     
     
     
     if (roomRecordDetail.recd_Stop_Date != nil) {
     
     NSDate *stopDate = [formatter dateFromString:roomRecordDetail.recd_Stop_Date];
     if ([stopDate compare:[NSDate dateWithTimeIntervalSinceNow: - INTERVAL_START_STOP_RECORD_DETAIL]] == NSOrderedDescending) {
     return CAN_START_RECORD_DETAIL;
     }
     }
     
     if (recordDetails.count == 0) {
     return CAN_START_RECORD_DETAIL;
     }
     */
    return CAN_NOT_START_RECORD_DETAIL;
}

-(void)updateStartRecord {
    // update start record detail, clear current stop record detail
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    roomRecordDetail.recd_Id = 0;
    roomRecordDetail.recd_Start_Date = [formatter stringFromDate:[NSDate date]];
    roomRecordDetail.recd_Stop_Date = @"";
}

-(STATUS_UPDATE_RECORD_DETAIL)canUpdateStopRecord {
    //check can update stop record
    
    //current number of record details in current room assignment
    NSMutableArray *recordDetails = [[RoomManagerV2 sharedRoomManager] loadAllRoomRecordDetailsByRoomAssignmentId:raModel.roomAssignment_Id andUserId:raModel.roomAssignment_UserId];
    
    //    if (durationTime < 60) {
    //        //show alert and return
    //        CustomAlertViewV2 *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_cancel_inspection] message:nil delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_YES] otherButtonTitles:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_NO], nil];
    //        alert.tag = tagAlertCancelCleaningLessThan1Minute;
    //        [alert show];
    //
    //        return CAN_NOT_START_RECORD_DETAIL;
    //    }
    
    if (recordDetails.count == MAX_ROOM_RECORD_DETAIL - 1) {
        //check input room status
        if (raModel.roomAssignmentRoomStatusId != ENUM_ROOM_STATUS_OI && raModel.roomAssignmentRoomStatusId != ENUM_ROOM_STATUS_VI && raModel.roomAssignmentRoomStatusId != ENUM_ROOM_STATUS_OD && raModel.roomAssignmentRoomStatusId != ENUM_ROOM_STATUS_VD && raModel.roomAssignmentRoomStatusId != ENUM_ROOM_STATUS_OOO && raModel.roomAssignmentRoomStatusId != ENUM_ROOM_STATUS_OOS) {
            
            //check input checklist
            CheckListManagerV2 *chkManager = [[CheckListManagerV2 alloc] init];
            if ([chkManager checkChecklistStatusWithRoomAssigmentId:raModel.roomAssignment_Id] == NO) {
                //show alert and return
                CustomAlertViewV2 *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:nil message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_room_detail_input_checklist_room_status] delegate:nil cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_OK] otherButtonTitles:nil];
                [alert show];
                
                return CAN_NOT_START_RECORD_DETAIL;
            }
        }
    }
    
    return CAN_START_RECORD_DETAIL;
}

-(void)updateStopRecord {
    //disable finish time button
    [finishTimeButton setUserInteractionEnabled:NO];
    [finishTimeButton setBackgroundImage:[UIImage imageNamed:IMAGE_COMPLETE_GRAY] forState:UIControlStateNormal];
    
    //set stop record detail
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    roomRecordDetail.recd_Stop_Date = [formatter stringFromDate:[NSDate date]];
    
    //update record detail to db
    if (roomRecordDetail.recd_Id != 0) {
        [[RoomManagerV2 sharedRoomManager] updateRoomRecordDetailData:roomRecordDetail];
    } else {
        [[RoomManagerV2 sharedRoomManager] insertRoomRecordDetailData:roomRecordDetail];
    }
    
    
    //update record duration inspection
    [self updateRecordDurationWithRecordDetail:roomRecordDetail];
    
    NSMutableArray *recordDetails = [[RoomManagerV2 sharedRoomManager] loadAllRoomRecordDetailsByRoomAssignmentId:raModel.roomAssignment_Id andUserId:raModel.roomAssignment_UserId];
    if (recordDetails.count >= MAX_ROOM_RECORD_DETAIL && countTimeStatus != FINISH) {
        //must auto complete the room because maid started, stoped 5 times
        [self confirmCompleteCleaningStatus];
    }
    
}

-(void)updateRecordDurationWithRecordDetail:(RoomRecordDetailModelV2 *)recordDetail {
    //    //load current record
    //    RoomRecordModelV2 *record = [[RoomRecordModelV2 alloc] init];
    //    record.rrec_room_assignment_id = recordDetail.recd_Ra_Id;
    //    record.rrec_User_Id = recordDetail.recd_User_Id;
    //
    //    [[RoomManagerV2 sharedRoomManager] loadRoomRecordDataByRoomIdAndUserId:record];
    //    if (record.rrec_Id == 0) {
    //        //is not exsting record ==> insert record
    //
    //    } else {
    //        //update record
    //
    //    }
    
    //update record
    
    //Hao Tran - Remove for unneccessary changed
    /*
     NSDateFormatter *f = [[NSDateFormatter alloc] init];
     [f setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
     NSDate *startDate = [f dateFromString:recordDetail.recd_Start_Date];
     NSDate *stopDate = [f dateFromString:recordDetail.recd_Stop_Date];
     NSTimeInterval timeDifference = [stopDate timeIntervalSinceDate:startDate];
     NSInteger duration = timeDifference + [roomRecord.rrec_Inspection_Duration integerValue];
     roomRecord.rrec_Inspection_Duration = [NSString stringWithFormat:@"%d", duration];
     roomRecord.rrec_PostStatus = POST_STATUS_SAVED_UNPOSTED;
     
     //calculate total pause inspection time
     startDate = [f dateFromString:roomRecord.rrec_Inspection_Start_Time];
     if ([roomRecord.rrec_Inspection_End_Time isEqualToString:@"1900-01-01 00:00:00"]) {
     stopDate = [NSDate date];
     } else {
     stopDate = [f dateFromString:roomRecord.rrec_Inspection_End_Time];
     }
     
     timeDifference = [stopDate timeIntervalSinceDate:startDate];
     NSInteger totalPause = timeDifference - duration;
     roomRecord.rrec_Inspected_Total_Pause_Time = [NSString stringWithFormat:@"%d", totalPause];
     
     [[RoomManagerV2 sharedRoomManager] updateRoomRecordData:roomRecord];
     */
    
}

@end
