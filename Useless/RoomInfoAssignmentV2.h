//
//  RoomInfo.h
//  EHouseKeeping
//
//  Created by tms on 5/12/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RoomDetailAssignmentViewV2.h"
#import "GuestInfoViewV2.h"
#import "ehkDefines.h"
#import "CountViewV2.h"
#import "DateTimePickerView.h"
#import "TopbarViewV2.h"
#import "AdhocMessageMain.h"
@interface RoomInfoAssignmentV2 : UIViewController<cleaningStatusViewDelegate,RoomDetailAssignmentViewV2Delegate> {
    
    RoomDetailAssignmentViewV2 *aRoomDetail;
    GuestInfoViewV2 *aGuestInfo;
    
    IBOutlet UINavigationBar *navigationBar;
    
    IBOutlet UINavigationItem *anavigationItem;
   
    IBOutlet UIButton *btnRoomDetail;
    IBOutlet UIButton *btnGuestInfo;
    IBOutlet UIScrollView *scrollView;
    IBOutlet UILabel *lblLocation;
    IBOutlet UIImageView *imgViewHotelLogo;
    
    NSString *roomName;
    
    //Data for RoomDetailView
    RoomModelV2 *roomModel;
    
    RoomAssignmentModelV2 *raDataModel;
    
    //Data for GuestInfoView
    GuestInfoModelV2 *guestInfoModel;
    
    BOOL isRoomDetailsActive;
    DateTimePickerView *aDateTimePickerView;
    CleaningStatusViewV2 *cleaningStatusView;
    CleaningStatusViewV2 *roomStatusView;
    
    IBOutlet UILabel *houseKeeperNamelabel;
    TopbarViewV2 * topBarView;
    
    BOOL isPass;
    BOOL isInspected;
    BOOL isCompleted;
    
    BOOL userCanInteraction;
    BOOL isFindByRoomView;
}

@property (nonatomic, readwrite) BOOL userCanInteraction;
@property (nonatomic, readwrite) BOOL isFindByRoomView;
@property (nonatomic, strong) UILabel *lblLocation;
//@property (nonatomic, retain) CLGView *clgView;
@property (nonatomic, strong) RoomDetailAssignmentViewV2 *aRoomDetail;
@property (nonatomic, strong) GuestInfoViewV2 *aGuestInfo;
@property (nonatomic, strong) DateTimePickerView *aDateTimePickerView;
@property (nonatomic, strong) IBOutlet UINavigationBar *navigationBar;
@property (nonatomic, strong) IBOutlet UISegmentedControl *segmentedSwitch;
@property (nonatomic, strong) IBOutlet UINavigationItem *anavigationItem;
@property (nonatomic, strong) IBOutlet UIScrollView *scrollView;
@property (nonatomic, strong) IBOutlet UIButton *btnRoomDetail;
@property (nonatomic, strong) IBOutlet UIButton *btnGuestInfo;
@property (nonatomic, strong) TopbarViewV2 *topBarView;

@property (nonatomic, strong) NSString *roomName;
@property (nonatomic, strong) RoomModelV2 *roomModel;
@property (nonatomic, strong) RoomAssignmentModelV2 *raDataModel;
@property (nonatomic, strong) GuestInfoModelV2 *guestInfoModel;
@property (nonatomic) BOOL isRoomDetailsActive;

@property (nonatomic, strong) CleaningStatusViewV2 *cleaningStatusView;
@property (nonatomic, strong) CleaningStatusViewV2 *roomStatusView;
@property (nonatomic) BOOL isPass;
@property (nonatomic) BOOL isInspected;
@property (nonatomic) BOOL isCompleted;

- (IBAction)openRoomDetail:(id)sender;
-(void) loadingData;
-(void) setCaptionsView;
//-(NSString*)roomStatusCode;
-(void) disableUserInteractionInView;
@end
