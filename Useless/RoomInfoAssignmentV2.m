//
//  RoomInfo.m
//  EHouseKeeping
//
//  Created by tms on 5/12/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "RoomInfoAssignmentV2.h"

#import "ehkDefines.h"
#import "RoomManagerV2.h"

#import "NetworkCheck.h"

#import "MBProgressHUD.h"
#import "EngineeringCaseViewV2.h"

#import "LostFoundViewV2.h"
#import "GuidelineViewV2.h"
#import "SyncManagerV2.h"
#import "EngineerCaseViewManagerV2.h"
#import "FindByRoomViewV2.h"

#define highlightImage      @"tab_count_active.png"
#define normalImage         @"tab_count_NOactive.png"
#define background          @"bg.png"
#define sizeTitle           22

@interface RoomInfoAssignmentV2 (PrivateMethods)

#define tagTopbarView 1234

-(void) loadTopbarView;
-(void) showTopbarView;
-(void) hideTopbarView;
-(void) setHiddenTopbarView;
-(void) adjustShowForViews;
-(void) adjustRemoveForViews;
-(void) addButtonHandleShowHideTopbar;
-(void) topbarTapped:(UIButton *) sender;
-(TopbarViewV2 *) getTopBarView;

@end

@implementation RoomInfoAssignmentV2
@synthesize aRoomDetail,aGuestInfo,segmentedSwitch,scrollView,navigationBar,anavigationItem;
@synthesize  btnRoomDetail,btnGuestInfo;
@synthesize roomName;
@synthesize roomModel, guestInfoModel, lblLocation,isRoomDetailsActive;
@synthesize aDateTimePickerView,cleaningStatusView;
@synthesize topBarView;
@synthesize roomStatusView;
@synthesize isPass;
@synthesize isInspected;
@synthesize isCompleted;
@synthesize userCanInteraction, isFindByRoomView;
@synthesize raDataModel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        userCanInteraction = YES;
    }
    
    return self;
}

-(NSString *) getEditString {
    NSDictionary *dic = [[LanguageManagerV2 sharedLanguageManager] getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_EDIT]];
    if (s == nil) {
        s = @"Edit";
    }
    return s;
}

-(NSString *) getCancelString {
    NSDictionary *dic = [[LanguageManagerV2 sharedLanguageManager] getCurrentDictionaryLanguage];
    return [dic valueForKey:[NSString stringWithFormat:@"%@", L_CANCEL]];
}

-(NSString *) getDoneString {
    NSDictionary *dic = [[LanguageManagerV2 sharedLanguageManager] getCurrentDictionaryLanguage];
    return [dic valueForKey:[NSString stringWithFormat:@"%@", L_DONE]];
}

-(NSString *) getBackString {
    NSDictionary *dic = [[LanguageManagerV2 sharedLanguageManager] getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_BACK]];
    if (s == nil) {
        s = @"Back";
    }
    return s;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
    [topBarView setCaptionsView];
    [self.view addSubview:topBarView];
    
    CGRect frame = aRoomDetail.view.frame;
    frame.origin.y = 105;
    [aRoomDetail.view setFrame:frame];
    
    if(isRoomDetailsActive) {
        [self openRoomDetail:nil];
    }
}

#pragma mark - View lifecycle
-(void)viewWillAppear:(BOOL)animated {
    //show wifi view
    if(!isDemoMode){
        [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationShowWifiView] object:nil];
    }
    
    //disable CheckList button on tabbarcontroller
//    UIButton *butCheckList = (UIButton *)[[self.tabBarController tabBar] viewWithTag:tagOfCheckListButton];
//    [butCheckList setEnabled:NO];
            
    //post a selectedHome notification
    //check if lower view controller is home view class, set select Home
    //else set find or other
    NSArray *viewControllers = self.navigationController.viewControllers;
    UIViewController *viewController = [viewControllers objectAtIndex:viewControllers.count - 2];
    if ([viewController isKindOfClass:[HomeViewV2 class]]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationSelectedHome] object:nil];
    } else {
        if ([viewController isKindOfClass:[FindByRoomViewV2 class]]) {
            [[HomeViewV2 shareHomeView] setSelectedButton:tagOfFindButton];
        }
    }
    
    if(aRoomDetail.raDataModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_PAUSE){
        if (isRoomDetailsActive == YES) {
            [self.aRoomDetail viewWillAppear:YES];
        } else {
            [self.aGuestInfo viewWillAppear:YES];
        }
    }
    
//    if([CommonVariable sharedCommonVariable].isBackFromChk){
//    }       
    
    [super viewWillAppear:animated];
 }

-(void)viewWillDisappear:(BOOL)animated {
    //Disable CheckList button on tabbarcontroller
//    UIButton *butCheckList = (UIButton *)[[self.tabBarController tabBar] viewWithTag:tagOfCheckListButton];
//    [butCheckList setEnabled:NO];
    
    [super viewWillDisappear:animated];
}

- (void)viewDidLoad {
    [[HomeViewV2 shareHomeView] waitingLoadingData];
    
    [super viewDidLoad];    
    self.roomModel.room_Id = [RoomManagerV2 getCurrentRoomNo];
    [[RoomManagerV2 sharedRoomManager] loadRoomModel:self.roomModel];
    
    // Do any additional setup after loading the view from its nib.
        
    //set background:
    // *imgbg=[UIImage imageNamed:background];
   // UIColor *bgColor=[[UIColor alloc] initWithPatternImage:imgbg];

    //self.view.backgroundColor=bgColor;
    
    if(aRoomDetail == nil) {
        // load screen of RoomDetail first.    
        self.aRoomDetail = [[RoomDetailAssignmentViewV2 alloc] initWithNibName:@"RoomDetailAssignmentViewV2" bundle:nil];  
        
        /*********************************************************/
        aRoomDetail.isPass = self.isPass;
        aRoomDetail.isCompleted = self.isCompleted;
        aRoomDetail.isInspected = self.isInspected;
        [aRoomDetail setDelegate:self];
        
        //set user interaction for find feature
//        [aRoomDetail setUserCanInteraction:userCanInteraction];
        if(userCanInteraction == TRUE) {
            aRoomDetail.userCanInteraction = TRUE;
            aRoomDetail.isFindByRoomView = NO;

            [aRoomDetail.listButtonView setUserInteractionEnabled:TRUE];
            [aRoomDetail.tableView setUserInteractionEnabled:TRUE];
        } else {
            aRoomDetail.userCanInteraction = FALSE;
            aRoomDetail.isFindByRoomView = isFindByRoomView;

            [aRoomDetail.listButtonView setUserInteractionEnabled:FALSE];
            [aRoomDetail.tableView setUserInteractionEnabled:TRUE];
        }
        
        /*********************************************************/
        
        aRoomDetail.superController = self;
        
        //set data for RoomDetail from roomModel
        self.aRoomDetail.dataModel = self.roomModel;
        self.aRoomDetail.raDataModel = self.raDataModel;
        
        //refresh view when set datamodel
        [aRoomDetail viewWillAppear:NO];
        
        CGRect frame = aRoomDetail.view.frame;
        frame.origin.y = 105;
        [aRoomDetail.view setFrame:frame];
        [self.view addSubview:aRoomDetail.view];
    }
    
    self.isRoomDetailsActive = YES;
    //config image for  switchview use button
    UIImage  *imgRoomDetailActive = [UIImage imageNamed:highlightImage];    
    UIImage *imgGuestInfo = [UIImage imageNamed:normalImage];
    
    //set image to button roomdetail.
    [btnRoomDetail setBackgroundImage:imgRoomDetailActive forState:UIControlStateNormal];
    [btnGuestInfo setBackgroundImage:imgGuestInfo forState:UIControlStateNormal];
    [scrollView setContentSize:CGSizeMake([scrollView bounds].size.width ,[scrollView bounds].size.height+100)];

    NSString *roomTitle = [NSString stringWithFormat:@"%@ ", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOM_NO_DETAIL]];
    roomTitle=[roomTitle stringByAppendingString:self.roomName];
    [self.navigationItem setTitle:roomTitle];

    [btnRoomDetail setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOM_DETAIL] forState:UIControlStateNormal];
    [btnRoomDetail setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [btnGuestInfo setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_GUEST_INFO] forState:UIControlStateNormal];
    [btnGuestInfo setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    // addObserver to add CLGView
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addCLGViewWithIndexSelect:) name:@"AddCLGWithSelectedIndex" object:nil];    

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addEngineeringView) name:[NSString stringWithFormat:@"%@", notificationAddEngineeringView] object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addLostAndFoundView) name:[NSString stringWithFormat:@"%@", notificationAddLostAndFoundView] object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addGuidelineView) name:[NSString stringWithFormat:@"%@", notificationGuidelineView] object:nil];
   
    SuperRoomModelV2 *modelSuper = [[SuperRoomModelV2 alloc] init];
    //[[RoomManagerV2 sharedRoomManager] getRoomAssignmentComplete];
    NSString *strGetPropertyName=[[RoomManagerV2 sharedRoomManager] getPropertyName:modelSuper];
    NSString *strGetBuildingName=[[RoomManagerV2 sharedRoomManager] getbuildingName:modelSuper];
    if (strGetBuildingName == nil) {
        strGetBuildingName = @" ";
    }
    
    if (strGetPropertyName == nil) {
        strGetPropertyName = @" ";
    }
    
    [lblLocation setText:[NSString stringWithFormat:@"%@  %@", strGetBuildingName, strGetPropertyName]];
    
    houseKeeperNamelabel.text=[[RoomManagerV2 sharedRoomManager] getHouseKeeperName];
    
    if (topBarView == nil) {
        topBarView = [[TopbarViewV2 alloc] initViewWithSuperModel:[[SuperRoomModelV2 alloc] init]];
        [self.view addSubview:topBarView];
    }
    
    /************ add a button as title view of navigation bar **********/
    UIButton *titleBarButton = [[UIButton alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
    //[titleBarButton setFrame:frame];
    [titleBarButton.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
    [titleBarButton.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
    [titleBarButton.titleLabel setFont:FONT_BUTTON_TOPBAR];
    titleBarButton.titleLabel.backgroundColor = [UIColor clearColor];
    titleBarButton.titleLabel.textAlignment = UITextAlignmentCenter;
    titleBarButton.titleLabel.textColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:1.0];
    
    [titleBarButton addTarget:self action:@selector(hiddenHeaderView) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationItem setTitleView: titleBarButton];

    //Back button
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *img = [UIImage imageNamed:@"but_back.png"];
    [button setFrame:CGRectMake(0, 0, img.size.width, img.size.height)];
    [button setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK] forState:UIControlStateNormal];
    [button setBackgroundImage:img forState:UIControlStateNormal];
    [button addTarget:self action:@selector(backBarPressed) forControlEvents:UIControlEventTouchUpInside];
    [button.titleLabel setFont:[UIFont boldSystemFontOfSize:12.0]];
    [button setContentEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = barButtonItem;

    //set captions for view
    [self setCaptionsView];

    /*************check enable or disable Guestinfo*********************/
    if(raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VD || raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VC || 
       raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VI || raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OOO ||
       raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OOS
       ) {
        [btnGuestInfo setBackgroundImage:[UIImage imageNamed:@"top_bt_grey_240x58.png"] forState:UIControlStateNormal];
        [btnGuestInfo setUserInteractionEnabled:NO];
    }
     /*************** check enable or disable Action Bar ********************/ // An Rework
//    UserManagerV2 *userMgr = [[UserManagerV2 alloc] init];
    if([UserManagerV2 isSupervisor])
    {
        aRoomDetail.listButtonView.hidden = TRUE;
    }
    if (roomStatusView == nil) {
        roomStatusView = [[CleaningStatusViewV2 alloc] initWithNibName:@"CleaningStatusViewV2" bundle:nil];
    }
        
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
}

#pragma mark - === Handle Check Save Data ===
#pragma mark
-(BOOL) checkSaveDataWithTagEvent:(NSInteger) tagOfEvent {
//    if(aRoomDetail.countTimeStatus == STOP){
//        return [aRoomDetail isNotSavedAndExit];
//    }
//
//    if (tagOfEvent == tagOfMessageButton) {   
//        
//        if(aRoomDetail.raDataModel.roomAssignment_PostStatus == POST_STATUS_SAVED_UNPOSTED){
//            UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ALERT_SAVEROOMDETAIL] delegate:nil cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_OK] otherButtonTitles:nil];
//            [alert show];
//            
//            return YES;
//        }
//        return NO;
//    }
//
//
//    if ([aRoomDetail isNotSavedAndExit] == NO) {
//        //NO
//        return NO; //no need to save before leaving
//    } else {
//        //YES
//        //need to save before leaving
//        
//        return YES;
//    }
    
    if(aRoomDetail.countTimeStatus == STOP) {
        return [aRoomDetail isNotSavedAndExit];
    }
    
    if (tagOfEvent == tagOfMessageButton) {
        //can move to message screen but must set message
        if (aRoomDetail.countTimeStatus == START || aRoomDetail.isSaved == NO) {
            [[CommonVariable sharedCommonVariable] setRoomIsCompleted:0];
        } else {
            [[CommonVariable sharedCommonVariable] setRoomIsCompleted:1];
        }
        
        return NO;
    }
    
    if (isRoomDetailsActive == NO) {
        if ([aGuestInfo checkSaveGuestInfo] == YES) {
            //YES
        } else {
            return YES;
        }
        
        if ([aRoomDetail isNotSavedAndExit] == YES) {            
            return YES;
        }
        
        return NO;
    } else {
        if ([aRoomDetail isNotSavedAndExit] == NO) {
            //NO
            return NO; //no need to save before leaving
        } else {
            //YES
            //need to save before leaving
            
            return YES;
        }
    }
}

-(void)backBarPressed {
//    if(aRoomDetail.countTimeStatus != NOT_START || aRoomDetail.countTimeStatus != START){
//        [self.navigationController popViewControllerAnimated:YES];
//    }
//    if (isRoomDetailsActive == NO) {
//        if ([aGuestInfo checkSaveGuestInfo] == YES) {
//            //YES, no need save guest info
//        } else {
//            //NO, did show alert in guest info
//            return;
//        }
//        
//        if (aRoomDetail.isSaved == NO) {
//            //show please complete the room
//            UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_complete_room_to_leave] delegate:nil cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_OK] otherButtonTitles:nil];
//            [alert show];
//        } else {
//            [self.navigationController popViewControllerAnimated:YES];
//        }
//        
//    } else
//        if([aRoomDetail isNotSavedAndExit] == NO){
//            [self.navigationController popViewControllerAnimated:YES];
//        }
    
    if (isRoomDetailsActive == NO) {
        if ([aGuestInfo checkSaveGuestInfo] == YES) {
            //YES, no need save guest info
            
        } else {
            //NO, did show alert in guest info
            return;
        }
        
        if ([aRoomDetail isNotSavedAndExit] == YES) {
            
            
        } else {
            [self.navigationController popViewControllerAnimated:YES];
        }        
    } else {
        if([aRoomDetail isNotSavedAndExit] == NO){
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

#pragma mark
#pragma mark hidden topbar
-(void)hiddenHeaderView {    
    BOOL isHidden = topBarView.hidden;
    [topBarView setHidden:!isHidden];
    if(isHidden) {        
        [btnRoomDetail setFrame:CGRectMake(0, 56, 168, 37)];
        [btnGuestInfo setFrame:CGRectMake(166, 56, 154, 37)];
        [scrollView setFrame:CGRectMake(0,56, 319, 327)];
        CGRect frame = aRoomDetail.view.frame;
        frame.origin.y = 104;
        [aRoomDetail.view setFrame:frame];
        frame = aGuestInfo.view.frame;
        frame.origin.y = 104;
        [aGuestInfo.view setFrame:frame];
    } else {       
        [btnRoomDetail setFrame:CGRectMake(0, 0, 168, 37)];
        [btnGuestInfo setFrame:CGRectMake(166, 0, 154, 37)];
        [scrollView setFrame:CGRectMake(0,56, 319, 327)];
        CGRect frame = aRoomDetail.view.frame;
        frame.origin.y = 56;
        [aRoomDetail.view setFrame:frame];
        frame = aGuestInfo.view.frame;
        frame.origin.y = 56;
        [aGuestInfo.view setFrame:frame];       
    }
}

#pragma mark
#pragma mark Save data
-(void)saveRoomDetail {
    [aRoomDetail saveData];
}

-(void)saveData{
    if(isRoomDetailsActive == YES) {
        [self saveRoomDetail];
        
        //save guest info if needed
        if (aGuestInfo.isSaved == NO) {
            [aGuestInfo saveDataWithoutHUD];
        }
    } else {
        [aGuestInfo saveData];
    }
    
    //if(isRoomDetailsActive) [self saveRoomDetail];
}

-(BOOL)isNotSavedAndExit{
    return [aRoomDetail isNotSavedAndExit];
}

#pragma mark
#pragma mark roomDetail Delegate
-(void)showPopupCleaningStatus:(UIViewController *)controller{
    if (cleaningStatusView == nil) {
        cleaningStatusView = [[CleaningStatusViewV2 alloc] initWithNibName:@"CleaningStatusViewV2" bundle:nil];
    }
    
    if(raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OC || raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OD || raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OOO || raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OI|| raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OOS || raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OPU) {        
        cleaningStatusView.isOccupied = YES;
    } else{
        cleaningStatusView.isOccupied = NO;
    }
    cleaningStatusView._currentCleaningStatus = aRoomDetail.raDataModel.roomAssignmentRoomCleaningStatusId;
    [cleaningStatusView setDelegate:self];
    cleaningStatusView.title = self.navigationItem.title;
    cleaningStatusView.typeStatus = ENUM_CLEANING_STATUS;
    cleaningStatusView.isFindByRoomView = isFindByRoomView;

    UIBarButtonItem *BackButtonRoomInfo = [[UIBarButtonItem alloc] initWithTitle: [self getBackString] style: UIBarButtonItemStyleBordered target: nil action: nil];    
    [[self navigationItem] setBackBarButtonItem: BackButtonRoomInfo];       

    [self.navigationItem.backBarButtonItem setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK]];
    [self.navigationController pushViewController:cleaningStatusView animated:YES];
}

-(void)showPopupRoomStatus:(UIViewController *)controller{
    if (roomStatusView == nil) {
        roomStatusView = [[CleaningStatusViewV2 alloc] initWithNibName:@"CleaningStatusViewV2" bundle:nil];
    }
    
    if(raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OC || raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OD || raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OOO || raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OI || raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OOS || raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OPU) {        
        roomStatusView.isOccupied = YES;
    } else{
        roomStatusView.isOccupied = NO;
    }    
    [roomStatusView setDelegate:self];
    roomStatusView.title = self.navigationItem.title;
    roomStatusView.typeStatus = ENUM_ROOM_STATUS;
    
    UIBarButtonItem *BackButtonRoomInfo = [[UIBarButtonItem alloc] initWithTitle: [self getBackString] style: UIBarButtonItemStyleBordered target: nil action: nil];    
    [[self navigationItem] setBackBarButtonItem: BackButtonRoomInfo];    
    
    [self.navigationItem.backBarButtonItem setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK]];
    [self.navigationController pushViewController:roomStatusView animated:YES];
}

#pragma mark
#pragma mark Cleaning Status
-(void)openCleaningStatusView {
    if (cleaningStatusView == nil) {
        cleaningStatusView = [[CleaningStatusViewV2 alloc] initWithNibName:@"CleaningStatusViewV2" bundle:nil];
    }
    [cleaningStatusView setDelegate:self];
    cleaningStatusView.title = self.navigationItem.title;
    cleaningStatusView.typeStatus = ENUM_CLEANING_STATUS;
    
    UIBarButtonItem *BackButtonRoomInfo = [[UIBarButtonItem alloc] initWithTitle: [self getBackString] style: UIBarButtonItemStyleBordered target: nil action: nil];    
    [[self navigationItem] setBackBarButtonItem: BackButtonRoomInfo]; 
    
    [self.navigationItem.backBarButtonItem setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK]];
    [self.navigationController pushViewController:cleaningStatusView animated:YES];    
}

-(void) openRoomStatusView {
    if (roomStatusView == nil) {
        roomStatusView = [[CleaningStatusViewV2 alloc] initWithNibName:@"CleaningStatusViewV2" bundle:nil];
    }
    [roomStatusView setDelegate:self];
    roomStatusView.title = self.navigationItem.title;
    roomStatusView.typeStatus = ENUM_ROOM_STATUS;
    
    UIBarButtonItem *BackButtonRoomInfo = [[UIBarButtonItem alloc] initWithTitle: [self getBackString] style: UIBarButtonItemStyleBordered target: nil action: nil];    
    [[self navigationItem] setBackBarButtonItem: BackButtonRoomInfo];    
    
    [self.navigationItem.backBarButtonItem setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK]];
    [self.navigationController pushViewController:roomStatusView animated:YES];
}

// delegate get cleaning status
-(void)cleaningStatus:(NSInteger)cleaningStatus isRoomStatus:(BOOL)isRoomStatus withViewController:(UIViewController *)controller{
    [controller.navigationController popViewControllerAnimated:NO];
    
    if(isRoomStatus){
        [aRoomDetail roomStatusDidSelect:cleaningStatus];
    } else{
        [aRoomDetail cleaningStatusDidSelect:cleaningStatus];
    }
}

-(void)cleaningStatus:(NSInteger)cleaningStatus isRoomStatus:(BOOL)isRoomStatus withTime:(NSDate *)date withViewController:(UIViewController *)controller{
    [controller.navigationController popViewControllerAnimated:NO];
    if(!isRoomStatus) {
        [aRoomDetail cleaningStatusDidSelect:cleaningStatus andDate:date];
    } 
}

-(void)removePickerViewGuestInfo:(NSNotification *)notification {
    [[self navigationController] setNavigationBarHidden:NO animated:NO];    
    [self.aDateTimePickerView.view removeFromSuperview];
    
    // post this data notification to guestinfo.
    NSString *dataDateGuestInfo=(NSString*)[notification object];
    [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@",returnDateTimePickerNotification]  object:dataDateGuestInfo];
    [self.navigationItem setHidesBackButton:NO];
    [self.navigationItem setHidesBackButton:YES];
}

// remove cleaningStatusView.
-(void)removeCleaningStatusView:(NSNotification*)nt {
    NSString *temp = (NSString *)[nt object];
    
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    [self.cleaningStatusView.view removeFromSuperview];
    
    if([temp caseInsensitiveCompare:@"cancel"] == NSOrderedSame) return;  // @ if user choice cancel then do not any thing .
    [[NSNotificationCenter defaultCenter] postNotificationName:@"closeCleaningStatusView" object:nil];
}

#pragma mark-
#pragma event switch view room detail and guest info
- (IBAction)openRoomDetail:(id)sender {
    [[self navigationItem] setRightBarButtonItem:nil];
    isRoomDetailsActive = YES;
    if(aRoomDetail == nil) {
        aRoomDetail=[[RoomDetailAssignmentViewV2 alloc] initWithNibName:@"RoomDetailAssignmentViewV2" bundle:nil];
        aRoomDetail.superController = self;
        [[RoomManagerV2 sharedRoomManager] loadRoomModel:self.roomModel];
        aRoomDetail.dataModel = self.roomModel;
        aRoomDetail.raDataModel = self.raDataModel;
    }
    
    //set user interaction for find feature
    [aRoomDetail setUserCanInteraction:userCanInteraction];
       
     UIImage  *imgRoomDetailActive=[UIImage imageNamed:highlightImage];
    [btnRoomDetail setBackgroundImage:imgRoomDetailActive forState:UIControlStateNormal];
    // set state of tabview guest info
    UIImage *imgGuestInfo=[UIImage imageNamed:normalImage];
    [btnGuestInfo setBackgroundImage:imgGuestInfo forState:UIControlStateNormal];

    if (self.aGuestInfo.statusEditing == YES) {
        [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", editCancelRoomInfoNotificaion] object:nil];
        [self.navigationItem setLeftBarButtonItem:nil];
        [self.navigationItem setHidesBackButton:NO animated:YES];
    }
    
    [self.aGuestInfo.view removeFromSuperview];
    [self.view addSubview:aRoomDetail.view];
    
    scrollView.clipsToBounds = YES;
    [scrollView setContentSize:CGSizeMake([scrollView bounds].size.width ,[scrollView bounds].size.height+50)];
    
    if(raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VD || raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VC){
        [btnGuestInfo setBackgroundImage:[UIImage imageNamed:@"top_bt_grey_240x58.png"] forState:UIControlStateNormal];
        [btnGuestInfo setUserInteractionEnabled:NO];
    }
}

- (IBAction)openGuestInfo:(id)sender {
    if (self.aGuestInfo.statusEditing == YES) {
        [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", editCancelRoomInfoNotificaion] object:nil];
        [self.navigationItem setLeftBarButtonItem:nil];
        [self.navigationItem setHidesBackButton:NO animated:YES];
    }
    
    isRoomDetailsActive = NO;
    if (aGuestInfo == nil) {
        aGuestInfo = [[GuestInfoViewV2 alloc] initWithNibName:NSStringFromClass([GuestInfoViewV2 class]) bundle:nil];
        aGuestInfo.parentVC = self;
        CGRect fr = aGuestInfo.view.frame;
        if (topBarView.isHidden) {
            fr.origin.y = 56;
        } else
            fr.origin.y = 95;
        [aGuestInfo.view setFrame:fr];
    }
    
    //set user interaction for find feature
    [aGuestInfo setUserCanInteraction:userCanInteraction];
    
    // set image to active guest info
    UIImage *imgGuestInfoActice=[UIImage imageNamed:highlightImage];  
    [btnGuestInfo setBackgroundImage:imgGuestInfoActice forState:UIControlStateNormal];

    // set image to display normal RoomDetail
    UIImage  *imgRoomDetail=[UIImage imageNamed:normalImage];
    [btnRoomDetail setBackgroundImage:imgRoomDetail forState:UIControlStateNormal];

    [self.aRoomDetail.view removeFromSuperview];
    [aGuestInfo.view setFrame:CGRectMake(0, 0, 320, 500)];
    
    //set parent view controller for show hud
    aGuestInfo.parentViewController = self;
    
    //[self.scrollView addSubview:aGuestInfo.view];
    [self.view addSubview:aGuestInfo.view];
    
    [aGuestInfo viewWillAppear:NO];
    CGRect frame = [aGuestInfo.view frame];
    if (topBarView.isHidden) {
        frame.origin.y = 56;
    } else
        frame.origin.y = 95;
    aGuestInfo.view.frame =frame;
    [self.view addSubview:aGuestInfo.view];

    [self.scrollView setContentSize:CGSizeMake([scrollView bounds].size.width , aGuestInfo.tbvGuestInfo.frame.size.height + 50)];    
}

- (void)viewDidUnload {  
    lblLocation = nil;  
    imgViewHotelLogo = nil;
    cleaningStatusView=nil;  
    houseKeeperNamelabel = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark -
#pragma mark Selector AddCLGView by push navigation
-(void) addCLGViewWithIndexSelect:(NSNotification *) selectedIndex{
    UIBarButtonItem *BackButtonRoomInfo = [[UIBarButtonItem alloc] initWithTitle: [self getBackString] style: UIBarButtonItemStyleBordered target: nil action: nil];    
    [[self navigationItem] setBackBarButtonItem: BackButtonRoomInfo];    

    
    NSString *temp = (NSString *)[selectedIndex object];
    NSInteger index = [temp intValue];
    
    if (index == 1) {     
  
        CountViewV2 *countview = [[CountViewV2 alloc]initWithNibName:NSStringFromClass([CountViewV2 class]) bundle:nil];
        [self.navigationController pushViewController:countview animated:YES];
        return;
    }
}

#pragma mark - Loading data
-(void)loadingData{
    //Disable sync feature guest info
    NSInteger userId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    NSInteger roomId = [RoomManagerV2 getCurrentRoomNo];
    
    if (guestInfoModel != nil) {
        if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp] == YES) {
            // show loading message
            MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
            
            [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getRaLoadingData]];
            [HUD show:YES];
            [self.tabBarController.view addSubview:HUD];
            
            //loading data            
            SyncManagerV2 *sManager = [[SyncManagerV2 alloc] init];
            
            if(![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
                [HUD removeFromSuperview];
                return;
            }
            
            if (![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
                [HUD removeFromSuperview];
                return;
            }    

            //[sManager getGuestInfoWithUserID:userId roomAssignmentID:roomId AndLastModified:nil];
            
            [aRoomDetail viewWillAppear:NO];
            [HUD removeFromSuperview];
        }
    }
}

#pragma mark - Add Engineering View
-(void) addEngineeringView {
    if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp] == NO) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[[LanguageManagerV2 sharedLanguageManager] getNoWifi] message:nil delegate:nil cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getOK] otherButtonTitles: nil];
        [alert show];
        
        return;
    }
    
    UIBarButtonItem *BackButtonRoomInfo = [[UIBarButtonItem alloc] initWithTitle: [self getBackString] style: UIBarButtonItemStyleBordered target: nil action: nil];    
    [[self navigationItem] setBackBarButtonItem: BackButtonRoomInfo];    
    
    EngineeringCaseViewV2 *ecView = [[EngineeringCaseViewV2 alloc] init];
    [self.navigationController pushViewController:ecView animated:YES];
}

#pragma mark - Add Lost And Found View
-(void) addLostAndFoundView {
    if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp] == NO) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[[LanguageManagerV2 sharedLanguageManager] getNoWifi] message:nil delegate:nil cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getOK] otherButtonTitles: nil];
        [alert show];
        return;
    }
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK] style:UIBarButtonItemStyleBordered target:nil action:nil];
    self.navigationItem.backBarButtonItem = backButton;
   
    LostFoundViewV2 *lfView = [[LostFoundViewV2 alloc] initWithNibName:@"LostFoundViewV2" bundle:nil];        
    [self.navigationController pushViewController:lfView animated:YES];
}

#pragma mark - Add guideline view
- (void)addGuidelineView {
    UIBarButtonItem *BackButtonRoomInfo = [[UIBarButtonItem alloc] initWithTitle: [self getBackString] style: UIBarButtonItemStyleBordered target: nil action: nil];    
    [[self navigationItem] setBackBarButtonItem: BackButtonRoomInfo];    
   
    GuidelineViewV2* aGuidelineView = [[GuidelineViewV2 alloc] initWithNibName:@"GuidelineViewV2" bundle:nil];
    
    //set flag room is not completed
    [aGuidelineView setIsRoomCompleted:NO];
    
    [self.navigationController pushViewController:aGuidelineView animated:YES];    
}

#pragma mark - set captions view
-(void)setCaptionsView {
    NSDictionary *dic = [[LanguageManagerV2 sharedLanguageManager] getCurrentDictionaryLanguage];
    [btnRoomDetail setTitle:[dic valueForKey:[NSString stringWithFormat:@"%@", L_ROOM_DETAIL]] forState:UIControlStateNormal];
    [btnGuestInfo setTitle:[dic valueForKey:[NSString stringWithFormat:@"%@", L_GUEST_INFO]] forState:UIControlStateNormal];
  
    UIButton *titleButton = (UIButton *)self.navigationItem.titleView;
    [titleButton setTitle:[NSString stringWithFormat:@"%@ %@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOM_TITLE], self.roomName] forState:UIControlStateNormal];
}

#pragma mark - === Set User Interaction ===
#pragma mark
-(void)disableUserInteractionInView {
    userCanInteraction = NO;
    
    isFindByRoomView = YES; // check RoomInfoAssignmentV2 accessed by FindByRoomView
}

@end
