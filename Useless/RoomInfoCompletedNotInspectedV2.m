//
//  RoomInfo.m
//  EHouseKeeping
//
//  Created by tms on 5/12/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "RoomInfoCompletedNotInspectedV2.h"
//#import "LocationModel.h"
//#import "UserManager.h"
#import "ehkDefines.h"
#import "RoomManagerV2.h"
//#import "RoomAssignmentEngine.h"
#import "NetworkCheck.h"
//#import "SynManager.h"
#import "MBProgressHUD.h"
#import "EngineeringCaseViewV2.h"
//#import "EngineeringCaseView.h"
//#import "LostFoundView.h"
//#import "HotelManager.h"
#import "LostFoundViewV2.h"
#import "GuidelineViewV2.h"
#import "SyncManagerV2.h"
#import "EngineeringCaseDetailModelV2.h"
#import "EngineerCaseViewManagerV2.h"
//#import "HomeViewV2.h"

#define highlightImage      @"tab_count_active.png"
#define normalImage         @"tab_count_NOactive.png"
#define background          @"bg.png"
#define sizeTitle           22


@interface RoomInfoCompletedNotInspectedV2 (PrivateMethods)

#define tagTopbarView 1234

-(void) loadTopbarView;
-(void) showTopbarView;
-(void) hideTopbarView;
-(void) setHiddenTopbarView;
-(void) adjustShowForViews;
-(void) adjustRemoveForViews;
-(void) addButtonHandleShowHideTopbar;
-(void) topbarTapped:(UIButton *) sender;
-(TopbarViewV2 *) getTopBarView;

@end

@implementation RoomInfoCompletedNotInspectedV2
@synthesize aRoomDetail,aGuestInfo,segmentedSwitch,scrollView,navigationBar,anavigationItem;
@synthesize  btnRoomDetail,btnGuestInfo;
@synthesize roomName;
@synthesize roomModel, guestInfoModel, lblLocation,isRoomDetailsActive;
//@synthesize clgView,
@synthesize aDateTimePickerView,cleaningStatusView;
@synthesize topBarView;
@synthesize roomStatusView;
@synthesize isPass;
@synthesize isInspected;
@synthesize isCompleted;
@synthesize raDataModel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

//- (void)dealloc
//{
//    [navigationBar release];
//    [anavigationItem release];
//    [btnRoomDetail release];
//    [btnGuestInfo release];
//    [scrollView release];
//    [roomName release];
//    [roomModel release];
//    [guestInfoModel release];
//    [aDateTimePickerView release];
//    [aRoomDetail release];
//    [aGuestInfo release];
//    [cleaningStatusView release];
////    [clgView release];
//    [lblLocation release];
//    [imgViewHotelLogo release];
//    
//    [houseKeeperNamelabel release];
//    [super dealloc];
//}


-(NSString *) getEditString {
    NSDictionary *dic = [[LanguageManagerV2 sharedLanguageManager] getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_EDIT]];
//    NSLog(@"edit string: %@ --------------", s);
    if (s == nil) {
        s = @"Edit";
    }
    return s;
}

-(NSString *) getCancelString {
    NSDictionary *dic = [[LanguageManagerV2 sharedLanguageManager] getCurrentDictionaryLanguage];
    return [dic valueForKey:[NSString stringWithFormat:@"%@", L_CANCEL]];
}

-(NSString *) getDoneString {
    NSDictionary *dic = [[LanguageManagerV2 sharedLanguageManager] getCurrentDictionaryLanguage];
    return [dic valueForKey:[NSString stringWithFormat:@"%@", L_DONE]];
}

-(NSString *) getBackString {
    NSDictionary *dic = [[LanguageManagerV2 sharedLanguageManager] getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_BACK]];
    if (s == nil) {
        s = @"Back";
    }
    return s;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    [topBarView setCaptionsView];
    [self.view addSubview:topBarView];

    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle


-(void)viewWillAppear:(BOOL)animated{
    // refresh data
    
    if(!animated){
        self.roomModel.room_Id = [RoomManagerV2 getCurrentRoomNo];
        [[RoomManagerV2 sharedRoomManager] loadRoomModel:self.roomModel];
    }
    
    
    //show wifi view
    if(!isDemoMode){
        [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationShowWifiView] object:nil];
    }
    
    //enable CheckList button on tabbarcontroller
//    UIButton *butCheckList = (UIButton *)[[self.tabBarController tabBar] viewWithTag:tagOfCheckListButton];
//    [butCheckList setEnabled:YES];
    
    //post a selectedHome notification
    [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationSelectedHome] object:nil];

    if([CommonVariable sharedCommonVariable].isBackFromChk){
        if (isRoomDetailsActive == YES) {
            [self.aRoomDetail viewWillAppear:NO];
        } else {
            [self.aGuestInfo viewWillAppear:NO];
        }

    }
        
    [super viewWillAppear:animated];
}

-(void)viewWillDisappear:(BOOL)animated {
    //Disable CheckList button on tabbarcontroller
//    UIButton *butCheckList = (UIButton *)[[self.tabBarController tabBar] viewWithTag:tagOfCheckListButton];
//    [butCheckList setEnabled:NO];
    
    [super viewWillDisappear:animated];
}

- (void)viewDidLoad
{
    [super viewDidLoad];    
    
    [[HomeViewV2 shareHomeView] waitingLoadingData];
    
    // Do any additional setup after loading the view from its nib.
    //Catch Notification checkListNotification to add button Back
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addBackButtonForCheckListView) name:[NSString stringWithFormat:@"%@", notificationCheckList] object:nil];
        
    //set background:
    UIImage *imgbg=[UIImage imageNamed:background];
    UIColor *bgColor=[[UIColor alloc] initWithPatternImage:imgbg];

    self.view.backgroundColor=bgColor;
    if(self.aRoomDetail == nil){
    //self.scrollView.backgroundColor=bgColor;
    // load screen of RoomDetail first.    
     self.aRoomDetail=[[RoomDetailCompletedNotInspectedViewV2 alloc] initWithNibName:@"RoomDetailCompletedNotInspectedViewV2" bundle:nil];  
    
    /*********************************************************/
    aRoomDetail.isPass = self.isPass;
    aRoomDetail.isCompleted = self.isCompleted;
    aRoomDetail.isInspected = self.isInspected;
    /*********************************************************/

    
    aRoomDetail.superController = self;
    
    //set data for RoomDetail from roomModel
    self.aRoomDetail.dataModel = self.roomModel;
    self.aRoomDetail.raDataModel = self.raDataModel;
    
    CGRect frame = aRoomDetail.view.frame;
    frame.origin.y = 105;
    [aRoomDetail.view setFrame:frame];
    [self.view addSubview:aRoomDetail.view];
    }
    
    self.isRoomDetailsActive=YES;
    //config image for  switchview use button
    UIImage  *imgRoomDetailActive=[UIImage imageNamed:highlightImage];    
    UIImage *imgGuestInfo=[UIImage imageNamed:normalImage];
    
    //set image to button roomdetail.
    [btnRoomDetail setBackgroundImage:imgRoomDetailActive forState:UIControlStateNormal];
    [btnGuestInfo setBackgroundImage:imgGuestInfo forState:UIControlStateNormal];
    [scrollView setContentSize:CGSizeMake([scrollView bounds].size.width ,[scrollView bounds].size.height+100)];

    NSString *roomTitle= [NSString stringWithFormat:@"%@ ", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOM_NO_DETAIL]];
    roomTitle=[roomTitle stringByAppendingString:self.roomName];
    [self.navigationItem setTitle:roomTitle];
    // add back button 

    [btnRoomDetail setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOM_DETAIL] forState:UIControlStateNormal];
    [btnRoomDetail setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [btnGuestInfo setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_GUEST_INFO] forState:UIControlStateNormal];
    [btnGuestInfo setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    // init CLGView
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addCLGViewWithIndexSelect:) name:@"AddCLGWithSelectedIndex" object:nil];
      
      [[NSNotificationCenter defaultCenter] addObserver: self selector:@selector(openCleaningStatusView) name:@"openCleaningStatusView" object:nil];
    
     [[NSNotificationCenter defaultCenter] addObserver: self selector:@selector(openRoomStatusView) name:@"openRoomStatusView" object:nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserver: self selector:@selector(removeCleaningStatusView:) name:@"removeCleaningStatusView" object:nil];
    //addObserver to add EngineeringView
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addEngineeringView) name:[NSString stringWithFormat:@"%@", notificationAddEngineeringView] object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addLostAndFoundView) name:[NSString stringWithFormat:@"%@", notificationAddLostAndFoundView] object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addGuidelineView) name:[NSString stringWithFormat:@"%@", notificationGuidelineView] object:nil];
    
   
    SuperRoomModelV2 *modelSuper = [[SuperRoomModelV2 alloc] init];
    [[RoomManagerV2 sharedRoomManager] getRoomAssignmentComplete];
    NSString *strGetPropertyName=[[RoomManagerV2 sharedRoomManager] getPropertyName:modelSuper];
    NSString *strGetBuildingName=[[RoomManagerV2 sharedRoomManager] getbuildingName:modelSuper];
    if (strGetBuildingName==nil) {
        strGetBuildingName=@" ";
    }
    if (strGetPropertyName==nil) {
        strGetPropertyName=@" ";
    }
    
    [lblLocation setText:[NSString stringWithFormat:@"%@  %@", strGetBuildingName, strGetPropertyName]];
    
    houseKeeperNamelabel.text=[[RoomManagerV2 sharedRoomManager] getHouseKeeperName];

    [RoomManagerV2 setCurrentRoomNo:roomModel.room_Id];
    
    if (topBarView == nil) {
        topBarView = [[TopbarViewV2 alloc] initViewWithSuperModel:[[SuperRoomModelV2 alloc] init]];
        [self.view addSubview:topBarView];
    }
    
    /************ add a button as title view of navigation bar **********/
    UIButton *titleBarButton = [[UIButton alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
    titleBarButton.titleLabel.backgroundColor = [UIColor clearColor];
    titleBarButton.titleLabel.font = [UIFont systemFontOfSize:sizeTitle];
    titleBarButton.titleLabel.textAlignment = UITextAlignmentCenter;
    titleBarButton.titleLabel.textColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:1.0];
    [titleBarButton addTarget:self action:@selector(hiddenHeaderView) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.titleView = titleBarButton;

    //Back button
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *img = [UIImage imageNamed:@"but_back.png"];
    [button setFrame:CGRectMake(0, 0, img.size.width, img.size.height)];
    [button setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK] forState:UIControlStateNormal];
    [button setBackgroundImage:img forState:UIControlStateNormal];
    [button addTarget:self action:@selector(backBarPressed) forControlEvents:UIControlEventTouchUpInside];
    [button.titleLabel setFont:[UIFont boldSystemFontOfSize:12.0]];
    [button setContentEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = barButtonItem;

    
    
    //set captions for view
    [self setCaptionsView];

    /*************check enable or disable Guestinfo*********************/
    if(raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VD || raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VC || 
       raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VI || raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OOO ||
       raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OOS){
        [btnGuestInfo setBackgroundImage:[UIImage imageNamed:@"top_bt_grey_240x58.png"] forState:UIControlStateNormal];
        [btnGuestInfo setUserInteractionEnabled:NO];
    }
    
    
    if (roomStatusView == nil) {
        roomStatusView = [[CleaningStatusViewV2 alloc] initWithNibName:@"CleaningStatusViewV2" bundle:nil];
    }
    
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
}

-(void)backBarPressed {
    if(isRoomDetailsActive){
        if(![aRoomDetail isNotSavedAndExit]){
            [self.navigationController popViewControllerAnimated:YES];
        }
    } else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark
#pragma mark hidden topbar
-(void)hiddenHeaderView{
    
    BOOL isHidden = topBarView.hidden;
    [topBarView setHidden:!isHidden];
    if(isHidden){
        
        [btnRoomDetail setFrame:CGRectMake(0, 56, 168, 37)];
        [btnGuestInfo setFrame:CGRectMake(166, 56, 154, 37)];
        [scrollView setFrame:CGRectMake(0,56, 319, 327)];
        CGRect frame = aRoomDetail.view.frame;
        frame.origin.y = 104;
        [aRoomDetail.view setFrame:frame];
        
        frame = aGuestInfo.view.frame;
        frame.origin.y = 104;
        [aGuestInfo.view setFrame:frame];
        
    } else
    {
       
        [btnRoomDetail setFrame:CGRectMake(0, 0, 168, 37)];
        [btnGuestInfo setFrame:CGRectMake(166, 0, 154, 37)];
        [scrollView setFrame:CGRectMake(0,56, 319, 327)];
        CGRect frame = aRoomDetail.view.frame;
        frame.origin.y = 56;
        [aRoomDetail.view setFrame:frame];

        frame = aGuestInfo.view.frame;
        frame.origin.y = 56;
        [aGuestInfo.view setFrame:frame];
    }


}

-(void)cancelEditRoomInfo
{
    [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", editCancelRoomInfoNotificaion] object:nil];
    UIBarButtonItem *EditButtonRoomInfo = [[UIBarButtonItem alloc] initWithTitle: [self getEditString] style: UIBarButtonItemStyleBordered target: self action: @selector(editRoomInfo)];    
    [[self navigationItem] setRightBarButtonItem:EditButtonRoomInfo];    
    
    [self.navigationItem setLeftBarButtonItem:nil];
    [self.navigationItem setHidesBackButton:NO animated:YES];
}

/*
 function for edit in Supervisor role
 */
-(void)editRoomInfo
{
   
    NSString *editGuestInfoNotification=[[NSString alloc] initWithFormat:@"%@",editRoomInfoNotification];
    [[NSNotificationCenter defaultCenter] postNotificationName:editGuestInfoNotification  object:nil];
    if (self.navigationItem.rightBarButtonItem.title==[self getEditString]) {
        
        UIBarButtonItem *DoneButtonRoomInfo = [[UIBarButtonItem alloc] initWithTitle: [self getDoneString] style: UIBarButtonItemStyleBordered target: self action: @selector(editRoomInfo)];    
        [[self navigationItem] setRightBarButtonItem:DoneButtonRoomInfo];    
        
        UIBarButtonItem *CancelButtonRoomInfo = [[UIBarButtonItem alloc] initWithTitle: [self getCancelString] style: UIBarButtonItemStyleBordered target: self action: @selector(cancelEditRoomInfo)];
        [self.navigationItem setHidesBackButton:YES animated:YES];
        [self.navigationItem setLeftBarButtonItem:CancelButtonRoomInfo animated:YES];

        
        CGSize size = self.scrollView.contentSize;
        size.height += 200;
        [self.scrollView setContentSize:size];
    }
    else
    {
        UIBarButtonItem *EditButtonRoomInfo = [[UIBarButtonItem alloc] initWithTitle: [self getEditString] style: UIBarButtonItemStyleBordered target: self action: @selector(editRoomInfo)];    
        [[self navigationItem] setRightBarButtonItem:EditButtonRoomInfo];  
        
        [self.navigationItem setLeftBarButtonItem:nil];
        [self.navigationItem setHidesBackButton:NO animated:YES];
        
        if (aGuestInfo != nil) {
            CGSize size = CGSizeMake([self.scrollView bounds].size.width, aGuestInfo.view.frame.size.height + 50);
            
            [self.scrollView setContentSize:size];
        }       
        
    }           

}

-(void)backToRoomAssignment
{
    [self.view removeFromSuperview];
}

-(void)openViewPicker
{
    [[self navigationController] setNavigationBarHidden:YES animated:NO];
     aDateTimePickerView=[[DateTimePickerView alloc] initWithNibName:@"DateTimePickerView" bundle:nil];
    [self.view addSubview:aDateTimePickerView.view];
    //[aDateTimePickerView release];
}

#pragma mark
#pragma mark Save data
-(void)saveRoomDetail {
    [aRoomDetail saveData];
}

-(void)saveData{
    
    if(isRoomDetailsActive) [self saveRoomDetail];
}

-(BOOL)isNotSavedAndExit{
    return [aRoomDetail isNotSavedAndExit];
}

#pragma mark
#pragma mark roomDetail Delegate
-(void)showPopupCleaningStatus:(UIViewController *)controller{
    if (cleaningStatusView == nil) {
        cleaningStatusView = [[CleaningStatusViewV2 alloc] initWithNibName:@"CleaningStatusViewV2" bundle:nil];
    }
    
    //roomModel.room_StatusId == ENUM_ROOM_STATUS_OCI || 
    if(raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OC || raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OD ||
       raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OOO || raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OI||
       raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OOS || raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OPU){
        
        cleaningStatusView.isOccupied = YES;
    } else{
        cleaningStatusView.isOccupied = NO;
    }
    
    [cleaningStatusView setDelegate:self];
    cleaningStatusView.title = self.navigationItem.title;
    cleaningStatusView.typeStatus = ENUM_CLEANING_STATUS;
    
    UIBarButtonItem *BackButtonRoomInfo = [[UIBarButtonItem alloc] initWithTitle: [self getBackString] style: UIBarButtonItemStyleBordered target: nil action: nil];    
    [[self navigationItem] setBackBarButtonItem: BackButtonRoomInfo];       

    [self.navigationItem.backBarButtonItem setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK]];
    [self.navigationController pushViewController:cleaningStatusView animated:YES];

}

-(void)showPopupRoomStatus:(UIViewController *)controller{
    if (roomStatusView == nil) {
        roomStatusView = [[CleaningStatusViewV2 alloc] initWithNibName:@"CleaningStatusViewV2" bundle:nil];
    }
    if(raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OC || raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OD ||
        raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OOO || raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OI||
       raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OOS || raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OPU){
        
        roomStatusView.isOccupied = YES;
    } else{
        roomStatusView.isOccupied = NO;
    }
        
    [roomStatusView setDelegate:self];
    roomStatusView.title = self.navigationItem.title;
    roomStatusView.typeStatus = ENUM_ROOM_STATUS;
    
    UIBarButtonItem *BackButtonRoomInfo = [[UIBarButtonItem alloc] initWithTitle: [self getBackString] style: UIBarButtonItemStyleBordered target: nil action: nil];    
    [[self navigationItem] setBackBarButtonItem: BackButtonRoomInfo];    

    
//    [cleaningStatusView loadData];
    [self.navigationItem.backBarButtonItem setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK]];
    [self.navigationController pushViewController:roomStatusView animated:YES];
 
}

#pragma mark
#pragma mark Cleaning Status


//open cleaning status view :
-(void)openCleaningStatusView
{
    if (cleaningStatusView == nil) {
        cleaningStatusView = [[CleaningStatusViewV2 alloc] initWithNibName:@"CleaningStatusViewV2" bundle:nil];
    }
    [cleaningStatusView setDelegate:self];
    cleaningStatusView.title = self.navigationItem.title;
    cleaningStatusView.typeStatus = ENUM_CLEANING_STATUS;
    
    UIBarButtonItem *BackButtonRoomInfo = [[UIBarButtonItem alloc] initWithTitle: [self getBackString] style: UIBarButtonItemStyleBordered target: nil action: nil];    
    [[self navigationItem] setBackBarButtonItem: BackButtonRoomInfo];    


    
     [self.navigationItem.backBarButtonItem setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK]];
     [self.navigationController pushViewController:cleaningStatusView animated:YES];
    
 
}

// open room status
-(void) openRoomStatusView{
    if (roomStatusView == nil) {
        roomStatusView = [[CleaningStatusViewV2 alloc] initWithNibName:@"CleaningStatusViewV2" bundle:nil];
    }
    [roomStatusView setDelegate:self];
    roomStatusView.title = self.navigationItem.title;
    roomStatusView.typeStatus = ENUM_ROOM_STATUS;
    
    UIBarButtonItem *BackButtonRoomInfo = [[UIBarButtonItem alloc] initWithTitle: [self getBackString] style: UIBarButtonItemStyleBordered target: nil action: nil];    
    [[self navigationItem] setBackBarButtonItem: BackButtonRoomInfo];    
    
    
    
    [self.navigationItem.backBarButtonItem setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK]];
    [self.navigationController pushViewController:roomStatusView animated:YES];
}
// delegate get cleaning status
-(void)cleaningStatus:(NSInteger)cleaningStatus isRoomStatus:(BOOL)isRoomStatus withViewController:(UIViewController *)controller{
    [controller.navigationController popViewControllerAnimated:YES];
    
    if(isRoomStatus){
        [aRoomDetail roomStatusDidSelect:cleaningStatus];
    } else{
        [aRoomDetail cleaningStatusDidSelect:cleaningStatus];
    }
}

-(void)cleaningStatus:(NSInteger)cleaningStatus isRoomStatus:(BOOL)isRoomStatus withTime:(NSDate *)date withViewController:(UIViewController *)controller{
    [controller.navigationController popViewControllerAnimated:NO];
    if(!isRoomStatus){
        [aRoomDetail cleaningStatusDidSelect:cleaningStatus andDate:date];
    } 
}
-(void)removePickerViewGuestInfo:(NSNotification *)notification
{
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    
    [self.aDateTimePickerView.view removeFromSuperview];
    
    // post this data notification to guestinfo.
    NSString *dataDateGuestInfo=(NSString*)[notification object];
    [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@",returnDateTimePickerNotification]  object:dataDateGuestInfo];
    [self.navigationItem setHidesBackButton:NO];
    [self.navigationItem setHidesBackButton:YES];
}

// remove cleaningStatusView.
-(void)removeCleaningStatusView:(NSNotification*)nt
{
    NSString *temp = (NSString *)[nt object];
    
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    [self.cleaningStatusView.view removeFromSuperview];
    
    if([temp caseInsensitiveCompare:@"cancel"] == NSOrderedSame) return;  // @ if user choice cancel then do not any thing .
    [[NSNotificationCenter defaultCenter] postNotificationName:@"closeCleaningStatusView" object:nil];
}

#pragma mark-
#pragma event switch view room detail and guest info


- (IBAction)openRoomDetail:(id)sender {
    [[self navigationItem] setRightBarButtonItem:nil];
    isRoomDetailsActive=YES;
    if(aRoomDetail==nil){
        aRoomDetail=[[RoomDetailCompletedNotInspectedViewV2 alloc] initWithNibName:@"RoomDetailCompletedNotInspectedViewV2" bundle:nil];
        aRoomDetail.superController = self;
    }
    
       
     UIImage  *imgRoomDetailActive=[UIImage imageNamed:highlightImage];
    [btnRoomDetail setBackgroundImage:imgRoomDetailActive forState:UIControlStateNormal];
    // set state of tabview guest info
    UIImage *imgGuestInfo=[UIImage imageNamed:normalImage];
    [btnGuestInfo setBackgroundImage:imgGuestInfo forState:UIControlStateNormal];

    if (self.aGuestInfo.statusEditing == YES) {
        [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", editCancelRoomInfoNotificaion] object:nil];
        [self.navigationItem setLeftBarButtonItem:nil];
        [self.navigationItem setHidesBackButton:NO animated:YES];
    }
    
    [self.aGuestInfo.view removeFromSuperview];    


    [self.view addSubview:aRoomDetail.view];
    [[RoomManagerV2 sharedRoomManager] loadRoomModel:self.roomModel];
    self.aRoomDetail.dataModel = self.roomModel;
    self.aRoomDetail.raDataModel = self.raDataModel;
    
    scrollView.clipsToBounds = YES;
     [scrollView setContentSize:CGSizeMake([scrollView bounds].size.width ,[scrollView bounds].size.height+50)];
//    
//    if(raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VD || raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VC){
//        [btnGuestInfo setBackgroundImage:[UIImage imageNamed:@"top_bt_grey_240x58.png"] forState:UIControlStateNormal];
//        [btnGuestInfo setUserInteractionEnabled:NO];
//    }
    
    
    if(raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VD || raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VC || 
       raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VI || raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OOO ||
       raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OOS){
        [btnGuestInfo setBackgroundImage:[UIImage imageNamed:@"top_bt_grey_240x58.png"] forState:UIControlStateNormal];
        [btnGuestInfo setUserInteractionEnabled:NO];
    }


}

- (IBAction)openGuestInfo:(id)sender {
    if (self.aGuestInfo.statusEditing == YES) {
        [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", editCancelRoomInfoNotificaion] object:nil];
        [self.navigationItem setLeftBarButtonItem:nil];
        [self.navigationItem setHidesBackButton:NO animated:YES];
    }
    isRoomDetailsActive=NO;
    if (aGuestInfo == nil) {
        aGuestInfo = [[GuestInfoViewV2 alloc] initWithNibName:NSStringFromClass([GuestInfoViewV2 class]) bundle:nil];
        aGuestInfo.parentVC = self;
        CGRect fr = aGuestInfo.view.frame;
        if (topBarView.isHidden) {
            fr.origin.y = 56;
        } else
            fr.origin.y = 95;
        [aGuestInfo.view setFrame:fr];
    }
    
    // set image to active guest info
    UIImage *imgGuestInfoActice=[UIImage imageNamed:highlightImage];  
    [btnGuestInfo setBackgroundImage:imgGuestInfoActice forState:UIControlStateNormal];

    // set image to display normal RoomDetail
    UIImage  *imgRoomDetail=[UIImage imageNamed:normalImage];
    [btnRoomDetail setBackgroundImage:imgRoomDetail forState:UIControlStateNormal];

    [self.aRoomDetail.view removeFromSuperview];
    [aGuestInfo.view setFrame:CGRectMake(0, 0, 320, 500)];
    
    [self.view addSubview:aGuestInfo.view];
    
    [aGuestInfo viewWillAppear:NO];
    CGRect frame = [aGuestInfo.view frame];
    if (topBarView.isHidden) {
        frame.origin.y = 56;
    } else
        frame.origin.y = 95;
    aGuestInfo.view.frame =frame;
    [self.view addSubview:aGuestInfo.view];
    //scrollView.clipsToBounds = YES;
    [self.scrollView setContentSize:CGSizeMake([scrollView bounds].size.width ,aGuestInfo.tbvGuestInfo.frame.size.height + 50)];
}

- (void)viewDidUnload
{  
    lblLocation = nil;  
    imgViewHotelLogo = nil;
    cleaningStatusView=nil;  
    houseKeeperNamelabel = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark -
#pragma mark Selector AddCLGView by push navigation

-(void) addCLGViewWithIndexSelect:(NSNotification *) selectedIndex{
    UIBarButtonItem *BackButtonRoomInfo = [[UIBarButtonItem alloc] initWithTitle: [self getBackString] style: UIBarButtonItemStyleBordered target: nil action: nil];    
    [[self navigationItem] setBackBarButtonItem: BackButtonRoomInfo];    

    
    NSString *temp = (NSString *)[selectedIndex object];
    NSInteger index = [temp intValue];
    
    if (index == 1) {     

        CountViewV2 *countview = [[CountViewV2 alloc]initWithNibName:NSStringFromClass([CountViewV2 class]) bundle:nil];
        [self.navigationController pushViewController:countview animated:YES];
        return;
    }
}

#pragma mark - Loading data
-(void)loadingData{
    //Disable sync feature guest info
    NSInteger userId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    NSInteger roomId = [RoomManagerV2 getCurrentRoomNo];
    
    if (guestInfoModel != nil) {
            if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp] == YES) {
                // show loading message
                MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
                
                [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getRaLoadingData]];
                [HUD show:YES];
                [self.tabBarController.view addSubview:HUD];
                
                //loading data
                
                SyncManagerV2 *sManager = [[SyncManagerV2 alloc] init];
                
                if(![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) 
                {
                    [HUD removeFromSuperview];
                    return;
                }
                
                if (![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) 
                {
                    [HUD removeFromSuperview];
                    return;
                }    

                //[sManager getGuestInfoWithUserID:userId roomAssignmentID:roomId AndLastModified:nil];
                
                [aRoomDetail viewWillAppear:NO];
                [HUD removeFromSuperview];
            }
    }
}

#pragma mark - Add Engineering View
-(void) addEngineeringView {
    if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp] == NO) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[[LanguageManagerV2 sharedLanguageManager] getNoWifi] message:nil delegate:nil cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getOK] otherButtonTitles: nil];
        [alert show];
        return;
    }
       
    UIBarButtonItem *BackButtonRoomInfo = [[UIBarButtonItem alloc] initWithTitle: [self getBackString] style: UIBarButtonItemStyleBordered target: nil action: nil];    
    [[self navigationItem] setBackBarButtonItem: BackButtonRoomInfo];    
    
    EngineeringCaseViewV2 *ecView = [[EngineeringCaseViewV2 alloc] init];
    [self.navigationController pushViewController:ecView animated:YES];
}

#pragma mark - Add Lost And Found View
-(void) addLostAndFoundView {
    if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp] == NO) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[[LanguageManagerV2 sharedLanguageManager] getNoWifi] message:nil delegate:nil cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getOK] otherButtonTitles: nil];
        [alert show];
        return;
    }
   
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK] style:UIBarButtonItemStyleBordered target:nil action:nil];
    self.navigationItem.backBarButtonItem = backButton;
    
    LostFoundViewV2 *lfView = [[LostFoundViewV2 alloc] initWithNibName:@"LostFoundViewV2" bundle:nil];        
    [self.navigationController pushViewController:lfView animated:YES];
}

#pragma mark - Add guideline view
- (void)addGuidelineView {
    UIBarButtonItem *BackButtonRoomInfo = [[UIBarButtonItem alloc] initWithTitle: [self getBackString] style: UIBarButtonItemStyleBordered target: nil action: nil];    
    [[self navigationItem] setBackBarButtonItem: BackButtonRoomInfo];    
   
    GuidelineViewV2* aGuidelineView = [[GuidelineViewV2 alloc] initWithNibName:@"GuidelineViewV2" bundle:nil];
    [self.navigationController pushViewController:aGuidelineView animated:YES];    
}

#pragma mark - set captions view
-(void)setCaptionsView {
    NSDictionary *dic = [[LanguageManagerV2 sharedLanguageManager] getCurrentDictionaryLanguage];
    [btnRoomDetail setTitle:[dic valueForKey:[NSString stringWithFormat:@"%@", L_ROOM_DETAIL]] forState:UIControlStateNormal];
    [btnGuestInfo setTitle:[dic valueForKey:[NSString stringWithFormat:@"%@", L_GUEST_INFO]] forState:UIControlStateNormal];
  
    UIButton *titleButton = (UIButton *)self.navigationItem.titleView;
    [titleButton setTitle:[NSString stringWithFormat:@"%@ %@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOM_TITLE], self.roomName] forState:UIControlStateNormal];

}
@end
