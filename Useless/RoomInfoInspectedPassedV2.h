//
//  RoomInfo.h
//  EHouseKeeping
//
//  Created by tms on 5/12/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GuestInfoViewV2.h"
#import "ehkDefines.h"
#import "CountViewV2.h"
#import "DateTimePickerView.h"
#import "TopbarViewV2.h"

@interface RoomInfoInspectedPassedV2 : UIViewController<cleaningStatusViewDelegate> {
    
    GuestInfoViewV2 *aGuestInfo;
   
    __unsafe_unretained IBOutlet UIButton *btnRoomDetail;
    __unsafe_unretained IBOutlet UIButton *btnGuestInfo;
    __unsafe_unretained IBOutlet UIScrollView *scrollView;
    
    NSString *roomName;
    
    //Data for RoomDetailView
    RoomModelV2 *roomModel;
    
    RoomAssignmentModelV2 *raDataModel;
    
    //Data for GuestInfoView
    GuestInfoModelV2 *guestInfoModel;
    
    BOOL isRoomDetailsActive;
    DateTimePickerView *aDateTimePickerView;
    CleaningStatusViewV2 *roomStatusView;
    
    TopbarViewV2 * topBarView;
    
    BOOL isPass;
    BOOL isInspected;
    BOOL isCompleted;
}
@property (nonatomic, strong) GuestInfoViewV2 *aGuestInfo;
@property (nonatomic, strong) DateTimePickerView *aDateTimePickerView;
@property (nonatomic, assign) UIScrollView *scrollView;
@property (nonatomic, assign) UIButton *btnRoomDetail;
@property (nonatomic, assign) UIButton *btnGuestInfo;
@property (nonatomic, strong) TopbarViewV2 *topBarView;

@property (nonatomic, strong) NSString *roomName;
@property (nonatomic, strong) RoomModelV2 *roomModel;
@property (nonatomic, strong) RoomAssignmentModelV2 *raDataModel;
@property (nonatomic, strong) GuestInfoModelV2 *guestInfoModel;
@property (nonatomic) BOOL isRoomDetailsActive;

@property (nonatomic, strong)  CleaningStatusViewV2 *roomStatusView;
@property (nonatomic) BOOL isPass;
@property (nonatomic) BOOL isInspected;
@property (nonatomic) BOOL isCompleted;

-(void) loadingData;

-(void) setCaptionsView;
-(void) showCheckList;
@end
