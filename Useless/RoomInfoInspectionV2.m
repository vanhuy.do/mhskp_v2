//
//  RoomInfo.m
//  EHouseKeeping
//
//  Created by tms on 5/12/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "RoomInfoInspectionV2.h"
#import "ehkDefines.h"
#import "RoomManagerV2.h"
#import "NetworkCheck.h"
#import "MBProgressHUD.h"
#import "EngineeringCaseViewV2.h"
#import "LostFoundViewV2.h"
#import "GuidelineViewV2.h"
#import "SyncManagerV2.h"
#import "EngineerCaseViewManagerV2.h"
#import "EngineeringCaseDetailModelV2.h"
#import "FindByRoomViewV2.h"

#define highlightImage      @"tab_count_active.png"
#define normalImage         @"tab_count_NOactive.png"
#define background          @"bg.png"
#define sizeTitle           22

@interface RoomInfoInspectionV2 (PrivateMethods)

#define tagTopbarView 1234

@end

@implementation RoomInfoInspectionV2
@synthesize aRoomDetail, aGuestInfo, scrollView;
@synthesize btnRoomDetail, btnGuestInfo;
@synthesize roomName;
@synthesize roomModel, guestInfoModel, isRoomDetailsActive;
@synthesize aDateTimePickerView;
@synthesize topBarView;
@synthesize roomStatusView;
@synthesize isPass;
@synthesize isInspected;
@synthesize isCompleted;
@synthesize raDataModel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


-(NSString *) getBackString {
    NSDictionary *dic = [[LanguageManagerV2 sharedLanguageManager] getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_BACK]];
    if (s == nil) {
        s = @"Back";
    }
    
    return s;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
    
    [topBarView setCaptionsView];
    [self.view addSubview:topBarView];
}

#pragma mark - View lifecycle
-(void)viewWillAppear:(BOOL)animated{
    // refresh data    
    
    //show wifi view
    if(!isDemoMode){
        [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationShowWifiView] object:nil];
    }
    
    //enable CheckList button on tabbarcontroller
//    UIButton *butCheckList = (UIButton *)[[self.tabBarController tabBar] viewWithTag:tagOfCheckListButton];
//    [butCheckList setEnabled:YES];
    
    //post a selectedHome notification
    //check if lower view controller is home view class, set select Home
    //else set find or other
    NSArray *viewControllers = self.navigationController.viewControllers;
    UIViewController *viewController = [viewControllers objectAtIndex:viewControllers.count - 2];
    if ([viewController isKindOfClass:[FindByRoomViewV2 class]]) {
        [[HomeViewV2 shareHomeView] setSelectedButton:tagOfFindButton];
    } else {
        [[HomeViewV2 shareHomeView] setSelectedButton:tagOfHomeButton];
    }

    if (isRoomDetailsActive == YES) {
        [self.aRoomDetail viewWillAppear:NO];
    } else {
        [self.aGuestInfo viewWillAppear:NO];
    }
        
    [super viewWillAppear:animated];
}

-(void)viewWillDisappear:(BOOL)animated {
    //Disable CheckList button on tabbarcontroller
//    UIButton *butCheckList = (UIButton *)[[self.tabBarController tabBar] viewWithTag:tagOfCheckListButton];
//    [butCheckList setEnabled:NO];
    
    [super viewWillDisappear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];    
    // Do any additional setup after loading the view from its nib.
    
    //enable flag to announce, is loading data
    [[HomeViewV2 shareHomeView] waitingLoadingData];
    
    // load screen of RoomDetail first. 
    if (aRoomDetail == nil) {
        self.aRoomDetail = [[RoomDetailInspectionViewV2 alloc] initWithNibName:@"RoomDetailInspectionViewV2" bundle:nil]; 
        
        /*********************************************************/
        aRoomDetail.isPass = self.isPass;
        aRoomDetail.isCompleted = self.isCompleted;
        aRoomDetail.isInspected = self.isInspected;
        [aRoomDetail setDelegate:self];
        /*********************************************************/
                
        aRoomDetail.superController = self;
        
        //set data for RoomDetail from roomModel
        self.aRoomDetail.dataModel = self.roomModel;
        
        //set room assignment data
        RoomAssignmentModelV2 *ramodel = [[RoomAssignmentModelV2 alloc] init];
        ramodel.roomAssignment_Id = [TasksManagerV2 getCurrentRoomAssignment];
        ramodel.roomAssignment_UserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
        [[RoomManagerV2 sharedRoomManager] load:ramodel];
        [aRoomDetail setRoomAssignmentModel:ramodel];
        
        CGRect frame = aRoomDetail.view.frame;
        frame.origin.y = 105;
        [aRoomDetail.view setFrame:frame];
    }
     
    [self.view addSubview:aRoomDetail.view];
    
    self.isRoomDetailsActive = YES;
    
    //config image for  switchview use button
    UIImage  *imgRoomDetailActive = [UIImage imageNamed:highlightImage];    
    UIImage *imgGuestInfo = [UIImage imageNamed:normalImage];
    
    //set image to button roomdetail.
    [btnRoomDetail setBackgroundImage:imgRoomDetailActive forState:UIControlStateNormal];
    [btnGuestInfo setBackgroundImage:imgGuestInfo forState:UIControlStateNormal];
    [scrollView setContentSize:CGSizeMake([scrollView bounds].size.width ,[scrollView bounds].size.height+100)];

    NSString *roomTitle = [NSString stringWithFormat:@"%@ ", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOM_NO_DETAIL]];
    roomTitle = [roomTitle stringByAppendingString:self.roomName];
    [self.navigationItem setTitle:roomTitle];
    
    [btnRoomDetail setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOM_DETAIL] forState:UIControlStateNormal];
    [btnRoomDetail setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [btnGuestInfo setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_GUEST_INFO] forState:UIControlStateNormal];
    [btnGuestInfo setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
    // addObserver to add CLGView
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addCLGViewWithIndexSelect:) name:@"AddCLGWithSelectedIndex" object:nil];
    
     [[NSNotificationCenter defaultCenter] addObserver: self selector:@selector(openRoomStatusView) name:@"openRoomStatusView" object:nil];
        
    //addObserver to add EngineeringView
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addEngineeringView) name:[NSString stringWithFormat:@"%@", notificationAddEngineeringView] object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addLostAndFoundView) name:[NSString stringWithFormat:@"%@", notificationAddLostAndFoundView] object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addGuidelineView) name:[NSString stringWithFormat:@"%@", notificationGuidelineView] object:nil];

    [RoomManagerV2 setCurrentRoomNo:roomModel.room_Id];
      
    // chinh X.Bui add this line code
    if (topBarView == nil) {
        topBarView = [[TopbarViewV2 alloc] initViewWithSuperModel:[[SuperRoomModelV2 alloc] init]];
        [self.view addSubview:topBarView];
    }
    
    /************ add a button as title view of navigation bar **********/
    UIButton *titleBarButton = [[UIButton alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
    titleBarButton.titleLabel.backgroundColor = [UIColor clearColor];
    [titleBarButton.titleLabel setFont:FONT_BUTTON_TOPBAR];
    titleBarButton.titleLabel.textAlignment = UITextAlignmentCenter;
    titleBarButton.titleLabel.textColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:1.0];
    [titleBarButton.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
    [titleBarButton.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
    [titleBarButton addTarget:self action:@selector(hiddenHeaderView) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.titleView = titleBarButton;

    //Back button
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *img = [UIImage imageNamed:@"but_back.png"];
    [button setFrame:CGRectMake(0, 0, img.size.width, img.size.height)];
    [button setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK] forState:UIControlStateNormal];
    [button setBackgroundImage:img forState:UIControlStateNormal];
    [button addTarget:self action:@selector(backBarPressed) forControlEvents:UIControlEventTouchUpInside];
    [button.titleLabel setFont:[UIFont boldSystemFontOfSize:12.0]];
    [button setContentEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = barButtonItem;
    
    //set captions for view
    [self setCaptionsView];

    /*************check enable or disable Guestinfo*********************/
    if(raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VD || raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VC || 
       raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VI || raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OOO ||
       raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OOS){
        [btnGuestInfo setBackgroundImage:[UIImage imageNamed:@"top_bt_grey_240x58.png"] forState:UIControlStateNormal];
        [btnGuestInfo setUserInteractionEnabled:NO];
    }
        
    if (roomStatusView == nil) {
        roomStatusView = [[CleaningStatusViewV2 alloc] initWithNibName:@"CleaningStatusViewV2" bundle:nil];
    }
    
    //end loading data
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
}

-(void)backBarPressed {
    if (isRoomDetailsActive == NO) {
        if ([aGuestInfo checkSaveGuestInfo] == YES) {
            //YES, no need save guest info
        } else {
            //NO, did show alert in guest info
            return;
        }
        
        if ([aRoomDetail isNotSavedAndExit] == YES) {
            
        } else {
            [self.navigationController popViewControllerAnimated:YES];
        }        
    } else {
        if([aRoomDetail isNotSavedAndExit] == NO){
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

#pragma mark - === Handle Check Save Data ===
#pragma mark
-(BOOL) checkSaveDataWithTagEvent:(NSInteger) tagOfEvent {
    //handle user pressed on check list
//    if (tagOfEvent == tagOfCheckListButton) {
//        
//        return NO;
//    }
    
    if (tagOfEvent == tagOfMessageButton) {
        //can move to message screen but must set message
        if ([aRoomDetail currentCountTimeStatusInspection] == START || aRoomDetail.isSaved == NO) {
            [[CommonVariable sharedCommonVariable] setRoomIsCompleted:0];
        } else {
            [[CommonVariable sharedCommonVariable] setRoomIsCompleted:1];
        }
        
        return NO;
    }
    
    if (isRoomDetailsActive == NO) {
        if ([aGuestInfo checkSaveGuestInfo] == YES) {
            //YES
        } else {
            return YES;
        }
        
        if ([aRoomDetail isNotSavedAndExit] == YES) {
            
            return YES;
        }
        
        return NO;
    } else {
        if ([aRoomDetail isNotSavedAndExit] == NO) {
            
            //NO
            return NO; //no need to save before leaving
        } else {
            //YES
            //need to save before leaving
            
            return YES;
        }
    }
}

#pragma mark
#pragma mark hidden topbar
-(void)hiddenHeaderView {    
    BOOL isHidden = topBarView.hidden;
    [topBarView setHidden:!isHidden];
    if(isHidden) {        
        [btnRoomDetail setFrame:CGRectMake(0, 56, 168, 37)];
        [btnGuestInfo setFrame:CGRectMake(166, 56, 154, 37)];
        [scrollView setFrame:CGRectMake(0,56, 319, 327)];
        CGRect frame = aRoomDetail.view.frame;
        frame.origin.y = 104;
        [aRoomDetail.view setFrame:frame];
        
        frame = aGuestInfo.view.frame;
        frame.origin.y = 104;
        [aGuestInfo.view setFrame:frame];
    } else {       
        [btnRoomDetail setFrame:CGRectMake(0, 0, 168, 37)];
        [btnGuestInfo setFrame:CGRectMake(166, 0, 154, 37)];
        [scrollView setFrame:CGRectMake(0,56, 319, 327)];
        CGRect frame = aRoomDetail.view.frame;
        frame.origin.y = 56;
        [aRoomDetail.view setFrame:frame];
       
        frame = aGuestInfo.view.frame;
        frame.origin.y = 56;
        [aGuestInfo.view setFrame:frame];
    }
}

-(void)backToRoomAssignment {
    [self.view removeFromSuperview];
}

-(void)openViewPicker {
    [[self navigationController] setNavigationBarHidden:YES animated:NO];
     aDateTimePickerView=[[DateTimePickerView alloc] initWithNibName:@"DateTimePickerView" bundle:nil];
    [self.view addSubview:aDateTimePickerView.view];
}

#pragma mark
#pragma mark Save data
-(void)saveRoomDetail {
    [aRoomDetail saveData];
}

-(void)saveData {    
    if(isRoomDetailsActive == YES) {
        [self saveRoomDetail];
        
        //save guest info if needed
        if (aGuestInfo.isSaved == NO) {
            [aGuestInfo saveDataWithoutHUD];
        }
    } else {
        [aGuestInfo saveData];
    }
}

-(BOOL)isNotSavedAndExit {
    return [aRoomDetail isNotSavedAndExit];
}

#pragma mark
#pragma mark roomDetail Delegate
-(void)showPopupRoomStatus:(UIViewController *)controller{
    if (roomStatusView == nil) {
        roomStatusView = [[CleaningStatusViewV2 alloc] initWithNibName:@"CleaningStatusViewV2" bundle:nil];
    }
    
    if(raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OC || raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OD ||
        raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OOO || raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OI||
       raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OOS || raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OPU){
        
        roomStatusView.isOccupied = YES;
    } else{
        roomStatusView.isOccupied = NO;
    }
        
    [roomStatusView setDelegate:self];
    roomStatusView.title = [NSString stringWithFormat:@"%@ %@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOM_TITLE], self.roomName];
    roomStatusView.typeStatus = ENUM_ROOM_STATUS;
    
    UIBarButtonItem *BackButtonRoomInfo = [[UIBarButtonItem alloc] initWithTitle: [self getBackString] style: UIBarButtonItemStyleBordered target: nil action: nil];    
    [[self navigationItem] setBackBarButtonItem: BackButtonRoomInfo];    
    
    [self.navigationItem.backBarButtonItem setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK]];
    [self.navigationController pushViewController:roomStatusView animated:YES];
}

// open room status
-(void) openRoomStatusView {
    if (roomStatusView == nil) {
        roomStatusView = [[CleaningStatusViewV2 alloc] initWithNibName:@"CleaningStatusViewV2" bundle:nil];
    }
    [roomStatusView setDelegate:self];
    roomStatusView.title = self.navigationItem.title;
    roomStatusView.typeStatus = ENUM_ROOM_STATUS;
    
    UIBarButtonItem *BackButtonRoomInfo = [[UIBarButtonItem alloc] initWithTitle: [self getBackString] style: UIBarButtonItemStyleBordered target: nil action: nil];    
    [[self navigationItem] setBackBarButtonItem: BackButtonRoomInfo];    
    
    [self.navigationItem.backBarButtonItem setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK]];
    [self.navigationController pushViewController:roomStatusView animated:YES];
}

// delegate get cleaning status
-(void)cleaningStatus:(NSInteger)cleaningStatus isRoomStatus:(BOOL)isRoomStatus withViewController:(UIViewController *)controller{
    [controller.navigationController popViewControllerAnimated:NO];
    
    if(isRoomStatus){
        [aRoomDetail roomStatusDidSelect:cleaningStatus];
    } else{
//        [aRoomDetail cleaningStatusDidSelect:cleaningStatus];
    }
}

-(void)cleaningStatus:(NSInteger)cleaningStatus isRoomStatus:(BOOL)isRoomStatus withTime:(NSDate *)date withViewController:(UIViewController *)controller{
    [controller.navigationController popViewControllerAnimated:NO];
    
    if(!isRoomStatus){
//        [aRoomDetail cleaningStatusDidSelect:cleaningStatus andDate:date];
    } 
}

-(void)removePickerViewGuestInfo:(NSNotification *)notification {
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    
    [self.aDateTimePickerView.view removeFromSuperview];
    
    // post this data notification to guestinfo.
    NSString *dataDateGuestInfo=(NSString*)[notification object];
    [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@",returnDateTimePickerNotification]  object:dataDateGuestInfo];
    [self.navigationItem setHidesBackButton:NO];
    [self.navigationItem setHidesBackButton:YES];
}

#pragma mark-
#pragma event switch view room detail and guest info
- (IBAction)openRoomDetail:(id)sender {
    [[self navigationItem] setRightBarButtonItem:nil];
    isRoomDetailsActive = YES;
 
    if(aRoomDetail == nil) {
        aRoomDetail=[[RoomDetailInspectionViewV2 alloc] initWithNibName:@"RoomDetailInspectionViewV2" bundle:nil];
        aRoomDetail.superController = self;
        [[RoomManagerV2 sharedRoomManager] loadRoomModel:self.roomModel];
        aRoomDetail.dataModel = self.roomModel;

        //set room assignment data
        RoomAssignmentModelV2 *ramodel = [[RoomAssignmentModelV2 alloc] init];
        ramodel.roomAssignment_Id = [TasksManagerV2 getCurrentRoomAssignment];
        ramodel.roomAssignment_UserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
        [[RoomManagerV2 sharedRoomManager] load:ramodel];
        [aRoomDetail setRoomAssignmentModel:ramodel];
    }
    
    UIImage *imgRoomDetailActive = [UIImage imageNamed:highlightImage];
    [btnRoomDetail setBackgroundImage:imgRoomDetailActive forState:UIControlStateNormal];
    
    if (btnGuestInfo.userInteractionEnabled == YES) {
        // set state of tabview guest info
        UIImage *imgGuestInfo=[UIImage imageNamed:normalImage];
        [btnGuestInfo setBackgroundImage:imgGuestInfo forState:UIControlStateNormal];
    }

    if (self.aGuestInfo.statusEditing == YES) {
        [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", editCancelRoomInfoNotificaion] object:nil];
        [self.navigationItem setLeftBarButtonItem:nil];
        [self.navigationItem setHidesBackButton:NO animated:YES];
    }
    
    [self.aGuestInfo.view removeFromSuperview];    
    [self.view addSubview:aRoomDetail.view];
        
    scrollView.clipsToBounds = YES;
    [scrollView setContentSize:CGSizeMake([scrollView bounds].size.width ,[scrollView bounds].size.height+50)];
    
    if(raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VD || raDataModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VC){
        [btnGuestInfo setBackgroundImage:[UIImage imageNamed:@"top_bt_grey_240x58.png"] forState:UIControlStateNormal];
        [btnGuestInfo setUserInteractionEnabled:NO];
    }
}

- (IBAction)openGuestInfo:(id)sender {
    if (self.aGuestInfo.statusEditing == YES) {
        [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", editCancelRoomInfoNotificaion] object:nil];
        [self.navigationItem setLeftBarButtonItem:nil];
        [self.navigationItem setHidesBackButton:NO animated:YES];
    }
    
    isRoomDetailsActive = NO;
    if (aGuestInfo == nil) {
        aGuestInfo = [[GuestInfoViewV2 alloc] initWithNibName:NSStringFromClass([GuestInfoViewV2 class]) bundle:nil];
        aGuestInfo.parentVC = self;
        aGuestInfo.roomNumber = raDataModel.roomAssignment_RoomId;
        
        CGRect fr = aGuestInfo.view.frame;
        if (topBarView.isHidden) {
            fr.origin.y = 56;
        } else
            fr.origin.y = 95;
        [aGuestInfo.view setFrame:fr];
    }
    
    //set parent view controller for show hud
    aGuestInfo.parentViewController = self;
    
    // set image to active guest info
    UIImage *imgGuestInfoActice=[UIImage imageNamed:highlightImage];  
    [btnGuestInfo setBackgroundImage:imgGuestInfoActice forState:UIControlStateNormal];

    // set image to display normal RoomDetail
    UIImage  *imgRoomDetail=[UIImage imageNamed:normalImage];
    [btnRoomDetail setBackgroundImage:imgRoomDetail forState:UIControlStateNormal];

    [self.aRoomDetail.view removeFromSuperview];
    [aGuestInfo.view setFrame:CGRectMake(0, 0, 320, 500)];
        
    [self.view addSubview:aGuestInfo.view];
    [aGuestInfo reloadView];
    
    [aGuestInfo viewWillAppear:NO];
    CGRect frame = [aGuestInfo.view frame];
    if (topBarView.isHidden) {
        frame.origin.y = 56;
    } else
        frame.origin.y = 95;
    
    aGuestInfo.view.frame =frame;
    [self.view addSubview:aGuestInfo.view];
    
    [self.scrollView setContentSize:CGSizeMake([scrollView bounds].size.width ,aGuestInfo.tbvGuestInfo.frame.size.height + 50)];
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark -
#pragma mark Selector AddCLGView by push navigation
-(void) addCLGViewWithIndexSelect:(NSNotification *) selectedIndex{
    UIBarButtonItem *BackButtonRoomInfo = [[UIBarButtonItem alloc] initWithTitle: [self getBackString] style: UIBarButtonItemStyleBordered target: nil action: nil];    
    [[self navigationItem] setBackBarButtonItem: BackButtonRoomInfo];    
    
    NSString *temp = (NSString *)[selectedIndex object];
    NSInteger index = [temp intValue];
    
    if (index == 1) {        
        CountViewV2 *countview = [[CountViewV2 alloc]initWithNibName:NSStringFromClass([CountViewV2 class]) bundle:nil];
        [self.navigationController pushViewController:countview animated:YES];
       
        return;        
    }
 }

#pragma mark - Loading data
-(void)loadingData{
    //Disable sync feature guest info
    NSInteger userId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    NSInteger roomId = [RoomManagerV2 getCurrentRoomNo];
    
    if (guestInfoModel != nil) {
        if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp] == YES) {
            // show loading message
            MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
            
            [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getRaLoadingData]];
            [HUD show:YES];
            [self.tabBarController.view addSubview:HUD];
            
            //loading data
            
            SyncManagerV2 *sManager = [[SyncManagerV2 alloc] init];
            
            if(![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
                [HUD removeFromSuperview];
                return;
            }
            
            if (![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
                [HUD removeFromSuperview];
                return;
            }    
            
            //[sManager getGuestInfoWithUserID:userId roomAssignmentID:roomId AndLastModified:nil];
            
            [aRoomDetail viewWillAppear:NO];
            [HUD removeFromSuperview];
        }
    }
}

#pragma mark - Add Engineering View
-(void) addEngineeringView {
    UIBarButtonItem *BackButtonRoomInfo = [[UIBarButtonItem alloc] initWithTitle: [self getBackString] style: UIBarButtonItemStyleBordered target: nil action: nil];    
    [[self navigationItem] setBackBarButtonItem: BackButtonRoomInfo];    
    
    EngineeringCaseViewV2 *ecView = [[EngineeringCaseViewV2 alloc] init];
    [self.navigationController pushViewController:ecView animated:YES];
}

#pragma mark - Add Lost And Found View
-(void) addLostAndFoundView {    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK] style:UIBarButtonItemStyleBordered target:nil action:nil];
    self.navigationItem.backBarButtonItem = backButton;
        
    LostFoundViewV2 *lfView = [[LostFoundViewV2 alloc] initWithNibName:@"LostFoundViewV2" bundle:nil];
    [self.navigationController pushViewController:lfView animated:YES];
}

#pragma mark - Add guideline view
- (void)addGuidelineView {
    UIBarButtonItem *BackButtonRoomInfo = [[UIBarButtonItem alloc] initWithTitle: [self getBackString] style: UIBarButtonItemStyleBordered target: nil action: nil];    
    [[self navigationItem] setBackBarButtonItem: BackButtonRoomInfo];    
   
    GuidelineViewV2* aGuidelineView = [[GuidelineViewV2 alloc] initWithNibName:@"GuidelineViewV2" bundle:nil];
    [self.navigationController pushViewController:aGuidelineView animated:YES];    
}

#pragma mark - set captions view
-(void)setCaptionsView {
    NSDictionary *dic = [[LanguageManagerV2 sharedLanguageManager] getCurrentDictionaryLanguage];
    [btnRoomDetail setTitle:[dic valueForKey:[NSString stringWithFormat:@"%@", L_ROOM_DETAIL]] forState:UIControlStateNormal];
    [btnGuestInfo setTitle:[dic valueForKey:[NSString stringWithFormat:@"%@", L_GUEST_INFO]] forState:UIControlStateNormal];
  
    UIButton *titleButton = (UIButton *)self.navigationItem.titleView;
    [titleButton setTitle:[NSString stringWithFormat:@"%@ %@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOM_TITLE], self.roomName] forState:UIControlStateNormal];
}

#pragma mark - === Show Check list ===
#pragma mark
-(void)showCheckList {    
    //call function add correct check list view
    [aRoomDetail butCheckListPressed:nil];
}

@end
