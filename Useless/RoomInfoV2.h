//
//  RoomInfo.h
//  EHouseKeeping
//
//  Created by tms on 5/12/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GuestInfoViewV2.h"
//#import "CLGView.h"
#import "ehkDefines.h"
#import "CountViewV2.h"
#import "DateTimePickerView.h"
#import "TopbarViewV2.h"

@interface RoomInfoV2 : UIViewController<cleaningStatusViewDelegate> {
    
    GuestInfoViewV2 *aGuestInfo;
    
    IBOutlet UINavigationBar *navigationBar;
    
    IBOutlet UINavigationItem *anavigationItem;
   
    IBOutlet UIButton *btnRoomDetail;
    IBOutlet UIButton *btnGuestInfo;
    IBOutlet UIScrollView *scrollView;
    IBOutlet UILabel *lblLocation;
    IBOutlet UIImageView *imgViewHotelLogo;
    
    NSString *roomName;
    
    // variable CLGView
//    CLGView *clgView;
    
    //Data for RoomDetailView
    RoomModelV2 *roomModel;
    
    //Data for GuestInfoView
    GuestInfoModelV2 *guestInfoModel;
    
    BOOL isRoomDetailsActive;
    DateTimePickerView *aDateTimePickerView;
    CleaningStatusViewV2 *cleaningStatusView;
    CleaningStatusViewV2 *roomStatusView;
    
    IBOutlet UILabel *houseKeeperNamelabel;
    TopbarViewV2 * topBarView;
    
    BOOL isPass;
    BOOL isInspected;
    BOOL isCompleted;
}
@property (nonatomic,strong) UILabel *lblLocation;
@property (nonatomic, strong) GuestInfoViewV2 *aGuestInfo;
@property (nonatomic,strong)  DateTimePickerView *aDateTimePickerView;
@property (nonatomic,strong) IBOutlet UINavigationBar *navigationBar;
@property (nonatomic,strong) IBOutlet UISegmentedControl *segmentedSwitch;
@property (nonatomic,strong) IBOutlet UINavigationItem *anavigationItem;
@property (nonatomic,strong) IBOutlet UIScrollView *scrollView;
@property (nonatomic,strong) IBOutlet UIButton *btnRoomDetail;
@property (nonatomic ,strong) IBOutlet UIButton *btnGuestInfo;
@property (nonatomic , strong)  TopbarViewV2 *topBarView;

@property (nonatomic,strong) NSString *roomName;
@property (nonatomic,strong) RoomModelV2 *roomModel;
@property (nonatomic,strong) GuestInfoModelV2 *guestInfoModel;
@property (nonatomic) BOOL isRoomDetailsActive;

@property (nonatomic ,strong) CleaningStatusViewV2 *cleaningStatusView;
@property (nonatomic,strong)  CleaningStatusViewV2 *roomStatusView;
@property (nonatomic) BOOL isPass;
@property (nonatomic) BOOL isInspected;
@property (nonatomic) BOOL isCompleted;

-(void) loadingData;

-(void) setCaptionsView;
@end
