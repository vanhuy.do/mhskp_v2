//
//  SectionCheckListContentTypeView.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol SectionCheckListContentTypeViewDelegate;

@interface SectionCheckListContentTypeView : UIView{
    UILabel *lblContentName;
    UILabel *lblContentDescription;
    UIImageView *imgArrowSection;
    id<SectionCheckListContentTypeViewDelegate> delegate;
    UIButton *btnToggle;
    BOOL isToggle;
    NSInteger section;
}

@property (nonatomic, assign) BOOL isToggle;
@property (nonatomic, assign) NSInteger section;
@property (nonatomic, retain)  UILabel *lblContentName;
@property (nonatomic, retain)  UILabel *lblContentDescription;
@property (nonatomic, retain)  UIImageView *imgArrowSection;
@property (nonatomic, assign) id<SectionCheckListContentTypeViewDelegate> delegate;

-(id) initWithSection:(NSInteger) sectionIndex contentName:(NSString *)content AndStatusArrow:(BOOL)isOpened;
-(void)toggleOpenWithUserAction:(BOOL) userAction;
@end

@protocol SectionCheckListContentTypeViewDelegate <NSObject>

@optional
-(void) sectionHeaderView:(SectionCheckListContentTypeView*) sectionheaderView sectionOpened:(NSInteger) section;
-(void) sectionHeaderView:(SectionCheckListContentTypeView*) sectionheaderView sectionClosed:(NSInteger) section;

@end