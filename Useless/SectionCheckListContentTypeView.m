//
//  SectionCheckListContentTypeView.m
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SectionCheckListContentTypeView.h"

@interface SectionCheckListContentTypeView (PrivateMethods)

-(void) toggleOpen:(id)sender;

@end

@implementation SectionCheckListContentTypeView
@synthesize lblContentName,lblContentDescription,imgArrowSection,delegate, section, isToggle;


-(id)init {
    self = [super init];
    if (self) {
        [self setFrame:CGRectMake(0, 0, 320, 50)];
        lblContentName = [[UILabel alloc] initWithFrame:CGRectMake(20, 4, 196, 39)];
        [lblContentName setBackgroundColor:[UIColor clearColor]];
        [lblContentName setTextColor:[UIColor colorWithRed:255 green:255 blue:255 alpha:1]];
        [lblContentName setFont:[UIFont fontWithName:@"Arial-BoldMT" size:15]];
        [self setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:imgBackgroundSection]]];
        if(isToggle == YES)
        {
            imgArrowSection = [[UIImageView alloc]initWithImage:[UIImage imageNamed:imgRightRowOpen]];
        }
        else
        {
            imgArrowSection = [[UIImageView alloc]initWithImage:[UIImage imageNamed:imgRightRow]];
        }
        
        [imgArrowSection setFrame:CGRectMake(288, 12, 25, 25)];
        btnToggle = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnToggle setFrame:CGRectMake(0, 0, 320, 50)];
        [btnToggle addTarget:self action:@selector(toggleOpen:) forControlEvents:UIControlEventTouchUpInside];
                [self addSubview:lblContentName];
        [self addSubview:imgArrowSection];
        [self addSubview:btnToggle];
    }
    return self;
}

-(id)initWithSection:(NSInteger)sectionIndex contentName:(NSString *)content AndStatusArrow:(BOOL)isOpened {
    self = [self init];
    if (self) {
        section = sectionIndex;
        lblContentName.text = content;
        isToggle = isOpened;
    }
    return self;
}

- (void)dealloc {
    [lblContentName release];
    [imgArrowSection release];
    [super dealloc];
}
#pragma mark - View lifecycle
    
/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
}
 */


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void)toggleOpen:(id)sender {
    [self toggleOpenWithUserAction:YES];
}

-(void)toggleOpenWithUserAction:(BOOL)userAction {
    if (userAction) {
        if (isToggle == NO) {
            if ([delegate respondsToSelector:@selector(sectionHeaderView:sectionOpened:)]) {
                isToggle = YES;
                [delegate sectionHeaderView:self sectionOpened:section];
                
                [imgArrowSection setImage:[UIImage imageNamed:imgRightRowOpen]];
            }
        }else {
            if ([delegate respondsToSelector:@selector(sectionHeaderView:sectionClosed:)]) {
                isToggle = NO;
                [imgArrowSection setImage:[UIImage imageNamed:imgRightRow]];
                [delegate sectionHeaderView:self sectionClosed:section];
            }
        }
    }
}


@end
