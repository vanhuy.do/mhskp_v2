//
//  CommonLib.h
//  FCS m-Housekeeping
//
//  Created by Christopher Felix on 22-Sep-2016.
//  Copyright (c) 2016 FCS Computer Systems. All rights reserved.
//

#define AlertDefaultFileNameSound @"mhskpdefault"
#define AlertDefaultFileSoundExtension @"mp3"

@interface CommonLib : NSObject {
    AVAudioPlayer *lAVAudioPlayer;
}

// MARK: Static Methods
+ (CommonLib*) sharedCommonLib;


// MARK: Non-static Method
- (void) playRingtone;

@end