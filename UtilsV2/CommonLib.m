//
//  CommonLib.m
//  FCS m-Housekeeping
//
//  Created by Christopher Felix on 22-Sep-2016.
//  Copyright (c) 2016 FCS Computer Systems. All rights reserved.
//

#import "CommonLib.h"

@implementation CommonLib

static CommonLib* sharedCommonLibInstance = nil;

//MARK: Static Methods
+ (CommonLib*) sharedCommonLib
{
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedCommonLibInstance = [[CommonLib alloc] init];
    });
    
    return sharedCommonLibInstance;
}

- (void) playRingtone {
    
    @try {
        if (!lAVAudioPlayer) {
            NSURL* musicFile = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:AlertDefaultFileNameSound ofType:AlertDefaultFileSoundExtension]];
            lAVAudioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:musicFile error:nil];
        }
        //Max volume
        [lAVAudioPlayer setVolume:1.0f];
        [lAVAudioPlayer setNumberOfLoops:1];
        [lAVAudioPlayer play];
    }
    @catch (NSException * e) {
        NSLog(@"E>CommonLib.m>playRingtone>%@", e.name);
    }
    @finally {
        // if (lAVAudioPlayer != nil) lAVAudioPlayer = nil;
    }
}

@end