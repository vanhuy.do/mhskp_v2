//
//  CommonVariable.h
//  mHouseKeeping
//
//  Created by TMS on 3/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SuperRoomModelV2.h"
//#import "LaudryOrderModelV2.h"
#import "AccessRight.h"

//#warning dthanhson edit for conrad

#define noPauseRoom                 -1
#define greaterThanOnePauseRoom     -2
#define unlimitedPauseRoom           0  //Hao Tran Add

//#warning end

@interface CommonVariable : NSObject {
    SuperRoomModelV2* superRoomModel;
    bool isSaveSuccessfull;
    bool isNotDiscardAllChange;
    bool isBackFromChk;
    bool isBackFromPosting;
    NSInteger roomIsCompleted;
    NSInteger isFromHome; // indicate user tap on message from Home
    //LaudryOrderModelV2 *lr_order_Model;
}
//@property (nonatomic,strong) LaudryOrderModelV2 *lr_order_Model;
@property (nonatomic, strong) SuperRoomModelV2* superRoomModel;
@property bool isSaveSuccessfull;
@property bool isNotDiscardAllChange;
@property bool  isBackFromChk;
@property bool  isBackFromPosting;
@property NSInteger roomIsCompleted;  // ==1 : started , == 2 completed
@property NSInteger isFromHome; // 1: is from home and room not completed, otherwise 0.
@property (nonatomic, assign) NSInteger currentPauseRoom;

//Hao Tran - Count rooms are cleaning or inspecting by current user
@property (nonatomic, assign) int currentStartedRoom;
// Dung Phan - Store previous started room for LogInvalidStartRoom
@property (nonatomic, assign) int prevStartedRoomID;
@property (nonatomic, assign) NSString* prevStartedRoomNo;

+ (CommonVariable*) sharedCommonVariable;

-(AccessRight *) parse:(NSInteger) value;
@end
