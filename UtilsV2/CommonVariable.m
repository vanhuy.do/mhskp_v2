//
//  CommonVariable.m
//  mHouseKeeping
//
//  Created by TMS on 3/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CommonVariable.h"

@implementation CommonVariable
@synthesize superRoomModel;
@synthesize isSaveSuccessfull;
@synthesize isNotDiscardAllChange;
@synthesize isBackFromChk;
@synthesize isBackFromPosting;
//@synthesize lr_order_Model;
@synthesize roomIsCompleted;
@synthesize isFromHome;
@synthesize currentPauseRoom;
//Hao Tran - Count rooms are cleaning or inspecting by current user
@synthesize currentStartedRoom;
// Dung Phan - Store previous started room for LogInvalidStartRoom
@synthesize prevStartedRoomID;
@synthesize prevStartedRoomNo;
static CommonVariable* sharedCommonVariableInstance = nil;
static NSString *currentLang = nil;

-(void)setCurrentPauseRoom:(NSInteger)pauseRoomValue
{
    //Check if access right unlimited pause room
    if([[UserManagerV2 sharedUserManager].currentUserAccessRight countTimesPauseRoom] == unlimitedPauseRoom){
        currentPauseRoom = noPauseRoom;
        return;
    }
    
    currentPauseRoom = pauseRoomValue;
}

+ (CommonVariable*) sharedCommonVariable
{
//	if (sharedCommonVariableInstance == nil) {
//        currentLang = [NSString string];
//        sharedCommonVariableInstance = [[super allocWithZone:NULL] init];
//    }
//    return sharedCommonVariableInstance;	
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        currentLang = [NSString string];
        sharedCommonVariableInstance = [[CommonVariable alloc] init];
        // Do any other initialisation stuff here
    });
    return sharedCommonVariableInstance;
}

-(AccessRight *) parse:(NSInteger) value
{
    AccessRight *accessRight = [[AccessRight alloc] init];
    unsigned int isAllowedView = (value >> 2) & (1UL);
    unsigned int isAllowedEdit = (value >> 1) & (1UL);
    unsigned int isActive = (value >> 0) & (1UL);
    
    accessRight.isAllowedView = (isAllowedView == 1);
    accessRight.isALlowedEdit = (isAllowedEdit == 1);
    accessRight.isActive = (isActive == 1);
    
    return accessRight;
}
/*
+ (id)allocWithZone:(NSZone *)zone
{
    return [[self sharedCommonVariable] retain];
}

- (id)copyWithZone:(NSZone *)zone
{
    return self;
}

- (id)retain
{
    return self;
}

- (NSUInteger)retainCount
{
    return NSUIntegerMax;  //denotes an object that cannot be released
}

`
- (id)autorelease
{
    return self;
}
*/
@end
