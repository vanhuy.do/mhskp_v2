//
//  CustomAlertViewV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 3/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomAlertViewV2 : UIAlertView {
    NSMutableArray *buttonStrings;
    BOOL didLayout;
}

@property (nonatomic, strong) NSMutableArray *buttonStrings;

-(id)initWithArrayButtons:(NSArray *) images title:(NSString *)title message:(NSString *)message delegate:(id)delegate cancelButtonTitle:(NSString *)cancelButtonTitle otherButtonTitles:(NSString *)otherButtonTitles, ...;

@end
