//
//  CustomAlertViewV2.m
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 3/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CustomAlertViewV2.h"
#import "DeviceManager.h"

#define tagImage 111

@implementation CustomAlertViewV2
@synthesize buttonStrings;

-(id)initWithArrayButtons:(NSArray *)images title:(NSString *)title message:(NSString *)message delegate:(id)delegate cancelButtonTitle:(NSString *)cancelButtonTitle otherButtonTitles:(NSString *)otherButtonTitles, ... {
    if (self = [super initWithTitle:title message:message delegate:delegate cancelButtonTitle:cancelButtonTitle otherButtonTitles: nil]) {
        va_list args;
        va_start(args, otherButtonTitles);
        self.buttonStrings = [NSMutableArray array];
        [buttonStrings addObject:cancelButtonTitle == nil ? @"" : cancelButtonTitle];
        for (NSString *anOtherButtonTitles = otherButtonTitles; anOtherButtonTitles != nil; anOtherButtonTitles = va_arg(args, NSString*)){
            [buttonStrings addObject:anOtherButtonTitles];
            [self addButtonWithTitle:anOtherButtonTitles];
        }
        
        NSArray *subviews = self.subviews;
        NSInteger countButton = 0;
        for (id subview in subviews) {
            if (![subview isKindOfClass:[UILabel class]] && ![subview isKindOfClass:[UIImageView class]]) {
                UIImage *img = nil;
//                img = [UIImage imageNamed:@"yes_bt.png"];
                img = [images objectAtIndex:countButton];
                UIImageView *imgView = [[UIImageView alloc] initWithImage:img];
                [imgView setFrame:CGRectMake(-5, -5, img.size.width / 2.0, img.size.height / 2.0)];
                [imgView setTag:tagImage];
                
                if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                    
                } else {
                    [subview insertSubview:imgView atIndex:1];
                    [subview sendSubviewToBack:imgView];
                }
                
                if ([subview isKindOfClass:NSClassFromString(@"UIThreePartButton")] ) {
                    
                    UILabel *theTitle = [[UILabel alloc] init];
                    [theTitle setFont:[UIFont fontWithName:@"Helvetica-Bold" size:18.0]];
                    [theTitle setTextColor:[UIColor whiteColor]];
                    [theTitle setShadowColor:[UIColor blackColor]];
                    [theTitle setShadowOffset:CGSizeMake(0, -1)];
                    
                    CGRect newFrame = theTitle.frame;
                    newFrame.origin.x = 0 + 5;
                    newFrame.origin.y = 0;
                    
                    if (countButton == 1) {
                        newFrame.origin.x += 5;
                    }
                    
                    newFrame.size.height = imgView.frame.size.height;
                    newFrame.size.width = imgView.frame.size.width;
                    [theTitle setFrame:newFrame];
                    
                    [theTitle setBackgroundColor:[UIColor clearColor]];             
                    [theTitle setTextAlignment:NSTextAlignmentCenter];
                    [imgView addSubview:theTitle];
                    [theTitle setText:[buttonStrings objectAtIndex:countButton]];
                    
                } else {
                    
                    if ([[[subview class] description] isEqualToString:@"UIAlertButton"]) {
                        UILabel *theTitle = [[UILabel alloc] init];
                        [theTitle setFont:[UIFont fontWithName:@"Helvetica-Bold" size:18.0]];
                        [theTitle setTextColor:[UIColor whiteColor]];
                        [theTitle setShadowColor:[UIColor blackColor]];
                        [theTitle setShadowOffset:CGSizeMake(0, -1)];
                        
                        CGRect newFrame = theTitle.frame;
                        newFrame.origin.x = 0 + 5;
                        
                        if (countButton == 1) {
                            newFrame.origin.x += 5;
                        }
                        
                        newFrame.origin.y = 0;
                        newFrame.size.height = imgView.frame.size.height;
                        newFrame.size.width = imgView.frame.size.width;
                        [theTitle setFrame:newFrame];
                        
                        [theTitle setBackgroundColor:[UIColor clearColor]];             
                        [theTitle setTextAlignment:NSTextAlignmentCenter];
                        [imgView addSubview:theTitle];
                        [theTitle setText:[buttonStrings objectAtIndex:countButton]];
                        
                        [subview bringSubviewToFront:imgView];
                    }
                    
                }

                countButton ++;
            }
        }
        didLayout = NO;
    }
    return self;
}

-(void)layoutSubviews {
    if (didLayout == NO) {
        didLayout = YES;
        NSArray *subviews = self.subviews;
        for (id subview in subviews) {
            if (![subview isKindOfClass:[UILabel class]] && ![subview isKindOfClass:[UIImageView class]]) {
                if (buttonStrings.count == 2) {
                    UIImageView *imgView = (UIImageView *)[subview viewWithTag:tagImage];
                    CGRect f = imgView.frame;
                    f.origin.x = -3;
                    f.origin.y = -3;
                    [imgView setFrame:f];
                    continue;
                }
                if (buttonStrings.count > 2) {
                    continue;
                }
                UIImageView *imgView = (UIImageView *)[subview viewWithTag:tagImage];
                CGRect frame = [(id)subview frame];
                [subview setFrame:CGRectMake(([(id)subview superview].frame.size.width - imgView.frame.size.width)/2.0 /*- frame.origin.x*/, frame.origin.y, imgView.frame.size.width - 7, imgView.frame.size.height - 10)];
            }
        }
    } else {
        //do nothing
    }
}

@end
