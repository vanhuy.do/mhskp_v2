//
//  CheckListDetailContentCellV2.h
//  mHouseKeeping
//
//  Created by TMS on 2/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CheckListDetailContentCellV2Delegate <NSObject>

@optional
-(void) btnDetailContentRatingPressedWithIndexPath:(NSIndexPath *)indexpath AndSender:(UIButton *) sender;
-(void) btnDetailContentCheckmarkPressedWithIndexPath:(NSIndexPath *)indexpath AndSender:(UIButton *)sender AndStatus:(BOOL) isStatus;
@end

@interface CheckListDetailContentCellV2 : UITableViewCell{
    UIImageView *imgCheck;
    UILabel *lblDetailContentName;
    UILabel *lblDetailContentPointInpected;
    UIButton *btnDetailContentRating;
    UIButton *btnDetailContentCheck;
    UILabel *lblLine;
    BOOL isChecked;
    NSIndexPath *indexpath;
    __unsafe_unretained id<CheckListDetailContentCellV2Delegate> delegate;
}

@property (assign, nonatomic) id<CheckListDetailContentCellV2Delegate> delegate;
@property (nonatomic, strong) NSIndexPath *indexpath;
@property (nonatomic, readwrite) BOOL isChecked;
@property (strong, nonatomic) IBOutlet UIButton *btnDetailContentRating;
@property (strong, nonatomic) IBOutlet UIButton *btnDetailContentCheck;
@property (nonatomic, strong) IBOutlet UIImageView *imgCheck;
@property (nonatomic, strong) IBOutlet UILabel *lblDetailContentName;
@property (nonatomic,strong) IBOutlet UILabel *lblDetailContentPointInpected;
@property (nonatomic,strong) IBOutlet UILabel *lblDetailMandatory;
@property (nonatomic, strong) IBOutlet UILabel *lblLine;
- (IBAction)btnRatingPressed:(id)sender;
- (IBAction)btnCheckPressed:(id)sender;
-(void) setCheckStatus:(BOOL)checkStatus;

@end
