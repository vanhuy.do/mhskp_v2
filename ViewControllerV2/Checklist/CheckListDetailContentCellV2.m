//
//  CheckListDetailContentCellV2.m
//  mHouseKeeping
//
//  Created by TMS on 2/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CheckListDetailContentCellV2.h"
#import "UIImage+Tint.h"
#import "ehkDefinesV2.h"

@implementation CheckListDetailContentCellV2
@synthesize lblLine;
@synthesize btnDetailContentRating;
@synthesize btnDetailContentCheck;
@synthesize imgCheck,lblDetailContentName,lblDetailContentPointInpected;
@synthesize isChecked;
@synthesize indexpath;
@synthesize delegate;

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)btnRatingPressed:(id)sender {
    if ([delegate respondsToSelector:@selector(btnDetailContentRatingPressedWithIndexPath:AndSender:)]) {
        [delegate btnDetailContentRatingPressedWithIndexPath:indexpath AndSender:sender];
    }
}

- (IBAction)btnCheckPressed:(id)sender {
    if(isChecked == NO){
        isChecked = YES;
        [btnDetailContentCheck setImage:[UIImage imageNamed:imgChecked] forState:UIControlStateNormal]; 
        if([delegate respondsToSelector:@selector(btnDetailContentCheckmarkPressedWithIndexPath:AndSender:AndStatus:)]){
            [delegate btnDetailContentCheckmarkPressedWithIndexPath:indexpath AndSender:sender AndStatus:YES];
        }
    }
    else
    {
        isChecked = NO;
        [btnDetailContentCheck setImage:[UIImage imageBeforeiOS7:imgUnCheck equaliOS7:imgUnCheckFlat] forState:UIControlStateNormal];
        if([delegate respondsToSelector:@selector(btnDetailContentCheckmarkPressedWithIndexPath:AndSender:AndStatus:)]){
            [delegate btnDetailContentCheckmarkPressedWithIndexPath:indexpath AndSender:sender AndStatus:NO];
        }
    }
}

-(void)setCheckStatus:(BOOL)checkStatus {
    isChecked = checkStatus;
    if(isChecked == YES){
        [btnDetailContentCheck setImage:[UIImage imageNamed:imgChecked] forState:UIControlStateNormal];    
    }
    else
    {
        [btnDetailContentCheck setImage:[UIImage imageBeforeiOS7:imgUnCheck equaliOS7:imgUnCheckFlat] forState:UIControlStateNormal];
    }
}

@end
