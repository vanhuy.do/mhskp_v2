//
//  CheckListDetailViewV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CheckListViewV2.h"
#import "CheckListTypeModelV2.h"
#import "CheckListContentTypeModelV2.h"
#import "CheckListDetailContentModelV2.h"
#import "SectionInfoV2.h"
#import "SectionCheckListContentTypeViewV2.h"
#import "CheckListDetailContentCellV2.h"
#import "PickerViewV2.h"
#import "CheckListItemCoreDBModelV2.h"
#import "RemarkViewV2.h"

enum ENUM_POPUP {
    ENUM_POPUP_NOT_SHOW = 1,
    ENUM_POPUP_SHOW
    };

@protocol CheckListDetailViewV2Delegate <NSObject>

-(void)didChangeScoreWithCheckList:(ChecklistFormScoreModel *) chklistFormScore;

@end

@interface CheckListDetailViewV2 : UIViewController <UITableViewDataSource, UITableViewDelegate, SectionCheckListContentTypeViewV2Delegate, PickerViewV2Delegate,RemarkViewV2Delegate, CheckListDetailContentCellV2Delegate, UIAlertViewDelegate>{
    CheckListTypeModelV2 *chkListTypeV2;
    NSMutableArray *sectionInfoArray;
    CheckListModelV2 *chkList;
    CheckListItemCoreDBModelV2 *chkListItemScoreView;
    __unsafe_unretained id <CheckListDetailViewV2Delegate> delegate;
    
    IBOutlet UITableView *tbvchkDetail;
    IBOutlet UIButton *btnSubmit;
    
    BOOL isUserInteraction;
    RemarkViewV2 *remarkView;
    NSString *checkListText;
    //List all sections did load at first time load view
    NSMutableArray *listCheckListItems;
    
    BOOL shouldSubmitBeforeClose;
    IBOutlet NSLayoutConstraint *constrHeightViewAroundRemarkAndButton;
}

@property (nonatomic, strong) NSMutableArray *sectionInfoArray;
@property (nonatomic, strong) CheckListTypeModelV2 *chkListTypeV2;
@property (nonatomic, readwrite) NSInteger totalReceived;
@property (nonatomic, readwrite) CGFloat totalPoint;
@property (nonatomic, assign) NSInteger totalPossible;
@property (nonatomic, assign) BOOL statusHightlightRed;
@property (nonatomic, assign) BOOL statusSave;
@property (nonatomic, assign) BOOL statusChooseChecklistItem;
@property (nonatomic, strong) NSString *msgbtnOK;
@property (nonatomic, strong) NSString *msgSaveCheckListDetail;
@property (nonatomic, strong) NSString *msgErrorCheckListDetail;
@property (strong, nonatomic) IBOutlet UIView *viewArroundRemarkAndButton;
@property (nonatomic, strong) CheckListModelV2 *chkList;
@property (nonatomic, strong) CheckListItemCoreDBModelV2 *chkListItemScoreView;
@property (assign, nonatomic) id<CheckListDetailViewV2Delegate> delegate;
@property (nonatomic, strong) NSString *roomNo;
@property (strong, nonatomic) IBOutlet UILabel *lblRemark;
@property (strong, nonatomic) NSString *remark;
@property (strong, nonatomic) IBOutlet UITextView *tvRemark;
@property (strong, nonatomic) IBOutlet UIView *viewRemark;

-(IBAction) btnSubmitClicked:(id)sender;
- (IBAction)btnRemark:(id)sender;

-(void) loadSectionDatas;
-(enum ENUM_POPUP) saveCheckListDetail:(NSNotification*)nt;
-(void) calculatorScore;
-(void) setCaptionView;
-(BOOL) isNotSavedAndExit;

@end
