//
//  CheckListDetailViewV2.m
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CheckListDetailViewV2.h"
#import "CheckListManagerV2.h"
#import "CustomAlertViewV2.h"
#import "CommonVariable.h"
#import "CustomAlertViewV2.h"
#import "TasksManagerV2.h"
//#import "HomeViewV2.h"
#import "TopbarViewV2.h"
#import "MyNavigationBarV2.h"
#import "DeviceManager.h"
#import "ehkDefinesV2.h"
#import "UIImage+Tint.h"

#define tagAlertHightlightRed 11
#define tagAlertBack 12

@interface CheckListDetailViewV2(PrivateMethods)

-(void) backBarPressed;

#define tagTopbarView 1234

-(void) loadTopbarView;
-(void) showTopbarView;
-(void) hideTopbarView;
-(void) setHiddenTopbarView;
-(void) adjustShowForViews;
-(void) adjustRemoveForViews;
-(void) addButtonHandleShowHideTopbar;
-(void) topbarTapped:(UIButton *) sender;
-(TopbarViewV2 *) getTopBarView;
-(BOOL) allowHomeButtonPress;
-(enum ENUM_POPUP) saveData;

@end

@implementation CheckListDetailViewV2

@synthesize chkListTypeV2;
@synthesize sectionInfoArray;
@synthesize totalReceived;
@synthesize totalPoint;
@synthesize statusHightlightRed;
@synthesize totalPossible;
@synthesize statusSave;
@synthesize statusChooseChecklistItem;
@synthesize msgbtnOK;
@synthesize msgSaveCheckListDetail;
@synthesize msgErrorCheckListDetail;
@synthesize chkList;
@synthesize chkListItemScoreView;
@synthesize delegate;
@synthesize roomNo, remark;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //Hightlight home button
    if([CommonVariable sharedCommonVariable].roomIsCompleted != 1) {
        [[HomeViewV2 shareHomeView] setSelectedButton:tagOfHomeButton];
    }
    
    CheckListManagerV2 *chkManager = [[CheckListManagerV2 alloc] init];
    NSMutableArray *arrayFormScore = [chkManager loadChecklistFormScoreByChkListId:chkList.chkListId AndChkUserId:[UserManagerV2 sharedUserManager].currentUser.userId];
    BOOL temp = FALSE;
    for(ChecklistFormScoreModel *chkFormScore in arrayFormScore) {
        NSString *formScore = [NSString stringWithFormat:@"%d", (int)chkFormScore.dfScore];
        if (![formScore isEqualToString:@"-1"]) {
            temp = TRUE;
            break;
        }
    }
    
    isUserInteraction = !temp;
    
    // Hao Tran[20130509/Disable Button Submit] - Disable button submit if not supervisor
    if(![UserManagerV2 isSupervisor])
    {
        isUserInteraction = NO;
    }
    // Hao Tran[20130509/Disable Button Submit] - END
    
    [self setCaptionView];
}

-(void) loadDataAfterDelayWithHUD:(MBProgressHUD *) HUD {
    [self loadSectionDatas];

    //hide saving data.
    [self performSelector:@selector(hiddenHUDAfterSaved:) withObject:HUD afterDelay:0.5];
}

-(void) loadCheckListWSAfterDelayWithHUD:(MBProgressHUD *) HUD {
    //enable flag to announce, is loading data
    [[HomeViewV2 shareHomeView] waitingLoadingData];
    
//    SyncManagerV2 *synManager = [[SyncManagerV2 alloc] init];
//    
//    //-------------WS----------------
//    NSInteger userId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
//    NSInteger hotelId = [[[UserManagerV2 sharedUserManager] currentUser] userHotelsId];
//    
//    [synManager getAllCheckListCategoriesWithUserId:userId AndHotelId:hotelId];
//    [synManager getAllCheckListItemWithUserId:userId AndHotelId:hotelId];
//    //-------------end---------------
    
    [self loadSectionDatas];
    //hide saving data.
    [self performSelector:@selector(hiddenHUDAfterSaved:) withObject:HUD afterDelay:0.5];
    
    //end loading data
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
}
- (void) setTextColorForRemark: (NSString *) text {
    [self.tvRemark setFont:[UIFont fontWithName:@"Helvetica-Bold" size:15]];
    [self.tvRemark setBackgroundColor:[UIColor clearColor]];
    [self.tvRemark setTextColor:[UIColor grayColor]];
    [self.tvRemark setText:text];
}

- (void) reloadUIRemarkView {
   //ADAM [20171027/AU-00102505] - Fix btnSubmit not visible in iphone 4 screen size
    constrHeightViewAroundRemarkAndButton.constant = self.viewRemark.isHidden? btnSubmit.frame.size.height :111;
}

- (void)viewDidLoad {
//#warning dthanhson edit : insert
//    [vHeaderChkListDetail setHidden:YES];
//#warning end
    
    [super viewDidLoad];
    //Resize content views of current VC to fit with Navigation bar and tabbar
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    // Do any additional setup after loading the view from its nib.
    //[self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:imgBackground]]];
    
    statusHightlightRed = NO;
    listCheckListItems = [NSMutableArray array];
    shouldSubmitBeforeClose = NO;
    if (![UserManagerV2 sharedUserManager].currentUserAccessRight.roomAssignmentChecklistRemark) {
        [self.viewRemark setHidden:YES];
        [self reloadUIRemarkView];
    } else {
        RoomAssignmentModelV2 *roomAssignmentModel = [[RoomAssignmentModelV2 alloc] init];
        roomAssignmentModel.roomAssignment_Id = (int)[TasksManagerV2 getCurrentRoomAssignment];
        roomAssignmentModel.roomAssignment_UserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
        [[RoomManagerV2 sharedRoomManager] load:roomAssignmentModel];
        checkListText = roomAssignmentModel.roomAssignmentChecklistRemark;
        if(!checkListText) {
            checkListText = @"";
        }
        [self setTextColorForRemark:checkListText];
        [self.viewRemark setHidden:NO];
        [self reloadUIRemarkView];
    }
//#warning dthanhson edit : remove
//    if(chkListTypeV2 !=nil) {
//        if (chkListTypeV2.chkType == tCheckmark) {
//            [imgType setImage:[UIImage imageNamed:imgCheckMark]];
//        } else {
//            [imgType setImage:[UIImage imageNamed:imgRating]];
//        }
//        
//        //set language name checklist form
//        if ([[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString: (NSString *)ENGLISH_LANGUAGE] == NO) {
//            [lblContentName setText:chkListTypeV2.chkTypeLangName];
//        } else {
//            [lblContentName setText:chkListTypeV2.chkTypeName];
//        }
//    }
//#warning end
    [self.lblRemark setTextColor:[UIColor colorWithRed:6/255.0 green:62/255.0 blue:127/255.0 alpha:1]];
    [self.lblRemark setFont:[UIFont fontWithName:@"Helvetica-Bold" size:15]];
    [self.lblRemark setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_EC_REMARK]];
    [tbvchkDetail setSeparatorColor:[UIColor clearColor]];    
    [tbvchkDetail setBackgroundView:nil];
    [tbvchkDetail setBackgroundColor:[UIColor clearColor]];
    [tbvchkDetail setOpaque:YES];
    //[self.viewRemark setHidden: YES];
    
    

    //set title bar
    NSString *chkListTitle = [NSString stringWithFormat:@"%@ - %@%@",[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_strchecklist],[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOM_NO],[RoomManagerV2 getCurrentRoomNo]];
    [self setTitle:chkListTitle];    
    
    MBProgressHUD	*HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getRaLoadingData]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES];
    
    [self performSelector:@selector(loadCheckListWSAfterDelayWithHUD:) withObject:HUD afterDelay:0.1];
        
//#warning dthanhson edit : remove
//    [lblReceived setText:[[LanguageManagerV2 sharedLanguageManager] getPointsReceived]];
//    [lblPossible setText:[[LanguageManagerV2 sharedLanguageManager] getPointsPossible]];
//    
//    if (chkListTypeV2.chkType == tCheckmark) {        
//        [lblTotal setText:[[LanguageManagerV2 sharedLanguageManager] getPointsTotal]];                
//    } else {        
//        [lblTotal setText:[[LanguageManagerV2 sharedLanguageManager] getRoomStandards]];        
//    }
//#warning end

    
    [btnSubmit setTitle:[L_SUBMIT currentKeyToLanguage] forState:UIControlStateNormal];
    
    [btnSubmit setBackgroundImage:[UIImage imageBeforeiOS7:imgBtnSubmitClicked equaliOS7:imgBtnSubmitClickedFlat] forState:UIControlEventTouchUpInside];
    if (true) {
        [btnSubmit setBackgroundImage:[UIImage imageBeforeiOS7:imgBtnSubmit equaliOS7:imgBtnSubmitFlat] forState:UIControlStateNormal];
    }
    else {
        [btnSubmit setBackgroundImage:[UIImage imageBeforeiOS7:imgBtnSubmitDisable equaliOS7:imgBtnSubmitDisableFlat] forState:UIControlStateNormal];
    }
    
    //back button navigation
    self.navigationItem.leftBarButtonItem = [MyNavigationBarV2 createBackButtonWithTarget:self selector:@selector(backBarPressed) forControlEvents:UIControlEventTouchUpInside];
    
    [self performSelector:@selector(loadTopbarView)];
}

-(BOOL) isNotSavedAndExit {
    if (statusSave == YES) {
        return FALSE;//saved
    } else {
        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObject:[UIImage imageNamed:@"yes_bt.png"]] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:msgSaveCheckListDetail delegate:self cancelButtonTitle:msgbtnOK otherButtonTitles:nil, nil]; 
        alert.delegate = self;
        alert.tag = 10000;
        [alert show];
        return TRUE;
    }
}

#pragma mark - Check CheckList Form Score
-(BOOL) checkRoomStatus {
    RoomAssignmentModelV2 *ramodel = [[RoomAssignmentModelV2 alloc] init];
    ramodel.roomAssignment_Id = (int)[TasksManagerV2 getCurrentRoomAssignment];
    ramodel.roomAssignment_UserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    [[RoomManagerV2 sharedRoomManager] load:ramodel];
    
    if (ramodel.roomAssignmentRoomInspectionStatusId == ENUM_INSPECTION_COMPLETED_FAIL || ramodel.roomAssignmentRoomInspectionStatusId == ENUM_INSPECTION_COMPLETED_PASS) {
        return YES;
    }
    
    return NO;
}

-(BOOL) isCheckChkListFormScore {
    CheckListManagerV2 *manager = [[CheckListManagerV2 alloc] init];
    ChecklistFormScoreModel *formScoreModel = [manager loadCheckListFormScoreByChkListId:chkList.chkListId AndChkFormId:chkListTypeV2.chkTypeId AndChkUserId:[UserManagerV2 sharedUserManager].currentUser.userId];
    
    if (formScoreModel.dfScore >= 0
        || [self checkRoomStatus] == YES
        || ([sectionInfoArray count] <= 0 && [listCheckListItems count] <=0)
        || statusChooseChecklistItem == NO) {
        
        return YES;
    } else {
        return NO;
    }
}

#pragma mark - Button Back
-(void)backBarPressed {
    //need to save before leaving
    BOOL saveChkListFormScore = [self isCheckChkListFormScore];
    if(saveChkListFormScore == YES && !shouldSubmitBeforeClose) {
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObject:[UIImage imageNamed:@"yes_bt.png"]] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:msgSaveCheckListDetail delegate:self cancelButtonTitle:msgbtnOK otherButtonTitles:nil, nil]; 
        alert.delegate = self;
        alert.tag = tagAlertBack;
        [alert show];
    }
}

#pragma mark - === Handle Check Save Data ===
#pragma mark
-(void) checkmarkSelectedIndexPath:(NSIndexPath*)indexpath status:(BOOL) isStatus
{
    //load checklist item
    SectionInfoV2 *sectionInfo = [sectionInfoArray objectAtIndex:indexpath.section];
    CheckListDetailContentModelV2 *model = [sectionInfo.rowHeights objectAtIndex:indexpath.row];
    
    //Load CheckList Item Score
    CheckListItemCoreDBModelV2 *chkListItemScore = model.itemCoreModel;
    chkListItemScore.chkPostStatus = POST_STATUS_SAVED_UNPOSTED;
    
    //Refer INI "CHECKLIST_DEFAULT_FULLMARK" for more detail
    NSInteger defaultValueChecklist = [[UserManagerV2 sharedUserManager].currentUserAccessRight valueForChecklistCheckMark];
    if (isStatus == YES) {
        if( defaultValueChecklist == MHChecklistCheckMark_CheckToAdd ) {
            chkListItemScore.chkItemCore = 1;
        } else if (defaultValueChecklist == MHChecklistCheckMark_CheckToMinus) {
            chkListItemScore.chkItemCore = 0;
        }
        else {
            chkListItemScore.chkItemCore = 1;
        }
    } else {
        if(defaultValueChecklist == MHChecklistCheckMark_CheckToAdd) {
            chkListItemScore.chkItemCore = 0;
        } else if (defaultValueChecklist == MHChecklistCheckMark_CheckToMinus){
            chkListItemScore.chkItemCore = 1;
        } else {
            chkListItemScore.chkItemCore = 0;
        }
    }
    
    CheckListManagerV2 *manager = [[CheckListManagerV2 alloc] init];
    [manager updateCheckListItemCoreByChkItemId:chkListItemScore];
}

-(BOOL) checkSaveDataWithTagEvent:(NSInteger) tagOfEvent {
    //YES
    //need to save before leaving
    BOOL saveChkListFormScore = [self isCheckChkListFormScore];
    if (tagOfEvent == tagOfMessageButton) {
        if (saveChkListFormScore == YES) {
            [[CommonVariable sharedCommonVariable] setRoomIsCompleted:0];
            return NO;
        }
        else{
            UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObject:[UIImage imageNamed:@"yes_bt.png"]] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:msgSaveCheckListDetail delegate:self cancelButtonTitle:msgbtnOK otherButtonTitles:nil, nil]; 
            alert.delegate = self;
            alert.tag = 10000;
            [alert show];
            return YES;
        }
    }
    else{
        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObject:[UIImage imageNamed:@"yes_bt.png"]] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_complete_room_to_leave] delegate:self cancelButtonTitle:msgbtnOK otherButtonTitles:nil, nil]; 
        alert.delegate = self;
        alert.tag = 10000;
        [alert show];
        //when go this screen can not move to another screen
        return YES;
    }
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Load Section Categories Data
-(void)loadSectionDatas {
    CheckListManagerV2 *manager = [[CheckListManagerV2 alloc] init];
    
    //load checklist items have the same checklist form id
    NSMutableArray *arraychkItemByFormId = [manager loadCheckListItemByChkFormId:chkListTypeV2.chkTypeId];
    NSMutableArray *arraychkItemByCategoryId = [[NSMutableArray alloc] init];
    
    sectionInfoArray = [[NSMutableArray alloc] init];
    
    //Insert CheckList Form Score
    ChecklistFormScoreModel *chkFormScore = [manager loadCheckListFormScoreByChkListId:chkList.chkListId AndChkFormId:chkListTypeV2.chkTypeId AndChkUserId:[UserManagerV2 sharedUserManager].currentUser.userId];
    if(chkFormScore.dfId == 0) {
        chkFormScore.dfFormId = chkListTypeV2.chkTypeId;
        chkFormScore.dfChecklistId = chkList.chkListId;
        chkFormScore.dfUserId = [UserManagerV2 sharedUserManager].currentUser.userId;
        chkFormScore.dfScore = -1;
        chkFormScore.dfPostStatus = POST_STATUS_SAVED_UNPOSTED;
        [manager insertCheckListFormScore:chkFormScore];
    }
    
    //load checklist items have the same checklist categories id
    for (CheckListDetailContentModelV2 *chkItems in arraychkItemByFormId) {
        if (arraychkItemByCategoryId.count > 0) {
            CheckListDetailContentModelV2 *chkItemInArray  = [arraychkItemByCategoryId objectAtIndex:0];
            
            
            if (chkItems.chkContentId != chkItemInArray.chkContentId) {
                //array checklist item has the same category id
                arraychkItemByCategoryId = [[NSMutableArray alloc] init];
                arraychkItemByCategoryId = [manager loadCheckListItemByChkCategoriesId:chkItems.chkContentId formId:chkListTypeV2.chkTypeId];
                
                //Load CheckList Categories
                CheckListContentTypeModelV2 *chkCategory = [[CheckListContentTypeModelV2 alloc] init];
                CheckListDetailContentModelV2 *chkItemByCategory = [arraychkItemByCategoryId objectAtIndex:0];
                chkCategory.chkContentId = chkItemByCategory.chkContentId;
                [manager loadCheckListCategories:chkCategory];
                
                //set checklist category for section
                SectionInfoV2 *sectionInfo = [[SectionInfoV2 alloc] init];
                sectionInfo.chkContentType = chkCategory;
                
                //Hao Tran - change request set max rate for checklist as default
                NSInteger defaulValueCheckRate = [[[UserManagerV2 sharedUserManager] currentUserAccessRight] valueForCheckMarkRate];
                
                //load checklist item
                for (CheckListDetailContentModelV2 *chkDetailContentModel in arraychkItemByCategoryId) {
                    //load checklist form score
                    ChecklistFormScoreModel *chkFormScore = [manager loadCheckListFormScoreByChkListId:chkList.chkListId AndChkFormId:chkDetailContentModel.chkTypeId AndChkUserId:[UserManagerV2 sharedUserManager].currentUser.userId];
                    
                    //load checklist item score
                    CheckListItemCoreDBModelV2 *chklistItemScoreDB = [manager loadCheckListItemScoreByItemId:chkDetailContentModel.chkDetailContentId AndFormScoreId:chkFormScore.dfId];
                    
                    //load checklist form
                    CheckListTypeModelV2 *chkForm = [[CheckListTypeModelV2 alloc] init];
                    chkForm.chkTypeId = chkDetailContentModel.chkTypeId;
                    [manager loadCheckListForm:chkForm];
                    
                    if (chklistItemScoreDB.chkItemCoreDetailContentId == 0) {
                        //insert checklist item score
                        chklistItemScoreDB.chkItemCoreDetailContentId = chkDetailContentModel.chkDetailContentId;
                        
                        //Hao Tran - Set Default check Mark of Check list is 1
                        if(chkForm.chkType == tCheckmark){
                            switch ([[[UserManagerV2 sharedUserManager] currentUserAccessRight] valueForChecklistCheckMark]) {
                                case MHChecklistCheckMark_Normal:
                                {
                                    chklistItemScoreDB.chkItemCore = 0;
                                }
                                    break;
                                case MHChecklistCheckMark_CheckToMinus:
                                {
                                    chklistItemScoreDB.chkItemCore = 1;
                                }
                                    break;
                                case MHChecklistCheckMark_CheckToAdd:
                                {
                                    chklistItemScoreDB.chkItemCore = 1;
                                }
                                    break;
                                    
                                default:
                                    break;
                            }
                        } else {
                            
                            if (defaulValueCheckRate == MHChecklistCheckMark_Normal) {
                                chklistItemScoreDB.chkItemCore = -1;
                            } else {
                                chklistItemScoreDB.chkItemCore = chkDetailContentModel.chkDetailContentPointInpected;
                            }
                        }
                        
                        chklistItemScoreDB.chkFormScore = chkFormScore.dfId;
                        chklistItemScoreDB.chkPostStatus = POST_STATUS_UN_CHANGED;
                        [manager insertCheckListItemCore:chklistItemScoreDB];
                        
                        //load checklist item score
                        [manager loadCheckListItemScoreByItemId:chklistItemScoreDB.chkItemCoreDetailContentId AndFormScoreId:chkFormScore.dfId];
                    } else {
                        
                    }
                    //set checklist item score for checklist item
                    chkDetailContentModel.itemCoreModel = chklistItemScoreDB;
                    
                    //set checklist form for checklist item;
                    chkDetailContentModel.typeModel = chkForm;
                    
                    [sectionInfo insertObjectToNextIndex:chkDetailContentModel];
                }
                
                [sectionInfoArray addObject:sectionInfo];
            }
        }
        else {
            arraychkItemByCategoryId = [[NSMutableArray alloc] init];
            arraychkItemByCategoryId = [manager loadCheckListItemByChkCategoriesId:chkItems.chkContentId formId:chkListTypeV2.chkTypeId];
            
            if (arraychkItemByCategoryId.count > 0) {
                //Load CheckList Categories
                CheckListContentTypeModelV2 *chkCategory = [[CheckListContentTypeModelV2 alloc] init];
                CheckListDetailContentModelV2 *chkItemByCategory = [arraychkItemByCategoryId objectAtIndex:0];
                chkCategory.chkContentId = chkItemByCategory.chkContentId;
                [manager loadCheckListCategories:chkCategory];
                
                //set checklist category for section
                SectionInfoV2 *sectionInfo = [[SectionInfoV2 alloc] init];
                sectionInfo.chkContentType = chkCategory;
                
                //load checklist item
                for (CheckListDetailContentModelV2 *chkDetailContentModel in arraychkItemByCategoryId) {
                    //load checklist form score
                    ChecklistFormScoreModel *chkFormScore = [manager loadCheckListFormScoreByChkListId:chkList.chkListId AndChkFormId:chkDetailContentModel.chkTypeId AndChkUserId:[UserManagerV2 sharedUserManager].currentUser.userId];
                    
                    //load checklist item score
                    CheckListItemCoreDBModelV2 *chklistItemScoreDB = [manager loadCheckListItemScoreByItemId:chkDetailContentModel.chkDetailContentId AndFormScoreId:chkFormScore.dfId];
                    
                    //load checklist form
                    CheckListTypeModelV2 *chkForm = [[CheckListTypeModelV2 alloc] init];
                    chkForm.chkTypeId = chkDetailContentModel.chkTypeId;
                    [manager loadCheckListForm:chkForm];
                    
                    if (chklistItemScoreDB.chkItemCoreDetailContentId == 0 || (chklistItemScoreDB.chkPostStatus == POST_STATUS_UN_CHANGED && chklistItemScoreDB.chkItemCore == -1)) {
                        //insert checklist item score
                        chklistItemScoreDB.chkItemCoreDetailContentId = chkDetailContentModel.chkDetailContentId;
                        //Hao Tran - Set Default check Mark of Check list is 1
                        if(chkForm.chkType == tCheckmark){
                            switch ([[[UserManagerV2 sharedUserManager] currentUserAccessRight] valueForChecklistCheckMark]) {
                                case MHChecklistCheckMark_Normal:
                                {
                                    chklistItemScoreDB.chkItemCore = 0;
                                }
                                    break;
                                case MHChecklistCheckMark_CheckToMinus:
                                {
                                    chklistItemScoreDB.chkItemCore = 1;
                                }
                                    break;
                                case MHChecklistCheckMark_CheckToAdd:
                                {
                                    chklistItemScoreDB.chkItemCore = 1;
                                }
                                    break;
                                    
                                default:
                                    break;
                            }
                        } else {
                            //Hao Tran - change request set max rate for checklist as default
                            NSInteger defaulValueChecklistRate = [[[UserManagerV2 sharedUserManager] currentUserAccessRight] valueForCheckMarkRate];
                            if (defaulValueChecklistRate == MHChecklistCheckMark_Normal) {
                                chklistItemScoreDB.chkItemCore = -1;
                            } else {
                                chklistItemScoreDB.chkItemCore = chkDetailContentModel.chkDetailContentPointInpected;
                            }
                        }
                        //END set default score
                        
                        chklistItemScoreDB.chkFormScore = chkFormScore.dfId;
                        chklistItemScoreDB.chkPostStatus = POST_STATUS_UN_CHANGED;
                        [manager insertCheckListItemCore:chklistItemScoreDB];
                        
                        //load checklist item score
                        [manager loadCheckListItemScoreByItemId:chklistItemScoreDB.chkItemCoreDetailContentId AndFormScoreId:chkFormScore.dfId];
                    }
                    
                    //set checklist item score for checklist item
                    chkDetailContentModel.itemCoreModel = chklistItemScoreDB;
                    
                    //set checklist form for checklist item;
                    chkDetailContentModel.typeModel = chkForm;
                    
                    [sectionInfo insertObjectToNextIndex:chkDetailContentModel];
                }
                
                [sectionInfoArray addObject:sectionInfo];
            }
        }
    }
    
    [self calculatorScore];
    
    //reload table views
    [tbvchkDetail reloadData];
}

#pragma mark - UITableView Datasource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return sectionInfoArray.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    SectionInfoV2 *sectioninfo = [sectionInfoArray objectAtIndex:section];
    NSInteger numberRowInSection = sectioninfo.rowHeights.count;
    return sectioninfo.open ? numberRowInSection : 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 50;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    //config datas for cell
    SectionInfoV2 *sectionInfo = [sectionInfoArray objectAtIndex:indexPath.section];
    CheckListDetailContentModelV2 *model = [sectionInfo.rowHeights objectAtIndex:indexPath.row];
    
    //set language name checklist item
    NSString *stringDisplay = nil;
    if ([[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString: (NSString *)ENGLISH_LANGUAGE] == NO) {
        stringDisplay = model.chkDetailContentLang;
    }
    else{
        stringDisplay = model.chkDetailContentName;
    }
    
    //Calculate the expected size based on the font and linebreak mode of your label
    CGSize maximumLabelSize = CGSizeMake(150, 9999);
    //Load CheckList Form
    CheckListTypeModelV2 *chkForm = model.typeModel;
    
    if (chkForm.chkType == tCheckmark) {
        maximumLabelSize = CGSizeMake(210, 9999);
    }
    
    CGSize expectedLabelSize = [stringDisplay sizeWithFont:[UIFont fontWithName:@"Arial-BoldMT" size:14.0] 
                                      constrainedToSize:maximumLabelSize 
                                          lineBreakMode:NSLineBreakByWordWrapping];
    
    //adjust the label the the new height
    if (expectedLabelSize.height < 55) {
        return 55;
    } else
        return expectedLabelSize.height;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    SectionInfoV2 *sectioninfo = [sectionInfoArray objectAtIndex:section];
    //set language name checklist category
    NSString *nameChkContentType = @"";
    if ([[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString: (NSString *)ENGLISH_LANGUAGE] == NO) {
        nameChkContentType = sectioninfo.chkContentType.chkContentNameLang;
    }
    else{
        nameChkContentType = sectioninfo.chkContentType.chkContentName;
    }
    if (sectioninfo.headerView == nil) {
        SectionCheckListContentTypeViewV2 *sview = [[SectionCheckListContentTypeViewV2 alloc] initWithSection:section contentName:nameChkContentType AndStatusArrow:sectioninfo.open];
        
        sectioninfo.headerView = sview;
        sview.delegate = self;
    }
    
    return sectioninfo.headerView;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifyCell = @"CellIndetify";
    CheckListDetailContentCellV2 *cell = nil;
    
    cell = (CheckListDetailContentCellV2 *)[tableView dequeueReusableCellWithIdentifier:identifyCell];
    
    if (cell == nil) {
        NSArray *array = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([CheckListDetailContentCellV2 class]) owner:self options:nil];
        cell = [array objectAtIndex:0];
        
        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
            [cell.btnDetailContentRating setBackgroundImage:[UIImage imageNamed:imgRatingBoxFlat] forState:UIControlStateNormal];
            [cell.btnDetailContentRating setBackgroundImage:[UIImage imageNamed:imgRatingBoxFlat] forState:UIControlStateHighlighted];
        }
    }
    
    // Hao Tran[20130509/Disable interact] - Disable interact checklist if not supervisor
    if(![UserManagerV2 isSupervisor])
    {
        [cell.btnDetailContentCheck setUserInteractionEnabled:NO];
        [cell.btnDetailContentRating setUserInteractionEnabled:NO];
    }
    // Hao Tran[20130509/Disable interact] - END
    
    //config datas for cell
    SectionInfoV2 *sectionInfo = [sectionInfoArray objectAtIndex:indexPath.section];
    CheckListDetailContentModelV2 *model = [sectionInfo.rowHeights objectAtIndex:indexPath.row];
        
    //Load CheckList Item Score
    CheckListItemCoreDBModelV2 *chkListItemScore = model.itemCoreModel;
    
    //Load CheckList Form
    CheckListTypeModelV2 *chkForm = model.typeModel;
    
    //Load CheckList From Score by checkList_FromId and checkList_Id
    

    [cell setUserInteractionEnabled:isUserInteraction];

    //set language name checklist item
    if ([[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString: (NSString *)ENGLISH_LANGUAGE] == NO) {
        cell.lblDetailContentName.text = model.chkDetailContentLang;
    }
    else{
        cell.lblDetailContentName.text = model.chkDetailContentName;
    }
    
    if (model.chkDetailContentPointInpected >= 0) {
        cell.lblDetailContentPointInpected.text = [NSString stringWithFormat:@"%d",(int)model.chkDetailContentPointInpected];
    }
    
    if(chkForm.chkType == tCheckmark) {
        [cell.imgCheck setHidden:YES];
        [cell.btnDetailContentRating setHidden:YES];
        [cell.btnDetailContentCheck setHidden:NO];
        [cell.lblDetailContentPointInpected setHidden:YES];
        [cell.lblDetailMandatory setHidden:YES];
//        if([[UserManagerV2 sharedUserManager].currentUserAccessRight isChecklistFullMark]) {
//            [cell setCheckStatus:chkListItemScore.chkItemCore == 0 ? YES : NO];
//        } else {
//            [cell setCheckStatus:chkListItemScore.chkItemCore > 0 ? YES : NO];
//        }
        NSInteger defaulValueCheckmark = [[[UserManagerV2 sharedUserManager] currentUserAccessRight] valueForChecklistCheckMark];
        if (defaulValueCheckmark == MHChecklistCheckMark_CheckToAdd) {
            [cell setCheckStatus:chkListItemScore.chkItemCore > 0 ? YES : NO];
            
        } else if (defaulValueCheckmark == MHChecklistCheckMark_CheckToMinus){
            [cell setCheckStatus:chkListItemScore.chkItemCore <= 0 ? YES : NO];
            
        } else {
            [cell setCheckStatus:chkListItemScore.chkItemCore <= 0 ? NO : YES];
        }
        
        if ([[UserManagerV2 sharedUserManager].currentUserAccessRight checklistMandatoryPass] > 0)
        {
            if (isUserInteraction && model.chkMandatoryPass > 0) {
                cell.lblDetailContentName.textColor = [UIColor orangeColor];
            } else if(model.chkMandatoryPass > 0 && chkListItemScore.chkItemCore <= 0 && !isUserInteraction) {
                cell.lblDetailContentName.textColor = [UIColor orangeColor];
            } else {
                cell.lblDetailContentName.textColor = [UIColor colorWithRed:6/255.0 green:62/255.0 blue:172/255.0 alpha:1.0];
            }
            
        } else {
            cell.lblDetailContentName.textColor = [UIColor colorWithRed:6/255.0 green:62/255.0 blue:172/255.0 alpha:1.0];
        }
        
    } else {
        [cell.btnDetailContentRating setHidden:NO];
        [cell.btnDetailContentRating setTitle:(chkListItemScore.chkItemCore < 0 ? @"N/A" : [NSString stringWithFormat:@"%d", (int)chkListItemScore.chkItemCore]) forState:UIControlStateNormal];
        [cell.btnDetailContentCheck setHidden:YES];
        [cell.lblDetailContentPointInpected setHidden:NO];
        
        if ([[UserManagerV2 sharedUserManager].currentUserAccessRight checklistMandatoryPass] > 0 && (model.chkMandatoryPass > 0)) {
            [cell.lblDetailMandatory setHidden:NO];
            [cell.lblDetailMandatory setText:[NSString stringWithFormat:@"%d",(int)model.chkMandatoryPass]];
            
            if(chkListItemScore.chkItemCore <= 0 && !isUserInteraction) {
                cell.lblDetailContentName.textColor = [UIColor orangeColor];
            } else {
                cell.lblDetailContentName.textColor = [UIColor colorWithRed:6.0/255.0 green:62.0/255.0 blue:127.0/255.0 alpha:1.0];
            }
        } else if (chkListItemScore.chkItemCore < 0) {
            [cell.imgCheck setHidden:YES];
            [cell.lblDetailMandatory setHidden:YES];
            if (statusHightlightRed == YES && model.chkMandatoryPass <= 0) {
                cell.lblDetailContentName.textColor = [UIColor colorWithRed:255.0/255.0 green:14.0/255.0 blue:11.0/255.0 alpha:1.0];
            }
        }
        else{
            [cell.lblDetailMandatory setHidden:YES];
            [cell.imgCheck setHidden:NO];
            cell.lblDetailContentName.textColor = [UIColor colorWithRed:6.0/255.0 green:62.0/255.0 blue:127.0/255.0 alpha:1.0];
        }
    }
    
    //handle re-location contents cell
    
    //Calculate the expected size based on the font and linebreak mode of your label
    CGSize maximumLabelSize = CGSizeMake(150, 9999);
    if (chkForm.chkType == tCheckmark) {
        maximumLabelSize = CGSizeMake(210, 9999);
    }
    
    CGSize expectedLabelSize = [cell.lblDetailContentName.text sizeWithFont:[UIFont fontWithName:@"Arial-BoldMT" size:14.0] 
                                         constrainedToSize:maximumLabelSize 
                                             lineBreakMode:NSLineBreakByWordWrapping];
    CGRect frameContentName = cell.lblDetailContentName.frame;
    
    if (expectedLabelSize.height < 55) {
        expectedLabelSize.height = 55;
    }
    
    frameContentName.size.height = expectedLabelSize.height;
    if (chkForm.chkType == tCheckmark) {
        frameContentName.size.width = 210;
    }
    [cell.lblDetailContentName setFrame:frameContentName];
    
    //image check
    CGRect frame = cell.imgCheck.frame;
    frame.origin.y = expectedLabelSize.height / 2 - frame.size.height / 2;
    [cell.imgCheck setFrame:frame];
    
    //label detail content point inspected
    frame = cell.lblDetailContentPointInpected.frame;
    frame.origin.y = expectedLabelSize.height / 2 - frame.size.height / 2;
    [cell.lblDetailContentPointInpected setFrame:frame];
    
    //label mandatory content
    frame = cell.lblDetailMandatory.frame;
    frame.origin.y = expectedLabelSize.height / 2 - frame.size.height / 2;
    [cell.lblDetailMandatory setFrame:frame];
    
    //btn detail content check
    frame = cell.btnDetailContentCheck.frame;
    frame.origin.y = expectedLabelSize.height / 2 - frame.size.height / 2;
    [cell.btnDetailContentCheck setFrame:frame];
    
    //btn detail content rating
    frame = cell.btnDetailContentRating.frame;
    frame.origin.y = expectedLabelSize.height / 2 - frame.size.height / 2;
    [cell.btnDetailContentRating setFrame:frame];
    
    //label line
    frame = cell.lblLine.frame;
    frame.origin.y = expectedLabelSize.height - 1;
    [cell.lblLine setFrame:frame];
    
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell setBackgroundColor:[UIColor clearColor]];
    [cell setIndexpath:indexPath];
    cell.delegate = self;
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    SectionInfoV2 *sectionInfo = [sectionInfoArray objectAtIndex:indexPath.section];
    CheckListDetailContentModelV2 *model = [sectionInfo.rowHeights objectAtIndex:indexPath.row];
    
    //Load CheckList Form
    CheckListTypeModelV2 *chkForm = model.typeModel;
    
    if(chkForm.chkType == tCheckmark) {
        CheckListDetailContentCellV2 *detailContentCell = (CheckListDetailContentCellV2*)[tableView cellForRowAtIndexPath:indexPath];
        [detailContentCell btnCheckPressed:detailContentCell.btnDetailContentCheck];
    }
}

#pragma mark - Section Header Delegate
-(void)sectionHeaderView:(SectionCheckListContentTypeViewV2 *)sectionheaderView sectionOpened:(NSInteger)section {    
    SectionInfoV2 *sectionInfo = [sectionInfoArray objectAtIndex:section];
    sectionInfo.open = YES;
    
    NSInteger countOfRowsToInsert = [sectionInfo.rowHeights count];
    if (countOfRowsToInsert == 0) {
        return;
    }
    
    NSMutableArray *indexPathsToInsert = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i < countOfRowsToInsert; i++) {
        [indexPathsToInsert addObject:[NSIndexPath indexPathForRow:i inSection:section]];
    }
    
    UITableViewRowAnimation insertAnimation;
    insertAnimation = UITableViewRowAnimationAutomatic;
    
    [tbvchkDetail beginUpdates];
    [tbvchkDetail insertRowsAtIndexPaths:indexPathsToInsert withRowAnimation:insertAnimation];
    [tbvchkDetail endUpdates];
    [tbvchkDetail scrollToRowAtIndexPath:[indexPathsToInsert objectAtIndex:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

-(void)sectionHeaderView:(SectionCheckListContentTypeViewV2 *)sectionheaderView sectionClosed:(NSInteger)section {
    SectionInfoV2 *sectionInfo = [sectionInfoArray
                                objectAtIndex:section];
    
    sectionInfo.open = NO;
    NSInteger countOfRowsToDelete = [tbvchkDetail numberOfRowsInSection:section];
    
    if (countOfRowsToDelete > 0) {
        NSMutableArray *indexPathsToDelete = [[NSMutableArray alloc] init];
        for (NSInteger i = 0; i < countOfRowsToDelete; i++) {
            [indexPathsToDelete addObject:[NSIndexPath indexPathForRow:i inSection:section]];
        }
        
        [tbvchkDetail beginUpdates];
        [tbvchkDetail deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:UITableViewRowAnimationAutomatic];
        [tbvchkDetail endUpdates];
    }
}

#pragma mark - CheckListDetailViewV2 Delegate Methods
-(void)btnDetailContentRatingPressedWithIndexPath:(NSIndexPath *)indexpath AndSender:(UIButton *)sender{
//    statusChooseChecklistItem = YES;
    SectionInfoV2 *sectionInfo = [sectionInfoArray objectAtIndex:indexpath.section];
    CheckListDetailContentModelV2 *model = [sectionInfo.rowHeights objectAtIndex:indexpath.row];
    NSMutableArray *array = [[NSMutableArray alloc] init];
    for (NSInteger index = 0; index <= model.chkDetailContentPointInpected; index ++) {
        [array addObject:[NSString stringWithFormat:@"%d",(int)index]];
    }
    
    PickerViewV2 *picker = [[PickerViewV2 alloc] initPickerViewV2WithDatas:array AndSelectedData:sender.titleLabel.text AndIndex:indexpath];
    picker.delegate = self;
    [picker showPickerViewV2];
}

-(void)btnDetailContentCheckmarkPressedWithIndexPath:(NSIndexPath *)indexpath AndSender:(UIButton *)sender AndStatus:(BOOL)isStatus{
//    statusChooseChecklistItem = YES;
    
    shouldSubmitBeforeClose = YES;
    [self checkmarkSelectedIndexPath:indexpath status:isStatus];
    [tbvchkDetail reloadData];
}

#pragma mark - PickerViewV2 Delegate Methods
-(void)didChoosePickerViewV2WithData:(NSString *)data AndIndex:(NSIndexPath *)index {    
    statusChooseChecklistItem = YES;

    //load checklist item
    SectionInfoV2 *sectionInfo = [sectionInfoArray objectAtIndex:index.section];
    CheckListDetailContentModelV2 *model = [sectionInfo.rowHeights objectAtIndex:index.row];
    
    //Load CheckList Item Score
    CheckListItemCoreDBModelV2 *chkListItemScore = model.itemCoreModel;
    chkListItemScore.chkPostStatus = POST_STATUS_SAVED_UNPOSTED;
    chkListItemScore.chkItemCore = [data integerValue];
    
    CheckListManagerV2 *manager = [[CheckListManagerV2 alloc] init];
    [manager updateCheckListItemCoreByChkItemId:chkListItemScore];
    
    //[tbvchkDetail reloadRowsAtIndexPaths:[NSArray arrayWithObject:index] withRowAnimation:UITableViewRowAnimationFade];
    [tbvchkDetail reloadData];
}

#pragma mark - Save CheckList Detail
-(enum ENUM_POPUP)saveCheckListDetail:(NSNotification *)nt {
    // @ show loading data .
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getRaSavingData]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES];  
    
    CheckListManagerV2 *manager = [[CheckListManagerV2 alloc] init];
    SyncManagerV2 *synManager = [[SyncManagerV2 alloc] init];
    NSInteger userId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    NSInteger roomAssignId = [TasksManagerV2 getCurrentRoomAssignment];

    NSInteger isMandatoryPass = 1;
    if (chkListTypeV2.chkType == tCheckmark) {
        NSMutableArray *postCheckListItems = [NSMutableArray array];
        
        NSInteger iniMandatoryValue = [[UserManagerV2 sharedUserManager].currentUserAccessRight checklistMandatoryPass];
        for (NSInteger indexSection = 0; indexSection < sectionInfoArray.count; indexSection ++) {
            SectionInfoV2 *sectionInfo = [sectionInfoArray objectAtIndex:indexSection];
            for (NSInteger indexRow=0; indexRow < sectionInfo.rowHeights.count; indexRow++) {
                
                CheckListDetailContentModelV2 *model = [sectionInfo.rowHeights objectAtIndex:indexRow];
                
                // Hao Tran[20130513/Fix Caculate Score] - Fix score always show 0% in check mark view
                if([listCheckListItems indexOfObject:model] == NSNotFound)
                {
                    [listCheckListItems addObject: model];
                }
                // Hao Tran[20130513/Fix Caculate Score] - END
                
                //Load CheckList Item Score
                CheckListItemCoreDBModelV2 *chkListItemScore = model.itemCoreModel;
                
                if (iniMandatoryValue > 0) {
                    if (model.chkMandatoryPass > 0) {
                        if (model.itemCoreModel.chkItemCore <= 0) {
                            isMandatoryPass = 0;
                        }
                    }
                }
                
                //Hao Tran [20140512/Change Checklist Logic] - Please refer function "isChecklistFullMark"
                /*
                //Hao Tran[20141119/Fix to post all check list] - Post all checklist check mark if INI is enabled
                if([[UserManagerV2 sharedUserManager].currentUserAccessRight isChecklistFullMark]) {
                    if(chkListItemScore.chkItemCore < 0) {
                        chkListItemScore.chkItemCore = 0;
                    }
                }
                //Hao Tran[20141119/Fix to post all check list] - END
                
                //Hao Tran [20141204/Fixed checklist item score missing in posting to server] - RC issue
                if (chkListItemScore.chkItemCore < 0) {
                    chkListItemScore.chkItemCore = 0;
                }*/
                
                if (chkListItemScore.chkItemCore != -1) {                    
                    //Update Score in CheckList Item Core
                    chkListItemScore.chkPostStatus = POST_STATUS_SAVED_UNPOSTED;
                    chkListItemScore.chkItemCore = model.itemCoreModel.chkItemCore;
                    [manager updateCheckListItemCoreByChkItemId:chkListItemScore];
                    
                    //set checklist itemcore for checklist item
                    model.itemCoreModel = chkListItemScore;
                    
                    //-------------Post WSLog
                    int switchPostLogStatus = (int)[[NSUserDefaults standardUserDefaults] integerForKey:ENABLE_LOGWS];
                     if (switchPostLogStatus == 1) {
                         NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
                         NSString *messageValue = [NSString stringWithFormat:@"CheckListDetail Post WSLog: Time:%@, UserId:%d, RoomAssignId:%d, ItemCore:%d", time, (int)userId, (int)roomAssignId, (int)chkListItemScore.chkItemCore];
                         [synManager postWSLogCheckListDetail:userId Message:messageValue MessageType:1];
                    }
                    //-------------End Post WSLog
                    
                    
                    
                    //------------------WS-----------------
                    //Hao Tran remove for change WS post checklist
                    //[synManager postCheckListItemWithUserId:userId RoomAssignId:roomAssignId AndChkListItem:model];
                    
                    [postCheckListItems addObject:model];
                    //------------------end----------------
                    
                 }
            }
        }
        
        [synManager postCheckListItemsWithUserId:userId RoomAssignId:roomAssignId AndChkListItems:postCheckListItems];
        
        //Show Total point in checkmark
        [self calculatorScore];
        
        //Update Score in CheckList Form Score
        ChecklistFormScoreModel *chkFormScore = [manager loadCheckListFormScoreByChkListId:chkList.chkListId AndChkFormId:chkListTypeV2.chkTypeId AndChkUserId:[UserManagerV2 sharedUserManager].currentUser.userId];
        chkFormScore.dfScore = round(totalPoint);
        chkFormScore.dfPostStatus = POST_STATUS_SAVED_UNPOSTED;
        chkFormScore.dfIsMandatoryPass = isMandatoryPass;
        [manager updateCheckListFormScore:chkFormScore];
        
        //set delegate change value in checklist detail
        if ([delegate respondsToSelector:@selector(didChangeScoreWithCheckList:)]) {
            [delegate didChangeScoreWithCheckList:chkFormScore];
        }
    } else {        
        totalReceived = 0;
        BOOL showAlert = NO;
        
        for (NSInteger indexSection=0; indexSection < sectionInfoArray.count; indexSection ++) {
            SectionInfoV2 *sectionInfo = [sectionInfoArray objectAtIndex:indexSection];
            if (showAlert == YES) {
                break;
            }
            
            for (NSInteger indexRow=0; indexRow < sectionInfo.rowHeights.count; indexRow++) {
                CheckListDetailContentModelV2 *model = [sectionInfo.rowHeights objectAtIndex:indexRow];
                
                //Load CheckList Item Score
                CheckListItemCoreDBModelV2 *chkListItemScore = model.itemCoreModel;
                
                if (showAlert == YES) {
                    break;
                }
                
                if (chkListItemScore.chkItemCore == -1) {
                    showAlert = YES;
                }
            }
        }
        
        //Show alert highlightRed
        if (showAlert) {
            
            // Hao Tran[20130509/Remove layout data] - remove sections no need to show again
            [self removeSectionsDidchoose];
            // Hao Tran[20130510/Remove layout data] - END
            
            // Hao Tran[20130509/Fix layout]
            //- remove loading indicator before show alert view cause we can't interact with view after alertview dismiss
            [self hiddenHUDAfterSaved:HUD];
            // Hao Tran[20130509/Fix layout] - END
            
            statusSave = NO;
            UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:msgErrorCheckListDetail delegate:self cancelButtonTitle:msgbtnOK otherButtonTitles: nil]; 
            alert.delegate = self;
            alert.tag = tagAlertHightlightRed;
            [alert show];
            return ENUM_POPUP_SHOW;
            
        } else {
            
            //Hao Tran [20130510/Fix post checklist item] - Fix to post all checklist to WS
            
            //Add last check list items
            for (NSInteger indexSection=0; indexSection < sectionInfoArray.count; indexSection ++) {
                SectionInfoV2 *sectionInfo = [sectionInfoArray objectAtIndex:indexSection];
                
                for (NSInteger indexRow=0; indexRow < sectionInfo.rowHeights.count; indexRow++) {                    
                    CheckListDetailContentModelV2 *model = [sectionInfo.rowHeights objectAtIndex:indexRow];
                    [listCheckListItems addObject:model];
                }
            }
            
            NSMutableArray *postCheckListItems = [NSMutableArray array];
            
            NSInteger iniMandatoryValue = [[UserManagerV2 sharedUserManager].currentUserAccessRight checklistMandatoryPass];
            for (CheckListDetailContentModelV2 *model in listCheckListItems) {
                //Load CheckList Item Score
                CheckListItemCoreDBModelV2 *chkListItemScore = model.itemCoreModel;
                
                //Update Score in CheckList Item Core
                chkListItemScore.chkPostStatus = POST_STATUS_SAVED_UNPOSTED;
                chkListItemScore.chkItemCore = model.itemCoreModel.chkItemCore;
                
                if (iniMandatoryValue > 0) {
                    if (model.chkMandatoryPass > 0) {
                        if (model.itemCoreModel.chkItemCore < model.chkMandatoryPass) {
                            isMandatoryPass = 0;
                        }
                    }
                }
                
                [manager updateCheckListItemCoreByChkItemId:chkListItemScore];
                
                //set checklist itemcore for checklist item
                model.itemCoreModel = chkListItemScore;
                
                //------------------WS-----------------
                //Hao Tran Remove for change WS post checklist
                //[synManager postCheckListItemWithUserId:userId RoomAssignId:roomAssignId AndChkListItem:model];
                
                [postCheckListItems addObject:model];
                //------------------end----------------
                
            }
            //Hao Tran [20130510/Fix post checklist item] - END
            
            //Post checklist with new WS post checklist
            BOOL isSuccess = [synManager postCheckListItemsWithUserId:userId RoomAssignId:roomAssignId AndChkListItems:postCheckListItems];
            
            //Show Total point in rating
            [self calculatorScore];
            
            //Update Score in CheckList Form Score
            ChecklistFormScoreModel *chkFormScore = [manager loadCheckListFormScoreByChkListId:chkList.chkListId AndChkFormId:chkListTypeV2.chkTypeId AndChkUserId:[UserManagerV2 sharedUserManager].currentUser.userId];
			chkFormScore.dfScore = totalPoint;
            chkFormScore.dfIsMandatoryPass = isMandatoryPass;
            if(isSuccess) {
                chkFormScore.dfPostStatus = POST_STATUS_POSTED;
            } else {
                chkFormScore.dfPostStatus = POST_STATUS_SAVED_UNPOSTED;
            }
            
            if (chkFormScore.dfUserId <= 0) { //Don't have checklist in database
                chkFormScore.dfChecklistId = chkList.chkListId;
                chkFormScore.dfFormId = chkListTypeV2.chkTypeId;
                [manager insertCheckListFormScore:chkFormScore];
            } else {
                [manager updateCheckListFormScore:chkFormScore];
            }
            
            //set delegate change value in checklist detail
            if ([delegate respondsToSelector:@selector(didChangeScoreWithCheckList:)]) {
                [delegate didChangeScoreWithCheckList:chkFormScore];
            }
        }
    }
    
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getMsgSaveMessage]];
    [tbvchkDetail reloadData];
    
    //hide saving data.
    [self performSelector:@selector(hiddenHUDAfterSaved:) withObject:HUD afterDelay:0.5];
    
    return ENUM_POPUP_NOT_SHOW;
}

-(enum ENUM_POPUP) saveData {
    //Only once save
    statusChooseChecklistItem = NO;
    if (statusSave == YES) {
        return ENUM_POPUP_NOT_SHOW;
    } else {
        statusSave = YES;
        
        CheckListManagerV2 *chkManager = [[CheckListManagerV2 alloc] init];
        NSMutableArray *arrayFormScore = [chkManager loadChecklistFormScoreByChkListId:chkList.chkListId AndChkUserId:[UserManagerV2 sharedUserManager].currentUser.userId];
        BOOL isHasFormScoreThanZero = FALSE;
        for(ChecklistFormScoreModel *chkFormScore in arrayFormScore) {
            NSString *formScore = [NSString stringWithFormat:@"%d", (int)chkFormScore.dfScore];
            if (![formScore isEqualToString:@"-1"]) {
                isHasFormScoreThanZero = TRUE;
                break;
            }
        }
        
        if(chkList.chkCore == -1 && isHasFormScoreThanZero == FALSE)
        {
            return [self saveCheckListDetail:nil];
        }
    }
    
    return ENUM_POPUP_NOT_SHOW;
}

// Hao Tran[20130510/Remove sections for show] - Remove sections that supervisor was rated so that they can't rate again in view
-(void)removeSectionsDidchoose
{
    NSMutableArray *listSectionsRemove = [NSMutableArray array];
    
    for (NSInteger indexSection = 0; indexSection < sectionInfoArray.count; indexSection ++) {
        SectionInfoV2 *sectionInfo = [sectionInfoArray objectAtIndex:indexSection];
        NSMutableArray *listRowsRemove = [NSMutableArray array];
        
        for (NSInteger indexRow = 0; indexRow < sectionInfo.rowHeights.count; indexRow++) {
            
            CheckListDetailContentModelV2 *model = [sectionInfo.rowHeights objectAtIndex:indexRow];
            CheckListItemCoreDBModelV2 *chkListItemScore = model.itemCoreModel;
            
            if (chkListItemScore.chkItemCore != -1) {
                [listRowsRemove addObject:model];
            }
        }
        
        if([listRowsRemove count] > 0)
        {
            [listCheckListItems addObjectsFromArray:listRowsRemove];
            if([sectionInfo.rowHeights count] == [listRowsRemove count])
            {
                [listSectionsRemove addObject:sectionInfo];
            }
            
            [sectionInfo.rowHeights removeObjectsInArray:listRowsRemove];
        }
    }
    
    if ([listSectionsRemove count] > 0) {
        [sectionInfoArray removeObjectsInArray:listSectionsRemove];
        
        //Set null to reload header view in section to avoid bug layout
        for (SectionInfoV2 *sectionInfo in sectionInfoArray) {
            sectionInfo.headerView = nil;
        }
    }
}
// Hao Tran[20130510/Remove sections for show] - END

#pragma mark - UIAlertView Delegate Methods
-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    switch (alertView.tag) {
        case tagAlertHightlightRed: {
            statusHightlightRed = YES;
            [tbvchkDetail reloadData];
        }
            break;
            
        default:
            break;
    }
}

-(void)calculatorScore {
    totalPossible = 0;
    totalReceived = 0;
    totalPoint = 0;
    
//    for (NSInteger indexSection=0; indexSection < sectionInfoArray.count; indexSection ++) {
//        SectionInfoV2 *sectionInfo = [sectionInfoArray objectAtIndex:indexSection];
//        
//        for (NSInteger indexRow=0; indexRow < sectionInfo.rowHeights.count; indexRow++) {
//            CheckListDetailContentModelV2 *model = [sectionInfo.rowHeights objectAtIndex:indexRow];
    
    for (CheckListDetailContentModelV2 *model in listCheckListItems)
    {
        //Load CheckList Item Score
        CheckListItemCoreDBModelV2 *chkListItemScore = model.itemCoreModel;
        
        if (chkListItemScore.chkItemCore != -1) {
            totalReceived += chkListItemScore.chkItemCore;
        }
        
        if (chkListTypeV2.chkType == tCheckmark) {
            totalPossible += 1;
        } else {
            totalPossible += model.chkDetailContentPointInpected;
        }
    }
    
//#warning dthanhson edit : remove
//    [lblPointsReceived setText:[NSString stringWithFormat:@"%d",totalReceived]];
    
    if (totalPossible > 0) {
        totalPoint = (1.0 * totalReceived / totalPossible)*100.0;
//        [lblPointsPossible setText:[NSString stringWithFormat:@"%d",totalPossible]];
//        [lblPointsTotal setText:[NSString stringWithFormat:@"%.0f %%",totalPoint]];
    }
//#warning end
}

#pragma didden after save.
-(void) hiddenHUDAfterSaved:(MBProgressHUD *)HUD{
    [HUD hide:YES];
    [HUD removeFromSuperview];
}

#pragma mark - set caption view
-(void)setCaptionView
{
    [self setEnableBtnSubmit:isUserInteraction];
    msgbtnOK = [[LanguageManagerV2 sharedLanguageManager] getOK];
    msgSaveCheckListDetail = [[LanguageManagerV2 sharedLanguageManager] getMsgAlertSaveCheckListDetail];
    msgErrorCheckListDetail = [[LanguageManagerV2 sharedLanguageManager] getMsgAlertErrorCheckListDetail];
    
    //refresh topview
    TopbarViewV2 *topview = [self getTopBarView];
    [topview refresh:[[SuperRoomModelV2 alloc] init]];
}

#pragma mark - Handle Topbar Methods
-(void)loadTopbarView {
    TopbarViewV2 *topbar = [[TopbarViewV2 alloc] initViewWithSuperModel:[[CommonVariable sharedCommonVariable] superRoomModel]];
    [topbar setTag:tagTopbarView];
    [self.view addSubview:topbar];
    [topbar setHidden:YES];
    
    [self addButtonHandleShowHideTopbar];
}

-(void)showTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationWillStartSelector:@selector(setHiddenTopbarView)];
    
    f = topbar.frame;
    f.origin.y = 0;
    [topbar setFrame:f];
    [topbar setAlpha:1.0];
    
    [self adjustShowForViews];
    
    [UIView commitAnimations];
}

-(void)hideTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDidStopSelector:@selector(setHiddenTopbarView)];
    [UIView setAnimationDelegate:self];
    
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    [topbar setAlpha:0.0];
    
    [self adjustRemoveForViews];
    
    [UIView commitAnimations];
}

-(void)setHiddenTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    [topbar setHidden:!topbar.hidden];
}

-(TopbarViewV2 *)getTopBarView {
    return (TopbarViewV2 *)[self.view viewWithTag:tagTopbarView];
}

-(void)addButtonHandleShowHideTopbar {
    UIView *vBarButton = [[UIView alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
    UIButton *titleBarButtonFirst = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 200, 30)];
    [titleBarButtonFirst setTag:1];
    titleBarButtonFirst.titleLabel.backgroundColor = [UIColor clearColor];
    [titleBarButtonFirst.titleLabel setFont: FONT_BUTTON_TOPBAR];
    titleBarButtonFirst.titleLabel.textAlignment = NSTextAlignmentCenter;
    [titleBarButtonFirst.titleLabel setAdjustsFontSizeToFitWidth:YES];
    titleBarButtonFirst.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
    
    [titleBarButtonFirst.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
    [titleBarButtonFirst.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
    [titleBarButtonFirst addTarget:self action:@selector(hiddenHeaderView) forControlEvents:UIControlEventTouchUpInside];
    [titleBarButtonFirst setTitle:roomNo forState:UIControlStateNormal];
    
    [vBarButton addSubview:titleBarButtonFirst];
    
    UIButton *titleBarButtonSecond = [[UIButton alloc] initWithFrame:CGRectMake(0, 22, 200, 22)];
    [titleBarButtonSecond setTag:2];
    [titleBarButtonSecond setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [titleBarButtonSecond.titleLabel setFont:FONT_BUTTON_TOPBAR_SECOND];
    [titleBarButtonSecond addTarget:self action:@selector(hiddenHeaderView) forControlEvents:UIControlEventTouchUpInside];
    [titleBarButtonSecond setTitle:[L_CHECKLIST currentKeyToLanguage] forState:UIControlStateNormal];
    
    [vBarButton addSubview:titleBarButtonSecond];
    
    [self.navigationItem setTitleView:vBarButton];
    //Move title to center vertical
    CGRect titleViewBounds = self.navigationItem.titleView.bounds;
    int moveLeftTitle = 0;
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        moveLeftTitle = 10;
    }
    self.navigationItem.titleView.bounds = CGRectMake(titleViewBounds.origin.x + moveLeftTitle, Y_TOPBAR_TITLE, titleViewBounds.size.width, titleViewBounds.size.height);
}

- (void) hiddenHeaderView {
    BOOL isHidden = [self getTopBarView].hidden;
    [[self getTopBarView] setHidden:!isHidden];
    
    CGRect tbvchkDetailFrame = tbvchkDetail.frame;
    
    CGFloat scale;
    if (isHidden) {
        scale = 45;
    }
    else {
        scale = -45;
    }
    
    tbvchkDetailFrame.origin.y += scale;
    tbvchkDetailFrame.size.height -= scale;
    
    [tbvchkDetail setFrame:tbvchkDetailFrame];
}

//-(void)addButtonHandleShowHideTopbar {
//    UIButton *titleBarButton = [[UIButton alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
//    
//    titleBarButton.titleLabel.backgroundColor = [UIColor clearColor];
//    titleBarButton.titleLabel.font = FONT_BUTTON_TOPBAR;
//    titleBarButton.titleLabel.textAlignment = NSTextAlignmentCenter;
//    titleBarButton.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
//    [titleBarButton.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
//    [titleBarButton.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
//    [titleBarButton.titleLabel setMinimumFontSize:10];
//    [titleBarButton.titleLabel setAdjustsFontSizeToFitWidth:YES];
//    [titleBarButton addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
//    [titleBarButton setTitle:[self title] forState:UIControlStateNormal];
//    self.navigationItem.titleView = titleBarButton;
//}

-(void)topbarTapped:(UIButton *)sender {
    TopbarViewV2 *topbar = [self getTopBarView];
    [sender setEnabled:NO];
    if (topbar.isHidden) {
        [self showTopbarView];
    } else {
        [self hideTopbarView];
    }
    [self performSelector:@selector(topbarButtonEnable:) withObject:sender afterDelay:0.5];
}

-(void) topbarButtonEnable:(UIButton *) topbarBtn {
    [topbarBtn setEnabled:YES];
}

-(void)adjustShowForViews {
    //adjust all views when show 
    TopbarViewV2 *topbar = [self getTopBarView];
    
    CGRect f = topbar.frame;
//#warning dthanhson edit : remove
//    CGRect fvChklistDetail = vHeaderChkListDetail.frame; 
//    [vHeaderChkListDetail setFrame:CGRectMake(0, f.size.height, 320, fvChklistDetail.size.height)];
//#warning end

    CGRect ftbv = tbvchkDetail.frame;
    [tbvchkDetail setFrame:CGRectMake(0, f.size.height, 320, ftbv.size.height - f.size.height)];
}

-(void)adjustRemoveForViews { 
    //adjust all views when hide
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
//#warning dthanhson edit : remove
//    CGRect fvChklistDetail = vHeaderChkListDetail.frame;
//    [vHeaderChkListDetail setFrame:CGRectMake(0, 0, 320, fvChklistDetail.size.height)];
//#warning end
    
    CGRect ftbv = tbvchkDetail.frame;
    [tbvchkDetail setFrame:CGRectMake(0, 0, 320, ftbv.size.height + f.size.height)];
}

-(BOOL)allowHomeButtonPress {
    return NO;
}

-(IBAction) btnSubmitClicked:(id)sender
{
    
    RoomAssignmentModelV2 *roomAssignmentModel = [[RoomAssignmentModelV2 alloc] init];
    roomAssignmentModel.roomAssignment_Id = (int)[TasksManagerV2 getCurrentRoomAssignment];
    roomAssignmentModel.roomAssignment_UserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    
    [[RoomManagerV2 sharedRoomManager] load:roomAssignmentModel];
    roomAssignmentModel.roomAssignmentChecklistRemark = self.tvRemark.text;
    [[RoomManagerV2 sharedRoomManager] update:roomAssignmentModel];

    //Hao Tran[20130509/Fix Crash] - Check save before leave this view, if alert require save befor quit then no popViewController
    if([self saveData] == ENUM_POPUP_NOT_SHOW)
    {
        
        //Pop to Room Detail View Controller after submit checklist successful
        NSArray *viewControllers = self.navigationController.viewControllers;
        for (UIViewController *curVC in viewControllers) {
            if([curVC isKindOfClass:[RoomAssignmentInfoViewController class]]) {
             
                [self.navigationController popToViewController:curVC animated:YES];
                break;
            }
        }
    }
    //Hao Tran[20130509/Fix Crash] - END
}

- (IBAction)btnRemark:(id)sender {
    if (remarkView == nil) {
        remarkView = [[RemarkViewV2 alloc] initWithNibName:@"RemarkViewV2" bundle:nil];
    }
    UIView *view = remarkView.view;
    [remarkView setDelegate:self];
    //[remarkView setTextinRemark:txtRemark.text != nil ? txtRemark.text : @""];
    [remarkView setTextinRemark:remark != nil ? remark : @""];
    [remarkView viewWillAppear:YES];
    remarkView.isAddSubview = YES;
    [self.tabBarController.view addSubview:view];
}
#pragma mark - Remark View Delegate
-(void)remarkViewV2DoneWithText:(NSString *)text andController:(UIViewController *)controllerView{
    
    //show wifi view for fix no wifi view
    if(!isDemoMode){
        [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationShowWifiView] object:nil];
    }
    //txtRemark.text = text;
    remark = text;
    if(remarkView) {
        [remarkView setDelegate:nil];
    }
    [self.tvRemark setFont:[UIFont fontWithName:@"Helvetica-Bold" size:15]];
    [self.tvRemark setBackgroundColor:[UIColor clearColor]];
    [self.tvRemark setTextColor:[UIColor grayColor]];
    [self.tvRemark setText:text];
    //[tblEngineer reloadData];
}
-(void) setEnableBtnSubmit:(BOOL) isEnable
{
    [btnSubmit setUserInteractionEnabled:isEnable];
    if (isEnable) {
        [btnSubmit setBackgroundImage:[UIImage imageBeforeiOS7:imgBtnSubmit equaliOS7:imgBtnSubmitFlat] forState:UIControlStateNormal];
    }
    else {
        [btnSubmit setBackgroundImage:[UIImage imageBeforeiOS7:imgBtnSubmitDisable equaliOS7:imgBtnSubmitDisableFlat] forState:UIControlStateNormal];
    }
}


@end
