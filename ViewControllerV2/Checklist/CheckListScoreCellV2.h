//
//  CheckListScoreCellV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CheckListScoreCellV2 : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblNameScoreCheckList;
@property (strong, nonatomic) IBOutlet UILabel *lblScoreCheckList;
@property (strong, nonatomic) IBOutlet UIImageView *imgStatusCheckList;
@property (strong, nonatomic) IBOutlet UILabel *lblLineCheckList;

@end
