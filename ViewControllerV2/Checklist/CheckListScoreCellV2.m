//
//  CheckListScoreCellV2.m
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CheckListScoreCellV2.h"

@implementation CheckListScoreCellV2
@synthesize lblNameScoreCheckList;
@synthesize lblScoreCheckList;
@synthesize imgStatusCheckList;
@synthesize lblLineCheckList;


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc {
//    [super dealloc];
}
@end
