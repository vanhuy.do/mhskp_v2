//
//  CheckListViewV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MenuCheckListCellV2.h"
#import "CheckListModelV2.h"
#import "CheckListTypeModelV2.h"
#import "CheckListDetailViewV2.h"
#import "CheckListScoreCellV2.h"
#import "MBProgressHUD.h"
#import "CommonVariable.h"
#import "SyncManagerV2.h"

@interface CheckListViewV2 : UIViewController<UITableViewDataSource,UITableViewDelegate, UIAlertViewDelegate, CheckListDetailViewV2Delegate>{
    NSMutableArray *datas;
    CheckListModelV2 *chkModel;
    BOOL statusChangeChkListDetail;
}

@property (strong, nonatomic) IBOutlet UIImageView *imgViewChecListScore;
@property (strong, nonatomic) IBOutlet UITableView *tbvScoreCheckList;
@property (nonatomic, strong) CheckListModelV2 *chkModel;
@property (nonatomic, strong) NSMutableArray *datas;
@property (strong, nonatomic) IBOutlet UITableView *tbvChecklist;
@property (strong, nonatomic) IBOutlet UILabel *lblProcessAudit;
@property (strong, nonatomic) IBOutlet UILabel *lblShortForm;
@property (strong, nonatomic) IBOutlet UILabel *lblLongForm;
@property (strong, nonatomic) IBOutlet UILabel *lblOveral;
@property (strong, nonatomic) IBOutlet UILabel *lblCheckListStatus;
@property (strong, nonatomic) IBOutlet UIImageView *imgChkStatus;
@property (strong, nonatomic) IBOutlet UILabel *lblResultProcessAudit;
@property (strong, nonatomic) IBOutlet UILabel *lblResultShortForm;
@property (strong, nonatomic) IBOutlet UILabel *lblResultLongForm;
@property (strong, nonatomic) IBOutlet UILabel *lblResultOverall;
@property (readwrite, nonatomic) NSInteger row;
@property (readwrite, nonatomic) NSInteger totalOveral;
@property (readwrite, nonatomic) BOOL statusSaveCheckList;
@property (strong, nonatomic) NSString *msgSaveCheckList;
@property (strong, nonatomic) NSString *msgbtnOk;
@property (readwrite, nonatomic) BOOL statusChangeChkListDetail;
@property (nonatomic, strong) NSString *roomNo;
@property (nonatomic, assign) NSInteger roomType;

-(void) loadDataCheckList;
-(void) saveStatusCheckList;
-(void) setCaptionView;
-(BOOL) isNotSavedAndExit;
@end
