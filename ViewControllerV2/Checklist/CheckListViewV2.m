 //
//  CheckListViewV2.m
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CheckListViewV2.h"
#import "CheckListManagerV2.h"
#import "CheckListScoreCellV2.h"
#import "RoomManagerV2.h"
#import "CustomAlertViewV2.h"
#import "ChecklistFormScoreModel.h"
#import "TasksManagerV2.h"
//#import "HomeViewV2.h"
#import "MyNavigationBarV2.h"
#import "DeviceManager.h"

#define imgStatusPass @"pass_icon.png"
#define imgStatusFail @"fail_icon.png"
#define tagAlertBack 12

@interface CheckListViewV2 (PrivateMethods)

#define tagTopbarView 1234

-(void) loadTopbarView;
-(void) showTopbarView;
-(void) hideTopbarView;
-(void) setHiddenTopbarView;
-(void) adjustShowForViews;
-(void) adjustRemoveForViews;
-(void) addButtonHandleShowHideTopbar;
-(void) topbarTapped:(UIButton *) sender;
-(TopbarViewV2 *) getTopBarView;
-(void) saveData;

@end

@implementation CheckListViewV2
@synthesize tbvChecklist;
@synthesize lblProcessAudit;
@synthesize lblShortForm;
@synthesize lblLongForm;
@synthesize lblOveral;
@synthesize lblCheckListStatus;
@synthesize imgChkStatus;
@synthesize lblResultProcessAudit;
@synthesize lblResultShortForm;
@synthesize lblResultLongForm;
@synthesize lblResultOverall;
@synthesize datas;
@synthesize imgViewChecListScore;
@synthesize tbvScoreCheckList;
@synthesize chkModel;
@synthesize row;
@synthesize totalOveral;
@synthesize statusSaveCheckList;
@synthesize msgbtnOk;
@synthesize msgSaveCheckList;
@synthesize statusChangeChkListDetail;
@synthesize roomNo;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    
    return self;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle
- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    //Hightlight home button
    if([CommonVariable sharedCommonVariable].roomIsCompleted != 1) {
        [[HomeViewV2 shareHomeView] setSelectedButton:tagOfHomeButton];
    }
    
//#warning dthanhson edit : insert
    [imgViewChecListScore setHidden:YES];
    [tbvScoreCheckList setHidden:YES];
//#warning end

    
    
    [self setCaptionView];
    
    //show wifi view
    if(!isDemoMode){
        [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationShowWifiView] object:nil];
    }
    
    [tbvChecklist reloadData];
}

-(void) loadDataCheckListFormService:(MBProgressHUD *) HUD {    
    //enable flag to announce, is loading data
    [[HomeViewV2 shareHomeView] waitingLoadingData];    
    
    SyncManagerV2 *synManager = [[SyncManagerV2 alloc] init];
    CheckListManagerV2 *manager = [[CheckListManagerV2 alloc] init];
    
    NSMutableArray *arrayChkList = [NSMutableArray array];
//    NSMutableArray *arrayRoomModel = [[NSMutableArray alloc] init];
    
    NSInteger userId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    NSInteger hotelId = [[[UserManagerV2 sharedUserManager] currentUser] userHotelsId];
    
    //check checklist exist or not
    chkModel = [[CheckListModelV2 alloc] init];
    chkModel.chkListId = [TasksManagerV2 getCurrentRoomAssignment];
    chkModel.chkListUserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    [manager loadCheckListData:chkModel];
    
    //load checklist form score
    NSMutableArray *arrayChkFormScore = [manager loadChecklistFormScoreByChkListId:chkModel.chkListId AndChkUserId:userId];
    
    if (arrayChkFormScore.count == 0) {                
        //load all room assign
        NSMutableArray *arrayRoomAssign = nil;
        [[RoomManagerV2 sharedRoomManager] getRoomInspection];
        arrayRoomAssign = [NSMutableArray arrayWithArray:[[RoomManagerV2 sharedRoomManager] roomAssignmentList]];
        
        // <<< insert checklist by all room >>>
//        for (NSDictionary *roomAssign in arrayRoomAssign) {            
//            //load room model
//            RoomModelV2 *roomModel = [[RoomModelV2 alloc] init];
//            roomModel.room_Id = [[roomAssign objectForKey:kRoomNo] integerValue];
//            [[RoomManagerV2 sharedRoomManager] loadRoomModel:roomModel];
//            
//            //load data of checkList by roomAssignId
//            CheckListModelV2 *chkModelWS = [[CheckListModelV2 alloc] init];
//            chkModelWS.chkListId  = [[roomAssign objectForKey:kRoomAssignmentID] integerValue];
//            chkModelWS.chkListUserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
//            [manager loadCheckListData:chkModelWS];
//            
//            if (chkModelWS.chkListRoomId == 0) {                
//                //Insert CheckList
//                chkModelWS = [[CheckListModelV2 alloc] init];
//                chkModelWS.chkListId = [[roomAssign objectForKey:kRoomAssignmentID] integerValue];
//                chkModelWS.chkListRoomId  = roomModel.room_Id;
//                chkModelWS.chkListUserId = userId;
//                chkModelWS.chkListStatus = tNotCompleted;
//                chkModelWS.chkCore = -1;
//                
//                [manager insertCheckList:chkModelWS];
//            }
//            
//            //load all room model
//            [arrayRoomModel addObject:roomModel];            
//        }
        
        //Get all checklist from database
        arrayChkList = [manager loadAllCheckListByCurrentUserData];
        
        //--------WS----------
        //Get checklist form score from WS
//        [synManager getCheckListRoomTypeWithUserId:userId HotelId:hotelId AndArrayRoomModel:arrayRoomModel AndArrayChkList:arrayChkList];
    }
    
    //Get all checkList form from WS
    [synManager getAllCheckListFormWithUserId:userId AndHotelId:hotelId];
    
    //[CRF-00001159/20150119] - Get Checklist RoomType from WS
    CheckListManagerV2 *checklistManager = [[CheckListManagerV2 alloc] init];
    [checklistManager getCheckListRoomTypeWSWithUserId:userId hotelId:hotelId lastModified:nil];
    
    //-------end----------
        
    //Get all checkList categories from WS
    //Remove for new ws checklist
    //[synManager getAllCheckListCategoriesWithUserId:userId AndHotelId:hotelId];
    //-------------end---------------
    
    //Get all checkList items from WS
    //Remove for new ws checklist items
    //[synManager getAllCheckListItemWithUserId:userId AndHotelId:hotelId];
    //-------------end---------------
    
    [self loadDataCheckList];
    
    //hide saving data.
    [self performSelector:@selector(hiddenHUDAfterSaved:) withObject:HUD afterDelay:0.5];
    
    //end loading data
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //Resize content views of current VC to fit with Navigation bar and tabbar
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    // for check is back from check list 
    [CommonVariable sharedCommonVariable].isBackFromChk = YES;
    
    //[self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:imgBackground]]];
    
    //set title bar
    NSString *chkListTitle = [NSString stringWithFormat:@"%@ %@",[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_CHECKLIST], [RoomManagerV2 getCurrentRoomNo]];
    [self setTitle:chkListTitle];

    //back button navigation
    self.navigationItem.leftBarButtonItem = [MyNavigationBarV2 createBackButtonWithTarget:self selector:@selector(backBarPressed) forControlEvents:UIControlEventTouchUpInside];
    
    [self performSelector:@selector(loadTopbarView)];
    
    MBProgressHUD	*HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getRaLoadingData]];
    [HUD show:YES];
    [self.tabBarController.view addSubview:HUD];
    
    [self performSelector:@selector(loadDataCheckListFormService:) withObject:HUD afterDelay:0.5];
}

-(BOOL) isNotSavedAndExit {
    if (statusSaveCheckList == YES || chkModel.chkCore > 0 || datas.count == 0) {
        return FALSE;//saved
    } else {
        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObject:[UIImage imageNamed:@"yes_bt.png"]] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:msgSaveCheckList delegate:self cancelButtonTitle:msgbtnOk otherButtonTitles:nil, nil]; 
        alert.delegate = self;
        alert.tag = 10000;
        [alert show];
        return TRUE;
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Check Check List
-(BOOL) checkRoomStatus {
    RoomAssignmentModelV2 *ramodel = [[RoomAssignmentModelV2 alloc] init];
    ramodel.roomAssignment_Id = (int)[TasksManagerV2 getCurrentRoomAssignment];
    ramodel.roomAssignment_UserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    [[RoomManagerV2 sharedRoomManager] load:ramodel];
    
    if(ramodel.roomAssignmentRoomInspectionStatusId == ENUM_INSPECTION_COMPLETED_FAIL || ramodel.roomAssignmentRoomInspectionStatusId == ENUM_INSPECTION_COMPLETED_PASS) {
        return YES;
    }

    return NO;
}

-(BOOL) isCheckChkList {
    CheckListManagerV2 *manager = [[CheckListManagerV2 alloc] init];
    CheckListModelV2 *chkListCheck = [[CheckListModelV2 alloc] init];
    chkListCheck.chkListId = [TasksManagerV2 getCurrentRoomAssignment];
    chkListCheck.chkListUserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    [manager loadCheckListData:chkListCheck];
    
    if ([self checkRoomStatus] == YES || datas.count <= 0 || statusChangeChkListDetail == NO) {
        return YES;
    } else {
        return NO;
    }
}

#pragma mark - Button back
-(void)backBarPressed {
    //need to save before leaving
    BOOL saveChkList = [self isCheckChkList];
    if(saveChkList == YES) {
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObject:[UIImage imageNamed:@"yes_bt.png"]] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:msgSaveCheckList delegate:self cancelButtonTitle:msgbtnOk otherButtonTitles:nil, nil]; 
        alert.delegate = self;
        alert.tag = tagAlertBack;
        [alert show];
    }    
}

#pragma mark - === Handle Check Save Data ===
#pragma mark
-(BOOL) checkSaveDataWithTagEvent:(NSInteger) tagOfEvent {
    //YES
    //need to save before leaving
    BOOL saveChkList = [self isCheckChkList];
    if (tagOfEvent == tagOfMessageButton) {
        if (saveChkList == YES) {
            [[CommonVariable sharedCommonVariable] setRoomIsCompleted:0];
            return NO;
        } else {
            UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObject:[UIImage imageNamed:@"yes_bt.png"]] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:msgSaveCheckList delegate:self cancelButtonTitle:msgbtnOk otherButtonTitles:nil, nil]; 
            alert.delegate = self;
            alert.tag = 10000;
            [alert show];            
            return YES;
        }
    } else { 
        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObject:[UIImage imageNamed:@"yes_bt.png"]] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_complete_room_to_leave] delegate:self cancelButtonTitle:msgbtnOk otherButtonTitles:nil, nil]; 
        alert.delegate = self;
        alert.tag = 10000;
        [alert show];
        //when go this screen can not move to another screen
        return YES;
    }
}

#pragma mark - UITableView Delegate methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

//#warning dthanhson edit : remove
//    if (tableView == tbvScoreCheckList) {
//        TopbarViewV2 *topbar = [self getTopBarView];
//        CGRect f = CGRectZero;
//        if ([topbar isHidden] == YES) {
//            f = CGRectZero;
//        } else {
//            f = [topbar frame];
//        }
//        
//        if (statusSaveCheckList == YES) {
//            //if checklist saved, table score checklist will has row checklist status
//            NSInteger height = (datas.count + 2) * 30;
////            [tbvChecklist setFrame:CGRectMake(0, 0, 320, 387 - height)];
//            [tbvChecklist setFrame:CGRectMake(0, f.size.height, 320, 387 - height - f.size.height)];
//            [tbvScoreCheckList setFrame:CGRectMake(0, 387 - height, 320, height)];
//            [imgViewChecListScore setFrame:CGRectMake(0, 387 - height, 320, height)];
//            return datas.count + 2;
//        } else {
//            //checklist does not save, table score checklist will not row checklist status
//             NSInteger height = (datas.count + 1) * 30;
////            [tbvChecklist setFrame:CGRectMake(0, 0, 320, 387 - height)];
//            [tbvChecklist setFrame:CGRectMake(0, f.size.height, 320, 387 - height - f.size.height)];
//            [tbvScoreCheckList setFrame:CGRectMake(0, 387 - height, 320, height)];
//            [imgViewChecListScore setFrame:CGRectMake(0, 387 - height, 320, height)];
//            return datas.count + 1;
//        }
//    }
//#warning end
    
    return datas.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//#warning dthanhson edit : remove
//    if(tableView == tbvScoreCheckList) {
//        return 30;
//    }
//#warning end
    
    return 56;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
//#warning dthanhson edit : remove
//    if(tableView == tbvScoreCheckList) {
//        //table score
//        static NSString *identifyScoreCell = @"IdentifyScoreCell";
//        CheckListScoreCellV2 *cell = nil;
//        cell = [tableView dequeueReusableCellWithIdentifier:identifyScoreCell];
//        if (cell == nil) {
//            NSArray *array = [[NSBundle mainBundle] loadNibNamed:@"CheckListScoreCellV2" owner:self options:nil];
//            cell = [array objectAtIndex:0];
//        }
//        
//        //config data
//        row = indexPath.row;
//        [cell.lblLineCheckList setHidden:YES];
//                
//        //set line for group checklist has same kind (group checkmark, group rating)
//        CheckListTypeModelV2 *model = [[CheckListTypeModelV2 alloc] init];
//        if (datas.count > indexPath.row) {
//            model = [datas objectAtIndex:indexPath.row];
//        }
//        
//        if (indexPath.row <= datas.count) {
//            if (datas.count > indexPath.row + 1) {
//                CheckListTypeModelV2 *nextModel = [datas objectAtIndex:indexPath.row + 1];
//                if (model.chkType == tCheckmark && nextModel.chkType == tRating) {
//                    [cell.lblLineCheckList setHidden:NO];
//                } else {
//                    [cell.lblLineCheckList setHidden:YES];
//                }
//            }
//        }
//        
//        //set line for checklist status
//        if (row == datas.count - 1) {
//            [cell.lblLineCheckList setHidden:NO];
//        }
//        
//        //----Load CheckList Form Score and set score on view
//        CheckListManagerV2 *chkManager = [[CheckListManagerV2 alloc] init];
//        ChecklistFormScoreModel *chkListFormScore = [chkManager loadCheckListFormScoreByChkListId:chkModel.chkListId AndChkFormId:model.chkTypeId AndChkUserId:[UserManagerV2 sharedUserManager].currentUser.userId];
//        
//        //set language name checklist form
//        if ([[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString: (NSString *)ENGLISH_LANGUAGE] == NO) {
//            [cell.lblNameScoreCheckList setText:[NSString stringWithFormat:@"%@ %%:", model.chkTypeLangName]];
//        } else {
//            [cell.lblNameScoreCheckList setText:[NSString stringWithFormat:@"%@ %%:", model.chkTypeName]];
//        }
//        
//        [cell.lblScoreCheckList setText:[NSString stringWithFormat:@"%d %%", chkListFormScore.dfScore < 0 ? 0 : chkListFormScore.dfScore]];
//        
//        //when table score checklist has no checklist status
//        if (row == datas.count) {            
//            [cell.lblNameScoreCheckList setText:[NSString stringWithFormat:@"%@",[[LanguageManagerV2 sharedLanguageManager] getOverral]]];
//            
//            [cell.lblScoreCheckList setText:[NSString stringWithFormat:@"%d %%",chkModel.chkCore < 0 ? 0 : chkModel.chkCore]];
//            
//            return cell;
//        }
//        
//        //set checklist status when score >= 90
//        if (row == datas.count + 1) {
//            [cell.lblNameScoreCheckList setText:[NSString stringWithFormat:@"%@",[[LanguageManagerV2 sharedLanguageManager] getChecklistStatus]]];
//            if (chkModel.chkListStatus == tChkPass) {
//                [cell.imgStatusCheckList setImage:[UIImage imageNamed:imgStatusPass]];
//            } else if (chkModel.chkListStatus == tChkFail) {
//                [cell.imgStatusCheckList setImage:[UIImage imageNamed:imgStatusFail]];
//            }
//            
//            [cell.lblScoreCheckList setHidden:YES];
//            
//            return cell;
//        }
//                
//        return cell;
//    }
//#warning end
    
    //table menu checklist
    static NSString *cellIndentify = @"cellIndentify";
    MenuCheckListCellV2 *cell = nil;
    cell = [tableView dequeueReusableCellWithIdentifier:cellIndentify];
    if(cell == nil) {
        NSArray *array = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([MenuCheckListCellV2 class]) owner:self options:nil];
        cell = [array objectAtIndex:0];
        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
            [cell.imgBgCell setImage:[UIImage imageNamed:imgBgBigBtnFlat]];
            [cell.imgStatusCheckList setImage:[UIImage imageNamed:imgCompleteFlat]];
        }
        else
        {
            [cell.imgBgCell setImage:[UIImage imageNamed:imgBgBigBtn]];
            [cell.imgStatusCheckList setImage:[UIImage imageNamed:imgComplete]];
        }
    }
    
    CheckListTypeModelV2 *model = [[CheckListTypeModelV2 alloc] init];
    if (datas.count > indexPath.row) {
        model = [datas objectAtIndex:indexPath.row];
    }

    //----Load CheckList Form Score and set score on view
    CheckListManagerV2 *chkManager = [[CheckListManagerV2 alloc] init];
    ChecklistFormScoreModel *chkListFormScore = [chkManager loadCheckListFormScoreByChkListId:chkModel.chkListId AndChkFormId:model.chkTypeId AndChkUserId:[UserManagerV2 sharedUserManager].currentUser.userId];
    
    //set status of checklist when checked
    NSString *formScore = [NSString stringWithFormat:@"%d", (int)chkListFormScore.dfScore];
    if(chkListFormScore.dfUserId == 0) {
        [cell.imgStatusCheckList setHidden:YES];
    } else if (![formScore isEqualToString:@"-1"]) {
        [cell.imgStatusCheckList setHidden:NO];
    } else{
        [cell.imgStatusCheckList setHidden:YES];
    }
    
    //set checklist is checkmark or rating
    if (model.chkType == tCheckmark) {
        [cell.imgCheckList setImage:[UIImage imageNamed:imgCheckMark]];
    } else {
        [cell.imgCheckList setImage:[UIImage imageNamed:imgRating] ];
    }
    
    //set language name checklist
    if ([[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString: (NSString *)ENGLISH_LANGUAGE] == NO) {
         [cell.lblNameChecklist setText:model.chkTypeLangName];
    } else {
        [cell.lblNameChecklist setText:model.chkTypeName];
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self navigateToCheckListDetail:indexPath];
}

-(void) navigateToCheckListDetail:(NSIndexPath*)indexPath
{
    MBProgressHUD	*HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getRaLoadingData]];
    [HUD show:YES];
    [self.tabBarController.view addSubview:HUD];
    
    CheckListManagerV2 *manager = [[CheckListManagerV2 alloc] init];
    //load checklist form row
    CheckListDetailViewV2 *chkDetailV2 = [[CheckListDetailViewV2 alloc]initWithNibName:NSStringFromClass([CheckListDetailViewV2 class]) bundle:nil];
    [chkDetailV2 setRoomNo:roomNo];
    
    CheckListTypeModelV2 *model = [[CheckListTypeModelV2 alloc] init];
    if (datas.count > indexPath.row) {
        model = [datas objectAtIndex:indexPath.row];
    }
    
    SyncManagerV2 *synManager = [[SyncManagerV2 alloc] init];
    NSInteger hotelId = [[[UserManagerV2 sharedUserManager] currentUser] userHotelsId];
    NSInteger userId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    
    //WS get checklist
    [synManager getCheckListCategoriesWithUserId:userId AndHotelId:hotelId AndChecklistId:(int)model.chkTypeId];
    [synManager getCheckListItemsWithUserId:userId AndHotelId:hotelId AndChecklistId:(int)model.chkTypeId];
    
    chkDetailV2.chkListTypeV2 = model;
    
    //get checklist for checklist item
    chkDetailV2.chkList = chkModel;
    
    //get score of checklist form score
    ChecklistFormScoreModel *chkFormScore = [manager loadCheckListFormScoreByChkListId:chkModel.chkListId AndChkFormId:model.chkTypeId AndChkUserId:[UserManagerV2 sharedUserManager].currentUser.userId];
    
    //set status save of checklist form
    if (chkFormScore.dfScore > 0) {
        chkDetailV2.statusSave = YES;
    } else {
        chkDetailV2.statusSave = NO;
    }
    
    //button back
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK] style:UIBarButtonItemStyleBordered target:nil action:nil];
    self.navigationItem.backBarButtonItem = backButton;
    
    //set delegate for checklist detail
    [chkDetailV2 setDelegate:self];
    
    [HUD hide:NO];
    [HUD removeFromSuperview];
    
    [self.navigationController pushViewController:chkDetailV2 animated:YES];
}

#pragma mark - Load Data for CheckList
-(void)loadDataCheckList {    
    CheckListManagerV2 *manager = [[CheckListManagerV2 alloc] init];
    datas = [[NSMutableArray alloc] init];
    NSInteger totalPointCheckListType = 0;
        
    //Load List CheckList FormScore by CheckListId
    chkModel = [[CheckListModelV2 alloc] init];
    chkModel.chkListId  = [TasksManagerV2 getCurrentRoomAssignment];
    chkModel.chkListUserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    [manager loadCheckListData:chkModel];
    
    if (chkModel.chkListRoomId.length <= 0) {
        RoomAssignmentModelV2 *roomAssignmentModel = [[RoomAssignmentModelV2 alloc] init];
        roomAssignmentModel.roomAssignment_Id = (int)[TasksManagerV2 getCurrentRoomAssignment];
        roomAssignmentModel.roomAssignment_UserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
        
        [[RoomManagerV2 sharedRoomManager] load:roomAssignmentModel];
        
        chkModel = [[CheckListModelV2 alloc] init];
        chkModel.chkListId = roomAssignmentModel.roomAssignment_Id;
        chkModel.chkListRoomId  = roomAssignmentModel.roomAssignment_RoomId;
        chkModel.chkListUserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
        
        int inspectionStatusId = (int)roomAssignmentModel.roomAssignmentRoomInspectionStatusId;
        if (inspectionStatusId == ENUM_INSPECTION_COMPLETED_PASS) {
            chkModel.chkListStatus = tChkPass;
        }
        else if (inspectionStatusId == ENUM_INSPECTION_COMPLETED_FAIL) {
            chkModel.chkListStatus = tChkFail;
        }
        else {
            chkModel.chkListStatus = tNotCompleted;
        }
        
        chkModel.chkPostStatus = POST_STATUS_UN_CHANGED;
        chkModel.chkCore = -1;
        [manager insertCheckList:chkModel];
    }
    
    NSMutableArray *arrayChkFormScore = [manager loadChecklistFormScoreByChkListId:chkModel.chkListId AndChkUserId:[UserManagerV2 sharedUserManager].currentUser.userId];
    
    // Son Nguyen Edit for just get all check list from
    //self.datas = [manager loadAllCheckListForm];
    //Hao Tran[20141120/Fixed checklist duplicated with different Hotel] - Fixed checklist duplicated with different Hotel for deployment RC
    self.datas = [manager loadChecklistFormByUserId:[UserManagerV2 sharedUserManager].currentUser.userId roomTypeId:(int)self.roomType];
    
    if ([self.datas count] <= 0) {
        self.datas = [manager loadCheckListFormDataByHotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId];
    }
    
//        for (ChecklistFormScoreModel *formScoreModel in arrayChkFormScore) {
//           
//            //load Checklist Form by CheckList FormScore
//            CheckListTypeModelV2 *chkListType = [[CheckListTypeModelV2 alloc] init];
//            chkListType.chkTypeId = formScoreModel.dfFormId;
//           [manager loadCheckListForm:chkListType];
//            [self.datas addObject:chkListType];
//        }
    

    //set score checklist if checklist form score have score
    BOOL isSaveFormScore = NO;
    
    //Get Checklist form by Id
    CheckListTypeModelV2 *chkListType = nil;
    CheckListManagerV2 *chkManager = [[CheckListManagerV2 alloc] init];
    for (ChecklistFormScoreModel *formScoreModel in arrayChkFormScore) {
        if (formScoreModel.dfScore > -1) {
            chkListType = [[CheckListTypeModelV2 alloc] init];
            chkListType.chkTypeId = formScoreModel.dfFormId;
            chkListType = [chkManager loadCheckListForm:chkListType];
            isSaveFormScore = YES;
            totalPointCheckListType += formScoreModel.dfScore;
        } else {
            isSaveFormScore = NO;
            break;
        }
    }
    
    if (chkModel.chkCore == -1 && isSaveFormScore == YES) {
        chkModel.chkCore = (totalPointCheckListType * 1.0 / datas.count);
        if (chkListType != nil && chkModel.chkCore >= chkListType.chkTypePassingScore) {
            chkModel.chkListStatus = chkStatusPass;//tChkPass;
        } else {
            chkModel.chkListStatus = chkStatusFail;//tChkFail;
        }
        [manager updateCheckList:chkModel];
    }
    
    //Set Status Save for CheckList Status
    if (chkModel.chkListStatus == chkStatusFail || chkModel.chkListStatus == chkStatusPass) {
        statusSaveCheckList = YES;
    } else {
        statusSaveCheckList = NO;
    }
    
    //set height for table score checklist and table checklist
// NSInteger height = (datas.count + 2) * 30;
// [tbvChecklist setFrame:CGRectMake(0, 0, 320, 387 - height)];

//#warning dthanhson edit : remove
//    [tbvScoreCheckList setFrame:CGRectMake(0, 387 - height, 320, height)];
//    [imgViewChecListScore setFrame:CGRectMake(0, 387 - height, 320, height)];
//    
//    [tbvScoreCheckList reloadData];
//#warning end
    
    [tbvChecklist reloadData];
    //}
    
    //Insert CheckList Form Score
    NSMutableArray *arrayChkFrom = [manager loadAllCheckListForm];
    
    for (CheckListTypeModelV2 *chkTypeModel in arrayChkFrom) {
        ChecklistFormScoreModel *chkFormScore = [manager loadCheckListFormScoreByChkListId:chkModel.chkListId AndChkFormId:chkTypeModel.chkTypeId AndChkUserId:[UserManagerV2 sharedUserManager].currentUser.userId];
        
        if(chkFormScore.dfId == 0) {
            chkFormScore.dfFormId = chkTypeModel.chkTypeId;
            chkFormScore.dfChecklistId = chkModel.chkListId;
            chkFormScore.dfUserId = [UserManagerV2 sharedUserManager].currentUser.userId;
            chkFormScore.dfScore = -1;
            chkFormScore.dfPostStatus = POST_STATUS_SAVED_UNPOSTED;
            [manager insertCheckListFormScore:chkFormScore];
        }
    }
}

-(void)saveStatusCheckList {
    // @ show loading data .
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getRaSavingData]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES];  
    
    CheckListManagerV2 *manager = [[CheckListManagerV2 alloc] init];
    NSInteger totalPointCheckListType = 0;
    totalOveral = 0;
    
    BOOL isCheckListPass = YES;
    CheckListTypeModelV2 *chkListType = nil;
    //get score from checklist form score to calculate total point
    for (CheckListTypeModelV2 *model in datas) {        
        ChecklistFormScoreModel *chkFormScore = [manager loadCheckListFormScoreByChkListId:chkModel.chkListId AndChkFormId:model.chkTypeId AndChkUserId:[UserManagerV2 sharedUserManager].currentUser.userId];
        
        if(chkFormScore.dfScore >= 0) {            
            totalPointCheckListType += chkFormScore.dfScore;
            chkListType = model;
            
            if([[UserManagerV2 sharedUserManager].currentUserAccessRight checklistMandatoryPass] > 0) {
                if (chkFormScore.dfIsMandatoryPass <= 0) {
                    isCheckListPass = NO;
                }
            }
            
        }
    }
    
    //total score of checklist (Overal)
    if (datas.count == 0) {
        totalOveral = 0;
    } else {
//        totalOveral = (totalPointCheckListType * 1.0 / datas.count);
        totalOveral = (totalPointCheckListType * 1.0);
    }
    
    if (totalOveral < 0) {
        totalOveral = 0;
    }
    
    //set score checklist
    chkModel.chkCore = totalOveral;
    
    //set status checklist
    if([[UserManagerV2 sharedUserManager].currentUserAccessRight checklistMandatoryPass] > 0) {
        if (isCheckListPass && chkListType != nil && totalOveral >= chkListType.chkTypePassingScore) {
            chkModel.chkListStatus = chkStatusPass;
        } else {
            chkModel.chkListStatus = chkStatusFail;
        }
    } else if (chkListType != nil && totalOveral >= chkListType.chkTypePassingScore) {
        chkModel.chkListStatus = chkStatusPass;//tChkPass;
    } else {
        chkModel.chkListStatus = chkStatusFail;//tChkFail;
    }
    
    //update checklist in database
    [manager updateCheckList:chkModel];
    [tbvChecklist reloadData];
    
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getMsgSaveMessage]];
    //hide saving data.
    [self performSelector:@selector(hiddenHUDAfterSaved:) withObject:HUD afterDelay:0.5];
}

#pragma mark - Save Status CheckList
-(void) saveData {    
    statusSaveCheckList = YES;
    statusChangeChkListDetail = NO;

    if(chkModel.chkCore == -1)
        [self saveStatusCheckList];
}

#pragma mark - Implement delegate checklist detail
-(void)didChangeScoreWithCheckList:(ChecklistFormScoreModel *)chklistFormScore {
    if (chklistFormScore.dfScore >= 0) {
        statusChangeChkListDetail = YES;
    } else {
        statusChangeChkListDetail = NO;
    }
    
    [self saveData];
}

#pragma mark - hidden HUD after save.
-(void) hiddenHUDAfterSaved:(MBProgressHUD *)HUD{
    [HUD hide:YES];
    [HUD removeFromSuperview];
}

#pragma mark - set caption
-(void)setCaptionView {
    msgbtnOk = [[LanguageManagerV2 sharedLanguageManager] getOK];
    msgSaveCheckList = [[LanguageManagerV2 sharedLanguageManager] getMsgAlertSaveCheckList];
    
    //refresh topview
    TopbarViewV2 *topview = [self getTopBarView];
    [topview refresh:[[SuperRoomModelV2 alloc] init]];
}

#pragma mark - Handle Topbar Methods
-(void)loadTopbarView {
    TopbarViewV2 *topbar = [[TopbarViewV2 alloc] initViewWithSuperModel:[[CommonVariable sharedCommonVariable] superRoomModel]];
    [topbar setTag:tagTopbarView];
    [self.view addSubview:topbar];
    [topbar setHidden:YES];

    [self addButtonHandleShowHideTopbar];
}

-(void)showTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationWillStartSelector:@selector(setHiddenTopbarView)];
    
    f = topbar.frame;
    f.origin.y = 0;
    [topbar setFrame:f];
    [topbar setAlpha:1.0];
    
    [self adjustShowForViews];
    
    [UIView commitAnimations];

}

-(void)hideTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDidStopSelector:@selector(setHiddenTopbarView)];
    [UIView setAnimationDelegate:self];
    
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    [topbar setAlpha:0.0];
    
    [self adjustRemoveForViews];
    
    [UIView commitAnimations];
}

-(void)setHiddenTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    [topbar setHidden:!topbar.hidden];
}

-(TopbarViewV2 *)getTopBarView {
    return (TopbarViewV2 *)[self.view viewWithTag:tagTopbarView];
}

//-(void)addButtonHandleShowHideTopbar {
//    UIButton *titleBarButton = [[UIButton alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
//    
//    titleBarButton.titleLabel.backgroundColor = [UIColor clearColor];
//    titleBarButton.titleLabel.font = FONT_BUTTON_TOPBAR;
//    titleBarButton.titleLabel.textAlignment = NSTextAlignmentCenter;
//    titleBarButton.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
//    [titleBarButton.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
//    [titleBarButton.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
//    [titleBarButton.titleLabel setMinimumFontSize:10];
//    [titleBarButton.titleLabel setAdjustsFontSizeToFitWidth:YES];
//    [titleBarButton addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
//    [titleBarButton setTitle:[self title] forState:UIControlStateNormal];
//    self.navigationItem.titleView = titleBarButton;
//}

-(void)topbarTapped:(UIButton *)sender {
    TopbarViewV2 *topbar = [self getTopBarView];
    [sender setEnabled:NO];
    if (topbar.isHidden) {
        [self showTopbarView];
    } else {
        [self hideTopbarView];
    }
    [self performSelector:@selector(topbarButtonEnable:) withObject:sender afterDelay:0.5];
}

-(void) topbarButtonEnable:(UIButton *) topbarBtn {
    [topbarBtn setEnabled:YES];
}

-(void)adjustShowForViews {
    //adjust all views when show 
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    NSInteger height = (datas.count + 2) * 30;
    
    [tbvChecklist setFrame:CGRectMake(0, f.size.height, 320, tbvChecklist.frame.size.height - height - f.size.height)];
}

-(void)adjustRemoveForViews { 
    //adjust all views when hide
    NSInteger height = (datas.count + 2) * 30;
    
    [tbvChecklist setFrame:CGRectMake(0, 0, 320, tbvChecklist.frame.size.height - height)];
}

-(void)addButtonHandleShowHideTopbar {
    UIView *vBarButton = [[UIView alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
    UIButton *titleBarButtonFirst = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 200, 30)];
    [titleBarButtonFirst setTag:1];
    titleBarButtonFirst.titleLabel.backgroundColor = [UIColor clearColor];
    [titleBarButtonFirst.titleLabel setFont: FONT_BUTTON_TOPBAR];
    titleBarButtonFirst.titleLabel.textAlignment = NSTextAlignmentCenter;
    [titleBarButtonFirst.titleLabel setAdjustsFontSizeToFitWidth:YES];
    titleBarButtonFirst.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
    
    [titleBarButtonFirst.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
    [titleBarButtonFirst.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
    [titleBarButtonFirst addTarget:self action:@selector(hiddenHeaderView) forControlEvents:UIControlEventTouchUpInside];
    [titleBarButtonFirst setTitle:roomNo forState:UIControlStateNormal];
    
    [vBarButton addSubview:titleBarButtonFirst];
    
    UIButton *titleBarButtonSecond = [[UIButton alloc] initWithFrame:CGRectMake(0, 22, 200, 22)];
    [titleBarButtonSecond setTag:2];
    [titleBarButtonSecond setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [titleBarButtonSecond.titleLabel setFont:FONT_BUTTON_TOPBAR_SECOND];
    [titleBarButtonSecond addTarget:self action:@selector(hiddenHeaderView) forControlEvents:UIControlEventTouchUpInside];
    [titleBarButtonSecond setTitle:[L_CHECKLIST currentKeyToLanguage] forState:UIControlStateNormal];
    
    [vBarButton addSubview:titleBarButtonSecond];
    
    [self.navigationItem setTitleView:vBarButton];
    
    //Move title to center vertical
    CGRect titleViewBounds = self.navigationItem.titleView.bounds;
    int moveLeftTitle = 0;
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        moveLeftTitle = 10;
    }
    self.navigationItem.titleView.bounds = CGRectMake(titleViewBounds.origin.x + moveLeftTitle, Y_TOPBAR_TITLE, titleViewBounds.size.width, titleViewBounds.size.height);
}

- (void) hiddenHeaderView {
    BOOL isHidden = [self getTopBarView].hidden;
    [[self getTopBarView] setHidden:!isHidden];
    
    CGRect tbvChecklistFrame = tbvChecklist.frame;
    
    CGFloat scale;
    if (isHidden) {
        scale = 45;
    }
    else {
        scale = -45;
    }
    
    tbvChecklistFrame.origin.y += scale;
    tbvChecklistFrame.size.height -= scale;
    
    [tbvChecklist setFrame:tbvChecklistFrame];
}


@end
