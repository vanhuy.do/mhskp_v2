//
//  MenuCheckListCellV2.h
//  mHouseKeeping
//
//  Created by TMS on 2/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuCheckListCellV2 : UITableViewCell {
    UIImageView *imgStatusCheckList;
    UIImageView *imgCheckList;
    UILabel *lblNameCheckList;
    UIImageView *imgActionCheckList;
}

@property (nonatomic, strong) IBOutlet UIImageView *imgStatusCheckList;
@property (nonatomic, strong) IBOutlet UIImageView *imgCheckList;
@property (nonatomic, strong) IBOutlet UILabel *lblNameChecklist;
@property (nonatomic, strong) IBOutlet UIImageView *imgActionCheckList;
@property (nonatomic, strong) IBOutlet UIImageView *imgBgCell;

@end
