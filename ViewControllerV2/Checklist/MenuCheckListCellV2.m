//
//  MenuCheckListCellV2.m
//  mHouseKeeping
//
//  Created by TMS on 2/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MenuCheckListCellV2.h"

@implementation MenuCheckListCellV2
@synthesize imgCheckList,imgStatusCheckList,lblNameChecklist, imgActionCheckList, imgBgCell;

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
