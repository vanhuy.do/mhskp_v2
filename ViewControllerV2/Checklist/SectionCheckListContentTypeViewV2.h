//
//  SectionCheckListContentTypeViewV2.h
//  mHouseKeeping
//
//  Created by TMS on 2/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SectionCheckListContentTypeViewV2Delegate;

@interface SectionCheckListContentTypeViewV2 : UIView{
    UILabel *lblContentName;
    UILabel *lblContentDescription;
    UIImageView *imgArrowSection;
    __unsafe_unretained id<SectionCheckListContentTypeViewV2Delegate> delegate;
    UIButton *btnToggle;
    BOOL isToggle;
    NSInteger section;
}

@property (nonatomic, assign) BOOL isToggle;
@property (nonatomic, assign) NSInteger section;
@property (nonatomic, strong)  UILabel *lblContentName;
@property (nonatomic, strong)  UILabel *lblContentDescription;
@property (nonatomic, strong)  UIImageView *imgArrowSection;
@property (assign, nonatomic) id<SectionCheckListContentTypeViewV2Delegate> delegate;

-(id) initWithSection:(NSInteger) sectionIndex contentName:(NSString *)content AndStatusArrow:(BOOL)isOpened;
-(void)toggleOpenWithUserAction:(BOOL) userAction;
@end

@protocol SectionCheckListContentTypeViewV2Delegate <NSObject>

@optional
-(void) sectionHeaderView:(SectionCheckListContentTypeViewV2*) sectionheaderView sectionOpened:(NSInteger) section;
-(void) sectionHeaderView:(SectionCheckListContentTypeViewV2*) sectionheaderView sectionClosed:(NSInteger) section;

@end
