//
//  SectionCheckListContentTypeViewV2.m
//  mHouseKeeping
//
//  Created by TMS on 2/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SectionCheckListContentTypeViewV2.h"
#import "ehkDefinesV2.h"
#import "UIImage+Tint.h"

@interface SectionCheckListContentTypeViewV2 (PrivateMethods)

-(void) toggleOpen:(id)sender;

@end

@implementation SectionCheckListContentTypeViewV2

@synthesize lblContentName,lblContentDescription,imgArrowSection, section, isToggle;
@synthesize delegate;


-(id)init {
    self = [super init];
    if (self) {
        [self setFrame:CGRectMake(0, 0, 320, 50)];
        lblContentName = [[UILabel alloc] initWithFrame:CGRectMake(20, 4, 196, 44)];
        [lblContentName setLineBreakMode:NSLineBreakByWordWrapping];
        [lblContentName setNumberOfLines:2];
        [lblContentName setBackgroundColor:[UIColor clearColor]];
        [lblContentName setTextColor:[UIColor colorWithRed:255 green:255 blue:255 alpha:1]];
        [lblContentName setFont:[UIFont fontWithName:@"Arial-BoldMT" size:15]];
        [self setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageBeforeiOS7:imgBackgroundSection equaliOS7:imgBackgroundSectionFlat]]];
        if(isToggle == YES)
        {
            imgArrowSection = [[UIImageView alloc]initWithImage:[UIImage imageBeforeiOS7:imgRightRowOpen equaliOS7:imgRightRowOpenFlat]];
        }
        else
        {
            imgArrowSection = [[UIImageView alloc]initWithImage:[UIImage imageBeforeiOS7:imgRightRow equaliOS7:imgRightRowFlat]];
        }
        
        [imgArrowSection setFrame:CGRectMake(288, 12, 25, 25)];
        btnToggle = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnToggle setFrame:CGRectMake(0, 0, 320, 50)];
        [btnToggle addTarget:self action:@selector(toggleOpen:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:lblContentName];
        [self addSubview:imgArrowSection];
        [self addSubview:btnToggle];
    }
    return self;
}

-(id)initWithSection:(NSInteger)sectionIndex contentName:(NSString *)content AndStatusArrow:(BOOL)isOpened {
    self = [self init];
    if (self) {
        section = sectionIndex;
        lblContentName.text = content;
        isToggle = isOpened;
    }
    return self;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void)toggleOpen:(id)sender {
    [self toggleOpenWithUserAction:YES];
}

-(void)toggleOpenWithUserAction:(BOOL)userAction {
    if (userAction) {
        if (isToggle == NO) {
            if ([delegate respondsToSelector:@selector(sectionHeaderView:sectionOpened:)]) {
                isToggle = YES;
                [delegate sectionHeaderView:self sectionOpened:section];
                
                [imgArrowSection setImage:[UIImage imageBeforeiOS7:imgRightRowOpen equaliOS7:imgRightRowOpenFlat]];
            }
        }else {
            if ([delegate respondsToSelector:@selector(sectionHeaderView:sectionClosed:)]) {
                isToggle = NO;
                [imgArrowSection setImage:[UIImage imageBeforeiOS7:imgRightRow equaliOS7:imgRightRowFlat]];
                [delegate sectionHeaderView:self sectionClosed:section];
            }
        }
    }
}
@end
