//
//  CleaningStatusView.h
//  eHouseKeeping
//
//  Created by chinh bui on 6/17/11.
//  Copyright 2011 TMA. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "RoomRecordModel.h"
#import "RoomManagerV2.h"
#import "TopbarViewV2.h"
#import "DateTimePicker.h"
#import "ehkDefinesV2.h"

enum ENUM_TYPE_STATUS {
    ENUM_CLEANING_STATUS = 1,
    ENUM_ROOM_STATUS = 2,
};

@protocol cleaningStatusViewDelegate <NSObject>
- (void)cleaningStatus:(NSInteger)roomStatus roomStatusText:(NSString*)roomStatusStrValue imageRoomStatus:(UIImage*)imageRoomStatus withViewController:(UIViewController*)controller;
- (void)cleaningStatus:(NSInteger)cleaningStatus isRoomStatus:(BOOL)isRoomStatus withTime:(NSDate*) date withViewController:(UIViewController*)controller;
@optional
- (void)cleaningStatus:(NSInteger)cleaningStatus isRoomStatus:(BOOL)isRoomStatus withViewController:(UIViewController*)controller;

@end

@interface CleaningStatusViewV2 : UIViewController <UITableViewDataSource,UITableViewDelegate,DateTimePickerViewV2Delegate, UIAlertViewDelegate>{
    NSMutableArray *arrayCleaningStatus;
    NSMutableArray *arrayDisplay;
    IBOutlet UIPickerView *pickerView;
    int actionSelecting;
    CleaningStatusModelV2 *cleaningStatusModel;
    RoomManagerV2 *roomManager;
    IBOutlet UIBarButtonItem *butDone;
    IBOutlet UIBarButtonItem *butCancel;
    IBOutlet UITableView *cleaningStatusTableView;
    
    BOOL isOccupied;
    BOOL isBlock;
    NSMutableArray *dataSourceOccupied;
    NSMutableArray *dataSourceVacant;
    IBOutlet UIButton *TitleButton;
    __unsafe_unretained id<cleaningStatusViewDelegate> delegate;
    
    enum ENUM_TYPE_STATUS typeStatus;
    TopbarViewV2 *topBarView;
    NSInteger clsID;
    
    enum ENUM_CLEANING_STATUS _currentCleaningStatus;
    
    BOOL isFindByRoomView;
    
    int roomStatusSelected;
    
    //for conrad CR
    NSMutableArray *roomStatusListForRA;
}

@property (nonatomic, readwrite) BOOL isFindByRoomView;

@property (nonatomic, strong) NSMutableArray *arrayCleaningStatus;
@property (nonatomic, strong) CleaningStatusModelV2 *cleaningStatusModel;
@property (nonatomic, strong) RoomManagerV2 *roomManager;
@property (nonatomic, strong) NSMutableArray *arrayDisplay;
@property (nonatomic, assign) id<cleaningStatusViewDelegate> delegate;

@property (nonatomic) enum ENUM_TYPE_STATUS typeStatus;
@property (nonatomic, strong) TopbarViewV2 *topBarView;
@property (nonatomic, strong) UIPickerView* pickerView;
@property (nonatomic) BOOL isOccupied;
@property (nonatomic) BOOL  isBlock;
@property (nonatomic) enum ENUM_CLEANING_STATUS _currentCleaningStatus;
@property (strong, nonatomic) IBOutlet UIView *vCleaningStatus;
@property (nonatomic, assign) enum IS_PUSHED_FROM isPushFrom;
@property (nonatomic, readwrite) NSInteger currentRoomStatus;

- (void)swithToFunctionCleaningStatus:(int)csParam;
- (void)loadCleaningStatusData;

//-(void)Start:(int)cleaning_status_id;

//-(void)pause:(int)cleaning_status_id;
//-(void)finished:(int)cleaning_status_id;
//-(void)Declined:(int)cleaning_status_id;
//-(void)serviceLate:(int)cleaning_status_id;
//-(void)DND:(int)cleaning_status_id;
//-(void)reloadInputViews;
//-(void)releaseResource;
@end
