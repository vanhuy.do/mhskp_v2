//
//  CleaningStatusView.m
//  eHouseKeeping
//
//  Created by chinh bui on 6/17/11.
//  Copyright 2011 TMA. All rights reserved.
//

#import "CleaningStatusViewV2.h"
#import "DateTimeUtility.h"
#import "ehkDefines.h"
#import "LanguageManager.h"
#import "RoomManagerV2.h"
#import "CheckListManagerV2.h"
#import "TasksManagerV2.h"
//#import "HomeViewV2.h"
#import "MyNavigationBarV2.h"
#import "LanguageManagerV2.h"
#import "CustomAlertViewV2.h"
#import "DeviceManager.h"

#define highlightImage      @"tab_count_active.png"
#define normalImage         @"tab_count_NOactive.png"
#define background          @"bg.png"


#define kCleaningStatusKey @"kCleaningStatusKey"
#define kCleaningStatusIconKey @"kCleaningStatusIconKey"

#define kRoomStatusKey @"kRoomStatusKey"
#define kRoomStausIconKey @"kRoomStausIconKey"

#define backgroundImage     @"bg.png"

#define tagLabel 1
#define tagImage 2
#define kFontLabel @"Arial-BoldMT"

#pragma mark image cleaning status
// define image of cleaning status

#pragma mark image room statsus
// define image of room status

//#define kPauseStatusKey @""
//#define kDeclinedServiceStatus @""
//#define kServiceLaterKey @""
//#define kDndKey @""
//#define kStartKey @""

@interface CleaningStatusViewV2 (PrivateMethods)
-(TopbarViewV2 *)getTopBarView;
-(void) showTopbarView;
-(void) hideTopbarView;
-(void) adjustShowForViews;
-(void) adjustRemoveForViews;
-(void)setHiddenTopbarView;
-(void) topbarTapped:(UIButton *) sender;
@end

@implementation CleaningStatusViewV2
@synthesize arrayCleaningStatus,cleaningStatusModel,roomManager,arrayDisplay;
@synthesize pickerView;
@synthesize delegate;
@synthesize typeStatus;
@synthesize topBarView;
@synthesize isOccupied;
@synthesize isBlock;
@synthesize _currentCleaningStatus;
@synthesize vCleaningStatus;
@synthesize isFindByRoomView;
@synthesize isPushFrom;
@synthesize currentRoomStatus;

int tagConfirmChangeCleaningStatus = 88;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    
    return self;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (IBAction)Done:(id)sender {    
    // calling update cleaning status.    
    [self swithToFunctionCleaningStatus:actionSelecting];
    
    // pass event for remove this view.
    [[NSNotificationCenter defaultCenter] postNotificationName:@"removeCleaningStatusView" object:Nil];
}

- (IBAction)cancel:(id)sender {
//    [[self navigationController] setNavigationBarHidden:NO animated:NO];
  //  [self.view.superview removeFromSuperview];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"removeCleaningStatusView" object:@"cancel"];
}

/**
 * start
 * Date : 
 *
 * @return none
 *
 *@param : csParam
 *
 * @author Chinh.Bui <>
 * @since 
 */
-(void)swithToFunctionCleaningStatus:(int)csParam {
    if(![arrayDisplay count]) return;
    
    CleaningStatusModelV2 *cleaningStatusModel1 = (CleaningStatusModelV2 *) [arrayCleaningStatus objectAtIndex:csParam];
   
    csParam = cleaningStatusModel1.cstat_Id;
    [RoomManagerV2 setCurrentCleaningStatus:csParam]; // @ set current temp cleaning Status.
    
    //not alloc so do not release
    //[cleaningStatusModel1 release];
}

#pragma mark - View lifecycle
-(NSInteger) CheckListStatus {
    CheckListManagerV2 *chkManager = [[CheckListManagerV2 alloc] init];
    CheckListModelV2 *chkModel = [[CheckListModelV2 alloc] init];
    chkModel.chkListId = [TasksManagerV2 getCurrentRoomAssignment];
    chkModel.chkListUserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    [chkManager loadCheckListData:chkModel];
        
    return chkModel.chkListStatus;
}

- (void)loadCleaningStatusData {
    //enable flag to announce, is loading data
    [[HomeViewV2 shareHomeView] waitingLoadingData];
        
    // in case view cleaningStatus
    if(typeStatus == ENUM_CLEANING_STATUS) {        
        dataSourceOccupied = [[NSMutableArray alloc] init];
        dataSourceVacant =  [[NSMutableArray alloc] init];

        NSMutableArray *cleaningStatusList = nil;
        
        if (isFindByRoomView == FALSE) {
            cleaningStatusList = [[RoomManagerV2 sharedRoomManager] loadAllCleaningStatusWithCleaningStatusID:_currentCleaningStatus];  
            
            /********load data for Occupied room and vacant room ********/
            for (int index = 0; index < [cleaningStatusList count]; index++) {
                CleaningStatusModelV2 *model = (CleaningStatusModelV2 *) [cleaningStatusList objectAtIndex:index];
                
                if(model.cstat_Id != ENUM_CLEANING_STATUS_PENDING_FIRST_TIME &
                   model.cstat_Id != ENUM_CLEANING_STATUS_PENDING_SECOND_TIME) {
                    if(model.cstat_Id == ENUM_CLEANING_STATUS_PAUSE || 
                       model.cstat_Id == ENUM_CLEANING_STATUS_START || 
                       model.cstat_Id == ENUM_CLEANING_STATUS_COMPLETED){
                        [dataSourceVacant addObject:model];
                    }
                    
                    [dataSourceOccupied addObject:model];
                }            
            }
        } else {
            cleaningStatusList = [[RoomManagerV2 sharedRoomManager] loadAllCleaningStatusForSupervisor];  
            
            for (int index = 0; index < [cleaningStatusList count]; index++) {
                CleaningStatusModelV2 *model = (CleaningStatusModelV2 *) [cleaningStatusList objectAtIndex:index];
                [dataSourceOccupied addObject:model];
            }
        }
    }
    
    if(typeStatus == ENUM_ROOM_STATUS) {
        [TitleButton setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_room_status] forState:UIControlStateNormal];
    } else{
        [TitleButton setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_CLEANING_STATUS] forState:UIControlStateNormal];
    }
    
    [TitleButton setHidden:YES]; //Set hide to same with Android
    
    // view room status
    if(typeStatus == ENUM_ROOM_STATUS) {
        dataSourceOccupied = [[NSMutableArray alloc] init];
        dataSourceVacant =  [[NSMutableArray alloc] init];
        
        NSMutableArray *roomStatusList = [[RoomManagerV2 sharedRoomManager] loadAllRoomStatus];
        
        for (int index = 0; index < [roomStatusList count]; index++) {
            RoomStatusModelV2 *roomModel = [roomStatusList objectAtIndex:index];
            if(![UserManagerV2 isSupervisor]) {
                if (roomModel.rstat_Id == ENUM_ROOM_STATUS_OD
                    || roomModel.rstat_Id == ENUM_ROOM_STATUS_VD
                    || roomModel.rstat_Id == ENUM_ROOM_STATUS_VC
                    || roomModel.rstat_Id == ENUM_ROOM_STATUS_OC
                    || roomModel.rstat_Id == ENUM_ROOM_STATUS_VPU)
                {
                    [dataSourceVacant addObject:roomModel];
                }
            } else {
                if (roomModel.rstat_Id == ENUM_ROOM_STATUS_OD
                    || roomModel.rstat_Id == ENUM_ROOM_STATUS_VD
                   // || roomModel.rstat_Id == ENUM_ROOM_STATUS_VC
                   // || roomModel.rstat_Id == ENUM_ROOM_STATUS_OC
                    || roomModel.rstat_Id == ENUM_ROOM_STATUS_VI
                    || roomModel.rstat_Id == ENUM_ROOM_STATUS_OI)
                {
                    [dataSourceVacant addObject:roomModel];
                }
            }
        }
    }

    [cleaningStatusTableView setBackgroundColor:[UIColor clearColor]];
    [cleaningStatusTableView setDataSource:self];
    [cleaningStatusTableView setDelegate:self];
    [cleaningStatusTableView reloadData];    
    
    //end loading data
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
}

- (void)viewDidLoad {
    //init arrayCleaningStatus
    self.arrayCleaningStatus = [NSMutableArray array];

    [super viewDidLoad];
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        //Clear redundant view for header, footter
        CGRect frameHeaderFooter = cleaningStatusTableView.tableHeaderView.frame;
        frameHeaderFooter.size.height = 0.1f;
        UIView *headerView = [[UIView alloc] initWithFrame:frameHeaderFooter];
        [cleaningStatusTableView setTableHeaderView:headerView];
        
        frameHeaderFooter = cleaningStatusTableView.tableFooterView.frame;
        frameHeaderFooter.size.height = 0.1f;
        UIView *footerView = [[UIView alloc] initWithFrame:frameHeaderFooter];
        [cleaningStatusTableView setTableFooterView:footerView];
    }
    
    //Resize content views of current VC to fit with Navigation bar and tabbar
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    UIColor *bgColor = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:backgroundImage]];
    self.view.backgroundColor = bgColor;
    
    if (topBarView == nil) {
        topBarView = [[TopbarViewV2 alloc] initViewWithSuperModel:[[SuperRoomModelV2 alloc] init]];
        [topBarView setTag:tagTopbarView];
        [self.view addSubview:topBarView];
        [topBarView setHidden:YES];
    }
    
//    UIButton *titleBarButton = [[UIButton alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
//    
//    titleBarButton.titleLabel.backgroundColor = [UIColor clearColor];
//    titleBarButton.titleLabel.font = FONT_BUTTON_TOPBAR;
//    titleBarButton.titleLabel.textAlignment = NSTextAlignmentCenter;
//    titleBarButton.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
//    [titleBarButton.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
//    [titleBarButton.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
//    [titleBarButton addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
//    
//    [titleBarButton setTitle:[NSString stringWithFormat:@"%@ %d",[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOM_TITLE],[RoomManagerV2 getCurrentRoomNo] ] forState:UIControlStateNormal];
    
//    self.navigationItem.titleView = titleBarButton; 
    [self addButtonHandleShowHideTopbar];
}

-(void)loadView {
    [super loadView];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self loadCleaningStatusData];
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        [self setDisableTitleBtn];
    }
}

-(void)backToRoomDetail {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Alert view Delegate
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == tagConfirmChangeCleaningStatus)
    {
        if(buttonIndex == 0) //YES
        {
            [delegate cleaningStatus:roomStatusSelected isRoomStatus:YES withViewController:self];
            //[self.navigationController popViewControllerAnimated:YES];
        }
    }
}

#pragma mark - tableView Delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(isOccupied) {
        return [dataSourceOccupied count];
    } else {
        return [dataSourceVacant count];
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CleaningStatusModelV2 *clsModel = nil;
    RoomStatusModelV2 *roomStatusModel = nil;
    if(isOccupied) {
        if(typeStatus == ENUM_ROOM_STATUS) {
            roomStatusModel =(RoomStatusModelV2 *) [dataSourceOccupied objectAtIndex:indexPath.row];
        } else {
            clsModel = (CleaningStatusModelV2 *) [dataSourceOccupied objectAtIndex:indexPath.row];
        }
    } else {
        if(typeStatus == ENUM_ROOM_STATUS) {
            roomStatusModel = (RoomStatusModelV2 *) [dataSourceVacant objectAtIndex:indexPath.row];
        } else {
            clsModel = (CleaningStatusModelV2 *) [dataSourceVacant objectAtIndex:indexPath.row];
        }
    }    
    
    UITableViewCell *cleaningStatusCell = nil;
    static NSString *cleaningStatusIdentifier = @"cleaningStatus";
    static NSString *roomStatusIdentifier = @"roomStatus";
    
    cleaningStatusCell = [tableView dequeueReusableCellWithIdentifier:cleaningStatusIdentifier];
    
    if (cleaningStatusCell == nil) {
        if(typeStatus == ENUM_CLEANING_STATUS) {
            cleaningStatusCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cleaningStatusIdentifier];
        } else {
            cleaningStatusCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:roomStatusIdentifier];
        }
        
        [cleaningStatusCell.textLabel setTextAlignment:NSTextAlignmentCenter];
        
//        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, 130, 30)];
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(150, 5, 130, 33)];
        label.tag = tagLabel;
        [label setFont:[UIFont fontWithName:kFontLabel size:20]];
        [label setTextColor:[UIColor colorWithRed:6.0f/255.0f green:62.0f/255.0f blue:127.0f/255.0f alpha:1]];
        [label setTextAlignment:NSTextAlignmentLeft];
        [cleaningStatusCell.contentView addSubview:label];
        
//        UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(150, 5, 30, 30)];
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(100, 5, 33, 33)];
        imgView .tag = tagImage;
        [cleaningStatusCell.contentView addSubview:imgView];
    }
        
    /********************** Text ****************************************/           
    UILabel *label = (UILabel*)[cleaningStatusCell.contentView viewWithTag:tagLabel];
    [label setBackgroundColor:[UIColor clearColor]];
    if(typeStatus == ENUM_CLEANING_STATUS) {
        if ([[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString:[NSString stringWithFormat:@"%@",ENGLISH_LANGUAGE]]) {
            label.text = clsModel.cstat_Name;
        } else{
            label.text = clsModel.cstat_Language;
        }
    } else {
        if ([[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString:[NSString stringWithFormat:@"%@",ENGLISH_LANGUAGE]]) {
//            label.text = roomStatusModel.rstat_Name;
            label.text = roomStatusModel.rstat_Code;
        } else{
//            label.text = roomStatusModel.rstat_Lang;
            label.text = roomStatusModel.rstat_Code;
        }
    }
    /********************** Text ****************************************/
    
    /********************** Image ****************************************/
    UIImageView *imgView = (UIImageView*)[cleaningStatusCell.contentView viewWithTag:tagImage];
    if(typeStatus == ENUM_CLEANING_STATUS){
        if(clsModel.cstat_Id == ENUM_CLEANING_STATUS_PENDING_SECOND_TIME){
//            [imgView setFrame:CGRectMake(135, 0, 60, 30)];
            [imgView setFrame:CGRectMake(135, 5, 60, 30)];
        }
    }
    
    if(typeStatus == ENUM_CLEANING_STATUS){      
        [imgView setImage:[UIImage imageWithData:clsModel.cstat_image]];
    } else {
        [imgView setImage:[UIImage imageWithData:roomStatusModel.rstat_Image]];
    }        
    
    /********************** Image ****************************************/
    
    [cleaningStatusCell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cleaningStatusCell;
}

//- (void)viewDidUnload
//{
//    //[cleaningStatusModel release];
//    
//    [cleaningStatusTableView release];
//    cleaningStatusTableView = nil;
//    [TitleButton release];
//    TitleButton = nil;
//    [super viewDidUnload];
//    
//    
//    // Release any retained subviews of the main view.
//    // e.g. self.myOutlet = nil;
//}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(typeStatus == ENUM_CLEANING_STATUS) {
        clsID = 0;
        CleaningStatusModelV2 *clsModel;
        
        if(isOccupied) {
            clsModel =(CleaningStatusModelV2*) [dataSourceOccupied objectAtIndex:indexPath.row];
            clsID = clsModel.cstat_Id;
        } else {
            clsModel =(CleaningStatusModelV2*) [dataSourceVacant objectAtIndex:indexPath.row];
            clsID = clsModel.cstat_Id;
        }
        
        if(clsModel.cstat_Id == ENUM_CLEANING_STATUS_SERVICE_LATER) {
            // add datetime picker                       
            DateTimePicker *dateTimePicker = [[DateTimePicker alloc] initWithUIDateTimeModel:UIDatePickerModeTime andDefaultDate:[NSDate date]];
            
            [dateTimePicker setDelegate:self];
            [dateTimePicker showPickerViewV2];            
        } else {
          [delegate cleaningStatus:clsID isRoomStatus:NO withViewController:self];  
        }        
    } else {
        NSInteger roomStatusID = 0;
        UIImage *imageRoomStatus = nil;
        NSString *roomStatusStrValue = nil;
        if(isOccupied) {
            RoomStatusModelV2 *roomStatusModel =(RoomStatusModelV2*) [dataSourceOccupied objectAtIndex:indexPath.row];
            roomStatusID = roomStatusModel.rstat_Id;
            imageRoomStatus = [UIImage imageWithData:roomStatusModel.rstat_Image];
            if ([[LanguageManagerV2 sharedLanguageManager] isEnglishLanguage]) {
                roomStatusStrValue = roomStatusModel.rstat_Name;
            } else {
                roomStatusStrValue = roomStatusModel.rstat_Lang;
            }
            
        } else {
            RoomStatusModelV2 *roomStatusModel =(RoomStatusModelV2*) [dataSourceVacant objectAtIndex:indexPath.row];
            roomStatusID = roomStatusModel.rstat_Id;
            if ([[LanguageManagerV2 sharedLanguageManager] isEnglishLanguage]) {
                roomStatusStrValue = roomStatusModel.rstat_Name;
            } else {
                roomStatusStrValue = roomStatusModel.rstat_Lang;
            }
            imageRoomStatus = [UIImage imageWithData:roomStatusModel.rstat_Image];
        }
        
        roomStatusSelected = (int)roomStatusID;
        
        if([delegate respondsToSelector:@selector(cleaningStatus:isRoomStatus:withViewController:)]) {
            [delegate cleaningStatus:roomStatusSelected isRoomStatus:YES withViewController:self];
        } else if ([delegate respondsToSelector:@selector(cleaningStatus:roomStatusText:imageRoomStatus:withViewController:)]){
            [delegate cleaningStatus:roomStatusSelected roomStatusText:roomStatusStrValue imageRoomStatus:imageRoomStatus withViewController:self];
        }
        
        //Remove for fix bug compare Android & iOS
        /*
        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title: @"" message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_room_confirmchangeroomstatus] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles:[[LanguageManagerV2 sharedLanguageManager] getNo], nil];
        alert.tag = tagConfirmChangeCleaningStatus;
        alert.delegate = self;
        [alert show];
        */
        //Hao Tran[20130513/Remove] - Remove because we will confirm change room status in delegate of UIAlertView in self
        //[delegate cleaningStatus:roomStatusID isRoomStatus:YES withViewController:self];
        //Hao Tran[20130513/Remove] - End
    }
}

- (void)dateTimePickerOK:(DateTimePicker *)controller didPickDate:(NSDate *)date{
    [delegate cleaningStatus:clsID isRoomStatus:NO withTime:date withViewController:self]; 
}

- (void)dateTimePickerCancel:(DateTimePicker *)controller {
    
}

#pragma mark - TopbarViewV2
-(TopbarViewV2 *)getTopBarView {
    return (TopbarViewV2 *)[self.view viewWithTag:tagTopbarView];
}

-(void) showTopbarView{
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationWillStartSelector:@selector(setHiddenTopbarView)];
    
    f = topbar.frame;
    f.origin.y = 0;
    [topbar setFrame:f];
    [topbar setAlpha:1.0];
    
    [self adjustShowForViews];
    
    [UIView commitAnimations];
}

-(void) hideTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDidStopSelector:@selector(setHiddenTopbarView)];
    [UIView setAnimationDelegate:self];
    
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    [topbar setAlpha:0.0];
    
    [self adjustRemoveForViews];
    
    [UIView commitAnimations];
}

-(void) adjustShowForViews {
    //adjust all views when show 
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    CGRect ftbv = vCleaningStatus.frame;
    [vCleaningStatus setFrame:CGRectMake(0, f.size.height, 320, ftbv.size.height - f.size.height)];
}
                           
-(void) adjustRemoveForViews {
    //adjust all views when hide
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    CGRect ftbv = vCleaningStatus.frame;
    [vCleaningStatus setFrame:CGRectMake(0, 0, 320, ftbv.size.height + f.size.height)];                          
}

-(void)topbarTapped:(UIButton *)sender {
    TopbarViewV2 *topbar = [self getTopBarView];
    [sender setEnabled:NO];
    if (topbar.isHidden) {
        [self showTopbarView];
    } else {
        [self hideTopbarView];
    }
    
    [self performSelector:@selector(topbarButtonEnable:) withObject:sender afterDelay:0.5];
}

-(void) topbarButtonEnable:(UIButton *) topbarBtn {
    [topbarBtn setEnabled:YES];
}

-(void)setHiddenTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    [topbar setHidden:!topbar.hidden];
}

-(void)addButtonHandleShowHideTopbar {
    UIView *vBarButton = [[UIView alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
    UIButton *titleBarButtonFirst = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 200, 30)];
    [titleBarButtonFirst setTag:1];
    titleBarButtonFirst.titleLabel.backgroundColor = [UIColor clearColor];
    [titleBarButtonFirst.titleLabel setFont: FONT_BUTTON_TOPBAR];
    titleBarButtonFirst.titleLabel.textAlignment = NSTextAlignmentCenter;
    [titleBarButtonFirst.titleLabel setAdjustsFontSizeToFitWidth:YES];
    titleBarButtonFirst.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
    
    [titleBarButtonFirst.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
    [titleBarButtonFirst.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
    [titleBarButtonFirst addTarget:self action:@selector(hiddenHeaderView) forControlEvents:UIControlEventTouchUpInside];
    [titleBarButtonFirst setTitle:[RoomManagerV2 getCurrentRoomNo] forState:UIControlStateNormal];
    
    [vBarButton addSubview:titleBarButtonFirst];
    
    UIButton *titleBarButtonSecond = [[UIButton alloc] initWithFrame:CGRectMake(0, 22, 200, 22)];
    [titleBarButtonSecond setTag:2];
    [titleBarButtonSecond setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [titleBarButtonSecond.titleLabel setFont:FONT_BUTTON_TOPBAR_SECOND];
    [titleBarButtonSecond addTarget:self action:@selector(hiddenHeaderView) forControlEvents:UIControlEventTouchUpInside];
    [titleBarButtonSecond setTitle:[L_room_status currentKeyToLanguage] forState:UIControlStateNormal];
    
    [vBarButton addSubview:titleBarButtonSecond];
    
    [self.navigationItem setTitleView:vBarButton];
    
    //Move title to center vertical
    CGRect titleViewBounds = self.navigationItem.titleView.bounds;
    int moveLeftTitle = 0; 
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        moveLeftTitle = 10;
    }
    self.navigationItem.titleView.bounds = CGRectMake(titleViewBounds.origin.x + moveLeftTitle, Y_TOPBAR_TITLE, titleViewBounds.size.width, titleViewBounds.size.height);
    
    //back button navigation
    self.navigationItem.leftBarButtonItem = [MyNavigationBarV2 createBackButtonWithTarget:self selector:@selector(backToRoomDetail) forControlEvents:UIControlEventTouchUpInside];

}

- (void) hiddenHeaderView {      
    BOOL isHidden = topBarView.hidden;
    [topBarView setHidden:!isHidden];
    
    CGRect vCleaningStatusFrame = vCleaningStatus.frame;
    
    CGFloat scale;
    if (isHidden) {
        scale = 45;
    }
    else {
        scale = -45;
    }
    
    vCleaningStatusFrame.origin.y += scale;
    vCleaningStatusFrame.size.height -= scale;
    
    [vCleaningStatus setFrame:vCleaningStatusFrame];
}
-(void) setDisableTitleBtn
{
    [TitleButton setUserInteractionEnabled:NO];
    [TitleButton setBackgroundImage:[UIImage imageNamed:imgGuestInfoActiveFlat] forState:UIControlStateNormal];
}

@end
