//
//  WifiIConViewController.h
//  mHouseKeeping
//
//  Created by khanhnguyen on 2/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"

@interface WifiIConViewController : UIViewController{
 
    UIImageView * wifiIcon;
}
@property(strong) UIImageView * wifiIcon;
- (id) init;
- (void) setHidden:(BOOL)isHidden;
@end
