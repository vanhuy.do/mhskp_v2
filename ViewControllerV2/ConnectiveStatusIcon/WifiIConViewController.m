//
//  WifiIConViewController.m
//  mHouseKeeping
//
//  Created by khanhnguyen on 2/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "WifiIConViewController.h"
#import "Reachability.h"
#import "ehkDefines.h"
#import "LogFileManager.h"
#import "DeviceManager.h"
//#import "AFNetworkReachabilityManager.h"
#import "NetworkCheck.h"
#import "Reachability1.h"
#define wifiIconFrame CGRectMake(0, 27, 45, 38)
#define selfFrame CGRectMake(275, 0, 45, 38)
#define IMG_WifiIconOff @"wifiIcon_off.png"
#define IMG_WifiIconOn @"wifiIcon_on.png"
#define IMG_WifiIconOn_Flat @"wifiIcon_on_2.png"

@implementation WifiIConViewController
@synthesize wifiIcon=_wifiIcon;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id) init{
  
    self = [super init];
    if (self) {
        [self.view setFrame:selfFrame];
        [self.view setTag:tagViewWifi];
        
        wifiIcon = [[UIImageView alloc] initWithFrame:wifiIconFrame];
        
        NetworkStatus netStatus = (NetworkStatus)[[NSUserDefaults standardUserDefaults] integerForKey:@"networkStatus"];
        switch (netStatus)
        {
            case NotReachable:
            {
                wifiIcon.image= [UIImage imageNamed:IMG_WifiIconOff];
                break;
            }
                
            case ReachableViaWWAN:
            {
                //Switch image belong to platform
                UIImage *imageSwitch = nil;
                if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                    imageSwitch = [UIImage imageNamed:IMG_WifiIconOn_Flat];
                } else {
                    imageSwitch = [UIImage imageNamed:IMG_WifiIconOn];
                }
                wifiIcon.image = imageSwitch;
                if (![[NetworkCheck sharedNetworkCheck] checkInternetConnection]) {
                    wifiIcon.image= [UIImage imageNamed:IMG_WifiIconOff];
                }
                break;
            }
            case ReachableViaWiFi:
            {
                //Switch image belong to platform
                UIImage *imageSwitch = nil;
                if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                    imageSwitch = [UIImage imageNamed:IMG_WifiIconOn_Flat];
                } else {
                    imageSwitch = [UIImage imageNamed:IMG_WifiIconOn];
                }
                wifiIcon.image = imageSwitch;
                if (![[NetworkCheck sharedNetworkCheck] checkInternetConnection]) {
                    wifiIcon.image= [UIImage imageNamed:IMG_WifiIconOff];
                }
                break;
            }
            default:
            {
                wifiIcon.image= [UIImage imageNamed:IMG_WifiIconOff];
                break;
            }
        }
        [self.view addSubview:wifiIcon];
        
    }
    return self;
}
- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    /**************** Tan Nguyen Add *********************/
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(reachabilityDidChanged:) name: kReachabilityChangedNotification object: nil];
    /**************** Tan Nguyen End Add *********************/
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(recheckConnectStatus) name: kReachabilityUpdateNotification object: nil];
    
}
- (void)recheckConnectStatus{
    UIImage *imageSwitch = nil;
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        imageSwitch = [UIImage imageNamed:IMG_WifiIconOn_Flat];
    } else {
        imageSwitch = [UIImage imageNamed:IMG_WifiIconOn];
    }
    
    if (![[NetworkCheck sharedNetworkCheck] checkInternetConnection]) {
        imageSwitch = [UIImage imageNamed:IMG_WifiIconOff];
    }
    wifiIcon.image = imageSwitch;
}

//Called by Reachability whenever status changes.
- (void) reachabilityDidChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
	NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
//	NetworkStatus netStatus = [curReach currentReachabilityStatus];
    BOOL connectionRequired= [curReach connectionRequired];
    NSString* statusString= @"";
    
    
    Reachability1 *reachability = [Reachability1 reachabilityForInternetConnection];
    [reachability startNotifier];
    
    NetworkStatus status = [reachability currentReachabilityStatus];
    
    if(status == NotReachable)
    {
        //No internet
        statusString = @"Access Not Available";
        //Minor interface detail- connectionRequired may return yes, even when the host is unreachable.  We cover that up here...
        connectionRequired= NO;
        wifiIcon.image = [UIImage imageNamed:IMG_WifiIconOff];
    }
    else if (status == ReachableViaWiFi)
    {
        //WiFi
        statusString= @"Reachable WiFi";
        //Switch image belong to platform
        UIImage *imageSwitch = nil;
        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
            imageSwitch = [UIImage imageNamed:IMG_WifiIconOn_Flat];
        } else {
            imageSwitch = [UIImage imageNamed:IMG_WifiIconOn];
        }
        wifiIcon.image = imageSwitch;
        if (![[NetworkCheck sharedNetworkCheck] checkInternetConnection]) {
            statusString = @"Access Not Available";
            //Minor interface detail- connectionRequired may return yes, even when the host is unreachable.  We cover that up here...
            connectionRequired= NO;
            wifiIcon.image = [UIImage imageNamed:IMG_WifiIconOff];
        }
    }
    else if (status == ReachableViaWWAN)
    {
        //3G
        statusString = @"Reachable WWAN";
        //Switch image belong to platform
        UIImage *imageSwitch = nil;
        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
            imageSwitch = [UIImage imageNamed:IMG_WifiIconOn_Flat];
        } else {
            imageSwitch = [UIImage imageNamed:IMG_WifiIconOn];
        }
        wifiIcon.image = imageSwitch;
        if (![[NetworkCheck sharedNetworkCheck] checkInternetConnection]) {
            statusString = @"Access Not Available";
            //Minor interface detail- connectionRequired may return yes, even when the host is unreachable.  We cover that up here...
            connectionRequired= NO;
            wifiIcon.image = [UIImage imageNamed:IMG_WifiIconOff];
        }
    }
    
    if([LogFileManager isLogConsole]) {
        NSLog(@"%@", statusString);
    }
    if(connectionRequired)
    {
        statusString= [NSString stringWithFormat: @"%@, Connection Required", statusString]; 
//        NSLog(@"%@", statusString);
    }
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void) setHidden:(BOOL)isHidden{
    [self.view setUserInteractionEnabled:!isHidden];
    [self.view setHidden:isHidden];
}

@end
