//
//  AmenitiesController.h
//  mHouseKeeping
//
//  Created by Quang Nguyen on 5/2/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AmenitiesItemCell.h"
#import "AmenitiesSection.h"
#import "AccessRight.h"
#import "PickerViewV2.h"
#import "AmenitiesManagerV2.h"
#import "MBProgressHUD.h"
#import "SyncManagerV2.h"
#import "DTAlertView.h"
#import <ZXingWidgetController.h>

@interface AmenitiesController : UIViewController<UITableViewDelegate, UITableViewDataSource, ItemCellDelegate, AmenitiesSectionDelegate, ZXingDelegate, PickerViewV2Delegate, MBProgressHUDDelegate, DTAlertViewDelegate>{
    NSMutableArray *datas;
    IBOutlet UITableView *tbvAmenities;
    IBOutlet UIButton *btnSubmit;
    IBOutlet UIButton *btnQR;
    IBOutlet UILabel *lblRoomNo;
    IBOutlet UILabel *lblSubmit;
    IBOutlet UITextField *txtRoomNo;
    IBOutlet UIScrollView *scrollView;
    BOOL isFromMainPosting;
    BOOL statusSave;
    
    AccessRight *QRCodeScanner;
    AccessRight *postingAmenities;
    AccessRight *actionAmenities;
    AmenitiesManagerV2 *amenManager;
    __weak IBOutlet UIButton *btnPostingHistory;
    __weak IBOutlet UIView *footerView;
    SyncManagerV2 *synManager;
    BOOL haveOldAmenities;
    IBOutlet UIView *viewRoomNumber;
    IBOutlet UIImageView *imgHeader;
}
//@property (nonatomic, strong) NSMutableArray *sectionDatas;
@property (assign, nonatomic) BOOL statusAddToCart;
@property (nonatomic) BOOL isFromMainPosting;
@property (strong, nonatomic) NSString *msgbtnYes;
@property (strong, nonatomic) NSString *msgbtnNo;
@property (strong, nonatomic) NSString *msgSaveCount;
@property (strong, nonatomic) NSString *msgDiscardCount;
@property (assign, nonatomic) NSInteger roomTypeId;

//@property (nonatomic, assign) NSInteger serviceId;
- (IBAction)btnPostingHistory_Clicked:(UIButton *)sender;

-(IBAction) btnSubmit_Clicked:(id)sender;
@end
