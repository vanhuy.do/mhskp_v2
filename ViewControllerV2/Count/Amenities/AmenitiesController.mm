//
//  AmenitiesController.m
//  mHouseKeeping
//
//  Created by Quang Nguyen on 5/2/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
// 

#import "AmenitiesController.h"
#import "AmenitiesItemCell.h"
#import "AmenitiesSectionInfo.h"
#import "AmenitiesSection.h"
#import "PickerViewV2.h"
#import "MyNavigationBarV2.h"
#import "AmenitiesOrderDetailsModelV2.h"
#import "AmenitiesManagerV2.h"
#import "AmenitiesItemModelV2.h"
#import "ehkConvert.h"
#import "DeviceManager.h"
#import "QRCodeReader.h"
#import "NetworkCheck.h"
#import "iToast.h"
#import "CountHistoryModel.h"
#import "CountHistoryManager.h"
#import "ehkDefines.h"
#import "TasksManagerV2.h"
#import "HomeViewV2.h"
#import "CustomAlertViewV2.h"
#import "DTAlertView.h"
#import "STEncryptorDES.h"
#import "UIImage+Tint.h"
#import "NSString+Common.h"
#import "HistoryPosting.h"

#define tagAlertBack 10
#define tagAlertNo 11
#define tagNoNetwork 61
#define tagUnpostData 62
#define tagHudPostAllOrders 63
#define tagTopbarView 1234
#define tagAlertSubmit 1235

typedef enum {
    tUsed,
    tNew
}tType;

@implementation AmenitiesController
//@synthesize sectionDatas;
@synthesize statusAddToCart;
@synthesize isFromMainPosting;
@synthesize msgbtnNo,
msgbtnYes,
msgSaveCount,
msgDiscardCount;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)enableQRCode:(BOOL)isEnable{
    [btnQR setAlpha:isEnable ? 1.0f : 0.7f];
    [btnQR setEnabled:isEnable];
}
//CRF1619
- (void)loadAccessQRCode{
    if ([UserManagerV2 sharedUserManager].currentUserAccessRight.isPostingFunctionAmenitiesQRCode == 1) {
        txtRoomNo.enabled = NO;
        [self enableQRCode:YES];
    }else if ([UserManagerV2 sharedUserManager].currentUserAccessRight.isPostingFunctionAmenitiesQRCode == 0) {
        txtRoomNo.enabled = YES;
        [self enableQRCode:NO];
    }
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self loadAccessRights];
    
    synManager = [[SyncManagerV2 alloc] init];
    amenManager = [AmenitiesManagerV2 sharedAmenitiesManager];
    
    if(!ENABLE_QRCODESCANNER) {
        if(ENABLE_CHECK_ACCESS_RIGHT_QRCODESCANNER) {
            [btnQR setHidden:NO];
            [self enableQRCode:NO];
            
            if (QRCodeScanner.isActive && !isDemoMode) {
                [self enableQRCode:YES];
            }
            
        } else {
            [btnQR setHidden:YES];
        }
    }
    
    //Set Enable button submit
    [btnSubmit setEnabled:![self isOnlyViewedAccessRight]];
    
    //Resize content views of current VC to fit with Navigation bar and tabbar
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        [self loadFlatResource];
    }
    if (![UserManagerV2 sharedUserManager].currentUserAccessRight.isShowActionHistoryPosting) {
        CGRect frame = footerView.frame;
        frame.size.height = 84;
        footerView.frame = frame;
        [tbvAmenities setTableFooterView:footerView];
        btnPostingHistory.hidden = YES;
    }
    
    //back button navigation
    self.navigationItem.leftBarButtonItem = [MyNavigationBarV2 createBackButtonWithTarget:self selector:@selector(backBarPressed) forControlEvents:UIControlEventTouchUpInside];
    
    // Do any additional setup after loading the view from its nib.
    MBProgressHUD	*HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getRaLoadingData]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES];
//    [self performSelector:@selector(loadDataAmenitiesFormService:) withObject:HUD afterDelay:0.1];
    [self performSelector:@selector(loadDataAmenitiesCategoryFormService:) withObject:HUD afterDelay:0.1];
    [self performSelector:@selector(loadTopbarView)];
    [lblRoomNo setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOM_NO]];
    

    if(isFromMainPosting) {
        [txtRoomNo setText:@""];
        _roomTypeId = 0;
        [txtRoomNo setEnabled:YES];
        [txtRoomNo setTextColor:[UIColor blackColor]];
        [self loadAccessQRCode];
        btnPostingHistory.hidden = ![UserManagerV2 sharedUserManager].currentUserAccessRight.isShowActionHistoryPosting;
    } else {
        int roomAssignment = (int)[TasksManagerV2 getCurrentRoomAssignment];
        RoomAssignmentModelV2 *roomAssignmentModel = [[RoomAssignmentModelV2 alloc] init];
        roomAssignmentModel.roomAssignment_Id = roomAssignment;
        [[RoomManagerV2 sharedRoomManager]getRoomIdByRoomAssignment:roomAssignmentModel];
        [txtRoomNo setText:roomAssignmentModel.roomAssignment_RoomId];
        [txtRoomNo setTextColor:[UIColor grayColor]];
    }

    [scrollView setBackgroundColor:[UIColor clearColor]];
    [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, scrollView.frame.size.height)];
    [tbvAmenities setBackgroundView:nil];
    [tbvAmenities setBackgroundColor:[UIColor clearColor]];
    [tbvAmenities setOpaque:YES];
    [self setEnableBtnAtionHistory:NO];
}

#pragma mark - Click Back
-(void)backBarPressed {
    //clear previous tag of event
//    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:TAG_OF_EVENT];
    
//    if(statusChooseLinen == NO)
//    {
        [self.navigationController popViewControllerAnimated:YES];
//    }
//    else
//    {
//        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:msgSaveCount delegate:self cancelButtonTitle:msgbtnYes otherButtonTitles:msgbtnNo, nil]; 
//        alert.delegate = self;
//        alert.tag = tagAlertBack;
//        [alert show];        
//    }
}

- (void) reachabilityDidChanged: (NSNotification* )note
{
    if(![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]){
        [btnSubmit setEnabled:NO];
        [self setEnableBtnAtionHistory:NO];
    }
    else{
        [btnSubmit setEnabled:YES];
        [self setEnableBtnAtionHistory:YES];
    }
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if(!isDemoMode){
        //show wifi view
        [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationShowWifiView] object:nil];
        if(![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]){
            [btnSubmit setEnabled:NO];
        }
        else{
            [btnSubmit setEnabled:YES];
        }
    }
    if(isFromMainPosting){
        if(!isDemoMode){
            [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(reachabilityDidChanged:) name: kReachabilityChangedNotification object: nil];
        }
        [[HomeViewV2 shareHomeView] setSelectedButton:tagOfPostingButton];
    } else{
        [[HomeViewV2 shareHomeView] setSelectedButton:tagOfHomeButton];
    }
    [self setCaptionsView];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    if(!isDemoMode){
        [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
    }
}

#pragma mark - UIAlertView Delegate Methods
-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    switch (alertView.tag) {
        case tagUnpostData:
        {
            if(buttonIndex == 0) {
                //YES:
                [self loadViewData];
                
            } else {
                //NO: continue posting
                [self postAllOldAmenitiesData];
            }
        }
            break;
            
        case tagAlertBack:
        {
            switch (buttonIndex)
            {
                case 0:
                {
                    //YES: save Amenities
                    
                    //if tag is message
                    if ([[NSUserDefaults standardUserDefaults] integerForKey:TAG_OF_EVENT] == tagOfMessageButton) {
                        [[HomeViewV2 shareHomeView] handleTabbarEventWithTagOfButton:[[NSUserDefaults standardUserDefaults] integerForKey:TAG_OF_EVENT]];
                    }
                    else{
                        //back to count menu
                        [self.navigationController popViewControllerAnimated:YES];
                    }
                }
                    break;
                    
                case 1:
                {
                    //NO: show popup discard
                    UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:msgDiscardCount delegate:self cancelButtonTitle:msgbtnYes otherButtonTitles:msgbtnNo, nil]; 
                    alert.delegate = self;
                    alert.tag = tagAlertNo;
                    [alert show];  
                    
                }
                    break;
                    
                default:
                    break;
            }
        }
            break;
        case tagNoNetwork:
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
            break;
        case tagAlertNo:
        {
            switch (buttonIndex)
            {
                case 0:
                {
                    //YES
                    //if tag is message
                    if ([[NSUserDefaults standardUserDefaults] integerForKey:TAG_OF_EVENT] == tagOfMessageButton) {
                        [[HomeViewV2 shareHomeView] handleTabbarEventWithTagOfButton:[[NSUserDefaults standardUserDefaults] integerForKey:TAG_OF_EVENT]];
                    }
                    else{
                        //back to count menu, no save
                        [self.navigationController popViewControllerAnimated:YES];
                    }
                    
                }
                    break;
                    
                case 1:
                {
                    //NO: cart screen
                    //[self btnAddToCartPressed:nil];
                    //[self cartButtonPressed];
                    
                }
                    break;
                    
                default:
                    break;
            }
            
        }
            break;
            
        case tagAlertSubmit:
        {
            switch (buttonIndex)
            {
                case 0:
                {
                    //YES
                    statusSave = YES;
                    statusAddToCart = YES;
                    int postingResult = [self saveAmenities];
//                    [self handleBtnAddToCartPressed];
//                    [self saveData];
                    
                    if(postingResult == RESPONSE_STATUS_OK || postingResult == RESPONSE_STATUS_NEW_RECORD_ADDED) {
                        if(isFromMainPosting) {
                            [txtRoomNo setText:@""];
                            [tbvAmenities reloadData];
                        } else {
                            
                            NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
                            for (UIViewController *aViewController in allViewControllers) {
                                if ([aViewController isKindOfClass:[RoomAssignmentInfoViewController class]]) {
                                    ((RoomAssignmentInfoViewController*) aViewController).isAlreadyPostedAmenities = YES;
                                    [self.navigationController popToViewController:aViewController animated:YES];
                                }
                            }
                        }
                    }
                }
                    break;
                    
//                case 1:
//                {
//                    //NO: cart screen
//                    //[self btnAddToCartPressed:nil];
//                    //[self cartButtonPressed];
//                    NSInteger userId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
//                    NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
//                    if (logCheckNewValue == 0 && logCheckUsedValue == 0) {
//                        NSLog(@"<%@><Linen><UserId:%d><Not confirm to post><input RoomId:%@>", time, userId, txtRoom.text);
//                    }
////                    else {
////                        NSInteger totalSections = [self numberOfSectionsInTableView:tbvLinen];
////                        
////                        for (int i = 0; i<totalSections; i++) {
////                            NSInteger totalRow = [self tableView:self.tbvLinen numberOfRowsInSection:i];
////                            for (int j = 0; j<totalRow; j++) {
////                                
////                                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:j inSection:i];
////                                UITableViewCell *cell = [self.tbvLinen cellForRowAtIndexPath:indexPath];
////                                
////                                UILabel *lblItemName = (UILabel *)[cell viewWithTag:tagItemName];
////                                UIButton *btnUsed = (UIButton *)[cell viewWithTag:tagUsedButton];
////                                UIButton *btnNew = (UIButton *)[cell viewWithTag:tagNewButton];
////                                
////                                NSString *valueBtnUsed = btnUsed.titleLabel.text;
////                                NSString *valueBtnNew = btnNew.titleLabel.text;
////                                if ([valueBtnNew isEqualToString:@"0"] && [valueBtnUsed isEqualToString:@"0"]) {
////                                    
////                                }
////                                else {
////                                    NSLog(@"<%@><Linen><UserId:%d><Not confirm to post><Input RoomId:%@><ItemName:%@><ItemUsed:%@><ItemNew:%@>", time, userId, txtRoom.text, lblItemName.text, valueBtnUsed, valueBtnNew);
////                                }
////                            }
////                        }
////                    }
//                    
//                }
//                    break;
                    
                default:
                    break;
            }
            
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - Load Data
-(void) loadAccessRights
{
    AccessRightModel *currentUserAccessRight = [[UserManagerV2 sharedUserManager] currentUserAccessRight];
    QRCodeScanner = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.QRCodeScanner];
    postingAmenities = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.postAmenities];
    actionAmenities = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.actionAmenities];
}

-(void) loadDataAmenitiesCategoryFormService:(MBProgressHUD *) HUD 
{
    //enable flag to announce, is loading data
    [[HomeViewV2 shareHomeView] waitingLoadingData];
    
    int countUnpost = [amenManager countTotalAmenitiesByUserId:[UserManagerV2 sharedUserManager].currentUser.userId];
    if(countUnpost > 0) {
        haveOldAmenities = YES;
        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:@"" message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_notify_unpost_data] delegate:self cancelButtonTitle:msgbtnYes otherButtonTitles:msgbtnNo, nil];
        alert.delegate = self;
        [alert setTag:tagUnpostData];
        [alert show];
        
    } else {
        
        NSInteger userId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
        NSInteger hotelId = [[[UserManagerV2 sharedUserManager] currentUser] userHotelsId];
        
        //-----------WS------------
        [synManager getAmenitiesCategoryListWithUserId:userId AndHotelId:hotelId];
        [synManager getAmenitiesItemListWithUserId:userId AndHotelId:hotelId];
        if ([[UserManagerV2 sharedUserManager].currentUserAccessRight isLoadAmenitiesByRoomType]) {
            [synManager getRoomTypeInventoryWSByServiceType:ServiceType_Amenities userId:userId hotelId:hotelId];
        }
        //-----------end-----------
        
        [self loadDataAmenitiesCategory];
        
    }
    //hide saving data.
    [self performSelector:@selector(hiddenHUDAfterSaved:) withObject:HUD afterDelay:0.5];
    
    
    //end loading data
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
}

-(void)loadViewData {
    
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getRaLoadingData]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES];
    
    //enable flag to announce, is loading data
    [[HomeViewV2 shareHomeView] waitingLoadingData];
    
    NSInteger userId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    NSInteger hotelId = [[[UserManagerV2 sharedUserManager] currentUser] userHotelsId];
    
    //-----------WS------------
    [synManager getAmenitiesCategoryListWithUserId:userId AndHotelId:hotelId];
    [synManager getAmenitiesItemListWithUserId:userId AndHotelId:hotelId];
    if ([[UserManagerV2 sharedUserManager].currentUserAccessRight isLoadAmenitiesByRoomType]) {
        [synManager getRoomTypeInventoryWSByServiceType:ServiceType_Amenities userId:userId hotelId:hotelId];
    }
    //-----------end-----------
    
    [self loadDataAmenitiesCategory];
    
    //hide saving data.
    [self performSelector:@selector(hiddenHUDAfterSaved:) withObject:HUD afterDelay:0.5];
    
    
    //end loading data
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
}

- (void)loadDataAmenitiesCategory {
    
    AmenitiesManagerV2 *manager = [AmenitiesManagerV2 sharedAmenitiesManager];
    datas = [NSMutableArray array];
    NSMutableArray *cateArray;
    if ([[UserManagerV2 sharedUserManager].currentUserAccessRight isLoadAmenitiesByRoomType]) {
        [self getRoomTypeByRoomNo];
        cateArray = [manager loadAllAmenitiesCategoriesByRoomTypeId:_roomTypeId];
    } else {
        cateArray = [manager loadAllAmenitiesCategories];
    }
    for (AmenitiesCategoriesModelV2 *cateModel in cateArray) {
        AmenitiesSectionInfo *sectionInfo = [[AmenitiesSectionInfo alloc] init];
        sectionInfo.amenitiesCategory = cateModel;
        NSInteger cateId = cateModel.amcaId;
        NSMutableArray *listItem;
        if ([[UserManagerV2 sharedUserManager].currentUserAccessRight isLoadAmenitiesByRoomType]) {
            listItem = [manager loadAmenitiesItemsModelByCateId:cateId andRoomTypeId:_roomTypeId];
        } else {
            listItem = [manager loadAmenitiesItemsModelByCateId:cateId];
        }
        for (AmenitiesItemsModelV2 *itemModel in listItem) {
            [sectionInfo insertObjectToNextIndex:itemModel];
            
        }
        [datas addObject:sectionInfo];
    }
    statusAddToCart = YES;
    [tbvAmenities reloadData];
}

- (void)getRoomTypeByRoomNo {
    
    if (![txtRoomNo.text isEqualToString:@""]) {
        int userId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
        RoomTypeModel *rtModel = [[RoomManagerV2 sharedRoomManager] getRoomTypeByRoomNoDBWithUserId:userId andRoomNumber:txtRoomNo.text];
        if (rtModel != nil) {
            _roomTypeId = rtModel.amsId;
        } else {
            rtModel = [[RoomManagerV2 sharedRoomManager] getRoomTypeByRoomNoWSWithUserId:userId hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId roomNumber:txtRoomNo.text];
            _roomTypeId = rtModel != nil ? rtModel.amsId : -1;
        }
    }
}

-(void)postAllOldAmenitiesData
{
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    HUD.delegate = self;
    HUD.tag = tagHudPostAllOrders;
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getRaLoadingData]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES];
    
    //enable flag to announce, is loading data
    [[HomeViewV2 shareHomeView] waitingLoadingData];
    
    SyncManagerV2 *syncManager = [[SyncManagerV2 alloc] init];
    [syncManager postAllAmenitiesItemWithUserId:[UserManagerV2 sharedUserManager].currentUser.userId AndPercentView:nil];
    
    //hide saving data.
    [self performSelector:@selector(hiddenHUDAfterSaved:) withObject:HUD afterDelay:0.0f];
    
    //end loading data
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
}

//#pragma mark - Load data amenities room type from service
//-(void) loadDataAmenitiesFormService:(MBProgressHUD *) HUD {
//    //enable flag to announce, is loading data
//    [[HomeViewV2 shareHomeView] waitingLoadingData];
//    
//    
//    SyncManagerV2 *synManager = [[SyncManagerV2 alloc] init];
//    
//    NSInteger hotelId = [[[UserManagerV2 sharedUserManager] currentUser] userHotelsId];
//    
//    //-----------WS------------
//    [synManager getRoomTypeListWithHotelId:hotelId];
//    //-----------end-----------
//    
//    [self loadDataAmenitiesLocation];
//    
//    
//    //hide saving data.
//    [self performSelector:@selector(hiddenHUDAfterSaved:) withObject:HUD afterDelay:0.5];
//    
//    //end loading data
//    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
//}


//#pragma mark - Load Amenities Location
//-(void)loadDataAmenitiesLocation{
//    
//    AmenitiesManagerV2 *manager = [[AmenitiesManagerV2 alloc] init];
//    
//    datas = [[NSMutableArray alloc] init];
//    NSMutableArray *array = [manager loadAllAmenitiesService];
//    for (AmenitiesServiceModelV2 *amenitiesServiceModel in array) {
//        AmenitiesSectionInfo *sectionInfo = [[AmenitiesSectionInfo alloc] init];
////        AmenitiesPostingModel *apModel = [[AmenitiesPostingModel alloc] init];
//        AmenitiesOrderDetailsModelV2 *orderDetail = [[AmenitiesOrderDetailsModelV2 alloc] init];
//        orderDetail.amdId = amenitiesServiceModel.amsId;
//        //set language name amenities service
//        if ([[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString: (NSString *)ENGLISH_LANGUAGE] == NO) {
////            model.amenitiesLocationTitle = amenitiesServiceModel.amsNameLang;
//            [sectionInfo insertObjectToNextIndex:orderDetail];
//        }
//        else{
////            model.amenitiesLocationTitle = amenitiesServiceModel.amsName;
//            [sectionInfo insertObjectToNextIndex:orderDetail];
//        }
//        
//        sectionInfo.amenitiesService = amenitiesServiceModel;
//        [datas addObject:sectionInfo];
//    }
////    [datas addObjectsFromArray:listItems];
//    statusAddToCart = YES;
//    [tbvAmenities reloadData];
//}

#pragma mark - Hud Delegate
-(void)hudWasHidden:(MBProgressHUD *)hud
{
    if(hud.tag == tagHudPostAllOrders) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void) hiddenHUDAfterSaved:(MBProgressHUD *)HUD{
    [HUD hide:YES];
    [HUD removeFromSuperview];
    //    [HUD release];
}

#pragma mark - UITableView Delegate methods
#pragma mark - UITableView Datasource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return datas.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    AmenitiesSectionInfo *sectioninfo = [datas objectAtIndex:section];
    NSInteger numberRowInSection = sectioninfo.rowHeights.count;
    return sectioninfo.open ? numberRowInSection : 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifycell = @"cellidentify";
    AmenitiesItemCell *cell = nil;
    
    cell = (AmenitiesItemCell *)[tableView dequeueReusableCellWithIdentifier:identifycell];
    if (cell == nil) {
        NSArray *array = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([AmenitiesItemCell class]) owner:self options:nil];
        cell = [array objectAtIndex:0];
        
        //Round Rect border for ios 7
        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
            cell.btnNewItem.layer.cornerRadius = 7.0f;
            cell.btnNewItem.layer.borderWidth = 1.0f;
            cell.btnNewItem.layer.borderColor = [[UIColor lightGrayColor] CGColor];
            
            cell.btnUsedItem.layer.cornerRadius = 7.0f;
            cell.btnUsedItem.layer.borderWidth = 1.0f;
            cell.btnUsedItem.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        }
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.delegate = self;
    
    AmenitiesSectionInfo *sectionInfo = [datas objectAtIndex:indexPath.section];
    AmenitiesItemsModelV2 *itmModel = [sectionInfo.rowHeights objectAtIndex:indexPath.row];
    UIImage *image = [UIImage imageWithData:itmModel.itemImage];
//    if (image == nil) {
//        image = [UIImage imageNamed:@"no_image_2.png"];
//    }
    [cell.imgItem setImage:image];
//    if(indexPath.section == 0 && indexPath.row == 0){
    if (indexPath.row == 0) {
        [cell.lblUsedItem setText:[L_amenity_item_used currentKeyToLanguage]];
        [cell.lblNewItem setText:[L_amenity_item_new currentKeyToLanguage]];
        [cell.btnUsedItem setFrame:CGRectMake(189, 27, 54, 37)];
        [cell.btnNewItem setFrame:CGRectMake(256, 27, 54, 37)];
    } else {
        [cell.lblUsedItem setText:@""];
        [cell.lblNewItem setText:@""];
        [cell.btnUsedItem setFrame:CGRectMake(189, 18, 54, 37)];
        [cell.btnNewItem setFrame:CGRectMake(256, 18, 54, 37)];
    }
    if (statusAddToCart == YES) {
        [cell.btnNewItem setTitle:[NSString stringWithFormat:@"%d", 0] forState:UIControlStateNormal];
        [cell.btnUsedItem setTitle:[NSString stringWithFormat:@"%d", 0] forState:UIControlStateNormal];
    }
    else {
        [cell.btnNewItem setTitle:[NSString stringWithFormat:@"%d", (int)itmModel.itemNew] forState:UIControlStateNormal];
        [cell.btnUsedItem setTitle:[NSString stringWithFormat:@"%d", (int)itmModel.itemUsed] forState:UIControlStateNormal];
    }
    
    if ([[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString: (NSString *)ENGLISH_LANGUAGE] == NO) {
        [cell.lblTitleItem setText:itmModel.itemLang];
    } else {
        [cell.lblTitleItem setText:itmModel.itemName];
    }
   
    [cell.lblLine setBackgroundColor:[UIColor blackColor]];
     
    [cell setPath:indexPath];
    return cell;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 60;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {

    AmenitiesSectionInfo *sectionInfo = [datas objectAtIndex:section];
    
    if (sectionInfo.headerView == nil) {
        AmenitiesSection *headerView = [[AmenitiesSection alloc] initWithSection:section AmenitiesCateModelV2:sectionInfo.amenitiesCategory andOpenStatus:NO];
        sectionInfo.headerView = headerView;
        headerView.delegate = self;
    }
    return sectionInfo.headerView;
}

#pragma mark - Section Header Delegate
-(void)sectionHeaderView:(AmenitiesSection *)sectionheaderView sectionOpened:(NSInteger)section {
    
    AmenitiesSectionInfo *sectionInfo = [datas objectAtIndex:section];
    sectionInfo.open = YES;
    NSInteger countOfRowsToInsert = [sectionInfo.rowHeights count];
    
    NSMutableArray *indexPathsToInsert = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i < countOfRowsToInsert; i++) {
        [indexPathsToInsert addObject:[NSIndexPath indexPathForRow:i inSection:section]];
    }
    
    UITableViewRowAnimation insertAnimation;
    insertAnimation = UITableViewRowAnimationAutomatic;
    
    
    [tbvAmenities beginUpdates];
    
    [tbvAmenities insertRowsAtIndexPaths:indexPathsToInsert withRowAnimation:insertAnimation];
    [tbvAmenities endUpdates];
    if (countOfRowsToInsert > 0) {
        [tbvAmenities scrollToRowAtIndexPath:[indexPathsToInsert objectAtIndex:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
  
}

-(void)sectionHeaderView:(AmenitiesSection *)sectionheaderView sectionClosed:(NSInteger)section {
    
    AmenitiesSectionInfo *sectionInfo = [datas objectAtIndex:section];
    
    sectionInfo.open = NO;
    NSInteger countOfRowsToDelete = [tbvAmenities numberOfRowsInSection:section];
    
    if (countOfRowsToDelete > 0) {
        NSMutableArray *indexPathsToDelete = [[NSMutableArray alloc] init];
        for (NSInteger i = 0; i < countOfRowsToDelete; i++) {
            [indexPathsToDelete addObject:[NSIndexPath indexPathForRow:i inSection:section]];
        }
        
        [tbvAmenities beginUpdates];
        [tbvAmenities deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:UITableViewRowAnimationAutomatic];
        [tbvAmenities endUpdates];
        
        //        [indexPathsToDelete release];
    }
}

#pragma mark - Item Cell Delegate Methods

//- (void) btnQtyItemCellPressedWithIndexPath:(NSIndexPath *) path sender:(UIButton *)button{
//    //statusAddToCart = NO;
//    NSMutableArray *array = [[NSMutableArray alloc] init];
//    for (NSInteger index = 0; index <= 10; index++) {
//        [array addObject:[NSString stringWithFormat:@"%d", index]];
//    }
//    
//    PickerViewV2 *picker = [[PickerViewV2 alloc] initPickerViewV2WithDatas:array AndSelectedData:button.titleLabel.text AndIndex:path];
//    [picker showPickerViewV2];
//    [picker setDelegate:self];
//    [picker setTIndex:tQty];
//}

-(void) btnUsedItemCellPressedWithIndexPath:(NSIndexPath *) path sender:(UIButton *)button
{
    //statusAddToCart = NO;
    NSMutableArray *array = [[NSMutableArray alloc] init];
    for (NSInteger index = 0; index <= 10; index++) {
        [array addObject:[NSString stringWithFormat:@"%d", (int)index]];
    }
    
    PickerViewV2 *picker = [[PickerViewV2 alloc] initPickerViewV2WithDatas:array AndSelectedData:button.titleLabel.text AndIndex:path];
    [picker showPickerViewV2];
    [picker setDelegate:self];
    [picker setTIndex:tUsed];
}

-(void) btnNewItemCellPressedWithIndexPath:(NSIndexPath *) path sender:(UIButton *)button
{
    //statusAddToCart = NO;
    NSMutableArray *array = [[NSMutableArray alloc] init];
    for (NSInteger index = 0; index <= 10; index++) {
        [array addObject:[NSString stringWithFormat:@"%d", (int)index]];
    }
    
    PickerViewV2 *picker = [[PickerViewV2 alloc] initPickerViewV2WithDatas:array AndSelectedData:button.titleLabel.text AndIndex:path];
    [picker showPickerViewV2];
    [picker setDelegate:self];
    [picker setTIndex:tNew];
}

#pragma mark - PickerView V2 Delegate methods
-(void)didChoosePickerViewV2WithData:(NSString *)data AndIndex:(NSIndexPath *)index WithtIndex:(NSInteger)tIndex{

    AmenitiesSectionInfo *sectionInfo = [datas objectAtIndex:index.section];
//    AmenitiesPostingModel *apModel = [sectionInfo.rowHeights objectAtIndex:index.row];
//    AmenitiesOrderDetailsModelV2 *orderDetailModel = [sectionInfo.rowHeights objectAtIndex:index.row];
    AmenitiesItemsModelV2 *itemsModel = [sectionInfo.rowHeights objectAtIndex:index.row];
    
    if ([data integerValue] > 0) {
        statusAddToCart = NO;
    }
    itemsModel.itemUsed = [data integerValue];
    itemsModel.itemNew = [data integerValue];
    
    [tbvAmenities reloadRowsAtIndexPaths:[NSArray arrayWithObject:index] withRowAnimation:UITableViewRowAnimationFade];
    
    //[btnSubmit setEnabled:[self isValidQty]];
    
}

-(BOOL) isOnlyViewedAccessRight
{
    //Hao Tran[20130517/Check Access Right] - Check access right only view
    if(isFromMainPosting) {
        if (postingAmenities.isAllowedView) {
            [btnSubmit setEnabled:NO];
            return YES;
        }
    } else if(actionAmenities.isAllowedView) {
        [btnSubmit setEnabled:NO];
        return YES;
    }
    //Hao Tran[20130517/Check Access Right] - END
    return NO;
}

-(BOOL)isValidQty
{
    if([self isOnlyViewedAccessRight]) {
        return NO;
    }
    
    int a = 0;
    for(NSInteger i=0;i<[datas count];i++){
        
        AmenitiesSectionInfo *sectionInfo = [datas objectAtIndex:i];
        
        for (NSInteger j = 0;j < [sectionInfo.rowHeights count];j++){
            
            NSIndexPath *index = [NSIndexPath indexPathForRow:j inSection:i];
            AmenitiesItemCell *itemCell = (AmenitiesItemCell *)[self tableView:tbvAmenities cellForRowAtIndexPath:index];
            NSString *itemUsed = [NSString stringWithFormat:@"%@", [itemCell.btnUsedItem titleForState:UIControlStateNormal]];
            a = [itemUsed intValue];
            if (a != 0) {
                break;
            }
            
            NSString *itemNew = [NSString stringWithFormat:@"%@", [itemCell.btnNewItem titleForState:UIControlStateNormal]];
            a = [itemNew intValue];
            if (a != 0) {
                break;
            }
            
        }
        
        if (a != 0) {
            break;
        }
        
    }
    
    /*
    if (txtRoomNo.text.length > 0) {
        if(a!=0){
            return YES;
        }
        else 
            return NO;
    }
    else {
        return NO;
    }
    */
    
    if(a != 0) {
        return YES;
    }

    return NO;
}


#pragma mark - Saving Data
-(int)saveAmenities
{
    // @ show loading data .
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getRaSavingData]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES];
    
    if(!amenManager){
        amenManager = [AmenitiesManagerV2 sharedAmenitiesManager];
    }
    
    NSInteger roomAssignId = [TasksManagerV2 getCurrentRoomAssignment];
    NSInteger userId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    NSInteger hotelId = [[[UserManagerV2 sharedUserManager] currentUser] userHotelsId];
    
    //Remove for unnecessary
    /*
    //save order
    AmenitiesOrdersModelV2 *orderModel = [[AmenitiesOrdersModelV2 alloc] init];
    orderModel.amenRoomId = roomAssignId;
    orderModel.amenUserId = userId;
    orderModel.amenStatus = 1;
    
    orderModel.amenCollectDate = date;
    orderModel.amenService = 1;
    NSMutableArray *arrayOrder = [amenManager loadAllAmenitiesOrdersModelByUserId:userId];
    if ([arrayOrder count] == 0) {
        [amenManager insertAmenitiesOrdersModel:orderModel];
    }
    */
    
    
    NSString *date = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSMutableArray *amenItemsList = [NSMutableArray array];
    
    //Check is online or offline
    BOOL isOnline = [[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp];
    for (NSInteger indexSection=0; indexSection < datas.count; indexSection ++) {
        
        MiniBarSectionInfoV2 *sectionInfo = [datas objectAtIndex:indexSection];
        for (NSInteger indexRow = 0; indexRow < sectionInfo.rowHeights.count; indexRow++) {
            
            AmenitiesItemsModelV2 *itemsModel = [sectionInfo.rowHeights objectAtIndex:indexRow];
            BOOL isPostingZeroQuantity = [[UserManagerV2 sharedUserManager].currentUserAccessRight isPostingZeroQuantity];
            //Hao Tran[20141105 / Apply Configuration POSTING_POST_ZEROTH_VALUE] - if 0 follow normal, if 1 post 0
            if(!isPostingZeroQuantity && (itemsModel.itemNew == 0 && itemsModel.itemNew == 0)) {
                continue;
            }
            
            //Hao Tran[20141105 / Apply Configuration POSTING_POST_ZEROTH_VALUE] - if 0 follow normal, if 1 post 0
            //if (itemsModel.itemNew != 0 || itemsModel.itemUsed != 0) {
                
                AmenitiesOrderDetailsModelV2 *detailModel = [[AmenitiesOrderDetailsModelV2 alloc] init];
                //detailModel.amdRoomTypeId = itemsModel.itemRoomType;
                //detailModel.amdAmenitiesOrderId = 0;
                
                detailModel.amdItemId = itemsModel.itemId;
                detailModel.amdCategoryId = itemsModel.itemCategoryId;
                detailModel.amdUsedQuantity = itemsModel.itemUsed;
                detailModel.amdNewQuantity = itemsModel.itemNew;
                detailModel.amdTractionDateTime = date;
                detailModel.amdRoomAssignId = roomAssignId;
                detailModel.amdUserId = userId;
                detailModel.amdRoomNumber = txtRoomNo.text;
                detailModel.amdHotelId = hotelId;
                
                //Save in offline mode
                if(isDemoMode){
                    [amenItemsList addObject:detailModel];
                }
                else{
                    [amenManager insertAmenitiesOrderDetailsModel:detailModel];
                    [amenItemsList addObject:detailModel];
                }
                
                /*
                int logWSvalue = [[NSUserDefaults standardUserDefaults] integerForKey:ENABLE_LOGWS];
                if(logWSvalue > 0) {
                    SyncManagerV2 *syncManager = [[SyncManagerV2 alloc] init];
                    //--------Start Post WSLog
                    NSString *messageValue = [NSString stringWithFormat:@"Post Amenities WSLog:Time:%@, UserId:%d, RoomId:%@, ItemName:%@(ID:%d), Used Quantity:%d, New Quantity:%d", date, userId, txtRoomNo.text, itemsModel.itemName, itemsModel.itemId,itemsModel.itemUsed, itemsModel.itemNew];
                    [syncManager postWSLogAmenities:userId Message:messageValue MessageType:1];
                    //--------End Post WSLog
                }*/
                //Hao Tran - Remove for change WS
                /*
                postSuccess = [syncManager postAmenitiesItemWithUserId:userId RoomAssignId:roomAssignId AndAmenitiesItem:detailModel WithDate:date];
                
                if (postSuccess) {
                    [amenManager deleteAmenitiesOrderDetailsModel:detailModel];
                }*/
            //}
        }
    }
    int result = RESPONSE_STATUS_NO_NETWORK;
    if (isDemoMode) {
        result = RESPONSE_STATUS_OK;
        [amenManager saveAmenitiesHistoryItems:amenItemsList];
    } else {
        if (isOnline) {
            if(amenItemsList.count > 0) {
                result = [amenManager postAmenitiesByUserId:[UserManagerV2 sharedUserManager].currentUser.userId amenitiesItems:amenItemsList];
            }
        }
        
        if (!isOnline) {
            UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:@"" message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_internet_exception] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getOK] otherButtonTitles:nil];
            alert.delegate = self;
            [alert setTag:tagNoNetwork];
            [alert show];
        } else if (result != RESPONSE_STATUS_OK && result != RESPONSE_STATUS_NEW_RECORD_ADDED) {
            
            //statusAddToCart = YES will clear data after posted success
            //statusAddToCart = NO will not clear data
            statusAddToCart = NO; //Because of some errors occured so we show information error.
            [[[[iToast makeText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_error_posting]] setGravity:iToastGravityBottom] setDuration:iToastDurationNormal] show];
        } else { //Post successfull
            if(haveOldAmenities) {
                [synManager postAllAmenitiesItemWithUserId:[UserManagerV2 sharedUserManager].currentUser.userId AndPercentView:nil];
            }
            if ([[UserManagerV2 sharedUserManager].currentUserAccessRight isLoadAmenitiesByRoomType]) {
                [txtRoomNo setText:@""];
                _roomTypeId = 0;
                [self loadDataAmenitiesCategory];
            }
            //[amenManager saveAmenitiesHistoryItems:amenItemsList];
        }
    }
    
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getMsgSaveMessage]];
    //hide saving data.
    [self performSelector:@selector(hiddenHUDAfterSaved:) withObject:HUD afterDelay:0.0f];
    
    return result;
}

#pragma mark - set Captions View
-(void)setCaptionsView {
//    [btnAddToCart setTitle:[[LanguageManagerV2 sharedLanguageManager] getAddToCart] forState:UIControlStateNormal];
    [self setTitle:[[LanguageManagerV2 sharedLanguageManager] getAmenities]];
    
    msgbtnNo = [[LanguageManagerV2 sharedLanguageManager] getNo];
    msgbtnYes = [[LanguageManagerV2 sharedLanguageManager] getYes];
    msgSaveCount = [[LanguageManagerV2 sharedLanguageManager] getMsgAlertSaveCounts];
    msgDiscardCount = [[LanguageManagerV2 sharedLanguageManager] getMsgAlertDiscardCounts];
    
    //refresh topview
//    TopbarViewV2 *topview = [self getTopBarView];
//    [topview refresh:[[SuperRoomModelV2 alloc] init]];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if([string isEqualToString:@"\n"])
    {
        [textField resignFirstResponder];
        return NO;
    }
    return YES;
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
//    [btnSubmit setEnabled:[self isValidQty]];
    [textField resignFirstResponder];
    if ([[UserManagerV2 sharedUserManager].currentUserAccessRight isLoadAmenitiesByRoomType] && ![txtRoomNo.text isEqualToString:@""]) {
        [self performSelector:@selector(loadDataAmenitiesCategory) withObject:nil];
    }
    [self setEnableBtnAtionHistory:YES];
    return YES;
}

- (NSMutableString*) aggregateData {
    
    NSMutableString *confirmData = [NSMutableString string];
    
    for(NSInteger i = 0; i<[datas count]; i++) {
        
        AmenitiesSectionInfo *sectioninfo = [datas objectAtIndex:i];
        for (NSInteger j = 0; j < [sectioninfo.rowHeights count]; j++) {
            
            NSIndexPath *index = [NSIndexPath indexPathForRow:j inSection:i];
            AmenitiesItemCell *amenItem = (AmenitiesItemCell *)[self tableView:tbvAmenities cellForRowAtIndexPath:index];
            NSString *itemUseStr = [NSString stringWithFormat:@"%@", [amenItem.btnUsedItem titleForState:UIControlStateNormal]];
            if ([itemUseStr integerValue] != 0) {
                [confirmData appendString:[NSString stringWithFormat:@"\n%@ - %@", itemUseStr, [amenItem.lblTitleItem text]]];
            }
        }
    }

    return confirmData;
    
}

//Thien Chau: CRF-00001277 > [Marriott] Confirmation Message before Submit Posting on Minibar / Engineering / Amenities / Lost & Found
- (void)alertView:(DTAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        //YES
        statusSave = YES;
        statusAddToCart = YES;
        int postingResult = [self saveAmenities];
        //                    [self handleBtnAddToCartPressed];
        //                    [self saveData];
        
        if (postingResult == RESPONSE_STATUS_OK || postingResult == RESPONSE_STATUS_NEW_RECORD_ADDED) {
            if (isFromMainPosting) {
                [txtRoomNo setText:@""];
                [tbvAmenities reloadData];
            } else {
                
                NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
                for (UIViewController *aViewController in allViewControllers) {
                    if ([aViewController isKindOfClass:[RoomAssignmentInfoViewController class]]) {
                        ((RoomAssignmentInfoViewController*) aViewController).isAlreadyPostedAmenities = YES;
//                        [self.navigationController popToViewController:aViewController animated:YES];
                        [self.navigationController popViewControllerAnimated:YES];
                    }
                }
            }
            [[[[iToast makeText:[[LanguageManagerV2 sharedLanguageManager] getMsgSaveMessage]] setGravity:iToastGravityBottom] setDuration:iToastDurationNormal] show];
        }
    }
}

- (IBAction)btnPostingHistory_Clicked:(UIButton *)sender {
    HistoryPosting *historyPosting = [[HistoryPosting alloc] initWithNibName:@"HistoryPosting" bundle:nil];
    //    historyPosting.isPushFrom = IS_PUSHED_FROM_PHYSICAL_CHECK;
    historyPosting.roomID = txtRoomNo.text;
    historyPosting.isFromPostingFunction = isFromMainPosting;
    historyPosting.selectedModule = ModuleHistory_Amenities;
    [self.navigationController pushViewController:historyPosting animated:YES];
}

-(IBAction) btnSubmit_Clicked:(id)sender
{
    NSString *roomNo = [txtRoomNo text];
    if(roomNo.length > 0 && [self isValidQty]) {
        RoomAssignmentModelV2 *roomAssignment = [[RoomAssignmentModelV2 alloc] init];
        roomAssignment.roomAssignment_RoomId = roomNo;
        roomAssignment.roomAssignment_UserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
        BOOL result = [[RoomManagerV2 sharedRoomManager] getRoomAssignmentByUserIDAndRoomID:roomAssignment];
        if([[UserManagerV2 sharedUserManager].currentUserAccessRight isValidateRoom] && !isDemoMode) {
            NSString *hotelId = [NSString stringWithFormat:@"%d", [[[UserManagerV2 sharedUserManager] currentUser] userHotelsId]];
            result = [[RoomManagerV2 sharedRoomManager] isValidRoomWSByUserId:[UserManagerV2 sharedUserManager].currentUser.userId roomNumber:roomNo hotelId:hotelId];
        } else {
            result = TRUE; //Force posting mini bar without checking validation room
        }
        //Hao Tran
        //No need to check data from main posting
        //        if(isFromMainPosting){
        //            if([[UserManagerV2 sharedUserManager].currentUserAccessRight isValidateRoom]) {
        //                result = [[RoomManagerV2 sharedRoomManager] isValidRoomWSByUserId:[UserManagerV2 sharedUserManager].currentUser.userId roomNumber:roomNo];
        //            } else {
        //                result = TRUE; //Force posting mini bar without checking validation room
        //            }
        //        }
        
        if(result)
        {
            [TasksManagerV2 setCurrentRoomAssignment:roomAssignment.roomAssignment_Id];
            //NSString *mess = [NSString stringWithFormat:@"Room %@ \n Submit Amenities Items?", roomNo];
            //NSString *mess = [NSString stringWithFormat:@"%@ %@ \n %@%@", [L_room currentKeyToLanguage],roomNo,[L_submit_amenities_items currentKeyToLanguage],@"?"];
            NSString *mess = [NSString stringWithFormat:@"%@ %@ \n %@%@ \n %@", [L_room currentKeyToLanguage],roomNo,[L_submit_amenities_items currentKeyToLanguage],@"?", [self aggregateData]];
            DTAlertView *alertView = [DTAlertView alertViewWithTitle:[L_confirm_title currentKeyToLanguage] message:mess delegate:self cancelButtonTitle:[L_YES currentKeyToLanguage] positiveButtonTitle:[L_NO currentKeyToLanguage]];
            [alertView show];
        } else {
            
            NSString *message = nil;
            if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                message = [NSString stringWithFormat:@"%@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOM_INVALID]];
            } else {
                message = [NSString stringWithFormat:@"\n\n\n\n\n%@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOM_INVALID]];
            }
            
            UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObject:[UIImage imageNamed:@"yes_bt.png"]] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:message delegate:nil cancelButtonTitle:[L_CLOSE currentKeyToLanguage] otherButtonTitles: nil];
            
            if(!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                UIImageView *alertImage = [[UIImageView alloc] initWithFrame:CGRectMake(80, 35, 120, 120)];
                [alertImage setImage:[UIImage imageNamed:@"icon_wrong room.png"]];
                [alert addSubview:alertImage];
            }
            [alert show];
        }
    } else {
        if (roomNo.length > 0) {
            NSString *message = nil;
            if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                message = [NSString stringWithFormat:@"%@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_INVALID_DATA]];
            } else {
                message = [NSString stringWithFormat:@"\n\n\n\n\n%@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_INVALID_DATA]];
            }
            
            UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObject:[UIImage imageNamed:@"yes_bt.png"]] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:message delegate:nil cancelButtonTitle:[L_CLOSE currentKeyToLanguage] otherButtonTitles: nil];
            
            if(!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                UIImageView *alertImage = [[UIImageView alloc] initWithFrame:CGRectMake(80, 35, 120, 120)];
                [alertImage setImage:[UIImage imageNamed:@"icon_wrong room.png"]];
                [alert addSubview:alertImage];
            }
            [alert show];
        } else {
            NSString *message = nil;
            if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                message = [NSString stringWithFormat:@"%@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOM_INVALID]];
            } else {
                message = [NSString stringWithFormat:@"\n\n\n\n\n%@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOM_INVALID]];
            }
            
            UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObject:[UIImage imageNamed:@"yes_bt.png"]] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:message delegate:nil cancelButtonTitle:[L_CLOSE currentKeyToLanguage] otherButtonTitles: nil];
            
            if(!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                UIImageView *alertImage = [[UIImageView alloc] initWithFrame:CGRectMake(80, 35, 120, 120)];
                [alertImage setImage:[UIImage imageNamed:@"icon_wrong room.png"]];
                [alert addSubview:alertImage];
            }
            [alert show];
            
        }
    }
}

#pragma mark - Handle Topbar Methods
-(void)loadTopbarView {
    TopbarViewV2 *topbar = [[TopbarViewV2 alloc] initViewWithSuperModel:[[CommonVariable sharedCommonVariable] superRoomModel]];
    [topbar setTag:tagTopbarView];
    [self.view addSubview:topbar];
    [topbar setHidden:YES];
    
    [topbar setAlpha:0.0];
    [self addButtonHandleShowHideTopbar];
}

-(void)showTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationWillStartSelector:@selector(setHiddenTopbarView)];
    
    f = topbar.frame;
    f.origin.y = 0;
    [topbar setFrame:f];
    [topbar setAlpha:1.0];
    
    //adjust all view when show
    CGRect fscv = scrollView.frame;
    
    CGRect viewRoomNoFrame = viewRoomNumber.frame;
    viewRoomNoFrame.origin.y += f.size.height;
    [viewRoomNumber setFrame:viewRoomNoFrame];
    
    [scrollView setFrame:CGRectMake(0, viewRoomNoFrame.size.height + f.size.height, 320, fscv.size.height - f.size.height)];
    
//    CGRect tbAmenitiesFrame = tbvAmenities.frame;
//    tbAmenitiesFrame.size.height -= f.size.height;
//    [tbvAmenities setFrame:tbAmenitiesFrame];
//    
//    CGRect btnSubmitFrame = btnSubmit.frame;
//    btnSubmitFrame.origin.y -= f.size.height;
//    [btnSubmit setFrame:btnSubmitFrame];
//    
//    CGRect lblSubmitFrame = lblSubmit.frame;
//    lblSubmitFrame.origin.y -= f.size.height;
//    [lblSubmit setFrame:lblSubmitFrame];
    
    [UIView commitAnimations];
}

-(void)hideTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDidStopSelector:@selector(setHiddenTopbarView)];
    [UIView setAnimationDelegate:self];
    
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    [topbar setAlpha:0.0];
    
    CGRect viewRoomNoFrame = viewRoomNumber.frame;
    viewRoomNoFrame.origin.y -= f.size.height;
    [viewRoomNumber setFrame:viewRoomNoFrame];
    
    //adjust all views when hide
    CGRect fscv = scrollView.frame;
    [scrollView setFrame:CGRectMake(0, viewRoomNoFrame.size.height, 320, fscv.size.height + f.size.height)];
    
//    CGRect tbAmenitiesFrame = tbvAmenities.frame;
//    tbAmenitiesFrame.size.height += f.size.height;
//    [tbvAmenities setFrame:tbAmenitiesFrame];
//    
//    CGRect btnSubmitFrame = btnSubmit.frame;
//    btnSubmitFrame.origin.y += f.size.height;
//    [btnSubmit setFrame:btnSubmitFrame];
//    
//    CGRect lblSubmitFrame = lblSubmit.frame;
//    lblSubmitFrame.origin.y += f.size.height;
//    [lblSubmit setFrame:lblSubmitFrame];
    
    [UIView commitAnimations];
}

-(void)setHiddenTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    [topbar setHidden:!topbar.hidden];
}

-(TopbarViewV2 *)getTopBarView {
    return (TopbarViewV2 *)[self.view viewWithTag:tagTopbarView];
}

-(void)addButtonHandleShowHideTopbar {
    UIView *vBarButton = [[UIView alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
    UIButton *titleBarButtonFirst = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 200, 30)];
    
    titleBarButtonFirst.titleLabel.backgroundColor = [UIColor clearColor];
    [titleBarButtonFirst.titleLabel setFont: FONT_BUTTON_TOPBAR];
    titleBarButtonFirst.titleLabel.textAlignment = NSTextAlignmentCenter;
    titleBarButtonFirst.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
    
    [titleBarButtonFirst.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
    [titleBarButtonFirst.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
    [titleBarButtonFirst addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    [titleBarButtonFirst setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_Counts] forState:UIControlStateNormal];
    
    [vBarButton addSubview:titleBarButtonFirst];
    
    
    UIButton *titleBarButtonSecond = [[UIButton alloc] initWithFrame:CGRectMake(0, 28, 200, 22)];
    [titleBarButtonSecond setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [titleBarButtonSecond.titleLabel setFont:FONT_BUTTON_TOPBAR_SECOND];
    [titleBarButtonSecond setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_AMENITIES] forState:UIControlStateNormal];
    [titleBarButtonSecond addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    [vBarButton addSubview:titleBarButtonSecond];
    
    self.navigationItem.titleView = vBarButton;
    //Move to center vertical
    CGRect titleViewBounds = self.navigationItem.titleView.bounds;
    self.navigationItem.titleView.bounds = CGRectMake(titleViewBounds.origin.x, Y_TOPBAR_TITLE, titleViewBounds.size.width, titleViewBounds.size.height);
}

-(void)topbarTapped:(UIButton *)sender {
    TopbarViewV2 *topbar = [self getTopBarView];
    [sender setEnabled:NO];
    if (topbar.isHidden) {
        [self showTopbarView];
    } else {
        [self hideTopbarView];
    }
    [self performSelector:@selector(topbarButtonEnable:) withObject:sender afterDelay:0.5];
}

-(void) topbarButtonEnable:(UIButton *) topbarBtn {
    [topbarBtn setEnabled:YES];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - QR Code Button Touched
-(IBAction)btnQRCodeScannerTouched:(id) sender
{
    ZXingWidgetController *widController = [[ZXingWidgetController alloc] initWithDelegate:self showCancel:YES OneDMode:NO showLicense:NO];
    QRCodeReader* qrcodeReader = [[QRCodeReader alloc] init];
    NSSet *readers = [[NSSet alloc ] initWithObjects:qrcodeReader,nil];
    widController.readers = readers;
    [self presentViewController:widController animated:YES completion:nil];
}

#pragma mark - ZxingDelegate
- (void)zxingController:(ZXingWidgetController*)controller didScanResult:(NSString *)resultString {
    resultString = [STEncryptorDES convertText:resultString];
    [self dismissViewControllerAnimated:YES completion:nil];
    
    txtRoomNo.text = resultString;
    [self textFieldShouldReturn:txtRoomNo];
}

- (void)zxingControllerDidCancel:(ZXingWidgetController*)controller {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - iOS 7 Resources
-(void)loadFlatResource
{
    [imgHeader setImage:[UIImage imageNamed:imgGuestInfoActiveFlat]];
    [btnSubmit setImage:[UIImage imageNamed:imgBtnSubmitFlat] forState:UIControlStateNormal];
    [btnSubmit setImage:[UIImage imageNamed:imgBtnSubmitClickedFlat] forState:UIControlStateHighlighted];
    
    [lblSubmit setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_SUBMIT]];
    [btnPostingHistory setBackgroundImage:[UIImage imageNamed:imgHistoryPostingBtnFlat] forState:UIControlStateHighlighted];
    [btnPostingHistory setBackgroundImage:[UIImage imageNamed:imgHistoryPostingBtnFlat] forState:UIControlStateNormal];
}
-(void) setEnableBtnAtionHistory:(BOOL) isEnable
{
    if(![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]){
        isEnable = NO;
    }
    [btnPostingHistory setUserInteractionEnabled:isEnable];
    [btnPostingHistory setBackgroundImage:[UIImage imageBeforeiOS7:(isEnable?imgCleaningStatusBtnAndActionBtn:imgCleaningStatusBtnAndActionBtnDisable)
                                                         equaliOS7:(isEnable?imgCleaningStatusBtnAndActionBtnFlat:imgCleaningStatusBtnAndActionBtnDisableFlat)]
                                 forState:UIControlStateNormal];
    [btnPostingHistory setBackgroundImage:[UIImage imageBeforeiOS7:(isEnable?imgCleaningStatusBtnAndActionBtnClicked:imgCleaningStatusBtnAndActionBtnDisable)
                                                         equaliOS7:(isEnable?imgCleaningStatusBtnAndActionBtnClickedFlat:imgCleaningStatusBtnAndActionBtnDisableFlat)]
                                 forState:UIControlEventTouchDown];
    [btnPostingHistory setTitle:[L_action_history currentKeyToLanguage] forState:UIControlStateNormal];
}

@end
