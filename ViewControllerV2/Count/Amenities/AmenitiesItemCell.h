//
//  AmenitiesItemCell.h
//  mHouseKeeping
//
//  Created by Quang Nguyen on 5/2/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol ItemCellDelegate <NSObject>

@optional
-(void) btnUsedItemCellPressedWithIndexPath:(NSIndexPath *) path sender:(UIButton *)button;
-(void) btnNewItemCellPressedWithIndexPath:(NSIndexPath *) path sender:(UIButton *)button;
@end

@interface AmenitiesItemCell : UITableViewCell{
    __unsafe_unretained id<ItemCellDelegate> delegate;
    NSIndexPath *path;
}
@property (nonatomic, strong) NSIndexPath *path; 
@property (nonatomic, assign) id<ItemCellDelegate> delegate;
@property (strong, nonatomic) IBOutlet UIImageView *imgItem;
@property (strong, nonatomic) IBOutlet UILabel *lblTitleItem;

@property (strong, nonatomic) IBOutlet UILabel *lblUsedItem;
@property (strong, nonatomic) IBOutlet UIButton *btnUsedItem;

@property (strong, nonatomic) IBOutlet UILabel *lblNewItem;
@property (strong, nonatomic) IBOutlet UIButton *btnNewItem;

@property (nonatomic, strong) IBOutlet UILabel *lblLine;

@end
