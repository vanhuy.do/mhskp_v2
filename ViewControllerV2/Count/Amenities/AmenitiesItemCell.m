//
//  AmenitiesItemCell.m
//  mHouseKeeping
//
//  Created by Quang Nguyen on 5/2/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "AmenitiesItemCell.h"

@implementation AmenitiesItemCell

@synthesize imgItem;
@synthesize lblTitleItem;
@synthesize path;
@synthesize delegate;
@synthesize lblLine;

@synthesize lblNewItem,lblUsedItem, btnNewItem, btnUsedItem;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc {
    //    [imgItem release];
    //    [lblTitleItem release];
    //    [lblUsed release];
    //    [lblNew release];
    //    [btnUsed release];
    //    [btnNew release];
    //    [super dealloc];
}

- (IBAction)btnUsedItemPressed:(id)sender {
    if ([delegate respondsToSelector:@selector(btnUsedItemCellPressedWithIndexPath:sender:)]) {
        [delegate btnUsedItemCellPressedWithIndexPath:path sender:sender];
    }
}

- (IBAction)btnNewItemPressed:(id)sender {
    if ([delegate respondsToSelector:@selector(btnNewItemCellPressedWithIndexPath:sender:)]) {
        [delegate btnNewItemCellPressedWithIndexPath:path sender:sender];
    }
}

@end
