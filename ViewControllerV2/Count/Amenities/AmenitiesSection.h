//
//  AmenitiesSection.h
//  mHouseKeeping
//
//  Created by Quang Nguyen on 5/2/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AmenitiesCategoryModelV2.h"

@class AmenitiesSection;
@protocol AmenitiesSectionDelegate <NSObject>

@optional
-(void) sectionHeaderView:(AmenitiesSection *) sectionheaderView sectionOpened:(NSInteger) section;
-(void) sectionHeaderView:(AmenitiesSection *) sectionheaderView sectionClosed:(NSInteger) section;
@end

@interface AmenitiesSection : UIView{
    
    UIImageView *imgSectionAmenitiesCategory;
    UILabel *lblSectionAmenitiesCategory;
    UIImageView *imgArrowSectionAmenitiesCategory;
    NSInteger section;
    BOOL isToggle;
    __unsafe_unretained id<AmenitiesSectionDelegate> delegate;

}
//@property (nonatomic, assign) id<AmenitiesSectionDelegate> delegate;
@property (nonatomic, assign) __unsafe_unretained id<AmenitiesSectionDelegate> delegate;
@property (nonatomic, assign) NSInteger section;
@property (strong, nonatomic) IBOutlet UIImageView *imgSectionAmenitiesCategory;
@property (strong, nonatomic) IBOutlet UILabel *lblSectionAmenitiesCategory;
@property (strong, nonatomic) IBOutlet UIImageView *imgArrowSectionAmenitiesCategory;
-(id) initWithSection:(NSInteger) sectionIndex AmenitiesCateModelV2:(AmenitiesCategoriesModelV2 *) cateModel andOpenStatus:(BOOL) isOpen;
@end
