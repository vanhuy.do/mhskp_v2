//
//  AmenitiesSection.m
//  mHouseKeeping
//
//  Created by Quang Nguyen on 5/2/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "AmenitiesSection.h"
#import "ehkDefinesV2.h"
#import "UIImage+Tint.h"

@implementation AmenitiesSection
@synthesize imgSectionAmenitiesCategory, 
imgArrowSectionAmenitiesCategory,
lblSectionAmenitiesCategory,
section,
delegate;

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void) initControls {
    [self setFrame:CGRectMake(0, 0, 320, 60)];
    
    UIImageView *bg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 60)];
    [bg setImage:[UIImage imageBeforeiOS7:imgBgBigBtn equaliOS7:imgBgBigBtnFlat]];
    [self addSubview:bg];
    
    imgSectionAmenitiesCategory = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 70, 60)];
    [imgSectionAmenitiesCategory setContentMode:UIViewContentModeScaleAspectFit];
    [self addSubview:imgSectionAmenitiesCategory];
    
    lblSectionAmenitiesCategory = [[UILabel alloc] initWithFrame:CGRectMake(70, 5, 220, 50)];
    [lblSectionAmenitiesCategory setFont:[UIFont fontWithName:@"Arial-BoldMT" size:17]];
    [lblSectionAmenitiesCategory setBackgroundColor:[UIColor clearColor]];
    [lblSectionAmenitiesCategory setTextColor:[UIColor colorWithRed:55.0/255.0 green:55.0/255.0 blue:55.0/255.0 alpha:1.0]];
    [lblSectionAmenitiesCategory setTextAlignment:NSTextAlignmentCenter];
    [self addSubview:lblSectionAmenitiesCategory];
    
    imgArrowSectionAmenitiesCategory = [[UIImageView alloc] initWithFrame:CGRectMake(285, 15, 30, 30)];
    [imgArrowSectionAmenitiesCategory setImage:[UIImage imageBeforeiOS7:imgRightRow equaliOS7:imgRightRowFlat]];
    [self addSubview:imgArrowSectionAmenitiesCategory];

    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(0, 0, 320, 70)];
    [button addTarget:self action:@selector(btnAmenSectionViewV2Pressed:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:button];
}

-(id)init {
    self = [super init];
    if (self) {
        [self initControls];
    }
    return self;
}

-(id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initControls];
    }
    return self;
}

- (void)dealloc {
    //    [imgLinenSection release];
    //    [lblLinenTitle release];
    //    [imgDetail release];
    //    [super dealloc];
}

-(void) toggleOpenWithUserAction:(BOOL) userAction {
    if (userAction) {
        if (isToggle == NO) {
            if ([delegate respondsToSelector:@selector(sectionHeaderView:sectionOpened:)]) {
                isToggle = YES;
                [delegate sectionHeaderView:self sectionOpened:section];
                
                [imgArrowSectionAmenitiesCategory setImage:[UIImage imageBeforeiOS7:imgRightRowOpen equaliOS7:imgRightRowOpenFlat]];
            }
        }else {
            if ([delegate respondsToSelector:@selector(sectionHeaderView:sectionClosed:)]) {
                isToggle = NO;
                [imgArrowSectionAmenitiesCategory setImage:[UIImage imageBeforeiOS7:imgRightRow equaliOS7:imgRightRowFlat]];
                [delegate sectionHeaderView:self sectionClosed:section];
            }
        }
    }
}

- (IBAction)btnAmenSectionViewV2Pressed:(id)sender {
    [self toggleOpenWithUserAction:YES];
}

-(id) initWithSection:(NSInteger) sectionIndex AmenitiesCateModelV2:(AmenitiesCategoriesModelV2 *) cateModel andOpenStatus:(BOOL) isOpen {
    self = [self init];
    
    if (self) {
        section = sectionIndex;
        if([[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString:[NSString stringWithFormat:@"%@",ENGLISH_LANGUAGE]]){
            lblSectionAmenitiesCategory.text = cateModel.amcaName;
        } else {
            lblSectionAmenitiesCategory.text = cateModel.amcaNameLang;
        }
        isToggle = isOpen;
        [imgSectionAmenitiesCategory setImage:[UIImage imageWithData:cateModel.amcaImage]];
        if (isOpen) {
            [imgArrowSectionAmenitiesCategory setImage:[UIImage imageBeforeiOS7:imgRightRowOpen equaliOS7:imgRightRowOpenFlat]];
        } else {
            [imgArrowSectionAmenitiesCategory setImage:[UIImage imageBeforeiOS7:imgRightRow equaliOS7:imgRightRowFlat]];
        }
    }
    
    return self;
}

@end
