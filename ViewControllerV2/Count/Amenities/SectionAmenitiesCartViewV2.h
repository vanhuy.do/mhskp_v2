//
//  SectionAmenitiesCartViewV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SectionAmenitiesCartViewV2 : UIView{
    UIImageView *imgSectionAmenitiesCategory;
    UILabel *lblSectionAmenitiesCategory;
    UIImageView *imgArrowSectionAmenitiesCategory;
}
@property (nonatomic, assign) NSInteger section;
@property (strong, nonatomic) IBOutlet UIImageView *imgSectionAmenitiesCategory;
@property (strong, nonatomic) IBOutlet UILabel *lblSectionAmenitiesCategory;
@property (strong, nonatomic) IBOutlet UIImageView *imgArrowSectionAmenitiesCategory;
-(id) initWithSection:(NSInteger) sectionIndex contentImg:(NSData *)imgName contentName:(NSString *)content AndArrow: (NSData *) imgArrow;
@end
