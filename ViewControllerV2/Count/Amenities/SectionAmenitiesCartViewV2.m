//
//  SectionAmenitiesCartViewV2.m
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SectionAmenitiesCartViewV2.h"

@implementation SectionAmenitiesCartViewV2
@synthesize imgSectionAmenitiesCategory, imgArrowSectionAmenitiesCategory, lblSectionAmenitiesCategory, section;

-(id)init{
    self = [super init];
    if(self){
        [self setFrame:CGRectMake(0, 0, 320, 60)];
        [self setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"big_bt_640x111.png"]]];
        
        imgSectionAmenitiesCategory = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 70, 60)];
       
        lblSectionAmenitiesCategory = [[UILabel alloc] initWithFrame:CGRectMake(100, 10, 180, 39)];
        [lblSectionAmenitiesCategory setFont:[UIFont fontWithName:@"Arial-BoldMT" size:17]];
        [lblSectionAmenitiesCategory setBackgroundColor:[UIColor clearColor]];
        [lblSectionAmenitiesCategory setTextColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:1]];
        [lblSectionAmenitiesCategory setNumberOfLines:2];
        [lblSectionAmenitiesCategory setLineBreakMode:NSLineBreakByWordWrapping];
        
        imgArrowSectionAmenitiesCategory = [[UIImageView alloc] initWithFrame:CGRectMake(288, 17, 25, 25)];
        [imgArrowSectionAmenitiesCategory setImage:[UIImage imageNamed:@"detail_open_59x60.png"]];
        
        [self addSubview:imgSectionAmenitiesCategory];
        [self addSubview:lblSectionAmenitiesCategory];
        [self addSubview:imgArrowSectionAmenitiesCategory];
        
    }
    return self;
}

-(id)initWithSection:(NSInteger)sectionIndex contentImg:(NSData *)imgName contentName:(NSString *)content AndArrow:(NSData *)imgArrow{
    self = [self init];
    if(self)
    {
        section = sectionIndex;
        [imgSectionAmenitiesCategory setImage:[UIImage imageWithData:imgName]];
        [lblSectionAmenitiesCategory setText:content];
        [imgArrowSectionAmenitiesCategory setImage:[UIImage imageWithData:imgArrow]];
    }
    return  self;
}

//- (void)didReceiveMemoryWarning
//{
//    // Releases the view if it doesn't have a superview.
//    [super didReceiveMemoryWarning];
//    
//    // Release any cached data, images, etc that aren't in use.
//}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
}
*/

//- (void)viewDidUnload
//{
//    [super viewDidUnload];
//    // Release any retained subviews of the main view.
//    // e.g. self.myOutlet = nil;
//}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
