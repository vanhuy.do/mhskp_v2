//
//  CountHistoryController.h
//  mHouseKeeping
//
//  Created by Mac User on 28/03/2013.
//
//

#import <UIKit/UIKit.h>
#import "CountHistorySection.h"
#import "CountHistorySectionInfo.h"
#import "CountHistoryItemCell.h"

@interface CountHistoryController : UIViewController <UITableViewDataSource, UITableViewDelegate, CountHistorySectionDelegate>{
    
    NSMutableArray *sectionDatas;
    BOOL isFromMinibarHistory;
//    BOOL isFromLinenHistory;
}
@property (assign, nonatomic) NSInteger countServiceId;
@property (nonatomic, strong) NSMutableArray *sectionDatas;
@property (strong, nonatomic) IBOutlet UITableView *tbvCountHistory;
@property (nonatomic) BOOL isFromMinibarHistory;
@property (nonatomic) NSInteger openSectionIndex;
//@property (nonatomic) BOOL isFromLinenHistory;
@end
