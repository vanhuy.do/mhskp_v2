//
//  CountHistoryController.m
//  mHouseKeeping
//
//  Created by Mac User on 28/03/2013.
//
//

#import "CountHistoryController.h"
//#import "CountSectionInfoV2.h"
#import "CountHistorySectionInfo.h"
//#import "CountItemCellV2.h"
#import "CountHistoryItemCell.h"
#import "CountHistorySection.h"
#import "CountHistoryManager.h"
#import "CountHistoryModel.h"
#import <QuartzCore/QuartzCore.h>
#import "MyNavigationBarV2.h"
#import "LogFileManager.h"
#import "DeviceManager.h"

@interface CountHistoryController (PrivateMethods)
-(void) cartButtonPressed;

#define tagTopbarView 1234
#define tagAlertSubmit 1235
#define tagAlertNo 11
#define isDemoMode ([[NSUserDefaults standardUserDefaults] integerForKey:ENABLE_DEMO_MODE] == 1)
//#define font    @"Arial-BoldMT"
//
//#define sizeNoResult        22.0f

-(void) loadTopbarView;
-(void) showTopbarView;
-(void) hideTopbarView;
-(void) setHiddenTopbarView;
-(void) adjustShowForViews;
-(void) adjustRemoveForViews;
-(void) addButtonHandleShowHideTopbar;
-(void) topbarTapped:(UIButton *) sender;
-(TopbarViewV2 *) getTopBarView;

@end

@implementation CountHistoryController
@synthesize sectionDatas,tbvCountHistory;
@synthesize countServiceId;
@synthesize isFromMinibarHistory;
@synthesize openSectionIndex;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        if([LogFileManager isLogConsole])
        {
            NSLog(@"Count History Controller");
        }
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //Resize content views of current VC to fit with Navigation bar and tabbar
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    [tbvCountHistory setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [tbvCountHistory setBackgroundView:nil];
    [tbvCountHistory setBackgroundColor:[UIColor clearColor]];
    [tbvCountHistory setOpaque:YES];
    [self performSelector:@selector(loadTopbarView)];
    self.openSectionIndex = NSNotFound;

    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//----------update code-
#pragma mark - View lifecycle
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
//    [self setCaptionsView];
    MBProgressHUD	*HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getRaLoadingData]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES];
    [self performSelector:@selector(loadDataAfterDelayWithHUD:) withObject:HUD afterDelay:0.1];

//    [self loadingDatas];
    //show wifi view
    if(!isDemoMode){
        [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationShowWifiView] object:nil];
    }
     self.openSectionIndex = NSNotFound;
}

#pragma mark - Load Linen
-(void)loadingDatas {
//    PhysicalHistoryManager *manager = [[PhysicalHistoryManager alloc] init];
    CountHistoryManager *manager = [[CountHistoryManager alloc] init];
    
    sectionDatas = [[NSMutableArray alloc] init];
     NSInteger userId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    NSArray *listRoomId = [manager loadRoomIdByUserId:userId AndServiceId:countServiceId];
    
    for (CountHistoryModel *countModel in listRoomId) {
        
        CountHistorySectionInfo *sectionInfo = [[CountHistorySectionInfo alloc] init];
        sectionInfo.countRoomModel = countModel;
        sectionInfo.open = false;
        NSMutableArray *listCountHistory = [manager loadAllCountHistoryByServiceId:countServiceId AndUserId:userId AndRoomId:countModel.room_id];
        for (CountHistoryModel *model in listCountHistory) {
            [sectionInfo insertObjectToNextIndex:model];
        }
        
        [sectionDatas addObject:sectionInfo];
        
    }
    
    if (sectionDatas.count == 0) {
        UILabel *lblAlert  =  [[UILabel alloc]init];
        lblAlert.frame     =  CGRectMake(15, 150, 290, 80);
//        if (isFromMinibarHistory){
//            lblAlert.text      =  @"NO MINIBAR HISTORY DATA";
//        }
//        else{
//            lblAlert.text = @"NO LINEN HISTORY DATA";
//        }
        [lblAlert setText:[[LanguageManagerV2 sharedLanguageManager] getNoResultsFound]];
        lblAlert.textAlignment = NSTextAlignmentCenter;
        lblAlert.layer.cornerRadius = 10;
//         [lblCategoryName setFont:[UIFont fontWithName:@"Arial-BoldMT" size:15]];
        [lblAlert setFont:[UIFont fontWithName:@"Arial-BoldMT" size:22]];
        lblAlert.numberOfLines = 2;
        [lblAlert setTextColor:[UIColor grayColor]];
        lblAlert.font = [UIFont boldSystemFontOfSize:20.0f];
        [tbvCountHistory addSubview:lblAlert];
    }


}


-(void) loadDataAfterDelayWithHUD:(MBProgressHUD *) HUD {
    
//    PhysicalHistoryManager *manager = [[PhysicalHistoryManager alloc] init];
    CountHistoryManager *manager = [[CountHistoryManager alloc] init];
    [manager deleteDatabaseAfterSomeDays];
    [self loadingDatas];
//    if (sectionDatas.count == 0){
//        [self addLabelNoData];
//    }
    //    [self handleBtnAddToCartPressed]
    //hide saving data.
    [self performSelector:@selector(hiddenHUDAfterLoad:) withObject:HUD afterDelay:0.5];
}

#pragma didden after save.
-(void) hiddenHUDAfterLoad:(MBProgressHUD *)HUD{
    
    [HUD hide:YES];
    [HUD removeFromSuperview];
    [tbvCountHistory reloadData];
}


#pragma mark - UITableView Datasource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return sectionDatas.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    CountHistorySectionInfo *sectionInfo = [sectionDatas objectAtIndex:section];
    NSInteger numberRowInSection = sectionInfo.rowHeights.count;
    return sectionInfo.open ? numberRowInSection : 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifycell = @"CountHistoryItemCell";
    CountHistoryItemCell *cell = nil;
    
    cell = (CountHistoryItemCell *)[tableView dequeueReusableCellWithIdentifier:identifycell];
    if (cell == nil) {
        NSArray *array = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([CountHistoryItemCell class]) owner:self options:nil];
        cell = [array objectAtIndex:0];
        [cell setUserInteractionEnabled:NO];
    }
    
    //    cell.delegate = self;
    CountHistorySectionInfo *sectionInfo = [sectionDatas objectAtIndex:indexPath.section];
    CountHistoryModel *model = [sectionInfo.rowHeights objectAtIndex:indexPath.row];
    [cell.lblItemTitle setText:[NSString stringWithFormat:@"%@",model.count_item_name]];
    [cell.lblUsedNo setText:[NSString stringWithFormat:@"%d",(int)model.count_collected]];
    [cell.lblNewNo setText:[NSString stringWithFormat:@"%d",(int)model.count_new]];
    [cell.lblUnderlineLabel setBackgroundColor:[UIColor blackColor]];
    
    if (indexPath.row == 0) {
        
        [cell.lblUsed setHidden:NO];
        [cell.lblNew setHidden:NO];
        switch (countServiceId) {
            case cMinibar:
                [cell.lblUsed setText:[L_minibar_item_used currentKeyToLanguage]];
                [cell.lblNew setText:[L_minibar_item_new currentKeyToLanguage]];
                break;
            case cLinen:
                [cell.lblUsed setText:[L_linen_item_used currentKeyToLanguage]];
                [cell.lblNew setText:[L_linen_item_new currentKeyToLanguage]];
                break;
            case cAmenities:
                [cell.lblUsed setText:[L_amenity_item_used currentKeyToLanguage]];
                [cell.lblNew setText:[L_amenity_item_new currentKeyToLanguage]];
                break;
        }
    }
    else
    {
        [cell.lblUsed setHidden:YES];
        [cell.lblNew setHidden:YES];

    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 55;//70//the number of value must equal the value of cell, if not the index will overwite
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 60;//60
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    CountHistorySectionInfo *sectionInfo = [sectionDatas objectAtIndex:section];
    if (sectionInfo.headerView == nil) {
        
        CountHistorySection *headerView = [[CountHistorySection alloc] initWithSection:section CountHistoryModel:sectionInfo.countRoomModel andOpenStatus:NO];
        sectionInfo.headerView = headerView;
        headerView.delegate = self;
    }
    return sectionInfo.headerView;
}

#pragma mark - set Captions View
//-(void)setCaptionsView {
//    //    [btnAddToCart setTitle:[[LanguageManagerV2 sharedLanguageManager] getAddToCart] forState:UIControlStateNormal];
//    //    [self setTitle:[[LanguageManagerV2 sharedLanguageManager] getLinen]];
//    //
//    msgbtnNo = [[LanguageManagerV2 sharedLanguageManager] getNo];
//    msgbtnYes = [[LanguageManagerV2 sharedLanguageManager] getYes];
//    msgSaveCount = [[LanguageManagerV2 sharedLanguageManager] getMsgAlertSaveCounts];
//    //    msgDiscardCount = [[LanguageManagerV2 sharedLanguageManager] getMsgAlertDiscardCounts];
//    
//    //refresh topview
//    TopbarViewV2 *topview = [self getTopBarView];
//    [topview refresh:[[SuperRoomModelV2 alloc] init]];
//}

//update code
#pragma mark - Section Header Delegate
-(void)sectionHeaderView:(CountHistorySection *)sectionheaderView sectionOpened:(NSInteger)section {
    
    CountHistorySectionInfo *sectionInfo = [sectionDatas objectAtIndex:section];
    sectionInfo.open = YES;
    
    NSInteger countOfRowsToInsert = [sectionInfo.rowHeights count];
    NSMutableArray *indexPathsToInsert = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i < countOfRowsToInsert; i++) {
        [indexPathsToInsert addObject:[NSIndexPath indexPathForRow:i inSection:section]];
    }
    
    UITableViewRowAnimation insertAnimation;
    
    
    UITableViewRowAnimation deleteAnimation;
    
    
    NSMutableArray *indexPathsToDelete = [[NSMutableArray alloc] init];
    
    NSInteger previousOpenSectionIndex = self.openSectionIndex;
    
    if (openSectionIndex == -1) {
        
    }
    else {
        if (previousOpenSectionIndex != NSNotFound) {
            
            CountHistorySectionInfo *previousOpenSection = [self.sectionDatas objectAtIndex:previousOpenSectionIndex];
            previousOpenSection.open = NO;
            [previousOpenSection.headerView toggleOpenWithUserAction:NO];
            
            NSInteger countOfRowsToDelete =[previousOpenSection.rowHeights count];
            for (NSInteger i = 0; i < countOfRowsToDelete; i++) {
                [indexPathsToDelete addObject:[NSIndexPath indexPathForRow:i inSection:previousOpenSectionIndex]];
            }
            
        }
    }
    
    if (previousOpenSectionIndex == NSNotFound || section < previousOpenSectionIndex) {
        insertAnimation = UITableViewRowAnimationTop;
        deleteAnimation = UITableViewRowAnimationBottom;
    }
    else {
        insertAnimation = UITableViewRowAnimationBottom;
        deleteAnimation = UITableViewRowAnimationTop;
    }
    
    
    [self.tbvCountHistory beginUpdates];
    
    [self.tbvCountHistory insertRowsAtIndexPaths:indexPathsToInsert withRowAnimation:insertAnimation];
    [self.tbvCountHistory deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:deleteAnimation];
    [self.tbvCountHistory endUpdates];
    
    if ([indexPathsToInsert count] > 0) {
        [self.tbvCountHistory scrollToRowAtIndexPath:[indexPathsToInsert objectAtIndex:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    self.openSectionIndex = section;

}

-(void)sectionHeaderView:(CountHistorySection *)sectionheaderView sectionClosed:(NSInteger)section {
    CountHistorySectionInfo *sectionInfo = [sectionDatas objectAtIndex:section];
    //
    sectionInfo.open = NO;
    NSInteger countOfRowsToDelete = [self.tbvCountHistory numberOfRowsInSection:section];
    
    if (countOfRowsToDelete > 0) {
        NSMutableArray *indexPathsToDelete = [[NSMutableArray alloc] init];
        for (NSInteger i = 0; i < countOfRowsToDelete; i++) {
            [indexPathsToDelete addObject:[NSIndexPath indexPathForRow:i inSection:section]];
        }
        
        [tbvCountHistory beginUpdates];
        [self.tbvCountHistory deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:UITableViewRowAnimationAutomatic];
        [tbvCountHistory endUpdates];
    }

    self.openSectionIndex = NSNotFound;
}

//
#pragma mark - Handle Topbar Methods
-(void)loadTopbarView {
    TopbarViewV2 *topbar = [[TopbarViewV2 alloc] initViewWithSuperModel:[[CommonVariable sharedCommonVariable] superRoomModel]];
    [topbar setTag:tagTopbarView];
    [self.view addSubview:topbar];
    [topbar setHidden:YES];
    
    [topbar setAlpha:0.0];
    [self addButtonHandleShowHideTopbar];
}

-(void)showTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationWillStartSelector:@selector(setHiddenTopbarView)];
    
    f = topbar.frame;
    f.origin.y = 0;
    [topbar setFrame:f];
    [topbar setAlpha:1.0];
    
    [self adjustShowForViews];
    
    [UIView commitAnimations];
}

-(void)hideTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDidStopSelector:@selector(setHiddenTopbarView)];
    [UIView setAnimationDelegate:self];
    
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    [topbar setAlpha:0.0];
    
    [self adjustRemoveForViews];
    
    [UIView commitAnimations];
}

-(void)setHiddenTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    [topbar setHidden:!topbar.hidden];
}

-(TopbarViewV2 *)getTopBarView {
    return (TopbarViewV2 *)[self.view viewWithTag:tagTopbarView];
}

-(void)addButtonHandleShowHideTopbar {
    UIView *vBarButton = [[UIView alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
    UIButton *titleBarButtonFirst = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 200, 30)];
    
    titleBarButtonFirst.titleLabel.backgroundColor = [UIColor clearColor];
    [titleBarButtonFirst.titleLabel setFont: FONT_BUTTON_TOPBAR];
    titleBarButtonFirst.titleLabel.textAlignment = NSTextAlignmentCenter;
    titleBarButtonFirst.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
    
    [titleBarButtonFirst.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
    [titleBarButtonFirst.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
    [titleBarButtonFirst addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    [titleBarButtonFirst setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_HISTORY] forState:UIControlStateNormal];
    
    [vBarButton addSubview:titleBarButtonFirst];
    
    
    UIButton *titleBarButtonSecond = [[UIButton alloc] initWithFrame:CGRectMake(0, 28, 200, 22)];
    [titleBarButtonSecond setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [titleBarButtonSecond.titleLabel setFont:FONT_BUTTON_TOPBAR_SECOND];
    
    if(countServiceId == cAmenities) {
        [titleBarButtonSecond setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_AMENITIES] forState:UIControlStateNormal];
    } else if (isFromMinibarHistory){
        [titleBarButtonSecond setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_MINIBAR] forState:UIControlStateNormal];
    }
    else {
        [titleBarButtonSecond setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_Linen_Title] forState:UIControlStateNormal];
    }
    [titleBarButtonSecond addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    [vBarButton addSubview:titleBarButtonSecond];
    
    self.navigationItem.titleView = vBarButton;
    
    //Move title to center vertical
    CGRect titleViewBounds = self.navigationItem.titleView.bounds;
    int moveLeftTitle = 0;
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        moveLeftTitle = 10;
    }
    self.navigationItem.titleView.bounds = CGRectMake(titleViewBounds.origin.x + moveLeftTitle, Y_TOPBAR_TITLE, titleViewBounds.size.width, titleViewBounds.size.height);
    
    //back button navigation
    self.navigationItem.leftBarButtonItem = [MyNavigationBarV2 createBackButtonWithTarget:self selector:@selector(btnBackPressed) forControlEvents:UIControlEventTouchUpInside];
    
}

-(void)btnBackPressed
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)topbarTapped:(UIButton *)sender {
    TopbarViewV2 *topbar = [self getTopBarView];
    [sender setEnabled:NO];
    if (topbar.isHidden) {
        [self showTopbarView];
    } else {
        [self hideTopbarView];
    }
    [self performSelector:@selector(topbarButtonEnable:) withObject:sender afterDelay:0.5];
}

-(void) topbarButtonEnable:(UIButton *) topbarBtn {
    [topbarBtn setEnabled:YES];
}

-(void)adjustShowForViews {
    //adjust all views when show
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    CGRect fRoomView = tbvCountHistory.frame;
    [tbvCountHistory setFrame:CGRectMake(0, f.size.height, 320, fRoomView.size.height)];
    
    //    CGRect ftbv = tbvLinen.frame;
    //    [tbvLinen setFrame:CGRectMake(0, f.size.height + fRoomView.size.height, 320, ftbv.size.height - f.size.height)];
    
}

-(void)adjustRemoveForViews {
    //adjust all views when hide
    //    TopbarViewV2 *topbar = [self getTopBarView];
    //    CGRect f = topbar.frame;
    
    CGRect fRoomView = tbvCountHistory.frame;
    [tbvCountHistory setFrame:CGRectMake(0, 0, 320, fRoomView.size.height)];
    
    //    CGRect ftbv = tbvLinen.frame;
    //    [tbvLinen setFrame:CGRectMake(0, fRoomView.size.height, 320, ftbv.size.height + f.size.height)];
    
}

@end
