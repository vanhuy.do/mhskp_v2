//
//  CountHistoryItemCell.h
//  mHouseKeeping
//
//  Created by Mac User on 28/03/2013.
//
//

#import <UIKit/UIKit.h>

@interface CountHistoryItemCell : UITableViewCell

//@property (strong, nonatomic) IBOutlet UIImageView *imageItem;
@property (strong, nonatomic) IBOutlet UILabel *lblItemTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblUsed;
@property (strong, nonatomic) IBOutlet UILabel *lblNew;
@property (strong, nonatomic) IBOutlet UILabel *lblUsedNo;
@property (strong, nonatomic) IBOutlet UILabel *lblNewNo;
@property (strong, nonatomic) IBOutlet UILabel *lblUnderlineLabel;
@end
