//
//  CountHistoryItemCell.m
//  mHouseKeeping
//
//  Created by Mac User on 28/03/2013.
//
//

#import "CountHistoryItemCell.h"

@implementation CountHistoryItemCell
//@synthesize imageItem;
@synthesize lblItemTitle;
@synthesize lblNew;
@synthesize lblNewNo;
@synthesize lblUsed;
@synthesize lblUsedNo;
@synthesize lblUnderlineLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
