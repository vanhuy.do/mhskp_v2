//
//  CountHistorySection.h
//  mHouseKeeping
//
//  Created by Mac User on 28/03/2013.
//
//

#import <UIKit/UIKit.h>
#import "CountOrdersModelV2.h"
#import "CountHistorySection.h"
#import "RoomModelV2.h"
#import "CountHistoryModel.h"
@class CountHistorySection;

@protocol CountHistorySectionDelegate <NSObject>

@optional
-(void) sectionHeaderView:(CountHistorySection *) sectionheaderView sectionOpened:(NSInteger) section;
-(void) sectionHeaderView:(CountHistorySection *) sectionheaderView sectionClosed:(NSInteger) section;

@end

@interface CountHistorySection : UIView{
    __unsafe_unretained id<CountHistorySectionDelegate> delegate;
    NSInteger section;
    BOOL isToggle;

}
@property (nonatomic, assign) NSInteger section;
@property (nonatomic, assign) id<CountHistorySectionDelegate> delegate;
@property (strong, nonatomic) IBOutlet UIImageView *imgPhysicalCHSection;
@property (strong, nonatomic) IBOutlet UILabel *lblCountHistoryTitle;
@property (strong, nonatomic) IBOutlet UIImageView *imgDetail;

-(void) toggleOpenWithUserAction:(BOOL) userAction;
//- (IBAction)btnPhisicalCHSectionPressed:(id)sender;
-(id) initWithSection:(NSInteger) section CountHistoryModel:(CountHistoryModel *) roomModel andOpenStatus:(BOOL) isOpen;
@end
