//
//  MinibarHistorySection.m
//  mHouseKeeping
//
//  Created by Mac User on 28/03/2013.
//
//

#import "CountHistorySection.h"
#import "ehkDefinesV2.h"
#import "UIImage+Tint.h"

@implementation CountHistorySection

@synthesize delegate;
@synthesize section;
@synthesize imgDetail;
@synthesize imgPhysicalCHSection;
@synthesize lblCountHistoryTitle;

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void) initControls {
    [self setFrame:CGRectMake(0, 0, 320, 60)];
    
    UIImageView *bg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 60)];
    [bg setImage:[UIImage imageBeforeiOS7:imgBgBigBtn equaliOS7:imgBgBigBtnFlat]];
    [self addSubview:bg];
    
    imgPhysicalCHSection = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 70, 60)];
    [imgPhysicalCHSection setContentMode:UIViewContentModeScaleAspectFit];
    [self addSubview:imgPhysicalCHSection];
    
    lblCountHistoryTitle = [[UILabel alloc] initWithFrame:CGRectMake(70, 5, 220, 50)];
    [lblCountHistoryTitle setFont:[UIFont fontWithName:@"Arial-BoldMT" size:17]];
    [lblCountHistoryTitle setBackgroundColor:[UIColor clearColor]];
    [lblCountHistoryTitle setTextColor:[UIColor colorWithRed:55.0/255.0 green:55.0/255.0 blue:55.0/255.0 alpha:1.0]];
    [lblCountHistoryTitle setTextAlignment:NSTextAlignmentCenter];
    [self addSubview:lblCountHistoryTitle];
    
    imgDetail = [[UIImageView alloc] initWithFrame:CGRectMake(285, 15, 30, 30)];
    [imgDetail setImage:[UIImage imageBeforeiOS7:imgRightRow equaliOS7:imgRightRowFlat]];
    [self addSubview:imgDetail];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(0, 0, 320, 70)];
    [button addTarget:self action:@selector(btnPhysicalCHPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:button];
}

-(id)init {
    self = [super init];
    if (self) {
        [self initControls];
    }
    return self;
}

-(id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initControls];
    }
    return self;
}

- (void)dealloc {
    //    [imgLinenSection release];
    //    [lblLinenTitle release];
    //    [imgDetail release];
    //    [super dealloc];
}

-(void) toggleOpenWithUserAction:(BOOL) userAction {
    if (userAction) {
        if (isToggle == NO) {
            if ([delegate respondsToSelector:@selector(sectionHeaderView:sectionOpened:)]) {
                isToggle = YES;
                [delegate sectionHeaderView:self sectionOpened:section];
                
                [imgDetail setImage:[UIImage imageBeforeiOS7:imgRightRowOpen equaliOS7:imgRightRowOpenFlat]];
            }
        }
        else {
            if ([delegate respondsToSelector:@selector(sectionHeaderView:sectionClosed:)]) {
                isToggle = NO;
                [imgDetail setImage:[UIImage imageBeforeiOS7:imgRightRow equaliOS7:imgRightRowFlat]];
                [delegate sectionHeaderView:self sectionClosed:section];
            }
        }
    }
    else
    {
        if (isToggle == NO) {
            isToggle = YES;
            [imgDetail setImage:[UIImage imageBeforeiOS7:imgRightRowOpen equaliOS7:imgRightRowOpenFlat]];
        }
        else {
            isToggle = NO;
            [imgDetail setImage:[UIImage imageBeforeiOS7:imgRightRow equaliOS7:imgRightRowFlat]];
        }
    }
}

- (IBAction)btnPhysicalCHPressed:(id)sender {
    [self toggleOpenWithUserAction:YES];
}

-(id) initWithSection:(NSInteger) sectionIndex CountHistoryModel:(CountHistoryModel *) model andOpenStatus:(BOOL) isOpen {
    self = [self init];
    
    if (self) {
        section = sectionIndex;
        lblCountHistoryTitle.text = model.room_id;
        isToggle = isOpen;
        //        [imgLinenSection setImage:[UIImage imageWithData:categoryModel.linenCategoryImage]];
        if (isOpen) {
            [imgDetail setImage:[UIImage imageBeforeiOS7:imgRightRowOpen equaliOS7:imgRightRowOpenFlat]];
        } else {
            [imgDetail setImage:[UIImage imageBeforeiOS7:imgRightRow equaliOS7:imgRightRowFlat]];
        }
    }
    
    return self;
}


@end
