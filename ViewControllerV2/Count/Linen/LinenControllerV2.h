//
//  LinenControllerV2.h
//  mHouseKeeping
//
//  Created by TMS on 2/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ZXingWidgetController.h>
#import "LinenSectionViewV2.h"
#import "LinenSectionInfoV2.h"
#import "LinenItemModelV2.h"
#import "LinenItemCellV2.h"
#import "PickerViewV2.h"
#import "MiniBarSectionInfoV2.h"
#import "MiniBarItemModelV2.h"
#import "MiniBarLinenCartViewV2.h"
#import "MBProgressHUD.h"
#import "AccessRight.h"
#import "CountOrdersModelV2.h"

@interface LinenControllerV2 : UIViewController <UITableViewDelegate, UITableViewDataSource, LinenSectionViewV2Delegate, ItemCellV2Delegate, PickerViewV2Delegate, MiniBarLinenCartViewV2Delegate, UITextFieldDelegate, UIScrollViewDelegate, ZXingDelegate, MBProgressHUDDelegate> {
    NSMutableArray *sectionDatas;
    UIButton *cartButton;
    NSMutableArray *datasLinenCart;
    UITextField *txtRoom;
    UIButton *btnQR;
    BOOL isFromMainPosting;
    UIView *viewRoom;
    UIButton *btnSubmit;
    
    NSInteger logCheckNewValue;
    NSInteger logCheckUsedValue;
//    IBOutlet UIButton *btnSubmit;
    IBOutlet UILabel *lblSubmit;
    IBOutlet UILabel *lblRoomNo;
    IBOutlet UIScrollView *scrollView;
    AccessRight *QRCodeScanner;
    AccessRight *postingLinen;
    AccessRight *actionLinen;
    
    CountOrdersModelV2 *countOrder;
    BOOL haveOldCountOrders;
    
    IBOutlet UIImageView *imgHeader;
    __weak IBOutlet UIButton *btnPostingHistory;
}

@property (nonatomic, strong) NSMutableArray *datasLinenCart;
@property (nonatomic, strong) UIButton *cartButton;
@property (nonatomic, strong) NSMutableArray *sectionDatas;
@property (strong, nonatomic) IBOutlet UITableView *tbvLinen;
@property (strong, nonatomic) IBOutlet UIButton *btnAddToCart;
@property (assign, nonatomic) NSInteger countServiceId;
@property (assign, nonatomic) BOOL statusChooseLinen;
@property (assign, nonatomic) NSInteger sumLinenUsed;
@property (assign, nonatomic) BOOL statusSave;
@property (strong, nonatomic) NSString *msgbtnYes;
@property (strong, nonatomic) NSString *msgbtnNo;
@property (strong, nonatomic) NSString *msgSaveCount;
@property (strong, nonatomic) NSString *msgDiscardCount;
@property (assign, nonatomic) BOOL statusCheckSaveData;
@property (assign, nonatomic) BOOL statusAddToCart;
@property (strong, nonatomic) IBOutlet UITextField *txtRoom;
@property (strong, nonatomic) IBOutlet UIButton *btnQR;
@property (nonatomic) BOOL isFromMainPosting;
@property (strong, nonatomic) IBOutlet UIView *viewRoom;
@property (strong, nonatomic) IBOutlet UIButton *btnSubmit;
@property (assign, nonatomic) NSInteger roomTypeId;

-(IBAction)btnAddToCartPressed:(id)sender;
-(IBAction) btnQRCodeScannerTouched:(id) sender;
-(void) handleBtnAddToCartPressed;
- (IBAction)btnPostingHistory_Click:(UIButton *)sender;
-(void) loadingDatas;
-(void) setCaptionsView;
-(IBAction) btnSubmit_Clicked:(id)sender;
//-(IBAction) checkTextField;
@end
