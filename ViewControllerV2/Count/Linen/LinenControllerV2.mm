//
//  LinenControllerV2.m
//  mHouseKeeping
//
//  Created by TMS on 2/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
#import <UIKit/UIEvent.h>
#import "LinenControllerV2.h"
#import "CountManagerV2.h"
#import "MBProgressHUD.h"
#import "UserManagerV2.h"
#import "RoomManagerV2.h"
#import "CustomAlertViewV2.h"
#import "TasksManagerV2.h"
#import "RoomAssignmentInfoViewController.h"
#import "CountHistoryModel.h"
#import "CountHistoryManager.h"
#import "MyNavigationBarV2.h"
#import "QRCodeReader.h"
#import "LogFileManager.h"
#import "ehkDefines.h"
#import "DeviceManager.h"
#import "iToast.h"
#import "NetworkCheck.h"
#import "HomeViewV2.h"
#import "LanguageManagerV2.h"
#import "DTAlertView.h"
#import "STEncryptorDES.h"
#import "UIImage+Tint.h"
#import "ehkLanguageDefines.h"
#import "NSString+Common.h"
#import "HistoryPosting.h"

#define tagAlertBack 10
#define tagAlertNo 11
#define tagTopbarView 1234
#define tagAlertSubmit 1235
#define heightLinenCell 70
#define heightLinenHeader 60
#define tagNoNetwork 61
#define tagUnpostData 62
#define tagHudPostAllOrders 63

typedef enum {
    tUsed = 1,
    tNew,
}tType;

@interface LinenControllerV2(PrivateMethods) <DTAlertViewDelegate>

-(void) cartButtonPressed;


-(void) loadTopbarView;
-(void) showTopbarView;
-(void) hideTopbarView;
-(void) setHiddenTopbarView;
-(void) adjustShowForViews;
-(void) adjustRemoveForViews;
-(void) addButtonHandleShowHideTopbar;
-(void) topbarTapped:(UIButton *) sender;
-(TopbarViewV2 *) getTopBarView;

@end

@implementation LinenControllerV2
@synthesize tbvLinen;
@synthesize btnAddToCart;
@synthesize sectionDatas;
@synthesize cartButton;
@synthesize datasLinenCart;
@synthesize countServiceId;
@synthesize statusChooseLinen;
@synthesize sumLinenUsed;
@synthesize statusSave;
@synthesize msgbtnNo;
@synthesize msgbtnYes;
@synthesize msgSaveCount;
@synthesize msgDiscardCount;
@synthesize statusCheckSaveData;
@synthesize statusAddToCart;
@synthesize txtRoom;
@synthesize btnQR;
@synthesize isFromMainPosting;
@synthesize viewRoom;
@synthesize btnSubmit;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        sectionDatas = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - Load Linen From Service
-(void) loadDataAfterDelayWithHUD:(MBProgressHUD *) HUD {
    //enable flag to announce, is loading data
    [[HomeViewV2 shareHomeView] waitingLoadingData];
    
    [self loadingDatas];
    
    [self handleBtnAddToCartPressed];
    
    //hide saving data.
    [self performSelector:@selector(hiddenHUDAfterSaved:) withObject:HUD afterDelay:0.5];
    
    //end loading data
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
}
-(void) loadDataLinenFormService:(MBProgressHUD *) HUD {
    //enable flag to announce, is loading data
    [[HomeViewV2 shareHomeView] waitingLoadingData];
    
    CountManagerV2 *countManager = [[CountManagerV2 alloc] init];
    //Delete all temporate order data
    [countManager deleteCountOrdersByPostStatus:(int)POST_STATUS_UN_CHANGED serviceId:(int)countServiceId];
    
    int countUnpost = [countManager countUnpostOrderItemsByUserId:[UserManagerV2 sharedUserManager].currentUser.userId serviceId:(int)countServiceId];
    if(countUnpost > 0) {
        haveOldCountOrders = YES;
        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:@"" message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_notify_unpost_data] delegate:self cancelButtonTitle:msgbtnYes otherButtonTitles:msgbtnNo, nil];
        alert.delegate = self;
        [alert setTag:tagUnpostData];
        [alert show];
        
    } else {
        haveOldCountOrders = NO;
        SyncManagerV2 *synManager = [[SyncManagerV2 alloc] init];
        
        NSInteger userId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
        NSInteger hotelId = [[[UserManagerV2 sharedUserManager] currentUser] userHotelsId];
        
        //-----------WS------------
        [synManager getLinenCategoryListWithUserId:userId AndHotelId:hotelId];
        [synManager getLinenItemListWithUserId:userId AndHotelId:hotelId];
        if ([[UserManagerV2 sharedUserManager].currentUserAccessRight isLoadLinenByRoomType]) {
            [synManager getRoomTypeInventoryWSByServiceType:ServiceType_Linen userId:userId hotelId:hotelId];
        }
        //-----------end-----------
        
        [self loadingDatas];
    }
    
    //Hao Tran - Remove for speed up showing data
    //[self handleBtnAddToCartPressed];
    //Hao Tran - END
    
    //hide saving data.
    [self performSelector:@selector(hiddenHUDAfterSaved:) withObject:HUD afterDelay:0.0f];
    
    //end loading data
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
}

-(void)loadViewData {
    
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getRaLoadingData]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES];
    
    //enable flag to announce, is loading data
    [[HomeViewV2 shareHomeView] waitingLoadingData];
    
    SyncManagerV2 *synManager = [[SyncManagerV2 alloc] init];
    NSInteger userId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    NSInteger hotelId = [[[UserManagerV2 sharedUserManager] currentUser] userHotelsId];
    
    //-----------WS------------
    [synManager getLinenCategoryListWithUserId:userId AndHotelId:hotelId];
    [synManager getLinenItemListWithUserId:userId AndHotelId:hotelId];
    if ([[UserManagerV2 sharedUserManager].currentUserAccessRight isLoadLinenByRoomType]) {
        [synManager getRoomTypeInventoryWSByServiceType:ServiceType_Linen userId:userId hotelId:hotelId];
    }
    //-----------end-----------
    
    [self loadingDatas];
    
    [self performSelector:@selector(hiddenHUDAfterSaved:) withObject:HUD afterDelay:0.0f];
    
    //end loading data
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
}

-(void)postAllOldCountOrderData
{
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    HUD.delegate = self;
    HUD.tag = tagHudPostAllOrders;
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getRaLoadingData]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES];
    
    //enable flag to announce, is loading data
    [[HomeViewV2 shareHomeView] waitingLoadingData];
    
    CountManagerV2 *countManager = [[CountManagerV2 alloc] init];
    [countManager postAllPendingCountDataByUserId:[UserManagerV2 sharedUserManager].currentUser.userId serviceId:(int)countServiceId];
    
    //hide saving data.
    [self performSelector:@selector(hiddenHUDAfterSaved:) withObject:HUD afterDelay:0.0f];
    
    //end loading data
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
}

-(void)hudWasHidden:(MBProgressHUD *)hud
{
    if(hud.tag == tagHudPostAllOrders)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - View lifecycle

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    CGRect fScrollView = scrollView.frame;
    [scrollView setFrame:CGRectMake(0, viewRoom.frame.size.height, fScrollView.size.width, fScrollView.size.height)];
}

- (void) reachabilityDidChanged: (NSNotification* )note
{
    if(![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]){
        [btnSubmit setEnabled:NO];
        [self setEnableBtnAtionHistory:NO];
    }
    else{
        [btnSubmit setEnabled:YES];
        [self setEnableBtnAtionHistory:YES];
    }
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if(!isDemoMode){
        //show wifi view
        [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationShowWifiView] object:nil];
        if(![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]){
            [btnSubmit setEnabled:NO];
        }
        else{
            [btnSubmit setEnabled:YES];
        }
    }
    if(isFromMainPosting){
        if(!isDemoMode){
            [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(reachabilityDidChanged:) name: kReachabilityChangedNotification object: nil];
        }
        [[HomeViewV2 shareHomeView] setSelectedButton:tagOfPostingButton];
    } else{
        [[HomeViewV2 shareHomeView] setSelectedButton:tagOfHomeButton];
    }
    [self setCaptionsView];
    
    //Hao Tran Remove because view update unpectedly
    //    MBProgressHUD	*HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    //    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getRaLoadingData]];
    //    [self.tabBarController.view addSubview:HUD];
    //    [HUD show:YES];
    //    [self performSelector:@selector(loadDataAfterDelayWithHUD:) withObject:HUD afterDelay:0.1];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    if(!isDemoMode){
        [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
    }
}
- (void)enableQRCode:(BOOL)isEnable{
    [btnQR setAlpha:isEnable ? 1.0f : 0.7f];
    [btnQR setEnabled:isEnable];
}
//CRF1619
- (void)loadAccessQRCode{
    if ([UserManagerV2 sharedUserManager].currentUserAccessRight.isPostingFunctionLinenQRCode == 1) {
        self.txtRoom.enabled = NO;
        [self enableQRCode:YES];
    }else if ([UserManagerV2 sharedUserManager].currentUserAccessRight.isPostingFunctionLinenQRCode == 0) {
        self.txtRoom.enabled = YES;
        [self enableQRCode:NO];
    }
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self loadAccessRights];
    if(!ENABLE_QRCODESCANNER)
    {
        if(ENABLE_CHECK_ACCESS_RIGHT_QRCODESCANNER)
        {
            [btnQR setHidden:NO];
            [self enableQRCode:NO];
            
            if (QRCodeScanner.isActive && !isDemoMode) {
                [self enableQRCode:YES];
            }
        }
        else
        {
            [btnQR setHidden:YES];
        }
    }
    
    [lblRoomNo setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOM_NO]];
    
    //Set Enable button submit
    [btnSubmit setEnabled:![self isOnlyViewedAccessRight]];
    NSInteger userId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
    if([LogFileManager isLogConsole])
    {
        NSLog(@"<%@><Linen><UserId:%d><viewDidLoad>", time, (int)userId);
    }
    
    scrollView.delegate = self;
    [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, scrollView.frame.size.height)];
    [btnSubmit setFrame:CGRectMake(10, 15 + tbvLinen.frame.size.height, 300, 64)];
    [btnPostingHistory setFrame:CGRectMake(10, 25 + tbvLinen.frame.size.height + CGRectGetHeight(btnSubmit.bounds), 300, 55)];
    [lblSubmit setFrame:CGRectMake(123, 25 + tbvLinen.frame.size.height, 135, 35)];
    
    //Resize content views of current VC to fit with Navigation bar and tabbar
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        [self loadFlatResource];
    }
    
    MBProgressHUD	*HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getRaLoadingData]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES];
    
    [self performSelector:@selector(loadDataLinenFormService:) withObject:HUD afterDelay:0.1];
    
    [tbvLinen setBackgroundView:nil];
    [tbvLinen setBackgroundColor:[UIColor clearColor]];
    [tbvLinen setOpaque:YES];
    statusSave = NO;
    
    //    cartButton = [[UIButton alloc]initWithFrame:CGRectMake(236, 6, 60, 32)];
    //    [cartButton setBackgroundImage:[UIImage imageNamed:imgTopCart] forState:UIControlStateNormal];
    //    [cartButton setTitle:[NSString stringWithFormat:@"%d", sumLinenUsed] forState:UIControlStateNormal];
    //    [cartButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 32, 0, 0)];
    //    [cartButton.titleLabel setFont:[UIFont fontWithName:@"Arial-BoldMT" size:15]];
    //    [cartButton addTarget:self action:@selector(cartButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    //    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithCustomView:cartButton];
    //    self.navigationItem.rightBarButtonItem = rightButton;
    
    //back button navigation
    self.navigationItem.leftBarButtonItem = [MyNavigationBarV2 createBackButtonWithTarget:self selector:@selector(backBarPressed) forControlEvents:UIControlEventTouchUpInside];
    
    //[self performSelector:@selector(loadTopbarView)];
    if(isFromMainPosting)
    {
        [txtRoom setText:@""];
        _roomTypeId = 0;
        [txtRoom setEnabled:YES];
        [txtRoom setTextColor:[UIColor blackColor]];
        [self loadAccessQRCode];
        btnPostingHistory.hidden = ![UserManagerV2 sharedUserManager].currentUserAccessRight.isShowActionHistoryPosting;
    }
    else
    {
        int roomAssignment = (int)[TasksManagerV2 getCurrentRoomAssignment];
        RoomAssignmentModelV2 *roomAssignmentModel = [[RoomAssignmentModelV2 alloc] init];
        roomAssignmentModel.roomAssignment_Id = roomAssignment;
        [[RoomManagerV2 sharedRoomManager]getRoomIdByRoomAssignment:roomAssignmentModel];
        [txtRoom setText:roomAssignmentModel.roomAssignment_RoomId];
        //        [txtRoom setEnabled:NO];
        [txtRoom setTextColor:[UIColor grayColor]];
    }
    
    [self loadTopbarView];
    [self setEnableBtnAtionHistory:NO];
    
}
#pragma mark - Click Back
-(void)backBarPressed {
    //clear previous tag of event
//    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:TAG_OF_EVENT];
    
//    if(statusChooseLinen == NO)
//    {
        [self.navigationController popViewControllerAnimated:YES];
//    }
//    else
//    {
//        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:msgSaveCount delegate:self cancelButtonTitle:msgbtnYes otherButtonTitles:msgbtnNo, nil];
//        alert.delegate = self;
//        alert.tag = tagAlertBack;
//        [alert show];
//    }
}

#pragma mark - === Handle Check Save Data ===
#pragma mark
-(BOOL) checkSaveDataWithTagEvent:(NSInteger) tagOfEvent {
    
    if (tagOfEvent == tagOfMessageButton) {
        if (statusChooseLinen == NO) {
            
            return NO;
            
        }
        else
        {
            //Show alert please save before leaving
            UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:msgSaveCount delegate:self cancelButtonTitle:msgbtnYes otherButtonTitles:msgbtnNo, nil];
            alert.delegate = self;
            alert.tag = tagAlertBack;
            [alert show];
            
            return YES;
            
        }
    }
    else
    {
        //Show alert complete room
        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_complete_room_to_leave] delegate:self cancelButtonTitle:msgbtnYes otherButtonTitles: nil];
        alert.delegate = self;
        [alert show];
        
        return YES;
    }
    
}

#pragma mark - UIAlertView Delegate Methods
-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    switch (alertView.tag) {
        case tagUnpostData:
        {
            if(buttonIndex == 0) {
                //YES:
                [self loadViewData];
                
            } else {
                //NO: continue posting
                [self postAllOldCountOrderData];
            }
        }
            break;
        case tagNoNetwork:
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
            break;
        case tagAlertBack:
        {
            switch (buttonIndex)
            {
                case 0:
                {
                    //YES: save minibar
                    [self saveLinenCart];
                    [self btnAddToCartPressed:btnAddToCart];
                    statusChooseLinen = NO;
                    
                    //if tag is message
                    if ([[NSUserDefaults standardUserDefaults] integerForKey:TAG_OF_EVENT] == tagOfMessageButton) {
                        [[HomeViewV2 shareHomeView] handleTabbarEventWithTagOfButton:[[NSUserDefaults standardUserDefaults] integerForKey:TAG_OF_EVENT]];
                    }
                    else{
                        //back to count menu
                        [self.navigationController popViewControllerAnimated:YES];
                    }
                }
                    break;
                    
                case 1:
                {
                    //NO: show popup discard
                    UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:msgDiscardCount delegate:self cancelButtonTitle:msgbtnYes otherButtonTitles:msgbtnNo, nil];
                    alert.delegate = self;
                    alert.tag = tagAlertNo;
                    [alert show];
                    
                }
                    break;
                    
                default:
                    break;
            }
        }
            break;
            
        case tagAlertNo:
        {
            switch (buttonIndex)
            {
                case 0:
                {
                    //YES
                    [datasLinenCart removeAllObjects];
                    statusChooseLinen = NO;
                    //if tag is message
                    if ([[NSUserDefaults standardUserDefaults] integerForKey:TAG_OF_EVENT] == tagOfMessageButton) {
                        [[HomeViewV2 shareHomeView] handleTabbarEventWithTagOfButton:[[NSUserDefaults standardUserDefaults] integerForKey:TAG_OF_EVENT]];
                    }
                    else{
                        //back to count menu, no save
                        [self.navigationController popViewControllerAnimated:YES];
                    }
                    
                }
                    break;
                    
                case 1:
                {
                    //NO: cart screen
                    //[self btnAddToCartPressed:nil];
                    //[self cartButtonPressed];
                    
                }
                    break;
                    
                default:
                    break;
            }
            
        }
            break;
            
        case tagAlertSubmit:
        {
            switch (buttonIndex)
            {
                case 0:
                {
                    //YES
                    //-------------Log----------
                    if([LogFileManager isLogConsole]) {
                        NSInteger userId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
                        NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
                        if (logCheckNewValue == 0 && logCheckUsedValue == 0) {
                            if([LogFileManager isLogConsole])
                            {
                                NSLog(@"<%@><Linen><UserId:%d><Summit to post><input RoomId:%@>", time, (int)userId, txtRoom.text);
                            }
                        }
                        else {
                            NSInteger totalSections = [self numberOfSectionsInTableView:tbvLinen];
                            
                            for (int i = 0; i<totalSections; i++) {
                                NSInteger totalRow = [self tableView:self.tbvLinen numberOfRowsInSection:i];
                                for (int j = 0; j<totalRow; j++) {
                                    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:j inSection:i];
                                    UITableViewCell *cell = [self.tbvLinen cellForRowAtIndexPath:indexPath];
                                    UILabel *lblItemName = (UILabel *)[cell viewWithTag:tagItemName];
                                    UIButton *btnUsed = (UIButton *)[cell viewWithTag:tagUsedButton];
                                    
                                    UIButton *btnNew = (UIButton *)[cell viewWithTag:tagNewButton];
                                    NSString *valueBtnUsed = btnUsed.titleLabel.text;
                                    NSString *valueBtnNew = btnNew.titleLabel.text;
                                    if ([valueBtnNew isEqualToString:@"0"] && [valueBtnUsed isEqualToString:@"0"]) {
                                        
                                    }
                                    else {
                                        if([LogFileManager isLogConsole])
                                        {
                                            NSLog(@"<%@><Minibar><UserId:%d><Submit to post><Input RoomId:%@><ItemName:%@><ItemUsed:%@><ItemNew:%@>", time, (int)userId, txtRoom.text, lblItemName.text, valueBtnUsed, valueBtnNew);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //-------------Log----------
                    statusSave = YES;
                    statusChooseLinen = YES;
                    statusAddToCart = YES;
                    
                    [self handleBtnAddToCartPressed];
                    int responseCode = [self saveData];
                    
                    if(isFromMainPosting)
                    {
                        [txtRoom setText:@""];
                        [tbvLinen reloadData];
                        [datasLinenCart removeAllObjects];
                        
                        //[self.navigationController popViewControllerAnimated:YES];
                    }
                    else
                    {
                        //response status different to RESPONSE_STATUS_OK will show alert view in saveMinibarCart
                        if(responseCode == RESPONSE_STATUS_OK) {
                            NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
                            for (UIViewController *aViewController in allViewControllers) {
                                if ([aViewController isKindOfClass:[RoomAssignmentInfoViewController class]]) {
                                    ((RoomAssignmentInfoViewController*) aViewController).isAlreadyPostedLinen = YES;
                                    [self.navigationController popToViewController:aViewController animated:YES];
                                }
                            }
                        }
                    }
                }
                    break;
                    
                case 1:
                {
                    if([LogFileManager isLogConsole])
                    {
                        //NO: cart screen
                        //[self btnAddToCartPressed:nil];
                        //[self cartButtonPressed];
                        NSInteger userId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
                        NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
                        if (logCheckNewValue == 0 && logCheckUsedValue == 0) {
                            if([LogFileManager isLogConsole]){
                                NSLog(@"<%@><Linen><UserId:%d><Not confirm to post><input RoomId:%@>", time, (int)userId, txtRoom.text);
                            }
                        }
                        else {
                            NSInteger totalSections = [self numberOfSectionsInTableView:tbvLinen];
                            
                            for (int i = 0; i<totalSections; i++) {
                                NSInteger totalRow = [self tableView:self.tbvLinen numberOfRowsInSection:i];
                                for (int j = 0; j<totalRow; j++) {
                                    
                                    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:j inSection:i];
                                    UITableViewCell *cell = [self.tbvLinen cellForRowAtIndexPath:indexPath];
                                    
                                    UILabel *lblItemName = (UILabel *)[cell viewWithTag:tagItemName];
                                    UIButton *btnUsed = (UIButton *)[cell viewWithTag:tagUsedButton];
                                    UIButton *btnNew = (UIButton *)[cell viewWithTag:tagNewButton];
                                    
                                    NSString *valueBtnUsed = btnUsed.titleLabel.text;
                                    NSString *valueBtnNew = btnNew.titleLabel.text;
                                    if ([valueBtnNew isEqualToString:@"0"] && [valueBtnUsed isEqualToString:@"0"]) {
                                        
                                    }
                                    else {
                                        NSLog(@"<%@><Linen><UserId:%d><Not confirm to post><Input RoomId:%@><ItemName:%@><ItemUsed:%@><ItemNew:%@>", time, (int)userId, txtRoom.text, lblItemName.text, valueBtnUsed, valueBtnNew);
                                    }
                                }
                            }
                        }
                    }
                    
                }
                    break;
                    
                default:
                    break;
            }
            
        }
            break;
            
        default:
            break;
    }
}


- (void)viewDidUnload
{
    [self setTbvLinen:nil];
    [self setBtnAddToCart:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc {
    //    [tbvLinen release];
    //    [btnAddToCart release];
    //    [super dealloc];
}

#pragma mark - Convert LinenItem to MiniBarItem
-(MiniBarItemModelV2*) convertMiniBarItemFromLinenItem:(LinenItemModelV2*)linenModel
{
    MiniBarItemModelV2 *copyModel = [[MiniBarItemModelV2 alloc] init];
    
    copyModel.minibarItemID = linenModel.linenItemID;
    copyModel.minibarItemImage = linenModel.linenItemImage;
    copyModel.minibarItemNew = linenModel.linenItemNew;
    copyModel.minibarItemTitle = linenModel.linenItemTitle;
    copyModel.minibarItemUsed = linenModel.linenItemUsed;
    return copyModel;
}

#pragma mark - Add to Cart Press
-(void)handleBtnAddToCartPressed
{
    if (datasLinenCart == nil) {
        datasLinenCart = [[NSMutableArray alloc] init];
    }
    
    
    for (NSInteger indexSection=0; indexSection < sectionDatas.count; indexSection ++) {
        LinenSectionInfoV2 *sectionInfo = [sectionDatas objectAtIndex:indexSection];
        
        MiniBarSectionInfoV2 *sectionCartLinen = nil;
        BOOL isExistingSectionInCart = NO;
        
        for (NSInteger indexRow=0; indexRow < sectionInfo.rowHeights.count; indexRow++) {
            LinenItemModelV2 *model = [sectionInfo.rowHeights objectAtIndex:indexRow];
            
            BOOL isExistingInCart = NO;
            
            for (MiniBarSectionInfoV2 *sectionInCart in datasLinenCart) {
                if (sectionInCart.minibarCategory.minibarCategoryID == sectionInfo.linenCategory.linenCategoryID) {
                    isExistingSectionInCart = YES;
                    sectionCartLinen = sectionInCart;
                    //check items in same category, if item is existing in this category, just update number, if not insert new item in this cart
                    for (MiniBarItemModelV2 *itemInCart in sectionInCart.rowHeights) {
                        if (itemInCart.minibarItemID == model.linenItemID) {
                            isExistingInCart = YES;
                            if (model.linenItemUsed > 0) {
                                itemInCart.minibarItemUsed += model.linenItemUsed;
                            } else {
                                if (model.linenItemNew > 0) {
                                    itemInCart.minibarItemNew += model.linenItemNew;
                                }
                            }
                            break;
                        }
                    }
                }
            }
            
            if (isExistingSectionInCart == NO) {
                if (sectionCartLinen == nil) {
                    sectionCartLinen = [[MiniBarSectionInfoV2 alloc] init];
                }
            }
            
            if (isExistingInCart == NO) {
                if (model.linenItemUsed > 0) {
                    MiniBarItemModelV2 *copyModel = [[MiniBarItemModelV2 alloc] init];
                    
                    copyModel.minibarItemID = model.linenItemID;
                    copyModel.minibarItemImage = model.linenItemImage;
                    copyModel.minibarItemNew = model.linenItemNew;
                    copyModel.minibarItemTitle = model.linenItemTitle;
                    copyModel.minibarItemUsed = model.linenItemUsed;
                    [sectionCartLinen insertObjectToNextIndex:copyModel];
                } else {
                    if (model.linenItemNew > 0) {
                        MiniBarItemModelV2 *copyModel = [[MiniBarItemModelV2 alloc] init];
                        
                        copyModel.minibarItemID = model.linenItemID;
                        copyModel.minibarItemImage = model.linenItemImage;
                        copyModel.minibarItemNew = model.linenItemNew;
                        copyModel.minibarItemTitle = model.linenItemTitle;
                        copyModel.minibarItemUsed = model.linenItemUsed;
                        [sectionCartLinen insertObjectToNextIndex:copyModel];
                    }
                }
            }
            
            //Hao Tran Remove
            //model.linenItemNew = 0;
            //model.linenItemUsed = 0;
        }
        if (sectionCartLinen.rowHeights.count > 0 && isExistingSectionInCart == NO) {
            MiniBarCategoryModelV2 *category = [[MiniBarCategoryModelV2 alloc] init];
            category.minibarCategoryID = sectionInfo.linenCategory.linenCategoryID;
            category.minibarCategoryImage = sectionInfo.linenCategory.linenCategoryImage;
            category.minibarCategoryTitle = sectionInfo.linenCategory.linenCategoryTitle;
            sectionCartLinen.minibarCategory = category;
            [datasLinenCart addObject:sectionCartLinen];
        }
    }
    if (datasLinenCart.count <= 0) {
        statusSave = NO;
    }
    
//    Hao Tran - Remove for somethings were made this application slow down
//    NSInteger countNew = 0;
//    NSInteger countUsed = 0;
//    for (NSInteger indexSection=0; indexSection < datasLinenCart.count; indexSection ++) {
//        MiniBarSectionInfoV2 *sectionInfo = [datasLinenCart objectAtIndex:indexSection];
//        for (NSInteger indexRow=0; indexRow < sectionInfo.rowHeights.count; indexRow++) {
//            MiniBarItemModelV2 *model = [sectionInfo.rowHeights objectAtIndex:indexRow];
//            if (model.minibarItemNew > 0) {
//                countNew += model.minibarItemNew;
//            }
//            if (model.minibarItemUsed > 0) {
//                countUsed += model.minibarItemUsed;
//            }
//        }
//    }
//    
//    [cartButton setTitle:[NSString stringWithFormat:@"%d", countUsed] forState:UIControlStateNormal];
    
}

- (IBAction)btnPostingHistory_Click:(UIButton *)sender {
    HistoryPosting *historyPosting = [[HistoryPosting alloc] initWithNibName:@"HistoryPosting" bundle:nil];
    //    historyPosting.isPushFrom = IS_PUSHED_FROM_PHYSICAL_CHECK;
    historyPosting.roomID = txtRoom.text;
    historyPosting.isFromPostingFunction = isFromMainPosting;
    historyPosting.selectedModule = ModuleHistory_Linen;
    [self.navigationController pushViewController:historyPosting animated:YES];
}


- (IBAction)btnAddToCartPressed:(id)sender {
    statusSave = YES;
    statusChooseLinen = YES;
    statusAddToCart = YES;
    [self handleBtnAddToCartPressed];
    [tbvLinen reloadData];
}

#pragma mark - Cart Press
-(void)cartButtonPressed {
    MiniBarLinenCartViewV2 *cartView = [[MiniBarLinenCartViewV2 alloc] initWithNibName:NSStringFromClass([MiniBarLinenCartViewV2 class]) bundle:nil];
    
    cartView.delegate = self;
    
    cartView.countCartServiceId = countServiceId;
    
    cartView.sectionDatas = datasLinenCart;
    
    for (LinenSectionInfoV2 *sectionInfo in datasLinenCart) {
        sectionInfo.open = YES;
    }
    
    if (statusSave == NO && statusChooseLinen == NO) {
        cartView.statusChooseQty = NO;
    }
    else
    {
        cartView.statusChooseQty = YES;
    }
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK] style:UIBarButtonItemStyleBordered target:nil action:nil];
    self.navigationItem.backBarButtonItem = backButton;
    
    [self.navigationController pushViewController:cartView animated:YES];
}

#pragma mark - Load Linen
-(void)loadingDatas {
    
    CountManagerV2 *manager = [[CountManagerV2 alloc] init];
    sectionDatas = [[NSMutableArray alloc] init];
    
    //Count Order
    countOrder = [[CountOrdersModelV2 alloc] init];
    countOrder.countRoomAssignId = [TasksManagerV2 getCurrentRoomAssignment];
    //    countOrder.countRoomId = 14085;
    countOrder.countServiceId = countServiceId;
    countOrder.countStatus = tStatusChecked;
    countOrder.countUserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    countOrder.countCollectDate = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
    countOrder.countRoomNumber = [RoomManagerV2 getCurrentRoomNo];
    countOrder.countPostStatus = POST_STATUS_UN_CHANGED;
    
    //Hao Tran - Remove for SG00054442
    //    BOOL isCheckCountOrder = [manager isCheckCountOrder:countOrder];
    //    if (isCheckCountOrder == TRUE) {
    //        [manager loadCountOrdersModelByRoomIdUserIdAndServiceId:countOrder];
    //    }
    
    [manager insertCountOrder:countOrder];
    countOrder =  [manager loadCountOrdersModelByRoomAssignId:(int)countOrder.countRoomAssignId roomNumber:countOrder.countRoomNumber userId:(int)countOrder.countUserId serviceId:(int)countOrder.countServiceId postStatus:(int)POST_STATUS_UN_CHANGED];
    
    //Hao Tran - END
    
    NSMutableArray *array;
    if ([[UserManagerV2 sharedUserManager].currentUserAccessRight isLoadLinenByRoomType]) {
        [self getRoomTypeByRoomNo];
        array = [manager loadAllCountCategoryByCountServiceId:countServiceId andRoomTypeId:_roomTypeId];
    } else {
        array = [manager loadAllCountCategoryByCountServiceId:countServiceId];
    }
    
    for (CountCategoryModelV2 *countCategories in array) {
        
        LinenSectionInfoV2 *sectionInfo = [[LinenSectionInfoV2 alloc] init];
        
        LinenCategoryModelV2 *categoryModel = [[LinenCategoryModelV2 alloc] init];
        categoryModel.linenCategoryID = countCategories.countCategoryId;
        //set language name linen category
        if ([[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString: (NSString *)ENGLISH_LANGUAGE] == NO) {
            categoryModel.linenCategoryTitle = countCategories.countCategoryNameLang;
        }
        else{
            categoryModel.linenCategoryTitle = countCategories.countCategoryName;
        }
        categoryModel.linenCategoryImage = UIImagePNGRepresentation([UIImage imageWithData:countCategories.countCategoryImage]);
        sectionInfo.linenCategory = categoryModel;
        
        NSMutableArray *arrayCountItem;
        if ([[UserManagerV2 sharedUserManager].currentUserAccessRight isLoadLinenByRoomType]) {
            arrayCountItem = [manager loadAllCountItemByCountCategoryId:countCategories.countCategoryId andRoomTypeId:_roomTypeId];
        } else {
            arrayCountItem = [manager loadAllCountItemByCountCategoryId:countCategories.countCategoryId];
        }
        for (CountItemsModelV2 *countItems in arrayCountItem) {
            
            CountOrderDetailsModelV2 *countOrderDetail = [manager loadCountOrderDetailsByItemId:countItems.countItemId AndOrderId:countOrder.countOrderId];
            
            sumLinenUsed += countOrderDetail.countCollected;
            
            LinenItemModelV2 *itemModel = [[LinenItemModelV2 alloc] init];
            itemModel.linenItemID = countItems.countItemId;
            //set language name linen item
            if ([[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString: (NSString *)ENGLISH_LANGUAGE] == NO) {
                itemModel.linenItemTitle = countItems.countItemLang;
            }
            else{
                itemModel.linenItemTitle = countItems.countItemName;
            }
            itemModel.linenItemImage = UIImagePNGRepresentation([UIImage imageWithData:countItems.countItemImage]);
            // CFG [20160921/CRF-00001275] - Auto select if there's a value to auto select.
            if (countOrderDetail.countCollected != 0) {
                itemModel.linenItemUsed = countOrderDetail.countCollected;
            }
            else {
                itemModel.linenItemUsed = countItems.countDefaultQuantity;
            }
            if (countOrderDetail.countNew != 0) {
                itemModel.linenItemNew = countOrderDetail.countNew;
            }
            else {
                itemModel.linenItemNew = countItems.countDefaultQuantity;
            }
            // CFG [20160921/CRF-00001275] - End.
            [sectionInfo insertObjectToNextIndex:itemModel];
            
            //check data with cartdatas
            for (MiniBarSectionInfoV2 *section in datasLinenCart) {
                if (section.minibarCategory.minibarCategoryID == countItems.countCategoryId) {
                    for (MiniBarItemModelV2 *minibar in section.rowHeights) {
                        if (minibar.minibarItemID == itemModel.linenItemID) {
                            itemModel.linenItemNew = minibar.minibarItemNew;
                            itemModel.linenItemUsed = minibar.minibarItemUsed;
                        }
                    }
                }
            }
        }
        
        [sectionDatas addObject:sectionInfo];
        
    }
    
    //Discart all item in Cart
    [datasLinenCart removeAllObjects];
    
    statusAddToCart = YES;
    
    //edit all views
    [self modifyAllViews];
    [tbvLinen reloadData];
}

- (void)getRoomTypeByRoomNo {
    
    if (![txtRoom.text isEqualToString:@""]) {
        int userId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
        RoomTypeModel *rtModel = [[RoomManagerV2 sharedRoomManager] getRoomTypeByRoomNoDBWithUserId:userId andRoomNumber:txtRoom.text];
        if (rtModel != nil) {
            _roomTypeId = rtModel.amsId;
        } else {
            rtModel = [[RoomManagerV2 sharedRoomManager] getRoomTypeByRoomNoWSWithUserId:userId hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId roomNumber:txtRoom.text];
            _roomTypeId = rtModel != nil ? rtModel.amsId : -1;
        }
    }
}

#pragma mark - UITableView Datasource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return sectionDatas.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    LinenSectionInfoV2 *sectioninfo = [sectionDatas objectAtIndex:section];
    NSInteger numberRowInSection = sectioninfo.rowHeights.count;
    return sectioninfo.open ? numberRowInSection : 0;
}

int tagItemName = 100;
int tagUsedButton = 101;
int tagNewButton = 102;
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifycell = @"cellidentify";
    LinenItemCellV2 *cell = nil;
    
    cell = (LinenItemCellV2 *)[tableView dequeueReusableCellWithIdentifier:identifycell];
    if (cell == nil) {
        NSArray *array = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([LinenItemCellV2 class]) owner:self options:nil];
        cell = [array objectAtIndex:0];
        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
            cell.btnNew.layer.cornerRadius = 7.0f;
            cell.btnNew.layer.borderWidth = 1.0f;
            cell.btnNew.layer.borderColor = [[UIColor lightGrayColor] CGColor];
            
            cell.btnUsed.layer.cornerRadius = 7.0f;
            cell.btnUsed.layer.borderWidth = 1.0f;
            cell.btnUsed.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        }
    }
    
    cell.delegate = self;
    LinenSectionInfoV2 *sectionInfo = [sectionDatas objectAtIndex:indexPath.section];
    LinenItemModelV2 *model = [sectionInfo.rowHeights objectAtIndex:indexPath.row];
    [cell.imgItem setImage:[UIImage imageWithData:model.linenItemImage]];
    [cell.lblTitleItem setText:model.linenItemTitle];
//    if(indexPath.section == 0 && indexPath.row == 0){
    if (indexPath.row == 0) {
        [cell.lblNew setText:[L_linen_item_new currentKeyToLanguage]];
        [cell.lblUsed setText:[L_linen_item_used currentKeyToLanguage]];
        [cell.btnUsed setFrame:CGRectMake(189, 27, 54, 37)];
        [cell.btnNew setFrame:CGRectMake(256, 27, 54, 37)];
    } else {
        [cell.lblNew setText:@""];
        [cell.lblUsed setText:@""];
        [cell.btnUsed setFrame:CGRectMake(189, 18, 54, 37)];
        [cell.btnNew setFrame:CGRectMake(256, 18, 54, 37)];
    }
    //add target for btnUsed and btnNew
    [cell.btnUsed addTarget:self action:@selector(tapbtnUsed) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnNew addTarget:self action:@selector(tapbtnNew) forControlEvents:UIControlEventTouchUpInside];
    
    // CFG [20160922/CRF-00001275] -
    //if (statusAddToCart == YES) {
    //    [cell.btnNew setTitle:[NSString stringWithFormat:@"%d", 0] forState:UIControlStateNormal];
    //    [cell.btnUsed setTitle:[NSString stringWithFormat:@"%d",0] forState:UIControlStateNormal];
    //} else {
        [cell.btnNew setTitle:[NSString stringWithFormat:@"%d", (int)model.linenItemNew] forState:UIControlStateNormal];
        [cell.btnUsed setTitle:[NSString stringWithFormat:@"%d", (int)model.linenItemUsed] forState:UIControlStateNormal];
    //}
    // CFG [20160922/CRF-00001275] - End.
    
    switch ([[UserManagerV2 sharedUserManager].currentUserAccessRight linenNewUsedDisplay]) {
        case 1:
            [cell.lblUsed setText:@""];
            if (indexPath.row == 0) {
                [cell.lblNew setText:[L_linen_item_used currentKeyToLanguage]];
                [cell.btnUsed setFrame:CGRectMake(256, 27, 54, 37)];
            } else {
                [cell.lblNew setText:@""];
                [cell.btnUsed setFrame:CGRectMake(256, 18, 54, 37)];
            }
            [cell.btnNew setFrame:CGRectMake(0, 0, 0, 0)];
            break;
        case 2:
            [cell.lblUsed setText:@""];
            [cell.btnUsed setFrame:CGRectMake(0, 0, 0, 0)];
            break;
        default:
            break;
    }
    
    [cell.lblTitleItem setTag:tagItemName];
    [cell.btnNew setTag:tagNewButton];
    [cell.btnUsed setTag:tagUsedButton];
    
    [cell setPath:indexPath];
    return cell;
}

-(void) tapbtnUsed{
    [txtRoom resignFirstResponder];
}
-(void) tapbtnNew{
    [txtRoom resignFirstResponder];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return heightLinenCell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return heightLinenHeader;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    LinenSectionInfoV2 *sectionInfo = [sectionDatas objectAtIndex:section];
    
    if (sectionInfo.headerView == nil) {
        LinenSectionViewV2 *headerView = [[LinenSectionViewV2 alloc] initWithSection:section LinenCategoryModelV2:sectionInfo.linenCategory andOpenStatus:NO];
        sectionInfo.headerView = headerView;
        headerView.delegate = self;
    }
    return sectionInfo.headerView;
}

//HaoTran[20130606] - Add function modified all layout views
#pragma mark - Calculate layout

//Calculate total frame of table Minibar
-(CGRect)totalTableLinenFrame
{
    CGFloat heightHeaders = 0;
    CGFloat heightCells = 0;
    for (MiniBarSectionInfoV2 *currentSection in sectionDatas) {
        heightHeaders += heightLinenHeader;
        if(currentSection.open == YES){
            heightCells += [currentSection.rowHeights count] * heightLinenCell;
        }
    }
    
    CGRect result = tbvLinen.frame;
    result.size.height = heightHeaders + heightCells;
    return result;
}

-(void)modifyAllViews
{
    CGRect tableMiniBarFrame = [self totalTableLinenFrame];
    [UIView animateWithDuration:0.5 animations:^{[tbvLinen setFrame:tableMiniBarFrame];}];
    [UIView animateWithDuration:0.2 animations:^{[btnSubmit setFrame:CGRectMake(10, 15 + tbvLinen.frame.size.height, 300, 64)];}];
    [UIView animateWithDuration:0.2 animations:^{[btnPostingHistory setFrame:CGRectMake(10, 25 + tbvLinen.frame.size.height + CGRectGetHeight(btnSubmit.bounds), 300, 55)];}];
    [UIView animateWithDuration:0.2 animations:^{[lblSubmit setFrame:CGRectMake(121, 25 + tbvLinen.frame.size.height, 135, 39)];}];
    [UIView animateWithDuration:0.5 animations:^{[scrollView setContentSize:CGSizeMake(scrollView.contentSize.width, btnSubmit.frame.origin.y + btnSubmit.frame.size.height + 55 + CGRectGetHeight(btnPostingHistory.bounds))];}];
}

//HaoTran[20130606] - END

#pragma mark - Section Header Delegate
-(void)sectionHeaderView:(LinenSectionViewV2 *)sectionheaderView sectionOpened:(NSInteger)section {
    
    //-----Log action click section----
    if([LogFileManager isLogConsole])
    {
        NSInteger userId = [[UserManagerV2 sharedUserManager] currentUser].userId;
        NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        NSLog(@"<%@><Linen><UserId:%d><click row at section index:%d to open>", time, (int)userId, (int)section);
    }
    LinenSectionInfoV2 *sectionInfo = [sectionDatas objectAtIndex:section];
    sectionInfo.open = YES;
    
    //edit all views
    [self modifyAllViews];
    
    NSInteger countOfRowsToInsert = [sectionInfo.rowHeights count];
    NSMutableArray *indexPathsToInsert = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i < countOfRowsToInsert; i++) {
        [indexPathsToInsert addObject:[NSIndexPath indexPathForRow:i
                                                         inSection:section]];
    }
    
    UITableViewRowAnimation insertAnimation;
    //    LinenSectionInfoV2 *sectionInfoPrevious = nil;
    //    if (section > 0) {
    //        sectionInfoPrevious = [sectionDatas objectAtIndex:section - 1];
    //        if (sectionInfoPrevious.open) {
    //            insertAnimation = UITableViewRowAnimationBottom;
    //        }
    //    } else
    //        insertAnimation = UITableViewRowAnimationTop;
    insertAnimation = UITableViewRowAnimationAutomatic;
    
    
    [self.tbvLinen beginUpdates];
    [self.tbvLinen insertRowsAtIndexPaths:indexPathsToInsert withRowAnimation:insertAnimation];
    [self.tbvLinen endUpdates];
    
    if([indexPathsToInsert count] > 0)
    {
        [self.tbvLinen scrollToRowAtIndexPath:[indexPathsToInsert objectAtIndex:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    //    [indexPathsToInsert release];
}

-(void)sectionHeaderView:(LinenSectionViewV2 *)sectionheaderView sectionClosed:(NSInteger)section {
    
    //-----Log action click section----
    if([LogFileManager isLogConsole])
    {
        NSInteger userId = [[UserManagerV2 sharedUserManager] currentUser].userId;
        NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSLog(@"<%@><Linen><UserId:%d><click row at section index:%d to close>", time, (int)userId, (int)section);
    }
    LinenSectionInfoV2 *sectionInfo = [sectionDatas objectAtIndex:section];
    sectionInfo.open = NO;
    
    //edit all views
    [self modifyAllViews];
    
    NSInteger countOfRowsToDelete = [self.tbvLinen numberOfRowsInSection:section];
    if (countOfRowsToDelete > 0) {
        NSMutableArray *indexPathsToDelete = [[NSMutableArray alloc] init];
        for (NSInteger i = 0; i < countOfRowsToDelete; i++) {
            [indexPathsToDelete addObject:[NSIndexPath indexPathForRow:i inSection:section]];
        }
        
        [tbvLinen beginUpdates];
        [self.tbvLinen deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:UITableViewRowAnimationAutomatic];
        [tbvLinen endUpdates];
        
        //        [indexPathsToDelete release];
    }
}

#pragma mark - Item Cell Delegate Methods
-(void)btnUsedItemCellPressedWithIndexPath:(NSIndexPath *)path sender:(UIButton *)button{
    //statusAddToCart = NO;
    NSMutableArray *array = [[NSMutableArray alloc] init];
    for (NSInteger index = 0; index <= 10; index++) {
        [array addObject:[NSString stringWithFormat:@"%d", (int)index]];
    }
    
    PickerViewV2 *picker = [[PickerViewV2 alloc] initPickerViewV2WithDatas:array AndSelectedData:button.titleLabel.text AndIndex:path];
    [picker showPickerViewV2];
    [picker setDelegate:self];
    [picker setTIndex:tUsed];
}

-(void)btnNewItemCellPressedWithIndexPath:(NSIndexPath *)path sender:(UIButton *)button{
    //statusAddToCart = NO;
    NSMutableArray *array = [[NSMutableArray alloc] init];
    for (NSInteger index = 0; index <= 10; index++) {
        [array addObject:[NSString stringWithFormat:@"%d", (int)index]];
    }
    
    PickerViewV2 *picker = [[PickerViewV2 alloc] initPickerViewV2WithDatas:array AndSelectedData:button.titleLabel.text AndIndex:path];
    [picker showPickerViewV2];
    [picker setDelegate:self];
    [picker setTIndex:tNew];
    
}

#pragma mark - PickerView V2 Delegate methods
-(void)didChoosePickerViewV2WithData:(NSString *)data AndIndex:(NSIndexPath *)index WithtIndex:(NSInteger)tIndex{
    LinenSectionInfoV2 *sectionInfo = [sectionDatas objectAtIndex:index.section];
    LinenItemModelV2 *model = [sectionInfo.rowHeights objectAtIndex:index.row];
    
    if ([data integerValue] > 0) {
        statusAddToCart = NO;
    }
    
    switch ([[UserManagerV2 sharedUserManager].currentUserAccessRight linenNewUsedDisplay]) {
        case 0:
            if (tIndex == tUsed) {
                model.linenItemUsed = [data integerValue];
                // DungPhan - 20150828: follow android logic - by FM
                if([[UserManagerV2 sharedUserManager].currentUserAccessRight isLinenSeperateUsedNew]) {
                    model.linenItemNew = [data integerValue];
                }
                //------------Log------------
                if([LogFileManager isLogConsole]) {
                    logCheckUsedValue = model.linenItemUsed;
                    logCheckNewValue = model.linenItemNew;
                    NSInteger userId = [[UserManagerV2 sharedUserManager] currentUser].userId;
                    NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
                    NSLog(@"<%@><Linen><UserId:%d><Input ItemName:%@><ItemUsed:%d><ItemNew:%d>", time, (int)userId,model.linenItemTitle, (int)model.linenItemUsed, (int)model.linenItemNew);
                }
            }
            
            if (tIndex == tNew) {
                model.linenItemNew = [data integerValue];
                //-------Log action chose pickerview---
                if([LogFileManager isLogConsole]) {
                    logCheckNewValue = model.linenItemNew;
                    NSInteger userId = [[UserManagerV2 sharedUserManager] currentUser].userId;
                    NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
                    NSLog(@"<%@><Linen><UserId:%d><Input ItemName:%@><ItemNew:%d>", time, (int)userId,model.linenItemTitle, (int)model.linenItemNew);
                }
            }
            break;
        case 1:
            if (tIndex == tUsed) {
                model.linenItemUsed = [data integerValue];
                //------------Log------------
                if([LogFileManager isLogConsole]) {
                    logCheckUsedValue = model.linenItemUsed;
                    NSInteger userId = [[UserManagerV2 sharedUserManager] currentUser].userId;
                    NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
                    NSLog(@"<%@><Linen><UserId:%d><Input ItemName:%@><ItemUsed:%d>", time, (int)userId,model.linenItemTitle, (int)model.linenItemUsed);
                }
            }
            break;
        case 2:
            if (tIndex == tNew) {
                model.linenItemNew = [data integerValue];
                //-------Log action chose pickerview---
                if([LogFileManager isLogConsole]) {
                    logCheckNewValue = model.linenItemNew;
                    NSInteger userId = [[UserManagerV2 sharedUserManager] currentUser].userId;
                    NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
                    NSLog(@"<%@><Linen><UserId:%d><Input ItemName:%@><ItemNew:%d>", time, (int)userId,model.linenItemTitle, (int)model.linenItemNew);
                }
            }
            break;
        default:
            break;
    }
    
    [tbvLinen reloadRowsAtIndexPaths:[NSArray arrayWithObject:index] withRowAnimation:UITableViewRowAnimationFade];
    
    // Get text value from btnUsed and btnNew
    
    //    [btnSubmit setEnabled:[self isValidQty:index]];
    //[btnSubmit setEnabled:[self isValidQty]];
    
    //    LinenItemCellV2 *linenItem = (LinenItemCellV2 *)[tbvLinen cellForRowAtIndexPath:index];
    //    NSString *itemUseStr = [NSString stringWithFormat:@"%@",[linenItem.btnUsed titleForState:UIControlStateNormal]];
    //    NSString *itemnewStr = [NSString stringWithFormat:@"%@",[linenItem.btnNew titleForState:UIControlStateNormal]];
    //    NSInteger i = [itemUseStr integerValue];
    //    NSInteger j = [itemnewStr integerValue];
    //
    //    if ((i != 0 && j!=0)) {
    //        [btnSubmit setEnabled:YES];
    //    }
    //    else
    //        [btnSubmit setEnabled:NO];
}

-(BOOL)isOnlyViewedAccessRight
{
    //Hao Tran[20130517/Check Access Right] - Check access right only view
    if(isFromMainPosting){
        if (postingLinen.isAllowedView){
            return YES;
        }
    } else if(actionLinen.isAllowedView) {
        return YES;
    }
    //Hao Tran[20130517/Check Access Right] - END
    
    return NO;
}

-(BOOL)isValidQty
{
    if([self isOnlyViewedAccessRight]) {
        return NO;
    }
    
    int a = 0;
    int b = 0;
    for(NSInteger i=0;i<[sectionDatas count];i++)
    {
        LinenSectionInfoV2 *sectioninfo = [sectionDatas objectAtIndex:i];
        for (NSInteger j = 0;j < [sectioninfo.rowHeights count];j++)
        {
            NSIndexPath *index = [NSIndexPath indexPathForRow:j inSection:i];
            LinenItemCellV2 *linenItem = (LinenItemCellV2 *)[self tableView:tbvLinen cellForRowAtIndexPath:index];
            NSString *itemUseStr = [NSString stringWithFormat:@"%@",[linenItem.btnUsed titleForState:UIControlStateNormal]];
            NSString *itemnewStr = [NSString stringWithFormat:@"%@",[linenItem.btnNew titleForState:UIControlStateNormal]];
            a = [itemUseStr intValue];
            b = [itemnewStr intValue];
            if (a != 0 || b != 0) {
                break;
            }
        }
        if (a != 0 || b != 0) {
            break;
        }
    }
    
    /*
    if (txtRoom.text.length > 0) {
        if(a!=0 && b!=0)
            return YES;
        else if(a!=0 && b==0)
            return YES;
        else if (a == 0 && b != 0)
            return YES;
        else if(a==0 && b==0)
            return NO;
    }
    else {
        return NO;
    }*/
    
    if(a != 0 || b != 0) {
        return YES;
    }
    
    return NO;
}

#pragma mark - Save Cart Minibar
//Return response code
-(int) saveData{
    int responseCode = RESPONSE_STATUS_OK;
    if (statusSave == YES) {
        responseCode = [self saveLinenCart];
        [self btnAddToCartPressed:btnAddToCart];
    }
    statusChooseLinen = NO;
    statusSave = NO;
    return  responseCode;
}

//Return response code
-(int)saveLinenCart
{
    int responseCode = RESPONSE_STATUS_OK;
    // @ show loading data .
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getRaSavingData]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES];
    
    ///Save minibar cart
    CountManagerV2 *manager = [[CountManagerV2 alloc] init];
    SyncManagerV2 *synManager = [[SyncManagerV2 alloc] init];
    NSInteger userId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    NSInteger roomAssignId = [TasksManagerV2 getCurrentRoomAssignment];
    
    //Update Order in minibar
    //CountOrdersModelV2 *countOrder = [[CountOrdersModelV2 alloc] init];
    countOrder.countRoomAssignId = roomAssignId;
    countOrder.countServiceId = countServiceId;
    countOrder.countStatus = tStatusChecked;
    countOrder.countUserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    countOrder.countCollectDate = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
    countOrder.countRoomNumber = [RoomManagerV2 getCurrentRoomNo];
    countOrder.countPostStatus = POST_STATUS_SAVED_UNPOSTED;
    [manager updateCountOrder:countOrder];
    
    //Hao Tran - Remove for SG00054442
    //    BOOL isCheckCountOrder = [manager isCheckCountOrder:countOrder];
    //    if (isCheckCountOrder == FALSE) {
    //        [manager insertCountOrder:countOrder];
    //    }
    //    [manager loadCountOrdersModelByRoomIdUserIdAndServiceId:countOrder];
    
    //[manager insertCountOrder:countOrder];
    //countOrder =  [manager loadCountOrdersModelByRoomAssignId:countOrder.countRoomAssignId roomNumber:countOrder.countRoomNumber userId:countOrder.countUserId serviceId:countOrder.countServiceId postStatus:POST_STATUS_SAVED_UNPOSTED];
    //Hao Tran - END
    
    //-----Get data for post WSLog
    int switchStatus = (int)[[NSUserDefaults standardUserDefaults] integerForKey:ENABLE_LOGWS];
    if (switchStatus == 1) {
        NSInteger totalSections = [self numberOfSectionsInTableView:tbvLinen];
        
        for (int i = 0; i<totalSections; i++) {
            NSInteger totalRow = [self tableView:self.tbvLinen numberOfRowsInSection:i];
            for (int j = 0; j<totalRow; j++) {
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:j inSection:i];
                UITableViewCell *cell = [self.tbvLinen cellForRowAtIndexPath:indexPath];
                UILabel *lblItemName = (UILabel *)[cell viewWithTag:tagItemName];
                UIButton *btnUsed = (UIButton *)[cell viewWithTag:tagUsedButton];
                
                UIButton *btnNew = (UIButton *)[cell viewWithTag:tagNewButton];
                NSString *valueBtnUsed = btnUsed.titleLabel.text;
                NSString *valueBtnNew = btnNew.titleLabel.text;
                NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
                
                NSString *messageValue = [NSString stringWithFormat:@"Line Post WSLog:Time:%@, UserId:%d, RoomId:%@, ItemName:%@, UsedValue:%@, NewValue:%@", time, (int)userId, txtRoom.text, lblItemName.text, valueBtnUsed, valueBtnNew];
                if ([valueBtnNew integerValue] != 0 || [valueBtnUsed integerValue] != 0) {
                    [synManager postWSLogCountService:userId Message:messageValue MessageType:1];
                }
            }
        }
    }
    //-----
    
    //save minibar or linen detail
    NSMutableArray *postItems = [[NSMutableArray alloc] init];
    BOOL isPostingZeroQuantity = [[UserManagerV2 sharedUserManager].currentUserAccessRight isPostingZeroQuantity];
    
    //Hao Tran Remove
    //save linen order detail
    //    for (NSInteger indexSection=0; indexSection < datasLinenCart.count; indexSection ++) {
    //        MiniBarSectionInfoV2 *sectionInfo = [datasLinenCart objectAtIndex:indexSection];
    //        for (NSInteger indexRow=0; indexRow < sectionInfo.rowHeights.count; indexRow++) {
    //            MiniBarItemModelV2 *model = [sectionInfo.rowHeights objectAtIndex:indexRow];
    
    for (NSInteger indexSection = 0; indexSection < sectionDatas.count; indexSection ++) {
        LinenSectionInfoV2 *sectionInfo = [sectionDatas objectAtIndex:indexSection];
        for (NSInteger indexRow = 0; indexRow < sectionInfo.rowHeights.count; indexRow++) {
            
            LinenItemModelV2 *currentLinenItem = [sectionInfo.rowHeights objectAtIndex:indexRow];
            MiniBarItemModelV2 *model = [self convertMiniBarItemFromLinenItem:currentLinenItem];
            
            //Hao Tran remove for PRD 2.2
            //if (model.minibarItemNew != 0 || model.minibarItemUsed != 0) {
            
            //Hao Tran[20141105 / Apply Configuration POSTING_POST_ZEROTH_VALUE] - if 0 follow normal, if 1 post 0
            if(!isPostingZeroQuantity && (model.minibarItemNew == 0 && model.minibarItemUsed == 0)) {
                continue;
            }
            
            CountOrderDetailsModelV2 *countOrderDetailModel = [manager loadCountOrderDetailsByItemId:model.minibarItemID AndOrderId:countOrder.countOrderId];
            
            if (countOrderDetailModel.countOrderDetailId != 0) {
                
                if (countOrderDetailModel.countNew != model.minibarItemNew || countOrderDetailModel.countCollected != model.minibarItemUsed) {
                    
                    //update Order detail in linen
                    countOrderDetailModel.countNew = model.minibarItemNew;
                    countOrderDetailModel.countCollected = model.minibarItemUsed;
                    countOrderDetailModel.countCollectedDate = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm"];
                    countOrderDetailModel.countPostStatus = POST_STATUS_SAVED_UNPOSTED;
                    
                    //update data
                    [manager updateCountOrderDetail:countOrderDetailModel];
                    
                }
                
            }
            else
            {
                //insert Order detail in linen
                CountOrderDetailsModelV2 *countOrderDetails = [[CountOrderDetailsModelV2 alloc] init];
                countOrderDetails.countCollected = model.minibarItemUsed;
                countOrderDetails.countNew = model.minibarItemNew;
                countOrderDetails.countItemId = model.minibarItemID ;
                countOrderDetails.countOrderId = countOrder.countOrderId;
                countOrderDetails.countCollectedDate = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm"];
                countOrderDetails.countPostStatus = POST_STATUS_SAVED_UNPOSTED;
                
                //insert data
                [manager insertCountOrderDetail:countOrderDetails];
                
            }
            
            //---------------------Post WS-------------------
            CountOrderDetailsModelV2 *countOrderDetailModelPost = [manager loadCountOrderDetailsByItemId:model.minibarItemID AndOrderId:countOrder.countOrderId];
            [postItems addObject:countOrderDetailModelPost];
            
            //                old WS
            //                BOOL statusPost = [synManager postLinenItemWithUserId:userId roomAssignId:roomAssignId AndLinenOrderModel:countOrderDetailModelPost];
            //
            //                if (statusPost == YES) {
            //                    //post is success, clear database
            //                    if (countOrderDetailModelPost.countOrderDetailId != 0) {
            //                        [manager deleteCountOrderDetail:countOrderDetailModelPost];
            //                    }
            //                    [datasLinenCart removeAllObjects];
            //                }
            //---------------------End-----------------------
            
            //}
        }
    }
    
    //post list of PostItem to new WS for Minibar and Linen
    NSArray *postedItems = nil;
//    if(ENABLE_POST_LINEN_WITH_ROOM_NUMBER){
    NSString *roomNumber = txtRoom.text;
    int count = 0;
    if(isDemoMode){
        count = 1;
    }
    else{
        postedItems = [synManager postAllLinenItemsWithUserId:userId roomassignId:(int)roomAssignId roomNumber:roomNumber AndLinenOrderModelList:postItems];
        count = (int)[postedItems count];
    }
    
//    } else {
//        postedItems = [[NSArray alloc] initWithArray:[synManager postAllLinenItemsWithUserId:userId roomAssignId:roomAssignId AndLinenOrderModelList:postItems]];
//    }
    
    if (count != 0) {
        
        for (CountOrderDetailsModelV2 *postedItem in postItems) {
            [manager deleteCountOrderDetail:postedItem];
            if (postedItem.countCollected != 0 || postedItem.countNew != 0) {
                [self saveCountHistoryItem:postedItem];
            }
        }
        [manager deleteCountOrderDetailsByOrderId:(int)countOrder.countOrderId];
        [manager deleteCountOrderByOrderId:(int)countOrder.countOrderId];
        
        //Post old items
        if (haveOldCountOrders) {
            [manager postAllPendingCountDataByUserId:(int)userId serviceId:(int)countServiceId];
        }
        
        //Hao Tran - Reset data for view
        for (NSInteger indexSection = 0; indexSection < sectionDatas.count; indexSection ++) {
            LinenSectionInfoV2 *sectionInfo = [sectionDatas objectAtIndex:indexSection];
            for (NSInteger indexRow = 0; indexRow < sectionInfo.rowHeights.count; indexRow++) {
                LinenItemModelV2 *linenItem = [sectionInfo.rowHeights objectAtIndex:indexRow];
                linenItem.linenItemUsed = 0;
                linenItem.linenItemNew = 0;
            }
        }
        
        [datasLinenCart removeAllObjects];
        sumLinenUsed = 0;
        if ([[UserManagerV2 sharedUserManager].currentUserAccessRight isLoadLinenByRoomType]) {
            [txtRoom setText:@""];
            _roomTypeId = 0;
        }
        [self loadingDatas];
        [[[[iToast makeText:[[LanguageManagerV2 sharedLanguageManager] getMsgSaveMessage]] setGravity:iToastGravityBottom] setDuration:iToastDurationNormal] show];
        
    } else {
        responseCode = RESPONSE_STATUS_NO_NETWORK;
        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:@"" message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_internet_exception] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getOK] otherButtonTitles:nil];
        alert.delegate = self;
        [alert setTag:tagNoNetwork];
        [alert show];
    }
    
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getMsgSaveMessage]];
    //hide saving data.
    [self performSelector:@selector(hiddenHUDAfterSaved:) withObject:HUD afterDelay:0.0f];
    
    return responseCode;
}

//Save data into table Count History
-(void) saveCountHistoryItem:(CountOrderDetailsModelV2 *) model
{
    CountHistoryModel *countHistoryModel = [[CountHistoryModel alloc]init];
    countHistoryModel.room_id = txtRoom.text;
    countHistoryModel.count_item_id = model.countItemId;
    countHistoryModel.count_collected = model.countCollected;
    countHistoryModel.count_new = model.countNew;
    countHistoryModel.count_service = countServiceId;
    countHistoryModel.count_date = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSInteger userId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    countHistoryModel.count_user_id = userId;
    
    CountHistoryManager *countHistoryManager = [[CountHistoryManager alloc]init];
    [countHistoryManager insertCountHistoryData:countHistoryModel];
}


#pragma didden after save.
-(void) hiddenHUDAfterSaved:(MBProgressHUD *)HUD{
    [HUD hide:YES];
    [HUD removeFromSuperview];
    //    [HUD release];
}

#pragma mark - MiniBarLinenCart delegate methods
-(void)saveMiniBarLinenCartItem
{
    statusChooseLinen = NO;
}

#pragma mark - set Captions View
-(void)setCaptionsView {
    [btnAddToCart setTitle:[[LanguageManagerV2 sharedLanguageManager] getAddToCart] forState:UIControlStateNormal];
    [self setTitle:[[LanguageManagerV2 sharedLanguageManager] getLinen]];
    
    msgbtnNo = [[LanguageManagerV2 sharedLanguageManager] getNo];
    msgbtnYes = [[LanguageManagerV2 sharedLanguageManager] getYes];
    msgSaveCount = [[LanguageManagerV2 sharedLanguageManager] getMsgAlertSaveCounts];
    msgDiscardCount = [[LanguageManagerV2 sharedLanguageManager] getMsgAlertDiscardCounts];
    
    //refresh topview
    TopbarViewV2 *topview = [self getTopBarView];
    [topview refresh:[[SuperRoomModelV2 alloc] init]];
}

#pragma mark - Handle Topbar Methods
-(void)loadTopbarView {
    TopbarViewV2 *topbar = [[TopbarViewV2 alloc] initViewWithSuperModel:[[CommonVariable sharedCommonVariable] superRoomModel]];
    [topbar setTag:tagTopbarView];
    [self.view addSubview:topbar];
    [topbar setHidden:YES];
    
    [topbar setAlpha:0.0];
    [self addButtonHandleShowHideTopbar];
}

-(void)showTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationWillStartSelector:@selector(setHiddenTopbarView)];
    
    f = topbar.frame;
    f.origin.y = 0;
    [topbar setFrame:f];
    [topbar setAlpha:1.0];
    
    [self adjustShowForViews];
    
    [UIView commitAnimations];
}

-(void)hideTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDidStopSelector:@selector(setHiddenTopbarView)];
    [UIView setAnimationDelegate:self];
    
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    [topbar setAlpha:0.0];
    
    [self adjustRemoveForViews];
    
    [UIView commitAnimations];
}

-(void)setHiddenTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    [topbar setHidden:!topbar.hidden];
}

-(TopbarViewV2 *)getTopBarView {
    return (TopbarViewV2 *)[self.view viewWithTag:tagTopbarView];
}

//-(void)addButtonHandleShowHideTopbar {
//    UIButton *titleBarButton = [[UIButton alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
//
//    titleBarButton.titleLabel.backgroundColor = [UIColor clearColor];
//    titleBarButton.titleLabel.font = FONT_BUTTON_TOPBAR;
//    titleBarButton.titleLabel.textAlignment = UITextAlignmentCenter;
//    titleBarButton.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
//    [titleBarButton.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
//    [titleBarButton.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
//    [titleBarButton addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
//    [titleBarButton setTitle:[self title] forState:UIControlStateNormal];
//    self.navigationItem.titleView = titleBarButton;
//}

-(void)addButtonHandleShowHideTopbar {
    UIView *vBarButton = [[UIView alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
    UIButton *titleBarButtonFirst = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 200, 30)];
    
    titleBarButtonFirst.titleLabel.backgroundColor = [UIColor clearColor];
    [titleBarButtonFirst.titleLabel setFont: FONT_BUTTON_TOPBAR];
    titleBarButtonFirst.titleLabel.textAlignment = NSTextAlignmentCenter;
    titleBarButtonFirst.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
    
    [titleBarButtonFirst.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
    [titleBarButtonFirst.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
    [titleBarButtonFirst addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    if(isFromMainPosting){
        [titleBarButtonFirst setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_Counts] forState:UIControlStateNormal];
    } else {
        [titleBarButtonFirst setTitle:txtRoom.text forState:UIControlStateNormal];
    }
    
    [vBarButton addSubview:titleBarButtonFirst];
    
    
    UIButton *titleBarButtonSecond = [[UIButton alloc] initWithFrame:CGRectMake(0, 28, 200, 22)];
    [titleBarButtonSecond setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [titleBarButtonSecond.titleLabel setFont:FONT_BUTTON_TOPBAR_SECOND];
    [titleBarButtonSecond setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_Linen_Title] forState:UIControlStateNormal];
    [titleBarButtonSecond addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    [vBarButton addSubview:titleBarButtonSecond];
    
    self.navigationItem.titleView = vBarButton;
    
    //Move title to center vertical
    CGRect titleViewBounds = self.navigationItem.titleView.bounds;
    int moveLeftTitle = 0;
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        moveLeftTitle = 10;
    }
    self.navigationItem.titleView.bounds = CGRectMake(titleViewBounds.origin.x + moveLeftTitle, Y_TOPBAR_TITLE, titleViewBounds.size.width, titleViewBounds.size.height);
}

-(void)topbarTapped:(UIButton *)sender {
    TopbarViewV2 *topbar = [self getTopBarView];
    [sender setEnabled:NO];
    if (topbar.isHidden) {
        [self showTopbarView];
    } else {
        [self hideTopbarView];
    }
    [self performSelector:@selector(topbarButtonEnable:) withObject:sender afterDelay:0.5];
}

-(void) topbarButtonEnable:(UIButton *) topbarBtn {
    [topbarBtn setEnabled:YES];
}

-(void)adjustShowForViews {
    
    //adjust all views when show
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    CGRect fRoomView = viewRoom.frame;
    [viewRoom setFrame:CGRectMake(0, f.size.height, 320, fRoomView.size.height)];
    
    CGRect fscv = scrollView.frame;
    [scrollView setFrame:CGRectMake(0, f.size.height + fRoomView.size.height, 320, fscv.size.height - f.size.height)];
    
    //Edit position in view
    [self modifyAllViews];
}

-(void)adjustRemoveForViews {
    //adjust all views when hide
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    CGRect fRoomView = viewRoom.frame;
    [viewRoom setFrame:CGRectMake(0, 0, 320, fRoomView.size.height)];
    
    //    CGRect ftbv = tbvLinen.frame;
    CGRect fscv = scrollView.frame;
    [scrollView setFrame:CGRectMake(0, fRoomView.size.height, 320, fscv.size.height + f.size.height)];
    
    //Edit position in view
    [self modifyAllViews];
}

/**
 * Name : shouldChangeCharactersInRange
 * Description : For hiding the keyboard. This must be implemented if you are working with UITextField
 * Parameter :
 * Return : BOOL
 */
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if([string isEqualToString:@"\n"])
    {
        [textField resignFirstResponder];
        return NO;
    }
    return YES;
}


/**
 * Name : textFieldShouldReturn
 * Description : Hiding the keyboard of UITextField
 * Parameter :
 * Return : BOOL
 */
- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    if([LogFileManager isLogConsole])
    {
        NSInteger userId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
        NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSLog(@"<%@><Linen><UserId:%d><Input RoomId:%@>", time, (int)userId, textField.text);
    }
    //[btnSubmit setEnabled:[self isValidQty]];
    [textField resignFirstResponder];
    if ([[UserManagerV2 sharedUserManager].currentUserAccessRight isLoadLinenByRoomType] && ![txtRoom.text isEqualToString:@""]) {
        [self performSelector:@selector(loadingDatas) withObject:nil];
    }
    [self setEnableBtnAtionHistory:YES];
    return YES;
}

- (NSMutableString*) aggregateData {
    
    NSMutableString *confirmData = [NSMutableString string];
    
    NSString *sLinenPostingConfirmationMessageDetailsFormat; // CFG [20161028/CRF-00001614]
    NSMutableString *sLinenPostingConfirmationMessageDetails; // CFG [20161028/CRF-00001614]
    
    sLinenPostingConfirmationMessageDetailsFormat = [[UserManagerV2 sharedUserManager].currentUserAccessRight getConfigByKey:USER_CONFIG_LINEN_POSTING_CONFIRMATION_MESSAGE_DETAILS_FORMAT]; // CFG [20161028/CRF-00001614]
    
    // CFG [20161028/CRF-00001614] - If empty, assign a default value.
    if ([sLinenPostingConfirmationMessageDetailsFormat isEqualToString:@""]) {
        sLinenPostingConfirmationMessageDetailsFormat = @"[UsedQty] - [ItemName]";
    }
    // CFG [20161028/CRF-00001614] - End.
    
    for(NSInteger i = 0; i<[sectionDatas count]; i++) {
        
        LinenSectionInfoV2 *sectioninfo = [sectionDatas objectAtIndex:i];
        for (NSInteger j = 0; j < [sectioninfo.rowHeights count]; j++) {
            
            sLinenPostingConfirmationMessageDetails = [sLinenPostingConfirmationMessageDetailsFormat mutableCopy];  // CFG [20161028/CRF-00001614]
            
            NSIndexPath *index = [NSIndexPath indexPathForRow:j inSection:i];
            LinenItemCellV2 *linenItem = (LinenItemCellV2 *)[self tableView:tbvLinen cellForRowAtIndexPath:index];
            NSString *itemUseStr = [NSString stringWithFormat:@"%@", [linenItem.btnUsed titleForState:UIControlStateNormal]];
            NSString *sNewQuantity = [NSString stringWithFormat:@"%@", [linenItem.btnNew titleForState:UIControlStateNormal]]; // CFG [20161028/CRF-00001614]
            
            // CFG [20170308/CRF-00001614] - Check if either one is not empty and add to the response message.
            if (([itemUseStr integerValue] != 0) || ([sNewQuantity integerValue] != 0))
            {
                if ([itemUseStr integerValue] >= 0) {
                    // [confirmData appendString:[NSString stringWithFormat:@"\n%@ - %@", itemUseStr, [linenItem.lblTitleItem text]]]; // CFG [20161028/CRF-00001614]
                    
                    sLinenPostingConfirmationMessageDetails = [[sLinenPostingConfirmationMessageDetails stringByReplacingOccurrencesOfString:@"[UsedQty]" withString:itemUseStr] mutableCopy]; // CFG [20161028/CRF-00001614]
                }
                
                // CFG [20161028/CRF-00001614] - New quantity.
                if ([sNewQuantity integerValue] >= 0) {
                    sLinenPostingConfirmationMessageDetails = [[sLinenPostingConfirmationMessageDetails stringByReplacingOccurrencesOfString:@"[NewQty]" withString:sNewQuantity] mutableCopy]; // CFG [20161028/CRF-00001614]
                }
                
                if (sLinenPostingConfirmationMessageDetails && sLinenPostingConfirmationMessageDetailsFormat && ![sLinenPostingConfirmationMessageDetails isEqualToString:sLinenPostingConfirmationMessageDetailsFormat]) {
                    sLinenPostingConfirmationMessageDetails = [[sLinenPostingConfirmationMessageDetails stringByReplacingOccurrencesOfString:@"[ItemName]" withString:[linenItem.lblTitleItem text]] mutableCopy];
                    
                    [confirmData appendString:@"\n"];
                    [confirmData appendString:sLinenPostingConfirmationMessageDetails];
                }
                // CFG [20161028/CRF-00001614] - End.
            }
            // CFG [20170308/CRF-00001614] - End.
        }
        
    }
    return confirmData;
}

// DungPhan - 20150924: CRF-00001277 > [Marriott] Confirmation Message before Submit Posting on Minibar / Engineering / Amenities / Lost & Found / Linen
- (void)alertView:(DTAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (buttonIndex)
    {
        case 0:
        {
            //YES
            //-------------Log----------
            if([LogFileManager isLogConsole]) {
                NSInteger userId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
                NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
                if (logCheckNewValue == 0 && logCheckUsedValue == 0) {
                    if([LogFileManager isLogConsole])
                    {
                        NSLog(@"<%@><Linen><UserId:%d><Summit to post><input RoomId:%@>", time, (int)userId, txtRoom.text);
                    }
                }
                else {
                    NSInteger totalSections = [self numberOfSectionsInTableView:tbvLinen];
                    
                    for (int i = 0; i<totalSections; i++) {
                        NSInteger totalRow = [self tableView:self.tbvLinen numberOfRowsInSection:i];
                        for (int j = 0; j<totalRow; j++) {
                            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:j inSection:i];
                            UITableViewCell *cell = [self.tbvLinen cellForRowAtIndexPath:indexPath];
                            UILabel *lblItemName = (UILabel *)[cell viewWithTag:tagItemName];
                            UIButton *btnUsed = (UIButton *)[cell viewWithTag:tagUsedButton];
                            
                            UIButton *btnNew = (UIButton *)[cell viewWithTag:tagNewButton];
                            NSString *valueBtnUsed = btnUsed.titleLabel.text;
                            NSString *valueBtnNew = btnNew.titleLabel.text;
                            if ([valueBtnNew isEqualToString:@"0"] && [valueBtnUsed isEqualToString:@"0"]) {
                                
                            }
                            else {
                                if([LogFileManager isLogConsole])
                                {
                                    NSLog(@"<%@><Minibar><UserId:%d><Submit to post><Input RoomId:%@><ItemName:%@><ItemUsed:%@><ItemNew:%@>", time, (int)userId, txtRoom.text, lblItemName.text, valueBtnUsed, valueBtnNew);
                                }
                            }
                        }
                    }
                }
            }
            //-------------Log----------
            statusSave = YES;
            statusChooseLinen = YES;
            statusAddToCart = YES;
            
            [self handleBtnAddToCartPressed];
            int responseCode = [self saveData];
            
            if (isFromMainPosting) {
                [txtRoom setText:@""];
                [tbvLinen reloadData];
                [datasLinenCart removeAllObjects];
                
                //[self.navigationController popViewControllerAnimated:YES];
            } else {
                //response status different to RESPONSE_STATUS_OK will show alert view in saveMinibarCart
                if (responseCode == RESPONSE_STATUS_OK) {
                    NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
                    for (UIViewController *aViewController in allViewControllers) {
                        if ([aViewController isKindOfClass:[RoomAssignmentInfoViewController class]]) {
                            ((RoomAssignmentInfoViewController*) aViewController).isAlreadyPostedLinen = YES;
//                            [self.navigationController popToViewController:aViewController animated:YES];
                            [self.navigationController popViewControllerAnimated:YES];
                        }
                    }
                }
            }
        }
            break;
            
        case 1:
        {
            if([LogFileManager isLogConsole])
            {
                //NO: cart screen
                //[self btnAddToCartPressed:nil];
                //[self cartButtonPressed];
                NSInteger userId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
                NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
                if (logCheckNewValue == 0 && logCheckUsedValue == 0) {
                    if([LogFileManager isLogConsole]){
                        NSLog(@"<%@><Linen><UserId:%d><Not confirm to post><input RoomId:%@>", time, (int)userId, txtRoom.text);
                    }
                }
                else {
                    NSInteger totalSections = [self numberOfSectionsInTableView:tbvLinen];
                    
                    for (int i = 0; i<totalSections; i++) {
                        NSInteger totalRow = [self tableView:self.tbvLinen numberOfRowsInSection:i];
                        for (int j = 0; j<totalRow; j++) {
                            
                            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:j inSection:i];
                            UITableViewCell *cell = [self.tbvLinen cellForRowAtIndexPath:indexPath];
                            
                            UILabel *lblItemName = (UILabel *)[cell viewWithTag:tagItemName];
                            UIButton *btnUsed = (UIButton *)[cell viewWithTag:tagUsedButton];
                            UIButton *btnNew = (UIButton *)[cell viewWithTag:tagNewButton];
                            
                            NSString *valueBtnUsed = btnUsed.titleLabel.text;
                            NSString *valueBtnNew = btnNew.titleLabel.text;
                            if ([valueBtnNew isEqualToString:@"0"] && [valueBtnUsed isEqualToString:@"0"]) {
                                
                            }
                            else {
                                NSLog(@"<%@><Linen><UserId:%d><Not confirm to post><Input RoomId:%@><ItemName:%@><ItemUsed:%@><ItemNew:%@>", time, (int)userId, txtRoom.text, lblItemName.text, valueBtnUsed, valueBtnNew);
                            }
                        }
                    }
                }
            }
            
        }
            break;
            
        default:
            break;
    }
}

-(IBAction) btnSubmit_Clicked:(id)sender
{
    NSString *roomNo = [txtRoom text];
    if(roomNo.length > 0 && [self isValidQty])
    {
        RoomAssignmentModelV2 *roomAssignment = [[RoomAssignmentModelV2 alloc] init];
        roomAssignment.roomAssignment_RoomId = roomNo;
        roomAssignment.roomAssignment_UserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
        BOOL result = [[RoomManagerV2 sharedRoomManager] getRoomAssignmentByUserIDAndRoomID:roomAssignment];
        if([[UserManagerV2 sharedUserManager].currentUserAccessRight isValidateRoom] && !isDemoMode) {
            NSString *hotelId = [NSString stringWithFormat:@"%d", [[[UserManagerV2 sharedUserManager] currentUser] userHotelsId]];
            result = [[RoomManagerV2 sharedRoomManager] isValidRoomWSByUserId:[UserManagerV2 sharedUserManager].currentUser.userId roomNumber:roomNo hotelId:hotelId];
        } else {
            result = TRUE; //Force posting mini bar without checking validation room
        }
        //Hao Tran - Set always allow submit data in Main Posting
        //        if(ENABLE_POST_LINEN_WITH_ROOM_NUMBER){
        //            if(isFromMainPosting){
        //                result = true;
        //            }
        //        }
        
        if(result)
        {
            [RoomManagerV2 setCurrentRoomNo:roomNo];
            [TasksManagerV2 setCurrentRoomAssignment:roomAssignment.roomAssignment_Id];
            //NSString *mess = [NSString stringWithFormat:@"Room %@ \n Submit Linen Items?", roomNo];
            //            NSString *mess = [NSString stringWithFormat:@"%@ %@ \n %@%@", [L_room currentKeyToLanguage],roomNo,[L_submit_linen_items currentKeyToLanguage],@"?"];
            //NSString *mess = [NSString stringWithFormat:@"%@ %@ \n %@%@", [L_room currentKeyToLanguage],roomNo,[L_submit_minibar_items currentKeyToLanguage],@"?"];
            NSString *mess = [NSString stringWithFormat:@"%@ %@ \n %@%@ \n %@", [L_room currentKeyToLanguage],roomNo,[L_submit_linen_items currentKeyToLanguage],@"?", [self aggregateData]];
            DTAlertView *alertView = [DTAlertView alertViewWithTitle:[L_confirm_title currentKeyToLanguage] message:mess delegate:self cancelButtonTitle:[L_YES currentKeyToLanguage] positiveButtonTitle:[L_NO currentKeyToLanguage]];
            [alertView show];
        }
        else
        {
            NSString *message = nil;
            if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                message = [NSString stringWithFormat:@"%@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOM_INVALID]];
            } else {
                message = [NSString stringWithFormat:@"\n\n\n\n\n%@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOM_INVALID]];
            }
            
            UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObject:[UIImage imageNamed:@"yes_bt.png"]] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:message delegate:nil cancelButtonTitle:[L_CLOSE currentKeyToLanguage] otherButtonTitles: nil];
            
            if(!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                UIImageView *alertImage = [[UIImageView alloc] initWithFrame:CGRectMake(80, 35, 120, 120)];
                [alertImage setImage:[UIImage imageNamed:@"icon_wrong room.png"]];
                [alert addSubview:alertImage];
            }
            [alert show];
            
            //-------------Log----------
            if([LogFileManager isLogConsole])
            {
                NSInteger userId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
                NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
                if (logCheckNewValue == 0 && logCheckUsedValue == 0) {
                    NSLog(@"<%@><Linen><UserId:%d><Invalid room to post><input RoomId:%@>", time, (int)userId, txtRoom.text);
                }
                else {
                    NSInteger totalSections = [self numberOfSectionsInTableView:tbvLinen];
                    
                    for (int i = 0; i<totalSections; i++) {
                        NSInteger totalRow = [self tableView:self.tbvLinen numberOfRowsInSection:i];
                        for (int j = 0; j<totalRow; j++) {
                            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:j inSection:i];
                            UITableViewCell *cell = [self.tbvLinen cellForRowAtIndexPath:indexPath];
                            UILabel *lblItemName = (UILabel *)[cell viewWithTag:tagItemName];
                            UIButton *btnUsed = (UIButton *)[cell viewWithTag:tagUsedButton];
                            
                            UIButton *btnNew = (UIButton *)[cell viewWithTag:tagNewButton];
                            NSString *valueBtnUsed = btnUsed.titleLabel.text;
                            NSString *valueBtnNew = btnNew.titleLabel.text;
                            if ([valueBtnNew isEqualToString:@"0"] && [valueBtnUsed isEqualToString:@"0"]) {
                                
                            }
                            else{
                                NSLog(@"<%@><Linen><UserId:%d><Invalid room to post><Input RoomId:%@><ItemName:%@><ItemUsed:%@><ItemNew:%@>", time, (int)userId, txtRoom.text, lblItemName.text, valueBtnUsed, valueBtnNew);
                            }
                        }
                    }
                }
            }
            //-------------Log----------
            
        }
        
    } else {
        if(roomNo.length > 0){
            NSString *message = nil;
            if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                message = [NSString stringWithFormat:@"%@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_INVALID_DATA]];
            } else {
                message = [NSString stringWithFormat:@"\n\n\n\n\n%@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_INVALID_DATA]];
            }
            
            UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObject:[UIImage imageNamed:@"yes_bt.png"]] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:message delegate:nil cancelButtonTitle:[L_CLOSE currentKeyToLanguage] otherButtonTitles: nil];
            
            if(!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                UIImageView *alertImage = [[UIImageView alloc] initWithFrame:CGRectMake(80, 35, 120, 120)];
                [alertImage setImage:[UIImage imageNamed:@"icon_wrong room.png"]];
                [alert addSubview:alertImage];
            }
            [alert show];
        } else {
            NSString *message = nil;
            if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                message = [NSString stringWithFormat:@"%@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOM_INVALID]];
            } else {
                message = [NSString stringWithFormat:@"\n\n\n\n\n%@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOM_INVALID]];
            }
            
            UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObject:[UIImage imageNamed:@"yes_bt.png"]] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:message delegate:nil cancelButtonTitle:[L_CLOSE currentKeyToLanguage] otherButtonTitles: nil];
            
            if(!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                UIImageView *alertImage = [[UIImageView alloc] initWithFrame:CGRectMake(80, 35, 120, 120)];
                [alertImage setImage:[UIImage imageNamed:@"icon_wrong room.png"]];
                [alert addSubview:alertImage];
            }
            [alert show];
            //-------------Log----------
            if([LogFileManager isLogConsole])
            {
                NSInteger userId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
                NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
                if (logCheckNewValue == 0 && logCheckUsedValue == 0) {
                    NSLog(@"<%@><Linen><UserId:%d><Invalid room to post><input RoomId:%@>", time, (int)userId, txtRoom.text);
                }
                else {
                    NSInteger totalSections = [self numberOfSectionsInTableView:tbvLinen];
                    
                    for (int i = 0; i<totalSections; i++) {
                        NSInteger totalRow = [self tableView:self.tbvLinen numberOfRowsInSection:i];
                        for (int j = 0; j<totalRow; j++) {
                            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:j inSection:i];
                            UITableViewCell *cell = [self.tbvLinen cellForRowAtIndexPath:indexPath];
                            UILabel *lblItemName = (UILabel *)[cell viewWithTag:tagItemName];
                            UIButton *btnUsed = (UIButton *)[cell viewWithTag:tagUsedButton];
                            
                            UIButton *btnNew = (UIButton *)[cell viewWithTag:tagNewButton];
                            NSString *valueBtnUsed = btnUsed.titleLabel.text;
                            NSString *valueBtnNew = btnNew.titleLabel.text;
                            if ([valueBtnNew isEqualToString:@"0"] && [valueBtnUsed isEqualToString:@"0"]) {
                                
                            }
                            else{
                                NSLog(@"<%@><Linen><UserId:%d><Invalid room to post><Input RoomId:%@><ItemName:%@><ItemUsed:%@><ItemNew:%@>", time, (int)userId, txtRoom.text, lblItemName.text, valueBtnUsed, valueBtnNew);
                            }
                        }
                    }
                }
            }
            //-------------Log----------
        }
    }
    
}

#pragma mark - Access Right
-(void) loadAccessRights
{
    AccessRightModel *currentUserAccessRight = [[UserManagerV2 sharedUserManager] currentUserAccessRight];
    QRCodeScanner = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.QRCodeScanner];
    postingLinen = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.postLinen];
    actionLinen = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.actionLinen];
    if (![UserManagerV2 sharedUserManager].currentUserAccessRight.isShowActionHistoryPosting) {
        btnPostingHistory.hidden = YES;
    }
}

#pragma mark - QR Code Button Touched
-(IBAction)btnQRCodeScannerTouched:(id) sender
{
    ZXingWidgetController *widController = [[ZXingWidgetController alloc] initWithDelegate:self showCancel:YES OneDMode:NO showLicense:NO];
    QRCodeReader* qrcodeReader = [[QRCodeReader alloc] init];
    NSSet *readers = [[NSSet alloc ] initWithObjects:qrcodeReader,nil];
    widController.readers = readers;
    
    [self presentViewController:widController animated:YES completion:nil];
}

#pragma mark - ZxingDelegate
- (void)zxingController:(ZXingWidgetController*)controller didScanResult:(NSString *)resultString {
    resultString = [STEncryptorDES convertText:resultString];
    [self dismissViewControllerAnimated:YES completion:nil];
    
    txtRoom.text = resultString;
    [self textFieldShouldReturn:txtRoom];
}

- (void)zxingControllerDidCancel:(ZXingWidgetController*)controller {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - iOS 7 Resources
-(void)loadFlatResource
{
    [imgHeader setImage:[UIImage imageNamed:imgGuestInfoActiveFlat]];
    [btnSubmit setImage:[UIImage imageNamed:imgBtnSubmitFlat] forState:UIControlStateNormal];
    [btnSubmit setImage:[UIImage imageNamed:imgBtnSubmitClickedFlat] forState:UIControlStateHighlighted];
    
    [lblSubmit setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_SUBMIT]];
    [btnPostingHistory setBackgroundImage:[UIImage imageNamed:imgHistoryPostingBtnFlat] forState:UIControlStateHighlighted];
    [btnPostingHistory setBackgroundImage:[UIImage imageNamed:imgHistoryPostingBtnFlat] forState:UIControlStateNormal];
}
-(void) setEnableBtnAtionHistory:(BOOL) isEnable
{
    if(![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]){
        isEnable = NO;
    }
    [btnPostingHistory setUserInteractionEnabled:isEnable];
    [btnPostingHistory setBackgroundImage:[UIImage imageBeforeiOS7:(isEnable?imgCleaningStatusBtnAndActionBtn:imgCleaningStatusBtnAndActionBtnDisable)
                                                         equaliOS7:(isEnable?imgCleaningStatusBtnAndActionBtnFlat:imgCleaningStatusBtnAndActionBtnDisableFlat)]
                                 forState:UIControlStateNormal];
    [btnPostingHistory setBackgroundImage:[UIImage imageBeforeiOS7:(isEnable?imgCleaningStatusBtnAndActionBtnClicked:imgCleaningStatusBtnAndActionBtnDisable)
                                                         equaliOS7:(isEnable?imgCleaningStatusBtnAndActionBtnClickedFlat:imgCleaningStatusBtnAndActionBtnDisableFlat)]
                                 forState:UIControlEventTouchDown];
    [btnPostingHistory setTitle:[L_action_history currentKeyToLanguage] forState:UIControlStateNormal];
}


@end

