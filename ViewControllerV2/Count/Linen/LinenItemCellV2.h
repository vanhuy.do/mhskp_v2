//
//  LinenItemCellV2.h
//  mHouseKeeping
//
//  Created by TMS on 2/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ItemCellV2Delegate <NSObject>

@optional
-(void) btnUsedItemCellPressedWithIndexPath:(NSIndexPath *) path sender:(UIButton *)button;
-(void) btnNewItemCellPressedWithIndexPath:(NSIndexPath *) path sender:(UIButton *)button;

@end

@interface LinenItemCellV2 : UITableViewCell {
    __unsafe_unretained id<ItemCellV2Delegate> delegate;
    NSIndexPath *path;
}

@property (nonatomic, strong) NSIndexPath *path; 
@property (nonatomic, assign) id<ItemCellV2Delegate> delegate;
@property (strong, nonatomic) IBOutlet UIImageView *imgItem;
@property (strong, nonatomic) IBOutlet UILabel *lblTitleItem;
@property (strong, nonatomic) IBOutlet UILabel *lblUsed;
@property (strong, nonatomic) IBOutlet UILabel *lblNew;
@property (strong, nonatomic) IBOutlet UIButton *btnUsed;
@property (strong, nonatomic) IBOutlet UIButton *btnNew;
- (IBAction)btnUsedPressed:(id)sender;
- (IBAction)btnNewPressed:(id)sender;
@end
