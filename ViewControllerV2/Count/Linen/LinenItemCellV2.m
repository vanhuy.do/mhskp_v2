//
//  LinenItemCellV2.m
//  mHouseKeeping
//
//  Created by TMS on 2/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LinenItemCellV2.h"

@implementation LinenItemCellV2
@synthesize imgItem;
@synthesize lblTitleItem;
@synthesize lblUsed;
@synthesize lblNew;
@synthesize btnUsed;
@synthesize btnNew;
@synthesize path;
@synthesize delegate;

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc {
//    [imgItem release];
//    [lblTitleItem release];
//    [lblUsed release];
//    [lblNew release];
//    [btnUsed release];
//    [btnNew release];
//    [super dealloc];
}
- (IBAction)btnUsedPressed:(id)sender {
    if ([delegate respondsToSelector:@selector(btnUsedItemCellPressedWithIndexPath:sender:)]) {
        [delegate btnUsedItemCellPressedWithIndexPath:path sender:sender];
    }
}

- (IBAction)btnNewPressed:(id)sender {
    if ([delegate respondsToSelector:@selector(btnNewItemCellPressedWithIndexPath:sender:)]) {
        [delegate btnNewItemCellPressedWithIndexPath:path sender:sender];
    }
}
@end
