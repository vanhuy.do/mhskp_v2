//
//  LinenSectionInfoV2.h
//  mHouseKeeping
//
//  Created by TMS on 2/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LinenSectionViewV2.h"
#import "LinenCategoryModelV2.h"

@interface LinenSectionInfoV2 : NSObject

@property (nonatomic, strong) LinenCategoryModelV2 *linenCategory;
@property (assign) BOOL open;
@property (strong) LinenSectionViewV2* headerView;

@property (nonatomic,strong,readonly) NSMutableArray *rowHeights;

- (NSUInteger)countOfRowHeights;
- (id)objectInRowHeightsAtIndex:(NSUInteger)idx;
- (void)insertObject:(id)anObject inRowHeightsAtIndex:(NSUInteger)idx;
- (void)removeObjectFromRowHeightsAtIndex:(NSUInteger)idx;
- (void)replaceObjectInRowHeightsAtIndex:(NSUInteger)idx withObject:(id)anObject;
- (void)getRowHeights:(id __unsafe_unretained [])buffer range:(NSRange)inRange;
- (void)insertRowHeights:(NSArray *)rowHeightArray atIndexes:(NSIndexSet *)indexes;
- (void)removeRowHeightsAtIndexes:(NSIndexSet *)indexes;
- (void)replaceRowHeightsAtIndexes:(NSIndexSet *)indexes withRowHeights:(NSArray *)rowHeightArray;
-(void) insertObjectToNextIndex:(id)anObject;
@end
