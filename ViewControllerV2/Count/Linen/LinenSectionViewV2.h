//
//  LinenSectionViewV2.h
//  mHouseKeeping
//
//  Created by TMS on 2/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LinenCategoryModelV2.h"

@class LinenSectionViewV2;

@protocol LinenSectionViewV2Delegate <NSObject>

@optional
-(void) sectionHeaderView:(LinenSectionViewV2 *) sectionheaderView sectionOpened:(NSInteger) section;
-(void) sectionHeaderView:(LinenSectionViewV2 *) sectionheaderView sectionClosed:(NSInteger) section;

@end

@interface LinenSectionViewV2 : UIView {
    __unsafe_unretained id<LinenSectionViewV2Delegate> delegate;
    NSInteger section;
    BOOL isToggle;
}

@property (nonatomic, assign) NSInteger section;
@property (nonatomic, assign) id<LinenSectionViewV2Delegate> delegate;
@property (strong, nonatomic) IBOutlet UIImageView *imgLinenSection;
@property (strong, nonatomic) IBOutlet UILabel *lblLinenTitle;
@property (strong, nonatomic) IBOutlet UIImageView *imgDetail;
- (IBAction)btnLinenSectionViewV2Pressed:(id)sender;
-(id) initWithSection:(NSInteger) section LinenCategoryModelV2:(LinenCategoryModelV2 *) categoryModel andOpenStatus:(BOOL) isOpen;
@end
