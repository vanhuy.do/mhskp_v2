//
//  LinenSectionViewV2.m
//  mHouseKeeping
//
//  Created by TMS on 2/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LinenSectionViewV2.h"
#import "UIImage+Tint.h"
#import "ehkDefinesV2.h"

@implementation LinenSectionViewV2
@synthesize imgLinenSection;
@synthesize lblLinenTitle;
@synthesize imgDetail;
@synthesize section;
@synthesize delegate;

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void) initControls {
    [self setFrame:CGRectMake(0, 0, 320, 60)];
    
    UIImageView *bg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 60)];
    [bg setImage:[UIImage imageBeforeiOS7:imgBgBigBtn equaliOS7:imgBgBigBtnFlat]];
    [self addSubview:bg];
    
    imgLinenSection = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 70, 60)];
    [imgLinenSection setContentMode:UIViewContentModeScaleAspectFit];
    [self addSubview:imgLinenSection];
    
    lblLinenTitle = [[UILabel alloc] initWithFrame:CGRectMake(70, 5, 220, 50)];
    [lblLinenTitle setFont:[UIFont fontWithName:@"Arial-BoldMT" size:17]];
    [lblLinenTitle setBackgroundColor:[UIColor clearColor]];
    [lblLinenTitle setTextColor:[UIColor colorWithRed:55.0/255.0 green:55.0/255.0 blue:55.0/255.0 alpha:1.0]];
    [lblLinenTitle setTextAlignment:NSTextAlignmentCenter];
    [self addSubview:lblLinenTitle];
    
    imgDetail = [[UIImageView alloc] initWithFrame:CGRectMake(285, 15, 30, 30)];
    [imgDetail setImage:[UIImage imageBeforeiOS7:imgRightRow equaliOS7:imgRightRowFlat]];
    [self addSubview:imgDetail];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(0, 0, 320, 70)];
    [button addTarget:self action:@selector(btnLinenSectionViewV2Pressed:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:button];
}

-(id)init {
    self = [super init];
    if (self) {
        [self initControls];
    }
    return self;
}

-(id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initControls];
    }
    return self;
}

- (void)dealloc {
//    [imgLinenSection release];
//    [lblLinenTitle release];
//    [imgDetail release];
//    [super dealloc];
}

-(void) toggleOpenWithUserAction:(BOOL) userAction {
    if (userAction) {
        if (isToggle == NO) {
            if ([delegate respondsToSelector:@selector(sectionHeaderView:sectionOpened:)]) {
                isToggle = YES;
                [delegate sectionHeaderView:self sectionOpened:section];
                
                [imgDetail setImage:[UIImage imageBeforeiOS7:imgRightRowOpen equaliOS7:imgRightRowOpenFlat]];
            }
        }else {
            if ([delegate respondsToSelector:@selector(sectionHeaderView:sectionClosed:)]) {
                isToggle = NO;
                [imgDetail setImage:[UIImage imageBeforeiOS7:imgRightRow equaliOS7:imgRightRowFlat]];
                [delegate sectionHeaderView:self sectionClosed:section];
            }
        }
    }
}

- (IBAction)btnLinenSectionViewV2Pressed:(id)sender {
    [self toggleOpenWithUserAction:YES];
}

-(id)initWithSection:(NSInteger )sectionIndex LinenCategoryModelV2:(LinenCategoryModelV2 *)categoryModel andOpenStatus:(BOOL)isOpen {
    self = [self init];
    
    if (self) {
        section = sectionIndex;
        lblLinenTitle.text = categoryModel.linenCategoryTitle;
        isToggle = isOpen;
        [imgLinenSection setImage:[UIImage imageWithData:categoryModel.linenCategoryImage]];
        if (isOpen) {
            [imgDetail setImage:[UIImage imageBeforeiOS7:imgRightRowOpen equaliOS7:imgRightRowOpenFlat]];
        } else {
            [imgDetail setImage:[UIImage imageBeforeiOS7:imgRightRow equaliOS7:imgRightRowFlat]];
        }
    }
    
    return self;
}
@end
