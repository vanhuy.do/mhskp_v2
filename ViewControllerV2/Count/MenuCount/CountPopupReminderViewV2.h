//
//  CountPopupReminderViewV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 3/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "LaudryOrderModelV2.h"
#import "CountOrdersModelV2.h"
#import "AmenitiesOrdersModelV2.h"
#import "CountServiceModelV2.h"
#import "CountViewV2.h"
#import "AccessRight.h"

@protocol CountPopupReminderViewV2Delegate <NSObject>

@optional
-(void) yesCountReminder;
-(void) noCountReminder;

@end

@interface CountPopupReminderViewV2 : UIView{
    
    __unsafe_unretained id<CountPopupReminderViewV2Delegate> delegate;
    //LaudryOrderModelV2 *laundryOrder;
    CountOrdersModelV2 *countOrder;
    AmenitiesOrdersModelV2 *amenitiesOrder;
    
    AccessRight *actionLostAndFound;
    AccessRight *actionEngineering;
    AccessRight *actionLaundry;
    AccessRight *actionMinibar;
    AccessRight *actionLinen;
    AccessRight *actionAmenities;
}

@property (assign, nonatomic) NSInteger roomId;
@property (assign, nonatomic) id<CountPopupReminderViewV2Delegate> delegate;
@property (strong, nonatomic)  UIImageView *imgBgAlert;
@property (strong, nonatomic)  UILabel *lblTitleAlert;
@property (strong, nonatomic)  UILabel *lblMessageAlert;
@property (strong, nonatomic)  UILabel *lblLaundry;
@property (strong, nonatomic)  UILabel *lblMinibar;
@property (strong, nonatomic)  UILabel *lblLinen;
@property (strong, nonatomic)  UILabel *lblAmenities;
@property (strong, nonatomic)  UIImageView *imgChkLaundry;
@property (strong, nonatomic)  UIImageView *imgChkMinibar;
@property (strong, nonatomic)  UIImageView *imgChkLinen;
@property (strong, nonatomic)  UIImageView *imgChkAmenities;
@property (strong, nonatomic)  UIButton *btnYesAlert;
@property (strong, nonatomic)  UIButton *btnNoAlert;
//@property (strong, nonatomic) LaudryOrderModelV2 *laundryOrder;
@property (strong, nonatomic) CountOrdersModelV2 *countOrder;
@property (strong, nonatomic) AmenitiesOrdersModelV2 *amenitiesOrder;
@property (nonatomic, assign) BOOL isAlreadyPostedLinen;
@property (nonatomic, assign) BOOL isAlreadyPostedMiniBar;
@property (nonatomic, assign) BOOL isAlreadyPostedAmenities;

-(id)initWithRoomId:(NSInteger) roomId;

-(BOOL) show;

-(void) hide;

-(BOOL) checkCountStatuses;

@end
