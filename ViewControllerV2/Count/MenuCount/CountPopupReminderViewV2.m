//
//  CountPopupReminderViewV2.m
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 3/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CountPopupReminderViewV2.h"
#import "CountManagerV2.h"
#import "UserManagerV2.h"
#import "ehkConvert.h"
#import "AmenitiesManagerV2.h"
#import "RoomManagerV2.h"
#import "TasksManagerV2.h"
#import "AccessRight.h"
#import "DeviceManager.h"
#import "ehkDefinesV2.h"
#import "UIImage+Tint.h"

@interface CountPopupReminderViewV2 (private) 

-(void) btnYesAlertPressed;
-(void) btnNoAlertPressed;

@end

@implementation CountPopupReminderViewV2
@synthesize roomId;
@synthesize delegate;
@synthesize imgBgAlert, imgChkLinen, imgChkLaundry, imgChkMinibar, imgChkAmenities, lblLinen, lblLaundry, lblMinibar, lblAmenities, lblTitleAlert, lblMessageAlert, btnNoAlert, btnYesAlert;
@synthesize /*laundryOrder,*/ countOrder, amenitiesOrder;
@synthesize isAlreadyPostedLinen, isAlreadyPostedMiniBar, isAlreadyPostedAmenities;

int nRoomAssignmentMandatoryMinibarPosting = 0, nRoomAssignmentMandatoryLinenPosting = 0, nRoomAssignmentMandatoryAmenitiesPosting = 0;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(id)init
{
    self = [super init];
    if (self) {
        //init view
        if([DeviceManager getDeviceScreenKind] == DeviceScreenKindRetina4_0){
            [self setFrame:CGRectMake(0, 0, 320, DeviceScreenSizeStandardHeight4_0)];
        } else {
            [self setFrame:CGRectMake(0, 0, 320, DeviceScreenSizeStandardHeight3_5)];
        }
        
    }
    return self;
}

-(id)initWithRoomId:(NSInteger)roomId1 {
    self = [super init];   
    
    //load access right
    [self loadAccessRights];
    
    //init view
    if([DeviceManager getDeviceScreenKind] == DeviceScreenKindRetina4_0){
         [self setFrame:CGRectMake(0, 0, 320, DeviceScreenSizeStandardHeight4_0)];
    } else {
        [self setFrame:CGRectMake(0, 0, 320, DeviceScreenSizeStandardHeight3_5)];
    }
    
    [self setBackgroundColor:[UIColor colorWithWhite:0.3 alpha:0.4]];
    imgBgAlert = [[UIImageView alloc] initWithFrame:CGRectMake(14, 135, 290, 220)];
    [imgBgAlert setImage:[UIImage imageBeforeiOS7:imgBGAlert equaliOS7:imgBGAlertFlat]];
    [self addSubview:imgBgAlert];
    
    lblTitleAlert = [[UILabel alloc] initWithFrame:CGRectMake(60, 146, 200, 21)];
    [lblTitleAlert setTextAlignment:NSTextAlignmentCenter];
    [lblTitleAlert setFont:[UIFont fontWithName:@"Arial-BoldMT" size:17]];
    [lblTitleAlert setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert]];
    [lblTitleAlert setBackgroundColor:[UIColor clearColor]];
    [lblTitleAlert setTextColor:[UIColor colorWithRed:255.0/255 green:255.0/255 blue:255.0/255 alpha:1]];
    [self addSubview:lblTitleAlert];
    
    lblMessageAlert = [[UILabel alloc] initWithFrame:CGRectMake(43, 176, 214, 54)];
    [lblMessageAlert setFont:[UIFont fontWithName:@"Arial" size:17]];
    [lblMessageAlert setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_please_confirm_there_are_no_more_count_update]];
    [lblMessageAlert setTextAlignment:NSTextAlignmentCenter];
    [lblMessageAlert setBackgroundColor:[UIColor clearColor]];
    [lblMessageAlert setTextColor:[UIColor colorWithRed:255.0/255 green:255.0/255 blue:255.0/255 alpha:1]];
    [lblMessageAlert setNumberOfLines:2];
    [self addSubview:lblMessageAlert];
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        [lblTitleAlert setTextColor:[UIColor blackColor]];
        [lblMessageAlert setTextColor:[UIColor blackColor]];
    }
    
    CGRect frame = CGRectMake(91, 122, 77, 21);
    CGRect fCheck = CGRectMake(194, 116, 32, 27);
    
    frame.origin.y = frame.origin.y + 118;
    fCheck.origin.y = fCheck.origin.y + 118;
    /*
    //Laundry
    lblLaundry = [[UILabel alloc] initWithFrame:CGRectMake(91, frame.origin.y + 38, 77, 21)];
    [lblLaundry setFont:[UIFont fontWithName:@"Arial" size:17]];
    [lblLaundry setText:[[LanguageManagerV2 sharedLanguageManager] getLaundry]];
    [lblLaundry setBackgroundColor:[UIColor clearColor]];
    [lblLaundry setTextColor:[UIColor colorWithRed:0.0/255 green:204.0/255 blue:255.0/255 alpha:1]];
    [self addSubview:lblLaundry];
    
    imgChkLaundry = [[UIImageView alloc] initWithFrame:CGRectMake(194, fCheck.origin.y + 38, 32, 27)];
    [imgChkLaundry setImage:[UIImage imageNamed:@"yes_icon2.png"]];
    [imgChkLaundry setHidden:YES];
    [self addSubview:imgChkLaundry];
    if (ENABLE_LAUNDRY == 0) {
        [lblLaundry setHidden:YES];
    } else {
        frame = lblLaundry.frame;
        fCheck = imgChkLaundry.frame;
    }
     */
    
    // CFG [20160928/CRF-00001612] - Variable declarations.
    @try {
        //[[UserManagerV2 sharedUserManager].currentUserAccessRight getRoomAssignmentSpecialAssignmentMethod];
        nRoomAssignmentMandatoryMinibarPosting = (int) [[UserManagerV2 sharedUserManager].currentUserAccessRight getConfigByKey:USER_CONFIG_ROOM_ASSIGNMENT_MANDATORY_MINIBAR_POSTING].integerValue;
        nRoomAssignmentMandatoryLinenPosting = (int) [[UserManagerV2 sharedUserManager].currentUserAccessRight getConfigByKey:USER_CONFIG_ROOM_ASSIGNMENT_MANDATORY_LINEN_POSTING].integerValue;
        nRoomAssignmentMandatoryAmenitiesPosting = (int) [[UserManagerV2 sharedUserManager].currentUserAccessRight getConfigByKey:USER_CONFIG_ROOM_ASSIGNMENT_MANDATORY_AMENITIES_POSTING].integerValue;
    } @catch (NSException *exception) {
        NSLog(@"CountPopupReminderViewV2.m>initWithRoomId>%@", exception.reason);
    } @finally {
        // TODO
    }
    // CFG [20160928/CRF-00001612] - End.
    
    //Minibar
    if (actionMinibar.isAllowedView || !actionMinibar.isActive || ENABLE_MINIBAR == 0)
    {
        lblMinibar = nil;
        imgChkMinibar = nil;
    }
    else
    {
        lblMinibar = [[UILabel alloc] initWithFrame:CGRectMake(91, /*207*/ frame.origin.y, 77, 21)];
        [lblMinibar setFont:[UIFont fontWithName:@"Arial" size:17]];
        [lblMinibar setText:[[LanguageManagerV2 sharedLanguageManager] getMiniBar]];
        [lblMinibar setBackgroundColor:[UIColor clearColor]];
        [lblMinibar setTextColor:[UIColor colorWithRed:0.0/255 green:204.0/255 blue:255.0/255 alpha:1]];
        
        [self addSubview:lblMinibar];
        
        imgChkMinibar = [[UIImageView alloc] initWithFrame:CGRectMake(194, fCheck.origin.y, 32, 27)];
        [imgChkMinibar setImage:[UIImage imageNamed:@"yes_icon2.png"]];
        [imgChkMinibar setHidden:YES];
        [self addSubview:imgChkMinibar];
        
        frame = lblMinibar.frame;
        fCheck = imgChkMinibar.frame;
    }
    
    //Linen
    if (actionLinen.isAllowedView || !actionLinen.isActive || ENABLE_LINEN == 0)
    {
        lblLinen = nil;
        imgChkLinen = nil;
    }
    else
    {
        lblLinen = [[UILabel alloc] initWithFrame:CGRectMake(91, frame.origin.y + 38, 77, 21)];
        [lblLinen setFont:[UIFont fontWithName:@"Arial" size:17]];
        [lblLinen setText:[[LanguageManagerV2 sharedLanguageManager] getLinen]];
        [lblLinen setBackgroundColor:[UIColor clearColor]];
        [lblLinen setTextColor:[UIColor colorWithRed:0.0/255 green:204.0/255 blue:255.0/255 alpha:1]];
        [self addSubview:lblLinen];
        
        imgChkLinen = [[UIImageView alloc] initWithFrame:CGRectMake(195, fCheck.origin.y + 38, 32, 27)];
        [imgChkLinen setImage:[UIImage imageBeforeiOS7:imgYesIcon equaliOS7:imgYesIconFlat]];
        [imgChkLinen setHidden:YES];
        [self addSubview:imgChkLinen];
        
        frame = lblLinen.frame;
        fCheck = imgChkLinen.frame;
    }
    
    
    //Amenities
    if(actionAmenities.isAllowedView || !actionAmenities.isActive || ENABLE_AMENITIES == 0) {
        lblAmenities = nil;
        imgChkAmenities = nil;
    } else {
        lblAmenities = [[UILabel alloc] initWithFrame:CGRectMake(91, frame.origin.y + 38, 77, 21)];
        [lblAmenities setFont:[UIFont fontWithName:@"Arial" size:17]];
        [lblAmenities setText:[[LanguageManagerV2 sharedLanguageManager] getAmenities]];
        [lblAmenities setBackgroundColor:[UIColor clearColor]];
        [lblAmenities setTextColor:[UIColor colorWithRed:0.0/255 green:204.0/255 blue:255.0/255 alpha:1]];
        [self addSubview:lblAmenities];
        
        imgChkAmenities = [[UIImageView alloc] initWithFrame:CGRectMake(194, fCheck.origin.y + 38, 32, 27)];
        [imgChkAmenities setImage:[UIImage imageBeforeiOS7:imgYesIcon equaliOS7:imgYesIconFlat]];
        [imgChkAmenities setHidden:YES];
        [self addSubview:imgChkAmenities];
        
        frame = lblAmenities.frame;
        fCheck = imgChkAmenities.frame;
    }
    
    //YES
    btnYesAlert = [[UIButton alloc] initWithFrame:CGRectMake(30, 358, 130, 46)];
    [btnYesAlert setTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] forState:UIControlStateNormal];
    [btnYesAlert setBackgroundImage:[UIImage imageBeforeiOS7:imgYesBtn equaliOS7:imgYesBtnFlat] forState:UIControlStateNormal];
    [btnYesAlert addTarget:self action:@selector(btnYesAlertPressed) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:btnYesAlert];
    
    //NO
    btnNoAlert = [[UIButton alloc] initWithFrame:CGRectMake(162, 358, 130, 46)];
    [btnNoAlert setTitle:[[LanguageManagerV2 sharedLanguageManager] getNo] forState:UIControlStateNormal];
    [btnNoAlert setBackgroundImage:[UIImage imageBeforeiOS7:imgNoBtn equaliOS7:imgNoBtnFlat] forState:UIControlStateNormal];
    [btnNoAlert addTarget:self action:@selector(btnNoAlertPressed) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:btnNoAlert];
    
    self.roomId = roomId1;
    
    return self;
}

-(void) loadAccessRights
{
    AccessRightModel *currentUserAccessRight = [[UserManagerV2 sharedUserManager] currentUserAccessRight];
    actionLostAndFound = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.actionLostAndFound];
    actionLaundry = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.actionLaundry];
    actionMinibar = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.actionMinibar];
    actionLinen = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.actionLinen];
    actionAmenities = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.actionAmenities];
    actionEngineering = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.actionEngineering];
}

-(BOOL)show {
       
    if ([self checkCountStatuses]) {
        [[[UIApplication sharedApplication] keyWindow] addSubview:self];
        
        CGRect f = self.frame;
        f.origin.x = 320;
        self.frame = f;
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.5];
        
        f.origin.x = 0;
        self.frame = f;
        
        [UIView commitAnimations];
        
        return YES;
    }
    else
    {
        [self hide];
        return NO;
    }
}

-(void) removeCountPopup {
    [self removeFromSuperview];
}

-(void)hide
{
    CGRect f = self.frame;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDidStopSelector:@selector(removeCountPopup)];
    
    f.origin.x = 320;
    self.frame = f;
    
    [UIView commitAnimations];
}

-(BOOL)checkCountStatuses
{
    //enable flag to announce, is loading data
    [[HomeViewV2 shareHomeView] waitingLoadingData];
    
    BOOL showPopup = NO;
    
    //Hao Tran - Remove for unneccessary
    /*
    LaundryManagerV2  * manager=[[LaundryManagerV2 alloc] init];
    LaudryOrderModelV2* orderModel=[[LaudryOrderModelV2 alloc] init];
    orderModel.laundryOrderRoomId=[TasksManagerV2 getCurrentRoomAssignment];
    orderModel.laundryOrderUserId = [[UserManagerV2 sharedUserManager] currentUser].userId;
    orderModel.laundryOrderCollectDate = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd"];
    
    [manager loadLaundryOrdersModelByRoomIdUserIdAndCollectDate:orderModel];
    */
    
    [btnYesAlert setHidden:NO]; // CFG [20160928/CRF-00001612]
    
    CountManagerV2 *cmanager = [[CountManagerV2 alloc] init];
    NSMutableArray *array = [cmanager loadAllCountSerice];
    if([array count] > 0) {
        for (CountServiceModelV2 *countService in array) {
            
            CountOrdersModelV2 *countOrderModel = [[CountOrdersModelV2 alloc] init];
            countOrderModel.countRoomAssignId = roomId;
            countOrderModel.countUserId = [[UserManagerV2 sharedUserManager] currentUser].userId;
            countOrderModel.countCollectDate = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
            countOrderModel.countServiceId = countService.countServiceId;
            
            
            //[cmanager loadCountOrdersModelByRoomIdUserIdAndServiceId:countOrderModel];
            //        //Check Minibar and Linen
            //        if (countOrderModel.countOrderId == 0) {
            //            continue;
            //        }
            
            if (countOrderModel.countServiceId == cMinibar) {
                if(imgChkMinibar)
                {
                    if (countOrderModel.countStatus == tStatusChecked) {
                        [imgChkMinibar setHidden:NO];
                    }
                    else
                    {
                        [imgChkMinibar setHidden:YES];
                        if (nRoomAssignmentMandatoryMinibarPosting > 0) [btnYesAlert setHidden:YES]; // CFG [20160928/CRF-00001612]
                        showPopup = YES;
                    }
                }
            }
            
            if (countOrderModel.countServiceId == cLinen) {
                if(imgChkLinen)
                {
                    if (countOrderModel.countStatus == tStatusChecked) {
                        [imgChkLinen setHidden:NO];
                    }
                    else
                    {
                        [imgChkLinen setHidden:YES];
                        if (nRoomAssignmentMandatoryLinenPosting > 0) [btnYesAlert setHidden:YES]; // CFG [20160928/CRF-00001612]
                        showPopup = YES;
                    }
                }
            }
            
            if (countOrderModel.countServiceId == cAmenities) {
                if(imgChkAmenities)
                {
                    if (countOrderModel.countStatus == tStatusChecked) {
                        [imgChkAmenities setHidden:NO];
                    }
                    else
                    {
                        [imgChkAmenities setHidden:YES];
                        if (nRoomAssignmentMandatoryAmenitiesPosting > 0) [btnYesAlert setHidden:YES]; // CFG [20160928/CRF-00001612]
                        showPopup = YES;
                    }
                }
            }
        }
    } else {
        if (actionMinibar.isAllowedView || !actionMinibar.isActive || ENABLE_MINIBAR == 0){
            
        } else {
            if(isAlreadyPostedMiniBar){
                [imgChkMinibar setHidden:NO];
            }
            else
            {
                [imgChkMinibar setHidden:YES];
                if (nRoomAssignmentMandatoryMinibarPosting > 0) [btnYesAlert setHidden:YES]; // CFG [20160928/CRF-00001612]
                showPopup = YES;
            }
        }
        
        if (actionLinen.isAllowedView || !actionLinen.isActive || ENABLE_LINEN == 0) {
            
        } else {
            if(isAlreadyPostedLinen){
                [imgChkLinen setHidden:NO];
            }
            else
            {
                [imgChkLinen setHidden:YES];
                if (nRoomAssignmentMandatoryLinenPosting > 0) [btnYesAlert setHidden:YES]; // CFG [20160928/CRF-00001612]
                showPopup = YES;
            }
        }
        
        if (actionAmenities.isAllowedView || !actionAmenities.isActive || ENABLE_AMENITIES == 0) {
            
        } else {
            if(isAlreadyPostedAmenities){
                [imgChkAmenities setHidden:NO];
            }
            else
            {
                [imgChkAmenities setHidden:YES];
                if (nRoomAssignmentMandatoryAmenitiesPosting > 0) [btnYesAlert setHidden:YES]; // CFG [20160928/CRF-00001612]
                showPopup = YES;
            }
        }
        
    }
    
    // CFG [20160928/CRF-00001612] - Check hidden property and rearrange the position.
    if (btnYesAlert.hidden == YES) {
        btnNoAlert.frame = CGRectMake(30, 358, 260, 46);
    }
    else {
        btnYesAlert.frame = CGRectMake(30, 358, 130, 46);
        btnNoAlert.frame = CGRectMake(162, 358, 130, 46);
    }
    // CFG [20160928/CRF-00001612] - End.
    
    
//    //Check Laundry
//    if (orderModel.laundryOrderStatus == tStatusChecked) {
//        [imgChkLaundry setHidden:NO];
//    }
//    else
//    {
//        [imgChkLaundry setHidden:YES];
//        showPopup = YES;
//    }
//    
//    //check Amenities
//    AmenitiesManagerV2 *amanager = [[AmenitiesManagerV2 alloc] init];
//    NSMutableArray *arrayAmenities = [amanager loadAllamenitiesOrderByRoomId:roomId AndUserId:[[UserManagerV2 sharedUserManager] currentUser].userId];
//    
//    if (arrayAmenities.count <=0) {
//        showPopup = YES;
//    }
//    
//    for (AmenitiesOrdersModelV2 *amenityOrder in arrayAmenities) {
//        //Check Amenities
//        if (amenityOrder.amenStatus == tStatusChecked) {
//            [imgChkAmenities setHidden:NO];
//            break;
//        }
//        else
//        {
//            [imgChkAmenities setHidden:YES];
//            showPopup = YES;
//        }
//    }
    
    //end loading data
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
    
    return showPopup;

}

-(void)btnNoAlertPressed
{

    if ([delegate respondsToSelector:@selector(noCountReminder)]) {
        [delegate noCountReminder];
    }
}

-(void)btnYesAlertPressed
{
    if ([delegate respondsToSelector:@selector(yesCountReminder)]) {
        [delegate yesCountReminder];
    }
}

@end
