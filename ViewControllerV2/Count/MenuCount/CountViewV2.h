//
//  CountViewV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MenuCountCellV2.h"
//#import "LaundryViewV2.h"
#import "MiniBarViewV2.h"
#import "LinenControllerV2.h"
//#import "AmenitiesLocationViewV2.h"
#import "CountServiceModelV2.h"
#import "AccessRight.h"

@interface CountViewV2 : UIViewController <UITableViewDataSource, UITableViewDelegate>
{
    BOOL isFromMainPosting;
    BOOL isFromHistory;
    
    AccessRight *postingLostAndFound;
    AccessRight *postingEngineering;
    AccessRight *postingLaundry;
    AccessRight *postingMinibar;
    AccessRight *postingLinen;
    AccessRight *postingAmenities;
    AccessRight *postingPhysicalCheck;
    AccessRight *actionLostAndFound;
    AccessRight *actionEngineering;
    AccessRight *actionMinibar;
    AccessRight *actionLinen;
    AccessRight *actionLaundry;
    AccessRight *actionAmenities;
}
@property (strong, nonatomic) IBOutlet UITableView *tbvMenuCount;
@property (strong, nonatomic) NSMutableArray *data;
@property (nonatomic) BOOL isFromMainPosting;
@property (nonatomic) BOOL isFromHistory;
- (void) setCaptionView;
- (void) loadDataCountService;

@end
