//
//  CountViewV2.m
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CountViewV2.h"
#import "CountManagerV2.h"
#import "CountPopupReminderViewV2.h"
#import "RoomManagerV2.h"
//#import "LaundryManagerV2.h"
#import "ehkConvert.h"
#import "NetworkCheck.h"
#import "CustomAlertViewV2.h"
#import "PhysicalCheckViewV2.h"
#import "EngineeringCaseViewV2.h"
#import "LostFoundViewV2.h"
#import "PhysicalCheckHistoryView.h"
#import "CountHistoryController.h"
#import "EngineeringHistoryController.h"
#import "AccessRight.h"
#import "LaFHistoryController.h"
#import "MyNavigationBarV2.h"
#import "LogFileManager.h"
//#import "LaundryViewV2.h"
//#import "AmenitiesCategoryViewV2.h"
//#import "AmenitiesLocationViewV2.h"
#import "AmenitiesController.h"
#import "DeviceManager.h"

#define imgEngineering @"btn_Engineering.png"
#define imgMinibar @"btn_Minibar.png"
#define imgLinen @"btn_Linen.png"
#define imgLostFound @"btn_LostFound.png"
#define imgPhysicalCheck @"btn_PhyscialCheck.png"

//#define ALERTTITLE @"Alert"
//#define YESTITLE @"OK"
//#define NONETWORKCONNECTION @"Network is not available, please check your device and try again later."


@interface CountViewV2 (PrivateMethods)

#define tagTopbarView 1234

-(void) loadTopbarView;
-(void) showTopbarView;
-(void) hideTopbarView;
-(void) setHiddenTopbarView;
-(void) adjustShowForViews;
-(void) adjustRemoveForViews;
-(void) addButtonHandleShowHideTopbar;
-(void) topbarTapped:(UIButton *) sender;
-(TopbarViewV2 *) getTopBarView;

@end

@implementation CountViewV2
@synthesize tbvMenuCount;
@synthesize data;
@synthesize isFromMainPosting;
@synthesize isFromHistory;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    //Resize content views of current VC to fit with Navigation bar and tabbar
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    [self loadAccessRights];
    // Do any additional setup after loading the view from its nib.
    [self loadDataCountService];
    [tbvMenuCount setBackgroundView:nil];
    [tbvMenuCount setBackgroundColor:[UIColor clearColor]];
    [tbvMenuCount setOpaque:YES];
    
    [self performSelector:@selector(loadTopbarView)];
}

- (void)viewWillAppear:(BOOL)animated{
    [self setCaptionView];
    //show wifi view
    if(!isDemoMode){
        [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationShowWifiView] object:nil];
    }
    
    [super viewWillAppear:animated];
    
    //set selection find in bottom bar
    if(isFromHistory) {
        [[HomeViewV2 shareHomeView] setSelectedButton:tagOfHistoryButton];
    }
    else if(isFromMainPosting) {
        [[HomeViewV2 shareHomeView] setSelectedButton:tagOfPostingButton];
    }
    else {
        [[HomeViewV2 shareHomeView] setSelectedButton:tagOfHomeButton];
    }
    
}
- (void)viewDidUnload
{
    //    [self setTbvMenuCount:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Access Rights
-(void) loadAccessRights
{
    AccessRightModel *currentUserAccessRight = [[UserManagerV2 sharedUserManager] currentUserAccessRight];
    postingLostAndFound = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.postLostAndFound];
    postingEngineering = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.postEngineering];
    postingLaundry = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.postLaundry];
    postingMinibar = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.postMinibar];
    postingLinen = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.postLinen];
    postingAmenities = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.postAmenities];
    if([LogFileManager isLogConsole])
    {
        NSLog(@"Posting Amenities:%@", postingAmenities.isActive? @"True":@"False");
    }
    postingPhysicalCheck = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.postPhysicalCheck];
    
    actionLostAndFound = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.actionLostAndFound];
    actionEngineering = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.actionEngineering];
    actionLaundry = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.actionLaundry];
    actionMinibar = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.actionMinibar];
    actionLinen = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.actionLinen];
    actionAmenities = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.actionAmenities];
    
}

#pragma mark - === Handle Check Save Data ===
#pragma mark
-(BOOL) checkSaveDataWithTagEvent:(NSInteger) tagOfEvent {
    
    if (tagOfEvent == tagOfMessageButton) {
        //go to message screen
        return NO;
    }
    else
    {
        if (isFromMainPosting == NO) {
            return ([CommonVariable sharedCommonVariable].roomIsCompleted == 1);
        }
        //Show alert complete room
//        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_complete_room_to_leave] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles: nil]; 
//        alert.delegate = self;
//        [alert show];
        
        return YES;
        
    }
    
}

#pragma mark - Check Access Right Posting

//Check access right has physical or not
//Return true if enabled
-(BOOL)hasPhysicalCheck
{
    BOOL hasPhysicalResult = NO;
    
    if(isFromMainPosting || isFromHistory)
    {
        if(postingPhysicalCheck.isActive || postingPhysicalCheck.isAllowedView)
        {
            hasPhysicalResult = YES;
        }
    }
    
    return hasPhysicalResult;
}

//Check access right has physical or not
//Return true if enabled
-(BOOL)hasLinen
{
    BOOL hasLinenResult = NO;
    if(isFromMainPosting || isFromHistory)
    {
        if (postingLinen.isActive || postingLinen.isAllowedView)
        {
            hasLinenResult = YES;
        }
    }
    else if(actionLinen.isActive || actionLinen.isAllowedView)
    {
        hasLinenResult = YES;
    }
    
    return hasLinenResult;
}

//Check access right has Minibar or not
//Return true if enabled
-(BOOL)hasMinibar
{
    BOOL hasMinibarResult = NO;
    
    if (isFromMainPosting || isFromHistory)
    {
        if (postingMinibar.isActive || postingMinibar.isAllowedView)
        {
            hasMinibarResult = YES;
        }
    }
    else if(actionMinibar.isActive || actionMinibar.isAllowedView)
    {
        hasMinibarResult = YES;
    }
    
    return hasMinibarResult;
}

//Check access right has Engineer or not
//Return true if enabled
-(BOOL)hasEngineer
{
    BOOL hasEngineerResult = NO;
    if (isFromMainPosting || isFromHistory)
    {
        if(postingEngineering.isActive || postingEngineering.isAllowedView)
        {
            hasEngineerResult = YES;
        }
    }
    else if(actionEngineering.isActive || actionEngineering.isAllowedView)
    {
        hasEngineerResult = YES;
    }

    return hasEngineerResult;
}

//Check access right has LostAndFound or not
//Return true if enabled
-(BOOL)hasLostAndFound
{
    BOOL hasLostFoundResult = NO;
    
    if(isFromMainPosting || isFromHistory)
    {
        if (postingLostAndFound.isActive || postingLostAndFound.isAllowedView)
        {
            hasLostFoundResult = YES;
        }
    }
    else if(actionLostAndFound.isActive || actionLostAndFound.isAllowedView)
    {
        hasLostFoundResult = YES;
    }

    return hasLostFoundResult;
}

//Check access right has Laundry or not
//Return true if enabled
-(BOOL)hasLaundry
{
    BOOL hasLaundryResult = NO;
    
    if(isFromMainPosting || isFromHistory)
    {
        if (postingLaundry.isActive || postingLaundry.isAllowedView)
        {
            hasLaundryResult = YES;
        }
    }
    else if(actionLaundry.isActive || actionLaundry.isAllowedView)
    {
        hasLaundryResult = YES;
    }
    
    return hasLaundryResult;
}

//Check access right has Amenities or not
//Return true if enabled
-(BOOL)hasAmenities
{
    BOOL hasAmenitiesResult = NO;
    
    if(isFromMainPosting || isFromHistory)
    {
        if (postingAmenities.isActive || postingAmenities.isAllowedView)
        {
            hasAmenitiesResult = YES;
        }
    }
    else if(actionAmenities.isActive || actionAmenities.isAllowedView)
    {
        hasAmenitiesResult = YES;
    }
    
    return hasAmenitiesResult;
}

#pragma mark - UITableView DataSource Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    int countPostingMenu = 0;
    
//    if([self hasPhysicalCheck])
    if(ENABLE_PHYSICAL && (isFromMainPosting || isFromHistory))
    {
        countPostingMenu ++;
    }
    
    //if([self hasMinibar])
    if(ENABLE_MINIBAR)
    {
        countPostingMenu ++;
    }
    
    //if([self hasLinen])
    if(ENABLE_LINEN)
    {
        countPostingMenu ++;
    }
    
    //if([self hasEngineer])
    if(ENABLE_ENGINEER)
    {
        countPostingMenu ++;
    }
    
    //if([self hasLostAndFound])
    if(ENABLE_LOSTFOUND)
    {
        countPostingMenu ++;
    }
    
    //if([self hasLaundry])
    if(ENABLE_LAUNDRY)
    {
        countPostingMenu ++;
    }
    
    //if([self hasAmenities])
    if(ENABLE_AMENITIES)
    {
        countPostingMenu ++;
    }
    
    return countPostingMenu;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 97;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    //    CountServiceModelV2 *model;
    
    static NSString *cellidentifier = @"CellIdentifier";
    MenuCountCellV2 *countcell = nil;
    countcell = [tableView dequeueReusableCellWithIdentifier:cellidentifier];
    if(countcell == nil){
        NSArray *array = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([MenuCountCellV2 class]) owner:self options:nil];
        countcell = [array objectAtIndex:0];
    }  
    
    NSInteger row = indexPath.row;
    
    //******
    //Count Posting Controls
    //******
    int countPhysicalCheck = 0;
    int countLinen = 0;
    int countMinibar = 0;
    int countEngineer = 0;
    int countLostAndFound = 0;
    int countLaundry = 0;
    int countAmenities = 0;
    
    NSInteger indexPhysical = -1;
    if (ENABLE_PHYSICAL && (isFromMainPosting || isFromHistory)){
        indexPhysical = 0;
        countPhysicalCheck = 1;
    }
    
    NSInteger indexMinibar = -1;
    if (ENABLE_MINIBAR){
        indexMinibar = countPhysicalCheck;
        countMinibar = 1;
    }
    
    NSInteger indexLinen = -1;
    if (ENABLE_LINEN) {
        indexLinen = countPhysicalCheck  + countMinibar;
        countLinen = 1;
    }
    
    NSInteger indexEngineer = -1;
    if (ENABLE_ENGINEER) {
        indexEngineer = countPhysicalCheck + countMinibar + countLinen;
        countEngineer = 1;
    }
    
    NSInteger indexLostFound = -1;
    if (ENABLE_LOSTFOUND) {
        indexLostFound = countPhysicalCheck + countMinibar + countLinen + countEngineer;
        countLostAndFound = 1;
    }
    
    NSInteger indexLaundry = -1;
    if (ENABLE_LAUNDRY) {
        indexLaundry = countPhysicalCheck + countMinibar + countLinen + countEngineer + countLostAndFound;
        countLaundry = 1;
    }
    
    NSInteger indexAmenities = -1;
    if (ENABLE_AMENITIES) {
        indexAmenities = countPhysicalCheck + countMinibar + countLinen + countEngineer + countLostAndFound + countLaundry;
        countAmenities = 1;
    }
    
    if (row == indexPhysical) {
        [countcell.imageView setImage:[UIImage imageNamed:imgPhysicalCheck]];
        
        //View Check
        if (![self hasPhysicalCheck]) {
            countcell.imageView.alpha = 0.5;
            [countcell setUserInteractionEnabled:NO];
        }
        
        if (isFromHistory) {
            countcell.label.frame = CGRectMake(139, 68, 172, 21);
            [countcell.label setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_PHYSICAL_CHECK_HISTORY]];
        }
        else{
            [countcell.label setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_Physical_Check_Title]];
        }
    }
    
    if (row == indexMinibar) {
        [countcell.imageView setImage:[UIImage imageNamed:imgMinibar]];
        //View Check
        if (![self hasMinibar]) {
            countcell.imageView.alpha = 0.5;
            [countcell setUserInteractionEnabled:NO];
        }
        
        if (isFromHistory) {
            countcell.label.frame = CGRectMake(139, 68, 172, 21);
            [countcell.label setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_MINIBAR_HISTORY]];
        }
        else{
            [countcell.label setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_MINIBAR]];
        }
    }
    
    if (row == indexLinen) {
        [countcell.imageView setImage:[UIImage imageNamed:imgLinen]];
        
        //View Check
        if (![self hasLinen]) {
            countcell.imageView.alpha = 0.5;
            [countcell setUserInteractionEnabled:NO];
        }
        
        if (isFromHistory) {
            countcell.label.frame = CGRectMake(139, 68, 172, 21);
            [countcell.label setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_LINEN_HISTORY]];
        }
        else{
            [countcell.label setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_Linen_Title]];
        }
    }
    
    if (row == indexEngineer) {
        [countcell.imageView setImage:[UIImage imageNamed:imgEngineering]];
        //View Check
        if (![self hasEngineer]) {
            countcell.imageView.alpha = 0.5;
            [countcell setUserInteractionEnabled:NO];
        }
        
        if (isFromHistory) {
            countcell.label.frame = CGRectMake(139, 68, 172, 21);
            [countcell.label setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ENGINEER_HISTORY]];
        }
        else{
            [countcell.label setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ENGINEERING_CASE]];
        }
    }
    
    if (row == indexLostFound) {
        [countcell.imageView setImage:[UIImage imageNamed:imgLostFound]];
        if(![self hasLostAndFound]){
            countcell.imageView.alpha = 0.5;
            [countcell setUserInteractionEnabled:NO];
        }
        
        if (isFromHistory) {
            countcell.label.frame = CGRectMake(139, 68, 172, 21);
            [countcell.label setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_LOST_FOUNT_HISTORY]];
        }
        else{
            [countcell.label setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_LOST_FOUND]];
        }
    }
    
    //add
    if (row == indexLaundry) {
        [countcell.imageView setImage:[UIImage imageNamed:imgEngineering]];
        countcell.label.frame = CGRectMake(139, 68, 172, 21);
        [countcell.label setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_LAUNDRY]];
    }
    
    if (row == indexAmenities) {
        [countcell.imageView setImage:[UIImage imageNamed:imgAmenities]];
        if(![self hasAmenities]){
            countcell.imageView.alpha = 0.5;
            [countcell setUserInteractionEnabled:NO];
        }
        if (isFromHistory) {
            countcell.label.frame = CGRectMake(139, 68, 172, 21);
            [countcell.label setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_AMENITIES_HISTORY]];
        }
        else{
            [countcell.label setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_AMENITIES]];
        }
        
    }
    
    [countcell setSelectionStyle:UITableViewCellSelectionStyleBlue];
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        countcell.backgroundColor = [UIColor clearColor];
    }
    
    return countcell;
}
#pragma mark - UITableView Delegate methods
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //hide wifi view
    if(!isDemoMode){
        [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationHideWifiView] object:nil];
    }
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [cell setSelected:YES animated:YES];
    
    NSInteger row = indexPath.row;
    
    ///******
    //Count Posting Controls
    //******
    int countPhysicalCheck = 0;
    int countLinen = 0;
    int countMinibar = 0;
    int countEngineer = 0;
    int countLostAndFound = 0;
    int countLaundry = 0;
    int countAmenities = 0;
    
    NSInteger indexPhysical = -1;
    if (ENABLE_PHYSICAL && (isFromMainPosting || isFromHistory))
    {
        indexPhysical = 0;
        countPhysicalCheck = 1;
    }
    
    NSInteger indexMinibar = -1;
    if (ENABLE_MINIBAR) {
        indexMinibar = countPhysicalCheck;
        countMinibar = 1;
    }
    
    NSInteger indexLinen = -1;
    if (ENABLE_LINEN) {
        indexLinen = countPhysicalCheck  + countMinibar;
        countLinen = 1;
    }
    
    NSInteger indexEngineer = -1;
    if (ENABLE_ENGINEER) {
        indexEngineer = countPhysicalCheck + countMinibar + countLinen;
        countEngineer = 1;
    }
    
    NSInteger indexLostFound = -1;
    if (ENABLE_LOSTFOUND) {
        indexLostFound = countPhysicalCheck + countMinibar + countLinen + countEngineer;
        countLostAndFound = 1;
    }
    
    NSInteger indexLaundry = -1;
    if (ENABLE_LAUNDRY) {
        indexLaundry = countPhysicalCheck + countMinibar + countLinen + countEngineer + countLostAndFound;
        countLaundry = 1;
    }
    
    NSInteger indexAmenities = -1;
    if (ENABLE_AMENITIES) {
        indexAmenities = countPhysicalCheck + countMinibar + countLinen + countEngineer + countLostAndFound + countLaundry;
        countAmenities = 1;
    }
    
    
    if (row == indexPhysical) {
        
//        if (isFromMainPosting){
//            PhysicalCheckVi ewV2 *physicalCheck = [[PhysicalCheckViewV2 alloc] initWithNibName:NSStringFromClass([PhysicalCheckViewV2 class]) bundle:nil];
//            physicalCheck.isFromMainPosting = isFromMainPosting;
//            
//            UIBarButtonItem *backButton = [[UIBarButtonItem alloc]initWithTitle:[[LanguageManagerV2 sharedLanguageManager]getBack] style:UIBarButtonItemStyleBordered target:nil action:nil];
//            self.navigationItem.backBarButtonItem = backButton;
//            [self.navigationController pushViewController:physicalCheck animated:YES];
//        }
        if (isFromHistory) {
            PhysicalCheckHistoryView *physicalHistory = [[PhysicalCheckHistoryView alloc] initWithNibName:NSStringFromClass([PhysicalCheckHistoryView class]) bundle:nil];
            [self.navigationController pushViewController:physicalHistory animated:YES];
            
        }
        else {
            PhysicalCheckViewV2 *physicalCheck = [[PhysicalCheckViewV2 alloc] initWithNibName:NSStringFromClass([PhysicalCheckViewV2 class]) bundle:nil];
            physicalCheck.isFromMainPosting = isFromMainPosting;
            [self.navigationController pushViewController:physicalCheck animated:YES];
        }
    }
    else if (row == indexMinibar) {
        
//        if (isFromMainPosting){
//            MiniBarViewV2 *minibar = [[MiniBarViewV2 alloc] initWithNibName:NSStringFromClass([MiniBarViewV2 class]) bundle:nil];
//            minibar.isFromMainPosting = isFromMainPosting;
//            
//            UIBarButtonItem *backButton = [[UIBarButtonItem alloc]initWithTitle:[[LanguageManagerV2 sharedLanguageManager]getBack] style:UIBarButtonItemStyleBordered target:nil action:nil];
//            self.navigationItem.backBarButtonItem = backButton;
//            
//            [minibar setTitle:[[LanguageManagerV2 sharedLanguageManager] getMiniBar]];
//            
//            minibar.countServiceId = cMinibar;
//            //        minibar.roomNumber = [RoomManagerV2 getCurrentRoomNo];
//            
//            
//            [self.navigationController pushViewController:minibar animated:YES];
//        }
        // push to Count History
        if (isFromHistory) {
            
            NSString *countHistoryClass = NSStringFromClass([CountHistoryController class]) ;
            CountHistoryController *countHistoryController = [[CountHistoryController alloc] initWithNibName:countHistoryClass bundle:nil];
            countHistoryController.isFromMinibarHistory = YES;
            countHistoryController.countServiceId = cMinibar;//get the serviceId
            [self.navigationController pushViewController:countHistoryController animated:YES];        
        }
        else {
            MiniBarViewV2 *minibar = [[MiniBarViewV2 alloc] initWithNibName:NSStringFromClass([MiniBarViewV2 class]) bundle:nil];
            minibar.isFromMainPosting = isFromMainPosting;
            [minibar setTitle:[[LanguageManagerV2 sharedLanguageManager] getMiniBar]];
            minibar.countServiceId = cMinibar;
            //        minibar.roomNumber = [RoomManagerV2 getCurrentRoomNo];
            [self.navigationController pushViewController:minibar animated:YES];
        }
        
    }
    else if (row == indexLinen) {
        
        //        if (isFromMainPosting){
        //            LinenControllerV2 *linen = [[LinenControllerV2 alloc] initWithNibName:NSStringFromClass([LinenControllerV2 class]) bundle:nil];
        //            linen.isFromMainPosting = isFromMainPosting;
        //            
        //            UIBarButtonItem *backButton = [[UIBarButtonItem alloc]initWithTitle:[[LanguageManagerV2 sharedLanguageManager]getBack] style:UIBarButtonItemStyleBordered target:nil action:nil];
        //            self.navigationItem.backBarButtonItem = backButton;
        //            
        //            [linen setTitle:[[LanguageManagerV2 sharedLanguageManager] getLinen]];
        //            
        //            linen.countServiceId = cLinen;
        //            
        //            [self.navigationController pushViewController:linen animated:YES];
        //        }
        
        if (isFromHistory) {
            NSString *countHistoryClass = NSStringFromClass([CountHistoryController class]) ;
            CountHistoryController *countHistoryController = [[CountHistoryController alloc] initWithNibName:countHistoryClass bundle:nil];
            countHistoryController.isFromMinibarHistory = NO;
            UIBarButtonItem *backButton = [[UIBarButtonItem alloc]initWithTitle:[[LanguageManagerV2 sharedLanguageManager]getBack] style:UIBarButtonItemStyleBordered target:nil action:nil];
            self.navigationItem.backBarButtonItem = backButton;
            countHistoryController.countServiceId = cLinen;//Get the serviceId
            [self.navigationController pushViewController:countHistoryController animated:YES];
        }
        else
        {
            LinenControllerV2 *linen = [[LinenControllerV2 alloc] initWithNibName:NSStringFromClass([LinenControllerV2 class]) bundle:nil];
            linen.isFromMainPosting = isFromMainPosting;
            [linen setTitle:[[LanguageManagerV2 sharedLanguageManager] getLinen]];
            linen.countServiceId = cLinen;
            [self.navigationController pushViewController:linen animated:YES];
        }
    }
    else if (row == indexEngineer) {
//        if (isFromMainPosting){
//            EngineeringCaseViewV2 *ecView = [[EngineeringCaseViewV2 alloc] init];
//            ecView.isFromMainPosting = isFromMainPosting;
//            [self.navigationController pushViewController:ecView animated:YES];
//        }
        if (isFromHistory) {
            EngineeringHistoryController *eHistoryController = [[EngineeringHistoryController alloc] initWithNibName:NSStringFromClass([EngineeringHistoryController class]) bundle:nil];
            UIBarButtonItem *backButton = [[UIBarButtonItem alloc]initWithTitle:[[LanguageManagerV2 sharedLanguageManager]getBack] style:UIBarButtonItemStyleBordered target:nil action:nil];
            self.navigationItem.backBarButtonItem = backButton;
            [self.navigationController pushViewController:eHistoryController animated:YES];
        }
        else {
            EngineeringCaseViewV2 *ecView = [[EngineeringCaseViewV2 alloc] init];
            ecView.isFromMainPosting = isFromMainPosting;
            [self.navigationController pushViewController:ecView animated:YES];
        }
    }
    
    else if (row == indexLostFound) {
        if (isFromHistory) {
            
            LaFHistoryController *lafhController = [[LaFHistoryController alloc] initWithNibName:NSStringFromClass([LaFHistoryController class]) bundle:nil];
            UIBarButtonItem *backButton = [[UIBarButtonItem alloc]initWithTitle:[[LanguageManagerV2 sharedLanguageManager]getBack] style:UIBarButtonItemStyleBordered target:nil action:nil];
            self.navigationItem.backBarButtonItem = backButton;
            [self.navigationController pushViewController:lafhController animated:YES];
        }
        else{
            LostFoundViewV2 *lfView = [[LostFoundViewV2 alloc] initWithNibName:@"LostFoundViewV2" bundle:nil];
            lfView.isFromMainPosting = isFromMainPosting;
            [self.navigationController pushViewController:lfView animated:YES];
        }
    }
    
    /*
    //-----------add
    else if (row == indexLaundry) {
        LaundryViewV2 *laundry = [[LaundryViewV2 alloc] initWithNibName:NSStringFromClass([LaundryViewV2 class]) bundle:nil];
        [self.navigationController pushViewController:laundry animated:YES];
    }
    */
    else if (row == indexAmenities) {
        if(isFromHistory) {
            
            NSString *countHistoryClass = NSStringFromClass([CountHistoryController class]) ;
            CountHistoryController *countHistoryController = [[CountHistoryController alloc] initWithNibName:countHistoryClass bundle:nil];
            countHistoryController.isFromMinibarHistory = NO;
            countHistoryController.countServiceId = cAmenities;
            [self.navigationController pushViewController:countHistoryController animated:YES];
        } else {
            //        AmenitiesCategoryViewV2 *cateView = [[AmenitiesCategoryViewV2 alloc] initWithNibName:NSStringFromClass([AmenitiesCategoryViewV2 class]) bundle:nil];
            AmenitiesController *amenController = [[AmenitiesController alloc] initWithNibName:NSStringFromClass([AmenitiesController class]) bundle:nil];
            amenController.isFromMainPosting = isFromMainPosting;
            [self.navigationController pushViewController:amenController animated:YES];
        }
    }
 
 
    /*////////-------------------------------------------------------------
        switch (indexPath.row) {
            case 0:
            {   
                LaundryViewV2 *laundry = [[LaundryViewV2 alloc] initWithNibName:NSStringFromClass([LaundryViewV2 class]) bundle:nil];
                         
                UIBarButtonItem *backButton = [[UIBarButtonItem alloc]initWithTitle:[[LanguageManagerV2 sharedLanguageManager]getBack] style:UIBarButtonItemStyleBordered target:nil action:nil];
                self.navigationItem.backBarButtonItem = backButton;
                [self.navigationController pushViewController:laundry animated:YES];
    
                
            }
                break;
            case 1:
            {
                MiniBarViewV2 *minibar = [[MiniBarViewV2 alloc] initWithNibName:NSStringFromClass([MiniBarViewV2 class]) bundle:nil];
                
                UIBarButtonItem *backButton = [[UIBarButtonItem alloc]initWithTitle:[[LanguageManagerV2 sharedLanguageManager]getBack] style:UIBarButtonItemStyleBordered target:nil action:nil];
                self.navigationItem.backBarButtonItem = backButton;
                
                [minibar setTitle:[[LanguageManagerV2 sharedLanguageManager] getMiniBar]];
                
                minibar.countServiceId = cMinibar;
                
                [self.navigationController pushViewController:minibar animated:YES];
            }
                break;
            case 2:
            {
                LinenControllerV2 *linen = [[LinenControllerV2 alloc] initWithNibName:NSStringFromClass([LinenControllerV2 class]) bundle:nil];
                
                UIBarButtonItem *backButton = [[UIBarButtonItem alloc]initWithTitle:[[LanguageManagerV2 sharedLanguageManager]getBack] style:UIBarButtonItemStyleBordered target:nil action:nil];
                self.navigationItem.backBarButtonItem = backButton;
                
                [linen setTitle:[[LanguageManagerV2 sharedLanguageManager] getLinen]];
                
                linen.countServiceId = cLinen;
                
                [self.navigationController pushViewController:linen animated:YES];
    
            }
                break;
            case 3:
            {
                AmenitiesLocationViewV2 *amenities = [[AmenitiesLocationViewV2 alloc] initWithNibName:NSStringFromClass([AmenitiesLocationViewV2 class]) bundle:nil];
                
                UIBarButtonItem *backButton = [[UIBarButtonItem alloc]initWithTitle:[[LanguageManagerV2 sharedLanguageManager]getBack] style:UIBarButtonItemStyleBordered target:nil action:nil];
                self.navigationItem.backBarButtonItem = backButton;
    
                [self.navigationController pushViewController:amenities animated:YES];
    
            }
                break;
            default:
                break;
        }
    */////////-------------------------------------------------------------
    
    [cell setSelected:NO animated:NO];
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 10;
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *footerview = [[UIView alloc]init];
    [footerview setBackgroundColor:[UIColor clearColor]];
    return footerview;
}
- (void) setCaptionView{
    if (isFromHistory) {
        [self setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_HISTORY]];
    }
    if (isFromMainPosting){
        [self setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_Counts]];
    }
    //refresh topview
    TopbarViewV2 *topview = [self getTopBarView];
    [topview refresh:[[SuperRoomModelV2 alloc] init]];
}

#pragma mark - Load Data Count Service
-(void)loadDataCountService
{
    //enable flag to announce, is loading data
    [[HomeViewV2 shareHomeView] waitingLoadingData];
    
    self.data = [NSMutableArray array];
    //Load data
    CountManagerV2 *manager = [[CountManagerV2 alloc] init];
    
    CountServiceModelV2 *model2 = [[CountServiceModelV2 alloc] init];
    model2.countServiceId  = cMinibar;
    [manager loadCountSerice:model2];
    [data addObject:model2];
    
    CountServiceModelV2 *model3 = [[CountServiceModelV2 alloc] init];
    model3.countServiceId  = cLinen;
    [manager loadCountSerice:model3];
    [data addObject:model3];
    
    [tbvMenuCount reloadData];
    
    //end loading data
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
}

#pragma mark - Handle Topbar Methods
-(void)loadTopbarView {
    TopbarViewV2 *topbar = [[TopbarViewV2 alloc] initViewWithSuperModel:[[CommonVariable sharedCommonVariable] superRoomModel]];
    [topbar setTag:tagTopbarView];
    [self.view addSubview:topbar];
    [topbar setHidden:YES];
    
    [topbar setAlpha:0.0];
    [self addButtonHandleShowHideTopbar];
}

-(void)showTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationWillStartSelector:@selector(setHiddenTopbarView)];
    
    f = topbar.frame;
    f.origin.y = 0;
    [topbar setFrame:f];
    [topbar setAlpha:1.0];
    
    [self adjustShowForViews];
    
    [UIView commitAnimations];
}

-(void)hideTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDidStopSelector:@selector(setHiddenTopbarView)];
    [UIView setAnimationDelegate:self];
    
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    [topbar setAlpha:0.0];
    
    [self adjustRemoveForViews];
    
    [UIView commitAnimations];
}

-(void)setHiddenTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    [topbar setHidden:!topbar.hidden];
}

-(TopbarViewV2 *)getTopBarView {
    return (TopbarViewV2 *)[self.view viewWithTag:tagTopbarView];
}

//-(void)addButtonHandleShowHideTopbar {
//    UIButton *titleBarButton = [[UIButton alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
//    
//    titleBarButton.titleLabel.backgroundColor = [UIColor clearColor];
//    titleBarButton.titleLabel.font = FONT_BUTTON_TOPBAR;
//    titleBarButton.titleLabel.textAlignment = NSTextAlignmentCenter;
//    titleBarButton.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
//    [titleBarButton.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
//    [titleBarButton.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
//    [titleBarButton addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
//    [titleBarButton setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_Counts] forState:UIControlStateNormal];
//    self.navigationItem.titleView = titleBarButton;
//}

-(void)addButtonHandleShowHideTopbar {
    UIView *vBarButton = [[UIView alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
    UIButton *titleBarButtonFirst = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 200, 30)];
    
    titleBarButtonFirst.titleLabel.backgroundColor = [UIColor clearColor];
    [titleBarButtonFirst.titleLabel setFont: FONT_BUTTON_TOPBAR];
    titleBarButtonFirst.titleLabel.textAlignment = NSTextAlignmentCenter;
    titleBarButtonFirst.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
    
    [titleBarButtonFirst.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
    [titleBarButtonFirst.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
    [titleBarButtonFirst addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    if (isFromMainPosting){
        [titleBarButtonFirst setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_Counts] forState:UIControlStateNormal];
    }
    else {
        
        NSString *roomNo = [RoomManagerV2 getCurrentRoomNo];
        [titleBarButtonFirst setTitle:roomNo forState:UIControlStateNormal];
    }
    
    if (isFromHistory) {
        //Set title for bottom tapbar
        [titleBarButtonFirst setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_HISTORY] forState:UIControlStateNormal];
    }
    
    [vBarButton addSubview:titleBarButtonFirst];
    
    
    UIButton *titleBarButtonSecond = [[UIButton alloc] initWithFrame:CGRectMake(0, 28, 200, 22)];
    [titleBarButtonSecond setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [titleBarButtonSecond.titleLabel setFont:FONT_BUTTON_TOPBAR_SECOND];
    
    if(isFromMainPosting){
        [titleBarButtonSecond setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_Menu_Title] forState:UIControlStateNormal];
    } else {
        [titleBarButtonSecond setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_Counts] forState:UIControlStateNormal];
    }
    
    [titleBarButtonSecond addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    [vBarButton addSubview:titleBarButtonSecond];
    
    self.navigationItem.titleView = vBarButton;
    
    //back button navigation
    self.navigationItem.leftBarButtonItem = [MyNavigationBarV2 createBackButtonWithTarget:self selector:@selector(btnBackPressed) forControlEvents:UIControlEventTouchUpInside];
    
    //Move title to center vertical
    CGRect titleViewBounds = self.navigationItem.titleView.bounds;
    int moveLeftTitle = 0;
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        moveLeftTitle = 10;
    }
    self.navigationItem.titleView.bounds = CGRectMake(titleViewBounds.origin.x + moveLeftTitle, Y_TOPBAR_TITLE, titleViewBounds.size.width, titleViewBounds.size.height);
    
}

-(void)btnBackPressed
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)topbarTapped:(UIButton *)sender {
    TopbarViewV2 *topbar = [self getTopBarView];
    [sender setEnabled:NO];
    if (topbar.isHidden) {
        [self showTopbarView];
    } else {
        [self hideTopbarView];
    }
    [self performSelector:@selector(topbarButtonEnable:) withObject:sender afterDelay:0.5];
}

-(void) topbarButtonEnable:(UIButton *) topbarBtn {
    [topbarBtn setEnabled:YES];
}

-(void)adjustShowForViews {
    //adjust all views when show 
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    CGRect ftbv = [tbvMenuCount frame];
    [tbvMenuCount setFrame:CGRectMake(0, f.size.height, 320, ftbv.size.height - f.size.height)];
}

-(void)adjustRemoveForViews { 
    //adjust all views when hide
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    CGRect ftbv = tbvMenuCount.frame;
    [tbvMenuCount setFrame:CGRectMake(0, 0, 320, ftbv.size.height + f.size.height)];
}

@end
