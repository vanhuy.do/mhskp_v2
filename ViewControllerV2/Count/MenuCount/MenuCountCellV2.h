//
//  MenuCountCellV2.h
//  mHouseKeeping
//
//  Created by TMS on 2/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuCountCellV2 : UITableViewCell{
    UIImageView *imageView;
    UILabel *label;
}
@property (nonatomic, strong) IBOutlet UIImageView *imageView;
@property (nonatomic, strong) IBOutlet UILabel *label;

@end
