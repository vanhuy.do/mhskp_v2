//
//  GrandTotalCartCellV2.h
//  mHouseKeeping
//
//  Created by TMS on 2/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GrandTotalCartCellV2 : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblUsed;
@property (strong, nonatomic) IBOutlet UILabel *lblNew;
@property (strong, nonatomic) IBOutlet UILabel *separatorVertical;
@property (strong, nonatomic) IBOutlet UILabel *separatorHorizontal;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblValueUsed;
@property (strong, nonatomic) IBOutlet UILabel *lblValueNew;

@end
