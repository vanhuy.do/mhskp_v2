//
//  GrandTotalCartCellV2.m
//  mHouseKeeping
//
//  Created by TMS on 2/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GrandTotalCartCellV2.h"

@implementation GrandTotalCartCellV2
@synthesize lblUsed;
@synthesize lblNew;
@synthesize separatorVertical;
@synthesize separatorHorizontal;
@synthesize lblTitle;
@synthesize lblValueUsed;
@synthesize lblValueNew;



- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc {
//    [lblUsed release];
//    [lblNew release];
//    [separatorVertical release];
//    [separatorHorizontal release];
//    [lblTitle release];
//    [lblValueUsed release];
//    [lblValueNew release];
//    [super dealloc];
}
@end
