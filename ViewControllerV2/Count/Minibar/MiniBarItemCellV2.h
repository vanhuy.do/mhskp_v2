//
//  MiniBarItemCellV2.h
//  mHouseKeeping
//
//  Created by TMS on 2/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MiniBarItemCellV2Delegate <NSObject>

@optional
-(void) btnUsedItemCellPressedWithIndexPath:(NSIndexPath *) path sender:(UIButton *)button;
-(void) btnNewItemCellPressedWithIndexPath:(NSIndexPath *) path sender:(UIButton *)button;

@end

@interface MiniBarItemCellV2 : UITableViewCell{
    __unsafe_unretained id<MiniBarItemCellV2Delegate> delegate;
    NSIndexPath *path;
}

@property (nonatomic, strong) NSIndexPath *path; 
@property (nonatomic, assign) id<MiniBarItemCellV2Delegate> delegate;
@property (strong, nonatomic) IBOutlet UIImageView *imgItemCell;
@property (strong, nonatomic) IBOutlet UILabel *lblTitleItem;
@property (strong, nonatomic) IBOutlet UILabel *lblUsedItem;
@property (strong, nonatomic) IBOutlet UILabel *lblNewItem;
@property (strong, nonatomic) IBOutlet UIButton *btnUsedItem;
@property (strong, nonatomic) IBOutlet UIButton *btnNewItem;
- (IBAction)btnUsedItemPressed:(id)sender;
- (IBAction)btnNewItemPressed:(id)sender;
@end
