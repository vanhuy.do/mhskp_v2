//
//  MiniBarItemCellV2.m
//  mHouseKeeping
//
//  Created by TMS on 2/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MiniBarItemCellV2.h"

@implementation MiniBarItemCellV2
@synthesize imgItemCell;
@synthesize lblTitleItem;
@synthesize lblUsedItem;
@synthesize lblNewItem;
@synthesize btnUsedItem;
@synthesize btnNewItem;
@synthesize path, delegate;

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc {
//    [imgItemCell release];
//    [lblTitleItem release];
//    [lblUsedItem release];
//    [lblNewItem release];
//    [btnUsedItem release];
//    [btnNewItem release];
//    [super dealloc];
}
- (IBAction)btnUsedItemPressed:(id)sender {
    if ([delegate respondsToSelector:@selector(btnUsedItemCellPressedWithIndexPath:sender:)]) {
        [delegate btnUsedItemCellPressedWithIndexPath:path sender:sender];
    }
}

- (IBAction)btnNewItemPressed:(id)sender {
    if ([delegate respondsToSelector:@selector(btnNewItemCellPressedWithIndexPath:sender:)]) {
        [delegate btnNewItemCellPressedWithIndexPath:path sender:sender];
    }
}
@end
