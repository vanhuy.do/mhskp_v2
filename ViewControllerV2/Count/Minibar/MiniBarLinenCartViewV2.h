//
//  MiniBarLinenCartViewV2.h
//  mHouseKeeping
//
//  Created by TMS on 2/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MiniBarSectionViewV2.h"
#import "MiniBarItemCellV2.h"
#import "PickerViewV2.h"
#import "MiniBarSectionInfoV2.h"
#import "MiniBarItemModelV2.h"

@protocol MiniBarLinenCartViewV2Delegate <NSObject>

@optional
-(void) saveMiniBarLinenCartItem;

@end


@interface MiniBarLinenCartViewV2 : UIViewController <UITableViewDataSource, UITableViewDelegate, MiniBarItemCellV2Delegate, MiniBarSectionViewV2Delegate, PickerViewV2Delegate> {
    NSMutableArray *sectionDatas;
    __unsafe_unretained id<MiniBarLinenCartViewV2Delegate> delegate;
}

@property (strong, nonatomic) IBOutlet UITableView *tbvCartItems;
@property (nonatomic, strong) NSMutableArray *sectionDatas;
@property (strong, nonatomic) IBOutlet UIView *grandTotalView;
@property (strong, nonatomic) IBOutlet UITableView *tbvGrandTotal;
@property (assign, nonatomic) NSInteger countCartServiceId;
@property (assign, nonatomic) id<MiniBarLinenCartViewV2Delegate> delegate;
@property (strong, nonatomic) NSString *msgbtnYes;
@property (strong, nonatomic) NSString *msgbtnNo;
@property (strong, nonatomic) NSString *msgSaveCount;
@property (strong, nonatomic) NSString *msgDiscardCount;
@property (assign, nonatomic) BOOL statusChooseQty;
@property (assign, nonatomic) BOOL statusSave;
@property (assign, nonatomic) BOOL statusCheckSaveData;

-(void) setCaptionsView;
-(void) saveMiniBarLinenCart;


@end
