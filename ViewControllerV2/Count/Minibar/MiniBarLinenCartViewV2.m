//
//  MiniBarLinenCartViewV2.m
//  mHouseKeeping
//
//  Created by TMS on 2/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MiniBarLinenCartViewV2.h"
#import "GrandTotalCartCellV2.h"
#import "MBProgressHUD.h"
#import "UserManagerV2.h"
#import "RoomManagerV2.h"
#import "CountManagerV2.h"
#import "CustomAlertViewV2.h"
#import "TasksManagerV2.h"
#import "NetworkCheck.h"
#import "MyNavigationBarV2.h"
#import "DeviceManager.h"
#define tagAlertBack 10
#define tagAlertNo 11

typedef enum {
    tUsed = 1,
    tNew,
}tType;

@interface MiniBarLinenCartViewV2 (PrivateMethods)

#define tagTopbarView 1234

-(void) loadTopbarView;
-(void) showTopbarView;
-(void) hideTopbarView;
-(void) setHiddenTopbarView;
-(void) adjustShowForViews;
-(void) adjustRemoveForViews;
-(void) addButtonHandleShowHideTopbar;
-(void) topbarTapped:(UIButton *) sender;
-(TopbarViewV2 *) getTopBarView;

@end

@implementation MiniBarLinenCartViewV2
@synthesize grandTotalView;
@synthesize tbvGrandTotal;
@synthesize tbvCartItems;
@synthesize sectionDatas;
@synthesize countCartServiceId;
@synthesize delegate;
@synthesize msgbtnNo;
@synthesize msgbtnYes;
@synthesize msgSaveCount;
@synthesize msgDiscardCount;
@synthesize statusChooseQty;
@synthesize statusSave;
@synthesize statusCheckSaveData;

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        sectionDatas = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle
- (void)viewDidUnload
{
    [self setTbvCartItems:nil];
    [self setGrandTotalView:nil];
    [self setTbvGrandTotal:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [self setCaptionsView];
    //show wifi view
    if(!isDemoMode){
        [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationShowWifiView] object:nil];
    }
    [super viewWillAppear:animated];
    
    //Move to center vertical
    CGRect titleViewBounds = self.navigationController.navigationBar.topItem.titleView.bounds;
    self.navigationController.navigationBar.topItem.titleView.bounds = CGRectMake(titleViewBounds.origin.x, Y_TOPBAR_TITLE, titleViewBounds.size.width, titleViewBounds.size.height);
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //[self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:imgBackground]]];
    
    [self.tbvCartItems setBackgroundView:nil];
    [self.tbvCartItems setBackgroundColor:[UIColor clearColor]];
    [self.tbvCartItems setOpaque:YES];

    [self.grandTotalView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"cart_bg.png"]]];
    
    [tbvGrandTotal setBackgroundView:nil];
    [tbvGrandTotal setBackgroundColor:[UIColor clearColor]];
    [tbvGrandTotal setOpaque:YES];
    CGRect frame = grandTotalView.frame;
    frame.size.height = 30 * (sectionDatas.count + 2);
    [grandTotalView setFrame:frame];
    frame = tbvGrandTotal.frame;
    frame.size.height = 30 * (sectionDatas.count + 2);
    [tbvGrandTotal setFrame:frame];
    
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *img = [UIImage imageNamed:@"btn_back.png"];
    [button setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK] forState:UIControlStateNormal];
    [button setBackgroundImage:img forState:UIControlStateNormal];
    [button addTarget:self action:@selector(backBarPressed) forControlEvents:UIControlEventTouchUpInside];
    [button setFrame:CGRectMake(0, 0,WIDTH_BACK_BUTTON , HEIGHT_BACK_BUTTON)];
    [button.titleLabel setFont:[UIFont boldSystemFontOfSize:FONT_BACK_BUTTON]];
    [button setContentEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
    UIView *backButtonView = [[UIView alloc] initWithFrame:button.frame];
    backButtonView.bounds = CGRectMake(backButtonView.bounds.origin.x, Y_BACK_BUTTON, backButtonView.bounds.size.width, backButtonView.bounds.size.height);
    [backButtonView addSubview:button];

    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButtonView];
    self.navigationItem.leftBarButtonItem = barButtonItem;
//    [barButtonItem release];

    [self performSelector:@selector(loadTopbarView)];
}

#pragma mark - Click Back
-(void)backBarPressed {
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - === Handle Check Save Data ===
#pragma mark
-(BOOL) checkSaveDataWithTagEvent:(NSInteger) tagOfEvent {
    
    if (tagOfEvent == tagOfMessageButton) {
        if (statusChooseQty == NO) {
            return NO;
        }
        else
        {
            //Show alert please save before leaving
            UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:msgSaveCount delegate:self cancelButtonTitle:msgbtnYes otherButtonTitles:msgbtnNo, nil]; 
            alert.delegate = self;
            alert.tag = tagAlertBack;
            [alert show];

            
            return YES;
        }
    }
    else
    {
        //Show alert complete room
        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_complete_room_to_leave] delegate:self cancelButtonTitle:msgbtnYes otherButtonTitles:nil, nil]; 
        alert.delegate = self;
        [alert show];
        
        return YES;
    }
    
}

#pragma mark - UIAlertView Delegate Methods
-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    switch (alertView.tag) {
        case tagAlertBack:
        {
            switch (buttonIndex)
            {
                case 0:
                {
                    //YES: save minibar
                    [self saveMiniBarLinenCart];
                    statusChooseQty = NO;
                    
                    //if tag is message
                    if ([[NSUserDefaults standardUserDefaults] integerForKey:TAG_OF_EVENT] == tagOfMessageButton) {
                        [[HomeViewV2 shareHomeView] handleTabbarEventWithTagOfButton:[[NSUserDefaults standardUserDefaults] integerForKey:TAG_OF_EVENT]];
                    }
                }
                    break;
                    
                case 1:
                {
                    //NO: show popup discard
                    UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:msgDiscardCount delegate:self cancelButtonTitle:msgbtnYes otherButtonTitles:msgbtnNo, nil]; 
                    alert.delegate = self;
                    alert.tag = tagAlertNo;
                    [alert show];
                    
                }
                    break;
                    
                default:
                    break;
            }
        }
            break;
            
        case tagAlertNo:
        {
            switch (buttonIndex)
            {
                case 0:
                {
                    //YES 
                    //if tag is message
                    if ([[NSUserDefaults standardUserDefaults] integerForKey:TAG_OF_EVENT] == tagOfMessageButton) {
                        [[HomeViewV2 shareHomeView] handleTabbarEventWithTagOfButton:[[NSUserDefaults standardUserDefaults] integerForKey:TAG_OF_EVENT]];
                    }
                    else{
                        //back to count menu, no save
                        NSArray *arrayController = [self.navigationController viewControllers];
                        for (UIViewController *viewController in arrayController) {
                            if ([viewController isKindOfClass:[CountViewV2 class]]) {
                                [self.navigationController popToViewController:viewController animated:YES];
                                break;
                            }
                        }
                    }

                }
                    break;
                    
                case 1:
                {
                    //NO: cart screen
                    //do no thing

                }
                    break;
                    
                default:
                    break;
            }
            
        }
            break;
            
        default:
            break;
    }
}


#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView == tbvGrandTotal) {
        return 1;
    }
    // Return the number of sections.
    return sectionDatas.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == tbvGrandTotal) {
        return sectionDatas.count + 2;
    }
    // Return the number of rows in the section.
    MiniBarSectionInfoV2 *sectioninfo = [sectionDatas objectAtIndex:section];
    NSInteger numberRowInSection = sectioninfo.rowHeights.count;
    return sectioninfo.open ? numberRowInSection : 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == tbvGrandTotal) {
        static NSString *identifyGrandCell = @"cellGrandTotal";
        GrandTotalCartCellV2 *cell = nil;
        
        cell = (GrandTotalCartCellV2 *)[tableView dequeueReusableCellWithIdentifier:identifyGrandCell];
        if (cell == nil) {
            NSArray *array = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([GrandTotalCartCellV2 class]) owner:self options:nil];
            cell = [array objectAtIndex:0];
        }
        
        
        [cell.lblNew setHidden:YES];
        [cell.lblUsed setHidden:YES];
        [cell.lblTitle setHidden:YES];
        [cell.lblValueNew setHidden:YES];
        [cell.lblValueUsed setHidden:YES];
        [cell.separatorHorizontal setHidden:YES];
        [cell.separatorVertical setHidden:NO];
        
        NSInteger row = indexPath.row;
        
        if (row == 0) {
            //title Used & New
            [cell.lblNew setText:[[LanguageManagerV2 sharedLanguageManager] getNew]];
            [cell.lblUsed setText:[[LanguageManagerV2 sharedLanguageManager] getUsed]];
            [cell.lblNew setHidden:NO];
            [cell.lblUsed setHidden:NO];
            CGRect frame = cell.separatorVertical.frame;
            frame.origin.y = 5;
            frame.size.height = 25;
            [cell.separatorVertical setFrame:frame];
            [cell.separatorHorizontal setHidden:NO];
            return cell;
        }
        
        if (row == sectionDatas.count + 1) {
            //Grand Total
            [cell.lblTitle setHidden:NO];
            [cell.lblTitle setText:[[LanguageManagerV2 sharedLanguageManager] getGrandTotal]];
            CGRect frame = cell.separatorVertical.frame;
            frame.origin.y = 0;
            frame.size.height = 25;
            [cell.separatorVertical setFrame:frame];
            
            NSInteger countNew = 0, countUsed = 0;
            for (MiniBarSectionInfoV2 *sectionInfo in sectionDatas) {
                for (MiniBarItemModelV2 *model in sectionInfo.rowHeights) {
                    countNew += model.minibarItemNew;
                    countUsed += model.minibarItemUsed;
                }
            }
            [cell.lblValueNew setHidden:NO];
            [cell.lblValueUsed setHidden:NO];
            [cell.lblValueNew setText: [NSString stringWithFormat:@"%d", (int)countNew]];
            [cell.lblValueUsed setText: [NSString stringWithFormat:@"%d", (int)countUsed]];
            
            return cell;
        }
        
        //Total of category
        [cell.lblTitle setHidden:NO];
        [cell.lblValueNew setHidden:NO];
        [cell.lblValueUsed setHidden:NO];
        
        MiniBarSectionInfoV2 *sectionInfo = [sectionDatas objectAtIndex:row - 1];
        [cell.lblTitle setText:[NSString stringWithFormat:@"%@ %@:", [[LanguageManagerV2 sharedLanguageManager] getSubtotal], sectionInfo.minibarCategory.minibarCategoryTitle]];
        NSInteger countUsed = 0, countNew = 0;
        for (MiniBarItemModelV2 *model in sectionInfo.rowHeights) {
            countNew += model.minibarItemNew;
            countUsed += model.minibarItemUsed;
        }
        [cell.lblValueNew setText: [NSString stringWithFormat:@"%d", (int)countNew]];
        [cell.lblValueUsed setText: [NSString stringWithFormat:@"%d", (int)countUsed]];
        
        if (row == sectionDatas.count) {
            [cell.separatorHorizontal setHidden:NO];
        }
        
        return cell;
    }
    
    
    static NSString *identifycell = @"cellidentify";
    MiniBarItemCellV2 *cell = nil;
    
    cell = (MiniBarItemCellV2 *)[tableView dequeueReusableCellWithIdentifier:identifycell];
    if (cell == nil) {
        NSArray *array = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([MiniBarItemCellV2 class]) owner:self options:nil];
        cell = [array objectAtIndex:0];
    }
    
    cell.delegate = self;
    MiniBarSectionInfoV2 *sectionInfo = [sectionDatas objectAtIndex:indexPath.section];
    MiniBarItemModelV2 *model = [sectionInfo.rowHeights objectAtIndex:indexPath.row];
    [cell.imgItemCell setImage:[UIImage imageWithData:model.minibarItemImage]];
    [cell.lblTitleItem setText:model.minibarItemTitle];
    [cell.lblNewItem setText:[[LanguageManagerV2 sharedLanguageManager] getNew]];
    [cell.lblUsedItem setText:[[LanguageManagerV2 sharedLanguageManager] getUsed]];
    [cell.btnNewItem setTitle:[NSString stringWithFormat:@"%d", (int)model.minibarItemNew] forState:UIControlStateNormal];
    [cell.btnUsedItem setTitle:[NSString stringWithFormat:@"%d", (int)model.minibarItemUsed] forState:UIControlStateNormal];
    [cell setPath:indexPath];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == tbvGrandTotal) {
        return 30;
    }
    return 70;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (tableView == tbvGrandTotal ) {
        return 0;
    }
    return 60;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (tableView == tbvGrandTotal) {
        return nil;
    }
    MiniBarSectionInfoV2 *sectionInfo = [sectionDatas objectAtIndex:section];
    
    if (sectionInfo.headerView == nil) {
        MiniBarSectionViewV2 *headerView = [[MiniBarSectionViewV2 alloc] initWithSection:section CategoryModelV2:sectionInfo.minibarCategory andOpenStatus:YES];
        sectionInfo.headerView = headerView;
        headerView.delegate = self;
    }
    return sectionInfo.headerView;
}

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

#pragma mark - Section Header Delegate
-(void)sectionHeaderView:(MiniBarSectionViewV2 *)sectionheaderView sectionOpened:(NSInteger)section {
    MiniBarSectionInfoV2 *sectionInfo = [sectionDatas objectAtIndex:section];
    sectionInfo.open = YES;
    
    NSInteger countOfRowsToInsert = [sectionInfo.rowHeights count];
    if (countOfRowsToInsert == 0) {
        return;
    }
    NSMutableArray *indexPathsToInsert = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i < countOfRowsToInsert; i++) {
        [indexPathsToInsert addObject:[NSIndexPath indexPathForRow:i
                                                         inSection:section]];
    }
    
    UITableViewRowAnimation insertAnimation;

    insertAnimation = UITableViewRowAnimationAutomatic;

    [self.tbvCartItems beginUpdates];
    
    [self.tbvCartItems insertRowsAtIndexPaths:indexPathsToInsert
                           withRowAnimation:insertAnimation];
    [self.tbvCartItems endUpdates];
    
    [self.tbvCartItems scrollToRowAtIndexPath:[indexPathsToInsert objectAtIndex:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    
}

-(void)sectionHeaderView:(MiniBarSectionViewV2 *)sectionheaderView sectionClosed:(NSInteger)section {
    MiniBarSectionInfoV2 *sectionInfo = [sectionDatas
                                         objectAtIndex:section];
    
    sectionInfo.open = NO;
    NSInteger countOfRowsToDelete = [self.tbvCartItems
                                     numberOfRowsInSection:section];
    
    if (countOfRowsToDelete > 0) {
        NSMutableArray *indexPathsToDelete = [[NSMutableArray alloc] init];
        for (NSInteger i = 0; i < countOfRowsToDelete; i++) {
            [indexPathsToDelete addObject:[NSIndexPath indexPathForRow:i
                                                             inSection:section]];
        }
        
        [self.tbvCartItems beginUpdates];
        [self.tbvCartItems deleteRowsAtIndexPaths:indexPathsToDelete
                               withRowAnimation:UITableViewRowAnimationAutomatic];
        [self.tbvCartItems endUpdates];
        
    }
}

#pragma mark - Item Cell Delegate Methods
-(void)btnUsedItemCellPressedWithIndexPath:(NSIndexPath *)path sender:(UIButton *)button{
    statusChooseQty = YES;
    NSMutableArray *array = [[NSMutableArray alloc] init];
    for (NSInteger index = 0; index <= 10; index++) {
        [array addObject:[NSString stringWithFormat:@"%d", (int)index]];
    }
    
    PickerViewV2 *picker = [[PickerViewV2 alloc] initPickerViewV2WithDatas:array AndSelectedData:button.titleLabel.text AndIndex:path];
    [picker showPickerViewV2];
    [picker setDelegate:self];
    [picker setTIndex:tUsed];
}

-(void)btnNewItemCellPressedWithIndexPath:(NSIndexPath *)path sender:(UIButton *)button{
    statusChooseQty = YES;
    NSMutableArray *array = [[NSMutableArray alloc] init];
    for (NSInteger index = 0; index <= 10; index++) {
        [array addObject:[NSString stringWithFormat:@"%d", (int)index]];
    }
    
    PickerViewV2 *picker = [[PickerViewV2 alloc] initPickerViewV2WithDatas:array AndSelectedData:button.titleLabel.text AndIndex:path];
    [picker showPickerViewV2];
    [picker setDelegate:self];
    [picker setTIndex:tNew];
}

#pragma mark - PickerView V2 Delegate methods
-(void)didChoosePickerViewV2WithData:(NSString *)data AndIndex:(NSIndexPath *)index WithtIndex:(NSInteger)tIndex{
    MiniBarSectionInfoV2 *sectionInfo = [sectionDatas objectAtIndex:index.section];
    MiniBarItemModelV2 *model = [sectionInfo.rowHeights objectAtIndex:index.row];
    
    if (tIndex == tUsed) {
        model.minibarItemUsed = [data integerValue];
    }
    
    if (tIndex == tNew) {
        model.minibarItemNew = [data integerValue];
    }
    
    [self.tbvCartItems reloadRowsAtIndexPaths:[NSArray arrayWithObject:index] withRowAnimation:UITableViewRowAnimationFade];
    [tbvGrandTotal reloadData];
}

#pragma mark - Save Cart Minibar or Linen
-(void) saveData{
    [self saveMiniBarLinenCart];
    statusChooseQty = NO;
    statusSave = YES;
    
}
-(void)saveMiniBarLinenCart
{
    // @ show loading data .
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getRaSavingData]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES]; 
    
    ///Save 
    CountManagerV2 *manager = [[CountManagerV2 alloc] init];
    SyncManagerV2 *syncManager = [[SyncManagerV2 alloc] init];
    NSInteger userId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    NSInteger roomAssignId = [TasksManagerV2 getCurrentRoomAssignment];
    NSInteger orderId = 0;
    
    //Update Order
    CountOrdersModelV2 *countOrder = [[CountOrdersModelV2 alloc] init];
    countOrder.countRoomAssignId = roomAssignId;
    countOrder.countServiceId = countCartServiceId;
    countOrder.countStatus = 1;
    countOrder.countUserId = userId;
    countOrder.countCollectDate = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
    countOrder.countRoomNumber = [RoomManagerV2 getCurrentRoomNo];
    countOrder.countPostStatus = POST_STATUS_SAVED_UNPOSTED;
    
    //Hao Tran - Remove for SG00054442
    //    BOOL isCheckCountOrder = [manager isCheckCountOrder:countOrder];
    //    if (isCheckCountOrder == FALSE) {
    //        [manager insertCountOrder:countOrder];
    //    }
    //    [manager loadCountOrdersModelByRoomIdUserIdAndServiceId:countOrder];
    
    [manager insertCountOrder:countOrder];
    countOrder =  [manager loadCountOrdersModelByRoomAssignId:(int)countOrder.countRoomAssignId roomNumber:countOrder.countRoomNumber userId:(int)countOrder.countUserId serviceId:(int)countOrder.countServiceId postStatus:(int)POST_STATUS_SAVED_UNPOSTED];
    
    //Hao Tran - END
    
    if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        //-----------------------get OrderId from WS for post Minibar-------------
        if (countOrder.countServiceId == cMinibar) {
            //get total quantity
            NSInteger totalQuantity = 0;
            for (NSInteger indexSection=0; indexSection < sectionDatas.count; indexSection ++) {
                MiniBarSectionInfoV2 *sectionInfo = [sectionDatas objectAtIndex:indexSection];
                for (NSInteger indexRow=0; indexRow < sectionInfo.rowHeights.count; indexRow++) {
                    MiniBarItemModelV2 *model = [sectionInfo.rowHeights objectAtIndex:indexRow];
                    if (model.minibarItemNew != 0 || model.minibarItemUsed != 0) {
                        totalQuantity += model.minibarItemUsed;
                    }
                }
            }
            //post minibar order Id
            NSString *roomNo = [RoomManagerV2 getCurrentRoomNo];
            
            if (roomNo == nil || roomNo.length == 0 || [roomNo isEqualToString:@""]) {
                NSString *message = nil;
                if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                    message = [NSString stringWithFormat:@"%@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOM_INVALID]];
                } else {
                    message = [NSString stringWithFormat:@"\n\n\n\n\n%@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOM_INVALID]];
                }
                
                UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObject:[UIImage imageNamed:@"yes_bt.png"]] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:message delegate:nil cancelButtonTitle:[L_CLOSE currentKeyToLanguage] otherButtonTitles: nil];
                
                if(!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                    UIImageView *alertImage = [[UIImageView alloc] initWithFrame:CGRectMake(80, 35, 120, 120)];
                    [alertImage setImage:[UIImage imageNamed:@"icon_wrong room.png"]];
                    [alert addSubview:alertImage];
                }
                [alert show];
                return;
            }
            orderId = [syncManager postMinibarOrderWithUserId:userId roomAssignId:roomAssignId totalQuantity:totalQuantity serviceCharge:0 AndSubTotal:totalQuantity WithDate:countOrder.countCollectDate];
        }
        //-------------------------------------------------------
    }
    
    //save minibar or linen detail
    NSMutableArray *postItems = [[NSMutableArray alloc] init];
    for (NSInteger indexSection=0; indexSection < sectionDatas.count; indexSection ++) {
        MiniBarSectionInfoV2 *sectionInfo = [sectionDatas objectAtIndex:indexSection];
        for (NSInteger indexRow=0; indexRow < sectionInfo.rowHeights.count; indexRow++) {
            MiniBarItemModelV2 *model = [sectionInfo.rowHeights objectAtIndex:indexRow];
            if (model.minibarItemNew != 0 || model.minibarItemUsed != 0) {
                               
                CountOrderDetailsModelV2 *countOrderDetailModel = [manager loadCountOrderDetailsByItemId:model.minibarItemID AndOrderId:countOrder.countOrderId];
                
                if (countOrderDetailModel.countOrderDetailId != 0) {
                    //update Order detail in minibar
                    countOrderDetailModel.countNew = model.minibarItemNew;
                    countOrderDetailModel.countCollected = model.minibarItemUsed;
                    countOrderDetailModel.countCollectedDate = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
                    countOrderDetailModel.countPostStatus = POST_STATUS_SAVED_UNPOSTED;
                    
                    [manager updateCountOrderDetail:countOrderDetailModel];
                }
                else
                {
                    //insert Order detail in minibar
                    CountOrderDetailsModelV2 *countOrderDetails = [[CountOrderDetailsModelV2 alloc] init];
                    countOrderDetails.countCollected = model.minibarItemUsed;
                    countOrderDetails.countNew = model.minibarItemNew;
                    countOrderDetails.countItemId = model.minibarItemID ;
                    countOrderDetails.countOrderId = countOrder.countOrderId;
                    countOrderDetails.countCollectedDate = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
                    countOrderDetails.countPostStatus = POST_STATUS_SAVED_UNPOSTED;
                    
                    [manager insertCountOrderDetail:countOrderDetails];
                }
                
                
                //-----------------Post WS--------------------------
                CountOrderDetailsModelV2 *countOrderDetailModelPost = [manager loadCountOrderDetailsByItemId:model.minibarItemID AndOrderId:countOrder.countOrderId];
                
                //implement new WS for post Minibar and Linen
                [postItems addObject:countOrderDetailModelPost];
                
//                BOOL statusPost = NO;
//                if (countOrder.countServiceId == cMinibar) {
//                    //post  minibar
//                    statusPost = [syncManager postMinibarItemWithUserId:userId  roomAssignId:roomAssignId AndMinibarOrderModel:countOrderDetailModelPost WithOrderId:orderId];
//                    
//                }
//                else
//                {
//                    //post linen
//                    statusPost = [syncManager postLinenItemWithUserId:userId roomAssignId:roomAssignId AndLinenOrderModel:countOrderDetailModelPost];
//
//                }
//                if (statusPost == YES) {
//                    //post success, clear database
//                    if (countOrderDetailModelPost.countOrderDetailId != 0) {
//                        [manager deleteCountOrderDetail:countOrderDetailModelPost];
//                    }
//                }
                //----------------End-----------------------------
                
            }
        }
    }
    
    //post list of PostItem to new WS for Minibar and Linen
    NSArray *postedItems = nil;
    if (countOrder.countServiceId == cMinibar) {
        postedItems = [[NSArray alloc] initWithArray:[syncManager postAllMinibarItemsWithUserId:userId roomAssignId:roomAssignId AndMinibarOrderModelList:postItems WithOrderId:orderId roomNumber:countOrder.countRoomNumber]];
    }
    else {
        postedItems = [[NSArray alloc] initWithArray:[syncManager postAllLinenItemsWithUserId:userId roomAssignId:roomAssignId roomNumber:countOrder.countRoomNumber AndLinenOrderModelList:postItems]];
    }
    
    if (postedItems != nil) {
        for (CountOrderDetailsModelV2 *postedItem in postItems) {
            [manager deleteCountOrderDetail:postedItem];
        }
        [self.sectionDatas removeAllObjects];            
        [self.tbvCartItems reloadData];
        CGRect frame = grandTotalView.frame;
        frame.size.height = 30 * (sectionDatas.count + 2);
        [grandTotalView setFrame:frame];
        frame = tbvGrandTotal.frame;
        frame.size.height = 30 * (sectionDatas.count + 2);
        [tbvGrandTotal setFrame:frame];
        [tbvGrandTotal reloadData];
    }
    
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getMsgSaveMessage]];
    //hide saving data.
    [self performSelector:@selector(hiddenHUDAfterSaved:) withObject:HUD afterDelay:0.5];
    
    if ([delegate respondsToSelector:@selector(saveMiniBarLinenCartItem)]) {
        [delegate saveMiniBarLinenCartItem];
    }

}

#pragma mark - Set Caption View
-(void)setCaptionsView {
    [self setTitle:[[LanguageManagerV2 sharedLanguageManager] getCart]];
    msgbtnNo = [[LanguageManagerV2 sharedLanguageManager] getNo];
    msgbtnYes = [[LanguageManagerV2 sharedLanguageManager] getYes];
    msgSaveCount = [[LanguageManagerV2 sharedLanguageManager] getMsgAlertSaveCounts];
    msgDiscardCount = [[LanguageManagerV2 sharedLanguageManager] getMsgAlertDiscardCounts];
    
    //refresh topview
    TopbarViewV2 *topview = [self getTopBarView];
    [topview refresh:[[SuperRoomModelV2 alloc] init]];
}

#pragma didden after save.
-(void) hiddenHUDAfterSaved:(MBProgressHUD *)HUD{
    [HUD hide:YES];
    [HUD removeFromSuperview];
//    [HUD release];
}


- (void)dealloc {
//    [tbvCartItems release];
//    [grandTotalView release];
//    [tbvGrandTotal release];
//    [super dealloc];
}

#pragma mark - Handle Topbar Methods
-(void)loadTopbarView {
    TopbarViewV2 *topbar = [[TopbarViewV2 alloc] initViewWithSuperModel:[[CommonVariable sharedCommonVariable] superRoomModel]];
    [topbar setTag:tagTopbarView];
    [self.view addSubview:topbar];
    [topbar setHidden:YES];
    
    [topbar setAlpha:0.0];
    [self addButtonHandleShowHideTopbar];
}

-(void)showTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationWillStartSelector:@selector(setHiddenTopbarView)];
    
    f = topbar.frame;
    f.origin.y = 0;
    [topbar setFrame:f];
    [topbar setAlpha:1.0];
    
    [self adjustShowForViews];
    
    [UIView commitAnimations];
}

-(void)hideTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDidStopSelector:@selector(setHiddenTopbarView)];
    [UIView setAnimationDelegate:self];
    
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    [topbar setAlpha:0.0];
    
    [self adjustRemoveForViews];
    
    [UIView commitAnimations];
}

-(void)setHiddenTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    [topbar setHidden:!topbar.hidden];
}

-(TopbarViewV2 *)getTopBarView {
    return (TopbarViewV2 *)[self.view viewWithTag:tagTopbarView];
}

-(void)addButtonHandleShowHideTopbar {
    UIButton *titleBarButton = [[UIButton alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
    
    titleBarButton.titleLabel.backgroundColor = [UIColor clearColor];
    titleBarButton.titleLabel.font = FONT_BUTTON_TOPBAR;
    titleBarButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    titleBarButton.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
    [titleBarButton.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
    [titleBarButton.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
    [titleBarButton addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    [titleBarButton setTitle:[[LanguageManagerV2 sharedLanguageManager] getCart] forState:UIControlStateNormal];
    self.navigationItem.titleView = titleBarButton;
}

-(void)topbarTapped:(UIButton *)sender {
    TopbarViewV2 *topbar = [self getTopBarView];
    [sender setEnabled:NO];
    if (topbar.isHidden) {
        [self showTopbarView];
    } else {
        [self hideTopbarView];
    }
    [self performSelector:@selector(topbarButtonEnable:) withObject:sender afterDelay:0.5];
}

-(void) topbarButtonEnable:(UIButton *) topbarBtn {
    [topbarBtn setEnabled:YES];
}

-(void)adjustShowForViews {
    //adjust all views when show 
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    CGRect ftbv = tbvCartItems.frame;
    [tbvCartItems setFrame:CGRectMake(0, f.size.height, 320, ftbv.size.height - f.size.height)];
}

-(void)adjustRemoveForViews { 
    //adjust all views when hide
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    CGRect ftbv = tbvCartItems.frame;
    [tbvCartItems setFrame:CGRectMake(0, 0, 320, ftbv.size.height + f.size.height)];
}


@end
