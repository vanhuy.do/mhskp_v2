//
//  MiniBarSectionViewV2.h
//  mHouseKeeping
//
//  Created by TMS on 2/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MiniBarCategoryModelV2.h"

@class MiniBarSectionViewV2;

@protocol MiniBarSectionViewV2Delegate <NSObject>

@optional
-(void) sectionHeaderView:(MiniBarSectionViewV2 *) sectionheaderView sectionOpened:(NSInteger) section;
-(void) sectionHeaderView:(MiniBarSectionViewV2 *) sectionheaderView sectionClosed:(NSInteger) section;

@end

@interface MiniBarSectionViewV2 : UIView {
    __unsafe_unretained id<MiniBarSectionViewV2Delegate> delegate;
    NSInteger section;
    BOOL isToggle;
}

@property (nonatomic, assign) NSInteger section;
@property (nonatomic, assign) id<MiniBarSectionViewV2Delegate> delegate;
@property (strong, nonatomic) IBOutlet UIImageView *imgSection;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UIImageView *imgDetail;
- (IBAction)btnSectionViewV2Pressed:(id)sender;
-(id) initWithSection:(NSInteger) section CategoryModelV2:(MiniBarCategoryModelV2 *) categoryModel andOpenStatus:(BOOL) isOpen;


@end
