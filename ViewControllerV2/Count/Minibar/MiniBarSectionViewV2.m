//
//  MiniBarSectionViewV2.m
//  mHouseKeeping
//
//  Created by TMS on 2/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MiniBarSectionViewV2.h"
#import "ehkDefinesV2.h"
#import "UIImage+Tint.h"

@implementation MiniBarSectionViewV2

@synthesize imgSection;
@synthesize lblTitle;
@synthesize imgDetail;
@synthesize section;
@synthesize delegate;

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void) initControls {
    [self setFrame:CGRectMake(0, 0, 320, 60)];
    
    UIImageView *bg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 60)];
    [bg setImage:[UIImage imageBeforeiOS7:imgBgBigBtn equaliOS7:imgBgBigBtnFlat]];
    [self addSubview:bg];
    
    imgSection = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 70, 60)];
    [imgSection setContentMode:UIViewContentModeScaleAspectFit];
    [self addSubview:imgSection];
    
    lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(70, 5, 220, 50)];
    [lblTitle setFont:[UIFont fontWithName:@"Arial-BoldMT" size:17]];
    [lblTitle setBackgroundColor:[UIColor clearColor]];
    [lblTitle setTextColor:[UIColor colorWithRed:55.0/255.0 green:55.0/255.0 blue:55.0/255.0 alpha:1.0]];
    [lblTitle setTextAlignment:NSTextAlignmentCenter];
    [lblTitle setNumberOfLines:2];
    [self addSubview:lblTitle];
    
    imgDetail = [[UIImageView alloc] initWithFrame:CGRectMake(285, 15, 30, 30)];
    [imgDetail setImage:[UIImage imageBeforeiOS7:imgRightRow equaliOS7:imgRightRowFlat]];
    [self addSubview:imgDetail];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(0, 0, 320, 70)];
    [button addTarget:self action:@selector(btnSectionViewV2Pressed:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:button];
}

-(id)init {
    self = [super init];
    if (self) {
        [self initControls];
    }
    return self;
}

-(id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initControls];
    }
    return self;
}

- (void)dealloc {
//    [imgSection release];
//    [lblTitle release];
//    [imgDetail release];
//    [super dealloc];
}

-(void) toggleOpenWithUserAction:(BOOL) userAction {
    if (userAction) {
        if (isToggle == NO) {
            if ([delegate respondsToSelector:@selector(sectionHeaderView:sectionOpened:)]) {
                isToggle = YES;
                [delegate sectionHeaderView:self sectionOpened:section];
                
                [imgDetail setImage:[UIImage imageBeforeiOS7:imgRightRowOpen equaliOS7:imgRightRowOpenFlat]];
            }
        }else {
            if ([delegate respondsToSelector:@selector(sectionHeaderView:sectionClosed:)]) {
                isToggle = NO;
                [imgDetail setImage:[UIImage imageBeforeiOS7:imgRightRow equaliOS7:imgRightRowFlat]];
                [delegate sectionHeaderView:self sectionClosed:section];
            }
        }
    }
}

- (IBAction)btnSectionViewV2Pressed:(id)sender {
    [self toggleOpenWithUserAction:YES];
}

-(id)initWithSection:(NSInteger )sectionIndex CategoryModelV2:(MiniBarCategoryModelV2 *)categoryModel andOpenStatus:(BOOL)isOpen {
    self = [self init];
    
    if (self) {
        section = sectionIndex;
        lblTitle.text = categoryModel.minibarCategoryTitle;
        isToggle = isOpen;
        [imgSection setImage:[UIImage imageWithData:categoryModel.minibarCategoryImage]];
        if (isOpen) {
            [imgDetail setImage:[UIImage imageBeforeiOS7:imgRightRowOpen equaliOS7:imgRightRowOpenFlat]];
        } else {
            [imgDetail setImage:[UIImage imageBeforeiOS7:imgRightRow equaliOS7:imgRightRowFlat]];
        }
    }
    
    return self;
}

@end
