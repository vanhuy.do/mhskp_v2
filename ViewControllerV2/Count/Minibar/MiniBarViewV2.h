//
//  MiniBarViewV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ZXingWidgetController.h>
#import "MiniBarSectionViewV2.h"
#import "MiniBarItemCellV2.h"
#import "PickerViewV2.h"
#import "MiniBarSectionInfoV2.h"
#import "MiniBarItemModelV2.h"
#import "CountCategoryModelV2.h"
#import "MiniBarLinenCartViewV2.h"
#import "SyncManagerV2.h"
#import "MBProgressHUD.h"
#import "AccessRight.h"

@interface MiniBarViewV2 : UIViewController <UITableViewDelegate, UITableViewDataSource, MiniBarSectionViewV2Delegate, MiniBarItemCellV2Delegate, PickerViewV2Delegate, MiniBarLinenCartViewV2Delegate, UITextFieldDelegate, UIScrollViewDelegate, ZXingDelegate, MBProgressHUDDelegate> {
    NSMutableArray *sectionDatas;
//    UIButton *cartButton;
    NSMutableArray *datasLaundryCart;
    NSInteger *roomNumber;
    UITextField *txtRoom;
    UIButton *btnQR;
    BOOL isFromMainPosting;
    UIView *viewRoom;
    
    IBOutlet UILabel *lblRoomNo;
    IBOutlet UILabel *lblFSubmit;
    NSInteger logCheckUsedValue;
    NSInteger logCheckNewValue;
    IBOutlet UIScrollView *scrollView;
    IBOutlet UIButton *btnSubmit;
    
    __weak IBOutlet UIButton *btnPostingHistory;
    AccessRight *QRCodeScanner;
    AccessRight *postingMinibar;
    AccessRight *actionMinibar;
    CountOrdersModelV2 *countOrder;
    BOOL haveOldCountOrders;
    NSInteger isPostingFunctionMinibarNew, isPosingFunctionMinibarUsed, isRoomDetailsActionMinibarNew, isRoomDetailsActionMinibarUsed;
    IBOutlet UIImageView *imgHeader;
}

@property (nonatomic, strong) NSMutableArray *datasLaundryCart;
//@property (nonatomic, strong) UIButton *cartButton;
@property (strong, nonatomic) IBOutlet UITableView *tbvMinibar;
@property (nonatomic, strong) NSMutableArray *sectionDatas;
@property (strong, nonatomic) IBOutlet UIButton *btnAddToCart;
@property (assign, nonatomic) NSInteger countServiceId;
@property (assign, nonatomic) BOOL statusSave;
@property (assign, nonatomic) BOOL statusChooseMinibar;
@property (assign, nonatomic) NSInteger sumMinibarUsed;
@property (strong, nonatomic) NSString *msgbtnYes;
@property (strong, nonatomic) NSString *msgbtnNo;
@property (strong, nonatomic) NSString *msgSaveCount;
@property (strong, nonatomic) NSString *msgDiscardCount;
@property (assign, nonatomic) BOOL statusCheckSaveData;
@property (assign, nonatomic) BOOL statusAddToCart;
@property (assign, nonatomic) NSInteger *roomNumber;
@property (strong, nonatomic) IBOutlet UITextField *txtRoom;
@property (strong, nonatomic) IBOutlet UIButton *btnQR;
@property (nonatomic) BOOL isFromMainPosting;
@property (strong, nonatomic) IBOutlet UIView *viewRoom;
@property (assign, nonatomic) NSInteger roomTypeId;

- (IBAction)btnAddToCartPressed:(id)sender;
-(IBAction) btnQRCodeScannerTouched:(id) sender;
-(void) loadingDatas;
-(void) setCaptionView;
-(void) handleBtnAddToCartPressed;
-(IBAction) btnSubmit_Clicked:(id)sender;
- (IBAction)btnPostingHistory_Clicked:(UIButton *)sender;

@end
