//
//  MiniBarViewV2.m
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MiniBarViewV2.h"
#import "MiniBarLinenCartViewV2.h"
#import "CountManagerV2.h"
#import "UserManagerV2.h"
#import "RoomManagerV2.h"
#import "MBProgressHUD.h"
#import "CustomAlertViewV2.h"
#import "CommonVariable.h"
#import "TasksManagerV2.h"
#import "NetworkCheck.h"
#import "RoomAssignmentInfoViewController.h"
#import "CountHistoryManager.h"
#import "CountHistoryModel.h"
#import "MyNavigationBarV2.h"
#import "QRCodeReader.h"
//#import "HomeViewV2.h"
#import "LogFileManager.h"
#import "DeviceManager.h"
#import "iToast.h"
#import "HomeViewV2.h"
#import "DTAlertView.h"
#import "STEncryptorDES.h"
#import "UIImage+Tint.h"
#import "NSString+Common.h"
#import "ehkDefines.h"
#import "HistoryPosting.h"

#define tagAlertBack 10
#define tagAlertNo 11
#define tagAlertSubmit 12
#define tagNoNetwork 61
#define tagUnpostData 62
#define tagHudPostAllOrders 63

//Max item for choose
#define maxItem 10
#define heightMinibarCell 70
#define heightMinibarHeader 60

typedef enum {
    tUsed = 1,
    tNew,
}tType;

@interface MiniBarViewV2(PrivateMethods) <DTAlertViewDelegate>

-(void)cartButtonPressed;
-(void) loadTopbarView;
-(void) showTopbarView;
-(void) hideTopbarView;
-(void) setHiddenTopbarView;
-(void) adjustShowForViews;
-(void) adjustRemoveForViews;
-(void) addButtonHandleShowHideTopbar;
-(void) topbarTapped:(UIButton *) sender;
-(TopbarViewV2 *) getTopBarView;

@end

@implementation MiniBarViewV2
@synthesize btnAddToCart;
@synthesize tbvMinibar;
@synthesize sectionDatas;
//@synthesize cartButton;
@synthesize datasLaundryCart;
@synthesize countServiceId;
@synthesize statusSave;
@synthesize statusChooseMinibar;
@synthesize sumMinibarUsed;
@synthesize msgbtnNo;
@synthesize msgbtnYes;
@synthesize msgSaveCount;
@synthesize msgDiscardCount;
@synthesize statusCheckSaveData;
@synthesize statusAddToCart;
@synthesize roomNumber;
@synthesize txtRoom;
@synthesize btnQR;
@synthesize isFromMainPosting;
@synthesize viewRoom;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        sectionDatas = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - Load Minibar from service
-(void) loadDataAfterDelayWithHUD:(MBProgressHUD *) HUD {
    
    
    //[self loadingDatas];
    
    [self handleBtnAddToCartPressed];
    
    //hide saving data.
    [self performSelector:@selector(hiddenHUDAfterSaved:) withObject:HUD afterDelay:0.5];
}

-(void) loadDataMinibarFormService:(MBProgressHUD *) HUD {
    //enable flag to announce, is loading data
    [[HomeViewV2 shareHomeView] waitingLoadingData];
    
    CountManagerV2 *countManager = [[CountManagerV2 alloc] init];
    //Delete all temporate order data
    [countManager deleteCountOrdersByPostStatus:(int)POST_STATUS_UN_CHANGED serviceId:(int)countServiceId];
    
    int countUnpost = [countManager countUnpostOrderItemsByUserId:[UserManagerV2 sharedUserManager].currentUser.userId serviceId:(int)countServiceId];
    if(countUnpost > 0) {
        haveOldCountOrders = YES;
        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:@"" message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_notify_unpost_data] delegate:self cancelButtonTitle:msgbtnYes otherButtonTitles:msgbtnNo, nil];
        alert.delegate = self;
        [alert setTag:tagUnpostData];
        [alert show];
        
    } else {
        haveOldCountOrders = NO;
        SyncManagerV2 *synManager = [[SyncManagerV2 alloc] init];
        
        NSInteger userId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
        NSInteger hotelId = [[[UserManagerV2 sharedUserManager] currentUser] userHotelsId];
        
        //-----------WS------------
        [synManager getMinibarCategoryListWithUserId:userId AndHotelId:hotelId];
        [synManager getMinibarItemListWithUserId:userId AndHotelId:hotelId];
        if ([[UserManagerV2 sharedUserManager].currentUserAccessRight isLoadMinibarByRoomType]) {
            [synManager getRoomTypeInventoryWSByServiceType:ServiceType_Minibar userId:userId hotelId:hotelId];
        }
        //-----------end-----------
        
        [self loadingDatas];
        
        //Hao Tran - Remove for speed up show view
        //[self handleBtnAddToCartPressed];
        //Hao Tran - END
    }
    
    //hide saving data.
    [self performSelector:@selector(hiddenHUDAfterSaved:) withObject:HUD afterDelay:0.0f];
    
    
    //end loading data
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
}


-(void)loadViewData
{
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getRaLoadingData]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES];
    
    //enable flag to announce, is loading data
    [[HomeViewV2 shareHomeView] waitingLoadingData];
    
    SyncManagerV2 *synManager = [[SyncManagerV2 alloc] init];
    
    NSInteger userId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    NSInteger hotelId = [[[UserManagerV2 sharedUserManager] currentUser] userHotelsId];
    
    //-----------WS------------
    [synManager getMinibarCategoryListWithUserId:userId AndHotelId:hotelId];
    [synManager getMinibarItemListWithUserId:userId AndHotelId:hotelId];
    if ([[UserManagerV2 sharedUserManager].currentUserAccessRight isLoadMinibarByRoomType]) {
        [synManager getRoomTypeInventoryWSByServiceType:ServiceType_Minibar userId:userId hotelId:hotelId];
    }
    //-----------end-----------
    
    [self loadingDatas];
    
    //hide saving data.
    [self performSelector:@selector(hiddenHUDAfterSaved:) withObject:HUD afterDelay:0.0f];
    
    //end loading data
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
}

-(void)postAllOldCountOrderData
{
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    HUD.delegate = self;
    HUD.tag = tagHudPostAllOrders;
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getRaLoadingData]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES];
    
    //enable flag to announce, is loading data
    [[HomeViewV2 shareHomeView] waitingLoadingData];
    
    CountManagerV2 *countManager = [[CountManagerV2 alloc] init];
    int result = [countManager postAllPendingCountDataByUserId:[UserManagerV2 sharedUserManager].currentUser.userId serviceId:(int)countServiceId];
    if (result > 0) {
        //success
        [[[[iToast makeText:[[LanguageManagerV2 sharedLanguageManager] getMsgSaveMessage]] setGravity:iToastGravityBottom] setDuration:iToastDurationNormal] show];
    }else{
        //failed
        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:@"" message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_internet_exception] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getOK] otherButtonTitles:nil];
        alert.delegate = self;
        [alert setTag:tagNoNetwork];
        [alert show];
    }
    //hide saving data.
    [self performSelector:@selector(hiddenHUDAfterSaved:) withObject:HUD afterDelay:0.0f];
    
    //end loading data
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
}

-(void)hudWasHidden:(MBProgressHUD *)hud
{
    if(hud.tag == tagHudPostAllOrders)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void) reachabilityDidChanged: (NSNotification* )note
{
    if(![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]){
        [btnSubmit setEnabled:NO];
        [self setEnableBtnAtionHistory:NO];
    }
    else{
        [btnSubmit setEnabled:YES];
        [self setEnableBtnAtionHistory:YES];
    }
}

#pragma mark - View lifecycle
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if(!isDemoMode){
        //show wifi view
        [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationShowWifiView] object:nil];
        if(![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]){
            [btnSubmit setEnabled:NO];
        }
        else{
            [btnSubmit setEnabled:YES];
        }
    }
    if(isFromMainPosting){
        if(!isDemoMode){
            [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(reachabilityDidChanged:) name: kReachabilityChangedNotification object: nil];
        }
        [[HomeViewV2 shareHomeView] setSelectedButton:tagOfPostingButton];
    } else{
        [[HomeViewV2 shareHomeView] setSelectedButton:tagOfHomeButton];
    }
    
    [self setCaptionView];
    
    //Hao Tran Remove because view is update unpectedly
    //    MBProgressHUD	*HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    //    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getRaLoadingData]];
    //    [self.tabBarController.view addSubview:HUD];
    //    [HUD show:YES];
    //
    //    [self performSelector:@selector(loadDataAfterDelayWithHUD:) withObject:HUD afterDelay:0.1];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    if(!isDemoMode){
        [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    CGRect frameRoomNumber = viewRoom.frame;
    [scrollView setFrame:CGRectMake(0,frameRoomNumber.origin.x + frameRoomNumber.size.height , scrollView.frame.size.width, scrollView.frame.size.height)];
}
- (void)enableQRCode:(BOOL)isEnable{
    [btnQR setAlpha:isEnable ? 1.0f : 0.7f];
    [btnQR setEnabled:isEnable];
}
//CRF1619
- (void)loadAccessQRCode{
    if ([UserManagerV2 sharedUserManager].currentUserAccessRight.isPostingFunctionMinibarQRCode == 1) {
        self.txtRoom.enabled = NO;
        [self enableQRCode:YES];
    }else if ([UserManagerV2 sharedUserManager].currentUserAccessRight.isPostingFunctionMinibarQRCode == 0) {
        self.txtRoom.enabled = YES;
        [self enableQRCode:NO];
    }
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self loadAccessRights];
    if(!ENABLE_QRCODESCANNER)
    {
        if(ENABLE_CHECK_ACCESS_RIGHT_QRCODESCANNER)
        {
            [btnQR setHidden:NO];
            [self enableQRCode:NO];
            if (QRCodeScanner.isActive && !isDemoMode) {
                [self enableQRCode:YES];
            }
        }
        else
        {
            [btnQR setHidden:YES];
        }
    }
    
    //Set Enable button submit
    [btnSubmit setEnabled:![self isOnlyViewedAccessRight]];
    
    if([LogFileManager isLogConsole])
    {
        NSInteger userId = [[UserManagerV2 sharedUserManager] currentUser].userId;
        NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSLog(@"<%@><Minibar><UserId:%d><viewDidLoad>", time, (int)userId);
    }
    scrollView.delegate = self;
    [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, scrollView.frame.size.height)];
    
    //Resize content views of current VC to fit with Navigation bar and tabbar
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        [self loadFlatResource];
    }
    
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getRaLoadingData]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES];
    
    [self performSelector:@selector(loadDataMinibarFormService:) withObject:HUD afterDelay:0.0f];
    //[self.tbvMinibar setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    [tbvMinibar setBackgroundView:nil];
    [tbvMinibar setBackgroundColor:[UIColor clearColor]];
    [tbvMinibar setOpaque:YES];
    statusSave = NO;
    
    //    cartButton = [[UIButton alloc]initWithFrame:CGRectMake(236, 6, 60, 32)];
    //    [cartButton setBackgroundImage:[UIImage imageNamed:imgTopCart] forState:UIControlStateNormal];
    //    [cartButton setTitle:[NSString stringWithFormat:@"%d", sumMinibarUsed] forState:UIControlStateNormal];
    //    [cartButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 32, 0, 0)];
    //    [cartButton.titleLabel setFont:[UIFont fontWithName:@"Arial-BoldMT" size:15]];
    //    [cartButton addTarget:self action:@selector(cartButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    //    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithCustomView:cartButton];
    //    self.navigationItem.rightBarButtonItem = rightButton;
    
    //back button navigation
    self.navigationItem.leftBarButtonItem = [MyNavigationBarV2 createBackButtonWithTarget:self selector:@selector(backBarPressed) forControlEvents:UIControlEventTouchUpInside];
    
    //[self performSelector:@selector(loadTopbarView)];
    
    if(isFromMainPosting)
    {
        [txtRoom setText:@""];
        _roomTypeId = 0;
        [txtRoom setEnabled:YES];
        [txtRoom setTextColor:[UIColor blackColor]];
        //CRF1619
        [self loadAccessQRCode];
        btnPostingHistory.hidden = ![UserManagerV2 sharedUserManager].currentUserAccessRight.isShowActionHistoryPosting;
    }
    else
    {
        int roomAssignment = (int)[TasksManagerV2 getCurrentRoomAssignment];
        RoomAssignmentModelV2 *roomAssignmentModel = [[RoomAssignmentModelV2 alloc] init];
        roomAssignmentModel.roomAssignment_Id = roomAssignment;
        [[RoomManagerV2 sharedRoomManager]getRoomIdByRoomAssignment:roomAssignmentModel];
        [txtRoom setText:roomAssignmentModel.roomAssignment_RoomId];
        [txtRoom setEnabled:NO];
        [txtRoom setTextColor:[UIColor grayColor]];
    }
    
    [self loadTopbarView];
    
    //Set title for label
    [lblRoomNo setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOM_NO]];
    [lblFSubmit setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_SUBMIT]];
    
    [btnSubmit setFrame:CGRectMake(10, 20 + tbvMinibar.frame.size.height, 300, 64)];
    [btnPostingHistory setFrame:CGRectMake(10, 30 + tbvMinibar.frame.size.height + CGRectGetHeight(btnSubmit.bounds), 300, 55)];
    [lblFSubmit setFrame:CGRectMake(121, 30 + tbvMinibar.frame.size.height, 135, 39)];
    [self setEnableBtnAtionHistory:NO];
}

#pragma mark - Click Back
-(void)backBarPressed {
    //clear previous tag of event
//    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:TAG_OF_EVENT];
    
//    if(statusChooseMinibar == NO)
//    {
        [self.navigationController popViewControllerAnimated:YES];
//    }
//    else
//    {
//        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:msgSaveCount delegate:self cancelButtonTitle:msgbtnYes otherButtonTitles:msgbtnNo, nil];
//        alert.delegate = self;
//        alert.tag = tagAlertBack;
//        [alert show];
//        
//    }
}

#pragma mark - === Handle Check Save Data ===
#pragma mark
-(BOOL) checkSaveDataWithTagEvent:(NSInteger) tagOfEvent {
    
    if (tagOfEvent == tagOfMessageButton) {
        if (statusChooseMinibar == NO) {
            
            return NO;
        }
        else
        {
            //Show alert please save before leaving
            UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:msgSaveCount delegate:self cancelButtonTitle:msgbtnYes otherButtonTitles:msgbtnNo, nil];
            alert.delegate = self;
            alert.tag = tagAlertBack;
            [alert show];
            
            return YES;
            
        }
    }
    else
    {
        //Show alert complete room
        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_complete_room_to_leave] delegate:self cancelButtonTitle:msgbtnYes otherButtonTitles: nil];
        alert.delegate = self;
        [alert show];
        
        return YES;
        
    }
    
}
- (void)deleteAllUnPostData{
    int userId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    CountManagerV2 *manager = [[CountManagerV2 alloc] init];
    CountOrderDetailAdapterV2 *countOrderDetailAdapter = [[CountOrderDetailAdapterV2 alloc] init];
    NSMutableArray *listCountOrderPending = [manager loadAllCountOrdersByUserId:userId AndServiceId:countServiceId];
    for (CountOrdersModelV2 *curCountOrder in listCountOrderPending) {
        
        if(curCountOrder.countPostStatus == POST_STATUS_UN_CHANGED){
            continue;
        }
        
        NSMutableArray *listCountDetails = [countOrderDetailAdapter loadAllCountOrderDetailByOrderId:curCountOrder.countOrderId];
        NSInteger totalQuantity = 0;
        
        //save minibar or linen detail
        NSMutableArray *postItems = [[NSMutableArray alloc] init];
        for (CountOrderDetailsModelV2 *curDetail in listCountDetails) {
            if (curDetail.countCollected != 0 || curDetail.countNew != 0) {
                totalQuantity += curDetail.countCollected;
            }
            [postItems addObject:curDetail];
        }
        CountHistoryManager *countHistoryManager = [[CountHistoryManager alloc]init];
        CountHistoryModel *countHistoryModel = [[CountHistoryModel alloc]init];
        
        for (CountOrderDetailsModelV2 *postedItem in postItems) {
            
            countHistoryModel.room_id = curCountOrder.countRoomNumber;
            countHistoryModel.count_item_id = postedItem.countItemId;
            countHistoryModel.count_collected = postedItem.countCollected;
            countHistoryModel.count_new = postedItem.countNew;
            countHistoryModel.count_service = curCountOrder.countServiceId;
            countHistoryModel.count_date = postedItem.countCollectedDate;
            countHistoryModel.count_user_id = userId;
            countHistoryModel.count_id = curCountOrder.countOrderId;
            [countHistoryManager insertCountHistoryData:countHistoryModel];
        }
        
        [manager deleteCountOrderDetailsByOrderId:(int)curCountOrder.countOrderId];
        [manager deleteCountOrderByOrderId:(int)curCountOrder.countOrderId];
    }
}
#pragma mark - UIAlertView Delegate Methods
-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    switch (alertView.tag) {
        case tagUnpostData:
        {
            if(buttonIndex == 0) {
                //YES:
                [self postAllOldCountOrderData];
            } else {
                //NO:
//                [self loadViewData];
                //must be delete all minibar post older
                [self deleteAllUnPostData];
                [self loadViewData];
                
            }
        }
            break;
        case tagNoNetwork:
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
            break;
        case tagAlertBack:
        {
            switch (buttonIndex)
            {
                case 0:
                {
                    //YES: save minibar
                    [self saveMinibarCart];
                    [self btnAddToCartPressed:btnAddToCart];
                    statusChooseMinibar = NO;
                    
                    //if tag is message
                    if ([[NSUserDefaults standardUserDefaults] integerForKey:TAG_OF_EVENT] == tagOfMessageButton) {
                        [[HomeViewV2 shareHomeView] handleTabbarEventWithTagOfButton:[[NSUserDefaults standardUserDefaults] integerForKey:TAG_OF_EVENT]];
                    }
                    else
                    {
                        [self.navigationController popViewControllerAnimated:YES];
                    }
                    
                }
                    break;
                    
                case 1:
                {
                    //NO: show popup discard
                    UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:msgDiscardCount delegate:self cancelButtonTitle:msgbtnYes otherButtonTitles:msgbtnNo, nil];
                    alert.delegate = self;
                    alert.tag = tagAlertNo;
                    [alert show];
                    
                }
                    break;
                    
                default:
                    break;
            }
        }
            break;
            
        case tagAlertNo:
        {
            switch (buttonIndex)
            {
                case 0:
                {
                    //YES
                    [datasLaundryCart removeAllObjects];
                    statusChooseMinibar = NO;
                    //if tag is message
                    if ([[NSUserDefaults standardUserDefaults] integerForKey:TAG_OF_EVENT] == tagOfMessageButton) {
                        [[HomeViewV2 shareHomeView] handleTabbarEventWithTagOfButton:[[NSUserDefaults standardUserDefaults] integerForKey:TAG_OF_EVENT]];
                    }
                    else{
                        //back to count menu, no save
                        [self.navigationController popViewControllerAnimated:YES];
                    }
                }
                    break;
                    
                case 1:
                {
                    //NO: cart screen
                    //[self btnAddToCartPressed:nil];
                    //[self cartButtonPressed];
                }
                    break;
                    
                default:
                    break;
            }
            
        }
            break;
            
        case tagAlertSubmit:
        {
            switch (buttonIndex)
            {
                case 0:
                {
                    //YES
                    //-------------Log----------
                    if([LogFileManager isLogConsole])
                    {
                        NSInteger userId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
                        NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
                        if (logCheckNewValue == 0 && logCheckUsedValue == 0) {
                            NSLog(@"<%@><Minibar><UserId:%d><Summit to post><input RoomId:%@>", time, (int)userId, txtRoom.text);
                        }
                        else {
                            NSInteger totalSections = [self numberOfSectionsInTableView:tbvMinibar];
                            
                            for (int i = 0; i<totalSections; i++) {
                                NSInteger totalRow = [self tableView:self.tbvMinibar numberOfRowsInSection:i];
                                for (int j = 0; j<totalRow; j++) {
                                    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:j inSection:i];
                                    UITableViewCell *cell = [self.tbvMinibar cellForRowAtIndexPath:indexPath];
                                    UILabel *lblItemName = (UILabel *)[cell viewWithTag:tagItemNameCell];
                                    UIButton *btnUsed = (UIButton *)[cell viewWithTag:tagUsedButtonCell];
                                    
                                    UIButton *btnNew = (UIButton *)[cell viewWithTag:tagNewButtonCell];
                                    NSString *valueBtnUsed = btnUsed.titleLabel.text;
                                    NSString *valueBtnNew = btnNew.titleLabel.text;
                                    if ([valueBtnNew isEqualToString:@"0"] && [valueBtnUsed isEqualToString:@"0"]) {
                                        
                                    }
                                    else {
                                        NSLog(@"<%@><Minibar><UserId:%d><Submit to post><Input RoomId:%@><ItemName:%@><ItemUsed:%@><ItemNew:%@>", time, (int)userId, txtRoom.text, lblItemName.text, valueBtnUsed, valueBtnNew);
                                    }
                                }
                            }
                        }
                    }
                    //-------------Log----------
                    
                    [self handleBtnAddToCartPressed];
                    int resultCode = [self saveMinibarCart];
                    if(isFromMainPosting)
                    {
                        [txtRoom setText:@""];
                        [tbvMinibar reloadData];
                        [datasLaundryCart removeAllObjects];
                        //[self.navigationController popViewControllerAnimated:YES];
                    }
                    else
                    {
                        //response status different to RESPONSE_STATUS_OK will show alert view in saveMinibarCart
                        if(resultCode == RESPONSE_STATUS_OK) {
                            NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
                            for (UIViewController *aViewController in allViewControllers) {
                                if ([aViewController isKindOfClass:[RoomAssignmentInfoViewController class]]) {
                                    ((RoomAssignmentInfoViewController*) aViewController).isAlreadyPostedMiniBar = YES;
                                    [self.navigationController popToViewController:aViewController animated:YES];
                                }
                            }
                        }
                    }
                    
                }
                    break;
                    
                case 1:
                {
                    if([LogFileManager isLogConsole])
                    {
                        NSInteger userId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
                        NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
                        if (logCheckNewValue == 0 && logCheckUsedValue == 0) {
                            NSLog(@"<%@><Minibar><UserId:%d><Not confirm to post><input RoomId:%@>", time, (int)userId, txtRoom.text);
                        }
                        else {
                            NSInteger totalSections = [self numberOfSectionsInTableView:tbvMinibar];
                            
                            for (int i = 0; i<totalSections; i++) {
                                NSInteger totalRow = [self tableView:self.tbvMinibar numberOfRowsInSection:i];
                                for (int j = 0; j<totalRow; j++) {
                                    
                                    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:j inSection:i];
                                    UITableViewCell *cell = [self.tbvMinibar cellForRowAtIndexPath:indexPath];
                                    
                                    UILabel *lblItemName = (UILabel *)[cell viewWithTag:tagItemNameCell];
                                    UIButton *btnUsed = (UIButton *)[cell viewWithTag:tagUsedButtonCell];
                                    UIButton *btnNew = (UIButton *)[cell viewWithTag:tagNewButtonCell];
                                    
                                    NSString *valueBtnUsed = btnUsed.titleLabel.text;
                                    NSString *valueBtnNew = btnNew.titleLabel.text;
                                    if ([valueBtnNew isEqualToString:@"0"] && [valueBtnUsed isEqualToString:@"0"]) {
                                        
                                    }
                                    else {
                                        NSLog(@"<%@><Minibar><UserId:%d><Not confirm to post><Input RoomId:%@><ItemName:%@><ItemUsed:%@><ItemNew:%@>", time, (int)userId, txtRoom.text, lblItemName.text, valueBtnUsed, valueBtnNew);
                                    }
                                }
                            }
                        }
                    }
                }
                    break;
                    
                default:
                    break;
            }
            
        }
            break;
            
        default:
            break;
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    
    [self setTbvMinibar:nil];
    [self setBtnAddToCart:nil];
    
    CountManagerV2 *manager = [[CountManagerV2 alloc] init];
    [manager deleteCountOrderByOrderId:(int)countOrder.countOrderId];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc {
    //    [tbvMinibar release];
    //    [btnAddToCart release];
    //    [super dealloc];
}

#pragma mark - Add to Cart Press
-(void)handleBtnAddToCartPressed
{
    if (datasLaundryCart == nil) {
        datasLaundryCart = [[NSMutableArray alloc] init];
    }
    
    
    for (NSInteger indexSection=0; indexSection < sectionDatas.count; indexSection ++) {
        MiniBarSectionInfoV2 *sectionInfo = [sectionDatas objectAtIndex:indexSection];
        
        MiniBarSectionInfoV2 *sectionCart = nil;
        BOOL isExistingSectionInCart = NO;
        
        for (NSInteger indexRow=0; indexRow < sectionInfo.rowHeights.count; indexRow++) {
            MiniBarItemModelV2 *model = [sectionInfo.rowHeights objectAtIndex:indexRow];
            
            BOOL isExistingInCart = NO;
            
            for (MiniBarSectionInfoV2 *sectionInCart in datasLaundryCart) {
                if (sectionInCart.minibarCategory.minibarCategoryID == sectionInfo.minibarCategory.minibarCategoryID) {
                    isExistingSectionInCart = YES;
                    sectionCart = sectionInCart;
                    //check items in same category, if item is existing in this category, just update number, if not insert new item in this cart
                    for (MiniBarItemModelV2 *itemInCart in sectionInCart.rowHeights) {
                        if (itemInCart.minibarItemID == model.minibarItemID) {
                            isExistingInCart = YES;
                            if (model.minibarItemUsed > 0) {
                                itemInCart.minibarItemUsed += model.minibarItemUsed;
                            } else {
                                if (model.minibarItemNew > 0) {
                                    itemInCart.minibarItemNew += model.minibarItemNew;
                                }
                            }
                            break;
                        }
                    }
                }
            }
            
            if (isExistingSectionInCart == NO) {
                if (sectionCart == nil) {
                    sectionCart = [[MiniBarSectionInfoV2 alloc] init];
                }
            }
            
            if (isExistingInCart == NO) {
                if (model.minibarItemUsed > 0) {
                    MiniBarItemModelV2 *copyModel = [[MiniBarItemModelV2 alloc] init];
                    copyModel.minibarItemID = model.minibarItemID;
                    copyModel.minibarItemImage = model.minibarItemImage;
                    copyModel.minibarItemNew = model.minibarItemNew;
                    copyModel.minibarItemTitle = model.minibarItemTitle;
                    copyModel.minibarItemUsed = model.minibarItemUsed;
                    [sectionCart insertObjectToNextIndex:copyModel];
                } else {
                    if (model.minibarItemNew > 0) {
                        MiniBarItemModelV2 *copyModel = [[MiniBarItemModelV2 alloc] init];
                        copyModel.minibarItemID = model.minibarItemID;
                        copyModel.minibarItemImage = model.minibarItemImage;
                        copyModel.minibarItemNew = model.minibarItemNew;
                        copyModel.minibarItemTitle = model.minibarItemTitle;
                        copyModel.minibarItemUsed = model.minibarItemUsed;
                        [sectionCart insertObjectToNextIndex:copyModel];
                    }
                }
            }
            
            //Hao Tran Remove
            //model.minibarItemNew = 0;
            //model.minibarItemUsed = 0;
            
        }
        if (sectionCart.rowHeights.count > 0 && isExistingSectionInCart == NO) {
            sectionCart.minibarCategory = sectionInfo.minibarCategory;
            [datasLaundryCart addObject:sectionCart];
        }
    }
    if (datasLaundryCart.count <= 0) {
        statusSave = NO;
    }
    
    //    NSInteger countNew = 0;
    //    NSInteger countUsed = 0;
    //    for (NSInteger indexSection=0; indexSection < datasLaundryCart.count; indexSection ++) {
    //        MiniBarSectionInfoV2 *sectionInfo = [datasLaundryCart objectAtIndex:indexSection];
    //        for (NSInteger indexRow=0; indexRow < sectionInfo.rowHeights.count; indexRow++) {
    //            MiniBarItemModelV2 *model = [sectionInfo.rowHeights objectAtIndex:indexRow];
    //            if (model.minibarItemNew > 0) {
    //                countNew += model.minibarItemNew;
    //            }
    //            if (model.minibarItemUsed > 0) {
    //                countUsed += model.minibarItemUsed;
    //            }
    //        }
    //    }
    
    //    [cartButton setTitle:[NSString stringWithFormat:@"%d", countUsed] forState:UIControlStateNormal];
    
}

- (IBAction)btnAddToCartPressed:(id)sender {
    statusSave = YES;
    statusChooseMinibar = YES;
    statusAddToCart = YES;
    [self handleBtnAddToCartPressed];
    [tbvMinibar reloadData];
    
}

#pragma mark - Load Minibar
-(void)loadingDatas {
    
    CountManagerV2 *manager = [[CountManagerV2 alloc] init];
    sectionDatas = [[NSMutableArray alloc] init];
    
    //Count Order
    countOrder = [[CountOrdersModelV2 alloc] init];
    NSInteger roomAssignId = [TasksManagerV2 getCurrentRoomAssignment];
    
    //Room AssignId = 0 if come from main posting
    if(isFromMainPosting){
        roomAssignId = 0;
    }
    countOrder.countRoomAssignId = roomAssignId;
    countOrder.countServiceId = countServiceId;
    countOrder.countStatus = tStatusChecked;
    countOrder.countUserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    countOrder.countCollectDate = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
    countOrder.countPostStatus = POST_STATUS_UN_CHANGED;
    
    countOrder.countRoomNumber = txtRoom.text;
    
    //Hao Tran - Remove for SG00054442
    //    BOOL isCheckCountOrder = [manager isCheckCountOrder:countOrder];
    //    if (isCheckCountOrder == TRUE) {
    //        [manager loadCountOrdersModelByRoomIdUserIdAndServiceId:countOrder];
    //    }
    
    [manager insertCountOrder:countOrder];
    countOrder =  [manager loadCountOrdersModelByRoomAssignId:(int)countOrder.countRoomAssignId roomNumber:countOrder.countRoomNumber userId:(int)countOrder.countUserId serviceId:(int)countOrder.countServiceId postStatus:(int)POST_STATUS_UN_CHANGED];
    
    //Hao Tran - END
    
    NSMutableArray *array;
    if ([[UserManagerV2 sharedUserManager].currentUserAccessRight isLoadMinibarByRoomType]) {
        [self getRoomTypeByRoomNo];
        array = [manager loadAllCountCategoryByCountServiceId:countServiceId andRoomTypeId:_roomTypeId];
    } else {
        array = [manager loadAllCountCategoryByCountServiceId:countServiceId];
    }
    
    for (CountCategoryModelV2 *countCategories in array) {
        
        MiniBarSectionInfoV2 *sectionInfo = [[MiniBarSectionInfoV2 alloc] init];
        
        MiniBarCategoryModelV2 *categoryModel = [[MiniBarCategoryModelV2 alloc] init];
        categoryModel.minibarCategoryID = countCategories.countCategoryId;
        //set language for name minibar category
        if ([[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString: (NSString *)ENGLISH_LANGUAGE] == NO) {
            categoryModel.minibarCategoryTitle = countCategories.countCategoryNameLang;
        }
        else{
            categoryModel.minibarCategoryTitle = countCategories.countCategoryName;
        }
        categoryModel.minibarCategoryImage = UIImagePNGRepresentation([UIImage imageWithData:countCategories.countCategoryImage]);
        sectionInfo.minibarCategory = categoryModel;
        
        NSMutableArray *arrayCountItem;
        if ([[UserManagerV2 sharedUserManager].currentUserAccessRight isLoadMinibarByRoomType]) {
            arrayCountItem = [manager loadAllCountItemByCountCategoryId:countCategories.countCategoryId andRoomTypeId:_roomTypeId];
        } else {
            arrayCountItem = [manager loadAllCountItemByCountCategoryId:countCategories.countCategoryId];
        }
        
        for (CountItemsModelV2 *countItems in arrayCountItem) {
            
            CountOrderDetailsModelV2 *countOrderDetail = [manager loadCountOrderDetailsByItemId:countItems.countItemId AndOrderId:countOrder.countOrderId];
            sumMinibarUsed += countOrderDetail.countCollected;
            
            MiniBarItemModelV2 *itemModel = [[MiniBarItemModelV2 alloc] init];
            itemModel.minibarItemID = countItems.countItemId;
            //set language for name minibar item
            if ([[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString: (NSString *)ENGLISH_LANGUAGE] == NO) {
                itemModel.minibarItemTitle = countItems.countItemLang;
            }
            else{
                itemModel.minibarItemTitle = countItems.countItemName;
            }
            itemModel.minibarItemImage = UIImagePNGRepresentation([UIImage imageWithData:countItems.countItemImage]);
            // CFG [20160928/CRF-00001275] - Auto select if there's a value to auto select.
            //itemModel.minibarItemUsed = countOrderDetail.countCollected;
            //itemModel.minibarItemNew = countOrderDetail.countNew;
            if (countOrderDetail.countCollected != 0) {
                itemModel.minibarItemUsed = countOrderDetail.countCollected;
            }
            else {
                itemModel.minibarItemUsed = countItems.countDefaultQuantity;
            }
            if (countOrderDetail.countNew != 0) {
                itemModel.minibarItemNew = countOrderDetail.countNew;
            }
            else {
                itemModel.minibarItemNew = countItems.countDefaultQuantity;
            }
            // CFG [20160928/CRF-00001275] - End.
            [sectionInfo insertObjectToNextIndex:itemModel];
            
            //check data with cartdatas
            for (MiniBarSectionInfoV2 *section in datasLaundryCart) {
                if (section.minibarCategory.minibarCategoryID == countItems.countCategoryId) {
                    for (MiniBarItemModelV2 *minibar in section.rowHeights) {
                        if (minibar.minibarItemID == itemModel.minibarItemID) {
                            itemModel.minibarItemNew = minibar.minibarItemNew;
                            itemModel.minibarItemUsed = minibar.minibarItemUsed;
                        }
                    }
                }
            }
        }
        
        [sectionDatas addObject:sectionInfo];
        
    }
    
    //Discart all item in Cart
    [datasLaundryCart removeAllObjects];
    
    statusAddToCart = YES;
    
    //Edit all position views
    [self modifyAllViews];
    [tbvMinibar reloadData];
    
}

- (void)getRoomTypeByRoomNo {
    
    if (![txtRoom.text isEqualToString:@""]) {
        int userId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
        RoomTypeModel *rtModel = [[RoomManagerV2 sharedRoomManager] getRoomTypeByRoomNoDBWithUserId:userId andRoomNumber:txtRoom.text];
        if (rtModel != nil) {
            _roomTypeId = rtModel.amsId;
        } else {
            rtModel = [[RoomManagerV2 sharedRoomManager] getRoomTypeByRoomNoWSWithUserId:userId hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId roomNumber:txtRoom.text];
            _roomTypeId = rtModel != nil ? rtModel.amsId : -1;
        }
    }
}

#pragma mark - UITableView Datasource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return sectionDatas.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    MiniBarSectionInfoV2 *sectioninfo = [sectionDatas objectAtIndex:section];
    NSInteger numberRowInSection = sectioninfo.rowHeights.count;
    return sectioninfo.open ? numberRowInSection : 0;
}
int tagItemNameCell = 1000;
int tagUsedButtonCell = 1001;
int tagNewButtonCell = 1002;

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifycell = @"cellidentify";
    MiniBarItemCellV2 *cell = nil;
    
    cell = (MiniBarItemCellV2 *)[tableView dequeueReusableCellWithIdentifier:identifycell];
    if (cell == nil) {
        NSArray *array = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([MiniBarItemCellV2 class]) owner:self options:nil];
        cell = [array objectAtIndex:0];
        
        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
            cell.btnNewItem.layer.cornerRadius = 7.0f;
            cell.btnNewItem.layer.borderWidth = 1.0f;
            cell.btnNewItem.layer.borderColor = [[UIColor lightGrayColor] CGColor];
            
            cell.btnUsedItem.layer.cornerRadius = 7.0f;
            cell.btnUsedItem.layer.borderWidth = 1.0f;
            cell.btnUsedItem.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        }
    }
    
    cell.delegate = self;
    MiniBarSectionInfoV2 *sectionInfo = [sectionDatas objectAtIndex:indexPath.section];
    MiniBarItemModelV2 *model = [sectionInfo.rowHeights objectAtIndex:indexPath.row];
    
    UIImage *image = [UIImage imageWithData:model.minibarItemImage];
//    if (image == nil) {
//        image = [UIImage imageNamed:@"no_image_2.png"];
//    }
    [cell.imgItemCell setImage:image];
    [cell.lblTitleItem setText:model.minibarItemTitle];
    
//    if (indexPath.section == 0 && indexPath.row == 0) {
    if (indexPath.row == 0) {
        [cell.lblNewItem setText:[[LanguageManagerV2 sharedLanguageManager] getNew]];
        [cell.lblUsedItem setText:[[LanguageManagerV2 sharedLanguageManager] getUsed]];
        [cell.btnUsedItem setFrame:CGRectMake(189, 27, 54, 37)];
        [cell.btnNewItem setFrame:CGRectMake(256, 27, 54, 37)];
    } else {
        [cell.lblNewItem setText:@""];
        [cell.lblUsedItem setText:@""];
        [cell.btnUsedItem setFrame:CGRectMake(189, 18, 54, 37)];
        [cell.btnNewItem setFrame:CGRectMake(256, 18, 54, 37)];
    }
    
    //add target for button used and new
    [cell.btnUsedItem addTarget:self action:@selector(tapbtnUsed) forControlEvents:UIControlEventTouchUpInside];
    
    [cell.btnNewItem addTarget:self action:@selector(tapbtnNew) forControlEvents:UIControlEventTouchUpInside];
    
    // CFG [20160922/CRF-00001275] -
    //if (statusAddToCart == YES) {
    //    [cell.btnNewItem setTitle:[NSString stringWithFormat:@"%d", 0] forState:UIControlStateNormal];
    //    [cell.btnUsedItem setTitle:[NSString stringWithFormat:@"%d",0] forState:UIControlStateNormal];
    //} else {
        [cell.btnNewItem setTitle:[NSString stringWithFormat:@"%d", (int)model.minibarItemNew] forState:UIControlStateNormal];
        [cell.btnUsedItem setTitle:[NSString stringWithFormat:@"%d", (int)model.minibarItemUsed] forState:UIControlStateNormal];
    //}
    // CFG [20160928/CRF-00001275] - End.
    
    switch ([[UserManagerV2 sharedUserManager].currentUserAccessRight minibarNewUsedDisplay]) {
        case 1:
            [cell.lblUsedItem setText:@""];
            if (indexPath.row == 0) {
                [cell.lblNewItem setText:[[LanguageManagerV2 sharedLanguageManager] getUsed]];
                [cell.btnUsedItem setFrame:CGRectMake(256, 27, 54, 37)];
            } else {
                [cell.lblNewItem setText:@""];
                [cell.btnUsedItem setFrame:CGRectMake(256, 18, 54, 37)];
            }
            [cell.btnNewItem setFrame:CGRectMake(0, 0, 0, 0)];
            break;
        case 2:
            [cell.lblUsedItem setText:@""];
            [cell.btnUsedItem setFrame:CGRectMake(0, 0, 0, 0)];
            break;
        default:
            break;
    }
    
    //CRF-00001615
    if([[UserManagerV2 sharedUserManager].currentUserAccessRight minibarNewUsedDisplay] == 0){
        BOOL bStatusOfNewItem = cell.btnNewItem.hidden;
        BOOL bStatusOfUsedItem = cell.btnUsedItem.hidden;
        //Login (RA/SUP) > Posting > Minibar
        if (isFromMainPosting) {
            bStatusOfNewItem = isPostingFunctionMinibarNew == 0 ? YES : (isPostingFunctionMinibarNew == 1 ? NO : bStatusOfNewItem);
            bStatusOfUsedItem = isPosingFunctionMinibarUsed == 0 ? YES : (isPosingFunctionMinibarUsed == 1 ? NO : bStatusOfUsedItem);
        }else{ // Login (RA/SUP) > Select Room > Start > Action > Posting > Minibar
            bStatusOfNewItem = isRoomDetailsActionMinibarNew == 0 ? YES : (isRoomDetailsActionMinibarNew == 1 ? NO : bStatusOfNewItem);
            bStatusOfUsedItem = isRoomDetailsActionMinibarUsed == 0 ? YES : (isRoomDetailsActionMinibarUsed == 1 ? NO : bStatusOfUsedItem);
        }
        cell.lblNewItem.hidden = bStatusOfNewItem;
        cell.btnNewItem.hidden = bStatusOfNewItem;
        cell.lblUsedItem.hidden = bStatusOfUsedItem;
        cell.btnUsedItem.hidden = bStatusOfUsedItem;
    }
    if (isDemoMode){
        cell.lblNewItem.hidden = NO;
        cell.btnNewItem.hidden = NO;
        cell.lblUsedItem.hidden = NO;
        cell.btnUsedItem.hidden = NO;
    }
    [cell.lblTitleItem setTag:tagItemNameCell];
    [cell.btnNewItem setTag:tagNewButtonCell];
    [cell.btnUsedItem setTag:tagUsedButtonCell];
    [cell setPath:indexPath];
    
    return cell;
}

-(void)tapbtnUsed{
    [txtRoom resignFirstResponder];
}
-(void)tapbtnNew{
    [txtRoom resignFirstResponder];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return heightMinibarCell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return heightMinibarHeader;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    MiniBarSectionInfoV2 *sectionInfo = [sectionDatas objectAtIndex:section];
    
    if (sectionInfo.headerView == nil) {
        MiniBarSectionViewV2 *headerView = [[MiniBarSectionViewV2 alloc] initWithSection:section CategoryModelV2:sectionInfo.minibarCategory andOpenStatus:NO];
        sectionInfo.headerView = headerView;
        headerView.delegate = self;
    }
    return sectionInfo.headerView;
}

#pragma mark - Calculate layout

//Calculate total frame of table Minibar
-(CGRect)totalTableMiniBarFrame
{
    CGFloat heightHeaders = 0;
    CGFloat heightCells = 0;
    for (MiniBarSectionInfoV2 *currentSection in sectionDatas) {
        heightHeaders += heightMinibarHeader;
        if(currentSection.open == YES){
            heightCells += [currentSection.rowHeights count] * heightMinibarCell;
        }
    }
    
    CGRect result = tbvMinibar.frame;
    result.size.height = heightHeaders + heightCells;
    return result;
}

//edit all position in views
-(void)modifyAllViews
{
    CGRect tableMiniBarFrame = [self totalTableMiniBarFrame];
    [UIView animateWithDuration:0.5 animations:^{[tbvMinibar setFrame:tableMiniBarFrame];}];
    [UIView animateWithDuration:0.2 animations:^{[btnSubmit setFrame:CGRectMake(10, 15 + tbvMinibar.frame.size.height, 300, 64)];}];
    [UIView animateWithDuration:0.2 animations:^{[lblFSubmit setFrame:CGRectMake(121, 25 + tbvMinibar.frame.size.height, 135, 39)];}];
    [UIView animateWithDuration:0.2 animations:^{[btnPostingHistory setFrame:CGRectMake(10, 25 + tbvMinibar.frame.size.height + CGRectGetHeight(btnSubmit.bounds), 300, 55)];}];
    [UIView animateWithDuration:0.5 animations:^{[scrollView setContentSize:CGSizeMake(scrollView.contentSize.width, btnSubmit.frame.origin.y + btnSubmit.frame.size.height + 110)];}];
}

#pragma mark - Section Header Delegate
-(void)sectionHeaderView:(MiniBarSectionViewV2 *)sectionheaderView sectionOpened:(NSInteger)section {
    
    //-----Log action click section----
    if([LogFileManager isLogConsole])
    {
        NSInteger userId = [[UserManagerV2 sharedUserManager] currentUser].userId;
        NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSLog(@"<%@><Minibar><UserId:%d><click row at section index:%d to open>", time, (int)userId, (int)section);
    }
    
    MiniBarSectionInfoV2 *sectionInfo = [sectionDatas objectAtIndex:section];
    sectionInfo.open = YES;
    
    //Edit all position views
    [self modifyAllViews];
    
    NSInteger countOfRowsToInsert = [sectionInfo.rowHeights count];
    NSMutableArray *indexPathsToInsert = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i < countOfRowsToInsert; i++) {
        [indexPathsToInsert addObject:[NSIndexPath indexPathForRow:i inSection:section]];
    }
    
    UITableViewRowAnimation insertAnimation;
    insertAnimation = UITableViewRowAnimationAutomatic;
    
    
    //    MiniBarSectionInfoV2 *sectionInfoPrevious = nil;
    //    if (section > 0) {
    //        sectionInfoPrevious = [sectionDatas objectAtIndex:section - 1];
    //        if (sectionInfoPrevious.open) {
    //            insertAnimation = UITableViewRowAnimationBottom;
    //        }
    //    } else
    //        insertAnimation = UITableViewRowAnimationTop;
    
    [self.tbvMinibar beginUpdates];
    
    [self.tbvMinibar insertRowsAtIndexPaths:indexPathsToInsert
                           withRowAnimation:insertAnimation];
    [self.tbvMinibar endUpdates];
    
    if([indexPathsToInsert count] > 0)
    {
        [self.tbvMinibar scrollToRowAtIndexPath:[indexPathsToInsert objectAtIndex:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    //    [indexPathsToInsert release];
    
}

-(void)sectionHeaderView:(MiniBarSectionViewV2 *)sectionheaderView sectionClosed:(NSInteger)section {
    //--------Log-------
    if([LogFileManager isLogConsole])
    {
        NSInteger userId = [[UserManagerV2 sharedUserManager] currentUser].userId;
        NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSLog(@"<%@><Minibar><UserId:%d><click row at section index:%d to close>", time, (int)userId, (int)section);
    }
    
    MiniBarSectionInfoV2 *sectionInfo = [sectionDatas
                                         objectAtIndex:section];
    
    sectionInfo.open = NO;
    
    //Edit all position views
    [self modifyAllViews];
    
    NSInteger countOfRowsToDelete = [self.tbvMinibar numberOfRowsInSection:section];
    if (countOfRowsToDelete > 0) {
        NSMutableArray *indexPathsToDelete = [[NSMutableArray alloc] init];
        for (NSInteger i = 0; i < countOfRowsToDelete; i++) {
            [indexPathsToDelete addObject:[NSIndexPath indexPathForRow:i inSection:section]];
        }
        
        [tbvMinibar beginUpdates];
        [self.tbvMinibar deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:UITableViewRowAnimationAutomatic];
        [tbvMinibar endUpdates];
    }
}

#pragma mark - Item Cell Delegate Methods
-(void)btnUsedItemCellPressedWithIndexPath:(NSIndexPath *)path sender:(UIButton *)button{
    //statusAddToCart = NO;
    NSMutableArray *array = [[NSMutableArray alloc] init];
    for (NSInteger index = 0; index <= maxItem; index++) {
        [array addObject:[NSString stringWithFormat:@"%d", (int)index]];
    }
    
    PickerViewV2 *picker = [[PickerViewV2 alloc] initPickerViewV2WithDatas:array AndSelectedData:button.titleLabel.text AndIndex:path];
    [picker showPickerViewV2];
    [picker setDelegate:self];
    [picker setTIndex:tUsed];
}

-(void)btnNewItemCellPressedWithIndexPath:(NSIndexPath *)path sender:(UIButton *)button{
    //statusAddToCart = NO;
    NSMutableArray *array = [[NSMutableArray alloc] init];
    for (NSInteger index = 0; index <= maxItem; index++) {
        [array addObject:[NSString stringWithFormat:@"%d", (int)index]];
    }
    
    PickerViewV2 *picker = [[PickerViewV2 alloc] initPickerViewV2WithDatas:array AndSelectedData:button.titleLabel.text AndIndex:path];
    [picker showPickerViewV2];
    [picker setDelegate:self];
    [picker setTIndex:tNew];
}

#pragma mark - PickerView V2 Delegate methods
-(void)didChoosePickerViewV2WithData:(NSString *)data AndIndex:(NSIndexPath *)index WithtIndex:(NSInteger)tIndex{
    MiniBarSectionInfoV2 *sectionInfo = [sectionDatas objectAtIndex:index.section];
    MiniBarItemModelV2 *model = [sectionInfo.rowHeights objectAtIndex:index.row];
    
    if ([data integerValue] > 0) {
        statusAddToCart = NO;
    }
    switch ([[UserManagerV2 sharedUserManager].currentUserAccessRight minibarNewUsedDisplay]) {
        case 0:
            switch ([[UserManagerV2 sharedUserManager].currentUserAccessRight miniBarSynchronizeUsedAndNew]) {
                case 0:{
                    if (tIndex == tUsed){
                        model.minibarItemUsed = [data integerValue];
                        //-------Log action chose pickerview---
                        logCheckUsedValue = model.minibarItemUsed;
                        logCheckNewValue = model.minibarItemNew;
                        if([LogFileManager isLogConsole])
                        {
                            NSInteger userId = [[UserManagerV2 sharedUserManager] currentUser].userId;
                            NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
                            NSLog(@"<%@><Minibar><UserId:%d><Input ItemName:%@><ItemUsed:%d><ItemNew:%d>", time, (int)userId,model.minibarItemTitle, (int)model.minibarItemUsed, (int)model.minibarItemNew);
                        }
                    }
                    if (tIndex == tNew) {
                        model.minibarItemNew = [data integerValue];
                        //-------Log action chose pickerview---
                        logCheckUsedValue = model.minibarItemUsed;
                        logCheckNewValue = model.minibarItemNew;
                        if([LogFileManager isLogConsole])
                        {
                            NSInteger userId = [[UserManagerV2 sharedUserManager] currentUser].userId;
                            NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
                            NSLog(@"<%@><Minibar><UserId:%d><Input ItemName:%@><ItemUsed:%d><ItemNew:%d>", time, (int)userId,model.minibarItemTitle, (int)model.minibarItemUsed, (int)model.minibarItemNew);
                        }
                    }
                    break;
                }
                case 1:{
                    if (tIndex == tUsed){
                        model.minibarItemUsed = [data integerValue];
                        model.minibarItemNew = [data integerValue];
                        //-------Log action chose pickerview---
                        logCheckUsedValue = model.minibarItemUsed;
                        logCheckNewValue = model.minibarItemNew;
                        if([LogFileManager isLogConsole])
                        {
                            NSInteger userId = [[UserManagerV2 sharedUserManager] currentUser].userId;
                            NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
                            NSLog(@"<%@><Minibar><UserId:%d><Input ItemName:%@><ItemUsed:%d><ItemNew:%d>", time, (int)userId,model.minibarItemTitle, (int)model.minibarItemUsed, (int)model.minibarItemNew);
                        }
                    }
                    if (tIndex == tNew) {
                        model.minibarItemNew = [data integerValue];
                        if(model.minibarItemUsed == 0){
                            model.minibarItemUsed = [data integerValue];
                        }
                        //-------Log action chose pickerview---
                        logCheckUsedValue = model.minibarItemUsed;
                        logCheckNewValue = model.minibarItemNew;
                        if([LogFileManager isLogConsole])
                        {
                            NSInteger userId = [[UserManagerV2 sharedUserManager] currentUser].userId;
                            NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
                            NSLog(@"<%@><Minibar><UserId:%d><Input ItemName:%@><ItemUsed:%d><ItemNew:%d>", time, (int)userId,model.minibarItemTitle, (int)model.minibarItemUsed, (int)model.minibarItemNew);
                        }
                    }
                    break;
                }
                case 2:{
                    if (tIndex == tUsed){
                        model.minibarItemUsed = [data integerValue];
                        if(model.minibarItemNew == 0){
                            model.minibarItemNew = [data integerValue];
                        }
                        //-------Log action chose pickerview---
                        logCheckUsedValue = model.minibarItemUsed;
                        logCheckNewValue = model.minibarItemNew;
                        if([LogFileManager isLogConsole])
                        {
                            NSInteger userId = [[UserManagerV2 sharedUserManager] currentUser].userId;
                            NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
                            NSLog(@"<%@><Minibar><UserId:%d><Input ItemName:%@><ItemUsed:%d><ItemNew:%d>", time, (int)userId,model.minibarItemTitle, (int)model.minibarItemUsed, (int)model.minibarItemNew);
                        }
                    }
                    if (tIndex == tNew) {
                        model.minibarItemNew = [data integerValue];
                        model.minibarItemUsed = [data integerValue];
                        //-------Log action chose pickerview---
                        logCheckUsedValue = model.minibarItemUsed;
                        logCheckNewValue = model.minibarItemNew;
                        if([LogFileManager isLogConsole])
                        {
                            NSInteger userId = [[UserManagerV2 sharedUserManager] currentUser].userId;
                            NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
                            NSLog(@"<%@><Minibar><UserId:%d><Input ItemName:%@><ItemUsed:%d><ItemNew:%d>", time, (int)userId,model.minibarItemTitle, (int)model.minibarItemUsed, (int)model.minibarItemNew);
                        }
                    }
                    break;
                }
                case 3:{
                    if (tIndex == tUsed){
                        model.minibarItemUsed = [data integerValue];
                        model.minibarItemNew = [data integerValue];
                        //-------Log action chose pickerview---
                        logCheckUsedValue = model.minibarItemUsed;
                        logCheckNewValue = model.minibarItemNew;
                        if([LogFileManager isLogConsole])
                        {
                            NSInteger userId = [[UserManagerV2 sharedUserManager] currentUser].userId;
                            NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
                            NSLog(@"<%@><Minibar><UserId:%d><Input ItemName:%@><ItemUsed:%d><ItemNew:%d>", time, (int)userId,model.minibarItemTitle, (int)model.minibarItemUsed, (int)model.minibarItemNew);
                        }
                    }
                    if (tIndex == tNew) {
                        model.minibarItemNew = [data integerValue];
                        if(model.minibarItemUsed != 0){
                            model.minibarItemUsed = [data integerValue];
                        }
                        //-------Log action chose pickerview---
                        logCheckUsedValue = model.minibarItemUsed;
                        logCheckNewValue = model.minibarItemNew;
                        if([LogFileManager isLogConsole])
                        {
                            NSInteger userId = [[UserManagerV2 sharedUserManager] currentUser].userId;
                            NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
                            NSLog(@"<%@><Minibar><UserId:%d><Input ItemName:%@><ItemUsed:%d><ItemNew:%d>", time, (int)userId,model.minibarItemTitle, (int)model.minibarItemUsed, (int)model.minibarItemNew);
                        }
                    }
                    break;
                }
                case 4:{
                    if (tIndex == tUsed){
                        model.minibarItemUsed = [data integerValue];
                        if(model.minibarItemNew != 0){
                            model.minibarItemNew = [data integerValue];
                        }
                        //-------Log action chose pickerview---
                        logCheckUsedValue = model.minibarItemUsed;
                        logCheckNewValue = model.minibarItemNew;
                        if([LogFileManager isLogConsole])
                        {
                            NSInteger userId = [[UserManagerV2 sharedUserManager] currentUser].userId;
                            NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
                            NSLog(@"<%@><Minibar><UserId:%d><Input ItemName:%@><ItemUsed:%d><ItemNew:%d>", time, (int)userId,model.minibarItemTitle, (int)model.minibarItemUsed, (int)model.minibarItemNew);
                        }
                    }
                    if (tIndex == tNew) {
                        model.minibarItemNew = [data integerValue];
                        model.minibarItemUsed = [data integerValue];
                        //-------Log action chose pickerview---
                        logCheckUsedValue = model.minibarItemUsed;
                        logCheckNewValue = model.minibarItemNew;
                        if([LogFileManager isLogConsole])
                        {
                            NSInteger userId = [[UserManagerV2 sharedUserManager] currentUser].userId;
                            NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
                            NSLog(@"<%@><Minibar><UserId:%d><Input ItemName:%@><ItemUsed:%d><ItemNew:%d>", time, (int)userId,model.minibarItemTitle, (int)model.minibarItemUsed, (int)model.minibarItemNew);
                        }
                    }
                    break;
                }
                case 5:{
                    if (tIndex == tUsed){
                        model.minibarItemUsed = [data integerValue];
                        if(model.minibarItemNew == 0){
                            model.minibarItemNew = [data integerValue];
                        }
                        //-------Log action chose pickerview---
                        logCheckUsedValue = model.minibarItemUsed;
                        logCheckNewValue = model.minibarItemNew;
                        if([LogFileManager isLogConsole])
                        {
                            NSInteger userId = [[UserManagerV2 sharedUserManager] currentUser].userId;
                            NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
                            NSLog(@"<%@><Minibar><UserId:%d><Input ItemName:%@><ItemUsed:%d><ItemNew:%d>", time, (int)userId,model.minibarItemTitle, (int)model.minibarItemUsed, (int)model.minibarItemNew);
                        }
                    }
                    if (tIndex == tNew) {
                        model.minibarItemNew = [data integerValue];
                        if(model.minibarItemUsed == 0){
                            model.minibarItemUsed = [data integerValue];
                        }
                        //-------Log action chose pickerview---
                        logCheckUsedValue = model.minibarItemUsed;
                        logCheckNewValue = model.minibarItemNew;
                        if([LogFileManager isLogConsole])
                        {
                            NSInteger userId = [[UserManagerV2 sharedUserManager] currentUser].userId;
                            NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
                            NSLog(@"<%@><Minibar><UserId:%d><Input ItemName:%@><ItemUsed:%d><ItemNew:%d>", time, (int)userId,model.minibarItemTitle, (int)model.minibarItemUsed, (int)model.minibarItemNew);
                        }
                    }
                    break;
                }
                case 6:{
                    if (tIndex == tUsed){
                        model.minibarItemUsed = [data integerValue];
                        model.minibarItemNew = [data integerValue];
                        //                if(model.minibarItemNew != 0){
                        //                    model.minibarItemNew = [data integerValue];
                        //                }
                        //-------Log action chose pickerview---
                        logCheckUsedValue = model.minibarItemUsed;
                        logCheckNewValue = model.minibarItemNew;
                        if([LogFileManager isLogConsole])
                        {
                            NSInteger userId = [[UserManagerV2 sharedUserManager] currentUser].userId;
                            NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
                            NSLog(@"<%@><Minibar><UserId:%d><Input ItemName:%@><ItemUsed:%d><ItemNew:%d>", time, (int)userId,model.minibarItemTitle, (int)model.minibarItemUsed, (int)model.minibarItemNew);
                        }
                    }
                    if (tIndex == tNew) {
                        model.minibarItemNew = [data integerValue];
                        if(model.minibarItemUsed != 0){
                            model.minibarItemUsed = [data integerValue];
                        }
                        //-------Log action chose pickerview---
                        logCheckUsedValue = model.minibarItemUsed;
                        logCheckNewValue = model.minibarItemNew;
                        if([LogFileManager isLogConsole])
                        {
                            NSInteger userId = [[UserManagerV2 sharedUserManager] currentUser].userId;
                            NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
                            NSLog(@"<%@><Minibar><UserId:%d><Input ItemName:%@><ItemUsed:%d><ItemNew:%d>", time, (int)userId,model.minibarItemTitle, (int)model.minibarItemUsed, (int)model.minibarItemNew);
                        }
                    }
                    break;
                }
                default:
                    break;
            }
            break;
        case 1:
            if (tIndex == tUsed){
                model.minibarItemUsed = [data integerValue];
                //-------Log action chose pickerview---
                logCheckUsedValue = model.minibarItemUsed;
                logCheckNewValue = model.minibarItemNew;
                if([LogFileManager isLogConsole])
                {
                    NSInteger userId = [[UserManagerV2 sharedUserManager] currentUser].userId;
                    NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
                    NSLog(@"<%@><Minibar><UserId:%d><Input ItemName:%@><ItemUsed:%d><ItemNew:%d>", time, (int)userId,model.minibarItemTitle, (int)model.minibarItemUsed, (int)model.minibarItemNew);
                }
            }
            break;
        case 2:
            if (tIndex == tNew) {
                model.minibarItemNew = [data integerValue];
                //-------Log action chose pickerview---
                logCheckUsedValue = model.minibarItemUsed;
                logCheckNewValue = model.minibarItemNew;
                if([LogFileManager isLogConsole])
                {
                    NSInteger userId = [[UserManagerV2 sharedUserManager] currentUser].userId;
                    NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
                    NSLog(@"<%@><Minibar><UserId:%d><Input ItemName:%@><ItemUsed:%d><ItemNew:%d>", time, (int)userId,model.minibarItemTitle, (int)model.minibarItemUsed, (int)model.minibarItemNew);
                }
            }
            break;
        default:
            break;
    }

//    if (tIndex == tUsed) {
//        model.minibarItemUsed = [data integerValue];
//        model.minibarItemNew = [data integerValue];
//        //-------Log action chose pickerview---
//        logCheckUsedValue = model.minibarItemUsed;
//        logCheckNewValue = model.minibarItemNew;
//        if([LogFileManager isLogConsole])
//        {
//            NSInteger userId = [[UserManagerV2 sharedUserManager] currentUser].userId;
//            NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
//            NSLog(@"<%@><Minibar><UserId:%d><Input ItemName:%@><ItemUsed:%d><ItemNew:%d>", time, userId,model.minibarItemTitle, model.minibarItemUsed, model.minibarItemNew);
//        }
//    }
//    
//    if (tIndex == tNew) {
//        model.minibarItemNew = [data integerValue];
//        
//        //-------Log action chose pickerview---
//        logCheckNewValue = model.minibarItemNew;
//        if([LogFileManager isLogConsole])
//        {
//            NSInteger userId = [[UserManagerV2 sharedUserManager] currentUser].userId;
//            NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
//            NSLog(@"<%@><Minibar><UserId:%d><Input ItemName:%@><ItemNew:%d>", time, userId,model.minibarItemTitle, model.minibarItemNew);
//        }
//    }
    
    [tbvMinibar reloadRowsAtIndexPaths:[NSArray arrayWithObject:index] withRowAnimation:UITableViewRowAnimationFade];
    
    //[btnSubmit setEnabled:[self isValidQty]];
}

-(BOOL) isOnlyViewedAccessRight
{
    //Hao Tran[20130517/Check Access Right] - check access right only allow view
    if(isFromMainPosting)
    {
        if(postingMinibar.isAllowedView)
        {
            return YES;
        }
    }
    else if(actionMinibar.isAllowedView)
    {
        return YES;
    }
    //Hao Tran[20130517/Check Access Right] - END
    return NO;
}

-(BOOL)isValidQty
{
    if([self isOnlyViewedAccessRight]) {
        return NO;
    }
    int a = 0;
    int b = 0;
    for(NSInteger i=0;i<[sectionDatas count];i++){
        
        LinenSectionInfoV2 *sectioninfo = [sectionDatas objectAtIndex:i];
        
        for (NSInteger j = 0;j < [sectioninfo.rowHeights count];j++){
            
            NSIndexPath *index = [NSIndexPath indexPathForRow:j inSection:i];
            MiniBarItemCellV2 *minibarItem = (MiniBarItemCellV2 *)[self tableView:tbvMinibar cellForRowAtIndexPath:index];
            NSString *itemUseStr = [NSString stringWithFormat:@"%@",[minibarItem.btnUsedItem titleForState:UIControlStateNormal]];
            NSString *itemnewStr = [NSString stringWithFormat:@"%@",[minibarItem.btnNewItem titleForState:UIControlStateNormal]];
            a = [itemUseStr intValue];
            b = [itemnewStr intValue];
            if (a != 0 || b != 0) {
                break;
            }
            
        }
        if (a != 0 || b != 0) {
            break;
        }
        
    }
    
    /*
    if (txtRoom.text.length > 0) {
        if(a!=0 && b!=0){
            return YES;
        }
        else if(a!=0 && b==0){
            return YES;
        }
        else if (a == 0 && b != 0){
            return YES;
        }
        else if(a==0 && b == 0){
            return NO;
        }
    }
    else {
        return NO;
    }*/
    
    if(a != 0 || b != 0) {
        return YES;
    }
    
    return NO;
}


#pragma mark - Cart Press
-(void)cartButtonPressed {
    
    //Add minibarlinencart view
    MiniBarLinenCartViewV2 *cartView = [[MiniBarLinenCartViewV2 alloc] initWithNibName:NSStringFromClass([MiniBarLinenCartViewV2 class]) bundle:nil];
    
    cartView.delegate = self;
    
    cartView.countCartServiceId = countServiceId;
    
    cartView.sectionDatas = datasLaundryCart;
    
    for (MiniBarSectionInfoV2 *sectionInfo in datasLaundryCart) {
        sectionInfo.open = YES;
    }
    
    if (statusSave == NO && statusChooseMinibar == NO) {
        cartView.statusChooseQty = NO;
    }
    else
    {
        cartView.statusChooseQty = YES;
    }
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK] style:UIBarButtonItemStyleBordered target:nil action:nil];
    self.navigationItem.backBarButtonItem = backButton;
    
    [self.navigationController pushViewController:cartView animated:YES];
}

#pragma mark - Save Cart Minibar
-(void) saveData{
    if (statusSave == YES) {
        [self saveMinibarCart];
        [self btnAddToCartPressed:btnAddToCart];
    }
    statusChooseMinibar = NO;
    statusSave = NO;
}

//Return WS response code
-(int)saveMinibarCart
{
    int responseCode = RESPONSE_STATUS_OK;
    
    // @ show loading data .
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getRaSavingData]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES];
    
    ///Save minibar cart
    CountManagerV2 *manager = [[CountManagerV2 alloc] init];
    SyncManagerV2 *syncManager = [[SyncManagerV2 alloc] init];
    NSInteger roomAssignId = [TasksManagerV2 getCurrentRoomAssignment];
    
    //Room AssignId = 0 if come from main posting
    if(isFromMainPosting){
        roomAssignId = 0;
    }
    
    NSInteger userId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    NSInteger orderId = 0;
    
    //Update Order in minibar
    //CountOrdersModelV2 *countOrder = [[CountOrdersModelV2 alloc] init];
    countOrder.countRoomAssignId = roomAssignId;
    countOrder.countServiceId = countServiceId;
    countOrder.countStatus = tStatusChecked;
    countOrder.countUserId =  userId;
    countOrder.countCollectDate = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
    countOrder.countPostStatus = POST_STATUS_SAVED_UNPOSTED;
    countOrder.countRoomNumber = txtRoom.text; //[RoomManagerV2 getCurrentRoomNo];
    
    //Hao Tran - Remove for SG00054442
    //    BOOL isCheckCountOrder = [manager isCheckCountOrder:countOrder];
    //    if (isCheckCountOrder == FALSE) {
    //        [manager insertCountOrder:countOrder];
    //    }
    //    [manager loadCountOrdersModelByRoomIdUserIdAndServiceId:countOrder];
    
    //[manager insertCountOrder:countOrder];
    //countOrder =  [manager loadCountOrdersModelByRoomAssignId:countOrder.countRoomAssignId roomNumber:countOrder.countRoomNumber userId:countOrder.countUserId serviceId:countOrder.countServiceId postStatus:POST_STATUS_SAVED_UNPOSTED];
    
    //Hao Tran - END
    
    //Start edit  Post WSLog
    int switchStatus = 0;
    if(!isDemoMode){
        switchStatus = (int)[[NSUserDefaults standardUserDefaults] integerForKey:ENABLE_LOGWS];
    }

    if (switchStatus == 1) {
        
        NSInteger totalSections = [self numberOfSectionsInTableView:tbvMinibar];
        
        for (int i = 0; i<totalSections; i++) {
            NSInteger totalRow = [self tableView:self.tbvMinibar numberOfRowsInSection:i];
            for (int j = 0; j<totalRow; j++) {
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:j inSection:i];
                UITableViewCell *cell = [self.tbvMinibar cellForRowAtIndexPath:indexPath];
                UILabel *lblItemName = (UILabel *)[cell viewWithTag:tagItemNameCell];
                UIButton *btnUsed = (UIButton *)[cell viewWithTag:tagUsedButtonCell];
                
                UIButton *btnNew = (UIButton *)[cell viewWithTag:tagNewButtonCell];
                NSString *valueBtnUsed = btnUsed.titleLabel.text;
                NSString *valueBtnNew = btnNew.titleLabel.text;
                NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
                NSString *messageValue = [NSString stringWithFormat:@"Minibar Post WSLog: Time:%@, UserId:%d, RoomId:%@, ItemName:%@, Used:%@, New:%@", time, (int)userId, txtRoom.text, lblItemName.text, valueBtnUsed, valueBtnNew];
                if ([valueBtnUsed integerValue] != 0 || [valueBtnNew integerValue] != 0) {
                    [syncManager postWSLogCountService:userId Message:messageValue MessageType:1];
                }
            }
        }
    }
    
    //End edit Post WSLog
    
    if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        //-----------post minibar order id WS for post minibar-------------
        NSInteger totalQuantity = 0;
        for (NSInteger indexSection=0; indexSection < datasLaundryCart.count; indexSection ++) {
            MiniBarSectionInfoV2 *sectionInfo = [datasLaundryCart objectAtIndex:indexSection];
            for (NSInteger indexRow=0; indexRow < sectionInfo.rowHeights.count; indexRow++) {
                
                MiniBarItemModelV2 *model = [sectionInfo.rowHeights objectAtIndex:indexRow];
                
                if (model.minibarItemNew != 0 || model.minibarItemUsed != 0) {
                    totalQuantity += model.minibarItemUsed;
                }
            }
        }
        orderId = [syncManager postMinibarOrderWithUserId:userId roomAssignId:roomAssignId totalQuantity:totalQuantity serviceCharge:0 AndSubTotal:totalQuantity WithDate:countOrder.countCollectDate];
        if(orderId > 0) {
            countOrder.countOrderWSId = orderId;
            countOrder.countPostStatus = POST_STATUS_POSTED;
        }
        //-----------End---------------------------------------------------
    }
    //Update for room number
    [manager updateCountOrder:countOrder];
    
    //save minibar or linen detail
    NSMutableArray *postItems = [[NSMutableArray alloc] init];
    
    BOOL isPostingZeroQuantity = [[UserManagerV2 sharedUserManager].currentUserAccessRight isPostingZeroQuantity];
    //Hao Tran Remove for post all minibar item (PRD 2.2)
    //save minibar order detail
    //    for (NSInteger indexSection = 0; indexSection < datasLaundryCart.count; indexSection ++) {
    //        MiniBarSectionInfoV2 *sectionInfo = [datasLaundryCart objectAtIndex:indexSection];
    
    for (NSInteger indexSection = 0; indexSection < sectionDatas.count; indexSection ++) {
        MiniBarSectionInfoV2 *sectionInfo = [sectionDatas objectAtIndex:indexSection];
        for (NSInteger indexRow = 0; indexRow < sectionInfo.rowHeights.count; indexRow++) {
            
            MiniBarItemModelV2 *model = [sectionInfo.rowHeights objectAtIndex:indexRow];
            
            //Hao Tran[20130711 / Remove for PRD 2.2] - Post all value minibar items
            //if (model.minibarItemNew != 0 || model.minibarItemUsed != 0) {
            
            //Hao Tran[20141105 / Apply Configuration POSTING_POST_ZEROTH_VALUE] - if 0 follow normal, if 1 post 0
            if(!isPostingZeroQuantity && (model.minibarItemNew == 0 && model.minibarItemUsed == 0)) {
                continue;
            }
            
            CountOrderDetailsModelV2 *countOrderDetailModel = [manager loadCountOrderDetailsByItemId:model.minibarItemID AndOrderId:countOrder.countOrderId];
            
                if (countOrderDetailModel.countOrderDetailId != 0) {
                    
                    if (countOrderDetailModel.countNew !=  model.minibarItemNew || countOrderDetailModel.countCollected != model.minibarItemUsed) {
                        //update Order detail in minibar
                        countOrderDetailModel.countNew = model.minibarItemNew;
                        countOrderDetailModel.countCollected = model.minibarItemUsed;
                        countOrderDetailModel.countCollectedDate = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
                        countOrderDetailModel.countPostStatus = POST_STATUS_SAVED_UNPOSTED;
                        
                        //save in database
                        [manager updateCountOrderDetail:countOrderDetailModel];
                    }
                    
                }
                else
                {
                    //insert Order detail in minibar
                    CountOrderDetailsModelV2 *countOrderDetails = [[CountOrderDetailsModelV2 alloc] init];
                    countOrderDetails.countCollected = model.minibarItemUsed;
                    countOrderDetails.countNew = model.minibarItemNew;
                    countOrderDetails.countItemId = model.minibarItemID ;
                    countOrderDetails.countOrderId = countOrder.countOrderId;
                    countOrderDetails.countCollectedDate = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
                    countOrderDetails.countPostStatus = POST_STATUS_SAVED_UNPOSTED;
                    
                    //save in database
                    [manager insertCountOrderDetail:countOrderDetails];
                }
                
                //-----------------------WS-----------------------
                CountOrderDetailsModelV2 *countOrderDetailModelPost = [manager loadCountOrderDetailsByItemId:model.minibarItemID AndOrderId:countOrder.countOrderId];
                [postItems addObject:countOrderDetailModelPost];
            //}
            //                old WS
            //                BOOL statusPost = [syncManager postMinibarItemWithUserId:userId  roomAssignId:roomAssignId AndMinibarOrderModel:countOrderDetailModelPost WithOrderId:orderId];
            //
            //                if (statusPost == YES) {
            //                    //post is success, clear database
            //                    if (countOrderDetailModelPost.countOrderDetailId != 0) {
            //                        [manager deleteCountOrderDetail:countOrderDetailModelPost];
            //                    }
            //                    [datasLaundryCart removeAllObjects];
            //                }
            //-----------------------End----------------------
            //}
        }
    }
    
    //post list of PostItem to new WS for Minibar and Linen
    NSArray *postedItems = nil;
    int count = 0;
    if (isDemoMode) {
        count = 1;
    } else {
        postedItems = [[NSArray alloc] initWithArray:[syncManager postAllMinibarItemsWithUserId:userId roomAssignId:roomAssignId AndMinibarOrderModelList:postItems WithOrderId:orderId roomNumber:countOrder.countRoomNumber isEnablePostingHistory:isFromMainPosting]];
        count = (int)[postedItems count];
    }
        
    //    postedItems = [syncManager postAllMinibarItemsWithUserId:userId roomAssignId:roomAssignId AndMinibarOrderModelList:postItems WithOrderId:orderId];
    //    if (postedItems != nil)
    if (count != 0) {
        
        for (CountOrderDetailsModelV2 *postedItem in postItems) {
            [manager deleteCountOrderDetail:postedItem];
            if (postedItem.countCollected != 0 || postedItem.countNew != 0) {
                [self saveCountHistoryItem:postedItem];
            }
        }
        [manager deleteCountOrderByOrderId:(int)countOrder.countOrderId];
        [manager deleteCountOrderDetailsByOrderId:(int)countOrder.countOrderId];
        
        //Post all older unposted minibar
    
        if(haveOldCountOrders) {
            [manager postAllPendingCountDataByUserId:(int)userId serviceId:(int)countServiceId];
        }
        
        //Hao Tran - Reset minibar selected
        for (NSInteger indexSection = 0; indexSection < sectionDatas.count; indexSection ++) {
            MiniBarSectionInfoV2 *sectionInfo = [sectionDatas objectAtIndex:indexSection];
            for (NSInteger indexRow = 0; indexRow < sectionInfo.rowHeights.count; indexRow++) {
                MiniBarItemModelV2 *minibarItem = [sectionInfo.rowHeights objectAtIndex:indexRow];
                minibarItem.minibarItemNew = 0;
                minibarItem.minibarItemUsed = 0;
            }
        }
        
        [datasLaundryCart removeAllObjects];
        sumMinibarUsed = 0;
        if ([[UserManagerV2 sharedUserManager].currentUserAccessRight isLoadMinibarByRoomType]) {
            [txtRoom setText:@""];
            _roomTypeId = 0;
        }
        [self loadingDatas];
        [[[[iToast makeText:[[LanguageManagerV2 sharedLanguageManager] getMsgSaveMessage]] setGravity:iToastGravityBottom] setDuration:iToastDurationNormal] show];
    } else {
        responseCode = RESPONSE_STATUS_NO_NETWORK;
        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:@"" message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_internet_exception] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getOK] otherButtonTitles:nil];
        alert.delegate = self;
        [alert setTag:tagNoNetwork];
        [alert show];
    }
    
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getMsgSaveMessage]];
    //hide saving data.
    [self performSelector:@selector(hiddenHUDAfterSaved:) withObject:HUD afterDelay:0.0f];
    return responseCode;
}

//Save data into table Count History
-(void) saveCountHistoryItem:(CountOrderDetailsModelV2 *) model
{
    CountHistoryModel *countHistoryModel = [[CountHistoryModel alloc]init];
    countHistoryModel.room_id = txtRoom.text;
    countHistoryModel.count_item_id = model.countItemId;
    countHistoryModel.count_collected = model.countCollected;
    countHistoryModel.count_new = model.countNew;
    countHistoryModel.count_service = countServiceId;
    countHistoryModel.count_date = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSInteger userId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    countHistoryModel.count_user_id = userId;
    
    CountHistoryManager *countHistoryManager = [[CountHistoryManager alloc]init];
    [countHistoryManager insertCountHistoryData:countHistoryModel];
}

#pragma didden after save.
-(void) hiddenHUDAfterSaved:(MBProgressHUD *)HUD{
    [HUD hide:YES];
    [HUD removeFromSuperview];
    //    [HUD release];
}

#pragma mark - MiniBarLinenCart delegate methods
-(void)saveMiniBarLinenCartItem
{
    statusChooseMinibar = NO;
}

#pragma mark - set caption view
-(void)setCaptionView
{
    [btnAddToCart setTitle:[[LanguageManagerV2 sharedLanguageManager] getAddToCart] forState:UIControlStateNormal];
    [self setTitle:[[LanguageManagerV2 sharedLanguageManager] getMiniBar]];
    
    msgbtnNo = [[LanguageManagerV2 sharedLanguageManager] getNo];
    msgbtnYes = [[LanguageManagerV2 sharedLanguageManager] getYes];
    msgSaveCount = [[LanguageManagerV2 sharedLanguageManager] getMsgAlertSaveCounts];
    msgDiscardCount = [[LanguageManagerV2 sharedLanguageManager] getMsgAlertDiscardCounts];
    
    //refresh topview
    TopbarViewV2 *topview = [self getTopBarView];
    [topview refresh:[[SuperRoomModelV2 alloc] init]];
}

#pragma mark - Handle Topbar Methods
-(void)loadTopbarView {
    TopbarViewV2 *topbar = [[TopbarViewV2 alloc] initViewWithSuperModel:[[CommonVariable sharedCommonVariable] superRoomModel]];
    [topbar setTag:tagTopbarView];
    [self.view addSubview:topbar];
    [topbar setHidden:YES];
    
    [topbar setAlpha:0.0];
    [self addButtonHandleShowHideTopbar];
}


-(void)showTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationWillStartSelector:@selector(setHiddenTopbarView)];
    
    f = topbar.frame;
    f.origin.y = 0;
    [topbar setFrame:f];
    [topbar setAlpha:1.0];
    
    [self adjustShowForViews];
    
    [UIView commitAnimations];
}

-(void)hideTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDidStopSelector:@selector(setHiddenTopbarView)];
    [UIView setAnimationDelegate:self];
    
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    [topbar setAlpha:0.0];
    
    [self adjustRemoveForViews];
    
    [UIView commitAnimations];
}

-(void)setHiddenTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    [topbar setHidden:!topbar.hidden];
}

-(TopbarViewV2 *)getTopBarView {
    return (TopbarViewV2 *)[self.view viewWithTag:tagTopbarView];
}

//-(void)addButtonHandleShowHideTopbar {
//    UIButton *titleBarButton = [[UIButton alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
//
//    titleBarButton.titleLabel.backgroundColor = [UIColor clearColor];
//    titleBarButton.titleLabel.font = FONT_BUTTON_TOPBAR;
//    titleBarButton.titleLabel.textAlignment = NSTextAlignmentCenter;
//    titleBarButton.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
//    [titleBarButton.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
//    [titleBarButton.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
//    [titleBarButton addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
//    [titleBarButton setTitle:[self title] forState:UIControlStateNormal];
//    self.navigationItem.titleView = titleBarButton;
//}

-(void)addButtonHandleShowHideTopbar {
    UIView *vBarButton = [[UIView alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
    UIButton *titleBarButtonFirst = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 200, 30)];
    
    titleBarButtonFirst.titleLabel.backgroundColor = [UIColor clearColor];
    [titleBarButtonFirst.titleLabel setFont: FONT_BUTTON_TOPBAR];
    titleBarButtonFirst.titleLabel.textAlignment = NSTextAlignmentCenter;
    titleBarButtonFirst.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
    
    [titleBarButtonFirst.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
    [titleBarButtonFirst.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
    [titleBarButtonFirst addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    if(isFromMainPosting){
        [titleBarButtonFirst setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_Counts] forState:UIControlStateNormal];
    } else {
        [titleBarButtonFirst setTitle:txtRoom.text forState:UIControlStateNormal];
    }
    
    [vBarButton addSubview:titleBarButtonFirst];
    
    
    UIButton *titleBarButtonSecond = [[UIButton alloc] initWithFrame:CGRectMake(0, 28, 200, 22)];
    [titleBarButtonSecond setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [titleBarButtonSecond.titleLabel setFont:FONT_BUTTON_TOPBAR_SECOND];
    [titleBarButtonSecond setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_MINIBAR] forState:UIControlStateNormal];//Set title for Tabbar
    [titleBarButtonSecond addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    [vBarButton addSubview:titleBarButtonSecond];
    
    self.navigationItem.titleView = vBarButton;
    
    //Move title to center vertical
    CGRect titleViewBounds = self.navigationItem.titleView.bounds;
    int moveLeftTitle = 0;
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        moveLeftTitle = 10;
    }
    self.navigationItem.titleView.bounds = CGRectMake(titleViewBounds.origin.x + moveLeftTitle, Y_TOPBAR_TITLE, titleViewBounds.size.width, titleViewBounds.size.height);
}

-(void)topbarTapped:(UIButton *)sender {
    TopbarViewV2 *topbar = [self getTopBarView];
    [sender setEnabled:NO];
    if (topbar.isHidden) {
        [self showTopbarView];
    } else {
        [self hideTopbarView];
    }
    [self performSelector:@selector(topbarButtonEnable:) withObject:sender afterDelay:0.5];
}

-(void) topbarButtonEnable:(UIButton *) topbarBtn {
    [topbarBtn setEnabled:YES];
}

-(void)adjustShowForViews {
    //adjust all views when show
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    CGRect fRoomView = viewRoom.frame;
    [viewRoom setFrame:CGRectMake(0, f.size.height, 320, fRoomView.size.height)];
    
    //    CGRect ftbv = tbvMinibar.frame;
    CGRect fscv = scrollView.frame;
    [scrollView setFrame:CGRectMake(0, f.size.height + fRoomView.size.height, 320, fscv.size.height - f.size.height)];
    
    //Edit position in view
    [self modifyAllViews];
}

-(void)adjustRemoveForViews {
    //adjust all views when hide
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    CGRect fRoomView = viewRoom.frame;
    [viewRoom setFrame:CGRectMake(0, 0, 320, fRoomView.size.height)];
    
    //    CGRect ftbv = tbvMinibar.frame;
    CGRect fscv = scrollView.frame;
    [scrollView setFrame:CGRectMake(0, fRoomView.size.height, 320, fscv.size.height + f.size.height)];
    
    //Edit position in view
    [self modifyAllViews];
}


/**
 * Name : shouldChangeCharactersInRange
 * Description : For hiding the keyboard. This must be implemented if you are working with UITextField
 * Parameter :
 * Return : BOOL
 */
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if([string isEqualToString:@"\n"])
    {
        [textField resignFirstResponder];
        return NO;
    }
    return YES;
}


/**
 * Name : textFieldShouldReturn
 * Description : Hiding the keyboard of UITextField
 * Parameter :
 * Return : BOOL
 */
- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    if([LogFileManager isLogConsole])
    {
        NSInteger userId = [[UserManagerV2 sharedUserManager] currentUser].userId;
        NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSLog(@"<%@><Minibar><UserId:%d><RoomId:%@>", time, (int)userId, textField.text);
    }
    
    //[btnSubmit setEnabled:[self isValidQty]];
    
    [textField resignFirstResponder];
    if ([[UserManagerV2 sharedUserManager].currentUserAccessRight isLoadMinibarByRoomType] && ![txtRoom.text isEqualToString:@""]) {
        [self performSelector:@selector(loadingDatas) withObject:nil];
    }
    [self setEnableBtnAtionHistory:YES];
    return YES;
}

- (NSMutableString*) aggregateData {
    
    NSMutableString *confirmData = [NSMutableString string];
    
    NSString *sMinibarPostingConfirmationMessageDetailsFormat; // CFG [20161028/CRF-00001614]
    NSMutableString *sMinibarPostingConfirmationMessageDetails; // CFG [20161028/CRF-00001614]
    
    sMinibarPostingConfirmationMessageDetailsFormat  = [[UserManagerV2 sharedUserManager].currentUserAccessRight getConfigByKey:USER_CONFIG_MINIBAR_POSTING_CONFIRMATION_MESSAGE_DETAILS_FORMAT]; // CFG [20161028/CRF-00001614]
    
    // CFG [20161028/CRF-00001614] - If empty, assign a default value.
    if ([sMinibarPostingConfirmationMessageDetailsFormat isEqualToString:@""]) {
        sMinibarPostingConfirmationMessageDetailsFormat = @"[UsedQty] - [ItemName]";
    }
    if (isDemoMode) {
        sMinibarPostingConfirmationMessageDetailsFormat = @"[UsedQty] U [NewQty] N - [ItemName]";
    }
    // CFG [20161028/CRF-00001614] - End.
    
    for(NSInteger i = 0; i<[sectionDatas count]; i++) {
        
        MiniBarSectionInfoV2 *sectioninfo = [sectionDatas objectAtIndex:i];
        for (NSInteger j = 0; j < [sectioninfo.rowHeights count]; j++) {
        
            sMinibarPostingConfirmationMessageDetails = [sMinibarPostingConfirmationMessageDetailsFormat mutableCopy];  // CFG [20161028/CRF-00001614]
            
            NSIndexPath *index = [NSIndexPath indexPathForRow:j inSection:i];
            MiniBarItemCellV2 *minibarItem = (MiniBarItemCellV2 *)[self tableView:tbvMinibar cellForRowAtIndexPath:index];
            NSString *itemUseStr = [NSString stringWithFormat:@"%@", [minibarItem.btnUsedItem titleForState:UIControlStateNormal]];
            NSString *sNewQuantity = [NSString stringWithFormat:@"%@", [minibarItem.btnNewItem titleForState:UIControlStateNormal]]; // CFG [20161028/CRF-00001614]
            
            if([itemUseStr integerValue] != 0 || [sNewQuantity integerValue] != 0){
                sMinibarPostingConfirmationMessageDetails = [[sMinibarPostingConfirmationMessageDetails stringByReplacingOccurrencesOfString:@"[UsedQty]" withString:itemUseStr] mutableCopy]; // CFG [20161028/CRF-00001614]
                // CFG [20161028/CRF-00001614] - New quantity.
                sMinibarPostingConfirmationMessageDetails = [[sMinibarPostingConfirmationMessageDetails stringByReplacingOccurrencesOfString:@"[NewQty]" withString:sNewQuantity] mutableCopy]; // CFG [20161028/CRF-00001614]
            }
            
            if (sMinibarPostingConfirmationMessageDetails && sMinibarPostingConfirmationMessageDetailsFormat && ![sMinibarPostingConfirmationMessageDetails isEqualToString:sMinibarPostingConfirmationMessageDetailsFormat]) {
                sMinibarPostingConfirmationMessageDetails = [[sMinibarPostingConfirmationMessageDetails stringByReplacingOccurrencesOfString:@"[ItemName]" withString:[minibarItem.lblTitleItem text]] mutableCopy];
                
                [confirmData appendString:@"\n"];
                [confirmData appendString:sMinibarPostingConfirmationMessageDetails];
            }
            // CFG [20161028/CRF-00001614] - End.
        }
        
        
    }
    return confirmData;
}

//Thien Chau: CRF-00001277 > [Marriott] Confirmation Message before Submit Posting on Minibar / Engineering / Amenities / Lost & Found
- (void)alertView:(DTAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == tagUnpostData) {
        return;
    }
    if (alertView.tag == tagNoNetwork)
    {
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    if (buttonIndex == 0) {
        if([LogFileManager isLogConsole])
        {
            NSInteger userId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
            NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
            if (logCheckNewValue == 0 && logCheckUsedValue == 0) {
                NSLog(@"<%@><Minibar><UserId:%d><Summit to post><input RoomId:%@>", time, (int)userId, txtRoom.text);
            }
            else {
                NSInteger totalSections = [self numberOfSectionsInTableView:tbvMinibar];
                
                for (int i = 0; i<totalSections; i++) {
                    NSInteger totalRow = [self tableView:self.tbvMinibar numberOfRowsInSection:i];
                    for (int j = 0; j<totalRow; j++) {
                        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:j inSection:i];
                        UITableViewCell *cell = [self.tbvMinibar cellForRowAtIndexPath:indexPath];
                        UILabel *lblItemName = (UILabel *)[cell viewWithTag:tagItemNameCell];
                        UIButton *btnUsed = (UIButton *)[cell viewWithTag:tagUsedButtonCell];
                        
                        UIButton *btnNew = (UIButton *)[cell viewWithTag:tagNewButtonCell];
                        NSString *valueBtnUsed = btnUsed.titleLabel.text;
                        NSString *valueBtnNew = btnNew.titleLabel.text;
                        if ([valueBtnNew isEqualToString:@"0"] && [valueBtnUsed isEqualToString:@"0"]) {
                            
                        }
                        else {
                            NSLog(@"<%@><Minibar><UserId:%d><Submit to post><Input RoomId:%@><ItemName:%@><ItemUsed:%@><ItemNew:%@>", time, (int)userId, txtRoom.text, lblItemName.text, valueBtnUsed, valueBtnNew);
                        }
                    }
                }
            }
        }
        //-------------Log----------
        
        [self handleBtnAddToCartPressed];
        int resultCode = [self saveMinibarCart];
        if(isFromMainPosting)
        {
            [txtRoom setText:@""];
            [tbvMinibar reloadData];
            [datasLaundryCart removeAllObjects];
            //[self.navigationController popViewControllerAnimated:YES];
        }
        else
        {
            //response status different to RESPONSE_STATUS_OK will show alert view in saveMinibarCart
            if(resultCode == RESPONSE_STATUS_OK) {
                NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
                for (UIViewController *aViewController in allViewControllers) {
                    if ([aViewController isKindOfClass:[RoomAssignmentInfoViewController class]]) {
                        ((RoomAssignmentInfoViewController*) aViewController).isAlreadyPostedMiniBar = YES;
//                        [self.navigationController popToViewController:aViewController animated:YES];
                        [self.navigationController popViewControllerAnimated:YES];
                    }
                }
            }
        }
        
    } else {
        if([LogFileManager isLogConsole])
        {
            NSInteger userId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
            NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
            if (logCheckNewValue == 0 && logCheckUsedValue == 0) {
                NSLog(@"<%@><Minibar><UserId:%d><Not confirm to post><input RoomId:%@>", time, (int)userId, txtRoom.text);
            }
            else {
                NSInteger totalSections = [self numberOfSectionsInTableView:tbvMinibar];
                
                for (int i = 0; i<totalSections; i++) {
                    NSInteger totalRow = [self tableView:self.tbvMinibar numberOfRowsInSection:i];
                    for (int j = 0; j<totalRow; j++) {
                        
                        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:j inSection:i];
                        UITableViewCell *cell = [self.tbvMinibar cellForRowAtIndexPath:indexPath];
                        
                        UILabel *lblItemName = (UILabel *)[cell viewWithTag:tagItemNameCell];
                        UIButton *btnUsed = (UIButton *)[cell viewWithTag:tagUsedButtonCell];
                        UIButton *btnNew = (UIButton *)[cell viewWithTag:tagNewButtonCell];
                        
                        NSString *valueBtnUsed = btnUsed.titleLabel.text;
                        NSString *valueBtnNew = btnNew.titleLabel.text;
                        if ([valueBtnNew isEqualToString:@"0"] && [valueBtnUsed isEqualToString:@"0"]) {
                            
                        }
                        else {
                            NSLog(@"<%@><Minibar><UserId:%d><Not confirm to post><Input RoomId:%@><ItemName:%@><ItemUsed:%@><ItemNew:%@>", time, (int)userId, txtRoom.text, lblItemName.text, valueBtnUsed, valueBtnNew);
                        }
                    }
                }
            }
        }
        
    }
}

-(IBAction) btnSubmit_Clicked:(id)sender
{
    NSString *roomNo = [txtRoom text];
    if(roomNo.length > 0 && [self isValidQty])
    {
        RoomAssignmentModelV2 *roomAssignment = [[RoomAssignmentModelV2 alloc] init];
        roomAssignment.roomAssignment_RoomId = roomNo;
        roomAssignment.roomAssignment_UserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
        BOOL result = [[RoomManagerV2 sharedRoomManager] getRoomAssignmentByUserIDAndRoomID:roomAssignment];
        
        //Hao Tran
        //No need to check data from main posting
        if(isFromMainPosting){
            if([[UserManagerV2 sharedUserManager].currentUserAccessRight isValidateRoom] && !isDemoMode) {
                NSString *hotelId = [NSString stringWithFormat:@"%d", [[[UserManagerV2 sharedUserManager] currentUser] userHotelsId]];
                result = [[RoomManagerV2 sharedRoomManager] isValidRoomWSByUserId:[UserManagerV2 sharedUserManager].currentUser.userId roomNumber:roomNo hotelId:hotelId];
            } else {
                result = TRUE; //Force posting mini bar without checking validation room
            }
        }
        
        if(result)
        {
            [TasksManagerV2 setCurrentRoomAssignment:roomAssignment.roomAssignment_Id];
            [RoomManagerV2 setCurrentRoomNo:roomNo];
            //NSString *mess = [NSString stringWithFormat:@"Room %@ \n Submit Minibar Items?", roomNo];
            //NSString *mess = [NSString stringWithFormat:@"%@ %@ \n %@%@", [L_room currentKeyToLanguage],roomNo,[L_submit_minibar_items currentKeyToLanguage],@"?"];
            NSString *mess = [NSString stringWithFormat:@"%@ %@ \n %@%@ \n %@", [L_room currentKeyToLanguage],roomNo,[L_submit_minibar_items currentKeyToLanguage],@"?", [self aggregateData]];
            DTAlertView *alertView = [DTAlertView alertViewWithTitle:[L_confirm_title currentKeyToLanguage] message:mess delegate:self cancelButtonTitle:[L_YES currentKeyToLanguage] positiveButtonTitle:[L_NO currentKeyToLanguage]];
            [alertView show];
        }
        else
        {
            NSString *message = nil;
            if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                message = [NSString stringWithFormat:@"%@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOM_INVALID]];
            } else {
                message = [NSString stringWithFormat:@"\n\n\n\n\n%@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOM_INVALID]];
            }
            
            UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObject:[UIImage imageNamed:@"yes_bt.png"]] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:message delegate:nil cancelButtonTitle:[L_CLOSE currentKeyToLanguage] otherButtonTitles: nil];
            
            if(!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                UIImageView *alertImage = [[UIImageView alloc] initWithFrame:CGRectMake(80, 35, 120, 120)];
                [alertImage setImage:[UIImage imageNamed:@"icon_wrong room.png"]];
                [alert addSubview:alertImage];
            }
            [alert show];
            
            //-------------Log----------
            if([LogFileManager isLogConsole]) {
                NSInteger userId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
                NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
                if (logCheckNewValue == 0 && logCheckUsedValue == 0) {
                    NSLog(@"<%@><Minibar><UserId:%d><Invalid room to post><input RoomId:%@>", time, (int)userId, txtRoom.text);
                }
                else {
                    NSInteger totalSections = [self numberOfSectionsInTableView:tbvMinibar];
                    
                    for (int i = 0; i<totalSections; i++) {
                        NSInteger totalRow = [self tableView:self.tbvMinibar numberOfRowsInSection:i];
                        for (int j = 0; j<totalRow; j++) {
                            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:j inSection:i];
                            UITableViewCell *cell = [self.tbvMinibar cellForRowAtIndexPath:indexPath];
                            UILabel *lblItemName = (UILabel *)[cell viewWithTag:tagItemNameCell];
                            UIButton *btnUsed = (UIButton *)[cell viewWithTag:tagUsedButtonCell];
                            
                            UIButton *btnNew = (UIButton *)[cell viewWithTag:tagNewButtonCell];
                            NSString *valueBtnUsed = btnUsed.titleLabel.text;
                            NSString *valueBtnNew = btnNew.titleLabel.text;
                            if ([valueBtnNew isEqualToString:@"0"] && [valueBtnUsed isEqualToString:@"0"]) {
                                
                            }
                            else{
                                NSLog(@"<%@><Minibar><UserId:%d><Invalid room to post><Input RoomId:%@><ItemName:%@><ItemUsed:%@><ItemNew:%@>", time, (int)userId, txtRoom.text, lblItemName.text, valueBtnUsed, valueBtnNew);
                            }
                        }
                    }
                }
            }
            //-------------Log----------
        }
        
    } else {
        if (roomNo.length > 0) {
            NSString *message = nil;
            if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                message = [NSString stringWithFormat:@"%@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_INVALID_DATA]];
            } else {
                message = [NSString stringWithFormat:@"\n\n\n\n\n%@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_INVALID_DATA]];
            }
            
            UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObject:[UIImage imageNamed:@"yes_bt.png"]] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:message delegate:nil cancelButtonTitle:[L_CLOSE currentKeyToLanguage] otherButtonTitles: nil];
            
            if(!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                UIImageView *alertImage = [[UIImageView alloc] initWithFrame:CGRectMake(80, 35, 120, 120)];
                [alertImage setImage:[UIImage imageNamed:@"icon_wrong room.png"]];
                [alert addSubview:alertImage];
            }
            [alert show];
        } else {
            NSString *message = nil;
            if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                message = [NSString stringWithFormat:@"%@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOM_INVALID]];
            } else {
                message = [NSString stringWithFormat:@"\n\n\n\n\n%@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOM_INVALID]];
            }
            
            UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObject:[UIImage imageNamed:@"yes_bt.png"]] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:message delegate:nil cancelButtonTitle:[L_CLOSE currentKeyToLanguage] otherButtonTitles: nil];
            
            if(!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                UIImageView *alertImage = [[UIImageView alloc] initWithFrame:CGRectMake(80, 35, 120, 120)];
                [alertImage setImage:[UIImage imageNamed:@"icon_wrong room.png"]];
                [alert addSubview:alertImage];
            }
            [alert show];
            
            //-------------Log----------
            if([LogFileManager isLogConsole])
            {
                NSInteger userId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
                NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
                if (logCheckNewValue == 0 && logCheckUsedValue == 0) {
                    NSLog(@"<%@><Minibar><UserId:%d><Invalid room to post><input RoomId:%@>", time, (int)userId, txtRoom.text);
                }
                else {
                    NSInteger totalSections = [self numberOfSectionsInTableView:tbvMinibar];
                    
                    for (int i = 0; i<totalSections; i++) {
                        NSInteger totalRow = [self tableView:self.tbvMinibar numberOfRowsInSection:i];
                        for (int j = 0; j<totalRow; j++) {
                            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:j inSection:i];
                            UITableViewCell *cell = [self.tbvMinibar cellForRowAtIndexPath:indexPath];
                            UILabel *lblItemName = (UILabel *)[cell viewWithTag:tagItemNameCell];
                            UIButton *btnUsed = (UIButton *)[cell viewWithTag:tagUsedButtonCell];
                            
                            UIButton *btnNew = (UIButton *)[cell viewWithTag:tagNewButtonCell];
                            NSString *valueBtnUsed = btnUsed.titleLabel.text;
                            NSString *valueBtnNew = btnNew.titleLabel.text;
                            if ([valueBtnNew isEqualToString:@"0"] && [valueBtnUsed isEqualToString:@"0"]) {
                                
                            }
                            else{
                                NSLog(@"<%@><Minibar><UserId:%d><Invalid room to post><Input RoomId:%@><ItemName:%@><ItemUsed:%@><ItemNew:%@>", time, (int)userId, txtRoom.text, lblItemName.text, valueBtnUsed, valueBtnNew);
                            }
                        }
                    }
                }
            }
            //-------------Log----------
        }
    }
}

- (IBAction)btnPostingHistory_Clicked:(UIButton *)sender {
    HistoryPosting *historyPosting = [[HistoryPosting alloc] initWithNibName:@"HistoryPosting" bundle:nil];
//    historyPosting.isPushFrom = IS_PUSHED_FROM_PHYSICAL_CHECK;
    historyPosting.roomID = txtRoom.text;
    historyPosting.isFromPostingFunction = isFromMainPosting;
    historyPosting.selectedModule = ModuleHistory_Minibar;
    [self.navigationController pushViewController:historyPosting animated:YES];
}

#pragma mark - Access Right
-(void) loadAccessRights
{
    AccessRightModel *currentUserAccessRight = [[UserManagerV2 sharedUserManager] currentUserAccessRight];
    QRCodeScanner = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.QRCodeScanner];
    postingMinibar = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.postMinibar];
    actionMinibar = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.actionMinibar];
    isPostingFunctionMinibarNew = [UserManagerV2 sharedUserManager].currentUserAccessRight.isPostingFunctionMinibarNew;
    isPosingFunctionMinibarUsed = [UserManagerV2 sharedUserManager].currentUserAccessRight.isPosingFunctionMinibarUsed;
    isRoomDetailsActionMinibarNew = [UserManagerV2 sharedUserManager].currentUserAccessRight.isRoomDetailsActionMinibarNew;
    isRoomDetailsActionMinibarUsed = [UserManagerV2 sharedUserManager].currentUserAccessRight.isRoomDetailsActionMinibarUsed;
    if (![UserManagerV2 sharedUserManager].currentUserAccessRight.isShowActionHistoryPosting) {
        btnPostingHistory.hidden = YES;
    }
}
#pragma mark - QR Code Button Touched
-(IBAction)btnQRCodeScannerTouched:(id) sender
{
    ZXingWidgetController *widController = [[ZXingWidgetController alloc] initWithDelegate:self showCancel:YES OneDMode:NO showLicense:NO];
    QRCodeReader* qrcodeReader = [[QRCodeReader alloc] init];
    NSSet *readers = [[NSSet alloc ] initWithObjects:qrcodeReader,nil];
    widController.readers = readers;
    [self presentViewController:widController animated:YES completion:nil];
}

#pragma mark - ZxingDelegate
- (void)zxingController:(ZXingWidgetController*)controller didScanResult:(NSString *)resultString {
    resultString = [STEncryptorDES convertText:resultString];
    [self dismissViewControllerAnimated:YES completion:nil];
    
    txtRoom.text = resultString;
    [self textFieldShouldReturn:txtRoom];
}

- (void)zxingControllerDidCancel:(ZXingWidgetController*)controller {
    [self dismissViewControllerAnimated:YES completion:nil];
}

//-(IBAction) btnSumbitClicked:(id)sender
//{
//    if (statusSave == YES) {
//        [self saveMinibarCart];
//    }
//    statusChooseMinibar = NO;
//    statusSave = NO;
//}

#pragma mark - iOS 7 Resources
-(void)loadFlatResource
{
    [imgHeader setImage:[UIImage imageNamed:imgGuestInfoActiveFlat]];
    [btnSubmit setImage:[UIImage imageNamed:imgBtnSubmitFlat] forState:UIControlStateNormal];
    [btnSubmit setImage:[UIImage imageNamed:imgBtnSubmitClickedFlat] forState:UIControlStateHighlighted];
    [btnPostingHistory setBackgroundImage:[UIImage imageNamed:imgHistoryPostingBtnFlat] forState:UIControlStateHighlighted];
    [btnPostingHistory setBackgroundImage:[UIImage imageNamed:imgHistoryPostingBtnFlat] forState:UIControlStateNormal];
}
-(void) setEnableBtnAtionHistory:(BOOL) isEnable
{
    if(![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]){
        isEnable = NO;
    }
    [btnPostingHistory setUserInteractionEnabled:isEnable];
    [btnPostingHistory setBackgroundImage:[UIImage imageBeforeiOS7:(isEnable?imgCleaningStatusBtnAndActionBtn:imgCleaningStatusBtnAndActionBtnDisable)
                                                         equaliOS7:(isEnable?imgCleaningStatusBtnAndActionBtnFlat:imgCleaningStatusBtnAndActionBtnDisableFlat)]
                                 forState:UIControlStateNormal];
    [btnPostingHistory setBackgroundImage:[UIImage imageBeforeiOS7:(isEnable?imgCleaningStatusBtnAndActionBtnClicked:imgCleaningStatusBtnAndActionBtnDisable)
                                                         equaliOS7:(isEnable?imgCleaningStatusBtnAndActionBtnClickedFlat:imgCleaningStatusBtnAndActionBtnDisableFlat)]
                                 forState:UIControlEventTouchDown];
    [btnPostingHistory setTitle:[L_action_history currentKeyToLanguage] forState:UIControlStateNormal];
}

@end
