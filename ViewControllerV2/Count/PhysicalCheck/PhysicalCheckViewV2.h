//
//  PhysicalCheckViewV2.h
//  mHouseKeeping
//
//  Created by Quang Nguyen on 2/18/13.
//
//

#import <UIKit/UIKit.h>
#import <ZXingWidgetController.h>

#import "NSString+Common.h"

@interface PhysicalCheckViewV2 : UIViewController<UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, ZXingDelegate, RemarkViewV2Delegate>
{
    BOOL isDND;
    int roomStatusId;
    BOOL isFromMainPosting;
    UIView *viewRoom;
    UILabel *lblDND;
    UILabel *lblRoomNo;
    UILabel *lblFSubmit;
    NSInteger indexOfRow;
    
    AccessRight *QRCodeScanner;
    AccessRight *postingPhysicalCheck;
    int rowSelected;
    
    IBOutlet UIImageView *imgHeader;
    
    NSString *valueRemark;
}

@property (weak, nonatomic) IBOutlet UIView *footerView;
@property (strong, nonatomic) IBOutlet UITableView *tbvMenu;
@property (strong, nonatomic) NSMutableArray *data;
@property (strong, nonatomic) IBOutlet UIButton *btnSubmit;
@property (strong, nonatomic) IBOutlet UIButton *btnDND;
@property (weak, nonatomic) IBOutlet UIButton *btnPostingHistory;
@property (strong, nonatomic) IBOutlet UITextField *txtRoom;
@property (strong, nonatomic) IBOutlet UIButton *btnQR;
@property (nonatomic) BOOL isDND;
@property (nonatomic) int roomStatusId;
@property (nonatomic) BOOL isFromMainPosting;
@property (strong, nonatomic) IBOutlet UIView *viewRoom;
@property (strong, nonatomic) IBOutlet UILabel *lblDND;
@property (strong, nonatomic) IBOutlet UILabel *lblRoomNo;
@property (strong, nonatomic) IBOutlet UILabel *lblFSubmit;
@property (nonatomic) NSInteger indexOfRow;

- (IBAction)didClickHistory:(UIButton *)sender;
//- (void) setCaptionView;
-(IBAction)btnSubmit_Clicked:(id)sender;
-(IBAction)btnDND_Clicked:(id)sender;
-(IBAction) btnQRCodeScannerTouched:(id) sender;

@end
