//
//  PhysicalCheckViewV2.m
//  mHouseKeeping
//
//  Created by Quang Nguyen on 2/18/13.
//
//

#import "PhysicalCheckViewV2.h"
#import "PhysicalCheckCellV2.h"
#import "PhysicalHistoryModel.h"
#import "PhysicalHistoryManager.h"
#import "RoomManagerV2.h"
#import "MyNavigationBarV2.h"
#import "QRCodeReader.h"
#import "LogFileManager.h"
#import "DeviceManager.h"
#import "ehkDefinesV2.h"
#import "iToast.h"
#import "NetworkCheck.h"
#import "SyncManagerV2.h"
#import "ehkDefines.h"
#import "CustomAlertViewV2.h"
#import "STEncryptorDES.h"
#import "UIImage+Tint.h"
#import "HistoryPosting.h"
#import "TasksManagerV2.h"
#import "TopbarViewV2.h"
#import "HomeViewV2.h"
#import "ZXingWidgetController.h"

@implementation PhysicalCheckViewV2
@synthesize tbvMenu;
@synthesize data;
@synthesize btnSubmit;
@synthesize isDND;
@synthesize btnDND;
@synthesize btnQR;
@synthesize txtRoom;
@synthesize roomStatusId;
@synthesize isFromMainPosting;
@synthesize viewRoom;
@synthesize lblDND;
@synthesize lblFSubmit;
@synthesize lblRoomNo;
@synthesize indexOfRow;
@synthesize btnPostingHistory;

#define tagOfPhysicalCell 100
#define tagOfRemarkCell 101
#define tagOfRemarkValue 102
#define FONTLABEL @"Helvetica-Bold"
#define FONTLABELSIZE 17
#define FONTTEXTSIZE 15
#define REDLABEL 6.0f/255.0f
#define GREENLABEL 62.0f/255.0f
#define BLUELABEL 127.0f/255.0f
#define tagText 5678

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)enableQRCode:(BOOL)isEnable{
    [btnQR setAlpha:isEnable ? 1.0f : 0.7f];
    [btnQR setEnabled:isEnable];
}
//CRF1619
- (void)loadAccessQRCode{
    if ([UserManagerV2 sharedUserManager].currentUserAccessRight.isPostingFunctionPhysicalCheckQRCode == 1) {
        self.txtRoom.enabled = NO;
        [self enableQRCode:YES];
    }else if ([UserManagerV2 sharedUserManager].currentUserAccessRight.isPostingFunctionPhysicalCheckQRCode == 0) {
        self.txtRoom.enabled = YES;
        [self enableQRCode:NO];
    }
}
- (void)viewDidLoad
{
    [self loadAccessRights];
    if(!ENABLE_QRCODESCANNER)
    {
        if(ENABLE_CHECK_ACCESS_RIGHT_QRCODESCANNER)
        {
            [btnQR setHidden:NO];
            [self enableQRCode:NO];
            
            if (QRCodeScanner.isActive && !isDemoMode) {
                [self enableQRCode:YES];
            }
            
        }
        else
        {
            [btnQR setHidden:YES];
        }
    }
   
    if([LogFileManager isLogConsole])
    {
        NSInteger userId = [[UserManagerV2 sharedUserManager] currentUser].userId;
        NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSLog(@"<%@><PhysicalCheck><UserId:%d><viewDidLoad>", time, (int)userId);
    }
    
    [super viewDidLoad];
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        //Clear redundant view for header, footter
        CGRect frameHeaderFooter = tbvMenu.tableHeaderView.frame;
        frameHeaderFooter.size.height = 0.1f;
        UIView *headerView = [[UIView alloc] initWithFrame:frameHeaderFooter];
        [tbvMenu setTableHeaderView:headerView];
        
//        frameHeaderFooter = tbvMenu.tableFooterView.frame;
//        frameHeaderFooter.size.height = 0.1f;
//        UIView *footerView = [[UIView alloc] initWithFrame:frameHeaderFooter];
//        [tbvMenu setTableFooterView:footerView];
        
        [self loadFlatResource];
    }
    
    
    
    [self setEnableBtnAtionHistory:NO];
    //Resize content views of current VC to fit with Navigation bar and tabbar
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    // Do any additional setup after loading the view from its nib.
//    self.data = [[RoomManagerV2 sharedRoomManager] loadAllRoomStatus];
    self.data = [[NSMutableArray alloc] init];
    [self loadAllRoomCheck];
    [tbvMenu setBackgroundView:nil];
    [tbvMenu setBackgroundColor:[UIColor clearColor]];
    [tbvMenu setOpaque:YES];
    [self.tbvMenu reloadData];
    isDND = NO;
    rowSelected = -1;
    [btnDND setImage:[UIImage imageNamed:@"btn_checkbox.png"] forState:UIControlStateNormal];
    [btnSubmit setEnabled:YES];
    
//    [lblRoomNo.text setText:[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOM_NO];
    
    [self performSelector:@selector(loadTopbarView)];
    
    if(isFromMainPosting)
    {
        [txtRoom setText:@""];
        [txtRoom setEnabled:YES];
        [txtRoom setTextColor:[UIColor blackColor]];
        [self loadAccessQRCode];
        btnPostingHistory.hidden = ![UserManagerV2 sharedUserManager].currentUserAccessRight.isShowActionHistoryPosting;
//        if (![UserManagerV2 sharedUserManager].currentUserAccessRight.isShowActionHistoryPosting) {
//            CGRect frame = self.footerView.frame;
//            frame.size.height = 84;
//            self.footerView.frame = frame;
//            [tbvMenu setTableFooterView:self.footerView];
//        }
    }
    else
    {
//        [RoomManagerV2 getCurrentRoomNo]
        int roomAssignment = (int)[TasksManagerV2 getCurrentRoomAssignment];
        RoomAssignmentModelV2 *roomAssignmentModel = [[RoomAssignmentModelV2 alloc] init];
        roomAssignmentModel.roomAssignment_Id = roomAssignment;
        [[RoomManagerV2 sharedRoomManager]getRoomIdByRoomAssignment:roomAssignmentModel];
        [txtRoom setText:roomAssignmentModel.roomAssignment_RoomId];
        [txtRoom setEnabled:NO];
        [txtRoom setTextColor:[UIColor grayColor]];
    }
    
    roomStatusId = 0;
    
    //Clear background of viewRoom
    [viewRoom setBackgroundColor:[UIColor clearColor]];

}
-(void) setEnableBtnAtionHistory:(BOOL) isEnable
{
    if(![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]){
        isEnable = NO;
    }
    [btnPostingHistory setUserInteractionEnabled:isEnable];
    [btnPostingHistory setBackgroundImage:[UIImage imageBeforeiOS7:(isEnable?imgCleaningStatusBtnAndActionBtn:imgCleaningStatusBtnAndActionBtnDisable)
                                                 equaliOS7:(isEnable?imgCleaningStatusBtnAndActionBtnFlat:imgCleaningStatusBtnAndActionBtnDisableFlat)]
                         forState:UIControlStateNormal];
    [btnPostingHistory setBackgroundImage:[UIImage imageBeforeiOS7:(isEnable?imgCleaningStatusBtnAndActionBtnClicked:imgCleaningStatusBtnAndActionBtnDisable)
                                                 equaliOS7:(isEnable?imgCleaningStatusBtnAndActionBtnClickedFlat:imgCleaningStatusBtnAndActionBtnDisableFlat)]
                         forState:UIControlEventTouchDown];
    [btnPostingHistory setTitle:[L_action_history currentKeyToLanguage] forState:UIControlStateNormal];
}
-(void)loadWSRemark
{
    // @ show loading data .
//    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
//    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getRaSavingData]];
//    [self.tabBarController.view addSubview:HUD];
//    [HUD show:YES];
    
    if([[UserManagerV2 sharedUserManager].currentUserAccessRight isShowPhyshicalCheckRemark]) {
        NSString *remarkWS = [[RoomManagerV2 sharedRoomManager] getRoomRemarksByUserId:[UserManagerV2 sharedUserManager].currentUser.userId hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId remarkType:RemarkType_PhyshicalCheck roomNumber:txtRoom.text];
        if(remarkWS.length > 0){
            valueRemark = remarkWS;
        }
        else{
            valueRemark = @"";
        }
        [tbvMenu reloadData];
    }
    [self setEnableBtnAtionHistory:YES];
//    [HUD hide:YES];
//    [HUD removeFromSuperview];
}

-(void)loadAllRoomCheck
{
    NSMutableArray *rooms = [[RoomManagerV2 sharedRoomManager] loadAllRoomStatus];
    
//#warning hard code OOS status
//    RoomStatusModelV2 *roomStatusOOS = [[RoomStatusModelV2 alloc] init];
//    roomStatusOOS.rstat_Code = @"OOS";
//    roomStatusOOS.rstat_Name = @"OOS";
//    roomStatusOOS.rstat_Lang = @"OOS";
//    roomStatusOOS.rstat_Image = UIImagePNGRepresentation([UIImage imageNamed:@"oos.png"]);
//    roomStatusOOS.rstat_Id = 11;
//    [rooms addObject:roomStatusOOS];
    if(isDemoMode){
        for(RoomStatusModelV2 *roomStatus in rooms){
            if(roomStatus.rstat_Id == ENUM_ROOM_STATUS_OC ||
               roomStatus.rstat_Id == ENUM_ROOM_STATUS_OD ||
               roomStatus.rstat_Id == ENUM_ROOM_STATUS_VC ||
               roomStatus.rstat_Id == ENUM_ROOM_STATUS_VD){
                [self.data addObject:roomStatus];
            }
        }
    }
    else{
        if([[UserManagerV2 sharedUserManager].currentUserAccessRight isPhysicalCheckRoomStatusFromRoomTable]) {
            for(RoomStatusModelV2 *roomStatus in rooms){
                if(roomStatus.rstat_PhysicalCheck == 1){
                    [self.data addObject:roomStatus];
                }
            }
        }
        else{
            for(RoomStatusModelV2 *roomStatus in rooms)
            {
                if(roomStatus.rstat_Id == ENUM_ROOM_STATUS_OC ||
                   roomStatus.rstat_Id == ENUM_ROOM_STATUS_OD ||
                   roomStatus.rstat_Id == ENUM_ROOM_STATUS_VC ||
                   roomStatus.rstat_Id == ENUM_ROOM_STATUS_VD ||
                   roomStatus.rstat_Id == ENUM_ROOM_STATUS_OOO ||
                   roomStatus.rstat_Id == ENUM_ROOM_STATUS_OOS
                   )
                {
                    //OC in first
                    if(roomStatus.rstat_Id == room_Status_Id_OC)
                    {
                        [self.data insertObject:roomStatus atIndex:0];
                    }
                    //OD in second
                    else if(roomStatus.rstat_Id == room_Status_Id_OD)
                    {
                        RoomStatusModelV2 *firstRoomStatus = nil;
                        if([self.data count] > 0)
                        {
                            firstRoomStatus = [self.data objectAtIndex:0];
                        }
                        
                        if(firstRoomStatus != nil && firstRoomStatus.rstat_Id == room_Status_Id_OC)
                        {
                            //Insert after OC
                            [self.data insertObject:roomStatus atIndex:1];
                        }
                        else //If don't have OC
                        {
                            [self.data insertObject:roomStatus atIndex:0];
                        }
                    }
                    else
                    {
                        [self.data addObject:roomStatus];
                    }
                }
            }
        }
    }
}

-(void)loadTopbarView {
    TopbarViewV2 *topbar = [[TopbarViewV2 alloc] initViewWithSuperModel:[[CommonVariable sharedCommonVariable] superRoomModel]];
    [topbar setTag:tagTopbarView];
    [self.view addSubview:topbar];
    [topbar setHidden:YES];
    
    [topbar setAlpha:0.0];
    [self addButtonHandleShowHideTopbar];
}

-(void)showTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationWillStartSelector:@selector(setHiddenTopbarView)];
    
    f = topbar.frame;
    f.origin.y = 0;
    [topbar setFrame:f];
    [topbar setAlpha:1.0];
    
    [self adjustShowForViews];
    
    [UIView commitAnimations];
}

-(void)hideTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDidStopSelector:@selector(setHiddenTopbarView)];
    [UIView setAnimationDelegate:self];
    
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    [topbar setAlpha:0.0];
    
    [self adjustRemoveForViews];
    
    [UIView commitAnimations];
}

-(void)setHiddenTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    [topbar setHidden:!topbar.hidden];
}

-(TopbarViewV2 *)getTopBarView {
    return (TopbarViewV2 *)[self.view viewWithTag:tagTopbarView];
}

-(void)addButtonHandleShowHideTopbar {
    UIView *vBarButton = [[UIView alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
    UIButton *titleBarButtonFirst = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 200, 30)];
    
    titleBarButtonFirst.titleLabel.backgroundColor = [UIColor clearColor];
    [titleBarButtonFirst.titleLabel setFont: FONT_BUTTON_TOPBAR];
    titleBarButtonFirst.titleLabel.textAlignment = NSTextAlignmentCenter;
    titleBarButtonFirst.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
    
    [titleBarButtonFirst.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
    [titleBarButtonFirst.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
    [titleBarButtonFirst addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];  ///WILL MODIFY
    [titleBarButtonFirst setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_Counts] forState:UIControlStateNormal];
    
    [vBarButton addSubview:titleBarButtonFirst];
    
    UIButton *titleBarButtonSecond = [[UIButton alloc] initWithFrame:CGRectMake(0, 28, 200, 22)];
    [titleBarButtonSecond setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [titleBarButtonSecond.titleLabel setFont:FONT_BUTTON_TOPBAR_SECOND];
    [titleBarButtonSecond setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_Physical_Check_Title] forState:UIControlStateNormal];// Set title'Minibar' for tabbar
    [titleBarButtonSecond addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];   ///WILL MODIFY
    [vBarButton addSubview:titleBarButtonSecond];
    
    self.navigationItem.titleView = vBarButton;
    
    //back button navigation
    self.navigationItem.leftBarButtonItem = [MyNavigationBarV2 createBackButtonWithTarget:self selector:@selector(btnBackPressed) forControlEvents:UIControlEventTouchUpInside];
    
    //Move title to center vertical
    CGRect titleViewBounds = self.navigationItem.titleView.bounds;
    int moveLeftTitle = 0;
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        moveLeftTitle = 10;
    }
    self.navigationItem.titleView.bounds = CGRectMake(titleViewBounds.origin.x + moveLeftTitle, Y_TOPBAR_TITLE, titleViewBounds.size.width, titleViewBounds.size.height);
    
    //set text by string get from xml's file for label 
    [lblRoomNo setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOM_NO]];
    [lblDND setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_DND_ROOM]];
    [lblFSubmit setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_SUBMIT]];
}

-(void)btnBackPressed
{
    if([LogFileManager isLogConsole])
    {
        NSInteger userId = [[UserManagerV2 sharedUserManager] currentUser].userId;
        NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSLog(@"<%@><Engineering><UserId:%d<Press back button>", time, (int)userId);
    }
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)topbarTapped:(UIButton *)sender {
    TopbarViewV2 *topbar = [self getTopBarView];
    [sender setEnabled:NO];
    if (topbar.isHidden) {
        [self showTopbarView];
    } else {
        [self hideTopbarView];
    }
    [self performSelector:@selector(topbarButtonEnable:) withObject:sender afterDelay:0.5];
}

-(void) topbarButtonEnable:(UIButton *) topbarBtn {
    [topbarBtn setEnabled:YES];
}

-(void)adjustShowForViews {
    //adjust all views when show
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    CGRect fRoomView = viewRoom.frame;
    [viewRoom setFrame:CGRectMake(0, f.size.height, 320, fRoomView.size.height)];
    
    CGRect ftbv = tbvMenu.frame;
//    [tbvMenu setFrame:CGRectMake(0, f.size.height + fRoomView.size.height + 33, 320, ftbv.size.height - f.size.height)];
//    [tbvMenu setFrame:CGRectMake(0, f.size.height + fRoomView.size.height, 320, ftbv.size.height - f.size.height)];
    [tbvMenu setFrame:CGRectMake(0, f.size.height +fRoomView.size.height, 320, ftbv.size.height - f.size.height - 5)];
    
}

-(void)adjustRemoveForViews {
    //adjust all views when hide
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    CGRect fRoomView = viewRoom.frame;
    [viewRoom setFrame:CGRectMake(0, 0, 320, fRoomView.size.height)];
    
    CGRect ftbv = tbvMenu.frame;
//    [tbvMenu setFrame:CGRectMake(0, fRoomView.size.height + 33, 320, ftbv.size.height + f.size.height)];
    [tbvMenu setFrame:CGRectMake(0, fRoomView.size.height, 320, ftbv.size.height + f.size.height + 5)];
 
}

- (void) reachabilityDidChanged: (NSNotification* )note
{
    if(![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]){
        [btnSubmit setEnabled:NO];
        [self setEnableBtnAtionHistory:NO];
    }
    else{
        [btnSubmit setEnabled:YES];
        [self setEnableBtnAtionHistory:YES];
    }
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if(!isDemoMode){
        //show wifi view
        [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationShowWifiView] object:nil];
        if(![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]){
            [btnSubmit setEnabled:NO];
        }
        else{
            [btnSubmit setEnabled:YES];
        }
    }
    if(isFromMainPosting){
        if(!isDemoMode){
            [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(reachabilityDidChanged:) name: kReachabilityChangedNotification object: nil];
        }
        [[HomeViewV2 shareHomeView] setSelectedButton:tagOfPostingButton];
    }
    else{
        [[HomeViewV2 shareHomeView] setSelectedButton:tagOfHomeButton];
    }
    [self setCaptionView];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    if(!isDemoMode){
        [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UITableView DataSource Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    if([[UserManagerV2 sharedUserManager].currentUserAccessRight isShowPhyshicalCheckRemark]) {
        return 2;
    }
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(section == 0) {
        return [self.data count];
    } else {
        return 1;
    }
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 1) {
        return 80;
    }
    return 44;
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    //    CountServiceModelV2 *model;
    
    static NSString *cellidentifier = @"CellIdentifier";
    static NSString *cellRemarkId = @"cellRemarkId";
    if(indexPath.section==0)
    {
        PhysicalCheckCellV2 *countcell = nil;
        [countcell setTag:tagOfPhysicalCell];
        countcell = [tableView dequeueReusableCellWithIdentifier:cellidentifier];
        if(countcell == nil){
            NSArray *array = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([PhysicalCheckCellV2 class]) owner:self options:nil];
            countcell = [array objectAtIndex:0];
            if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                [countcell.contentView setBackgroundColor:[UIColor whiteColor]];
            }
        }
        RoomStatusModelV2 *roomStatus = (RoomStatusModelV2 *)[self.data objectAtIndex:indexPath.row];
        
        if(roomStatus.rstat_Id == roomStatusId) {
            [tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
        }
        [countcell.imageView setImage:[UIImage imageWithData:roomStatus.rstat_Image]];
        
        if([[LanguageManagerV2 sharedLanguageManager] currentLanguageIsEqualTo:@"%@", ENGLISH_LANGUAGE]){
            [countcell.label setText:roomStatus.rstat_Name];
        } else {
            [countcell.label setText:roomStatus.rstat_Lang];
        }
        
        //Uncomment for room status code
        //[countcell.label setText:roomStatus.rstat_Code];
        
        if(isDND && (roomStatus.rstat_Id == room_Status_Id_VD || roomStatus.rstat_Id == room_Status_Id_VC))
        {
            [countcell setSelectionStyle:UITableViewCellSelectionStyleNone];
            if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                [countcell.contentView setBackgroundColor:[UIColor grayColor]];
            } else {
                [countcell setBackgroundColor:[UIColor grayColor]];
            }
        }
        else
        {
            [countcell setSelectionStyle:UITableViewCellSelectionStyleBlue];
            if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                [countcell.contentView setBackgroundColor:[UIColor whiteColor]];
            } else {
                [countcell setBackgroundColor:[UIColor whiteColor]];
            }
        }
        return countcell;
    }
    else
    {
        UITableViewCell *cell = nil;
        //Remark cell
        if (indexPath.row == 0) {
            UILabel *lblRemark = nil;
            UILabel *remarkValue = nil;
            
            cell = [tableView dequeueReusableCellWithIdentifier:cellRemarkId];
            if(!cell) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellRemarkId];
                lblRemark = [[UILabel alloc] initWithFrame:CGRectMake(10,0, 100,30 )];
                //set tag for remark label
                lblRemark.tag = tagOfRemarkCell;
                //clear background color
                lblRemark.backgroundColor = [UIColor clearColor];
                
                [lblRemark setTextColor:[UIColor colorWithRed:REDLABEL green:GREENLABEL blue:BLUELABEL alpha:1]];
                [lblRemark setFont:[UIFont fontWithName:FONTLABEL size:FONTLABELSIZE]];
                
                //Value of remark after user input
                remarkValue = [[UILabel alloc] initWithFrame:CGRectMake(10,30 ,280, 50)];
                //set tag for remark value
                remarkValue.tag = tagOfRemarkValue;
                remarkValue.backgroundColor = [UIColor clearColor];
                [remarkValue setNumberOfLines:2];
                
                //nextImageView.frame = CGRectMake(267,5, 25, 25);
                [cell.contentView addSubview:lblRemark];
                [cell.contentView addSubview:remarkValue];
                //[cell setAccessoryView:nextImageView];
            }
            
            lblRemark = (UILabel*)[cell.contentView viewWithTag:tagOfRemarkCell];
            remarkValue = (UILabel*)[cell.contentView viewWithTag:tagOfRemarkValue];
            [remarkValue setText:valueRemark];
            
            //Set value for label remark
            [lblRemark setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_remark_title]];
        }
        
        return cell;
    }
}
#pragma mark - UITableView Delegate methods
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section==0)
    {
        rowSelected = (int)indexPath.row;
        RoomStatusModelV2 *roomStatus = (RoomStatusModelV2 *)[self.data objectAtIndex:indexPath.row];
        //    if(isDND && ([roomStatus.rstat_Name isEqualToString:@"VD"] || [roomStatus.rstat_Name isEqualToString:@"VC"]))
        if((!isDemoMode && ((isDND && (roomStatus.rstat_Id == room_Status_Id_VD || roomStatus.rstat_Id == room_Status_Id_VC)) || ![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp] || postingPhysicalCheck.isAllowedView)) || (isDemoMode && ((isDND && (roomStatus.rstat_Id == room_Status_Id_VD || roomStatus.rstat_Id == room_Status_Id_VC)) || postingPhysicalCheck.isAllowedView)))
        {
            [btnSubmit setEnabled:NO];
        }
        else
        {
            [btnSubmit setEnabled:YES];
        }
        
        
        //Hao Tran[20130517/Check Access right] - Check access right only view
//        if(!isDemoMode)
//        {
//            if(postingPhysicalCheck.isAllowedView)
//            {
//                [btnSubmit setEnabled:NO];
//            }
//        }
        
        //Hao Tran[20130517/Check Access right] - END
        
        roomStatusId = roomStatus.rstat_Id;
        //----------Log action----------
        if([LogFileManager isLogConsole])
        {
            NSInteger UserId = [[UserManagerV2 sharedUserManager] currentUser].userId;
            NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
            RoomStatusModelV2 *roomStatus = (RoomStatusModelV2 *)[self.data objectAtIndex:indexPath.row];
            indexOfRow = indexPath.row;
            NSLog(@"<%@><PhysicalCheck><UserId:%d><Click row %@ status>", time, (int)UserId, roomStatus.rstat_Name);
        }
    }
    else
    {
        RemarkViewV2 *remarkView = [[RemarkViewV2 alloc] initWithNibName:NSStringFromClass([RemarkViewV2 class]) bundle:nil];
        remarkView.delegate = self;
//        [remarkView setDelegate:self];
        [remarkView setTextinRemark:valueRemark];
        [self.navigationController pushViewController:remarkView animated:YES];
    }
    
}
-(void)remarkViewV2DoneWithText:(NSString *)text andController:(UIViewController *)controllerView{

    [controllerView.navigationController popViewControllerAnimated:YES];
//    UITableViewCell *cellRemark = [tbvMenu cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
//    // [self tableView:tbvMenu cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
//    
//    UILabel *remarkValue = (UILabel*)[cellRemark.contentView viewWithTag:tagOfRemarkValue];
//    [remarkValue setText:text];
    
    valueRemark = text;
    [tbvMenu reloadData];
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 10;
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *footerview = [[UIView alloc]init];
    [footerview setBackgroundColor:[UIColor clearColor]];
    return footerview;
}
- (void) setCaptionView{
    [self setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_Counts]];
    
    //refresh topview
//    TopbarViewV2 *topview = [self getTopBarView];
//    [topview refresh:[[SuperRoomModelV2 alloc] init]];
}

- (void)savePhysicalCheckHistory {
    
    PhysicalHistoryManager *manager = [[PhysicalHistoryManager alloc] init];
//    NSMutableArray *floorArray = [manager loadAllPhysicalHistoryByFloorId:];
    PhysicalHistoryModel *physicalHistory = [[PhysicalHistoryModel alloc] init];
    physicalHistory.ph_room_id = txtRoom.text;
    
    //Get FloorID
    RoomManagerV2 *roomManager = [[RoomManagerV2 alloc] init];
    RoomModelV2 *roomModel = [[RoomModelV2 alloc]init];
    roomModel.room_Id = txtRoom.text;
    [roomManager loadRoomModel:roomModel];
    physicalHistory.ph_floor_id = roomModel.room_floor_id;
    physicalHistory.ph_dnd = isDND;
    physicalHistory.ph_status = roomStatusId;
    physicalHistory.ph_date = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSInteger currentUserID = [[UserManagerV2 sharedUserManager] currentUser].userId;
    physicalHistory.ph_user_id = currentUserID;
    [manager insertPhysicalHistoryModel:physicalHistory];
}

-(void)savePhysicalCheckHistoryWithFloorId:(int)floorId {
    
    PhysicalHistoryManager *manager = [[PhysicalHistoryManager alloc] init];
    PhysicalHistoryModel *physicalHistory = [[PhysicalHistoryModel alloc] init];
    physicalHistory.ph_room_id = txtRoom.text;
    physicalHistory.ph_floor_id = floorId;
    physicalHistory.ph_dnd = isDND;
    physicalHistory.ph_status = roomStatusId;
    physicalHistory.ph_date = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSInteger currentUserID = [[UserManagerV2 sharedUserManager] currentUser].userId;
    physicalHistory.ph_user_id = currentUserID;
    [manager insertPhysicalHistoryModel:physicalHistory];
}

-(IBAction)btnSubmit_Clicked:(id)sender {
    
    NSString *roomNo = [txtRoom text];
    if (roomNo.length > 0 && roomStatusId > 0) {
        
        RoomAssignmentModelV2 *roomAssignment = [[RoomAssignmentModelV2 alloc] init];
        roomAssignment.roomAssignment_RoomId = roomNo;
        roomAssignment.roomAssignment_UserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
        if (isDemoMode) {
            //NSString *mess = [NSString stringWithFormat:@"Room %@ \n Submit Room Status?", roomNo];
            NSString *mess = [NSString stringWithFormat:@"%@ %@ \n %@?", [L_room currentKeyToLanguage],roomNo,[L_submit_room_status_message currentKeyToLanguage]];
            UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:[L_confirm_title currentKeyToLanguage] message:mess delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles:[[LanguageManagerV2 sharedLanguageManager] getNo], nil];
            alert.tag = 1;
            [alert show];
        } else {
            BOOL result = [[RoomManagerV2 sharedRoomManager] getRoomAssignmentByUserIDAndRoomID:roomAssignment];
            if ([[UserManagerV2 sharedUserManager].currentUserAccessRight isValidateRoom] && !isDemoMode) {
                NSString *hotelId = [NSString stringWithFormat:@"%d", [[[UserManagerV2 sharedUserManager] currentUser] userHotelsId]];
                result = [[RoomManagerV2 sharedRoomManager] isValidRoomWSByUserId:[UserManagerV2 sharedUserManager].currentUser.userId roomNumber:roomNo hotelId:hotelId];
                
            } else {
                result = TRUE; //Force posting mini bar without checking validation room
            }
            //NO need to check room assignment
            //BOOL result = YES;
            if (result) {
                //NSString *mess = [NSString stringWithFormat:@"Room %@ \n Submit Room Status?", roomNo];
                NSString *mess = [NSString stringWithFormat:@"%@ %@ \n %@", [L_room currentKeyToLanguage],roomNo,[L_submit_room_status_message currentKeyToLanguage]];
                UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:[L_confirm_title currentKeyToLanguage]  message:mess delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles:[[LanguageManagerV2 sharedLanguageManager] getNo], nil];
                alert.tag = 1;
                [alert show];
            } else {
                NSString *message = nil;
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
                    message = [NSString stringWithFormat:@"%@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOM_INVALID]];
                } else {
                    message = [NSString stringWithFormat:@"\n\n\n\n\n%@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOM_INVALID]];
                }
                
                UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObject:[UIImage imageNamed:@"yes_bt.png"]] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:message delegate:nil cancelButtonTitle:[L_CLOSE currentKeyToLanguage] otherButtonTitles: nil];
                
                if (!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
                    UIImageView *alertImage = [[UIImageView alloc] initWithFrame:CGRectMake(80, 35, 120, 120)];
                    [alertImage setImage:[UIImage imageNamed:@"icon_wrong room.png"]];
                    [alert addSubview:alertImage];
                }
                [alert show];
                if ([LogFileManager isLogConsole]) {
                    NSInteger userId = [[UserManagerV2 sharedUserManager] currentUser].userId;
                    NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
                    NSLog(@"<%@><Engineering><UserId:%d><Input room No:%@><Invalid Room No to submit>", time, (int)userId, txtRoom.text);
                }
            }
            
        }
        
    } else {
        if (roomNo.length > 0) {
            UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObject:[UIImage imageNamed:@"yes_bt.png"]] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:[L_select_room_status currentKeyToLanguage] delegate:nil cancelButtonTitle:[L_CLOSE currentKeyToLanguage] otherButtonTitles: nil];
            [alert show];
        } else {
            NSString *message = nil;
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
                message = [NSString stringWithFormat:@"%@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOM_INVALID]];
            } else {
                message = [NSString stringWithFormat:@"\n\n\n\n\n%@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOM_INVALID]];
            }
            
            UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObject:[UIImage imageNamed:@"yes_bt.png"]] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:message delegate:nil cancelButtonTitle:[L_CLOSE currentKeyToLanguage] otherButtonTitles: nil];
            
            if (!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
                UIImageView *alertImage = [[UIImageView alloc] initWithFrame:CGRectMake(80, 35, 120, 120)];
                [alertImage setImage:[UIImage imageNamed:@"icon_wrong room.png"]];
                [alert addSubview:alertImage];
            }
            [alert show];
            if ([LogFileManager isLogConsole]) {
                NSInteger userId = [[UserManagerV2 sharedUserManager] currentUser].userId;
                NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
                NSLog(@"<%@><Engineering><UserId:%d><Empty Room No to submit>", time, (int)userId);
            }
        }
    }
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    switch (alertView.tag)
    {
        case 1:
        {
            if(buttonIndex == 0)
            {
                //--------------------------Log-------------
                if([LogFileManager isLogConsole])
                {
                    NSInteger userId = [[UserManagerV2 sharedUserManager] currentUser].userId;
                    NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
                    
                    if (isDND) {
                        
                        //Hao Tran [20141121/Fixed DND can't post to WS] - Fixed for RC deployment
                        //isDND = !isDND;
                        if (indexOfRow == 2) {
                            NSLog(@"<%@><PhysicalCheck><UserId:%d><Press submit to post><Input RoomId:%@><Check DND><Check OC>", time, (int)userId, txtRoom.text);
                        }
                        else if (indexOfRow == 3){
                            NSLog(@"<%@><PhysicalCheck><UserId:%d><Press submit to post><Input RoomId:%@><Check DND><Check OD>", time, (int)userId, txtRoom.text);
                        }
                        else if (indexOfRow == 4) {
                            NSLog(@"<%@><PhysicalCheck><UserId:%d><Press submit to post><Input RoomId:%@><Check DND><Check OO>", time, (int)userId, txtRoom.text);
                        }
                    } else {
                        if (indexOfRow == 0) {
                            NSLog(@"<%@><PhysicalCheck><UserId:%d><Press submit to post><Input RoomId:%@><Check VC>", time, (int)userId, txtRoom.text);
                        }
                        else if (indexOfRow == 1){
                            NSLog(@"<%@><PhysicalCheck><UserId:%d><Press submit to post><Input RoomId:%@><Check VD>", time, (int)userId, txtRoom.text);
                        }
                        else if (indexOfRow == 2) {
                            NSLog(@"<%@><PhysicalCheck><UserId:%d><Press submit to post><Input RoomId:%@><Check OC>", time, (int)userId, txtRoom.text);
                        }
                        else if (indexOfRow == 3) {
                            NSLog(@"<%@><PhysicalCheck><UserId:%d><Press submit to post><Input RoomId:%@><Check OD>", time, (int)userId, txtRoom.text);
                        }
                        else if (indexOfRow == 4) {
                            NSLog(@"<%@><PhysicalCheck><UserId:%d><Press submit to post><Input RoomId:%@><Check OO>", time, (int)userId, txtRoom.text);
                        }
                        
                    }
                }
                //--------------------------Log-------------
               
                [self submitPhysicalCheck];
                
                //Remove for pop view controller
                [btnDND setImage:[UIImage imageNamed:@"btn_checkbox.png"] forState:UIControlStateNormal];
                [tbvMenu reloadData];
                [btnSubmit setEnabled:NO];
                
                //[self.navigationController popViewControllerAnimated:YES];
    
            } else {
                if ([LogFileManager isLogConsole]) {
                    
                    NSInteger userId = [[UserManagerV2 sharedUserManager] currentUser].userId;
                    NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
                    
                    if (isDND) {
                        if (indexOfRow == 2) {
                            NSLog(@"<%@><PhysicalCheck><UserId:%d><Not confirm to post><Input RoomId:%@><Check DND><Check OC>", time, (int)userId, txtRoom.text);
                        }
                        else if (indexOfRow == 3){
                            NSLog(@"<%@><PhysicalCheck><UserId:%d><Not confirm to post><Input RoomId:%@><Check DND><Check OD>", time, (int)userId, txtRoom.text);
                        }
                        else if (indexOfRow == 4) {
                            NSLog(@"<%@><PhysicalCheck><UserId:%d><Not confirm to post><Input RoomId:%@><Check DND><Check OO>", time, (int)userId, txtRoom.text);
                        }
                    } else {
                        if (indexOfRow == 0) {
                            NSLog(@"<%@><PhysicalCheck><UserId:%d><Not confirm to post><Input RoomId:%@><Check VC>", time, (int)userId, txtRoom.text);
                        }
                        else if (indexOfRow == 1){
                            NSLog(@"<%@><PhysicalCheck><UserId:%d><Not confirm to post><Input RoomId:%@><Check VD>", time, (int)userId, txtRoom.text);
                        }
                        else if (indexOfRow == 2) {
                            NSLog(@"<%@><PhysicalCheck><UserId:%d><Not confirm to post><Input RoomId:%@><Check OC>", time, (int)userId, txtRoom.text);
                        }
                        else if (indexOfRow == 3) {
                            NSLog(@"<%@><PhysicalCheck><UserId:%d><Not confirm to post><Input RoomId:%@><Check OD>", time, (int)userId, txtRoom.text);
                        }
                        else if (indexOfRow == 4) {
                            NSLog(@"<%@><PhysicalCheck><UserId:%d><Not confirm to post><Input RoomId:%@><Check OO>", time, (int)userId, txtRoom.text);
                        }
                        
                    }
                }
            }
            break;
        }
    }
}

-(IBAction)btnDND_Clicked:(id)sender
{
    //not set dnd for room VD, VC selected
    if(!isDND && rowSelected >= 0){
        RoomStatusModelV2 *roomStatus = (RoomStatusModelV2 *)[self.data objectAtIndex:rowSelected];
        if(roomStatus.rstat_Id == room_Status_Id_VD || roomStatus.rstat_Id == room_Status_Id_VC) {
            return;
        }
    }
    
    if(isDND)
    {
        [btnDND setImage:[UIImage imageNamed:@"btn_checkbox.png"] forState:UIControlStateNormal];
        if([LogFileManager isLogConsole])
        {
            NSInteger userId = [[UserManagerV2 sharedUserManager] currentUser].userId;
            NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSLog(@"<%@><UserId:%d><Uncheck DND>", time, (int)userId);
        }
    }
    else
    {
        [btnDND setImage:[UIImage imageNamed:@"btn_checkbox(checked).png"] forState:UIControlStateNormal];
        if([LogFileManager isLogConsole])
        {
            NSInteger userId = [[UserManagerV2 sharedUserManager] currentUser].userId;
            NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSLog(@"<%@><UserId:%d><Check DND>", time, (int)userId);
        }
    }
    
    isDND = !isDND;
//    [btnSubmit setEnabled:NO];
    [tbvMenu reloadData];
}

/**
 * Name : shouldChangeCharactersInRange
 * Description : For hiding the keyboard. This must be implemented if you are working with UITextField
 * Parameter :
 * Return : BOOL
 */
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if([string isEqualToString:@"\n"])
    {
        [textField resignFirstResponder];
        return NO;
    }
    return YES;
}


/**f
 * Name : textFieldShouldReturn
 * Description : Hiding the keyboard of UITextField
 * Parameter :
 * Return : BOOL
 */
- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    if([LogFileManager isLogConsole]) {
        NSInteger userId = [[UserManagerV2 sharedUserManager] currentUser].userId;
        NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSLog(@"<%@><PhysicalCheck><UserId:%d><Input RoomId:%@>", time, (int)userId, textField.text);
    }
    [textField resignFirstResponder];
    [self performSelector:@selector(loadWSRemark) withObject:nil];
    return YES;
}

- (void)submitPhysicalCheck
{
    // @ show loading data .
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getRaSavingData]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES];
    SyncManagerV2 *syncManager = [[SyncManagerV2 alloc] init];
    int currentUserID = [[UserManagerV2 sharedUserManager] currentUser].userId;
    int hotelId = [[UserManagerV2 sharedUserManager] currentUser].userHotelsId;
    NSString *roomNo = txtRoom.text;

    int valDND = 0;
    if(isDND) {
        valDND = ENUM_CLEANING_STATUS_DND;
    }
    
    
    int switchLogWS = (int)[[NSUserDefaults standardUserDefaults] integerForKey:ENABLE_LOGWS];
    if (switchLogWS == 1) {
        NSString *checkStatus = nil;
//        if (indexOfRow == 0) {
//            checkStatus = @"Check VC";
//        }
//        else if (indexOfRow == 1)
//            checkStatus = @"Check VD";
//        else if (indexOfRow == 2)
//            checkStatus = @"Check OC";
//        else if (indexOfRow == 3)
//            checkStatus = @"Check OD";
//        else if (indexOfRow == 4)
//            checkStatus = @"Check OO";
        NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSString *messageValue = [NSString stringWithFormat:@"PhysicalCheck Post WSLog: Time:%@, UserId:%d, ValDND:%d, RoomId:%@, CheckStatus %@, indexOfRow:%d", time, currentUserID, valDND, roomNo, checkStatus, (int)indexOfRow];

        [syncManager postWSLogForPhysicalCheck:currentUserID Message:messageValue MessageType:1];
    }
    if(isDemoMode){
        [self savePhysicalCheckHistory];
        [txtRoom setText:@""];
        valueRemark = @"";
    }
    else{
        
        //Hao Tran [20140512]- Fix submit without room status
        if (roomStatusId == 0) {
            UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObject:[UIImage imageNamed:@"yes_bt.png"]] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:[L_select_room_status currentKeyToLanguage] delegate:nil cancelButtonTitle:[L_CLOSE currentKeyToLanguage] otherButtonTitles: nil];
            [alert show];
            //hide saving data.
            [self performSelector:@selector(hiddenHUDAfterSaved:) withObject:HUD afterDelay:0.5];
            return;
        }
        
        eHousekeepingService_PostPhysicalCheck* result = [syncManager postPhysicalCheckWithUserId:currentUserID andRoomNo:roomNo andHotelID:hotelId andStatusId:roomStatusId andCleaningStatusId:valDND];
        
        //Update room remark
        if([[UserManagerV2 sharedUserManager].currentUserAccessRight isShowPhyshicalCheckRemark]) {
            
            if([[UserManagerV2 sharedUserManager].currentUserAccessRight isRemarkConsolidation]) {
                
                [[RoomManagerV2 sharedRoomManager] updateRoomRemarksByUserId:[UserManagerV2 sharedUserManager].currentUser.userId hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId remarkType:RemarkType_ConsolidationRemark roomNumber:txtRoom.text remarkValue:valueRemark];
                
            } else {
                [[RoomManagerV2 sharedRoomManager] updateRoomRemarksByUserId:[UserManagerV2 sharedUserManager].currentUser.userId hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId remarkType:RemarkType_PhyshicalCheck roomNumber:txtRoom.text remarkValue:valueRemark];
            }
        }
        CountOrderDetailsModelV2 *item = [CountOrderDetailsModelV2 new];
        item.countNew = roomStatusId;
        NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
        item.countCollectedDate = time;
        NSArray *listItem = @[item];
        NSInteger userId = [[UserManagerV2 sharedUserManager] currentUser].userId;
        CountManagerV2 *manager = [[CountManagerV2 alloc] init];
        [manager postHistoryItemsWithUserId:userId AndMinibarOrderModelList:listItem roomNumber:txtRoom.text];
        if(result != nil && [result.ResponseStatuses.respCode isEqualToNumber:[NSNumber numberWithInt:RESPONSE_STATUS_OK]]) {
            NSMutableArray *floors = result.FloorInfo.FloorDetails;
            eHousekeepingService_FloorDetails *detail = (eHousekeepingService_FloorDetails*)floors[0];
            // DungPhan - 20150826: Save Physical Check History with floorId in case of Room Assign/Unassign
            [self savePhysicalCheckHistoryWithFloorId:[detail.ID_ intValue]];
            [syncManager getRoomAssignmentWithUserID:[[[UserManagerV2 sharedUserManager] currentUser] userId] AndLastModified:[[HomeViewV2 shareHomeView] getTempLastModifiedRoomAssignment] AndPercentView: nil];
            [txtRoom setText:@""];
            valueRemark = @"";
        }
        
    }
    
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getMsgSaveMessage]];
    
    //hide saving data.
    [self performSelector:@selector(hiddenHUDAfterSaved:) withObject:HUD afterDelay:0.5];
    
}

#pragma didden after save.
-(void) hiddenHUDAfterSaved:(MBProgressHUD *)HUD{
    [HUD hide:YES];
    [HUD removeFromSuperview];
    //    [HUD release];
}


#pragma mark - Access Right
-(void) loadAccessRights
{
    AccessRightModel *currentUserAccessRight = [[UserManagerV2 sharedUserManager] currentUserAccessRight];
    QRCodeScanner = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.QRCodeScanner];
    postingPhysicalCheck = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.postPhysicalCheck];
}

#pragma mark - QR Code Button Touched
-(IBAction)btnQRCodeScannerTouched:(id) sender
{
    ZXingWidgetController *widController = [[ZXingWidgetController alloc] initWithDelegate:self showCancel:YES OneDMode:NO showLicense:NO];
    QRCodeReader* qrcodeReader = [[QRCodeReader alloc] init];
    NSSet *readers = [[NSSet alloc ] initWithObjects:qrcodeReader,nil];
    widController.readers = readers;
    [self presentViewController:widController animated:YES completion:nil];
}

#pragma mark - ZxingDelegate
- (void)zxingController:(ZXingWidgetController*)controller didScanResult:(NSString *)resultString {
    resultString = [STEncryptorDES convertText:resultString];
    [self dismissViewControllerAnimated:YES completion:nil];
    
    txtRoom.text = resultString;
    [self setEnableBtnAtionHistory:YES];
}

- (void)zxingControllerDidCancel:(ZXingWidgetController*)controller {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - iOS 7 Resources
-(void)loadFlatResource
{
    [imgHeader setImage:[UIImage imageNamed:imgGuestInfoActiveFlat]];
    [btnSubmit setImage:[UIImage imageNamed:imgBtnSubmitFlat] forState:UIControlStateNormal];
    [btnSubmit setImage:[UIImage imageNamed:imgBtnSubmitClickedFlat] forState:UIControlStateHighlighted];
    [btnPostingHistory setBackgroundImage:[UIImage imageNamed:imgHistoryPostingBtnFlat] forState:UIControlStateHighlighted];
    [btnPostingHistory setBackgroundImage:[UIImage imageNamed:imgHistoryPostingBtnFlat] forState:UIControlStateNormal];
}

- (IBAction)didClickHistory:(UIButton *)sender {
    HistoryPosting *historyPosting = [[HistoryPosting alloc] initWithNibName:@"HistoryPosting" bundle:nil];
    historyPosting.isPushFrom = IS_PUSHED_FROM_PHYSICAL_CHECK;
    historyPosting.roomID = txtRoom.text;
    historyPosting.selectedModule = ModuleHistory_Physical;
    historyPosting.isFromPostingFunction = isFromMainPosting;
    [self.navigationController pushViewController:historyPosting animated:YES];
}
@end
