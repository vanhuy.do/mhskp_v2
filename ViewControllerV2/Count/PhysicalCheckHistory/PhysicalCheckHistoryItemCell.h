//
//  PhysicalCheckHistoryItemCell.h
//  mHouseKeeping
//
//  Created by Mac User on 25/03/2013.
//
//

#import <UIKit/UIKit.h>
//@protocol ItemCellDelegate <NSObject>
//@end
@interface PhysicalCheckHistoryItemCell : UITableViewCell{
//    __unsafe_unretained id<ItemCellDelegate> delegate;
//    NSIndexPath *path;

}
@property (nonatomic, strong) IBOutlet UILabel *lblRoom;
@property (nonatomic, strong) IBOutlet UILabel *lblStatus;
@property (nonatomic, strong) IBOutlet UILabel *lblDND;
@property (nonatomic, strong) IBOutlet UILabel *lblUnderLine;
@end
