//
//  PhysicalCheckHistoryItemCell.m
//  mHouseKeeping
//
//  Created by Mac User on 25/03/2013.
//
//

#import "PhysicalCheckHistoryItemCell.h"

@implementation PhysicalCheckHistoryItemCell
@synthesize lblDND;
@synthesize lblRoom;
@synthesize lblStatus;
@synthesize lblUnderLine;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
