//
//  PhysicalCheckHistorySection.h
//  mHouseKeeping
//
//  Created by Mac User on 26/03/2013.
//
//

#import <UIKit/UIKit.h>
#import "PhysicalHistoryModel.h"
@class PhysicalCheckHistorySection;

@protocol PhysicalCheckHistorySectionDelegate <NSObject>

@optional
-(void) sectionHeaderView:(PhysicalCheckHistorySection *) sectionheaderView sectionOpened:(NSInteger) section;
-(void) sectionHeaderView:(PhysicalCheckHistorySection *) sectionheaderView sectionClosed:(NSInteger) section;
@end

@interface PhysicalCheckHistorySection : UIView{
    __unsafe_unretained id<PhysicalCheckHistorySectionDelegate> delegate;
    NSInteger section;
    BOOL isToggle;
}
@property (nonatomic, assign) NSInteger section;
@property (nonatomic, assign) id<PhysicalCheckHistorySectionDelegate> delegate;
@property (strong, nonatomic) IBOutlet UIImageView *imgPhysicalCHSection;
@property (strong, nonatomic) IBOutlet UILabel *lblPhysicalCHTitle;
@property (strong, nonatomic) IBOutlet UIImageView *imgDetail;
//- (IBAction)btnPhisicalCHSectionPressed:(id)sender;
-(void) toggleOpenWithUserAction:(BOOL) userAction;
-(id) initWithSection:(NSInteger) section PhysicalHistoryModel:(FloorModelV2 *) model andOpenStatus:(BOOL) isOpen;
@end
