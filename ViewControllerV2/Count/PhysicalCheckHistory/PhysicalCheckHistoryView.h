//
//  PhysicalCheckHistoryView.h
//  mHouseKeeping
//
//  Created by Mac User on 25/03/2013.
//
//

#import <UIKit/UIKit.h>
#import "PhysicalCheckHistorySection.h"
#import "PhysicalHistorySectionInfo.h"
#import "PhysicalCheckHistoryItemCell.h"

@interface PhysicalCheckHistoryView : UIViewController<UITableViewDataSource, UITableViewDelegate, PhysicalCheckHistorySectionDelegate>{
    NSMutableArray *sectionDatas;

    NSString *msgSaveCount;
    NSString *msgbtnYes;
    NSString *msgbtnNo;
//    UILabel *lblAlert;
    UITextView *textView ;
}
@property (nonatomic, strong) IBOutlet UITableView *tbvPhysicalCH;
@property (nonatomic, strong) NSMutableArray *sectionDatas;
@property (nonatomic) NSInteger openSectionIndex;
@property (strong, nonatomic) NSMutableArray *listPhysicalHistory;
@property (strong, nonatomic) NSArray *listFloor;
@end
