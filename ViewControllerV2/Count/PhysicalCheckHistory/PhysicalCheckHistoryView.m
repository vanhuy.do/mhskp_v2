//
//  PhysicalCheckHistoryView.m
//  mHouseKeeping
//
//  Created by Mac User on 25/03/2013.
//
//

#import "PhysicalCheckHistoryView.h"
#import "PhysicalCheckHistoryItemCell.h"
#import "PhysicalHistorySectionInfo.h"
#import "PhysicalHistoryModel.h"
#import "PhysicalHistoryManager.h"
#import "MBProgressHUD.h"
#import <QuartzCore/QuartzCore.h>
#import "MyNavigationBarV2.h"
#import "DeviceManager.h"
#import "TopbarViewV2.h"
#import "SyncManagerV2.h"
#import "LanguageManagerV2.h"
#import "CommonVariable.h"

#define heightTbvNoResult   240.0f
#define tagNoResult         12
//#define sizeNoResult        22.0f
//#define font                @"Arial-BoldMT"

@interface PhysicalCheckHistoryView (PrivateMethods)
-(void) cartButtonPressed;

#define tagTopbarView 1234
#define tagAlertSubmit 1235
#define tagAlertNo 11

-(void) loadTopbarView;
-(void) showTopbarView;
-(void) hideTopbarView;
-(void) setHiddenTopbarView;
-(void) adjustShowForViews;
-(void) adjustRemoveForViews;
-(void) addButtonHandleShowHideTopbar;
-(void) topbarTapped:(UIButton *) sender;
-(TopbarViewV2 *) getTopBarView;

@end

@implementation PhysicalCheckHistoryView
@synthesize tbvPhysicalCH;
@synthesize sectionDatas;
@synthesize openSectionIndex;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //Resize content views of current VC to fit with Navigation bar and tabbar
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    // Do any additional setup after loading the view from its nib.
//    [self.tbvPhysicalCH setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [tbvPhysicalCH setBackgroundView:nil];
    [tbvPhysicalCH setBackgroundColor:[UIColor clearColor]];
    [tbvPhysicalCH setOpaque:YES];
    [self performSelector:@selector(loadTopbarView)];
     self.openSectionIndex = NSNotFound;
//     [textView addObserver:self forKeyPath:@"contentSize" options:(NSKeyValueObservingOptionNew) context:NULL];
    
//    [lblAlert setHidden:YES];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Load Linen
-(void)loadingDatas {
    
    SyncManagerV2 *syncManager = [[SyncManagerV2 alloc] init];
    
    NSString *floorLastModified = [[UnassignManagerV2 sharedUnassignManager] getFloorLastModifiedDate];
    if (floorLastModified.length <= 0)
    {
        int hotelID = [[[UserManagerV2 sharedUserManager] currentUser] userHotelsId];
        [syncManager getFloorListFromWSWithFromWS:hotelID andLastModified:floorLastModified AndPercentView:nil];
    }
    
    PhysicalHistoryManager *manager = [[PhysicalHistoryManager alloc] init];
    NSInteger userID = [[UserManagerV2 sharedUserManager] currentUser].userId;
    sectionDatas = [[NSMutableArray alloc] init];
    NSMutableArray *listFloor = [[NSMutableArray alloc] init];
    if(isDemoMode) {
        NSMutableArray *listPhysicalHistory = [[NSMutableArray alloc] init];
        listPhysicalHistory = [manager loadAllPhysicalHistoryData];
        NSMutableArray *listTempFloor = [[UnassignManagerV2 sharedUnassignManager] loadFloors];
        for (PhysicalHistoryModel *physicalModel in listPhysicalHistory) {
            for (FloorModelV2 *floorModel in listTempFloor) {
                if(floorModel.floor_id == physicalModel.ph_floor_id && physicalModel.ph_user_id == userID)
                {
                    [listFloor addObject:floorModel];
                }
            }
        }
    }
    else{
        
        listFloor = [manager loadAllFloorContainsPhysicalHistoryByUserId:userID];
        
    }
    
    for (FloorModelV2 *floorModel in listFloor) {
        
        PhysicalHistorySectionInfo *sectionInfo = [[PhysicalHistorySectionInfo alloc] init];
        sectionInfo.floorModel = floorModel;
        sectionInfo.open = false;
        
        NSMutableArray *listHistory = [manager loadAllPhysicalHistoryByFloorId:floorModel.floor_id AndUserId:userID];
        for (PhysicalHistoryModel *model in listHistory) {
            [sectionInfo insertObjectToNextIndex:model];
        }
        
        [sectionDatas addObject:sectionInfo];
        
    }
    if (sectionDatas.count == 0) {
        UILabel *lblAlert  =  [[UILabel alloc]init];
        lblAlert.frame     =  CGRectMake(15, 150, 290, 80);
        [lblAlert setText:[[LanguageManagerV2 sharedLanguageManager] getNoResultsFound]];
        lblAlert.textAlignment = NSTextAlignmentCenter;
        lblAlert.layer.cornerRadius = 10;
        [lblAlert setFont:[UIFont fontWithName:@"Arial-BoldMT" size:22]];
        lblAlert.numberOfLines = 2;
        [lblAlert setTextColor:[UIColor grayColor]];
        lblAlert.font = [UIFont boldSystemFontOfSize:20.0f];
        [tbvPhysicalCH addSubview:lblAlert];
    }

    
}
#pragma mark - View lifecycle
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
//    [self loadingDatas];
    MBProgressHUD	*HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getRaLoadingData]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES];
    [self performSelector:@selector(loadDataAfterDelayWithHUD:) withObject:HUD afterDelay:0.1];
    [self setCaptionsView];
    
    //show wifi view
    if(!isDemoMode){
        [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationShowWifiView] object:nil];
    }
     self.openSectionIndex = NSNotFound;
    [tbvPhysicalCH reloadData];
}

-(void) loadDataAfterDelayWithHUD:(MBProgressHUD *) HUD {
    
    PhysicalHistoryManager *manager = [[PhysicalHistoryManager alloc] init];
    [manager deleteDatabaseAfterSomeDays];
    [self loadingDatas];
    if (sectionDatas.count == 0){
        [self addLabelNoData];
    }
//    [self handleBtnAddToCartPressed]
    //hide saving data.
    [self performSelector:@selector(hiddenHUDAfterLoad:) withObject:HUD afterDelay:0.5];
}

#pragma didden after save.
-(void) hiddenHUDAfterLoad:(MBProgressHUD *)HUD{
  
    [HUD hide:YES];
    [HUD removeFromSuperview];
    [tbvPhysicalCH reloadData];
}


#pragma mark - UITableView Datasource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return sectionDatas.count;

}
-(void)addLabelNoData{

 
//    UILabel *titleNoResult = [[UILabel alloc] initWithFrame:CGRectMake(0, 60, 320, heightTbvNoResult)];
//    UILabel *titleNoResult = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 300)];
//    [titleNoResult setTextColor:[UIColor grayColor]];
////    [titleNoResult setBackgroundColor:[UIColor clearColor]];
//    [titleNoResult setTextAlignment:NSTextAlignmentCenter];
//    [titleNoResult setFont:[UIFont fontWithName:font size:sizeNoResult]];
//    [titleNoResult setText:[[LanguageManagerV2 sharedLanguageManager] getNoResultsFound]];
//    [self.tbvPhysicalCH addSubview:titleNoResult];
    
//    textView = [[UITextView alloc] initWithFrame:CGRectMake(10, 40, 300, 240)];
//    
//    //The rounded corner part, where you specify your view's corner radius:
//    [textView setTextAlignment:NSTextAlignmentCenter];
//    [textView setFont:[UIFont fontWithName:font size:sizeNoResult]];
//    [textView setTextColor:[UIColor grayColor]];
//    [textView setText:[[LanguageManagerV2 sharedLanguageManager] getNoResultsFound]];
//    textView.layer.cornerRadius = 8;
//    textView.clipsToBounds = YES;
//    [self.tbvPhysicalCH addSubview:textView];
    
}
//-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
//    textView = object;
//    //Center vertical alignment
//    //CGFloat topCorrect = ([tv bounds].size.height - [tv contentSize].height * [tv zoomScale])/2.0;
//    //topCorrect = ( topCorrect < 0.0 ? 0.0 : topCorrect );
//    //tv.contentOffset = (CGPoint){.x = 0, .y = -topCorrect};
//    
//    //Bottom vertical alignment
//    CGFloat topCorrect = ([textView bounds].size.height - [textView contentSize].height);
//    topCorrect = (topCorrect <0.0 ? 0.0 : topCorrect);
//    textView.contentOffset = (CGPoint){.x = 0, .y = -topCorrect};
//}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    PhysicalHistorySectionInfo *sectionInfo = [sectionDatas objectAtIndex:section];
    NSInteger numberRowInSection = sectionInfo.rowHeights.count;
    return sectionInfo.open ? numberRowInSection : 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifycell = @"physicalCheckHistoryItemCell";
    PhysicalCheckHistoryItemCell *cell = nil;
    
    cell = (PhysicalCheckHistoryItemCell *)[tableView dequeueReusableCellWithIdentifier:identifycell];
    if (cell == nil) {
        NSArray *array = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([PhysicalCheckHistoryItemCell class]) owner:self options:nil];

        cell = [array objectAtIndex:0];
    }
    
//    cell.delegate = self;
    PhysicalHistorySectionInfo *sectionInfo = [sectionDatas objectAtIndex:indexPath.section];
    PhysicalHistoryModel *model = [sectionInfo.rowHeights objectAtIndex:indexPath.row];
    [cell.lblRoom setText:model.ph_room_id];
    
//    [cell.lblStatus setText:[NSString stringWithFormat:@"%d",model.ph_status]];
    NSInteger status = model.ph_status;
    
    RoomStatusModelV2 *roomStatus = [[RoomStatusModelV2 alloc] init];
    roomStatus.rstat_Id = (int)status;
    roomStatus = [[RoomManagerV2 sharedRoomManager] loadRoomStatusData:roomStatus];
    
    if([[LanguageManagerV2 sharedLanguageManager] currentLanguageIsEqualTo:@"%@", ENGLISH_LANGUAGE]){
        [cell.lblStatus setText:roomStatus.rstat_Name];
    } else {
        [cell.lblStatus setText:roomStatus.rstat_Lang];
    }
    
    /*
    if(model.ph_dnd)
       [cell.lblStatus setText:@"Occupied"];
    else if ((status == ENUM_ROOM_STATUS_VC) || (status == ENUM_ROOM_STATUS_VD))
        [cell.lblStatus setText:@"Vacant"];
    else if ((status == ENUM_ROOM_STATUS_OC)|| (status == ENUM_ROOM_STATUS_OD))
         [ cell.lblStatus setText:@"Occupied"];
    else if (status == ENUM_ROOM_STATUS_OOO)
        [cell.lblStatus setText:@"Vacant"];*/
    
    [cell.lblUnderLine setBackgroundColor:[UIColor blackColor]];
    
    if (model.ph_dnd) {
        [cell.lblDND setText:[NSString stringWithFormat:@"DND"]];
    }
    else
        [cell.lblDND setText:@"-"];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;//70//the number of value must equal the value of cell, if not the index will overwite
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 60;//60
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {

    PhysicalHistorySectionInfo *sectionInfo = [sectionDatas objectAtIndex:section];
    if (sectionInfo.headerView == nil) {
        PhysicalCheckHistorySection *headerView = [[PhysicalCheckHistorySection alloc] initWithSection:section PhysicalHistoryModel:sectionInfo.floorModel andOpenStatus:NO];
        sectionInfo.headerView = headerView;
        headerView.delegate = self;
    }
    return sectionInfo.headerView;
}

#pragma mark - set Captions View
-(void)setCaptionsView {
//    [btnAddToCart setTitle:[[LanguageManagerV2 sharedLanguageManager] getAddToCart] forState:UIControlStateNormal];
//    [self setTitle:[[LanguageManagerV2 sharedLanguageManager] getLinen]];
//    
    msgbtnNo = [[LanguageManagerV2 sharedLanguageManager] getNo];
    msgbtnYes = [[LanguageManagerV2 sharedLanguageManager] getYes];
    msgSaveCount = [[LanguageManagerV2 sharedLanguageManager] getMsgAlertSaveCounts];
//    msgDiscardCount = [[LanguageManagerV2 sharedLanguageManager] getMsgAlertDiscardCounts];
    
    //refresh topview
    TopbarViewV2 *topview = [self getTopBarView];
    [topview refresh:[[SuperRoomModelV2 alloc] init]];
}

//update code
#pragma mark - Section Header Delegate
-(void)sectionHeaderView:(PhysicalCheckHistorySection *)sectionheaderView sectionOpened:(NSInteger)section {

    PhysicalHistorySectionInfo *sectionInfo = [sectionDatas objectAtIndex:section];
    sectionInfo.open = YES;
    
    NSInteger countOfRowsToInsert = [sectionInfo.rowHeights count];
    NSMutableArray *indexPathsToInsert = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i < countOfRowsToInsert; i++) {
        [indexPathsToInsert addObject:[NSIndexPath indexPathForRow:i inSection:section]];
    }
    
    UITableViewRowAnimation insertAnimation;
    
    
    UITableViewRowAnimation deleteAnimation;
    
    
    NSMutableArray *indexPathsToDelete = [[NSMutableArray alloc] init];
    
    NSInteger previousOpenSectionIndex = self.openSectionIndex;
    
    if (openSectionIndex == -1) {
        
    }
    else {
        if (previousOpenSectionIndex != NSNotFound) {
            
            PhysicalHistorySectionInfo *previousOpenSection = [self.sectionDatas objectAtIndex:previousOpenSectionIndex];
            previousOpenSection.open = NO;
            [previousOpenSection.headerView toggleOpenWithUserAction:NO];
            
            NSInteger countOfRowsToDelete =[previousOpenSection.rowHeights count];
            for (NSInteger i = 0; i < countOfRowsToDelete; i++) {
                [indexPathsToDelete addObject:[NSIndexPath indexPathForRow:i inSection:previousOpenSectionIndex]];
            }
            
        }
    }
    
    if (previousOpenSectionIndex == NSNotFound || section < previousOpenSectionIndex) {
        insertAnimation = UITableViewRowAnimationTop;
        deleteAnimation = UITableViewRowAnimationBottom;
    }
    else {
        insertAnimation = UITableViewRowAnimationBottom;
        deleteAnimation = UITableViewRowAnimationTop;
    }
    
    
    [self.tbvPhysicalCH beginUpdates];
    
    [self.tbvPhysicalCH insertRowsAtIndexPaths:indexPathsToInsert withRowAnimation:insertAnimation];
    [self.tbvPhysicalCH deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:deleteAnimation];
    [self.tbvPhysicalCH endUpdates];
    
    [self.tbvPhysicalCH scrollToRowAtIndexPath:[indexPathsToInsert objectAtIndex:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    self.openSectionIndex = section;

}

-(void)sectionHeaderView:(PhysicalCheckHistorySection *)sectionheaderView sectionClosed:(NSInteger)section {

    PhysicalHistorySectionInfo *sectionInfo = [sectionDatas objectAtIndex:section];
    //
    sectionInfo.open = NO;
    NSInteger countOfRowsToDelete = [self.tbvPhysicalCH numberOfRowsInSection:section];
    
    if (countOfRowsToDelete > 0) {
        NSMutableArray *indexPathsToDelete = [[NSMutableArray alloc] init];
        for (NSInteger i = 0; i < countOfRowsToDelete; i++) {
            [indexPathsToDelete addObject:[NSIndexPath indexPathForRow:i inSection:section]];
        }
        
        [tbvPhysicalCH beginUpdates];
        [self.tbvPhysicalCH deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:UITableViewRowAnimationAutomatic];
        [tbvPhysicalCH endUpdates];
    }
    
    self.openSectionIndex = NSNotFound;
}

//
#pragma mark - Handle Topbar Methods
-(void)loadTopbarView {
    TopbarViewV2 *topbar = [[TopbarViewV2 alloc] initViewWithSuperModel:[[CommonVariable sharedCommonVariable] superRoomModel]];
    [topbar setTag:tagTopbarView];
    [self.view addSubview:topbar];
    [topbar setHidden:YES];
    
    [topbar setAlpha:0.0];
    [self addButtonHandleShowHideTopbar];
}

-(void)showTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationWillStartSelector:@selector(setHiddenTopbarView)];
    
    f = topbar.frame;
    f.origin.y = 0;
    [topbar setFrame:f];
    [topbar setAlpha:1.0];
    
    [self adjustShowForViews];
    
    [UIView commitAnimations];
}

-(void)hideTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDidStopSelector:@selector(setHiddenTopbarView)];
    [UIView setAnimationDelegate:self];
    
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    [topbar setAlpha:0.0];
    
    [self adjustRemoveForViews];
    
    [UIView commitAnimations];
}

-(void)setHiddenTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    [topbar setHidden:!topbar.hidden];
}

-(TopbarViewV2 *)getTopBarView {
    return (TopbarViewV2 *)[self.view viewWithTag:tagTopbarView];
}

-(void)addButtonHandleShowHideTopbar {
    UIView *vBarButton = [[UIView alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
    UIButton *titleBarButtonFirst = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 200, 30)];
    
    titleBarButtonFirst.titleLabel.backgroundColor = [UIColor clearColor];
    [titleBarButtonFirst.titleLabel setFont: FONT_BUTTON_TOPBAR];
    titleBarButtonFirst.titleLabel.textAlignment = NSTextAlignmentCenter;
    titleBarButtonFirst.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
    
    [titleBarButtonFirst.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
    [titleBarButtonFirst.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
    [titleBarButtonFirst addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    [titleBarButtonFirst setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_HISTORY] forState:UIControlStateNormal];
    
    [vBarButton addSubview:titleBarButtonFirst];
    
    
    UIButton *titleBarButtonSecond = [[UIButton alloc] initWithFrame:CGRectMake(0, 28, 200, 22)];//22
    [titleBarButtonSecond setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [titleBarButtonSecond.titleLabel setFont:FONT_BUTTON_TOPBAR_SECOND];
    [titleBarButtonSecond setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_Physical_Check_Title] forState:UIControlStateNormal];
    [titleBarButtonSecond addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    [vBarButton addSubview:titleBarButtonSecond];
    
    self.navigationItem.titleView = vBarButton;
    
    //Move title to center vertical
    CGRect titleViewBounds = self.navigationItem.titleView.bounds;
    int moveLeftTitle = 0;
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        moveLeftTitle = 10;
    }
    self.navigationItem.titleView.bounds = CGRectMake(titleViewBounds.origin.x + moveLeftTitle, Y_TOPBAR_TITLE, titleViewBounds.size.width, titleViewBounds.size.height);
    
    //back button navigation
    self.navigationItem.leftBarButtonItem = [MyNavigationBarV2 createBackButtonWithTarget:self selector:@selector(btnBackPressed) forControlEvents:UIControlEventTouchUpInside];
}

-(void)btnBackPressed
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)topbarTapped:(UIButton *)sender {
    TopbarViewV2 *topbar = [self getTopBarView];
    [sender setEnabled:NO];
    if (topbar.isHidden) {
        [self showTopbarView];
    } else {
        [self hideTopbarView];
    }
    [self performSelector:@selector(topbarButtonEnable:) withObject:sender afterDelay:0.5];
}

-(void) topbarButtonEnable:(UIButton *) topbarBtn {
    [topbarBtn setEnabled:YES];
}

-(void)adjustShowForViews {
    //adjust all views when show
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    CGRect fRoomView = tbvPhysicalCH.frame;
    [tbvPhysicalCH setFrame:CGRectMake(0, f.size.height, 320, fRoomView.size.height)];
    
//    CGRect ftbv = tbvLinen.frame;
//    [tbvLinen setFrame:CGRectMake(0, f.size.height + fRoomView.size.height, 320, ftbv.size.height - f.size.height)];
    
}

-(void)adjustRemoveForViews {
    //adjust all views when hide
//    TopbarViewV2 *topbar = [self getTopBarView];
//    CGRect f = topbar.frame;
    
    CGRect fRoomView = tbvPhysicalCH.frame;
    [tbvPhysicalCH setFrame:CGRectMake(0, 0, 320, fRoomView.size.height)];
    
//    CGRect ftbv = tbvLinen.frame;
//    [tbvLinen setFrame:CGRectMake(0, fRoomView.size.height, 320, ftbv.size.height + f.size.height)];
    
}


@end
