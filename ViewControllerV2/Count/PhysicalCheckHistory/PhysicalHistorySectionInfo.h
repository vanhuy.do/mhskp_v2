//
//  PhysicalHistorySectionInfo.h
//  mHouseKeeping
//
//  Created by Mac User on 26/03/2013.
//
//

#import <Foundation/Foundation.h>
#import "PhysicalCheckHistorySection.h"
#import "FloorModelV2.h"

@interface PhysicalHistorySectionInfo : NSObject
@property (nonatomic, strong) FloorModelV2 *floorModel;
@property (assign) BOOL open;
@property (strong) PhysicalCheckHistorySection* headerView;
@property (nonatomic,strong,readonly) NSMutableArray *rowHeights;

- (NSUInteger)countOfRowHeights;
- (id)objectInRowHeightsAtIndex:(NSUInteger)idx;
- (void)insertObject:(id)anObject inRowHeightsAtIndex:(NSUInteger)idx;
- (void)removeObjectFromRowHeightsAtIndex:(NSUInteger)idx;
- (void)replaceObjectInRowHeightsAtIndex:(NSUInteger)idx withObject:(id)anObject;
- (void)getRowHeights:(id __unsafe_unretained [])buffer range:(NSRange)inRange;
- (void)insertRowHeights:(NSArray *)rowHeightArray atIndexes:(NSIndexSet *)indexes;
- (void)removeRowHeightsAtIndexes:(NSIndexSet *)indexes;
- (void)replaceRowHeightsAtIndexes:(NSIndexSet *)indexes withRowHeights:(NSArray *)rowHeightArray;
-(void) insertObjectToNextIndex:(id)anObject;
@end
