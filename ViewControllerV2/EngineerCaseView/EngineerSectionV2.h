//
//  LFCategorySectionV2.h
//  mHouseKeeping
//
//  Created by TMS on 1/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "engineerSectionCheckListContentTypeViewV2.h"
#import "EngineeringCategoriesModeV2.h"
@interface EngineerSectionV2 : NSObject

@property (nonatomic, strong) EngineeringCategoriesModeV2 *chkContentType;
@property  BOOL open;
@property (nonatomic,strong) engineerSectionCheckListContentTypeViewV2* headerView;

@property (nonatomic,strong) NSMutableArray *rowHeights;

- (NSUInteger)countOfRowHeights;
- (id)objectInRowHeightsAtIndex:(NSUInteger)idx;
- (void)insertObject:(id)anObject inRowHeightsAtIndex:(NSUInteger)idx;
- (void)removeObjectFromRowHeightsAtIndex:(NSUInteger)idx;
- (void)replaceObjectInRowHeightsAtIndex:(NSUInteger)idx withObject:(id)anObject;
- (void)getRowHeights:(id __unsafe_unretained [])buffer range:(NSRange)inRange;
- (void)insertRowHeights:(NSArray *)rowHeightArray atIndexes:(NSIndexSet *)indexes;
- (void)removeRowHeightsAtIndexes:(NSIndexSet *)indexes;
- (void)replaceRowHeightsAtIndexes:(NSIndexSet *)indexes withRowHeights:(NSArray *)rowHeightArray;
-(void) insertObjectToNextIndex:(id)anObject;

@end
