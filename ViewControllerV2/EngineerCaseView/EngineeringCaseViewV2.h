//
//  EngineeringCaseViewV2.h
//  mHouseKeeping
//
//  Created by ThuongNM on 2/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ZXingWidgetController.h>
#import "engineerListDetailViewV2.h"
#import "PhotoScrollViewV2.h"
#import "PhotoViewV2.h"
#import "AlertAdvancedSearch.h"
#import "RemarkViewV2.h"

@interface EngineeringCaseViewV2 : UIViewController<UITableViewDataSource,UITableViewDelegate, PhotoViewV2Delegate, engineerListDetailViewV2delegate,RemarkViewV2Delegate, UIScrollViewDelegate, AlertAdvancedSearchDelegate,UIGestureRecognizerDelegate, UIAlertViewDelegate, UITextFieldDelegate, UITextViewDelegate, ZXingDelegate>{
    IBOutlet UILabel *localtion;
    IBOutlet UITextField *txtRoom;
    IBOutlet UITableView *tblEngineer;
    IBOutlet UITableViewCell *tblcell;
    IBOutlet UIImageView *imgHeader;
    
    NSMutableArray *arrayList;
    NSMutableArray *arrayTitle;
    __weak IBOutlet UIButton *btnPostingHistory;
    NSMutableArray *arrayIndex;
    NSInteger indexTemp;
    NSMutableArray *photoList;
    NSMutableArray *photoDeleteList;
    NSInteger indexDelete;
    PhotoScrollViewV2 *photoViewScroll;
    NSString *remarkText;
    BOOL statusChooseSave;
    BOOL statusCheckSaveData;
    
    int countRow;
    
    BOOL isFirstLoad;
    BOOL isUpdatedText;
    BOOL isUpdatedPhoto;
    BOOL isBack;
    
    UIButton *btnQR;
    UIButton *btnSubmit;
    BOOL isFromMainPosting;
    UILabel *lblSubmit;
    IBOutlet UIScrollView *scrollView;
    
    AccessRight *QRCodeScanner;
    AccessRight *postingEngineering;
    AccessRight *actionEngineering;
    
    RemarkViewV2 *remarkView;
}

@property BOOL isFirstLoad;

@property NSInteger indexDelete;
@property (nonatomic, strong) PhotoScrollViewV2 *photoViewScroll;
@property (nonatomic, strong) NSString *remarkText;
@property (nonatomic, strong) UILabel *localtion;
@property (nonatomic, strong) UITextField *txtRoom;
@property (nonatomic, strong) UITableView *tblEngineer;
@property (nonatomic, strong) UITableViewCell *tblcell;
@property (nonatomic, strong) NSMutableArray *arrayList;
@property (nonatomic, strong) NSMutableArray *arrayTitle;
@property (nonatomic, strong) NSMutableArray *arrayIndex;
@property (nonatomic, strong) NSMutableArray *photoList;
@property (nonatomic, strong) NSMutableArray *photoDeleteList;
@property (strong, nonatomic) NSString *msgbtnYes;
@property (strong, nonatomic) NSString *msgbtnNo;
@property (strong, nonatomic) NSString *remark;
@property (strong, nonatomic) NSString *msgSaveCount;
@property (strong, nonatomic) NSString *msgDiscardCount;
@property (nonatomic) int countRow;
@property (strong, nonatomic) IBOutlet UIButton *btnSubmit;
@property (strong, nonatomic) IBOutlet UIButton *btnQR;
@property (nonatomic) BOOL isFromMainPosting;
@property (strong, nonatomic) IBOutlet UILabel *lblSubmit;
@property (strong, nonatomic) IBOutlet UIView *viewRoom;

- (IBAction)btnPostingHistory_Click:(UIButton *)sender;
-(NSString *) getBackString;
-(void)setCaptionView;
-(IBAction)btnSubmit_Clicked:(id)sender;
-(IBAction) btnQRCodeScannerTouched:(id) sender;
@end
