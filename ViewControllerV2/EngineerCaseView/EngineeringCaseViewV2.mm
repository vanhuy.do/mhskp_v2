//
//  EngineeringCaseViewV2.m
//  mHouseKeeping
//
//  Created by ThuongNM on 2/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "EngineeringCaseViewV2.h"
#import "customCellEngineer.h"
#import "engineerListDetailViewV2.h"
#import "RemarkViewV2.h"
#import "PhotoViewV2.h"
#import <QuartzCore/QuartzCore.h>
#import "AlertAdvancedSearch.h"
#import "zoomPhotoViewV2.h"
#import "EngineerCaseViewManagerV2.h"
#import "RoomManagerV2.h"
#import "EngineeringCaseDetailModelV2.h"
#import "EngineeringItemModelV2.h"
#import "CustomAlertViewV2.h"
#import "NetworkCheck.h"
#import "EngineerImageModelV2.h"
#import "RoomAssignmentInfoViewController.h"
#import "EngineeringHistoryManager.h"
#import "EngineeringHistoryModel.h"
#import "MyNavigationBarV2.h"
#import "QRCodeReader.h"
#import "LogFileManager.h"
#import "DeviceManager.h"
#import "LanguageManagerV2.h"
#import "TasksManagerV2.h"
#import "HomeViewV2.h"
#import "DTAlertView.h"
#import "STEncryptorDES.h"
#import "UIImage+Tint.h"
#import "NSString+Common.h"
#import "HistoryPosting.h"

#define background    @"bg.png"
#define HEIGHT 60
#define WIDTH  85
#define WIDTHIMAGE 85

#define DELETEPHOTOMSG @""
#define DELETEIMAGE @"icon_garbage.png"
#define PHOTOIMAGE @"photo_icon.png"
#define TESTIMAGE  @"Shampoo_pic.png"
#define BUTTON_YES @"yes_bt.png"
#define BUTTON_NO @"no_bt.png"
#define REMARKTITLE @"Remarks"
#define CATEGORYTITLE @"Category"
#define ITEMTITLE   @"Item"
#define COLORTITLE @"Color"

#define tagAlertBack 10
#define tagAlertNo 11
#define tagAlertSaveThenBack 12
#define tagAlertDelete 13
#define tagAlertSubmit 14
#define tagTopbarView 1234
#define tagRemarkCell 1235
#define maxRowOfTableItem 5
#define scrollViewWidth 320
#define scrollViewHeight 500

@interface EngineeringCaseViewV2 (PrivateMethods) <DTAlertViewDelegate>
-(void) saveEngineering;
-(void) loadDataBase;
-(void) loadTopbarView;
-(void) showTopbarView;
-(void) hideTopbarView;
-(void) setHiddenTopbarView;
-(void) adjustShowForViews;
-(void) adjustRemoveForViews;
-(void) addButtonHandleShowHideTopbar;
-(void) topbarTapped:(UIButton *) sender;
-(TopbarViewV2 *) getTopBarView;
-(void) btnArrowDetailPressed:(UIButton *) sender;
-(void) initPhotoScrollView;

@end

@implementation EngineeringCaseViewV2

@synthesize localtion,
txtRoom,
tblEngineer,
arrayList,
tblcell,
arrayTitle,
photoList,
remarkText,
arrayIndex,
btnQR,
isFromMainPosting,
remark,
viewRoom;

@synthesize indexDelete, photoViewScroll, msgbtnNo, msgbtnYes, msgSaveCount,
msgDiscardCount, isFirstLoad, photoDeleteList, countRow, btnSubmit, lblSubmit;

BOOL isPostPhotoSuccess = FALSE;
BOOL isPostECSuccess = FALSE;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    
    return self;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}
- (void)enableQRCode:(BOOL)isEnable{
    [btnQR setAlpha:isEnable ? 1.0f : 0.7f];
    [btnQR setEnabled:isEnable];
}
//CRF1619
- (void)loadAccessQRCode{
    if ([UserManagerV2 sharedUserManager].currentUserAccessRight.isPostingFunctionEngineeringQRCode == 1) {
        self.txtRoom.enabled = NO;
        [self enableQRCode:YES];
    }else if ([UserManagerV2 sharedUserManager].currentUserAccessRight.isPostingFunctionEngineeringQRCode == 0) {
        self.txtRoom.enabled = YES;
        [self enableQRCode:NO];
    }
}
- (void)viewDidLoad {
    
    [super viewDidLoad];
    //Resize content views of current VC to fit with Navigation bar and tabbar
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    //[txtRemark setHidden:YES];
    [self loadAccessRights];
    
    if(!ENABLE_QRCODESCANNER)
    {
        if(ENABLE_CHECK_ACCESS_RIGHT_QRCODESCANNER)
        {
            [btnQR setHidden:NO];
            [self enableQRCode:NO];
            
            if (QRCodeScanner.isActive && !isDemoMode) {
                [self enableQRCode:YES];
            }
            
        }
        else
        {
            [btnQR setHidden:YES];
        }
    }
    
    //Set Enable button submit
    [btnSubmit setEnabled:![self isOnlyViewedAccessRight]];
    
    if([LogFileManager isLogConsole])
    {
        NSInteger userId = [[UserManagerV2 sharedUserManager] currentUser].userId;
        NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSLog(@"<%@><Engineering><UserId:%d><viewDidLoad>", time, (int)userId);
    }
    
    scrollView.delegate = self;
    [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, scrollView.frame.size.height)];
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        //Clear redundant view for header, footter
        CGRect frameHeaderFooter = tblEngineer.tableHeaderView.frame;
        frameHeaderFooter.size.height = 0.1f;
        UIView *headerView = [[UIView alloc] initWithFrame:frameHeaderFooter];
        [tblEngineer setTableHeaderView:headerView];
        
        frameHeaderFooter = tblEngineer.tableFooterView.frame;
        frameHeaderFooter.size.height = 0.1f;
        UIView *footerView = [[UIView alloc] initWithFrame:frameHeaderFooter];
        [tblEngineer setTableFooterView:footerView];
        
        [self loadFlatResource];
    }
    
    if(!isFirstLoad) {
        remarkText = @"";
        isUpdatedText = FALSE;
        isUpdatedPhoto = FALSE;
        statusChooseSave = YES;
        
        // Do any additional setup after loading the view from its nib.
        [self performSelector:@selector(loadDataBase)
                   withObject:nil afterDelay:0.2];
        
        isFirstLoad = YES;
    }
    isBack = NO;
    countRow = 1;
    
    //txtRemark.text = @"Remark";
    //txtRemark.textColor = [UIColor lightGrayColor];
    
    
    //    [localtion setText: [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_EC_LOCATIONS]];
    //    [localtion setText: @"Room No."];
    //    [localtion setTextColor:[UIColor colorWithRed:6/255.0 green:62/255.0 blue:127/255.0 alpha:1]];
    [localtion setTextColor:[UIColor whiteColor]];
    
    [tblEngineer setBackgroundColor:[UIColor clearColor]];
    
    //back button navigation
    self.navigationItem.leftBarButtonItem = [MyNavigationBarV2 createBackButtonWithTarget:self selector:@selector(backBarPressed) forControlEvents:UIControlEventTouchUpInside];
    
    //[self performSelector:@selector(loadTopbarView)];
    
    //Resize frame for ScrollView and more height for 4.0 inch screen
    int deviceKind = [DeviceManager getDeviceScreenKind];
    int moreHeight = 0;
    if(deviceKind == DeviceScreenKindRetina4_0){
        moreHeight = 81;
    }
    CGRect fViewRoom = viewRoom.frame;
    [scrollView setFrame:CGRectMake(0, fViewRoom.size.height + 10, scrollViewWidth, scrollViewHeight + moreHeight)];
    
    /*
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        CGRect frameRemark = txtRemark.frame;
        frameRemark.origin.x = 0;
        frameRemark.size.width = self.view.bounds.size.width;
        frameRemark.origin.y += 15;
        [txtRemark setFrame:frameRemark];
    } else {
        txtRemark.layer.cornerRadius = 10.0f;
        txtRemark.layer.borderWidth = 1.0f;
        txtRemark.layer.borderColor = [[UIColor grayColor] CGColor];
    }*/
    
    if(isFromMainPosting)
    {
        [txtRoom setText:@""];
        [txtRoom setEnabled:YES];
        [txtRoom setTextColor:[UIColor blackColor]];
        [self loadAccessQRCode];
        btnPostingHistory.hidden = ![UserManagerV2 sharedUserManager].currentUserAccessRight.isShowActionHistoryPosting;
    }
    else
    {
        //        [txtRoom setText:[NSString stringWithFormat:@"%@ %d", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOM_TITLE], [RoomManagerV2 getCurrentRoomNo]]];
        //        [txtRoom setText:[NSString stringWithFormat:@"%d", [RoomManagerV2 getCurrentRoomNo]]];
        int roomAssignment = (int)[TasksManagerV2 getCurrentRoomAssignment];
        RoomAssignmentModelV2 *roomAssignmentModel = [[RoomAssignmentModelV2 alloc] init];
        roomAssignmentModel.roomAssignment_Id = roomAssignment;
        [[RoomManagerV2 sharedRoomManager]getRoomIdByRoomAssignment:roomAssignmentModel];
        [txtRoom setText:roomAssignmentModel.roomAssignment_RoomId];
        [txtRoom setTextColor:[UIColor colorWithRed:102.0f/255.0f green:102.0f/255.0f
                                               blue:102.0f/255.0f alpha:1]];
        [txtRoom setEnabled:NO];
        [txtRoom setTextColor:[UIColor grayColor]];
    }
    
    [self loadTopbarView];
    
    // Set title for label
    [localtion setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOM_NO]];
    [lblSubmit setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_SUBMIT]];
    //disable btnSubmit
    //    [btnSubmit setEnabled:NO];
    //    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(enableMethod:)name:UITextFieldTextDidChangeNotification object:txtRoom];
    [self setEnableBtnAtionHistory:NO];
}

- (void) reachabilityDidChanged: (NSNotification* )note
{
    if(![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]){
        [btnSubmit setEnabled:NO];
        [self setEnableBtnAtionHistory:NO];
    }
    else{
        [btnSubmit setEnabled:YES];
        [self setEnableBtnAtionHistory:YES];
    }
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    
    if(!isDemoMode){
        //show wifi view
        [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationShowWifiView] object:nil];
        if(![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]){
            [btnSubmit setEnabled:NO];
        }
        else{
            [btnSubmit setEnabled:YES];
        }
    }
    [[HomeViewV2 shareHomeView].slidingMenuBar showAll];
    if(isFromMainPosting){
        if(!isDemoMode){
            [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(reachabilityDidChanged:) name: kReachabilityChangedNotification object: nil];
        }
        [[HomeViewV2 shareHomeView] setSelectedButton:tagOfPostingButton];
    } else{
        [[HomeViewV2 shareHomeView] setSelectedButton:tagOfHomeButton];
    }
    
    [self setCaptionView];
    
    //Fix lost navigation bar after choose picture from PhotoViewV2
    [self.navigationController setNavigationBarHidden:NO];
    
    //    [txtRemark setFrame:CGRectMake(10, txtRemark.frame.origin.y - 50, txtRemark.frame.size.width, txtRemark.frame.size.height)];
    //    [lblSubmit setFrame:CGRectMake(122, lblSubmit.frame.origin.y - 50, lblSubmit.frame.size.width, lblSubmit.frame.size.height)];
    //    [btnSubmit setFrame:CGRectMake(10, btnSubmit.frame.origin.y - 50, btnSubmit.frame.size.width, btnSubmit.frame.size.height)];
    //    [tblEngineer reloadData];
    
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    if(!isDemoMode){
        [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
    }
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(void)dealloc {
    //    [localtion release ];
    //    [btnRoom release];
    //    [arrayList release];
    //    [tblcell release];
    //    [tblEngineer release];
    //    [arrayTitle release];
    //    [remarkText release];
    //[super dealloc];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)btnPostingHistory_Click:(UIButton *)sender {
    HistoryPosting *historyPosting = [[HistoryPosting alloc] initWithNibName:@"HistoryPosting" bundle:nil];
    //    historyPosting.isPushFrom = IS_PUSHED_FROM_PHYSICAL_CHECK;
    historyPosting.roomID = txtRoom.text;
    historyPosting.isFromPostingFunction = isFromMainPosting;
    historyPosting.selectedModule = ModuleHistory_Engineering;
    [self.navigationController pushViewController:historyPosting animated:YES];
}

-(NSString *) getBackString {
    NSDictionary *dic = [[LanguageManagerV2 sharedLanguageManager] getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_BACK]];
    return s;
}

#pragma mark - UITableView Delegate & Datasource

-(int)sectionIndexRemark
{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.01f;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section == [self sectionIndexRemark]){
        return 10.0f;
    }
    
    return 0.0f;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    //    return [arrayList count];
    if(section == [self sectionIndexRemark])
    {
        return 1;
    }
    return countRow;
}

- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    //    if ( indexPath.row == 4) {
    //        return 60;
    //    }
    if(indexPath.section == [self sectionIndexRemark]){
        return 55;
    }
    return 50 ;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.section == [self sectionIndexRemark]) {
        static NSString *cellIdentifierRemark = @"RemarkCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifierRemark];
        UILabel *lblRemarkContent;
        
        if(cell == nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifierRemark];
                
                [cell.textLabel setTextColor:[UIColor colorWithRed:6/255.0 green:62/255.0 blue:127/255.0 alpha:1]];
                [cell.textLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:15]];
                [cell.textLabel setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_EC_REMARK]];
                
                lblRemarkContent = [[UILabel alloc] initWithFrame:CGRectMake(90, 0, tableView.bounds.size.width - 100, 60)];
                [lblRemarkContent setTag:tagRemarkCell];
                [lblRemarkContent setFont:[UIFont fontWithName:@"Helvetica-Bold" size:15]];
                [lblRemarkContent setAdjustsFontSizeToFitWidth:NO];
                [lblRemarkContent setNumberOfLines:2];
                [lblRemarkContent setLineBreakMode:NSLineBreakByTruncatingTail];
                [lblRemarkContent setBackgroundColor:[UIColor clearColor]];
                [lblRemarkContent setTextColor:[UIColor grayColor]];
                
                [cell addSubview:lblRemarkContent];
        }else {
            
            lblRemarkContent = (UILabel *)[cell viewWithTag:tagRemarkCell];
        }
        
        [lblRemarkContent setText:remark];
        
        return cell;
    } else {
        
        static NSString *MyIdentifier = @"tblCellView";
        
        customCellEngineer *cell = (customCellEngineer *)[tableView dequeueReusableCellWithIdentifier:MyIdentifier];
        
        if(cell == nil) {
            [[NSBundle mainBundle] loadNibNamed:@"customCellEngineer" owner:self options:nil];
            cell = (customCellEngineer*)tblcell;
            if(indexPath.row == countRow-1 && [arrayList count] != maxRowOfTableItem && [arrayTitle count] != maxRowOfTableItem)
            {
                [cell.issue setText:@""];
                [cell.detail setText:@""];
            }
            else if([arrayList count] == maxRowOfTableItem && [arrayTitle count] == maxRowOfTableItem){
                [cell.issue setText:[arrayList objectAtIndex:indexPath.row]];
                [cell.detail setText:[arrayTitle objectAtIndex:indexPath.row]];
            }
            else
            {
                if([arrayList count] != 0)
                {
                    [cell.issue setText:[arrayList objectAtIndex:indexPath.row]];
                }
                else {
                    [cell.issue setText:@""];
                }
                if([arrayTitle count] != 0)
                {
                    [cell.detail setText:[arrayTitle objectAtIndex:indexPath.row]];
                }
                else {
                    [cell.detail setText:@""];
                }
            }
        }
        else{
            [cell.issue setText:[arrayList objectAtIndex:indexPath.row]];
            [cell.detail setText:[arrayTitle objectAtIndex:indexPath.row]];
        }
        
        [cell.next setBackgroundImage:[UIImage imageNamed:@"gray_arrow_36x36.png"]
                             forState:UIControlStateNormal];
        [cell.next addTarget:self action:@selector(btnArrowDetailPressed:) forControlEvents:UIControlEventTouchUpInside];
        [cell.next setTag:indexPath.row];
        
        [cell.remark setHidden:TRUE];
        [cell.remarkValue setHidden:TRUE];
        
        [cell.issue setHidden:FALSE];
        [cell.detail setHidden:FALSE];
        
        //[cell.issue setText:[arrayList objectAtIndex:indexPath.row]];
        //    [cell.issue setText:@""];
        [cell.issue setTextColor:[UIColor colorWithRed:6/255.0 green:62/255.0 blue:127/255.0 alpha:1]];
        
        
        //[cell.detail setText:[arrayTitle objectAtIndex:indexPath.row]];
        //    [cell.detail setTextColor:[UIColor colorWithRed:6/255.0 green:62/255.0
        //                                               blue:127/255.0 alpha:1]];
        //    [cell.detail setText:@""];
        
        [cell.detail setTag:indexPath.row];
        
        
        //    if(indexPath.row == 4) {
        //        [cell.issue setHidden:TRUE];
        //        [cell.detail setHidden:TRUE];
        //
        //        [cell.remark setHidden:FALSE];
        //        [cell.remarkValue setHidden:FALSE];
        //
        //        [cell.remark setText:[arrayList objectAtIndex:4]];
        //        [cell.remark setTextColor:[UIColor colorWithRed:6/255.0 green:62/255.0
        //                                                   blue:127/255.0 alpha:1]];
        //
        //        EngineeringCaseDetailModelV2 *modelDetail = [[EngineeringCaseDetailModelV2 alloc] init];
        //        modelDetail.ecd_room_id = [RoomManagerV2 getCurrentRoomNo];
        //        modelDetail.ecd_user_id = [[UserManagerV2 sharedUserManager] currentUser].userId;
        //        [[EngineerCaseViewManagerV2 sharedEngineerCaseViewManagerV2] loadEngineerDetailModelByRoomIdUserIdAndServiceId:modelDetail];
        //
        //        if(statusChooseSave){
        //            modelDetail.ecd_reMark = @"";
        //        }
        //
        //        if(modelDetail.ecd_id != 0) {
        //            if(isUpdatedText == FALSE)
        //                remarkText = modelDetail.ecd_reMark;
        //
        //            [cell.remarkValue setText:remarkText];
        //        } else
        //            [cell.remarkValue setText:remarkText];
        //
        //        [cell.remarkValue setTextColor:[UIColor colorWithRed:6/255.0 green:62/255.0
        //                                                        blue:127/255.0 alpha:1]];
        //        [cell.remarkValue setTag:indexPath.row];
        //    } else {
        //        [cell.remark setHidden:TRUE];
        //        [cell.remarkValue setHidden:TRUE];
        //
        //        [cell.issue setHidden:FALSE];
        //        [cell.detail setHidden:FALSE];
        //
        //        [cell.issue setText:[arrayList objectAtIndex:indexPath.row]];
        //        [cell.issue setTextColor:[UIColor colorWithRed:6/255.0 green:62/255.0
        //                                                  blue:127/255.0 alpha:1]];
        //
        //
        //        [cell.detail setText:[arrayTitle objectAtIndex:indexPath.row]];
        //        [cell.detail setTextColor:[UIColor colorWithRed:6/255.0 green:62/255.0
        //                                                   blue:127/255.0 alpha:1]];
        //
        //
        //        [cell.detail setTag:indexPath.row];
        //    }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        return cell;
    }
}

- (void)tableView:(UITableView *)aTableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        if(countRow == 1)
        {
            return;
        }
        else{
            [arrayList removeObjectAtIndex:indexPath.row];
            [arrayTitle removeObjectAtIndex:indexPath.row];
            countRow = countRow-1;
            [tblEngineer setFrame:CGRectMake(0, 0, tblEngineer.frame.size.width, 130 + (44 * countRow))];
            if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                [photoViewScroll setFrame:CGRectMake(0, tblEngineer.frame.origin.y + 88 + (44 * countRow) + 10, photoViewScroll.frame.size.width, photoViewScroll.frame.size.height)];
            }else{
                [photoViewScroll setFrame:CGRectMake(10, tblEngineer.frame.origin.y + 100 + (44 * countRow) + 10, photoViewScroll.frame.size.width, photoViewScroll.frame.size.height)];
            }
            [lblSubmit setFrame:CGRectMake(122, photoViewScroll.frame.origin.y + photoViewScroll.frame.size.height + 20, lblSubmit.frame.size.width, lblSubmit.frame.size.height)];
            [btnSubmit setFrame:CGRectMake(10, photoViewScroll.frame.origin.y + photoViewScroll.frame.size.height + 10, btnSubmit.frame.size.width, btnSubmit.frame.size.height)];
            [btnPostingHistory setFrame:CGRectMake(10, photoViewScroll.frame.origin.y + photoViewScroll.frame.size.height + 20 + CGRectGetHeight(btnSubmit.bounds), btnPostingHistory.frame.size.width, btnPostingHistory.frame.size.height)];
            [scrollView setContentSize:CGSizeMake(scrollView.contentSize.width, scrollView.contentSize.height + 70)];
            [tblEngineer reloadData];
        }
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    indexTemp = indexPath.row;
    //-------------Log------
    if([LogFileManager isLogConsole]){
        NSInteger userId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
        NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSLog(@"<%@><Engineering><UserId:%d><Click row at index:%d>", time, (int)userId, (int)indexTemp);
    }
    //-------------Log------
    
    if(indexPath.section == [self sectionIndexRemark]) {
        [self showEngineerRemarkView];
    } else {
        
        UIBarButtonItem *BackButtonRoomInfo = [[UIBarButtonItem alloc] initWithTitle: [self getBackString] style: UIBarButtonItemStyleBordered target: nil action: nil];
        [[self navigationItem] setBackBarButtonItem: BackButtonRoomInfo];
        
        
        [self.navigationItem.backBarButtonItem setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK]];
        
        
        engineerListDetailViewV2 *ecView = [[engineerListDetailViewV2 alloc] init];
        ecView.delegate = self;
        [self.navigationController pushViewController:ecView animated:YES];
        
        
        //    if(indexPath.row == 4) {
        //        RemarkViewV2 *remarkView = [[RemarkViewV2 alloc] init];
        //        remarkView.delegate = self;
        //        remarkView.textinRemark = remarkText;
        //
        //
        //        [self.navigationController pushViewController:remarkView animated:YES];
        //    } else {
        //        engineerListDetailViewV2 *ecView = [[engineerListDetailViewV2 alloc] init];
        //        ecView.delegate = self;
        //        [self.navigationController pushViewController:ecView animated:YES];
        //    }
        
        //statusChooseSave = NO;
    }
}

-(void)btnArrowDetailPressed:(UIButton *)sender {
    indexTemp = sender.tag;
    
    engineerListDetailViewV2 *ecView = [[engineerListDetailViewV2 alloc] init];
    ecView.delegate = self;
    [self.navigationController pushViewController:ecView animated:YES];
    
    //    if(sender.tag == 4) {
    //        RemarkViewV2 *remarkView = [[RemarkViewV2 alloc] init];
    //        remarkView.delegate = self;
    //        remarkView.textinRemark = remarkText;
    //        [self.navigationController pushViewController:remarkView animated:YES];
    //    } else {
    //        engineerListDetailViewV2 *ecView = [[engineerListDetailViewV2 alloc] init];
    //        ecView.delegate = self;
    //        [self.navigationController pushViewController:ecView animated:YES];
    //    }
    
    statusChooseSave = NO;
}

#pragma mark - Init Photo View
-(void) initPhotoScrollView {
    
    if([LogFileManager isLogConsole])
    {
        NSLog(@"isUpdatedPhoto %d",isUpdatedPhoto);
    }
    
    if (!photoViewScroll) {
        photoViewScroll = [[PhotoScrollViewV2 alloc] init];
    }
    
    [photoViewScroll setBackgroundColor:[UIColor grayColor]];
    photoViewScroll.delegate = self;
    if ([photoList count] > 0) {
        
        [lblSubmit setFrame:CGRectMake(122, photoViewScroll.frame.origin.y + photoViewScroll.frame.size.height + 20, lblSubmit.frame.size.width, lblSubmit.frame.size.height)];
        [btnSubmit setFrame:CGRectMake(10, photoViewScroll.frame.origin.y + photoViewScroll.frame.size.height + 10, btnSubmit.frame.size.width, btnSubmit.frame.size.height)];
        [btnPostingHistory setFrame:CGRectMake(10, photoViewScroll.frame.origin.y + photoViewScroll.frame.size.height + 20 + CGRectGetHeight(btnSubmit.bounds), btnPostingHistory.frame.size.width, btnPostingHistory.frame.size.height)];
    }
    else {
        //CGRect photoFrame = CGRectMake(10, (txtRemark.frame.origin.y + txtRemark.frame.size.height + 10), 300, 70);
        CGRect tblEngineerFrame = tblEngineer.frame;
        tblEngineerFrame.size = tblEngineer.contentSize;
        [tblEngineer setFrame:tblEngineerFrame];
        
        CGRect photoFrame = CGRectMake(10, tblEngineer.frame.origin.y + tblEngineer.frame.size.height + 10, 300, 70);
        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
            photoFrame.origin.x = 0;
            photoFrame.size.width = self.view.bounds.size.width;
        }
        photoViewScroll.frame = photoFrame;
        [lblSubmit setFrame:CGRectMake(122, photoViewScroll.frame.origin.y + photoViewScroll.frame.size.height + 20, lblSubmit.frame.size.width, lblSubmit.frame.size.height)];
        [btnSubmit setFrame:CGRectMake(10, photoViewScroll.frame.origin.y + photoViewScroll.frame.size.height + 10, btnSubmit.frame.size.width, btnSubmit.frame.size.height)];
        [btnPostingHistory setFrame:CGRectMake(10, photoViewScroll.frame.origin.y + photoViewScroll.frame.size.height + 20 + CGRectGetHeight(btnSubmit.bounds), btnPostingHistory.frame.size.width, btnPostingHistory.frame.size.height)];
    }
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        [photoViewScroll.layer setCornerRadius:0];
    } else {
        [photoViewScroll.layer setCornerRadius:10];
    }
    
    
    [scrollView addSubview:photoViewScroll];
    
    if ([photoViewScroll.subviews count] > 0) {
        for (UIView* subview in photoViewScroll.subviews) {
            [subview removeFromSuperview];
        }
    }
    
    EngineeringCaseDetailModelV2 *modelDetail = [[EngineeringCaseDetailModelV2 alloc] init];
    modelDetail.ecd_room_id = [RoomManagerV2 getCurrentRoomNo];
    modelDetail.ecd_user_id = [[UserManagerV2 sharedUserManager] currentUser].userId;
    [[EngineerCaseViewManagerV2 sharedEngineerCaseViewManagerV2] loadEngineerDetailModelByRoomIdUserIdAndServiceId:modelDetail];
    
    NSMutableArray *arrayPhoto = [NSMutableArray array];
    
    if([photoList count] <= 0) {
        
        if(isUpdatedPhoto) {
            photoList = [[EngineerCaseViewManagerV2 sharedEngineerCaseViewManagerV2] loadAlLEngineerImageByDetailID:(int)modelDetail.ecd_id];
            arrayPhoto = [NSMutableArray arrayWithArray:photoList];
        }
    }
    else {
        if(isUpdatedPhoto) {
            arrayPhoto = [NSMutableArray arrayWithArray:photoList];
        }
        
    }
    
    //add data in here
    //first display the photo Button, and add more images if any. If the number of images = 6 the photo Button is gray.
    CGFloat cxLocation = 0;
    if ([arrayPhoto count] == 0) {
        cxLocation += (photoViewScroll.frame.size.width - 80) / 2;
        UIImage *photoImage = [UIImage imageNamed:PHOTOIMAGE];
        UIView *photoView = [[UIView alloc] initWithFrame:CGRectMake(cxLocation, 5, WIDTH, HEIGHT)];
        UIButton *photoButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
        [photoButton setBackgroundImage:photoImage forState:UIControlStateNormal];
        [photoButton addTarget:self action:@selector(selectPhotoButton) forControlEvents:UIControlEventTouchDown];
        
        [photoView addSubview:photoButton];
        [photoViewScroll addSubview:photoView];
    }
    else {
        if ([arrayPhoto count] > 0 ) {
            if ([arrayPhoto count] < 6) {
                cxLocation += 10;
                UIImage *photoImage = [UIImage imageNamed:PHOTOIMAGE];
                UIView *photoView = [[UIView alloc] initWithFrame:CGRectMake(cxLocation + 10, 5, WIDTH, HEIGHT)];
                UIButton *photoButton=[[UIButton alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
                [photoButton setBackgroundImage:photoImage forState:UIControlStateNormal];
                [photoButton addTarget:self action:@selector(selectPhotoButton) forControlEvents:UIControlEventTouchDown];
                
                [photoView addSubview:photoButton];
                
                [photoViewScroll addSubview:photoView];
                
                cxLocation += WIDTH;
            }
            
            for (int i = 0; i < [arrayPhoto count]; i++) {
                cxLocation += 10;
                EngineerImageModelV2 *model = [arrayPhoto objectAtIndex:i];
                UIImage *photoImage = [UIImage imageWithData:model.enim_image];
                
                UIView *photoView = [[UIView alloc]initWithFrame:CGRectMake(cxLocation + 10, 5, WIDTH, HEIGHT)];
                UIButton *photoButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, WIDTHIMAGE, HEIGHT)];
                photoButton.tag = i;
                [photoButton setBackgroundImage:photoImage forState:UIControlStateNormal];
                UILongPressGestureRecognizer *longpressGesture =[[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(responseLongPressButton:)];
                longpressGesture.delegate = self;
                longpressGesture.minimumPressDuration = 1;
                [photoButton addGestureRecognizer:longpressGesture];
                [photoButton addTarget:self action:@selector(tapImageToZoom:) forControlEvents:UIControlEventTouchUpInside];
                [photoView addSubview:photoButton];
                
                //[photoView addSubview:deleteButton];
                [photoViewScroll addSubview:photoView];
                
                cxLocation += WIDTH;
            }
        }
    }
    
    [photoViewScroll setContentSize:CGSizeMake(cxLocation, HEIGHT)];
}

#pragma mark - Photoviewv2 delegate
-(void)forwardtoPhotoView {
    PhotoViewV2 *viewPhoto = [[PhotoViewV2 alloc] init];
    viewPhoto.delegate = self;
    [viewPhoto setHidesBottomBarWhenPushed:YES];
    [self.navigationController pushViewController:viewPhoto animated:YES];
}

-(void)processPhoto:(UIImage *)photo {
    if (!photoList) {
        self.photoList = [[NSMutableArray alloc] init];
    }
    
    EngineerImageModelV2 *image = [[EngineerImageModelV2 alloc] init];
    image.enim_image = UIImageJPEGRepresentation(photo, 1.0);
    
    [photoList addObject:image];
    
    isUpdatedPhoto = YES;
    statusChooseSave = NO;
    
    [self performSelector:@selector(initPhotoScrollView)];
    
    
}

-(void)selectPhotoButton {
    //    if ([photoList count] == 6) {
    //        return;
    //    }
    //    else {
    PhotoViewV2 *viewPhoto = [[PhotoViewV2 alloc] init];
    viewPhoto.delegate = self;
    
    [viewPhoto setHidesBottomBarWhenPushed:YES];
    //[viewPhoto setTitle:[[LanguageManagerV2 sharedLanguageManager] getENGINEERING]];
    //UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK] style:UIBarButtonItemStyleBordered target:nil action:nil];
    //self.navigationItem.backBarButtonItem = backButton;
    
    [self.navigationController pushViewController:viewPhoto animated:NO];
    //    }
    
}

-(void)tapImageToZoom:(id)sender {
    UIButton *zoomButton = (UIButton *)sender;
    zoomPhotoViewV2 *zoomedView = [[zoomPhotoViewV2 alloc] initWithNibName:@"zoomPhotoViewV2" bundle:nil];
    
    UINavigationController *navZoomPhoto = [[UINavigationController alloc] initWithRootViewController:zoomedView];
    [self presentViewController:navZoomPhoto animated:NO completion:nil];
    
    if ([photoList count] > zoomButton.tag) {
        EngineerImageModelV2 *image = [photoList objectAtIndex:zoomButton.tag];
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [zoomedView.zoomPhoto setImage:[UIImage imageWithData:image.enim_image]];
//        });
        zoomedView.imageData = image.enim_image;
        //[zoomedView.zoomPhoto setImage:[UIImage imageWithData:image.enim_image]];
    }
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_DONE] style:UIBarButtonItemStyleDone target:self action:@selector(tapDoneButtonInZoomView:)];
    zoomedView.navigationItem.leftBarButtonItem = doneButton;
    
    navZoomPhoto.navigationBar.tintColor = [UIColor blackColor];
    navZoomPhoto.navigationBar.alpha = 0.7f;
    navZoomPhoto.navigationBar.translucent = YES;
}

-(void)tapDoneButtonInZoomView:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    [[HomeViewV2 shareHomeView].slidingMenuBar showAll];
}

#pragma mark - engineerListDetailViewV2delegate  Methods
//-(void)didSeclectRow:(NSString *)temp index:(NSInteger)_index category_id:(NSInteger)_category_id item_id:(NSInteger)_item_id {
-(void)didSeclectRow:(NSString *)categoryName itemName:(NSString*)item index:(NSInteger)_index category_id:(NSInteger)_category_id item_id:(NSInteger)_item_id
{
    if(indexTemp < [arrayList count])
    {
        [arrayList removeObjectAtIndex:indexTemp];
        [arrayList insertObject:categoryName atIndex:indexTemp];
    }
    else{
        [arrayList addObject:categoryName];
    }
    
    if(indexTemp < [arrayTitle count])
    {
        [arrayTitle removeObjectAtIndex:indexTemp];
        [arrayTitle insertObject:item atIndex:indexTemp];
    }
    else{
        [arrayTitle addObject:item];
    }

//    [arrayIndex removeObjectAtIndex:indexTemp];
//    [arrayIndex insertObject:[NSString stringWithFormat:@"%d", indexTemp] atIndex:indexTemp];
    
    if(_category_id){
        isUpdatedText = TRUE;
        statusChooseSave = NO;
    }
    
    [LogFileManager logDebugMode:@"index = %d, countRow = %d, indexTemp = %d", _index, countRow, indexTemp];
    if(countRow < maxRowOfTableItem && indexTemp == countRow - 1)
    {
        countRow++;
        //        [scrollView setFrame:CGRectMake(0, 172 + (44 * countRow), 320, 215) ];
        //        [tblEngineer setFrame:CGRectMake(0, 73, 320, 99)];
        
        [tblEngineer setFrame:CGRectMake(0, 0, tblEngineer.frame.size.width, 130 + (44 * countRow))];
        //[txtRemark setFrame:CGRectMake(10, tblEngineer.frame.origin.y + tblEngineer.frame.size.height + 10, txtRemark.frame.size.width, txtRemark.frame.size.height)];//172
        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
            [photoViewScroll setFrame:CGRectMake(0, tblEngineer.frame.origin.y + 88 + (44 * countRow) + 10, photoViewScroll.frame.size.width, photoViewScroll.frame.size.height)];
        }else{
            [photoViewScroll setFrame:CGRectMake(10, tblEngineer.frame.origin.y + 100 + (44 * countRow) + 10, photoViewScroll.frame.size.width, photoViewScroll.frame.size.height)];
        }
        [lblSubmit setFrame:CGRectMake(122, photoViewScroll.frame.origin.y + photoViewScroll.frame.size.height + 20, lblSubmit.frame.size.width, lblSubmit.frame.size.height)];
        [btnSubmit setFrame:CGRectMake(10, photoViewScroll.frame.origin.y + photoViewScroll.frame.size.height + 10, btnSubmit.frame.size.width, btnSubmit.frame.size.height)];
        [btnPostingHistory setFrame:CGRectMake(10, photoViewScroll.frame.origin.y + photoViewScroll.frame.size.height + 20 + CGRectGetHeight(btnSubmit.bounds), btnPostingHistory.frame.size.width, btnPostingHistory.frame.size.height)];
        [scrollView setContentSize:CGSizeMake(scrollView.contentSize.width, scrollView.contentSize.height + 80)];
    }
    //    if (countRow>1) {
    //        [btnSubmit setEnabled:YES];
    //    }
    [tblEngineer reloadData];
    //[btnSubmit setEnabled:[self isValidQty]];
}
//-(void)enableMethod:(id)sender{
//    if (countRow>1) {
//        if (![txtRoom.text isEqualToString:@""]) {
//            [btnSubmit setEnabled:YES];
//        }
//        else
//            [btnSubmit setEnabled:NO];
//    }
//}

-(BOOL)isOnlyViewedAccessRight
{
    //Hao Tran[20130517/ Check Access Right] - check only allow view
    if(isFromMainPosting)
    {
        if (postingEngineering.isAllowedView)
        {
            return YES;
        }
    }
    else if(actionEngineering.isAllowedView)
    {
        return YES;
    }
    //Hao Tran[20130517/ Check Access Right] - END
    
    return NO;
}

-(BOOL)isValidQty{
    
    if([self isOnlyViewedAccessRight]){
        return NO;
    }
    
    if (countRow > 1) {
        /*
        if ([txtRoom.text isEqualToString:@""]) {
            return NO;
        }*/
        return YES;
    }
    return NO;
}
#pragma mark - LongPress Response
-(void)responseLongPressButton:(UILongPressGestureRecognizer*)sender {
    if (sender.state == UIGestureRecognizerStateBegan) {
        self.indexDelete = sender.view.tag;
        
        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_delete_confirmation] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles:[[LanguageManagerV2 sharedLanguageManager] getNo], nil];
        alert.delegate = self;
        alert.tag = tagAlertDelete;
        [alert show];
    }
}

/*
#pragma mark - remark Delegate
-(void) remarkViewV2DoneWithText:(NSString *)text andController:(UIViewController *)controllerView {
    self.remarkText = text;
    
    if(![text isEqualToString:@""]){
        isUpdatedText = TRUE;
        statusChooseSave = NO;
    }
    
    
    [tblEngineer reloadData];
}
*/

#pragma mark -
-(void) alertAdvancedCancelButtonPressed {
    //do no thing
}

-(void)backBarPressed {
    
//    if(!statusChooseSave){
//        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:BUTTON_YES], [UIImage imageNamed:BUTTON_NO], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:msgSaveCount delegate:self cancelButtonTitle:msgbtnYes
//                                                           otherButtonTitles:msgbtnNo, nil];
//        alert.delegate = self;
//        alert.tag = tagAlertBack;
//        [alert show];
//        
//    } else{
        [self.navigationController popViewControllerAnimated:YES];
//    }
    
    //    if(statusChooseSave == YES) {
    //        [self.navigationController popViewControllerAnimated:YES];
    //    } else {
    //        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:BUTTON_YES], [UIImage imageNamed:BUTTON_NO], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:msgSaveCount delegate:self cancelButtonTitle:msgbtnYes
    //                                                otherButtonTitles:msgbtnNo, nil];
    //        alert.delegate = self;
    //        alert.tag = tagAlertBack;
    //        [alert show];
    //    }
}

#pragma mark - UIAlertView Delegate Methods
-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    switch (alertView.tag) {
        case tagAlertSaveThenBack:{
            switch (buttonIndex) {
                case 0:{
                    [self.navigationController popViewControllerAnimated:YES];
                } break;
                default:
                    break;
            }
            
            
        } break;
        case tagAlertBack: {
            switch (buttonIndex) {
                case 0: {
                    //YES: save minibar
                    statusChooseSave = YES;
                    isBack = YES;
                    
                    if(isFromMainPosting)
                    {
                        NSString *roomNo = [txtRoom text];
                        if(roomNo.length > 0)
                        {
                            RoomAssignmentModelV2 *roomAssignment = [[RoomAssignmentModelV2 alloc] init];
                            roomAssignment.roomAssignment_RoomId = roomNo;
                            roomAssignment.roomAssignment_UserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
                            BOOL result = [[RoomManagerV2 sharedRoomManager] getRoomAssignmentByUserIDAndRoomID:roomAssignment];
                            if(result)
                            {
                                [self saveEngineering];
                            }
                            else
                            {
                                NSString *message = nil;
                                if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                                    message = [NSString stringWithFormat:@"%@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOM_INVALID]];
                                } else {
                                    message = [NSString stringWithFormat:@"\n\n\n\n\n%@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOM_INVALID]];
                                }
                                
                                UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObject:[UIImage imageNamed:@"yes_bt.png"]] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:message delegate:nil cancelButtonTitle:[L_CLOSE currentKeyToLanguage] otherButtonTitles: nil];
                                
                                if(!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                                    UIImageView *alertImage = [[UIImageView alloc] initWithFrame:CGRectMake(80, 35, 120, 120)];
                                    [alertImage setImage:[UIImage imageNamed:@"icon_wrong room.png"]];
                                    [alert addSubview:alertImage];
                                }
                                [alert show];
                            }
                        }
                        else
                        {
                            NSString *message = nil;
                            if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                                message = [NSString stringWithFormat:@"%@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOM_INVALID]];
                            } else {
                                message = [NSString stringWithFormat:@"\n\n\n\n\n%@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOM_INVALID]];
                            }
                            
                            UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObject:[UIImage imageNamed:@"yes_bt.png"]] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:message delegate:nil cancelButtonTitle:[L_CLOSE currentKeyToLanguage] otherButtonTitles: nil];
                            
                            if(!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                                UIImageView *alertImage = [[UIImageView alloc] initWithFrame:CGRectMake(80, 35, 120, 120)];
                                [alertImage setImage:[UIImage imageNamed:@"icon_wrong room.png"]];
                                [alert addSubview:alertImage];
                            }
                            [alert show];
                        }
                    }
                    
                    
                    
                    //
                    //                    isUpdatedPhoto = NO;
                    //                    [photoList removeAllObjects];
                    //                    [self performSelector:@selector(initPhotoScrollView) withObject:nil
                    //                               afterDelay:0.2];
                    
                    
                    if (statusCheckSaveData == YES) {
                        [CommonVariable sharedCommonVariable].isSaveSuccessfull = true;
                    }
                    
                    
                }
                    break;
                    
                case 1: {
                    //NO: show popup discard
                    UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:BUTTON_YES], [UIImage imageNamed:BUTTON_NO], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:msgDiscardCount delegate:self cancelButtonTitle:msgbtnYes otherButtonTitles:msgbtnNo, nil];
                    alert.delegate = self;
                    alert.tag = tagAlertNo;
                    [alert show];
                    
                }
                    break;
                    
                default:
                    break;
            }
        }
            break;
            
        case tagAlertNo: {
            switch (buttonIndex) {
                case 0: {
                    //YES: back to count menu
                    if (statusCheckSaveData == YES) {
                        [CommonVariable sharedCommonVariable].isSaveSuccessfull = true;
                    } else {
                        [self.navigationController popViewControllerAnimated:YES];
                    }
                }
                    break;
                    
                case 1: {
                    if (statusCheckSaveData == YES) {
                        [CommonVariable sharedCommonVariable].isSaveSuccessfull = true;
                        [CommonVariable sharedCommonVariable].isNotDiscardAllChange = true;
                    }
                }
                    break;
                    
                default:
                    break;
            }
        }
            break;
            
        case tagAlertSubmit:
        {
            if(buttonIndex == 0)
            {
                //-------Log---------
                if([LogFileManager isLogConsole])
                {
                    NSInteger userId = [[UserManagerV2 sharedUserManager] currentUser].userId;
                    NSString *date = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
                    int currentCountRow = [arrayList count] > maxRowOfTableItem ? countRow:(countRow - 1);
                    for (int i = 0; i< currentCountRow; i++) {
                        
                        NSLog(@"<%@><Engineering><UserId:%d><Submit to post><Input RoomId:%@><Remark:%@><Issue:%@><Detail:%@>", date, (int)userId, txtRoom.text, remark, [arrayList objectAtIndex:i], [arrayTitle objectAtIndex:i]);
                    }
                }
                //-------Log---------
                
                statusChooseSave = YES;
                [self saveEngineeringHistory];
                [self saveEngineering];
                if(isDemoMode){
                    isPostECSuccess = TRUE;
                }
                if(isFromMainPosting)
                {
                    if (!isPostECSuccess) {
                        [tblEngineer setFrame:CGRectMake(0, 0, tblEngineer.frame.size.width, tblEngineer.frame.size.height + (44 * countRow))];
                        //[txtRemark setFrame:CGRectMake(10, 115 + (44 * countRow), txtRemark.frame.size.width, txtRemark.frame.size.height)];//172
                        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                            [photoViewScroll setFrame:CGRectMake(0, 88 + (44 * countRow) + 10, photoViewScroll.frame.size.width, photoViewScroll.frame.size.height)];
                        }else{
                            [photoViewScroll setFrame:CGRectMake(10, 100 + (44 * countRow) + 10, photoViewScroll.frame.size.width, photoViewScroll.frame.size.height)];
                        }
                        [lblSubmit setFrame:CGRectMake(122, photoViewScroll.frame.origin.y + photoViewScroll.frame.size.height + 20, lblSubmit.frame.size.width, lblSubmit.frame.size.height)];
                        [btnSubmit setFrame:CGRectMake(10, photoViewScroll.frame.origin.y + photoViewScroll.frame.size.height + 10, btnSubmit.frame.size.width, btnSubmit.frame.size.height)];
                        [btnPostingHistory setFrame:CGRectMake(10, photoViewScroll.frame.origin.y + photoViewScroll.frame.size.height + 20 + CGRectGetHeight(btnSubmit.bounds), btnPostingHistory.frame.size.width, btnPostingHistory.frame.size.height)];
                        [scrollView setContentSize:CGSizeMake(scrollView.contentSize.width, scrollView.contentSize.height + 70)];
                        [tblEngineer reloadData];
                        
                    }
                    else {
                        [txtRoom setText:@""];
                        countRow = 1;
                        //arrayList = [[NSMutableArray alloc] initWithObjects:@" ", @" ", @" ", @" ", @" ", nil];
                        [arrayList removeAllObjects];
                        //                        [txtRemark setFrame:CGRectMake(10, 146, 300, 50)];
                        //                        [btnSubmit setFrame:CGRectMake(10, 298, 300, 64)];
                        //                        [lblSubmit setFrame:CGRectMake(122, 309, 163, 35)];
                        [tblEngineer setFrame:CGRectMake(0, 0, tblEngineer.frame.size.width, tblEngineer.frame.size.height + (44 * countRow))];
                        //[txtRemark setFrame:CGRectMake(10, tblEngineer.frame.origin.y + tblEngineer.frame.size.height + 10, txtRemark.frame.size.width, txtRemark.frame.size.height)];//172
                        //[photoViewScroll setFrame:CGRectMake(10, txtRemark.frame.origin.y + txtRemark.frame.size.height + 10, photoViewScroll.frame.size.width, photoViewScroll.frame.size.height)];
                        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                            [photoViewScroll setFrame:CGRectMake(0, tblEngineer.frame.origin.y + 88 + (44 * countRow) + 10, photoViewScroll.frame.size.width, photoViewScroll.frame.size.height)];
                        }else{
                            [photoViewScroll setFrame:CGRectMake(10, tblEngineer.frame.origin.y + 100 + (44 * countRow) + 10, photoViewScroll.frame.size.width, photoViewScroll.frame.size.height)];
                        }
                        [lblSubmit setFrame:CGRectMake(122, photoViewScroll.frame.origin.y + photoViewScroll.frame.size.height + 20, lblSubmit.frame.size.width, lblSubmit.frame.size.height)];
                        [btnSubmit setFrame:CGRectMake(10, photoViewScroll.frame.origin.y + photoViewScroll.frame.size.height + 10, btnSubmit.frame.size.width, btnSubmit.frame.size.height)];
                        [btnPostingHistory setFrame:CGRectMake(10, photoViewScroll.frame.origin.y + photoViewScroll.frame.size.height + 20 + CGRectGetHeight(btnSubmit.bounds), btnPostingHistory.frame.size.width, btnPostingHistory.frame.size.height)];
                        [scrollView setContentSize:CGSizeMake(scrollView.contentSize.width, scrollView.contentSize.height + 70)];
                        
                        [tblEngineer reloadData];
                    }
                    //[self.navigationController popViewControllerAnimated:YES];
                }
                else
                {
                    NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
                    for (UIViewController *aViewController in allViewControllers) {
                        if ([aViewController isKindOfClass:[RoomAssignmentInfoViewController class]]) {
                            [self.navigationController popToViewController:aViewController animated:YES];
                        }
                    }
                }
            }
            //-------Log---------
            else {
                //-------Log---------
                if([LogFileManager isLogConsole])
                {
                    NSInteger userId = [[UserManagerV2 sharedUserManager] currentUser].userId;
                    NSString *date = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
                    int currentCountRow = [arrayList count]>maxRowOfTableItem ? countRow:(countRow-1);
                    for (int i=0; i<currentCountRow; i++) {
                        NSLog(@"<%@><Engineering><UserId:%d><Not confirm to post><Input RoomId:%@><Remark:%@><Issue:%@><Detail:%@>", date, (int)userId, txtRoom.text, remark, [arrayList objectAtIndex:i], [arrayTitle objectAtIndex:i]);
                    }
                }
                //-------Log---------
            }
            break;
        }
            
        case tagAlertDelete: {
            switch (buttonIndex) {
                case 0: {
                    //YES: Delete photolist
                    if ([photoList count] > indexDelete) {
                        EngineerImageModelV2 *imageDelete = [photoList objectAtIndex:indexDelete];
                        //                        [[EngineerCaseViewManagerV2 sharedEngineerCaseViewManagerV2] deleteEngineerImageModelV2:imageDelete];
                        
                        if (!self.photoDeleteList) {
                            self.photoDeleteList = [[NSMutableArray alloc] init];
                        }
                        
                        [self.photoDeleteList addObject:imageDelete];
                        
                        [photoList removeObjectAtIndex:indexDelete];
                        
                        
                        [self performSelector:@selector(initPhotoScrollView)];
                        isUpdatedPhoto = YES;
                        
                    }
                }
                    break;
                    
                case 1: {
                    // NO
                }
                    break;
                    
                default:
                    break;
            }
        }
            break;
            
        default:
            break;
    }
}

//-(BOOL)textViewShouldBeginEditing:(UITextView *)textView
//{
//    [self showRemarkView];
//    return NO;
//}

-(void) showEngineerRemarkView
{
    if (remarkView == nil) {
        remarkView = [[RemarkViewV2 alloc] initWithNibName:@"RemarkViewV2" bundle:nil];
    }
    UIView *view = remarkView.view;
    [remarkView setDelegate:self];
    //[remarkView setTextinRemark:txtRemark.text != nil ? txtRemark.text : @""];
    [remarkView setTextinRemark:remark != nil ? remark : @""];
    [remarkView viewWillAppear:YES];
    remarkView.isAddSubview = YES;
    [self.tabBarController.view addSubview:view];
}

#pragma mark - Remark View Delegate
-(void)remarkViewV2DoneWithText:(NSString *)text andController:(UIViewController *)controllerView{
  
    //show wifi view for fix no wifi view
    if(!isDemoMode){
        [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationShowWifiView] object:nil];
    }
    //txtRemark.text = text;
    remark = text;
    if(remarkView) {
        [remarkView setDelegate:nil];
    }
    [tblEngineer reloadData];
}

#pragma mark - Save Data
-(void)saveData {
    statusChooseSave = YES;
    [self saveEngineering];
    
    isUpdatedPhoto = NO;
    [photoList removeAllObjects];
    [self performSelector:@selector(initPhotoScrollView) withObject:nil
               afterDelay:0.2];
}

-(void) checkSaveData {
    statusCheckSaveData = YES;
    if (statusChooseSave == YES) {
        [CommonVariable sharedCommonVariable].isSaveSuccessfull = true;
    } else {
        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:msgSaveCount delegate:self cancelButtonTitle:msgbtnYes
                                                           otherButtonTitles:msgbtnNo, nil];
        alert.delegate = self;
        alert.tag = tagAlertBack;
        [alert show];
    }
}

#pragma mark - === Handle Check Save Data ===
#pragma mark
-(BOOL) checkSaveDataWithTagEvent:(NSInteger) tagOfEvent {
    
    
    if(!statusChooseSave){
        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:BUTTON_YES], [UIImage imageNamed:BUTTON_NO], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:msgSaveCount delegate:self cancelButtonTitle:msgbtnYes
                                                           otherButtonTitles:msgbtnNo, nil];
        alert.delegate = self;
        alert.tag = tagAlertBack;
        [alert show];
        return YES;
    } else {
        if(tagOfEvent == tagOfFindButton || tagOfEvent == tagOfSettingButton || tagOfEvent == tagOfUnassignButton || tagOfEvent == tagOfJobButton){
            //Show alert complete room
            UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_complete_room_to_leave] delegate:self cancelButtonTitle:msgbtnYes otherButtonTitles: nil];
            alert.delegate = self;
            [alert show];
            
            return YES;
        }
        
        if(tagOfEvent == tagOfMessageButton){
            [[CommonVariable sharedCommonVariable] setRoomIsCompleted:0];
            return NO;
        }
    }
    
    return NO;
    
    //    if(tagOfEvent == tagOfFindButton || tagOfEvent == tagOfSettingButton || tagOfEvent == tagOfUnassignButton || tagOfEvent == tagOfJobButton){
    //        //Show alert complete room
    //        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_complete_room_to_leave] delegate:self cancelButtonTitle:msgbtnYes otherButtonTitles: nil];
    //        alert.delegate = self;
    //        [alert show];
    //
    //        return YES;
    //
    //    } else{
    //        return NO;
    //    }
}


#pragma didden after save.
-(void) hiddenHUDAfterSaved:(MBProgressHUD *)HUD{
    [HUD hide:YES];
    [HUD removeFromSuperview];
}

#pragma mark - Handle Topbar Methods
-(void)loadTopbarView {
    TopbarViewV2 *topbar = [[TopbarViewV2 alloc] initViewWithSuperModel:[[CommonVariable sharedCommonVariable] superRoomModel]];
    [topbar setTag:tagTopbarView];
    [self.view addSubview:topbar];
    [topbar setHidden:YES];
    [topbar setAlpha:0.0];
    [self addButtonHandleShowHideTopbar];
}

-(void)showTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationWillStartSelector:@selector(setHiddenTopbarView)];
    
    f = topbar.frame;
    f.origin.y = 0;
    [topbar setFrame:f];
    [topbar setAlpha:1.0];
    
    [self adjustShowForViews];
    
    [UIView commitAnimations];
}

-(void)hideTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDidStopSelector:@selector(setHiddenTopbarView)];
    [UIView setAnimationDelegate:self];
    
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    [topbar setAlpha:0.0];
    
    [self adjustRemoveForViews];
    
    [UIView commitAnimations];
}

-(void)setHiddenTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    [topbar setHidden:!topbar.hidden];
}

-(TopbarViewV2 *)getTopBarView {
    return (TopbarViewV2 *)[self.view viewWithTag:tagTopbarView];
}

//-(void)addButtonHandleShowHideTopbar {
//    UIButton *titleBarButton = [[UIButton alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
//
//    titleBarButton.titleLabel.backgroundColor = [UIColor clearColor];
//    titleBarButton.titleLabel.font = FONT_BUTTON_TOPBAR;
//    titleBarButton.titleLabel.textAlignment = NSTextAlignmentCenter;
//    titleBarButton.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
//    [titleBarButton.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
//    [titleBarButton.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
//    [titleBarButton addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
//    [titleBarButton setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ENGINEERING_CASE]
//                    forState:UIControlStateNormal];
//    self.navigationItem.titleView = titleBarButton;
//}

-(void)addButtonHandleShowHideTopbar {
    UIView *vBarButton = [[UIView alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
    UIButton *titleBarButtonFirst = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 200, 30)];
    
    titleBarButtonFirst.titleLabel.backgroundColor = [UIColor clearColor];
    [titleBarButtonFirst.titleLabel setFont: FONT_BUTTON_TOPBAR];
    titleBarButtonFirst.titleLabel.textAlignment = NSTextAlignmentCenter;
    titleBarButtonFirst.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
    
    [titleBarButtonFirst.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
    [titleBarButtonFirst.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
    [titleBarButtonFirst addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    if(isFromMainPosting){
        [titleBarButtonFirst setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_Counts] forState:UIControlStateNormal];
    } else {
        [titleBarButtonFirst setTitle:txtRoom.text forState:UIControlStateNormal];
    }
    
    [vBarButton addSubview:titleBarButtonFirst];
    
    
    UIButton *titleBarButtonSecond = [[UIButton alloc] initWithFrame:CGRectMake(0, 28, 200, 22)];
    [titleBarButtonSecond setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [titleBarButtonSecond.titleLabel setFont:FONT_BUTTON_TOPBAR_SECOND];
    [titleBarButtonSecond setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ENGINEERING_CASE] forState:UIControlStateNormal];//Set Title for Tabbar
    [titleBarButtonSecond addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    [vBarButton addSubview:titleBarButtonSecond];
    
    self.navigationItem.titleView = vBarButton;
    
    //Move title to center vertical
    CGRect titleViewBounds = self.navigationItem.titleView.bounds;
    int moveLeftTitle = 0;
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        moveLeftTitle = 10;
    }
    self.navigationItem.titleView.bounds = CGRectMake(titleViewBounds.origin.x + moveLeftTitle, Y_TOPBAR_TITLE, titleViewBounds.size.width, titleViewBounds.size.height);
}

-(void)topbarTapped:(UIButton *)sender {
    TopbarViewV2 *topbar = [self getTopBarView];
    [sender setEnabled:NO];
    
    if (topbar.isHidden) {
        [self showTopbarView];
    } else {
        [self hideTopbarView];
    }
    
    [self performSelector:@selector(topbarButtonEnable:) withObject:sender afterDelay:0.5];
}

-(void) topbarButtonEnable:(UIButton *) topbarBtn {
    [topbarBtn setEnabled:YES];
}

-(void)adjustShowForViews {
    //adjust all views when show
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect fTopBar = topbar.frame;
    
    [viewRoom setFrame:CGRectMake(0, fTopBar.size.height, viewRoom.frame.size.width, viewRoom.frame.size.height)];
    CGRect fViewRoom = viewRoom.frame;
    
    [tblEngineer setFrame:CGRectMake(tblEngineer.frame.origin.x, tblEngineer.frame.origin.y, tblEngineer.frame.size.width, tblEngineer.frame.size.height + fTopBar.size.height)];
    
    //More height for 4.0 inch screen
    int deviceKind = [DeviceManager getDeviceScreenKind];
    int moreHeight = 0;
    if(deviceKind == DeviceScreenKindRetina4_0){
        moreHeight = 81;
    }
    [scrollView setFrame:CGRectMake(0, fTopBar.size.height + fViewRoom.size.height + 10, scrollViewWidth, scrollViewHeight - fTopBar.size.height + moreHeight)];
}

-(void)adjustRemoveForViews {
    //adjust all views when hide
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect fTopBar = topbar.frame;
    
    [viewRoom setFrame:CGRectMake(0, 0, viewRoom.frame.size.width, viewRoom.frame.size.height)];
    CGRect fViewRoom = viewRoom.frame;
    
    [tblEngineer setFrame:CGRectMake(tblEngineer.frame.origin.x, tblEngineer.frame.origin.y, tblEngineer.frame.size.width, tblEngineer.frame.size.height - fTopBar.size.height)];
    
    //More height for 4.0 inch screen
    int deviceKind = [DeviceManager getDeviceScreenKind];
    int moreHeight = 0;
    if(deviceKind == DeviceScreenKindRetina4_0){
        moreHeight = 81;
    }
    [scrollView setFrame:CGRectMake(0, fViewRoom.size.height + 10, scrollViewWidth, scrollViewHeight + moreHeight)];
}

#pragma mark - Load Data
-(void) loadDataBase {
    [[HomeViewV2 shareHomeView] waitingLoadingData];
    
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getLoadingData]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES];
    
    /*Binh Vo - Start Get Engineering Category, Item from WS*/
    NSString *categoryLastDateModified = [[EngineerCaseViewManagerV2 sharedEngineerCaseViewManagerV2] getEngineeringCategoryLastModifiedDate];
    //[[EngineerCaseViewManagerV2 sharedEngineerCaseViewManagerV2] getDataEngineeringCategoryWithUserID:[NSNumber numberWithInt:[[UserManagerV2 sharedUserManager] currentUser].userId] AndLastModified:categoryLastDateModified AndPercentView:HUD];
    
    NSString *hotelId = [NSString stringWithFormat:@"%d", [[[UserManagerV2 sharedUserManager] currentUser] userHotelsId]];
    [[EngineerCaseViewManagerV2 sharedEngineerCaseViewManagerV2] getDataEngineeringCategoryWithUserID:[NSNumber numberWithInt:[[UserManagerV2 sharedUserManager] currentUser].userId] lastModified:categoryLastDateModified AndHotelID:hotelId AndPercentView:HUD];
    
    [[EngineerCaseViewManagerV2 sharedEngineerCaseViewManagerV2] getDataEngineeringItemWithUserID:[NSNumber numberWithInt:[[UserManagerV2 sharedUserManager] currentUser].userId] AndHotelID:[NSNumber numberWithInt:[[[UserManagerV2 sharedUserManager] currentUser] userHotelsId]] AndLastModified:@""];
    /*Binh Vo - End Get*/
    
    //    arrayList = [[NSMutableArray alloc] initWithObjects:[NSString stringWithFormat:@"%@ 1", [[LanguageManagerV2 sharedLanguageManager] getIssue]], [NSString stringWithFormat:@"%@ 2", [[LanguageManagerV2 sharedLanguageManager] getIssue]], [NSString stringWithFormat:@"%@ 3", [[LanguageManagerV2 sharedLanguageManager] getIssue]], [NSString stringWithFormat:@"%@ 4", [[LanguageManagerV2 sharedLanguageManager] getIssue]], [[LanguageManagerV2 sharedLanguageManager] getRemark], nil];
    arrayList = [[NSMutableArray alloc] init];
    arrayTitle = [[NSMutableArray alloc] init];
//    arrayList = [[NSMutableArray alloc] initWithObjects:@" ", @" ", @" ", @" ", @" ", nil];
//    
//    arrayTitle = [[NSMutableArray alloc] initWithObjects:@" ", @" ", @" ", @" ", @" ", nil];
//    
//    arrayIndex = [[NSMutableArray alloc] initWithObjects:@"0", @"1", @"2", @"3", @"4", nil];
    
    NSMutableArray *listEngineerCase ;
    EngineeringCaseDetailModelV2 *modelDetail = [[EngineeringCaseDetailModelV2 alloc] init];
    modelDetail.ecd_room_id = [RoomManagerV2 getCurrentRoomNo];
    modelDetail.ecd_user_id = [[UserManagerV2 sharedUserManager] currentUser].userId;
    [[EngineerCaseViewManagerV2 sharedEngineerCaseViewManagerV2] loadEngineerDetailModelByRoomIdUserIdAndServiceId:modelDetail];
    
    if(modelDetail.ecd_id != 0) {
        listEngineerCase = [[EngineerCaseViewManagerV2 sharedEngineerCaseViewManagerV2] loadAllEngineerCaseViewModelV2:modelDetail.ecd_id];
        for(int i = 0; i < [listEngineerCase count]; i++) {
            //EngineerCaseViewModelV2 *model = [listEngineerCase objectAtIndex:i];
            
            // NSMutableArray *listItem = [[EngineerCaseViewManagerV2 sharedEngineerCaseViewManagerV2] loadAllEngineeringItembyItemCategogy:model.ec_category_id item_id:model.ec_item_id];
            
            //EngineeringItemModelV2 *modelItem = [listItem objectAtIndex:0];
            //            [arrayTitle removeObjectAtIndex:model.ec_index];
            //            [arrayTitle insertObject:modelItem.eni_name atIndex:model.ec_index];
        }
    }
    
    [self performSelector:@selector(initPhotoScrollView) withObject:nil
               afterDelay:0.2];
    
    [tblEngineer reloadData];
    
    [self performSelector:@selector(hiddenHUDAfterSaved:) withObject:HUD
               afterDelay:0.5];
    
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
}

#pragma mark - Save Data
-(void)saveEngineering {
    
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.view];
    //    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getMsgSaveMessage]];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getLoadingData]];
    [self.view addSubview:HUD];
    [HUD show:YES];
    
    EngineeringCaseDetailModelV2 *modelDetail = [[EngineeringCaseDetailModelV2 alloc] init];
    modelDetail.ecd_room_id = [RoomManagerV2 getCurrentRoomNo];
    modelDetail.ecd_user_id = [[UserManagerV2 sharedUserManager] currentUser].userId;
    [[EngineerCaseViewManagerV2 sharedEngineerCaseViewManagerV2] loadEngineerDetailModelByRoomIdUserIdAndServiceId:modelDetail];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd-MMM-yyyy HH:mm:ss"];
    NSDate *now = [NSDate date];
    modelDetail.ecd_date = [dateFormat stringFromDate:now];
    
    //    modelDetail.ecd_reMark = remarkText;
//    NSString *remark = @"";
//    if(txtRemark.textColor == [UIColor blackColor])
//    {
//        remark = txtRemark.text;
//    }
    modelDetail.ecd_reMark = remark;
    
    if(modelDetail.ecd_id != 0)
        [[EngineerCaseViewManagerV2 sharedEngineerCaseViewManagerV2] updateEngineeringCaseDetailModelV2:modelDetail];
    else
        [[EngineerCaseViewManagerV2 sharedEngineerCaseViewManagerV2] insertEngineeringCaseDetailModelV2:modelDetail];
    
    [[EngineerCaseViewManagerV2 sharedEngineerCaseViewManagerV2] loadEngineerDetailModelByRoomIdUserIdAndServiceId:modelDetail];
    
    NSMutableArray *arrayEngineerCase = [[EngineerCaseViewManagerV2 sharedEngineerCaseViewManagerV2] loadAllEngineerCaseViewModelV2:modelDetail.ecd_id];
    
    if([arrayEngineerCase count] > 0) {
        for(int a = 0; a < [arrayEngineerCase count]; a++) {
            EngineerCaseViewModelV2 *model = [arrayEngineerCase objectAtIndex:a];
            [[EngineerCaseViewManagerV2 sharedEngineerCaseViewManagerV2] deleteEngineerCaseViewModelV2:model];
        }
    }
    
    NSMutableArray *array = [[EngineerCaseViewManagerV2 sharedEngineerCaseViewManagerV2] loadAllEngineeringItemModelV2];
    EngineerCaseViewModelV2 *engineerModel = [[EngineerCaseViewModelV2 alloc] init];
    
    for(int i = 0; i < [array count]; i++) {
        EngineeringItemModelV2 *itemModel = [array objectAtIndex:i];
        
        if([arrayTitle count] > 0) {
            for(int j = 0; j < [arrayTitle count]; j++) {
                // NSLog(@"%@ %@", itemModel.eni_name, [arrayTitle objectAtIndex:j]);
                // FM [20160328] - Add eni_name_lang to cater for Chinese Language
                if(([itemModel.eni_name isEqualToString:[arrayTitle objectAtIndex:j]]) ||
                    ([itemModel.eni_name_lang isEqualToString:[arrayTitle objectAtIndex:j]]))
                {
                    engineerModel.ec_category_id = itemModel.eni_category_id;
                    engineerModel.ec_item_id = itemModel.eni_id;
                    engineerModel.ec_detail_id = modelDetail.ecd_id;
                    engineerModel.ec_index = [[arrayIndex objectAtIndex:j] integerValue];
                    [[EngineerCaseViewManagerV2 sharedEngineerCaseViewManagerV2] insertEngineerCaseViewModelV2:engineerModel];
                }
            }
        }
    }
    
    NSMutableArray *arrayECModel = [[EngineerCaseViewManagerV2 sharedEngineerCaseViewManagerV2] loadAllEngineerCaseViewModelV2:modelDetail.ecd_id];
    
    if ([photoDeleteList count] > 0) {
        for ( EngineerImageModelV2 *deletePhoto in photoDeleteList) {
            [[EngineerCaseViewManagerV2 sharedEngineerCaseViewManagerV2] deleteEngineerImageModelV2:deletePhoto];
            //            [photoDeleteList removeObject:deletePhoto];
        }
        
        [photoDeleteList removeAllObjects];
    }
    
    NSMutableArray *arrayImage = [[EngineerCaseViewManagerV2 sharedEngineerCaseViewManagerV2] loadAlLEngineerImageByDetailID:(int)modelDetail.ecd_id];
    
    if([arrayImage count] > 0) {
        for(int a = 0; a < [arrayImage count]; a++) {
            EngineerImageModelV2 *model = [arrayImage objectAtIndex:a];
            [[EngineerCaseViewManagerV2 sharedEngineerCaseViewManagerV2] deleteEngineerImageModelV2:model];
        }
    }
    
    for(int i = 0; i < [photoList count]; i++) {
        EngineerImageModelV2 *model = [photoList objectAtIndex:i];
        model.enim_id = i + 1;
        model.enim_detail_id = modelDetail.ecd_id;
        
        NSInteger isSuccess = [[EngineerCaseViewManagerV2 sharedEngineerCaseViewManagerV2] insertEngineerImageModelV2:model];
        
        if(isSuccess == 0)
            [[EngineerCaseViewManagerV2 sharedEngineerCaseViewManagerV2] updateEngineerImageModelV2:model];
    }
    
    NSMutableArray *arrayPhoto = [[EngineerCaseViewManagerV2 sharedEngineerCaseViewManagerV2] loadAlLEngineerImageByDetailID:(int)modelDetail.ecd_id];
    
    // post data in here
    // post photo in here
    //    BOOL isPostPhotoSuccess = FALSE;
    //    BOOL isPostECSuccess = FALSE;
    //-------------------Post WSLog
    NSInteger userId = [[UserManagerV2 sharedUserManager] currentUser].userId;
    NSString *date = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
    int currentCountRow = [arrayList count]>maxRowOfTableItem ? countRow:(countRow-1);
    int switchStatus = (int)[[NSUserDefaults standardUserDefaults] integerForKey:ENABLE_LOGWS];
    if (switchStatus == 1) {
        for (int i=0; i < currentCountRow; i++) {
            NSString *messageValue = [NSString stringWithFormat:@"Engineering Post WSLog: Time:%@, USerId:%d, RoomId:%@, Remark:%@, Issue:%@, Detail:%@", date, (int)userId, txtRoom.text, remark, [arrayList objectAtIndex:i], [arrayTitle objectAtIndex:i]];
            [[EngineerCaseViewManagerV2 sharedEngineerCaseViewManagerV2] postWSLog:userId Message:messageValue MessageType:1];
        }
    }
    
    if(isDemoMode){
        isPostECSuccess = TRUE;
    } else {
        if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
            
            //remove for new WS post Engineering
            //        for (EngineerImageModelV2 *imageModel in arrayPhoto) {
            //            isPostPhotoSuccess = [[EngineerCaseViewManagerV2 sharedEngineerCaseViewManagerV2] postECAttachPhotoWithUserID:[NSNumber numberWithInteger:modelDetail.ecd_user_id] AndRoomAssignID:[NSNumber numberWithInteger:modelDetail.ecd_room_id] AndPhoto:imageModel.enim_image];
            //        }
            //
            //        if(isPostPhotoSuccess == TRUE || [arrayPhoto count] <= 0) {
            //            for (int i = 0; i < [arrayECModel count]; i++) {
            //                EngineerCaseViewModelV2 *model = [arrayECModel objectAtIndex:i];
            //
            //                isPostECSuccess = [[EngineerCaseViewManagerV2 sharedEngineerCaseViewManagerV2] postEngineeringCaseWithUserID:[NSNumber numberWithInteger:modelDetail.ecd_user_id] AndRoomAssignID:[NSNumber numberWithInteger:modelDetail.ecd_room_id] AndEngineeringItemID:[NSNumber numberWithInteger:model.ec_item_id] AndReportedTime:modelDetail.ecd_date                                                                                                                    AndRemark:modelDetail.ecd_reMark];
            //            }
            //        }
            
            //apply for new WS post Engineering
            int maxIndexPhoto = (int)[arrayPhoto count] - 1;
            for (int i = 0; i < [arrayECModel count]; i++) {
                EngineerCaseViewModelV2 *model = [arrayECModel objectAtIndex:i];
                
                EngineerImageModelV2 *imageModel = nil;
                if(i <= maxIndexPhoto) {
                    imageModel = [arrayPhoto objectAtIndex:i];
                }
                if(isDemoMode){
                    isPostECSuccess = TRUE;
                } else {
                    isPostECSuccess = [[EngineerCaseViewManagerV2 sharedEngineerCaseViewManagerV2] postEngineeringCaseWithUserID:[NSNumber numberWithInteger:modelDetail.ecd_user_id] AndRoomAssignID:modelDetail.ecd_room_id AndEngineeringItemID:[NSNumber numberWithInteger:model.ec_item_id] AndReportedTime:modelDetail.ecd_date AndRemark:modelDetail.ecd_reMark AndPhoto: ((imageModel == nil) ? nil : imageModel.enim_image) AndExtension:@"JPEG"];
                }
            }
            
            if(isPostECSuccess == TRUE) {
                [[EngineerCaseViewManagerV2 sharedEngineerCaseViewManagerV2] deleteAllEngineerImageData:(int)modelDetail.ecd_id];
                
                [[EngineerCaseViewManagerV2 sharedEngineerCaseViewManagerV2] deleteAllEngineerCaseViewData:(int)modelDetail.ecd_id];
                
                [[EngineerCaseViewManagerV2 sharedEngineerCaseViewManagerV2] deleteEngineeringCaseDetailModelV2:modelDetail];
            }
        }
    }
    
    if (!isPostECSuccess) {
        //Fail
        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:BUTTON_YES], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_save_fail] delegate:nil cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles: nil];
        alert.tag = tagAlertSaveThenBack;
        [alert show];
    }
    else {
        if(isBack){
            if(isFromMainPosting)
            {
                UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:BUTTON_YES], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_save_success] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles: nil];
                alert.tag = tagAlertSaveThenBack;
                [alert show];
            }
        }
        else{
            
            UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:BUTTON_YES], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_save_success] delegate:nil cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles: nil];
            
            [alert show];
            
        }
    }
    
    
    
    
    /************************ clear data after save  ************************/
    if (isPostECSuccess){
        for (int indexArray = 0; indexArray < [arrayTitle count]; indexArray++) {
            [arrayTitle removeObjectAtIndex:indexArray];
            [arrayTitle insertObject:@" " atIndex:indexArray];
            
        }
        //    self.remarkText = @" ";
        //-------add------
        //    if (isPostECSuccess) {
        remark = @"";
        //    }
        //-------add------
        //    txtRemark.text = @"";//Comment
        
        if (!self.photoDeleteList) {
            self.photoDeleteList = [[NSMutableArray alloc] init];
        }
        
        for (int indexArray = 0; indexArray < [photoList count]; indexArray++) {
            EngineerImageModelV2 *imageDelete = [photoList objectAtIndex:indexArray];
            [self.photoDeleteList addObject:imageDelete];
        }
        ///-----add
        //    if (isPostECSuccess) {
        [photoList removeAllObjects];
        isUpdatedPhoto = NO;
        [self performSelector:@selector(initPhotoScrollView)];
        [tblEngineer reloadData];
        //    }
        ///-----add
        //    [photoList removeAllObjects];
        //    isUpdatedPhoto = NO;
        //    [self performSelector:@selector(initPhotoScrollView)];
        //    [tblEngineer reloadData];
    }
    /************************ ************************/
    
    isUpdatedText = NO;
    [self performSelector:@selector(hiddenHUDAfterSaved:) withObject:HUD afterDelay:0.5];
}

//update code ------------------------
-(void)saveEngineeringHistory{
    
    EngineeringHistoryModel *ehistoryModel = [[EngineeringHistoryModel alloc] init];
    ehistoryModel.eh_room_id = txtRoom.text;
    ehistoryModel.eh_user_id = [[UserManagerV2 sharedUserManager] currentUser].userId;
    ehistoryModel.eh_date = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
    //save engineering item
    NSMutableArray *array = [[EngineerCaseViewManagerV2 sharedEngineerCaseViewManagerV2] loadAllEngineeringItemModelV2];
    //    EngineerCaseViewModelV2 *engineerModel = [[EngineerCaseViewModelV2 alloc] init];
    
    for(int i = 0; i < [array count]; i++) {
        EngineeringItemModelV2 *itemModel = [array objectAtIndex:i];
        
        if([arrayTitle count] > 0) {
            for(int j = 0; j < [arrayTitle count]; j++) {
                
                // FM [20160328] - Add eni_name_lang to cater for Chinese Language
                //if([itemModel.eni_name isEqualToString:[arrayTitle objectAtIndex:j]]) {
                
                if(([itemModel.eni_name isEqualToString:[arrayTitle objectAtIndex:j]]) ||
                   ([itemModel.eni_name_lang isEqualToString:[arrayTitle objectAtIndex:j]]))
                {
                    ehistoryModel.eh_item_id = itemModel.eni_id;
                    EngineeringHistoryManager *eHistoryManager = [[EngineeringHistoryManager alloc] init];
                    [eHistoryManager insertEngineeringHistory:ehistoryModel];
                }
            }
        }
    }
    
}

#pragma mark - set caption view
-(void)setCaptionView {
    msgbtnNo = [[LanguageManagerV2 sharedLanguageManager] getNo];
    msgbtnYes = [[LanguageManagerV2 sharedLanguageManager] getYes];
    msgSaveCount = [[LanguageManagerV2 sharedLanguageManager] getMsgAlertSaveCounts];
    msgDiscardCount = [[LanguageManagerV2 sharedLanguageManager] getMsgAlertDiscardCounts];
}

/**
 * Name : shouldChangeCharactersInRange
 * Description : For hiding the keyboard. This must be implemented if you are working with UITextField
 * Parameter :
 * Return : BOOL
 */
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if([string isEqualToString:@"\n"])
    {
        [textField resignFirstResponder];
        return NO;
    }
    return YES;
}


/**
 * Name : textFieldShouldReturn
 * Description : Hiding the keyboard of UITextField
 * Parameter :
 * Return : BOOL
 */
- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    NSInteger userId = [[UserManagerV2 sharedUserManager] currentUser].userId;
    NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
    if([LogFileManager isLogConsole])
    {
        NSLog(@"<%@><Engineering><Input UserId:%d><RoomId:%@><Remark:%@>", time, (int)userId, textField.text, remark);
    }
    //[btnSubmit setEnabled:[self isValidQty]];
    [textField resignFirstResponder];
    [self setEnableBtnAtionHistory:YES];
    return YES;
}

/**
 * Name : shouldChangeCharactersInRange
 * Description : For hiding the keyboard. This must be implemented if you are working with UITextField
 * Parameter :
 * Return : BOOL
 */
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)string
{
    if([string isEqualToString:@"\n"])
    {
        NSInteger userId = [[UserManagerV2 sharedUserManager] currentUser].userId;
        NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
        if([LogFileManager isLogConsole])
        {
            NSLog(@"<%@><Engineering><Input UserId:%d><RoomId:%@><Remark:%@>", time, (int)userId, txtRoom.text, textView.text);
        }
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}


/**
 * Name : textFieldShouldReturn
 * Description : Hiding the keyboard of UITextField
 * Parameter :
 * Return : BOOL
 */
- (BOOL) textViewShouldReturn:(UITextView *)textView
{
    [textView resignFirstResponder];
    return YES;
}

/*
- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    if(txtRemark.textColor == [UIColor lightGrayColor])
    {
        txtRemark.text = @"";
        txtRemark.textColor = [UIColor blackColor];
    }
    
    return YES;
}*/


//- (void) textViewDidChange:(UITextView *)textView
//{
//    if(txtRemark.text.length == 0)
//    {
//        //txtRemark.text = @"Remark";
//        txtRemark.textColor = [UIColor lightGrayColor];
//        [txtRemark resignFirstResponder];
//    }
//}

- (NSMutableString*) aggregateData {
    NSMutableString *confirmData = [NSMutableString string];
    for(NSInteger i = 0; i<[arrayTitle count]; i++) {
            NSString *itemEng = [NSString stringWithFormat:@"1 - %@", arrayTitle[i]];
            [confirmData appendString:[NSString stringWithFormat:@"\n%@", itemEng]];
    }
    return confirmData;
}

//Thien Chau: CRF-00001277 > [Marriott] Confirmation Message before Submit Posting on Minibar / Engineering / Amenities / Lost & Found
- (void)alertView:(DTAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {

    if (buttonIndex == 0) {
        //-------Log---------
        if([LogFileManager isLogConsole])
        {
            NSInteger userId = [[UserManagerV2 sharedUserManager] currentUser].userId;
            NSString *date = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
            int currentCountRow = [arrayList count] > maxRowOfTableItem ? countRow:(countRow - 1);
            for (int i = 0; i< currentCountRow; i++) {
                
                NSLog(@"<%@><Engineering><UserId:%d><Submit to post><Input RoomId:%@><Remark:%@><Issue:%@><Detail:%@>", date, (int)userId, txtRoom.text, remark, [arrayList objectAtIndex:i], [arrayTitle objectAtIndex:i]);
            }
        }
        //-------Log---------
        
        statusChooseSave = YES;
        [self saveEngineeringHistory];
        [self saveEngineering];
        if(isDemoMode){
            isPostECSuccess = TRUE;
        }
        if(isFromMainPosting)
        {
            if (!isPostECSuccess) {
                [tblEngineer setFrame:CGRectMake(0, 0, tblEngineer.frame.size.width, tblEngineer.frame.size.height + (44 * countRow))];
                //[txtRemark setFrame:CGRectMake(10, 115 + (44 * countRow), txtRemark.frame.size.width, txtRemark.frame.size.height)];//172
                if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                    [photoViewScroll setFrame:CGRectMake(0, 88 + (44 * countRow) + 10, photoViewScroll.frame.size.width, photoViewScroll.frame.size.height)];
                }else{
                    [photoViewScroll setFrame:CGRectMake(10, 100 + (44 * countRow) + 10, photoViewScroll.frame.size.width, photoViewScroll.frame.size.height)];
                }
                [lblSubmit setFrame:CGRectMake(122, photoViewScroll.frame.origin.y + photoViewScroll.frame.size.height + 20, lblSubmit.frame.size.width, lblSubmit.frame.size.height)];
                [btnSubmit setFrame:CGRectMake(10, photoViewScroll.frame.origin.y + photoViewScroll.frame.size.height + 10, btnSubmit.frame.size.width, btnSubmit.frame.size.height)];
                [btnPostingHistory setFrame:CGRectMake(10, photoViewScroll.frame.origin.y + photoViewScroll.frame.size.height + 20 + CGRectGetHeight(btnSubmit.bounds), btnPostingHistory.frame.size.width, btnPostingHistory.frame.size.height)];
                [scrollView setContentSize:CGSizeMake(scrollView.contentSize.width, scrollView.contentSize.height + 70)];
                [tblEngineer reloadData];
                
            }
            else {
                [txtRoom setText:@""];
                countRow = 1;
                //arrayList = [[NSMutableArray alloc] initWithObjects:@" ", @" ", @" ", @" ", @" ", nil];
                [arrayList removeAllObjects];
                //                        [txtRemark setFrame:CGRectMake(10, 146, 300, 50)];
                //                        [btnSubmit setFrame:CGRectMake(10, 298, 300, 64)];
                //                        [lblSubmit setFrame:CGRectMake(122, 309, 163, 35)];
                [tblEngineer setFrame:CGRectMake(0, 0, tblEngineer.frame.size.width, tblEngineer.frame.size.height + (44 * countRow))];
                //[txtRemark setFrame:CGRectMake(10, tblEngineer.frame.origin.y + tblEngineer.frame.size.height + 10, txtRemark.frame.size.width, txtRemark.frame.size.height)];//172
                //[photoViewScroll setFrame:CGRectMake(10, txtRemark.frame.origin.y + txtRemark.frame.size.height + 10, photoViewScroll.frame.size.width, photoViewScroll.frame.size.height)];
                if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                    [photoViewScroll setFrame:CGRectMake(0, tblEngineer.frame.origin.y + 88 + (44 * countRow) + 10, photoViewScroll.frame.size.width, photoViewScroll.frame.size.height)];
                }else{
                    [photoViewScroll setFrame:CGRectMake(10, tblEngineer.frame.origin.y + 100 + (44 * countRow) + 10, photoViewScroll.frame.size.width, photoViewScroll.frame.size.height)];
                }
                [lblSubmit setFrame:CGRectMake(122, photoViewScroll.frame.origin.y + photoViewScroll.frame.size.height + 20, lblSubmit.frame.size.width, lblSubmit.frame.size.height)];
                [btnSubmit setFrame:CGRectMake(10, photoViewScroll.frame.origin.y + photoViewScroll.frame.size.height + 10, btnSubmit.frame.size.width, btnSubmit.frame.size.height)];
                [btnPostingHistory setFrame:CGRectMake(10, photoViewScroll.frame.origin.y + photoViewScroll.frame.size.height + 20 + CGRectGetHeight(btnSubmit.bounds), btnPostingHistory.frame.size.width, btnPostingHistory.frame.size.height)];
                [scrollView setContentSize:CGSizeMake(scrollView.contentSize.width, scrollView.contentSize.height + 70)];
                
                [tblEngineer reloadData];
            }
            //[self.navigationController popViewControllerAnimated:YES];
        }
        else
        {
            NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
            for (UIViewController *aViewController in allViewControllers) {
                if ([aViewController isKindOfClass:[RoomAssignmentInfoViewController class]]) {
                    [self.navigationController popToViewController:aViewController animated:YES];
                }
            }
        }
    } else {
        //-------Log---------
        if([LogFileManager isLogConsole])
        {
            NSInteger userId = [[UserManagerV2 sharedUserManager] currentUser].userId;
            NSString *date = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
            int currentCountRow = [arrayList count]>maxRowOfTableItem ? countRow:(countRow-1);
            for (int i=0; i<currentCountRow; i++) {
                NSLog(@"<%@><Engineering><UserId:%d><Not confirm to post><Input RoomId:%@><Remark:%@><Issue:%@><Detail:%@>", date, (int)userId, txtRoom.text, remark, [arrayList objectAtIndex:i], [arrayTitle objectAtIndex:i]);
            }
        }
        //-------Log---------
    }
}

-(IBAction)btnSubmit_Clicked:(id)sender
{
    NSString *roomNo = [txtRoom text];
    if(roomNo.length > 0 && [self isValidQty]) {
        RoomAssignmentModelV2 *roomAssignment = [[RoomAssignmentModelV2 alloc] init];
        roomAssignment.roomAssignment_RoomId = roomNo;
        roomAssignment.roomAssignment_UserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
        BOOL result = [[RoomManagerV2 sharedRoomManager] getRoomAssignmentByUserIDAndRoomID:roomAssignment];
        if([[UserManagerV2 sharedUserManager].currentUserAccessRight isValidateRoom] && !isDemoMode) {
            NSString *hotelId = [NSString stringWithFormat:@"%d", [[[UserManagerV2 sharedUserManager] currentUser] userHotelsId]];
            result = [[RoomManagerV2 sharedRoomManager] isValidRoomWSByUserId:[UserManagerV2 sharedUserManager].currentUser.userId roomNumber:roomNo hotelId:hotelId];
        } else {
            result = TRUE; //Force posting mini bar without checking validation room
        }
        //Hao Tran
        //No need to check room data from main posting
        //        if(isFromMainPosting){
        //            result = TRUE;
        //        }
        
        if (result) {
            [RoomManagerV2 setCurrentRoomNo:roomNo];
            //NSString *mess = [NSString stringWithFormat:@"Room %@ \n Submit Engineering Items?", roomNo];
            //NSString *mess = [NSString stringWithFormat:@"%@ %@ \n %@%@", [L_room currentKeyToLanguage],roomNo,[L_submit_linen_items currentKeyToLanguage],@"?"];
            NSString *mess = [NSString stringWithFormat:@"%@ %@ \n %@%@ \n %@", [L_room currentKeyToLanguage],roomNo,[L_submit_engineering_items currentKeyToLanguage],@"?", [self aggregateData]];
            DTAlertView *alertView = [DTAlertView alertViewWithTitle:[L_confirm_title currentKeyToLanguage] message:mess delegate:self cancelButtonTitle:[L_YES currentKeyToLanguage] positiveButtonTitle:[L_NO currentKeyToLanguage]];
            [alertView show];
        } else {
            NSString *message = nil;
            if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                message = [NSString stringWithFormat:@"%@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOM_INVALID]];
            } else {
                message = [NSString stringWithFormat:@"\n\n\n\n\n%@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOM_INVALID]];
            }
            
            UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObject:[UIImage imageNamed:@"yes_bt.png"]] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:message delegate:nil cancelButtonTitle:[L_CLOSE currentKeyToLanguage] otherButtonTitles: nil];
            
            if(!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                UIImageView *alertImage = [[UIImageView alloc] initWithFrame:CGRectMake(80, 35, 120, 120)];
                [alertImage setImage:[UIImage imageNamed:@"icon_wrong room.png"]];
                [alert addSubview:alertImage];
            }
            [alert show];
            //-------Log----
            if([LogFileManager isLogConsole])
            {
                NSInteger currentCountRow = [arrayList count]>maxRowOfTableItem ? countRow:(countRow-1);
                NSInteger userId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
                NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
                for (int i = 0; i<currentCountRow; i++){
                    NSLog(@"<%@><Engineering><UserId:%d><Invalid room to post><input roomId:%@><remark:%@><Issue:%@><Detail:%@>", time, (int)userId, txtRoom.text, remark, [arrayList objectAtIndex:i], [arrayTitle objectAtIndex:i]);
                }
            }
        }
    } else {
        if (roomNo.length > 0) {
            NSString *message = nil;
            if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                message = [NSString stringWithFormat:@"%@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_INVALID_DATA]];
            } else {
                message = [NSString stringWithFormat:@"\n\n\n\n\n%@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_INVALID_DATA]];
            }
            
            UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObject:[UIImage imageNamed:@"yes_bt.png"]] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:message delegate:nil cancelButtonTitle:[L_CLOSE currentKeyToLanguage] otherButtonTitles: nil];
            
            if(!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                UIImageView *alertImage = [[UIImageView alloc] initWithFrame:CGRectMake(80, 35, 120, 120)];
                [alertImage setImage:[UIImage imageNamed:@"icon_wrong room.png"]];
                [alert addSubview:alertImage];
            }
            [alert show];
        } else {
            NSString *message = nil;
            if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                message = [NSString stringWithFormat:@"%@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOM_INVALID]];
            } else {
                message = [NSString stringWithFormat:@"\n\n\n\n\n%@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOM_INVALID]];
            }
            
            UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObject:[UIImage imageNamed:@"yes_bt.png"]] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:message delegate:nil cancelButtonTitle:[L_CLOSE currentKeyToLanguage] otherButtonTitles: nil];
            
            if(!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                UIImageView *alertImage = [[UIImageView alloc] initWithFrame:CGRectMake(80, 35, 120, 120)];
                [alertImage setImage:[UIImage imageNamed:@"icon_wrong room.png"]];
                [alert addSubview:alertImage];
            }
            [alert show];
            //-------Log----
            if([LogFileManager isLogConsole])
            {
                NSInteger currentCountRow = [arrayList count]>maxRowOfTableItem ? countRow:(countRow-1);
                NSInteger userId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
                NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
                for (int i = 0; i<currentCountRow; i++){
                    NSLog(@"<%@><Engineering><UserId:%d><input roomId:%@><remark:%@><Issue:%@><Detail:%@>", time, (int)userId, txtRoom.text, remark, [arrayList objectAtIndex:i], [arrayTitle objectAtIndex:i]);
                }
            }
        }
    }
}

#pragma mark - Access Right
-(void) loadAccessRights
{
    AccessRightModel *currentUserAccessRight = [[UserManagerV2 sharedUserManager] currentUserAccessRight];
    QRCodeScanner = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.QRCodeScanner];
    postingEngineering = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.postEngineering];
    actionEngineering = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.actionEngineering];
    if (![UserManagerV2 sharedUserManager].currentUserAccessRight.isShowActionHistoryPosting) {
        btnPostingHistory.hidden = YES;
    }
}

#pragma mark - QR Code Button Touched
-(IBAction)btnQRCodeScannerTouched:(id) sender
{
    ZXingWidgetController *widController = [[ZXingWidgetController alloc] initWithDelegate:self showCancel:YES OneDMode:NO showLicense:NO];
    QRCodeReader* qrcodeReader = [[QRCodeReader alloc] init];
    NSSet *readers = [[NSSet alloc ] initWithObjects:qrcodeReader,nil];
    widController.readers = readers;
    [self presentViewController:widController animated:YES completion:nil];
}

#pragma mark - ZxingDelegate
- (void)zxingController:(ZXingWidgetController*)controller didScanResult:(NSString *)resultString {
    resultString = [STEncryptorDES convertText:resultString];
    [self dismissViewControllerAnimated:YES completion:nil];
    
    txtRoom.text = resultString;
    [self textFieldShouldReturn:txtRoom];
}

- (void)zxingControllerDidCancel:(ZXingWidgetController*)controller {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - iOS 7 Resources
-(void)loadFlatResource
{
    [imgHeader setImage:[UIImage imageNamed:imgGuestInfoActiveFlat]];
    [btnSubmit setImage:[UIImage imageNamed:imgBtnSubmitFlat] forState:UIControlStateNormal];
    [btnSubmit setImage:[UIImage imageNamed:imgBtnSubmitClickedFlat] forState:UIControlStateHighlighted];
    [btnPostingHistory setBackgroundImage:[UIImage imageNamed:imgHistoryPostingBtnFlat] forState:UIControlStateHighlighted];
    [btnPostingHistory setBackgroundImage:[UIImage imageNamed:imgHistoryPostingBtnFlat] forState:UIControlStateNormal];
}
-(void) setEnableBtnAtionHistory:(BOOL) isEnable
{
    if(![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]){
        isEnable = NO;
    }
    [btnPostingHistory setUserInteractionEnabled:isEnable];
    [btnPostingHistory setBackgroundImage:[UIImage imageBeforeiOS7:(isEnable?imgCleaningStatusBtnAndActionBtn:imgCleaningStatusBtnAndActionBtnDisable)
                                                         equaliOS7:(isEnable?imgCleaningStatusBtnAndActionBtnFlat:imgCleaningStatusBtnAndActionBtnDisableFlat)]
                                 forState:UIControlStateNormal];
    [btnPostingHistory setBackgroundImage:[UIImage imageBeforeiOS7:(isEnable?imgCleaningStatusBtnAndActionBtnClicked:imgCleaningStatusBtnAndActionBtnDisable)
                                                         equaliOS7:(isEnable?imgCleaningStatusBtnAndActionBtnClickedFlat:imgCleaningStatusBtnAndActionBtnDisableFlat)]
                                 forState:UIControlEventTouchDown];
    [btnPostingHistory setTitle:[L_action_history currentKeyToLanguage] forState:UIControlStateNormal];
}
@end
