//
//  customCellEngineer.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface customCellEngineer : UITableViewCell{
    IBOutlet UILabel *issue;
    IBOutlet UILabel *detail;
    IBOutlet UIButton *next;
    IBOutlet UILabel *remark;
    IBOutlet UILabel *remarkValue;
}

@property (nonatomic, strong) UILabel *issue;
@property (nonatomic, strong) UILabel *detail;
@property (nonatomic, strong) UIButton *next;
@property (nonatomic, strong) UILabel *remark;
@property (nonatomic, strong) UILabel *remarkValue;

@end
