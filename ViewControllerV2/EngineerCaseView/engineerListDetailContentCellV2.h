//
//  engineerListDetailContentCellV2.h
//  mHouseKeeping
//
//  Created by TMS on 2/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol engineerListDetailContentCellV2Delegate <NSObject>

@optional
-(void) btnDetailContentRatingPressedWithIndexPath:(NSIndexPath *)indexpath AndSender:(UIButton *) sender;

@end

@interface engineerListDetailContentCellV2: UITableViewCell {
    UILabel *ContentName;
    BOOL isChecked;
    NSIndexPath *indexpath;
    __unsafe_unretained id<engineerListDetailContentCellV2Delegate> delegate;
}

@property (nonatomic, assign)  id<engineerListDetailContentCellV2Delegate> delegate;
@property (nonatomic, strong) NSIndexPath *indexpath;
@property (nonatomic, readwrite) BOOL isChecked;
@property (retain, nonatomic) IBOutlet UIButton *btnDetailContentCheck;
@property (nonatomic, strong) IBOutlet UILabel *ContentName;

- (IBAction)btnRatingPressed:(id)sender;
- (IBAction)btnCheckPressed:(id)sender;
-(void) setCheckStatus:(BOOL)checkStatus;

@end
