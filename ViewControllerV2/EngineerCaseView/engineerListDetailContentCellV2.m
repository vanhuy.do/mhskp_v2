//
//  engineerListDetailContentCellV2.m
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "engineerListDetailContentCellV2.h"
#import "UIImage+Tint.h"
#import "ehkDefinesV2.h"

@implementation engineerListDetailContentCellV2

@synthesize btnDetailContentCheck, ContentName;
@synthesize isChecked;
@synthesize indexpath;
@synthesize delegate;

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void)dealloc{
//    [btnDetailContentCheck release];
//    [ContentName release];
//    [imgIcon release];
}

- (IBAction)btnRatingPressed:(id)sender {
    if ([delegate respondsToSelector:@selector(btnDetailContentRatingPressedWithIndexPath:AndSender:)]) {
        [delegate btnDetailContentRatingPressedWithIndexPath:indexpath AndSender:sender];
    }
}

- (IBAction)btnCheckPressed:(id)sender {
    if(isChecked == NO) {
        isChecked = YES;
        [btnDetailContentCheck setImage:[UIImage imageBeforeiOS7:imgChecked equaliOS7:imgCheckedFlat] forState:UIControlStateNormal];
    } else {
        isChecked = NO;
        [btnDetailContentCheck setImage:[UIImage imageBeforeiOS7:imgUnCheck equaliOS7:imgUnCheckFlat] forState:UIControlStateNormal];
    }
}

-(void)setCheckStatus:(BOOL)checkStatus {
    isChecked = checkStatus;
    if(isChecked == YES) {
        [btnDetailContentCheck setImage:[UIImage imageBeforeiOS7:imgChecked equaliOS7:imgCheckedFlat] forState:UIControlStateNormal];
    } else {
        [btnDetailContentCheck setImage:[UIImage imageBeforeiOS7:imgUnCheck equaliOS7:imgUnCheckFlat] forState:UIControlStateNormal];
    }
}

@end
