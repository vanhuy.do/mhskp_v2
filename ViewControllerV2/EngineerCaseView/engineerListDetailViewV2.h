//
//  engineerListDetailViewV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CheckListViewV2.h"
#import "CheckListTypeModelV2.h"
#import "CheckListDetailContentModelV2.h"
#import "EngineerSectionV2.h"
#import "SectionCheckListContentTypeViewV2.h"
#import "CheckListDetailContentCellV2.h"
#import "PickerViewV2.h"
#import "engineerListDetailContentCellV2.h"

@protocol engineerListDetailViewV2delegate <NSObject>

@optional
-(void)didSeclectRow:(NSString *)categoryName itemName:(NSString*)item index:(NSInteger)_index category_id:(NSInteger)_category_id item_id:(NSInteger)_item_id;

@end

@interface engineerListDetailViewV2 : UIViewController <UITableViewDataSource, UITableViewDelegate, SectionCheckListContentTypeViewV2Delegate, PickerViewV2Delegate, CheckListDetailContentCellV2Delegate, UISearchBarDelegate, engineerSectionCheckListContentTypeViewV2Delegate, engineerListDetailContentCellV2Delegate> {
    CheckListTypeModelV2 *chkListTypeV2;
    IBOutlet engineerListDetailContentCellV2 * engineerCell;
    NSMutableArray *sectionInfoArray;
    NSMutableArray *arrayEngineer;
    NSMutableArray * listItem;
    NSMutableArray *arrayAirConDetail;
    IBOutlet UISearchBar *search;
//    IBOutlet UILabel *lbltitle;
    IBOutlet UIImageView *imageview;
    UIView *backgroundSearchBar;
    __unsafe_unretained id<engineerListDetailViewV2delegate> delegate;
    BOOL isListDown;
    BOOL statusCheckSaveData;
    BOOL statusChooseSave;
    
     UIButton *hideSearchBtn;
}

@property (nonatomic,strong)UIButton *hideSearchBtn;
@property (nonatomic, assign) id<engineerListDetailViewV2delegate> delegate;
@property (nonatomic, strong) IBOutlet engineerListDetailContentCellV2 * engineerCell;
@property (strong, nonatomic) IBOutlet UITableView *tbvchkDetail;
@property (nonatomic, strong) NSMutableArray *sectionInfoArray;
@property (nonatomic, strong) NSMutableArray *arrayEngineer;
@property (nonatomic, strong) NSMutableArray *arrayAirConDetail;
@property (nonatomic, strong) NSMutableArray *listItem;
@property (strong, nonatomic) IBOutlet UIImageView *imgType;
@property (strong, nonatomic) IBOutlet UILabel *lblContentName;
@property (nonatomic, strong) CheckListTypeModelV2 *chkListTypeV2;
@property (nonatomic, strong) IBOutlet UISearchBar *search;
//@property (nonatomic, strong) IBOutlet UILabel *lbltitle;
@property (nonatomic, strong) IBOutlet UIImageView *imageview;
@property (nonatomic, strong) UIView *backgroundSearchBar;
@property BOOL isListDown;
@property (strong, nonatomic) NSString *msgbtnYes;
@property (strong, nonatomic) NSString *msgbtnNo;
@property (strong, nonatomic) NSString *msgSaveCount;
@property (strong, nonatomic) NSString *msgDiscardCount;

-(void)loadDataFromDatabase;
-(NSMutableArray *)loadAllEngineerItem;
-(void)setCaptionView;

@end
