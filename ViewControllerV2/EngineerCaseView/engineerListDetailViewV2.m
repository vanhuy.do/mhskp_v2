//
//  engineerListDetailViewV2.m
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "engineerListDetailViewV2.h"
#import "engineerListDetailContentCellV2.h"
#import "engineerSectionCheckListContentTypeViewV2.h"
#import "LanguageManager.h"
#import "EngineerCaseViewManagerV2.h"
#import "EngineeringItemModelV2.h"
#import "EngineeringCategoriesModeV2.h"
#import "CustomAlertViewV2.h"
#import "MyNavigationBarV2.h"
#import "LogFileManager.h"
#import "DeviceManager.h"

#define tagTopbarView 1234
#define tagAlertBack 10
#define tagAlertNo 11

@interface engineerListDetailViewV2(PrivateMethods)
-(void) loadTopbarView;
-(void) showTopbarView;
-(void) hideTopbarView;
-(void) setHiddenTopbarView;
-(void) adjustShowForViews;
-(void) adjustRemoveForViews;
-(void) addButtonHandleShowHideTopbar;
-(void) topbarTapped:(UIButton *) sender;
-(TopbarViewV2 *) getTopBarView;
@end

@implementation engineerListDetailViewV2
@synthesize imgType;
@synthesize lblContentName, search, imageview, delegate;
@synthesize chkListTypeV2;
@synthesize tbvchkDetail, engineerCell;
@synthesize sectionInfoArray, arrayEngineer, arrayAirConDetail, backgroundSearchBar, listItem, isListDown;
@synthesize msgbtnNo, msgbtnYes, msgSaveCount, msgDiscardCount;
@synthesize hideSearchBtn;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    
    return self;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle
- (void)viewDidLoad {
    //---------Log-------
    if([LogFileManager isLogConsole])
    {
        NSInteger userId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
        NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSLog(@"<%@><EngiListDetail><UserId:%d>", time, (int)userId);
    }
    //---------Log-------
    
    [self setCaptionView];
    
    [self loadDataFromDatabase];
    
    [imageview setBackgroundColor:[UIColor grayColor]];
//    [lbltitle setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_the_top_item]];
    
    NSArray *subviews = search.subviews;
    for (id subview in subviews) {
        if ([subview isKindOfClass:NSClassFromString(@"UISearchBarBackground")]) {
            UIView *sview = (UIView *) subview;
            UIView *bg = [[UIView alloc] initWithFrame:sview.frame];
            [bg setFrame:CGRectMake(sview.frame.origin.x, sview.frame.origin.y, 320, 44)];
//              bg.backgroundColor = [UIColor colorWithRed:254.0/255.0 green:248/255.0 blue:215/255.0 alpha:1.0];
            bg.backgroundColor = [UIColor grayColor];
            [self.search insertSubview:bg aboveSubview:sview];
            self.backgroundSearchBar= bg;
//            [bg release];
            [sview removeFromSuperview];
        }
    }
    
    search.placeholder = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_SEARCH];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didSeclectRow1:) name:@"A" object:nil];
    
    [super viewDidLoad];
    //Resize content views of current VC to fit with Navigation bar and tabbar
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    // Do any additional setup after loading the view from its nib.
    if(chkListTypeV2 != nil) {
        if (chkListTypeV2.chkType == tCheckmark) {
            [imgType setImage:[UIImage imageNamed:imgCheckMark]];
        } else {
            [imgType setImage:[UIImage imageNamed:imgRating]];
        }
        
        [lblContentName setText:chkListTypeV2.chkTypeName];
    }
    
    [tbvchkDetail setSeparatorColor:[UIColor clearColor]];    
    [tbvchkDetail setBackgroundView:nil];
    [tbvchkDetail setBackgroundColor:[UIColor clearColor]];
    [tbvchkDetail setOpaque:YES];
    
    [self setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_CHECKLIST]];
        
    [self performSelector:@selector(loadTopbarView)];
}

-(void)exitSearchBtn:(id)sender
{
//    isListDown=NO;
    isListDown = YES;
    
    [search setShowsCancelButton:NO animated:NO];
    [search resignFirstResponder];
    
    if (hideSearchBtn) {
        [hideSearchBtn removeFromSuperview];
    }
    //    if ([sectionDataSource count]>0) {
    //        return;    
    //    }else
    //    {
    //        [sectionDataSource removeAllObjects];
    //        
    //        [sectionDataSource addObjectsFromArray:sectionInfoArray];
    //        
    //        @try{
    //            
    //            [subjectTableView reloadData];
    //            
    //        }
    //        
    //        @catch(NSException *e){
    //            
    //        }
    //
    //    }
    
}

- (void)viewDidUnload {
    [self setLblContentName:nil];
    [self setImgType:nil];
    [self setTbvchkDetail:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc {
//    [sectionInfoArray release];
//    [chkListTypeV2 release];
//    [lblContentName release];
//    [imgType release];
//    [tbvchkDetail release];
//    [arrayEngineer release];
//    [arrayAirConDetail release];
//    [listItem release];
//    [super dealloc];
}

-(void)didSeclectRow1:(NSString *)temp{
    
}

#pragma mark - UITableView Datasource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [listItem count];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    EngineerSectionV2 *sectioninfo = [listItem objectAtIndex:section];
    NSInteger numberRowInSection = sectioninfo.rowHeights.count;

    if (isListDown) {
        return numberRowInSection;
    }
    
    return sectioninfo.open ? numberRowInSection : 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if(isListDown) {
        return  0;
    } else {
        return 55;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 55;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    EngineerSectionV2 *sectioninfo = [sectionInfoArray objectAtIndex:section];
//    NSString *arrayImage=[arrayEngineer objectAtIndex:section];
    
    if (sectioninfo.headerView == nil) {
        engineerSectionCheckListContentTypeViewV2 *sview = [[engineerSectionCheckListContentTypeViewV2 alloc] initWithSection:section contentName: [[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString:[NSString stringWithFormat:@"%@",ENGLISH_LANGUAGE]]? sectioninfo.chkContentType.enca_name:sectioninfo.chkContentType.enca_lang_name imageName:sectioninfo.chkContentType.enca_image AndStatusArrow:sectioninfo.open];

        sectioninfo.headerView = sview;
        sview.delegate = self;
    }    
    
    return sectioninfo.headerView;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifyCell = @"CellIndetify";
    engineerListDetailContentCellV2 *cell = nil;
    
    cell = (engineerListDetailContentCellV2 *)[tableView dequeueReusableCellWithIdentifier:identifyCell];
    
    if (cell == nil) {
        NSArray *array = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([engineerListDetailContentCellV2 class]) owner:self options:nil];
        cell = [array objectAtIndex:0];
    }
    
    EngineerSectionV2 *sectionInfo;
   
    if(isListDown) {
        sectionInfo = [listItem objectAtIndex:0];
    } else {
        sectionInfo = [listItem objectAtIndex:indexPath.section];
    }
    
    EngineeringItemModelV2 *model = [sectionInfo.rowHeights objectAtIndex:indexPath.row];
//#warning remove image for conrad version
//    if(sectionInfo.chkContentType != nil && sectionInfo.chkContentType.enca_image != nil){
//        [cell.imgIcon setImage:[UIImage imageWithData: sectionInfo.chkContentType.enca_image]];
//    }
//#warning end
    
    if ([[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString:(NSString *)ENGLISH_LANGUAGE]) {
        cell.ContentName.text = model.eni_name;
    } else{
        cell.ContentName.text = model.eni_name_lang;
    }

    
    [cell setDelegate:self];
    [cell setIndexpath:indexPath];  
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell setBackgroundColor:[UIColor clearColor]];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    EngineerSectionV2 *sectionInfo;

    if(isListDown) {        
        sectionInfo = [listItem objectAtIndex:indexPath.section];
        EngineeringItemModelV2 *model = [sectionInfo.rowHeights objectAtIndex:indexPath.row];
        
        EngineerSectionV2 *sectionToFind = nil;
        
        for (EngineerSectionV2 *currentSection in sectionInfoArray) {
            for (EngineeringItemModelV2 *currentEnginerring in currentSection.rowHeights) {
                if (model == currentEnginerring) {
                    sectionToFind = currentSection;
                    break;
                }
            }
            if (sectionToFind != nil) {
                break;
            }
        }
        
        if([delegate respondsToSelector:@selector(didSeclectRow:itemName:index:category_id:item_id:)]) {
            if([LogFileManager isLogConsole])
            {
                NSLog(@"%@",sectionToFind.chkContentType.enca_name);
            }
//            [delegate didSeclectRow:sectionInfo.chkContentType.enca_name itemName:model.eni_name index:indexPath.row category_id:model.eni_category_id item_id:model.eni_id];
            if([[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString:[NSString stringWithFormat:@"%@",ENGLISH_LANGUAGE]]) {
                [delegate didSeclectRow:sectionToFind.chkContentType.enca_name itemName:model.eni_name index:indexPath.row category_id:model.eni_category_id item_id:model.eni_id];
            } else {
                [delegate didSeclectRow:sectionToFind.chkContentType.enca_lang_name itemName:model.eni_name_lang index:indexPath.row category_id:model.eni_category_id item_id:model.eni_id];
            }
        }
    }
    else {
        sectionInfo = [sectionInfoArray objectAtIndex:indexPath.section];
        EngineeringItemModelV2 *model = [sectionInfo.rowHeights objectAtIndex:indexPath.row];
        
        if([delegate respondsToSelector:@selector(didSeclectRow:itemName:index:category_id:item_id:)]) {
            if([[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString:[NSString stringWithFormat:@"%@",ENGLISH_LANGUAGE]]) {
                [delegate didSeclectRow:sectionInfo.chkContentType.enca_name itemName:model.eni_name index:indexPath.row category_id:model.eni_category_id item_id:model.eni_id];
            } else {
                [delegate didSeclectRow:sectionInfo.chkContentType.enca_lang_name itemName:model.eni_name_lang index:indexPath.row category_id:model.eni_category_id item_id:model.eni_id];
            }
        }
    }
    
    [self.navigationController popViewControllerAnimated:YES];  
}

#pragma mark - Section Header Delegate
-(void)sectionHeaderView:(SectionCheckListContentTypeViewV2 *)sectionheaderView sectionOpened:(NSInteger)section {
    EngineerSectionV2 *sectionInfo = [sectionInfoArray objectAtIndex:section];
    sectionInfo.open = YES;
    
    NSInteger countOfRowsToInsert = [sectionInfo.rowHeights count];
    NSMutableArray *indexPathsToInsert = [[NSMutableArray alloc] init];
    
    for (NSInteger i = 0; i < countOfRowsToInsert; i++) {
        [indexPathsToInsert addObject:[NSIndexPath indexPathForRow:i inSection:section]];
//        NSLog(@"indexpath: %d %d", i, section);
    }
    
    UITableViewRowAnimation insertAnimation;
    insertAnimation = UITableViewRowAnimationTop;
    
    [self.tbvchkDetail beginUpdates];
    [self.tbvchkDetail insertRowsAtIndexPaths:indexPathsToInsert withRowAnimation:insertAnimation];
    [self.tbvchkDetail endUpdates];
    
    if([indexPathsToInsert count] > 0)
    {
        [self.tbvchkDetail scrollToRowAtIndexPath:[indexPathsToInsert objectAtIndex:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    
    //-----------------Log---------------
    if([LogFileManager isLogConsole])
    {
        NSInteger userId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
        NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSLog(@"<%@><EngiListDetail><UserId:%d><click section index to open:%d><Issue:%@>", time, (int)userId, (int)section, sectionInfo);
    }
    //-----------------Log---------------
    
//    [indexPathsToInsert release];
}

-(void)sectionHeaderView:(SectionCheckListContentTypeViewV2 *)sectionheaderView sectionClosed:(NSInteger)section {
    EngineerSectionV2 *sectionInfo = [sectionInfoArray
                                      objectAtIndex:section];    
    sectionInfo.open = NO;
    
    NSInteger countOfRowsToDelete = [self.tbvchkDetail
                                     numberOfRowsInSection:section];
    
    if (countOfRowsToDelete > 0) {
        NSMutableArray *indexPathsToDelete = [[NSMutableArray alloc] init];
    
        for (NSInteger i = 0; i < countOfRowsToDelete; i++) {
            [indexPathsToDelete addObject:[NSIndexPath indexPathForRow:i
                                                             inSection:section]];
        }
        
        [tbvchkDetail beginUpdates];
        [self.tbvchkDetail deleteRowsAtIndexPaths:indexPathsToDelete
                                 withRowAnimation:UITableViewRowAnimationTop];
        [tbvchkDetail endUpdates];
        
//        [indexPathsToDelete release];
    }
    //-----------------Log---------------
    if([LogFileManager isLogConsole])
    {
        NSInteger userId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
        NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSLog(@"<%@><EngiListDetail><UserId:%d><click section index to close:%d>", time, (int)userId, (int)section);
    }
    //-----------------Log---------------
}

-(void) checkSaveData {    
    statusCheckSaveData = YES;
    
    UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:msgSaveCount delegate:self cancelButtonTitle:msgbtnYes otherButtonTitles:msgbtnNo, nil]; 
    alert.delegate = self;
    alert.tag = tagAlertBack;
    [alert show];
}

#pragma mark - CheckListDetailViewV2 Delegate Methods
-(void)btnDetailContentRatingPressedWithIndexPath:(NSIndexPath *)indexpath AndSender:(UIButton *)sender {
    PickerViewV2 *picker = [[PickerViewV2 alloc] initPickerViewV2WithDatas:[NSArray arrayWithObjects:@"0", @"1", @"2", @"3", @"4", @"5", nil] AndSelectedData:sender.titleLabel.text AndIndex:indexpath];
    picker.delegate = self;
    [picker showPickerViewV2];   
}

#pragma mark - PickerViewV2 Delegate Methods
-(void)didChoosePickerViewV2WithData:(NSString *)data AndIndex:(NSIndexPath *)index {
    EngineerSectionV2 *sectionInfo = [sectionInfoArray objectAtIndex:index.section];
    CheckListDetailContentModelV2 *model = [sectionInfo.rowHeights objectAtIndex:index.row];
    CheckListItemCoreDBModelV2 *chkItemCore = model.itemCoreModel;
    chkItemCore.chkItemCore = [data integerValue];
    
    [tbvchkDetail reloadRowsAtIndexPaths:[NSArray arrayWithObject:index] withRowAnimation:UITableViewRowAnimationFade];
}

#pragma mark - Load Data
-(void)loadDataFromDatabase {
    [[HomeViewV2 shareHomeView] waitingLoadingData];
    
    listItem = [[NSMutableArray alloc] init];
    sectionInfoArray = [[NSMutableArray alloc] init];

    // load category 
    NSMutableArray *categoryArray = [[EngineerCaseViewManagerV2 sharedEngineerCaseViewManagerV2] loadAllEngineeringCategoriesModeV2];
    for (int i = 0; i < [categoryArray count]; i++) {
        EngineeringCategoriesModeV2 *categoryObj = [categoryArray objectAtIndex:i];
        NSMutableArray *itemArray = [[EngineerCaseViewManagerV2 sharedEngineerCaseViewManagerV2] loadAllEngineeringItemID:(int)categoryObj.enca_id];
        
        EngineerSectionV2 *section1 = [[EngineerSectionV2 alloc] init];
        section1.chkContentType = categoryObj;
        
        for (int j = 0; j < [itemArray count]; j++) {
            EngineeringItemModelV2 *itemObj = [itemArray objectAtIndex:j];
            [section1 insertObjectToNextIndex:itemObj];
        }
        
        [sectionInfoArray addObject:section1];
    }
    
    [listItem addObjectsFromArray:sectionInfoArray];
    
    [tbvchkDetail reloadData];
    
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
}

-(NSMutableArray *)loadAllEngineerItem {
    NSMutableArray *arrayItem = [NSMutableArray array];
    sectionInfoArray = [[NSMutableArray alloc] init];

    // load category 
    NSMutableArray *categoryArray = [[EngineerCaseViewManagerV2 sharedEngineerCaseViewManagerV2] loadAllEngineeringCategoriesModeV2];
    
    for (int i = 0; i < [categoryArray count]; i++) {
        EngineeringCategoriesModeV2 *categoryObj = [categoryArray objectAtIndex:i];
        NSMutableArray *itemArray = [[EngineerCaseViewManagerV2 sharedEngineerCaseViewManagerV2] loadAllEngineeringItemID:
                                     (int)categoryObj.enca_id];
        for (int j = 0; j < [itemArray count]; j++) {
            EngineeringItemModelV2 *itemObj = [itemArray objectAtIndex:j];
            [arrayItem addObject:itemObj];           
        }        
    }     

    return arrayItem;
}

 #pragma mark - uisearchbar Methods
- (void)searchBar:(UISearchBar *)theSearchBar textDidChange:(NSString *)searchText {        
    [listItem removeAllObjects];// remove all data that belongs to previous search
    
    if(searchText.length == 0) {
        
        [listItem addObjectsFromArray:sectionInfoArray];
        isListDown = NO;
        
        @try{            
            [tbvchkDetail reloadData];
        }    
        @catch(NSException *e){
            
        }
        
//        [m_searchBar resignFirstResponder];
//        
//        m_searchBar.text = @"";
//        
//        [subjectTableView reloadData];
//        
//        return;
    } 
    else {
        isListDown = YES;
        NSInteger counter = 0;
        
        EngineerSectionV2 *msSubjectResutl = [[EngineerSectionV2 alloc] init];
        for (EngineerSectionV2 *msSubjectSection in sectionInfoArray) {
            NSMutableArray *itemArray=msSubjectSection.rowHeights;
            
            for( EngineeringItemModelV2 *subjectItem in itemArray) {                
                //NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];
                
                NSRange r = [[subjectItem.eni_name lowercaseString] rangeOfString:[searchText lowercaseString]];
                
                if(r.location != NSNotFound) {                    
                    if(r.location== 0) {
                        [msSubjectResutl insertObjectToNextIndex:subjectItem];
                    }
                }
                
                counter++;
                
//                [pool release];
            }            
        }
        
        [listItem addObject:msSubjectResutl];
        [tbvchkDetail reloadData];
    }
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [search resignFirstResponder];   
    
}

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    // only show the status bars cancel button while in edit mode
    self.hideSearchBtn=[[UIButton alloc] initWithFrame:CGRectMake(0, 0, 320,340)];
    [self.hideSearchBtn addTarget:self action:@selector(exitSearchBtn:) forControlEvents:UIControlEventTouchUpInside];
    [self.hideSearchBtn setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:hideSearchBtn];
    
//    isListDown=YES;
    search.showsCancelButton = NO;
    search.autocorrectionType = UITextAutocorrectionTypeNo;
    
    // flush the previous search content
    
    //    [sectionDataSource removeAllObjects];
    
    
}

-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    if(searchBar.text.length == 0)
    {
        isListDown = NO;
        [tbvchkDetail reloadData];
    }
}

#pragma mark - Handle Topbar Methods
-(void)loadTopbarView {
    TopbarViewV2 *topbar = [[TopbarViewV2 alloc] initViewWithSuperModel:[[CommonVariable sharedCommonVariable] superRoomModel]];
    [topbar setTag:tagTopbarView];
    
    [self.view addSubview:topbar];
    [topbar setHidden:YES];
    [topbar setAlpha:0.0];
    [self addButtonHandleShowHideTopbar];
}

-(void)showTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationWillStartSelector:@selector(setHiddenTopbarView)];
    
    f = topbar.frame;
    f.origin.y = 0;
    [topbar setFrame:f];
    [topbar setAlpha:1.0];
    
    [self adjustShowForViews];
    
    [UIView commitAnimations];
}

-(void)hideTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDidStopSelector:@selector(setHiddenTopbarView)];
    [UIView setAnimationDelegate:self];
    
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    [topbar setAlpha:0.0];
    
    [self adjustRemoveForViews];
    
    [UIView commitAnimations];
}

-(void)setHiddenTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    [topbar setHidden:!topbar.hidden];
}

-(TopbarViewV2 *)getTopBarView {
    return (TopbarViewV2 *)[self.view viewWithTag:tagTopbarView];
}

-(void)addButtonHandleShowHideTopbar {
    
    UIView *vBarButton = [[UIView alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
    UIButton *titleBarButtonFirst = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 200, 30)];
    
    titleBarButtonFirst.titleLabel.backgroundColor = [UIColor clearColor];
    [titleBarButtonFirst.titleLabel setFont: FONT_BUTTON_TOPBAR];
    titleBarButtonFirst.titleLabel.textAlignment = NSTextAlignmentCenter;
    titleBarButtonFirst.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
    
    [titleBarButtonFirst.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
    [titleBarButtonFirst.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
    [titleBarButtonFirst addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    [titleBarButtonFirst setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_Counts] forState:UIControlStateNormal];
    
    [vBarButton addSubview:titleBarButtonFirst];
    
    
    UIButton *titleBarButtonSecond = [[UIButton alloc] initWithFrame:CGRectMake(0, 28, 200, 22)];
    [titleBarButtonSecond setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [titleBarButtonSecond.titleLabel setFont:FONT_BUTTON_TOPBAR_SECOND];
    [titleBarButtonSecond setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ENGINEERING_CASE] forState:UIControlStateNormal];//Set Title for Tabbar
    [titleBarButtonSecond addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    [vBarButton addSubview:titleBarButtonSecond];
    
    self.navigationItem.titleView = vBarButton;

    //back button navigation
    self.navigationItem.leftBarButtonItem = [MyNavigationBarV2 createBackButtonWithTarget:self selector:@selector(btnBackPressed) forControlEvents:UIControlEventTouchUpInside];
    
    //Move title to center vertical
    CGRect titleViewBounds = self.navigationItem.titleView.bounds;
    int moveLeftTitle = 0;
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        moveLeftTitle = 10;
    }
    self.navigationItem.titleView.bounds = CGRectMake(titleViewBounds.origin.x + moveLeftTitle, Y_TOPBAR_TITLE, titleViewBounds.size.width, titleViewBounds.size.height);
}

-(void)btnBackPressed
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)topbarTapped:(UIButton *)sender {
    TopbarViewV2 *topbar = [self getTopBarView];
    [sender setEnabled:NO];
    if (topbar.isHidden) {
        [self showTopbarView];
    } else {
        [self hideTopbarView];
    }
    [self performSelector:@selector(topbarButtonEnable:) withObject:sender afterDelay:0.5];
}

-(void) topbarButtonEnable:(UIButton *) topbarBtn {
    [topbarBtn setEnabled:YES];
}

-(void)adjustShowForViews {
    //adjust all views when show 
//    TopbarViewV2 *topbar = [self getTopBarView];
//    CGRect f = topbar.frame;
    
    [search setFrame:CGRectMake(0,50, 320, 44)];
    [imageview setFrame:CGRectMake(0,50, 320, 44)];
//    [self.lbltitle setFrame:CGRectMake(12, 50, 120,21)];
    [tbvchkDetail setFrame:CGRectMake(0,93, 320, 314)];
}

-(void)adjustRemoveForViews { 
    //adjust all views when hide
    [search setFrame:CGRectMake(0,0, 320, 44)];
    [imageview setFrame:CGRectMake(0,0, 320, 44)];
//    [self.lbltitle setFrame:CGRectMake(12, 11, 120, 21)];
    [tbvchkDetail setFrame:CGRectMake(0, 43, 320, 344)];
}

#pragma mark - UIAlertView Delegate Methods
-(void)alertView:(UIAlertView *)alertView 
    didDismissWithButtonIndex:(NSInteger)buttonIndex {
    switch (alertView.tag) {
        case tagAlertBack: {
            switch (buttonIndex) {
                case 0: {
                    //YES: save minibar
//                    [self saveEngineering];
                    statusChooseSave = YES;
                    if (statusCheckSaveData == YES) {
                        [CommonVariable sharedCommonVariable].isSaveSuccessfull = true;
                    }
                }
                    break;
                    
                case 1: {
                    //NO: show popup discard
                    UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:msgDiscardCount delegate:self cancelButtonTitle:msgbtnYes otherButtonTitles:msgbtnNo, nil]; 
                    alert.delegate = self;
                    alert.tag = tagAlertNo;
                    [alert show];
                }
                    break;
                    
                default:
                    break;
            }
        }
            break;
            
        case tagAlertNo: {
            switch (buttonIndex) {
                case 0: {
                    //YES: back to count menu
                    if (statusCheckSaveData == YES) {
                        [CommonVariable sharedCommonVariable].isSaveSuccessfull = true;
                    } else {
                        [self.navigationController popViewControllerAnimated:YES];
                    }                    
                }
                    break;
                    
                case 1: {
                    if (statusCheckSaveData == YES) {
                        [CommonVariable sharedCommonVariable].isSaveSuccessfull = true;
                        [CommonVariable sharedCommonVariable].isNotDiscardAllChange = true;
                    }                    
                }
                    break;
                    
                default:
                    break;
            }            
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - set caption view
-(void)setCaptionView {    
    msgbtnNo = [[LanguageManagerV2 sharedLanguageManager] getNo];
    msgbtnYes = [[LanguageManagerV2 sharedLanguageManager] getYes];
    msgSaveCount = [[LanguageManagerV2 sharedLanguageManager] getMsgAlertSaveCounts];
    msgDiscardCount = [[LanguageManagerV2 sharedLanguageManager] getMsgAlertDiscardCounts];
}

@end
