//
//  engineerSectionCheckListContentTypeViewV2.h
//  mHouseKeeping
//
//  Created by TMS on 2/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol engineerSectionCheckListContentTypeViewV2Delegate;

@interface engineerSectionCheckListContentTypeViewV2 : UIView {
    UILabel *lblContentName;
    UILabel *lblContentDescription;
    UIImageView *imgArrowSection;
    UIImageView *imageIcon;
    __unsafe_unretained id<engineerSectionCheckListContentTypeViewV2Delegate> delegate;
    UIButton *btnToggle;
    BOOL isToggle;
    NSInteger section;
}

@property (nonatomic, assign) BOOL isToggle;
@property (nonatomic, assign) NSInteger section;
@property (nonatomic, strong) UILabel *lblContentName;
@property (nonatomic, strong) UILabel *lblContentDescription;
@property (nonatomic, strong) UIImageView *imageIcon;
@property (nonatomic, strong) UIImageView *imgArrowSection;
@property (nonatomic, assign) id<engineerSectionCheckListContentTypeViewV2Delegate> 
delegate;

-(id) initWithSection:(NSInteger) sectionIndex contentName:(NSString *)content imageName:(NSData *)image AndStatusArrow:(BOOL)isOpened;
-(void)toggleOpenWithUserAction:(BOOL) userAction;

@end

@protocol engineerSectionCheckListContentTypeViewV2Delegate <NSObject>
@optional
-(void) sectionHeaderView:(engineerSectionCheckListContentTypeViewV2*) sectionheaderView sectionOpened:(NSInteger) section;
-(void) sectionHeaderView:(engineerSectionCheckListContentTypeViewV2*) sectionheaderView sectionClosed:(NSInteger) section;

@end
