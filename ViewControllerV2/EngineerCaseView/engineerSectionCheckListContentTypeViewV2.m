//
//  engineerSectionCheckListContentTypeViewV2.m
//  mHouseKeeping
//
//  Created by TMS on 2/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "engineerSectionCheckListContentTypeViewV2.h"
#import "ehkDefinesV2.h"
#import "UIImage+Tint.h"

@interface engineerSectionCheckListContentTypeViewV2 (PrivateMethods)

-(void) toggleOpen:(id)sender;

@end

@implementation engineerSectionCheckListContentTypeViewV2

@synthesize lblContentName,lblContentDescription,imgArrowSection,delegate, section, isToggle,imageIcon;


-(id)init {
    self = [super init];
    if (self) {
        [self setFrame:CGRectMake(0, 0, 320, 65)];
        lblContentName = [[UILabel alloc] initWithFrame:CGRectMake(150, 4, 196, 39)];
        [lblContentName setBackgroundColor:[UIColor clearColor]];
        [lblContentName setTextColor:[UIColor colorWithRed:0/255.0 green:0/255.0 
                                                      blue:0/255.0 alpha:1]];
        [lblContentName setFont:[UIFont fontWithName:@"Arial-BoldMT" size:18]];
        [self setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:imgBackgroundCategory]]];
        imageIcon = [[UIImageView alloc] initWithFrame:CGRectMake(5, 4, 80, 50)];
             
        if(isToggle == YES) {
            imgArrowSection = [[UIImageView alloc]initWithImage:[UIImage imageBeforeiOS7:imgRightRowOpen equaliOS7:imgRightRowOpenFlat]];
        } else {
            imgArrowSection = [[UIImageView alloc]initWithImage:[UIImage imageBeforeiOS7:imgRightRow equaliOS7:imgRightRowFlat]];
        }
        
        [imgArrowSection setFrame:CGRectMake(288, 12, 25, 25)];
        btnToggle = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnToggle setFrame:CGRectMake(0, 0, 320, 50)];
        [btnToggle addTarget:self action:@selector(toggleOpen:) forControlEvents:UIControlEventTouchUpInside];
        
        [self addSubview:lblContentName];
        [self addSubview:imgArrowSection];
        [self addSubview:btnToggle];
        [self addSubview:imageIcon];
    }
    
    return self;
}

-(id)initWithSection:(NSInteger)sectionIndex contentName:(NSString *)content imageName:(NSData *)image AndStatusArrow:(BOOL)isOpened {
    self = [self init];
    
    if (self) {
        section = sectionIndex;
        lblContentName.text = content;
        UIImage *img = [UIImage imageNamed:@"no_image_2.png"];
        if (image) {
            img = [UIImage imageWithData:image];
        }
        [imageIcon setImage:img];
        isToggle = isOpened;
    }
    
    return self;
}

- (void)dealloc {
//    [lblContentName release];
//    [imgArrowSection release];
//    [super dealloc];
}

#pragma mark - View lifecycle
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void)toggleOpen:(id)sender {
    [self toggleOpenWithUserAction:YES];
}

-(void)toggleOpenWithUserAction:(BOOL)userAction {
    if (userAction) {
        if (isToggle == NO) {
            if ([delegate respondsToSelector:@selector(sectionHeaderView:sectionOpened:)]) {
                isToggle = YES;
                [delegate sectionHeaderView:self sectionOpened:section];
                
                [imgArrowSection setImage:[UIImage imageBeforeiOS7:imgRightRowOpen equaliOS7:imgRightRowOpenFlat]];
            }
        } else {
            if ([delegate respondsToSelector:@selector(sectionHeaderView:sectionClosed:)]) {
                isToggle = NO;
                [imgArrowSection setImage:[UIImage imageBeforeiOS7:imgRightRow equaliOS7:imgRightRowFlat]];
                [delegate sectionHeaderView:self sectionClosed:section];
            }
        }
    }
}
@end
