//
//  EngineeringHistoryController.h
//  mHouseKeeping
//
//  Created by Mac User on 02/04/2013.
//
//

#import <UIKit/UIKit.h>
#import "EngineeringHistorySection.h"
#import "EngineeringHistorySectionInfo.h"
#import "EngineeringHistoryItemCell.h"

@interface EngineeringHistoryController : UIViewController<UITableViewDataSource, UITableViewDelegate, EngineeringHistorySectionDelegate>{
    
    NSMutableArray *sectionDatas;
}

@property (nonatomic, strong) NSMutableArray *sectionDatas;
@property (strong, nonatomic) IBOutlet UITableView *tbvEngineeringHistory;
@property (nonatomic) NSInteger openSectionIndex;
@end
