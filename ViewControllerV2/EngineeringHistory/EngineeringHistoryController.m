//
//  EngineeringHistoryController.m
//  mHouseKeeping
//
//  Created by Mac User on 02/04/2013.
//
//

#import "EngineeringHistoryController.h"
#import "EngineeringHistoryManager.h"
#import "EngineeringHistoryItemCell.h"
#import "MyNavigationBarV2.h"
#import "DeviceManager.h"

@interface EngineeringHistoryController (privateMethods)
-(void) cartButtonPressed;

#define tagTopbarView 1234
#define isDemoMode ([[NSUserDefaults standardUserDefaults] integerForKey:ENABLE_DEMO_MODE] == 1)
//#define tagAlertSubmit 1235
//#define tagAlertNo 11

-(void) loadTopbarView;
-(void) showTopbarView;
-(void) hideTopbarView;
-(void) setHiddenTopbarView;
-(void) adjustShowForViews;
-(void) adjustRemoveForViews;
-(void) addButtonHandleShowHideTopbar;
-(void) topbarTapped:(UIButton *) sender;
-(TopbarViewV2 *) getTopBarView;
@end

@implementation EngineeringHistoryController
@synthesize sectionDatas;
@synthesize tbvEngineeringHistory;
@synthesize openSectionIndex;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //Resize content views of current VC to fit with Navigation bar and tabbar
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    // Do any additional setup after loading the view from its nib.
    [tbvEngineeringHistory setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [tbvEngineeringHistory setBackgroundView:nil];
    [tbvEngineeringHistory setBackgroundColor:[UIColor clearColor]];
    [tbvEngineeringHistory setOpaque:YES];
    self.openSectionIndex = NSNotFound;
    [self performSelector:@selector(loadTopbarView)];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - View lifecycle
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //    [self setCaptionsView];
    
//    [self loadingDatas];
    MBProgressHUD	*HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getRaLoadingData]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES];
    [self performSelector:@selector(loadDataAfterDelayWithHUD:) withObject:HUD afterDelay:0.1];
    
    //show wifi view
    if(!isDemoMode){
        [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationShowWifiView] object:nil];
    }
    self.openSectionIndex = NSNotFound;
    
}

#pragma mark - Load EngineeringHistory
-(void)loadingDatas {
   
//    CountHistoryManager *manager = [[CountHistoryManager alloc] init];
    EngineeringHistoryManager *manager = [[EngineeringHistoryManager alloc] init];
    
    sectionDatas = [[NSMutableArray alloc] init];
    NSInteger userId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    NSArray *listRoomId = [manager loadRoomIdByUserId:userId];
    
    for (EngineeringHistoryModel *eHistoryModel in listRoomId) {
        
        EngineeringHistorySectionInfo *sectionInfo = [[EngineeringHistorySectionInfo alloc] init];
        sectionInfo.eHistoryRoomModel = eHistoryModel;
        sectionInfo.open = false;
        NSMutableArray *listEngineeringHistory = [manager loadAllEngineeringHistoryByUserId:userId AndRoomId:eHistoryModel.eh_room_id];
        for (EngineeringHistoryModel *model in listEngineeringHistory) {
            [sectionInfo insertObjectToNextIndex:model];
        }
        
        [sectionDatas addObject:sectionInfo];
        
    }
    if (sectionDatas.count == 0) {
        UILabel *lblAlert  =  [[UILabel alloc]init];
        lblAlert.frame     =  CGRectMake(15, 150, 290, 80);
        [lblAlert setText:[[LanguageManagerV2 sharedLanguageManager] getNoResultsFound]];
        lblAlert.textAlignment = NSTextAlignmentCenter;
        lblAlert.layer.cornerRadius = 10;
        [lblAlert setFont:[UIFont fontWithName:@"Arial-BoldMT" size:22]];
        lblAlert.numberOfLines = 2;
        [lblAlert setTextColor:[UIColor grayColor]];
        lblAlert.font = [UIFont boldSystemFontOfSize:20.0f];
        [tbvEngineeringHistory addSubview:lblAlert];
    }
    
}


-(void) loadDataAfterDelayWithHUD:(MBProgressHUD *) HUD {
    
    
    EngineeringHistoryManager *manager = [[EngineeringHistoryManager alloc] init];
    [manager deleteDatabaseAfterSomeDays];
    [self loadingDatas];
    [self performSelector:@selector(hiddenHUDAfterLoad:) withObject:HUD afterDelay:0.5];
    
}

#pragma didden after save.
-(void) hiddenHUDAfterLoad:(MBProgressHUD *)HUD{
    
    [HUD hide:YES];
    [HUD removeFromSuperview];
    [tbvEngineeringHistory reloadData];
}


#pragma mark - UITableView Datasource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return sectionDatas.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
//    CountHistorySectionInfo *sectionInfo = [sectionDatas objectAtIndex:section];
    EngineeringHistorySectionInfo *sectionInfo = [sectionDatas objectAtIndex:section];
    NSInteger numberRowInSection = sectionInfo.rowHeights.count;
    return sectionInfo.open ? numberRowInSection : 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifycell = @"EngineeringHistoryItemCell";
    EngineeringHistoryItemCell *cell = nil;
    
    cell = (EngineeringHistoryItemCell *)[tableView dequeueReusableCellWithIdentifier:identifycell];
    if (cell == nil) {
        NSArray *array = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([EngineeringHistoryItemCell class]) owner:self options:nil];
        cell = [array objectAtIndex:0];
    }
    
    //    cell.delegate = self;
    EngineeringHistorySectionInfo *sectionInfo = [sectionDatas objectAtIndex:indexPath.section];
    EngineeringHistoryModel *model = [sectionInfo.rowHeights objectAtIndex:indexPath.row];
    [cell.lblItemName setText:[NSString stringWithFormat:@"%@",model.eh_item_name]];
    [cell.lblLine setBackgroundColor:[UIColor blackColor]];

    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;//70//the number of value must equal the value of cell, if not the index will overwite
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 60;//60
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    EngineeringHistorySectionInfo *sectionInfo = [sectionDatas objectAtIndex:section];
    
    if (sectionInfo.headerView == nil) {
        
        EngineeringHistorySection *headerView = [[EngineeringHistorySection alloc] initWithSection:section EngineeringHistoryModel:sectionInfo.eHistoryRoomModel andOpenStatus:NO];
        sectionInfo.headerView = headerView;
        headerView.delegate = self;
    }
    return sectionInfo.headerView;
}

#pragma mark - set Captions View
//-(void)setCaptionsView {
//    //    [btnAddToCart setTitle:[[LanguageManagerV2 sharedLanguageManager] getAddToCart] forState:UIControlStateNormal];
//    //    [self setTitle:[[LanguageManagerV2 sharedLanguageManager] getLinen]];
//    //
//    msgbtnNo = [[LanguageManagerV2 sharedLanguageManager] getNo];
//    msgbtnYes = [[LanguageManagerV2 sharedLanguageManager] getYes];
//    msgSaveCount = [[LanguageManagerV2 sharedLanguageManager] getMsgAlertSaveCounts];
//    //    msgDiscardCount = [[LanguageManagerV2 sharedLanguageManager] getMsgAlertDiscardCounts];
//
//    //refresh topview
//    TopbarViewV2 *topview = [self getTopBarView];
//    [topview refresh:[[SuperRoomModelV2 alloc] init]];
//}

//update code
#pragma mark - Section Header Delegate
-(void)sectionHeaderView:(EngineeringHistorySection *)sectionheaderView sectionOpened:(NSInteger)section {
    
    EngineeringHistorySectionInfo *sectionInfo = [sectionDatas objectAtIndex:section];
    sectionInfo.open = YES;
    
    NSInteger countOfRowsToInsert = [sectionInfo.rowHeights count];
    NSMutableArray *indexPathsToInsert = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i < countOfRowsToInsert; i++) {
        [indexPathsToInsert addObject:[NSIndexPath indexPathForRow:i inSection:section]];
    }
    
    UITableViewRowAnimation insertAnimation;
    
    
    UITableViewRowAnimation deleteAnimation;
    
    
    NSMutableArray *indexPathsToDelete = [[NSMutableArray alloc] init];
    
    NSInteger previousOpenSectionIndex = self.openSectionIndex;
    
    if (openSectionIndex == -1) {
        
    }
    else {
        if (previousOpenSectionIndex != NSNotFound) {
            
            EngineeringHistorySectionInfo *previousOpenSection = [self.sectionDatas objectAtIndex:previousOpenSectionIndex];
            previousOpenSection.open = NO;
            [previousOpenSection.headerView toggleOpenWithUserAction:NO];
            
            NSInteger countOfRowsToDelete =[previousOpenSection.rowHeights count];
            for (NSInteger i = 0; i < countOfRowsToDelete; i++) {
                [indexPathsToDelete addObject:[NSIndexPath indexPathForRow:i inSection:previousOpenSectionIndex]];
            }
            
        }
    }
    
    if (previousOpenSectionIndex == NSNotFound || section < previousOpenSectionIndex) {
        insertAnimation = UITableViewRowAnimationTop;
        deleteAnimation = UITableViewRowAnimationBottom;
    }
    else {
        insertAnimation = UITableViewRowAnimationBottom;
        deleteAnimation = UITableViewRowAnimationTop;
    }
    
    
    [self.tbvEngineeringHistory beginUpdates];
    
    [self.tbvEngineeringHistory insertRowsAtIndexPaths:indexPathsToInsert withRowAnimation:insertAnimation];
    [self.tbvEngineeringHistory deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:deleteAnimation];
    [self.tbvEngineeringHistory endUpdates];
    
    [self.tbvEngineeringHistory scrollToRowAtIndexPath:[indexPathsToInsert objectAtIndex:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    self.openSectionIndex = section;
    
}

-(void)sectionHeaderView:(EngineeringHistorySection *)sectionheaderView sectionClosed:(NSInteger)section {

    EngineeringHistorySectionInfo *sectionInfo = [sectionDatas objectAtIndex:section];
    sectionInfo.open = NO;
    NSInteger countOfRowsToDelete = [self.tbvEngineeringHistory numberOfRowsInSection:section];
    
    if (countOfRowsToDelete > 0) {
        NSMutableArray *indexPathsToDelete = [[NSMutableArray alloc] init];
        for (NSInteger i = 0; i < countOfRowsToDelete; i++) {
            [indexPathsToDelete addObject:[NSIndexPath indexPathForRow:i inSection:section]];
        }
        
        [tbvEngineeringHistory beginUpdates];
        [self.tbvEngineeringHistory deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:UITableViewRowAnimationAutomatic];
        [tbvEngineeringHistory endUpdates];
    }
    
    self.openSectionIndex = NSNotFound;
}

#pragma mark - Handle Topbar Methods
-(void)loadTopbarView {
    TopbarViewV2 *topbar = [[TopbarViewV2 alloc] initViewWithSuperModel:[[CommonVariable sharedCommonVariable] superRoomModel]];
    [topbar setTag:tagTopbarView];
    [self.view addSubview:topbar];
    [topbar setHidden:YES];
    
    [topbar setAlpha:0.0];
    [self addButtonHandleShowHideTopbar];
}

-(void)showTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationWillStartSelector:@selector(setHiddenTopbarView)];
    
    f = topbar.frame;
    f.origin.y = 0;
    [topbar setFrame:f];
    [topbar setAlpha:1.0];
    
    [self adjustShowForViews];
    
    [UIView commitAnimations];
}

-(void)hideTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDidStopSelector:@selector(setHiddenTopbarView)];
    [UIView setAnimationDelegate:self];
    
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    [topbar setAlpha:0.0];
    
    [self adjustRemoveForViews];
    
    [UIView commitAnimations];
}

-(void)setHiddenTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    [topbar setHidden:!topbar.hidden];
}

-(TopbarViewV2 *)getTopBarView {
    return (TopbarViewV2 *)[self.view viewWithTag:tagTopbarView];
}

-(void)addButtonHandleShowHideTopbar {
    UIView *vBarButton = [[UIView alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
    UIButton *titleBarButtonFirst = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 200, 30)];
    
    titleBarButtonFirst.titleLabel.backgroundColor = [UIColor clearColor];
    [titleBarButtonFirst.titleLabel setFont: FONT_BUTTON_TOPBAR];
    titleBarButtonFirst.titleLabel.textAlignment = NSTextAlignmentCenter;
    titleBarButtonFirst.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
    
    [titleBarButtonFirst.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
    [titleBarButtonFirst.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
    [titleBarButtonFirst addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    [titleBarButtonFirst setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_HISTORY] forState:UIControlStateNormal];
    
    [vBarButton addSubview:titleBarButtonFirst];
    
    
    UIButton *titleBarButtonSecond = [[UIButton alloc] initWithFrame:CGRectMake(0, 28, 200, 22)];
    [titleBarButtonSecond setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [titleBarButtonSecond.titleLabel setFont:FONT_BUTTON_TOPBAR_SECOND];
    [titleBarButtonSecond setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ENGINEERING_CASE] forState:UIControlStateNormal];
    [titleBarButtonSecond addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    [vBarButton addSubview:titleBarButtonSecond];
    
    self.navigationItem.titleView = vBarButton;
    //Move title to center vertical
    CGRect titleViewBounds = self.navigationItem.titleView.bounds;
    int moveLeftTitle = 0;
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        moveLeftTitle = 10;
    }
    self.navigationItem.titleView.bounds = CGRectMake(titleViewBounds.origin.x + moveLeftTitle, Y_TOPBAR_TITLE, titleViewBounds.size.width, titleViewBounds.size.height);
    
    //back button navigation
    self.navigationItem.leftBarButtonItem = [MyNavigationBarV2 createBackButtonWithTarget:self selector:@selector(btnBackPressed) forControlEvents:UIControlEventTouchUpInside];
}

-(void)btnBackPressed
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)topbarTapped:(UIButton *)sender {
    TopbarViewV2 *topbar = [self getTopBarView];
    [sender setEnabled:NO];
    if (topbar.isHidden) {
        [self showTopbarView];
    } else {
        [self hideTopbarView];
    }
    [self performSelector:@selector(topbarButtonEnable:) withObject:sender afterDelay:0.5];
}

-(void) topbarButtonEnable:(UIButton *) topbarBtn {
    [topbarBtn setEnabled:YES];
}

-(void)adjustShowForViews {
    //adjust all views when show
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    CGRect fRoomView = tbvEngineeringHistory.frame;
    [tbvEngineeringHistory setFrame:CGRectMake(0, f.size.height, 320, fRoomView.size.height)];
//    [tbvEngineeringHistory setFrame:CGRectMake(0, f.size.height, 320, fRoomView.size.height - f.size.height)];
    
    //    CGRect ftbv = tbvLinen.frame;
    //    [tbvLinen setFrame:CGRectMake(0, f.size.height + fRoomView.size.height, 320, ftbv.size.height - f.size.height)];
    
}

-(void)adjustRemoveForViews {
    //adjust all views when hide
//    TopbarViewV2 *topbar = [self getTopBarView];
//    CGRect f = topbar.frame;
    
    CGRect fRoomView = tbvEngineeringHistory.frame;
    [tbvEngineeringHistory setFrame:CGRectMake(0, 0, 320, fRoomView.size.height)];
//    [tbvEngineeringHistory setFrame:CGRectMake(0, 0, 3209, fRoomView.size.height + f.size.height)];
    
    //    CGRect ftbv = tbvLinen.frame;
    //    [tbvLinen setFrame:CGRectMake(0, fRoomView.size.height, 320, ftbv.size.height + f.size.height)];
    
}

@end
