//
//  EngineeringHistoryItemCell.h
//  mHouseKeeping
//
//  Created by Mac User on 02/04/2013.
//
//

#import <UIKit/UIKit.h>

@interface EngineeringHistoryItemCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *lblItemName;
@property (nonatomic, strong) IBOutlet UILabel *lblLine;
@end
