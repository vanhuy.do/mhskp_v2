//
//  EngineeringHistoryItemCell.m
//  mHouseKeeping
//
//  Created by Mac User on 02/04/2013.
//
//

#import "EngineeringHistoryItemCell.h"

@implementation EngineeringHistoryItemCell
@synthesize lblItemName,lblLine;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
