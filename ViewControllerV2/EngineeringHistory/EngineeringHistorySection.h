//
//  EngineeringHistorySection.h
//  mHouseKeeping
//
//  Created by Mac User on 02/04/2013.
//
//

#import <UIKit/UIKit.h>
#import "EngineeringHistoryModel.h"

@class EngineeringHistorySection;
@protocol EngineeringHistorySectionDelegate <NSObject>

@optional
-(void) sectionHeaderView:(EngineeringHistorySection *) sectionheaderView sectionOpened:(NSInteger) section;
-(void) sectionHeaderView:(EngineeringHistorySection *) sectionheaderView sectionClosed:(NSInteger) section;

@end

@interface EngineeringHistorySection : UIView{
    __unsafe_unretained id<EngineeringHistorySectionDelegate> delegate;
    NSInteger section;
    BOOL isToggle;
}
@property (nonatomic, assign) NSInteger section;
@property (nonatomic, assign) id<EngineeringHistorySectionDelegate> delegate;
@property (strong, nonatomic) IBOutlet UIImageView *imgEHistorySection;
@property (strong, nonatomic) IBOutlet UILabel *lblEHistoryTitle;
@property (strong, nonatomic) IBOutlet UIImageView *imgDetail;

-(void) toggleOpenWithUserAction:(BOOL) userAction;
-(id) initWithSection:(NSInteger) section EngineeringHistoryModel:(EngineeringHistoryModel *) roomModel andOpenStatus:(BOOL) isOpen;

@end
