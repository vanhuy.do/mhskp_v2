//
//  EngineeringHistorySectionInfo.h
//  mHouseKeeping
//
//  Created by Mac User on 02/04/2013.
//
//

#import <Foundation/Foundation.h>
#import "EngineeringHistoryModel.h"
#import "EngineeringHistorySection.h"

@interface EngineeringHistorySectionInfo : NSObject

//@property (nonatomic, strong) CountHistoryModel *countRoomModel;
@property (nonatomic, strong) EngineeringHistoryModel *eHistoryRoomModel;
@property (assign) BOOL open;
//@property (strong) CountHistorySection* headerView;
@property (strong) EngineeringHistorySection *headerView;
@property (nonatomic,strong,readonly) NSMutableArray *rowHeights;

- (NSUInteger)countOfRowHeights;
- (id)objectInRowHeightsAtIndex:(NSUInteger)idx;
- (void)insertObject:(id)anObject inRowHeightsAtIndex:(NSUInteger)idx;
- (void)removeObjectFromRowHeightsAtIndex:(NSUInteger)idx;
- (void)replaceObjectInRowHeightsAtIndex:(NSUInteger)idx withObject:(id)anObject;
- (void)getRowHeights:(id __unsafe_unretained [])buffer range:(NSRange)inRange;
- (void)insertRowHeights:(NSArray *)rowHeightArray atIndexes:(NSIndexSet *)indexes;
- (void)removeRowHeightsAtIndexes:(NSIndexSet *)indexes;
- (void)replaceRowHeightsAtIndexes:(NSIndexSet *)indexes withRowHeights:(NSArray *)rowHeightArray;
-(void) insertObjectToNextIndex:(id)anObject;

@end
