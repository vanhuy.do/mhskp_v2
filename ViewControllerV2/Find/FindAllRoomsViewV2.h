//
//  FindAllRoomsViewV2.h
//  mHouseKeeping
//
//  Created by Nghia Truong on 2/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PickerViewV2.h"

@interface FindAllRoomsViewV2 : UIViewController
<UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate,
PickerViewV2Delegate,UIGestureRecognizerDelegate> {
    UIButton *btnRoomStatus;
    UISearchBar *searchBar;
    UILabel *lblByRoomStatus;
    UILabel *lblRoomNo;
    UILabel *lblRoomStatus;
    UILabel *lblVIP;
    UILabel *lblCleaningStatus;
    UITableView *tbvContent;
    UITableView *tbvNoResult;
    
    NSMutableArray *listDisplayData;
    NSMutableArray *listTempData;
    UIButton *btnBack;
    BOOL isEdit;
    NSInteger roomStatusID;
    NSInteger filterType;
    BOOL isLoadFromWS;
    BOOL isDueRoom;
    BOOL isHaveReasonRemark;
    NSString *selectedRoomStatus;
    NSMutableArray *listRoomStatusCleaningStatus;
    //Data for picker view
    NSString* nameVC;
    NSString* nameOC;
    NSString* nameVD;
    NSString* nameOD;
    NSString* nameVI;
    NSString* nameOI;
    NSString* nameOOS;
    NSString* nameOOO;
    NSString* nameVPU;
    NSString* nameOPU;
    NSString* nameOCSO;
    NSString* namePending;
    NSString* nameDND;
    NSString* nameDoubleLock;
    NSString* nameServiceLater;
    NSString* nameDeclinedService;
    NSString* nameStarted;
    NSString* nameStop;
    NSString* nameCompleted;
    NSString* nameReassignPending;
    NSString* nameInspected;
    NSString* nameRushRoom;
    NSString* nameQueueRoom;
    NSString* nameUnassignRoom;
    NSString* nameOccupiedDueOut;
    NSString* nameProfileNotesExist;
    NSString* nameStayOver;
    NSString* nameODDO;
    NSString* nameAll;
    NSString* nameArrival;
}

@property (nonatomic, strong) IBOutlet UIButton *btnRoomStatus;
@property (nonatomic, strong) IBOutlet UISearchBar *searchBar;
@property (nonatomic, strong) IBOutlet UILabel *lblByRoomStatus;
@property (nonatomic, strong) IBOutlet UILabel *lblRoomNo;
@property (nonatomic, strong) IBOutlet UILabel *lblRoomStatus;
@property (nonatomic, strong) IBOutlet UILabel *lblVIP;
@property (nonatomic, strong) IBOutlet UILabel *lblCleaningStatus;
@property (nonatomic, strong) IBOutlet UITableView *tbvContent;
@property (nonatomic, strong) IBOutlet UITableView *tbvNoResult;
@property (nonatomic, strong) NSMutableArray *listDisplayData;
@property (nonatomic, strong) NSMutableArray *listTempData;
@property (strong, nonatomic) IBOutlet UIView *controlsView;
@property (readwrite, nonatomic) BOOL isLoadFromWS;
@property (strong, nonatomic) NSString *selectedRoomStatus;

-(IBAction)btnRoomStatusPressed:(id)sender;
-(void) loadData;
-(void) setCaptionsView;
@end
