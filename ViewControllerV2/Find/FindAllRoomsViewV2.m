//
//  FindAllRoomsViewV2.m
//  mHouseKeeping
//
//  Created by Nghia Truong on 2/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FindAllRoomsViewV2.h"
#import "FindByRoomCellV2.h"
#import "RoomManagerV2.h"
#import "RoomAssignmentInfoViewController.h"
#import "UserManagerV2.h"
#import "MyNavigationBarV2.h"
#import "DeviceManager.h"
#import "ehkDefinesV2.h"
#import "UIImage+Tint.h"
#import "LogFileManager.h"
#define ARRIVAL_KEY @"Arrival"
#define kRoomNo             @"RoomNo"
#define kGuestName          @"GuestName"
#define kRMStatus           @"RMStatus"
#define kCleaningStatus     @"CleaningStatus"
#define kVIP                @"VIP"
#define kRoomAssignmentID   @"RoomAssignmentID"

#define markImage           @"!.png"
#define backgroundImage     @"bg.png"

#define all                 @""
//#define nameVC              @"VC"
//#define nameOC              @"OC"
//#define nameVD              @"VD"
//#define nameOD              @"OD"
//#define nameVI              @"VI"
//#define nameOI              @"OI"
//#define nameOOS             @"OOS"
//#define nameOOO             @"OOO"
//#define nameVPU             @"VPU"
//#define nameOCSO            @"OC-SO"
//#define namePending         @"Pending"
//#define nameDND             @"DND"
//#define nameServiceLater    @"Service Later"
//#define nameDeclinedService @"Declined Service"
//#define nameCompleted       @"Completed"
//#define nameInspected       @"Inspected"
//#define nameRushRoom        @"Rush Room"
//#define nameQueueRoom       @"Queue Room"
//#define nameUnassignRoom    @"Unassign"

#define tagNoResult         12345

//#define heightTbvContent    44.0f
#define heightTbvContent    60.0f
#define heightTbvNoResult   240.0f

#define font                @"Arial-BoldMT"

#define sizeNoResult        22.0f
#define sizeNormal          17.0f
#define sizeSmall           10.0f

#define colorAlpha          1.0
#define colorWhite          255/255.0
#define colorBlueR          105/255.0
#define colorBlueG          218/255.0
#define colorBlueB          222/255.0

#define roomStatusType      1
#define cleaningStatusType  2
#define kindOfRoomType      3
#define unassignType        4
#define inspectedCase       -2

@interface FindAllRoomsViewV2(PrivateMethods)

-(void) btnDetailArrowPressed:(NSInteger)index;

-(void) loadTopbarView;
-(void) showTopbarView;
-(void) hideTopbarView;
-(void) setHiddenTopbarView;
-(void) adjustShowForViews;
-(void) adjustRemoveForViews;
-(void) addButtonHandleShowHideTopbar;
-(void) topbarTapped:(UIButton *) sender;
-(TopbarViewV2 *) getTopBarView;
-(void) btnBackPressed;
-(void) addBackButton;
//- (void)addAccessoryButton:(UITableViewCell *)cell  andIndexPath:(NSIndexPath *)indexPath;

@end
@implementation FindAllRoomsViewV2
@synthesize controlsView;

@synthesize btnRoomStatus, lblByRoomStatus, lblVIP, lblRoomNo, lblRoomStatus, lblCleaningStatus, searchBar, tbvContent, tbvNoResult, listDisplayData, listTempData, isLoadFromWS, selectedRoomStatus;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle
-(void)viewWillAppear:(BOOL)animated {
    [self setCaptionsView];
    [super viewWillAppear:animated];
    
    //set selection find in bottom bar
    [[HomeViewV2 shareHomeView] setSelectedButton:tagOfFindButton];
    
    // Load Data
    [self performSelector:@selector(loadData) withObject:nil afterDelay:0.5];
}

- (void)viewDidLoad {
    [self loadAllRoomStatusAndCleaningStatus];
    roomStatusID = 1;
    filterType = 1;
    [tbvContent setBackgroundView:nil];
    [tbvContent setBackgroundColor:[UIColor clearColor]];
    [tbvContent setOpaque:YES];
    
    [tbvNoResult setBackgroundView:nil];
    [tbvNoResult setBackgroundColor:[UIColor clearColor]];
    [tbvNoResult setOpaque:YES];
    [tbvNoResult setHidden:YES];
    
    [super viewDidLoad];
    //Resize content views of current VC to fit with Navigation bar and tabbar
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        [self loadFlatResource];
    }
    [self performSelector:@selector(loadTopbarView)];
    
    [self addBackButton];
    
    UILongPressGestureRecognizer *longpressGesture=[[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(responseLongPressButton:)];
    longpressGesture.delegate=self;
    longpressGesture.minimumPressDuration=2;
    [tbvContent addGestureRecognizer:longpressGesture];
    
    
}


-(void)responseLongPressButton:(UILongPressGestureRecognizer*)sender
{
    if (sender.state ==UIGestureRecognizerStateBegan) {
        if (tbvContent.editing) {
            [tbvContent setEditing:NO];
        }
        else
        {
            [tbvContent setEditing:YES];
        }
    }
    
}

- (void)viewDidUnload {
    [self setControlsView:nil];
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


-(int)getRoomIndicatorGuestByArrivalDate:(NSString*)arrivalDate departureDate:(NSString*)departureDate
{
    NSString *arrivalDateCompare = nil;
    NSString *departureDateCompare = nil;
    NSString *nowDate = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd"];
    
    if(arrivalDate.length > 0) {
        arrivalDateCompare = [ehkConvert DateToStringWithString:arrivalDate fromFormat:@"yyyy-MM-dd HH:mm:ss" toFormat:@"yyyy-MM-dd"];
        
    }
    
    if(departureDate.length > 0) {
        departureDateCompare = [ehkConvert DateToStringWithString:departureDate fromFormat:@"yyyy-MM-dd HH:mm:ss" toFormat:@"yyyy-MM-dd"];
    }
    
    //Checking Back To Back
    if(arrivalDateCompare.length > 0 && departureDateCompare.length > 0) {
        if([arrivalDateCompare caseInsensitiveCompare:nowDate] == NSOrderedSame && [departureDateCompare caseInsensitiveCompare:nowDate] == NSOrderedSame){
            return RoomIndicatorGuestBackToBack;
        }
    }
    
    //Checking Due In Guest
    if(arrivalDateCompare.length > 0) {
        if([arrivalDateCompare caseInsensitiveCompare:nowDate] == NSOrderedSame) {
            return RoomIndicatorGuestDueIn;
        }
    }
    
    //Checking Due Out Guest
    if(departureDateCompare.length > 0){
        if([departureDateCompare caseInsensitiveCompare:nowDate] == NSOrderedSame) {
            return RoomIndicatorGuestDueOut;
        }
    }
    
    return 0;
}


#pragma mark -
#pragma mark UITableView Editing rows
-(NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSIndexPath *rowToSelect = indexPath;
    if (isEdit) {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        rowToSelect = nil;
    }
    return rowToSelect;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
	UITableViewCellEditingStyle style = UITableViewCellEditingStyleNone;
    return  style;
}

#pragma mark -
#pragma mark UITableView Moving rows
-(BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath{
    BOOL canMove = NO;
    
    if (indexPath.section == 0) {
        NSDictionary *rowData = [listDisplayData objectAtIndex:indexPath.row];
        CleaningStatusModelV2 *csmodel = [[CleaningStatusModelV2 alloc] init];
        csmodel.cstat_Id = [[rowData objectForKey:kCleaningStatusID] intValue];
        [[RoomManagerV2 sharedRoomManager] loadCleaningStatus:csmodel];
        
        if ([csmodel.cstat_Name isEqualToString:@"DND"]) {
            return NO;
        }
        
        canMove = indexPath.row <= [listDisplayData count];
    }
    return canMove;
}

- (NSIndexPath *)tableView:(UITableView *)tableView targetIndexPathForMoveFromRowAtIndexPath:(NSIndexPath *)sourceIndexPath toProposedIndexPath:(NSIndexPath *)proposedDestinationIndexPath {
    NSDictionary *rowData = [listDisplayData objectAtIndex:proposedDestinationIndexPath.row];
    CleaningStatusModelV2 *csmodel = [[CleaningStatusModelV2 alloc] init];
    csmodel.cstat_Id = [[rowData objectForKey:kCleaningStatusID] intValue];
    [[RoomManagerV2 sharedRoomManager] loadCleaningStatus:csmodel];
    
    if ([csmodel.cstat_Name isEqualToString:@"DND"]) {
        return sourceIndexPath;
    }
    return proposedDestinationIndexPath;
}

-(void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath{
    if ([fromIndexPath isEqual:toIndexPath]) {
        return;
    }
    
    NSInteger rowFrom, rowTo;
    rowFrom = fromIndexPath.row;
    rowTo = toIndexPath.row;
    
    NSMutableDictionary *fromRowData = [[NSMutableDictionary alloc] initWithDictionary: [listDisplayData objectAtIndex:fromIndexPath.row]];
    NSDictionary *toRowData = [listDisplayData objectAtIndex:toIndexPath.row];
    
    NSInteger priorityFrom, priorityTo;
    priorityTo = [[toRowData valueForKey:kRaPrioritySortOrder] integerValue];
    priorityFrom = [[fromRowData valueForKey:kRaPrioritySortOrder] integerValue];
    //Change priorities
    if (rowFrom < rowTo) {
        // To Priority + 1
        priorityFrom = priorityTo + 1;
    } else if (rowFrom > rowTo) {
        // To Priority - 1
        priorityFrom = priorityTo - 1;
    }
    
    NSString *priorityFromStr = [[NSString alloc] initWithFormat:@"%d", (int)priorityFrom];
    
    [fromRowData setValue:priorityFromStr forKey:kRaPrioritySortOrder];
    
    [listDisplayData removeObjectAtIndex:fromIndexPath.row];
    
    [listDisplayData insertObject:fromRowData atIndex:toIndexPath.row];
    
    //    [priorityFromStr release];
    //    [fromRowData release];
    
    //update room assignment priority
    RoomAssignmentModelV2 *model = [[RoomAssignmentModelV2 alloc] init];
    
    NSInteger raID = [[fromRowData objectForKey:kRoomAssignmentID] integerValue];
    NSInteger priority = [[fromRowData objectForKey:kRaPrioritySortOrder] integerValue];
    model.roomAssignment_Id = (int)raID;
    model.roomAssignment_UserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    [[RoomManagerV2 sharedRoomManager] load:model];
    model.roomAssignment_Priority_Sort_Order = priority;
    
    [[RoomManagerV2 sharedRoomManager] update:model];
}

-(BOOL)tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([indexPath section] == 0) {
        return NO;
    }
    return YES;
}

-(void)tableView:(UITableView *)tableView willBeginEditingRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([indexPath section] == 0) {
        //redraw underline label
        //UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        //[self drawUnderlinedLabel:(UILabel *)[cell viewWithTag:tagRMStatus] inTableViewCell:cell];
    }
}

-(void)tableView:(UITableView *)tableView didEndEditingRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([indexPath section] == 0) {
        [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationTop];
    }
}


#pragma mark - UITableView Delegate Methods
-(CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    if(tableView == tbvContent)
//        return heightTbvContent;
//    else if(tableView == tbvNoResult)
//        return heightTbvNoResult;
//    
//    return 0;
    
    if([self.listDisplayData count] > 0) {
        NSDictionary *rowData = [self.listDisplayData objectAtIndex:indexPath.row];
        NSString *roomNo = (NSString *)[rowData objectForKey:kRoomNo];
        if(roomNo.length > 0) {
            RoomBlocking *curRoomBlocking = [[RoomManagerV2 sharedRoomManager] loadRoomBlockingByUserId:[UserManagerV2 sharedUserManager].currentUser.userId roomNumber:roomNo];
            NSUInteger row = [indexPath row];
            NSDictionary *rowData =(NSDictionary*)[self.listDisplayData objectAtIndex:row];
            NSString *guestArrivalTime = (NSString *)[rowData objectForKey:kRaGuestArrivalTime];
            NSString *guestDepartureTime = (NSString *)[rowData objectForKey:kRaGuestDepartureTime];
            NSString *guestName = [rowData objectForKey:kGuestName];
            int roomIndicatorGuest = [self getRoomIndicatorGuestByArrivalDate:guestArrivalTime departureDate:guestDepartureTime];
            isDueRoom = (roomIndicatorGuest == RoomIndicatorGuestDueIn) || (roomIndicatorGuest == RoomIndicatorGuestDueOut) || (roomIndicatorGuest == RoomIndicatorGuestBackToBack);
            [LogFileManager logDebugMode:@"isdue = %@", isDueRoom ? @"True" : @"False"];
            
            isHaveReasonRemark = (curRoomBlocking.roomblocking_reason.length > 0) && ![curRoomBlocking.roomblocking_reason isEqualToString:@""] && ![curRoomBlocking.roomblocking_reason isEqualToString:@"(null)"] && (curRoomBlocking.roomblocking_remark_physical_check.length > 0) && ![curRoomBlocking.roomblocking_remark_physical_check isEqualToString:@""] && ![curRoomBlocking.roomblocking_remark_physical_check isEqualToString:@"(null)"] && curRoomBlocking.roomblocking_oosdurations.length > 0;
            if(curRoomBlocking != nil
               && curRoomBlocking.roomblocking_is_blocked > RoomBlockingStatus_Normal
               && isHaveReasonRemark
               && (isDueRoom || guestName.length > 0)) {
                
                //Extend the height when room was blocked
                return 105;
            }
            else if (curRoomBlocking != nil
                     && curRoomBlocking.roomblocking_is_blocked > RoomBlockingStatus_Normal
                     && isHaveReasonRemark
                     && !isDueRoom && guestName.length == 0)
            {
                return 85;
            }
        }
    }
    return 65;
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView
numberOfRowsInSection:(NSInteger)section {
    if(tableView == tbvContent)
        return [listDisplayData count];
    
    return 1;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if(tableView == tbvNoResult) {
        cell.backgroundColor = [UIColor clearColor];
    }
    else{
        cell.backgroundColor = [UIColor colorWithRed:colorWhite green:colorWhite
                                                blue:colorWhite alpha:colorAlpha];
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView
        cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentify = @"cellIdentify";
    static NSString *cellNoIndentify = @"cellNoIndentify";
    
    UITableViewCell *cell = nil;
    
    if(tableView == tbvNoResult) {
        cell = [tableView dequeueReusableCellWithIdentifier:cellNoIndentify];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentify];
            
            UILabel *titleNoResult = [[UILabel alloc]
                                      initWithFrame:CGRectMake(0, 0, 320, 240)];
            [titleNoResult setTextColor:[UIColor grayColor]];
            [titleNoResult setBackgroundColor:[UIColor clearColor]];
            [titleNoResult setTextAlignment:NSTextAlignmentCenter];
            [titleNoResult setTag:tagNoResult];
            [titleNoResult setFont:[UIFont fontWithName:font size:sizeNoResult]];
            
            [cell.contentView addSubview:titleNoResult];
        }
        UILabel *lblNoResult = (UILabel *)[cell viewWithTag:tagNoResult];
        [lblNoResult setText:[[LanguageManagerV2 sharedLanguageManager] getNoResultsFound]];
        
        return cell;
    }
    
    if(tableView == tbvContent) {
//        FindByRoomCellV2 *cell = nil;
//        cell = (FindByRoomCellV2 *)[tableView dequeueReusableCellWithIdentifier:cellIdentify];
//        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
//        
//        if (cell == nil) {
//            NSArray *arrayCell = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([FindByRoomCellV2 class]) owner:self options:nil];
//            cell = [arrayCell objectAtIndex:0];
//            //            [cell.btnDetailArrow addTarget:self action:@selector(btnDetailPressed:) forControlEvents:UIControlEventTouchUpInside];
//            [cell.lblRoomStatus setFont:[UIFont fontWithName:font size:sizeNormal]];
//            //            [cell.lblGuestName setFont:[UIFont fontWithName:font size:sizeSmall]];
//            [cell.lblRoomNo setFont:[UIFont fontWithName:font size:sizeNormal]];
//            [cell.lblVIP setFont:[UIFont fontWithName:font size:sizeNormal]];
//        }
//        
//        //set tag for detail arrow to know what's row is pressed
//        //        [cell.btnDetailArrow setTag:indexPath.row];
//        
//        NSDictionary *rowData = [self.listDisplayData objectAtIndex:indexPath.row];
//        
//        UILabel *lblRoomNoCell = (UILabel *)[cell viewWithTag:tagRoomNo];
//        UILabel *lblRoomStatusCell = (UILabel *)[cell viewWithTag:tagRMStatus];
//        UILabel *lblVipRoom = (UILabel *)[cell viewWithTag:tagVIP];
//        UIImageView *imgFirstIconCleaningStatus = (UIImageView *)[cell viewWithTag:tagFirstIconCleaningStatus];
//        UIImageView *imgSecondIconCleaningStatus = (UIImageView *)[cell viewWithTag:tagSecondIconCleaningStatus];
//        UILabel *lblServiceLaterTime = (UILabel *)[cell viewWithTag:tagTimeServiceLater];
//        UIImageView *imgRushOrQueueRoom = (UIImageView *)[cell viewWithTag:tagRushOrQueueRoom];
//        UILabel *lblDueOutRoom = (UILabel *)[cell viewWithTag:tagDueOutRoom];
//        UILabel *lblArrivalTime = (UILabel *)[cell viewWithTag:tagArrivalTime];
//        UILabel *lblGuestName = (UILabel *)[cell viewWithTag:tagGuestName];
//        UILabel *lblRAName = (UILabel *)[cell viewWithTag:tagRAName];
//        
//        NSString *roomNo = (NSString *)[rowData objectForKey:kRoomNo];
//        NSString *guestName = [rowData objectForKey:kGuestName];
//        NSString *vip = (NSString *)[rowData objectForKey:kVIP];
//        NSInteger cleaningStatus = [(NSString *)[rowData objectForKey:kCleaningStatusID] integerValue];
//        NSString *roomStatus = (NSString *)[rowData objectForKey:kRMStatus];
//        NSString *guestArrivalTime = (NSString *)[rowData objectForKey:kRaGuestArrivalTime];
//        NSInteger kindOfRoom = [(NSString *)[rowData objectForKey:kRaKindOfRoom] integerValue];
//        NSInteger isMockRoom = [(NSString *)[rowData objectForKey:kRaIsMockRoom] integerValue];
//        NSInteger isReassignedRoom = [(NSString *)[rowData objectForKey:kRaIsReassignedRoom] integerValue];
//        NSData *cleaningStatusIcon = (NSData *)[rowData objectForKey:kCleaningStatus];
//        NSString *roomAssignmentTime = (NSString *)[rowData objectForKey:kRoomAssignmentTime];
//        
//        [lblRoomNoCell setText:roomNo];
//        [lblRoomStatusCell setText:roomStatus];
//        [lblVipRoom setText:(vip == nil || [vip isEqualToString:@""])?@"-":vip];
//        [lblDueOutRoom setText:@"[D/O]"];
//        [lblArrivalTime setText:guestArrivalTime];
//        [lblGuestName setText:(guestName == nil || [guestName isEqualToString:@""])?@"-":guestName];
//        
//        if (kindOfRoom == ENUM_KIND_OF_ROOM_NORMAL) {
//            [imgRushOrQueueRoom setHidden:YES];
//        }
//        else if (kindOfRoom == ENUM_KIND_OF_ROOM_RUSH) {
//            [imgRushOrQueueRoom setHidden:NO];
//            [imgRushOrQueueRoom setImage:[UIImage imageBeforeiOS7:imgRushRoom equaliOS7:imgRushRoomFlat]];
//        }
//        else if (kindOfRoom == ENUM_KIND_OF_ROOM_QUEUE) {
//            [imgRushOrQueueRoom setHidden:NO];
//            [imgRushOrQueueRoom setImage:[UIImage imageBeforeiOS7:imgQueueRoom equaliOS7:imgQueueRoomFlat]];
//        }
//        else {
//            NSLog(@"OUT OF VALUE RANGE");
//            [imgRushOrQueueRoom setHidden:YES];
//        }
//        
//        BOOL isDueOut = false; //departuretime < currenttime (HH:mm:ss)
//        BOOL isGuestComing = false; // arrivalTime > departureTime
//        
//        if (isDueOut) {
//            [cell setBackgroundColor:[UIColor colorWithRed:32/255.0f green:178/255.0f blue:170/255.0f alpha:1]];
//            [lblDueOutRoom setHidden:NO];
//            [lblDueOutRoom setFrame:lblDueOutFrame];
//            if (isGuestComing) {
//                [lblArrivalTime setHidden:NO];
//                [lblArrivalTime setFrame:lblArrivalTimeFrameCase1];
//                [lblGuestName setFrame:lblGuestNameFrameInCase1];
//            }
//            else {
//                [lblArrivalTime setHidden:YES];
//                [lblGuestName setFrame:lblGuestNameFrameInCase3];
//            }
//        }
//        else {
//            [lblDueOutRoom setHidden:YES];
//            if (isGuestComing) {
//                [lblArrivalTime setHidden:NO];
//                [lblArrivalTime setFrame:lblArrivalTimeFrameCase2];
//                [lblGuestName setFrame:lblGuestNameFrameInCase2];
//            }
//            else {
//                [lblArrivalTime setHidden:YES];
//                [lblGuestName setFrame:lblGuestNameFrameInCase4];
//            }
//        }
//        [cell setAccessoryType:UITableViewCellAccessoryNone];
//        
//        //CRF-0000682: blocking room sign, remark and reason
//        //Only RA apply blocking status
//        RoomBlocking *curRoomBlocking = [[RoomManagerV2 sharedRoomManager] loadRoomBlockingByUserId:[UserManagerV2 sharedUserManager].currentUser.userId roomNumber:roomNo];
//        UIImageView *blockRoomStatus = (UIImageView*)[cell.contentView viewWithTag:tagBlockingImage];
//        UILabel *lblBlockReason = (UILabel*)[cell.contentView viewWithTag:tagBlockingReason];
//        UILabel *lblBlockRemark = (UILabel*)[cell.contentView viewWithTag:tagBlockingRemark];
//        if(curRoomBlocking.roomblocking_is_blocked == RoomBlockingStatus_Blocked) {
//            [blockRoomStatus setHidden:NO];
//            [lblBlockRemark setHidden:NO];
//            [lblBlockReason setHidden:NO];
//            [blockRoomStatus setImage:[UIImage imageNamed:imgBlockRoom]];
//            [lblBlockReason setText:curRoomBlocking.roomblocking_reason];
//            [lblBlockRemark setText:curRoomBlocking.roomblocking_remark_physical_check];
//        } else if(curRoomBlocking.roomblocking_is_blocked == RoomBlockingStatus_Release) {
//            [blockRoomStatus setHidden:NO];
//            [blockRoomStatus setImage:[UIImage imageNamed:imgReleaseRoom]];
//            [lblBlockReason setText:curRoomBlocking.roomblocking_reason];
//            [lblBlockRemark setText:curRoomBlocking.roomblocking_remark_physical_check];
//        } else {
//            [blockRoomStatus setHidden:YES];
//            [lblBlockRemark setHidden:YES];
//            [lblBlockReason setHidden:YES];
//        }
//
//        [imgFirstIconCleaningStatus setHidden:NO];
//        [imgSecondIconCleaningStatus setHidden:NO];
//        [lblServiceLaterTime setHidden:YES];
//        [lblRAName setHidden:YES];
//        
//        if (isMockRoom == 1) {
//            [imgFirstIconCleaningStatus setImage:[UIImage imageBeforeiOS7:imgComplete equaliOS7:imgCompleteFlat]];
//            [imgSecondIconCleaningStatus setImage:[UIImage imageWithData:cleaningStatusIcon]];
//        }
//        else if (isReassignedRoom == 1){
//            [imgFirstIconCleaningStatus setImage:[UIImage imageBeforeiOS7:imgPending equaliOS7:imgPendingFlat]];
//            if (cleaningStatus == ENUM_CLEANING_STATUS_PENDING_SECOND_TIME) {
//                [imgSecondIconCleaningStatus setImage:[UIImage imageBeforeiOS7:imgPending equaliOS7:imgPendingFlat]];
//            }
//            else {
//                [imgSecondIconCleaningStatus setImage:[UIImage imageWithData:cleaningStatusIcon]];
//            }
//            
//        }
//        else {
//            [imgSecondIconCleaningStatus setHidden:YES];
//            [imgFirstIconCleaningStatus setImage:[UIImage imageWithData:cleaningStatusIcon]];
//            if (cleaningStatus == ENUM_CLEANING_STATUS_SERVICE_LATER) {
//                [lblServiceLaterTime setHidden:NO];
//                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
//                [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
//                [dateFormat setLocale:[NSLocale currentLocale]];
//                [dateFormat setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
//                NSDate *assignedDate = [dateFormat dateFromString:roomAssignmentTime];
//                [dateFormat setDateFormat:@"HH:mm"];
//                [lblServiceLaterTime setText:[dateFormat stringFromDate:assignedDate]];
//            }
//            else {
//                [lblServiceLaterTime setHidden:YES];
//            }
//        }
//        
//        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
//        
//        return cell;
        
        
        
        FindByRoomCellV2 *cell = nil;
        cell = (FindByRoomCellV2 *)[tableView dequeueReusableCellWithIdentifier:cellIdentify];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        if (cell == nil) {
            NSArray *arrayCell = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([FindByRoomCellV2 class]) owner:self options:nil];
            cell = [arrayCell objectAtIndex:0];
            [cell.lblRoomStatus setFont:[UIFont fontWithName:font size:sizeNormal]];
            [cell.lblRoomNo setFont:[UIFont fontWithName:font size:sizeNormal]];
            [cell.lblVIP setFont:[UIFont fontWithName:font size:sizeNormal]];
        }
        
        NSDictionary *rowData = [self.listDisplayData objectAtIndex:indexPath.row];
        
        UILabel *lblRoomNoCell = (UILabel *)[cell viewWithTag:tagRoomNo];
        UILabel *lblRoomStatusCell = (UILabel *)[cell viewWithTag:tagRMStatus];
        UILabel *lblVipRoom = (UILabel *)[cell viewWithTag:tagVIP];
        UIImageView *imgFirstIconCleaningStatus = (UIImageView *)[cell viewWithTag:tagFirstIconCleaningStatus];
        UIImageView *imgSecondIconCleaningStatus = (UIImageView *)[cell viewWithTag:tagSecondIconCleaningStatus];
        UILabel *lblServiceLaterTime = (UILabel *)[cell viewWithTag:tagTimeServiceLater];
        UIImageView *imgRushOrQueueRoom = (UIImageView *)[cell viewWithTag:tagRushOrQueueRoom];
        UILabel *lblDueOutRoom = (UILabel *)[cell viewWithTag:tagDueOutRoom];
        UILabel *lblDepartureTime = (UILabel *)[cell viewWithTag:tagDepartureTime];
        UILabel *lblDueInRoom = (UILabel *)[cell viewWithTag:tagDueInRoom];
        UILabel *lblArrivalTime = (UILabel *)[cell viewWithTag:tagArrivalTime];
        UILabel *lblGuestName = (UILabel *)[cell viewWithTag:tagGuestName];
        UILabel *lblRAName = (UILabel *)[cell viewWithTag:tagRAName];
        UILabel *lblRoomType = (UILabel *)[cell viewWithTag:tagRoomType];
        UIImageView *imgProfileNotes = (UIImageView *)[cell viewWithTag:tagProfileNotes]; // CFG [20160928/CRF-00000827]
        UIImageView *imgStayOver = (UIImageView *)[cell viewWithTag:tagStayOver]; // CRF [20161106/CRF-00001293]
        NSString *roomNo = (NSString *)[rowData objectForKey:kRoomNo];
        NSString *guestName = [rowData objectForKey:kGuestName];
        NSString *vip = (NSString *)[rowData objectForKey:kVIP];
        NSInteger cleaningStatus = [(NSString *)[rowData objectForKey:kCleaningStatusID] integerValue];
        NSString *roomStatus = (NSString *)[rowData objectForKey:kRMStatus];
        NSString *guestArrivalTime = (NSString *)[rowData objectForKey:kRaGuestArrivalTime];
        NSString *guestDepartureTime = (NSString *)[rowData objectForKey:kRaGuestDepartureTime];
        NSInteger kindOfRoom = [(NSString *)[rowData objectForKey:kRaKindOfRoom] integerValue];
        NSInteger isMockRoom = [(NSString *)[rowData objectForKey:kRaIsMockRoom] integerValue];
        NSInteger isReassignedRoom = [(NSString *)[rowData objectForKey:kRaIsReassignedRoom] integerValue];
        NSData *cleaningStatusIcon = (NSData *)[rowData objectForKey:kCleaningStatus];
        NSString *roomAssignmentTime = (NSString *)[rowData objectForKey:kRoomAssignmentTime];
        NSString *roomTypeValue = (NSString *)[rowData objectForKey:kRoomType];
        lblRoomType.text = roomTypeValue;
        [lblRoomNoCell setText:roomNo];
        [lblRoomStatusCell setText:roomStatus];
        [lblVipRoom setText:(vip == nil || [vip isEqualToString:@""] || [vip isEqualToString:@"0"])?@"-":vip];
        [lblDueOutRoom setText:@"[D/O]"];
        [lblArrivalTime setText:guestArrivalTime];
        [lblGuestName setText:(guestName == nil || [guestName isEqualToString:@""])?@"-":guestName];
        
        // CFG [20160913/CRF-00001439] - Changed to binary AND operation.
        if ((kindOfRoom & ENUM_KIND_OF_ROOM_RUSH) == ENUM_KIND_OF_ROOM_RUSH) {
            [imgRushOrQueueRoom setHidden:NO];
            [imgRushOrQueueRoom setImage:[UIImage imageBeforeiOS7:imgRushRoom equaliOS7:imgRushRoomFlat]];
        }
        else if ((kindOfRoom & ENUM_KIND_OF_ROOM_QUEUE) == ENUM_KIND_OF_ROOM_QUEUE) {
            [imgRushOrQueueRoom setHidden:NO];
            [imgRushOrQueueRoom setImage:[UIImage imageBeforeiOS7:imgQueueRoom equaliOS7:imgQueueRoomFlat]];
        }
        else {
//            if([LogFileManager isLogConsole])
//            {
//                NSLog(@"OUT OF VALUE RANGE");
//            }
            [imgRushOrQueueRoom setHidden:YES];
        }
        // CFG [20160913/CRF-00001439] - End.
        
        // CFG [20160928/CRF-00000827] - Show notes icon.
        if ((kindOfRoom & ENUM_KIND_OF_ROOM_PROFILE_NOTES_EXISTS) == ENUM_KIND_OF_ROOM_PROFILE_NOTES_EXISTS) {
            [imgProfileNotes setHidden:NO];
        }
        else {
            [imgProfileNotes setHidden:YES];
        }
        // CFG [20160928/CRF-00000827] - End.
        
        // CRF [20161106/CRF-00001293] - Show notes icon.
        if ((kindOfRoom & ENUM_KIND_OF_ROOM_STAY_OVER_EXISTS) == ENUM_KIND_OF_ROOM_STAY_OVER_EXISTS)  {
            [imgStayOver setHidden:NO];
            if(imgProfileNotes.isHidden == NO) {
                [imgStayOver setFrame:CGRectMake(imgProfileNotes.frame.origin.x - imgProfileNotes.frame.size.width, imgProfileNotes.frame.origin.y, imgProfileNotes.frame.size.width, imgProfileNotes.frame.size.height)];
            }
        }
        else {
            [imgStayOver setHidden:YES];
        }
        // CRF [20161106/CRF-00001293] - End.
        int roomIndicatorGuest = [self getRoomIndicatorGuestByArrivalDate:guestArrivalTime departureDate:guestDepartureTime];
        
        if(roomIndicatorGuest == RoomIndicatorGuestDueIn) { //light yellow green color
            [cell setBackgroundColor:[UIColor colorWithRed:253/255.0f green:255/255.0f blue:206/255.0f alpha:1]];
            
            [lblDueInRoom setHidden:YES];
            [lblArrivalTime setHidden:YES];
            
            //Warning! Temporary use label Due Out and label departure time for Arrival time
            //[lblDueOutRoom setFrame:lblDueOutFrame];//Hao Tran - Remove for uneccessary frame for cell in HomeViewV2
            
            //Change frames for Due Out Infor
            //Decreasing width of Guest Infor
            if(lblDepartureTime.isHidden /*&& lblDueOutRoom.isHidden*/) {
                CGRect frlblDueOut = lblDueOutRoom.frame;
                CGRect frlblDepartureTime = lblDepartureTime.frame;
                CGRect frlblGuestName = lblGuestName.frame;
                frlblGuestName.size.width -= (frlblDueOut.size.width + frlblDepartureTime.size.width + 2);
                frlblGuestName.origin.x += (frlblDueOut.size.width + frlblDepartureTime.size.width + 2);
                [lblGuestName setFrame:frlblGuestName];
                [lblDueOutRoom setHidden:NO];
                [lblDepartureTime setHidden:NO];
            }
            
            [lblDueOutRoom setText:@"[D/I]"];
            [lblDepartureTime setText:[ehkConvert DateToStringWithString:guestArrivalTime fromFormat:@"yyyy-MM-dd HH:mm:ss" toFormat:@"HH:mm"]];
            //[lblGuestName setFrame:lblGuestNameFrameInCase3];
            [lblGuestName setHidden:YES];
            
        }
        else if(roomIndicatorGuest == RoomIndicatorGuestDueOut) { //light green color
            [cell setBackgroundColor:[UIColor colorWithRed:172/255.0f green:255/255.0f blue:203/255.0f alpha:1]];
            
            if(guestName.length > 0) {
                [lblGuestName setHidden:NO];
                [lblGuestName setText:guestName];
            } else {
                //No need to check guest for displaying green background - matching with Android
                //[cell setBackgroundColor:[UIColor whiteColor]];
                [lblGuestName setHidden:YES];
            }
            
            [lblDueInRoom setHidden:YES];
            [lblArrivalTime setHidden:YES];
            
            //[lblDueOutRoom setFrame:lblDueOutFrame];//Hao Tran - Remove for uneccessary frame for cell in HomeViewV2
            
            //Change frames for Due Out Infor
            //Decreasing width of Guest Infor
            if(lblDepartureTime.isHidden /*&& lblDueOutRoom.isHidden*/) {
                CGRect frlblDueOut = lblDueOutRoom.frame;
                CGRect frlblDepartureTime = lblDepartureTime.frame;
                CGRect frlblGuestName = lblGuestName.frame;
                frlblGuestName.size.width -= (frlblDueOut.size.width + frlblDepartureTime.size.width + 2);
                frlblGuestName.origin.x += (frlblDueOut.size.width + frlblDepartureTime.size.width + 2);
                [lblGuestName setFrame:frlblGuestName];
                [lblDueOutRoom setHidden:NO];
                [lblDepartureTime setHidden:NO];
            }
            
            [lblDueOutRoom setText:@"[D/O]"];
            [lblDepartureTime setText:[ehkConvert DateToStringWithString:guestDepartureTime fromFormat:@"yyyy-MM-dd HH:mm:ss" toFormat:@"HH:mm"]];
            //[lblGuestName setFrame:lblGuestNameFrameInCase3];
            
        }
        else if(roomIndicatorGuest == RoomIndicatorGuestBackToBack) { //light green color
            [cell setBackgroundColor:[UIColor colorWithRed:172/255.0f green:255/255.0f blue:203/255.0f alpha:1]];
            
            //Change frames for Due Out Infor
            //Decreasing width of Guest Infor
            if(lblDepartureTime.isHidden /*&& lblDueOutRoom.isHidden*/) {
                CGRect frlblDueOut = lblDueOutRoom.frame;
                CGRect frlblDepartureTime = lblDepartureTime.frame;
                CGRect frlblGuestName = lblGuestName.frame;
                frlblGuestName.size.width -= (frlblDueOut.size.width + frlblDepartureTime.size.width + 2);
                frlblGuestName.origin.x += (frlblDueOut.size.width + frlblDepartureTime.size.width + 2);
                [lblGuestName setFrame:frlblGuestName];
                [lblDueOutRoom setHidden:NO];
                [lblDepartureTime setHidden:NO];
            }
            
            [lblDueOutRoom setText:@"[D/O]"];
            [lblDepartureTime setText:[ehkConvert DateToStringWithString:guestDepartureTime fromFormat:@"yyyy-MM-dd HH:mm:ss" toFormat:@"HH:mm"]];
            [lblDueInRoom setHidden:NO];
            [lblDueInRoom setText:@"[D/I]"];
            [lblArrivalTime setHidden:NO];
            [lblArrivalTime setText:[ehkConvert DateToStringWithString:guestArrivalTime fromFormat:@"yyyy-MM-dd HH:mm:ss" toFormat:@"HH:mm"]];
            
            //[lblGuestName setFrame:lblGuestNameFrameInCase3];
            if(guestName.length > 0) {
                [lblGuestName setHidden:NO];
                [lblGuestName setText:guestName];
            } else {
                [lblGuestName setHidden:YES];
            }
            
        }
        else { //Normal
            [cell setBackgroundColor:[UIColor whiteColor]];
            
            //Change frames for Due Out Infor
            //Increasing width of Guest Infor
            if(!lblDepartureTime.isHidden) {
                CGRect frlblDueOut = lblDueOutRoom.frame;
                CGRect frlblDepartureTime = lblDepartureTime.frame;
                CGRect frlblGuestName = lblGuestName.frame;
                frlblGuestName.size.width += (frlblDueOut.size.width + frlblDepartureTime.size.width + 2);
                frlblGuestName.origin.x -= (frlblDueOut.size.width + frlblDepartureTime.size.width + 2);
                [lblGuestName setFrame:frlblGuestName];
                [lblDepartureTime setHidden:YES];
                [lblDueOutRoom setHidden:YES];
            }
            
            [lblDueInRoom setHidden:YES];
            [lblArrivalTime setHidden:YES];
            
            if(guestName.length > 0) {
                [lblGuestName setHidden:NO];
                [lblGuestName setText:guestName];
            } else {
                [lblGuestName setHidden:YES];
                [lblDueOutRoom setText:@"-"];
            }
        }
        
        [cell setAccessoryType:UITableViewCellAccessoryNone];
        
        //CRF-0000682: blocking room sign, remark and reason
        RoomBlocking *curRoomBlocking = [[RoomManagerV2 sharedRoomManager] loadRoomBlockingByUserId:[UserManagerV2 sharedUserManager].currentUser.userId roomNumber:roomNo];
        UIImageView *blockRoomStatus = (UIImageView*)[cell.contentView viewWithTag:tagBlockingImage];
        UILabel *lblBlockReason = (UILabel*)[cell.contentView viewWithTag:tagBlockingReason];
        UILabel *lblBlockRemark = (UILabel*)[cell.contentView viewWithTag:tagBlockingRemark];
        UILabel *lblAddReason = (UILabel*)[cell.contentView viewWithTag:tagAddition];
        
        if(curRoomBlocking.roomblocking_is_blocked > 0 && !isDueRoom  && guestName.length == 0 )
        {
            if(curRoomBlocking.roomblocking_is_blocked == RoomBlockingStatus_Release) {
                [blockRoomStatus setImage:[UIImage imageNamed:imgReleaseRoom]];
            } else {
                [blockRoomStatus setImage:[UIImage imageNamed:imgBlockRoom]];
            }
            
            [blockRoomStatus setHidden:NO];
            
            if(isHaveReasonRemark){
                NSString *reason = [@"Reason: " stringByAppendingString:curRoomBlocking.roomblocking_reason];
                NSString *remark = [@"Remark: " stringByAppendingString:curRoomBlocking.roomblocking_remark_physical_check];
                [lblAddReason setHidden:NO];
                [lblBlockReason setHidden:NO];
                [lblBlockRemark setHidden:YES];
                [lblAddReason setText:reason];
                [lblBlockReason setText:remark];
            }
            else{
                [lblAddReason setHidden:YES];
                [lblBlockReason setHidden:YES];
                [lblBlockRemark setHidden:YES];
            }
            [lblGuestName setHidden:YES];
        }
        else if (curRoomBlocking.roomblocking_is_blocked > 0 && (isDueRoom || guestName.length > 0)){
            
            if(curRoomBlocking.roomblocking_is_blocked == RoomBlockingStatus_Release) {
                [blockRoomStatus setImage:[UIImage imageNamed:imgReleaseRoom]];
            } else {
                [blockRoomStatus setImage:[UIImage imageNamed:imgBlockRoom]];
            }
            
            [blockRoomStatus setHidden:NO];
            
            if(isHaveReasonRemark){
                NSString *reason = [@"Reason: " stringByAppendingString:curRoomBlocking.roomblocking_reason];
                NSString *remark = [@"Remark: " stringByAppendingString:curRoomBlocking.roomblocking_remark_physical_check];
                [lblAddReason setHidden:YES];
                [lblBlockReason setHidden:NO];
                [lblBlockRemark setHidden:NO];
                [lblBlockReason setText:reason];
                [lblBlockRemark setText:remark];
            }
            else{
                [lblAddReason setHidden:YES];
                [lblBlockReason setHidden:YES];
                [lblBlockRemark setHidden:YES];
            }
            [lblGuestName setHidden:NO];
        }
        else {
            [blockRoomStatus setHidden:YES];
            [lblBlockRemark setHidden:YES];
            [lblBlockReason setHidden:YES];
            [lblAddReason setHidden:YES];
            [lblGuestName setHidden:NO];
        }
        
        [imgFirstIconCleaningStatus setHidden:NO];
        [imgSecondIconCleaningStatus setHidden:NO];
        [lblServiceLaterTime setHidden:YES];
        [lblRAName setHidden:YES];
        
        if (isMockRoom == 1) {
            [imgFirstIconCleaningStatus setImage:[UIImage imageBeforeiOS7:imgComplete equaliOS7:imgCompleteFlat]];
            [imgSecondIconCleaningStatus setImage:[UIImage imageWithData:cleaningStatusIcon]];
        }
        else if (isReassignedRoom == 1){
            [imgFirstIconCleaningStatus setImage:[UIImage imageBeforeiOS7:imgPending equaliOS7:imgPendingFlat]];
            //            if (cleaningStatus == ENUM_CLEANING_STATUS_PENDING_SECOND_TIME) {
            //                [imgSecondIconCleaningStatus setImage:[UIImage imageNamed:@"icon_pending.png"]];
            //            }
            //            else {
            //                [imgSecondIconCleaningStatus setImage:[UIImage imageWithData:cleaningStatusIcon]];
            //            }
            [imgSecondIconCleaningStatus setImage:[UIImage imageWithData:cleaningStatusIcon]];
        }
        else {
            [imgSecondIconCleaningStatus setHidden:YES];
            [imgFirstIconCleaningStatus setImage:[UIImage imageWithData:cleaningStatusIcon]];
            if (cleaningStatus == ENUM_CLEANING_STATUS_SERVICE_LATER) {
                [lblServiceLaterTime setHidden:NO];
                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                [dateFormat setLocale:[NSLocale currentLocale]];
                [dateFormat setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
                NSDate *assignedDate = [dateFormat dateFromString:roomAssignmentTime];
                [dateFormat setDateFormat:@"HH:mm"];
                [lblServiceLaterTime setText:[dateFormat stringFromDate:assignedDate]];
            }
            else {
                [lblServiceLaterTime setHidden:YES];
            }
        }
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
        
        return cell;
    }
    
    return cell;
}


//- (void)addAccessoryButton:(UITableViewCell *)cell  andIndexPath:(NSIndexPath *)indexPath{
//    /*****************************************/
//    UIButton *accessoryButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 24, 24)];
//    [accessoryButton setBackgroundColor:[UIColor clearColor]];
//    //    [accessoryButton setImage:[UIImage imageNamed:@"gray_arrow_36x36.png"] forState:UIControlStateNormal];
//    [accessoryButton setImage:[UIImage imageNamed:@"gray_arrow_36x36.png"] forState:UIControlStateNormal];
//    [accessoryButton setTag:indexPath.row];
//    
//    [accessoryButton addTarget:self action:@selector(btnDetailPressed:) forControlEvents:UIControlEventTouchUpInside];
//    
//    [cell setAccessoryView:accessoryButton];
//    //cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
//    /*****************************************/
//    
//}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //    if (tableView == tbvContent) {
    //        [self btnDetailArrowPressed:indexPath.row];
    //    }
    
    if([UserManagerV2 isSupervisor] && [listDisplayData count] > 0)
    {
        NSDictionary *roomAssignmentdata =[listDisplayData objectAtIndex:indexPath.row];
        
        NSString *roomNo = [roomAssignmentdata objectForKey:kRoomNo];
        [RoomManagerV2 setCurrentRoomNo:roomNo];
        [TasksManagerV2 setCurrentRoomAssignment:[[roomAssignmentdata objectForKey:kRoomAssignmentID] integerValue]];
        
        RoomAssignmentInfoViewController *viewController = [[RoomAssignmentInfoViewController alloc] initWithOption:IS_PUSHED_FROM_FIND_BY_ROOM andRoomAssignmentData:roomAssignmentdata];
        viewController.isLoadFromFind = YES;
        [self.navigationController pushViewController:viewController animated:YES];
        
        [self.navigationItem.backBarButtonItem setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK]];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationSelectedFind] object:nil];
        
        self.isLoadFromWS = FALSE;
        [[HomeViewV2 shareHomeView] setSelectedButton:tagOfFindButton];
    }
    
}


#pragma mark - UISearchBar Delegate Methods
-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBarA {
    [searchBarA setShowsCancelButton:YES animated:NO];
    for(UIView *subView in searchBarA.subviews){
        if([subView isKindOfClass:UIButton.class])
        {
            UIButton *cancelButton = (UIButton*)subView;
            [cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            UIColor *tintColor = COLOR_CANCEL_SEARCH_BAR;
            [cancelButton setTintColor:tintColor];
            [cancelButton setTitle:[[LanguageManagerV2 sharedLanguageManager] getCancel] forState:UIControlStateNormal];
        }
    }
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBarA {
    [searchBarA setShowsCancelButton:NO animated:NO];
    
    [searchBarA resignFirstResponder];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBarA {
    [searchBarA setShowsCancelButton:NO animated:NO];
    [searchBarA resignFirstResponder];
    [searchBarA setText:@""];
    [self searchBar:searchBarA textDidChange:@""];
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    if ([searchText isEqualToString:@""] || (!searchText)) {
        [listDisplayData removeAllObjects];
        [listDisplayData addObjectsFromArray:listTempData];
        [tbvNoResult setHidden:YES];
        [tbvContent setHidden:NO];
    } else {
        [listDisplayData removeAllObjects];
        
        for (NSDictionary *rowData in listTempData) {
            NSString *roomNo = [rowData objectForKey:kRoomNo];
            NSString *roomStatus = [rowData objectForKey:kRMStatus];
            NSString *vip = [rowData objectForKey:kVIP];
            
            NSRange r = [roomNo rangeOfString:searchText
                                      options:NSCaseInsensitiveSearch];
            
            NSRange r1 = [roomStatus rangeOfString:searchText
                                           options:NSCaseInsensitiveSearch];
            
            NSRange r2 = [vip rangeOfString:searchText
                                    options:NSCaseInsensitiveSearch];
            
            if(r.location != NSNotFound || r1.location != NSNotFound
               || r2.location != NSNotFound) {
                [listDisplayData addObject:rowData];
                [tbvNoResult setHidden:YES];
                [tbvContent setHidden:NO];
            }
        }
    }
    
    if([listDisplayData count] <= 0) {
        [tbvContent setHidden:YES];
        [tbvNoResult setHidden:NO];
    }
    
    if([listDisplayData count]>1) {
        for(int i = 0; i < [listDisplayData count]; i++) {
            for(int j = i + 1; j <= [listDisplayData count] - 1; j++) {
                NSDictionary *row1 = [listDisplayData  objectAtIndex:i];
                NSString *roomNo1 = [row1 objectForKey:kRoomNo];
                
                NSDictionary *row2 = [listDisplayData  objectAtIndex:j];
                NSString *roomNo2 = [row2 objectForKey:kRoomNo];
                
                if([roomNo1 isEqualToString:roomNo2]) {
                    [listDisplayData removeObjectAtIndex:j];
                }
            }
        }
    }
    
    [tbvContent reloadData];
}
-(void) hiddenHUDAfterSaved:(MBProgressHUD *)HUD{
    [HUD hide:YES];
    [HUD removeFromSuperview];
    
}
#pragma mark - Load Data
-(void)loadData {
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getLoadingData]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES];
    
    NSLog(@"roomStatusID =%d",(int)roomStatusID);
    [[HomeViewV2 shareHomeView] waitingLoadingData];
    
    /*
     if(isLoadFromWS) {
     [UnassignManagerV2 		FromWSWith:[UserManagerV2 sharedUserManager].currentUser andLastModified:nil AndPercentView:HUD];
     if(roomStatusID == inspectedCase && filterType == roomStatusType)
     {
     //Inspected case = OI + VI
     [[RoomManagerV2 sharedRoomManager] GetFindRoomAssignmentListWSByUserID:[[[UserManagerV2 sharedUserManager]currentUser]userId] AndRoomstatus:room_Status_Id_VI AndFilterType:filterType AndLastModified:nil AndPercentView:HUD];//VI
     [[RoomManagerV2 sharedRoomManager] GetFindRoomAssignmentListWSByUserID:[[[UserManagerV2 sharedUserManager]currentUser]userId] AndRoomstatus:room_Status_Id_OI AndFilterType:filterType AndLastModified:nil AndPercentView:HUD];//OI
     listTempData = [UnassignManagerV2 loadFloorsHaveFindInspectedRoom:[[[UserManagerV2 sharedUserManager] currentUser] userId]];
     }
     else
     {
     [[RoomManagerV2 sharedRoomManager] GetFindRoomAssignmentListWSByUserID:[[[UserManagerV2 sharedUserManager]currentUser]userId] AndRoomstatus:roomStatusID AndFilterType:filterType AndLastModified:nil AndPercentView:HUD];
     switch (filterType) {
     case roomStatusType:
     {
     listTempData = [UnassignManagerV2 loadFloorsHaveFindRoom:[[[UserManagerV2 sharedUserManager] currentUser] userId] andRoomStatusID:roomStatusID];
     break;
     }
     
     case cleaningStatusType:
     {
     listTempData = [UnassignManagerV2 loadFloorsHaveFindCleaningStatus:[[[UserManagerV2 sharedUserManager] currentUser] userId] andCleaningStatusID:roomStatusID];
     break;
     }
     default:
     break;
     }
     }
     
     
     [tbvContent reloadData];
     }*/
    
    
    if(isLoadFromWS) {
        if(roomStatusID == -1) {
        }
        else {
            NSString *hotelId = [NSString stringWithFormat:@"%d", [[[UserManagerV2 sharedUserManager] currentUser] userHotelsId]];
            [[RoomManagerV2 sharedRoomManager] GetFindRoomAssignmentListWSByUserID:[[[UserManagerV2 sharedUserManager]currentUser]userId] AndRoomstatus:roomStatusID AndFilterType:filterType AndLastModified:nil AndHotelID:hotelId AndPercentView:HUD];
            
            if(filterType == unassignType)
            {
                listTempData = [[RoomManagerV2 sharedRoomManager] getUnassginRoomsByUserId:[UserManagerV2 sharedUserManager].currentUser.userId] ;
            }
            else if(filterType == kindOfRoomType)
            {
                listTempData = [[RoomManagerV2 sharedRoomManager]getAllRoomByRoomByUserId:[[[UserManagerV2 sharedUserManager] currentUser] userId] AndKindOfRoom:roomStatusID];
            }
            else
            {
                self.listTempData = [NSMutableArray arrayWithArray:[[RoomManagerV2 sharedRoomManager] getAllRoomInspectionWithSupervisorId:[[[UserManagerV2 sharedUserManager] currentUser] userId] andRoomStatusID:roomStatusID]];
            }
            
        }
        
    }
    
    self.listDisplayData = [NSMutableArray arrayWithArray:listTempData];
    
    if([listDisplayData count] > 1) {
        for(int i = 0; i < [listDisplayData count]; i++) {
            for(int j = i + 1; j <[listDisplayData count]; j++) {
                NSDictionary *row1 = [listDisplayData  objectAtIndex:i];
                NSString *roomNo1 = [row1 objectForKey:kRoomNo];
                
                NSDictionary *row2 = [listDisplayData objectAtIndex:j];
                NSString *roomNo2 = [row2 objectForKey:kRoomNo];
                
                if([roomNo1 isEqualToString:roomNo2]) {
                    [listDisplayData removeObjectAtIndex:j];
                }
            }
        }
    }
    
    if([listDisplayData count] <= 0) {
        [tbvContent setHidden:YES];
        [tbvNoResult setHidden:NO];
    } else {
        [tbvContent setHidden: NO];
        [tbvNoResult setHidden:YES];
        [tbvContent reloadData];
    }
    [self performSelector:@selector(hiddenHUDAfterSaved:) withObject:HUD afterDelay:0.5];
    
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
}

//#pragma mark - Action for Button Detail Arrow
//-(void) btnDetailPressed:(UIButton *) sender {
//    [self btnDetailArrowPressed:sender.tag];
//}
//
//-(void)btnDetailArrowPressed:(NSInteger)index {
//    //    NSInteger index = sender.tag;
//    
//    NSDictionary *rowData =[self.listDisplayData objectAtIndex:index];
//    
//    RoomModelV2 *roomModel = [[RoomModelV2 alloc] init];
//    roomModel.room_Id = [rowData objectForKey:kRoomNo];
//    [[RoomManagerV2 sharedRoomManager] loadRoomModel:roomModel];
//    
//    GuestInfoModelV2 *guestInfoModel = [[GuestInfoModelV2 alloc] init];
//    guestInfoModel.guestRoomId = roomModel.room_Id;
//    [[RoomManagerV2 sharedRoomManager] loadByRoomIDGuestInfo:guestInfoModel];
//    
//    //load room assignment model
//    RoomAssignmentModelV2 *ramodel = [[RoomAssignmentModelV2 alloc] init];
//    ramodel.roomAssignment_Id = [[rowData objectForKey:kRoomAssignmentID] integerValue];
//    ramodel.roomAssignment_UserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
//    [[RoomManagerV2 sharedRoomManager] load:ramodel];
//    
//    //set current room no
//    NSString *roomNo = [rowData objectForKey:kRoomNo];
//    [RoomManagerV2 setCurrentRoomNo:roomNo];
//    
//    UIViewController *aRoomInfo = nil;
//    
//    NSInteger inspectionStatusId = [[rowData objectForKey:KInspection_Status_id] integerValue];
//    
//    if (inspectionStatusId == ENUM_INSPECTION_COMPLETED_PASS) {
//        //Room is pass
//        aRoomInfo = [[RoomInfoInspectedPassedV2 alloc] initWithNibName:@"RoomInfoInspectedPassedV2" bundle:nil];
//    } else if (inspectionStatusId == ENUM_INSPECTION_COMPLETED_FAIL) {
//        //Room is fail
//        aRoomInfo = [[RoomInfoInspectedFailedV2 alloc] initWithNibName:@"RoomInfoInspectedFailedV2" bundle:nil];
//    } else if (inspectionStatusId == ENUM_INSPECTION_PAUSE || inspectionStatusId == ENUM_INSPECTION_PENDING || inspectionStatusId == ENUM_INSPECTION_STARTED) {
//        //Room is pause, pending or start
//        
//        //handle room status
//        NSString *roomStatus = [rowData objectForKey:kRMStatus];
//        
//        if ([roomStatus isEqualToString:ROOM_STATUS_OCI] || [roomStatus isEqualToString:ROOM_STATUS_VCI] || [roomStatus isEqualToString:ROOM_STATUS_OOO] || [roomStatus isEqualToString:ROOM_STATUS_OOS]) {
//            //Room is passed, but inspection status is not completed
//            aRoomInfo = [[RoomInfoInspectedPassedV2 alloc] initWithNibName:@"RoomInfoInspectedPassedV2" bundle:nil];
//        }
//        
//        if ([roomStatus isEqualToString:ROOM_STATUS_VD] || [roomStatus isEqualToString:ROOM_STATUS_OD]) {
//            //Room is not cleaned, supervisor can only view this room
//            aRoomInfo = [[RoomInfoAssignmentV2 alloc] initWithNibName:@"RoomInfoAssignmentV2" bundle:nil];
//            if ([aRoomInfo respondsToSelector:@selector(disableUserInteractionInView)]) {
//                [(id)aRoomInfo disableUserInteractionInView];
//            }
//        }
//        
//        if ([roomStatus isEqualToString:ROOM_STATUS_OC] || [roomStatus isEqualToString:ROOM_STATUS_VC] || [roomStatus isEqualToString:ROOM_STATUS_OPU] || [roomStatus isEqualToString:ROOM_STATUS_VPU]) {
//            //Can inspected this room
//            aRoomInfo = [[RoomInfoInspectionV2 alloc] initWithNibName:@"RoomInfoInspectionV2" bundle:nil];
//        }
//    }
//    
//    [(id)aRoomInfo setRoomName : [rowData objectForKey:kRoomNo]];
//    [(id)aRoomInfo setRoomModel : roomModel];
//    [(id)aRoomInfo setGuestInfoModel : guestInfoModel];
//    [(id)aRoomInfo setRaDataModel:ramodel];
//    
//    //set current RoomAssignmentId
//    [TasksManagerV2 setCurrentRoomAssignment:[[rowData objectForKey:kRoomAssignmentID] integerValue]];
//    
//    // set Not loading from WS, get data from local
//    self.isLoadFromWS = FALSE;
//    
//    [self.navigationController pushViewController:aRoomInfo animated:YES];
//    
//    //set selected button is find in bottom bar
//    [[HomeViewV2 shareHomeView] setSelectedButton:tagOfFindButton];
//    
//    [(id)aRoomInfo loadingData];
//}

#pragma mark - Action for Button Room Status
-(void)loadAllRoomStatusAndCleaningStatus
{
    //Hard code
    nameInspected = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_find_room_inspected];//@"Inspected";
    nameRushRoom = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_find_room_rush_room];//@"Rush Room";
    nameQueueRoom = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_find_room_queue_room]; //@"Queue Room";
    nameUnassignRoom = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_find_room_unassign]; //@"Unassign";
    nameOOS = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_find_room_oos]; //@"OOS";
    nameOccupiedDueOut = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_find_room_occupied_due_out]; //@"Occupied Due-Out";
    nameODDO = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_find_room_od_od]; //@"OD-DO"
    nameProfileNotesExist = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_find_room_profile_notes_exist];
    nameStayOver = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_find_room_stay_over];
    nameDoubleLock = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_find_room_double_lock];
    bool isEnglish = [[LanguageManagerV2 sharedLanguageManager] currentLanguageIsEqualTo:@"%@", ENGLISH_LANGUAGE];
    NSMutableArray *listRoomStatus = [[RoomManagerV2 sharedRoomManager] loadAllRoomStatus];
    NSMutableArray *listCleaningStatus = [[RoomManagerV2 sharedRoomManager] loadAllCleaningStatus];
    if(listRoomStatus.count <= 0){
        nameVC = @"VC";
        nameOC = @"OC";
        nameVD = @"VD";
        nameOD = @"OD";
        nameVI = @"VI";
        nameOI = @"OI";
        nameOOS = @"OOS";
        nameOOO = @"OOO";
        nameVPU = @"VPU";
        nameOPU = @"OPU";
        nameOCSO = @"OC-SO";
    } else {
        if(isEnglish){
            for (int i = 0; i < listRoomStatus.count; i ++) {
                RoomStatusModelV2 *curRoomStatus = [listRoomStatus objectAtIndex:i];
                // Not add Room Status to the list if rstat_FindStatus = 0
                if (curRoomStatus.rstat_FindStatus == 0) {
                    //                    continue;
                }
                if(curRoomStatus.rstat_Id == ENUM_ROOM_STATUS_VC){
                    nameVC = curRoomStatus.rstat_Name;
                } else if(curRoomStatus.rstat_Id == ENUM_ROOM_STATUS_OC){
                    nameOC = curRoomStatus.rstat_Name;
                } else if(curRoomStatus.rstat_Id == ENUM_ROOM_STATUS_VD){
                    nameVD = curRoomStatus.rstat_Name;
                } else if(curRoomStatus.rstat_Id == ENUM_ROOM_STATUS_OD){
                    nameOD = curRoomStatus.rstat_Name;
                } else if(curRoomStatus.rstat_Id == ENUM_ROOM_STATUS_VI){
                    nameVI = curRoomStatus.rstat_Name;
                } else if(curRoomStatus.rstat_Id == ENUM_ROOM_STATUS_OI){
                    nameOI = curRoomStatus.rstat_Name;
                } else if(curRoomStatus.rstat_Id == ENUM_ROOM_STATUS_OOS){
                    nameOOS = curRoomStatus.rstat_Name;
                } else if(curRoomStatus.rstat_Id == ENUM_ROOM_STATUS_OOO){
                    nameOOO = curRoomStatus.rstat_Name;
                } else if(curRoomStatus.rstat_Id == ENUM_ROOM_STATUS_VPU){
                    nameVPU = curRoomStatus.rstat_Name;
                } else if(curRoomStatus.rstat_Id == ENUM_ROOM_STATUS_OPU) {
                    nameOPU = curRoomStatus.rstat_Name;
                } else if(curRoomStatus.rstat_Id == ENUM_ROOM_STATUS_OC_SO){
                    nameOCSO = curRoomStatus.rstat_Name;
                }
            }
        } else {
            for (int i = 0; i < listRoomStatus.count; i ++) {
                RoomStatusModelV2 *curRoomStatus = [listRoomStatus objectAtIndex:i];
                // Not add Room Status to the list if rstat_FindStatus = 0
                if (curRoomStatus.rstat_FindStatus == 0) {
                    //                    continue;
                }
                if(curRoomStatus.rstat_Id == ENUM_ROOM_STATUS_VC){
                    nameVC = curRoomStatus.rstat_Lang;
                } else if(curRoomStatus.rstat_Id == ENUM_ROOM_STATUS_OC){
                    nameOC = curRoomStatus.rstat_Lang;
                } else if(curRoomStatus.rstat_Id == ENUM_ROOM_STATUS_VD){
                    nameVD = curRoomStatus.rstat_Lang;
                } else if(curRoomStatus.rstat_Id == ENUM_ROOM_STATUS_OD){
                    nameOD = curRoomStatus.rstat_Lang;
                } else if(curRoomStatus.rstat_Id == ENUM_ROOM_STATUS_VI){
                    nameVI = curRoomStatus.rstat_Lang;
                } else if(curRoomStatus.rstat_Id == ENUM_ROOM_STATUS_OI){
                    nameOI = curRoomStatus.rstat_Lang;
                } else if(curRoomStatus.rstat_Id == ENUM_ROOM_STATUS_OOS){
                    nameOOS = curRoomStatus.rstat_Lang;
                } else if(curRoomStatus.rstat_Id == ENUM_ROOM_STATUS_OOO){
                    nameOOO = curRoomStatus.rstat_Lang;
                } else if(curRoomStatus.rstat_Id == ENUM_ROOM_STATUS_VPU){
                    nameVPU = curRoomStatus.rstat_Lang;
                } else if(curRoomStatus.rstat_Id == ENUM_ROOM_STATUS_OPU) {
                    nameOPU = curRoomStatus.rstat_Lang;
                } else if(curRoomStatus.rstat_Id == ENUM_ROOM_STATUS_OC_SO){
                    nameOCSO = curRoomStatus.rstat_Lang;
                }
            }
        }
    }
    
    
    if(listCleaningStatus.count <= 0){
        namePending = @"Pending";
        nameDND = @"DND";
        nameServiceLater = @"Service Later";
        nameDeclinedService = @"Declined Service";
        nameStarted = @"Start";
        nameStop = @"Stop";
        nameCompleted = @"Completed";
        nameReassignPending = @"Reassign Pending";
    } else {
        if(isEnglish){
            for (int i = 0; i < listCleaningStatus.count; i ++) {
                CleaningStatusModelV2 *curCleaningStatus = [listCleaningStatus objectAtIndex:i];
                if(curCleaningStatus.cstat_Id == ENUM_CLEANING_STATUS_PENDING_FIRST_TIME){
                    namePending = curCleaningStatus.cstat_Name;
                } else if(curCleaningStatus.cstat_Id == ENUM_CLEANING_STATUS_DND){
                    nameDND = curCleaningStatus.cstat_Name;
                } else if(curCleaningStatus.cstat_Id == ENUM_CLEANING_STATUS_SERVICE_LATER){
                    nameServiceLater = curCleaningStatus.cstat_Name;
                } else if(curCleaningStatus.cstat_Id == ENUM_CLEANING_STATUS_DECLINE_SERVICE){
                    nameDeclinedService = curCleaningStatus.cstat_Name;
                }else if(curCleaningStatus.cstat_Id == ENUM_CLEANING_STATUS_START){
                    nameStarted = curCleaningStatus.cstat_Name;
                } else if(curCleaningStatus.cstat_Id == ENUM_CLEANING_STATUS_COMPLETED){
                    nameCompleted = curCleaningStatus.cstat_Name;
                }else if(curCleaningStatus.cstat_Id == ENUM_CLEANING_STATUS_PAUSE){
                    nameStop = curCleaningStatus.cstat_Name;
                }else if(curCleaningStatus.cstat_Id == ENUM_CLEANING_STATUS_PENDING_SECOND_TIME){
                    nameReassignPending = curCleaningStatus.cstat_Name;
                }
            }
        } else {
            for (int i = 0; i < listCleaningStatus.count; i ++) {
                CleaningStatusModelV2 *curCleaningStatus = [listCleaningStatus objectAtIndex:i];
                if(curCleaningStatus.cstat_Id == ENUM_CLEANING_STATUS_PENDING_FIRST_TIME){
                    namePending = curCleaningStatus.cstat_Language;
                } else if(curCleaningStatus.cstat_Id == ENUM_CLEANING_STATUS_DND){
                    nameDND = curCleaningStatus.cstat_Language;
                } else if(curCleaningStatus.cstat_Id == ENUM_CLEANING_STATUS_SERVICE_LATER){
                    nameServiceLater = curCleaningStatus.cstat_Language;
                } else if(curCleaningStatus.cstat_Id == ENUM_CLEANING_STATUS_DECLINE_SERVICE){
                    nameDeclinedService = curCleaningStatus.cstat_Language;
                } else if(curCleaningStatus.cstat_Id == ENUM_CLEANING_STATUS_START){
                    nameStarted = curCleaningStatus.cstat_Language;
                } else if(curCleaningStatus.cstat_Id == ENUM_CLEANING_STATUS_COMPLETED){
                    nameCompleted = curCleaningStatus.cstat_Language;
                }else if(curCleaningStatus.cstat_Id == ENUM_CLEANING_STATUS_PAUSE){
                    nameStop = curCleaningStatus.cstat_Language;
                }else if(curCleaningStatus.cstat_Id == ENUM_CLEANING_STATUS_PENDING_SECOND_TIME){
                    nameReassignPending = curCleaningStatus.cstat_Language;
                }
            }
        }
    }
    if (isEnglish) {
        nameAll = @"All";
    } else {
        nameAll = [L_ALL currentKeyToLanguage];
    }
    nameArrival = ARRIVAL_KEY;
    if(!isDemoMode){
        listRoomStatusCleaningStatus = [NSMutableArray array];
        if(nameVC)
            [listRoomStatusCleaningStatus addObject:@{@"name":nameVC, @"FilterType": @(roomStatusType), @"GeneralFilter": @(room_Status_Id_VC)}];
        if(nameVD)
            [listRoomStatusCleaningStatus addObject:@{@"name":nameVD, @"FilterType": @(roomStatusType), @"GeneralFilter": @(room_Status_Id_VD)}];
        if(nameOC)
            [listRoomStatusCleaningStatus addObject:@{@"name":nameOC, @"FilterType": @(roomStatusType), @"GeneralFilter": @(room_Status_Id_OC)}];
        if(nameOD)
            [listRoomStatusCleaningStatus addObject:@{@"name":nameOD, @"FilterType": @(roomStatusType), @"GeneralFilter": @(room_Status_Id_OD)}];
        if(nameVI)
            [listRoomStatusCleaningStatus addObject:@{@"name":nameVI, @"FilterType": @(roomStatusType), @"GeneralFilter": @(room_Status_Id_VI)}];
        if(nameOI)
            [listRoomStatusCleaningStatus addObject:@{@"name":nameOI, @"FilterType": @(roomStatusType), @"GeneralFilter": @(room_Status_Id_OI)}];
        if(nameVPU)
            [listRoomStatusCleaningStatus addObject:@{@"name":nameVPU, @"FilterType": @(roomStatusType), @"GeneralFilter": @(room_Status_Id_VPU)}];
        if(nameOPU)
            [listRoomStatusCleaningStatus addObject:@{@"name":nameOPU, @"FilterType": @(roomStatusType), @"GeneralFilter": @(ENUM_ROOM_STATUS_OPU)}];
        if(nameOOO)
            [listRoomStatusCleaningStatus addObject:@{@"name":nameOOO, @"FilterType": @(roomStatusType), @"GeneralFilter": @(room_Status_Id_OO)}];
        if(nameOOS)
            [listRoomStatusCleaningStatus addObject:@{@"name":nameOOS, @"FilterType": @(roomStatusType), @"GeneralFilter": @(room_Status_Id_OOS)}];
        if(nameOCSO)
            [listRoomStatusCleaningStatus addObject:@{@"name":nameOCSO, @"FilterType": @(roomStatusType), @"GeneralFilter": @(room_Status_Id_OCSO)}];
        if(nameDND)
            [listRoomStatusCleaningStatus addObject:@{@"name":nameDND, @"FilterType": @(cleaningStatusType), @"GeneralFilter": @(cleaning_Status_Id_DND)}];
        if(nameServiceLater)
            [listRoomStatusCleaningStatus addObject:@{@"name":nameServiceLater, @"FilterType": @(cleaningStatusType), @"GeneralFilter": @(cleaning_Status_Id_ServiceLater)}];
        if(nameDeclinedService)
            [listRoomStatusCleaningStatus addObject:@{@"name":nameDeclinedService, @"FilterType": @(cleaningStatusType), @"GeneralFilter": @(cleaning_Status_Id_DeclinedService)}];
        if(nameStarted)
            [listRoomStatusCleaningStatus addObject:@{@"name":nameStarted, @"FilterType": @(cleaningStatusType), @"GeneralFilter": @(ENUM_CLEANING_STATUS_START)}];
        if(nameCompleted)
            [listRoomStatusCleaningStatus addObject:@{@"name":nameCompleted, @"FilterType": @(cleaningStatusType), @"GeneralFilter": @(cleaning_Status_Id_Completed)}];
        if(nameStop)
            [listRoomStatusCleaningStatus addObject:@{@"name":nameStop, @"FilterType": @(cleaningStatusType), @"GeneralFilter": @(cleaning_Status_Id_Stop)}];
        if(namePending)
            [listRoomStatusCleaningStatus addObject:@{@"name":namePending, @"FilterType": @(cleaningStatusType), @"GeneralFilter": @(cleaning_Status_Id_Pending)}];
        if(nameReassignPending)
            [listRoomStatusCleaningStatus addObject:@{@"name":nameReassignPending, @"GeneralFilter": @(cleaning_Status_Id_Reassign_Pending)}];
        if(nameDoubleLock)
            [listRoomStatusCleaningStatus addObject:@{@"name":nameDoubleLock, @"FilterType": @(cleaningStatusType), @"GeneralFilter": @(cleaning_Status_Id_Double_Lock)}];
        if(nameInspected)
            [listRoomStatusCleaningStatus addObject:@{@"name":nameInspected, @"FilterType": @(roomStatusType), @"GeneralFilter": @(inspectedCase)}];
        if(nameRushRoom)
            [listRoomStatusCleaningStatus addObject:@{@"name":nameRushRoom, @"FilterType": @(kindOfRoomType), @"GeneralFilter": @(ENUM_KIND_OF_ROOM_RUSH)}];
        if(nameQueueRoom)
            [listRoomStatusCleaningStatus addObject:@{@"name":nameQueueRoom, @"FilterType": @(kindOfRoomType), @"GeneralFilter": @(ENUM_KIND_OF_ROOM_QUEUE)}];
        //        if(nameProfileNotesExist)
        //            [listRoomStatusCleaningStatus addObject:@{@"name":nameProfileNotesExist, @"FilterType": @(kindOfRoomType), @"GeneralFilter": @(ENUM_KIND_OF_ROOM_PROFILE_NOTES_EXISTS)}];
        //        if(nameStayOver)
        //            [listRoomStatusCleaningStatus addObject:@{@"name":nameStayOver, @"FilterType": @(kindOfRoomType), @"GeneralFilter": @(ENUM_KIND_OF_ROOM_STAY_OVER_EXISTS)}];
        if(nameOccupiedDueOut)
            [listRoomStatusCleaningStatus addObject:@{@"name":nameOccupiedDueOut, @"FilterType": @(roomOccupiedType), @"GeneralFilter": @(ENUM_ROOM_TYPE_ROOM_OCCUPIED_DUEOUT)}];
        if(nameUnassignRoom)
            [listRoomStatusCleaningStatus addObject:@{@"name":nameUnassignRoom, @"FilterType": @(unassignType), @"GeneralFilter": @(ENUM_ROOM_STATUS_UNASSIGN)}];
        if(nameODDO)
            [listRoomStatusCleaningStatus addObject:@{@"name":nameODDO, @"FilterType": @(roomOccupiedType), @"GeneralFilter": @(ENUM_ROOM_TYPE_OCCUPIED_OD_DO)}];
        [listRoomStatusCleaningStatus addObject:@{@"name":nameArrival, @"FilterType": @(arrivalType)}];
        if(nameAll)
            [listRoomStatusCleaningStatus addObject:@{@"name":nameAll, @"FilterType": @(allRoomType)}];
    } else {
        listRoomStatusCleaningStatus = [[NSMutableArray alloc] init];
        [listRoomStatusCleaningStatus addObject:@{@"name":nameAll, @"FilterType": @(allRoomType)}];
        [listRoomStatusCleaningStatus addObject:@{@"name":nameOC, @"FilterType": @(roomStatusType), @"GeneralFilter": @(room_Status_Id_OC)}];
        [listRoomStatusCleaningStatus addObject:@{@"name":nameOD, @"FilterType": @(roomStatusType), @"GeneralFilter": @(room_Status_Id_OD)}];
        [listRoomStatusCleaningStatus addObject:@{@"name":nameVC, @"FilterType": @(roomStatusType), @"GeneralFilter": @(room_Status_Id_VC)}];
        [listRoomStatusCleaningStatus addObject:@{@"name":nameVD, @"FilterType": @(roomStatusType), @"GeneralFilter": @(room_Status_Id_VD)}];
        [listRoomStatusCleaningStatus addObject:@{@"name":nameVI, @"FilterType": @(roomStatusType), @"GeneralFilter": @(room_Status_Id_VI)}];
        [listRoomStatusCleaningStatus addObject:@{@"name":nameOI, @"FilterType": @(roomStatusType), @"GeneralFilter": @(room_Status_Id_OI)}];
        [listRoomStatusCleaningStatus addObject:@{@"name":nameDND, @"FilterType": @(cleaningStatusType), @"GeneralFilter": @(cleaning_Status_Id_DND)}];
        [listRoomStatusCleaningStatus addObject:@{@"name":nameServiceLater, @"FilterType": @(cleaningStatusType), @"GeneralFilter": @(cleaning_Status_Id_ServiceLater)}];
        [listRoomStatusCleaningStatus addObject:@{@"name":nameDeclinedService, @"FilterType": @(cleaningStatusType), @"GeneralFilter": @(cleaning_Status_Id_DeclinedService)}];
        [listRoomStatusCleaningStatus addObject:@{@"name":nameStarted, @"FilterType": @(cleaningStatusType), @"GeneralFilter": @(ENUM_CLEANING_STATUS_START)}];
        [listRoomStatusCleaningStatus addObject:@{@"name":nameCompleted, @"FilterType": @(cleaningStatusType), @"GeneralFilter": @(cleaning_Status_Id_Completed)}];
        [listRoomStatusCleaningStatus addObject:@{@"name":nameStop, @"FilterType": @(cleaningStatusType), @"GeneralFilter": @(cleaning_Status_Id_Stop)}];
        [listRoomStatusCleaningStatus addObject:@{@"name":nameReassignPending, @"FilterType": @(cleaningStatusType), @"GeneralFilter": @(cleaning_Status_Id_Pending)}];
    }
}
-(IBAction)btnRoomStatusPressed:(id)sender {
    UIButton *button = (UIButton *) sender;
    
    //    PickerViewV2 *picker = [[PickerViewV2 alloc] initPickerViewV2WithDatas:[NSArray arrayWithObjects: all,nameVC, nameOC, nameVD, nameOD, nameVI, nameOI, nameOOS, nameOOO, nil] AndSelectedData:button.titleLabel.text AndIndex:nil];
//    NSString *strPending = [L_strpending currentKeyToLanguage];
//    NSString *strDND = [L_strdnd currentKeyToLanguage];
//    NSString *strDoubleLock = [L_strdouble_lock currentKeyToLanguage];
//    NSString *strServiceLater = [L_service_later currentKeyToLanguage];
//    NSString *strDeclinedService = [L_declined_service currentKeyToLanguage];
//    NSString *strCompleted = [L_strcompleted currentKeyToLanguage];
//    NSString *strInspected = [L_find_room_inspected currentKeyToLanguage];
//    NSString *strRushRoom = [L_find_room_rush_room currentKeyToLanguage];
//    NSString *strQueneRoom = [L_find_room_queue_room currentKeyToLanguage];
//    NSString *strUnassignRoom = [L_find_room_unassign currentKeyToLanguage];
    NSMutableArray *listTitle = [NSMutableArray new];
    for (NSDictionary *dict in listRoomStatusCleaningStatus) {
        [listTitle addObject:dict[@"name"]];
    }
    
    PickerViewV2 *picker = [[PickerViewV2 alloc] initPickerViewV2WithDatas:listTitle AndSelectedData:button.titleLabel.text AndIndex:nil];
    picker.delegate = self;
    [picker showPickerViewV2];
}

#pragma mark - PickerViewV2 Delegate Methods
-(void)didChooseFindRoomPickerViewV2WithData:(NSString *)data AndIndex:(NSIndexPath *)index WithtIndex:(NSInteger)tIndex{
    
//    switch (tIndex) {
//        case 0:
//            //VC
//            roomStatusID = room_Status_Id_VC;
//            filterType = roomStatusType;
//            break;
//            
//        case 1:
//            //VD
//            roomStatusID = room_Status_Id_VD;
//            filterType = roomStatusType;
//            break;
//            
//        case 2:
//            //OC
//            roomStatusID = room_Status_Id_OC;
//            filterType = roomStatusType;
//            break;
//            
//        case 3:
//            //OD
//            roomStatusID = room_Status_Id_OD;
//            filterType = roomStatusType;
//            break;
//            
//        case 4:
//            //VI
//            roomStatusID = room_Status_Id_VI;
//            filterType = roomStatusType;
//            break;
//            
//        case 5:
//            //OI
//            roomStatusID = room_Status_Id_OI;
//            filterType = roomStatusType;
//            break;
//            
//        case 6:
//            //VPU
//            roomStatusID = room_Status_Id_VPU;
//            filterType = roomStatusType;
//            break;
//            
//        case 7:
//            //OC-SO
//            roomStatusID = room_Status_Id_OCSO;
//            filterType = roomStatusType;
//            break;
//            
//        case 8:
//            //Pending
//            roomStatusID = cleaning_Status_Id_Pending;
//            filterType = cleaningStatusType;
//            break;
//            
//        case 9:
//            //DND
//            roomStatusID = cleaning_Status_Id_DND;
//            filterType = cleaningStatusType;
//            break;
//            
//        case 10:
//            //Service Later
//            roomStatusID = cleaning_Status_Id_ServiceLater;
//            filterType = cleaningStatusType;
//            break;
//            
//        case 11:
//            //Declined Service
//            roomStatusID = cleaning_Status_Id_DeclinedService;
//            filterType = cleaningStatusType;
//            break;
//        case 12:
//            //Double Lock Service
//            roomStatusID = cleaning_Status_Id_DoubleLock;
//            filterType = cleaningStatusType;
//            break;
//        case 13:
//            //Completed
//            roomStatusID = cleaning_Status_Id_Completed;
//            filterType = cleaningStatusType;
//            break;
//            
//        case 14:
//            //Inspected
//            roomStatusID = inspectedCase;
//            filterType = roomStatusType;
//            break;
//            
//        case 15:
//            //OOS
//            roomStatusID = room_Status_Id_OOS;
//            filterType = roomStatusType;
//            break;
//            
//        case 16:
//            //OOO
//            roomStatusID = room_Status_Id_OO;
//            filterType = roomStatusType;
//            break;
//            
//        case 17:
//            //Rush
//            roomStatusID = ENUM_KIND_OF_ROOM_RUSH;
//            filterType = kindOfRoomType;
//            break;
//            
//        case 18:
//            //Queue room
//            roomStatusID = ENUM_KIND_OF_ROOM_QUEUE;
//            filterType = kindOfRoomType;
//            break;
//            
//        case 19:
//            //Unassign room
//            roomStatusID = ENUM_ROOM_STATUS_UNASSIGN;
//            filterType = unassignType;
//            break;
//            
//        default:
//            break;
//    }
    NSDictionary *dict = listRoomStatusCleaningStatus[tIndex];
    if(dict[@"FilterType"]){
        filterType = [dict[@"FilterType"] intValue];
    }
    if(dict[@"GeneralFilter"]){
        roomStatusID = [dict[@"GeneralFilter"] intValue];
    }
    [btnRoomStatus setTitle:data forState:UIControlStateNormal];
    self.selectedRoomStatus = data;
    
    [listTempData removeAllObjects];
    [listDisplayData removeAllObjects];
    
    // get data form WS
    self.isLoadFromWS = TRUE;
    [self performSelector:@selector(loadData) withObject:nil afterDelay:0.5];
    
    //
    //    for(NSDictionary *rowData in listTempData) {
    //        NSString *roomStatus = [rowData objectForKey:kRMStatus];
    //        if([data isEqualToString:roomStatus]) {
    //            [listDisplayData addObject:rowData];
    //            [tbvNoResult setHidden:YES];
    //            [tbvContent setHidden:NO];
    //        } else if([data isEqualToString:[[LanguageManagerV2 sharedLanguageManager] getAll]]) {
    //            [listDisplayData addObjectsFromArray:listTempData];
    //            [tbvNoResult setHidden:YES];
    //            [tbvContent setHidden:NO];
    //        }
    //    }
    //
    //    if([listDisplayData count] <= 0) {
    //        [tbvContent setHidden:YES];
    //        [tbvNoResult setHidden:NO];
    //    }
    //
    //    if([listDisplayData count]>1) {
    //        for(int i = 0; i < [listDisplayData count]; i++) {
    //            for(int j = i + 1; j < [listDisplayData count]; j++) {
    //                NSDictionary *row1 = [listDisplayData  objectAtIndex:i];
    //                NSString *roomNo1 = [row1 objectForKey:kRoomNo];
    //
    //                NSDictionary *row2 = [listDisplayData  objectAtIndex:j];
    //                NSString *roomNo2 = [row2 objectForKey:kRoomNo];
    //
    //                if([roomNo1 isEqualToString:roomNo2]) {
    //                    [listDisplayData removeObjectAtIndex:j];
    //                }
    //            }
    //        }
    //    }
    //
    //    [tbvContent reloadData];
}

#pragma mark  - Set Captions View
-(void)setCaptionsView {
    [self setTitle:[NSString stringWithFormat:@"%@ - %@ %@", [[LanguageManagerV2 sharedLanguageManager] getFind], [[LanguageManagerV2 sharedLanguageManager] getBy], [[LanguageManagerV2 sharedLanguageManager] getRoomTitle]]];
    
    [lblByRoomStatus setText:[[LanguageManagerV2 sharedLanguageManager] getByRoomStatus]];
    [lblByRoomStatus setFont:[UIFont fontWithName:font size:sizeNormal]];
    
    [lblRoomNo setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_rm]];
    [lblRoomNo setFont:[UIFont fontWithName:font size:sizeNormal]];
    
    [lblRoomStatus setText:[[LanguageManagerV2 sharedLanguageManager] getRMStatus]];
    [lblRoomStatus setFont:[UIFont fontWithName:font size:sizeNormal]];
    
    [lblVIP setText:[[LanguageManagerV2 sharedLanguageManager] getVIP]];
    [lblVIP setFont:[UIFont fontWithName:font size:sizeNormal]];
    
    [lblCleaningStatus setText:[[LanguageManagerV2 sharedLanguageManager] getCleaningStatus]];
    [lblCleaningStatus setFont:[UIFont fontWithName:font size:sizeNormal]];
    
//#warning modify here
    if(selectedRoomStatus == nil)
        self.selectedRoomStatus = @"VC"; //@"VC"
//#warning end
    [btnRoomStatus setTitle:selectedRoomStatus forState:UIControlStateNormal];
    [btnRoomStatus.titleLabel setFont:[UIFont fontWithName:font size:sizeNormal]];
    
    [searchBar setPlaceholder:[[LanguageManagerV2 sharedLanguageManager] getSearch]];
    [searchBar setTintColor:[UIColor colorWithRed:colorBlueR green:colorBlueG
                                             blue:colorBlueB alpha:colorAlpha]];
    [searchBar setValue:[L_CANCEL currentKeyToLanguage] forKey:@"_cancelButtonText"];
}

#pragma mark - === Handle Topbar Methods ===
#pragma mark
-(void)loadTopbarView {
    TopbarViewV2 *topbar = [[TopbarViewV2 alloc] initViewWithSuperModel:[[CommonVariable sharedCommonVariable] superRoomModel]];
    [topbar setTag:tagTopbarView];
    [self.view addSubview:topbar];
    [topbar setHidden:YES];
    
    [topbar setAlpha:0.0];
    [self addButtonHandleShowHideTopbar];
}

-(void)showTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationWillStartSelector:@selector(setHiddenTopbarView)];
    
    f = topbar.frame;
    f.origin.y = 0;
    [topbar setFrame:f];
    [topbar setAlpha:1.0];
    
    [self adjustShowForViews];
    
    [UIView commitAnimations];
}

-(void)hideTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDidStopSelector:@selector(setHiddenTopbarView)];
    [UIView setAnimationDelegate:self];
    
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    [topbar setAlpha:0.0];
    
    [self adjustRemoveForViews];
    
    [UIView commitAnimations];
}

-(void)setHiddenTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    [topbar setHidden:!topbar.hidden];
}

-(TopbarViewV2 *)getTopBarView {
    return (TopbarViewV2 *)[self.view viewWithTag:tagTopbarView];
}

//-(void)addButtonHandleShowHideTopbar {
//    UIButton *titleBarButton = [[UIButton alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
//
//    titleBarButton.titleLabel.backgroundColor = [UIColor clearColor];
//    [titleBarButton.titleLabel setFont: FONT_BUTTON_TOPBAR];
//    titleBarButton.titleLabel.textAlignment = NSTextAlignmentCenter;
//    titleBarButton.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
//    [titleBarButton.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
//    [titleBarButton.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
//    [titleBarButton addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
//    [titleBarButton setTitle:[[LanguageManagerV2 sharedLanguageManager] getFind] forState:UIControlStateNormal];
//    self.navigationItem.titleView = titleBarButton;
//}

-(void)addButtonHandleShowHideTopbar {
    UIView *vBarButton = [[UIView alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
    UIButton *titleBarButtonFirst = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 200, 30)];
    
    titleBarButtonFirst.titleLabel.backgroundColor = [UIColor clearColor];
    [titleBarButtonFirst.titleLabel setFont: FONT_BUTTON_TOPBAR];
    titleBarButtonFirst.titleLabel.textAlignment = NSTextAlignmentCenter;
    titleBarButtonFirst.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
    
    [titleBarButtonFirst.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
    [titleBarButtonFirst.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
    [titleBarButtonFirst addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    [titleBarButtonFirst setTitle:[[LanguageManagerV2 sharedLanguageManager] getFind] forState:UIControlStateNormal];
    
    [vBarButton addSubview:titleBarButtonFirst];
    
    
    UIButton *titleBarButtonSecond = [[UIButton alloc] initWithFrame:CGRectMake(0, 22, 200, 22)];
    [titleBarButtonSecond setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [titleBarButtonSecond.titleLabel setFont:FONT_BUTTON_TOPBAR_SECOND];
    [titleBarButtonSecond setTitle:[[LanguageManagerV2 sharedLanguageManager] getRoomTitle] forState:UIControlStateNormal];
    [titleBarButtonSecond addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    [vBarButton addSubview:titleBarButtonSecond];
    
    self.navigationItem.titleView = vBarButton;
    
    //Move title to center vertical
    CGRect titleViewBounds = self.navigationItem.titleView.bounds;
    int moveLeftTitle = 0;
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        moveLeftTitle = 10;
    }
    self.navigationItem.titleView.bounds = CGRectMake(titleViewBounds.origin.x + moveLeftTitle, Y_TOPBAR_TITLE, titleViewBounds.size.width, titleViewBounds.size.height);
    
}

-(void)topbarTapped:(UIButton *)sender {
    TopbarViewV2 *topbar = [self getTopBarView];
    [sender setEnabled:NO];
    if (topbar.isHidden) {
        [self showTopbarView];
    } else {
        [self hideTopbarView];
    }
    [self performSelector:@selector(topbarButtonEnable:) withObject:sender afterDelay:0.3];
}

-(void) topbarButtonEnable:(UIButton *) topbarBtn {
    [topbarBtn setEnabled:YES];
}

-(void)adjustShowForViews {
    //adjust all views when show
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    CGRect ftbv = controlsView.frame;
    [controlsView setFrame:CGRectMake(0, f.size.height, 320, ftbv.size.height)];
    
    ftbv = tbvContent.frame;
    [tbvContent setFrame:CGRectMake(0, f.size.height + controlsView.frame.size.height, 320, ftbv.size.height - f.size.height)];
    
    ftbv = tbvNoResult.frame;
    [tbvNoResult setFrame:CGRectMake(0, f.size.height + controlsView.frame.size.height, 320, ftbv.size.height - f.size.height)];
}

-(void)adjustRemoveForViews {
    //adjust all views when hide
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    [controlsView setFrame:CGRectMake(0, 0, 320, controlsView.frame.size.height)];
    
    CGRect ftbv = tbvContent.frame;
    [tbvContent setFrame:CGRectMake(0, controlsView.frame.size.height, 320, ftbv.size.height + f.size.height)];
    
    ftbv = tbvNoResult.frame;
    [tbvNoResult setFrame:CGRectMake(0, controlsView.frame.size.height, 320, ftbv.size.height + f.size.height)];
}

#pragma mark - === Handle Back Button ===
#pragma mark

-(void)backBarPressed {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)addBackButton {
    //back button navigation
    self.navigationItem.leftBarButtonItem = [MyNavigationBarV2 createBackButtonWithTarget:self selector:@selector(backBarPressed) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - iOS 7 Resources
-(void)loadFlatResource
{
    [btnRoomStatus setBackgroundImage:[UIImage imageNamed:imgOptionSetting_Flat] forState:UIControlStateNormal];
}

@end
