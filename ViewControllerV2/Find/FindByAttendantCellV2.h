//
//  FindByAttendantCellV2.h
//  mHouseKeeping
//
//  Created by Nghia Truong on 2/16/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//
#import <UIKit/UIKit.h>

@interface FindByAttendantCellV2 : UITableViewCell {
    UILabel *lblName;
    UILabel *lblAtdName;
    UILabel *lblCleaningRoom;
//    UILabel *lblCurrent;
//    UILabel *lblTime;
//    UIButton *btnDetailArrow;
}

@property (nonatomic, strong) IBOutlet UILabel *lblName;
@property (nonatomic, strong) IBOutlet UILabel *lblAtdName;
@property (nonatomic, strong) IBOutlet UILabel *lblCleaningRoom;
//@property (nonatomic, strong) IBOutlet UILabel *lblCurrent;
//@property (nonatomic, strong) IBOutlet UILabel *lblTime;
//@property (nonatomic, strong) IBOutlet UIButton *btnDetailArrow;

@end
