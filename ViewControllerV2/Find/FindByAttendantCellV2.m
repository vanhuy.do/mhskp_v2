//
//  FindByAttendantCellV2.m
//  mHouseKeeping
//
//  Created by Nghia Truong on 2/16/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "FindByAttendantCellV2.h"

@implementation FindByAttendantCellV2

@synthesize lblName;//, lblCurrent, lblTime, btnDetailArrow;
@synthesize lblAtdName;
@synthesize lblCleaningRoom;

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:
(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
    }
    
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)dealloc {
//    [lblName release];
//    [lblCurrent release];
//    [lblTime release];
//    [btnDetailArrow release];
//    
//    [super dealloc];
}
@end
