//
//  FindByAttendantViewV2.h
//  mHouseKeeping
//
//  Created by Nghia Truong on 2/16/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//
#import <UIKit/UIKit.h>


@interface FindByAttendantViewV2 : UIViewController <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate> {
    UISearchBar *searchBar;
    UIButton *btnAllTab;
    UIButton *btnGreenHourTab;
    UIButton *btnRedHourTab;
    UIImageView *imgGreenHour;
    UIImageView *imgRedHour;
    UILabel *lblName;
    UILabel *lblCurrent;
    UILabel *lblTime;
    UITableView *tbvContent;
    UITableView *tbvNoResult;
    
    NSMutableArray *listDisplayData;
    NSMutableArray *listTempData; 
    
    NSInteger isAllTabActive;
        
    UIButton *btnBack;
}

@property (nonatomic, strong) IBOutlet UISearchBar *searchBar;
@property (nonatomic, strong) IBOutlet UIButton *btnAllTab;
@property (nonatomic, strong) IBOutlet UIButton *btnGreenHourTab;
@property (nonatomic, strong) IBOutlet UIButton *btnRedHourTab;
@property (nonatomic, strong) IBOutlet UIImageView *imgGreenHour;
@property (nonatomic, strong) IBOutlet UIImageView *imgRedHour;
@property (nonatomic, strong) IBOutlet UILabel *lblName;
@property (nonatomic, strong) IBOutlet UILabel *lblCurrent;
@property (nonatomic, strong) IBOutlet UILabel *lblTime;
@property (nonatomic, strong) IBOutlet UITableView *tbvContent;
@property (nonatomic, strong) IBOutlet UITableView *tbvNoResult;
@property (nonatomic, strong) NSMutableArray *listDisplayData;
@property (nonatomic, strong) NSMutableArray *listTempData;
@property (nonatomic) NSInteger isAllTabActive;
@property (strong, nonatomic) IBOutlet UIView *controlsView;

-(IBAction)btnAllTabPressed:(id)sender;
-(IBAction)btnGreenHourTabPressed:(id)sender;
-(IBAction)btnRedHourTabPressed:(id)sender;

-(void) loadData;
-(void) setCaptionsView;

@end
