//
//  FindByAttendantViewV2.m
//  mHouseKeeping
//
//  Created by Nghia Truong on 2/16/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "FindByAttendantViewV2.h"
#import "FindByAttendantCellV2.h"
#import "RoomManagerV2.h"
#import "TimePerformanceSummaryViewV2.h"
#import "FindCompletedPassFailViewV2.h"
#import "FindManagerV2.h"
#import "NetworkCheck.h"
#import "MyNavigationBarV2.h"
#import "DeviceManager.h"
#import "UIImage+Tint.h"
#import "ehkDefinesV2.h"

#define kAttendantName      @"AttendantName"
#define kCurrentStatus      @"CurrentStatus"
#define kTime               @"Time"
#define kRoomNumber         @"RoomNumber"
#define kRoomAssignmentID   @"RoomAssignmentID"

//#define normalImage         @"tab_count_NOactive.png"
#define greenHourImage      @"time_green.png"
#define redHourImage        @"time_red.png"
#define backgroundImage     @"bg.png"

#define signPlus            @"+"
#define signMinus           @"-"

#define tagNoResult         12

#define heightTbvContent    55.0f
#define heightTbvNoResult   240.0f

#define font                @"Arial-BoldMT"

#define sizeNoResult        22.0f
#define sizeALl             20.0f
#define sizeNormal          17.0f
#define sizeSmall           10.0f

#define colorAlpha          1.0
#define colorWhite          255/255.0
#define colorBlueR          105/255.0
#define colorBlueG          218/255.0
#define colorBlueB          222/255.0
#define colorGreenR         77/255.0
#define colorGreenG         211/255.0
#define colorGreenB         100/255.0
#define colorRedR           223/255.0
#define colorRedG           81/255.0
#define colorRedB           59/255.0

@interface FindByAttendantViewV2 (PrivateMethods)

-(void) btnDetailArrowPressed:(NSInteger)index;
-(void) loadTopbarView;
-(void) showTopbarView;
-(void) hideTopbarView;
-(void) setHiddenTopbarView;
-(void) adjustShowForViews;
-(void) adjustRemoveForViews;
-(void) addButtonHandleShowHideTopbar;
-(void) topbarTapped:(UIButton *) sender;
-(TopbarViewV2 *) getTopBarView;
-(void) btnBackPressed;
-(void) addBackButton;
-(void) loadData;
@end

@implementation FindByAttendantViewV2
@synthesize controlsView;

@synthesize lblName, lblCurrent, lblTime, searchBar, imgGreenHour, imgRedHour, btnAllTab, btnRedHourTab, btnGreenHourTab, tbvContent, tbvNoResult, listDisplayData, listTempData, isAllTabActive;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle
-(void)viewWillAppear:(BOOL)animated {
    [self setCaptionsView];
    [super viewWillAppear:animated];
    
    //set selection find in bottom bar
    [[HomeViewV2 shareHomeView] setSelectedButton:tagOfFindButton];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //Resize content views of current VC to fit with Navigation bar and tabbar
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        //Clear redundant view for header, footter
        CGRect frameHeaderFooter = tbvContent.tableHeaderView.frame;
        frameHeaderFooter.size.height = 0.1f;
        UIView *headerView = [[UIView alloc] initWithFrame:frameHeaderFooter];
        [tbvContent setTableHeaderView:headerView];
        
        frameHeaderFooter = tbvContent.tableFooterView.frame;
        frameHeaderFooter.size.height = 0.1f;
        UIView *footerView = [[UIView alloc] initWithFrame:frameHeaderFooter];
        [tbvContent setTableFooterView:footerView];
        
        [self loadFlatResource];
    }
    
    self.isAllTabActive = 0;
    
    //config image for switchview use button
    UIImage *imageActive = [UIImage imageBeforeiOS7:imgTabCountActive equaliOS7:imgTabCountActiveFlat];
    UIImage *imgNoActive = [UIImage imageBeforeiOS7:imgTabCountNoActive equaliOS7:imgTabCountNoActiveFlat];
    
    [btnAllTab setBackgroundImage:imageActive forState:UIControlStateNormal];
    [btnGreenHourTab setBackgroundImage:imgNoActive forState:UIControlStateNormal];
    [btnRedHourTab setBackgroundImage:imgNoActive forState:UIControlStateNormal];
    
    [tbvContent setBackgroundView:nil];
    [tbvContent setBackgroundColor:[UIColor clearColor]];
    [tbvContent setOpaque:YES];
    
    [tbvNoResult setBackgroundView:nil];
    [tbvNoResult setBackgroundColor:[UIColor clearColor]];
    [tbvNoResult setOpaque:YES];
    [tbvNoResult setHidden:YES];
    
    // Load Data
    [self performSelector:@selector(loadData) withObject:nil afterDelay:0.3];
    
    [self performSelector:@selector(loadTopbarView)];
    
    //add back button
    [self addBackButton];
}

- (void)viewDidUnload {
    [self setBtnAllTab:nil];
    [self setBtnGreenHourTab:nil];
    [self setBtnRedHourTab:nil];
    [self setImgGreenHour:nil];
    [self setImgRedHour:nil];
    [self setLblName:nil];
    [self setLblCurrent:nil];
    [self setLblTime:nil];
    [self setTbvContent:nil];
    [self setTbvNoResult:nil];
    [self setSearchBar:nil];
    [self setListDisplayData:nil];
    [self setListTempData:nil];
    
    [self setControlsView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Action for UIButton
-(IBAction)btnAllTabPressed:(id)sender {
    isAllTabActive = 0;
    
    //config image for switchview use button
    UIImage *imageActive = [UIImage imageBeforeiOS7:imgTabCountActive equaliOS7:imgTabCountActiveFlat];
    UIImage *imgNoActive = [UIImage imageBeforeiOS7:imgTabCountNoActive equaliOS7:imgTabCountNoActiveFlat];
    
    [btnAllTab setBackgroundImage:imageActive forState:UIControlStateNormal];
    [btnGreenHourTab setBackgroundImage:imgNoActive forState:UIControlStateNormal];
    [btnRedHourTab setBackgroundImage:imgNoActive forState:UIControlStateNormal];
    
    [listDisplayData removeAllObjects];
    [listDisplayData addObjectsFromArray:listTempData];
    
    if([listDisplayData count] <= 0) {
        [tbvContent setHidden:YES];
        [tbvNoResult setHidden:NO];
    } else {
        [tbvContent setHidden:NO];
        [tbvNoResult setHidden:YES];
    }
    
    [tbvContent reloadData];
}

-(IBAction)btnGreenHourTabPressed:(id)sender {
    isAllTabActive = 1;
    
    //config image for switchview use button
    UIImage *imageActive = [UIImage imageBeforeiOS7:imgTabCountActive equaliOS7:imgTabCountActiveFlat];
    UIImage *imgNoActive = [UIImage imageBeforeiOS7:imgTabCountNoActive equaliOS7:imgTabCountNoActiveFlat];
    
    [btnAllTab setBackgroundImage:imgNoActive forState:UIControlStateNormal];
    [btnGreenHourTab setBackgroundImage:imgNoActive forState:UIControlStateNormal];
    [btnRedHourTab setBackgroundImage:imageActive forState:UIControlStateNormal];
    
    [listDisplayData removeAllObjects];
    
    for(NSDictionary *row in listTempData) {
        NSString *time = [row objectForKey:kTime];
        NSRange range = [time rangeOfString:signMinus];
        
        if(range.location != NSNotFound)
            [listDisplayData addObject:row];
    }
    
    if([listDisplayData count] <= 0) {
        [tbvContent setHidden:YES];
        [tbvNoResult setHidden:NO];
    } else {
        [tbvContent setHidden:NO];
        [tbvNoResult setHidden:YES];
    }
    
    [tbvContent reloadData];
}

-(IBAction)btnRedHourTabPressed:(id)sender {
    isAllTabActive = 2;
    
    //config image for switchview use button
    UIImage *imageActive = [UIImage imageBeforeiOS7:imgTabCountActive equaliOS7:imgTabCountActiveFlat];
    UIImage *imgNoActive = [UIImage imageBeforeiOS7:imgTabCountNoActive equaliOS7:imgTabCountNoActiveFlat];
    
    [btnAllTab setBackgroundImage:imgNoActive forState:UIControlStateNormal];
    [btnGreenHourTab setBackgroundImage:imageActive forState:UIControlStateNormal];
    [btnRedHourTab setBackgroundImage:imgNoActive forState:UIControlStateNormal];
    
    [listDisplayData removeAllObjects];
    
    for (NSDictionary *rowData in listTempData) {
        NSString *time = [rowData objectForKey:kTime];
        
        NSRange range = [time rangeOfString:signMinus];
        
        if(range.location == NSNotFound) {
            [listDisplayData addObject:rowData];
        }
    }
    
    if([listDisplayData count] <= 0) {
        [tbvContent setHidden:YES];
        [tbvNoResult setHidden:NO];
    } else {
        [tbvContent setHidden:NO];
        [tbvNoResult setHidden:YES];
    }
    
    [tbvContent reloadData];
}

#pragma mark - UITableView Delegate Methods
-(CGFloat)tableView:(UITableView *)tableView 
    heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(tableView == tbvContent)
        return heightTbvContent;
    else if(tableView == tbvNoResult)
        return heightTbvNoResult;
    
    return 0;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(tableView == tbvContent)
        return [listDisplayData count];
    
    return 1;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if(tableView == tbvNoResult) {
        cell.backgroundColor = [UIColor clearColor];
    }
    else{
        cell.backgroundColor = [UIColor colorWithRed:colorWhite green:colorWhite blue:colorWhite alpha:colorAlpha];
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView 
        cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentify = @"cellIdentify";
    static NSString *cellNoIndentify = @"cellNoIndentify";
    
    UITableViewCell *cell = nil;
    
    if(tableView == tbvNoResult) {
        [tbvContent setHidden:YES];
        [tbvNoResult setHidden:NO];
        cell = [tableView dequeueReusableCellWithIdentifier:cellNoIndentify];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellNoIndentify];
            
            UILabel *titleNoResult = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, heightTbvNoResult)];
            [titleNoResult setTextColor:[UIColor grayColor]];
            [titleNoResult setBackgroundColor:[UIColor clearColor]];
            [titleNoResult setTextAlignment:NSTextAlignmentCenter];
            [titleNoResult setTag:tagNoResult];
            [titleNoResult setFont:[UIFont fontWithName:font size:sizeNoResult]];
            
            [cell.contentView addSubview:titleNoResult];
        }
        
        UILabel *lblNoResult = (UILabel *)[cell viewWithTag:tagNoResult];
        [lblNoResult setText:[[LanguageManagerV2 sharedLanguageManager] getNoResultsFound]];
        
        return cell;
    }
    
    if(tableView == tbvContent) {
        [tbvContent setHidden:NO];
        [tbvNoResult setHidden:YES];
        FindByAttendantCellV2 *cell = nil;
        cell = (FindByAttendantCellV2 *)[tableView dequeueReusableCellWithIdentifier:
                                    cellIdentify];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        if (cell == nil) {
            NSArray *arrayCell = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([FindByAttendantCellV2 class]) owner:self options:nil];
            cell = [arrayCell objectAtIndex:0];
//            [cell.btnDetailArrow addTarget:self action:@selector(btnDetailArrowPressed:) forControlEvents:UIControlEventTouchUpInside];
//            [cell.lblName setFont:[UIFont fontWithName:font size:sizeNormal]];
//            [cell.lblCurrent setFont:[UIFont fontWithName:font size:sizeNormal]];
//            [cell.lblTime setFont:[UIFont fontWithName:font size:sizeNormal]];
            
        }
        
//        [cell.btnDetailArrow setTag:indexPath.row];
        
        NSDictionary *rowData = [listDisplayData objectAtIndex:indexPath.row];
                
        [cell.lblName setText:[rowData objectForKey:kAttendantName]];
        
        [cell.lblAtdName setText:[rowData objectForKey:kAtdName]];
        
        NSString *roomNumber = [rowData objectForKey:kRoomNumber];
        if([roomNumber isEqualToString:@""])
        {
            roomNumber = @"-";
        }
        [cell.lblCleaningRoom setText:roomNumber];
        
        NSInteger currentStatus = [[rowData objectForKey:kCurrentStatus] integerValue];
        
        switch (currentStatus) {
            case IN_ROOM: {
                //attendant is in room so display room number
                [cell.lblCleaningRoom setText:[rowData objectForKey:kRoomNumber]];
            }
                break;
                
            case LUNCH:
            case MOVING: {                
                //attendant have luch or is moving
                if ([[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString:(NSString *)ENGLISH_LANGUAGE] == YES) {
                    [cell.lblCleaningRoom setText:[rowData objectForKey:kAttendantStatusName]];                    
                } else {                    
                    [cell.lblCleaningRoom setText:[rowData objectForKey:kAttendantStatusLang]];
                }                
            }
                
            default:
                break;
        }        
        
//        NSString *time = [rowData objectForKey:kTime];
//        NSRange range1 = [time rangeOfString:signMinus];
//        
//        if(range1.location != NSNotFound) 
//            [cell.lblTime setTextColor:[UIColor colorWithRed:colorRedR green:colorRedG 
//                                                        blue:colorRedB alpha:colorAlpha]];
//        else 
//            [cell.lblTime setTextColor:[UIColor colorWithRed:colorGreenR green:colorGreenG 
//                                                        blue:colorGreenB alpha:colorAlpha]];
//        
//        [cell.lblTime setText:time];        

        return cell;
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == tbvContent) {
        [self btnDetailArrowPressed:indexPath.row];
        
        
    }
}

#pragma mark - UISearchBar Delegate Methods
-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBarA {    
    [searchBarA setShowsCancelButton:YES animated:NO];
    for(UIView *subView in searchBarA.subviews){
        if([subView isKindOfClass:UIButton.class]) {
            UIButton *cancelButton = (UIButton *)subView;
            [cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];

            UIColor *tintColor = COLOR_CANCEL_SEARCH_BAR;
            [cancelButton setTintColor:tintColor];
            [cancelButton setTitle:[[LanguageManagerV2 sharedLanguageManager] getCancel] forState:UIControlStateNormal];
        }
    }
    
    [searchBarA becomeFirstResponder];
}

-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBarA {
    [searchBarA setShowsCancelButton:NO animated:NO];
    [searchBarA resignFirstResponder];
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBarA {
    [searchBarA setShowsCancelButton:NO animated:NO];
    [searchBarA resignFirstResponder];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBarA {
    [searchBarA setShowsCancelButton:NO animated:NO];
    [searchBarA resignFirstResponder];
    [searchBarA setText:@""];
    
    [self searchBar:searchBarA textDidChange:@""];
}

-(void)searchBar:(UISearchBar *)searchBarA textDidChange:(NSString *)searchText {    
    if ((!searchText) || [searchText isEqualToString:@""]) {
        [listDisplayData removeAllObjects];
        [listDisplayData addObjectsFromArray:listTempData];
        
        [tbvNoResult setHidden:YES];
        [tbvContent setHidden:NO];        
    } else {
        [listDisplayData removeAllObjects];
        
        for (NSDictionary *rowData in listTempData) { 
            NSString *name = [rowData objectForKey:kAttendantName];
            
            NSRange r = [name rangeOfString:searchText options:NSCaseInsensitiveSearch];
            
            if(r.location != NSNotFound) {
                [listDisplayData addObject:rowData];
                [tbvNoResult setHidden:YES];
                [tbvContent setHidden:NO];
            } 
        }              
        
        //call ws to search when did not find any results
        if (listDisplayData.count == 0) {
            if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
                listTempData = [[FindManagerV2 sharedFindManagerV2] findDataAttendantWithUserID:[NSNumber numberWithInt:[[[UserManagerV2 sharedUserManager] currentUser] userId]] AndHotelID:[NSNumber numberWithInt:[[[UserManagerV2 sharedUserManager] currentUser] userHotelsId]] AndAttendantName:searchText];
            }
        }
    }
    
    if([listDisplayData count] <= 0) {
        [tbvContent setHidden:YES];
        [tbvNoResult setHidden:NO];
    }
    
    if([listDisplayData count] > 1) {
        for(int i = 0; i < [listDisplayData count]; i++) {
            for(int j = i + 1; j < [listDisplayData count] - 1; j++) {            
                NSDictionary *row1 = [listDisplayData  objectAtIndex:i];
                NSString *guestName1 = [row1 objectForKey:kAttendantName];
                
                NSDictionary *row2 = [listDisplayData  objectAtIndex:j];
                NSString *guestName2 = [row2 objectForKey:kAttendantName];
                
                if([guestName1 isEqualToString:guestName2])
                    [listDisplayData removeObjectAtIndex:j];
            }
        }
    }
    
    [tbvContent reloadData];
}

#pragma mark - === Load Data ===
#pragma mark
-(void)loadData {
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_RA_LOADING_DATA]];
    [HUD show:YES];
    [self.tabBarController.view addSubview:HUD];
    
    NSMutableArray *array = nil;
        
    array = [[FindManagerV2 sharedFindManagerV2] findDataAttendantWithUserID:[NSNumber numberWithInt:[[[UserManagerV2 sharedUserManager] currentUser] userId]] AndHotelID:[NSNumber numberWithInt:[[[UserManagerV2 sharedUserManager] currentUser] userHotelsId]] AndAttendantName:nil];
    
    self.listTempData = [NSMutableArray arrayWithArray:[array sortedArrayUsingComparator:^NSComparisonResult(NSDictionary *obj1, NSDictionary *obj2) {
        NSString *fullNameObj1 = [obj1 objectForKey:kAttendantName];
        NSString *fullNameObj2 = [obj2 objectForKey:kAttendantName];
        return [fullNameObj1 compare:fullNameObj2 options:NSCaseInsensitiveSearch];
    }]];
    self.listDisplayData = [NSMutableArray arrayWithArray:listTempData];
    
    [tbvContent reloadData];
    
    [HUD hide:YES];
    [HUD removeFromSuperview];
}

#pragma mark - Action for Button Detail Arrow
-(void)btnDetailArrowPressed:(NSInteger)index {
    if([listDisplayData count] > 0)
    {
        NSDictionary *rowData = [listDisplayData objectAtIndex:index];
        
//        NSString *roomID = [rowData objectForKey:kRoomNo];
        
//        NSCharacterSet *notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
//        if ([roomID rangeOfCharacterFromSet:notDigits].location == NSNotFound) {
//            // roomID consists only of the digits 0 through 9
//            RoomModelV2 *roomModel = [[RoomModelV2 alloc] init];
//            roomModel.room_Id = [rowData objectForKey:kRoomNo];
//            [[RoomManagerV2 sharedRoomManager] loadRoomModel:roomModel];
//            
//            GuestInfoModelV2 *guestInfoModel = [[GuestInfoModelV2 alloc] init];
//            guestInfoModel.guestRoomId = roomModel.room_Id;
//            [[RoomManagerV2 sharedRoomManager] loadByRoomIDGuestInfo:guestInfoModel];
//            
//            RoomInfoV2 *aRoomInfo = [[RoomInfoV2 alloc] initWithNibName:@"RoomInfoV2" bundle:nil];
//            
//            aRoomInfo.roomName = [rowData objectForKey:kRoomNo];
//            aRoomInfo.roomModel = roomModel;
//            aRoomInfo.guestInfoModel = guestInfoModel;
//            
//            //set current RoomAssignmentId use in ChecklistView
//            [TasksManagerV2 setCurrentRoomAssignment:[[rowData objectForKey:kRoomAssignmentID] integerValue]];
//            
//            [self.navigationController pushViewController:aRoomInfo animated:YES];
//            
//            [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationSelectedFind] object:nil];
//            
//            [aRoomInfo loadingData];
//        } else {
            TimePerformanceSummaryViewV2 *timePerformanceView = [[TimePerformanceSummaryViewV2 alloc] initWithNibName:NSStringFromClass([TimePerformanceSummaryViewV2 class]) bundle:nil];
            timePerformanceView.attendantData = rowData;
            
            [self.navigationItem.backBarButtonItem setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK]];
            [self.navigationController pushViewController:timePerformanceView animated:YES];
//        }
    }
    
}

#pragma mark  - Set Captions View
-(void)setCaptionsView {
    [self setTitle:[NSString stringWithFormat:@"%@ - %@ %@", [[LanguageManagerV2 sharedLanguageManager] getFind], [[LanguageManagerV2 sharedLanguageManager] getBy], [[LanguageManagerV2 sharedLanguageManager] getAttendant]]];
        
    [lblName setText:[[LanguageManagerV2 sharedLanguageManager] getRAName]];

//    [lblCurrent setText:[[LanguageManagerV2 sharedLanguageManager] getCurrent]];
    [lblCurrent setText:@""];
    [lblCurrent setFont:[UIFont fontWithName:font size:sizeNormal]];
    
    [lblTime setText:[[LanguageManagerV2 sharedLanguageManager] getCleaningRoom]];
    [lblTime setFont:[UIFont fontWithName:font size:sizeNormal]];

//    [searchBar setPlaceholder:[[LanguageManagerV2 sharedLanguageManager] getSearch]];
    [searchBar setPlaceholder:[L_ios_ra_name currentKeyToLanguage]];
    [searchBar setTintColor:[UIColor colorWithRed:colorBlueR green:colorBlueG blue:colorBlueB alpha:colorAlpha]];
    [searchBar setValue:[L_CANCEL currentKeyToLanguage] forKey:@"_cancelButtonText"];
    
    [btnAllTab setTitle:[[LanguageManagerV2 sharedLanguageManager] getALL] 
               forState:UIControlStateNormal];
    [btnAllTab.titleLabel setFont:[UIFont fontWithName:font size:sizeALl]];
    [btnAllTab setTitleColor:[UIColor colorWithRed:colorWhite green:colorWhite blue:colorWhite alpha:colorAlpha] forState:UIControlStateNormal];
    
//    [imgGreenHour setImage:[UIImage imageNamed:greenHourImage]];
    [imgGreenHour setImage:[UIImage imageNamed:imgAheadSchedule]];
//    [imgRedHour setImage:[UIImage imageNamed:redHourImage]];
    [imgRedHour setImage:[UIImage imageNamed:imgBehindSchedule]];
}

#pragma mark - === Handle Topbar Methods ===
#pragma mark
-(void)loadTopbarView {
    TopbarViewV2 *topbar = [[TopbarViewV2 alloc] initViewWithSuperModel:[[CommonVariable sharedCommonVariable] superRoomModel]];
    [topbar setTag:tagTopbarView];
    [self.view addSubview:topbar];
    [topbar setHidden:YES];
    
    [topbar setAlpha:0.0];
    [self addButtonHandleShowHideTopbar];
}

-(void)showTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationWillStartSelector:@selector(setHiddenTopbarView)];
    
    f = topbar.frame;
    f.origin.y = 0;
    [topbar setFrame:f];
    [topbar setAlpha:1.0];
    
    [self adjustShowForViews];
    
    [UIView commitAnimations];
}

-(void)hideTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDidStopSelector:@selector(setHiddenTopbarView)];
    [UIView setAnimationDelegate:self];
    
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    [topbar setAlpha:0.0];
    
    [self adjustRemoveForViews];
    
    [UIView commitAnimations];
}

-(void)setHiddenTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    [topbar setHidden:!topbar.hidden];
}

-(TopbarViewV2 *)getTopBarView {
    return (TopbarViewV2 *)[self.view viewWithTag:tagTopbarView];
}

-(void)addButtonHandleShowHideTopbar {
    UIView *vBarButton = [[UIView alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
    UIButton *titleBarButtonFirst = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 200, 30)];
    
    titleBarButtonFirst.titleLabel.backgroundColor = [UIColor clearColor];
    [titleBarButtonFirst.titleLabel setFont: FONT_BUTTON_TOPBAR];
    titleBarButtonFirst.titleLabel.textAlignment = NSTextAlignmentCenter;
    titleBarButtonFirst.titleLabel.textColor = COLOR_BUTTON_TOPBAR;

    [titleBarButtonFirst.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
    [titleBarButtonFirst.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
    [titleBarButtonFirst addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    [titleBarButtonFirst setTitle:[[LanguageManagerV2 sharedLanguageManager] getFind] forState:UIControlStateNormal];
    
    [vBarButton addSubview:titleBarButtonFirst];
    
    
    UIButton *titleBarButtonSecond = [[UIButton alloc] initWithFrame:CGRectMake(0, 22, 200, 22)];
    [titleBarButtonSecond setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [titleBarButtonSecond.titleLabel setFont:FONT_BUTTON_TOPBAR_SECOND];
    [titleBarButtonSecond setTitle:[[LanguageManagerV2 sharedLanguageManager] getRoomAttendant] forState:UIControlStateNormal];
    [titleBarButtonSecond addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    [vBarButton addSubview:titleBarButtonSecond];
    
    self.navigationItem.titleView = vBarButton;
    
    //Move title to center vertical
    CGRect titleViewBounds = self.navigationItem.titleView.bounds;
    int moveLeftTitle = 0;
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        moveLeftTitle = 10;
    }
    self.navigationItem.titleView.bounds = CGRectMake(titleViewBounds.origin.x + moveLeftTitle, Y_TOPBAR_TITLE, titleViewBounds.size.width, titleViewBounds.size.height);
    
//    UIButton *titleBarButton = [[UIButton alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
//    
//    titleBarButton.titleLabel.backgroundColor = [UIColor clearColor];
//    [titleBarButton.titleLabel setFont: FONT_BUTTON_TOPBAR];
//    titleBarButton.titleLabel.textAlignment = NSTextAlignmentCenter;
//    titleBarButton.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
//    [titleBarButton.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
//    [titleBarButton.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
//    [titleBarButton addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
//    [titleBarButton setTitle:[[LanguageManagerV2 sharedLanguageManager] getFind] forState:UIControlStateNormal];
//    self.navigationItem.titleView = titleBarButton;
}

-(void)topbarTapped:(UIButton *)sender {
    TopbarViewV2 *topbar = [self getTopBarView];
    [sender setEnabled:NO];
    if (topbar.isHidden) {
        [self showTopbarView];
    } else {
        [self hideTopbarView];
    }
    
    [self performSelector:@selector(topbarButtonEnable:) withObject:sender afterDelay:0.3];
}

-(void) topbarButtonEnable:(UIButton *) topbarBtn {
    [topbarBtn setEnabled:YES];
}

-(void)adjustShowForViews {
    //adjust all views when show 
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    [controlsView setFrame:CGRectMake(0, f.size.height, 320, controlsView.frame.size.height)];
    
    CGRect ftbv = tbvContent.frame;
    [tbvContent setFrame:CGRectMake(0, f.size.height + controlsView.frame.size.height, 320, ftbv.size.height - f.size.height)];
    
    ftbv = tbvNoResult.frame;
    [tbvNoResult setFrame:CGRectMake(0, f.size.height + controlsView.frame.size.height, 320, ftbv.size.height - f.size.height)];
}

-(void)adjustRemoveForViews { 
    //adjust all views when hide
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    CGRect ftbv = controlsView.frame;
    [controlsView setFrame:CGRectMake(0, 0, 320, ftbv.size.height)];
    
    ftbv = tbvContent.frame;
    [tbvContent setFrame:CGRectMake(0, controlsView.frame.size.height, 320, ftbv.size.height + f.size.height)];
    
    ftbv = tbvNoResult.frame;
    [tbvNoResult setFrame:CGRectMake(0, controlsView.frame.size.height, 320, ftbv.size.height + f.size.height)];
}

#pragma mark - === Handle Back Button ===
#pragma mark
-(void)backBarPressed {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)addBackButton {
    //back button navigation
    self.navigationItem.leftBarButtonItem = [MyNavigationBarV2 createBackButtonWithTarget:self selector:@selector(backBarPressed) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - iOS 7 Resources
-(void)loadFlatResource
{
    [btnAllTab setBackgroundImage:[UIImage imageNamed:imgTabCountActiveFlat] forState:UIControlStateNormal];
    [btnRedHourTab setBackgroundImage:[UIImage imageNamed:imgTabCountActiveFlat] forState:UIControlStateNormal];
    [btnGreenHourTab setBackgroundImage:[UIImage imageNamed:imgTabCountActiveFlat] forState:UIControlStateNormal];
}

@end
