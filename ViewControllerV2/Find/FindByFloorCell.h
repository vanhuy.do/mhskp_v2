//
//  FindByFloorCell.h
//  mHouseKeeping
//
//  Created by truong quang trong on 1/3/17.
//
//

#import <UIKit/UIKit.h>

@interface FindByFloorCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblFloorName;
@property (weak, nonatomic) IBOutlet UIImageView *imgCheckBox;

@end
