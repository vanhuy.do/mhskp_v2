//
//  FindByFloorSelectView.h
//  mHouseKeeping
//
//  Created by truong quang trong on 1/3/17.
//
//

#import <UIKit/UIKit.h>
@protocol FindByFloorSelectViewDelegate <NSObject>
@optional
-(void)transferTheValueOfTableView:(NSMutableArray *)objectArray;

@end
@interface FindByFloorSelectView : UIViewController <UITableViewDelegate, UITableViewDataSource> {
    __unsafe_unretained id <FindByFloorSelectViewDelegate>delegate;
}
- (IBAction)btnGoClick:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *labelGo;
@property (weak, nonatomic) IBOutlet UIButton *goButton;
@property (nonatomic,strong) NSMutableArray *dataTable;
@property (nonatomic,strong) NSMutableArray *toList;
@property (nonatomic,strong) NSMutableArray *tappedDataArray;
@property (nonatomic,assign) id <FindByFloorSelectViewDelegate>delegate;
@end
