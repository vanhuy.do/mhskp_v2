		//
//  FindByFloorSelectView.m
//  mHouseKeeping
//
//  Created by truong quang trong on 1/3/17.
//
//

#import "FindByFloorSelectView.h"
#import "FindByFloorCell.h"
@interface FindByFloorSelectView ()

@end

@implementation FindByFloorSelectView
@synthesize toList,dataTable,tappedDataArray;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self.goButton setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_GO] forState:UIControlStateNormal];
    
    [self loadData];
}
-(void)loadData
{
    NSMutableArray *listAllUser =[[UnassignManagerV2 sharedUnassignManager] getFloorListOnlineFromWSWith:[UserManagerV2 sharedUserManager].currentUser andLastModified:nil];
    FloorModelV2 *tempObject=[listAllUser objectAtIndex:0];
    tempObject.isSelected = YES;
    toList = [NSMutableArray arrayWithArray:listAllUser];
    
    if (!toList) {
        toList=[[NSMutableArray alloc] init];
    }
    if (!dataTable) {
        dataTable=[[NSMutableArray alloc] init];
    }
//    for (int i=0;i< [toList count];i++) {
//        if (self.tappedDataArray && [self.tappedDataArray count]>0) {
//            for (int j=0;j<[self.tappedDataArray count];j++) {
//                FloorModelV2  *ultappedData=[self.tappedDataArray objectAtIndex:j];
//                FloorModelV2 *ultoList=[toList objectAtIndex:i];
//                if (ultappedData.floor_id==ultoList.floor_id) {
//                    ultoList.isSelected=YES;
//                    [self.tappedDataArray replaceObjectAtIndex:j withObject:ultoList];
//                    
//                }
//            }
//            
//        }
//    }
    if ([self.tappedDataArray count] > 0) {
        [self.dataTable addObjectsFromArray:tappedDataArray];
    } else {
        [self.dataTable addObjectsFromArray:toList];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDelegate Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [dataTable count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *sectionIdentified = @"cellIdentified";
    FindByFloorCell *cell = nil;
    
    cell = [tableView dequeueReusableCellWithIdentifier:sectionIdentified];
    if (cell == nil) {
        NSArray *arrayCell = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([FindByFloorCell class]) owner:self options:nil];
        cell = [arrayCell objectAtIndex:0];
        
    }
    //Load data to cells
    FloorModelV2 *personi = nil;
      personi = [dataTable objectAtIndex:indexPath.row];
    cell.lblFloorName.text = personi.floor_name;
    if (!personi.isSelected) {
        [cell.imgCheckBox setImage:[UIImage imageNamed:@"btn_uncheck.png"]];
        
    }else {
        [cell.imgCheckBox setImage:[UIImage imageNamed:@"btn_checked.png"]];
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    FloorModelV2 *toObject = nil;
    toObject = [dataTable objectAtIndex:indexPath.row];
    FindByFloorCell *selectedCell=(FindByFloorCell*)[tableView cellForRowAtIndexPath:indexPath];
    if (toObject.isSelected) {
        [selectedCell.imgCheckBox setImage:[UIImage imageNamed:@"btn_uncheck.png"]];
        toObject.isSelected=NO;
    }else
    {
        [selectedCell.imgCheckBox setImage:[UIImage imageNamed:@"btn_checked.png"]];
        toObject.isSelected=YES;
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}
- (IBAction)btnGoClick:(id)sender {
    if (self.tappedDataArray) {
        
        [self.tappedDataArray removeAllObjects];
    }
    self.tappedDataArray=[[NSMutableArray alloc] init];
    for (int i=0; i<[dataTable count]; i++) {
        FloorModelV2 *tempObject=[dataTable objectAtIndex:i];
        if (tempObject.isSelected) {
            [self.tappedDataArray addObject:tempObject];
            
        }
    }
    [_delegate transferTheValueOfTableView:self.tappedDataArray];
}
@end
