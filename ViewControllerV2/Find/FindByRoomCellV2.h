//
//  FindByRoomCellV2.h
//  mHouseKeeping
//
//  Created by Nghia Truong on 2/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol FindByRoomCellV2Delegate <NSObject>

@optional
- (void)didClickClean:(NSDictionary*)data;
- (void)didClickStatus:(NSDictionary*)data;
//-(void) didChoosePickerViewV2WithData:(NSString *) data AndIndex:(NSIndexPath *) index;
//-(void) didChoosePickerViewV2WithData:(NSString *) data AndIndex:(NSIndexPath *) index WithtIndex:(NSInteger) tIndex;
//-(void) didChooseFindRoomPickerViewV2WithData:(NSString *) data AndIndex:(NSIndexPath *) index WithtIndex:(NSInteger) tIndex;
//-(void) didChooseDemoModeFindRoomPickerViewV2WithData:(NSString *) data AndIndex:(NSIndexPath *) index WithtIndex:(NSInteger) tIndex;
//-(void) didChoosePickerViewV2WithData:(NSString *) data AndIndexRow:(NSInteger) index;
//-(void) didChoosePickerViewV2WithData:(NSString *) data AndIndexItemRow:(NSIndexPath *) index;
//-(void) didChooseFindRoomPickerViewV2WithPickerView:(PickerViewV2*)pickerView withText:(NSString *)data WithtIndex:(NSInteger)tIndex;
//-(void) didChooseDate:(PickerViewV2*)pickerView withDate:(NSDate*)dateSelected;

@end
@interface FindByRoomCellV2 : UITableViewCell {
    UILabel *lblRoomNo;
    UILabel *lblGuestName;
    UILabel *lblRoomStatus;
    UIImageView *imgRoomStatus;
    UILabel *lblVIP;
    UIImageView *imgCleaningStatus;
//    UIButton *btnDetailArrow;
    UIImageView *imgRush;
    UIImageView *imgPreviousDay;
}

@property (nonatomic, strong) IBOutlet UILabel *lblRoomNo;
@property (nonatomic, strong) IBOutlet UILabel *lblGuestName;
@property (nonatomic, strong) IBOutlet UILabel *lblRoomStatus;
@property (nonatomic, strong) IBOutlet UIImageView *imgRoomStatus;
@property (nonatomic, strong) IBOutlet UILabel *lblVIP;
@property (nonatomic, strong) IBOutlet UIImageView *imgCleaningStatus;
@property (nonatomic, strong) IBOutlet UIImageView *imgRush;
//@property (nonatomic, strong) IBOutlet UIButton *btnDetailArrow;
@property (strong, nonatomic) IBOutlet UIImageView *imgPreviousDay;
@property (nonatomic, strong) NSDictionary *rowData;
@property (nonatomic, assign) id<FindByRoomCellV2Delegate> delegate;
- (void)initEvent;

@end
