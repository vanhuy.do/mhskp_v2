//
//  FindByRoomCellV2.m
//  mHouseKeeping
//
//  Created by Nghia Truong on 2/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FindByRoomCellV2.h"

@implementation FindByRoomCellV2{
    bool isLongPress;
}
@synthesize delegate;
@synthesize lblVIP, lblRoomNo, lblRoomStatus, lblGuestName,
    imgCleaningStatus, imgRoomStatus, imgRush, imgPreviousDay;

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:
    (NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
    }
    
    return self;
}
- (void)longPressStatus:(UILongPressGestureRecognizer*)gesture {
//    NSLog(@"%ld", (long)gesture.state);
    if (gesture.state == UIGestureRecognizerStateChanged) {
        if (!isLongPress) {
            if ([delegate respondsToSelector:@selector(didClickStatus:)]) {
                [delegate didClickStatus:self.rowData];
            }
 
            NSLog(@"Long Press longPressStatus");
        }
        isLongPress = YES;
    }
    if ( gesture.state == UIGestureRecognizerStateEnded) {
        isLongPress = NO;
    }
}
- (void)longPressClean:(UILongPressGestureRecognizer*)gesture {
//    NSLog(@"%ld", (long)gesture.state);
    if (gesture.state == UIGestureRecognizerStateChanged) {
        if (!isLongPress) {
            if ([delegate respondsToSelector:@selector(didClickClean:)]) {
                [delegate didClickClean:self.rowData];
            }
        }
        isLongPress = YES;
    }
    if ( gesture.state == UIGestureRecognizerStateEnded) {
        isLongPress = NO;
    }
}
- (void)initEvent{
    isLongPress = NO;
    UIButton *lblRoomStatusCell = (UIButton *)[self viewWithTag:111];
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressStatus:)];
    [lblRoomStatusCell addGestureRecognizer:longPress];
    
    UIButton *lblRoomCleanCell = (UIButton *)[self viewWithTag:112];
    UILongPressGestureRecognizer *longClientPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressClean:)];
    [lblRoomCleanCell addGestureRecognizer:longClientPress];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)dealloc {
}

@end
