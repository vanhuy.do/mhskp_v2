//
//  FindByRoomFloorViewV2.h
//  mHouseKeeping
//
//  Created by Quang Nguyen on Mar 18, 2013.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PickerViewV2.h"

#define kRoomNo             @"RoomNo"
#define kFloorName          @"FloorName"
#define kGuestName          @"GuestName"
#define kRMStatus           @"RMStatus"
#define kCleaningStatus     @"CleaningStatus"
#define kVIP                @"VIP"
#define kRoomAssignmentID   @"RoomAssignmentID"

#define markImage           @"!.png"
#define backgroundImage     @"bg.png"

//#define all                 @""
//#define nameVC              @"VC"
//#define nameOC              @"OC"
//#define nameVD              @"VD"
//#define nameOD              @"OD"
//#define nameVI              @"VI"
//#define nameOI              @"OI"
//#define nameOOS             @"OOS"
//#define nameOOO             @"OOO"
//#define nameVPU             @"VPU"
//#define nameOCSO            @"OC-SO"
//#define namePending         @"Pending"
//#define nameDND             @"DND"
//#define nameServiceLater    @"Service Later"
//#define nameDeclinedService @"Declined Service"
//#define nameCompleted       @"Completed"
//#define nameInspected       @"Inspected"
//#define nameRushRoom        @"Rush Room"
//#define nameQueueRoom       @"Queue Room"
//#define nameUnassignRoom    @"Unassign"

#define tagNoResult         12345
#define heightTbvContent    55.0f

#define font                @"Arial-BoldMT"

#define sizeNoResult        22.0f
#define sizeNormal          17.0f
#define sizeSmall           10.0f

#define colorAlpha          1.0
#define colorWhite          255/255.0
#define colorBlueR          105/255.0
#define colorBlueG          218/255.0
#define colorBlueB          222/255.0

@interface FindByRoomFloorViewV2 : UIViewController
    <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, PickerViewV2Delegate>
{
    UIButton *btnRoomStatus;
    UILabel *lblByRoomStatus;
    UITableView *tbvContent;
    UILabel *lblNoResult;
    NSMutableArray *listDisplayData;
    NSMutableArray *listTempData;
    UIButton *btnBack;
    BOOL isEdit;
    NSInteger roomStatusID;
    NSString* generalFilter;
    
    NSInteger filterType;
    BOOL isLoadFromWS;
    BOOL isLoadByFindRoomNo;
    NSString *selectedRoomStatus;
    IBOutlet UIImageView *imgHeader;
    
    //Data for picker view
    NSString* nameVC;
    NSString* nameOC;
    NSString* nameVD;
    NSString* nameOD;
    NSString* nameVI;
    NSString* nameOI;
    NSString* nameOOS;
    NSString* nameOOO;
    NSString* nameVPU;
    NSString* nameOPU;
    NSString* nameOCSO;
    NSString* namePending;
    NSString* nameDND;
    NSString* nameDoubleLock;
    NSString* nameServiceLater;
    NSString* nameDeclinedService;
    NSString* nameStarted;
    NSString* nameStop;
    NSString* nameCompleted;
    NSString* nameReassignPending;
    NSString* nameInspected;
    NSString* nameRushRoom;
    NSString* nameQueueRoom;
    NSString* nameUnassignRoom;
    NSString* nameOccupiedDueOut;
    NSString* nameProfileNotesExist;
    NSString* nameStayOver;
    NSString* nameODDO;
    NSString* nameAll;
    NSString* nameArrival;
    
    //Hold all data for pickerview
    NSMutableArray *listRoomStatusCleaningStatus;
}
@property (strong, nonatomic) IBOutlet UITextField *txtRoom;
@property (nonatomic, strong) IBOutlet UIButton *btnRoomStatus;
@property (nonatomic, strong) IBOutlet UILabel *lblByRoomStatus;
@property (nonatomic, strong) IBOutlet UITableView *tbvContent;
@property (nonatomic, strong) NSMutableArray *listDisplayData;
@property (nonatomic, strong) NSMutableArray *listTempData;
@property (nonatomic, strong) NSMutableArray *listTempFloor;
@property (strong, nonatomic) IBOutlet UIView *controlsView;
@property (readwrite, nonatomic) BOOL isLoadFromWS;
@property (readwrite, nonatomic) BOOL isLoadByFindRoomNo;
@property (strong, nonatomic) NSString *selectedRoomStatus;

-(IBAction)btnRoomStatusPressed:(id)sender;
-(void) loadData;
-(void) setCaptionsView;
-(void) setEnableTxtRoom:(BOOL) isEnable;
@end
