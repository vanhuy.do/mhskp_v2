//
//  FindByRoomViewV2.m
//  mHouseKeeping
//
//  Created by Nghia Truong on 2/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FindByRoomFloorViewV2.h"
#import "FindByRoomCellV2.h"
#import "RoomManagerV2.h"
#import "RoomAssignmentInfoViewController.h"
#import "FindByRoomFloorCellV2.h"
#import "FindByRoomViewV2.h"
#import "NetworkCheck.h"
#import "MyNavigationBarV2.h"
#import "LogFileManager.h"
#import "DeviceManager.h"
#import "ehkDefinesV2.h"
#import "UIImage+Tint.h"
#import "HomeViewV2.h"
#define ARRIVAL_KEY @"Arrival"
@interface FindByRoomFloorViewV2(PrivateMethods)

-(void) btnDetailArrowPressed:(NSInteger)index;
-(void) loadTopbarView;
-(void) showTopbarView;
-(void) hideTopbarView;
-(void) setHiddenTopbarView;
-(void) adjustShowForViews;
-(void) adjustRemoveForViews;
-(void) addButtonHandleShowHideTopbar;
-(void) topbarTapped:(UIButton *) sender;
-(TopbarViewV2 *) getTopBarView;
-(void) btnBackPressed;
-(void) addBackButton;
- (void)addAccessoryButton:(UITableViewCell *)cell  andIndexPath:(NSIndexPath *)indexPath;

@end
@implementation FindByRoomFloorViewV2
@synthesize controlsView;

@synthesize lblByRoomStatus, btnRoomStatus, tbvContent, listDisplayData, listTempData, isLoadFromWS, isLoadByFindRoomNo, selectedRoomStatus, txtRoom, listTempFloor;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle
-(void)viewWillAppear:(BOOL)animated {
    [self setCaptionsView];
    [super viewWillAppear:animated];
    [imgHeader setHidden:!isLoadByFindRoomNo];
    [self setEnableTxtRoom:isLoadByFindRoomNo];
    [btnRoomStatus setHidden:isLoadByFindRoomNo];
    //set selection find in bottom bar
    [[HomeViewV2 shareHomeView] setSelectedButton:tagOfFindButton];
    
    
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    //Resize content views of current VC to fit with Navigation bar and tabbar
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        [self loadFlatResource];
    }
    
    if(!isDemoMode){
        roomStatusID = 1;
        filterType = 1;
    } else {
        filterType = allRoomType;
    }
    
    [tbvContent setBackgroundView:nil];
    [tbvContent setBackgroundColor:[UIColor clearColor]];
    [tbvContent setOpaque:YES];
    
    [self performSelector:@selector(loadTopbarView)];
    [self addBackButton];
    
    //Multilanguage for picker view
    [self loadAllRoomStatusAndCleaningStatus];
    //Set label again
    if (selectedRoomStatus == nil) {
        if (isDemoMode) {
            self.selectedRoomStatus = @"All";
        } else {
            self.selectedRoomStatus = nameVC;
        }
    }
    [imgHeader setHidden:!isLoadByFindRoomNo];
    [self setEnableTxtRoom:isLoadByFindRoomNo];
    [btnRoomStatus setHidden:isLoadByFindRoomNo];
    [btnRoomStatus setTitle:selectedRoomStatus forState:UIControlStateNormal];
    
    // Load Data
    [self performSelector:@selector(loadData) withObject:nil afterDelay:0.0f];
}


-(void)responseLongPressButton:(UILongPressGestureRecognizer*)sender
{
    if (sender.state ==UIGestureRecognizerStateBegan) {
        if (tbvContent.editing) {
            [tbvContent setEditing:NO];
        }
        else
        {
            [tbvContent setEditing:YES];
        }
    }
    
}

- (void)viewDidUnload {
    [self setControlsView:nil];
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


#pragma mark -
#pragma mark UITableView Editing rows
-(NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSIndexPath *rowToSelect = indexPath;
    if (isEdit) {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        rowToSelect = nil;
    }
    return rowToSelect;
}

//- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
//	UITableViewCellEditingStyle style = UITableViewCellEditingStyleNone;
//    return  style;
//}
//
//-(BOOL)tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath{
//    if ([indexPath section] == 0) {
//        return NO;
//    }
//    return YES;
//}
//
//-(void)tableView:(UITableView *)tableView willBeginEditingRowAtIndexPath:(NSIndexPath *)indexPath{
//    if ([indexPath section] == 0) {
//        //redraw underline label
//        //UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
//        //[self drawUnderlinedLabel:(UILabel *)[cell viewWithTag:tagRMStatus] inTableViewCell:cell];
//    }
//}
//
//-(void)tableView:(UITableView *)tableView didEndEditingRowAtIndexPath:(NSIndexPath *)indexPath{
//    if ([indexPath section] == 0) {
//        [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationTop];
//    }
//}


#pragma mark - UITableView Delegate Methods
-(CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return heightTbvContent;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView
numberOfRowsInSection:(NSInteger)section {
    return [listTempData count];
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    cell.backgroundColor = [UIColor colorWithRed:colorWhite green:colorWhite
                                            blue:colorWhite alpha:colorAlpha];
}

-(UITableViewCell *)tableView:(UITableView *)tableView
        cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *IdentifyCell = @"IdentifyCell";
    FindByRoomFloorCellV2 *cell = nil;
    cell = [tableView dequeueReusableCellWithIdentifier:IdentifyCell];
    if(cell == nil){
        NSArray *array = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([FindByRoomFloorCellV2 class]) owner:self options:nil];
        cell = [array objectAtIndex:0];
    }
    
    FloorModelV2 *floorModel = (FloorModelV2*)[listTempData objectAtIndex:indexPath.row];
    
    cell.floorLabel.text = [NSString stringWithFormat:@"%@ %@",[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ios_unassign_floor], floorModel.floor_name];
    
    //        int numberOfUnAssignRoom = [UnassignManagerV2 numberOfUnAssignRoomOnFloor:floorModel.floor_id];
    //        cell.numOfRoomLabel.text = [NSString stringWithFormat:@"(%d)", numberOfUnAssignRoom];
    
    [cell.contentView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageBeforeiOS7:imgBgBigBtn equaliOS7:imgBgBigBtnFlat]]];
    [cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
    
    return cell;
}

- (void)addAccessoryButton:(UITableViewCell *)cell  andIndexPath:(NSIndexPath *)indexPath{
    /*****************************************/
    UIButton *accessoryButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 24, 24)];
    [accessoryButton setBackgroundColor:[UIColor clearColor]];
    //    [accessoryButton setImage:[UIImage imageNamed:@"gray_arrow_36x36.png"] forState:UIControlStateNormal];
    [accessoryButton setImage:[UIImage imageNamed:@"gray_arrow_36x36.png"] forState:UIControlStateNormal];
    [accessoryButton setTag:indexPath.row];
    
    [accessoryButton addTarget:self action:@selector(btnDetailPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell setAccessoryView:accessoryButton];
    //cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
    /*****************************************/
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    if (tableView == tbvContent) {
        [self btnDetailArrowPressed:indexPath.row];
    }
}

-(void) hiddenHUDAfterSaved:(MBProgressHUD *)HUD{
    [HUD hide:YES];
    [HUD removeFromSuperview];
    
}

#pragma mark - Load Data

-(int) getBuildingIdByUserId:(int) userId
{
    /*
    // get roomAssignment and get buildingId from roomassignment
    RoomAssignmentModelV2 *roomAssignment = [[RoomAssignmentModelV2 alloc] init];
    roomAssignment.roomAssignment_UserId = [UserManagerV2 sharedUserManager].currentUser.userId;
    
    NSMutableArray *roomAssignmentList = [[RoomManagerV2 sharedRoomManager] loadAllRoomAssignmentsByUserID:roomAssignment];
    if([roomAssignmentList count] > 0){
        roomAssignment = (RoomAssignmentModelV2*)[roomAssignmentList objectAtIndex:0];
    }
    
    RoomModelV2 *roomModel = [[RoomModelV2 alloc] init];
    roomModel.room_Id = roomAssignment.roomAssignment_RoomId;
    [[RoomManagerV2 sharedRoomManager] loadRoomModel:roomModel];
    
    //Hao Tran - Hard code building id if BuildingId doesn't has data
    if(roomModel.room_Building_Id <= 0){
        return 1;
    }
    
    return roomModel.room_Building_Id;
     */
    
    NSInteger buildingId = [[NSUserDefaults standardUserDefaults] integerForKey:[NSString stringWithFormat:@"%@%d", CURRENT_BUILDING_ID, [UserManagerV2 sharedUserManager].currentUser.userId]];
    if(buildingId <= 0) {
        return 1;
    }
    
    return (int)buildingId;
}

-(void)loadAllRoomStatusAndCleaningStatus
{
    //Hard code
    nameInspected = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_find_room_inspected];//@"Inspected";
    nameRushRoom = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_find_room_rush_room];//@"Rush Room";
    nameQueueRoom = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_find_room_queue_room]; //@"Queue Room";
    nameUnassignRoom = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_find_room_unassign]; //@"Unassign";
    nameOOS = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_find_room_oos]; //@"OOS";
    nameOccupiedDueOut = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_find_room_occupied_due_out]; //@"Occupied Due-Out";
    nameODDO = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_find_room_od_od]; //@"OD-DO"
    nameProfileNotesExist = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_find_room_profile_notes_exist];
    nameStayOver = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_find_room_stay_over];
    nameDoubleLock = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_find_room_double_lock];
    bool isEnglish = [[LanguageManagerV2 sharedLanguageManager] currentLanguageIsEqualTo:@"%@", ENGLISH_LANGUAGE];
    NSMutableArray *listRoomStatus = [[RoomManagerV2 sharedRoomManager] loadAllRoomStatus];
    NSMutableArray *listCleaningStatus = [[RoomManagerV2 sharedRoomManager] loadAllCleaningStatus];
    if(listRoomStatus.count <= 0){
        nameVC = @"VC";
        nameOC = @"OC";
        nameVD = @"VD";
        nameOD = @"OD";
        nameVI = @"VI";
        nameOI = @"OI";
        nameOOS = @"OOS";
        nameOOO = @"OOO";
        nameVPU = @"VPU";
        nameOPU = @"OPU";
        nameOCSO = @"OC-SO";
    } else {
        if(isEnglish){
            for (int i = 0; i < listRoomStatus.count; i ++) {
                RoomStatusModelV2 *curRoomStatus = [listRoomStatus objectAtIndex:i];
                // Not add Room Status to the list if rstat_FindStatus = 0
                if (curRoomStatus.rstat_FindStatus == 0) {
//                    continue;
                }
                if(curRoomStatus.rstat_Id == ENUM_ROOM_STATUS_VC){
                    nameVC = curRoomStatus.rstat_Name;
                } else if(curRoomStatus.rstat_Id == ENUM_ROOM_STATUS_OC){
                    nameOC = curRoomStatus.rstat_Name;
                } else if(curRoomStatus.rstat_Id == ENUM_ROOM_STATUS_VD){
                    nameVD = curRoomStatus.rstat_Name;
                } else if(curRoomStatus.rstat_Id == ENUM_ROOM_STATUS_OD){
                    nameOD = curRoomStatus.rstat_Name;
                } else if(curRoomStatus.rstat_Id == ENUM_ROOM_STATUS_VI){
                    nameVI = curRoomStatus.rstat_Name;
                } else if(curRoomStatus.rstat_Id == ENUM_ROOM_STATUS_OI){
                    nameOI = curRoomStatus.rstat_Name;
                } else if(curRoomStatus.rstat_Id == ENUM_ROOM_STATUS_OOS){
                    nameOOS = curRoomStatus.rstat_Name;
                } else if(curRoomStatus.rstat_Id == ENUM_ROOM_STATUS_OOO){
                    nameOOO = curRoomStatus.rstat_Name;
                } else if(curRoomStatus.rstat_Id == ENUM_ROOM_STATUS_VPU){
                    nameVPU = curRoomStatus.rstat_Name;
                } else if(curRoomStatus.rstat_Id == ENUM_ROOM_STATUS_OPU) {
                    nameOPU = curRoomStatus.rstat_Name;
                } else if(curRoomStatus.rstat_Id == ENUM_ROOM_STATUS_OC_SO){
                    nameOCSO = curRoomStatus.rstat_Name;
                }
            }
        } else {
            for (int i = 0; i < listRoomStatus.count; i ++) {
                RoomStatusModelV2 *curRoomStatus = [listRoomStatus objectAtIndex:i];
                // Not add Room Status to the list if rstat_FindStatus = 0
                if (curRoomStatus.rstat_FindStatus == 0) {
//                    continue;
                }
                if(curRoomStatus.rstat_Id == ENUM_ROOM_STATUS_VC){
                    nameVC = curRoomStatus.rstat_Lang;
                } else if(curRoomStatus.rstat_Id == ENUM_ROOM_STATUS_OC){
                    nameOC = curRoomStatus.rstat_Lang;
                } else if(curRoomStatus.rstat_Id == ENUM_ROOM_STATUS_VD){
                    nameVD = curRoomStatus.rstat_Lang;
                } else if(curRoomStatus.rstat_Id == ENUM_ROOM_STATUS_OD){
                    nameOD = curRoomStatus.rstat_Lang;
                } else if(curRoomStatus.rstat_Id == ENUM_ROOM_STATUS_VI){
                    nameVI = curRoomStatus.rstat_Lang;
                } else if(curRoomStatus.rstat_Id == ENUM_ROOM_STATUS_OI){
                    nameOI = curRoomStatus.rstat_Lang;
                } else if(curRoomStatus.rstat_Id == ENUM_ROOM_STATUS_OOS){
                    nameOOS = curRoomStatus.rstat_Lang;
                } else if(curRoomStatus.rstat_Id == ENUM_ROOM_STATUS_OOO){
                    nameOOO = curRoomStatus.rstat_Lang;
                } else if(curRoomStatus.rstat_Id == ENUM_ROOM_STATUS_VPU){
                    nameVPU = curRoomStatus.rstat_Lang;
                } else if(curRoomStatus.rstat_Id == ENUM_ROOM_STATUS_OPU) {
                    nameOPU = curRoomStatus.rstat_Lang;
                } else if(curRoomStatus.rstat_Id == ENUM_ROOM_STATUS_OC_SO){
                    nameOCSO = curRoomStatus.rstat_Lang;
                }
            }
        }
    }
    
    
    if(listCleaningStatus.count <= 0){
        namePending = @"Pending";
        nameDND = @"DND";
        nameServiceLater = @"Service Later";
        nameDeclinedService = @"Declined Service";
        nameStarted = @"Start";
        nameStop = @"Stop";
        nameCompleted = @"Completed";
        nameReassignPending = @"Reassign Pending";
    } else {
        if(isEnglish){
            for (int i = 0; i < listCleaningStatus.count; i ++) {
                CleaningStatusModelV2 *curCleaningStatus = [listCleaningStatus objectAtIndex:i];
                if(curCleaningStatus.cstat_Id == ENUM_CLEANING_STATUS_PENDING_FIRST_TIME){
                    namePending = curCleaningStatus.cstat_Name;
                } else if(curCleaningStatus.cstat_Id == ENUM_CLEANING_STATUS_DND){
                    nameDND = curCleaningStatus.cstat_Name;
                } else if(curCleaningStatus.cstat_Id == ENUM_CLEANING_STATUS_SERVICE_LATER){
                    nameServiceLater = curCleaningStatus.cstat_Name;
                } else if(curCleaningStatus.cstat_Id == ENUM_CLEANING_STATUS_DECLINE_SERVICE){
                    nameDeclinedService = curCleaningStatus.cstat_Name;
                }else if(curCleaningStatus.cstat_Id == ENUM_CLEANING_STATUS_START){
                    nameStarted = curCleaningStatus.cstat_Name;
                } else if(curCleaningStatus.cstat_Id == ENUM_CLEANING_STATUS_COMPLETED){
                    nameCompleted = curCleaningStatus.cstat_Name;
                }else if(curCleaningStatus.cstat_Id == ENUM_CLEANING_STATUS_PAUSE){
                    nameStop = curCleaningStatus.cstat_Name;
                }else if(curCleaningStatus.cstat_Id == ENUM_CLEANING_STATUS_PENDING_SECOND_TIME){
                    nameReassignPending = curCleaningStatus.cstat_Name;
                }
            }
        } else {
            for (int i = 0; i < listCleaningStatus.count; i ++) {
                CleaningStatusModelV2 *curCleaningStatus = [listCleaningStatus objectAtIndex:i];
                if(curCleaningStatus.cstat_Id == ENUM_CLEANING_STATUS_PENDING_FIRST_TIME){
                    namePending = curCleaningStatus.cstat_Language;
                } else if(curCleaningStatus.cstat_Id == ENUM_CLEANING_STATUS_DND){
                    nameDND = curCleaningStatus.cstat_Language;
                } else if(curCleaningStatus.cstat_Id == ENUM_CLEANING_STATUS_SERVICE_LATER){
                    nameServiceLater = curCleaningStatus.cstat_Language;
                } else if(curCleaningStatus.cstat_Id == ENUM_CLEANING_STATUS_DECLINE_SERVICE){
                    nameDeclinedService = curCleaningStatus.cstat_Language;
                } else if(curCleaningStatus.cstat_Id == ENUM_CLEANING_STATUS_START){
                    nameStarted = curCleaningStatus.cstat_Language;
                } else if(curCleaningStatus.cstat_Id == ENUM_CLEANING_STATUS_COMPLETED){
                    nameCompleted = curCleaningStatus.cstat_Language;
                }else if(curCleaningStatus.cstat_Id == ENUM_CLEANING_STATUS_PAUSE){
                    nameStop = curCleaningStatus.cstat_Language;
                }else if(curCleaningStatus.cstat_Id == ENUM_CLEANING_STATUS_PENDING_SECOND_TIME){
                    nameReassignPending = curCleaningStatus.cstat_Language;
                }
            }
        }
    }
    if (isEnglish) {
        nameAll = @"All";
    } else {
        nameAll = [L_ALL currentKeyToLanguage];
    }
    nameArrival = ARRIVAL_KEY;
    if(!isDemoMode){
        listRoomStatusCleaningStatus = [NSMutableArray array];
        if(nameVC)
            [listRoomStatusCleaningStatus addObject:@{@"name":nameVC, @"FilterType": @(roomStatusType), @"GeneralFilter": @(room_Status_Id_VC)}];
        if(nameVD)
            [listRoomStatusCleaningStatus addObject:@{@"name":nameVD, @"FilterType": @(roomStatusType), @"GeneralFilter": @(room_Status_Id_VD)}];
        if(nameOC)
            [listRoomStatusCleaningStatus addObject:@{@"name":nameOC, @"FilterType": @(roomStatusType), @"GeneralFilter": @(room_Status_Id_OC)}];
        if(nameOD)
            [listRoomStatusCleaningStatus addObject:@{@"name":nameOD, @"FilterType": @(roomStatusType), @"GeneralFilter": @(room_Status_Id_OD)}];
        if(nameVI)
            [listRoomStatusCleaningStatus addObject:@{@"name":nameVI, @"FilterType": @(roomStatusType), @"GeneralFilter": @(room_Status_Id_VI)}];
        if(nameOI)
            [listRoomStatusCleaningStatus addObject:@{@"name":nameOI, @"FilterType": @(roomStatusType), @"GeneralFilter": @(room_Status_Id_OI)}];
        if(nameVPU)
            [listRoomStatusCleaningStatus addObject:@{@"name":nameVPU, @"FilterType": @(roomStatusType), @"GeneralFilter": @(room_Status_Id_VPU)}];
        if(nameOPU)
            [listRoomStatusCleaningStatus addObject:@{@"name":nameOPU, @"FilterType": @(roomStatusType), @"GeneralFilter": @(ENUM_ROOM_STATUS_OPU)}];
        if(nameOOO)
            [listRoomStatusCleaningStatus addObject:@{@"name":nameOOO, @"FilterType": @(roomStatusType), @"GeneralFilter": @(room_Status_Id_OO)}];
        if(nameOOS)
            [listRoomStatusCleaningStatus addObject:@{@"name":nameOOS, @"FilterType": @(roomStatusType), @"GeneralFilter": @(room_Status_Id_OOS)}];
        if(nameOCSO)
            [listRoomStatusCleaningStatus addObject:@{@"name":nameOCSO, @"FilterType": @(roomStatusType), @"GeneralFilter": @(room_Status_Id_OCSO)}];
        if(nameDND)
            [listRoomStatusCleaningStatus addObject:@{@"name":nameDND, @"FilterType": @(cleaningStatusType), @"GeneralFilter": @(cleaning_Status_Id_DND)}];
        if(nameServiceLater)
            [listRoomStatusCleaningStatus addObject:@{@"name":nameServiceLater, @"FilterType": @(cleaningStatusType), @"GeneralFilter": @(cleaning_Status_Id_ServiceLater)}];
        if(nameDeclinedService)
            [listRoomStatusCleaningStatus addObject:@{@"name":nameDeclinedService, @"FilterType": @(cleaningStatusType), @"GeneralFilter": @(cleaning_Status_Id_DeclinedService)}];
        if(nameStarted)
            [listRoomStatusCleaningStatus addObject:@{@"name":nameStarted, @"FilterType": @(cleaningStatusType), @"GeneralFilter": @(ENUM_CLEANING_STATUS_START)}];
        if(nameCompleted)
            [listRoomStatusCleaningStatus addObject:@{@"name":nameCompleted, @"FilterType": @(cleaningStatusType), @"GeneralFilter": @(cleaning_Status_Id_Completed)}];
        if(nameStop)
            [listRoomStatusCleaningStatus addObject:@{@"name":nameStop, @"FilterType": @(cleaningStatusType), @"GeneralFilter": @(cleaning_Status_Id_Stop)}];
        if(namePending)
            [listRoomStatusCleaningStatus addObject:@{@"name":namePending, @"FilterType": @(cleaningStatusType), @"GeneralFilter": @(cleaning_Status_Id_Pending)}];
        if(nameReassignPending)
            [listRoomStatusCleaningStatus addObject:@{@"name":nameReassignPending, @"GeneralFilter": @(cleaning_Status_Id_Reassign_Pending)}];
        if(nameDoubleLock)
            [listRoomStatusCleaningStatus addObject:@{@"name":nameDoubleLock, @"FilterType": @(cleaningStatusType), @"GeneralFilter": @(cleaning_Status_Id_Double_Lock)}];
        if(nameInspected)
            [listRoomStatusCleaningStatus addObject:@{@"name":nameInspected, @"FilterType": @(roomStatusType), @"GeneralFilter": @(inspectedCase)}];
        if(nameRushRoom)
            [listRoomStatusCleaningStatus addObject:@{@"name":nameRushRoom, @"FilterType": @(kindOfRoomType), @"GeneralFilter": @(ENUM_KIND_OF_ROOM_RUSH)}];
        if(nameQueueRoom)
            [listRoomStatusCleaningStatus addObject:@{@"name":nameQueueRoom, @"FilterType": @(kindOfRoomType), @"GeneralFilter": @(ENUM_KIND_OF_ROOM_QUEUE)}];
//        if(nameProfileNotesExist)
//            [listRoomStatusCleaningStatus addObject:@{@"name":nameProfileNotesExist, @"FilterType": @(kindOfRoomType), @"GeneralFilter": @(ENUM_KIND_OF_ROOM_PROFILE_NOTES_EXISTS)}];
//        if(nameStayOver)
//            [listRoomStatusCleaningStatus addObject:@{@"name":nameStayOver, @"FilterType": @(kindOfRoomType), @"GeneralFilter": @(ENUM_KIND_OF_ROOM_STAY_OVER_EXISTS)}];
        if(nameOccupiedDueOut)
            [listRoomStatusCleaningStatus addObject:@{@"name":nameOccupiedDueOut, @"FilterType": @(roomOccupiedType), @"GeneralFilter": @(ENUM_ROOM_TYPE_ROOM_OCCUPIED_DUEOUT)}];
        if(nameUnassignRoom)
            [listRoomStatusCleaningStatus addObject:@{@"name":nameUnassignRoom, @"FilterType": @(unassignType), @"GeneralFilter": @(ENUM_ROOM_STATUS_UNASSIGN)}];
        if(nameODDO)
            [listRoomStatusCleaningStatus addObject:@{@"name":nameODDO, @"FilterType": @(roomOccupiedType), @"GeneralFilter": @(ENUM_ROOM_TYPE_OCCUPIED_OD_DO)}];
        [listRoomStatusCleaningStatus addObject:@{@"name":nameArrival, @"FilterType": @(arrivalType)}];
        if(nameAll)
            [listRoomStatusCleaningStatus addObject:@{@"name":nameAll, @"FilterType": @(allRoomType)}];
    } else {
        listRoomStatusCleaningStatus = [[NSMutableArray alloc] init];
        [listRoomStatusCleaningStatus addObject:@{@"name":nameAll, @"FilterType": @(allRoomType)}];
        [listRoomStatusCleaningStatus addObject:@{@"name":nameOC, @"FilterType": @(roomStatusType), @"GeneralFilter": @(room_Status_Id_OC)}];
        [listRoomStatusCleaningStatus addObject:@{@"name":nameOD, @"FilterType": @(roomStatusType), @"GeneralFilter": @(room_Status_Id_OD)}];
        [listRoomStatusCleaningStatus addObject:@{@"name":nameVC, @"FilterType": @(roomStatusType), @"GeneralFilter": @(room_Status_Id_VC)}];
        [listRoomStatusCleaningStatus addObject:@{@"name":nameVD, @"FilterType": @(roomStatusType), @"GeneralFilter": @(room_Status_Id_VD)}];
        [listRoomStatusCleaningStatus addObject:@{@"name":nameVI, @"FilterType": @(roomStatusType), @"GeneralFilter": @(room_Status_Id_VI)}];
        [listRoomStatusCleaningStatus addObject:@{@"name":nameOI, @"FilterType": @(roomStatusType), @"GeneralFilter": @(room_Status_Id_OI)}];
        [listRoomStatusCleaningStatus addObject:@{@"name":nameDND, @"FilterType": @(cleaningStatusType), @"GeneralFilter": @(cleaning_Status_Id_DND)}];
        [listRoomStatusCleaningStatus addObject:@{@"name":nameServiceLater, @"FilterType": @(cleaningStatusType), @"GeneralFilter": @(cleaning_Status_Id_ServiceLater)}];
        [listRoomStatusCleaningStatus addObject:@{@"name":nameDeclinedService, @"FilterType": @(cleaningStatusType), @"GeneralFilter": @(cleaning_Status_Id_DeclinedService)}];
        [listRoomStatusCleaningStatus addObject:@{@"name":nameStarted, @"FilterType": @(cleaningStatusType), @"GeneralFilter": @(ENUM_CLEANING_STATUS_START)}];
        [listRoomStatusCleaningStatus addObject:@{@"name":nameCompleted, @"FilterType": @(cleaningStatusType), @"GeneralFilter": @(cleaning_Status_Id_Completed)}];
        [listRoomStatusCleaningStatus addObject:@{@"name":nameStop, @"FilterType": @(cleaningStatusType), @"GeneralFilter": @(cleaning_Status_Id_Stop)}];
        [listRoomStatusCleaningStatus addObject:@{@"name":nameReassignPending, @"FilterType": @(cleaningStatusType), @"GeneralFilter": @(cleaning_Status_Id_Pending)}];
    }
}

-(void)loadData {
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getLoadingData]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES];
    
    if(isLoadByFindRoomNo) {
        generalFilter = [txtRoom text];
        filterType = findRoomNo;
    } else {
        if (filterType == allRoomType || filterType == arrivalType) {
            generalFilter = @"";
        } else {
            generalFilter = [NSString stringWithFormat:@"%d", (int)roomStatusID];
        }
    }
    [[HomeViewV2 shareHomeView] waitingLoadingData];
    
    int buildingId = [self getBuildingIdByUserId:[[[UserManagerV2 sharedUserManager]currentUser]userId]];
    
    if (isLoadFromWS) {
        if(roomStatusID == inspectedCase && filterType == roomStatusType) {
            if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
                listTempData = [[RoomManagerV2 sharedRoomManager] getOnlineFindFloorDetailListWith:[[[UserManagerV2 sharedUserManager]currentUser]userId] AndHotelId:[[[UserManagerV2 sharedUserManager]currentUser]userHotelsId] AndBuildingId:buildingId AndGeneralFilter:[NSString stringWithFormat:@"%d", ENUM_ROOM_STATUS_VI] AndFilterType:roomStatusType AndPercentView:HUD];//VI
                
                NSMutableArray *listFloorsOI = [[RoomManagerV2 sharedRoomManager] getOnlineFindFloorDetailListWith:[[[UserManagerV2 sharedUserManager]currentUser]userId] AndHotelId:[[[UserManagerV2 sharedUserManager]currentUser]userHotelsId] AndBuildingId:buildingId AndGeneralFilter:[NSString stringWithFormat:@"%d", ENUM_ROOM_STATUS_OI] AndFilterType:roomStatusType AndPercentView:HUD];//OI
                
                //Add OI Floors to VI Floors without duplicate floor
                for (FloorModelV2 *curOI in listFloorsOI) {
                    BOOL foundFloor = NO;
                    for (FloorModelV2 *curVI in listTempData) {
                        if (curOI.floor_id == curVI.floor_id) {
                            foundFloor = YES;
                            break;
                        }
                    }
                    
                    if(!foundFloor) {
                        [listTempData addObject:curOI];
                    }
                }
            } else {
                listTempData = [[UnassignManagerV2 sharedUnassignManager] loadFloorsHaveFindInspectedRoom:[[[UserManagerV2 sharedUserManager] currentUser] userId]];
            }
        } else {
            if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
                if (!isLoadByFindRoomNo) {
                    listTempData = [[RoomManagerV2 sharedRoomManager] getOnlineFindFloorDetailListWith:[[[UserManagerV2 sharedUserManager]currentUser]userId] AndHotelId:[[[UserManagerV2 sharedUserManager]currentUser]userHotelsId] AndBuildingId:buildingId AndGeneralFilter:generalFilter AndFilterType:filterType AndPercentView:HUD];
                }
            } else {
                //Load offline floors
                //Only floors having rooms have been loaded before returned list floors data
                switch (filterType) {
                    case roomStatusType:
                    {
                        listTempData = [[UnassignManagerV2 sharedUnassignManager] loadFloorsHaveFindRoom:[[[UserManagerV2 sharedUserManager] currentUser] userId] andRoomStatusID:roomStatusID];
                        break;
                    }
                        
                    case cleaningStatusType:
                    {
                        listTempData = [[UnassignManagerV2 sharedUnassignManager] loadFloorsHaveFindCleaningStatus:[[[UserManagerV2 sharedUserManager] currentUser] userId] andCleaningStatusID:roomStatusID];
                        break;
                    }
                    case unassignType:
                    {
                        listTempData = [[UnassignManagerV2 sharedUnassignManager] loadFloorsHaveFindRoom:[[[UserManagerV2 sharedUserManager] currentUser] userId] andRoomStatusID:roomStatusID];
                        break;
                    }
                    case kindOfRoomType:
                    {
                        listTempData = [[UnassignManagerV2 sharedUnassignManager] loadFloorsHaveFindKindOfRoom:[[[UserManagerV2 sharedUserManager] currentUser] userId] andKindOfRoom:roomStatusID];
                        break;
                    }
                    case roomOccupiedType:
                    {
                        listTempData = [[UnassignManagerV2 sharedUnassignManager] loadFloorsByUserId:[[[UserManagerV2 sharedUserManager] currentUser] userId] filterType:filterType statusId:roomStatusID];
                        break;
                    }
                    case allRoomType:
                    {
                        listTempData = [[UnassignManagerV2 sharedUnassignManager] loadFloors];
                    }
                    default:
                        break;
                }
            }
        }
        
        
        [tbvContent reloadData];
    }
    
    if (listTempData.count == 0) {
//        lblNoResult  =  [[UILabel alloc]init];
//        lblNoResult.frame     =  CGRectMake(15, 150, 290, 80);
//        [lblNoResult setText:[[LanguageManagerV2 sharedLanguageManager] getNoResultsFound]];
//        lblNoResult.textAlignment = NSTextAlignmentCenter;
//        lblNoResult.layer.cornerRadius = 10;
//        [lblNoResult setFont:[UIFont fontWithName:@"Arial-BoldMT" size:22]];
//        lblNoResult.numberOfLines = 2;
//        [lblNoResult setTextColor:[UIColor grayColor]];
//        //lblAlert.font = [UIFont boldSystemFontOfSize:20.0f];
        [tbvContent setSeparatorStyle:UITableViewCellSeparatorStyleNone];
//        [tbvContent addSubview:lblNoResult];
//    }
//    else{
//        if(lblNoResult){
//            [lblNoResult removeFromSuperview];
//        }
    }
    
    [self performSelector:@selector(hiddenHUDAfterSaved:) withObject:HUD afterDelay:0.5];
    
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
}

#pragma mark - Action for Button Detail Arrow
-(void) btnDetailPressed:(UIButton *) sender {
    [self btnDetailArrowPressed:sender.tag];
}

-(void)btnDetailArrowPressed:(NSInteger)index {
    
    FindByRoomViewV2 *findByRoomView = [[FindByRoomViewV2 alloc] initWithNibName:NSStringFromClass([FindByRoomViewV2 class]) bundle:nil];
    FloorModelV2 *floorModel = (FloorModelV2*)[listTempData objectAtIndex:index];
    findByRoomView.isLoadFromWS = TRUE;
    findByRoomView.generalFilterType = generalFilter;
    findByRoomView.floorId = floorModel.floor_id;
    findByRoomView.statusId = roomStatusID;
    
    if(isLoadByFindRoomNo)
    {
        findByRoomView.filterId = 6;
    }
    else{
        findByRoomView.filterId = filterType;
    }
    
    [self.navigationController pushViewController:findByRoomView animated:YES];
    
}

#pragma mark - Action for Button Room Status
-(IBAction)btnRoomStatusPressed:(id)sender {
    UIButton *button = (UIButton *) sender;
    
    //PickerViewV2 *picker = [[PickerViewV2 alloc] initPickerViewV2WithDatas:[NSArray arrayWithObjects: all,nameVC, nameOC, nameVD, nameOD, nameVI, nameOI, nameOOS, nameOOO, nil] AndSelectedData:button.titleLabel.text AndIndex:nil];
//    PickerViewV2 *picker = [[PickerViewV2 alloc] initPickerViewV2WithDatas:[NSArray arrayWithObjects: nameVC, nameVD, nameOC, nameOD, nameVI, nameOI, nameVPU, nameOCSO, namePending, nameDND, nameServiceLater, nameDeclinedService, nameCompleted, nameInspected, nameOOS, nameOOO, nameRushRoom, nameQueueRoom, nameUnassignRoom, nil] AndSelectedData:button.titleLabel.text AndIndex:nil];
    NSMutableArray *listTitle = [NSMutableArray new];
    for (NSDictionary *dict in listRoomStatusCleaningStatus) {
        [listTitle addObject:dict[@"name"]];
    }
    PickerViewV2 *picker = [[PickerViewV2 alloc] initPickerViewV2WithDatas:listTitle AndSelectedData:button.titleLabel.text AndIndex:nil];
    picker.delegate = self;
    [picker showPickerViewV2];
}

#pragma mark - PickerViewV2 Delegate Methods
-(void)didChooseFindRoomPickerViewV2WithData:(NSString *)data AndIndex:(NSIndexPath *)index WithtIndex:(NSInteger)tIndex{
    
    NSDictionary *dict = listRoomStatusCleaningStatus[tIndex];
    if(dict[@"FilterType"]){
        filterType = [dict[@"FilterType"] intValue];
    }
    if(dict[@"GeneralFilter"]){
        roomStatusID = [dict[@"GeneralFilter"] intValue];
    }
    
    [btnRoomStatus setTitle:data forState:UIControlStateNormal];
    self.selectedRoomStatus = data;
    
    [listTempData removeAllObjects];
    [listDisplayData removeAllObjects];
    
    // get data form WS
    self.isLoadFromWS = TRUE;
    [self performSelector:@selector(loadData) withObject:nil afterDelay:0.5];
}

- (void) didChooseDemoModeFindRoomPickerViewV2WithData:(NSString *)data AndIndex:(NSIndexPath *)index WithtIndex:(NSInteger)tIndex{
    NSDictionary *dict = listRoomStatusCleaningStatus[tIndex];
    if(dict[@"FilterType"]){
        filterType = [dict[@"FilterType"] intValue];
    }
    if(dict[@"GeneralFilter"]){
        roomStatusID = [dict[@"GeneralFilter"] intValue];
    }
    
    //    switch (tIndex) {
    //        case 0:
    //            //All
    //            filterType = allRoomType;
    //            break;
    //
    //        case 1:
    //            //OC
    //            roomStatusID = room_Status_Id_OC;
    //            filterType = roomStatusType;
    //            break;
    //
    //        case 2:
    //            //OD
    //            roomStatusID = room_Status_Id_OD;
    //            filterType = roomStatusType;
    //            break;
    //
    //        case 3:
    //            //VC
    //            roomStatusID = room_Status_Id_VC;
    //            filterType = roomStatusType;
    //            break;
    //
    //        case 4:
    //            //VD
    //            roomStatusID = room_Status_Id_VD;
    //            filterType = roomStatusType;
    //            break;
    //
    //        case 5:
    //            //VI
    //            roomStatusID = room_Status_Id_VI;
    //            filterType = roomStatusType;
    //            break;
    //
    //        case 6:
    //            //OI
    //            roomStatusID = room_Status_Id_OI;
    //            filterType = roomStatusType;
    //            break;
    //
    //        case 7:
    //            //DND
    //            roomStatusID = cleaning_Status_Id_DND;
    //            filterType = cleaningStatusType;
    //            break;
    //
    //        case 8:
    //            //Service Later
    //            roomStatusID = cleaning_Status_Id_ServiceLater;
    //            filterType = cleaningStatusType;
    //            break;
    //
    //        case 9:
    //            //Declined Service
    //            roomStatusID = cleaning_Status_Id_DeclinedService;
    //            filterType = cleaningStatusType;
    //            break;
    //
    //        case 10:
    //            //Completed
    //            roomStatusID = cleaning_Status_Id_Completed;
    //            filterType = cleaningStatusType;
    //            break;
    //        
    //        default:
    //            break;
    //    }
    
    [btnRoomStatus setTitle:data forState:UIControlStateNormal];
    self.selectedRoomStatus = data;
    
    [listTempData removeAllObjects];
    [listDisplayData removeAllObjects];
    
    // get data form WS
    self.isLoadFromWS = TRUE;
    [self performSelector:@selector(loadData) withObject:nil afterDelay:0.0];
}

#pragma mark  - Set Captions View
-(void)setCaptionsView {
    [self setTitle:[NSString stringWithFormat:@"%@ - %@ %@", [[LanguageManagerV2 sharedLanguageManager] getFind], [[LanguageManagerV2 sharedLanguageManager] getBy], [[LanguageManagerV2 sharedLanguageManager] getRoomTitle]]];
    if(isLoadByFindRoomNo)
    {
        [lblByRoomStatus setText:[[LanguageManagerV2 sharedLanguageManager] getByRoomNo]];
        [lblByRoomStatus setFont:[UIFont fontWithName:font size:sizeNormal]];
    }
    else{
        [lblByRoomStatus setText:[[LanguageManagerV2 sharedLanguageManager] getByRoomStatus]];
        [lblByRoomStatus setFont:[UIFont fontWithName:font size:sizeNormal]];
    }
    
    
//#warning modify here
    if(selectedRoomStatus == nil){
        if(isDemoMode){
            self.selectedRoomStatus = [L_ALL currentKeyToLanguage];;
        }
        else{
            self.selectedRoomStatus = nameVC;
        }//@"VC"
    }
//#warning end
    [btnRoomStatus setTitle:selectedRoomStatus forState:UIControlStateNormal];
    [btnRoomStatus.titleLabel setFont:[UIFont fontWithName:font size:sizeNormal]];
    
}

#pragma mark - === Handle Topbar Methods ===
#pragma mark
-(void)loadTopbarView {
    TopbarViewV2 *topbar = [[TopbarViewV2 alloc] initViewWithSuperModel:[[CommonVariable sharedCommonVariable] superRoomModel]];
    [topbar setTag:tagTopbarView];
    [self.view addSubview:topbar];
    [topbar setHidden:YES];
    
    [topbar setAlpha:0.0];
    [self addButtonHandleShowHideTopbar];
}

-(void)showTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationWillStartSelector:@selector(setHiddenTopbarView)];
    
    f = topbar.frame;
    f.origin.y = 0;
    [topbar setFrame:f];
    [topbar setAlpha:1.0];
    
    [self adjustShowForViews];
    
    [UIView commitAnimations];
}

-(void)hideTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDidStopSelector:@selector(setHiddenTopbarView)];
    [UIView setAnimationDelegate:self];
    
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    [topbar setAlpha:0.0];
    
    [self adjustRemoveForViews];
    
    [UIView commitAnimations];
}

-(void)setHiddenTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    [topbar setHidden:!topbar.hidden];
}

-(TopbarViewV2 *)getTopBarView {
    return (TopbarViewV2 *)[self.view viewWithTag:tagTopbarView];
}

-(void)addButtonHandleShowHideTopbar {
    UIView *vBarButton = [[UIView alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
    UIButton *titleBarButtonFirst = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 200, 30)];
    
    titleBarButtonFirst.titleLabel.backgroundColor = [UIColor clearColor];
    [titleBarButtonFirst.titleLabel setFont: FONT_BUTTON_TOPBAR];
    titleBarButtonFirst.titleLabel.textAlignment = NSTextAlignmentCenter;
    titleBarButtonFirst.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
    
    [titleBarButtonFirst.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
    [titleBarButtonFirst.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
    [titleBarButtonFirst addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    [titleBarButtonFirst setTitle:[[LanguageManagerV2 sharedLanguageManager] getFind] forState:UIControlStateNormal];
    
    [vBarButton addSubview:titleBarButtonFirst];
    
    
    UIButton *titleBarButtonSecond = [[UIButton alloc] initWithFrame:CGRectMake(0, 22, 200, 22)];
    [titleBarButtonSecond setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [titleBarButtonSecond.titleLabel setFont:FONT_BUTTON_TOPBAR_SECOND];
    [titleBarButtonSecond setTitle:[[LanguageManagerV2 sharedLanguageManager] getRoomTitle] forState:UIControlStateNormal];
    [titleBarButtonSecond addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    [vBarButton addSubview:titleBarButtonSecond];
    
    self.navigationItem.titleView = vBarButton;
    //Move title to center vertical
    CGRect titleViewBounds = self.navigationItem.titleView.bounds;
    int moveLeftTitle = 0;
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        moveLeftTitle = 10;
    }
    self.navigationItem.titleView.bounds = CGRectMake(titleViewBounds.origin.x + moveLeftTitle, Y_TOPBAR_TITLE, titleViewBounds.size.width, titleViewBounds.size.height);
    
}

-(void)topbarTapped:(UIButton *)sender {
    TopbarViewV2 *topbar = [self getTopBarView];
    [sender setEnabled:NO];
    if (topbar.isHidden) {
        [self showTopbarView];
    } else {
        [self hideTopbarView];
    }
    [self performSelector:@selector(topbarButtonEnable:) withObject:sender afterDelay:0.3];
}

-(void) topbarButtonEnable:(UIButton *) topbarBtn {
    [topbarBtn setEnabled:YES];
}

-(void)adjustShowForViews {
    //adjust all views when show
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    CGRect ftbv = controlsView.frame;
    [controlsView setFrame:CGRectMake(0, f.size.height, 320, ftbv.size.height)];
    
    ftbv = tbvContent.frame;
    [tbvContent setFrame:CGRectMake(0, f.size.height + controlsView.frame.size.height, 320, ftbv.size.height - f.size.height)];
}

-(void)adjustRemoveForViews {
    //adjust all views when hide
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    [controlsView setFrame:CGRectMake(0, 0, 320, controlsView.frame.size.height)];
    
    CGRect ftbv = tbvContent.frame;
    [tbvContent setFrame:CGRectMake(0, controlsView.frame.size.height, 320, ftbv.size.height + f.size.height)];
}

#pragma mark - === Handle Back Button ===
#pragma mark

-(void)backBarPressed {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)addBackButton {
    //back button navigation
    self.navigationItem.leftBarButtonItem = [MyNavigationBarV2 createBackButtonWithTarget:self selector:@selector(backBarPressed) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - iOS 7 Resources
-(void)loadFlatResource
{
    [btnRoomStatus setBackgroundImage:[UIImage imageNamed:imgOptionSetting_Flat] forState:UIControlStateNormal];
    [imgHeader setImage:[UIImage imageNamed:imgGuestInfoActiveFlat]];
}
-(void) setEnableTxtRoom:(BOOL) isEnable
{
    [txtRoom setHidden:(!isEnable)];
    [txtRoom setTextColor:[UIColor grayColor]];
}
-(void) loadFloorByRoomNo
{
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getRaSavingData]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES];
    int buildingId = [self getBuildingIdByUserId:[[[UserManagerV2 sharedUserManager]currentUser]userId]];
    if(isDemoMode)
    {
        if(isLoadByFindRoomNo)
        {
            if(![[txtRoom text] isEqualToString:@""])
            {
                generalFilter = [txtRoom text];
                filterType = findRoomNo;
                RoomManagerV2 *roomManager = [[RoomManagerV2 alloc] init];
                RoomModelV2 *roomModel = [[RoomModelV2 alloc]init];
                listTempData = [[NSMutableArray alloc] init];
                roomModel.room_Id = generalFilter;
                [roomManager loadRoomModel:roomModel];
                listTempFloor = [[UnassignManagerV2 sharedUnassignManager] loadFloors];
                for(FloorModelV2 *floorModel in listTempFloor)
                {
                    if(floorModel.floor_building_id == buildingId && floorModel.floor_id == roomModel.room_floor_id)
                    {
                        [listTempData addObject:floorModel];
                        break;
                    }
                }
            }
        }
    }
    else{
        if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp])
        {
            if(isLoadByFindRoomNo)
            {
                if(![[txtRoom text] isEqualToString:@""])
                {
                    generalFilter = [txtRoom text];
                    filterType = findRoomNo;
                    listTempData = [[RoomManagerV2 sharedRoomManager] getOnlineFindFloorDetailListWith:[[[UserManagerV2 sharedUserManager]currentUser]userId] AndHotelId:[[[UserManagerV2 sharedUserManager]currentUser]userHotelsId] AndBuildingId:buildingId AndGeneralFilter:generalFilter AndFilterType:filterType AndPercentView:HUD];
                }
            }
            
        }
    }
    if (listTempData.count == 0) {
        [tbvContent setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        [tbvContent addSubview:lblNoResult];
    }
    else{
        [lblNoResult removeFromSuperview];
    }
    [tbvContent reloadData];
    [HUD hide:YES];
    [HUD removeFromSuperview];
}
- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    if([textField text]) {
        generalFilter = [textField text];
    }
    [textField resignFirstResponder];
    [self performSelector:@selector(loadFloorByRoomNo) withObject:nil];
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if([string isEqualToString:@"\n"])
    {
        [textField resignFirstResponder];
        return NO;
    }
    return YES;
}

@end
