//
//  FindByRoomViewV2.h
//  mHouseKeeping
//
//  Created by Nghia Truong on 2/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PickerViewV2.h"

@interface FindByRoomViewV2 : UIViewController <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, UIGestureRecognizerDelegate> {
    UISearchBar *searchBar;
    UILabel *lblRoomNo;
    UILabel *lblRoomStatus;
    UILabel *lblVIP;
    UILabel *lblCleaningStatus;
    UITableView *tbvContent;
    NSMutableArray *listDisplayData;
    NSMutableArray *listTempData;
    UIButton *btnBack;
    BOOL isEdit;
    BOOL isLoadFromWS;
    BOOL isFirstLoadViewWillAppear;
    BOOL isDueRoom;
    BOOL isHaveReasonRemark;
    NSInteger floorId;
    NSInteger statusId;
    NSInteger filterId;
    NSMutableArray *roomAssignmentData;
}
@property (nonatomic, readwrite) NSString *generalFilterType;
@property (nonatomic, strong) IBOutlet UISearchBar *searchBar;
@property (nonatomic, strong) IBOutlet UILabel *lblRoomNo;
@property (nonatomic, strong) IBOutlet UILabel *lblRoomStatus;
@property (nonatomic, strong) IBOutlet UILabel *lblVIP;
@property (nonatomic, strong) IBOutlet UILabel *lblCleaningStatus;
@property (nonatomic, strong) IBOutlet UITableView *tbvContent;
@property (nonatomic, strong) NSMutableArray *listDisplayData;
@property (nonatomic, strong) NSMutableArray *listTempData;
@property (strong, nonatomic) IBOutlet UIView *controlsView;
@property (readwrite, nonatomic) BOOL isLoadFromWS;
@property (readwrite, nonatomic) BOOL isLoadFromZone;
@property BOOL isLoadFromFind;
@property (nonatomic) NSInteger floorId;
@property (nonatomic) NSInteger statusId;
@property (nonatomic) NSInteger filterId;
@property (nonatomic, retain) NSMutableArray *roomAssignmentData;

-(void) loadData;
-(void) setCaptionsView;
@end
