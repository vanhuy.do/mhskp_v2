//
//  FindByRoomViewV2.h
//  mHouseKeeping
//
//  Created by Nghia Truong on 2/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PickerViewV2.h"
#import "FindByRoomCellV2.h"
#import "CleaningStatusPopupView.h"
#import "StatusPopupView.h"
#import "ZoneModelV2.h"

@interface FindByZoneViewV2 : UIViewController <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, UIGestureRecognizerDelegate, PickerViewV2Delegate, FindByRoomCellV2Delegate, StatusPopupViewDelegate> {
    UISearchBar *searchBar;
    UILabel *lblRoomNo;
    UILabel *lblRoomStatus;
    UILabel *lblVIP;
    UILabel *lblCleaningStatus;
    UITableView *tbvContent;
    NSMutableArray *listDisplayData;
    NSMutableArray *listTempData;
    UIButton *btnBack;
    __weak IBOutlet UIButton *btnFilter;
    BOOL isEdit;
    BOOL isLoadFromWS;
    BOOL isFirstLoadViewWillAppear;
    BOOL isDueRoom;
    BOOL isHaveReasonRemark;
    NSInteger floorId;
    NSInteger statusId;
    NSInteger filterId;
    NSMutableArray *roomAssignmentData;
    NSMutableArray *zoneListData;
    NSMutableArray *zoneListFilter;
    StatusPopupView *cleaningStatusPopup;
    StatusPopupView *roomStatusPopup;
    NSMutableArray *listCleaningStatus;
    NSMutableArray *listRoomStatus;
    ZoneModelV2 *currentObject;
    PickerViewV2 *picker ;
}
@property (nonatomic, readwrite) NSString *generalFilterType;
@property (nonatomic, strong) IBOutlet UISearchBar *searchBar;
@property (nonatomic, strong) IBOutlet UILabel *lblRoomNo;
@property (nonatomic, strong) IBOutlet UILabel *lblRoomStatus;
@property (nonatomic, strong) IBOutlet UILabel *lblVIP;
@property (nonatomic, strong) IBOutlet UILabel *lblCleaningStatus;
@property (nonatomic, strong) IBOutlet UITableView *tbvContent;
@property (nonatomic, strong) NSMutableArray *listDisplayData;
@property (nonatomic, strong) NSMutableArray *listTempData;
@property (strong, nonatomic) IBOutlet UIView *controlsView;
@property (readwrite, nonatomic) BOOL isLoadFromWS;
@property (readwrite, nonatomic) BOOL isLoadFromZone;
@property (nonatomic) NSInteger floorId;
@property (nonatomic) NSInteger statusId;
@property (nonatomic) NSInteger filterId;
@property (nonatomic, retain) NSMutableArray *roomAssignmentData;
- (IBAction)didClickFilter:(UIButton *)sender;

-(void) loadData;
-(void) setCaptionsView;
@end
