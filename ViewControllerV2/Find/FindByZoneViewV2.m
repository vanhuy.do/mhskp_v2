//
//  FindByRoomViewV2.m
//  mHouseKeeping
//
//  Created by Nghia Truong on 2/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FindByZoneViewV2.h"
#import "FindByRoomCellV2.h"
#import "RoomManagerV2.h"
#import "RoomAssignmentInfoViewController.h"
#import "FindByRoomFloorViewV2.h"
#import "MyNavigationBarV2.h"
#import "LogFileManager.h"
#import "DeviceManager.h"
#import "UIImage+Tint.h"
#import "ehkDefinesV2.h"
#import "TasksManagerV2.h"
#import "HomeViewV2.h"
#import "UnassignManagerV2.h"
#import "PhysicalCheckManagerV2.h"


#define kRoomNo             @"RoomNo"
#define kGuestName          @"GuestName"
#define kRMStatus           @"RMStatus"
#define kCleaningStatus     @"CleaningStatus"
#define kVIP                @"VIP"
#define kRoomAssignmentID   @"RoomAssignmentID"

#define markImage           @"!.png"
#define backgroundImage     @"bg.png"

#define tagNoResult         12345
#define heightTbvContentCell    60.0f
#define heightRoomBlockingCell  133.0f //105.0f
#define heightTbvNoResult   240.0f

#define font                @"Arial-BoldMT"

#define sizeNoResult        22.0f
#define sizeNormal          17.0f
#define sizeSmall           10.0f

#define colorAlpha          1.0
#define colorWhite          255/255.0
#define colorBlueR          105/255.0
#define colorBlueG          218/255.0
#define colorBlueB          222/255.0

#define roomStatusType      1
#define cleaningStatusType  2


@interface FindByZoneViewV2(PrivateMethods)

-(void) btnDetailArrowPressed:(NSInteger)index;

-(void) loadTopbarView;
-(void) showTopbarView;
-(void) hideTopbarView;
-(void) setHiddenTopbarView;
-(void) adjustShowForViews;
-(void) adjustRemoveForViews;
-(void) addButtonHandleShowHideTopbar;
-(void) topbarTapped:(UIButton *) sender;
-(TopbarViewV2 *) getTopBarView;
-(void) btnBackPressed;
-(void) addBackButton;
- (void)addAccessoryButton:(UITableViewCell *)cell  andIndexPath:(NSIndexPath *)indexPath;

@end
@implementation FindByZoneViewV2
@synthesize controlsView;

@synthesize lblVIP, lblRoomNo, lblRoomStatus, lblCleaningStatus, searchBar, tbvContent,  listDisplayData, listTempData, isLoadFromWS, floorId, statusId, filterId, generalFilterType, roomAssignmentData;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle
-(void)viewWillAppear:(BOOL)animated {
    [self setCaptionsView];
    [super viewWillAppear:animated];
    
//    if(!isFirstLoadViewWillAppear) {
//        [self loadLocalData];
//    }
    isFirstLoadViewWillAppear = NO;
    
    //set selection find in bottom bar
    [[HomeViewV2 shareHomeView] setSelectedButton:tagOfFindButton];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //flag to check first load viewWillAppear
    isFirstLoadViewWillAppear = YES;
    
    //Resize content views of current VC to fit with Navigation bar and tabbar
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        //Clear redundant view for header, footter
        CGRect frameHeaderFooter = tbvContent.tableHeaderView.frame;
        frameHeaderFooter.size.height = 0.1f;
        UIView *headerView = [[UIView alloc] initWithFrame:frameHeaderFooter];
        [tbvContent setTableHeaderView:headerView];
        
        frameHeaderFooter = tbvContent.tableFooterView.frame;
        frameHeaderFooter.size.height = 0.1f;
        UIView *footerView = [[UIView alloc] initWithFrame:frameHeaderFooter];
        [tbvContent setTableFooterView:footerView];
    }
    
    [tbvContent setBackgroundView:nil];
    [tbvContent setBackgroundColor:[UIColor clearColor]];
    [tbvContent setOpaque:YES];
    
    [self performSelector:@selector(loadTopbarView)];
    
    [self addBackButton];
    
//    UILongPressGestureRecognizer *longpressGesture=[[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(responseLongPressButton:)];
//    longpressGesture.delegate=self;
//    longpressGesture.minimumPressDuration=2;
//    [tbvContent addGestureRecognizer:longpressGesture];
    [self loadCleaningStatusData];
    [self performSelector:@selector(getListZone) withObject:nil afterDelay:0.1];
}

- (void)loadCleaningStatusData {
    NSMutableArray *cleaningStatusList = [[RoomManagerV2 sharedRoomManager] loadAllCleaningStatus];
    listCleaningStatus = [NSMutableArray new];
    for (int index = 0; index < [cleaningStatusList count]; index++) {
        CleaningStatusModelV2 *model = (CleaningStatusModelV2 *) [cleaningStatusList objectAtIndex:index];
        [listCleaningStatus addObject:model];
    }
    NSMutableArray *roomStatusList = [[RoomManagerV2 sharedRoomManager] loadAllRoomStatus];
    listRoomStatus = [NSMutableArray new];
    for (int index = 0; index < [roomStatusList count]; index++) {
        RoomStatusModelV2 *roomModel = [roomStatusList objectAtIndex:index];
        [listRoomStatus addObject:roomModel];
    }
}
-(void)responseLongPressButton:(UILongPressGestureRecognizer*)sender
{
    if (sender.state ==UIGestureRecognizerStateBegan) {
        if (tbvContent.editing) {
            [tbvContent setEditing:NO];
        }
        else
        {
            [tbvContent setEditing:YES];
        }
    }
    
}

- (void)viewDidUnload {
    [self setControlsView:nil];
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


#pragma mark -
#pragma mark UITableView Editing rows
-(NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSIndexPath *rowToSelect = indexPath;
    if (isEdit) {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        rowToSelect = nil;
    }
    return rowToSelect;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
	UITableViewCellEditingStyle style = UITableViewCellEditingStyleNone;
    return  style;
}

#pragma mark -
#pragma mark UITableView Moving rows
-(BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath{
    BOOL canMove = NO;
    
    if (indexPath.section == 0) {
        NSDictionary *rowData = [listDisplayData objectAtIndex:indexPath.row];
        CleaningStatusModelV2 *csmodel = [[CleaningStatusModelV2 alloc] init];
        csmodel.cstat_Id = [[rowData objectForKey:kCleaningStatusID] intValue];
        [[RoomManagerV2 sharedRoomManager] loadCleaningStatus:csmodel];
        
        if ([csmodel.cstat_Name isEqualToString:@"DND"]) {
            return NO;
        }
        
        canMove = indexPath.row <= [listDisplayData count];
    }
    return canMove;
}

- (NSIndexPath *)tableView:(UITableView *)tableView targetIndexPathForMoveFromRowAtIndexPath:(NSIndexPath *)sourceIndexPath toProposedIndexPath:(NSIndexPath *)proposedDestinationIndexPath {
    NSDictionary *rowData = [listDisplayData objectAtIndex:proposedDestinationIndexPath.row];
    CleaningStatusModelV2 *csmodel = [[CleaningStatusModelV2 alloc] init];
    csmodel.cstat_Id = [[rowData objectForKey:kCleaningStatusID] intValue];
    [[RoomManagerV2 sharedRoomManager] loadCleaningStatus:csmodel];
    
    if ([csmodel.cstat_Name isEqualToString:@"DND"]) {
        return sourceIndexPath;
    }
    return proposedDestinationIndexPath;
}

-(void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath{
    if ([fromIndexPath isEqual:toIndexPath]) {
        return;
    }
    
    NSInteger rowFrom, rowTo;
    rowFrom = fromIndexPath.row;
    rowTo = toIndexPath.row;
    
    NSMutableDictionary *fromRowData = [[NSMutableDictionary alloc] initWithDictionary: [listDisplayData objectAtIndex:fromIndexPath.row]];
    NSDictionary *toRowData = [listDisplayData objectAtIndex:toIndexPath.row];
    
    NSInteger priorityFrom, priorityTo;
    priorityTo = [[toRowData valueForKey:kRaPrioritySortOrder] integerValue];
    priorityFrom = [[fromRowData valueForKey:kRaPrioritySortOrder] integerValue];
    //Change priorities
    if (rowFrom < rowTo) {
        // To Priority + 1
        priorityFrom = priorityTo + 1;
    } else if (rowFrom > rowTo) {
        // To Priority - 1
        priorityFrom = priorityTo - 1;
    }
    
    NSString *priorityFromStr = [[NSString alloc] initWithFormat:@"%d", (int)priorityFrom];
    
    [fromRowData setValue:priorityFromStr forKey:kRaPrioritySortOrder];
    
    [listDisplayData removeObjectAtIndex:fromIndexPath.row];
    
    [listDisplayData insertObject:fromRowData atIndex:toIndexPath.row];
    
    //update room assignment priority
    RoomAssignmentModelV2 *model = [[RoomAssignmentModelV2 alloc] init];
    
    NSInteger raID = [[fromRowData objectForKey:kRoomAssignmentID] integerValue];
    NSInteger priority = [[fromRowData objectForKey:kRaPrioritySortOrder] integerValue];
    model.roomAssignment_Id = (int)raID;
    model.roomAssignment_UserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    [[RoomManagerV2 sharedRoomManager] load:model];
    model.roomAssignment_Priority_Sort_Order = priority;
    
    [[RoomManagerV2 sharedRoomManager] update:model];
}

-(BOOL)tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([indexPath section] == 0) {
        return NO;
    }
    return YES;
}

-(void)tableView:(UITableView *)tableView willBeginEditingRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([indexPath section] == 0) {
        //redraw underline label
        //UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        //[self drawUnderlinedLabel:(UILabel *)[cell viewWithTag:tagRMStatus] inTableViewCell:cell];
    }
}

-(void)tableView:(UITableView *)tableView didEndEditingRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([indexPath section] == 0) {
        [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationTop];
    }
}


#pragma mark - UITableView Delegate Methods
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if([self.listDisplayData count] > 0) {
        NSDictionary *rowData = [self.listDisplayData objectAtIndex:indexPath.row];
        NSString *roomNo = (NSString *)[rowData objectForKey:kRoomNo];
        if(roomNo.length > 0) {
            RoomBlocking *curRoomBlocking = [[RoomManagerV2 sharedRoomManager] loadRoomBlockingByUserId:[UserManagerV2 sharedUserManager].currentUser.userId roomNumber:roomNo];
            NSUInteger row = [indexPath row];
            NSDictionary *rowData =(NSDictionary*)[self.listDisplayData objectAtIndex:row];
            NSString *guestArrivalTime = (NSString *)[rowData objectForKey:kRaGuestArrivalTime];
            NSString *guestDepartureTime = (NSString *)[rowData objectForKey:kRaGuestDepartureTime];
            NSString *guestName = [rowData objectForKey:kGuestName];
            int roomIndicatorGuest = [self getRoomIndicatorGuestByArrivalDate:guestArrivalTime departureDate:guestDepartureTime];
            isDueRoom = (roomIndicatorGuest == RoomIndicatorGuestDueIn) || (roomIndicatorGuest == RoomIndicatorGuestDueOut) || (roomIndicatorGuest == RoomIndicatorGuestBackToBack);
            [LogFileManager logDebugMode:@"isdue = %@", isDueRoom ? @"True" : @"False"];
            
            isHaveReasonRemark = (curRoomBlocking.roomblocking_reason.length > 0) || (curRoomBlocking.roomblocking_remark_physical_check.length > 0 || curRoomBlocking.roomblocking_oosdurations.length > 0);
            
            if(curRoomBlocking != nil
               && curRoomBlocking.roomblocking_is_blocked > RoomBlockingStatus_Normal
               && isHaveReasonRemark
               && (isDueRoom || guestName.length > 0)) {
                
                //Extend the height when room was blocked
                return heightRoomBlockingCell;
            }
            else if (curRoomBlocking != nil
                     && curRoomBlocking.roomblocking_is_blocked > RoomBlockingStatus_Normal
                     && isHaveReasonRemark
                     && !isDueRoom && guestName.length == 0)
            {
                return 107;
            }
        }
    }
    return 65;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView
numberOfRowsInSection:(NSInteger)section {
    if(tableView == tbvContent)
        return [listDisplayData count];
    
    return 1;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
//    cell.backgroundColor = [UIColor colorWithRed:colorWhite green:colorWhite
//                                            blue:colorWhite alpha:colorAlpha];
}
- (void)didClickStatus:(id)data{
    if (roomStatusPopup == nil) {
        roomStatusPopup = [[StatusPopupView alloc] initWithDelegate:self withTitle:[L_room_status currentKeyToLanguage] withData:listRoomStatus];
    }
    roomStatusPopup.data = data;
    [self.view addSubview:roomStatusPopup];
}
- (void)didClickClean:(id)data{
    if (cleaningStatusPopup == nil) {
        cleaningStatusPopup = [[StatusPopupView alloc] initWithDelegate:self withTitle:[L_CLEANING_STATUS currentKeyToLanguage] withData:listCleaningStatus];
    }
    cleaningStatusPopup.data = data;
    [self.view addSubview:cleaningStatusPopup];
}
- (void)didClickItemAtIndex:(id)object withData:(NSDictionary*)data{
    [cleaningStatusPopup removeFromSuperview];
    [roomStatusPopup removeFromSuperview];
    NSInteger cleaningStatus = [(NSString *)[data objectForKey:kCleaningStatusID] integerValue];
    NSInteger *roomStatus = [(NSString *)[data objectForKey:kRoomStatusId] integerValue];

    if ([object isKindOfClass:[CleaningStatusModelV2 class]]) {
        CleaningStatusModelV2 *data = (CleaningStatusModelV2*)object;
        cleaningStatus = data.cstat_Id;
        
    }else if ([object isKindOfClass:[RoomStatusModelV2 class]]) {
        RoomStatusModelV2 *data = (RoomStatusModelV2*)object;
        roomStatus = data.rstat_Id;
    }
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getSavingData]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES];
    [[HomeViewV2 shareHomeView] waitingLoadingData];
    [self.navigationController.view setUserInteractionEnabled:NO];
    
    [[PhysicalCheckManagerV2 sharedPhysicalCheckManagerV2] postPhysicalCheckWithUserId:[[[UserManagerV2 sharedUserManager]currentUser]userId] andRoomNo:(NSString *)[data objectForKey:kRoomNo] andHotelID:[[[UserManagerV2 sharedUserManager]currentUser]userHotelsId] andStatusId:roomStatus andCleaningStatusId:cleaningStatus];
    
    [self loadListDetail:currentObject withHUD:HUD];
    
    [self.navigationController.view setUserInteractionEnabled:YES];
    [self performSelector:@selector(hiddenHUDAfterSaved:) withObject:HUD afterDelay:0.5];
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
}
-(UITableViewCell *)tableView:(UITableView *)tableView
        cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentify = @"cellIdentify";
    
    FindByRoomCellV2 *cell = nil;
    cell = (FindByRoomCellV2 *)[tableView dequeueReusableCellWithIdentifier:cellIdentify];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    if (cell == nil) {
        NSArray *arrayCell = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([FindByRoomCellV2 class]) owner:self options:nil];
        cell = [arrayCell objectAtIndex:0];
        [cell.lblRoomStatus setFont:[UIFont fontWithName:font size:sizeNormal]];
        [cell.lblRoomNo setFont:[UIFont fontWithName:font size:sizeNormal]];
        [cell.lblVIP setFont:[UIFont fontWithName:font size:sizeNormal]];
        cell.delegate = self;
        [cell initEvent];
    }
    
    NSDictionary *rowData = [self.listDisplayData objectAtIndex:indexPath.row];
    cell.rowData = rowData;
    UILabel *lblRoomNoCell = (UILabel *)[cell viewWithTag:tagRoomNo];
    UILabel *lblRoomStatusCell = (UILabel *)[cell viewWithTag:tagRMStatus];
    
 
    UILabel *lblVipRoom = (UILabel *)[cell viewWithTag:tagVIP];
    UIImageView *imgFirstIconCleaningStatus = (UIImageView *)[cell viewWithTag:tagFirstIconCleaningStatus];
    UIImageView *imgSecondIconCleaningStatus = (UIImageView *)[cell viewWithTag:tagSecondIconCleaningStatus];
    UILabel *lblServiceLaterTime = (UILabel *)[cell viewWithTag:tagTimeServiceLater];
    UIImageView *imgRushOrQueueRoom = (UIImageView *)[cell viewWithTag:tagRushOrQueueRoom];
    UILabel *lblDueOutRoom = (UILabel *)[cell viewWithTag:tagDueOutRoom];
    UILabel *lblDepartureTime = (UILabel *)[cell viewWithTag:tagDepartureTime];
    UILabel *lblDueInRoom = (UILabel *)[cell viewWithTag:tagDueInRoom];
    UILabel *lblArrivalTime = (UILabel *)[cell viewWithTag:tagArrivalTime];
    UILabel *lblGuestName = (UILabel *)[cell viewWithTag:tagGuestName];
    UILabel *lblRAName = (UILabel *)[cell viewWithTag:tagRAName];
    UILabel *lblRoomType = (UILabel *)[cell viewWithTag:tagRoomType];
    UIImageView *imgProfileNotes = (UIImageView *)[cell viewWithTag:tagProfileNotes]; // CFG [20160928/CRF-00000827]
    UIImageView *imgStayOver = (UIImageView *)[cell viewWithTag:tagStayOver]; // CRF [20161106/CRF-00001293]
    NSString *roomNo = (NSString *)[rowData objectForKey:kRoomNo];
    NSString *guestName = [rowData objectForKey:kGuestName];
    NSString *vip = (NSString *)[rowData objectForKey:kVIP];
    NSInteger cleaningStatus = [(NSString *)[rowData objectForKey:kCleaningStatusID] integerValue];
    NSString *roomStatus = (NSString *)[rowData objectForKey:kRMStatus];
    NSString *guestArrivalTime = (NSString *)[rowData objectForKey:kRaGuestArrivalTime];
    NSString *guestDepartureTime = (NSString *)[rowData objectForKey:kRaGuestDepartureTime];
    NSInteger kindOfRoom = [(NSString *)[rowData objectForKey:kRaKindOfRoom] integerValue];
    NSInteger isMockRoom = [(NSString *)[rowData objectForKey:kRaIsMockRoom] integerValue];
    NSInteger isReassignedRoom = [(NSString *)[rowData objectForKey:kRaIsReassignedRoom] integerValue];
    NSData *cleaningStatusIcon = (NSData *)[rowData objectForKey:kCleaningStatus];
    NSString *roomAssignmentTime = (NSString *)[rowData objectForKey:kRoomAssignmentTime];
    NSString *roomTypeValue = (NSString *)[rowData objectForKey:kRoomType];
    
    //20150108 - Show icon previous day
    UIImageView *imgPreviousDate = (UIImageView *)[cell viewWithTag:tagPreviousDate];
    int roomStatusID = [[rowData objectForKey:kRoomStatusId] intValue];
    if (roomStatusID == ENUM_ROOM_STATUS_OC || roomStatusID == ENUM_ROOM_STATUS_VC) {
        if ([[rowData objectForKey:kLastCleaningDate] length] > 0) {
            NSDateFormatter *dFormat = [[NSDateFormatter alloc] init];
            [dFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            //[dFormat setLocale:[NSLocale currentLocale]];
            //[dFormat setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
            
            if([[rowData objectForKey:kLastCleaningDate] isEqualToString:@"1900-01-01 00:00:00"]) {
                
                imgPreviousDate.hidden = YES;
                
            } else if ([DateTimeUtility compareDatetimeValue:[rowData objectForKey:kLastCleaningDate]] == DateComparison_LessThanCurrent) {
                imgPreviousDate.hidden = NO;
                [imgPreviousDate setFrame:CGRectMake(self.view.bounds.size.width - 30,
                                                     25,
                                                     imgPreviousDate.frame.size.width,
                                                     imgPreviousDate.frame.size.height)];
            } else {
                imgPreviousDate.hidden = YES;
            }
        } else if([roomStatus isEqualToString:ROOM_STATUS_VD] || [roomStatus isEqualToString:ROOM_STATUS_OD]){
            imgPreviousDate.hidden = YES;
        }
        else {
            imgPreviousDate.hidden = YES;
        }
    } else {
        imgPreviousDate.hidden = YES;
    }
    [lblRoomNoCell setText:roomNo];
    [lblRoomStatusCell setText:roomStatus];
    [lblVipRoom setText:(vip == nil || [vip isEqualToString:@""] || [vip isEqualToString:@"0"])?@"-":vip];
    [lblDueOutRoom setText:@"[D/O]"];
    [lblArrivalTime setText:guestArrivalTime];
    [lblGuestName setText:(guestName == nil || [guestName isEqualToString:@""])?@"-":guestName];
    
    // CFG [20160913/CRF-00001439] - Changed to binary AND operation.
    if ((kindOfRoom & ENUM_KIND_OF_ROOM_RUSH) == ENUM_KIND_OF_ROOM_RUSH) {
        [imgRushOrQueueRoom setHidden:NO];
        [imgRushOrQueueRoom setImage:[UIImage imageBeforeiOS7:imgRushRoom equaliOS7:imgRushRoomFlat]];
    }
    else if ((kindOfRoom & ENUM_KIND_OF_ROOM_QUEUE) == ENUM_KIND_OF_ROOM_QUEUE) {
        [imgRushOrQueueRoom setHidden:NO];
        [imgRushOrQueueRoom setImage:[UIImage imageBeforeiOS7:imgQueueRoom equaliOS7:imgQueueRoomFlat]];
    }
    else {
//        if([LogFileManager isLogConsole])
//        {
//            NSLog(@"OUT OF VALUE RANGE");
//        }
        [imgRushOrQueueRoom setHidden:YES];
    }
    // CFG [20160913/CRF-00001439] - End.
    
    // CFG [20160928/CRF-00000827] - Show notes icon.
    if ((kindOfRoom & ENUM_KIND_OF_ROOM_PROFILE_NOTES_EXISTS) == ENUM_KIND_OF_ROOM_PROFILE_NOTES_EXISTS) {
        [imgProfileNotes setHidden:NO];
    }
    else {
        [imgProfileNotes setHidden:YES];
    }
    // CFG [20160928/CRF-00000827] - End.
    
    // CRF [20161106/CRF-00001293] - Show notes icon.
    if ((kindOfRoom & ENUM_KIND_OF_ROOM_STAY_OVER_EXISTS) == ENUM_KIND_OF_ROOM_STAY_OVER_EXISTS) {
        [imgStayOver setHidden:NO];
        if(imgProfileNotes.isHidden == NO) {
            [imgStayOver setFrame:CGRectMake(imgProfileNotes.frame.origin.x - imgProfileNotes.frame.size.width, imgProfileNotes.frame.origin.y, imgProfileNotes.frame.size.width, imgProfileNotes.frame.size.height)];
        } else {
            [imgStayOver setFrame:imgProfileNotes.frame];
        }
    }
    else {
        [imgStayOver setHidden:YES];
    }
    // CRF [20161106/CRF-00001293] - End.
    
    int roomIndicatorGuest = [self getRoomIndicatorGuestByArrivalDate:guestArrivalTime departureDate:guestDepartureTime];
    
    if(roomIndicatorGuest == RoomIndicatorGuestDueIn) { //light yellow green color
        [cell setBackgroundColor:[UIColor colorWithRed:253/255.0f green:255/255.0f blue:206/255.0f alpha:1]];
        
        [lblDueInRoom setHidden:YES];
        [lblArrivalTime setHidden:YES];
        
        //Warning! Temporary use label Due Out and label departure time for Arrival time
        //[lblDueOutRoom setFrame:lblDueOutFrame];//Hao Tran - Remove for uneccessary frame for cell in HomeViewV2
        
        //Change frames for Due Out Infor
        //Decreasing width of Guest Infor
        if(lblDepartureTime.isHidden /*&& lblDueOutRoom.isHidden*/) {
            CGRect frlblDueOut = lblDueOutRoom.frame;
            CGRect frlblDepartureTime = lblDepartureTime.frame;
            CGRect frlblGuestName = lblGuestName.frame;
            frlblGuestName.size.width -= (frlblDueOut.size.width + frlblDepartureTime.size.width + 2);
            frlblGuestName.origin.x += (frlblDueOut.size.width + frlblDepartureTime.size.width + 2);
            [lblGuestName setFrame:frlblGuestName];
            [lblDueOutRoom setHidden:NO];
            [lblDepartureTime setHidden:NO];
        }
        
        [lblDueOutRoom setText:@"[D/I]"];
        [lblDepartureTime setText:[ehkConvert DateToStringWithString:guestArrivalTime fromFormat:@"yyyy-MM-dd HH:mm:ss" toFormat:@"HH:mm"]];
        //[lblGuestName setFrame:lblGuestNameFrameInCase3];
        [lblGuestName setHidden:YES];
        
    }
    else if(roomIndicatorGuest == RoomIndicatorGuestDueOut) { //light green color
        [cell setBackgroundColor:[UIColor colorWithRed:172/255.0f green:255/255.0f blue:203/255.0f alpha:1]];
        
        if(guestName.length > 0) {
            [lblGuestName setHidden:NO];
            [lblGuestName setText:guestName];
        } else {
            //No need to check guest for displaying green background - matching with Android
            //[cell setBackgroundColor:[UIColor whiteColor]];
            [lblGuestName setHidden:YES];
        }
        
        [lblDueInRoom setHidden:YES];
        [lblArrivalTime setHidden:YES];
        
        //[lblDueOutRoom setFrame:lblDueOutFrame];//Hao Tran - Remove for uneccessary frame for cell in HomeViewV2
        
        //Change frames for Due Out Infor
        //Decreasing width of Guest Infor
        if(lblDepartureTime.isHidden /*&& lblDueOutRoom.isHidden*/) {
            CGRect frlblDueOut = lblDueOutRoom.frame;
            CGRect frlblDepartureTime = lblDepartureTime.frame;
            CGRect frlblGuestName = lblGuestName.frame;
            frlblGuestName.size.width -= (frlblDueOut.size.width + frlblDepartureTime.size.width + 2);
            frlblGuestName.origin.x += (frlblDueOut.size.width + frlblDepartureTime.size.width + 2);
            [lblGuestName setFrame:frlblGuestName];
            [lblDueOutRoom setHidden:NO];
            [lblDepartureTime setHidden:NO];
        }
        
        [lblDueOutRoom setText:@"[D/O]"];
        [lblDepartureTime setText:[ehkConvert DateToStringWithString:guestDepartureTime fromFormat:@"yyyy-MM-dd HH:mm:ss" toFormat:@"HH:mm"]];
        //[lblGuestName setFrame:lblGuestNameFrameInCase3];
        
    }
    else if(roomIndicatorGuest == RoomIndicatorGuestBackToBack) { //light green color
        [cell setBackgroundColor:[UIColor colorWithRed:172/255.0f green:255/255.0f blue:203/255.0f alpha:1]];
        
        //Change frames for Due Out Infor
        //Decreasing width of Guest Infor
        if(lblDepartureTime.isHidden /*&& lblDueOutRoom.isHidden*/) {
            CGRect frlblDueOut = lblDueOutRoom.frame;
            CGRect frlblDepartureTime = lblDepartureTime.frame;
            CGRect frlblGuestName = lblGuestName.frame;
            frlblGuestName.size.width -= (frlblDueOut.size.width + frlblDepartureTime.size.width + 2);
            frlblGuestName.origin.x += (frlblDueOut.size.width + frlblDepartureTime.size.width + 2);
            [lblGuestName setFrame:frlblGuestName];
            [lblDueOutRoom setHidden:NO];
            [lblDepartureTime setHidden:NO];
        }
        
        [lblDueOutRoom setText:@"[D/O]"];
        [lblDepartureTime setText:[ehkConvert DateToStringWithString:guestDepartureTime fromFormat:@"yyyy-MM-dd HH:mm:ss" toFormat:@"HH:mm"]];
        [lblDueInRoom setHidden:NO];
        [lblDueInRoom setText:@"[D/I]"];
        [lblArrivalTime setHidden:NO];
        [lblArrivalTime setText:[ehkConvert DateToStringWithString:guestArrivalTime fromFormat:@"yyyy-MM-dd HH:mm:ss" toFormat:@"HH:mm"]];
        
        //[lblGuestName setFrame:lblGuestNameFrameInCase3];
        if(guestName.length > 0) {
            [lblGuestName setHidden:NO];
            [lblGuestName setText:guestName];
        } else {
            [lblGuestName setHidden:YES];
        }
        
    }
    else { //Normal
        [cell setBackgroundColor:[UIColor whiteColor]];
        
        //Change frames for Due Out Infor
        //Increasing width of Guest Infor
        if(!lblDepartureTime.isHidden) {
            CGRect frlblDueOut = lblDueOutRoom.frame;
            CGRect frlblDepartureTime = lblDepartureTime.frame;
            CGRect frlblGuestName = lblGuestName.frame;
            frlblGuestName.size.width += (frlblDueOut.size.width + frlblDepartureTime.size.width + 2);
            frlblGuestName.origin.x -= (frlblDueOut.size.width + frlblDepartureTime.size.width + 2);
            [lblGuestName setFrame:frlblGuestName];
            [lblDepartureTime setHidden:YES];
            [lblDueOutRoom setHidden:YES];
        }
        
        [lblDueInRoom setHidden:YES];
        [lblArrivalTime setHidden:YES];
        
        if(guestName.length > 0) {
            [lblGuestName setHidden:NO];
            [lblGuestName setText:guestName];
        } else {
            [lblGuestName setHidden:YES];
            [lblDueOutRoom setText:@"-"];
        }
    }
    
    [cell setAccessoryType:UITableViewCellAccessoryNone];
    
    //CRF-0000682: blocking room sign, remark and reason
    RoomBlocking *curRoomBlocking = [[RoomManagerV2 sharedRoomManager] loadRoomBlockingByUserId:[UserManagerV2 sharedUserManager].currentUser.userId roomNumber:roomNo];
    UIImageView *blockRoomStatus = (UIImageView*)[cell.contentView viewWithTag:tagBlockingImage];
    UILabel *lblBlockReason = (UILabel*)[cell.contentView viewWithTag:tagBlockingReason];
    UILabel *lblBlockRemark = (UILabel*)[cell.contentView viewWithTag:tagBlockingRemark];
    UILabel *lblAddReason = (UILabel*)[cell.contentView viewWithTag:tagAddition];
    UILabel *lblBlockDuration = (UILabel*)[cell.contentView viewWithTag:tagOOSDuration];
    
    isHaveReasonRemark = (curRoomBlocking.roomblocking_reason.length > 0) || (curRoomBlocking.roomblocking_remark_physical_check.length > 0 || curRoomBlocking.roomblocking_oosdurations.length > 0);
    
    if(curRoomBlocking.roomblocking_is_blocked > 0 && !isDueRoom  && guestName.length == 0 )
    {
        if(curRoomBlocking.roomblocking_is_blocked == RoomBlockingStatus_Release) {
            [blockRoomStatus setImage:[UIImage imageNamed:imgReleaseRoom]];
        } else {
            [blockRoomStatus setImage:[UIImage imageNamed:imgBlockRoom]];
        }
        
        [blockRoomStatus setHidden:NO];
        
        if(isHaveReasonRemark){
            NSString *reason = [NSString stringWithFormat:@"%@: %@", [L_reason currentKeyToLanguage], curRoomBlocking.roomblocking_reason];
            NSString *remark = [NSString stringWithFormat:@"%@: %@",[L_remark_title currentKeyToLanguage], curRoomBlocking.roomblocking_remark_physical_check];
            NSString *blockingDuration =  [NSString stringWithFormat:@"%@: %@",[L_oos_duration currentKeyToLanguage], curRoomBlocking.roomblocking_oosdurations];
            
            [lblAddReason setHidden:NO];
            [lblBlockReason setHidden:NO];
            [lblBlockRemark setHidden:YES];
            [lblBlockDuration setHidden:YES];
            [lblAddReason setText:reason];
            [lblBlockReason setText:remark];
            [lblBlockRemark setText:blockingDuration];
        }
        else{
            [lblAddReason setHidden:YES];
            [lblBlockReason setHidden:YES];
            [lblBlockRemark setHidden:YES];
            [lblBlockDuration setHidden:YES];
        }
        [lblGuestName setHidden:YES];
    }
    else if (curRoomBlocking.roomblocking_is_blocked > 0 && (isDueRoom || guestName.length > 0)){
        
        if(curRoomBlocking.roomblocking_is_blocked == RoomBlockingStatus_Release) {
            [blockRoomStatus setImage:[UIImage imageNamed:imgReleaseRoom]];
        } else {
            [blockRoomStatus setImage:[UIImage imageNamed:imgBlockRoom]];
        }
        
        [blockRoomStatus setHidden:NO];
        
        if(isHaveReasonRemark){
            NSString *reason = [NSString stringWithFormat:@"%@: %@", [L_reason currentKeyToLanguage], curRoomBlocking.roomblocking_reason];
            NSString *remark = [NSString stringWithFormat:@"%@: %@",[L_remark_title currentKeyToLanguage], curRoomBlocking.roomblocking_remark_physical_check];
            NSString *blockingDuration =  [NSString stringWithFormat:@"%@: %@",[L_oos_duration currentKeyToLanguage], curRoomBlocking.roomblocking_oosdurations];
            
            [lblAddReason setHidden:YES];
            [lblBlockReason setHidden:NO];
            [lblBlockRemark setHidden:NO];
            [lblBlockDuration setHidden:NO];
            [lblBlockReason setText:reason];
            [lblBlockRemark setText:remark];
            [lblBlockDuration setText:blockingDuration];
        }
        else{
            [lblAddReason setHidden:YES];
            [lblBlockReason setHidden:YES];
            [lblBlockRemark setHidden:YES];
            [lblBlockDuration setHidden:YES];
        }
        [lblGuestName setHidden:NO];
    }
    else {
        [blockRoomStatus setHidden:YES];
        [lblBlockRemark setHidden:YES];
        [lblBlockReason setHidden:YES];
        [lblAddReason setHidden:YES];
        [lblGuestName setHidden:NO];
        [lblBlockDuration setHidden:YES];
    }
    
    [imgFirstIconCleaningStatus setHidden:NO];
    [imgSecondIconCleaningStatus setHidden:NO];
    [lblServiceLaterTime setHidden:YES];
    [lblRAName setHidden:YES];
    
    if (isMockRoom == 1) {
        [imgFirstIconCleaningStatus setImage:[UIImage imageBeforeiOS7:imgComplete equaliOS7:imgCompleteFlat]];
        [imgSecondIconCleaningStatus setImage:[UIImage imageWithData:cleaningStatusIcon]];
    }
    else if (isReassignedRoom == 1){
        [imgFirstIconCleaningStatus setImage:[UIImage imageBeforeiOS7:imgPending equaliOS7:imgPendingFlat]];
        //            if (cleaningStatus == ENUM_CLEANING_STATUS_PENDING_SECOND_TIME) {
        //                [imgSecondIconCleaningStatus setImage:[UIImage imageNamed:@"icon_pending.png"]];
        //            }
        //            else {
        //                [imgSecondIconCleaningStatus setImage:[UIImage imageWithData:cleaningStatusIcon]];
        //            }
        [imgSecondIconCleaningStatus setImage:[UIImage imageWithData:cleaningStatusIcon]];
    }
    else {
        [imgSecondIconCleaningStatus setHidden:YES];
        [imgFirstIconCleaningStatus setImage:[UIImage imageWithData:cleaningStatusIcon]];
        if (cleaningStatus == ENUM_CLEANING_STATUS_SERVICE_LATER) {
            [lblServiceLaterTime setHidden:NO];
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            [dateFormat setLocale:[NSLocale currentLocale]];
            [dateFormat setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
            NSDate *assignedDate = [dateFormat dateFromString:roomAssignmentTime];
            [dateFormat setDateFormat:@"HH:mm"];
            [lblServiceLaterTime setText:[dateFormat stringFromDate:assignedDate]];
        }
        else {
            [lblServiceLaterTime setHidden:YES];
        }
    }
    
    [lblRoomType setText:roomTypeValue];
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
    
    return cell;
    
}


- (void)addAccessoryButton:(UITableViewCell *)cell  andIndexPath:(NSIndexPath *)indexPath{
    /*****************************************/
    UIButton *accessoryButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 24, 24)];
    [accessoryButton setBackgroundColor:[UIColor clearColor]];
    //    [accessoryButton setImage:[UIImage imageNamed:@"gray_arrow_36x36.png"] forState:UIControlStateNormal];
    [accessoryButton setImage:[UIImage imageNamed:@"gray_arrow_36x36.png"] forState:UIControlStateNormal];
    [accessoryButton setTag:indexPath.row];
    
    [accessoryButton addTarget:self action:@selector(btnDetailPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell setAccessoryView:accessoryButton];
    //cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
    /*****************************************/
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if([UserManagerV2 isSupervisor] && [listDisplayData count] > 0)
    {
        NSDictionary *roomAssignmentdata =[listDisplayData objectAtIndex:indexPath.row];
        
        NSString *roomNo = [roomAssignmentdata objectForKey:kRoomNo];
        [RoomManagerV2 setCurrentRoomNo:roomNo];
        [TasksManagerV2 setCurrentRoomAssignment:[[roomAssignmentdata objectForKey:kRoomAssignmentID] integerValue]];
        
        RoomAssignmentInfoViewController *viewController = [[RoomAssignmentInfoViewController alloc] initWithOption:IS_PUSHED_FROM_FIND_BY_ROOM andRoomAssignmentData:roomAssignmentdata];
        viewController.isLoadFromFind = YES;
        [self.navigationController pushViewController:viewController animated:YES];
        
        [self.navigationItem.backBarButtonItem setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK]];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationSelectedFind] object:nil];
        
        self.isLoadFromWS = FALSE;
        [[HomeViewV2 shareHomeView] setSelectedButton:tagOfFindButton];
    }
    
}

-(int)getRoomIndicatorGuestByArrivalDate:(NSString*)arrivalDate departureDate:(NSString*)departureDate
{
    NSString *arrivalDateCompare = nil;
    NSString *departureDateCompare = nil;
    NSString *nowDate = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd"];
    
    if(arrivalDate.length > 0) {
        arrivalDateCompare = [ehkConvert DateToStringWithString:arrivalDate fromFormat:@"yyyy-MM-dd HH:mm:ss" toFormat:@"yyyy-MM-dd"];
        
    }
    
    if(departureDate.length > 0) {
        departureDateCompare = [ehkConvert DateToStringWithString:departureDate fromFormat:@"yyyy-MM-dd HH:mm:ss" toFormat:@"yyyy-MM-dd"];
    }
    
    //Checking Back To Back
    if(arrivalDateCompare.length > 0 && departureDateCompare.length > 0) {
        if([arrivalDateCompare caseInsensitiveCompare:nowDate] == NSOrderedSame && [departureDateCompare caseInsensitiveCompare:nowDate] == NSOrderedSame){
            return RoomIndicatorGuestBackToBack;
        }
    }
    
    //Checking Due In Guest
    if(arrivalDateCompare.length > 0) {
        if([arrivalDateCompare caseInsensitiveCompare:nowDate] == NSOrderedSame) {
            return RoomIndicatorGuestDueIn;
        }
    }
    
    //Checking Due Out Guest
    if(departureDateCompare.length > 0){
        if([departureDateCompare caseInsensitiveCompare:nowDate] == NSOrderedSame) {
            return RoomIndicatorGuestDueOut;
        }
    }
    
    return 0;
}

#pragma mark - UISearchBar Delegate Methods
-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBarA {
    [searchBarA setShowsCancelButton:YES animated:NO];
    for(UIView *subView in searchBarA.subviews){
        if([subView isKindOfClass:UIButton.class])
        {
            UIButton *cancelButton = (UIButton*)subView;
            [cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            UIColor *tintColor = COLOR_CANCEL_SEARCH_BAR;
            [cancelButton setTintColor:tintColor];
            [cancelButton setTitle:[[LanguageManagerV2 sharedLanguageManager] getCancel] forState:UIControlStateNormal];
        }
    }
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBarA {
    [searchBarA setShowsCancelButton:NO animated:NO];
    
    [searchBarA resignFirstResponder];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBarA {
    [searchBarA setShowsCancelButton:NO animated:NO];
    [searchBarA resignFirstResponder];
    [searchBarA setText:@""];
    [self searchBar:searchBarA textDidChange:@""];
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    if ([searchText isEqualToString:@""] || (!searchText)) {
        [listDisplayData removeAllObjects];
        [listDisplayData addObjectsFromArray:listTempData];
        [tbvContent setHidden:NO];
    } else {
        [listDisplayData removeAllObjects];
        
        for (NSDictionary *rowData in listTempData) {
            NSString *roomNo = [rowData objectForKey:kRoomNo];
            NSString *roomStatus = [rowData objectForKey:kRMStatus];
            NSString *vip = [rowData objectForKey:kVIP];
            
            NSRange r = [roomNo rangeOfString:searchText
                                      options:NSCaseInsensitiveSearch];
            
            NSRange r1 = [roomStatus rangeOfString:searchText
                                           options:NSCaseInsensitiveSearch];
            
            NSRange r2 = [vip rangeOfString:searchText
                                    options:NSCaseInsensitiveSearch];
            
            if(r.location != NSNotFound || r1.location != NSNotFound
               || r2.location != NSNotFound) {
                [listDisplayData addObject:rowData];
                //                [tbvNoResult setHidden:YES];
                [tbvContent setHidden:NO];
            }
        }
    }
    
    if([listDisplayData count] <= 0) {
        [tbvContent setHidden:YES];
    }
    
    if([listDisplayData count]>1) {
        for(int i = 0; i < [listDisplayData count]; i++) {
            for(int j = i + 1; j <= [listDisplayData count] - 1; j++) {
                NSDictionary *row1 = [listDisplayData  objectAtIndex:i];
                NSString *roomNo1 = [row1 objectForKey:kRoomNo];
                
                NSDictionary *row2 = [listDisplayData  objectAtIndex:j];
                NSString *roomNo2 = [row2 objectForKey:kRoomNo];
                
                if([roomNo1 isEqualToString:roomNo2]) {
                    [listDisplayData removeObjectAtIndex:j];
                }
            }
        }
    }
    
    [tbvContent reloadData];
}
-(void) hiddenHUDAfterSaved:(MBProgressHUD *)HUD{
    [HUD hide:YES];
    [HUD removeFromSuperview];
    
}
-(int) getBuildingIdByUserId:(int) userId
{
    /*
    // get roomAssignment and get buildingId from roomassignment
    RoomAssignmentModelV2 *roomAssignment = [[RoomAssignmentModelV2 alloc] init];
    roomAssignment.roomAssignment_UserId = [UserManagerV2 sharedUserManager].currentUser.userId;
    
    NSMutableArray *roomAssignmentList = [[RoomManagerV2 sharedRoomManager] loadAllRoomAssignmentsByUserID:roomAssignment];
    if([roomAssignmentList count] > 0){
        roomAssignment = (RoomAssignmentModelV2*)[roomAssignmentList objectAtIndex:0];
    }
    
    RoomModelV2 *roomModel = [[RoomModelV2 alloc] init];
    roomModel.room_Id = roomAssignment.roomAssignment_RoomId;
    [[RoomManagerV2 sharedRoomManager] loadRoomModel:roomModel];
    
    //Hao Tran - Hard code building id if BuildingId doesn't has data
    if(roomModel.room_Building_Id <= 0){
        return 1;
    }
    
    return roomModel.room_Building_Id;
     */
    
    NSInteger buildingId = [[NSUserDefaults standardUserDefaults] integerForKey:[NSString stringWithFormat:@"%@%d", CURRENT_BUILDING_ID, [UserManagerV2 sharedUserManager].currentUser.userId]];
    if(buildingId <= 0) {
        return 1;
    }
    
    return (int)buildingId;
    
}
#pragma mark - Load Data
- (void)getListZone{
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getPleasewaitdot]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES];
    [[HomeViewV2 shareHomeView] waitingLoadingData];
    [self.navigationController.view setUserInteractionEnabled:NO];
//    NSString *zoneLastModified = [[UnassignManagerV2 sharedUnassignManager] getZoneLastModifiedDate];
    
    zoneListData = [[NSMutableArray alloc] initWithArray:[[UnassignManagerV2 sharedUnassignManager] getZoneRoomListWithUser:[[UserManagerV2 sharedUserManager]currentUser] andlastModified:nil]];
    if (zoneListData.count == 0) {
        zoneListData = [[UnassignManagerV2 sharedUnassignManager] getAllZone];
    }
    
    if (zoneListData == nil || zoneListData.count == 0) {
        [HUD hide:YES];
        return;
    }
    zoneListFilter = [NSMutableArray new];
    for (ZoneModelV2 *obj in zoneListData) {
        [zoneListFilter addObject:obj.zone_name];
    }
    currentObject = zoneListData[0];
    [self loadListDetail:currentObject withHUD:HUD];
    [self.navigationController.view setUserInteractionEnabled:YES];
    [self performSelector:@selector(hiddenHUDAfterSaved:) withObject:HUD afterDelay:0.5];
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
    
}
- (void)loadListDetail:(ZoneModelV2*)firstObject withHUD:(MBProgressHUD*)HUD{
    [btnFilter setTitle:firstObject.zone_name forState:UIControlStateNormal];
    int buildingId = [self getBuildingIdByUserId:[[[UserManagerV2 sharedUserManager]currentUser]userId]];
    [[RoomManagerV2 sharedRoomManager] GetZoneFindRoomDetailsListWSByUserID:[[[UserManagerV2 sharedUserManager]currentUser]userId]
                                                                 AndFloorId:0
                                                              AndBuildingId:buildingId
                                                                 AndHotelId:[[[UserManagerV2 sharedUserManager]currentUser]userHotelsId]
                                                              AndFilterType:9
                                                       AndGeneralFilterType:[NSString stringWithFormat:@"%d", (int)firstObject.zone_id]
                                                             AndPercentView:HUD];
    self.listTempData = [[RoomManagerV2 sharedRoomManager] getZoneRoomDetailsListByFloorId:0 userId:[[[UserManagerV2 sharedUserManager] currentUser] userId] filterType:9 generalFilter:[NSString stringWithFormat:@"%d", (int)firstObject.zone_id]];
    listDisplayData = self.listTempData;
    NSLog(@"%@", self.listTempData);
    [tbvContent reloadData];
}
- (IBAction)didClickFilter:(UIButton *)sender {
    UIButton *button = (UIButton *) sender;
    if (!picker) {
        picker = [[PickerViewV2 alloc] initPickerViewV2WithDatas:zoneListFilter AndSelectedData:button.titleLabel.text AndIndex:nil];
        picker.delegate = self;
    }
    [picker showPickerViewV2];
    
}
-(void)didChooseFindRoomPickerViewV2WithData:(NSString *)data AndIndex:(NSIndexPath *)index WithtIndex:(NSInteger)tIndex{
    [picker hidePickerViewV2];
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getLoadingData]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES];
    [[HomeViewV2 shareHomeView] waitingLoadingData];
    currentObject = zoneListData[tIndex];
    [self loadListDetail:currentObject withHUD:HUD];
    [self.navigationController.view setUserInteractionEnabled:YES];
    [self performSelector:@selector(hiddenHUDAfterSaved:) withObject:HUD afterDelay:0.5];
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
}
-(void)loadData {
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getLoadingData]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES];
    [[HomeViewV2 shareHomeView] waitingLoadingData];
    
    [self.navigationController.view setUserInteractionEnabled:NO];
    
    //if(isLoadFromWS) {
    //        if(statusId == -2 && filterId == roomStatusType)
    //        {
    //            //Inpected case
    //            self.listTempData = [[RoomManagerV2 sharedRoomManager] getAllRoomByFloorId:floorId AndUserId:[[[UserManagerV2 sharedUserManager] currentUser] userId]];
    //        }
    //        else
    //        {
    //            switch (filterId) {
    //                case roomStatusType:
    //                {
    //                    self.listTempData = [[RoomManagerV2 sharedRoomManager] getAllRoomByFloorId:floorId AndUserId:[[[UserManagerV2 sharedUserManager] currentUser] userId] AndRoomStatus:statusId];
    //                    break;
    //                }
    //
    //                case cleaningStatusType:
    //                {
    //                    self.listTempData = [[RoomManagerV2 sharedRoomManager] getAllRoomByFloorId:floorId AndUserId:[[[UserManagerV2 sharedUserManager] currentUser] userId] AndCleaningStatus:statusId];
    //                    break;
    //                }
    //                default:
    //                    break;
    //            }
    //        }
    //
    ////            [[RoomManagerV2 sharedRoomManager] GetFindRoomAssignmentListWSByUserID:[[[UserManagerV2 sharedUserManager]currentUser]userId] AndRoomstatus:roomStatusID AndFilterType:filterType AndLastModified:nil];
    ////            self.listTempData = [NSMutableArray arrayWithArray:[[RoomManagerV2 sharedRoomManager] getAllRoomInspectionWithSupervisorId:[[[UserManagerV2 sharedUserManager] currentUser] userId] andRoomStatusID:roomStatusID]];
    //
    //
    //
    //
    //}
    
    int buildingId = [self getBuildingIdByUserId:[[[UserManagerV2 sharedUserManager]currentUser]userId]];
    
    //Load WS data and save in database local
    if(statusId == inspectedCase && filterId == roomStatusType)
    {
        [[RoomManagerV2 sharedRoomManager] GetFindRoomDetailsListWSByUserID:[[[UserManagerV2 sharedUserManager]currentUser]userId] AndFloorId:floorId AndBuildingId:buildingId AndHotelId:[[[UserManagerV2 sharedUserManager]currentUser]userHotelsId] AndFilterType:filterId AndGeneralFilterType:[NSString stringWithFormat:@"%d", ENUM_ROOM_STATUS_VI] AndPercentView:HUD];//VI
        
        [[RoomManagerV2 sharedRoomManager] GetFindRoomDetailsListWSByUserID:[[[UserManagerV2 sharedUserManager]currentUser]userId] AndFloorId:floorId AndBuildingId:buildingId AndHotelId:[[[UserManagerV2 sharedUserManager]currentUser]userHotelsId] AndFilterType:filterId AndGeneralFilterType:[NSString stringWithFormat:@"%d", ENUM_ROOM_STATUS_OI] AndPercentView:HUD];//OI
    }
    else
    {
        [[RoomManagerV2 sharedRoomManager] GetFindRoomDetailsListWSByUserID:[[[UserManagerV2 sharedUserManager]currentUser]userId] AndFloorId:floorId AndBuildingId:buildingId AndHotelId:[[[UserManagerV2 sharedUserManager]currentUser]userHotelsId] AndFilterType:filterId AndGeneralFilterType:generalFilterType AndPercentView:HUD];
    }
    
    //Load database local
    [self loadLocalData];
    
    [self.navigationController.view setUserInteractionEnabled:YES];
    [self performSelector:@selector(hiddenHUDAfterSaved:) withObject:HUD afterDelay:0.5];
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
}

-(void)loadLocalData
{
    if(statusId == inspectedCase && filterId == roomStatusType)
    {
        //Inpected case
        self.listTempData = [[RoomManagerV2 sharedRoomManager] getAllRoomByFloorId:floorId AndUserId:[[[UserManagerV2 sharedUserManager] currentUser] userId]];
    }
    else
    {
        switch (filterId) {
            case roomStatusType:
            {
                //self.listTempData = [[RoomManagerV2 sharedRoomManager] getAllRoomByFloorId:floorId AndUserId:[[[UserManagerV2 sharedUserManager] currentUser] userId] AndRoomStatus:statusId];
                self.listTempData = [[RoomManagerV2 sharedRoomManager] getRoomDetailsListByFloorId:floorId userId:[[[UserManagerV2 sharedUserManager] currentUser] userId] filterType:filterId generalFilter:generalFilterType];
            }
                break;
            case cleaningStatusType:
            {
                self.listTempData = [[RoomManagerV2 sharedRoomManager] getAllRoomByFloorId:floorId AndUserId:[[[UserManagerV2 sharedUserManager] currentUser] userId] AndCleaningStatus:statusId];
                break;
            }
                
            case kindOfRoomType:
            {
                self.listTempData = [[RoomManagerV2 sharedRoomManager] getAllRoomByFloorId:floorId AndUserId:[[[UserManagerV2 sharedUserManager] currentUser] userId] AndKindOfRoom:statusId];
                break;
            }
                
            case unassignType:
            {
                self.listTempData = [[RoomManagerV2 sharedRoomManager]getUnassginRoomsByFloorId:floorId AndUserId:[[[UserManagerV2 sharedUserManager] currentUser] userId]];
                break;
            }
                
            case roomOccupiedType:
            {
                self.listTempData = [[RoomManagerV2 sharedRoomManager] getAllRoomByFloorId:floorId userId:[[[UserManagerV2 sharedUserManager] currentUser] userId] filterType:filterId statusId:statusId];
                break;
            }
            case findRoomNo:
            {
                self.listTempData = [[RoomManagerV2 sharedRoomManager] getAllRoomByFloorId:floorId userId:[[[UserManagerV2 sharedUserManager] currentUser] userId] filterType:filterId generalFilter:generalFilterType];
                break;
            }
            case allRoomType:
            {
                self.listTempData = [[RoomManagerV2 sharedRoomManager] getAllOfRoomByFloorId:floorId AndUserId:[[[UserManagerV2 sharedUserManager] currentUser] userId]];
                break;
            }
            default:
                break;
        }
    }
    
    NSMutableArray *listRA = [[FindManagerV2 sharedFindManagerV2] findDataAttendantWithUserID:[NSNumber numberWithInt:[[[UserManagerV2 sharedUserManager] currentUser] userId]] AndHotelID:[NSNumber numberWithInt:[[[UserManagerV2 sharedUserManager] currentUser] userHotelsId]] AndAttendantName:nil];
    if(listRA.count != 0)
    {
        for(NSDictionary *roomAssigment in listTempData)
        {
            for(NSDictionary *RA in listRA)
            {
                if(![[roomAssigment valueForKey:kraHousekeeperID] isEqualToString: [RA valueForKey:kAttendantID]])
                {
                    int userID = [[[UserManagerV2 sharedUserManager] currentUser] userId];
                    NSString *houseKeeperId = [roomAssigment valueForKey:kraHousekeeperID];
                    [[RoomManagerV2 sharedRoomManager] updateIsCheckedRa:userID houseKeeperId:houseKeeperId];
                    break;
                }
                break;
            }
        }
    }
    
    self.listDisplayData = [NSMutableArray arrayWithArray:listTempData];
    
    if([listDisplayData count] > 1) {
        for(int i = 0; i < [listDisplayData count]; i++) {
            for(int j = i + 1; j <[listDisplayData count]; j++) {
                NSDictionary *row1 = [listDisplayData  objectAtIndex:i];
                NSString *roomNo1 = [row1 objectForKey:kRoomNo];
                
                NSDictionary *row2 = [listDisplayData  objectAtIndex:j];
                NSString *roomNo2 = [row2 objectForKey:kRoomNo];
                
                if([roomNo1 isEqualToString:roomNo2]) {
                    [listDisplayData removeObjectAtIndex:j];
                }
            }
        }
    }
    
    if([listDisplayData count] <= 0) {
        [tbvContent setHidden:YES];
    } else {
        [tbvContent setHidden: NO];
        [tbvContent reloadData];
    }
}


#pragma mark  - Set Captions View
-(void)setCaptionsView {
    [self setTitle:[NSString stringWithFormat:@"%@ - %@ %@", [[LanguageManagerV2 sharedLanguageManager] getFind], [[LanguageManagerV2 sharedLanguageManager] getBy], [[LanguageManagerV2 sharedLanguageManager] getRoomTitle]]];
    
    [lblRoomNo setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_rm]];
    [lblRoomNo setFont:[UIFont fontWithName:font size:sizeNormal]];
    
    [lblRoomStatus setText:[[LanguageManagerV2 sharedLanguageManager] getRMStatus]];
    [lblRoomStatus setFont:[UIFont fontWithName:font size:sizeNormal]];
    
    [lblVIP setText:[[LanguageManagerV2 sharedLanguageManager] getVIP]];
    [lblVIP setFont:[UIFont fontWithName:font size:sizeNormal]];
    
    [lblCleaningStatus setText:[[LanguageManagerV2 sharedLanguageManager] getCleaningStatus]];
    [lblCleaningStatus setFont:[UIFont fontWithName:font size:sizeNormal]];
    
    [searchBar setPlaceholder:[[LanguageManagerV2 sharedLanguageManager] getSearch]];
    [searchBar setTintColor:[UIColor colorWithRed:colorBlueR green:colorBlueG
                                             blue:colorBlueB alpha:colorAlpha]];
    [searchBar setValue:[L_CANCEL currentKeyToLanguage] forKey:@"_cancelButtonText"];
}

#pragma mark - === Handle Topbar Methods ===
#pragma mark
-(void)loadTopbarView {
    TopbarViewV2 *topbar = [[TopbarViewV2 alloc] initViewWithSuperModel:[[CommonVariable sharedCommonVariable] superRoomModel]];
    [topbar setTag:tagTopbarView];
    [self.view addSubview:topbar];
    [topbar setHidden:YES];
    
    [topbar setAlpha:0.0];
    [self addButtonHandleShowHideTopbar];
}

-(void)showTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationWillStartSelector:@selector(setHiddenTopbarView)];
    
    f = topbar.frame;
    f.origin.y = 0;
    [topbar setFrame:f];
    [topbar setAlpha:1.0];
    
    [self adjustShowForViews];
    
    [UIView commitAnimations];
}

-(void)hideTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDidStopSelector:@selector(setHiddenTopbarView)];
    [UIView setAnimationDelegate:self];
    
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    [topbar setAlpha:0.0];
    
    [self adjustRemoveForViews];
    
    [UIView commitAnimations];
}

-(void)setHiddenTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    [topbar setHidden:!topbar.hidden];
}

-(TopbarViewV2 *)getTopBarView {
    return (TopbarViewV2 *)[self.view viewWithTag:tagTopbarView];
}

-(void)addButtonHandleShowHideTopbar {
    UIView *vBarButton = [[UIView alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
    UIButton *titleBarButtonFirst = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 200, 30)];
    
    titleBarButtonFirst.titleLabel.backgroundColor = [UIColor clearColor];
    [titleBarButtonFirst.titleLabel setFont: FONT_BUTTON_TOPBAR];
    titleBarButtonFirst.titleLabel.textAlignment = NSTextAlignmentCenter;
    titleBarButtonFirst.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
    
    [titleBarButtonFirst.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
    [titleBarButtonFirst.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
    [titleBarButtonFirst addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    [titleBarButtonFirst setTitle:[[LanguageManagerV2 sharedLanguageManager] getFind] forState:UIControlStateNormal];
    
    [vBarButton addSubview:titleBarButtonFirst];
    
    
    UIButton *titleBarButtonSecond = [[UIButton alloc] initWithFrame:CGRectMake(0, 22, 200, 22)];
    [titleBarButtonSecond setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [titleBarButtonSecond.titleLabel setFont:FONT_BUTTON_TOPBAR_SECOND];
    [titleBarButtonSecond setTitle:[[LanguageManagerV2 sharedLanguageManager] getRoomTitle] forState:UIControlStateNormal];
    [titleBarButtonSecond addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    [vBarButton addSubview:titleBarButtonSecond];
    
    self.navigationItem.titleView = vBarButton;
    //Move title to center vertical
    CGRect titleViewBounds = self.navigationItem.titleView.bounds;
    int moveLeftTitle = 0;
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        moveLeftTitle = 10;
    }
    self.navigationItem.titleView.bounds = CGRectMake(titleViewBounds.origin.x + moveLeftTitle, Y_TOPBAR_TITLE, titleViewBounds.size.width, titleViewBounds.size.height);
}

-(void)topbarTapped:(UIButton *)sender {
    TopbarViewV2 *topbar = [self getTopBarView];
    [sender setEnabled:NO];
    if (topbar.isHidden) {
        [self showTopbarView];
    } else {
        [self hideTopbarView];
    }
    [self performSelector:@selector(topbarButtonEnable:) withObject:sender afterDelay:0.3];
}

-(void) topbarButtonEnable:(UIButton *) topbarBtn {
    [topbarBtn setEnabled:YES];
}

-(void)adjustShowForViews {
    //adjust all views when show
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    CGRect ftbv = controlsView.frame;
    [controlsView setFrame:CGRectMake(0, f.size.height, 320, ftbv.size.height)];
    
    ftbv = tbvContent.frame;
    [tbvContent setFrame:CGRectMake(0, f.size.height + controlsView.frame.size.height, 320, ftbv.size.height - f.size.height)];
    
}

-(void)adjustRemoveForViews {
    //adjust all views when hide
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    [controlsView setFrame:CGRectMake(0, 0, 320, controlsView.frame.size.height)];
    
    CGRect ftbv = tbvContent.frame;
    [tbvContent setFrame:CGRectMake(0, controlsView.frame.size.height, 320, ftbv.size.height + f.size.height)];
    
}

#pragma mark - === Handle Back Button ===
#pragma mark

-(void)backBarPressed {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)addBackButton {
    //back button navigation
    self.navigationItem.leftBarButtonItem = [MyNavigationBarV2 createBackButtonWithTarget:self selector:@selector(backBarPressed) forControlEvents:UIControlEventTouchUpInside];
}


@end
