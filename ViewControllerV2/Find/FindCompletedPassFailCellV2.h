//
//  FindCompletedPassFailCellV2.h
//  mHouseKeeping
//
//  Created by Nghia Truong on 2/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FindCompletedPassFailCellV2 : UITableViewCell {
    UILabel *lblRoomNo;
    UILabel *lblGuestName;
    UILabel *lblRoomStatus;
    UILabel *lblVIP;
    UILabel *lblDuration;
    UIImageView *imgRoomStatus;
    UIButton *btnDetailArrow;
    
}

@property (nonatomic, strong) IBOutlet UILabel *lblRoomNo;
@property (nonatomic, strong) IBOutlet UILabel *lblGuestName;
@property (nonatomic, strong) IBOutlet UILabel *lblRoomStatus;
@property (nonatomic, strong) IBOutlet UILabel *lblVIP;
@property (nonatomic, strong) IBOutlet UILabel *lblDuration;
@property (nonatomic, strong) IBOutlet UIImageView *imgRoomStatus;
@property (nonatomic, strong) IBOutlet UIButton *btnDetailArrow;


@end
