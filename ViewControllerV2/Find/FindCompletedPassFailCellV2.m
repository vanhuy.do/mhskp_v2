//
//  FindCompletedPassFailCellV2.m
//  mHouseKeeping
//
//  Created by Nghia Truong on 2/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FindCompletedPassFailCellV2.h"

@implementation FindCompletedPassFailCellV2

@synthesize lblRoomNo, lblGuestName, lblVIP, lblDuration, lblRoomStatus, imgRoomStatus, btnDetailArrow;

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:
(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
    }
    
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)dealloc {
//    [lblVIP release];
//    [lblRoomStatus release];
//    [lblRoomNo release];
//    [lblGuestName release];
//    [lblDuration release];
//    [imgRoomStatus release];
//    [btnDetailArrow release];
//    
//    [super dealloc];
}
@end
