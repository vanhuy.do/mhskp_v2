//
//  FindCompletedPassFailViewV2.h
//  mHouseKeeping
//
//  Created by Nghia Truong on 2/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    PASS_ROOM_ASSIGNMENT_STATUS = 1,
    FAIL_ROOM_ASSIGNMENT_STATUS = 2,
    COMPLETED_ROOM_ASSIGNMENT_STATUS = 3,
} PASS_FAIL_ROOM_ASSIGNMENT_STATUS;

@interface FindCompletedPassFailViewV2 : UIViewController <UITableViewDataSource, UITableViewDelegate> {
    UILabel *lblNameAndType;
    UILabel *lblRoomNo;
    UILabel *lblRoomStatus;
    UILabel *lblVIP;
    UILabel *lblDuration;
    UITableView *tbvContent;    
    UITableView *tbvNoResult;
        
    NSMutableArray *listDisplayData;
    NSMutableArray *listTempData;
    NSString *attendantName;
    NSString *typeView;
    
    UIButton *btnBack;
    
    BOOL isPassRooms;
    UILabel *lblNumOfRoom;
    PASS_FAIL_ROOM_ASSIGNMENT_STATUS typeRoomAssignmentStatus;
    IBOutlet UIImageView *imgHeader;
}

@property (nonatomic, strong) IBOutlet UILabel *lblNameAndType;
@property (nonatomic, strong) IBOutlet UILabel *lblRoomNo;
@property (nonatomic, strong) IBOutlet UILabel *lblRoomStatus;
@property (nonatomic, strong) IBOutlet UILabel *lblVIP;
@property (nonatomic, strong) IBOutlet UILabel *lblDuration;
@property (nonatomic, strong) IBOutlet UITableView *tbvContent;
@property (nonatomic, strong) IBOutlet UITableView *tbvNoResult;
@property (nonatomic, strong) NSMutableArray *listDisplayData;
@property (nonatomic, strong) NSMutableArray *listTempData;
@property (nonatomic, strong) NSString *attendantName;
@property (nonatomic, strong) NSString *typeView;
@property (strong, nonatomic) IBOutlet UIView *controlsView;
@property (nonatomic, strong) IBOutlet UILabel *lblNumOfRoom;
@property (nonatomic, assign) int attendantId;

-(void) loadData;
-(void) setCaptionsView;
-(void) setRoomAssignmentsData:(NSMutableArray *) roomAssignments WithType:(PASS_FAIL_ROOM_ASSIGNMENT_STATUS) type;

@end
