//
//  FindCompletedPassFailViewV2.m
//  mHouseKeeping
//
//  Created by Nghia Truong on 2/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FindCompletedPassFailViewV2.h"
#import "FindCompletedPassFailCellV2.h"
#import "FindPendingCellV2.h"
#import "FindManagerV2.h"
#import "RoomAssignmentInfoViewController.h"
//#import "HomeViewV2.h"
#import "MyNavigationBarV2.h"
#import "LogFileManager.h"
#import "DeviceManager.h"
#import "ehkDefinesV2.h"
#import "UIImage+Tint.h"

#define kRoomNo             @"RoomNo"
#define kGuestName          @"GuestName"
#define kRMStatus           @"RMStatus"
#define kCleaningStatus     @"CleaningStatus"
#define kVIP                @"VIP"
#define kRoomAssignmentID   @"RoomAssignmentID"
#define kDurationUsed        @"DurationUsed"

#define markImage           @"!.png"
#define backgroundImage     @"bg.png"

#define heightTbvContent    55.0f
#define heightTbvNoResult   280.0f

#define colorWhite          255/255.0
#define colorAlpha          1.0
#define colorBlueR          105/255.0
#define colorBlueG          218/255.0
#define colorBlueB          222/255.0
#define colorGreenR         77/255.0
#define colorGreenG         211/255.0
#define colorGreenB         100/255.0
#define colorRedR           223/255.0
#define colorRedG           81/255.0
#define colorRedB           59/255.0

#define tagNoRoom           12

#define font                @"Arial-BoldMT"

#define sizeNoRoom          22.0f
#define sizeSmall           10.0f
#define sizeNormal          17.0f

#define nameVC              @"VC"
#define nameOC              @"OC"
#define nameVD              @"VD"
#define nameOD              @"OD"
#define nameVI              @"VI"
#define nameOI              @"OI"

@interface FindCompletedPassFailViewV2 (PrivateMethods)

-(void) btnDetailArrowPressed:(UIButton *) sender;

-(void) loadTopbarView;
-(void) showTopbarView;
-(void) hideTopbarView;
-(void) setHiddenTopbarView;
-(void) adjustShowForViews;
-(void) adjustRemoveForViews;
-(void) addButtonHandleShowHideTopbar;
-(void) topbarTapped:(UIButton *) sender;
-(TopbarViewV2 *) getTopBarView;
-(void) btnBackPressed;
-(void) addBackButton;

@end

@implementation FindCompletedPassFailViewV2
@synthesize controlsView;

@synthesize lblVIP, lblRoomNo, lblRoomStatus, lblDuration, lblNameAndType, tbvContent, tbvNoResult, listDisplayData, listTempData, attendantName, typeView, lblNumOfRoom;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - View lifecycle
-(void)viewWillAppear:(BOOL)animated {
    [self setCaptionsView];
    [super viewWillAppear:animated];
    [[HomeViewV2 shareHomeView] setSelectedButton:tagOfFindButton];
}

-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [tbvContent setBackgroundView:nil];
    [tbvContent setBackgroundColor:[UIColor clearColor]];
    [tbvContent setOpaque:YES];
    [tbvContent setHidden:([listDisplayData count] == 0)];
    
    [tbvNoResult setBackgroundView:nil];
    [tbvNoResult setBackgroundColor:[UIColor clearColor]];
    [tbvNoResult setOpaque:YES];
    [tbvNoResult setHidden:([listDisplayData count] != 0)];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Resize content views of current VC to fit with Navigation bar and tabbar
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        //Clear redundant view for header, footter
        CGRect frameHeaderFooter = tbvContent.tableHeaderView.frame;
        frameHeaderFooter.size.height = 0.1f;
        UIView *headerView = [[UIView alloc] initWithFrame:frameHeaderFooter];
        [tbvContent setTableHeaderView:headerView];
        
        frameHeaderFooter = tbvContent.tableFooterView.frame;
        frameHeaderFooter.size.height = 0.1f;
        UIView *footerView = [[UIView alloc] initWithFrame:frameHeaderFooter];
        [tbvContent setTableFooterView:footerView];
        
        [self loadFlatResource];
    }
    
    [self performSelector:@selector(loadTopbarView)];
    
    [self addBackButton];
}

- (void)viewDidUnload {
    [self setLblNameAndType:nil];
    [self setLblVIP:nil];
    [self setLblRoomNo:nil];
    [self setLblDuration:nil];
    [self setLblRoomStatus:nil];
    [self setTbvContent:nil];
    [self setTbvNoResult:nil];
    [self setAttendantName:nil];
    [self setTypeView:nil];
    [self setListDisplayData:nil];
    [self setListTempData:nil];
    
    [self setControlsView:nil];
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc {
}

#pragma mark - UITableView Delegate Methods
-(CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(tableView == tbvContent)
    {
        NSDictionary *rowData = [(NSDictionary*)[self.listDisplayData objectAtIndex:indexPath.row] objectForKey:keyRoomData];
        NSString *guestName = [rowData objectForKey:kGuestName];
        if(![guestName isEqualToString: @" "])
        {
            return 63.0f;
        }
        return heightTbvContent;
    }
    
    else if(tableView == tbvNoResult)
        return heightTbvNoResult;
    
    return 0;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(tableView == tbvContent)
        return [listDisplayData count];
    
    return 1;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    cell.backgroundColor = [UIColor colorWithRed:colorWhite green:colorWhite
                                            blue:colorWhite alpha:colorAlpha];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentify = @"cellIdentify";
    static NSString *cellNoIndentify = @"cellNoIndentify";
    
    UITableViewCell *cell = nil;
    
    if(tableView == tbvNoResult) {
        cell = [tableView dequeueReusableCellWithIdentifier:cellNoIndentify];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellNoIndentify];//autorelease
            
            UILabel *titleNoResult = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, heightTbvNoResult)];
            [titleNoResult setTextColor:[UIColor grayColor]];
            [titleNoResult setBackgroundColor:[UIColor clearColor]];
            [titleNoResult setTextAlignment:NSTextAlignmentCenter];
            [titleNoResult setTag:tagNoRoom];
            [titleNoResult setFont:[UIFont fontWithName:font size:sizeNoRoom]];
            
            [cell.contentView addSubview:titleNoResult];
        }
        
        UILabel *lblNoResult = (UILabel *)[cell viewWithTag:tagNoRoom];
        [lblNoResult setText:[[LanguageManagerV2 sharedLanguageManager] getNoResultsFound]];
        
        return cell;
    }
    
    if(tableView == tbvContent) {
        FindPendingCellV2 *cell = nil;
        cell = (FindPendingCellV2 *)[tableView dequeueReusableCellWithIdentifier:
                                     cellIdentify];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        if (cell == nil) {
            NSArray *arrayCell = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([FindPendingCellV2 class]) owner:self options:nil];
            cell = [arrayCell objectAtIndex:0];
            [cell.btnDetailArrow addTarget:self action:@selector(btnDetailArrowPressed:) forControlEvents:UIControlEventTouchUpInside];
            [cell.btnDetailArrow setTag:indexPath.row];
        }
        
        //set accessory view = btnDetailArrow
        [cell setAccessoryView:cell.btnDetailArrow];
        
        NSDictionary *rowData = [(NSDictionary*)[self.listDisplayData objectAtIndex:indexPath.row] objectForKey:keyRoomData];
        
        UILabel *lblRoomNoCell = (UILabel *)[cell viewWithTag:tagRoomNo];
        UILabel *lblRoomStatusCell = (UILabel *)[cell viewWithTag:tagRMStatus];
        UILabel *lblVipRoom = (UILabel *)[cell viewWithTag:tagVIP];
        UIImageView *imgFirstIconCleaningStatus = (UIImageView *)[cell viewWithTag:tagFirstIconCleaningStatus];
        UIImageView *imgSecondIconCleaningStatus = (UIImageView *)[cell viewWithTag:tagSecondIconCleaningStatus];
        UILabel *lblServiceLaterTime = (UILabel *)[cell viewWithTag:tagTimeServiceLater];
        UIImageView *imgRushOrQueueRoom = (UIImageView *)[cell viewWithTag:tagRushOrQueueRoom];
        UILabel *lblDueOutRoom = (UILabel *)[cell viewWithTag:tagDueOutRoom];
        UILabel *lblArrivalTime = (UILabel *)[cell viewWithTag:tagArrivalTime];
        UILabel *lblGuestName = (UILabel *)[cell viewWithTag:tagGuestName];
        UILabel *lblRAName = (UILabel *)[cell viewWithTag:tagRAName];
        UILabel *lblRoomType = (UILabel *)[cell viewWithTag:tagRoomType];
        UIImageView *imgProfileNotes = (UIImageView *)[cell viewWithTag:tagProfileNotes]; // CFG [20160928/CRF-00000827]
        UIImageView *imgStayOver = (UIImageView *)[cell viewWithTag:tagStayOver]; // CRF [20161106/CRF-00001293]
        NSString *roomTypeValue = (NSString *)[rowData objectForKey:kRoomType];
        NSString *roomNo = (NSString *)[rowData objectForKey:kRoomNo];
        NSString *guestName = [rowData objectForKey:kGuestName];
        NSString *vip = (NSString *)[rowData objectForKey:kVIP];
        NSInteger cleaningStatus = [(NSString *)[rowData objectForKey:kCleaningStatusID] integerValue];
        NSString *roomStatus = (NSString *)[rowData objectForKey:kRMStatus];
        //        NSString *raFirstName = (NSString *)[rowData objectForKey:kRAFirstName];
        //        NSString *raLastName = (NSString *)[rowData objectForKey:kRALastName];
        //        NSString *guestFirstName = (NSString *)[rowData objectForKey:kGuestFirstName];
        //        NSString *guestLastName = (NSString *)[rowData objectForKey:kGuestLastName];
        NSString *guestArrivalTime = (NSString *)[rowData objectForKey:kRaGuestArrivalTime];
        //        NSString *guestDepartureTime = (NSString *)[rowData objectForKey:kRaGuestDepartureTime];
        NSInteger kindOfRoom = [(NSString *)[rowData objectForKey:kRaKindOfRoom] integerValue];
        NSInteger isMockRoom = [(NSString *)[rowData objectForKey:kRaIsMockRoom] integerValue];
        NSInteger isReassignedRoom = [(NSString *)[rowData objectForKey:kRaIsReassignedRoom] integerValue];
        NSData *cleaningStatusIcon = (NSData *)[rowData objectForKey:kCleaningStatus];
        NSString *roomAssignmentTime = (NSString *)[rowData objectForKey:kRoomAssignmentTime];
        
        [lblRoomNoCell setText:roomNo];
        [lblRoomStatusCell setText:roomStatus];
        //[lblVipRoom setText:(vip == nil || [vip isEqualToString:@""])?@"-":vip];
        [lblVipRoom setText:(vip == nil || [vip isEqualToString:@""] || [vip isEqualToString:@"0"])?@"-":vip];
        [lblDueOutRoom setText:@"[D/O]"];
        [lblArrivalTime setText:guestArrivalTime];
        [lblGuestName setText:(guestName == nil || [guestName isEqualToString:@""])?@"-":guestName];
        
        // CFG [20160913/CRF-00001439] - Changed to binary AND operation.
        if ((kindOfRoom & ENUM_KIND_OF_ROOM_RUSH) == ENUM_KIND_OF_ROOM_RUSH) {
            [imgRushOrQueueRoom setHidden:NO];
            [imgRushOrQueueRoom setImage:[UIImage imageBeforeiOS7:imgRushRoom equaliOS7:imgRushRoomFlat]];
        }
        else if ((kindOfRoom & ENUM_KIND_OF_ROOM_QUEUE) == ENUM_KIND_OF_ROOM_QUEUE) {
            [imgRushOrQueueRoom setHidden:NO];
            [imgRushOrQueueRoom setImage:[UIImage imageBeforeiOS7:imgQueueRoom equaliOS7:imgQueueRoomFlat]];
        } else {
//            if([LogFileManager isLogConsole])
//            {
//                NSLog(@"OUT OF VALUE RANGE");
//            }
            [imgRushOrQueueRoom setHidden:YES];
        }
        // CFG [20160913/CRF-00001439] - End.
        
        // CFG [20160928/CRF-00000827] - Show notes icon.
        if ((kindOfRoom & ENUM_KIND_OF_ROOM_PROFILE_NOTES_EXISTS) == ENUM_KIND_OF_ROOM_PROFILE_NOTES_EXISTS) {
            [imgProfileNotes setHidden:NO];
        }
        else {
            [imgProfileNotes setHidden:YES];
        }
        // CFG [20160928/CRF-00000827] - End.
        
        // CRF [20161106/CRF-00001293] - Show notes icon.
        if ((kindOfRoom & ENUM_KIND_OF_ROOM_STAY_OVER_EXISTS) == ENUM_KIND_OF_ROOM_STAY_OVER_EXISTS)  {
            [imgStayOver setHidden:NO];
            if(imgProfileNotes.isHidden == NO) {
                [imgStayOver setFrame:CGRectMake(imgProfileNotes.frame.origin.x - imgProfileNotes.frame.size.width, imgProfileNotes.frame.origin.y, imgProfileNotes.frame.size.width, imgProfileNotes.frame.size.height)];
            } else {
                [imgStayOver setFrame:imgProfileNotes.frame];
            }
        }
        else {
            [imgStayOver setHidden:YES];
        }
        // CRF [20161106/CRF-00001293] - End.
        BOOL isDueOut = false; //departuretime < currenttime (HH:mm:ss)
        BOOL isGuestComing = false; // arrivalTime > departureTime
        
        if (isDueOut) {
            [cell setBackgroundColor:[UIColor colorWithRed:32/255.0f green:178/255.0f blue:170/255.0f alpha:1]];
            [lblDueOutRoom setHidden:NO];
            [lblDueOutRoom setFrame:lblDueOutFrame];
            if (isGuestComing) {
                [lblArrivalTime setHidden:NO];
                [lblArrivalTime setFrame:lblArrivalTimeFrameCase1];
                [lblGuestName setFrame:lblGuestNameFrameInCase1];
            }
            else {
                [lblArrivalTime setHidden:YES];
                [lblGuestName setFrame:lblGuestNameFrameInCase3];
            }
        }
        else {
            [lblDueOutRoom setHidden:YES];
            if (isGuestComing) {
                [lblArrivalTime setHidden:NO];
                [lblArrivalTime setFrame:lblArrivalTimeFrameCase2];
                [lblGuestName setFrame:lblGuestNameFrameInCase2];
            }
            else {
                [lblArrivalTime setHidden:YES];
                [lblGuestName setFrame:lblGuestNameFrameInCase4];
            }
        }
        [cell setAccessoryType:UITableViewCellAccessoryNone];
        [imgFirstIconCleaningStatus setHidden:NO];
        [imgSecondIconCleaningStatus setHidden:NO];
        [lblServiceLaterTime setHidden:YES];
        [lblRAName setHidden:YES];
        
        if (isMockRoom == 1) {
            [imgFirstIconCleaningStatus setImage:[UIImage imageBeforeiOS7:imgComplete equaliOS7:imgCompleteFlat]];
            [imgSecondIconCleaningStatus setImage:[UIImage imageWithData:cleaningStatusIcon]];
        }
        else if (isReassignedRoom == 1){
            [imgFirstIconCleaningStatus setImage:[UIImage imageBeforeiOS7:imgPending equaliOS7:imgPendingFlat]];
            //            if (cleaningStatus == ENUM_CLEANING_STATUS_PENDING_SECOND_TIME) {
            //                [imgSecondIconCleaningStatus setImage:[UIImage imageNamed:@"icon_pending.png"]];
            //            }
            //            else {
            //                [imgSecondIconCleaningStatus setImage:[UIImage imageWithData:cleaningStatusIcon]];
            //            }
            [imgSecondIconCleaningStatus setImage:[UIImage imageWithData:cleaningStatusIcon]];
        }
        else {
            [imgSecondIconCleaningStatus setHidden:YES];
            [imgFirstIconCleaningStatus setImage:[UIImage imageWithData:cleaningStatusIcon]];
            if (cleaningStatus == ENUM_CLEANING_STATUS_SERVICE_LATER) {
                [lblServiceLaterTime setHidden:NO];
                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                [dateFormat setLocale:[NSLocale currentLocale]];
                [dateFormat setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
                NSDate *assignedDate = [dateFormat dateFromString:roomAssignmentTime];
                [dateFormat setDateFormat:@"HH:mm"];
                [lblServiceLaterTime setText:[dateFormat stringFromDate:assignedDate]];
            }
            else {
                [lblServiceLaterTime setHidden:YES];
            }
        }
        
        [lblRoomType setText:roomTypeValue];
        
        RoomBlocking *curRoomBlocking = [[RoomManagerV2 sharedRoomManager] loadRoomBlockingByUserId:_attendantId roomNumber:roomNo];
        UIImageView *blockRoomStatus = (UIImageView*)[cell.contentView viewWithTag:tagBlockingImage];
        if(curRoomBlocking.roomblocking_is_blocked == RoomBlockingStatus_Release) {
            [blockRoomStatus setHidden:NO];
            [blockRoomStatus setImage:[UIImage imageNamed:imgReleaseRoom]];
        } else if (curRoomBlocking.roomblocking_is_blocked == RoomBlockingStatus_Blocked){
            [blockRoomStatus setHidden:NO];
            [blockRoomStatus setImage:[UIImage imageNamed:imgBlockRoom]];
        } else {
            [blockRoomStatus setHidden:YES];
        }
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
        
        return cell;
    }
    
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //    FindCompletedPassFailCellV2 *cell = (FindCompletedPassFailCellV2 *)[tableView cellForRowAtIndexPath:indexPath];
    //    [self btnDetailArrowPressed:cell.btnDetailArrow];
    if([listDisplayData count] > 0)
    {
        NSDictionary *data =[listDisplayData objectAtIndex:indexPath.row];
        NSDictionary *roomData = [data objectForKey:keyRoomData];
        RoomAssignmentModelV2 *roomAssignmentModel = [data objectForKey:keyRoomAssignmentModel];
        RoomModelV2 *roomModel = [data objectForKey:keyRoomModel];
        RoomRecordModelV2 *roomRecordModel = [data objectForKey:keyRoomRecordModel];
        
        [self insertRoomAssignmentModel:roomAssignmentModel];
        [self insertRoomModel:roomModel];
        [self insertRoomRecordModel:roomRecordModel];
        //[self insertGuestInfoFromRowData:roomData];
        
        [RoomManagerV2 setCurrentRoomNo:roomModel.room_Id];
        [TasksManagerV2 setCurrentRoomAssignment:[[roomData objectForKey:kRoomAssignmentID] integerValue]];
        
        //Hao Tran - remove for can't show room detail because WS GetPrevRoomAssignmentList return date time IN THE PAST
        //so can't get data from current date
        //NSDictionary *roomAssignmentData = [[RoomManagerV2 sharedRoomManager] getRoomAssignmentDataByRoomAssignmentId:roomAssignmentModel.roomAssignment_Id andRaUserId:[UserManagerV2 sharedUserManager].currentUser.userId];
        
        //Replace with this (no filter current date)
        NSDictionary *roomAssignmentData = [[RoomManagerV2 sharedRoomManager] getRoomAssignmentDataByRoomAssignmentId:roomAssignmentModel.roomAssignment_Id andRaUserId:[UserManagerV2 sharedUserManager].currentUser.userId isFilterCurrentDate:NO];
        
        RoomAssignmentInfoViewController *viewController = [[RoomAssignmentInfoViewController alloc] initWithOption:IS_PUSHED_FROM_FIND_BY_RA andRoomAssignmentData:roomAssignmentData];
        viewController.isLoadFromFind = YES;
        [self.navigationController pushViewController:viewController animated:YES];
        
        [self.navigationItem.backBarButtonItem setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK]];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationSelectedFind] object:nil];
    }
}

-(void) insertRoomAssignmentModel:(RoomAssignmentModelV2 *)roomAssignmentModel
{
    RoomAssignmentModelV2 *ramodel = [[RoomAssignmentModelV2 alloc] init];
    ramodel.roomAssignment_Id = roomAssignmentModel.roomAssignment_Id;
    ramodel.roomAssignment_UserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    [[RoomManagerV2 sharedRoomManager] load:ramodel];
    if (ramodel.roomAssignment_RoomId.length > 0) {
        [[RoomManagerV2 sharedRoomManager] update:roomAssignmentModel];
    }
    else {
        [[RoomManagerV2 sharedRoomManager] insert:roomAssignmentModel];
    }
}

-(void) insertRoomModel:(RoomModelV2 *)roomModel
{
    RoomModelV2 *room = [[RoomModelV2 alloc] init];
    room.room_Id = roomModel.room_Id;
    [[RoomManagerV2 sharedRoomManager] loadRoomModel:room];
    if (room.room_Id.length > 0) {
        [[RoomManagerV2 sharedRoomManager] updateRoomModel:roomModel];
    }
    else {
        [[RoomManagerV2 sharedRoomManager] insertRoomModel:roomModel];
    }
}

-(void) insertRoomRecordModel:(RoomRecordModelV2 *)roomRecordModel
{
    RoomRecordModelV2 *roomRecord = [[RoomRecordModelV2 alloc] init];
    roomRecord.rrec_room_assignment_id = roomRecordModel.rrec_room_assignment_id;
    roomRecord.rrec_User_Id = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    [[RoomManagerV2 sharedRoomManager] loadRoomRecordDataByRoomIdAndUserId:roomRecord];
    if (roomRecord.rrec_Id != 0) {
        [[RoomManagerV2 sharedRoomManager] updateRoomRecordData:roomRecordModel];
    }
    else {
        [[RoomManagerV2 sharedRoomManager] insertRoomRecordData:roomRecordModel];
    }
}

#pragma mark - Load Data
-(void)setRoomAssignmentsData:(NSMutableArray *)roomAssignments WithType:(PASS_FAIL_ROOM_ASSIGNMENT_STATUS)type {
    typeRoomAssignmentStatus = type;
    
    self.listDisplayData = [NSMutableArray arrayWithArray:roomAssignments];
    [self.lblNumOfRoom setText:[NSString stringWithFormat:@"%d", (int)[listDisplayData count]]];
    [tbvContent reloadData];
}

-(void)loadData {
    [[RoomManagerV2 sharedRoomManager] getRoomAssignments];
    
    NSMutableArray *arrayTemp = [RoomManagerV2 sharedRoomManager].roomAssignmentList;
    
    self.listTempData = [[NSMutableArray alloc] initWithArray:arrayTemp];
    
    NSMutableArray *array = [NSMutableArray array];
    
    if([typeView isEqualToString:[[LanguageManagerV2 sharedLanguageManager]
                                  getCompletedRoom]]) {
        for(int i = 0; i < [listTempData count]; i++) {
            NSDictionary *rowData = [listTempData objectAtIndex:i];
            
            NSString *roomStatus = [rowData objectForKey:kRMStatus];
            if([roomStatus isEqualToString:nameVC] || [roomStatus isEqualToString:nameOC]
               || [roomStatus isEqualToString:nameVI]
               || [roomStatus isEqualToString:nameOI]) {
                [array addObject:rowData];
            }
        }
    }
    
    if([typeView isEqualToString:[[LanguageManagerV2 sharedLanguageManager]
                                  getPassInspection]]) {
        for(int i = 0; i < [listTempData count]; i++) {
            NSDictionary *rowData = [listTempData objectAtIndex:i];
            
            NSString *roomStatus = [rowData objectForKey:kRMStatus];
            if([roomStatus isEqualToString:nameVI] || [roomStatus isEqualToString:nameOI]) {
                [array addObject:rowData];
            }
        }
    }
    
    if([typeView isEqualToString:[[LanguageManagerV2 sharedLanguageManager]
                                  getFailInspection]]) {
        for(int i = 0; i < [listTempData count]; i++) {
            NSDictionary *rowData = [listTempData objectAtIndex:i];
            
            NSString *roomStatus = [rowData objectForKey:kRMStatus];
            if([roomStatus isEqualToString:nameVD]) {
                [array addObject:rowData];
            }
        }
    }
    
    self.listDisplayData = [[NSMutableArray alloc] initWithArray:array];
    
    if([listDisplayData count] <= 0) {
        [tbvContent setHidden:YES];
        [tbvNoResult setHidden:NO];
    }
    
    if([listDisplayData count]>1) {
        for(int i = 0; i < [listDisplayData count]; i++) {
            for(int j = i + 1; j < [listDisplayData count] - 1; j++) {
                NSDictionary *row1 = [listDisplayData  objectAtIndex:i];
                NSString *roomNo1 = [row1 objectForKey:kRoomNo];
                NSString *guestName1 = [row1 objectForKey:kGuestName];
                
                NSDictionary *row2 = [listDisplayData  objectAtIndex:j];
                NSString *roomNo2 = [row2 objectForKey:kRoomNo];
                NSString *guestName2 = [row2 objectForKey:kGuestName];
                
                if([roomNo1 isEqualToString:roomNo2]) {
                    if([guestName1 isEqualToString:guestName2])
                        [listDisplayData removeObjectAtIndex:j];
                }
            }
        }
    }
    
    [tbvContent reloadData];
}

#pragma mark - Action for Button Detail Arrow
-(void) insertRoomAssignmentFromRowData:(NSDictionary *) rowData {
    RoomAssignmentModelV2 *ramodel = [[RoomAssignmentModelV2 alloc] init];
    ramodel.roomAssignment_Id = [[rowData objectForKey:kraID] intValue];
    ramodel.roomAssignment_UserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    [[RoomManagerV2 sharedRoomManager] load:ramodel];
    if (ramodel.roomAssignment_RoomId.length > 0) {
        //room assignment with raId is existing
        ramodel.roomAssignment_AssignedDate = [rowData objectForKey:kraAssignedTime];
        ramodel.roomAssignment_LastModified = [rowData objectForKey:kraLastModified];
        ramodel.roomAssignment_PostStatus = (int)POST_STATUS_UN_CHANGED;
        ramodel.roomAssignment_RoomId = [rowData objectForKey:kraRoomNo];
        //set user id is current supervisor
        ramodel.roomAssignment_UserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
        ramodel.roomAssignmentHousekeeperId = [[rowData objectForKey:kraHousekeeperID] integerValue];
        ramodel.roomAssignmentRoomCleaningStatusId = [[rowData objectForKey:kraCleaningStatusID] integerValue];
        ramodel.roomAssignmentRoomExpectedCleaningTime = [[rowData objectForKey:kraExpectedCleaningTime] integerValue];
        ramodel.roomAssignmentRoomExpectedInspectionTime = [[rowData objectForKey:kraExpectedInspectTime] integerValue];
        ramodel.roomAssignmentRoomInspectionStatusId = [[rowData objectForKey:kraInspectedStatus] integerValue];
        ramodel.roomAssignmentRoomStatusId = [[rowData objectForKey:kraRoomStatusID] integerValue];
        
        //update room assignment
        [[RoomManagerV2 sharedRoomManager] update:ramodel];
    } else {
        //need insert room assignment because it does not existing in db
        ramodel.roomAssignment_AssignedDate = [rowData objectForKey:kraAssignedTime];
        ramodel.roomAssignment_LastModified = [rowData objectForKey:kraLastModified];
        ramodel.roomAssignment_PostStatus = (int)POST_STATUS_UN_CHANGED;
        ramodel.roomAssignment_RoomId = [rowData objectForKey:kraRoomNo];
        //set user id is current supervisor
        ramodel.roomAssignment_UserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
        ramodel.roomAssignmentHousekeeperId = [[rowData objectForKey:kraHousekeeperID] integerValue];
        ramodel.roomAssignmentRoomCleaningStatusId = [[rowData objectForKey:kraCleaningStatusID] integerValue];
        ramodel.roomAssignmentRoomExpectedCleaningTime = [[rowData objectForKey:kraExpectedCleaningTime] integerValue];
        ramodel.roomAssignmentRoomExpectedInspectionTime = [[rowData objectForKey:kraExpectedInspectTime] integerValue];
        ramodel.roomAssignmentRoomInspectionStatusId = [[rowData objectForKey:kraInspectedStatus] integerValue];
        ramodel.roomAssignmentRoomStatusId = [[rowData objectForKey:kraRoomStatusID] integerValue];
        [[RoomManagerV2 sharedRoomManager] insert:ramodel];
    }
}

-(void) insertRoomFromRowData:(NSDictionary *) rowData {
    RoomModelV2 *room = [[RoomModelV2 alloc] init];
    room.room_Id = [rowData objectForKey:kraRoomNo];
    [[RoomManagerV2 sharedRoomManager] loadRoomModel:room];
    
    if (room.room_HotelId == 0) {
        //room is not existing
        room.room_AdditionalJob = [rowData objectForKey:kraAdditionalJob];
        room.room_Building_Id = [[rowData objectForKey:kraBuildingID] integerValue];
        room.room_Building_Name = [rowData objectForKey:kraBuildingName];
        room.room_building_namelang = [rowData objectForKey:krabuildingLang];
        room.room_floor_id = [[rowData objectForKey:kraFloorID] integerValue];
        room.room_Guest_Name = [NSString stringWithFormat:@"%@ %@", [rowData objectForKey:kraGuestFirstName], [rowData objectForKey:kraGuestLastName]];
        room.room_GuestReference = [rowData objectForKey:kraGuestPreference];
        room.room_HotelId = [[rowData objectForKey:kraHotelID] intValue];
        room.room_Lang = @"";
        room.room_LastModified = [rowData objectForKey:kraLastModified];
        room.room_PostStatus = (int)POST_STATUS_UN_CHANGED;
        room.room_remark = [rowData objectForKey:kraRemark];
        room.room_TypeId = [[rowData objectForKey:kraRoomTypeID] intValue];
        room.room_VIPStatusId = [rowData objectForKey:kraVIP];
        room.room_floor_id = [[rowData objectForKey:kraFloorID] integerValue];
        
        [[RoomManagerV2 sharedRoomManager] insertRoomModel:room];
    } else {
        //room is existing
        room.room_AdditionalJob = [rowData objectForKey:kraAdditionalJob];
        room.room_Building_Id = [[rowData objectForKey:kraBuildingID] integerValue];
        room.room_Building_Name = [rowData objectForKey:kraBuildingName];
        room.room_building_namelang = [rowData objectForKey:krabuildingLang];
        room.room_floor_id = [[rowData objectForKey:kraFloorID] integerValue];
        room.room_Guest_Name = [NSString stringWithFormat:@"%@ %@", [rowData objectForKey:kraGuestFirstName], [rowData objectForKey:kraGuestLastName]];
        room.room_GuestReference = [rowData objectForKey:kraGuestPreference];
        room.room_HotelId = [[rowData objectForKey:kraHotelID] intValue];
        room.room_Lang = @"";
        room.room_LastModified = [rowData objectForKey:kraLastModified];
        room.room_PostStatus = (int)POST_STATUS_UN_CHANGED;
        room.room_remark = [rowData objectForKey:kraRemark];
        room.room_TypeId = [[rowData objectForKey:kraRoomTypeID] intValue];
        room.room_VIPStatusId = [rowData objectForKey:kraVIP];
        room.room_floor_id = [[rowData objectForKey:kraFloorID] integerValue];
        [[RoomManagerV2 sharedRoomManager] updateRoomModel:room];
    }
}

-(void) insertGuestInfoFromRowData:(NSDictionary *) rowData {
    GuestInfoModelV2 *gmodel = [[GuestInfoModelV2 alloc] init];
    gmodel.guestId = [rowData objectForKey:kraRoomNo];
    [[RoomManagerV2 sharedRoomManager] loadGuestInfo:gmodel];
    
    if (gmodel.guestRoomId.length <= 0) {
        
        //Removed because of changing WS for Guest Information
        //[[RoomManagerV2 sharedRoomManager] updateGuestInfo:[[rowData objectForKey:kraHousekeeperID] integerValue]  WithRoomAssignID:[[rowData objectForKey:kraID] integerValue]];
        [[RoomManagerV2 sharedRoomManager] updateGuestInfoByUserId:[UserManagerV2 sharedUserManager].currentUser.userId hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId roomNumber:[rowData objectForKey:kraRoomNo]];
    } else {
        //room is existing
    }
}

-(void) insertRoomRecordFromRowData:(NSDictionary *) rowData {
    RoomRecordModelV2 *record = [[RoomRecordModelV2 alloc] init];
    
    record.rrec_User_Id = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    record.rrec_room_assignment_id = [[rowData objectForKey:kraID] intValue];
    
    [[RoomManagerV2 sharedRoomManager] loadRoomRecordDataByRoomIdAndUserId:record];
    
    record.rrec_Cleaning_Start_Time = [rowData objectForKey:kraCleanStartTm];
    record.rrec_Cleaning_End_Time = [rowData objectForKey:kraCleanEndTm];
    
    record.rrec_Inspection_Start_Time = [rowData objectForKey:kraInspectStartTm];
    record.rrec_Inspection_End_Time = [rowData objectForKey:kraInspectEndTm];
    record.rrec_Total_Inspected_Time = [NSString stringWithFormat:@"%d", [[rowData objectForKey:kraTotalInspectedTime] intValue] * 60];
    
    //Hao Tran - Remove unneccessary things
    /*
    NSDate *startDate = nil;
    NSDate *endDate = nil;
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    [f setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    startDate = [f dateFromString:record.rrec_Inspection_Start_Time];
    endDate = [f dateFromString:record.rrec_Inspection_End_Time];
    
    NSTimeInterval timeDifference = [endDate timeIntervalSinceDate:startDate];
    NSInteger timeDifferenceInt = (NSInteger) timeDifference;
    record.rrec_Inspected_Total_Pause_Time = [NSString stringWithFormat:@"%d", (timeDifferenceInt - [record.rrec_Inspection_Duration integerValue]) > 0 ? (timeDifferenceInt - [record.rrec_Inspection_Duration integerValue]) : 0];
    
    record.rrec_Cleaning_Duration = [NSString stringWithFormat:@"%d", [[rowData objectForKey:kraTotalCleaningTime] integerValue] * 60];
    
    startDate = [f dateFromString:record.rrec_Cleaning_Start_Time];
    endDate = [f dateFromString:record.rrec_Cleaning_End_Time];
    
    timeDifference = [endDate timeIntervalSinceDate:startDate];
    timeDifferenceInt = (NSInteger) timeDifference;
    record.rrec_Cleaning_Total_Pause_Time = [NSString stringWithFormat:@"%d", (timeDifferenceInt - [record.rrec_Cleaning_Duration integerValue]) > 0 ? (timeDifferenceInt - [record.rrec_Cleaning_Duration integerValue]) : 0];
    */
    
    record.rrec_PostStatus = (int)POST_STATUS_UN_CHANGED;
    
    if (record.rrec_Id == 0) {
        [[RoomManagerV2 sharedRoomManager] insertRoomRecordData:record];
    } else
        [[RoomManagerV2 sharedRoomManager] updateRoomRecordData:record];
}

-(void)btnDetailArrowPressed:(UIButton *)sender {
    
//    NSInteger index = sender.tag;
//    NSDictionary *rowData = [listDisplayData objectAtIndex:index];
//    
//    //insert to database room, room assignment, guest info
//    [self insertRoomAssignmentFromRowData:rowData];
//    [self insertRoomFromRowData:rowData];
//    [self insertGuestInfoFromRowData:rowData];
//    [self insertRoomRecordFromRowData:rowData];
//    
//    switch (typeRoomAssignmentStatus) {
//        case PASS_ROOM_ASSIGNMENT_STATUS:
//        {
//            RoomInfoInspectedPassedV2 *aRoomInfo = nil;
//            
//            RoomModelV2 *roomModel = [[RoomModelV2 alloc] init];
//            roomModel.room_Id = [rowData objectForKey:kraRoomNo];
//            [[RoomManagerV2 sharedRoomManager] loadRoomModel:roomModel];
//            
//            //set current room no
//            [RoomManagerV2 setCurrentRoomNo:roomModel.room_Id];
//            
//            RoomAssignmentModelV2 *raModel = [[RoomAssignmentModelV2 alloc] init];
//            raModel.roomAssignment_Id = [[rowData objectForKey:kraID] integerValue];
//            raModel.roomAssignment_UserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
//            [[RoomManagerV2 sharedRoomManager] load:raModel];
//            
//            //pass
//            aRoomInfo = [[RoomInfoInspectedPassedV2 alloc] initWithNibName:@"RoomInfoInspectedPassedV2" bundle:nil];
//            
//            GuestInfoModelV2 *guestInfoModel = [[GuestInfoModelV2 alloc] init];
//            guestInfoModel.guestRoomId = roomModel.room_Id;
//            [[RoomManagerV2 sharedRoomManager] loadByRoomIDGuestInfo:guestInfoModel];
//            
//            
//            aRoomInfo.roomName = [rowData objectForKey:kraRoomNo];
//            aRoomInfo.roomModel = roomModel;
//            aRoomInfo.guestInfoModel = guestInfoModel;
//            aRoomInfo.raDataModel = raModel;
//            
//            //set current RoomAssignmentId use in ChecklistView
//            [TasksManagerV2 setCurrentRoomAssignment:[[rowData objectForKey:kraID] integerValue]];
//            
//            [self.navigationController pushViewController:aRoomInfo animated:YES];
//            [aRoomInfo loadingData];
//            
//            [self.navigationItem.backBarButtonItem setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK]];
//            
//        }
//            break;
//            
//        case FAIL_ROOM_ASSIGNMENT_STATUS:
//        {
//            RoomInfoInspectedFailedV2 *aRoomInfo = nil;
//            
//            RoomModelV2 *roomModel = [[RoomModelV2 alloc] init];
//            roomModel.room_Id = [rowData objectForKey:kraRoomNo];
//            [[RoomManagerV2 sharedRoomManager] loadRoomModel:roomModel];
//            
//            //set current room no
//            [RoomManagerV2 setCurrentRoomNo:roomModel.room_Id];
//            
//            RoomAssignmentModelV2 *raModel = [[RoomAssignmentModelV2 alloc] init];
//            raModel.roomAssignment_Id = [[rowData objectForKey:kraID] integerValue];
//            raModel.roomAssignment_UserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
//            [[RoomManagerV2 sharedRoomManager] load:raModel];
//            
//            //fail
//            aRoomInfo = [[RoomInfoInspectedFailedV2 alloc] initWithNibName:@"RoomInfoInspectedFailedV2" bundle:nil];
//            
//            GuestInfoModelV2 *guestInfoModel = [[GuestInfoModelV2 alloc] init];
//            guestInfoModel.guestRoomId = roomModel.room_Id;
//            [[RoomManagerV2 sharedRoomManager] loadByRoomIDGuestInfo:guestInfoModel];
//            
//            
//            [aRoomInfo setRoomName : [rowData objectForKey:kraRoomNo]];
//            [aRoomInfo setRoomModel : roomModel];
//            [aRoomInfo setGuestInfoModel : guestInfoModel];
//            [aRoomInfo setRaDataModel: raModel];
//            
//            //set current RoomAssignmentId use in ChecklistView
//            [TasksManagerV2 setCurrentRoomAssignment:[[rowData objectForKey:kraID] integerValue]];
//            
//            [self.navigationController pushViewController:aRoomInfo animated:YES];
//            [aRoomInfo loadingData];
//            
//            [self.navigationItem.backBarButtonItem setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK]];
//        }
//            break;
//            
//        case COMPLETED_ROOM_ASSIGNMENT_STATUS:
//        {
//            //RoomInfoInspectionV2
//            RoomInfoInspectionV2 *aRoomInfoAssignment = [[RoomInfoInspectionV2 alloc] initWithNibName:@"RoomInfoInspectionV2" bundle:nil];
//            
//            
//            RoomModelV2 *roomModel = [[RoomModelV2  alloc] init];
//            roomModel.room_Id = [rowData objectForKey:kraRoomNo];
//            [[RoomManagerV2 sharedRoomManager] loadRoomModel:roomModel];
//            
//            //set current room no
//            [RoomManagerV2 setCurrentRoomNo:roomModel.room_Id];
//            
//            GuestInfoModelV2 *guestInfoModel = [[GuestInfoModelV2 alloc] init];
//            guestInfoModel.guestRoomId = roomModel.room_Id;
//            [[RoomManagerV2 sharedRoomManager] loadByRoomIDGuestInfo:guestInfoModel];
//            
//            RoomAssignmentModelV2 *roomAssignmentModel = [[RoomAssignmentModelV2 alloc] init];
//            roomAssignmentModel.roomAssignment_Id = [[rowData objectForKey:kraID] intValue];
//            roomAssignmentModel.roomAssignment_UserId = [UserManagerV2 sharedUserManager].currentUser.userId;
//            [[RoomManagerV2 sharedRoomManager] load:roomAssignmentModel];
//            
//            
//            aRoomInfoAssignment.roomName = [rowData objectForKey:kraRoomNo];
//            aRoomInfoAssignment.roomModel = roomModel;
//            aRoomInfoAssignment.guestInfoModel = guestInfoModel;
//            aRoomInfoAssignment.raDataModel = roomAssignmentModel;
//            
//            //set current RoomAssignmentId use in ChecklistView
//            [TasksManagerV2 setCurrentRoomAssignment:[[rowData objectForKey:kraID] integerValue]];
//            
//            [self.navigationController pushViewController:aRoomInfoAssignment animated:YES];
//            [aRoomInfoAssignment loadingData];
//            
//            [self.navigationItem.backBarButtonItem setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK]];
//        }
//            break;
//            
//        default:
//        {
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_data_error] message:nil delegate:nil cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getOK] otherButtonTitles: nil];
//            [alert show];
//        }
//            break;
//    }
}

#pragma mark  - Set Captions View
-(void)setCaptionsView {
    [self setTitle:[[LanguageManagerV2 sharedLanguageManager] getFind]];
    
    [self.lblNameAndType setText:typeView];
    
    [lblNameAndType setFont:[UIFont fontWithName:font size:sizeNormal]];
    
    [lblRoomNo setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOM_NO]];
    [lblRoomNo setFont:[UIFont fontWithName:font size:sizeNormal]];
    
    [lblRoomStatus setText:[[LanguageManagerV2 sharedLanguageManager] getRMStatus]];
    [lblRoomStatus setFont:[UIFont fontWithName:font size:sizeNormal]];
    
    [lblVIP setText:[[LanguageManagerV2 sharedLanguageManager] getVIP]];
    [lblVIP setFont:[UIFont fontWithName:font size:sizeNormal]];
    
    [lblDuration setText:[[LanguageManagerV2 sharedLanguageManager] getCleaningStatus]];
    [lblDuration setFont:[UIFont fontWithName:font size:sizeNormal]];
}

#pragma mark - === Handle Topbar Methods ===
#pragma mark
-(void)loadTopbarView {
    TopbarViewV2 *topbar = [[TopbarViewV2 alloc] initViewWithSuperModel:[[CommonVariable sharedCommonVariable] superRoomModel]];
    [topbar setTag:tagTopbarView];
    [self.view addSubview:topbar];
    [topbar setHidden:YES];
    
    [topbar setAlpha:0.0];
    [self addButtonHandleShowHideTopbar];
}

-(void)showTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationWillStartSelector:@selector(setHiddenTopbarView)];
    
    f = topbar.frame;
    f.origin.y = 0;
    [topbar setFrame:f];
    [topbar setAlpha:1.0];
    
    [self adjustShowForViews];
    
    [UIView commitAnimations];
}

-(void)hideTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDidStopSelector:@selector(setHiddenTopbarView)];
    [UIView setAnimationDelegate:self];
    
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    [topbar setAlpha:0.0];
    
    [self adjustRemoveForViews];
    
    [UIView commitAnimations];
}

-(void)setHiddenTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    [topbar setHidden:!topbar.hidden];
}

-(TopbarViewV2 *)getTopBarView {
    return (TopbarViewV2 *)[self.view viewWithTag:tagTopbarView];
}

//-(void)addButtonHandleShowHideTopbar {
//    UIButton *titleBarButton = [[UIButton alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
//
//    titleBarButton.titleLabel.backgroundColor = [UIColor clearColor];
//    [titleBarButton.titleLabel setFont: FONT_BUTTON_TOPBAR];
//    titleBarButton.titleLabel.textAlignment = NSTextAlignmentCenter;
//    titleBarButton.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
//    [titleBarButton.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
//    [titleBarButton.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
//    [titleBarButton addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
//    [titleBarButton setTitle:[[LanguageManagerV2 sharedLanguageManager] getFind] forState:UIControlStateNormal];
//    self.navigationItem.titleView = titleBarButton;
//}

-(void)addButtonHandleShowHideTopbar {
    
    UIView *vBarButton = [[UIView alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
    UIButton *titleBarButtonFirst = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 200, 30)];
    
    titleBarButtonFirst.titleLabel.backgroundColor = [UIColor clearColor];
    [titleBarButtonFirst.titleLabel setFont: FONT_BUTTON_TOPBAR];
    titleBarButtonFirst.titleLabel.textAlignment = NSTextAlignmentCenter;
    titleBarButtonFirst.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
    
    [titleBarButtonFirst.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
    [titleBarButtonFirst.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
    [titleBarButtonFirst addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    [titleBarButtonFirst setTitle:[[LanguageManagerV2 sharedLanguageManager] getFind] forState:UIControlStateNormal];
    
    [vBarButton addSubview:titleBarButtonFirst];
    
    
    UIButton *titleBarButtonSecond = [[UIButton alloc] initWithFrame:CGRectMake(0, 22, 200, 22)];
    [titleBarButtonSecond setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [titleBarButtonSecond.titleLabel setFont:FONT_BUTTON_TOPBAR_SECOND];
    [titleBarButtonSecond setTitle: attendantName forState:UIControlStateNormal];
    [titleBarButtonSecond addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    [vBarButton addSubview:titleBarButtonSecond];
    
    self.navigationItem.titleView = vBarButton;
    
    //Move title to center vertical
    CGRect titleViewBounds = self.navigationItem.titleView.bounds;
    int moveLeftTitle = 0;
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        moveLeftTitle = 10;
    }
    self.navigationItem.titleView.bounds = CGRectMake(titleViewBounds.origin.x + moveLeftTitle, Y_TOPBAR_TITLE, titleViewBounds.size.width, titleViewBounds.size.height);
}

-(void)topbarTapped:(UIButton *)sender {
    TopbarViewV2 *topbar = [self getTopBarView];
    [sender setEnabled:NO];
    if (topbar.isHidden) {
        [self showTopbarView];
    } else {
        [self hideTopbarView];
    }
    [self performSelector:@selector(topbarButtonEnable:) withObject:sender afterDelay:0.3];
}

-(void) topbarButtonEnable:(UIButton *) topbarBtn {
    [topbarBtn setEnabled:YES];
}

-(void)adjustShowForViews {
    //adjust all views when show
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    CGRect ftbv = controlsView.frame;
    
    [controlsView setFrame:CGRectMake(0, f.size.height, 320, ftbv.size.height)];
    
    ftbv = tbvContent.frame;
    [tbvContent setFrame:CGRectMake(0, f.size.height + controlsView.frame.size.height, 320, ftbv.size.height - f.size.height)];
    
    ftbv = tbvNoResult.frame;
    [tbvNoResult setFrame:CGRectMake(0, f.size.height + controlsView.frame.size.height, 320, ftbv.size.height - f.size.height)];
}

-(void)adjustRemoveForViews {
    //adjust all views when hide
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    CGRect ftbv = controlsView.frame;
    [controlsView setFrame:CGRectMake(0, 0, 320, ftbv.size.height)];
    
    ftbv = tbvContent.frame;
    [tbvContent setFrame:CGRectMake(0, controlsView.frame.size.height, 320, ftbv.size.height + f.size.height)];
    
    ftbv = tbvNoResult.frame;
    [tbvNoResult setFrame:CGRectMake(0, controlsView.frame.size.height, 320, ftbv.size.height + f.size.height)];
}

#pragma mark - === Handle Back Button ===
#pragma mark

-(void)backBarPressed {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)addBackButton {
    //back button navigation
    self.navigationItem.leftBarButtonItem = [MyNavigationBarV2 createBackButtonWithTarget:self selector:@selector(backBarPressed) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - iOS 7 Resources
-(void)loadFlatResource
{
    [imgHeader setImage:[UIImage imageNamed:imgLaundryCartButtonFlat]];
}

@end
