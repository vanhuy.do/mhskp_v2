//
//  FindMainMenuCellV2.h
//  mHouseKeeping
//
//  Created by Nghia Truong on 2/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FindMainMenuCellV2 : UITableViewCell {
    UIImageView *imgFindCategory;
    UILabel *lblFindCategory;
}

@property (strong, nonatomic) IBOutlet UIImageView *imgFindCategory;
@property (strong, nonatomic) IBOutlet UILabel *lblFindCategory;

@end
