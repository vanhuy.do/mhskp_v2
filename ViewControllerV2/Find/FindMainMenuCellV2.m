//
//  FindMainMenuCellV2.m
//  mHouseKeeping
//
//  Created by Nghia Truong on 2/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FindMainMenuCellV2.h"

@implementation FindMainMenuCellV2

@synthesize lblFindCategory, imgFindCategory;

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void)dealloc{
//    [imgFindCategory release];
//    [lblFindCategory release];
}

@end
