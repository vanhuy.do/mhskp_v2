//
//  FindMainMenuViewV2.h
//  mHouseKeeping
//
//  Created by Nghia Truong on 2/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FindMainMenuCellV2.h"

@interface FindMainMenuViewV2 : UIViewController {
    UITableView *tbvFindCategory;
    
    UIButton *btnBack;
}

@property (strong, nonatomic) IBOutlet UITableView *tbvFindCategory;
@property (strong, nonatomic) IBOutlet FindMainMenuCellV2 *findCategoryCell;
@property (readwrite, nonatomic) BOOL isFindRoomNo;
@property (nonatomic, strong) NSArray *dataSource;

//-(void) setCaptionsView;

@end
