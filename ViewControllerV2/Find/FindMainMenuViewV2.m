//
//  FindMainMenuViewV2.m
//  mHouseKeeping
//
//  Created by Nghia Truong on 2/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FindMainMenuViewV2.h"
#import "FindByRoomViewV2.h"
#import "FindByAttendantViewV2.h"
#import "FindByRoomFloorViewV2.h"
#import "MyNavigationBarV2.h"
#import "FindAllRoomsViewV2.h"
#import "DeviceManager.h"
#import "ehkDefines.h"
#import "UserManagerV2.h"
#import "HomeViewV2.h"
#import "FindCellObject.h"
#import "FindByZoneViewV2.h"
#import "FindByFloorViewV2.h"
#import "CustomAlertViewV2.h"

#define imgAttendant    @"attendant.png"
#define imgRoom         @"room_bt.png"
#define imgRoomNo       @"roomnumber_btn.png"
#define backgroundImage @"bg.png"

#define tagAttendant    1
#define tagRoom         2
#define tagRoomByFindStatus 3
#define tagRoomByFindRoomNo 4
#define font            @"Arial-BoldMT"
#define fontSize        22.0f
#define isDemoMode ([[NSUserDefaults standardUserDefaults] integerForKey:ENABLE_DEMO_MODE] == 1)

typedef NS_ENUM (NSInteger, FindByType) {
    FindByType_Attendant,
    FindByType_RoomStatus,
    FindByType_RoomNo,
    FindByType_Zone,
    FindByType_Floor
};

@interface FindMainMenuViewV2(PrivateMethods)

-(void) loadTopbarView;
-(void) showTopbarView;
-(void) hideTopbarView;
-(void) setHiddenTopbarView;
-(void) adjustShowForViews;
-(void) adjustRemoveForViews;
-(void) addButtonHandleShowHideTopbar;
-(void) topbarTapped:(UIButton *) sender;
-(TopbarViewV2 *) getTopBarView;
-(void) btnBackPressed;
-(void) addBackButton;

@end

@implementation FindMainMenuViewV2

@synthesize findCategoryCell, tbvFindCategory, isFindRoomNo;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //show wifi view
    if(!isDemoMode){
        [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationShowWifiView] object:nil];
    }
    
    //set selection find in bottom bar
    [[HomeViewV2 shareHomeView] setSelectedButton:tagOfFindButton];
    
    [self createDataSourceForFindMainView];
}

- (void)viewDidLoad {
    [tbvFindCategory setBackgroundView:nil];
    [tbvFindCategory setBackgroundColor:[UIColor clearColor]];
    [tbvFindCategory setOpaque:YES];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //Resize content views of current VC to fit with Navigation bar and tabbar
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    [self performSelector:@selector(loadTopbarView)];
    
    //add back button
    [self addBackButton];
}

- (void)viewDidUnload {
    
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark UITableView Delegate Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView  numberOfRowsInSection:(NSInteger)section {
    //EditedBy-DungPhan-2015/03/03
    //    isFindRoomNo = [[UserManagerV2 sharedUserManager].currentUserAccessRight isFindRoomNo];
    //    if([UserManagerV2 isSupervisor])
    //    {
    //        return isFindRoomNo == YES ? 3:2;
    //
    //    }
    //    return isFindRoomNo == YES ? 2:1;
    return [self.dataSource count];//End edit
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 120;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *IdentifyCell = @"IdentifyCell";
    FindMainMenuCellV2 *cell = nil;
    cell = [tableView dequeueReusableCellWithIdentifier:IdentifyCell];
    
    if(cell == nil){
        NSArray *array = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([FindMainMenuCellV2 class]) owner:self options:nil];
        cell = [array objectAtIndex:0];
        [cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
        
        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
            cell.backgroundColor = [UIColor clearColor];
        }
    }
    //EditedBy-DungPhan-2015/03/03
    FindCellObject *cellOjb = [self.dataSource objectAtIndex:indexPath.row];
    [cell.imgFindCategory setImage:[UIImage imageNamed:cellOjb.imageName]];
    [cell.lblFindCategory setFont:[UIFont fontWithName:font size:fontSize]];
    [cell.lblFindCategory setText:cellOjb.title];
    [cell setTag:cellOjb.tag];
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        cell.backgroundColor = [UIColor clearColor];
    }
    return cell;
    //    if(![UserManagerV2 isSupervisor])
    //    {
    //        switch (indexPath.row) {
    //            case 0: {
    //                [cell.imgFindCategory setImage:[UIImage imageNamed:imgRoom]];
    //                [cell.lblFindCategory setFont:[UIFont fontWithName:font size:fontSize]];
    //                isFindRoomNo = [[UserManagerV2 sharedUserManager].currentUserAccessRight isFindRoomNo];
    //                if(isFindRoomNo)
    //                {
    //                    [cell.lblFindCategory setText:[[LanguageManagerV2 sharedLanguageManager] getRoomTitleByFindStatus]];
    //                    [cell setTag:tagRoomByFindStatus];
    //                }
    //                else{
    //                    [cell.lblFindCategory setText:[[LanguageManagerV2 sharedLanguageManager] getRoomTitle]];
    //                    [cell setTag:tagRoom];
    //                }
    //            }
    //                break;
    //            case 1: {
    //
    //                if(isDemoMode)
    //                {
    //                    [cell.imgFindCategory setImage:[UIImage imageNamed:imgRoomNo]];
    //                    [cell.lblFindCategory setFont:[UIFont fontWithName:font size:fontSize]];
    //                    [cell.lblFindCategory setText:[[LanguageManagerV2 sharedLanguageManager] getRoomTitleByFindRoomNo]];
    //                    [cell setTag:tagRoomByFindRoomNo];
    //                }
    //                else{
    //                    isFindRoomNo = [[UserManagerV2 sharedUserManager].currentUserAccessRight isFindRoomNo];
    //                    if(isFindRoomNo)
    //                    {
    //                        [cell.imgFindCategory setImage:[UIImage imageNamed:imgRoomNo]];
    //                        [cell.lblFindCategory setFont:[UIFont fontWithName:font size:fontSize]];
    //                        [cell.lblFindCategory setText:[[LanguageManagerV2 sharedLanguageManager] getRoomTitleByFindRoomNo]];
    //                        [cell setTag:tagRoomByFindRoomNo];
    //                    }
    //                }
    //            }
    //
    //            default:
    //                break;
    //        }
    //    }
    //    else
    //    {
    //        switch (indexPath.row) {
    //            case 0: {
    //                [cell.imgFindCategory setImage:[UIImage imageNamed:imgAttendant]];
    //                [cell.lblFindCategory setText:[[LanguageManagerV2 sharedLanguageManager] getAttendant]];
    //                [cell.lblFindCategory setFont:[UIFont fontWithName:font size:fontSize]];
    //                [cell setTag:tagAttendant];
    //            }
    //                break;
    //
    //            case 1: {
    //                [cell.imgFindCategory setImage:[UIImage imageNamed:imgRoom]];
    //                [cell.lblFindCategory setFont:[UIFont fontWithName:font size:fontSize]];
    //                isFindRoomNo = [[UserManagerV2 sharedUserManager].currentUserAccessRight isFindRoomNo];
    //                if(isFindRoomNo)
    //                {
    //                    [cell.lblFindCategory setText:[[LanguageManagerV2 sharedLanguageManager] getRoomTitleByFindStatus]];
    //                    [cell setTag:tagRoomByFindStatus];
    //                }
    //                else{
    //                    [cell.lblFindCategory setText:[[LanguageManagerV2 sharedLanguageManager] getRoomTitle]];
    //                    [cell setTag:tagRoom];
    //                }
    //            }
    //                break;
    //            case 2: {
    //                isFindRoomNo = [[UserManagerV2 sharedUserManager].currentUserAccessRight isFindRoomNo];
    //                if(isFindRoomNo)
    //                {
    //                    [cell.imgFindCategory setImage:[UIImage imageNamed:imgRoomNo]];
    //                    [cell.lblFindCategory setFont:[UIFont fontWithName:font size:fontSize]];
    //                    [cell.lblFindCategory setText:[[LanguageManagerV2 sharedLanguageManager] getRoomTitleByFindRoomNo]];
    //                    [cell setTag:tagRoomByFindRoomNo];
    //                }
    //            }
    //
    //            default:
    //                break;
    //        }
    //    }
    //    return cell;
    //End edit
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [cell setSelected:NO animated:YES];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    //EditedBy-DungPhan-2015/03/03
    FindCellObject *cellObj = [self.dataSource objectAtIndex:indexPath.row];
    switch (cellObj.tag) {
        case FindByType_Attendant: {
            FindByAttendantViewV2 *findByAttendantView = [[FindByAttendantViewV2 alloc] initWithNibName:NSStringFromClass([FindByAttendantViewV2 class]) bundle:nil];
            [self.navigationController pushViewController:findByAttendantView animated:YES];
        }
            break;
            
        case FindByType_RoomStatus: {
            
            BOOL isFindRoomByFloor = [[UserManagerV2 sharedUserManager].currentUserAccessRight isFindRoomByFloor];
            
            if(isFindRoomByFloor) {
                FindByRoomFloorViewV2 *findByRoomView = [[FindByRoomFloorViewV2 alloc] initWithNibName:NSStringFromClass([FindByRoomFloorViewV2 class]) bundle:nil];
                findByRoomView.isLoadFromWS = TRUE;
                findByRoomView.isLoadByFindRoomNo = FALSE;
                [self.navigationController pushViewController:findByRoomView animated:YES];
            } else {
                FindAllRoomsViewV2  *findAllRoomsView = [[FindAllRoomsViewV2 alloc] initWithNibName:NSStringFromClass([FindAllRoomsViewV2 class]) bundle:nil];
                findAllRoomsView.isLoadFromWS = TRUE;
                [self.navigationController pushViewController:findAllRoomsView animated:YES];
            }
        }
            break;
        case FindByType_RoomNo: {
            FindByRoomFloorViewV2 *findByRoomView = [[FindByRoomFloorViewV2 alloc] initWithNibName:NSStringFromClass([FindByRoomFloorViewV2 class]) bundle:nil];
            findByRoomView.isLoadFromWS = TRUE;
            findByRoomView.isLoadByFindRoomNo = TRUE;
            [self.navigationController pushViewController:findByRoomView animated:YES];
            break;
        }
        case FindByType_Zone: {
            FindByZoneViewV2 *findByRoomView = [[FindByZoneViewV2 alloc] initWithNibName:NSStringFromClass([FindByZoneViewV2 class]) bundle:nil];
            [self.navigationController pushViewController:findByRoomView animated:YES];
            break;
        }
        case FindByType_Floor: {
            BOOL isOnline = [[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp];
            if (isOnline) {
                FindByFloorViewV2 *findByRoomView = [[FindByFloorViewV2 alloc] initWithNibName:NSStringFromClass([FindByFloorViewV2 class]) bundle:nil];
                [self.navigationController pushViewController:findByRoomView animated:YES];
            }
            else {
                if (!isOnline) {
                    UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:@"" message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_internet_exception] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getOK] otherButtonTitles:nil];
                    alert.delegate = self;
                    //[alert setTag:tagNoNetwork];
                    [alert show];
                }
            }
            break;
        }
        default:
            break;
    }
    //    if(![UserManagerV2 isSupervisor])
    //    {
    //        switch (indexPath.row) {
    //            case 0: {
    //                BOOL isFindRoomByFloor = [[UserManagerV2 sharedUserManager].currentUserAccessRight isFindRoomByFloor];
    //
    //                if(isFindRoomByFloor) {
    //                    FindByRoomFloorViewV2 *findByRoomView = [[FindByRoomFloorViewV2 alloc] initWithNibName:NSStringFromClass([FindByRoomFloorViewV2 class]) bundle:nil];
    //                    findByRoomView.isLoadFromWS = TRUE;
    //                    findByRoomView.isLoadByFindRoomNo = FALSE;
    //                    [self.navigationController pushViewController:findByRoomView animated:YES];
    //                } else {
    //                    FindAllRoomsViewV2  *findAllRoomsView = [[FindAllRoomsViewV2 alloc] initWithNibName:NSStringFromClass([FindAllRoomsViewV2 class]) bundle:nil];
    //                    findAllRoomsView.isLoadFromWS = TRUE;
    //                    [self.navigationController pushViewController:findAllRoomsView animated:YES];
    //                }
    //            }
    //                break;
    //            case 1: {
    //                FindByRoomFloorViewV2 *findByRoomView = [[FindByRoomFloorViewV2 alloc] initWithNibName:NSStringFromClass([FindByRoomFloorViewV2 class]) bundle:nil];
    //                findByRoomView.isLoadFromWS = TRUE;
    //                findByRoomView.isLoadByFindRoomNo = TRUE;
    //                [self.navigationController pushViewController:findByRoomView animated:YES];
    //            }
    //            default:
    //                break;
    //        }
    //    }
    //    else{
    //        switch (indexPath.row) {
    //            case 0: {
    //                FindByAttendantViewV2 *findByAttendantView = [[FindByAttendantViewV2 alloc] initWithNibName:NSStringFromClass([FindByAttendantViewV2 class]) bundle:nil];
    //                [self.navigationController pushViewController:findByAttendantView animated:YES];
    //            }
    //                break;
    //
    //            case 1: {
    //
    //                BOOL isFindRoomByFloor = [[UserManagerV2 sharedUserManager].currentUserAccessRight isFindRoomByFloor];
    //
    //                if(isFindRoomByFloor) {
    //                    FindByRoomFloorViewV2 *findByRoomView = [[FindByRoomFloorViewV2 alloc] initWithNibName:NSStringFromClass([FindByRoomFloorViewV2 class]) bundle:nil];
    //                    findByRoomView.isLoadFromWS = TRUE;
    //                    findByRoomView.isLoadByFindRoomNo = FALSE;
    //                    [self.navigationController pushViewController:findByRoomView animated:YES];
    //                } else {
    //                    FindAllRoomsViewV2  *findAllRoomsView = [[FindAllRoomsViewV2 alloc] initWithNibName:NSStringFromClass([FindAllRoomsViewV2 class]) bundle:nil];
    //                    findAllRoomsView.isLoadFromWS = TRUE;
    //                    [self.navigationController pushViewController:findAllRoomsView animated:YES];
    //                }
    //            }
    //                break;
    //            case 2: {
    //                FindByRoomFloorViewV2 *findByRoomView = [[FindByRoomFloorViewV2 alloc] initWithNibName:NSStringFromClass([FindByRoomFloorViewV2 class]) bundle:nil];
    //                findByRoomView.isLoadFromWS = TRUE;
    //                findByRoomView.isLoadByFindRoomNo = TRUE;
    //                [self.navigationController pushViewController:findByRoomView animated:YES];
    //            }
    //            default:
    //                break;
    //        }
    //    }
    //End edit
}

#pragma mark - === Handle Topbar Methods ===

-(void)loadTopbarView {
    TopbarViewV2 *topbar = [[TopbarViewV2 alloc] initViewWithSuperModel:[[CommonVariable sharedCommonVariable] superRoomModel]];
    [topbar setTag:tagTopbarView];
    [self.view addSubview:topbar];
    [topbar setHidden:YES];
    
    [topbar setAlpha:0.0];
    [self addButtonHandleShowHideTopbar];
}

-(void)showTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationWillStartSelector:@selector(setHiddenTopbarView)];
    
    f = topbar.frame;
    f.origin.y = 0;
    [topbar setFrame:f];
    [topbar setAlpha:1.0];
    
    [self adjustShowForViews];
    
    [UIView commitAnimations];
}

-(void)hideTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDidStopSelector:@selector(setHiddenTopbarView)];
    [UIView setAnimationDelegate:self];
    
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    [topbar setAlpha:0.0];
    
    [self adjustRemoveForViews];
    
    [UIView commitAnimations];
}

-(void)setHiddenTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    [topbar setHidden:!topbar.hidden];
}

-(TopbarViewV2 *)getTopBarView {
    return (TopbarViewV2 *)[self.view viewWithTag:tagTopbarView];
}

-(void)addButtonHandleShowHideTopbar {
    
    
    UIButton *titleBarButton = [[UIButton alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
    
    titleBarButton.titleLabel.backgroundColor = [UIColor clearColor];
    [titleBarButton.titleLabel setFont: FONT_BUTTON_TOPBAR];
    titleBarButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    titleBarButton.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
    
    [titleBarButton.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
    [titleBarButton.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
    [titleBarButton addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    [titleBarButton setTitle:[[LanguageManagerV2 sharedLanguageManager] getFind] forState:UIControlStateNormal];
    self.navigationItem.titleView = titleBarButton;
    
    //Move title to center vertical
    CGRect titleViewBounds = self.navigationItem.titleView.bounds;
    int moveLeftTitle = 0;
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        moveLeftTitle = 10;
    }
    
    [titleBarButton setContentEdgeInsets:UIEdgeInsetsMake(-10, -moveLeftTitle, 0, 0)];
    self.navigationItem.titleView.bounds = CGRectMake(titleViewBounds.origin.x + moveLeftTitle, Y_TOPBAR_TITLE, titleViewBounds.size.width, titleViewBounds.size.height);
    
}

-(void)topbarTapped:(UIButton *)sender {
    TopbarViewV2 *topbar = [self getTopBarView];
    [sender setEnabled:NO];
    if (topbar.isHidden) {
        [self showTopbarView];
    } else {
        [self hideTopbarView];
    }
    
    [self performSelector:@selector(topbarButtonEnable:) withObject:sender afterDelay:0.3];
}

-(void) topbarButtonEnable:(UIButton *) topbarBtn {
    [topbarBtn setEnabled:YES];
}

-(void)adjustShowForViews {
    //adjust all views when show
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    CGRect ftbv = tbvFindCategory.frame;
    [tbvFindCategory setFrame:CGRectMake(0, f.size.height, 320, ftbv.size.height - f.size.height)];
}

-(void)adjustRemoveForViews {
    //adjust all views when hide
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    CGRect ftbv = tbvFindCategory.frame;
    [tbvFindCategory setFrame:CGRectMake(0, 0, 320, ftbv.size.height + f.size.height)];
}

#pragma mark - === Handle Back Button ===
#pragma mark
-(void)backBarPressed {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)addBackButton {
    //back button navigation
    self.navigationItem.leftBarButtonItem = [MyNavigationBarV2 createBackButtonWithTarget:self selector:@selector(backBarPressed) forControlEvents:UIControlEventTouchUpInside];
}

//EditedBy-DungPhan-2015/03/03
#pragma mark - Private Methods

- (void)createDataSourceForFindMainView {
    AccessRightModel *accessRightModel = [UserManagerV2 sharedUserManager].currentUserAccessRight;
    NSMutableArray *data = [[NSMutableArray alloc] init];
    int valueCount = [accessRightModel isFindRoomNo];
    switch (valueCount) {
        case 0:
            if (accessRightModel.findRoom > 0) {
                FindCellObject *findRoomStatusObj = [[FindCellObject alloc] init];
                findRoomStatusObj.imageName = imgRoom;
                if ([accessRightModel isFindRoomNo] > 0) {
                    findRoomStatusObj.title = [[LanguageManagerV2 sharedLanguageManager] getRoomTitleByFindStatus];
                } else {
                    findRoomStatusObj.title = [[LanguageManagerV2 sharedLanguageManager] getRoomTitle];
                    
                }
                findRoomStatusObj.tag = FindByType_RoomStatus;
                [data addObject:findRoomStatusObj];
            }
            if (accessRightModel.findAttendantPendingCompleted > 0 && [UserManagerV2 isSupervisor]) {
                FindCellObject *findAttendantObj = [[FindCellObject alloc] init];
                findAttendantObj.imageName = imgAttendant;
                findAttendantObj.title = [[LanguageManagerV2 sharedLanguageManager] getAttendant];
                findAttendantObj.tag = FindByType_Attendant;
                [data addObject:findAttendantObj];
            }
            break;
            
        case 1:
            if (accessRightModel.findAttendantPendingCompleted > 0 && [UserManagerV2 isSupervisor]) {
                FindCellObject *findAttendantObj = [[FindCellObject alloc] init];
                findAttendantObj.imageName = imgAttendant;
                findAttendantObj.title = [[LanguageManagerV2 sharedLanguageManager] getAttendant];
                findAttendantObj.tag = FindByType_Attendant;
                [data addObject:findAttendantObj];
            }
            if (accessRightModel.findRoom > 0) {
                FindCellObject *findRoomStatusObj = [[FindCellObject alloc] init];
                findRoomStatusObj.imageName = imgRoom;
                if ([accessRightModel isFindRoomNo] > 0) {
                    findRoomStatusObj.title = [[LanguageManagerV2 sharedLanguageManager] getRoomTitleByFindStatus];
                } else {
                    findRoomStatusObj.title = [[LanguageManagerV2 sharedLanguageManager] getRoomTitle];
                    
                }
                findRoomStatusObj.tag = FindByType_RoomStatus;
                [data addObject:findRoomStatusObj];
            }
            if ([accessRightModel isFindRoomNo] > 0) {
                FindCellObject *findRoomNoObj = [[FindCellObject alloc] init];
                findRoomNoObj.imageName = imgRoomNo;
                findRoomNoObj.title = [[LanguageManagerV2 sharedLanguageManager] getRoomTitleByFindRoomNo];
                findRoomNoObj.tag = FindByType_RoomNo;
                [data addObject:findRoomNoObj];
            }
            break;
        case 2:
            if (accessRightModel.findAttendantPendingCompleted > 0 && [UserManagerV2 isSupervisor]) {
                FindCellObject *findAttendantObj = [[FindCellObject alloc] init];
                findAttendantObj.imageName = imgAttendant;
                findAttendantObj.title = [[LanguageManagerV2 sharedLanguageManager] getAttendant];
                findAttendantObj.tag = FindByType_Attendant;
                [data addObject:findAttendantObj];
            }
            
            if (accessRightModel.findRoom > 0) {
                FindCellObject *findRoomStatusObj = [[FindCellObject alloc] init];
                findRoomStatusObj.imageName = imgRoom;
                if ([accessRightModel isFindRoomNo] > 0) {
                    findRoomStatusObj.title = [[LanguageManagerV2 sharedLanguageManager] getRoomTitleByFindStatus];
                } else {
                    findRoomStatusObj.title = [[LanguageManagerV2 sharedLanguageManager] getRoomTitle];
                    
                }
                findRoomStatusObj.tag = FindByType_RoomStatus;
                [data addObject:findRoomStatusObj];
            }
            
            if ([accessRightModel isFindRoomNo] > 0) {
                FindCellObject *findRoomNoObj = [[FindCellObject alloc] init];
                findRoomNoObj.imageName = imgRoomNo;
                findRoomNoObj.title = [[LanguageManagerV2 sharedLanguageManager] getRoomTitleByFindRoomNo];
                findRoomNoObj.tag = FindByType_RoomNo;
                [data addObject:findRoomNoObj];
            }
        {
            //alway add zone
            FindCellObject *findZoneObj = [[FindCellObject alloc] init];
            findZoneObj.imageName = imgRoomNo;
            findZoneObj.title = @"Zone";
            findZoneObj.tag = FindByType_Zone;
            [data addObject:findZoneObj];
        }
            break;
        case 3:
            if (accessRightModel.findAttendantPendingCompleted > 0 && [UserManagerV2 isSupervisor]) {
                FindCellObject *findAttendantObj = [[FindCellObject alloc] init];
                findAttendantObj.imageName = imgAttendant;
                findAttendantObj.title = [[LanguageManagerV2 sharedLanguageManager] getAttendant];
                findAttendantObj.tag = FindByType_Attendant;
                [data addObject:findAttendantObj];
            }
            
            if (accessRightModel.findRoom > 0) {
                FindCellObject *findRoomStatusObj = [[FindCellObject alloc] init];
                findRoomStatusObj.imageName = imgRoom;
                if ([accessRightModel isFindRoomNo] > 0) {
                    findRoomStatusObj.title = [[LanguageManagerV2 sharedLanguageManager] getRoomTitleByFindStatus];
                } else {
                    findRoomStatusObj.title = [[LanguageManagerV2 sharedLanguageManager] getRoomTitle];
                    
                }
                findRoomStatusObj.tag = FindByType_RoomStatus;
                [data addObject:findRoomStatusObj];
            }
            
            if ([accessRightModel isFindRoomNo] > 0) {
                FindCellObject *findRoomNoObj = [[FindCellObject alloc] init];
                findRoomNoObj.imageName = imgRoomNo;
                findRoomNoObj.title = [[LanguageManagerV2 sharedLanguageManager] getRoomTitleByFindRoomNo];
                findRoomNoObj.tag = FindByType_RoomNo;
                [data addObject:findRoomNoObj];
            }
        {
            //alway add floor
            FindCellObject *findFloorObj = [[FindCellObject alloc] init];
            findFloorObj.imageName = imgRoomNo;
            findFloorObj.title = @"Floor";
            findFloorObj.tag = FindByType_Floor;
            [data addObject:findFloorObj];
        }
            break;
        default:
            break;
    }
//    if (accessRightModel.findAttendantPendingCompleted > 0 && [UserManagerV2 isSupervisor]) {
//        FindCellObject *findAttendantObj = [[FindCellObject alloc] init];
//        findAttendantObj.imageName = imgAttendant;
//        findAttendantObj.title = [[LanguageManagerV2 sharedLanguageManager] getAttendant];
//        findAttendantObj.tag = FindByType_Attendant;
//        [data addObject:findAttendantObj];
//    }
//    
//    if (accessRightModel.findRoom > 0) {
//        FindCellObject *findRoomStatusObj = [[FindCellObject alloc] init];
//        findRoomStatusObj.imageName = imgRoom;
//        if ([accessRightModel isFindRoomNo] > 0) {
//            findRoomStatusObj.title = [[LanguageManagerV2 sharedLanguageManager] getRoomTitleByFindStatus];
//        } else {
//            findRoomStatusObj.title = [[LanguageManagerV2 sharedLanguageManager] getRoomTitle];
//            
//        }
//        findRoomStatusObj.tag = FindByType_RoomStatus;
//        [data addObject:findRoomStatusObj];
//    }
//    
//    if ([accessRightModel isFindRoomNo] > 0) {
//        FindCellObject *findRoomNoObj = [[FindCellObject alloc] init];
//        findRoomNoObj.imageName = imgRoomNo;
//        findRoomNoObj.title = [[LanguageManagerV2 sharedLanguageManager] getRoomTitleByFindRoomNo];
//        findRoomNoObj.tag = FindByType_RoomNo;
//        [data addObject:findRoomNoObj];
//    }
//    //alway add zone
//    FindCellObject *findZoneObj = [[FindCellObject alloc] init];
//    findZoneObj.imageName = imgRoomNo;
//    findZoneObj.title = @"Zone";
//    findZoneObj.tag = FindByType_Zone;
//    [data addObject:findZoneObj];
//    //alway add floor
//    FindCellObject *findFloorObj = [[FindCellObject alloc] init];
//    findFloorObj.imageName = imgRoomNo;
//    findFloorObj.title = @"Floor";
//    findFloorObj.tag = FindByType_Floor;
//    [data addObject:findFloorObj];
    
    self.dataSource = [NSArray arrayWithArray:data];
    [tbvFindCategory reloadData];
}//End edit

@end
