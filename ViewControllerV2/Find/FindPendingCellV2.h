//
//  FindPendingCellV2.h
//  mHouseKeeping
//
//  Created by Nghia Truong on 2/20/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FindPendingCellV2 : UITableViewCell {
    UILabel *lblRoomNo;
    UILabel *lblGuestName;
    UILabel *lblRoomStatus;
    UILabel *lblVIP;
    UIImageView *imgCleaningStatus;
    UIButton *btnDetailArrow;
    
    UIImageView *imgFirstCleaning;
    UIImageView *imgSecondCleaning;
    UILabel *lblServiceLaterTime;
    UILabel *lblDueOutRoom;
    UILabel *lblArrivalTime;
    UIImageView *imgRushOrQueueRoom;
    UIImageView *imgProfileNotes; // CFG [20160928/CRF-00000827]
}

@property (nonatomic, strong) IBOutlet UILabel *lblRoomNo;
@property (nonatomic, strong) IBOutlet UILabel *lblGuestName;
@property (nonatomic, strong) IBOutlet UILabel *lblRoomStatus;
@property (nonatomic, strong) IBOutlet UILabel *lblVIP;
@property (nonatomic, strong) IBOutlet UIImageView *imgCleaningStatus;
@property (nonatomic, strong) IBOutlet UIButton *btnDetailArrow;
@property (nonatomic, strong) IBOutlet UIImageView *imgFirstCleaning;
@property (nonatomic, strong) IBOutlet UIImageView *imgSecondCleaning;
@property (nonatomic, strong) IBOutlet UILabel *lblServiceLaterTime;
@property (nonatomic, strong) IBOutlet UILabel *lblDueOutRoom;
@property (nonatomic, strong) IBOutlet UILabel *lblArrivalTime;
@property (nonatomic, strong) IBOutlet UIImageView *imgRushOrQueueRoom;
@property (nonatomic, strong) IBOutlet UIImageView *imgProfileNotes; // CFG [20160928/CRF-00000827]

@end
