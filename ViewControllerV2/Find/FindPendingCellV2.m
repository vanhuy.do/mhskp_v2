//
//  FindPendingCellV2.m
//  mHouseKeeping
//
//  Created by Nghia Truong on 2/20/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "FindPendingCellV2.h"

@implementation FindPendingCellV2

@synthesize lblVIP, lblRoomNo, lblRoomStatus, lblGuestName, btnDetailArrow, imgCleaningStatus;
@synthesize imgFirstCleaning, imgSecondCleaning, lblServiceLaterTime, lblArrivalTime, lblDueOutRoom, imgRushOrQueueRoom;
@synthesize imgProfileNotes; // CFG [20160928/CRF-00000827]

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:
    (NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
    }
    
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)dealloc {
}

@end
