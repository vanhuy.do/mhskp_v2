//
//  FindPendingViewV2.h
//  mHouseKeeping
//
//  Created by Nghia Truong on 2/20/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FindManagerV2.h"
#import "AccessRight.h"

@interface FindPendingViewV2 : UIViewController <UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate> {
    UILabel *lblNameAndType;
    UILabel *lblRoomNo;
    UILabel *lblRoomStatus;
    UILabel *lblVIP;
    UILabel *lblCleaningStatus;
    UILabel *lblNumOfRoom;
    UITableView *tbvContent;
    UITableView *tbvNoResult;
    
    NSMutableArray *listDisplayData;
    NSMutableArray *listTempData;
    NSString *attendantName;
    NSString *typeView;
        
    UIButton *btnBack;
    AccessRight *roomAssignment;
    IBOutlet UIImageView *imgHeader;
    BOOL isDueRoom;
    BOOL isHaveReasonRemark;
}
    
@property (nonatomic, strong) IBOutlet UILabel *lblNameAndType;
@property (nonatomic, strong) IBOutlet UILabel *lblRoomNo;
@property (nonatomic, strong) IBOutlet UILabel *lblRoomStatus;
@property (nonatomic, strong) IBOutlet UILabel *lblVIP;
@property (nonatomic, strong) IBOutlet UILabel *lblCleaningStatus;
@property (nonatomic, strong) IBOutlet UILabel *lblNumOfRoom;
@property (nonatomic, strong) IBOutlet UITableView *tbvContent;
@property (nonatomic, strong) IBOutlet UITableView *tbvNoResult;
@property (nonatomic, strong) NSMutableArray *listDisplayData;
@property (nonatomic, strong) NSMutableArray *listTempData;
@property (nonatomic, strong) NSString *attendantName;
@property (nonatomic, strong) NSString *typeView;
@property (strong, nonatomic) IBOutlet UIView *controlsView;

//Catch roomdata selected in tableview to update in this view
//Null if not selected yet
@property (strong, nonatomic) NSDictionary *selectedPendingRoom;
    
-(void) loadData;
-(void) setCaptionsView;

@end
