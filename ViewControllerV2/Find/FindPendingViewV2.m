//
//  FindPendingViewV2.m
//  mHouseKeeping
//
//  Created by Nghia Truong on 2/20/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "FindPendingViewV2.h"
#import "FindPendingCellV2.h"
#import "FindByRoomCellV2.h"
#import "RoomManagerV2.h"
#import "RoomAssignmentInfoViewController.h"
//#import "HomeViewV2.h"
#import "MyNavigationBarV2.h"
#import "LogFileManager.h"
#import "DeviceManager.h"
#import "ehkDefinesV2.h"
#import "UIImage+Tint.h"

#define kRoomNo             @"RoomNo"
#define kGuestName          @"GuestName"
#define kRMStatus           @"RMStatus"
#define kCleaningStatus     @"CleaningStatus"
#define kVIP                @"VIP"
#define kRoomAssignmentID   @"RoomAssignmentID"
#define kDurationUsed       @"DurationUsed"

#define backgroundImage     @"bg.png"

#define heightRoomBlockingCell  133.0f //105.0f
#define heightTbvContent    60.0f
#define heightTbvNoResult   280.0f

#define colorWhite          255/255.0
#define colorAlpha          1.0
#define colorBlueR          105/255.0
#define colorBlueG          218/255.0
#define colorBlueB          222/255.0

#define tagNoRoom           12

#define font                @"Arial-BoldMT"

#define sizeNoRoom          22.0f
#define sizeSmall           10.0f
#define sizeNormal          17.0f

#define nameVD              @"VD"
#define nameOD              @"OD"

@interface FindPendingViewV2(PrivateMethods)

-(void) btnDetailArrowPressed:(UIButton *) sender;

-(void) loadTopbarView;
-(void) showTopbarView;
-(void) hideTopbarView;
-(void) setHiddenTopbarView;
-(void) adjustShowForViews;
-(void) adjustRemoveForViews;
-(void) addButtonHandleShowHideTopbar;
-(void) topbarTapped:(UIButton *) sender;
-(TopbarViewV2 *) getTopBarView;
-(void) btnBackPressed;
-(void) addBackButton;
-(void) updateSequenceForFromIndex:(NSInteger)fromIndex toToIndex:(NSInteger) toIndex;

@end

@implementation FindPendingViewV2
@synthesize controlsView;
@synthesize selectedPendingRoom;

@synthesize lblNameAndType, lblVIP, lblRoomNo, lblRoomStatus, lblCleaningStatus, tbvContent, tbvNoResult, listDisplayData, listTempData, attendantName, typeView, lblNumOfRoom;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

-(int)getRoomIndicatorGuestByArrivalDate:(NSString*)arrivalDate departureDate:(NSString*)departureDate
{
    NSString *arrivalDateCompare = nil;
    NSString *departureDateCompare = nil;
    NSString *nowDate = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd"];
    
    if(arrivalDate.length > 0) {
        arrivalDateCompare = [ehkConvert DateToStringWithString:arrivalDate fromFormat:@"yyyy-MM-dd HH:mm:ss" toFormat:@"yyyy-MM-dd"];
        
    }
    
    if(departureDate.length > 0) {
        departureDateCompare = [ehkConvert DateToStringWithString:departureDate fromFormat:@"yyyy-MM-dd HH:mm:ss" toFormat:@"yyyy-MM-dd"];
    }
    
    //Checking Back To Back
    if(arrivalDateCompare.length > 0 && departureDateCompare.length > 0) {
        if([arrivalDateCompare caseInsensitiveCompare:nowDate] == NSOrderedSame && [departureDateCompare caseInsensitiveCompare:nowDate] == NSOrderedSame){
            return RoomIndicatorGuestBackToBack;
        }
    }
    
    //Checking Due In Guest
    if(arrivalDateCompare.length > 0) {
        if([arrivalDateCompare caseInsensitiveCompare:nowDate] == NSOrderedSame) {
            return RoomIndicatorGuestDueIn;
        }
    }
    
    //Checking Due Out Guest
    if(departureDateCompare.length > 0){
        if([departureDateCompare caseInsensitiveCompare:nowDate] == NSOrderedSame) {
            return RoomIndicatorGuestDueOut;
        }
    }
    
    return 0;
}


#pragma mark - View lifecycle
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //set selection find in bottom bar
    [[HomeViewV2 shareHomeView] setSelectedButton:tagOfFindButton];
    [self setCaptionsView];
    
    //Catch roomdata selected in tableview to update in this view
    //Null if not selected yet
    selectedPendingRoom = nil;
    
    [tbvContent reloadData];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [tbvContent setEditing:NO];
}

-(void) loadAccessRights
{
    AccessRightModel *currentUserAccessRight = [[UserManagerV2 sharedUserManager] currentUserAccessRight];
    roomAssignment = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.roomAssignment];
    
}
- (void)viewDidLoad {
    [self loadAccessRights];
    [tbvContent setBackgroundView:nil];
    [tbvContent setBackgroundColor:[UIColor clearColor]];
    [tbvContent setOpaque:YES];
    
    [tbvNoResult setBackgroundView:nil];
    [tbvNoResult setBackgroundColor:[UIColor clearColor]];
    [tbvNoResult setOpaque:YES];
    [tbvNoResult setHidden:YES];
    
    // Load Data
    //    [self loadData];
    
    [super viewDidLoad];
    //Resize content views of current VC to fit with Navigation bar and tabbar
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    // Do any additional setup after loading the view from its nib.
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        //Clear redundant view for header, footter
        CGRect frameHeaderFooter = tbvContent.tableHeaderView.frame;
        frameHeaderFooter.size.height = 0.1f;
        UIView *headerView = [[UIView alloc] initWithFrame:frameHeaderFooter];
        [tbvContent setTableHeaderView:headerView];
        
        frameHeaderFooter = tbvContent.tableFooterView.frame;
        frameHeaderFooter.size.height = 0.1f;
        UIView *footerView = [[UIView alloc] initWithFrame:frameHeaderFooter];
        [tbvContent setTableFooterView:footerView];
        
        [self loadFlatResource];
    }
    
    [self performSelector:@selector(loadTopbarView)];
    
    //add back button
    [self addBackButton];
    
    //add long gesture in uitableview
    UILongPressGestureRecognizer *longpressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(responseLongPressButton:)];
    longpressGesture.delegate = self;
    longpressGesture.minimumPressDuration = 2;
    [tbvContent addGestureRecognizer:longpressGesture];
}

- (void)viewDidUnload {
    [self setLblNameAndType:nil];
    [self setLblVIP:nil];
    [self setLblRoomNo:nil];
    [self setLblCleaningStatus:nil];
    [self setLblRoomStatus:nil];
    [self setTbvContent:nil];
    [self setTbvNoResult:nil];
    [self setAttendantName:nil];
    [self setTypeView:nil];
    [self setListDisplayData:nil];
    [self setListTempData:nil];
    
    [self setControlsView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc {
    //    [lblNameAndType release];
    //    [lblVIP release];
    //    [lblRoomNo release];
    //    [lblCleaningStatus release];
    //    [lblRoomStatus release];
    //    [tbvContent release];
    //    [tbvNoResult release];
    //    [listDisplayData release];
    //    [listTempData release];
    //    [guestName release];
    //    [typeView release];
    //
    //    [super dealloc];
}

#pragma mark - UITableView Delegate Methods
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(listDisplayData.count > 0) {
        NSDictionary *rowData = [[self.listDisplayData objectAtIndex:indexPath.row] objectForKey:keyRoomData];
        NSString *roomNo = (NSString *)[rowData objectForKey:kRoomNo];
        if(roomNo.length > 0) {
            RoomBlocking *curRoomBlocking = [[RoomManagerV2 sharedRoomManager] loadRoomBlockingByUserId:[UserManagerV2 sharedUserManager].currentUser.userId roomNumber:roomNo];
            NSUInteger row = [indexPath row];
            NSDictionary *rowData = [(NSDictionary*)[self.listDisplayData objectAtIndex:row] objectForKey:keyRoomData];
            NSString *guestArrivalTime = (NSString *)[rowData objectForKey:kRaGuestArrivalTime];
            NSString *guestDepartureTime = (NSString *)[rowData objectForKey:kRaGuestDepartureTime];
            NSString *guestName = [rowData objectForKey:kGuestName];
            int roomIndicatorGuest = [self getRoomIndicatorGuestByArrivalDate:guestArrivalTime departureDate:guestDepartureTime];
            isDueRoom = (roomIndicatorGuest == RoomIndicatorGuestDueIn) || (roomIndicatorGuest == RoomIndicatorGuestDueOut) || (roomIndicatorGuest == RoomIndicatorGuestBackToBack);
            [LogFileManager logDebugMode:@"isdue = %@", isDueRoom ? @"True" : @"False"];
            
            isHaveReasonRemark = (curRoomBlocking.roomblocking_reason.length > 0) || (curRoomBlocking.roomblocking_remark_physical_check.length > 0 || curRoomBlocking.roomblocking_oosdurations.length > 0);
            if(curRoomBlocking != nil
               && curRoomBlocking.roomblocking_is_blocked > RoomBlockingStatus_Normal
               && isHaveReasonRemark
               && (isDueRoom || guestName.length > 0)) {
                
                //Extend the height when room was blocked
                return heightRoomBlockingCell;
            }
            else if (curRoomBlocking != nil
                     && curRoomBlocking.roomblocking_is_blocked > RoomBlockingStatus_Normal
                     && isHaveReasonRemark
                     && !isDueRoom && guestName.length == 0)
            {
                return 107.0f;
            }
            
            return 65;
        }
    }
    
    return heightTbvNoResult;
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(tableView == tbvContent)
        return [listDisplayData count];
    
    return 1;
}

//-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
//    cell.backgroundColor = [UIColor colorWithRed:colorWhite green:colorWhite blue:colorWhite alpha:colorAlpha];
//}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentify = @"cellIdentify";
    static NSString *cellNoIndentify = @"cellNoIndentify";
    
    UITableViewCell *cell = nil;
    
    if(tableView == tbvNoResult) {
        cell = [tableView dequeueReusableCellWithIdentifier:cellNoIndentify];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellNoIndentify];//autorelease
            
            UILabel *titleNoResult = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, heightTbvNoResult)];
            [titleNoResult setTextColor:[UIColor grayColor]];
            [titleNoResult setBackgroundColor:[UIColor clearColor]];
            [titleNoResult setTextAlignment:NSTextAlignmentCenter];
            [titleNoResult setTag:tagNoRoom];
            [titleNoResult setFont:[UIFont fontWithName:font size:sizeNoRoom]];
            
            [cell.contentView addSubview:titleNoResult];
        }
        
        UILabel *lblNoResult = (UILabel *)[cell viewWithTag:tagNoRoom];
        [lblNoResult setText:[[LanguageManagerV2 sharedLanguageManager] getNoResultsFound]];
        
        return cell;
    }
    
    if(tableView == tbvContent) {
        FindByRoomCellV2 *cell = nil;
        cell = (FindByRoomCellV2 *)[tableView dequeueReusableCellWithIdentifier:cellIdentify];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        if (cell == nil) {
            NSArray *arrayCell = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([FindByRoomCellV2 class]) owner:self options:nil];
            cell = [arrayCell objectAtIndex:0];
            [cell.lblRoomStatus setFont:[UIFont fontWithName:font size:sizeNormal]];
            [cell.lblRoomNo setFont:[UIFont fontWithName:font size:sizeNormal]];
            [cell.lblVIP setFont:[UIFont fontWithName:font size:sizeNormal]];
        }
        
        NSDictionary *rowData = [[self.listDisplayData objectAtIndex:indexPath.row] objectForKey:keyRoomData];
        
        UILabel *lblRoomNoCell = (UILabel *)[cell viewWithTag:tagRoomNo];
        UILabel *lblRoomStatusCell = (UILabel *)[cell viewWithTag:tagRMStatus];
        UILabel *lblVipRoom = (UILabel *)[cell viewWithTag:tagVIP];
        UIImageView *imgFirstIconCleaningStatus = (UIImageView *)[cell viewWithTag:tagFirstIconCleaningStatus];
        UIImageView *imgSecondIconCleaningStatus = (UIImageView *)[cell viewWithTag:tagSecondIconCleaningStatus];
        UILabel *lblServiceLaterTime = (UILabel *)[cell viewWithTag:tagTimeServiceLater];
        UIImageView *imgRushOrQueueRoom = (UIImageView *)[cell viewWithTag:tagRushOrQueueRoom];
        UILabel *lblDueOutRoom = (UILabel *)[cell viewWithTag:tagDueOutRoom];
        UILabel *lblDepartureTime = (UILabel *)[cell viewWithTag:tagDepartureTime];
        UILabel *lblDueInRoom = (UILabel *)[cell viewWithTag:tagDueInRoom];
        UILabel *lblArrivalTime = (UILabel *)[cell viewWithTag:tagArrivalTime];
        UILabel *lblGuestName = (UILabel *)[cell viewWithTag:tagGuestName];
        UILabel *lblRAName = (UILabel *)[cell viewWithTag:tagRAName];
        UILabel *lblRoomType = (UILabel *)[cell viewWithTag:tagRoomType];
        UIImageView *imgProfileNotes = (UIImageView *)[cell viewWithTag:tagProfileNotes]; // CFG [20160928/CRF-00000827]
        UIImageView *imgStayOver = (UIImageView *)[cell viewWithTag:tagStayOver]; // CRF [20161106/CRF-00001293]
        NSString *roomTypeValue = [rowData objectForKey:kRoomType];
        NSString *roomNo = (NSString *)[rowData objectForKey:kRoomNo];
        NSString *guestName = [rowData objectForKey:kGuestName];
        NSString *vip = (NSString *)[rowData objectForKey:kVIP];
        NSInteger cleaningStatus = [(NSString *)[rowData objectForKey:kCleaningStatusID] integerValue];
        NSString *roomStatus = (NSString *)[rowData objectForKey:kRMStatus];
        NSString *guestArrivalTime = (NSString *)[rowData objectForKey:kRaGuestArrivalTime];
        NSString *guestDepartureTime = (NSString *)[rowData objectForKey:kRaGuestDepartureTime];
        NSInteger kindOfRoom = [(NSString *)[rowData objectForKey:kRaKindOfRoom] integerValue];
        NSInteger isMockRoom = [(NSString *)[rowData objectForKey:kRaIsMockRoom] integerValue];
        NSInteger isReassignedRoom = [(NSString *)[rowData objectForKey:kRaIsReassignedRoom] integerValue];
        NSData *cleaningStatusIcon = (NSData *)[rowData objectForKey:kCleaningStatus];
        NSString *roomAssignmentTime = (NSString *)[rowData objectForKey:kRoomAssignmentTime];
        //20150108 - Show icon previous day
        UIImageView *imgPreviousDate = (UIImageView *)[cell viewWithTag:tagPreviousDate];
        int roomStatusID = [[rowData objectForKey:kRoomStatusId] intValue];
        if (roomStatusID == ENUM_ROOM_STATUS_OC || roomStatusID == ENUM_ROOM_STATUS_VC) {
            if ([[rowData objectForKey:kLastCleaningDate] length] > 0) {
                NSDateFormatter *dFormat = [[NSDateFormatter alloc] init];
                [dFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                
                if([[rowData objectForKey:kLastCleaningDate] isEqualToString:@"1900-01-01 00:00:00"]) {
                    imgPreviousDate.hidden = YES;
                } else if ([DateTimeUtility compareDatetimeValue:[rowData objectForKey:kLastCleaningDate]] == DateComparison_LessThanCurrent) {
                    imgPreviousDate.hidden = NO;
                    [imgPreviousDate setFrame:CGRectMake(self.view.bounds.size.width - 30,
                                                         12,
                                                         imgPreviousDate.frame.size.width,
                                                         imgPreviousDate.frame.size.height)];
                } else {
                    imgPreviousDate.hidden = YES;
                }
            } else {
                imgPreviousDate.hidden = NO;
                [imgPreviousDate setFrame:CGRectMake(self.view.bounds.size.width - 30,
                                                     12,
                                                     imgPreviousDate.frame.size.width,
                                                     imgPreviousDate.frame.size.height)];
            }
        } else if([roomStatus isEqualToString:ROOM_STATUS_VD] || [roomStatus isEqualToString:ROOM_STATUS_OD]){
            imgPreviousDate.hidden = YES;
        } else {
            imgPreviousDate.hidden = YES;
        }
        [lblRoomNoCell setText:roomNo];
        [lblRoomStatusCell setText:roomStatus];
        [lblVipRoom setText:(vip == nil || [vip isEqualToString:@""] || [vip isEqualToString:@"0"])?@"-":vip];
        [lblDueOutRoom setText:@"[D/O]"];
        [lblArrivalTime setText:guestArrivalTime];
        [lblGuestName setText:(guestName == nil || [guestName isEqualToString:@""])?@"-":guestName];
        
        // CFG [20160913/CRF-00001439] - Changed to binary AND operation.
        if ((kindOfRoom & ENUM_KIND_OF_ROOM_RUSH) == ENUM_KIND_OF_ROOM_RUSH) {
            [imgRushOrQueueRoom setHidden:NO];
            [imgRushOrQueueRoom setImage:[UIImage imageBeforeiOS7:imgRushRoom equaliOS7:imgRushRoomFlat]];
        }
        else if ((kindOfRoom & ENUM_KIND_OF_ROOM_QUEUE) == ENUM_KIND_OF_ROOM_QUEUE) {
            [imgRushOrQueueRoom setHidden:NO];
            [imgRushOrQueueRoom setImage:[UIImage imageBeforeiOS7:imgQueueRoom equaliOS7:imgQueueRoomFlat]];
        }
        else {
//            if([LogFileManager isLogConsole])
//            {
//                NSLog(@"OUT OF VALUE RANGE");
//            }
            [imgRushOrQueueRoom setHidden:YES];
        }
        // CFG [20160913/CRF-00001439] - End.
        
        // CFG [20160928/CRF-00000827] - Show notes icon.
        if ((kindOfRoom & ENUM_KIND_OF_ROOM_PROFILE_NOTES_EXISTS) == ENUM_KIND_OF_ROOM_PROFILE_NOTES_EXISTS) {
            [imgProfileNotes setHidden:NO];
        }
        else {
            [imgProfileNotes setHidden:YES];
        }
        // CFG [20160928/CRF-00000827] - End.
        
        // CRF [20161106/CRF-00001293] - Show notes icon.
        if ((kindOfRoom & ENUM_KIND_OF_ROOM_STAY_OVER_EXISTS) == ENUM_KIND_OF_ROOM_STAY_OVER_EXISTS) {
            [imgStayOver setHidden:NO];
            if(imgProfileNotes.isHidden == NO) {
                [imgStayOver setFrame:CGRectMake(imgProfileNotes.frame.origin.x - imgProfileNotes.frame.size.width, imgProfileNotes.frame.origin.y, imgProfileNotes.frame.size.width, imgProfileNotes.frame.size.height)];
            } else {
                [imgStayOver setFrame:imgProfileNotes.frame];
            }
        }
        else {
            [imgStayOver setHidden:YES];
        }
        // CRF [20161106/CRF-00001293] - End.
        int roomIndicatorGuest = [self getRoomIndicatorGuestByArrivalDate:guestArrivalTime departureDate:guestDepartureTime];
        isDueRoom = (roomIndicatorGuest == RoomIndicatorGuestDueIn) || (roomIndicatorGuest == RoomIndicatorGuestDueOut) || (roomIndicatorGuest == RoomIndicatorGuestBackToBack);
        
        if(roomIndicatorGuest == RoomIndicatorGuestDueIn) { //light yellow green color
            [cell setBackgroundColor:[UIColor colorWithRed:253/255.0f green:255/255.0f blue:206/255.0f alpha:1]];
            
            [lblDueInRoom setHidden:YES];
            [lblArrivalTime setHidden:YES];
            
            //Warning! Temporary use label Due Out and label departure time for Arrival time
            //[lblDueOutRoom setFrame:lblDueOutFrame];//Hao Tran - Remove for uneccessary frame for cell in HomeViewV2
            
            //Change frames for Due Out Infor
            //Decreasing width of Guest Infor
            if(lblDepartureTime.isHidden /*&& lblDueOutRoom.isHidden*/) {
                CGRect frlblDueOut = lblDueOutRoom.frame;
                CGRect frlblDepartureTime = lblDepartureTime.frame;
                CGRect frlblGuestName = lblGuestName.frame;
                frlblGuestName.size.width -= (frlblDueOut.size.width + frlblDepartureTime.size.width + 2);
                frlblGuestName.origin.x += (frlblDueOut.size.width + frlblDepartureTime.size.width + 2);
                [lblGuestName setFrame:frlblGuestName];
                [lblDueOutRoom setHidden:NO];
                [lblDepartureTime setHidden:NO];
            }
            
            [lblDueOutRoom setText:@"[D/I]"];
            [lblDepartureTime setText:[ehkConvert DateToStringWithString:guestArrivalTime fromFormat:@"yyyy-MM-dd HH:mm:ss" toFormat:@"HH:mm"]];
            //[lblGuestName setFrame:lblGuestNameFrameInCase3];
            [lblGuestName setHidden:YES];
            
        }
        else if(roomIndicatorGuest == RoomIndicatorGuestDueOut) { //light green color
            [cell setBackgroundColor:[UIColor colorWithRed:172/255.0f green:255/255.0f blue:203/255.0f alpha:1]];
            
            if(guestName.length > 0) {
                [lblGuestName setHidden:NO];
                [lblGuestName setText:guestName];
            } else {
                //No need to check guest for displaying green background - matching with Android
                //[cell setBackgroundColor:[UIColor whiteColor]];
                [lblGuestName setHidden:YES];
            }
            
            [lblDueInRoom setHidden:YES];
            [lblArrivalTime setHidden:YES];
            
            //[lblDueOutRoom setFrame:lblDueOutFrame];//Hao Tran - Remove for uneccessary frame for cell in HomeViewV2
            
            //Change frames for Due Out Infor
            //Decreasing width of Guest Infor
            if(lblDepartureTime.isHidden /*&& lblDueOutRoom.isHidden*/) {
                CGRect frlblDueOut = lblDueOutRoom.frame;
                CGRect frlblDepartureTime = lblDepartureTime.frame;
                CGRect frlblGuestName = lblGuestName.frame;
                frlblGuestName.size.width -= (frlblDueOut.size.width + frlblDepartureTime.size.width + 2);
                frlblGuestName.origin.x += (frlblDueOut.size.width + frlblDepartureTime.size.width + 2);
                [lblGuestName setFrame:frlblGuestName];
                [lblDueOutRoom setHidden:NO];
                [lblDepartureTime setHidden:NO];
            }
            
            [lblDueOutRoom setText:@"[D/O]"];
            [lblDepartureTime setText:[ehkConvert DateToStringWithString:guestDepartureTime fromFormat:@"yyyy-MM-dd HH:mm:ss" toFormat:@"HH:mm"]];
            //[lblGuestName setFrame:lblGuestNameFrameInCase3];
            
        }
        else if(roomIndicatorGuest == RoomIndicatorGuestBackToBack) { //light green color
            [cell setBackgroundColor:[UIColor colorWithRed:172/255.0f green:255/255.0f blue:203/255.0f alpha:1]];
            
            //Change frames for Due Out Infor
            //Decreasing width of Guest Infor
            if(lblDepartureTime.isHidden /*&& lblDueOutRoom.isHidden*/) {
                CGRect frlblDueOut = lblDueOutRoom.frame;
                CGRect frlblDepartureTime = lblDepartureTime.frame;
                CGRect frlblGuestName = lblGuestName.frame;
                frlblGuestName.size.width -= (frlblDueOut.size.width + frlblDepartureTime.size.width + 2);
                frlblGuestName.origin.x += (frlblDueOut.size.width + frlblDepartureTime.size.width + 2);
                [lblGuestName setFrame:frlblGuestName];
                [lblDueOutRoom setHidden:NO];
                [lblDepartureTime setHidden:NO];
            }
            
            [lblDueOutRoom setText:@"[D/O]"];
            [lblDepartureTime setText:[ehkConvert DateToStringWithString:guestDepartureTime fromFormat:@"yyyy-MM-dd HH:mm:ss" toFormat:@"HH:mm"]];
            [lblDueInRoom setHidden:NO];
            [lblDueInRoom setText:@"[D/I]"];
            [lblArrivalTime setHidden:NO];
            [lblArrivalTime setText:[ehkConvert DateToStringWithString:guestArrivalTime fromFormat:@"yyyy-MM-dd HH:mm:ss" toFormat:@"HH:mm"]];
            
            //[lblGuestName setFrame:lblGuestNameFrameInCase3];
            if(guestName.length > 0) {
                [lblGuestName setHidden:NO];
                [lblGuestName setText:guestName];
            } else {
                [lblGuestName setHidden:YES];
            }
            
        }
        else { //Normal
            [cell setBackgroundColor:[UIColor whiteColor]];
            
            //Change frames for Due Out Infor
            //Increasing width of Guest Infor
            if(!lblDepartureTime.isHidden) {
                CGRect frlblDueOut = lblDueOutRoom.frame;
                CGRect frlblDepartureTime = lblDepartureTime.frame;
                CGRect frlblGuestName = lblGuestName.frame;
                frlblGuestName.size.width += (frlblDueOut.size.width + frlblDepartureTime.size.width + 2);
                frlblGuestName.origin.x -= (frlblDueOut.size.width + frlblDepartureTime.size.width + 2);
                [lblGuestName setFrame:frlblGuestName];
                [lblDepartureTime setHidden:YES];
                [lblDueOutRoom setHidden:YES];
            }
            
            [lblDueInRoom setHidden:YES];
            [lblArrivalTime setHidden:YES];
            
            if(guestName.length > 0) {
                [lblGuestName setHidden:NO];
                [lblGuestName setText:guestName];
            } else {
                [lblGuestName setHidden:YES];
                [lblDueOutRoom setText:@"-"];
            }
        }
        
        [cell setAccessoryType:UITableViewCellAccessoryNone];
        
        //CRF-0000682: blocking room sign, remark and reason
        RoomBlocking *curRoomBlocking = [[RoomManagerV2 sharedRoomManager] loadRoomBlockingByUserId:[UserManagerV2 sharedUserManager].currentUser.userId roomNumber:roomNo];
        UIImageView *blockRoomStatus = (UIImageView*)[cell.contentView viewWithTag:tagBlockingImage];
        UILabel *lblBlockReason = (UILabel*)[cell.contentView viewWithTag:tagBlockingReason];
        UILabel *lblBlockRemark = (UILabel*)[cell.contentView viewWithTag:tagBlockingRemark];
        UILabel *lblAddReason = (UILabel*)[cell.contentView viewWithTag:tagAddition];
        UILabel *lblBlockDuration = (UILabel*)[cell.contentView viewWithTag:tagOOSDuration];
        
        isHaveReasonRemark = (curRoomBlocking.roomblocking_reason.length > 0) || (curRoomBlocking.roomblocking_remark_physical_check.length > 0 || curRoomBlocking.roomblocking_oosdurations.length > 0);
        if(curRoomBlocking.roomblocking_is_blocked > 0 && !isDueRoom  && guestName.length == 0 )
        {
            if(curRoomBlocking.roomblocking_is_blocked == RoomBlockingStatus_Release) {
                [blockRoomStatus setImage:[UIImage imageNamed:imgReleaseRoom]];
            } else {
                [blockRoomStatus setImage:[UIImage imageNamed:imgBlockRoom]];
            }
            
            [blockRoomStatus setHidden:NO];
            
            if(isHaveReasonRemark){
                NSString *reason = [NSString stringWithFormat:@"%@: %@", [L_reason currentKeyToLanguage], curRoomBlocking.roomblocking_reason];
                NSString *remark = [NSString stringWithFormat:@"%@: %@",[L_remark_title currentKeyToLanguage], curRoomBlocking.roomblocking_remark_physical_check];
                NSString *blockingDuration =  [NSString stringWithFormat:@"%@: %@",[L_oos_duration currentKeyToLanguage], curRoomBlocking.roomblocking_oosdurations];
                
                [lblAddReason setHidden:NO];
                [lblBlockReason setHidden:NO];
                [lblBlockRemark setHidden:YES];
                [lblBlockDuration setHidden:YES];
                [lblAddReason setText:reason];
                [lblBlockReason setText:remark];
                [lblBlockRemark setText:blockingDuration];
            }
            else{
                [lblAddReason setHidden:YES];
                [lblBlockReason setHidden:YES];
                [lblBlockRemark setHidden:YES];
                [lblBlockDuration setHidden:YES];
            }
            [lblGuestName setHidden:YES];
        }
        else if (curRoomBlocking.roomblocking_is_blocked > 0 && (isDueRoom || guestName.length > 0)){
            
            if(curRoomBlocking.roomblocking_is_blocked == RoomBlockingStatus_Release) {
                [blockRoomStatus setImage:[UIImage imageNamed:imgReleaseRoom]];
            } else {
                [blockRoomStatus setImage:[UIImage imageNamed:imgBlockRoom]];
            }
            
            [blockRoomStatus setHidden:NO];
            
            if(isHaveReasonRemark){
                NSString *reason = [NSString stringWithFormat:@"%@: %@", [L_reason currentKeyToLanguage], curRoomBlocking.roomblocking_reason];
                NSString *remark = [NSString stringWithFormat:@"%@: %@",[L_remark_title currentKeyToLanguage], curRoomBlocking.roomblocking_remark_physical_check];
                NSString *blockingDuration =  [NSString stringWithFormat:@"%@: %@",[L_oos_duration currentKeyToLanguage], curRoomBlocking.roomblocking_oosdurations];
                
                [lblAddReason setHidden:YES];
                [lblBlockReason setHidden:NO];
                [lblBlockRemark setHidden:NO];
                [lblBlockDuration setHidden:NO];
                [lblBlockReason setText:reason];
                [lblBlockRemark setText:remark];
                [lblBlockDuration setText:blockingDuration];
            }
            else{
                [lblAddReason setHidden:YES];
                [lblBlockReason setHidden:YES];
                [lblBlockRemark setHidden:YES];
                [lblBlockDuration setHidden:YES];
            }
            [lblGuestName setHidden:NO];
        }
        else {
            [blockRoomStatus setHidden:YES];
            [lblBlockRemark setHidden:YES];
            [lblBlockReason setHidden:YES];
            [lblAddReason setHidden:YES];
            [lblBlockDuration setHidden:YES];
            [lblGuestName setHidden:NO];
        }
        
        [imgFirstIconCleaningStatus setHidden:NO];
        [imgSecondIconCleaningStatus setHidden:NO];
        [lblServiceLaterTime setHidden:YES];
        [lblRAName setHidden:YES];
        
        if (isMockRoom == 1) {
            [imgFirstIconCleaningStatus setImage:[UIImage imageBeforeiOS7:imgComplete equaliOS7:imgCompleteFlat]];
            [imgSecondIconCleaningStatus setImage:[UIImage imageWithData:cleaningStatusIcon]];
        }
        else if (isReassignedRoom == 1){
            [imgFirstIconCleaningStatus setImage:[UIImage imageBeforeiOS7:imgPending equaliOS7:imgPendingFlat]];
            //            if (cleaningStatus == ENUM_CLEANING_STATUS_PENDING_SECOND_TIME) {
            //                [imgSecondIconCleaningStatus setImage:[UIImage imageNamed:@"icon_pending.png"]];
            //            }
            //            else {
            //                [imgSecondIconCleaningStatus setImage:[UIImage imageWithData:cleaningStatusIcon]];
            //            }
            [imgSecondIconCleaningStatus setImage:[UIImage imageWithData:cleaningStatusIcon]];
        }
        else {
            [imgSecondIconCleaningStatus setHidden:YES];
            [imgFirstIconCleaningStatus setImage:[UIImage imageWithData:cleaningStatusIcon]];
            if (cleaningStatus == ENUM_CLEANING_STATUS_SERVICE_LATER) {
                [lblServiceLaterTime setHidden:NO];
                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                [dateFormat setLocale:[NSLocale currentLocale]];
                [dateFormat setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
                NSDate *assignedDate = [dateFormat dateFromString:roomAssignmentTime];
                [dateFormat setDateFormat:@"HH:mm"];
                [lblServiceLaterTime setText:[dateFormat stringFromDate:assignedDate]];
            }
            else {
                [lblServiceLaterTime setHidden:YES];
            }
        }
        
        [lblRoomType setText:roomTypeValue];
        [cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
        
        return cell;
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //    FindPendingCellV2 *cell = (FindPendingCellV2 *)[tableView cellForRowAtIndexPath:indexPath];
    //    [self btnDetailArrowPressed:cell.btnDetailArrow];
    if([listDisplayData count] > 0)
    {
        NSDictionary *data =[listDisplayData objectAtIndex:indexPath.row];
        //Catch roomData to update view if RoomDetail come back
        selectedPendingRoom = data;
        
        NSDictionary *roomData = [data objectForKey:keyRoomData];
        RoomAssignmentModelV2 *roomAssignmentModel = [data objectForKey:keyRoomAssignmentModel];
        RoomModelV2 *roomModel = [data objectForKey:keyRoomModel];
        RoomRecordModelV2 *roomRecordModel = [data objectForKey:keyRoomRecordModel];
        
        [self insertRoomAssignmentModel:roomAssignmentModel];
        [self insertRoomModel:roomModel];
        [self insertRoomRecordModel:roomRecordModel];
        [self insertGuestInfoFromRowData:roomData];
        
        [RoomManagerV2 setCurrentRoomNo:roomModel.room_Id];
        [TasksManagerV2 setCurrentRoomAssignment:[[roomData objectForKey:kRoomAssignmentID] integerValue]];
        
        NSDictionary *roomAssignmentData = [[NSDictionary alloc] init];
        if(isDemoMode)
            roomAssignmentData = [[RoomManagerV2 sharedRoomManager] getRoomAssignmentDataByRoomAssignmentId:roomAssignmentModel.roomAssignment_Id andRaUserId:roomAssignmentModel.roomAssignmentHousekeeperId];
        else
            roomAssignmentData = [[RoomManagerV2 sharedRoomManager] getRoomAssignmentDataByRoomAssignmentId:roomAssignmentModel.roomAssignment_Id andRaUserId:roomAssignmentModel.roomAssignment_UserId];
        
        RoomAssignmentInfoViewController *viewController = [[RoomAssignmentInfoViewController alloc] initWithOption:IS_PUSHED_FROM_FIND_BY_RA andRoomAssignmentData:roomAssignmentData];
        viewController.isLoadFromFind = YES;
        [self.navigationController pushViewController:viewController animated:YES];
        
        //[self.navigationItem.backBarButtonItem setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK]];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationSelectedFind] object:nil];
    }
}

#pragma mark -
#pragma mark UITableView Editing rows
-(NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSIndexPath *rowToSelect = indexPath;
    
    return rowToSelect;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
	return UITableViewCellEditingStyleNone;
}

#pragma mark -
#pragma mark UITableView Moving rows
-(BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath{
    BOOL canMove = YES;
    
    if (indexPath.section == 0) {
        NSDictionary *rowData = [listDisplayData objectAtIndex:indexPath.row];
        CleaningStatusModelV2 *csmodel = [[CleaningStatusModelV2 alloc] init];
        csmodel.cstat_Id = [[rowData objectForKey:kCleaningStatusID] intValue];
        [[RoomManagerV2 sharedRoomManager] loadCleaningStatus:csmodel];
        
        if ([csmodel.cstat_Name isEqualToString:@"DND"]) {
            return NO;
        }
        
        //        canMove = indexPath.row <= [listDisplayData count];
    }
    
    return canMove;
}

- (NSIndexPath *)tableView:(UITableView *)tableView targetIndexPathForMoveFromRowAtIndexPath:(NSIndexPath *)sourceIndexPath toProposedIndexPath:(NSIndexPath *)proposedDestinationIndexPath {
    
    NSDictionary *rowData = [listDisplayData objectAtIndex:proposedDestinationIndexPath.row];
    CleaningStatusModelV2 *csmodel = [[CleaningStatusModelV2 alloc] init];
    csmodel.cstat_Id = [[rowData objectForKey:kCleaningStatusID] intValue];
    [[RoomManagerV2 sharedRoomManager] loadCleaningStatus:csmodel];
    
    if ([csmodel.cstat_Name isEqualToString:@"DND"]) {
        return sourceIndexPath;
    }
    
    return proposedDestinationIndexPath;
}

-(void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath{
    if ([fromIndexPath isEqual:toIndexPath]) {
        return;
    }
    
    NSInteger rowFrom, rowTo;
    rowFrom = fromIndexPath.row;
    rowTo = toIndexPath.row;
    
    NSMutableDictionary *fromRowData = [[NSMutableDictionary alloc] initWithDictionary: [listDisplayData objectAtIndex:fromIndexPath.row]];
    //    NSDictionary *toRowData = [listDisplayData objectAtIndex:toIndexPath.row];
    
    //Update sequence fromRowData to toRowData
    [self updateSequenceForFromIndex:fromIndexPath.row toToIndex:toIndexPath.row];
    
    [listDisplayData removeObjectAtIndex:fromIndexPath.row];
    
    [listDisplayData insertObject:fromRowData atIndex:toIndexPath.row];
}

-(BOOL)tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath{
    return NO;
}

-(void)responseLongPressButton:(UILongPressGestureRecognizer*)sender {
    
    if(!roomAssignment.isALlowedEdit)
    {
        return;
    }
    
    if (sender.state == UIGestureRecognizerStateBegan) {
        if (tbvContent.editing) {
            [tbvContent setEditing:NO];
        }
        else
        {
            [tbvContent setEditing:YES];
        }
    }
}

#pragma mark - Load Data
// Load data for pending room
-(void)loadData {
    //enable flag to announce, is loading data
    [[HomeViewV2 shareHomeView] waitingLoadingData];
    
    [[RoomManagerV2 sharedRoomManager] getRoomAssignments];
    NSMutableArray *arrayTemp = [RoomManagerV2 sharedRoomManager].roomAssignmentList;
    
    self.listTempData = [[NSMutableArray alloc] initWithArray:arrayTemp];
    
    NSMutableArray *array = [NSMutableArray array];
    
    for(int i = 0; i < [listTempData count]; i++) {
        NSDictionary *rowData = [listTempData objectAtIndex:i];
        
        NSString *roomStatus = [rowData objectForKey:kRMStatus];
        if([roomStatus isEqualToString:nameVD] || [roomStatus isEqualToString:nameOD]) {
            [array addObject:rowData];
        }
    }
    
    self.listDisplayData = [[NSMutableArray alloc] initWithArray:array];
    
    if([listDisplayData count] <= 0) {
        [tbvContent setHidden:YES];
        [tbvNoResult setHidden:NO];
    }
    
    if([listDisplayData count]>1) {
        for(int i = 0; i < [listDisplayData count]; i++) {
            for(int j = i + 1; j < [listDisplayData count] + 1; j++) {
                NSDictionary *row1 = [listDisplayData  objectAtIndex:i];
                NSString *roomNo1 = [row1 objectForKey:kRoomNo];
                NSString *guestName1 = [row1 objectForKey:kGuestName];
                
                NSDictionary *row2 = [listDisplayData  objectAtIndex:j];
                NSString *roomNo2 = [row2 objectForKey:kRoomNo];
                NSString *guestName2 = [row2 objectForKey:kGuestName];
                
                if([roomNo1 isEqualToString:roomNo2]) {
                    if([guestName1 isEqualToString:guestName2])
                        [listDisplayData removeObjectAtIndex:j];
                }
            }
        }
    }
    
    [tbvContent reloadData];
    
    
    //end loading data
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
}

#pragma mark - Action for Button Detail Arrow
-(void) insertRoomAssignmentFromRowData:(NSDictionary *) rowData {
    RoomAssignmentModelV2 *ramodel = [[RoomAssignmentModelV2 alloc] init];
    ramodel.roomAssignment_Id = [[rowData objectForKey:kraID] intValue];
    ramodel.roomAssignment_UserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    [[RoomManagerV2 sharedRoomManager] load:ramodel];
    if (ramodel.roomAssignment_RoomId.length > 0) {
        //room assignment with raId is existing
        ramodel.roomAssignment_AssignedDate = [rowData objectForKey:kraAssignedTime];
        ramodel.roomAssignment_LastModified = [rowData objectForKey:kraLastModified];
        ramodel.roomAssignment_PostStatus = (int)POST_STATUS_UN_CHANGED;
        ramodel.roomAssignment_RoomId = [rowData objectForKey:kraRoomNo];
        //set user id is current supervisor
        ramodel.roomAssignment_UserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
        ramodel.roomAssignmentHousekeeperId = [[rowData objectForKey:kraHousekeeperID] integerValue];
        ramodel.roomAssignmentRoomCleaningStatusId = [[rowData objectForKey:kraCleaningStatusID] integerValue];
        ramodel.roomAssignmentRoomExpectedCleaningTime = [[rowData objectForKey:kraExpectedCleaningTime] integerValue];
        ramodel.roomAssignmentRoomExpectedInspectionTime = [[rowData objectForKey:kraExpectedInspectTime] integerValue];
        ramodel.roomAssignmentRoomInspectionStatusId = [[rowData objectForKey:kraInspectedStatus] integerValue];
        ramodel.roomAssignmentRoomStatusId = [[rowData objectForKey:kraRoomStatusID] integerValue];
        
        //update room assignment
        [[RoomManagerV2 sharedRoomManager] update:ramodel];
    } else {
        //need insert room assignment because it does not existing in db
        ramodel.roomAssignment_AssignedDate = [rowData objectForKey:kraAssignedTime];
        ramodel.roomAssignment_LastModified = [rowData objectForKey:kraLastModified];
        ramodel.roomAssignment_PostStatus = (int)POST_STATUS_UN_CHANGED;
        ramodel.roomAssignment_RoomId = [rowData objectForKey:kraRoomNo];
        //set user id is current supervisor
        ramodel.roomAssignment_UserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
        ramodel.roomAssignmentHousekeeperId = [[rowData objectForKey:kraHousekeeperID] integerValue];
        ramodel.roomAssignmentRoomCleaningStatusId = [[rowData objectForKey:kraCleaningStatusID] integerValue];
        ramodel.roomAssignmentRoomExpectedCleaningTime = [[rowData objectForKey:kraExpectedCleaningTime] integerValue];
        ramodel.roomAssignmentRoomExpectedInspectionTime = [[rowData objectForKey:kraExpectedInspectTime] integerValue];
        ramodel.roomAssignmentRoomInspectionStatusId = [[rowData objectForKey:kraInspectedStatus] integerValue];
        ramodel.roomAssignmentRoomStatusId = [[rowData objectForKey:kraRoomStatusID] integerValue];
        [[RoomManagerV2 sharedRoomManager] insert:ramodel];
    }
}

-(void) insertRoomFromRowData:(NSDictionary *) rowData {
    RoomModelV2 *room = [[RoomModelV2 alloc] init];
    room.room_Id = [rowData objectForKey:kraRoomNo];
    [[RoomManagerV2 sharedRoomManager] loadRoomModel:room];
    
    if (room.room_HotelId == 0) {
        //room is not existing
        room.room_AdditionalJob = [rowData objectForKey:kraAdditionalJob];
        room.room_Building_Id = [[rowData objectForKey:kraBuildingID] integerValue];
        room.room_Building_Name = [rowData objectForKey:kraBuildingName];
        room.room_building_namelang = [rowData objectForKey:krabuildingLang];
        room.room_floor_id = [[rowData objectForKey:kraFloorID] integerValue];
        room.room_Guest_Name = [NSString stringWithFormat:@"%@ %@", [rowData objectForKey:kraGuestFirstName], [rowData objectForKey:kraGuestLastName]];
        room.room_GuestReference = [rowData objectForKey:kraGuestPreference];
        room.room_HotelId = [[rowData objectForKey:kraHotelID] intValue];
        room.room_Lang = @"";
        room.room_LastModified = [rowData objectForKey:kraLastModified];
        room.room_PostStatus = (int)POST_STATUS_UN_CHANGED;
        room.room_remark = [rowData objectForKey:kraRemark];
        room.room_TypeId = [[rowData objectForKey:kraRoomTypeID] intValue];
        room.room_VIPStatusId = [rowData objectForKey:kraVIP];
        room.room_floor_id = [[rowData objectForKey:kraFloorID] integerValue];
        
        [[RoomManagerV2 sharedRoomManager] insertRoomModel:room];
    } else {
        //room is existing
        room.room_AdditionalJob = [rowData objectForKey:kraAdditionalJob];
        room.room_Building_Id = [[rowData objectForKey:kraBuildingID] integerValue];
        room.room_Building_Name = [rowData objectForKey:kraBuildingName];
        room.room_building_namelang = [rowData objectForKey:krabuildingLang];
        room.room_floor_id = [[rowData objectForKey:kraFloorID] integerValue];
        room.room_Guest_Name = [NSString stringWithFormat:@"%@ %@", [rowData objectForKey:kraGuestFirstName], [rowData objectForKey:kraGuestLastName]];
        room.room_GuestReference = [rowData objectForKey:kraGuestPreference];
        room.room_HotelId = [[rowData objectForKey:kraHotelID] intValue];
        room.room_Lang = @"";
        room.room_LastModified = [rowData objectForKey:kraLastModified];
        room.room_PostStatus = (int)POST_STATUS_UN_CHANGED;
        room.room_remark = [rowData objectForKey:kraRemark];
        room.room_TypeId = [[rowData objectForKey:kraRoomTypeID] intValue];
        room.room_VIPStatusId = [rowData objectForKey:kraVIP];
        room.room_floor_id = [[rowData objectForKey:kraFloorID] integerValue];
        [[RoomManagerV2 sharedRoomManager] updateRoomModel:room];
    }
}

-(void) insertGuestInfoFromRowData:(NSDictionary *) rowData {
    GuestInfoModelV2 *gmodel = [[GuestInfoModelV2 alloc] init];
    gmodel.guestId = [[rowData objectForKey:kraRoomNo] integerValue];
    [[RoomManagerV2 sharedRoomManager] loadGuestInfo:gmodel];
    
    if (gmodel.guestRoomId.length <= 0) {
        //Removed because of changing WS for Guest Information
        //room is not existing
        //[[RoomManagerV2 sharedRoomManager] updateGuestInfo:[[rowData objectForKey:kraHousekeeperID] integerValue]  WithRoomAssignID:[[rowData objectForKey:kraID] integerValue]];
        [[RoomManagerV2 sharedRoomManager] updateGuestInfoByUserId:[UserManagerV2 sharedUserManager].currentUser.userId hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId roomNumber:[rowData objectForKey:kraRoomNo]];
    } else {
        //room is existing
    }
}

-(void) insertRoomAssignmentModel:(RoomAssignmentModelV2 *)roomAssignmentModel
{
    RoomAssignmentModelV2 *ramodel = [[RoomAssignmentModelV2 alloc] init];
    ramodel.roomAssignment_Id = roomAssignmentModel.roomAssignment_Id;
    ramodel.roomAssignment_UserId = roomAssignmentModel.roomAssignment_UserId;
    [[RoomManagerV2 sharedRoomManager] load:ramodel];
    if (ramodel.roomAssignment_RoomId.length > 0) {
        [[RoomManagerV2 sharedRoomManager] update:roomAssignmentModel];
    }
    else {
        [[RoomManagerV2 sharedRoomManager] insert:roomAssignmentModel];
    }
}

-(void) insertRoomModel:(RoomModelV2 *)roomModel
{
    RoomModelV2 *room = [[RoomModelV2 alloc] init];
    room.room_Id = roomModel.room_Id;
    [[RoomManagerV2 sharedRoomManager] loadRoomModel:room];
    if (room.room_Id.length > 0) {
        [[RoomManagerV2 sharedRoomManager] updateRoomModel:roomModel];
    }
    else {
        [[RoomManagerV2 sharedRoomManager] insertRoomModel:roomModel];
    }
}

-(void) insertRoomRecordModel:(RoomRecordModelV2 *)roomRecordModel
{
    RoomRecordModelV2 *roomRecord = [[RoomRecordModelV2 alloc] init];
    roomRecord.rrec_room_assignment_id = roomRecordModel.rrec_room_assignment_id;
    roomRecord.rrec_User_Id = roomRecordModel.rrec_User_Id;
    [[RoomManagerV2 sharedRoomManager] loadRoomRecordDataByRoomIdAndUserId:roomRecord];
    if (roomRecord.rrec_Id != 0) {
        [[RoomManagerV2 sharedRoomManager] updateRoomRecordData:roomRecordModel];
    }
    else {
        [[RoomManagerV2 sharedRoomManager] insertRoomRecordData:roomRecordModel];
    }
}

-(void)btnDetailArrowPressed:(UIButton *)sender {
    NSInteger index = sender.tag;
    NSDictionary *data =[listDisplayData objectAtIndex:index];
    NSDictionary *roomData = [data objectForKey:keyRoomData];
    RoomAssignmentModelV2 *roomAssignmentModel = [data objectForKey:keyRoomAssignmentModel];
    RoomModelV2 *roomModel = [data objectForKey:keyRoomModel];
    RoomRecordModelV2 *roomRecordModel = [data objectForKey:keyRoomRecordModel];
    
    //insert to database room, room assignment, guest info
    //    [self insertRoomAssignmentFromRowData:roomAssignmentModel];
    //    [self insertRoomFromRowData:roomModel];
    [self insertRoomAssignmentModel:roomAssignmentModel];
    [self insertRoomModel:roomModel];
    [self insertRoomRecordModel:roomRecordModel];
    [self insertGuestInfoFromRowData:roomData];
    
    //    RoomInfoAssignmentV2 *aRoomInfoAssignment = [[RoomInfoAssignmentV2 alloc] initWithNibName:@"RoomInfoAssignmentV2" bundle:nil];
    //
    //    RoomModelV2 *roomModel = [[RoomModelV2  alloc] init];
    //    roomModel.room_Id = [[roomData objectForKey:kraRoomNo] intValue];
    //    [[RoomManagerV2 sharedRoomManager] loadRoomModel:roomModel];
    
    //set current room no
    [RoomManagerV2 setCurrentRoomNo:roomModel.room_Id];
    
    //    GuestInfoModelV2 *guestInfoModel = [[GuestInfoModelV2 alloc] init];
    //    guestInfoModel.guestRoomId = roomModel.room_Id;
    //    [[RoomManagerV2 sharedRoomManager] loadByRoomIDGuestInfo:guestInfoModel];
    //
    //    RoomAssignmentModelV2 *roomAssignmentModel = [[RoomAssignmentModelV2 alloc] init];
    //    roomAssignmentModel.roomAssignment_Id = [[rowData objectForKey:kraID] intValue];
    //    roomAssignmentModel.roomAssignment_UserId = [UserManagerV2 sharedUserManager].currentUser.userId;
    //    [[RoomManagerV2 sharedRoomManager] load:roomAssignmentModel];
    
    //    aRoomInfoAssignment.roomName = [rowData objectForKey:kraRoomNo];
    //    aRoomInfoAssignment.roomModel = roomModel;
    //    aRoomInfoAssignment.guestInfoModel = guestInfoModel;
    //    aRoomInfoAssignment.raDataModel = roomAssignmentModel;
    
    //set current RoomAssignmentId use in ChecklistView
    [TasksManagerV2 setCurrentRoomAssignment:[[roomData objectForKey:kRoomAssignmentID] integerValue]];
    
    //disable for find feature
    //    [aRoomInfoAssignment disableUserInteractionInView];
    
    NSDictionary *roomAssignmentData = [[RoomManagerV2 sharedRoomManager] getRoomAssignmentDataByRoomAssignmentId:roomAssignmentModel.roomAssignment_Id andRaUserId:roomAssignmentModel.roomAssignment_UserId];
    
    RoomAssignmentInfoViewController *viewController = [[RoomAssignmentInfoViewController alloc] initWithOption:IS_PUSHED_FROM_FIND_BY_RA andRoomAssignmentData:roomAssignmentData];
    viewController.isLoadFromFind = YES;
    [self.navigationController pushViewController:viewController animated:YES];
    
    [self.navigationItem.backBarButtonItem setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK]];
    
    //    [self.navigationController pushViewController:aRoomInfoAssignment animated:YES];
    //    [aRoomInfoAssignment loadingData];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationSelectedFind] object:nil];
}

#pragma mark  - Set Captions View
-(void)setCaptionsView {
    [self setTitle:[[LanguageManagerV2 sharedLanguageManager] getFind]];
    
    //    [self.lblNameAndType setText:[NSString stringWithFormat:@"%@ - %@",
    //                                  attendantName, typeView]];
    
    [self.lblNameAndType setText:[NSString stringWithFormat:@"%@", [L_PENDING_ROOM currentKeyToLanguage]]];
    [self.lblNumOfRoom setText:[NSString stringWithFormat:@"%d", (int)[listDisplayData count]]];
    
    //    [lblNameAndType setFont:[UIFont fontWithName:font size:sizeNormal]];
    
    [lblRoomNo setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOM_NO]];
    [lblRoomNo setFont:[UIFont fontWithName:font size:sizeNormal]];
    
    [lblRoomStatus setText:[[LanguageManagerV2 sharedLanguageManager] getRMStatus]];
    [lblRoomStatus setFont:[UIFont fontWithName:font size:sizeNormal]];
    
    [lblVIP setText:[[LanguageManagerV2 sharedLanguageManager] getVIP]];
    [lblVIP setFont:[UIFont fontWithName:font size:sizeNormal]];
    
    [lblCleaningStatus setText:[[LanguageManagerV2 sharedLanguageManager] getCleaningStatus]];
    [lblCleaningStatus setFont:[UIFont fontWithName:font size:sizeNormal]];
}

#pragma mark - === Handle Topbar Methods ===
#pragma mark
-(void)loadTopbarView {
    TopbarViewV2 *topbar = [[TopbarViewV2 alloc] initViewWithSuperModel:[[CommonVariable sharedCommonVariable] superRoomModel]];
    [topbar setTag:tagTopbarView];
    [self.view addSubview:topbar];
    [topbar setHidden:YES];
    
    [topbar setAlpha:0.0];
    [self addButtonHandleShowHideTopbar];
}

-(void)showTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationWillStartSelector:@selector(setHiddenTopbarView)];
    
    f = topbar.frame;
    f.origin.y = 0;
    [topbar setFrame:f];
    [topbar setAlpha:1.0];
    
    [self adjustShowForViews];
    
    [UIView commitAnimations];
}

-(void)hideTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDidStopSelector:@selector(setHiddenTopbarView)];
    [UIView setAnimationDelegate:self];
    
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    [topbar setAlpha:0.0];
    
    [self adjustRemoveForViews];
    
    [UIView commitAnimations];
}

-(void)setHiddenTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    [topbar setHidden:!topbar.hidden];
}

-(TopbarViewV2 *)getTopBarView {
    return (TopbarViewV2 *)[self.view viewWithTag:tagTopbarView];
}

//-(void)addButtonHandleShowHideTopbar {
//    UIButton *titleBarButton = [[UIButton alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
//
//    titleBarButton.titleLabel.backgroundColor = [UIColor clearColor];
//    [titleBarButton.titleLabel setFont: FONT_BUTTON_TOPBAR];
//    titleBarButton.titleLabel.textAlignment = NSTextAlignmentCenter;
//    titleBarButton.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
//    [titleBarButton.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
//    [titleBarButton.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
//    [titleBarButton addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
//    [titleBarButton setTitle:[[LanguageManagerV2 sharedLanguageManager] getFind] forState:UIControlStateNormal];
//    self.navigationItem.titleView = titleBarButton;
//}

-(void)addButtonHandleShowHideTopbar {
    
    UIView *vBarButton = [[UIView alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
    UIButton *titleBarButtonFirst = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 200, 30)];
    
    titleBarButtonFirst.titleLabel.backgroundColor = [UIColor clearColor];
    [titleBarButtonFirst.titleLabel setFont: FONT_BUTTON_TOPBAR];
    titleBarButtonFirst.titleLabel.textAlignment = NSTextAlignmentCenter;
    titleBarButtonFirst.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
    
    [titleBarButtonFirst.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
    [titleBarButtonFirst.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
    [titleBarButtonFirst addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    [titleBarButtonFirst setTitle:[[LanguageManagerV2 sharedLanguageManager] getFind] forState:UIControlStateNormal];
    
    [vBarButton addSubview:titleBarButtonFirst];
    
    UIButton *titleBarButtonSecond = [[UIButton alloc] initWithFrame:CGRectMake(0, 22, 200, 22)];
    [titleBarButtonSecond setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [titleBarButtonSecond.titleLabel setFont:FONT_BUTTON_TOPBAR_SECOND];
    [titleBarButtonSecond setTitle: attendantName forState:UIControlStateNormal];
    [titleBarButtonSecond addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    [vBarButton addSubview:titleBarButtonSecond];
    
    self.navigationItem.titleView = vBarButton;
    //Move title to center vertical
    CGRect titleViewBounds = self.navigationItem.titleView.bounds;
    int moveLeftTitle = 0;
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        moveLeftTitle = 10;
    }
    self.navigationItem.titleView.bounds = CGRectMake(titleViewBounds.origin.x + moveLeftTitle, Y_TOPBAR_TITLE, titleViewBounds.size.width, titleViewBounds.size.height);
    
}

-(void)topbarTapped:(UIButton *)sender {
    TopbarViewV2 *topbar = [self getTopBarView];
    [sender setEnabled:NO];
    if (topbar.isHidden) {
        [self showTopbarView];
    } else {
        [self hideTopbarView];
    }
    [self performSelector:@selector(topbarButtonEnable:) withObject:sender afterDelay:0.3];
}

-(void) topbarButtonEnable:(UIButton *) topbarBtn {
    [topbarBtn setEnabled:YES];
}

-(void)adjustShowForViews {
    //adjust all views when show
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    CGRect ftbv = controlsView.frame;
    [controlsView setFrame:CGRectMake(0, f.size.height, 320, ftbv.size.height)];
    
    ftbv = tbvContent.frame;
    [tbvContent setFrame:CGRectMake(0, f.size.height + controlsView.frame.size.height, 320, ftbv.size.height - f.size.height)];
    
    ftbv = tbvNoResult.frame;
    [tbvNoResult setFrame:CGRectMake(0, f.size.height + controlsView.frame.size.height, 320, ftbv.size.height - f.size.height)];
}

-(void)adjustRemoveForViews {
    //adjust all views when hide
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    CGRect ftbv = controlsView.frame;
    [controlsView setFrame:CGRectMake(0, 0, 320, ftbv.size.height)];
    
    ftbv = tbvContent.frame;
    [tbvContent setFrame:CGRectMake(0, controlsView.frame.size.height, 320, ftbv.size.height + f.size.height)];
    
    ftbv = tbvNoResult.frame;
    [tbvNoResult setFrame:CGRectMake(0, controlsView.frame.size.height, 320, ftbv.size.height + f.size.height)];
}

#pragma mark - === Handle Back Button ===
#pragma mark

-(void)backBarPressed {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)addBackButton {
    //back button navigation
    self.navigationItem.leftBarButtonItem = [MyNavigationBarV2 createBackButtonWithTarget:self selector:@selector(backBarPressed) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - === Update Sequence ===
#pragma mark
-(void)updateSequenceForFromIndex:(NSInteger)fromIndex toToIndex:(NSInteger)toIndex {
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_RA_LOADING_DATA_CONTENT]];
    [HUD show:YES];
    [self.tabBarController.view addSubview:HUD];
    
    //call update sequence here
    NSDictionary *fromRowData = [listDisplayData objectAtIndex:fromIndex];
    RoomAssignmentModelV2 *fromRoomAssignmentModel = [fromRowData objectForKey:keyRoomAssignmentModel];
    
    NSDictionary *toRowData = [listDisplayData objectAtIndex:toIndex];
    RoomAssignmentModelV2 *toRoomAssignmentModel = [toRowData objectForKey:keyRoomAssignmentModel];
    
    [[RoomManagerV2 sharedRoomManager] changeRoomAssignmentSequenceFromrRoomAssignId:
     fromRoomAssignmentModel.roomAssignment_Id toRoomAssignId:toRoomAssignmentModel.roomAssignment_Id forUserId:fromRoomAssignmentModel.roomAssignment_UserId];
    
    [HUD hide:YES];
    [HUD removeFromSuperview];
}

#pragma mark - iOS 7 Resources
-(void)loadFlatResource
{
    [imgHeader setImage:[UIImage imageNamed:imgLaundryCartButtonFlat]];
}

@end
