//
//  TimePerformanceSummaryViewV2.h
//  mHouseKeeping
//
//  Created by Nghia Truong on 2/17/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//
#import <UIKit/UIKit.h>

@interface TimePerformanceSummaryViewV2 : UIViewController <UITableViewDataSource, UITableViewDelegate>{
    UILabel *lblNameAttendant;
    UITableView *tbvContent;
    NSDictionary *dictionaryInitialText;
    NSMutableArray *listDisplayData;
    __unsafe_unretained NSDictionary *attendantData;
        
    UIButton *btnBack;
    
    //all room assignment data of attendant, save as dictionary
    NSMutableArray *roomAssignmentData;
    
    //distribute all room assignment
    NSMutableArray *completedRoomAssignments;
    NSMutableArray *pendingRoomAssignments;
    NSMutableArray *passedRoomAssignments;
    NSMutableArray *failedRoomAssignments;
    
    NSMutableArray *guestInfoData;
}

@property (nonatomic, strong) NSMutableArray *completedRoomAssignments;
@property (nonatomic, strong) NSMutableArray *pendingRoomAssignments;
@property (nonatomic, strong) NSMutableArray *passedRoomAssignments;
@property (nonatomic, strong) NSMutableArray *failedRoomAssignments;

@property (nonatomic, strong) NSMutableArray *roomAssignmentData;
@property (nonatomic, strong) NSMutableArray *guestInfoData;
@property (nonatomic, strong) IBOutlet UILabel *lblNameAttendant;
@property (nonatomic, strong) IBOutlet UITableView *tbvContent;
@property (nonatomic, strong) NSDictionary *dictionaryInitialText;
@property (nonatomic, strong) NSMutableArray *listDisplayData;
@property (nonatomic, assign) NSDictionary *attendantData;
@property (strong, nonatomic) IBOutlet UIView *controlsView;

-(void) loadData;
@end
