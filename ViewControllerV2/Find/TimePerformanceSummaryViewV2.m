//
//  TimePerformanceSummaryViewV2.m
//  mHouseKeeping
//
//  Created by Nghia Truong on 2/17/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "TimePerformanceSummaryViewV2.h"
#import "FindCompletedPassFailViewV2.h"
#import "FindPendingViewV2.h"
#import "LanguageManagerV2.h"
#import "FindManagerV2.h"
#import "MyNavigationBarV2.h"
#import "DeviceManager.h"
#import "ehkDefinesV2.h"
#import "UIImage+Tint.h"

#define tagAheadSchedule            1
#define tagRoomPending              2
#define tagRoomCompleted            3
#define tagPass                     4
#define tagFail                     5

#define tagDetailRoomPending        6
#define tagDetailRoomCompleted      7
#define tagDetailPass               8
#define tagDetailFail               9

#define kAheadSchedule              @"ahead schedule"
#define kRoomPending                @"Room pending"
#define kRoomCompleted              @"Room completed"
#define kPass                       @"pass"
#define kFail                       @"fail"

#define kAttendantName      @"AttendantName"
#define kCurrentStatus      @"CurrentStatus"
#define kTime               @"Time"
#define kRoomNumber         @"RoomNumber"
#define kRoomAssignmentID   @"RoomAssignmentID"

#define kValueAheadSchedule         @" Value ahead schedule"
#define kValueRoomPending           @"Value room pending"
#define kValueRoomCompleted         @"Value room completed"
#define kValuePass                  @"Value pass"
#define kValueFail                  @"Value fail"
#define kFindGuestName                  @"Value guest name"

#define pendingImage                @"Éspending_icon.png"
#define completedImage              @"Icon_completed.png"
#define detailArrowImage            @"gray_arrow_36x36.png"
#define backgroundImage             @"bg.png"

#define colorWhite                  255/255.0
#define colorAlpha                  1.0
#define colorBlueR                  105/255.0
#define colorBlueG                  218/255.0
#define colorBlueB                  222/255.0
#define colorGrayBlueR              99/255.0
#define colorGrayBlueG              129/255.0
#define colorGrayBlueB              155/255.0

#define font                        @"Arial-BoldMT"

#define sizeNormal                  17.0f

#define heightSection0              44.0f
#define heightSection1              60.0f

#define numberRowSection0           3
#define numberRowSection1           2

@interface TimePerformanceSummaryViewV2 (PrivateMethod)

-(void) loadStringTitles;
-(void) btnDetailArrowPressed:(UIButton *) sender;

-(void) loadTopbarView;
-(void) showTopbarView;
-(void) hideTopbarView;
-(void) setHiddenTopbarView;
-(void) adjustShowForViews;
-(void) adjustRemoveForViews;
-(void) addButtonHandleShowHideTopbar;
-(void) topbarTapped:(UIButton *) sender;
-(TopbarViewV2 *) getTopBarView;
-(void) btnBackPressed;
-(void) addBackButton;

//calculate number of room type of attendant's room assignment
-(NSInteger) numberOfRoomPendingFromRoomAssignments:(NSMutableArray *) roomAssignments;
-(NSInteger) numberOfRoomCompletedFromRoomAssignments:(NSMutableArray *) roomAssignments;
-(NSInteger) numberOfRoomPassFromRoomAssignments:(NSMutableArray *) roomAssignments;
-(NSInteger) numberOfRoomFailFromRoomAssignments:(NSMutableArray *) roomAssignments;

@end

@implementation TimePerformanceSummaryViewV2
@synthesize controlsView;

@synthesize lblNameAttendant, tbvContent, dictionaryInitialText, listDisplayData;
@synthesize attendantData;

@synthesize roomAssignmentData = _roomAssignmentData;
@synthesize guestInfoData = _guestInfoData;

@synthesize pendingRoomAssignments = _pendingRoomAssignments;
@synthesize completedRoomAssignments = _completedRoomAssignments;
@synthesize passedRoomAssignments = _passedRoomAssignments;
@synthesize failedRoomAssignments = _failedRoomAssignments;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        //init distribute room assignments
        self.pendingRoomAssignments = [NSMutableArray array];
        self.completedRoomAssignments = [NSMutableArray array];
        self.passedRoomAssignments = [NSMutableArray array];
        self.failedRoomAssignments = [NSMutableArray array];
    }
    return self;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle
-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self setTitle:[[LanguageManagerV2 sharedLanguageManager] getFind]];
    
    //set selection find in bottom bar
    [[HomeViewV2 shareHomeView] setSelectedButton:tagOfFindButton];
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        //Clear redundant view for header, footter
        CGRect frameHeaderFooter = tbvContent.tableHeaderView.frame;
        frameHeaderFooter.size.height = 0.1f;
        UIView *headerView = [[UIView alloc] initWithFrame:frameHeaderFooter];
        [tbvContent setTableHeaderView:headerView];
        
        frameHeaderFooter = tbvContent.tableFooterView.frame;
        frameHeaderFooter.size.height = 0.1f;
        UIView *footerView = [[UIView alloc] initWithFrame:frameHeaderFooter];
        [tbvContent setTableFooterView:footerView];
    }
    
    [self.lblNameAttendant setText:[attendantData objectForKey:kAttendantName]];
    
    dictionaryInitialText = [[NSMutableDictionary alloc] init];
    [self loadStringTitles]; 
    
    [tbvContent setBackgroundView:nil];
    [tbvContent setBackgroundColor:[UIColor clearColor]];
    [tbvContent setOpaque:YES];
    
    [self performSelector:@selector(loadData) withObject:nil afterDelay:0.0f];
    
    //Resize content views of current VC to fit with Navigation bar and tabbar
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    [self performSelector:@selector(loadTopbarView)];
    
    //add back button
    [self addBackButton];
}

- (void)viewDidUnload {
    [self setLblNameAttendant:nil];
    [self setTbvContent:nil];
    [self setDictionaryInitialText:nil];
    [self setListDisplayData:nil];
    
    [self setControlsView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

# pragma mark - Initial String
-(void)loadStringTitles {
    NSDictionary *dictionary = [[LanguageManagerV2 sharedLanguageManager] getCurrentDictionaryLanguage];
    
    NSString *aheadSchedule, *roomPending, *roomCompleted, *pass, *fail;
    
    aheadSchedule = [dictionary valueForKey:
                    [NSString stringWithFormat:@"%@", L_AHEAD_SCHEDULE]];
    
    roomPending = [dictionary valueForKey:
                       [NSString stringWithFormat:@"%@", L_room_pending]];
    
    roomCompleted = [dictionary valueForKey:
                      [NSString stringWithFormat:@"%@", L_COMPLETED_ROOM]];
    
    aheadSchedule = [dictionary valueForKey:
                     [NSString stringWithFormat:@"%@", nil]];
    
    roomPending = [dictionary valueForKey:
                   [NSString stringWithFormat:@"%@", nil]];
    
    roomCompleted = [dictionary valueForKey:
                     [NSString stringWithFormat:@"%@", nil]];
    
    pass = [dictionary valueForKey:
                           [NSString stringWithFormat:@"%@", L_PASS]];
    
    fail = [dictionary valueForKey:
            [NSString stringWithFormat:@"%@", L_FAIL]];
        
    if (aheadSchedule == nil) {
        aheadSchedule = [[LanguageManagerV2 sharedLanguageManager] getAheadSchedule];
    }
    
    if (roomPending == nil) {
        //roomPending = [[LanguageManagerV2 sharedLanguageManager] getRoomPending];
        roomPending = [L_room_pending currentKeyToLanguage];
    } 
    
    if (roomCompleted == nil) {
//        roomCompleted = [[LanguageManagerV2 sharedLanguageManager] getRoomComplete];
        roomCompleted = [[LanguageManagerV2 sharedLanguageManager] getCompletedRoom];
    }
    
    if (pass == nil) {
        pass = [[LanguageManagerV2 sharedLanguageManager] getPass];
    }
    
    if (fail == nil) {
        fail = [[LanguageManagerV2 sharedLanguageManager] getFail];
    }
        
    [dictionaryInitialText setValue:aheadSchedule forKey:kAheadSchedule];
    [dictionaryInitialText setValue:roomPending forKey:kRoomPending];    
    [dictionaryInitialText setValue:roomCompleted forKey:kRoomCompleted];
    [dictionaryInitialText setValue:pass forKey:kPass];
    [dictionaryInitialText setValue:fail forKey:kFail];
    
    [tbvContent reloadData];
}

#pragma mark - === Calculate number room type ===
#pragma mark
-(NSInteger)numberOfRoomPendingFromRoomAssignments:(NSMutableArray *)roomAssignments {
    NSInteger numberOfRoomPending = 0;
    for (NSDictionary *roomData in roomAssignments) {
        NSDictionary *roomAssignment = [roomData objectForKey:keyRoomData];
        NSInteger cleaningStatus = [(NSString *)[roomAssignment objectForKey:kCleaningStatusID] integerValue];
        
        if (cleaningStatus != ENUM_CLEANING_STATUS_COMPLETED)
        {
            numberOfRoomPending ++;
            [_pendingRoomAssignments addObject:roomData];
        }
    }
    return numberOfRoomPending;
}

-(NSInteger)numberOfRoomCompletedFromRoomAssignments:(NSMutableArray *)roomAssignments {
    NSInteger numberOfRoomCompleted = 0;
    for (NSDictionary *roomData in roomAssignments) {
        NSDictionary *roomAssignment = [roomData objectForKey:keyRoomData];
        NSInteger cleaningStatus = [(NSString *)[roomAssignment objectForKey:kCleaningStatusID] integerValue];
        
        if (cleaningStatus == ENUM_CLEANING_STATUS_COMPLETED) {
            numberOfRoomCompleted ++;
            [_completedRoomAssignments addObject:roomData];
        }
    }
    
    return numberOfRoomCompleted;
}

-(NSInteger)numberOfRoomPassFromRoomAssignments:(NSMutableArray *)roomAssignments {
    NSInteger numberOfRoomPass = 0;
    for (NSDictionary *roomData in roomAssignments) {
        NSDictionary *roomAssignment = [roomData objectForKey:keyRoomData];
        NSInteger inspectedStatus = [[roomAssignment objectForKey:kRoomInspectedStatusId] integerValue];
        NSInteger roomStatus = [(NSString *)[roomAssignment objectForKey:kRoomStatusId] integerValue];
        
        if (inspectedStatus == ENUM_INSPECTION_COMPLETED_PASS && (roomStatus == ENUM_ROOM_STATUS_OI || roomStatus == ENUM_ROOM_STATUS_VI)) {
            numberOfRoomPass ++;
            [_passedRoomAssignments addObject:roomData];
        }
    }
    
    return numberOfRoomPass;
}

-(NSInteger)numberOfRoomFailFromRoomAssignments:(NSMutableArray *)roomAssignments {
    NSInteger numberOfRoomFail = 0;
    for (NSDictionary *roomData in roomAssignments) {
        NSDictionary *roomAssignment = [roomData objectForKey:keyRoomData];
        NSInteger inspectedStatus = [[roomAssignment objectForKey:kRoomInspectedStatusId] integerValue];
        NSInteger roomStatus = [(NSString *)[roomAssignment objectForKey:kRoomStatusId] integerValue];
        NSInteger cleaningStatus = [[roomAssignment objectForKey:kCleaningStatusID] integerValue];
        
        if (inspectedStatus == ENUM_INSPECTION_COMPLETED_FAIL && cleaningStatus == ENUM_CLEANING_STATUS_COMPLETED && (roomStatus == ENUM_ROOM_STATUS_VD || roomStatus == ENUM_ROOM_STATUS_OD))
        {
            numberOfRoomFail ++;
            [_failedRoomAssignments addObject:roomData];
        }
    }
    
    return numberOfRoomFail;
}

#pragma mark - Load Data
// Load data for all result find by attendant
-(void)loadData {
    //enable flag to announce, is loading data
    [[HomeViewV2 shareHomeView] waitingLoadingData];
    
    //show HUD
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_RA_LOADING_DATA]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES];
    
//    NSString *attendantName = [self.attendantData objectForKey:kAttendantName];
    NSInteger attendantId = [[attendantData objectForKey:kAttendantID] integerValue];
    
    NSString *lastModifiedStr = nil;
    //If filter rooms of RA today
    //if([[UserManagerV2 sharedUserManager].currentUserAccessRight isFindRARoomsToday]){
    //    lastModifiedStr = [ehkConvert getTodayDateString]; //today date string format "yyyy-MM-dd HH:mm:ss"
    //}
    
    NSArray *temp = [[FindManagerV2 sharedFindManagerV2] findRoomAssignmentWithUserID:attendantId hotelId:[[UserManagerV2 sharedUserManager]currentUser].userHotelsId AndSupervisorId:[[UserManagerV2 sharedUserManager]currentUser].userId AndLastModified:lastModifiedStr];
    
    self.roomAssignmentData = [NSMutableArray arrayWithArray:temp];
//    [NSMutableArray arrayWithArray:[[RoomManagerV2 sharedRoomManager] getAllRoomAssignmentDatas:attendantId]];
    
    self.listDisplayData = [NSMutableArray array];
    NSMutableDictionary *row = [NSMutableDictionary dictionary];

    //calculate number of room
    NSInteger pendingRooms = [self numberOfRoomPendingFromRoomAssignments:_roomAssignmentData];
    NSInteger completedRooms = [self numberOfRoomCompletedFromRoomAssignments:_roomAssignmentData];
    NSInteger passRooms = [self numberOfRoomPassFromRoomAssignments:_roomAssignmentData];
    NSInteger failRooms = [self numberOfRoomFailFromRoomAssignments:_roomAssignmentData];
    
    [row setObject:[NSString stringWithFormat:@"%d", (int)pendingRooms] forKey:kValueRoomPending];
    
    [row setObject:[NSString stringWithFormat:@"%d", (int)completedRooms] forKey:kValueRoomCompleted];
    
    [row setObject:[NSString stringWithFormat:@"%d", (int)passRooms] forKey:kValuePass];
    
    [row setObject:[NSString stringWithFormat:@"%d", (int)failRooms] forKey:kValueFail];
    
    [listDisplayData addObject:row];
    
    [tbvContent reloadData];
    
    //hide HUD
    [HUD hide:YES];
    [HUD removeFromSuperview];
    
    
    //end loading data
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
}

#pragma mark - Selector for Button Detail Arrow
-(void) handleShowListOfRoomAssignmentWithType:(NSInteger) tag {
    if(tag == tagDetailRoomCompleted) {
        FindCompletedPassFailViewV2 *findView = [[FindCompletedPassFailViewV2 alloc] initWithNibName:NSStringFromClass([FindCompletedPassFailViewV2 class]) bundle:nil];
        
        findView.attendantName = [attendantData objectForKey:kAttendantName];
        findView.typeView = [[LanguageManagerV2 sharedLanguageManager] getCompletedRoom];
        findView.attendantId = [[attendantData objectForKey:kAttendantID] intValue];
        [self.navigationController pushViewController:findView animated:YES];
        
        //init data for find view
        [findView setRoomAssignmentsData:_completedRoomAssignments WithType:COMPLETED_ROOM_ASSIGNMENT_STATUS];
        
    } else if(tag == tagDetailPass) {
        FindCompletedPassFailViewV2 *findView = [[FindCompletedPassFailViewV2 alloc] initWithNibName:NSStringFromClass([FindCompletedPassFailViewV2 class]) 
                                                                                              bundle:nil];
        
        findView.attendantName = [attendantData objectForKey:kAttendantName];
        findView.typeView = [[LanguageManagerV2 sharedLanguageManager] getPassInspection];;
        findView.attendantId = [[attendantData objectForKey:kAttendantID] intValue];
        
        [self.navigationController pushViewController:findView animated:YES];
        
        //init data for find view
        [findView setRoomAssignmentsData:_passedRoomAssignments WithType:PASS_ROOM_ASSIGNMENT_STATUS];
    } else if(tag == tagDetailFail) {
        FindCompletedPassFailViewV2 *findView = [[FindCompletedPassFailViewV2 alloc] initWithNibName:NSStringFromClass([FindCompletedPassFailViewV2 class]) 
                                                                                              bundle:nil];
        
        findView.attendantName = [attendantData objectForKey:kAttendantName];
        findView.typeView = [[LanguageManagerV2 sharedLanguageManager] getFailInspection];;
        findView.attendantId = [[attendantData objectForKey:kAttendantID] intValue];
        
        [self.navigationController pushViewController:findView animated:YES];
        
        //init data for find view
        [findView setRoomAssignmentsData:_failedRoomAssignments WithType:FAIL_ROOM_ASSIGNMENT_STATUS];
        
    } else if(tag == tagDetailRoomPending) {
        FindPendingViewV2 *findView = [[FindPendingViewV2 alloc] initWithNibName:NSStringFromClass([FindPendingViewV2 class]) bundle:nil];
        
        findView.attendantName = [attendantData objectForKey:kAttendantName];
        findView.listDisplayData = [NSMutableArray arrayWithArray:_pendingRoomAssignments];
        findView.typeView = [[LanguageManagerV2 sharedLanguageManager] getPendingRoom];
        
        [self.navigationController pushViewController:findView animated:YES];
    }
}

-(void)btnDetailArrowPressed:(UIButton *) sender {
    NSInteger tag = sender.tag;
    
    [self handleShowListOfRoomAssignmentWithType:tag];
}

#pragma mark - UITableView Datasource Methods
-(CGFloat)tableView:(UITableView *)tableView 
    heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger section = indexPath.section;
    switch (section) {
        case 0:
            return heightSection0;
            break;
            
        case 1:
            return heightSection1;
            break;
        
        default:
            return 0;
            break;
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView 
    numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return numberRowSection0;
            break;
            
        case 1:
            return numberRowSection1;
            break;
            
        default:
            return 0;
            break;
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    cell.backgroundColor = [UIColor colorWithRed:colorWhite green:colorWhite 
                                            blue:colorWhite alpha:colorAlpha];
}

-(UITableViewCell *)tableView:(UITableView *)tableView 
        cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellAhead = @"cellAhead";
    static NSString *cellPending = @"cellPending";
    static NSString *cellCompleted = @"cellCompleted";
    static NSString *cellPass = @"cellPass";
    static NSString *cellFail = @"cellFail";
    
    UITableViewCell *cell = nil;
    NSUInteger section = indexPath.section;
    
    UIColor *titleColor = [UIColor colorWithRed:colorGrayBlueR green:colorGrayBlueG
                                           blue:colorGrayBlueB alpha:colorAlpha];
    UIColor *titleColor1 = [UIColor blackColor];
    UIColor *valueColor = [UIColor grayColor];
    
    UIFont *font1 = [UIFont fontWithName:font size:sizeNormal];
    
    if(cell == nil) {
        switch (section) {
            case 0: { 
                NSUInteger row = indexPath.row;                
                switch (row) {
                    case 0: { // init Ahead Schedule row
                        cell = [tableView dequeueReusableCellWithIdentifier:
                                cellAhead];
                        if(cell == nil) {
                            cell = [[UITableViewCell alloc] 
                                     initWithStyle:UITableViewCellStyleDefault 
                                    reuseIdentifier:cellAhead];
//                            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
                            
                            UILabel *titleAheadSchedule = [[UILabel alloc]                                             initWithFrame:CGRectMake(10, 4, 160, 30)];                            
                            [titleAheadSchedule setTextColor:titleColor];   
                            [titleAheadSchedule setFont:font1];
                            [titleAheadSchedule setBackgroundColor:[UIColor clearColor]];
                            [titleAheadSchedule setText:[dictionaryInitialText 
                                                         objectForKey:kAheadSchedule]];         
                            
                            [cell.contentView addSubview:titleAheadSchedule];
                            
                            UILabel *valueAheadSchedule = [[UILabel alloc]                                             initWithFrame:CGRectMake(200, 4, 60, 30)];                            
                            [valueAheadSchedule setTextColor:valueColor];   
                            [valueAheadSchedule setFont:font1];
                            [valueAheadSchedule setBackgroundColor:[UIColor clearColor]];
                            [valueAheadSchedule setTextAlignment:NSTextAlignmentCenter];
                            [valueAheadSchedule setTag:tagAheadSchedule];
                            
                            [cell.contentView addSubview:valueAheadSchedule];
                        }
                    }
                        break;
                        
                    case 1: { // init Room Pending row
                        cell = [tableView dequeueReusableCellWithIdentifier:
                                cellPending];
                        if(cell == nil) {
                            cell = [[UITableViewCell alloc] 
                                     initWithStyle:UITableViewCellStyleDefault 
                                     reuseIdentifier:cellPending];
//                            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
                            
                            UILabel *titleRoomPending = [[UILabel alloc] 
                                                initWithFrame:CGRectMake(10, 4, 160, 30)];
                            [titleRoomPending setTextColor:titleColor];   
                            [titleRoomPending setFont:font1];
                            [titleRoomPending setBackgroundColor:[UIColor clearColor]];
                            [titleRoomPending setText:[dictionaryInitialText 
                                                              objectForKey:kRoomPending]];                        
                            
                            [cell.contentView addSubview:titleRoomPending];
                            
                            
//                            UIImageView *imagePending = [[UIImageView alloc] initWithFrame:CGRectMake(170, 8, 25, 25)];
//                            [imagePending setImage:[UIImage imageNamed:pendingImage]];
//                            
//                            [cell.contentView addSubview:imagePending];
                            
                            
                            UILabel *valueRoomPending = [[UILabel alloc] 
                                                initWithFrame:CGRectMake(210, 4, 40, 30)];                            
                            [valueRoomPending setFont:font1];
                            [valueRoomPending setTextColor:valueColor];
                            [valueRoomPending setTextAlignment:NSTextAlignmentLeft];
                            [valueRoomPending setBackgroundColor:[UIColor clearColor]];
                            [valueRoomPending setTextAlignment:NSTextAlignmentCenter];
                            [valueRoomPending setTag:tagRoomPending];
                            
                            [cell.contentView addSubview:valueRoomPending];
                            
                            
                            UIButton *btnDetailArrow = [UIButton buttonWithType:UIButtonTypeCustom];
                            [btnDetailArrow setFrame:CGRectMake(265, 8, 25, 25)];
                            [btnDetailArrow setBackgroundImage:[UIImage imageNamed:detailArrowImage] forState:UIControlStateNormal];
                            [btnDetailArrow addTarget:self action:@selector(btnDetailArrowPressed:) forControlEvents:UIControlEventTouchUpInside];
                            [btnDetailArrow setTag:tagDetailRoomPending];
                            
                            [cell.contentView addSubview:btnDetailArrow];
                        }
                    }
                        break;
                        
                    case 2: { // init Rooms Completed row
                        cell = [tableView dequeueReusableCellWithIdentifier:
                                cellCompleted];
                        if(cell == nil) {
                            cell = [[UITableViewCell alloc] 
                                     initWithStyle:UITableViewCellStyleDefault 
                                     reuseIdentifier:cellCompleted];
//                            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
                            
                            UILabel *titleRoomCompleted = [[UILabel alloc] 
                                            initWithFrame:CGRectMake(10, 4, 160, 30)];
                            [titleRoomCompleted setTextColor:titleColor];   
                            [titleRoomCompleted setFont:font1];
                            [titleRoomCompleted setBackgroundColor:[UIColor clearColor]];
                            [titleRoomCompleted setText:[dictionaryInitialText 
                                                       objectForKey:kRoomCompleted]];                        
                            
                            [cell.contentView addSubview:titleRoomCompleted];
                            
                            
//                            UIImageView *imageCompleted = [[UIImageView alloc] initWithFrame:CGRectMake(170, 8, 25, 25)];
//                            [imageCompleted setImage:[UIImage imageNamed:completedImage]];
//                            
//                            [cell.contentView addSubview:imageCompleted];
                            
                            
                            UILabel *valueRoomCompleted = [[UILabel alloc] 
                                            initWithFrame:CGRectMake(210, 4, 40, 30)];                            
                            [valueRoomCompleted setFont:font1];
                            [valueRoomCompleted setTextColor:valueColor];
                            [valueRoomCompleted setTextAlignment:NSTextAlignmentLeft];
                            [valueRoomCompleted setBackgroundColor:[UIColor clearColor]];
                            [valueRoomCompleted setTextAlignment:NSTextAlignmentCenter];
                            [valueRoomCompleted setTag:tagRoomCompleted];
                            
                            [cell.contentView addSubview:valueRoomCompleted];
                            
                            
                            UIButton *btnDetailArrow = [UIButton buttonWithType:UIButtonTypeCustom];
                            [btnDetailArrow setFrame:CGRectMake(265, 8, 25, 25)];
                            [btnDetailArrow setBackgroundImage:[UIImage imageNamed:detailArrowImage] forState:UIControlStateNormal];
                            [btnDetailArrow addTarget:self action:@selector(btnDetailArrowPressed:) forControlEvents:UIControlEventTouchUpInside];
                            [btnDetailArrow setTag:tagDetailRoomCompleted];
                            
                            [cell.contentView addSubview:btnDetailArrow];
                        }
                    }
                        break;
                                            
                    default:
                        break;
                }   
            }        
                break;

                
            case 1: { 
                NSUInteger row = indexPath.row;
                switch (row) {
                    case 0: { // init Pass row
                        cell = [tableView dequeueReusableCellWithIdentifier:
                                cellPass];
                        if(cell ==nil) {
                            cell = [[UITableViewCell alloc] 
                                     initWithStyle:UITableViewCellStyleDefault 
                                     reuseIdentifier:cellPass];
//                            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
                            
                            UIImageView *imagePass = [[UIImageView alloc] initWithFrame:CGRectMake(10, 5, 50, 50)];
                            [imagePass setImage:[UIImage imageBeforeiOS7:imgPassIcon equaliOS7:imgPassIconFlat]];
                            
                            [cell.contentView addSubview:imagePass];
                            
                            
                            UILabel *titlePass = [[UILabel alloc] 
                                            initWithFrame:CGRectMake(80, 14, 100, 30)];
                            [titlePass setTextColor:titleColor1];   
                            [titlePass setFont:font1];
                            [titlePass setBackgroundColor:[UIColor clearColor]];
                            [titlePass setText:[dictionaryInitialText objectForKey:kPass]];           
                            
                            [cell.contentView addSubview:titlePass];
                            
                            
                            UILabel *valuePass = [[UILabel alloc] 
                                            initWithFrame:CGRectMake(210, 14, 40, 30)];
                            [valuePass setTextColor:valueColor];   
                            [valuePass setFont:font1];
                            [valuePass setBackgroundColor:[UIColor clearColor]];
                            [valuePass setTextAlignment:NSTextAlignmentCenter];
                            [valuePass setTag:tagPass];
                            
                            [cell.contentView addSubview:valuePass];
                            
                            
                            UIButton *btnDetailArrow = [UIButton buttonWithType:UIButtonTypeCustom];
                            [btnDetailArrow setFrame:CGRectMake(265, 17, 25, 25)];
                            [btnDetailArrow setBackgroundImage:[UIImage imageNamed:detailArrowImage] forState:UIControlStateNormal];
                            [btnDetailArrow addTarget:self action:@selector(btnDetailArrowPressed:) forControlEvents:UIControlEventTouchUpInside];
                            [btnDetailArrow setTag:tagDetailPass];
                            
                            [cell.contentView addSubview:btnDetailArrow];
                        }
                    }
                        break;
                        
                    case 1: { // init Fail row   
                        cell = [tableView dequeueReusableCellWithIdentifier:
                                cellFail];
                        if(cell ==nil) {
                            cell = [[UITableViewCell alloc] 
                                     initWithStyle:UITableViewCellStyleDefault 
                                     reuseIdentifier:cellFail] ;
//                            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
                            
                            UIImageView *imageFail = [[UIImageView alloc] initWithFrame:CGRectMake(10, 5, 50, 50)];
                            [imageFail setImage:[UIImage imageBeforeiOS7:imgFailIcon equaliOS7:imgFailIconFlat]];
                            
                            [cell.contentView addSubview:imageFail];
                            
                            
                            UILabel *titleFail = [[UILabel alloc] 
                                            initWithFrame:CGRectMake(80, 14, 100, 30)];
                            [titleFail setTextColor:titleColor1];   
                            [titleFail setFont:font1];
                            [titleFail setBackgroundColor:[UIColor clearColor]];
                            [titleFail setText:[dictionaryInitialText objectForKey:kFail]];           
                            
                            [cell.contentView addSubview:titleFail];
                            
                            
                            UILabel *valueFail = [[UILabel alloc] 
                                            initWithFrame:CGRectMake(210, 14, 40, 30)];
                            [valueFail setTextColor:valueColor];   
                            [valueFail setFont:font1];
                            [valueFail setBackgroundColor:[UIColor clearColor]];
                            [valueFail setTextAlignment:NSTextAlignmentCenter];
                            [valueFail setTag:tagFail];
                            
                            [cell.contentView addSubview:valueFail];
                            
                            
                            UIButton *btnDetailArrow = [UIButton buttonWithType:UIButtonTypeCustom];
                            [btnDetailArrow setFrame:CGRectMake(265, 17, 25, 25)];
                            [btnDetailArrow setBackgroundImage:[UIImage imageNamed:detailArrowImage] forState:UIControlStateNormal];
                            [btnDetailArrow addTarget:self action:@selector(btnDetailArrowPressed:) forControlEvents:UIControlEventTouchUpInside];
                            [btnDetailArrow setTag:tagDetailFail];
                            
                            [cell.contentView addSubview:btnDetailArrow];
                        }
                    }
                        break;
                        
                    default:
                        break;
                }                                
            }
                break;
                
            default:
                break;
        }
    }
    
    UILabel *lblAheadSchedule, *lblRoomPending, *lblRoomCompleted, *lblPass, *lblFail;
    
    lblAheadSchedule = (UILabel *) [cell viewWithTag:tagAheadSchedule];
    lblRoomPending = (UILabel *) [cell viewWithTag:tagRoomPending];
    lblRoomCompleted = (UILabel *) [cell viewWithTag:tagRoomCompleted];
    lblPass = (UILabel *) [cell viewWithTag:tagPass];
    lblFail = (UILabel *) [cell viewWithTag:tagFail];
    
    
    //set ahead time of attendant
    [lblAheadSchedule setText:[attendantData objectForKey:kTime]];
    
    //only have 1 row data
    NSDictionary *rowData = [listDisplayData objectAtIndex:0];
    
    [lblRoomPending setText:[rowData objectForKey:kValueRoomPending]];
    [lblRoomCompleted setText:[rowData objectForKey:kValueRoomCompleted]];
    [lblPass setText:[rowData objectForKey:kValuePass]];
    [lblFail setText:[rowData objectForKey:kValueFail]];
    
    return cell;
}

#pragma mark - === UITableView Delegate Methods ===
#pragma mark
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.section) {
        case 0:
        {
            switch (indexPath.row) {
                case 0:
                {
                    //do nothing
                    
                }
                    break;
                    
                case 1:
                {
                    //PENDING
                    [self handleShowListOfRoomAssignmentWithType:tagDetailRoomPending];
                }
                    break;
                    
                case 2:
                {
                    //COMPLETED
                    [self handleShowListOfRoomAssignmentWithType:tagDetailRoomCompleted];
                }
                    break;
                    
                default:
                    break;
            }
        }
            break;
            
        case 1:
        {
            switch (indexPath.row) {
                case 0:
                {
                    //PASS
                    [self handleShowListOfRoomAssignmentWithType:tagDetailPass];
                }
                    break;
                    
                case 1:
                {
                    //FAIL
                    [self handleShowListOfRoomAssignmentWithType:tagDetailFail];
                }
                    break;
                    
                default:
                    break;
            }
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - === Handle Topbar Methods ===
#pragma mark
-(void)loadTopbarView {
    TopbarViewV2 *topbar = [[TopbarViewV2 alloc] initViewWithSuperModel:[[CommonVariable sharedCommonVariable] superRoomModel]];
    [topbar setTag:tagTopbarView];
    [self.view addSubview:topbar];
    [topbar setHidden:YES];
    
    [topbar setAlpha:0.0];
    [self addButtonHandleShowHideTopbar];
}

-(void)showTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationWillStartSelector:@selector(setHiddenTopbarView)];
    
    f = topbar.frame;
    f.origin.y = 0;
    [topbar setFrame:f];
    [topbar setAlpha:1.0];
    
    [self adjustShowForViews];
    
    [UIView commitAnimations];
}

-(void)hideTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDidStopSelector:@selector(setHiddenTopbarView)];
    [UIView setAnimationDelegate:self];
    
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    [topbar setAlpha:0.0];
    
    [self adjustRemoveForViews];
    
    [UIView commitAnimations];
}

-(void)setHiddenTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    [topbar setHidden:!topbar.hidden];
}

-(TopbarViewV2 *)getTopBarView {
    return (TopbarViewV2 *)[self.view viewWithTag:tagTopbarView];
}

-(void)addButtonHandleShowHideTopbar {
//    UIButton *titleBarButton = [[UIButton alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
//    
//    titleBarButton.titleLabel.backgroundColor = [UIColor clearColor];
//    [titleBarButton.titleLabel setFont: FONT_BUTTON_TOPBAR];
//    titleBarButton.titleLabel.textAlignment = NSTextAlignmentCenter;
//    titleBarButton.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
//    [titleBarButton.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
//    [titleBarButton.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
//    [titleBarButton addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
//    [titleBarButton setTitle:[[LanguageManagerV2 sharedLanguageManager] getFind] forState:UIControlStateNormal];
//    self.navigationItem.titleView = titleBarButton;
    
    
    UIView *vBarButton = [[UIView alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
    UIButton *titleBarButtonFirst = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 200, 30)];
    
    titleBarButtonFirst.titleLabel.backgroundColor = [UIColor clearColor];
    [titleBarButtonFirst.titleLabel setFont: FONT_BUTTON_TOPBAR];
    titleBarButtonFirst.titleLabel.textAlignment = NSTextAlignmentCenter;
    titleBarButtonFirst.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
    
    [titleBarButtonFirst.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
    [titleBarButtonFirst.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
    [titleBarButtonFirst addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    [titleBarButtonFirst setTitle:[[LanguageManagerV2 sharedLanguageManager] getFind] forState:UIControlStateNormal];
    
    [vBarButton addSubview:titleBarButtonFirst];
    
    
    UIButton *titleBarButtonSecond = [[UIButton alloc] initWithFrame:CGRectMake(0, 22, 200, 22)];
    [titleBarButtonSecond setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [titleBarButtonSecond.titleLabel setFont:FONT_BUTTON_TOPBAR_SECOND];
    [titleBarButtonSecond setTitle: [attendantData objectForKey:kAttendantName] forState:UIControlStateNormal];
    [titleBarButtonSecond addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    [vBarButton addSubview:titleBarButtonSecond];
    
    self.navigationItem.titleView = vBarButton;
    //Move title to center vertical
    CGRect titleViewBounds = self.navigationItem.titleView.bounds;
    int moveLeftTitle = 0;
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        moveLeftTitle = 10;
    }
    self.navigationItem.titleView.bounds = CGRectMake(titleViewBounds.origin.x + moveLeftTitle, Y_TOPBAR_TITLE, titleViewBounds.size.width, titleViewBounds.size.height);
}

-(void)topbarTapped:(UIButton *)sender {
    TopbarViewV2 *topbar = [self getTopBarView];
    [sender setEnabled:NO];
    if (topbar.isHidden) {
        [self showTopbarView];
    } else {
        [self hideTopbarView];
    }
    [self performSelector:@selector(topbarButtonEnable:) withObject:sender afterDelay:0.3];
}

-(void) topbarButtonEnable:(UIButton *) topbarBtn {
    [topbarBtn setEnabled:YES];
}

-(void)adjustShowForViews {
    //adjust all views when show 
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    CGRect ftbv = controlsView.frame;
    
    [controlsView setFrame:CGRectMake(0, f.size.height, 320, ftbv.size.height)];
    
    ftbv = tbvContent.frame;
//    [tbvContent setFrame:CGRectMake(0, f.size.height + controlsView.frame.size.height, 320, ftbv.size.height - f.size.height)];
    
    [tbvContent setFrame:CGRectMake(0, f.size.height, 320, ftbv.size.height - f.size.height)];
}

-(void)adjustRemoveForViews { 
    //adjust all views when hide
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    CGRect ftbv = controlsView.frame;
    [controlsView setFrame:CGRectMake(0, 0, 320, ftbv.size.height)];
    
    ftbv = tbvContent.frame;
//    [tbvContent setFrame:CGRectMake(0, controlsView.frame.size.height, 320, ftbv.size.height + f.size.height)];
    
    [tbvContent setFrame:CGRectMake(0, 3, 320, ftbv.size.height + f.size.height)];
}

#pragma mark - === Handle Back Button ===
#pragma mark

-(void)backBarPressed {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)addBackButton {
    //back button navigation
    self.navigationItem.leftBarButtonItem = [MyNavigationBarV2 createBackButtonWithTarget:self selector:@selector(backBarPressed) forControlEvents:UIControlEventTouchUpInside];
}

@end
