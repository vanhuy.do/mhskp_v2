//
//  GuestInfoViewV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GuestInfoModelV2.h"
#import "UserManagerV2.h"
#import "AddJobGuestInfoModelV2.h"
#import "ehkConvert.h"
#import "GuestPreferenceViewV2.h"

@interface GuestInfoViewV2 : UIViewController<UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate,GuestPreferenceViewV2delegate>{
    NSString *guestName;
    NSString *houseKeeperName;
    NSString *languagePref;
    NSString *checkIn;
    NSString *checkOut;
    NSString *guestPreference;
    NSString *vipStatus;
    
    IBOutlet  UITableView *tbvGuestInfo;
    CGPoint *cgPoint;
    
    BOOL statusEditing;
    NSIndexPath *indexPathSelected;
    int rowSelectedEdit;
    
    //flag reload data when sync done
    BOOL isReloadDataWhenSyncDone;
    
    BOOL userCanInteraction;
    
    BOOL isSaved;
    
    __unsafe_unretained UIViewController *parentViewController;
    
    BOOL isAlreadyLoadProfileNote;
    NSMutableArray *listProfileNoteTypes;
    NSMutableDictionary *listProfileNotes;
}

@property (nonatomic, readwrite) BOOL isSaved;
@property (nonatomic, assign) UIViewController *parentViewController;
@property (nonatomic, readwrite) BOOL userCanInteraction;
@property (nonatomic, strong) IBOutlet  UITableView *tbvGuestInfo;

@property (nonatomic,strong) NSString *languagePref;
@property (nonatomic,strong) NSString *checkIn;
@property (nonatomic,strong) NSString *checkOut;
@property (nonatomic,strong) NSString *guestPreference;
@property (nonatomic,strong) NSString *guestName;
@property (nonatomic,strong) NSString *houseKeeperName;
@property (nonatomic,strong) NSString *vipStatus;
@property (nonatomic) BOOL statusEditing;
@property (nonatomic) CGPoint *cgPoint;
@property (nonatomic, strong) GuestInfoModelV2 *dataModel;
@property (nonatomic, strong) AddJobGuestInfoModelV2 *addJobGuestInfo;
@property (nonatomic) int isPushFrom;
@property (nonatomic, strong) NSIndexPath *indexPathSelected;

@property (nonatomic, readwrite) BOOL isReloadDataWhenSyncDone;

@property (nonatomic, strong) NSString *textdataGuestPreference;
@property (nonatomic, strong) NSString *roomNumber;
@property (nonatomic, strong) UIViewController *parentVC;

-(BOOL) checkSaveGuestInfo;
-(void) setCaptionsView;
-(void) loadDataGuestInfo;
-(void)saveData;
-(void) saveDataWithoutHUD;
-(void) reloadView;

@end


