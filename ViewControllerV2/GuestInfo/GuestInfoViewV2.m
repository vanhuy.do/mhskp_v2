//
//  GuestInfoViewV2.m
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GuestInfoViewV2.h"
#import "MBProgressHUD.h"
#import "RoomManagerV2.h"
#import "NetworkCheck.h"
#import "TasksManagerV2.h"
#import "GuestInfoManagerV2.h"
#import "ProfileNoteViewV2.h"
#import "DeviceManager.h"
#import "HomeViewV2.h"
#import "CustomBadge.h"
#import "CustomAlertViewV2.h"

#define imgStar     @"guest_star.png"
#define imgNoPhoto  @"no_photo.png"

@implementation GuestInfoViewV2
@synthesize languagePref, cgPoint, checkIn, checkOut, guestName, guestPreference, houseKeeperName, vipStatus, statusEditing, dataModel, indexPathSelected, isReloadDataWhenSyncDone, tbvGuestInfo, textdataGuestPreference;
@synthesize userCanInteraction;
@synthesize parentViewController;
@synthesize isSaved;
@synthesize addJobGuestInfo, roomNumber, parentVC, isPushFrom;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        userCanInteraction = YES;
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        //Clear redundant view for header, footter
        CGRect frameHeaderFooter = tbvGuestInfo.tableHeaderView.frame;
        frameHeaderFooter.size.height = 0.1f;
        UIView *headerView = [[UIView alloc] initWithFrame:frameHeaderFooter];
        [tbvGuestInfo setTableHeaderView:headerView];
        
        frameHeaderFooter = tbvGuestInfo.tableFooterView.frame;
        frameHeaderFooter.size.height = 0.1f;
        UIView *footerView = [[UIView alloc] initWithFrame:frameHeaderFooter];
        [tbvGuestInfo setTableFooterView:footerView];
    }
    
    self.tbvGuestInfo.backgroundColor = [UIColor clearColor];
    [self setCaptionsView];
    isAlreadyLoadProfileNote = NO;
    //[self loadDataGuestInfo];
    isSaved = YES;
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void)loadDataGuestInfo{
    //enable flag to announce, is loading data
    [[HomeViewV2 shareHomeView] waitingLoadingData];
    
    //GuestInfoAdapterV2 * manager = [[GuestInfoAdapterV2 alloc] init];
    
   //NSString *roomId = [RoomManagerV2 getCurrentRoomNo];
    //self.dataModel = [manager loadGuestInfoDataByRoomID:roomId];;
    
    if (dataModel.guestImage == nil) {
        dataModel.guestImage = UIImagePNGRepresentation([UIImage imageNamed:@"no_photo.png"]);
    }
    
    [tbvGuestInfo reloadData];
    
    
    //end loading data
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
}


#pragma mark - UITableView Delegate methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if(dataModel.guestPreferenceCode.length > 0 || listProfileNoteTypes.count > 0){
        return 2;
    }
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(section == 0){
        return 5;
    } else if(section == 1){ //Guest Preference and profile note
        
        return 1 + [listProfileNoteTypes count]; 
    }
    
    return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    // init layout for tableview
    static NSString *cellTableIdentifier = @"Cell";
    static NSString *cellTxfGuestPrefIdentifier = @"cell2";
    static NSString *cellLangIdentifier = @"langIdentifier";
    static NSString *cellCheckInIdentifier = @"CheckInIdentifier";
    static NSString *cellCheckOutIdentifier = @"CheckOutIdentifier";
    static NSString *cellNumberOfGuestIdentifier = @"NumberOfGuestIdentifier";
    static NSString *cellProfileNoteTypeIdentifier = @"cellProfileNoteType";
    
    int tagGuestImage = 1;
    int tagGuestImageLabel = 2;
    int tagGuestNameLabel = 3;
    int tagGuestNameValue = 4;
    int tagGuestLangLabel = 5;
    int tagGuestLangValue = 6;
    //int tagGuestCheckInLabel = 7;
    int tagGuestCheckInValue = 8;
    //int tagGuestCheckOutLabel = 9;
    int tagGuestCheckOutValue = 10;
    int tagGuestHouseKeeperLabel = 11;
    int tagGuestHouseKeeperValue = 12;
    int tagGuestVipNumber = 13;
    int tagGuestNumberLabel = 14;
    int tagGuestNumberValue = 15;
    int tagProfileNote = 16;
    int tagBadgNumberProfile = 17;
    int tagGuestTypeLabel = 18;
    int tagGuestTypeValue = 19;
    
    UITableViewCell *cell1;
    UITableViewCell *cellTxfGuestPref;
    UITableViewCell *cellLang;
    UITableViewCell *cellCheckIn;
    UITableViewCell *cellCheckOut;
    UITableViewCell *cellNumberGuest;
    UITableViewCell *cellProfileNoteType;
    
    if(indexPath.section == 1)
    {
        if(indexPath.row == 0) //GUEST PREFERENCE
        {
            cellTxfGuestPref=[tbvGuestInfo dequeueReusableCellWithIdentifier:cellTxfGuestPrefIdentifier];
            if(cellTxfGuestPref == nil){
                cellTxfGuestPref = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellTxfGuestPrefIdentifier];
                [cellTxfGuestPref setSelectionStyle:UITableViewCellSelectionStyleNone];
                
                // display label control in table view
                CGRect guestPrefLabelRect = CGRectMake(10, 1, 150, 30);
                UILabel *guestPrefLabel = [[UILabel alloc] initWithFrame:guestPrefLabelRect];
                guestPrefLabel.textAlignment = NSTextAlignmentLeft;
                guestPrefLabel.font = [UIFont boldSystemFontOfSize:12];
                guestPrefLabel.text = self.guestPreference;
                guestPrefLabel.tag=16;
                guestPrefLabel.textColor=[UIColor colorWithRed:6.0/255 green:62.0/255 blue:127.0/255 alpha:1];
                guestPrefLabel.backgroundColor = [UIColor clearColor];
                [cellTxfGuestPref.contentView addSubview:guestPrefLabel];
                
                //diaplay value gues preference of this cell
                CGRect guestPrefValueRect = CGRectMake(120, 1, 150 , 35);
                UILabel *txtguestPref=[[UILabel alloc] initWithFrame:guestPrefValueRect];
                [txtguestPref setBackgroundColor:[UIColor clearColor]];
                
                txtguestPref.font= [UIFont boldSystemFontOfSize:12];
                txtguestPref.tag=17;
                //txtguestPref.delegate = self;
                [txtguestPref setTextColor:[UIColor colorWithRed:51.0/255 green:51.0/255 blue:51.0/255 alpha:1]];
                
                [txtguestPref setText:dataModel.guestPreferenceCode];
                
                [txtguestPref setLineBreakMode:NSLineBreakByTruncatingTail];
                [txtguestPref setNumberOfLines:3];
                
                
                [cellTxfGuestPref.contentView addSubview:txtguestPref];
                
                // add image of guest to cell of table
                //UIImage *imageGuestArrow=[UIImage imageNamed:imgRowGray];
                //UIImageView *imgView=[[UIImageView alloc] initWithImage:imageGuestArrow];
                //imgView.frame=CGRectMake(270, 7, 25 , 25);
                //imgView.tag = 1;
                //[cellTxfGuestPref.contentView addSubview:imgView];
                
            }
            
            
            UILabel *txtguestPref = (UILabel*)[cellTxfGuestPref.contentView viewWithTag:17];
            txtguestPref.font= [UIFont boldSystemFontOfSize:12];
            txtguestPref.tag=17;
            [txtguestPref setUserInteractionEnabled:NO];
            //set user can interaction when editing is YES
            if (self.statusEditing) {
                [txtguestPref setUserInteractionEnabled:YES];
            }
            //set guestPrefValue from roomModel of guestInfoModel
            [txtguestPref setText:@""];
            
            //whether use guest info of Additional Job or Room Assignment
            [txtguestPref setText:dataModel.guestPreferenceCode];
    
            
            CGSize t = CGSizeZero;
            t = [dataModel.guestPreferenceCode sizeWithFont:[UIFont boldSystemFontOfSize:12] constrainedToSize:CGSizeMake(150, 9999999) lineBreakMode:NSLineBreakByWordWrapping];
            CGRect guestPrefValueRect = CGRectMake(120, 9, 150, t.height > 66 ? 45 : t.height);
            
            [txtguestPref setFrame:guestPrefValueRect];
            
            if (userCanInteraction == NO) {
                [cellTxfGuestPref setUserInteractionEnabled:NO];
            }
            
            return cellTxfGuestPref;
        }
        else //PROFILE NOTES
        {
            cellProfileNoteType = [tbvGuestInfo dequeueReusableCellWithIdentifier:cellProfileNoteTypeIdentifier];
            if(cellProfileNoteType == nil){
                cellProfileNoteType = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellProfileNoteTypeIdentifier];
                [cellProfileNoteType setSelectionStyle:UITableViewCellSelectionStyleNone];
                
                // display label control in table view
                CGRect profileNoteLabelRect = CGRectMake(10, 1, 290, 50);
                UILabel *profileNoteLabel = [[UILabel alloc] initWithFrame:profileNoteLabelRect];
                profileNoteLabel.textAlignment = NSTextAlignmentLeft;
                profileNoteLabel.font = [UIFont boldSystemFontOfSize:14];
                profileNoteLabel.tag = tagProfileNote;
                profileNoteLabel.textColor=[UIColor colorWithRed:6.0/255 green:62.0/255 blue:127.0/255 alpha:1];
                profileNoteLabel.backgroundColor = [UIColor clearColor];
                [cellProfileNoteType.contentView addSubview:profileNoteLabel];
                
                UIColor *badgeFrameColor = nil;
                BOOL isShining = NO;
                if(SYSTEM_VERSION_LESS_THAN(@"7.0")){
                    badgeFrameColor = [UIColor whiteColor];
                    isShining = YES;
                } else {
                    badgeFrameColor = [UIColor clearColor];
                }
                
                CustomBadge *customBadge = [CustomBadge customBadgeWithString:nil
                                                              withStringColor:[UIColor whiteColor]
                                                               withInsetColor:[UIColor redColor]
                                                               withBadgeFrame:YES
                                                          withBadgeFrameColor:badgeFrameColor
                                                                    withScale:1.0
                                                                  withShining:isShining];
                customBadge.tag = tagBadgNumberProfile;
                [customBadge setFrame:CGRectMake(230 ,15, 27, 25)];
                [cellProfileNoteType.contentView addSubview:customBadge];
                
                // add image of guest to cell of table
                UIImage *imageGuestArrow = [UIImage imageNamed:imgRowGray];
                UIImageView *imgView = [[UIImageView alloc] initWithImage:imageGuestArrow];
                imgView.frame = CGRectMake(270, 15, 25 , 25);
                [cellProfileNoteType.contentView addSubview:imgView];
                
            }
            
            ProfileNoteTypeV2 *curProfileNoteType = [listProfileNoteTypes objectAtIndex:indexPath.row - 1];
            UILabel *profileNoteLabel = (UILabel*)[cellProfileNoteType viewWithTag:tagProfileNote];
            [profileNoteLabel setText:curProfileNoteType.profilenote_type_description];
            
            NSMutableArray *profileNotes = [listProfileNotes objectForKey:[NSNumber numberWithInteger:curProfileNoteType.profilenote_type_id]];
            CustomBadge *customBadge = (CustomBadge*)[cellProfileNoteType viewWithTag:tagBadgNumberProfile];
            [customBadge setBadgeText:[NSString stringWithFormat:@"%d", (int)[profileNotes count]]];
            
            [cellProfileNoteType setUserInteractionEnabled:YES];
            
            return cellProfileNoteType;
        }
    }
    else if(indexPath.section == 0)
    {
        switch (indexPath.row) {
            case 0:
            {
                UIImageView *guestImageView = nil;
                UILabel *lblGuestImage = nil;
                UILabel *guestNameValue = nil;
                UILabel *houseKeeprNameValue = nil;
                UILabel *lblNumberVIP = nil;
                UILabel *lblGuestTypeValue = nil;
                
                cell1 = [tbvGuestInfo dequeueReusableCellWithIdentifier:cellTableIdentifier];
                if(cell1 == nil){
                    cell1 = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellTableIdentifier];
                    [cell1 setSelectionStyle:UITableViewCellSelectionStyleNone];
                    
                    guestImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imgNoPhoto]];
                    guestImageView.frame = CGRectMake(10, 10, 85 , 100);
                    guestImageView.tag = tagGuestImage;
                    [cell1.contentView addSubview:guestImageView];
                    
                    lblGuestImage = [[UILabel alloc] initWithFrame:CGRectMake(17, 35, 70, 50)];
                    
                    if (dataModel.guestImage == nil) {
                        [lblGuestImage setText:[L_no_guest_photo currentKeyToLanguage]];
                    }
                    
                    [lblGuestImage setTextAlignment:NSTextAlignmentCenter];
                    [lblGuestImage setBackgroundColor:[UIColor clearColor]];
                    [lblGuestImage setFont:[UIFont fontWithName:@"Arial-BoldMT" size:12]];
                    [lblGuestImage setNumberOfLines:2];
                    [lblGuestImage setLineBreakMode:NSLineBreakByWordWrapping];
                    [lblGuestImage setTextColor:[UIColor colorWithRed:181.0/255 green:181.0/255 blue:181.0/255 alpha:1]];
                    lblGuestImage.tag = tagGuestImageLabel;
                    [cell1.contentView addSubview:lblGuestImage];
                    
                    
                    // add label name of guest to cell of table
                    CGRect guestTypeLabelRect = CGRectMake(100, 0, 150 , 25);
                    UILabel *guestTypeLabel = [[UILabel alloc] initWithFrame:guestTypeLabelRect];
                    guestTypeLabel.textAlignment = NSTextAlignmentLeft;
                    guestTypeLabel.font = [UIFont boldSystemFontOfSize:12];
                    guestTypeLabel.tag = tagGuestTypeLabel;
                    guestTypeLabel.text = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_guest_type];
                    guestTypeLabel.textColor = [UIColor colorWithRed:6.0/255 green:62.0/255 blue:127.0/255 alpha:1];
                    [guestTypeLabel setBackgroundColor:[UIColor clearColor]];
                    [cell1.contentView addSubview:guestTypeLabel];
                    
                    // add value name of guest to cell of table
                    CGRect guesTypeValueRect = CGRectMake(100, 13, 150 , 30);
                    lblGuestTypeValue = [[UILabel alloc] initWithFrame:guesTypeValueRect];
                    lblGuestTypeValue.textAlignment = NSTextAlignmentLeft;
                    lblGuestTypeValue.font = [UIFont boldSystemFontOfSize:12];
                    lblGuestTypeValue.tag = tagGuestTypeValue;
                    [lblGuestTypeValue setAdjustsFontSizeToFitWidth:YES];
                    [lblGuestTypeValue setBackgroundColor:[UIColor clearColor]];
                    [lblGuestTypeValue setTextColor:[UIColor colorWithRed:51.0/255 green:51.0/255 blue:51.0/255 alpha:1]];
                    
                    if(dataModel.guestType == GuestTypeCurrent) {
                        lblGuestTypeValue.text = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_guest_in_house];
                    } else {
                        lblGuestTypeValue.text = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_guest_arriving];
                    }
                    
                    [cell1.contentView addSubview:lblGuestTypeValue];
                    
                    // add label name of guest to cell of table
                    CGRect guestNameLabelRect = CGRectMake(100, 30, 150 , 25);
                    UILabel *guestNameLabel = [[UILabel alloc] initWithFrame:guestNameLabelRect];
                    guestNameLabel.textAlignment = NSTextAlignmentLeft;
                    guestNameLabel.font = [UIFont boldSystemFontOfSize:12];
                    guestNameLabel.tag = tagGuestNameLabel;
                    guestNameLabel.text = self.guestName;
                    guestNameLabel.textColor= [UIColor colorWithRed:6.0/255 green:62.0/255 blue:127.0/255 alpha:1];
                    [guestNameLabel setBackgroundColor:[UIColor clearColor]];
                    [cell1.contentView addSubview:guestNameLabel];
                    
                    // add value name of guest to cell of table
                    CGRect guestNameValueRect = CGRectMake(100, 40, 150 , 30);
                    guestNameValue = [[UILabel alloc] initWithFrame:guestNameValueRect];
                    guestNameValue.textAlignment = NSTextAlignmentLeft;
                    guestNameValue.font = [UIFont boldSystemFontOfSize:12];
                    guestNameValue.tag = tagGuestNameValue;
                    [guestNameValue setAdjustsFontSizeToFitWidth:YES];
                    [guestNameValue setBackgroundColor:[UIColor clearColor]];
                    [guestNameValue setTextColor:[UIColor colorWithRed:51.0/255 green:51.0/255 blue:51.0/255 alpha:1]];
                    [guestNameValue setNumberOfLines:2];
                    
                    [cell1.contentView addSubview:guestNameValue];
                    
                    // add label name of housekeeperName to cell of table
                    CGRect houseKepperLabelRect = CGRectMake(100, 65, 150 , 25);
                    UILabel *houseKeeprNameLabel = [[UILabel alloc] initWithFrame:houseKepperLabelRect];
                    houseKeeprNameLabel.textAlignment = NSTextAlignmentLeft;
                    houseKeeprNameLabel.font = [UIFont boldSystemFontOfSize:12];
                    houseKeeprNameLabel.tag = tagGuestHouseKeeperLabel;
                    houseKeeprNameLabel.text = self.houseKeeperName;
                    houseKeeprNameLabel.textColor=[UIColor colorWithRed:6.0/255 green:62.0/255 blue:127.0/255 alpha:1];
                    [houseKeeprNameLabel setBackgroundColor:[UIColor clearColor]];
                    [cell1.contentView addSubview:houseKeeprNameLabel];
                    
                    houseKeeprNameValue = [[UILabel alloc] init];
                    houseKeeprNameValue.textAlignment = NSTextAlignmentLeft;
                    houseKeeprNameValue.font = [UIFont boldSystemFontOfSize:12];
                    houseKeeprNameValue.tag = tagGuestHouseKeeperValue;
                    [houseKeeprNameValue setTextColor:[UIColor colorWithRed:51.0/255 green:51.0/255 blue:51.0/255 alpha:1]];
                    [houseKeeprNameValue setLineBreakMode:NSLineBreakByWordWrapping];
                    [houseKeeprNameValue setBackgroundColor:[UIColor clearColor]];
                    
                    [cell1.contentView addSubview:houseKeeprNameValue];
                    
                    if(![dataModel.guestVIPCode isEqualToString:@"(null)"] && ![dataModel.guestVIPCode isEqualToString:@""] && ![dataModel.guestVIPCode isEqualToString:@"0"] && (dataModel.guestVIPCode != nil)){
                        //Image VIP
                        UIImage *imageGuestStar=[UIImage imageNamed:imgStar];
                        UIImageView *imgViewGuestStar=[[UIImageView alloc] initWithImage:imageGuestStar];
                        imgViewGuestStar.frame=CGRectMake(230, 10, 47 , 45);
                        imgViewGuestStar.tag = 1;
                        [cell1.contentView addSubview:imgViewGuestStar];
                        
                        //lable number VIP
                        lblNumberVIP = [[UILabel alloc] initWithFrame:CGRectMake(230, 22, 50, 20)];
                        [lblNumberVIP setTextAlignment:NSTextAlignmentCenter];
                        [lblNumberVIP setBackgroundColor:[UIColor clearColor]];
                        [lblNumberVIP setTextColor:[UIColor colorWithRed:6.0/255 green:62.0/255 blue:127.0/255 alpha:1]];
                        [lblNumberVIP setFont:[UIFont fontWithName:@"Arial-BoldMT" size:18]];
                        [lblNumberVIP setMinimumFontSize:12.0f];
                        lblNumberVIP.tag = tagGuestVipNumber;
                        [cell1.contentView addSubview:lblNumberVIP];
                        
                        //label text VIP
                        UILabel *lblTextVIP = [[UILabel alloc] initWithFrame:CGRectMake(230, 55, 50, 30)];
                        lblTextVIP.text = [[LanguageManagerV2 sharedLanguageManager] getVIP];
                        [lblTextVIP setTextAlignment:NSTextAlignmentCenter];
                        [lblTextVIP setBackgroundColor:[UIColor clearColor]];
                        [lblTextVIP setTextColor:[UIColor colorWithRed:6.0/255 green:62.0/255 blue:127.0/255 alpha:1]];
                        [lblTextVIP setFont:[UIFont fontWithName:@"Arial-BoldMT" size:18]];
                        [cell1.contentView addSubview:lblTextVIP];
                        
                        //SET VIP NUMBER
                        if(lblNumberVIP == nil) {
                            lblNumberVIP = (UILabel*)[cell1.contentView viewWithTag:tagGuestVipNumber];
                        }
                        
                        lblNumberVIP.text = dataModel.guestVIPCode;
                    }
                    
                }
                
                //SET GUEST IMAGE
                if(guestImageView == nil){
                    guestImageView = (UIImageView*)[cell1.contentView viewWithTag:tagGuestImage];
                }
                if (dataModel.guestImage) {
                    [guestImageView setImage: [UIImage imageWithData:dataModel.guestImage]];
                }
                
                
                //SET GUEST NAME
                if(guestNameValue == nil){
                    guestNameValue = (UILabel*)[cell1.contentView viewWithTag:tagGuestNameValue];
                }
                // set guestNameValue from dataModel
                [guestNameValue setText:dataModel.guestName];
                
                if (userCanInteraction == NO) {
                    [cell1 setUserInteractionEnabled:NO];
                }
                
                //SET HOUSEKEEPER NAME
                if(houseKeeprNameValue == nil){
                    houseKeeprNameValue = (UILabel*)[cell1.contentView viewWithTag:tagGuestHouseKeeperValue];
                }
                
                // add value name of housekeeperName to cell of table
                CGRect houseKepperValueRect = CGRectMake(100, 80, 150 , 30);
                CGSize t = CGSizeZero;
                t = [dataModel.guestHousekeeperName sizeWithFont:[UIFont boldSystemFontOfSize:12] constrainedToSize:CGSizeMake(150, 9999999) lineBreakMode:NSLineBreakByWordWrapping];
                
                if (t.height > 15) {
                    houseKepperValueRect.size.height = t.height + 10;
                }
                
                [houseKeeprNameValue setNumberOfLines:t.height/15];
                [houseKeeprNameValue setFrame:houseKepperValueRect];
                //set houseKeeprNameValue from UserManager currentUser
                [houseKeeprNameValue setText:dataModel.guestHousekeeperName];
                
                return cell1;
            }
                break;
                
            case 1: // Number of Guest
            {
                UILabel *numberGuestValue = nil;
                
                cellNumberGuest = [tbvGuestInfo dequeueReusableCellWithIdentifier:cellNumberOfGuestIdentifier];
                if(cellNumberGuest == nil){
                    cellNumberGuest = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellNumberOfGuestIdentifier];
                    [cellNumberGuest setSelectionStyle:UITableViewCellSelectionStyleNone];
                    
                    
                    // display label control in table view
                    CGRect numberGuestLabelRect = CGRectMake(10, 1, 150 , 30);
                    UILabel *numberGuestLabel = [[UILabel alloc] initWithFrame:numberGuestLabelRect];
                    numberGuestLabel.textAlignment = NSTextAlignmentLeft;
                    numberGuestLabel.font = [UIFont boldSystemFontOfSize:12];
                    numberGuestLabel.text = [L_no_of_guest currentKeyToLanguage];
                    numberGuestLabel.tag = tagGuestNumberLabel;
                    numberGuestLabel.textColor = [UIColor colorWithRed:6.0/255 green:62.0/255 blue:127.0/255 alpha:1];
                    [numberGuestLabel setBackgroundColor:[UIColor clearColor]];
                    [cellNumberGuest.contentView addSubview:numberGuestLabel];
                    
                    //display value of language cell
                    CGRect numberGuestValueRect = CGRectMake(140, 5, 150 , 30);
                    UILabel *numberGuestValue=[[UILabel alloc] initWithFrame:numberGuestValueRect];
                    numberGuestValue.textAlignment = NSTextAlignmentRight;
                    numberGuestValue.font = [UIFont boldSystemFontOfSize:12];
                    numberGuestValue.tag = tagGuestNumberValue;
                    [numberGuestValue setTextColor:[UIColor colorWithRed:51.0/255 green:51.0/255 blue:51.0/255 alpha:1]];
                    [numberGuestValue setBackgroundColor:[UIColor clearColor]];
                    
                    [numberGuestValue setBackgroundColor:[UIColor clearColor]];
                    [cellNumberGuest.contentView addSubview:numberGuestValue];
                }
                
                if(numberGuestValue == nil){
                    numberGuestValue = (UILabel*)[cellNumberGuest.contentView viewWithTag:tagGuestNumberValue];
                }
                [numberGuestValue setText:dataModel.guestNumber];
                
                if (userCanInteraction == NO) {
                    [cellNumberGuest setUserInteractionEnabled:NO];
                }
                
                return cellNumberGuest;
            }
                break;
            case 2: //Language Preference
            {
                UILabel *languageValue = nil;
                cellLang = [tbvGuestInfo dequeueReusableCellWithIdentifier:cellLangIdentifier];
                if(cellLang == nil){
                    cellLang = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellLangIdentifier];
                    [cellLang setSelectionStyle:UITableViewCellSelectionStyleNone];
                    
                    
                    // display label control in table view
                    CGRect languagePrefLabelRect = CGRectMake(10, 1, 150 , 30);
                    UILabel *languagePrefLabel = [[UILabel alloc] initWithFrame:languagePrefLabelRect];
                    languagePrefLabel.textAlignment = NSTextAlignmentLeft;
                    languagePrefLabel.font = [UIFont boldSystemFontOfSize:12];
                    languagePrefLabel.text = self.languagePref;
                    languagePrefLabel.tag = tagGuestLangLabel;
                    languagePrefLabel.textColor=[UIColor colorWithRed:6.0/255 green:62.0/255 blue:127.0/255 alpha:1];
                    [languagePrefLabel setBackgroundColor:[UIColor clearColor]];
                    [cellLang.contentView addSubview:languagePrefLabel];
                    
                    //display value of language cell
                    CGRect languageValueRect = CGRectMake(140, 10, 150 , 30);
                    languageValue=[[UILabel alloc] initWithFrame:languageValueRect];
                    languageValue.textAlignment = NSTextAlignmentRight;
                    languageValue.font = [UIFont boldSystemFontOfSize:12];
                    languageValue.tag = tagGuestLangValue;
                    [languageValue setTextColor:[UIColor colorWithRed:51.0/255 green:51.0/255 blue:51.0/255 alpha:1]];
                    [languageValue setBackgroundColor:[UIColor clearColor]];
                    
                    [cellLang.contentView addSubview:languageValue];
                    
                    /* Hao Tran - Remove for language reference don't allow edit
                    //edit language with text feild
                    UITextField *txtLanguagePref=[[UITextField alloc] initWithFrame:languageValueRect];
                    txtLanguagePref.textAlignment=NSTextAlignmentRight;
                    txtLanguagePref.font=[UIFont boldSystemFontOfSize:12];
                    txtLanguagePref.tag=10;
                    txtLanguagePref.delegate=self;
                    [txtLanguagePref setUserInteractionEnabled:NO];
                    
                    //set languageValue from dataModel
                    [cellLang.contentView addSubview:txtLanguagePref];
                     */
                }
                
                /* Hao Tran - Remove for language reference don't allow edit
                UITextField *txtLanguagePref=(UITextField*)[cellLang.contentView viewWithTag:10];
                txtLanguagePref.textAlignment=NSTextAlignmentRight;
                
                //whether use guest info of Additional Job or Room Assignment
                if(isPushFrom == IS_PUSHED_FROM_ADD_JOB_LIST || isPushFrom == IS_PUSHED_FROM_ADD_JOB_DETAIL){
                    txtLanguagePref.text = addJobGuestInfo.addjob_guestinfo_lang_pref;
                } else {
                    txtLanguagePref.text = dataModel.guestLangPref;
                }
                txtLanguagePref.font=[UIFont boldSystemFontOfSize:12];
                txtLanguagePref.tag=10;
                txtLanguagePref.delegate=self;
                [txtLanguagePref setUserInteractionEnabled:NO];
                */
                
                //whether use guest info of Additional Job or Room Assignment
                if(languageValue == nil){
                    languageValue = (UILabel*)[cellLang.contentView viewWithTag:tagGuestLangValue];
                }
                [languageValue setText:dataModel.guestLangPref];
                
                if (userCanInteraction == NO) {
                    [cellLang setUserInteractionEnabled:NO];
                }
                
                return cellLang;
                
            }
                break;
            case 3:
            {
                UILabel *checkInValue = nil;
                cellCheckIn = [tbvGuestInfo dequeueReusableCellWithIdentifier:cellCheckInIdentifier];
                if(cellCheckIn == nil){
                    cellCheckIn = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellCheckInIdentifier];
                    [cellCheckIn setSelectionStyle:UITableViewCellSelectionStyleNone];
                    
                    // display label control in table view
                    CGRect checkInLabelRect = CGRectMake(10, 1, 150 , 30);
                    UILabel *checkInLabel = [[UILabel alloc] initWithFrame:checkInLabelRect];
                    checkInLabel.textAlignment = NSTextAlignmentLeft;
                    checkInLabel.font = [UIFont boldSystemFontOfSize:12];
                    checkInLabel.text = self.checkIn;
                    checkInLabel.tag=11;
                    checkInLabel.textColor = [UIColor colorWithRed:6.0/255 green:62.0/255 blue:127.0/255 alpha:1];
                    [checkInLabel setBackgroundColor:[UIColor clearColor]];
                    [cellCheckIn.contentView addSubview:checkInLabel];
                    
                    //display data control in table view
                    CGRect checkInValueRect = CGRectMake(150, 1, 140 , 30);
                    UILabel *checkInValue=[[UILabel alloc] initWithFrame:checkInValueRect];
                    checkInValue.textAlignment = NSTextAlignmentRight;
                    checkInValue.font = [UIFont boldSystemFontOfSize:12];
                    checkInValue.tag = tagGuestCheckInValue;
                    [checkInValue setBackgroundColor:[UIColor clearColor]];
                    [checkInValue setTextColor:[UIColor colorWithRed:51.0/255 green:51.0/255 blue:51.0/255 alpha:1]];
                    [cellCheckIn.contentView addSubview:checkInValue];
                }
                
                if(checkInValue == nil){
                    checkInValue = (UILabel*)[cellCheckIn.contentView viewWithTag:tagGuestCheckInValue];
                }
                
                if(dataModel.guestCheckInTime.length > 0) {
                    //set checkInValue from dataModel
                    NSString *checkinDateStr = nil;
                    NSDate *checkinDate = nil;
                    NSDateFormatter *formater = [[NSDateFormatter alloc] init];
                    [formater setDateFormat:@"yyyyMMdd:HHmmss"];
                    checkinDate = [formater dateFromString:dataModel.guestCheckInTime];
                    [formater setDateFormat:@"dd/MM/yyyy HH:mm"];
                    checkinDateStr = [formater stringFromDate:checkinDate];
                    if([checkinDateStr rangeOfString:@"00:00"].location != NSNotFound)
                    {
                        [formater setDateFormat:@"dd/MM/yyyy"];
                        checkinDateStr = [formater stringFromDate:checkinDate];
                    }
                    [checkInValue setText:checkinDateStr];
                } else {
                    [checkInValue setText:@""];
                    
                }
                
                if (userCanInteraction == NO) {
                    [cellCheckIn setUserInteractionEnabled:NO];
                }
                
                return cellCheckIn;
            }
                break;
            case 4:
            {
                UILabel *checkOutValue = nil;
                cellCheckOut=[tbvGuestInfo dequeueReusableCellWithIdentifier:cellCheckOutIdentifier];
                if(cellCheckOut == nil){
                    cellCheckOut = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellCheckOutIdentifier];
                    [cellCheckOut setSelectionStyle:UITableViewCellSelectionStyleNone];
                    
                    
                    // display label control in table view
                    CGRect checkOutLabelRect = CGRectMake(10, 1, 150 , 30);
                    UILabel *checkOutLabel = [[UILabel alloc] initWithFrame:checkOutLabelRect];
                    checkOutLabel.textAlignment = NSTextAlignmentLeft;
                    checkOutLabel.font = [UIFont boldSystemFontOfSize:12];
                    checkOutLabel.text = self.checkOut;
                    checkOutLabel.tag=14;
                    checkOutLabel.textColor=[UIColor colorWithRed:6.0/255 green:62.0/255 blue:127.0/255 alpha:1];
                    [checkOutLabel setBackgroundColor:[UIColor clearColor]];
                    [cellCheckOut.contentView addSubview:checkOutLabel];
                    
                    //diaplay value check-out of this cell
                    CGRect checkOutValueRect = CGRectMake(150, 1, 140 , 30);
                    UILabel *checkOutValue=[[UILabel alloc] initWithFrame:checkOutValueRect];
                    checkOutValue.textAlignment = NSTextAlignmentRight;
                    checkOutValue.font = [UIFont boldSystemFontOfSize:12];
                    [checkOutValue setTextColor:[UIColor colorWithRed:51.0/255 green:51.0/255 blue:51.0/255 alpha:1]];
                    
                    checkOutValue.tag = tagGuestCheckOutValue;
                    [checkOutValue setBackgroundColor:[UIColor clearColor]];
                    [cellCheckOut.contentView addSubview:checkOutValue];
                }
                
                if(!checkOutValue) {
                    checkOutValue = (UILabel*)[cellCheckOut.contentView viewWithTag:tagGuestCheckOutValue];
                }
                
                if(dataModel.guestCheckOutTime.length > 0) {
                    NSString *checkoutDateStr = nil;
                    NSDate *checkoutDate = nil;
                    NSDateFormatter *formater = [[NSDateFormatter alloc] init];
                    [formater setDateFormat:@"yyyyMMdd:HHmmss"];
                    checkoutDate = [formater dateFromString:dataModel.guestCheckOutTime];
                    
                    [formater setDateFormat:@"dd/MM/yyyy HH:mm"];
                    checkoutDateStr = [formater stringFromDate:checkoutDate];
                    if([checkoutDateStr rangeOfString:@"00:00"].location != NSNotFound)
                    {
                        [formater setDateFormat:@"dd/MM/yyyy"];
                        checkoutDateStr = [formater stringFromDate:checkoutDate];
                    }
                    [checkOutValue setText:checkoutDateStr];
                } else {
                    [checkOutValue setText:@""];
                }
                
                if (userCanInteraction == NO) {
                    [cellCheckOut setUserInteractionEnabled:NO];
                }
                
                return cellCheckOut;
            }
                break;
                return nil;
        }
    }
    return nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.3f;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 2.3f;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return nil;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return nil;
}
-(CGFloat)tableView:(UITableView *)atableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0)
    {
        if(indexPath.row == 0)
        {
            CGSize t = [dataModel.guestHousekeeperName sizeWithFont:[UIFont boldSystemFontOfSize:12] constrainedToSize:CGSizeMake(150, 9999999) lineBreakMode:NSLineBreakByWordWrapping];
            if (t.height > 15) {
                return 120 + t.height;
            }
            return 120.0;
        }
        else
        {
            return 33.0;
        }
    }
    else //section 1: section of Guest Reference
    {
        if(indexPath.row == 0){
            return 66.0;
        } else {
            return 50.0;
        }
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    /*switch (indexPath.row) {
     case 4:
     {
     //can edit in 2 class RoomInfoInspectionV2 or RoomInfoAssignmentV2
     if ([parentViewController isKindOfClass:[RoomInfoInspectionV2 class]] == YES || [parentViewController isKindOfClass:[RoomInfoAssignmentV2 class]] == YES) {
     GuestPreferenceViewV2 *guestPreferenceView = [[GuestPreferenceViewV2 alloc] init];
     [guestPreferenceView setFrame:CGRectMake(-320, 0, 320, 396)];
     [UIView beginAnimations:nil context:nil];
     [UIView setAnimationDuration:0.4];
     [guestPreferenceView setFrame:CGRectMake(0, 0, 320, 396)];
     [self.view.superview addSubview:guestPreferenceView];
     [UIView commitAnimations];
     [guestPreferenceView displayDataGuestPreference];
     [guestPreferenceView.valueGuestPreference setText:dataModel.guestGuestRef];
     guestPreferenceView.delegate = self;
     }
     }
     break;
     
     default:
     break;
     }*/
    
    if (indexPath.section == 1) {
        if(indexPath.row > 0){ //Selected Profile Note
            ProfileNoteTypeV2 *selectedNoteType = [listProfileNoteTypes objectAtIndex:indexPath.row - 1];
            NSMutableArray *profileNotes = [listProfileNotes objectForKey:[NSNumber numberWithInteger:selectedNoteType.profilenote_type_id]];
            ProfileNoteViewV2 *profileNoteView = [[ProfileNoteViewV2 alloc] initWithNibName:@"ProfileNoteViewV2" bundle:nil];
            profileNoteView.listProfileNotes = profileNotes;
            profileNoteView.roomNumber = roomNumber;
            
            [self.parentVC.navigationController pushViewController:profileNoteView animated:YES];
        }
    }
}

-(void)setCaptionsView{
    NSDictionary *dic = [[LanguageManagerV2 sharedLanguageManager] getCurrentDictionaryLanguage];
    self.guestName = [dic valueForKey:[NSString stringWithFormat:@"%@", L_GUEST_NAME]];
    self.houseKeeperName = [dic valueForKey:[NSString stringWithFormat:@"%@", L_HOUSEKEEPER_NAME]];
    self.vipStatus = [dic valueForKey:[NSString stringWithFormat:@"%@", L_VIP_STATUS]];
    self.languagePref = [dic valueForKey:[NSString stringWithFormat:@"%@", L_LANGUAGE_PREF]];
    self.checkIn = [dic valueForKey:[NSString stringWithFormat:@"%@", L_CHECK_IN]];
    self.checkOut = [dic valueForKey:[NSString stringWithFormat:@"%@", L_CHECK_OUT]];
    self.guestPreference = [dic valueForKey:[NSString stringWithFormat:@"%@", L_GUEST_REFERENCE]];
}
#pragma mark - GusetInfo Delegate Methods

-(void)EditGuestPref:(NSString *)temp{
    //    if ([addJobGuestInfo.addjob_guestinfo_pref_desc isEqualToString:temp] == YES ||[dataModel.guestGuestRef isEqualToString:temp] == YES || temp == nil || [temp isEqualToString:@""]) {
    //        return;
    //    }
    //
    //    isSaved = NO;
    //    //whether use guest info of Additional Job or Room Assignment
    //    if(isPushFrom == IS_PUSHED_FROM_ADD_JOB_LIST || isPushFrom == IS_PUSHED_FROM_ADD_JOB_DETAIL){
    //        addJobGuestInfo.addjob_guestinfo_pref_desc = temp;
    //        addJobGuestInfo.addjob_guestinfo_post_status = POST_STATUS_SAVED_UNPOSTED;
    //    } else {
    //        //dataModel.guestGuestRef = temp;
    //        dataModel.guestPostStatus = POST_STATUS_SAVED_UNPOSTED;
    //    }
    //    
    //    [tbvGuestInfo reloadData];
    //    
}

//Reload View only call one time
-(void)reloadView
{
    if(!isAlreadyLoadProfileNote){
        
        MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.view];
        [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getRaLoadingData]];
        [self.view.superview addSubview:HUD];
        [HUD show:YES];
        
        //Load Profile note from WS
        //isAlreadyLoadProfileNote = [[RoomManagerV2 sharedRoomManager] loadWSProfileNoteByUserId:[UserManagerV2 sharedUserManager].currentUser.userId hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId roomNumber:roomNumber];
        isAlreadyLoadProfileNote = YES;
        int userId = [UserManagerV2 sharedUserManager].currentUser.userId;
        //listProfileNoteTypes = [[RoomManagerV2 sharedRoomManager] loadProfileNoteTypesByUserId:userId roomNumber:roomNumber];
        listProfileNoteTypes = [[RoomManagerV2 sharedRoomManager] loadProfileNoteTypesByUserId:userId roomNumber:roomNumber guestId:(int)dataModel.guestId];
        if(!listProfileNotes){
            listProfileNotes = [[NSMutableDictionary alloc] init];
        }
        [listProfileNotes removeAllObjects];
        
        if([listProfileNoteTypes count] > 0){
            for (ProfileNoteTypeV2 *curNoteTye in listProfileNoteTypes) {
//                NSMutableArray *profileNotes = [[RoomManagerV2 sharedRoomManager] loadProfileNotesByUserId:userId profileNoteTypeId:curNoteTye.profilenote_type_id];
                NSMutableArray *profileNotes = [[RoomManagerV2 sharedRoomManager] loadProfileNotesByUserId:userId roomNumber:roomNumber guestId:(int)dataModel.guestId];
                
                if([profileNotes count] > 0) {
                    [listProfileNotes setObject:profileNotes forKey:[NSNumber numberWithInt:(int)curNoteTye.profilenote_type_id]];
                }
            }
        }
        //Reset frame
        CGRect tbGuestFr = tbvGuestInfo.frame;
        //tbGuestFr.size.height += (55.0f * listProfileNoteTypes.count);
        tbGuestFr.size.height = [self totalHeightGuestInfoTable];
        
        CGRect viewFrame = self.view.frame;
        viewFrame.size.height = [self totalHeightGuestInfoTable];
        
        [tbvGuestInfo setFrame:tbGuestFr];
        [tbvGuestInfo setScrollEnabled:NO];
        [self performSelector:@selector(hiddenHUDAfterSaved:) withObject:HUD afterDelay:0.5];
    }
    
    [tbvGuestInfo reloadData];
}

-(CGFloat) totalHeightGuestInfoTable
{
    CGFloat height =    120 //index [0:0]
                        + 33*4 //[0:1] - [0:4]
                        + 66 //[1:0]
                        + 55 * listProfileNoteTypes.count//[1:N*profilenote]
                        + 40; //More height for flexible space
    
    return height;
}

-(void)saveData{
    
    // @ show loading data .
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:parentViewController.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_RA_SAVING_DATA]];
    [parentViewController.tabBarController.view addSubview:HUD];
    [HUD show:YES];
    
    // save Guest Preference
    [self saveDataWithoutHUD];
    
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getMsgSaveMessage]];
    [self performSelector:@selector(hiddenHUDAfterSaved:) withObject:HUD afterDelay:0.5];
    
}

-(void)saveDataWithoutHUD {
    // save Guest Preference
    if (dataModel.guestPostStatus == POST_STATUS_SAVED_UNPOSTED) {
        //[[RoomManagerV2 sharedRoomManager] updateGuestInfo:dataModel];
        
        //post guest info if needed
        if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
            SyncManagerV2 *smanager = [[SyncManagerV2 alloc] init];
            BOOL result = [smanager postGuestInfo:dataModel WithRoomAssignID:[TasksManagerV2 getCurrentRoomAssignment]];
            if (result == YES) {
                dataModel.guestPostStatus = POST_STATUS_POSTED;
            }
        }
    }
    
    isSaved = YES;
}

#pragma didden after save.
-(void) hiddenHUDAfterSaved:(MBProgressHUD *)HUD{
    [HUD hide:YES];
    [HUD removeFromSuperview];
    //    [HUD release];
    
}

#pragma mark - === Check save Guest Info ===
#pragma mark
-(BOOL)checkSaveGuestInfo {
    if (isSaved == NO) {
        //show please complete the room
        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ALERT_SAVEROOMDETAIL] delegate:nil cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_OK] otherButtonTitles:nil];
        [alert show];
        
        return NO;
    }
    
    return YES;
}

@end
