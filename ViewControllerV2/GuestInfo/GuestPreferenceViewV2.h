//
//  GuestPreferenceViewV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol GuestPreferenceViewV2delegate <NSObject>

@optional

-(void)EditGuestPref:(NSString *)temp;

@end

@interface GuestPreferenceViewV2 : UIView{
    UITextView *txtGuestPreference;
    UITextView *valueGuestPreference;
    UIButton *btnGuestPreference;
    UIButton *btnOKGuestPreference;
   __unsafe_unretained id <GuestPreferenceViewV2delegate> delegate;
}
@property(nonatomic,assign)id <GuestPreferenceViewV2delegate> delegate;
@property (strong, nonatomic) UITextView *txtGuestPreference;
@property (strong, nonatomic) UIButton *btnGuestPreference;
@property (strong, nonatomic) UIButton *btnOKGuestPreference;
@property (nonatomic, strong) UITextView *valueGuestPreference;

-(void) displayDataGuestPreference;
@end
