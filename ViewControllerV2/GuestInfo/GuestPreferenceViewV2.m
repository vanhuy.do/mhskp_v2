//
//  GuestPreferenceViewV2.m
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GuestPreferenceViewV2.h"
#define imgOK @"yes_bt.png"

@interface GuestPreferenceViewV2(PrivateMethods)

-(void) btnOKGuestPreferencePressed;

@end

@implementation GuestPreferenceViewV2
@synthesize txtGuestPreference, btnGuestPreference, btnOKGuestPreference;
@synthesize valueGuestPreference,delegate;

-(id)init {
    self = [super init];
    if (self) {
        //init control
        
                
        btnGuestPreference = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [btnGuestPreference setFrame:CGRectMake(33, 47, 267, 137)];
        [btnGuestPreference setUserInteractionEnabled:NO];
        
        //text is Guest Preference
        txtGuestPreference = [[UITextView alloc] initWithFrame:CGRectMake(39, 50, 257, 125)];
        [txtGuestPreference setText:@""];
        [txtGuestPreference setUserInteractionEnabled:NO];
        [txtGuestPreference setTextColor:[UIColor colorWithRed:6.0/255 green:62.0/255 blue:127.0/255 alpha:1.0]];
        [txtGuestPreference setFont:[UIFont fontWithName:@"Arial-BoldMT" size:12.0]];
        
       
        //text input in guest preference
        valueGuestPreference = [[UITextView alloc] initWithFrame:CGRectMake(39, 72, 257, 103)];
        [valueGuestPreference setUserInteractionEnabled:NO];
        [valueGuestPreference setTextColor:[UIColor colorWithRed:51.0/255.0 green:51.0/255 blue:51.0/255 alpha:1.0]];
        [valueGuestPreference setEditable:NO];
        [valueGuestPreference becomeFirstResponder];
        
        btnOKGuestPreference = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnOKGuestPreference setFrame:CGRectMake(101, 185, 108, 37)];
        [btnOKGuestPreference setBackgroundImage:[UIImage imageNamed:imgOK] forState:UIControlStateNormal];
        [btnOKGuestPreference setTitle:[[LanguageManagerV2 sharedLanguageManager] getOK] forState:UIControlStateNormal];

        [btnOKGuestPreference.titleLabel setTextAlignment:NSTextAlignmentCenter];
        [btnOKGuestPreference setTitleEdgeInsets:UIEdgeInsetsMake(0, 13, 0, 0)];

    
        [btnOKGuestPreference addTarget:self action:@selector(btnOKGuestPreferencePressed) forControlEvents:UIControlEventTouchUpInside];
        
        [self addSubview:btnGuestPreference];
        [self addSubview:txtGuestPreference];
        [self addSubview:btnOKGuestPreference];
        [self addSubview:valueGuestPreference];
        
        [self setBackgroundColor:[UIColor colorWithWhite:0.3 alpha:0.7]];
    }
    return self;
}

-(void)displayDataGuestPreference{
    NSString *textPreference = [[LanguageManagerV2 sharedLanguageManager] getGuestPreference];
    [txtGuestPreference setText:textPreference];
}

-(void)btnOKGuestPreferencePressed{
    
    if([delegate respondsToSelector:@selector(EditGuestPref:)] ){
        [delegate EditGuestPref:valueGuestPreference.text];
    }

    [self removeFromSuperview];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
