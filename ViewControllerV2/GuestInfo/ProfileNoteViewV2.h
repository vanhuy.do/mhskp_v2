//
//  ProfileNoteViewV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileNoteViewV2 : UIViewController <UITableViewDataSource, UITableViewDelegate>
{
    ProfileNoteV2 *selectedProfileNote;
}

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (strong, nonatomic) IBOutlet UITableView *tbvProfileNotes;
@property (weak, nonatomic) IBOutlet UITextView *txtNote;

@property (strong, nonatomic) NSMutableArray *listProfileNotes;

@property (nonatomic) BOOL isPushFrom;
@property (nonatomic, strong) NSString *roomNumber;

@end
