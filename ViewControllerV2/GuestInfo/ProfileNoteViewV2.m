//
//  ProfileNoteViewV2.m
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ProfileNoteViewV2.h"
#import "RoomManagerV2.h"
#import "ehkConvert.h"
#import "NetworkCheck.h"
#import "MyNavigationBarV2.h"
#import "DeviceManager.h"

@implementation ProfileNoteViewV2
@synthesize tbvProfileNotes;
@synthesize listProfileNotes;
@synthesize isPushFrom;
@synthesize roomNumber;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //Resize content views of current VC to fit with Navigation bar and tabbar
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    [self performSelector:@selector(loadTopbarView)];
}
- (void)viewWillAppear:(BOOL)animated{
    [self setCaptionView];
    //show wifi view
    if(!isDemoMode){
        [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationShowWifiView] object:nil];
    }
    [super viewWillAppear:animated];
    
    //set selection find in bottom bar
    if(isPushFrom == IS_PUSHED_FROM_ROOM_ASSIGNMENT) {
        [[HomeViewV2 shareHomeView] setSelectedButton:tagOfHomeButton];
    } else if(isPushFrom == IS_PUSHED_FROM_FIND_BY_RA){
              //|| isPushFrom == IS_PUSHED_FROM_FIND_BY_ROOM) {
        [[HomeViewV2 shareHomeView] setSelectedButton:tagOfFindButton];
    } else {
        [[HomeViewV2 shareHomeView] setSelectedButton:tagOfAdditionalJob];
    }
    
}
- (void)viewDidUnload
{
    //    [self setTbvMenuCount:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}



#pragma mark - UITableView DataSource Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return  [listProfileNotes count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    static NSString *cellidentifier = @"CellIdentifier";
    //static NSString *cellDetailidentifier = @"CellDetailIdentifier";
    UITableViewCell *cellDisplay = nil;

    cellDisplay = [tableView dequeueReusableCellWithIdentifier:cellidentifier];

    
    if(cellDisplay == nil){
        cellDisplay = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidentifier];
        [self addAccessoryButton:cellDisplay andIndexPath:indexPath];

        [cellDisplay setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cellDisplay.textLabel setTextColor:[UIColor grayColor]];
    }
    
    ProfileNoteV2 *curProfilenote = [listProfileNotes objectAtIndex:indexPath.row];
    [cellDisplay.textLabel setText:curProfilenote.profilenote_description];
    [cellDisplay setUserInteractionEnabled:YES];
    
    return cellDisplay;
}

- (void)addAccessoryButton:(UITableViewCell *)cell  andIndexPath:(NSIndexPath *)indexPath{
    /*****************************************/
    UIButton *accessoryButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 24, 24)];
    [accessoryButton setBackgroundColor:[UIColor clearColor]];
    //    [accessoryButton setImage:[UIImage imageNamed:@"gray_arrow_36x36.png"] forState:UIControlStateNormal];
    [accessoryButton setImage:[UIImage imageNamed:@"gray_arrow_36x36.png"] forState:UIControlStateNormal];
    [accessoryButton setTag:indexPath.row];
    
    //[accessoryButton addTarget:self action:@selector(btnDetailPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell setAccessoryView:accessoryButton];
    //cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
    /*****************************************/
    
}

#pragma mark - UITableView Delegate methods
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    selectedProfileNote = [listProfileNotes objectAtIndex:indexPath.row];
    //[tbvProfileNotes reloadData];
    
    _txtNote.text = selectedProfileNote.profilenote_description;
    _txtNote.hidden = NO;
    tbvProfileNotes.hidden = YES;
}

- (void) setCaptionView{
    
    [self setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_profile_note_title]];
    
    //refresh topview
    TopbarViewV2 *topview = [self getTopBarView];
    [topview refresh:[[SuperRoomModelV2 alloc] init]];
}

#pragma mark - Handle Topbar Methods
-(void)loadTopbarView {
    TopbarViewV2 *topbar = [[TopbarViewV2 alloc] initViewWithSuperModel:[[CommonVariable sharedCommonVariable] superRoomModel]];
    [topbar setTag:tagTopbarView];
    [self.view addSubview:topbar];
    [topbar setHidden:YES];
    
    [topbar setAlpha:0.0];
    [self addButtonHandleShowHideTopbar];
}

-(void)showTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationWillStartSelector:@selector(setHiddenTopbarView)];
    
    f = topbar.frame;
    f.origin.y = 0;
    [topbar setFrame:f];
    [topbar setAlpha:1.0];
    
    [self adjustShowForViews];
    
    [UIView commitAnimations];
}

-(void)hideTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDidStopSelector:@selector(setHiddenTopbarView)];
    [UIView setAnimationDelegate:self];
    
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    [topbar setAlpha:0.0];
    
    [self adjustRemoveForViews];
    
    [UIView commitAnimations];
}

-(void)setHiddenTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    [topbar setHidden:!topbar.hidden];
}

-(TopbarViewV2 *)getTopBarView {
    return (TopbarViewV2 *)[self.view viewWithTag:tagTopbarView];
}


-(void)addButtonHandleShowHideTopbar {
    UIView *vBarButton = [[UIView alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
    UIButton *titleBarButtonFirst = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 200, 30)];
    
    titleBarButtonFirst.titleLabel.backgroundColor = [UIColor clearColor];
    [titleBarButtonFirst.titleLabel setFont: FONT_BUTTON_TOPBAR];
    titleBarButtonFirst.titleLabel.textAlignment = NSTextAlignmentCenter;
    titleBarButtonFirst.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
    [titleBarButtonFirst.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
    [titleBarButtonFirst.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
    [titleBarButtonFirst addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    [titleBarButtonFirst setTitle:roomNumber forState:UIControlStateNormal];
    [vBarButton addSubview:titleBarButtonFirst];
    
    UIButton *titleBarButtonSecond = [[UIButton alloc] initWithFrame:CGRectMake(0, 28, 200, 22)];
    [titleBarButtonSecond setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [titleBarButtonSecond.titleLabel setFont:FONT_BUTTON_TOPBAR_SECOND];
    [titleBarButtonSecond setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_profile_note_title] forState:UIControlStateNormal];
    
    [titleBarButtonSecond addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    [vBarButton addSubview:titleBarButtonSecond];
    
    self.navigationItem.titleView = vBarButton;
    
    //back button navigation
    self.navigationItem.leftBarButtonItem = [MyNavigationBarV2 createBackButtonWithTarget:self selector:@selector(btnBackPressed) forControlEvents:UIControlEventTouchUpInside];
    
    //Move title to center vertical
    CGRect titleViewBounds = self.navigationItem.titleView.bounds;
    int moveLeftTitle = 0;
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        moveLeftTitle = 10;
    }
    self.navigationItem.titleView.bounds = CGRectMake(titleViewBounds.origin.x + moveLeftTitle, Y_TOPBAR_TITLE, titleViewBounds.size.width, titleViewBounds.size.height);
    
}

-(void)btnBackPressed
{
    //Back to list profile note
    if(selectedProfileNote){
        selectedProfileNote = nil;
        _txtNote.hidden = YES;
        tbvProfileNotes.hidden = NO;
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void)topbarTapped:(UIButton *)sender {
    TopbarViewV2 *topbar = [self getTopBarView];
    [sender setEnabled:NO];
    if (topbar.isHidden) {
        [self showTopbarView];
    } else {
        [self hideTopbarView];
    }
    [self performSelector:@selector(topbarButtonEnable:) withObject:sender afterDelay:0.5];
}

-(void) topbarButtonEnable:(UIButton *) topbarBtn {
    [topbarBtn setEnabled:YES];
}

-(void)adjustShowForViews {
    //adjust all views when show 
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
//    CGRect ftbv = [tbvProfileNotes frame];
//    [tbvProfileNotes setFrame:CGRectMake(0, f.size.height, 320, ftbv.size.height - f.size.height)];
//    CGRect txtF = [_txtNote frame];
//    [_txtNote setFrame:CGRectMake(0, f.size.height, txtF.size.width, txtF.size.height - f.size.height)];
    
    CGRect contentViewFrame = _contentView.frame;
    [_contentView setFrame:CGRectMake(0, f.size.height, contentViewFrame.size.width, contentViewFrame.size.height - f.size.height)];
}

-(void)adjustRemoveForViews { 
    //adjust all views when hide
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
//    CGRect ftbv = tbvProfileNotes.frame;
//    [tbvProfileNotes setFrame:CGRectMake(0, 0, 320, ftbv.size.height + f.size.height)];
//    CGRect txtF = [_txtNote frame];
//    [_txtNote setFrame:CGRectMake(0, f.size.height, txtF.size.width, txtF.size.height + f.size.height)];
    
    CGRect contentViewFrame = _contentView.frame;
    [_contentView setFrame:CGRectMake(0, 0, contentViewFrame.size.width, contentViewFrame.size.height + f.size.height)];
}

@end
