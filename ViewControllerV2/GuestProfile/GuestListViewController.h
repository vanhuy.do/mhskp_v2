//
//  GuestListViewController.h
//  mHouseKeeping
//
//  Created by Gambogo on 7/16/15.
//
//

#import <UIKit/UIKit.h>
#import "UITableView+DragLoad.h"

@interface GuestListViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>

@property (nonatomic, strong) IBOutlet UITableView *tbGuestProfiles;
@property (nonatomic, strong) IBOutlet UIButton *btnCombobox1;
@property (nonatomic, strong) IBOutlet UIButton *btnCombobox2;
@property (nonatomic, strong) IBOutlet UITextField *txtSearch;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *topAlignView;

@end
