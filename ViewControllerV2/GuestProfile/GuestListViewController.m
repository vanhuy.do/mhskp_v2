//
//  GuestListViewController.m
//  mHouseKeeping
//
//  Created by Gambogo on 7/16/15.
//
//

#import "GuestListViewController.h"
#import "GuestProfileCell.h"
#import "MyNavigationBarV2.h"
#import "DeviceManager.h"
#import "GuestProfileViewController.h"
#import "PickerViewV2.h"

#define TableRowHeight 60.0f
#define TableBackgroundGreen [UIColor colorWithRed:206/255.0f green:255/255.0f blue:207/255.0f alpha:1.0f]
#define TableLabelGreen [UIColor colorWithRed:5/255.0f green:123/255.0f blue:9/255.0f alpha:1.0f]

@interface GuestListViewController ()<PickerViewV2Delegate, UITableViewDragLoadDelegate>
{
    NSInteger firstComboIndexSelected;
    NSInteger secondComboIndexSelected;
    NSInteger pageNumber;
    NSMutableArray *listGuest;
    
}
@end

@implementation GuestListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Resize content views of current VC to fit with Navigation bar and tabbar
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    _btnCombobox1.titleLabel.adjustsFontSizeToFitWidth = YES;
    _btnCombobox2.titleLabel.adjustsFontSizeToFitWidth = YES;
    
    firstComboIndexSelected = 0;
    secondComboIndexSelected = 0;
    pageNumber = 0;
    
    //back button navigation
    self.navigationItem.leftBarButtonItem = [MyNavigationBarV2 createBackButtonWithTarget:self selector:@selector(backBarPressed) forControlEvents:UIControlEventTouchUpInside];
    [self addButtonHandleShowHideTopbar];
    [self loadTopbarView];
    
    NSString *titleInHouse = [@"inhouse_guest_profile" currentKeyToLanguage];
    NSString *titleOnDay = [@"onday_guest_profile" currentKeyToLanguage];
    [_btnCombobox1 setTitle:titleInHouse forState:UIControlStateNormal];
    [_btnCombobox2 setTitle:titleOnDay forState:UIControlStateNormal];
    
    _txtSearch.placeholder = [L_SEARCH currentKeyToLanguage];
    
    [_tbGuestProfiles setDragDelegate:self refreshDatePermanentKey:@"GuestList"];
    _tbGuestProfiles.showRefreshView = NO;
    listGuest = [[NSMutableArray alloc] init];
    [self findGuestInfo];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[HomeViewV2 shareHomeView] setSelectedButton:tagOfGuestProfileButton];
    
//    [self findGuestInfo];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - View Interaction

-(void)backBarPressed
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Guest Data

-(void) findGuestInfo
{
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getRaLoadingData]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES];
    
    [self performSelector:@selector(findGuestData:) withObject:HUD afterDelay:0.0f];
}

-(void) findGuestData:(MBProgressHUD *) HUD {
    //enable flag to announce, is loading data
    [[HomeViewV2 shareHomeView] waitingLoadingData];
    
    int userId = [UserManagerV2 sharedUserManager].currentUser.userId;
    int buidingId = [[UserManagerV2 sharedUserManager] getBuildingIdByUserId:userId];
    
    NSMutableArray* tempList = [[RoomManagerV2 sharedRoomManager] findGuestStayedHistoryRoutineWSByUserId:userId searchKey:_txtSearch.text hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId buildingId:buidingId filterMethod:(int)firstComboIndexSelected filterRange:(int)secondComboIndexSelected pageNumber:(int)pageNumber];
    if (tempList.count > 0) {
        pageNumber++;
        [listGuest addObjectsFromArray:tempList];
    }
    [_tbGuestProfiles reloadData];
    
    //hide saving data.
    [self performSelector:@selector(hiddenHUDAfterSaved:) withObject:HUD afterDelay:0.0f];
    
    //end loading data
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
}

#pragma didden after save.

-(void) hiddenHUDAfterSaved:(MBProgressHUD *)HUD{
    [HUD hide:YES];
    [HUD removeFromSuperview];
}

#pragma mark - Topbar Functions

-(void)loadTopbarView {
    TopbarViewV2 *topbar = [[TopbarViewV2 alloc] initViewWithSuperModel:[[CommonVariable sharedCommonVariable] superRoomModel]];
    [topbar setTag:tagTopbarView];
    [self.view addSubview:topbar];
    [topbar setHidden:YES];
    
    [topbar setAlpha:0.0];
    [self addButtonHandleShowHideTopbar];
}

-(void)adjustShowForViews {
    //adjust all views when show
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    _topAlignView.constant = f.size.height;
    
}

-(void)adjustRemoveForViews {
    _topAlignView.constant = 0;
}

-(void)showTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationWillStartSelector:@selector(setHiddenTopbarView)];
    
    f = topbar.frame;
    f.origin.y = 0;
    [topbar setFrame:f];
    [topbar setAlpha:1.0];
    
    [self adjustShowForViews];
    
    [UIView commitAnimations];
}

-(void)hideTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDidStopSelector:@selector(setHiddenTopbarView)];
    [UIView setAnimationDelegate:self];
    
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    [topbar setAlpha:0.0];
    
    [self adjustRemoveForViews];
    
    [UIView commitAnimations];
}

-(void)setHiddenTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    [topbar setHidden:!topbar.hidden];
}

-(TopbarViewV2 *)getTopBarView {
    return (TopbarViewV2 *)[self.view viewWithTag:tagTopbarView];
}


-(void)topbarTapped:(UIButton *)sender {
    TopbarViewV2 *topbar = [self getTopBarView];
    [sender setEnabled:NO];
    if (topbar.isHidden) {
        [self showTopbarView];
    } else {
        [self hideTopbarView];
    }
    [self performSelector:@selector(topbarButtonEnable:) withObject:sender afterDelay:0.5];
}

-(void) topbarButtonEnable:(UIButton *) topbarBtn {
    [topbarBtn setEnabled:YES];
}

-(void)addButtonHandleShowHideTopbar {
    
    UIView *vBarButton = [[UIView alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
    UIButton *titleBarButtonFirst = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 200, 30)];
    
    titleBarButtonFirst.titleLabel.backgroundColor = [UIColor clearColor];
    [titleBarButtonFirst.titleLabel setFont: FONT_BUTTON_TOPBAR];
    titleBarButtonFirst.titleLabel.textAlignment = NSTextAlignmentCenter;
    titleBarButtonFirst.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
    
    [titleBarButtonFirst.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
    [titleBarButtonFirst.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
    [titleBarButtonFirst addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    [titleBarButtonFirst setTitle:[L_title_guest_profile currentKeyToLanguage] forState:UIControlStateNormal];
    
    [vBarButton addSubview:titleBarButtonFirst];
    
    
    UIButton *titleBarButtonSecond = [[UIButton alloc] initWithFrame:CGRectMake(0, 28, 200, 22)];
    [titleBarButtonSecond setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [titleBarButtonSecond.titleLabel setFont:FONT_BUTTON_TOPBAR_SECOND];
    [titleBarButtonSecond setTitle:@"" forState:UIControlStateNormal];//Set title for Tabbar
    [titleBarButtonSecond addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    [vBarButton addSubview:titleBarButtonSecond];
    
    self.navigationItem.titleView = vBarButton;
    
    //Move title to center vertical
    CGRect titleViewBounds = self.navigationItem.titleView.bounds;
    int moveLeftTitle = 0;
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        moveLeftTitle = 10;
    }
    self.navigationItem.titleView.bounds = CGRectMake(titleViewBounds.origin.x + moveLeftTitle, Y_TOPBAR_TITLE, titleViewBounds.size.width, titleViewBounds.size.height);
}

#pragma mark - TextField Delegate

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}

-(IBAction)textFieldTextChanged:(UITextField *)textField
{
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [_txtSearch resignFirstResponder];
    pageNumber = 0;
    listGuest = [[NSMutableArray alloc] init];
    [self findGuestInfo];
    return NO;
}

-(void)dismissKeyboard {
    [_txtSearch resignFirstResponder];
}

#pragma mark - Drag delegate methods

- (void)dragTableDidTriggerRefresh:(UITableView *)tableView
{
    //[self performSelector:@selector(loadDataWithoutLoadingIndicator) withObject:nil afterDelay:0.05];
}

- (void)dragTableRefreshCanceled:(UITableView *)tableView
{
    //cancel refresh request(generally network request) here
    //[NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(finishRefresh) object:nil];
}

- (void)dragTableDidTriggerLoadMore:(UITableView *)tableView
{
//    pageNumber++;
    [self findGuestInfo];
    [self performSelector:@selector(finishLoadMore) withObject:nil afterDelay:0.02];
}

- (void)dragTableLoadMoreCanceled:(UITableView *)tableView
{
    //cancel load more request(generally network request) here
    //[NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(finishLoadMore) object:nil];
}

-(void) finishLoadMore
{
    [NSThread sleepForTimeInterval:1];
    [_tbGuestProfiles finishLoadMore];
}

#pragma mark - Table Delegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return TableRowHeight;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return listGuest.count + 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"GuestProfileCell";
    
    GuestProfileCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        NSArray *nibCell = [[NSBundle mainBundle] loadNibNamed:@"GuestProfileCell" owner:self options:nil];
        
        if ([nibCell count] > 0) {
            cell = [nibCell objectAtIndex:0];
        }
    }
    
    if (listGuest.count == 0) {
            UILabel *lblAlert  =  [[UILabel alloc]init];
            lblAlert.frame     =  CGRectMake(15, 5, 290, 80);
            [lblAlert setText:[L_no_guest_found currentKeyToLanguage]];
            lblAlert.textAlignment = NSTextAlignmentCenter;
            lblAlert.layer.cornerRadius = 10;
            [lblAlert setFont:[UIFont fontWithName:@"Arial-BoldMT" size:20]];
            lblAlert.numberOfLines = 2;
            [lblAlert setTextColor:[UIColor grayColor]];
            lblAlert.font = [UIFont boldSystemFontOfSize:20.0f];
            [_tbGuestProfiles addSubview:lblAlert];
        
        return cell;
    }
    
    if (indexPath.row == 0) {
        [cell.contentView setBackgroundColor:TableBackgroundGreen];
        [cell.lblRoom setTextColor:TableLabelGreen];
        [cell.lblGuestName setTextColor:TableLabelGreen];
        [cell.lblRoom setText:[@"title_room_guest_profile" currentKeyToLanguage]];
        [cell.lblGuestName setText:[@"title_guest_guest_profile" currentKeyToLanguage]];
        
    } else {
        [cell.contentView setBackgroundColor:[UIColor whiteColor]];
        [cell.lblRoom setTextColor:[UIColor blackColor]];
        [cell.lblGuestName setTextColor:[UIColor blackColor]];
        
        eHousekeepingService_FindGuestInformation *curRoom = listGuest[indexPath.row - 1];
        [cell.lblRoom setText:curRoom.RoomNo];
        [cell.lblGuestName setText:curRoom.Name];
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.row == 0) {
        return;
    }
    
    eHousekeepingService_FindGuestInformation *curGuest = listGuest[indexPath.row - 1];
    GuestProfileViewController *guestProfileVC = [[GuestProfileViewController alloc] initWithNibName:@"GuestProfileViewController" bundle:nil];
    guestProfileVC.guestInfo = curGuest;
    [self.navigationController pushViewController:guestProfileVC animated:YES];
}

#pragma mark - Cobobox

-(IBAction)firstComboPressed:(id)sender
{
    NSString *titleInHouse = [@"inhouse_guest_profile" currentKeyToLanguage];
    NSString *titleDueIn = [@"duein_guest_profile" currentKeyToLanguage];
    NSString *titleDueOut = [@"dueout_guest_profile" currentKeyToLanguage];
    
    NSMutableArray *listFirstFilter = [NSMutableArray arrayWithObjects:titleInHouse, titleDueIn, titleDueOut, nil];
    PickerViewV2 *picker = [[PickerViewV2 alloc] initPickerViewV2WithDatas:listFirstFilter AndSelectedData:nil AndIndex:nil];
    picker.delegate = self;
    picker.showFromObject = _btnCombobox1;
    [picker showPickerViewV2];
    
    [_txtSearch resignFirstResponder];
}

-(IBAction)secondComboPressed:(id)sender
{
    NSString *titleOnDay = [@"onday_guest_profile" currentKeyToLanguage];
    NSString *titlePass1Week = [@"pass_one_week_guest_profile" currentKeyToLanguage];
    NSString *titleFuture1Week = [@"future_one_week_guest_profile" currentKeyToLanguage];
    NSString *titlePass1Month = [@"pass_one_month_guest_profile" currentKeyToLanguage];
    NSString *titleFuture1Month = [@"future_one_month_guest_profile" currentKeyToLanguage];
    
    NSMutableArray *listFirstFilter = [NSMutableArray arrayWithObjects:titleOnDay, titlePass1Week, titleFuture1Week, titlePass1Month, titleFuture1Month, nil];
    PickerViewV2 *picker = [[PickerViewV2 alloc] initPickerViewV2WithDatas:listFirstFilter AndSelectedData:nil AndIndex:nil];
    picker.delegate = self;
    picker.showFromObject = _btnCombobox2;
    [picker showPickerViewV2];
    
    [_txtSearch resignFirstResponder];
}


-(void) didChooseFindRoomPickerViewV2WithPickerView:(PickerViewV2*)pickerView withText:(NSString *)data WithtIndex:(NSInteger)tIndex
{
    if (pickerView.showFromObject == _btnCombobox1) {
        
        firstComboIndexSelected = tIndex;
        [_btnCombobox1 setTitle:data forState:UIControlStateNormal];
        pageNumber = 0;
        [self findGuestInfo];
        
    } else if(pickerView.showFromObject == _btnCombobox2) {
        
        secondComboIndexSelected = tIndex;
        [_btnCombobox2 setTitle:data forState:UIControlStateNormal];
        pageNumber = 0;
        listGuest = [[NSMutableArray alloc] init];
        [self findGuestInfo];
    }
}

@end
