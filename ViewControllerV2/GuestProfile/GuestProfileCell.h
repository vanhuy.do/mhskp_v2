//
//  GuestProfileCell.h
//  mHouseKeeping
//
//  Created by Gambogo on 7/16/15.
//
//

#import <UIKit/UIKit.h>

@interface GuestProfileCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *lblRoom;
@property (nonatomic, strong) IBOutlet UILabel *lblGuestName;

@end
