//
//  GuestProfileItem.h
//  mHouseKeeping
//
//  Created by Gambogo on 7/17/15.
//
//

#import <UIKit/UIKit.h>

@interface GuestProfileItemCell : UITableViewCell

@property (nonatomic, retain) IBOutlet UILabel *lblProfileTitle;
@property (nonatomic, retain) IBOutlet UILabel *lblProfileValue;
@property (nonatomic, retain) IBOutlet UILabel *lblPrefDescription;

@end
