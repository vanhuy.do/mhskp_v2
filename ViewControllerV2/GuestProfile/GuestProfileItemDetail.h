//
//  GuestProfileItemDetail.h
//  mHouseKeeping
//
//  Created by Gambogo on 7/24/15.
//
//

#import <UIKit/UIKit.h>
#import "GuestProfileItemCell.h"
#import "GuestProfileViewController.h"

@interface GuestProfileItemDetail : UICollectionViewCell <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>

@property (nonatomic, strong) IBOutlet UITableView *tbGuestProfiles;

@property (nonatomic, strong) eHousekeepingService_FindGuestInformation *guestDetail;
@property (nonatomic, strong) NSMutableArray *listProfileNoteTypes;
@property (nonatomic, strong) UIViewController *parentVC;

-(void) initializeViewData;

@end
