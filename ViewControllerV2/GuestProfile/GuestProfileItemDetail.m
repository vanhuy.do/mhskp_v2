//
//  GuestProfileItemDetail.m
//  mHouseKeeping
//
//  Created by Gambogo on 7/24/15.
//
//

#import "GuestProfileItemDetail.h"
#import "DeviceManager.h"
#import "ProfileNoteViewV2.h"

@implementation GuestProfileItemDetail

- (void)awakeFromNib {
    // Initialization code
}

-(void)initializeViewData {
    [_tbGuestProfiles reloadData];
}

#pragma mark - Table Delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1) {
        if (indexPath.row == 0) {
            return 70.0f;
        }
        
        return 50.0f;
    }
    
    return  40.0f;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 8;
    }
    
    return 1 + _listProfileNoteTypes.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"GuestProfileItemCell";
    static NSString *cellProfileNoteTypeIdentifier = @"cellProfileNoteType";
    
    int tagProfileNote = 16;
    int tagBadgNumberProfile = 17;
    
    if (indexPath.section == 0) {
        GuestProfileItemCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        if (cell == nil) {
            NSArray *nibCell = [[NSBundle mainBundle] loadNibNamed:@"GuestProfileItemCell" owner:self options:nil];
            
            if ([nibCell count] > 0) {
                cell = [nibCell objectAtIndex:0];
            }
        }
        
        if (indexPath.row == 0) { //Room Number
            cell.lblProfileTitle.text = [@"title_roomno_guest_profile" currentKeyToLanguage];
            cell.lblProfileValue.text = _guestDetail.RoomNo;
            
        } else if (indexPath.row == 1) { //Guest Name
            cell.lblProfileTitle.text = [@"title_guest_name_profile" currentKeyToLanguage];
            cell.lblProfileValue.text = _guestDetail.Name;
            
        } else if (indexPath.row == 2) { //VIP
            cell.lblProfileTitle.text = [@"title_vip_guest_profile" currentKeyToLanguage];
            cell.lblProfileValue.text = _guestDetail.VIP;
            
        } else if (indexPath.row == 3) { //No. of Guest
            cell.lblProfileTitle.text = [@"title_no_of_guest_profile" currentKeyToLanguage];
            cell.lblProfileValue.text = _guestDetail.NoOfGuest;
            
        } else if (indexPath.row == 4) { //Language
            cell.lblProfileTitle.text = [@"title_language_guest_profile" currentKeyToLanguage];
            cell.lblProfileValue.text = _guestDetail.LangPreference;
            
        } else if (indexPath.row == 5) { //Check-In
            cell.lblProfileTitle.text = [@"title_checkin_guest_profile" currentKeyToLanguage];
            cell.lblProfileValue.text = _guestDetail.CheckInDate;
            
        } else if (indexPath.row == 6) { //Check-Out
            cell.lblProfileTitle.text = [@"title_checkout_guest_profile" currentKeyToLanguage];
            cell.lblProfileValue.text = _guestDetail.CheckOutDate;
            
        } else if (indexPath.row == 7) { //Room Status
            cell.lblProfileTitle.text = [@"title_room_status_guest_profile" currentKeyToLanguage];
            cell.lblProfileValue.text = _guestDetail.RoomStatus;
        }
        
        return cell;
    } else {
        
        if (indexPath.row == 0) { // Preferences
            GuestProfileItemCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            
            if (cell == nil) {
                NSArray *nibCell = [[NSBundle mainBundle] loadNibNamed:@"GuestProfileItemCell" owner:self options:nil];
                
                if ([nibCell count] > 0) {
                    cell = [nibCell objectAtIndex:0];
                }
            }
            
            cell.lblProfileTitle.text = [@"title_preference_guest_profile" currentKeyToLanguage];
            [cell.lblProfileValue setHidden:YES];
            [cell.lblPrefDescription setHidden:NO];
            cell.lblPrefDescription.text = _guestDetail.PreferenceCodes;
            return cell;
            
        } else {// Notes
            UITableViewCell *cellProfileNoteType = [tableView dequeueReusableCellWithIdentifier:cellProfileNoteTypeIdentifier];
            if(cellProfileNoteType == nil){
                cellProfileNoteType = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellProfileNoteTypeIdentifier];
                [cellProfileNoteType setSelectionStyle:UITableViewCellSelectionStyleNone];
                
                // display label control in table view
                CGRect profileNoteLabelRect = CGRectMake(10, 1, 290, 50);
                UILabel *profileNoteLabel = [[UILabel alloc] initWithFrame:profileNoteLabelRect];
                profileNoteLabel.textAlignment = NSTextAlignmentLeft;
                profileNoteLabel.font = [UIFont boldSystemFontOfSize:14];
                profileNoteLabel.tag = tagProfileNote;
                profileNoteLabel.textColor = [UIColor blackColor]; //[UIColor colorWithRed:6.0/255 green:62.0/255 blue:127.0/255 alpha:1];
                profileNoteLabel.backgroundColor = [UIColor clearColor];
                [cellProfileNoteType.contentView addSubview:profileNoteLabel];
                
                UIColor *badgeFrameColor = nil;
                BOOL isShining = NO;
                if(SYSTEM_VERSION_LESS_THAN(@"7.0")){
                    badgeFrameColor = [UIColor whiteColor];
                    isShining = YES;
                } else {
                    badgeFrameColor = [UIColor clearColor];
                }
                
                CustomBadge *customBadge = [CustomBadge customBadgeWithString:nil
                                                              withStringColor:[UIColor whiteColor]
                                                               withInsetColor:[UIColor redColor]
                                                               withBadgeFrame:YES
                                                          withBadgeFrameColor:badgeFrameColor
                                                                    withScale:1.0
                                                                  withShining:isShining];
                customBadge.tag = tagBadgNumberProfile;
                [customBadge setFrame:CGRectMake(230 ,15, 27, 25)];
                [cellProfileNoteType.contentView addSubview:customBadge];
                
                // add image of guest to cell of table
                UIImage *imageGuestArrow = [UIImage imageNamed:imgRowGray];
                UIImageView *imgView = [[UIImageView alloc] initWithImage:imageGuestArrow];
                imgView.frame = CGRectMake(270, 15, 25 , 25);
                [cellProfileNoteType.contentView addSubview:imgView];
                
            }
            NSMutableDictionary *curProfileNoteType = [_listProfileNoteTypes objectAtIndex:indexPath.row - 1];
            UILabel *profileNoteLabel = (UILabel*)[cellProfileNoteType viewWithTag:tagProfileNote];
            [profileNoteLabel setText:curProfileNoteType[TypeDescriptionKey]];
            
            NSMutableArray *listProfileNotes = curProfileNoteType[ListProfileNoteKey];
            CustomBadge *customBadge = (CustomBadge*)[cellProfileNoteType viewWithTag:tagBadgNumberProfile];
            [customBadge setBadgeText:[NSString stringWithFormat:@"%d", (int)[listProfileNotes count]]];
            [cellProfileNoteType setUserInteractionEnabled:YES];
            return cellProfileNoteType;
        }
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 1) {
        if(indexPath.row > 0){ //Selected Profile Note
            NSMutableDictionary *curProfileNoteType = [_listProfileNoteTypes objectAtIndex:indexPath.row - 1];
            NSMutableArray *listProfileNotes = curProfileNoteType[ListProfileNoteKey];
            
            ProfileNoteViewV2 *profileNoteView = [[ProfileNoteViewV2 alloc] initWithNibName:@"ProfileNoteViewV2" bundle:nil];
            profileNoteView.listProfileNotes = listProfileNotes;
            profileNoteView.roomNumber = _guestDetail.RoomNo;
            [self.parentVC.navigationController pushViewController:profileNoteView animated:YES];
        }
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 1) {
        return 15.0f;
    }
    
    return 0.1f;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1f;
}

@end
