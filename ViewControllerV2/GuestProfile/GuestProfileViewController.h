//
//  GuestListViewController.h
//  mHouseKeeping
//
//  Created by Gambogo on 7/16/15.
//
//

#import <UIKit/UIKit.h>

#define GuestInfomationKey @"GuestInformation"
#define GuestRoomInformationKey @"GuestRoomInformation"
#define ListProfileNoteTypeKey @"ListProfileNoteType"
#define ListProfileNoteKey @"ListProfileNote"

#define TypeCodeKey @"TypeCode"
#define TypeDescriptionKey @"TypeDescription"
#define ListNoteKey @"ListNoteKey"

@interface GuestProfileViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic, strong) IBOutlet NSLayoutConstraint *topAlignView;
@property (nonatomic, strong) IBOutlet UICollectionView *collectionGuest;
@property (nonatomic, assign) eHousekeepingService_FindGuestInformation* guestInfo;

@end
