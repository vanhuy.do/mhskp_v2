//
//  GuestListViewController.m
//  mHouseKeeping
//
//  Created by Gambogo on 7/16/15.
//
//

#import "GuestProfileViewController.h"
#import "GuestProfileCell.h"
#import "MyNavigationBarV2.h"
#import "DeviceManager.h"
#import "GuestProfileItemDetail.h"

#define TableBackgroundGreen [UIColor colorWithRed:206/255.0f green:255/255.0f blue:207/255.0f alpha:1.0f]
#define TableLabelGreen [UIColor colorWithRed:5/255.0f green:123/255.0f blue:9/255.0f alpha:1.0f]

static NSString *identifier = @"GuestProfileItemDetail";

@interface GuestProfileViewController () <PickerViewV2Delegate>
{
    eHousekeepingService_GuestRoomInformation *guestRoomInformation;
    NSMutableArray *listProfileNotes;
}
@end

@implementation GuestProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Resize content views of current VC to fit with Navigation bar and tabbar
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    //back button navigation
    self.navigationItem.leftBarButtonItem = [MyNavigationBarV2 createBackButtonWithTarget:self selector:@selector(backBarPressed) forControlEvents:UIControlEventTouchUpInside];
    [self addButtonHandleShowHideTopbar];
    [self loadTopbarView];
    
    UINib *cellNib = [UINib nibWithNibName:@"GuestProfileItemDetail" bundle:nil];
    [_collectionGuest registerNib:cellNib forCellWithReuseIdentifier:identifier];
    
    // Configure layout
    CGSize sizeScreen = self.view.bounds.size;
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setMinimumLineSpacing:0.0f];
    [flowLayout setMinimumInteritemSpacing:0.0f];
    [flowLayout setItemSize:CGSizeMake(sizeScreen.width, sizeScreen.height - 44.0f)];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [_collectionGuest setCollectionViewLayout:flowLayout];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[HomeViewV2 shareHomeView] setSelectedButton:tagOfGuestProfileButton];
    
    [self findGuestInfo];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Collection View Delegate

//- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
//    return UIEdgeInsetsMake(0, 0, 0, 0); // top, left, bottom, right
//}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    
    return 0.0f;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
//    return listGuest.count;
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    GuestProfileItemDetail *cell = (GuestProfileItemDetail *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    cell.guestDetail = _guestInfo;
    cell.listProfileNoteTypes = listProfileNotes;
    cell.parentVC = self;
    [cell initializeViewData];
    
    [cell updateConstraintsIfNeeded];
    [cell layoutSubviews];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize collectionViewSize = collectionView.bounds.size;
    return collectionViewSize;
}

#pragma mark - View Interaction

-(void)backBarPressed
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) findGuestInfo
{
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getRaLoadingData]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES];
    
    [self performSelector:@selector(findGuestData:) withObject:HUD afterDelay:0.0f];
}

-(void) findGuestData:(MBProgressHUD *) HUD {
    
    //enable flag to announce, is loading data
    [[HomeViewV2 shareHomeView] waitingLoadingData];
    
    [self updateProfileNoteGuestByGuestList];
    
    [_collectionGuest reloadData];
    
    //hide saving data.
    [self performSelector:@selector(hiddenHUDAfterSaved:) withObject:HUD afterDelay:0.0f];
    
    //end loading data
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
}

-(void) updateProfileNoteGuestByGuestList {
    
    NSMutableArray *listWSGuestProfileNote = _guestInfo.mGuestProfileNoteList.GuestProfileNoteDetail;
    NSMutableSet *typeCodeSet = [NSMutableSet set];
    
    //Unique type code
    for (eHousekeepingService_GuestProfileNoteDetail *curProfileNote in listWSGuestProfileNote) {
        if (curProfileNote.strTypeCode.length > 0) {
            [typeCodeSet addObject:curProfileNote.strTypeCode];
        } else if (curProfileNote.strTypeCode.length <= 0) {
            [typeCodeSet addObject:@" "];
        }
    }
    
    listProfileNotes = [NSMutableArray array];
    
    //List Profile Note Type
    for (NSString *typeCode in typeCodeSet) {
        
        NSMutableDictionary *profileNoteTypeDict = [NSMutableDictionary dictionary];
        NSMutableArray *listNotes = [NSMutableArray array];
        [profileNoteTypeDict setObject:typeCode forKey:TypeCodeKey];
        
        for (eHousekeepingService_GuestProfileNoteDetail *curProfileNote in listWSGuestProfileNote) {
            
            if (curProfileNote.strTypeCode <= 0) {
                curProfileNote.strTypeCode = @" ";
            }
            
            if([curProfileNote.strTypeCode isEqualToString:typeCode]) {
                
                if (curProfileNote.strTypeDescription.length > 0) {
                    [profileNoteTypeDict setObject:curProfileNote.strTypeDescription forKey:TypeDescriptionKey];
                }
                
                ProfileNoteV2 *profileNote = [[ProfileNoteV2 alloc] init];
                profileNote.profilenote_last_modified = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
                profileNote.profilenote_room_number = _guestInfo.RoomNo;
                profileNote.profilenote_user_id = [UserManagerV2 sharedUserManager].currentUser.userId;
                profileNote.profilenote_description = curProfileNote.strNoteDescription;
                profileNote.profilenote_guest_id = 0;
                profileNote.profilenote_type_id = 0;
                [listNotes addObject:profileNote];
            }
        }
        
        [profileNoteTypeDict setObject:listNotes forKey:ListProfileNoteKey];
        
        NSString *currentTypeCode = profileNoteTypeDict[TypeDescriptionKey];
        if (listNotes.count <= 0 && [typeCode isEqualToString:@" "] && currentTypeCode.length <= 0) {
            
        } else {
            [listProfileNotes addObject:profileNoteTypeDict];
        }
    }
}

#pragma didden after save.

-(void) hiddenHUDAfterSaved:(MBProgressHUD *)HUD{
    [HUD hide:YES];
    [HUD removeFromSuperview];
}

#pragma mark - Topbar Functions

-(void)loadTopbarView {
    TopbarViewV2 *topbar = [[TopbarViewV2 alloc] initViewWithSuperModel:[[CommonVariable sharedCommonVariable] superRoomModel]];
    [topbar setTag:tagTopbarView];
    [self.view addSubview:topbar];
    [topbar setHidden:YES];
    
    [topbar setAlpha:0.0];
    [self addButtonHandleShowHideTopbar];
}

-(void)adjustShowForViews {
    //adjust all views when show
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    _topAlignView.constant = f.size.height;
    [_collectionGuest reloadData];
    
}

-(void)adjustRemoveForViews {
    _topAlignView.constant = 0;
    [_collectionGuest reloadData];
}

-(void)showTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationWillStartSelector:@selector(setHiddenTopbarView)];
    
    f = topbar.frame;
    f.origin.y = 0;
    [topbar setFrame:f];
    [topbar setAlpha:1.0];
    
    [self adjustShowForViews];
    
    [UIView commitAnimations];
}

-(void)hideTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDidStopSelector:@selector(setHiddenTopbarView)];
    [UIView setAnimationDelegate:self];
    
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    [topbar setAlpha:0.0];
    
    [self adjustRemoveForViews];
    
    [UIView commitAnimations];
}

-(void)setHiddenTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    [topbar setHidden:!topbar.hidden];
}

-(TopbarViewV2 *)getTopBarView {
    return (TopbarViewV2 *)[self.view viewWithTag:tagTopbarView];
}


-(void)topbarTapped:(UIButton *)sender {
    TopbarViewV2 *topbar = [self getTopBarView];
    [sender setEnabled:NO];
    if (topbar.isHidden) {
        [self showTopbarView];
    } else {
        [self hideTopbarView];
    }
    [self performSelector:@selector(topbarButtonEnable:) withObject:sender afterDelay:0.5];
}

-(void) topbarButtonEnable:(UIButton *) topbarBtn {
    [topbarBtn setEnabled:YES];
}

-(void)addButtonHandleShowHideTopbar {
    
    UIView *vBarButton = [[UIView alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
    UIButton *titleBarButtonFirst = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 200, 30)];
    
    titleBarButtonFirst.titleLabel.backgroundColor = [UIColor clearColor];
    [titleBarButtonFirst.titleLabel setFont: FONT_BUTTON_TOPBAR];
    titleBarButtonFirst.titleLabel.textAlignment = NSTextAlignmentCenter;
    titleBarButtonFirst.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
    
    [titleBarButtonFirst.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
    [titleBarButtonFirst.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
    [titleBarButtonFirst addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    [titleBarButtonFirst setTitle:[L_title_guest_profile currentKeyToLanguage] forState:UIControlStateNormal];
    
    [vBarButton addSubview:titleBarButtonFirst];
    
    UIButton *titleBarButtonSecond = [[UIButton alloc] initWithFrame:CGRectMake(0, 28, 200, 22)];
    [titleBarButtonSecond setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [titleBarButtonSecond.titleLabel setFont:FONT_BUTTON_TOPBAR_SECOND];
    [titleBarButtonSecond setTitle:@"" forState:UIControlStateNormal];//Set title for Tabbar
    [titleBarButtonSecond addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    [vBarButton addSubview:titleBarButtonSecond];
    
    self.navigationItem.titleView = vBarButton;
    
    //Move title to center vertical
    CGRect titleViewBounds = self.navigationItem.titleView.bounds;
    int moveLeftTitle = 0;
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        moveLeftTitle = 10;
    }
    self.navigationItem.titleView.bounds = CGRectMake(titleViewBounds.origin.x + moveLeftTitle, Y_TOPBAR_TITLE, titleViewBounds.size.width, titleViewBounds.size.height);
}



@end
