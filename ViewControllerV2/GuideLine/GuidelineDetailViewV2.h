//
//  GuidelineDetailViewV2.h
//  mHouseKeeping
//
//  Created by TMS on 2/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetailGuidelineEntryV2.h"
#import "GuideItemsV2.h"
#import "DDPageControl.h"
#import "GuidelineManagerV2.h"
#import "TopbarViewV2.h"
#import "CommonVariable.h"
#import "GuidelineItem.h"

@interface GuidelineDetailViewV2 : UIViewController<UIScrollViewDelegate, UIAlertViewDelegate> {
    DetailGuidelineEntryV2* entry;
    GuideModelV2* guideline;
    NSMutableArray* guideArray;
    TopbarViewV2    *topBarView;
    
    BOOL _isRoomCompleted;
    IBOutlet UIImageView *imgHeader;
}

@property (nonatomic, readwrite) BOOL isRoomCompleted;
@property (strong, nonatomic) DetailGuidelineEntryV2* entry;
@property (strong, nonatomic) TopbarViewV2    *topBarView;
@property (strong, nonatomic) IBOutlet UICollectionView *collectionGuidelineItems;
@property (assign, nonatomic) enum IS_PUSHED_FROM isPushFrom;

@property (nonatomic, strong) GuideModelV2 *guideline;
@property (nonatomic, strong) NSMutableArray *guideArray;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *topCollectionContrains;

@end
