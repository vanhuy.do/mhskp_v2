//
//  GuidelineDetailViewV2.m
//  mHouseKeeping
//
//  Created by TMS on 2/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GuidelineDetailViewV2.h"
#import "CommonVariable.h"
#import "ehkDefinesV2.h"
#import "UserManagerV2.h"
#import "LanguageManagerV2.h"
#import "LogFileManager.h"
#import "MyNavigationBarV2.h"
#import "DeviceManager.h"
#import "CustomAlertViewV2.h"

@implementation GuidelineDetailViewV2 {
    NSInteger currentGuideLine;
}

@synthesize entry;
@synthesize topBarView;
@synthesize isRoomCompleted = _isRoomCompleted;
@synthesize isPushFrom;
@synthesize guideArray;
@synthesize guideline;

static NSString *identifier = @"GuidelineCell";

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        //init room is completed
        _isRoomCompleted = YES;
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if(isPushFrom == IS_PUSHED_FROM_ADD_JOB_DETAIL || isPushFrom == IS_PUSHED_FROM_ADD_JOB_LIST){
        [[HomeViewV2 shareHomeView] setSelectedButton:tagOfAdditionalJob];
    } else {
        //Hightlight home button
        [[HomeViewV2 shareHomeView] setSelectedButton:tagOfHomeButton];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UINib *cellNib = [UINib nibWithNibName:@"GuidelineItem" bundle:nil];
    [self.collectionGuidelineItems registerNib:cellNib forCellWithReuseIdentifier:identifier];
    //[self.collectionGuidelineItems registerClass:[GuidelineItem class] forCellWithReuseIdentifier:identifier];
    /* end of subclass-based cells block */
    
    CGSize sizeScreen = self.view.bounds.size;
    
    // Configure layout
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setMinimumLineSpacing:0.0f];
    [flowLayout setMinimumInteritemSpacing:0.0f];
    [flowLayout setItemSize:CGSizeMake(sizeScreen.width, sizeScreen.height - 44.0f)];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [self.collectionGuidelineItems setCollectionViewLayout:flowLayout];
    
    //Resize content views of current VC to fit with Navigation bar and tabbar
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    [self.navigationItem setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_Guideline]];
    
    // Add title view
    CGRect frame = CGRectMake(0, 0, 200, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame] ;
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont systemFontOfSize:TITLE_SIZE];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor colorWithRed:47.0/255.0 green:144.0/255.0 blue:186.0/255.0 alpha:1.0];
    
    NSDictionary *dic = [[LanguageManagerV2 sharedLanguageManager] getCurrentDictionaryLanguage];
    NSString *firstTitle = [dic valueForKey:[NSString stringWithFormat:@"%@", L_Guideline]];
    NSString *secondTitle;
    
    if ([[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]]) {
        secondTitle = guideline.guide_Name;
    } else {
        secondTitle = guideline.guide_NameLang;
    }
    
    //create title bar
    self.navigationItem.titleView = [MyNavigationBarV2 createTitleBarWithFirstRow:firstTitle secondRow:secondTitle target:self selector:@selector(hiddenHeaderView) forControlEvents:UIControlEventTouchUpInside];
    //Move to middle of view
    self.navigationItem.titleView.bounds = [MyNavigationBarV2 middleFrameForNavigationTitleView:self.navigationItem.titleView];
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        [self loadFlatResource];
    }
    
    //back button navigation
    self.navigationItem.leftBarButtonItem = [MyNavigationBarV2 createBackButtonWithTarget:self selector:@selector(btnBackPressed) forControlEvents:UIControlEventTouchUpInside];
    
    // add topbar 
    if (topBarView == nil) {
        topBarView = [[TopbarViewV2 alloc] initViewWithSuperModel:[[CommonVariable sharedCommonVariable] superRoomModel]];
        [self.view addSubview:topBarView];
        topBarView.hidden = YES;
    }
    
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_RA_SAVING_DATA]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES];
    
    //Load Checklist Data
    [self performSelector:@selector(initializeData:) withObject:HUD afterDelay:0.2f];
}

-(void)btnBackPressed
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)initializeData:(MBProgressHUD*)HUD {
    
    [[HomeViewV2 shareHomeView] waitingLoadingData];
    
    [[GuidelineManagerV2 sharedGuidelineManagerV2] getGuideListWSWithHotelId:[[[UserManagerV2 sharedUserManager] currentUser] userHotelsId] categoryId:guideline.guide_CategoryId andLastModified:nil];

    self.guideArray = [[GuidelineManagerV2 sharedGuidelineManagerV2] loadAllGuideItemByCategoryId:self.guideline.guide_CategoryId];
    
//    if (guideArray.count > 0) {
//        [self setDescriptionText:0];
//    }
    
    [_collectionGuidelineItems reloadData];
    
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
    
    [HUD hide:YES];
    [HUD removeFromSuperview];
    
    if(guideArray.count <= 0)
    {
        UIAlertView *notifyNoData = [[UIAlertView alloc] initWithTitle:@"" message:[L_guideline_item_have_no_data currentKeyToLanguage] delegate:self cancelButtonTitle:[L_OK currentKeyToLanguage] otherButtonTitles: nil];
        notifyNoData.tag = 298;
        [notifyNoData show];
    }
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 298) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
 
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark -
#pragma mark NAVIGATION BAR Methods
#define heightScale 45
#define TABLE_FULL_FRAME CGRectMake(0, 0, 320, 387)
#define TABLE_FULL_FRAME_4_0 CGRectMake(0, 0, 320, 468)
#define TABLE_SORT_FRAME CGRectMake(0, 50, 320, 337)
#define TABLE_SORT_FRAME_4_0 CGRectMake(0, 50, 320, 411)
#define SCROLL_FRAME CGRectMake(29, 84, 262, 151)
#define TEXTVIEW_FULL_FRAME CGRectMake(16, 294, 292, 80)
#define TEXTVIEW_SORT_FRAME CGRectMake(16, 294, 292, 30)
#define TXT_BG_FULL_FRAME CGRectMake(10, 289, 303, 90)
#define TXT_BG_SORT_FRAME CGRectMake(10, 289, 303, 40)

- (void) hiddenHeaderView {
    BOOL isHidden = topBarView.hidden;
    isHidden = !isHidden;
    [topBarView setHidden:isHidden];
    
    if (isHidden) {
        _topCollectionContrains.constant = 0.0f;
    } else {
        _topCollectionContrains.constant = 44.0f;
    }
    [_collectionGuidelineItems reloadData];
//    if(!isHidden){
//        [self.guideScroll setFrame:TABLE_FULL_FRAME];
//    } else {
//        [self.guideScroll setFrame:TABLE_SORT_FRAME];
//    }
}

#pragma mark - Collection View Delegate

//- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
//    return UIEdgeInsetsMake(0, 0, 0, 0); // top, left, bottom, right
//}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    
    return 0.0f;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return guideArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    GuideItemsV2 *curGuideItem = guideArray[indexPath.row];
    NSMutableArray *listGuideItemDetails = [[GuidelineManagerV2 sharedGuidelineManagerV2] loadGuideItemDetailsByGuideItemId:curGuideItem.gitem_id];
    GuidelineItem *cell = (GuidelineItem *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    cell.listGuideDetail = listGuideItemDetails;
    cell.guideLineItem = curGuideItem;
    [cell initializeViewData];
    
    [cell updateConstraintsIfNeeded];
    [cell layoutSubviews];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize collectionViewSize = collectionView.bounds.size;
    return collectionViewSize;
}
#pragma mark - UIPageControl Methods
/*
- (IBAction)changePage:(id)sender
{
    //int page = guidelinePageControl._currentPage;
    
	// update the scroll view to the appropriate page
    CGRect frame = guidelineScrollView.frame;
    frame.origin.x = frame.size.width * page;
    frame.origin.y = 0;
    [guidelineScrollView scrollRectToVisible:frame animated:YES];
    
	// Set the boolean used when scrolls originate from the UIPageControl. See scrollViewDidScroll: above.
    pageControlUsed = YES;
}

- (void)firstChangePage
{
    int page = guidelineScrollView._currentPage;
    
	// update the scroll view to the appropriate page
    CGRect frame = guidelineScrollView.frame;
    frame.origin.x = frame.size.width * page;
    frame.origin.y = 0;
    [guidelineScrollView scrollRectToVisible:frame animated:YES];
    
	// Set the boolean used when scrolls originate from the UIPageControl. See scrollViewDidScroll: above.
    pageControlUsed = YES;
    //    NSLog(@"current page: %d", pageControl._currentPage);
}

*/

#pragma mark - === Set Captions View ===
#pragma mark

-(BOOL) checkSaveDataWithTagEvent:(NSInteger) tagOfEvent {
    //NO
    
    if(_isRoomCompleted == NO){
        if(tagOfEvent == tagOfSettingButton){
            UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_complete_room_to_leave] delegate:nil cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles: nil];   
            [alert show];
            
            return YES; //no need to save before leaving, but need to complete the room before leaving
        } else {
            return NO;
        }
        
    } else {
        return NO;
    }
    
}

#pragma mark - iOS 7 Resources
-(void)loadFlatResource
{
    [imgHeader setImage:[UIImage imageNamed:imgGuestInfoActiveFlat]];
}

@end
