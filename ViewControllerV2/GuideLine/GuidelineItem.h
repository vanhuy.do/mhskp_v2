//
//  GuidelineItem.h
//  mHouseKeeping
//
//  Created by Gambogo on 3/19/15.
//
//

#import <UIKit/UIKit.h>
#import "DDPageControl.h"
#import "GuideModelV2.h"
#import "GuideItemsV2.h"
#import "GuideItemDetail.h"
#define SCROLL_WIDTH 262
#define SCROLL_HEIGHT 151

@interface GuidelineItem : UICollectionViewCell
{
    BOOL pageControlUsed;
    NSMutableArray *listItemImageViews;
}

@property (strong, nonatomic) IBOutlet UIImageView *guidelineTextViewBorder;
@property (strong, nonatomic) IBOutlet UILabel *lblGuidelineDescriptions;
//@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (strong, nonatomic) IBOutlet UIScrollView *guideScroll;
@property (strong, nonatomic) IBOutlet UILabel *guidelineName;
@property (strong, nonatomic) IBOutlet UILabel *guidelineDetail;
@property (strong, nonatomic) IBOutlet UIScrollView *guidelineScrollView;
@property (strong, nonatomic) IBOutlet UITextView *guidelineDescriptions;
@property (assign, nonatomic) enum IS_PUSHED_FROM isPushFrom;
@property (strong, nonatomic) IBOutlet DDPageControl *guidelinePageControl;
@property (strong, nonatomic) GuideModelV2* guideline;
@property (strong, nonatomic) NSMutableArray* listGuideDetail;
@property (strong, nonatomic) GuideItemsV2* guideLineItem;

-(void)initializeViewData;

@end
