//
//  GuidelineItem.m
//  mHouseKeeping
//
//  Created by Gambogo on 3/19/15.
//
//

#import "GuidelineItem.h"

@implementation GuidelineItem

@synthesize guidelineDetail;
@synthesize guidelineName;
@synthesize guidelineDescriptions;
@synthesize lblGuidelineDescriptions;
@synthesize guidelinePageControl;
@synthesize guideLineItem;

- (void)awakeFromNib {
    // Initialization code
}


#pragma mark - UIScrollView Methods
// Initialize the scroll view and add image
-(void)initializeViewData
{
    
    if ([[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString:(NSString *)ENGLISH_LANGUAGE]) {
        [guidelineDescriptions setText:guideLineItem.gitem_content];
        guidelineName.text = guideLineItem.gitem_Name;
    } else {
        [guidelineDescriptions setText:guideLineItem.gitem_content_lang];
        guidelineName.text = guideLineItem.gitem_NameLang;
    }
    
    NSInteger kNumberOfPages = [self.listGuideDetail count];
    
    // a page is the width of the scroll view
    _guidelineScrollView.pagingEnabled = YES;
    _guidelineScrollView.contentSize = CGSizeMake(_guidelineScrollView.frame.size.width * kNumberOfPages, 405);
    [_guidelineScrollView.layer setCornerRadius:10.0f];
    [self showImageInScrollView:_guidelineScrollView];
    [self.guidelineScrollView setBackgroundColor:[UIColor clearColor]];
    [self loadGuidelineView];
}

// Add image into scrollview
-(void)showImageInScrollView:(UIScrollView *)scrollView{
    
    if (!listItemImageViews) {
        listItemImageViews = [NSMutableArray array];
    }
    
    //Remove old all img items
    for (UIView *curImageView in listItemImageViews) {
        [curImageView removeFromSuperview];
    }
    
    UIImageView *imgView;
    CGRect frame;
    UIImage *img;
    CGFloat w,h,x,y;
    for (int temp = 0; temp < [self.listGuideDetail count]; temp ++) {
        GuideItemDetail *anItem = (GuideItemDetail*)[self.listGuideDetail objectAtIndex:temp];
        if (!anItem.gitem_image) {
            continue;
        }
        img = [UIImage imageWithData:anItem.gitem_image];
        [img scale];
        w = scrollView.frame.size.width;
        h = scrollView.frame.size.height;
        frame.size = CGSizeMake(w, h);
        x = floorf((scrollView.frame.size.width - w) / 2) + temp * scrollView.frame.size.width;
        y = floorf((scrollView.frame.size.height - h) / 2);
        frame.origin = CGPointMake(x, y);
        h = SCROLL_HEIGHT;
        imgView = [[UIImageView alloc] initWithFrame:frame];
        imgView.image = img;
        imgView.backgroundColor = [UIColor clearColor];
        
        [listItemImageViews addObject:imgView];
        [scrollView addSubview:imgView];
    }
    
    [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width*[self.listGuideDetail count], scrollView.frame.size.height)];
}

// Set text for description
-(void) setDescriptionText:(int)page {
    GuideItemsV2* anItem = (GuideItemsV2*)[self.listGuideDetail objectAtIndex:page];
    
    if ([[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString:(NSString *)ENGLISH_LANGUAGE]) {
        [guidelineDescriptions setText:anItem.gitem_content];
        guidelineName.text = anItem.gitem_Name;
    } else {
        [guidelineDescriptions setText:anItem.gitem_content_lang];
        guidelineName.text = anItem.gitem_NameLang;
    }
}

#pragma mark - UIScrollViewDelegate Methods
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if (pageControlUsed)
    {
        // do nothing - the scroll was initiated from the page control, not the user dragging
        return;
    }
    
    // Switch the indicator when more than 50% of the previous/next page is visible
    CGFloat pageWidth = scrollView.frame.size.width;
    int page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    if (page >= [self.listGuideDetail count]) {
        page = (int)[self.listGuideDetail count]-1;
    }
    [guidelinePageControl setCurrentPage:page];
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    pageControlUsed = NO;
}

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    
    pageControlUsed = NO;
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    
    if (decelerate == NO) {
        // Switch the indicator when more than 50% of the previous/next page is visible
        CGFloat pageWidth = scrollView.frame.size.width;
        int page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        if (page >= [self.listGuideDetail count]) {
            page = (int)[self.listGuideDetail count] - 1;
        }
        [guidelinePageControl setCurrentPage:page];
        [self setDescriptionText:page];
        pageControlUsed = YES;
    }
}

#pragma mark - Private Functions
-(void)loadGuidelineView
{
    /*
    [guidelineName setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_guideline_setup]];
    guidelineDetail.hidden = YES;
    [lblGuidelineDescriptions setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_guideline_desciption]];
     */
    [lblGuidelineDescriptions setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_guideline_desciption]];
    // Set up page control
    //guidelinePageControl = [[DDPageControl alloc] initWithType:DDPageControlTypeOnFullOffFull];
    [guidelinePageControl setNumberOfPages:[self.listGuideDetail count]];
    //guidelinePageControl.frame = CGRectMake(25, 235, 300, 30);
    guidelinePageControl.backgroundColor = [UIColor clearColor];
    [guidelinePageControl setNumberOfPages:[self.listGuideDetail count]];
}

@end
