//
//  GuidelineViewV2.h
//  mHouseKeeping
//
//  Created by TMS on 2/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "GuidelineManagerV2.h"
#import "TopbarViewV2.h"
#import "CommonVariable.h"
#import "MBProgressHUD.h"

@interface GuidelineViewV2 : UIViewController <UITableViewDataSource, UITableViewDelegate>{
    
    IBOutlet UITableView *GuidelineTableView;
    NSMutableArray* imageArray;
    TopbarViewV2* topBarView;
    
    MBProgressHUD *HUD;
    
    BOOL _isRoomCompleted;
}

@property (nonatomic, readwrite) BOOL isRoomCompleted;
@property (strong, nonatomic) IBOutlet UITableView *GuidelineTableView;
@property (strong, nonatomic) NSMutableArray* imageArray;
@property (strong, nonatomic) TopbarViewV2* topBarView;
@property (strong, nonatomic) NSString* roomNo;
@property (nonatomic, assign) int roomTypeId;
@property (nonatomic, assign) enum IS_PUSHED_FROM isPushFrom;

- (void) initializeData;
@end
