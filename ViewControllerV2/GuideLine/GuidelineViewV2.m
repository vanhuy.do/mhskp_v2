//
//  GuidelineViewV2.m
//  mHouseKeeping
//
//  Created by TMS on 2/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GuidelineViewV2.h"
#import "GuidelineImageV2.h"
#import "GuidelineDetailViewV2.h"
#import "CommonVariable.h"
#import "ehkDefinesV2.h"
#import "UserManagerV2.h"
#import "LanguageManagerV2.h"
#import "ehkDefines.h"
#import "NetworkCheck.h"
#import "TasksManagerV2.h"
#import "MyNavigationBarV2.h"
#import "LogFileManager.h"
#import "DeviceManager.h"
#import "HomeViewV2.h"
#import "GuideModelV2.h"
#import "CustomAlertViewV2.h"

#define CELL_HEIGHT         92
#define SECTION_DISTANCE    2

enum {
    IMAGE_TAG = 5,
    BACKGROUND_TAG,
    TEXT_TAG,
};

@implementation GuidelineViewV2
@synthesize GuidelineTableView;
@synthesize imageArray;
@synthesize topBarView;
@synthesize isRoomCompleted = _isRoomCompleted;
@synthesize roomNo;
@synthesize roomTypeId;
@synthesize isPushFrom;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        //init room is completed
        _isRoomCompleted = YES;
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}



#pragma mark - View lifecycle

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if(isPushFrom == IS_PUSHED_FROM_ADD_JOB_LIST || isPushFrom == IS_PUSHED_FROM_ADD_JOB_DETAIL) {
        [[HomeViewV2 shareHomeView] setSelectedButton:tagOfAdditionalJob];
    } else {
        [[HomeViewV2 shareHomeView] setSelectedButton:tagOfHomeButton];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //Resize content views of current VC to fit with Navigation bar and tabbar
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    [self.GuidelineTableView setBackgroundColor:[UIColor clearColor]];
    
    HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_RA_LOADING_DATA]];
    [HUD show:YES];
    [self.tabBarController.view addSubview:HUD];
    [self performSelector:@selector(initializeData) withObject:nil afterDelay:0.3];
//    [self initializeData];

    
    // Add title view
    CGRect frame = CGRectMake(0, 0, 230, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame] ;
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont systemFontOfSize:TITLE_SIZE];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor colorWithRed:47.0/255.0 green:144.0/255.0 blue:186.0/255.0 alpha:1.0];
    
    UIView *vBarButton = [[UIView alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
    UIButton *titleBarButtonFirst = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 200, 30)];
    [titleBarButtonFirst setTag:1];
    titleBarButtonFirst.titleLabel.backgroundColor = [UIColor clearColor];
    [titleBarButtonFirst.titleLabel setFont: FONT_BUTTON_TOPBAR];
    titleBarButtonFirst.titleLabel.textAlignment = NSTextAlignmentCenter;
    [titleBarButtonFirst.titleLabel setAdjustsFontSizeToFitWidth:YES];
    titleBarButtonFirst.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
    
    [titleBarButtonFirst.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
    [titleBarButtonFirst.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
    [titleBarButtonFirst addTarget:self action:@selector(hiddenHeaderView) forControlEvents:UIControlEventTouchUpInside];
    [titleBarButtonFirst setTitle:self.roomNo forState:UIControlStateNormal];
    
    [vBarButton addSubview:titleBarButtonFirst];
    
    UIButton *titleBarButtonSecond = [[UIButton alloc] initWithFrame:CGRectMake(0, 22, 200, 22)];
    [titleBarButtonSecond setTag:2];
    [titleBarButtonSecond setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [titleBarButtonSecond.titleLabel setFont:FONT_BUTTON_TOPBAR_SECOND];
    [titleBarButtonSecond addTarget:self action:@selector(hiddenHeaderView) forControlEvents:UIControlEventTouchUpInside];
    [titleBarButtonSecond setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_Guideline] forState:UIControlStateNormal];
    [vBarButton addSubview:titleBarButtonSecond];
    
    [self.navigationItem setTitleView:vBarButton];
    
    //Move title to center vertical
    CGRect titleViewBounds = self.navigationItem.titleView.bounds;
    int moveLeftTitle = 0;
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        moveLeftTitle = 15;
    }
    self.navigationItem.titleView.bounds = CGRectMake(titleViewBounds.origin.x + moveLeftTitle, Y_TOPBAR_TITLE, titleViewBounds.size.width, titleViewBounds.size.height);
    
    //back button navigation
    self.navigationItem.leftBarButtonItem = [MyNavigationBarV2 createBackButtonWithTarget:self selector:@selector(btnBackPressed) forControlEvents:UIControlEventTouchUpInside];
    
    // add topbar
    if (topBarView == nil) {
        topBarView = [[TopbarViewV2 alloc] initViewWithSuperModel:[[CommonVariable sharedCommonVariable] superRoomModel]];
        [self.view addSubview:topBarView];
        topBarView.hidden = YES;
    }
    
     
}

-(void)btnBackPressed
{
    [self.navigationController popViewControllerAnimated:YES];
}

// Intialize fake data
- (void) initializeData {   
    [[HomeViewV2 shareHomeView] waitingLoadingData];
    
    NSString *currentRoomNo = [RoomManagerV2 getCurrentRoomNo];
    RoomModelV2 *rmodel = [[RoomModelV2 alloc] init];
    rmodel.room_Id = currentRoomNo;
    [[RoomManagerV2 sharedRoomManager] loadRoomModel:rmodel];
    
    if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
        int currentUserId = [[UserManagerV2 sharedUserManager] currentUser].userId;
        int buidingId = [[UserManagerV2 sharedUserManager] getBuildingIdByUserId:currentUserId];
        [[GuidelineManagerV2 sharedGuidelineManagerV2] getGuideWSWithHotelId:[[[UserManagerV2 sharedUserManager] currentUser] userHotelsId] buidingId: buidingId roomTypeId:roomTypeId andLastModified:nil/*[[GuidelineManagerV2 sharedGuidelineManagerV2] getLatestLastModifiedOfGuide]*/];
        
    }
    
    self.imageArray = [[GuidelineManagerV2 sharedGuidelineManagerV2] loadAllGuideByRoomType:roomTypeId/*rmodel.room_TypeId*/];
    
    if (HUD) {
        [HUD hide:YES];
        [HUD removeFromSuperview];
    }
    
    [GuidelineTableView reloadData];
    
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
}

- (void)viewDidUnload
{
    [self setGuidelineTableView:nil];
    
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


-(BOOL) checkSaveDataWithTagEvent:(NSInteger) tagOfEvent {
        //NO
        
    if(_isRoomCompleted == NO){
        if(tagOfEvent == tagOfSettingButton){
            UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_complete_room_to_leave] delegate:nil cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles: nil];   
            [alert show];
            
            return YES; //no need to save before leaving, but need to complete the room before leaving
        } else {
            return NO;
        }
  
    } else {
        return NO;
    }
    
}


#pragma mark -
#pragma mark NAVIGATION BAR Methods
#define heightScale 45
#define TABLE_FULL_FRAME CGRectMake(0, 0, 320, 380)
#define TABLE_SORT_FRAME CGRectMake(0, 60, 320, 320)

- (void) hiddenHeaderView {
    //    if (![UserManagerV2 isSupervisor]){        
    BOOL isHidden = topBarView.hidden;
    [topBarView setHidden:!isHidden];
    
    if(!isHidden){
        [self.GuidelineTableView setFrame:TABLE_FULL_FRAME];
    } else {
        [self.GuidelineTableView setFrame:TABLE_SORT_FRAME];
    }
}



#pragma mark -
#pragma mark UITableViewDelegate Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if([LogFileManager isLogConsole])
    {
        NSLog(@"count = %d", (int)[self.imageArray count]);
    }
    return [self.imageArray count];;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}



-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return CELL_HEIGHT;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *guidelineCellIdentifier = @"GuidelineCellV2";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:guidelineCellIdentifier];

    if (cell == nil) {
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"GuidelineCellV2" owner:self options:nil];
        if ([nibArray count] > 0) {
            cell = [nibArray objectAtIndex:0];
        }
    }
    GuideModelV2* anGuide = (GuideModelV2*)[self.imageArray objectAtIndex:indexPath.section];
    UIImageView *imageView = (UIImageView *)[cell viewWithTag:IMAGE_TAG];
    imageView.image = [UIImage imageWithData:anGuide.guide_Image];
    imageView.layer.masksToBounds = YES;
    imageView.layer.cornerRadius = 7.0;
    UILabel* text = (UILabel*)[cell viewWithTag:TEXT_TAG];
    
    if ([[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString:(NSString *)ENGLISH_LANGUAGE]) {
        text.text = anGuide.guide_Name;
    } else {
        text.text = anGuide.guide_NameLang;
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UIBarButtonItem *BackButtonRoomInfo = [[UIBarButtonItem alloc] initWithTitle: [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK] style: UIBarButtonItemStyleBordered target: nil action: nil];    
    [[self navigationItem] setBackBarButtonItem: BackButtonRoomInfo];  

    GuidelineDetailViewV2* aDetail = [[GuidelineDetailViewV2 alloc] initWithNibName:@"GuidelineDetailViewV2" bundle:nil];
    aDetail.guideline = (GuideModelV2*)[self.imageArray objectAtIndex:indexPath.section];
    aDetail.isPushFrom = self.isPushFrom;
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [cell setSelected:NO animated:YES];
    
    //set flag is room completed
    aDetail.isRoomCompleted = _isRoomCompleted;
    
    [self.navigationController pushViewController:aDetail animated:YES];
}


@end
