//
//  MiniBarViewV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ZXingWidgetController.h>
#import "MiniBarSectionViewV2.h"
#import "MiniBarItemCellV2.h"
#import "PickerViewV2.h"
#import "MiniBarSectionInfoV2.h"
#import "MiniBarItemModelV2.h"
#import "CountCategoryModelV2.h"
#import "MiniBarLinenCartViewV2.h"
#import "SyncManagerV2.h"
#import "MBProgressHUD.h"
#import "AccessRight.h"

@interface HistoryPosting : UIViewController <UITableViewDelegate, UITableViewDataSource, PickerViewV2Delegate, UITextFieldDelegate, UIScrollViewDelegate,  ZXingDelegate, MBProgressHUDDelegate, MiniBarSectionViewV2Delegate, MiniBarItemCellV2Delegate>
{
    NSMutableArray *sectionDatas;
    AccessRight *QRCodeScanner;
    NSMutableArray *listHistoryOptions;
    
    NSString *dateFilterSelected;
}
@property (nonatomic) int selectedModule;
@property (nonatomic, strong) IBOutlet UIButton *btnQR;
@property (nonatomic, strong) IBOutlet UIButton *btnOption;
@property (nonatomic, strong) IBOutlet UIButton *btnSelectDate;
@property (nonatomic, strong) IBOutlet UITextField *txtRoom;
@property (nonatomic, strong) IBOutlet UILabel *lblRoomNo;
@property (nonatomic, strong) IBOutlet UIView *viewRoom;
@property (nonatomic, strong) IBOutlet UIImageView *imgHeader;
@property (nonatomic, assign) BOOL isFromMainPosting, isFromPostingFunction;
@property (nonatomic, assign) NSInteger isPushFrom;
@property (nonatomic, strong) IBOutlet UITableView *tbvHistoryPosting;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *topAlignViewRoom;
@property (nonatomic, strong) NSString *roomID;
@end
