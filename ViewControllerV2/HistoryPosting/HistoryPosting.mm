//
//  MiniBarViewV2.m
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "HistoryPosting.h"
#import "MiniBarLinenCartViewV2.h"
#import "CountManagerV2.h"
#import "UserManagerV2.h"
#import "RoomManagerV2.h"
#import "MBProgressHUD.h"
#import "CustomAlertViewV2.h"
#import "CommonVariable.h"
#import "TasksManagerV2.h"
#import "NetworkCheck.h"
#import "RoomAssignmentInfoViewController.h"
#import "CountHistoryManager.h"
#import "CountHistoryModel.h"
#import "MyNavigationBarV2.h"
#import "QRCodeReader.h"
#import "LogFileManager.h"
#import "DeviceManager.h"
#import "iToast.h"
#import "HomeViewV2.h"
#import "STEncryptorDES.h"
#import "ehkDefines.h"
#import "NSString+Common.h"
#define heightMinibarCell 70
#define heightMinibarHeader 60
#define tagItemNameCell  1000
#define tagUsedButtonCell  1001
#define tagNewButtonCell  1002


typedef enum {
    tUsed = 1,
    tNew,
}tType;

@implementation HistoryPosting
@synthesize selectedModule;
@synthesize btnQR;
@synthesize isFromMainPosting;
@synthesize txtRoom;
@synthesize lblRoomNo;
@synthesize tbvHistoryPosting;
@synthesize viewRoom;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
}

#pragma mark - Load Minibar from service

-(void) loadHistoryService:(MBProgressHUD *) HUD {
    //enable flag to announce, is loading data
    [[HomeViewV2 shareHomeView] waitingLoadingData];
    
    CountManagerV2 *countManager = [[CountManagerV2 alloc] init];
    NSMutableArray *listPostingHistory = [countManager getPostingHistoryWSByUserId:[UserManagerV2 sharedUserManager].currentUser.userId roomNumber:txtRoom.text hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId moduleId:selectedModule dateFilter:dateFilterSelected];
    
    if (listPostingHistory.count <= 0) {
        
        [sectionDatas removeAllObjects];
        [tbvHistoryPosting reloadData];

        //hide saving data.
        [self performSelector:@selector(hiddenHUDAfterSaved:) withObject:HUD afterDelay:0.0f];
        
        //end loading data
        [[HomeViewV2 shareHomeView] endWaitingLoadingData];
        return;
    }
    
    
    NSArray *sortedPostingCategoryHistory = [listPostingHistory sortedArrayUsingComparator:^NSComparisonResult(eHousekeepingService_PostingHistoryDetails *obj1, eHousekeepingService_PostingHistoryDetails *obj2) {
        return [obj1.CategoryDescription compare:obj2.CategoryDescription];
    }];
    
    NSArray *sortedPostingItemHistory = [listPostingHistory sortedArrayUsingComparator:^NSComparisonResult(eHousekeepingService_PostingHistoryDetails *obj1, eHousekeepingService_PostingHistoryDetails *obj2) {
        return [obj1.ItemDescription compare:obj2.ItemDescription];
    }];
    if (self.isFromPostingFunction) {
        sortedPostingItemHistory = [listPostingHistory sortedArrayUsingComparator:^NSComparisonResult(eHousekeepingService_PostingHistoryDetails *obj1, eHousekeepingService_PostingHistoryDetails *obj2) {
            NSDate *date1 = [ehkConvert convertStringToDate:obj1.TransactionDateTime formatInputString:@"yyyy-MM-dd HH:mm:ss"];
            NSDate *date2 = [ehkConvert convertStringToDate:obj2.TransactionDateTime formatInputString:@"yyyy-MM-dd HH:mm:ss"];
            return [date1 compare:date2];
        }];
    }
    if (self.isPushFrom == IS_PUSHED_FROM_PHYSICAL_CHECK) {
        
        sectionDatas = [NSMutableArray new];
        for (int i = 0; i < sortedPostingCategoryHistory.count; i ++) {
            eHousekeepingService_PostingHistoryDetails *curPostingHistory = sortedPostingCategoryHistory[i];
            NSString *status = @"";
            int currentStatus = [curPostingHistory.NewQuantity intValue];
            if (currentStatus == room_Status_Id_VC) {
                status = @"VC";
            }else if (currentStatus == room_Status_Id_VD) {
                status = @"VD";
            }else if (currentStatus == room_Status_Id_OC) {
                status = @"OC";
            }else if (currentStatus == room_Status_Id_OD) {
                status = @"OD";
            }else if (currentStatus == room_Status_Id_OO) {
                status = @"OO";
            }else if (currentStatus == room_Status_Id_VI) {
                status = @"VI";
            }else if (currentStatus == room_Status_Id_OI) {
                status = @"OI";
            }else if (currentStatus == room_Status_Id_VPU) {
                status = @"VPU";
            }else if (currentStatus == room_Status_Id_OCSO) {
                status = @"OCSO";
            }else if (currentStatus == room_Status_Id_OOS) {
                status = @"OOS";
            }
            curPostingHistory.CategoryDescription = status;
        }
        [sectionDatas addObject:sortedPostingItemHistory];
    }else{
        sectionDatas = [NSMutableArray array];
        
        NSMutableSet *listCategoryName = [NSMutableSet set];
        NSArray *arrayCateggoryName = nil;
        if (self.isFromPostingFunction) {
            for (int j = 0; j < sortedPostingItemHistory.count; j++ ) {
                eHousekeepingService_PostingHistoryDetails *detail = sortedPostingItemHistory[j];
                NSString *userName = detail.UserName;
                if (![listCategoryName containsObject:userName]) {
                    [listCategoryName addObject:userName];
                }
            }
            arrayCateggoryName = [listCategoryName allObjects];
            arrayCateggoryName = [arrayCateggoryName sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
        }else{
            for (int i = 0; i < sortedPostingCategoryHistory.count; i ++) {
                eHousekeepingService_PostingHistoryDetails *curPostingHistory = sortedPostingCategoryHistory[i];
                NSString *categoryName = curPostingHistory.CategoryDescription;
                if (categoryName.length > 0) {
                    [listCategoryName addObject:categoryName];
                } else {
                    [listCategoryName addObject:@""];
                }
            }
            arrayCateggoryName = [listCategoryName allObjects];
        }
            
        for(int i = 0; i < arrayCateggoryName.count; i ++) {
            MiniBarSectionInfoV2 *sectionInfo = [[MiniBarSectionInfoV2 alloc] init];
            
            MiniBarCategoryModelV2 *categoryModel = [[MiniBarCategoryModelV2 alloc] init];
            categoryModel.minibarCategoryID = 1;
            categoryModel.minibarCategoryTitle = arrayCateggoryName[i];
            //categoryModel.minibarCategoryImage = UIImagePNGRepresentation([UIImage imageWithData:countCategories.countCategoryImage]);
            sectionInfo.minibarCategory = categoryModel;
            
            //Buffer history item
            MiniBarItemModelV2 *itemModel = nil;
            
            for (int j = 0; j < sortedPostingItemHistory.count; j++ ) {
                
                eHousekeepingService_PostingHistoryDetails *detail = sortedPostingItemHistory[j];
                NSString *categoryName = self.isFromPostingFunction ? detail.UserName : detail.CategoryDescription;
                if (categoryModel.minibarCategoryTitle.length <= 0 && categoryName.length <= 0) {
                    
                    if (!itemModel) { //If don't have any item
                        //Append item into category null
                        itemModel = [[MiniBarItemModelV2 alloc] init];
                        itemModel.minibarItemID = 0;
                        itemModel.minibarItemTitle = detail.ItemDescription;
                        //itemModel.minibarItemImage = UIImagePNGRepresentation([UIImage imageWithData:countItems.countItemImage]);
                        itemModel.minibarItemUsed = [detail.UsedQuantity integerValue];
                        itemModel.minibarItemNew = [detail.NewQuantity integerValue];
                        itemModel.CategoryDescription = detail.CategoryDescription;
                        [sectionInfo insertObjectToNextIndex:itemModel];
                        
                    } else if ([itemModel.minibarItemTitle caseInsensitiveCompare:detail.ItemDescription] != NSOrderedSame) {
                        //Append item into category null
                        itemModel = [[MiniBarItemModelV2 alloc] init];
                        itemModel.minibarItemID = 0;
                        itemModel.minibarItemTitle = detail.ItemDescription;
                        //itemModel.minibarItemImage = UIImagePNGRepresentation([UIImage imageWithData:countItems.countItemImage]);
                        itemModel.minibarItemUsed = [detail.UsedQuantity integerValue];
                        itemModel.minibarItemNew = [detail.NewQuantity integerValue];
                        itemModel.CategoryDescription = detail.CategoryDescription;
                        [sectionInfo insertObjectToNextIndex:itemModel];
                        
                    } else {
                        itemModel.minibarItemUsed += [detail.UsedQuantity integerValue];
                        itemModel.minibarItemNew += [detail.NewQuantity integerValue];
                    }
                    
                } else {
                    if ([categoryModel.minibarCategoryTitle caseInsensitiveCompare:categoryName] == NSOrderedSame) {
                        if (!itemModel) { //If don't have any item
                            //Append item into category null
                            itemModel = [[MiniBarItemModelV2 alloc] init];
                            itemModel.minibarItemID = 0;
                            itemModel.minibarItemTitle = detail.ItemDescription;
                            //itemModel.minibarItemImage = UIImagePNGRepresentation([UIImage imageWithData:countItems.countItemImage]);
                            itemModel.minibarItemUsed = [detail.UsedQuantity integerValue];
                            itemModel.minibarItemNew = [detail.NewQuantity integerValue];
                            itemModel.CategoryDescription = detail.CategoryDescription;
                            [sectionInfo insertObjectToNextIndex:itemModel];
                            
                        } else if ([itemModel.minibarItemTitle caseInsensitiveCompare:detail.ItemDescription] != NSOrderedSame) {
                            //Append item into category null
                            itemModel = [[MiniBarItemModelV2 alloc] init];
                            itemModel.minibarItemID = 0;
                            itemModel.minibarItemTitle = detail.ItemDescription;
                            //itemModel.minibarItemImage = UIImagePNGRepresentation([UIImage imageWithData:countItems.countItemImage]);
                            itemModel.minibarItemUsed = [detail.UsedQuantity integerValue];
                            itemModel.minibarItemNew = [detail.NewQuantity integerValue];
                            itemModel.CategoryDescription = detail.CategoryDescription;
                            [sectionInfo insertObjectToNextIndex:itemModel];
                            
                        } else {
                            itemModel.minibarItemUsed += [detail.UsedQuantity integerValue];
                            itemModel.minibarItemNew += [detail.NewQuantity integerValue];
                        }
                    }
                }
            }
            
            [sectionDatas addObject:sectionInfo];
        }
        if (self.isFromPostingFunction) {
            NSMutableArray *newSectionDatas = [NSMutableArray new];
            for (int i = 0; i < sectionDatas.count; i++ ) {
                MiniBarSectionInfoV2 *sessionInfo = sectionDatas[i];
                NSArray *listItem = sessionInfo.rowHeights;
                listItem = [listItem sortedArrayUsingComparator:^NSComparisonResult(eHousekeepingService_PostingHistoryDetails *obj1, eHousekeepingService_PostingHistoryDetails *obj2) {
                    return [obj1.CategoryDescription compare:obj2.CategoryDescription];
                }];

                NSString *currectCategoryString = @"";
                NSMutableArray *newListeItem = [NSMutableArray new];
                for (int j=0; j<listItem.count; j++) {
                    MiniBarItemModelV2 *itemModel = listItem[j];
                    if (currectCategoryString.length == 0 || ![currectCategoryString isEqualToString:itemModel.CategoryDescription]) {
                        currectCategoryString = itemModel.CategoryDescription;
                        [newListeItem addObject:currectCategoryString];
                    }
                    [newListeItem addObject:itemModel];
                }
                [sessionInfo replaceAllRownHeight:newListeItem];
                [newSectionDatas addObject:sessionInfo];
            }
            sectionDatas = newSectionDatas;
        }
        
    }
    [tbvHistoryPosting reloadData];
    //hide saving data.
    [self performSelector:@selector(hiddenHUDAfterSaved:) withObject:HUD afterDelay:0.0f];
    
    //end loading data
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
}


#pragma mark - View lifecycle
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    //Set high light button menu
    if(_isPushFrom == IS_PUSHED_FROM_FIND_BY_RA || _isPushFrom == IS_PUSHED_FROM_FIND_BY_ROOM){
        [[HomeViewV2 shareHomeView] setSelectedButton:tagOfFindButton];
    } else if(_isPushFrom == IS_PUSHED_FROM_ROOM_ASSIGNMENT || _isPushFrom == IS_PUSHED_FROM_COMPLETE){
        [[HomeViewV2 shareHomeView] setSelectedButton:tagOfHomeButton];
    }
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    if(!isDemoMode){
        [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    if (self.isFromPostingFunction) {
        [txtRoom setText:self.roomID];
    }
    AccessRightModel *currentUserAccessRight = [[UserManagerV2 sharedUserManager] currentUserAccessRight];
    QRCodeScanner = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.QRCodeScanner];
    if (QRCodeScanner.isActive && !isDemoMode) {
        [btnQR setAlpha:1.0f];
        [btnQR setEnabled:YES];
        [txtRoom setEnabled:NO];
        [txtRoom setTextColor:[UIColor blackColor]];
    } else {
        [btnQR setHidden:NO];
        [btnQR setAlpha:0.7f];
        [btnQR setEnabled:NO];
        [txtRoom setEnabled:YES];
        [txtRoom setTextColor:[UIColor grayColor]];
    }
    
    int roomAssignment = (int)[TasksManagerV2 getCurrentRoomAssignment];
    RoomAssignmentModelV2 *roomAssignmentModel = [[RoomAssignmentModelV2 alloc] init];
    roomAssignmentModel.roomAssignment_Id = roomAssignment;
    [[RoomManagerV2 sharedRoomManager]getRoomIdByRoomAssignment:roomAssignmentModel];
    [txtRoom setText:roomAssignmentModel.roomAssignment_RoomId];
    
    //Resize content views of current VC to fit with Navigation bar and tabbar
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        [self loadFlatResource];
    }
    
    listHistoryOptions = [self loadOptionsService];
    if (!self.isFromPostingFunction) {
        selectedModule = ModuleHistory_Minibar;
    }
    
    [self setFilterDate:[NSDate date]];
    [self loadHistoryData];
    
    //back button navigation
    self.navigationItem.leftBarButtonItem = [MyNavigationBarV2 createBackButtonWithTarget:self selector:@selector(backBarPressed) forControlEvents:UIControlEventTouchUpInside];
    
    //[self performSelector:@selector(loadTopbarView)];
    
    if(isFromMainPosting)
    {
        [txtRoom setText:@""];
        [txtRoom setEnabled:YES];
        [txtRoom setTextColor:[UIColor blackColor]];
        
        
    }
    else
    {
        int roomAssignment = (int)[TasksManagerV2 getCurrentRoomAssignment];
        RoomAssignmentModelV2 *roomAssignmentModel = [[RoomAssignmentModelV2 alloc] init];
        roomAssignmentModel.roomAssignment_Id = roomAssignment;
        [[RoomManagerV2 sharedRoomManager]getRoomIdByRoomAssignment:roomAssignmentModel];
        [txtRoom setText:roomAssignmentModel.roomAssignment_RoomId];
        [txtRoom setEnabled:NO];
        [txtRoom setTextColor:[UIColor grayColor]];
    }
    if (self.isFromPostingFunction) {
        //Set title for label
        [lblRoomNo setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOM_NO]];
        [_btnOption setTitle:[L_MINIBAR currentKeyToLanguage] forState:UIControlStateNormal];
        switch (self.selectedModule) {
            case ModuleHistory_Minibar:{
                [_btnOption setTitle:[L_MINIBAR currentKeyToLanguage] forState:UIControlStateNormal];
                break;
            }case ModuleHistory_Linen:{
                [_btnOption setTitle:[L_Linen_Title currentKeyToLanguage] forState:UIControlStateNormal];
                break;
            }case ModuleHistory_Amenities:{
                [_btnOption setTitle:[L_AMENITIES currentKeyToLanguage] forState:UIControlStateNormal];
                break;
            }case ModuleHistory_LostAndFound:{
                [_btnOption setTitle:[L_LOSTANDFOUND_CASE_TITLE currentKeyToLanguage] forState:UIControlStateNormal];
                break;
            }case ModuleHistory_Engineering:{
                [_btnOption setTitle:[L_ENGINEERING_CASE currentKeyToLanguage] forState:UIControlStateNormal];
                break;
            }case ModuleHistory_Physical:{
                [_btnOption setTitle:[L_Physical_Check_Title currentKeyToLanguage] forState:UIControlStateNormal];
                break;
            }
            default:
                break;
        }
        
        _btnOption.enabled = NO;
        [txtRoom setText:self.roomID];
    }
    [self loadTopbarView];
}

#pragma mark - View Action

-(void)setFilterDate:(NSDate*)filterDate
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd 00:00:00"];
    dateFilterSelected = [dateFormat stringFromDate:filterDate];
    [dateFormat setDateFormat:@"dd MMM yyyy"];
    [_btnSelectDate setTitle:[dateFormat stringFromDate:filterDate]  forState:UIControlStateNormal];
}

-(IBAction)btnOptionServicePressed:(id)sender
{
    PickerViewV2 *picker = [[PickerViewV2 alloc] initPickerViewV2WithDatas:listHistoryOptions AndSelectedData:@"" AndIndex:nil];
    [picker setDelegate:self];
    [picker showPickerViewV2];
}

-(IBAction)btnDatePressPressed:(id)sender
{
    PickerViewV2 *picker = [[PickerViewV2 alloc] initPickerViewV2WithDatas:nil AndSelectedData:@"" AndIndex:nil];
    [picker shouldShowDatePicker:YES];
    [picker showPickerViewV2];
    [picker setDelegate:self];
}

-(void)backBarPressed {
    
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - UITableView Datasource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return sectionDatas.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.isPushFrom == IS_PUSHED_FROM_PHYSICAL_CHECK) {
        NSArray *listItem = sectionDatas[section];
        return listItem.count;
    }else{
        MiniBarSectionInfoV2 *sectioninfo = [sectionDatas objectAtIndex:section];
        NSInteger numberRowInSection = sectioninfo.rowHeights.count;
        return sectioninfo.open ? numberRowInSection : 0;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.isPushFrom == IS_PUSHED_FROM_PHYSICAL_CHECK) {
        NSArray *arrItem = sectionDatas[indexPath.section];
        static NSString *identifycell = @"cellidentify";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifycell];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifycell];
            cell.backgroundColor = [UIColor clearColor];
            cell.contentView.backgroundColor = [UIColor clearColor];
            cell.textLabel.textAlignment = NSTextAlignmentCenter;
            UIView *breakView = [[UIView alloc] initWithFrame:CGRectMake(0, heightMinibarCell-1, [UIScreen mainScreen].bounds.size.width, 1)];
            breakView.backgroundColor = [UIColor lightGrayColor];
            [cell.contentView addSubview:breakView];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        eHousekeepingService_PostingHistoryDetails *curPostingHistory = arrItem[indexPath.row];
        cell.textLabel.text = curPostingHistory.CategoryDescription;
        return cell;
        
    }else{
        static NSString *identifycell = @"cellidentify";
        MiniBarItemCellV2 *cell = nil;
        
        cell = (MiniBarItemCellV2 *)[tableView dequeueReusableCellWithIdentifier:identifycell];
        if (cell == nil) {
            NSArray *array = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([MiniBarItemCellV2 class]) owner:self options:nil];
            cell = [array objectAtIndex:0];
            
            if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                cell.btnNewItem.layer.cornerRadius = 7.0f;
                cell.btnNewItem.layer.borderWidth = 1.0f;
                cell.btnNewItem.layer.borderColor = [[UIColor lightGrayColor] CGColor];
                
                cell.btnUsedItem.layer.cornerRadius = 7.0f;
                cell.btnUsedItem.layer.borderWidth = 1.0f;
                cell.btnUsedItem.layer.borderColor = [[UIColor lightGrayColor] CGColor];
            }
        }
        
        cell.delegate = self;
        MiniBarSectionInfoV2 *sectionInfo = [sectionDatas objectAtIndex:indexPath.section];
        MiniBarItemModelV2 *model = nil;
        if (self.isFromPostingFunction) {
            if ([[sectionInfo.rowHeights objectAtIndex:indexPath.row] isKindOfClass:[NSString class]]) {
                [cell.lblTitleItem setText:[sectionInfo.rowHeights objectAtIndex:indexPath.row]];
                [cell.lblNewItem setHidden:YES];
                [cell.lblUsedItem setHidden:YES];
                [cell.btnUsedItem setHidden:YES];
                [cell.btnNewItem setHidden:YES];
                cell.lblTitleItem.textAlignment = NSTextAlignmentCenter;
                cell.lblTitleItem.frame = CGRectMake(0, 10, [UIScreen mainScreen].bounds.size.width, heightMinibarCell - 20);
                [cell setPath:indexPath];
                return cell;
            }else{
                [cell.lblNewItem setHidden:NO];
                [cell.lblUsedItem setHidden:NO];
                [cell.btnUsedItem setHidden:NO];
                [cell.btnNewItem setHidden:NO];
                cell.lblTitleItem.textAlignment = NSTextAlignmentLeft;
                cell.lblTitleItem.frame = CGRectMake(70, 4, 120, 62);
            }
        }
        
        model = [sectionInfo.rowHeights objectAtIndex:indexPath.row];
        
        UIImage *image = [UIImage imageWithData:model.minibarItemImage];
        [cell.imgItemCell setImage:image];
        [cell.lblTitleItem setText:model.minibarItemTitle];
        
        if ((indexPath.row == 0 && !self.isFromPostingFunction) || (indexPath.row == 1 && self.isFromPostingFunction)){
            [cell.lblNewItem setHidden:NO];
            [cell.lblUsedItem setHidden:NO];
            [cell.lblNewItem setText:[[LanguageManagerV2 sharedLanguageManager] getNew]];
            [cell.lblUsedItem setText:[[LanguageManagerV2 sharedLanguageManager] getUsed]];
            [cell.btnUsedItem setFrame:CGRectMake(189, 27, 54, 37)]; // CFG [20170308/CRF-00001614] - Changed from 256, 27, 54, 37
            [cell.btnNewItem setFrame:CGRectMake(256, 27, 54, 37)]; // CFG [20170308/CRF-00001614] - Changed from 189, 27, 54, 37
            
        } else {
            [cell.lblNewItem setHidden:YES];
            [cell.lblUsedItem setHidden:YES];
            [cell.lblNewItem setText:@""];
            [cell.lblUsedItem setText:@""];
            [cell.btnUsedItem setFrame:CGRectMake(189, 27, 54, 37)]; // CFG [20170308/CRF-00001614] - Changed from 256, 27, 54, 37
            [cell.btnNewItem setFrame:CGRectMake(256, 27, 54, 37)]; // CFG [20170308/CRF-00001614] - Changed from 189, 27, 54, 37
        }
        
        if (selectedModule == ModuleHistory_Minibar) {
            [cell.lblNewItem setText:[[LanguageManagerV2 sharedLanguageManager] getNew]];
            [cell.lblUsedItem setText:[[LanguageManagerV2 sharedLanguageManager] getUsed]];
            
        } else if(selectedModule == ModuleHistory_Linen) {
            [cell.lblUsedItem setText:[L_linen_item_used currentKeyToLanguage]]; // CFG [20170308/CRF-00001614]
            [cell.lblNewItem setText:[L_amenity_item_new currentKeyToLanguage]]; // CFG [20170308/CRF-00001614]
        } else if(selectedModule == ModuleHistory_Amenities) {
            [cell.lblUsedItem setText:[L_amenity_item_used currentKeyToLanguage]];
            [cell.lblNewItem setText:[L_amenity_item_new currentKeyToLanguage]];
            
        } else if (selectedModule == ModuleHistory_Engineering) {
            [cell.lblUsedItem setHidden:YES];
            [cell.lblNewItem setHidden:YES];
            [cell.btnNewItem setHidden:YES];
            [cell.btnUsedItem setHidden:YES];
            
        } else if(selectedModule == ModuleHistory_LostAndFound) {
            [cell.lblUsedItem setHidden:YES];
            [cell.lblNewItem setHidden:YES];
            [cell.btnNewItem setHidden:NO];
            [cell.btnUsedItem setHidden:YES];
        }
        
        //add target for button used and new
        [cell.btnUsedItem addTarget:self action:@selector(tapbtnUsed) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnNewItem addTarget:self action:@selector(tapbtnNew) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnUsedItem setTitle:[NSString stringWithFormat:@"%d", (int)model.minibarItemUsed] forState:UIControlStateNormal];
        [cell.btnNewItem setTitle:[NSString stringWithFormat:@"%d", (int)model.minibarItemNew] forState:UIControlStateNormal];
        
        
        [cell.lblTitleItem setTag:tagItemNameCell];
        [cell.btnNewItem setTag:tagNewButtonCell];
        [cell.btnUsedItem setTag:tagUsedButtonCell];
        [cell setPath:indexPath];
        
        return cell;
    }
    return [UITableViewCell new];
}

-(void)tapbtnUsed{
    [txtRoom resignFirstResponder];
}
-(void)tapbtnNew{
    [txtRoom resignFirstResponder];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return heightMinibarCell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (self.isPushFrom == IS_PUSHED_FROM_PHYSICAL_CHECK) {
        return 0;
    }
    return heightMinibarHeader;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (self.isPushFrom == IS_PUSHED_FROM_PHYSICAL_CHECK) {
        return [UIView new];
    }
    MiniBarSectionInfoV2 *sectionInfo = [sectionDatas objectAtIndex:section];
    
    if (sectionInfo.headerView == nil) {
        MiniBarSectionViewV2 *headerView = [[MiniBarSectionViewV2 alloc] initWithSection:section CategoryModelV2:sectionInfo.minibarCategory andOpenStatus:NO];
        sectionInfo.headerView = headerView;
        headerView.delegate = self;
    }
    return sectionInfo.headerView;
}

#pragma mark - Calculate layout

//Calculate total frame of table Minibar
-(CGRect)totalTableMiniBarFrame
{
    CGFloat heightHeaders = 0;
    CGFloat heightCells = 0;
    for (MiniBarSectionInfoV2 *currentSection in sectionDatas) {
        heightHeaders += heightMinibarHeader;
        if(currentSection.open == YES){
            heightCells += [currentSection.rowHeights count] * heightMinibarCell;
        }
    }
    
    CGRect result = tbvHistoryPosting.frame;
    result.size.height = heightHeaders + heightCells;
    return result;
}

#pragma mark - Section Header Delegate
-(void)sectionHeaderView:(MiniBarSectionViewV2 *)sectionheaderView sectionOpened:(NSInteger)section {
    
    MiniBarSectionInfoV2 *sectionInfo = [sectionDatas objectAtIndex:section];
    sectionInfo.open = YES;
    
    NSInteger countOfRowsToInsert = [sectionInfo.rowHeights count];
    NSMutableArray *indexPathsToInsert = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i < countOfRowsToInsert; i++) {
        [indexPathsToInsert addObject:[NSIndexPath indexPathForRow:i inSection:section]];
    }
    
    UITableViewRowAnimation insertAnimation;
    insertAnimation = UITableViewRowAnimationAutomatic;
    
    [self.tbvHistoryPosting beginUpdates];
    
    [self.tbvHistoryPosting insertRowsAtIndexPaths:indexPathsToInsert withRowAnimation:insertAnimation];
    [self.tbvHistoryPosting endUpdates];
    
    if([indexPathsToInsert count] > 0)
    {
        [self.tbvHistoryPosting scrollToRowAtIndexPath:[indexPathsToInsert objectAtIndex:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    
}

-(void)sectionHeaderView:(MiniBarSectionViewV2 *)sectionheaderView sectionClosed:(NSInteger)section {
    
    
    MiniBarSectionInfoV2 *sectionInfo = [sectionDatas objectAtIndex:section];
    
    sectionInfo.open = NO;
    
    
    NSInteger countOfRowsToDelete = [self.tbvHistoryPosting numberOfRowsInSection:section];
    if (countOfRowsToDelete > 0) {
        NSMutableArray *indexPathsToDelete = [[NSMutableArray alloc] init];
        for (NSInteger i = 0; i < countOfRowsToDelete; i++) {
            [indexPathsToDelete addObject:[NSIndexPath indexPathForRow:i inSection:section]];
        }
        
        [tbvHistoryPosting beginUpdates];
        [self.tbvHistoryPosting deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:UITableViewRowAnimationAutomatic];
        [tbvHistoryPosting endUpdates];
    }
}

#pragma mark - Item Cell Delegate Methods
-(void)btnUsedItemCellPressedWithIndexPath:(NSIndexPath *)path sender:(UIButton *)button{
    
}

-(void)btnNewItemCellPressedWithIndexPath:(NSIndexPath *)path sender:(UIButton *)button{
    
}

#pragma mark - History Processing

-(NSMutableArray*) loadOptionsService
{
    NSMutableArray *listOptions = [NSMutableArray array];
    [listOptions addObject:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_MINIBAR]];
    [listOptions addObject:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_LINEN]];
    [listOptions addObject:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_AMENITIES]];
    [listOptions addObject:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_LOST_FOUND]];
    [listOptions addObject:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ENGINEERING_CASE]];
    return listOptions;
}

-(void) loadHistoryData
{
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getRaLoadingData]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES];
    
    [self performSelector:@selector(loadHistoryService:) withObject:HUD afterDelay:0.0f];
}

#pragma mark - PickerView V2 Delegate methods
-(void) didChooseDate:(PickerViewV2*)pickerView withDate:(NSDate*)dateSelected
{
    [self setFilterDate:dateSelected];
    [self loadHistoryData];
}

-(void)didChoosePickerViewV2WithData:(NSString *)data AndIndex:(NSIndexPath *)index WithtIndex:(NSInteger)tIndex{
    
    [_btnOption setTitle:data forState:UIControlStateNormal];

    NSInteger indexSelected = NSNotFound;
    for (int i = 0; i < listHistoryOptions.count; i ++) {
        if ([listHistoryOptions[i] isEqualToString:data]) {
            indexSelected = i;
            break;
        }
    }
    
    if (indexSelected != NSNotFound) {
        selectedModule = (int)(indexSelected + 1);
        //[self setFilterDate:[NSDate date]];
        [self loadHistoryData];
    } else {
        [sectionDatas removeAllObjects];
    }
}


#pragma didden after save.
-(void) hiddenHUDAfterSaved:(MBProgressHUD *)HUD{
    [HUD hide:YES];
    [HUD removeFromSuperview];
    //    [HUD release];
}

#pragma mark - Handle Topbar Methods

-(void)loadTopbarView {
    TopbarViewV2 *topbar = [[TopbarViewV2 alloc] initViewWithSuperModel:[[CommonVariable sharedCommonVariable] superRoomModel]];
    [topbar setTag:tagTopbarView];
    [self.view addSubview:topbar];
    [topbar setHidden:YES];
    
    [topbar setAlpha:0.0];
    [self addButtonHandleShowHideTopbar];
}


-(void)showTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationWillStartSelector:@selector(setHiddenTopbarView)];
    
    f = topbar.frame;
    f.origin.y = 0;
    [topbar setFrame:f];
    [topbar setAlpha:1.0];
    
    [self adjustShowForViews];
    
    [UIView commitAnimations];
}

-(void)hideTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDidStopSelector:@selector(setHiddenTopbarView)];
    [UIView setAnimationDelegate:self];
    
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    [topbar setAlpha:0.0];
    
    [self adjustRemoveForViews];
    
    [UIView commitAnimations];
}

-(void)setHiddenTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    [topbar setHidden:!topbar.hidden];
}

-(TopbarViewV2 *)getTopBarView {
    return (TopbarViewV2 *)[self.view viewWithTag:tagTopbarView];
}

-(void)addButtonHandleShowHideTopbar {
    UIView *vBarButton = [[UIView alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
    UIButton *titleBarButtonFirst = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 200, 30)];
    
    titleBarButtonFirst.titleLabel.backgroundColor = [UIColor clearColor];
    [titleBarButtonFirst.titleLabel setFont: FONT_BUTTON_TOPBAR];
    titleBarButtonFirst.titleLabel.textAlignment = NSTextAlignmentCenter;
    titleBarButtonFirst.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
    
    [titleBarButtonFirst.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
    [titleBarButtonFirst.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
    [titleBarButtonFirst addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    if(isFromMainPosting){
        [titleBarButtonFirst setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_Counts] forState:UIControlStateNormal];
    } else {
        [titleBarButtonFirst setTitle:txtRoom.text forState:UIControlStateNormal];
    }
    
    [vBarButton addSubview:titleBarButtonFirst];
    
    
    UIButton *titleBarButtonSecond = [[UIButton alloc] initWithFrame:CGRectMake(0, 28, 200, 22)];
    [titleBarButtonSecond setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [titleBarButtonSecond.titleLabel setFont:FONT_BUTTON_TOPBAR_SECOND];
    [titleBarButtonSecond setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_posting_history] forState:UIControlStateNormal];//Set title for Tabbar
    [titleBarButtonSecond addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    [vBarButton addSubview:titleBarButtonSecond];
    
    self.navigationItem.titleView = vBarButton;
    
    //Move title to center vertical
    CGRect titleViewBounds = self.navigationItem.titleView.bounds;
    int moveLeftTitle = 0;
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        moveLeftTitle = 10;
    }
    self.navigationItem.titleView.bounds = CGRectMake(titleViewBounds.origin.x + moveLeftTitle, Y_TOPBAR_TITLE, titleViewBounds.size.width, titleViewBounds.size.height);
}

-(void)topbarTapped:(UIButton *)sender {
    TopbarViewV2 *topbar = [self getTopBarView];
    [sender setEnabled:NO];
    if (topbar.isHidden) {
        [self showTopbarView];
    } else {
        [self hideTopbarView];
    }
    [self performSelector:@selector(topbarButtonEnable:) withObject:sender afterDelay:0.5];
}

-(void) topbarButtonEnable:(UIButton *) topbarBtn {
    [topbarBtn setEnabled:YES];
}

-(void)adjustShowForViews {
    //adjust all views when show
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    _topAlignViewRoom.constant = f.size.height;
    
}

-(void)adjustRemoveForViews {
    _topAlignViewRoom.constant = 0;
}


/**
 * Name : shouldChangeCharactersInRange
 * Description : For hiding the keyboard. This must be implemented if you are working with UITextField
 * Parameter :
 * Return : BOOL
 */
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if([string isEqualToString:@"\n"])
    {
        [textField resignFirstResponder];
        return NO;
    }
    return YES;
}


/**
 * Name : textFieldShouldReturn
 * Description : Hiding the keyboard of UITextField
 * Parameter :
 * Return : BOOL
 */
- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - QR Code Button Touched
-(IBAction)btnQRCodeScannerTouched:(id) sender
{
    ZXingWidgetController *widController = [[ZXingWidgetController alloc] initWithDelegate:self showCancel:YES OneDMode:NO showLicense:NO];
    QRCodeReader* qrcodeReader = [[QRCodeReader alloc] init];
    NSSet *readers = [[NSSet alloc ] initWithObjects:qrcodeReader,nil];
    widController.readers = readers;
    [self presentViewController:widController animated:YES completion:nil];
}

#pragma mark - ZxingDelegate
- (void)zxingController:(ZXingWidgetController*)controller didScanResult:(NSString *)resultString {
    resultString = [STEncryptorDES convertText:resultString];
    [self dismissViewControllerAnimated:YES completion:nil];
    
    txtRoom.text = resultString;
    [self textFieldShouldReturn:txtRoom];
}

- (void)zxingControllerDidCancel:(ZXingWidgetController*)controller {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - iOS 7 Resources
-(void)loadFlatResource
{
    //[imgHeader setImage:[UIImage imageNamed:imgGuestInfoActiveFlat]];
}

@end
