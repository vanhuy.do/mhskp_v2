//
//  HomeView.h
//  eHouseKeeping
//
//  Created by tms on 5/6/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ZXingWidgetController.h>
#import "LoginViewV2.h"
#import "RoomCompletedViewV2.h"
#import "TasksManagerV2.h"
#import "CheckListViewV2.h"
#import "TopbarViewV2.h"
#import "WifiIConViewController.h"
#import "CustomBadge.h"
#import "SettingViewV2.h"
#import "AccessRight.h"
#import "CleaningStatusPopupView.h"
#import "CleaningStatusConfirmPopupView.h"
#import "MBProgressHUD.h"
#import "AdditionalJobManagerV2.h"
#import "SlidingMenuBar.h"
#import "PickerViewV2.h"
#import "SupervisorFilterDetail.h"
#import "eHouseKeepingAppDelegate.h"

@interface HomeViewV2 : UIViewController <UINavigationControllerDelegate, UITableViewDelegate, UITableViewDataSource, UIAlertViewDelegate,UIGestureRecognizerDelegate, LoginViewDelegate, SettingViewDelegate, CleaningStatusPopupDelegate, CleaningStatusConfirmPopupDelegate, MBProgressHUDDelegate, ZXingDelegate, SlidingMenuBarDelegate, PickerViewV2Delegate>{
    
    IBOutlet UITableView *roomTableView;
    IBOutlet UITableViewCell *tvCell;
    
    IBOutlet UIButton *butViewCleanedRooms;
    IBOutlet UILabel *labelHousekeeperName;
    //Data for room assignment
    NSMutableArray *roomAssignmentData;
    
    //UIScrollView *tabBarScrollView;
    int countTabbarButtons;
    NSArray * arrButTabbar;
    
    //Variable for Check Restriction RoomAssign
    BOOL        isRestriction; // AnLe Work Here

    LoginViewV2 *loginView;
    //Variable for cancel thread syncRoomAssignment
    BOOL        isSyncMethod;
    //Variable for synNow
    BOOL        isSyncNow;
    //Variable for Login View is showwing
    BOOL        isLoginShowwing;
    //Variable for sync period
    CGFloat     syncPeriod;
    //Touch button to sync
    BOOL        isTouchSync;
    //variable for begin sync
    STATUS_START_SYNC_NOW        startSyncNow;
    
    //variable for getting message
    BOOL        isGettingMessage;
    
    //variable for is loading data
    BOOL        isLoadingData;
    
    //Thread syncRoomAssignment
    NSThread    *threadSyncWithPeriod;
    
    BOOL isStartedMessageReciever; // timer to get message is working or not
    
    // variable for cancel.
    BOOL isAbleAnnouceMessage;
    // variable for period get new message:
    CGFloat AnnounceMessagePeriod;
    //Variable completedRoomView
    RoomCompletedViewV2 *roomCompletedView;
    //Flag isEditing
    BOOL isEdit;
    BOOL isDueRoom;
    
    //Array contain syncData RoomAssign when tableview is editing
    //    NSMutableArray *syncData;
    //selected button in tabbar
    //NSInteger currentSelected;
    //selected button GuestInfo or RoomDetail
    NSString *currentSelectedView;
    IBOutlet UIImageView *hotelLogo;
    TopbarViewV2 * topBarView;    //
    IBOutlet UILabel *nameUserLabel;
    IBOutlet UILabel *buildingNameLabel;
    IBOutlet UILabel *label_location_property_name;
    IBOutlet UILabel *labelRoom;
    IBOutlet UILabel *labelRMStatus;
    IBOutlet UILabel *labelVIP;
    IBOutlet UILabel *labelCleaningStatus;
    IBOutlet UILabel *labelHousekeeper;
    IBOutlet UILabel *labelGuestCell;
    Boolean isOpeningPopupNotification;
    WifiIConViewController * wifiViewController;
    NSInteger numberOfMustPostRecords;
    
    BOOL isSynCompletedDialogShowing;
    BOOL isSynInCompletedDialogShowing;
    
    MBProgressHUD *waitingProgress;
    
    BOOL isReloadData;
    
    int countInThread;
    NSInteger numberOfTagButton;
    
    AccessRight *roomAssignment;
    AccessRight *restrictionAssignment;
    AccessRight *roomCompleted;
    AccessRight *manualUpdateRoomStatus;
    AccessRight *QRCodeScanner;
    AccessRight *messages;
    AccessRight *postingLostAndFound;
    AccessRight *postingEngineering;
    AccessRight *postingLaundry;
    AccessRight *postingMinibar;
    AccessRight *postingLinen;
    AccessRight *postingAmenities;
    AccessRight *postingPhysicalCheck;
    AccessRight *settings;
    AccessRight *additionalJob;
    AccessRight *findRoom;
    AccessRight *findAttendant;
    AccessRight *findInspection;
    
    CleaningStatusPopupView *cleaningStatusPopup;
    CleaningStatusConfirmPopupView *cleaningStatusConfirmPopup;
    
    NSDictionary *selectedRoomAssignment;
    
    //Button Of QR Code
    UIButton *qrScanButton;
    
    BOOL isUsingQRCode;
    
    AdditionalJobManagerV2 *addjobManager;
    
    BOOL isCenterTitle;
    UIView *coverPanicView;
    
    BOOL isShowingAlertForceLogout;
    
    NSInteger firstComboIndexSelected;
    NSInteger secondComboIndexSelected;
    NSInteger filterDetailId;
    
    NSMutableArray *listRoomStatusOptions;
    NSMutableArray *listRoomStatusTitles;
    
    NSMutableArray *listZoneOptions;
    NSMutableArray *listZoneTitles;
    
    NSMutableArray *listFloorOptions;
    NSMutableArray *listFloorTitles;
    
    NSMutableArray *listRoomTypeOptions;
    NSMutableArray *listRoomTypeTitles;
    
    NSMutableArray *listOtherActivityDetailDisplay;
    UINavigationController *pttNavigationController;
    BOOL openedPTT;
}
@property (nonatomic, strong) IBOutlet UIImageView *hotelLogo;
@property (nonatomic, strong) IBOutlet UILabel *nameUserLabel;
@property (nonatomic, strong) IBOutlet UILabel *buildingNameLabel;

@property (nonatomic, strong) IBOutlet NSLayoutConstraint *heightConstrainBtnComplete;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *heightDropboxView;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *topSpaceDropboxView;
@property (nonatomic, strong) IBOutlet UIButton *firstComboBox;
@property (nonatomic, strong) IBOutlet UIButton *secondComboBox;
@property (nonatomic, strong) IBOutlet UIView *dropboxView;

@property (nonatomic, strong) NSTimer *timerCheckingLoginUserInformation;
@property (nonatomic) int countPendingAddJobs;
@property (nonatomic, readwrite) NSInteger numberOfMustPostRecords;
@property (nonatomic, strong) WifiIConViewController * wifiViewController;
@property (nonatomic, strong) TopbarViewV2 * topBarView;
@property (nonatomic, strong) NSMutableArray *syncData;
@property (nonatomic, strong) RoomCompletedViewV2 *roomCompletedView;
@property (nonatomic, strong) LoginViewV2 *loginView;
@property (nonatomic, strong) NSArray * arrButTabbar;
@property (nonatomic, retain) NSMutableArray *roomAssignmentData;
@property (nonatomic, strong) UITableView *roomTableView;
@property (nonatomic, strong) UITableViewCell *tvCell;
@property (nonatomic, strong) UIButton *butViewCleanedRooms;
@property (nonatomic, strong) UILabel *labelHousekeeperName;
@property (nonatomic ,strong) UILabel *label_location_property_name;
@property (nonatomic, strong) NSString *currentSelectedView;
@property (nonatomic) Boolean isOpeningPopupNotification;
@property (nonatomic, readwrite) BOOL isLoginShowwing;
@property (nonatomic, strong) SlidingMenuBar *slidingMenuBar;
@property (nonatomic, strong) SlidingMenuBar *slidingLegend;

-(IBAction)butViewCleanedRoomsPressed:(id)sender;
-(void) drawUnderlinedLabel:(UILabel *)label inTableViewCell:(UITableViewCell *)cell;
-(void) syncMethod;
-(void) cancelSyncMethod;
-(void) butEditPressed:(id) sender;
-(void) syncButtonPressed:(id)sender;
-(void) saveButtonPressed:(id)sender;
-(void) announceNewMessage:(NSNotification *)notif;
-(void) saveHomeView;
-(void) reLoadDataSyncDone:(NSNotification *)notificate;

//Get messages and show pop up (if any) from WS
//And update as read message to WS
-(void) getMessagePopupFromWS;
-(void) messageReciever:(BOOL) isShowHUD callFrom:(int)callFrom;
-(void) loadUserListByCurrentUser;
-(void) updateUnreadMessage;

//update badge number in tabbar
-(void)updateBadgeNumber:(bool)isSendRequest; //whether allowing request count pending additional job or not

// Set badge number for button menu
- (void)setBadgeNumberTabbarWith:(enum TAG_TABBAR_BUTTON)tagButtonTabbar  number:(NSNumber*)numberOfMsg;

//Count unread message with user ID
-(int) countUnreadMessageByUserId:(int)userId;
-(void) vibrate;
-(void) loudTone;
-(void) vibe:(id)sender;
-(void) setCaptionsView;
-(void) getHotelInfoWSWithPercentView:(MBProgressHUD*)percentView;
-(void) loadRoomAssignmentData;

-(void) handleTabbarEventWithTagOfButton:(NSInteger) tagOfButton;

//MARK: For collecting user information scheduler
-(void) startSchedulingUserInformation;
-(void) stopSchedulingUserInformation;

//Log out current user
//This is navigate to Login form
-(void)logOutUser;

+(HomeViewV2 *) shareHomeView;
-(void) setSelectedButton:(NSInteger) selectedIndex;

-(void) updateRecordSyncWithNumber:(NSInteger) record;

-(NSString *) getTempLastModifiedRoomAssignment;
-(BOOL) isSyncing;

#pragma mark - === Enable loading data ===
#pragma mark
-(void) waitingLoadingData;
-(void) endWaitingLoadingData;

@end
