//
//  HomeView.m
//  eHouseKeeping
//
//  Created by tms on 5/6/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
//////////////////////////
#import "HomeViewV2.h"
#import "RoomCompletedViewV2.h"
#import "RoomInspectedViewV2.h"
#import "ehkDefines.h"
#import "ehkDefinesV2.h"
#import "UserManagerV2.h"
#import "RoomManagerV2.h"
#import "MBProgressHUD.h"
#import "SyncManagerV2.h"
#import "NetworkCheck.h"
#import "CheckMemory.h"
#import <AudioToolbox/AudioToolbox.h>
#import "SettingModelV2.h"
#import "SettingAdapterV2.h"
#import "FindMainMenuViewV2.h"
#import "MyNavigationBarV2.h"
#import "AdhocMessageMain.h"
#import "UnassignFloorViewV2.h"
#import "EngineeringCaseViewV2.h"
#import "RoomAssignmentInfoViewController.h"
#import "SettingViewV2.h"
#import "CommonVariable.h"
#import "AccessRightModel.h"
#import "CustomTabBar.h"
#import "MarqueeLabel.h"
#import "QRCodeReader.h"
#import "iToast.h"
#import "LogFileManager.h"
#import "DeviceManager.h"
#import "AdditionalJobListByFloorViewV2.h"
#import "AdditionalJobDetailsScreenV2.h"
#import "UIImage+Tint.h"
#import "SlidingMenuBar.h"
#import "eHouseKeepingAppDelegate.h"
#import "PanicManager.h"
#import "AlertTableViewViewController.h"
#import "PopupMessageItemModel.h"
#import "RoomBlocking.h"
#import "NSString+Common.h"
#import "OtherActivityViewController.h"
#import "CustomAlertViewV2.h"
#import "GuestListViewController.h"
#import "OtherActivityManager.h"
#import "OtherActivityCell.h"
#import "OtherActivityModel.h"
#import "STEncryptorDES.h"
#import "CommonLib.h"

#define titleBar            @"ROOM ASSIGNMENT"
#define kDefaultValueRaCleanTm @"1900-01-01 00:00:00"

#define sizeTitle                   22
#define sizeSubtitle                15
// hidden header when tap navigationBar
#define heightScale 45

#define tagUnderline        6
#define tagGuestTitle       7
#define tagRAInitial        8
#define tagCustomeBadgeSync 119
#define tagHomeScrollView   1001
#define tagAlertForceUserLogout     15
#define tagAlertLogOut              10
#define tagAlertNewMessage          11
#define tagAlertOneMessage          111
#define TagSynInCompletedDialogShowing 12
#define TagSynCompletedDialogShowing 13
#define tagTabbarBG 114

#define kRoomNo              @"RoomNo"
#define kRMStatus            @"RMStatus"
#define kCleaningStatus      @"CleaningStatus"
//#define kCleaningStatusID      @"CleaningStatusID"

#define kVIP                 @"VIP"
#define kGuestName           @"GuestName"
#define kTitleLogOut        @"TitleLogOut"
#define kMessageLogOut      @"MessageLogOut"
#define kYesLogOut          @"YesLogOut"
#define kNoLogOut           @"NoLogOut"
#define kTitleCheckList     @"TitleCheckList"
#define kOKCheckList        @"OKCheckList"
#define kRoomAssignmentID   @"RoomAssignmentID"
#define kRaPrioritySortOrder @"RaPrioritySortOrder"

#define fontNavigationbar @"Arial-BoldMT"
#define tagHudRemoveLoginView 1245

@interface OtherActivityDetailsDisplay : NSObject
@property (nonatomic, retain) OtherActivityAssignmentModel *otherActivityDetail;
@property (nonatomic, retain) NSString *otherActivityString;
@end

@implementation OtherActivityDetailsDisplay
@synthesize otherActivityDetail, otherActivityString;
@end

@interface HomeViewV2 (PrivateMethods)
-(void) butJobPressed;
-(NSArray *) getNameOfTabBut;
- (void) showPopupNewMessage: (NSNotification *) notif;
-(void) syncHomeView;
-(void) setCaptionsTabbar;
-(void) updateSyncPeriodTime:(NSNotification *) notif;
-(void) saveData;
-(BOOL) allowHomeButtonPress;
-(void)getRoomStatusesWSWithPercentView:(MBProgressHUD*)percentView;
-(void)getCleaningStatusesWSWithPercentView:(MBProgressHUD*)percentView;
-(void) updateRoomAssignmentPriorities;
-(void) performSync;
-(void) changeCaptionsTabbar;
-(BOOL) checkSaveDataWithTagEvent:(NSInteger) tagOfEvent;
-(void) getMessageFromServer;
-(void) disableButtonInTabbar:(UIButton *) button;
-(void) showMessageIsGetting;
-(void) hideMessageIsGetting;
-(void) showIsSyncing;
-(void) hideIsSyncing;
-(void) getMessageSubjectWithPercentView:(MBProgressHUD*)percentView;

@end

@implementation HomeViewV2

NSString *LOADING_DATA = @"Loading Data",
*LOADING_GET_ROOM_STATUS = @"Get Room Status",
*LOADING_GET_CLEANING_STATUS = @"Get Cleaning Status";

@synthesize hotelLogo;
@synthesize roomTableView;
@synthesize roomAssignmentData;
@synthesize tvCell;
@synthesize butViewCleanedRooms;
@synthesize labelHousekeeperName ; // tabBarScrollView=_tabBarScrollView;
@synthesize arrButTabbar=_arrButTabbar;
@synthesize loginView, roomCompletedView, label_location_property_name, currentSelectedView, isOpeningPopupNotification, isLoginShowwing, syncData;//currentSelected
@synthesize buildingNameLabel;
@synthesize nameUserLabel;
@synthesize topBarView;
@synthesize wifiViewController;
@synthesize numberOfMustPostRecords;
@synthesize countPendingAddJobs;
@synthesize slidingMenuBar;
@synthesize timerCheckingLoginUserInformation;

#pragma mark - === Singleton Home View
#pragma mark
static HomeViewV2 *me = nil;

+(HomeViewV2 *)shareHomeView {
    return me;
}

#pragma mark - Save Home View - RoomAssignment
-(void) saveData {

}

-(void) hiddenHUDAfterSaved:(MBProgressHUD *)HUD{
    [HUD hide:YES];
    [HUD removeFromSuperview];

}

-(void)saveHomeView {
    if ([self.roomTableView isEditing] == YES) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[[LanguageManagerV2 sharedLanguageManager] getMsgCanNotSaveWhenEdit] message:nil delegate:nil cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getOK] otherButtonTitles:nil];
        [alert show];

        return;
    }
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getMsgSaveMessage]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES];

    RoomAssignmentModelV2 *raModel = [[RoomAssignmentModelV2 alloc] init];

    for (NSDictionary *raDic in roomAssignmentData) {
        raModel.roomAssignment_Id = (int)[[raDic objectForKey:kRoomAssignmentID] integerValue];
        raModel.roomAssignment_UserId = [UserManagerV2 sharedUserManager].currentUser.userId;
        [[RoomManagerV2 sharedRoomManager] load:raModel];

        raModel.roomAssignment_Priority_Sort_Order = [[raDic objectForKey:kRaPrioritySortOrder] integerValue];
        [[RoomManagerV2 sharedRoomManager] update:raModel];
    }

    //hidden after 0.5s
    [self performSelector:@selector(hiddenHUDAfterSaved:) withObject:HUD afterDelay:0.0f];

    //reload roomAssignment Data
    //RoomAssignmentEngine *roomAssignmentEngine = [[RoomAssignmentEngine alloc] init];
    //SuperRoomModel *modelSuper =
    if([UserManagerV2 isSupervisor]){
        if ([[UserManagerV2 sharedUserManager].currentUserAccessRight isEnabledSupHomeFilter]) {
            [[RoomManagerV2 sharedRoomManager] getRoomInspectionByFilterType:(int)firstComboIndexSelected filterDetailId:(int)filterDetailId];
        } else {
            [[RoomManagerV2 sharedRoomManager] getRoomInspection];
        }
    } else{
        [[RoomManagerV2 sharedRoomManager] getRoomAssignments];
    }


    //self.roomAssignmentData = [[RoomManagerV2 sharedRoomManager] roomAssignmentList];
    [self getOtherActivityAssignment];
    [self loadRoomAssignmentData];
    //self.roomAssignmentData = modelSuper.roomAssignmentList;

    [self.roomTableView reloadData];
}

#pragma mark -
#pragma mark PrivateMethods
//Remove Button Tabbar For PRD 2.2 Release 2
/*
 -(NSArray *)getNameImageTabButton {
 //add history
 NSMutableArray *arrName = [[NSMutableArray alloc] initWithObjects:@"HOME", @"SYNC NOW", @"POSTING", @"FIND",@"UNASSIGN",@"SETTING", @"LOGOUT", @"HISTORY",@"MESSAGE", @"JOB", nil] ;
 return arrName;
 }

 -(NSArray *)getNameOfTabBut{
 NSDictionary *dic = [[LanguageManagerV2 sharedLanguageManager] getCurrentDictionaryLanguage];
 NSString *home,* find, * unassign, *checklist, * messageTitle, * save, * syncnow, * setting, * logout, * history, *job;
 home = [dic valueForKey:[NSString stringWithFormat:@"%@", L_BTN_HOME]];
 checklist = [dic valueForKey:[NSString stringWithFormat:@"%@", L_BTN_CHECK_LIST]];
 messageTitle = [dic valueForKey:[NSString stringWithFormat:@"%@", L_BTN_MESSAGE]];
 save = [dic valueForKey:[NSString stringWithFormat:@"%@", L_BTN_SAVE]];
 syncnow = [dic valueForKey:[NSString stringWithFormat:@"%@", L_BTN_SYNC_NOW]];
 setting = [dic valueForKey:[NSString stringWithFormat:@"%@",L_BTN_SETTING]];
 logout = [dic valueForKey:[NSString stringWithFormat:@"%@", L_BTN_LOGOUT]];
 find = [dic valueForKey:[NSString stringWithFormat:@"%@", L_BTN_FIND]];
 unassign = [dic valueForKey:[NSString stringWithFormat:@"%@", L_BTN_UNASSIGN]];
 //Update
 history = [dic valueForKey:[NSString stringWithFormat:@"%@",L_HISTORY]];

 if (home == nil) {
 home = @"HOME";
 }
 if (checklist == nil) {
 checklist = @"CHECK LIST";
 }
 if (messageTitle == nil) {
 messageTitle = @"MESSAGE";
 }
 if (save == nil) {
 save = @"SAVE";
 }
 if (setting == nil) {
 setting = @"SETTING";
 }
 if (syncnow == nil) {
 syncnow = @"SYNCH";
 }
 if (logout == nil) {
 logout = @"LOGOUT";
 }
 if (find == nil) {
 find = @"FIND";
 }
 if (unassign == nil) {
 unassign = @"UNASSIGN";
 }
 //Update by Nhat
 if (history == nil) {
 history = @"HISTORY";
 }

 if(job == nil) {
 job = @"JOB";
 }

 NSMutableArray *arrName = [[NSMutableArray alloc] initWithObjects:home,syncnow, save ,checklist, messageTitle,find,unassign ,setting, logout,history, job, nil] ;
 return arrName;
 }
 */

-(void)setSelectedButton:(NSInteger)selectedIndex{
    //set selectedButton
    slidingMenuBar.currentSelected = (int)selectedIndex;
    [slidingMenuBar highlightSelectedTag];

    //Remove Button Tabbar For PRD 2.2 Release 2
    /*
     for (UIView * btn in tabBarScrollView.subviews) {
     if([btn isKindOfClass:[UIButton class]])
     {
     UIButton *but = (UIButton *)btn;

     UIImageView *imgView = (UIImageView *)[but viewWithTag:1000];
     if (imgView != nil) {
     [imgView removeFromSuperview];
     }

     if(SYSTEM_VERSION_LESS_THAN(@"7.0")){

     //Remove for similiar to color in android, color button will not change in selected
     //[but setTitleColor:[UIColor colorWithRed:4.0f/255.0 green:62.0f/255.0 blue:48.0f/255.0 alpha:1.0] forState:UIControlStateNormal];

     if ([but tag] == selectedIndex) {
     UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imgActive]];
     [imgView setAlpha:0.8f];
     CGRect frame = imgView.frame;
     frame.size.width = widthTabBut - 6;
     frame.size.height = heightTabBut - 2;
     frame.origin.x = (btn.frame.size.width - frame.size.width)/2.0;
     frame.origin.y = (btn.frame.size.height - frame.size.height)/2.0;

     imgView.frame = frame;
     imgView.tag = 1000;

     //[but addSubview:imgView];

     [but insertSubview:imgView atIndex:1];

     //Remove for similiar to color in android, color button will not change in selected
     //but setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];

     }
     } else {
     if ([but tag] == selectedIndex) {
     UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imgActiveFlat]];
     [imgView setAlpha:0.8f];
     CGRect frame = imgView.frame;
     frame.size.width = widthTabBut - 6;
     frame.size.height = heightTabBut - 2;
     frame.origin.x = (btn.frame.size.width - frame.size.width)/2.0;
     frame.origin.y = (btn.frame.size.height - frame.size.height)/2.0;

     imgView.frame = frame;
     imgView.tag = 1000;
     [but insertSubview:imgView atIndex:1];

     }
     }
     }
     }
     */
}

#pragma mark - Animation Selected Tabbar
//Remove Button Tabbar For PRD 2.2 Release 2
/*
 -(void)setTabBarSelectedIndex:(int)indexTabbar willAnimate:(BOOL)animateValue
 {
 animateValue = NO;
 //hard code animate
 if(animateValue)
 {
 UIView * fromView = self.tabBarController.selectedViewController.view;
 UIView * toView = [[self.tabBarController.viewControllers objectAtIndex:indexTabbar] view];
 [UIView transitionFromView:fromView
 toView:toView
 duration:1
 options:(indexTabbar > self.tabBarController.selectedIndex ? UIViewAnimationOptionTransitionFlipFromLeft : UIViewAnimationOptionTransitionFlipFromRight)
 completion:^(BOOL finished) {
 if (finished) {
 self.tabBarController.selectedIndex = indexTabbar;
 }
 }];
 }
 else
 {
 self.tabBarController.selectedIndex = indexTabbar;
 }
 }
 */

#pragma mark - Badge number

-(void)updateBadgeNumber:(bool)isSendRequest
{
    //Set badge number of Home
    [self setBadgeNumberTabbarWith:tagOfHomeButton number:[NSNumber numberWithInteger:[roomAssignmentData count]]];
    int countPendingJobs = 0;
    if(isDemoMode){
        //Set badge number of Additional Job
        NSMutableArray *listAdditionalJob = [[AdditionalJobManagerV2 sharedAddionalJobManager] loadAllAddJobFloorsByUserId:[UserManagerV2 sharedUserManager].currentUser.userId searchId:0];
        for (int i = 0; i < [listAdditionalJob count]; i ++) {
            AddJobFloorModelV2 *curFloor = [listAdditionalJob objectAtIndex:i];
            countPendingJobs += curFloor.addjob_floor_number_of_job;
        }
        //Only get count additional job number success
        if(countPendingJobs >= 0) {countPendingAddJobs = countPendingJobs;}
    }
    else{
        if(isSendRequest){
            countPendingJobs = [[AdditionalJobManagerV2 sharedAddionalJobManager] loadWSAddJobCountPendingByUserId:[UserManagerV2 sharedUserManager].currentUser.userId hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId];
            //Only get count additional job number success
            if(countPendingJobs >= 0) {countPendingAddJobs = countPendingJobs;}
        }
    }

    //Set badge number of Additional Job
    [self setBadgeNumberTabbarWith:tagOfAdditionalJob number:[NSNumber numberWithInt:countPendingAddJobs]];
}

// Set badge number for button menu
- (void)setBadgeNumberTabbarWith:(TAG_TABBAR_BUTTON)tagButtonTabbar  number:(NSNumber*)numberOfMsg {
    //UIButton *button = (UIButton *)[tabBarScrollView viewWithTag:tagButtonTabbar];
    UIButton *button = (UIButton*)[slidingMenuBar buttonWithTag:tagButtonTabbar];
    for (UIView *subView in button.subviews) {
        if ([subView isKindOfClass:[CustomBadge class] ]) {
            [subView removeFromSuperview];
        }
    }
    if ([numberOfMsg intValue] > 0) {

        int moreWidthForNumber = 0;
        int moveLeft = 0;
        NSMutableString *numberString = [[NSMutableString alloc] initWithString:@""];

        if([numberOfMsg intValue] < 10){
            [numberString appendString:[numberOfMsg stringValue]];
        } else if([numberOfMsg intValue] >= 10 && [numberOfMsg intValue] <= 99){
            [numberString appendString:[numberOfMsg stringValue]];
            moreWidthForNumber += 3;
            moveLeft = 4;
        } else if([numberOfMsg intValue] >= 100 && [numberOfMsg intValue] <= 999){
            [numberString appendString:[numberOfMsg stringValue]];
            moreWidthForNumber += 6;
            moveLeft = 7;
        } else { // >= 1000
            [numberString appendString:@"999+"];
            moreWidthForNumber += 10;
            moveLeft = 10;
        }

        UIColor *badgeFrameColor = nil;
        BOOL isShining = NO;
        if(SYSTEM_VERSION_LESS_THAN(@"7.0")){
            badgeFrameColor = [UIColor whiteColor];
            isShining = YES;
        } else {
            badgeFrameColor = [UIColor clearColor];
        }

        CustomBadge *customBadge1 = [CustomBadge customBadgeWithString:numberString
                                                       withStringColor:[UIColor whiteColor]
                                                        withInsetColor:[UIColor redColor]
                                                        withBadgeFrame:YES
                                                   withBadgeFrameColor:badgeFrameColor
                                                             withScale:1.0
                                                           withShining:isShining];
        //Rectangle for badge
        //customBadge1.badgeCornerRoundness = 0.1f;

        [customBadge1 setFrame:CGRectMake(53 - moveLeft, 0, 27 + moreWidthForNumber, 25)];
        [button addSubview:customBadge1];
    }
}

#pragma mark - InitScrollViewTabBar Methods
-(void)handleTabbarEventWithTagOfButton:(NSInteger)tagOfButton {
    //UIButton *butLogOut = (UIButton *)[[self.tabBarController tabBar] viewWithTag:tagOfLogOutButton];
    //UIButton *sender = (UIButton *)[[self.tabBarController tabBar] viewWithTag:tagOfButton];

    switch (tagOfButton) {
        case tagOfHomeButton: // HOME
            //show navigationBar
        {
            if ([[self navigationController] isToolbarHidden]) {
                [[self navigationController] setNavigationBarHidden:NO animated:NO];
            }

            //int homeIndex = 0;

            //Check to animation 1 view, If tabbar selected animated, popRootView will not animated and vice versa
            //BOOL willAnimateView = (self.tabBarController.selectedIndex != homeIndex);
            //[self setTabBarSelectedIndex:homeIndex willAnimate:willAnimateView];
            [self.navigationController popToRootViewControllerAnimated:YES];

            //reset roomNo
            [RoomManagerV2 resetCurrentRoomNo];
            [TasksManagerV2  resetCurrentRoomAssignment];

            //enable butLogOut
            //[butLogOut setEnabled:YES];

            //[self.tabBarController setSelectedIndex:0];
            [self setSelectedButton:tagOfHomeButton];
        }
            break;
        case tagOfSyncNowButton: // SYNC NOW
        {
            UIButton *syncButton = [slidingMenuBar buttonWithTag:tagOfSyncNowButton];
            [self syncButtonPressed:syncButton];
        }
            break;
        case tagOfPostingButton: // POSTING
        {

            //            /**********************
            //             //
            //             // new save function
            //             //
            //             **********************/
            //            UIViewController *viewcontroller = ((UINavigationController *)self.tabBarController.selectedViewController).topViewController;
            //            if ([viewcontroller respondsToSelector:@selector(saveData)]) {
            //                [(id)viewcontroller saveData];
            //            }
            //
            //            [self saveButtonPressed:sender];

            if (slidingMenuBar.currentSelected == tagOfPostingButton) {
                break;
            }

            //int homeIndex = 0;
            //[self setTabBarSelectedIndex:homeIndex willAnimate:(self.tabBarController.selectedIndex != homeIndex)];
            [self setSelectedButton:tagOfPostingButton];

            //check if viewControllers contain FindMainMenuViewV2
            BOOL isContainPosting = NO;
            [self.tabBarController setSelectedIndex:0];
            NSArray *viewControllers = [self.navigationController viewControllers];
            for (UIViewController *viewController in viewControllers) {
                if ([viewController isKindOfClass:[CountViewV2 class]]) {
                    CountViewV2 *currentCountView = (CountViewV2*)viewController;
                    //if not come from History
                    if(!currentCountView.isFromHistory)
                    {
                        [self.navigationController popToViewController:viewController animated:YES];
                        isContainPosting = YES;
                        break;
                    }
                }
            }

            if (isContainPosting == YES) {
                break;
            }


            CountViewV2 *countView = [[CountViewV2 alloc] initWithNibName:@"CountViewV2" bundle:nil];
            countView.isFromMainPosting = YES;
            [self.navigationController pushViewController:countView animated:YES];

        }
            break;

            //        case 4: // CHECK LIST
            //        {
            //            if (self.currentSelected == tagOfCheckListButton) {
            //                break;
            //            }
            //            [self.tabBarController setSelectedIndex:0];
            //
            //            //------------------------------//
            //            [self setSelectedButton:[sender tag]];
            //
            //            //disable butLogOut
            //            [butLogOut setEnabled:NO];
            //
            //            //perform function to add check list view
            //            UIViewController *topViewController = self.navigationController.topViewController;
            //            [topViewController performSelector:@selector(showCheckList)];
            ////            //push CheckListView to navigationController
            ////            CheckListViewV2 *checklistView = [[CheckListViewV2 alloc] initWithNibName:NSStringFromClass([CheckListViewV2 class]) bundle:nil];
            ////            [self.navigationController pushViewController:checklistView animated:YES];
            //
            //        }
            //            break;

            //        case 4: // MESSAGE
            //        {
            //            [CommonVariable sharedCommonVariable].isFromHome = 0;
            ////            if (((self.currentSelected == tagOfHomeButton) || (currentSelected == tagOfCheckListButton)) && ![CommonVariable sharedCommonVariable].roomIsCompleted) {
            ////                [CommonVariable sharedCommonVariable].isFromHome = 1;
            ////            }
            //            if ((self.currentSelected == tagOfHomeButton) && ![CommonVariable sharedCommonVariable].roomIsCompleted) {
            //                [CommonVariable sharedCommonVariable].isFromHome = 1;
            //            }
            //            [self butMessagePressed:sender];
            //
            //            //disable butLogOut
            //            [butLogOut setEnabled:NO];
            //        }
            //            break;
        case tagOfFindButton: //FIND
        {
            //check user is supervisor
            //            if (![UserManagerV2 isSupervisor]) {
            //                UIAlertView *alertCheckList = [[UIAlertView alloc] initWithTitle:[[LanguageManagerV2 sharedLanguageManager] getMsgNotSupervisor] message:@"" delegate:nil cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getOK] otherButtonTitles:nil];
            //                [alertCheckList show];
            //                break;
            //            }

            if (slidingMenuBar.currentSelected == tagOfFindButton) {
                break;
            }

            //int homeIndex = 0;
            //[self setTabBarSelectedIndex:homeIndex willAnimate:(self.tabBarController.selectedIndex != homeIndex)];
            [self setSelectedButton:tagOfFindButton];

            //check if viewControllers contain FindMainMenuViewV2
            BOOL isContainFind = NO;
            [self.tabBarController setSelectedIndex:0];
            NSArray *viewControllers = [self.navigationController viewControllers];
            for (UIViewController *viewController in viewControllers) {
                if ([viewController isKindOfClass:[FindMainMenuViewV2 class]]) {
                    [self.navigationController popToViewController:viewController animated:YES];
                    isContainFind = YES;
                    break;
                }
            }

            if (isContainFind == YES) {
                break;
            }

            //push FindMainMenuViewV2 to navigationController
            FindMainMenuViewV2 *findMainMenuView = [[FindMainMenuViewV2 alloc] initWithNibName:NSStringFromClass([FindMainMenuViewV2 class]) bundle:nil];

            [self.navigationController pushViewController:findMainMenuView animated:YES];
            [findMainMenuView setTitle:[[LanguageManagerV2 sharedLanguageManager] getFind]];
        }
            break;
        case tagOfUnassignButton:// UNASSIGN
        {
            //int homeIndex = 0;
            //[self setTabBarSelectedIndex:homeIndex willAnimate:(self.tabBarController.selectedIndex != homeIndex)];
            [self setSelectedButton:tagOfUnassignButton];

            //check if viewControllers contain FindMainMenuViewV2
            BOOL isContainUnassign = NO;
            NSArray *viewControllers = [self.navigationController viewControllers];
            for (UIViewController *viewController in viewControllers) {
                if ([viewController isKindOfClass:[UnassignFloorViewV2 class]]) {
                    [self.navigationController popToViewController:viewController animated:YES];
                    isContainUnassign = YES;
                    break;
                }
            }

            if (isContainUnassign == YES) {
                break;
            }

            UnassignFloorViewV2 *unAssignFloorView = [[UnassignFloorViewV2 alloc] initWithNibName:@"UnassignFloorViewV2" bundle:nil];
            //         [self.navigationItem.backBarButtonItem setTitle:[[LanguageManagerV2 sharedLanguageManager]  getStringLanguageByStringId:L_BACK]];

            [self.navigationController pushViewController:unAssignFloorView animated:YES];

        }
            break;

        case tagOfPTT:{
            [self clickPTT:nil];
            break;
        }
        case tagOfSettingButton: // SETTING
        {

            //Replace for push setting view instead of showing tabbar with index
            /*
             [self setSelectedButton:[sender tag]];
             int settingTabbarIndex = 2;
             [self setTabBarSelectedIndex:settingTabbarIndex willAnimate:(self.tabBarController.selectedIndex != settingTabbarIndex)];
             */

            [self setSelectedButton:tagOfSettingButton];

            //check if viewControllers contain FindMainMenuViewV2
            BOOL isContainSetting = NO;
            NSArray *viewControllers = [self.navigationController viewControllers];
            for (UIViewController *viewController in viewControllers) {
                if ([viewController isKindOfClass:[SettingViewV2 class]]) {
                    [self.navigationController popToViewController:viewController animated:YES];
                    isContainSetting = YES;
                    break;
                }
            }

            if (isContainSetting == YES) {
                break;
            }

            SettingViewV2 *settingView = [[SettingViewV2 alloc] initWithNibName:@"SettingViewV2" bundle:nil];
            [self.navigationController pushViewController:settingView animated:YES];
            break;
        }
        case tagOfLogOutButton: //LOGOUT
        {
            //            [self.tabBarController setSelectedIndex:0];
            //            [self setSelectedButton:sender.tag];
            //
            //            CountViewV2 *countView = [[CountViewV2 alloc] initWithNibName:@"CountViewV2" bundle:nil];
            //            countView.isFromMainPosting = YES;
            //            [self.navigationController pushViewController:countView animated:YES];
            //            break;

            //            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[[LanguageManagerV2 sharedLanguageManager] getLogout] message:[[LanguageManagerV2 sharedLanguageManager] getMsgLogout] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getNo] otherButtonTitles:[[LanguageManagerV2 sharedLanguageManager] getYes], nil];
            //            [alert setTag:tagAlertLogOut];
            //            [alert show];

            UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:imgYesBtn], [UIImage imageNamed:imgNoBtn], nil] title:[[LanguageManagerV2 sharedLanguageManager] getLogout] message:[[LanguageManagerV2 sharedLanguageManager] getMsgLogout] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles:[[LanguageManagerV2 sharedLanguageManager] getNo], nil];
            alert.tag = tagAlertLogOut;
            [alert show];
            break;
        }

            //        case 8: // LOGOUT
            //        {
            ////            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[[LanguageManagerV2 sharedLanguageManager] getLogout] message:[[LanguageManagerV2 sharedLanguageManager] getMsgLogout] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getNo] otherButtonTitles:[[LanguageManagerV2 sharedLanguageManager] getYes], nil];
            ////            [alert setTag:tagAlertLogOut];
            ////            [alert show];
            ////            break;
            //
            //
            //        }
            //            break;
            //Update History
        case tagOfHistoryButton: //HISTORY
        {
            //int homeIndex = 0;
            //[self setTabBarSelectedIndex:homeIndex willAnimate:(self.tabBarController.selectedIndex != homeIndex)];
            [self setSelectedButton:tagOfLogOutButton];

            if (slidingMenuBar.currentSelected == tagOfPostingButton) {
                break;
            }

            //check if viewControllers contain FindMainMenuViewV2
            BOOL isContainHistory = NO;
            [self.tabBarController setSelectedIndex:0];
            NSArray *viewControllers = [self.navigationController viewControllers];
            for (UIViewController *viewController in viewControllers) {
                if ([viewController isKindOfClass:[CountViewV2 class]]) {
                    CountViewV2 *currentCountView = (CountViewV2*)viewController;
                    if(currentCountView.isFromHistory)
                    {
                        [self.navigationController popToViewController:viewController animated:YES];
                        isContainHistory = YES;
                        break;
                    }
                }
            }

            if (isContainHistory == YES) {
                break;
            }

            CountViewV2 *countViewHistory = [[CountViewV2 alloc] initWithNibName:@"CountViewV2" bundle:nil];
            countViewHistory.isFromHistory = YES;
            countViewHistory.isFromMainPosting = YES;
            //            NSLog(@"%d",sender.tag);
            [self.navigationController pushViewController:countViewHistory animated:YES];
            break;
        }
        case tagOfMessageButton: // MESSAGE
        {
            //int homeIndex = 0;
            //[self setTabBarSelectedIndex:homeIndex willAnimate:(self.tabBarController.selectedIndex != homeIndex)];

            //[self setSelectedButton:tagOfMessageButton];
            [self setSelectedButton:tagOfMessageButton];

            //check if viewControllers contain AdhocMessageMain
            BOOL isContainMessageView = NO;
            [self.tabBarController setSelectedIndex:0];
            NSArray *viewControllers = [self.navigationController viewControllers];
            for (UIViewController *viewController in viewControllers) {
                if ([viewController isKindOfClass:[AdhocMessageMain class]]) {
                    [self.navigationController popToViewController:viewController animated:YES];
                    isContainMessageView = YES;
                    break;
                }
            }

            if (isContainMessageView == YES) {
                break;
            }

            //Create View main message
            if (messages.isALlowedEdit) {
                AdhocMessageMain *mainMessage = [[AdhocMessageMain alloc] initWithNibName:@"AdhocMessageMain" bundle:nil];
                int countUnreadMessage = [[HomeViewV2 shareHomeView] countUnreadMessageByUserId:[[UserManagerV2 sharedUserManager] currentUser].userId];
                if(countUnreadMessage > 0)
                    [self.navigationController pushViewController:mainMessage animated:NO];
                else{
                    [self.navigationController pushViewController:mainMessage animated:YES];
                }
                break;
            } else {
                AdHocMessageBoxV2 *messageBox = [[AdHocMessageBoxV2 alloc] initWithNibName:@"AdHocMessageBoxV2" bundle:nil];
                messageBox.isInBox = YES;
                messageBox.isOutBox = NO;
                [self.navigationController pushViewController:messageBox animated:YES];
                break;
            }
            break;
        }

        case tagOfAdditionalJob: //Additional Job
        {
            if (slidingMenuBar.currentSelected == tagOfAdditionalJob) {
                break;
            }

            //int homeIndex = 0;
            //[self setTabBarSelectedIndex:homeIndex willAnimate:(self.tabBarController.selectedIndex != homeIndex)];
            [self setSelectedButton:tagOfAdditionalJob];

            //check if viewControllers contain AdditionalJobListByFloorViewV2
            BOOL isJobByFloorView = NO;
            NSArray *viewControllers = [self.navigationController viewControllers];
            for (UIViewController *viewController in viewControllers) {
                if ([viewController isKindOfClass:[AdditionalJobListByFloorViewV2 class]]) {
                    [self.navigationController popToViewController:viewController animated:YES];
                    isJobByFloorView = YES;
                    break;
                }
            }

            if (isJobByFloorView == YES) {
                break;
            }

            //Create View main message
            AdditionalJobListByFloorViewV2 *jobByFloorView = [[AdditionalJobListByFloorViewV2 alloc] initWithNibName: @"AdditionalJobListByFloorViewV2" bundle:nil];
            [self.navigationController pushViewController:jobByFloorView animated:YES];
        }
            break;

        case tagOfPanicButton:
        {
            if([PanicManager isRunningPanic]){
                [self.slidingMenuBar setAlwaysHoldPanicButton:NO];
                [coverPanicView removeFromSuperview];
                [[PanicManager sharedPanicManager] stopPanic];
            } else {
                [self.slidingMenuBar setAlwaysHoldPanicButton:YES];
                [self addCoverPanicView];
                [[PanicManager sharedPanicManager] panicNowWithTabbarController:self.tabBarController];
            }
        }
            break;
        case tagOfGuestProfileButton:
        {
            [self setSelectedButton:tagOfGuestProfileButton];
            GuestListViewController *guestListVC = [[GuestListViewController alloc] initWithNibName:@"GuestListViewController" bundle:nil];
            [self.navigationController pushViewController:guestListVC animated:YES];
        }
            break;
        default:
            break;
    }

    //reset tag of event
    [[NSUserDefaults standardUserDefaults] setInteger:-1 forKey:TAG_OF_EVENT];
}

-(void) butTabBarPressed:(id)sender{
    NSInteger selectedIndex = [sender tag];

    // Sync
    if ([sender tag] == tagOfSyncNowButton) {
        [self syncButtonPressed:sender];
        return;
    }
    if (selectedIndex == slidingMenuBar.currentSelected) {
        return;
    }

    //--------------------------------------------------------------------
    //Check to finish cleaning room before navigate to another view
    NSString* completedRoomMessage = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_complete_room_to_leave];
    if ([CommonVariable sharedCommonVariable].currentPauseRoom == noPauseRoom || [CommonVariable sharedCommonVariable].currentPauseRoom == [TasksManagerV2 getCurrentRoomAssignment]) {
        completedRoomMessage = [L_complete_room_to_leave currentKeyToLanguage];
    }

    bool isMustCompleteRoom = NO;
    NSArray *arrayVC = self.navigationController.viewControllers;
    for (UIViewController *currentVC in arrayVC) {
        if([currentVC isKindOfClass:[RoomAssignmentInfoViewController class]])
        {
            isMustCompleteRoom = YES;
            break;
        }
    }

    //Messag Menu Pressed
    if([sender tag] == tagOfMessageButton)
    {
        isMustCompleteRoom = NO;
    }

    if(!([CommonVariable sharedCommonVariable].roomIsCompleted == 1)
       && isMustCompleteRoom)
    {
        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:imgYesBtn], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:completedRoomMessage delegate:nil cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getOK] otherButtonTitles: nil];
        [alert show];

        return;
    }
    //End check
    //--------------------------------------------------------------------

    //--------------------------------------------------------------------
    //Check to finish Additional Job Detail before navigate to another view
    bool isMustCompleteAddJobDetail = NO;
    for (UIViewController *currentVC in arrayVC) {
        if([currentVC isKindOfClass:[AdditionalJobDetailsScreenV2 class]])
        {
            AdditionalJobDetailsScreenV2 *addJobDetailVC = (AdditionalJobDetailsScreenV2*)currentVC;
            if(addJobDetailVC.detailSelected.addjob_detail_status == AddJobStatus_Started){
                isMustCompleteAddJobDetail = YES;
            }
            break;
        }
    }

    //Messag Menu Pressed
    if([sender tag] == tagOfMessageButton)
    {
        isMustCompleteAddJobDetail = NO;
    }

    if(isMustCompleteAddJobDetail)
    {
        //Re-use message complete of room detail
        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:imgYesBtn], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:completedRoomMessage delegate:nil cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getOK] otherButtonTitles: nil];
        [alert show];

        return;
    }
    //End check
    //--------------------------------------------------------------------

    //--------------------------------------------------------------------
    //Check to finish Other Activity before navigate to another view
    bool isMustCompleteOtherActivity = NO;
    for (UIViewController *currentVC in arrayVC) {
        if ([currentVC isKindOfClass:[OtherActivityViewController class]]) {
            OtherActivityViewController *otherActivityVC = (OtherActivityViewController*)currentVC;
            if (otherActivityVC.activityAssignmentDetail.oaa_status_id == OtherActivityStatus_Start) {
                isMustCompleteOtherActivity = YES;
            }
            break;
        }
    }

    //Message Menu Pressed
    if ([sender tag] == tagOfMessageButton) {
        isMustCompleteOtherActivity = NO;
    }

    if (isMustCompleteOtherActivity) {
        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:imgYesBtn], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:[L_complete_the_other_activity_before_leaving currentKeyToLanguage] delegate:nil cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getOK] otherButtonTitles: nil];
        [alert show];

        return;
    }
    //End check
    //--------------------------------------------------------------------

    // REMOVE FOR CHECK SAVE MESSAGE BECAUSE THIS ALREADY CHECKED ABOVE
    //    if (self.currentSelected != tagOfPostingButton) {
    //        UIViewController *viewcontroller = ((UINavigationController *)self.tabBarController.selectedViewController).topViewController;
    //
    //        if ([viewcontroller respondsToSelector:@selector(checkSaveDataWithTagEvent:)]) {
    //            if ([(id)viewcontroller checkSaveDataWithTagEvent:[sender tag]] == YES) {
    //                //need save data before leaving
    //
    //                //save tag of event
    //                [[NSUserDefaults standardUserDefaults] setInteger:[sender tag] forKey:TAG_OF_EVENT];
    //
    //            }
    //            else
    //            {
    //                //don't display alert when change from historyview to another view
    //                //                UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:completedRoomMessage delegate:nil cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getOK] otherButtonTitles: nil];
    //                //                [alert show];
    //                //                return;
    //            }
    //        }
    //    }

    //handle tabbar event in separate method
    [self handleTabbarEventWithTagOfButton:[sender tag]];

}

//Hao Tran - Remove for PRD 2.2 release 2
/*
 //count number tabbar for current User, apply for supervisor, RA
 -(int)countTabbarButtons
 {
 //Hardcode number of supervisor button
 int countTabButtons = numOfTabBut - numOfFixedTabBut;

 if(![UserManagerV2 isSupervisor])
 {
 countTabButtons = countTabButtons - 2;
 }

 //Check has button message
 int noHaveButtonMessage = 0;
 if(!(messages.isAllowedView || messages.isActive || messages.isALlowedEdit))
 {
 noHaveButtonMessage = 1;
 }
 countTabButtons -= noHaveButtonMessage;

 //Check has button setting
 int noHaveButtonSetting = 0;
 if(!(settings.isAllowedView || settings.isActive || settings.isALlowedEdit))
 {
 noHaveButtonSetting = 1;
 }
 countTabButtons -= noHaveButtonSetting;

 //Check has button Unassigned
 if(![[[UserManagerV2 sharedUserManager] currentUserAccessRight] hasUnassignRoom]){
 countTabButtons -= 1;
 }

 return countTabButtons;
 }
 - (void)scrollViewDidScroll:(UIScrollView *)scrollView
 {
 if(scrollView.tag == tagHomeScrollView)
 {
 //        NSLog(@"scrollViewDidScroll %d : %f", scrollView.tag, scrollView.contentOffset.x);
 UIImageView *imgRightView = (UIImageView *)[[self.tabBarController tabBar] viewWithTag:1002];
 imgRightView.hidden = YES;

 UIImageView *imgLeftView = (UIImageView *)[[self.tabBarController tabBar] viewWithTag:1003];
 imgLeftView.hidden = YES;

 if(scrollView.contentOffset.x > 0.0)
 {
 imgLeftView.frame = CGRectMake(scrollView.contentOffset.x - 1, 0, 5, heightTabBut);
 imgLeftView.hidden = NO;
 }

 if(scrollView.contentOffset.x < countTabbarButtons * widthTabBut)
 {
 imgRightView.frame = CGRectMake(scrollView.contentOffset.x + 315, 0, 5, heightTabBut);
 imgRightView.hidden = NO;
 }
 }
 }


 -(void)initScrollViewTabbar:(NSInteger) numberOfBtn{
 tabBarScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.tabBarController.view.frame.size.width, HEIGHT_TAB_BAR)];
 [tabBarScrollView setContentSize:CGSizeMake(numberOfBtn * widthTabBut, HEIGHT_TAB_BAR)];
 [tabBarScrollView setBackgroundColor:[UIColor whiteColor]];
 [tabBarScrollView setShowsVerticalScrollIndicator:NO];
 [tabBarScrollView setShowsHorizontalScrollIndicator:NO];
 tabBarScrollView.tag = tagHomeScrollView;
 tabBarScrollView.delegate = self;

 UIImage *img = [UIImage imageNamed:@"bottom.png"];
 CGSize newSizeImage = CGSizeMake(self.tabBarController.view.frame.size.width, HEIGHT_TAB_BAR);
 img = [self resizeImage:img newSize:newSizeImage];
 [tabBarScrollView setBackgroundColor:[UIColor colorWithPatternImage:img]];
 */
/*
 //Clear color
 [tabBarScrollView setBackgroundColor:[UIColor clearColor]];

 //init supervisor image
 UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:urlImgSupervisor]];
 [imgView setTag:numOfTabBut + 1];
 CGRect frame = imgView.frame;
 frame.size.width = 320;
 frame.size.height = 19;
 frame.origin.x = 0;
 frame.origin.y = -19;
 imgView.frame = frame;
 [[self.tabBarController tabBar] insertSubview:imgView aboveSubview:tabBarScrollView];
 [imgView setHidden:YES];

 //    UIImageView *imgViewLeftMenuIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"menuICON_left.png"]];
 //    UIImageView *imgViewLeftMenuIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:urlImgSupervisor]];
 //    imgViewLeftMenuIcon.frame = CGRectMake(0, -19, 22, 44);
 //    imgViewLeftMenuIcon.tag = numOfTabBut + 2;
 //    [[self.tabBarController tabBar] insertSubview:imgViewLeftMenuIcon aboveSubview:tabBarScrollView];
 //    [imgViewLeftMenuIcon setHidden:YES];

 }


 -(void)initScrollViewTabbarButtons:(NSInteger) numberOfBtn{
 [[self.tabBarController tabBar] insertSubview:tabBarScrollView aboveSubview:self.tabBarController.view];
 if (numberOfBtn == numOfTabBut){
 int moveButtonToLeft = 0;
 for (int temp = 0; temp < numberOfBtn; temp ++){

 UIButton *butTabBar = [UIButton buttonWithType:UIButtonTypeCustom];

 switch (temp) {
 case 0:
 {
 [butTabBar setTag:tagOfHomeButton];
 }
 break;
 case 1:
 {
 if(ENABLE_ADDITIONAL_JOB)
 {
 //Check access right for show button or not
 if(!(additionalJob.isActive||additionalJob.isALlowedEdit||additionalJob.isAllowedView))
 {
 //ignore add tabbar button addjob
 moveButtonToLeft += 1;
 continue;
 }
 }
 else
 {
 moveButtonToLeft += 1;
 continue;
 }
 [butTabBar setTag:tagOfAdditionalJob];
 }
 break;
 case 2:
 {
 [butTabBar setTag:tagOfPostingButton];

 }
 break;
 case 3:
 {
 [butTabBar setTag:tagOfHistoryButton];
 }
 break;

 case 4:
 {
 [butTabBar setTag:tagOfFindButton];
 }
 break;

 case 5:
 {
 if(![[[UserManagerV2 sharedUserManager] currentUserAccessRight] hasUnassignRoom]){
 moveButtonToLeft +=1;
 continue;
 }
 [butTabBar setTag:tagOfUnassignButton];
 }
 break;
 case 6:
 {

 //whether or not add Tab bar button MESSAGE
 if(!ENABLE_MESSAGE)
 {
 if(ENABLE_CHECK_ACCESS_RIGHT_MESSAGE)
 {
 //message is disable
 if(!(messages.isAllowedView || messages.isActive || messages.isALlowedEdit))
 {
 //ignore add tabbar button messsage
 moveButtonToLeft += 1;
 continue;
 }
 }
 else
 {
 moveButtonToLeft += 1;
 continue;
 }
 }
 [butTabBar setTag:tagOfMessageButton];

 }
 break;
 case 7:
 {
 [butTabBar setTag:tagOfSyncNowButton];

 }
 break;
 case 8:
 {
 if(settings.isAllowedView || settings.isActive || settings.isALlowedEdit){
 [butTabBar setTag:tagOfSettingButton];
 }
 else{
 moveButtonToLeft += 1;
 continue;
 }

 }
 break;

 case 9:
 {

 [butTabBar setTag:tagOfLogOutButton];

 }
 break;
 default:
 break;
 }

 numberOfTagButton++;

 [butTabBar setFrame:CGRectMake((temp - moveButtonToLeft) * widthTabBut, 0, widthTabBut, heightTabBut)];
 [butTabBar addTarget:self action:@selector(butTabBarPressed:) forControlEvents:UIControlEventTouchUpInside];
 [butTabBar setTitleEdgeInsets:UIEdgeInsetsMake(35, 0, 0, 0)];
 UIFont *buttabfont = [UIFont boldSystemFontOfSize:12];
 [butTabBar setTitleColor:[UIColor colorWithRed:4.0f/255.0 green:62.0f/255.0 blue:48.0f/255.0 alpha:1.0] forState:UIControlStateNormal];
 [butTabBar.titleLabel setFont:buttabfont];

 [tabBarScrollView addSubview:butTabBar];
 }

 //whether or not add Tab bar button MESSAGE
 if(!ENABLE_MESSAGE)
 {
 if(ENABLE_CHECK_ACCESS_RIGHT_MESSAGE)
 {
 //message is disable
 if(!(messages.isAllowedView || messages.isActive || messages.isALlowedEdit))
 {
 //decrease number tab button
 numberOfBtn -= 1;
 }
 }
 }

 //Button unassigned is disabled
 if(![[[UserManagerV2 sharedUserManager] currentUserAccessRight] hasUnassignRoom]){
 numberOfBtn -= 1;
 }

 //button setting is disable
 if(!(settings.isAllowedView || settings.isActive || settings.isALlowedEdit))
 {
 numberOfBtn -= 1;
 }

 if(!ENABLE_ADDITIONAL_JOB)
 {
 numberOfBtn -= 1;
 }
 else if(!(additionalJob.isActive||additionalJob.isALlowedEdit||additionalJob.isAllowedView))
 {
 numberOfBtn -= 1;
 }

 UIImageView *imgViewRightMenuIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imgMenuRight]];
 imgViewRightMenuIcon.frame = CGRectMake(315, 0, 5, heightTabBut);
 imgViewRightMenuIcon.tag = 1002;
 imgViewRightMenuIcon.hidden = YES;
 [tabBarScrollView addSubview:imgViewRightMenuIcon];

 UIImageView *imgViewLeftMenuIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imgMenuRight]];
 imgViewLeftMenuIcon.frame = CGRectMake(0, 0, 5, heightTabBut);
 imgViewLeftMenuIcon.tag = 1003;
 imgViewLeftMenuIcon.hidden = YES;
 [tabBarScrollView addSubview:imgViewLeftMenuIcon];


 if(numberOfBtn > 4)
 {
 imgViewRightMenuIcon.hidden = NO;
 }

 //        [tabBarScrollView setContentSize:CGSizeMake((numberOfBtn - 1) * widthTabBut, heightTabBut)];
 [tabBarScrollView setContentSize:CGSizeMake(numberOfBtn * widthTabBut, heightTabBut)];

 }
 else{
 int moveButtonToLeft = 0;

 for (int temp = 0; temp < numberOfBtn; temp ++) {

 UIButton *butTabBar = [UIButton buttonWithType:UIButtonTypeCustom];
 //Edit-----------------------
 switch (temp) {
 case 0:
 {
 [butTabBar setTag:tagOfHomeButton];

 }
 break;
 case 1:
 {
 if(ENABLE_ADDITIONAL_JOB)
 {
 //Check access right for show button or not
 if(!(additionalJob.isActive||additionalJob.isALlowedEdit||additionalJob.isAllowedView))
 {
 //ignore add tabbar button addjob
 moveButtonToLeft += 1;
 continue;
 }
 }
 else
 {
 moveButtonToLeft += 1;
 continue;
 }

 [butTabBar setTag:tagOfAdditionalJob];
 }
 break;
 case 2:
 {
 [butTabBar setTag:tagOfPostingButton];

 }
 break;
 case 3:
 {
 [butTabBar setTag:tagOfHistoryButton];

 }
 break;
 case 4:
 {
 //whether or not add Tab bar button MESSAGE
 if(!ENABLE_MESSAGE)
 {
 if(ENABLE_CHECK_ACCESS_RIGHT_MESSAGE)
 {
 //message is disable
 if(!(messages.isAllowedView || messages.isActive || messages.isALlowedEdit))
 {
 //ignore add tabbar button messsage
 moveButtonToLeft += 1;
 continue;
 }
 }
 else
 {
 moveButtonToLeft += 1;
 continue;
 }
 }
 [butTabBar setTag:tagOfMessageButton];

 }
 break;
 case 5:
 {

 [butTabBar setTag:tagOfSyncNowButton];

 }
 break;

 case 6:
 {
 if(settings.isAllowedView || settings.isActive || settings.isALlowedEdit){
 [butTabBar setTag:tagOfSettingButton];
 }
 else{
 moveButtonToLeft += 1;
 continue;
 }

 }
 break;
 case 7:
 {

 [butTabBar setTag:tagOfLogOutButton];

 }
 break;

 default:
 break;
 }


 numberOfTagButton++;

 [butTabBar setFrame:CGRectMake((temp - moveButtonToLeft) * widthTabBut, 0, widthTabBut, heightTabBut)];

 [butTabBar addTarget:self action:@selector(butTabBarPressed:) forControlEvents:UIControlEventTouchUpInside];
 [butTabBar setTitleEdgeInsets:UIEdgeInsetsMake(35, 0, 0, 0)];
 UIFont *buttabfont = [UIFont boldSystemFontOfSize:12];
 [butTabBar setTitleColor:[UIColor colorWithRed:4.0f/255.0 green:62.0f/255.0 blue:48.0f/255.0 alpha:1.0] forState:UIControlStateNormal];
 [butTabBar.titleLabel setFont:buttabfont];

 [tabBarScrollView addSubview:butTabBar];
 }

 //        UIButton *jobButton = [UIButton buttonWithType:UIButtonTypeCustom];
 //        jobButton.tag = tagOfJobButton;
 //        [jobButton setFrame:CGRectMake(4 * widthTabBut, 0, widthTabBut, heightTabBut)];
 //        [jobButton addTarget:self action:@selector(butTabBarPressed:) forControlEvents:UIControlEventTouchUpInside];
 //        [jobButton setBackgroundImage:[UIImage imageNamed:menuJob] forState:UIControlStateNormal];
 //        [jobButton setTitle:@"JOB" forState:UIControlStateNormal];
 //        [jobButton setTitleEdgeInsets:UIEdgeInsetsMake(35, 0, 0, 0)];
 //        UIFont *font = [UIFont boldSystemFontOfSize:12];
 //        [jobButton setTitleColor:[UIColor colorWithRed:4.0f/255.0 green:62.0f/255.0 blue:48.0f/255.0 alpha:1.0] forState:UIControlStateNormal];
 //        [jobButton.titleLabel setFont:font];
 // [tabBarScrollView addSubview:jobButton];

 //whether or not add Tab bar button MESSAGE
 if(!ENABLE_MESSAGE)
 {
 if(ENABLE_CHECK_ACCESS_RIGHT_MESSAGE)
 {
 //message is disable
 if(!(messages.isAllowedView || messages.isActive || messages.isALlowedEdit))
 {
 //decrease number tab button
 numberOfBtn -= 1;
 }
 }
 }

 if(!(settings.isAllowedView || settings.isActive || settings.isALlowedEdit))
 {
 numberOfBtn -= 1;
 }

 if(!ENABLE_ADDITIONAL_JOB)
 {
 numberOfBtn -= 1;
 }
 else if(!(additionalJob.isActive||additionalJob.isALlowedEdit||additionalJob.isAllowedView))
 {
 numberOfBtn -= 1;
 }

 UIImageView *imgViewRightMenuIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imgMenuRight]];
 imgViewRightMenuIcon.frame = CGRectMake(315, 0, 5, heightTabBut);
 imgViewRightMenuIcon.tag = 1002;
 imgViewRightMenuIcon.hidden = YES;
 [tabBarScrollView addSubview:imgViewRightMenuIcon];

 UIImageView *imgViewLeftMenuIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imgMenuRight]];
 imgViewLeftMenuIcon.frame = CGRectMake(0, 0, 5, heightTabBut);
 imgViewLeftMenuIcon.tag = 1003;
 imgViewLeftMenuIcon.hidden = YES;
 [tabBarScrollView addSubview:imgViewLeftMenuIcon];

 if(numberOfBtn > 4)
 {
 imgViewRightMenuIcon.hidden = NO;
 }

 //        [tabBarScrollView setContentSize:CGSizeMake((numberOfBtn - moveButtonToLeft) * widthTabBut, heightTabBut)];
 [tabBarScrollView setContentSize:CGSizeMake(numberOfBtn * widthTabBut, heightTabBut)];
 }
 }

 -(void) insertScrollViewTabbar{
 [[self.tabBarController tabBar] insertSubview:tabBarScrollView aboveSubview:self.tabBarController.view];
 [self setSelectedButton:tagOfHomeButton];
 }

 -(void)createScrollViewTabBar{
 [self initScrollViewTabbar:numOfTabBut];
 [self initScrollViewTabbarButtons: numOfTabBut];
 [self insertScrollViewTabbar];

 //Count tabbar button again
 countTabbarButtons = [self countTabbarButtons];
 }
 */

#pragma mark -
#pragma mark InitHomeView Methods

-(void)drawUnderlinedLabel:(UILabel *)label inTableViewCell:(UITableViewCell *)cell{

    //Predicate iOS >= 6
    // NSString *string = label.text;
    //CGSize stringSize = [string sizeWithFont:label.font];

    //Replace with this
    NSAttributedString* theText = label.attributedText;
    CGRect rectSize = [theText boundingRectWithSize:CGSizeMake(label.frame.size.width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin context:NULL];
    CGSize stringSize = rectSize.size;
    //End replace

    CGRect rect = label.frame;
    CGRect labelFrame;
    //NSLog(@"%f %f",rect.origin.x,stringSize.width);
    if (stringSize.width > rect.size.width) {
        labelFrame = CGRectMake(rect.origin.x, stringSize.height - 4, rect.size.width, 2);
    } else {
        labelFrame = CGRectMake(rect.origin.x + (rect.size.width - stringSize.width)/2.0, stringSize.height - 4, stringSize.width, 2);
    }
    UILabel *lineLabel = (UILabel *)[cell viewWithTag:tagUnderline];
    lineLabel.frame = labelFrame;
    //NSLog(@"%f %f %f %f",labelFrame.origin.x,labelFrame.origin.y, labelFrame.size.width, labelFrame.size.height);
    //UILabel *lineLabel = [[UILabel alloc] initWithFrame:labelFrame];
    lineLabel.backgroundColor =[UIColor blackColor];
    //[cell addSubview:lineLabel];
    //[lineLabel release];
}

/**
 *  butViewCleanedRooms Touch Up Inside
 **/
- (IBAction)butViewCleanedRoomsPressed:(id)sender {

    [self.navigationItem.backBarButtonItem setTitle:[[LanguageManagerV2 sharedLanguageManager]  getStringLanguageByStringId:L_BACK]];

    if (![UserManagerV2 isSupervisor]) {
        if (self.roomCompletedView == nil) {
            roomCompletedView = [[RoomCompletedViewV2 alloc] initWithNibName:@"RoomCompletedViewV2" bundle:nil];
        } else {
            [roomCompletedView loadDataTable];
        }
        [self.navigationController pushViewController:roomCompletedView animated:YES];

    }else
    {
        RoomInspectedViewV2*  roomInspectedView = [[RoomInspectedViewV2 alloc] initWithNibName:@"RoomInspectedViewV2" bundle:nil];

        [self.navigationController pushViewController:roomInspectedView animated:YES];
    }

}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    //    [super didReceiveMemoryWarning];

    // Release any cached data, images, etc that aren't in use.
}
#pragma mark -
#pragma mark View lifecycle

-(void)viewDidAppear:(BOOL)animated{

    [super viewDidAppear:animated];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];

    self.hidesBottomBarWhenPushed = YES;

    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        [butViewCleanedRooms setBackgroundImage:[UIImage imageNamed:imgBtnCompletedAndInspectedFlat] forState:UIControlStateNormal];
    }

    //Move title to center vertical
    CGRect titleViewBounds = self.navigationItem.titleView.bounds;
    int moveLeftTitle = 0;
    if(!isCenterTitle){
        isCenterTitle = YES;
        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
            moveLeftTitle = 10;
        }
    }
    self.navigationItem.titleView.bounds = CGRectMake(titleViewBounds.origin.x + moveLeftTitle, Y_TOPBAR_TITLE, titleViewBounds.size.width, titleViewBounds.size.height);

    //disable butLogOut on tabbarcontroller
    if (isSyncNow == NO) {
        UIButton *butLogOut = (UIButton *)[[self.tabBarController tabBar] viewWithTag:tagOfLogOutButton];
        [butLogOut setEnabled:YES];
    }

    //set HomeView is selectedButton in TabBar
    [self setSelectedButton:tagOfHomeButton];

    //Reload data when screen appear
    //RoomAssignmentEngine *roomAssignmentEngine = [[RoomAssignmentEngine alloc] init];
    //SuperRoomModel *modelSuper =[roomAssignmentEngine loadInfoRoomAssignment];
    if([UserManagerV2 isSupervisor]){
        if ([[UserManagerV2 sharedUserManager].currentUserAccessRight isEnabledSupHomeFilter]) {
            [[RoomManagerV2 sharedRoomManager] getRoomInspectionByFilterType:(int)firstComboIndexSelected filterDetailId:(int)filterDetailId];
        } else {
            [[RoomManagerV2 sharedRoomManager] getRoomInspection];
        }
    } else{
        //Set title for button complete
        if(!butViewCleanedRooms.isHidden){
            [[RoomManagerV2 sharedRoomManager] getTotalRoomComlete];
            NSInteger amount = [[RoomManagerV2 sharedRoomManager].roomAssignmentList count];
            //NSString *title = [NSString stringWithFormat:@"%@ (%i)",[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_COMPLETED],amount];
            NSString *title = [NSString stringWithFormat:@"%@ (%i)",[L_BTN_VIEW_ASSIGNMENT currentKeyToLanguage],(int)amount];
            [butViewCleanedRooms setTitle:title forState:UIControlStateNormal];
        }
        [[RoomManagerV2 sharedRoomManager] getRoomAssignments];
    }
    [self getOtherActivityAssignment];
    [self loadRoomAssignmentData];
    [self updateBadgeNumber:NO];
    [self.roomTableView reloadData];

    self.currentSelectedView = [NSString stringWithFormat:@"%@", notificationSaveHomeView];
    if (![loginView.view.superview isEqual:self.tabBarController.view]) {
        if(!isDemoMode) {
            //show wifi view
            [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationShowWifiView] object:nil];
        }

    } else {
        //hide wifi view
        [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationHideWifiView] object:nil];
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    [roomTableView setEditing:NO];
}

- (void)responseLongPressButton:(UILongPressGestureRecognizer*)sender {
    if (!roomAssignment.isALlowedEdit) {
        return;
    }
    if (sender.state == UIGestureRecognizerStateBegan) {
        if (roomTableView.editing) {
            [roomTableView setEditing:NO];
            [roomTableView reloadData];
        } else {
            [roomTableView setEditing:YES];
        }
    }
}

- (void)responseLongPressButtonForChangeCleaningStatus:(UILongPressGestureRecognizer*)sender {

    CGPoint touchPoint = [sender locationInView:roomTableView];
    NSIndexPath *indexPath = [roomTableView indexPathForRowAtPoint:touchPoint];

    //HaoTran[20130515/Implement re-order] - Setting reorder for list room
    if (roomAssignment.isALlowedEdit) {
        if (sender.state == UIGestureRecognizerStateBegan) {
            if (roomTableView.editing) {
                [roomTableView setEditing:NO];
                [roomTableView reloadData];
            } else {
                [roomTableView setEditing:YES];
            }
        }
        return;
    }
    //HaoTran[20130515/Implement re-order] - END

    //2014/11/21 Fixed Wrong UAR because the longpress button is just for Changing the cleaning status and Drag and Drog function

    /*if (!manualUpdateRoomStatus.isActive) {
     return;
     }*/

    /*
     if (restrictionAssignment.isActive) {
     if (([CommonVariable sharedCommonVariable].currentPauseRoom == noPauseRoom && indexPath.row != 0)
     ||([CommonVariable sharedCommonVariable].currentPauseRoom != greaterThanOnePauseRoom && indexPath.row != 0 && indexPath.row != 1)) {
     return;
     }
     }
     */
    NSObject *object = [roomAssignmentData objectAtIndex:indexPath.row];
    if ([object isKindOfClass:[NSDictionary class]]) {

        selectedRoomAssignment = (NSDictionary*)object;

        if ([(NSString *)[selectedRoomAssignment objectForKey:kRoomStatusId] integerValue] == ENUM_ROOM_STATUS_OD || ([[selectedRoomAssignment objectForKey:kRoomStatusId] integerValue] == ENUM_ROOM_STATUS_OC
                                                                                                                      && [UserManagerV2 isSupervisor]
                                                                                                                      && ![[[UserManagerV2 sharedUserManager] currentUserAccessRight] roomAssignment])) {

            if (cleaningStatusPopup == nil) {
                cleaningStatusPopup = [[CleaningStatusPopupView alloc] initWithDelegate:self andOption:NO];
            }
            [cleaningStatusPopup setRoomNo:(NSString *)[selectedRoomAssignment objectForKey:kRoomNo]];

            //[self.tabBarController.view addSubview:cleaningStatusPopup];
            [self.tabBarController addSubview:cleaningStatusPopup];
        }
    }
}

//Remove Button Tabbar For PRD 2.2 Release 2
/*
 change size of bottom bar items and reload the view
 */
/*
 -(void) changeTabBarScrollViewToNumOfButton:(NSInteger) numOfBtns{

 if (numOfBtns < numOfTabBut) {
 [tabBarScrollView removeFromSuperview];
 tabBarScrollView = nil;
 [self initScrollViewTabbar:numOfBtns];
 [self initScrollViewTabbarButtons:numOfBtns];
 [self setCaptionsTabbar];
 [self insertScrollViewTabbar];

 }
 else if(numOfBtns == numOfTabBut){
 [tabBarScrollView removeFromSuperview];
 tabBarScrollView = nil;
 [self initScrollViewTabbar:numOfTabBut];
 [self initScrollViewTabbarButtons: numOfTabBut];
 [self setCaptionsTabbar];
 [self insertScrollViewTabbar];
 }

 //Update badge number in tabbar view
 [self updateBadgeNumber: NO];
 }
 */

#pragma mark - User Information

-(void) startSchedulingUserInformation
{
    if (timerCheckingLoginUserInformation) {
        [timerCheckingLoginUserInformation invalidate];
        timerCheckingLoginUserInformation = nil;
    }
    timerCheckingLoginUserInformation = [NSTimer scheduledTimerWithTimeInterval:[[UserManagerV2 sharedUserManager].currentUserAccessRight userCheckingLoginInterval] target:self selector:@selector(checkValidUserLogin) userInfo:nil repeats:YES];
}

-(void) stopSchedulingUserInformation
{
    if (timerCheckingLoginUserInformation) {
        [timerCheckingLoginUserInformation invalidate];
        timerCheckingLoginUserInformation = nil;
    }
}

-(void)checkValidUserLogin
{
    BOOL shouldLogoutUser = [[UserManagerV2 sharedUserManager] shouldLogOutUser];

    if (shouldLogoutUser) {

        //Fixed multiple alert logout user
        if (isShowingAlertForceLogout) {
            return;
        }
        isShowingAlertForceLogout = YES;
        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:imgYesBtn], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_message_logout_user] message:@"" delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getOK] otherButtonTitles: nil];
        alert.tag = tagAlertForceUserLogout;
        [alert show];
    }
}

//Log out current user
-(void)logOutUser
{

    //Reset combobox for supervisor filter
    [self resetComboboxes];

    //Fixed last subview still appear after re-login when app force
    [self.tabBarController removeLastSubview];

    //Fixed Panic still running when logout user
    if([PanicManager isRunningPanic]){
        [self.slidingMenuBar setAlwaysHoldPanicButton:NO];
        [coverPanicView removeFromSuperview];
        [[PanicManager sharedPanicManager] stopPanic];
    }

    //Fixed duplicated show alert force logout
    isShowingAlertForceLogout = NO;

    //Stop current timer for checking user information
    [self stopSchedulingUserInformation];

    //Let server know current user already log out
    [[UserManagerV2 sharedUserManager] postLogOutWSForUserId:[UserManagerV2 sharedUserManager].currentUser.userId];

    ////Remove remote notification when app terminated
    //[[UIApplication sharedApplication] unregisterForRemoteNotifications];

    //Set Home Tabbar to avoid bug layout in setting
    //int homeIndex = 0;
    //[self setTabBarSelectedIndex:homeIndex willAnimate:NO];

    //clear additional job count
    countPendingAddJobs = 0;

    //Save logout date
    int currentUserId = [UserManagerV2 sharedUserManager].currentUser.userId;
    UserLogModelV2 *userLogData = [[UserManagerV2 sharedUserManager] getUserLogByUserId:currentUserId];
    if(userLogData){
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSString *logoutDate = [formatter stringFromDate:[NSDate date]];
        userLogData.userLastLogoutDate = logoutDate;
        userLogData.userLastLoginDate = nil;
        [[UserManagerV2 sharedUserManager] updateUserLog:userLogData];
    }

    if (isEdit == YES) {
        [[self.navigationItem rightBarButtonItem] setTitle:[self getTextForEditButton]];
        [[self.navigationItem rightBarButtonItem] setStyle:UIBarButtonItemStylePlain];
        [roomTableView setEditing:NO];
        isEdit = NO;
    }
    [self.roomAssignmentData removeAllObjects];
    [roomTableView setEditing:NO];
    [roomTableView reloadData];

    //LogOut here
    if (loginView == nil) {
        loginView = [[LoginViewV2 alloc] initWithNibName:@"LoginViewV2" bundle:nil];
        [loginView setDelegate:self];
        //Catch Frame
        [loginView.view setFrame:self.tabBarController.view.bounds];
        [self.tabBarController.view addSubview:loginView.view];
    }

    //set variable isLoginShowwing is YES to dismiss Alert and Sync when you stay in Login View
    self.isLoginShowwing = YES;
    if (isDemoMode) {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:IS_LOAD_LANGUAGE];
        [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:ENABLE_DEMO_MODE];
    }
    BOOL isAlreadyLoadLanguage = [[NSUserDefaults standardUserDefaults] boolForKey:IS_LOAD_LANGUAGE];
    if(!isAlreadyLoadLanguage) {
        [loginView performSelector:@selector(updateLanguageSystem) withObject:nil afterDelay:0.01f];
    }

    //Clear information before Log out
    [self clearSubviewsContentInHomeView];
    [[HomeViewV2 shareHomeView].slidingMenuBar hideAll];
    [[HomeViewV2 shareHomeView].slidingLegend hideAll];
    //Remove for add loginview without animation
    /*
     [UIView transitionWithView:self.tabBarController.view duration:1
     options:UIViewAnimationOptionTransitionCurlDown
     animations:^ { [self.tabBarController.view addSubview:loginView.view]; }
     completion:nil];
     */

    [self.tabBarController.view addSubview:loginView.view];
    [loginView setCaptionsView];

    //hide wifi view
    [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationHideWifiView] object:nil];

    //send notification to clear message view
    [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationClearMessageMainView] object:nil];
}

-(void) clearSubviewsContentInHomeView
{
    [self.navigationController popToRootViewControllerAnimated:NO];
    [roomTableView reloadData];

    [self.labelHousekeeperName setText:@""];
    [self.label_location_property_name setText:@""];
    [self.buildingNameLabel setText:@""];

    //hide image signal supervisor
    /*
     UIImageView *imgView = (UIImageView *)[[self.tabBarController tabBar] viewWithTag:numOfTabBut + 1];
     [imgView setHidden:YES];
     */

    _heightConstrainBtnComplete.constant = 0.0f;
    [butViewCleanedRooms setHidden:YES];
    [labelHousekeeper setText:@""];
    [nameUserLabel setText:@""];

    //Clear top bar view
    BOOL isHiddenTopbar = (topBarView.alpha == 0.0f);
    if(!isHiddenTopbar){
        [self hiddenHeaderView];
    }

    //Remove all menu buttons
    [slidingMenuBar removeAllButtons];

    //Remove Button Tabbar For PRD 2.2 Release 2
    /*
     NSArray *arrayViews = [tabBarScrollView subviews];
     for (UIView *currentView in arrayViews) {
     [currentView setHidden:YES];
     }*/
}

/*
 this function actice when login OK
 */
-(void) removeLoginViewV2:(NSNotification *)notificate {
    [self hideComboboxView];
    //Clear old data in navigation view
    NSArray *titleBarSubviews = [self.navigationItem.titleView subviews];
    for (UIView *currentView in titleBarSubviews) {
        if([currentView isKindOfClass:[UIButton class]]){
            UIButton *currentButton = (UIButton*)currentView;
            [currentButton setTitle:@"" forState:UIControlStateNormal];
        }
    }

    //Hide room table for waiting load data
    [roomTableView setHidden:YES];

    //Load access right
    [self loadAccessRights];

    if(isDemoMode){
        [qrScanButton setEnabled:YES];
    }
    else{
        if(!ENABLE_QRCODESCANNER)
        {
            if(ENABLE_CHECK_ACCESS_RIGHT_QRCODESCANNER)
            {
                [qrScanButton setHidden:NO];
                [qrScanButton setAlpha:0.7f];
                [qrScanButton setEnabled:NO];

                if (QRCodeScanner.isActive) {
                    [qrScanButton setAlpha:1.0f];
                    [qrScanButton setEnabled:YES];
                }

            }
            else
            {
                [qrScanButton setHidden:YES];
            }
        }
    }

    //Show wifi view
    if(!isDemoMode)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationShowWifiView] object:nil];
    }

    SyncManagerV2 *smanager = [[SyncManagerV2 alloc] init];
    //hard code maxOperations
    int maxOperations = 9;
    __block int pastedOperations = 0;
    [SyncManagerV2 setMaxSyncOperations:maxOperations];
    [SyncManagerV2 setNextStepOperations:(float)(1/(float)maxOperations)];

    //enable flag to announce, is loading data
    [[HomeViewV2 shareHomeView] waitingLoadingData];

    [CommonVariable sharedCommonVariable].roomIsCompleted = 0;
    [CommonVariable sharedCommonVariable].currentPauseRoom = noPauseRoom;

    //set isLoginShowwing is NO
    self.isLoginShowwing = NO;

    // sync data with server
    __block MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    //[HUD createCustomViewProgressBar:300];
    [self.tabBarController.view addSubview:HUD];

    //    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getRaLoadingData]];
    HUD.tag = tagHudRemoveLoginView;
    HUD.delegate = self;
    [HUD show:YES];
    // Get user list
    [HUD setLabelText:[L_RA_LOADING_DATA currentKeyToLanguage]];

    //    //create cancel function layout
    //    HUD.hasCancelButton = YES;

    HUD.minSize = CGSizeMake(200,HUD.minSize.height); //200 is max size of text

    //[[AdHocMessageManagerV2 sharedAdHocMessageManagerV2] setHud:HUD];

    //get Room Assignment WS
    //    [HUD setLabelText:@"Get Room Assignment"];

    //Because this is in main thread so can't update percentage of progress bar view
    //we need perform get data from other thread and update percent view in main thread.
    //dispatch_group_async is perform in background task
    //dispatch_group_notify to wait all thread in group thread finished to update data
    dispatch_group_t group = dispatch_group_create();

    //MARK: Get data in back ground and HUD progress showing
    dispatch_group_async(group,dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^ {
        //Hao Tran - Remove for speed up login
        //NSMutableArray *raModels;
        int countRoomAssignments = 0;
        if ([UserManagerV2 isSupervisor] ) {
            //Hao Tran - Remove for speed up login
            //raModels = [[RoomManagerV2 sharedRoomManager] getAllRoomAssignmentsWithUserId:[[[UserManagerV2 sharedUserManager] currentUser] userId]];
            countRoomAssignments = [[RoomManagerV2 sharedRoomManager] countAllRoomAssignmentsWithUserId:[[[UserManagerV2 sharedUserManager] currentUser] userId]];
            //Hao Tran - END
        }
        else {
            //Hao Tran - Remove for speed up login
            //RoomAssignmentModelV2 *ramodel = [[RoomAssignmentModelV2 alloc] init];
            //ramodel.roomAssignment_UserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
            //raModels = [[RoomManagerV2 sharedRoomManager] loadAllRoomAssignmentsByUserID:ramodel];
            countRoomAssignments = [[RoomManagerV2 sharedRoomManager] countAllRoomAssignmentsByUserID: [[[UserManagerV2 sharedUserManager] currentUser] userId]];
            //Hao Tran - END
        }
        pastedOperations ++;
        HUD.progress = (float)(pastedOperations/(float)maxOperations);

        //Remove for always update room assignment after login
        //if (countRoomAssignments <= 0) {
        if(!isDemoMode)
        {
            [[RoomManagerV2 sharedRoomManager] getRoomTypeListWSWithHotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId];
            [smanager getRoomAssignmentWithUserID:[[[UserManagerV2 sharedUserManager] currentUser] userId] AndLastModified:[self getTempLastModifiedRoomAssignment] AndPercentView:HUD];
        }
        //}
        pastedOperations ++;
        HUD.progress = (float)(pastedOperations/(float)maxOperations);



        pastedOperations ++;
        HUD.progress = (float)(pastedOperations/(float)maxOperations);

        //HaoTran[20130607/Enhace Load User List] - END
        //-----------------------------------------------------------

        //get room inspection status
        if(!isDemoMode)
        {
            NSString *lastModifiedRoomInspection = [[UserManagerV2 sharedUserManager] getCurrentUserConfigValueByKey:USER_CONFIG_ROOM_INSPECTION_LASTMODIFIED];
            [[RoomManagerV2 sharedRoomManager] getInspectionStatusWSWithUserId:[[[UserManagerV2 sharedUserManager] currentUser] userId] andLastModified:lastModifiedRoomInspection AndPercentView:HUD];
        }
        pastedOperations ++;
        HUD.progress = (float)(pastedOperations/(float)maxOperations);

        //get Room Statuses WS
        //[HUD setLabelText:LOADING_GET_ROOM_STATUS];
        if(!isDemoMode)
        {
            [self getRoomStatusesWSWithPercentView:HUD];
        }

        pastedOperations ++;
        HUD.progress = (float)(pastedOperations/(float)maxOperations);

        //get Cleaning Statuses WS
        //[HUD setLabelText:LOADING_GET_CLEANING_STATUS];
        if(!isDemoMode)
        {
            [self getCleaningStatusesWSWithPercentView:HUD];
        }

        pastedOperations ++;
        HUD.progress = (float)(pastedOperations/(float)maxOperations);

        // getHotelInfo
        [self getHotelInfoWSWithPercentView:HUD];
        pastedOperations ++;
        HUD.progress = (float)(pastedOperations/(float)maxOperations);
        //Hao Tran - Remove for speed up getting data
        /*
         //get message subject
         if(ENABLE_MESSAGE || (!ENABLE_MESSAGE && ENABLE_CHECK_ACCESS_RIGHT_MESSAGE && messages.isActive)) {
         [self getMessageSubjectWithPercentView:HUD];
         }*/
        // Only load other activity data when INI enable
        if (![UserManagerV2 isSupervisor]) {
            if ([[UserManagerV2 sharedUserManager].currentUserAccessRight isEnableOtherActivity]) {

                NSInteger buildingId = [[NSUserDefaults standardUserDefaults] integerForKey:[NSString stringWithFormat:@"%@%d", CURRENT_BUILDING_ID, [UserManagerV2 sharedUserManager].currentUser.userId]];
                [[OtherActivityManager sharedOtherActivityManager] loadWSOtherActivitiesLocationByUserId:[UserManagerV2 sharedUserManager].currentUser.userId hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId lastModified:nil];
                [[OtherActivityManager sharedOtherActivityManager] loadWSOtherActivitiesStatusByUserId:[UserManagerV2 sharedUserManager].currentUser.userId lastModified:nil];
                [[OtherActivityManager sharedOtherActivityManager] loadWSOtherActivityAssignmentByUserId:[UserManagerV2 sharedUserManager].currentUser.userId hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId buildingId:(int)buildingId dateTime:[ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"]];
            }
        }
        pastedOperations ++;
        HUD.progress = (float)(pastedOperations/(float)maxOperations);
        //Update unread message
        [[HomeViewV2 shareHomeView] messageReciever:NO callFrom:IS_PUSHED_FROM_HOME];
        //Get panic config
        if(!isDemoMode)
        {
            [[PanicManager sharedPanicManager] getPanicConfigByUserId:[UserManagerV2 sharedUserManager].currentUser.userId hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId lastModified:nil];
        }

        pastedOperations ++;
        HUD.progress = (float)(pastedOperations/(float)maxOperations);
    });

    //MARK: Update UI after load data success and remove HUD showing
    dispatch_group_notify(group,dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^ {

        dispatch_async(dispatch_get_main_queue(), ^{

            if([UserManagerV2 isSupervisor]) {
                if ([[UserManagerV2 sharedUserManager].currentUserAccessRight isEnabledSupHomeFilter]) {
                    [self showComboboxView];
                } else {
                    [self hideComboboxView];
                }
            } else {
                [self hideComboboxView];
            }

            _heightConstrainBtnComplete.constant = 38.0f;
            [butViewCleanedRooms setHidden:NO];
            //Show room table after finish load data

            [SyncManagerV2 setMaxSyncOperations:0];
            [SyncManagerV2 setNextStepOperations:0];

            if([UserManagerV2 isSupervisor]) {
                if ([[UserManagerV2 sharedUserManager].currentUserAccessRight isEnabledSupHomeFilter]) {
                    [[RoomManagerV2 sharedRoomManager] getRoomInspectionByFilterType:(int)firstComboIndexSelected filterDetailId:(int)filterDetailId];
                } else {
                    [[RoomManagerV2 sharedRoomManager] getRoomInspection];
                }
            } else {
                [[RoomManagerV2 sharedRoomManager] getRoomAssignments];
            }
            [self getOtherActivityAssignment];
            [self loadRoomAssignmentData];
            [roomTableView reloadData];
            //Update badge number in tabbar view

            SuperRoomModelV2 *modelSuper = [[SuperRoomModelV2 alloc] init];
            [self.roomTableView reloadData];
            self.labelHousekeeperName.text = [[RoomManagerV2 sharedRoomManager] getHouseKeeperName];

            NSString *strGetPropertyName = [[RoomManagerV2 sharedRoomManager] getPropertyName:modelSuper];
            NSString *strGetBuildingName = [[RoomManagerV2 sharedRoomManager] getbuildingName:modelSuper];
            if (strGetBuildingName==nil) {
                strGetBuildingName=@" ";
            }
            if (strGetPropertyName==nil) {
                strGetPropertyName=@" ";
            }
            [self.label_location_property_name setText:[NSString stringWithFormat:@"%@ %@", strGetBuildingName, strGetPropertyName]];
            [self.buildingNameLabel setText:[NSString stringWithFormat:@"%@", strGetPropertyName]];

            //#warning dthanhson edit
            //    NSDictionary *dic = [[LanguageManagerV2 sharedLanguageManager] getCurrentDictionaryLanguage];
            //    UIButton *labelTitle = (UIButton *)self.navigationItem.titleView;
            //    if (![UserManagerV2 isSupervisor]){
            //        [labelTitle setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ASSIGNMENT] forState:UIControlStateNormal];
            //    }
            //    else
            //    {
            //        [labelTitle setTitle:[dic valueForKey:[NSString stringWithFormat:@"%@", L_INSPECTION]] forState:UIControlStateNormal];
            //    }
            //#warning end

            ////////////////////////// setup font , color , font size of label , button//////////
            if (topBarView == nil) {
                SuperRoomModelV2 *superModel = [[SuperRoomModelV2 alloc] init];
                topBarView = [[TopbarViewV2 alloc] initViewWithSuperModel:superModel];
                [self.view addSubview:topBarView];
            } else {
                [topBarView refresh:[[SuperRoomModelV2 alloc] init]];
            }

            //Show topbar
            BOOL isHiddenTopbar = (topBarView.alpha == 0.0f);
            if(isHiddenTopbar){
                [self hiddenHeaderView];
            }

            [self populateDataIntoNavigationbar];

            if (wifiViewController == nil) {
                wifiViewController = [[WifiIConViewController alloc] init];
            }
            [self.tabBarController.view addSubview:wifiViewController.view];

            NSData* logoData = [[RoomManagerV2 sharedRoomManager] getHotelLogo:modelSuper];
            if (logoData!=nil) {
                self.hotelLogo.image = [UIImage imageWithData:logoData];
            }

            //check is supervisor
            /*
             UIImageView *imgView = (UIImageView *)[[self.tabBarController tabBar] viewWithTag:numOfTabBut + 1];//edit
             [imgView setHidden:![UserManagerV2 isSupervisor]];*/

            //set tabBar to HomeView (RoomAssignment) and popToRootController
            [self.tabBarController setSelectedIndex:0];
            [self.navigationController popToRootViewControllerAnimated:NO];

            //Remove Button Tabbar For PRD 2.2 Release 2
            /*
             // change bottom bar
             [tabBarScrollView setHidden:NO];
             if ([UserManagerV2 isSupervisor]) {
             // show supervisor image
             //if(numberOfTagButton < numOfTabBut){
             // add view
             numberOfTagButton = 0;
             [self changeTabBarScrollViewToNumOfButton: numOfTabBut];
             //}
             }
             else{
             // remove view
             numberOfTagButton = 0;
             [self changeTabBarScrollViewToNumOfButton: numOfTabBut - 2];
             }

             //Count tabbar button again
             countTabbarButtons = [self countTabbarButtons];

             //scroll tabBarScroll to 0
             CGRect frame = tabBarScrollView.frame;
             frame.origin.x = 0;
             tabBarScrollView.frame = frame;
             [tabBarScrollView scrollRectToVisible:frame animated:NO];
             */
            [self addSlidingLegend];
            [self addSlidingMenuBar];
            if(isDemoMode)
                [self updateBadgeNumber: NO];
            else
                [self updateBadgeNumber: YES];
            //Check and show pop up messages sent from WS
            [self getMessagePopupFromWS];

            //Update unread messages
            [self updateUnreadMessage];

            //Show alert new message
            if(isDemoMode)
                [self announceNewMessage:nil];
            else{
                NSString *willShowAlertMessage = [[UserManagerV2 sharedUserManager] getCurrentUserConfigValueByKey:USER_CONFIG_MESSAGE_IS_SHOW_NOTIFICATION];
                if(willShowAlertMessage.length > 0) {
                    if([willShowAlertMessage caseInsensitiveCompare:@"true"] == NSOrderedSame
                       || [willShowAlertMessage caseInsensitiveCompare:@"yes"] == NSOrderedSame) {
                        [self announceNewMessage:nil];
                    }
                }
            }

            //Hao Tran remove - only get message in message view or sync
            /*
             if (!isStartedMessageReciever) {
             if([LogFileManager isLogConsole])
             {
             NSLog(@"get message");
             }
             if(ENABLE_MESSAGE || (!ENABLE_MESSAGE && ENABLE_CHECK_ACCESS_RIGHT_MESSAGE && (messages.isActive || messages.isALlowedEdit || messages.isAllowedView)))
             {
             [NSTimer scheduledTimerWithTimeInterval:MESSAGE_DURATION
             target:self
             selector:@selector(getMessageFromServer)
             userInfo:nil
             repeats:YES];
             }
             isStartedMessageReciever = YES;
             }*/

            if(!isDemoMode)
            {
                //show wifi view
                [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationShowWifiView] object:nil];
            }

            //    UIButton *butCheckList = (UIButton *) [[self.tabBarController tabBar] viewWithTag:tagOfCheckListButton];
            //    [butCheckList setEnabled:NO];
            //    [NSThread detachNewThreadSelector:@selector(updateUnreadMessage) toTarget:self withObject:nil];

            //    [self loadCheckListForAllRoom];

            //update sync number
            [smanager updateMustPostRecords];

            [self setCaptionsView];

            //set visible
            [labelRoom setHidden:NO];
            [labelRMStatus setHidden:NO];
            [labelVIP setHidden:NO];
            [labelCleaningStatus setHidden:NO];
            if([UserManagerV2 isSupervisor])
            {
                _heightConstrainBtnComplete.constant = 0.0f;
                [butViewCleanedRooms setHidden:YES];
            }
            else
            {
                _heightConstrainBtnComplete.constant = 38.0f;
                [butViewCleanedRooms setHidden:NO];
            }

            //Remove Button Tabbar For PRD 2.2 Release 2
            //[tabBarScrollView setHidden:NO];

            /*
             //Remove image from tabbarscrollview
             UIImageView *imgViewTabbar = (UIImageView *)[self.tabBarController.tabBar viewWithTag:tagTabbarBG];
             [imgViewTabbar removeFromSuperview];
             */

            [roomTableView setHidden:NO];

            //end loading data
            [[HomeViewV2 shareHomeView] endWaitingLoadingData];

            [HUD hide:YES];
            [HUD removeFromSuperview];
            [[HomeViewV2 shareHomeView].slidingMenuBar showAll];
        });

    });

    //Remove for iOS 6 and later
    //dispatch_release(group);
    [[NSNotificationCenter defaultCenter] postNotificationName:kReachabilityUpdateNotification object:nil];
}

- (void)getOtherActivityAssignment {

    NSMutableArray * listOtherActivities = [[OtherActivityManager sharedOtherActivityManager] loadOtherActivityAssignmentsByUserId:[[UserManagerV2 sharedUserManager] currentUser].userId];

    bool isEnglishLang = [[LanguageManagerV2 sharedLanguageManager] currentLanguageIsEqualTo:@"%@",ENGLISH_LANGUAGE];
    [listOtherActivityDetailDisplay removeAllObjects];
    for (OtherActivityAssignmentModel *detail in listOtherActivities) {
        OtherActivityDetailsDisplay *displayCell = [[OtherActivityDetailsDisplay alloc] init];
        displayCell.otherActivityDetail = detail;
        OtherActivityModel *model = [[OtherActivityManager sharedOtherActivityManager] loadOtherActivityById:detail.oaa_activities_id];
        if (model) {
            if (isEnglishLang) {
                displayCell.otherActivityString = model.oa_name;
            } else {
                displayCell.otherActivityString = model.oa_name_lang;
            }
        }
        [listOtherActivityDetailDisplay addObject:displayCell];
    }
}

-(void) loadCheckListForAllRoom {
    CheckListManagerV2 *manager = [[CheckListManagerV2 alloc] init];
    NSInteger userId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    if ([[[UserManagerV2 sharedUserManager] currentUser] userSupervisorId] == 0) {
        return;
    }

    NSMutableArray *arrayRoomAssign = nil;

    if ([[UserManagerV2 sharedUserManager].currentUserAccessRight isEnabledSupHomeFilter]) {
        [[RoomManagerV2 sharedRoomManager] getRoomInspectionByFilterType:(int)firstComboIndexSelected filterDetailId:(int)filterDetailId];
    } else {
        [[RoomManagerV2 sharedRoomManager] getRoomInspection];
    }

    arrayRoomAssign = [NSMutableArray arrayWithArray:[[RoomManagerV2 sharedRoomManager] roomAssignmentList]];

    //insert checklist by all room
    for (NSDictionary *roomAssign in arrayRoomAssign) {
        //load room model
        RoomModelV2 *roomModel = [[RoomModelV2 alloc] init];
        roomModel.room_Id = [roomAssign objectForKey:kRoomNo];
        [[RoomManagerV2 sharedRoomManager] loadRoomModel:roomModel];

        //load data of checkList by roomAssignId
        CheckListModelV2 *chkModelWS = [[CheckListModelV2 alloc] init];
        chkModelWS.chkListId  = [[roomAssign objectForKey:kRoomAssignmentID] integerValue];
        chkModelWS.chkListUserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
        [manager loadCheckListData:chkModelWS];

        if (chkModelWS.chkListRoomId.length <= 0) {
            //Insert CheckList
            chkModelWS = [[CheckListModelV2 alloc] init];
            chkModelWS.chkListId = [[roomAssign objectForKey:kRoomAssignmentID] integerValue];
            chkModelWS.chkListRoomId  = roomModel.room_Id;
            chkModelWS.chkListUserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];

            int inspectionStatusId = [[roomAssign objectForKey:KInspection_Status_id] intValue];
            if (inspectionStatusId == ENUM_INSPECTION_COMPLETED_PASS) {
                chkModelWS.chkListStatus = chkStatusPass;//tChkPass;
            }
            else if (inspectionStatusId == ENUM_INSPECTION_COMPLETED_FAIL) {
                chkModelWS.chkListStatus = chkStatusFail;//tChkFail;
            }
            else {
                chkModelWS.chkListStatus = tNotCompleted;
            }

            chkModelWS.chkCore = -1;

            [manager insertCheckList:chkModelWS];
        }
    }
}

-(NSString *) getTextForEditButton{
    return [[LanguageManagerV2 sharedLanguageManager] getEdit];
}

-(NSString *) getTextForEditingButton{
    return [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_DONE];
}


-(void) backToHomeViewFromCheckListView
{
    [self setSelectedButton:tagOfHomeButton];
    [self.tabBarController setSelectedIndex:tagOfHomeButton - 1];
}

-(void) handleNotificationSelectedHome{
    [self setSelectedButton:tagOfHomeButton];
    //NSLog(@"handle Notification Selected Home");
}

-(void) setSelectedCurrentView:(NSNotification *) notification
{
    NSString *condition = (NSString *)[notification object];
    //    NSLog(@"condition: %@", condition);
    if ([condition isEqualToString:[NSString stringWithFormat:@"%@", notificationSaveGuestInfo]]) {
        //        NSLog(@"guestInfo");
        self.currentSelectedView = [NSString stringWithFormat:@"%@", notificationSaveGuestInfo];
    }
    if ([condition isEqualToString:[NSString stringWithFormat:@"%@", notificationSaveRoomDetail]]) {
        //        NSLog(@"roomInfo");
        self.currentSelectedView = [NSString stringWithFormat:@"%@", notificationSaveRoomDetail];
    }
    if ([condition isEqualToString:[NSString stringWithFormat:@"%@", notificationSaveCountChareable]]) {
        //        NSLog(@"notification Save Count Chareable");
        self.currentSelectedView = [NSString stringWithFormat:@"%@", notificationSaveCountChareable];
    }
    if ([condition isEqualToString:[NSString stringWithFormat:@"%@", notificationSaveCountNonChareable]]) {
        //        NSLog(@"notification Save Count Non Chareable");
        self.currentSelectedView = [NSString stringWithFormat:@"%@", notificationSaveCountNonChareable];
    }

    if ([condition isEqualToString:[NSString stringWithFormat:@"%@", notificationSaveCleaningStatus]]) {
        //        NSLog(@"notification Save Count Non Chareable");
        self.currentSelectedView = [NSString stringWithFormat:@"%@", notificationSaveCleaningStatus];
    }

    if ([condition isEqualToString:[NSString stringWithFormat:@"%@", notificationSaveLaundry]]) {
        //        NSLog(@"notification Save Count Non Chareable");
        self.currentSelectedView = [NSString stringWithFormat:@"%@", notificationSaveLaundry];
    }

    if ([condition isEqualToString:[NSString stringWithFormat:@"%@", notificationSaveSetting]]) {
        //        NSLog(@"notification Save Count Non Chareable");
        self.currentSelectedView = [NSString stringWithFormat:@"%@", notificationSaveSetting];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
#if DEFAULT_ENABLE_PTT
    NotifReg(self, @selector(exitPtt), @"EXIT_PTT");
#endif
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        //Clear redundant view for header, footter
        CGRect frameHeaderFooter = roomTableView.tableHeaderView.frame;
        frameHeaderFooter.size.height = 0.1f;
        UIView *headerView = [[UIView alloc] initWithFrame:frameHeaderFooter];
        [roomTableView setTableHeaderView:headerView];

        frameHeaderFooter = roomTableView.tableFooterView.frame;
        frameHeaderFooter.size.height = 0.1f;
        UIView *footerView = [[UIView alloc] initWithFrame:frameHeaderFooter];
        [roomTableView setTableFooterView:footerView];
    }

    //Resize content views of current VC to fit with Navigation bar and tabbar
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }

    isCenterTitle = NO;
    isShowingAlertForceLogout = NO;
    countInThread = 0;
    numberOfTagButton = 0;

    [self hideComboboxView];
    [self resetComboboxes];
    [self.roomTableView setBackgroundView:nil];
    [self.roomTableView setBackgroundColor:[UIColor clearColor]];
    [self moveDropboxViewDown];
    [self loadAccessRights];
    [self.hotelLogo setBackgroundColor:[UIColor clearColor]];

    //set default language
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
    dispatch_async(queue, ^{
        [[LanguageManagerV2 sharedLanguageManager] setCurrentLanguage:[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage]];
    });
    //    [[LanguageManagerV2 sharedLanguageManager] setCurrentLanguage:[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage]];
    loginView = [[LoginViewV2 alloc] initWithNibName:@"LoginViewV2" bundle:nil];
    [loginView setDelegate:self];
    //Catch Frame
    //    CGRect frame = [UIScreen mainScreen].bounds;//loginView.view.frame;
    //    int deviceKind = [DeviceManager getDeviceScreenKind];
    //    if(deviceKind == DeviceScreenKindRetina4_0)
    //    {
    //        frame.size.height = DeviceScreenSizeStandardHeight4_0;
    //    }
    //    else
    //    {
    //        frame.size.height = DeviceScreenSizeStandardHeight3_5;
    //    }
    //    frame.origin.y = 0;
    //    loginView.view.frame = frame;
    //    int deviceKind = [DeviceManager getDeviceScreenKind];
    //    if(deviceKind == DeviceScreenKindRetina3_5)
    //    {
    //        CGRect frame = CGRectMake(0,0, [UIScreen mainScreen].bounds.size.width,DeviceScreenSizeStandardHeight3_5);
    //        loginView.view.frame = frame;
    //    }
    [loginView.view setFrame:self.tabBarController.view.bounds];
    [self.tabBarController.view addSubview:loginView.view];
    //set captions for loginView
    [loginView setCaptionsView];

    //Set restriction Table
    isRestriction = NO;

    listOtherActivityDetailDisplay = [[NSMutableArray alloc] init];

    //Remove Button Tabbar For PRD 2.2 Release 2
    //[self createScrollViewTabBar];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reLoadDataSyncDone:) name:[NSString stringWithFormat:@"%@", notificationReloadDataAllViewWhenSyncDone] object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hideWifiView) name:[NSString stringWithFormat:@"%@", notificationHideWifiView] object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showWifiView) name:[NSString stringWithFormat:@"%@", notificationShowWifiView] object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setCaptionsView) name:@"reloadCaptionHomeView" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(removeLoginViewV2:) name:@"removeLoginViewV2" object:nil];
    //Catch notification selectedHome to change selected button in tabbar
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleNotificationSelectedHome) name:[NSString stringWithFormat:@"%@", notificationSelectedHome] object:nil];
    //Catch notification setSelected GuestInfo or RoomInfo
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setSelectedCurrentView:) name:[NSString stringWithFormat:@"%@", notificationSetSelectedCurrentView] object:nil];
    //Catch notification Announce New Message
    //    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(announceNewMessage:) name:[NSString stringWithFormat:@"%@", notificationAnnounceNewMessage] object:nil];
    //Catch notification change language for tabbar
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeCaptionsTabbar) name:[NSString stringWithFormat:@"%@", notificationChangeLanguageTabbar] object:nil];

    roomTableView.backgroundColor = [UIColor clearColor];
    [self addButtonHandleShowHideTopbar];

    // set announce message period  60 second -> 1 minute
    AnnounceMessagePeriod = 1;

    //Alloc and start thread sync room assignment
    //    syncThread = [[NSThread alloc] initWithTarget:self selector:@selector(syncMethod) object:nil];
    //    [syncThread start];

    [NSThread detachNewThreadSelector:@selector(syncMethod) toTarget:self withObject:nil];

    //init variable roomCompletedView
    //self.roomCompletedView = [[RoomCompletedView alloc] initWithNibName:@"RoomCompletedView" bundle:nil];
    //init variable syncData, it contain sync data when tableview is editing
    self.syncData = [NSMutableArray array];

    self.isOpeningPopupNotification = NO;  // set default  did not show popup.
    //init variable isLoginShowwing
    self.isLoginShowwing = YES;

    if (wifiViewController == nil) {
        wifiViewController = [[WifiIConViewController alloc] init];
    }
    [self.tabBarController.view addSubview:wifiViewController.view];

    ///add long gesture in uitableview
    UILongPressGestureRecognizer *longpressGesture=[[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(responseLongPressButtonForChangeCleaningStatus:)];
    longpressGesture.delegate=self;
    longpressGesture.minimumPressDuration = 1.3f;
    [roomTableView addGestureRecognizer:longpressGesture];

    //set singleton home view
    me = self;
    // Show current number of unrread message

    isSynCompletedDialogShowing = NO;
    isSynInCompletedDialogShowing = NO;

    //Set hidden
    //[labelRoom setHidden:YES];
    //[labelRMStatus setHidden:YES];
    //[labelVIP setHidden:YES];

    _heightConstrainBtnComplete.constant = 0.0f;
    [butViewCleanedRooms setHidden:YES];
    [labelCleaningStatus setHidden:YES];

    //Remove Button Tabbar For PRD 2.2 Release 2
    //Set image for tabBarScrollView
    /*
     UIImageView *imgViewTabbar = nil;

     if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
     imgViewTabbar = [[UIImageView alloc] initWithImage:[UIImage imageWithColor:[UIColor colorWithRed:99/255.0f green:202/255.0f blue:206/255.0f alpha:1.0f]]];
     } else {
     imgViewTabbar = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imgBottom]];
     }

     imgViewTabbar.tag = tagTabbarBG;
     CGRect bgTabbarFrame = imgViewTabbar.frame;
     bgTabbarFrame.size = CGSizeMake(self.tabBarController.view.frame.size.width, HEIGHT_TAB_BAR);
     [imgViewTabbar setFrame:bgTabbarFrame];
     [self.tabBarController.tabBar insertSubview:imgViewTabbar aboveSubview:tabBarScrollView];
     [self hideTabBar:self.tabBarController];
     */
    //Hao Tran - End remove button tabbar for PRD 2.2 Release 2
    [[NSNotificationCenter defaultCenter] postNotificationName:kReachabilityUpdateNotification object:nil];
}

//-(void)loadLanguage
//{
//    // Check local database
//    LanguageManagerV2 *manager = [[LanguageManagerV2 alloc]init];
//    NSMutableArray *arrayLang = [manager loadAllLanguageModel];
//
//    // if not exist on db
////    if([arrayLang count] == 0)
////    {
////        // Get ws, Insert language to db
////        arrayLang = [LanguageManagerV2 getLanguageListFromWS];
////        //        NSLog(@"array language list:%d",[arrayLang count]);
////
////
////    }
//}

//@ reload data after sync done.
-(void)reLoadDataSyncDone:(NSNotification *)notificate
{
    [[HomeViewV2 shareHomeView] waitingLoadingData];
    //reload roomAssignment Data
    //RoomAssignmentEngine *roomAssignmentEngine = [[RoomAssignmentEngine alloc] init];
    //SuperRoomModel *modelSuper =[roomAssignmentEngine loadInfoRoomAssignment];
    [[RoomManagerV2 sharedRoomManager] getRoomAssignments];
    //self.roomAssignmentData = [[RoomManagerV2 sharedRoomManager] roomAssignmentList];
    [self getOtherActivityAssignment];
    [self loadRoomAssignmentData];
    //self.roomAssignmentData = modelSuper.roomAssignmentList;
    //[roomAssignmentEngine release];
    [self.roomTableView reloadData];

    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
}
//- (void)viewDidUnload
//{
//    [roomTableView release];
//    roomTableView = nil;
//    [tvCell release];
//    tvCell = nil;
//    self.roomAssignmentData = nil;
//    [butViewCleanedRooms release];
//    butViewCleanedRooms = nil;
//    [labelHousekeeperName release];
//    labelHousekeeperName = nil;
//    //[tabBarScrollView release];
//    tabBarScrollView = nil;
//    [self.roomInfoView release];
//    self.roomInfoView = nil;
//    [threadSyncWithPeriod release];
//    threadSyncWithPeriod = nil;
//    [roomCompletedView release];
//    self.roomCompletedView = nil;
//    [syncData release];
//    self.syncData = nil;
//    [self setHotelLogo:nil];
//    [labelRoom release];
//    labelRoom = nil;
//    [labelRMStatus release];
//    labelRMStatus = nil;
//    [labelVIP release];
//    labelVIP = nil;
//    [labelCleaningStatus release];
//    labelCleaningStatus = nil;
//    [labelHousekeeper release];
//    labelHousekeeper = nil;
//    [labelGuestCell release];
//    labelGuestCell = nil;
//    [buildingNameLabel release];
//    buildingNameLabel = nil;
//    [nameUserLabel release];
//    nameUserLabel = nil;
//    [super viewDidUnload];
//    // Release any retained subviews of the main view.
//    // e.g. self.myOutlet = nil;
//    //NSLog(@"viewdidunload");
//}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
    //    return NO;
}

#pragma mark - Hidden Header view when tap on navigationBar

- (void) hiddenHeaderView {

    [self populateDataIntoNavigationbar];

    CGRect frame1 = CGRectZero;
    CGRect frame2 = CGRectZero;
    CGRect frame3 = CGRectZero;
    CGRect frame4 = CGRectZero;
    CGRect frame5 = CGRectZero;

    BOOL isHidden = (topBarView.alpha == 0.0f);

    if(!isHidden){
        frame1  = CGRectMake(0, roomTableView.frame.origin.y - heightScale, roomTableView.frame.size.width, roomTableView.frame.size.height + heightScale);
        frame2 = CGRectMake(labelRoom.frame.origin.x, labelRoom.frame.origin.y - heightScale, labelRoom.frame.size.width, labelRoom.frame.size.height );
        frame3 = CGRectMake(labelRMStatus.frame.origin.x, labelRMStatus.frame.origin.y - heightScale, labelRMStatus.frame.size.width, labelRMStatus.frame.size.height );
        frame4 = CGRectMake(labelVIP.frame.origin.x, labelVIP.frame.origin.y - heightScale, labelVIP.frame.size.width, labelVIP.frame.size.height );
        frame5 = CGRectMake(labelCleaningStatus.frame.origin.x, labelCleaningStatus.frame.origin.y - heightScale, labelCleaningStatus.frame.size.width, labelCleaningStatus.frame.size.height);
        [self moveDropboxViewUp];
    } else {
        frame1 = CGRectMake(0, roomTableView.frame.origin.y + heightScale, roomTableView.frame.size.width, roomTableView.frame.size.height - heightScale);
        frame2 = CGRectMake(labelRoom.frame.origin.x, labelRoom.frame.origin.y + heightScale, labelRoom.frame.size.width, labelRoom.frame.size.height);
        frame3 = CGRectMake(labelRMStatus.frame.origin.x, labelRMStatus.frame.origin.y + heightScale, labelRMStatus.frame.size.width, labelRMStatus.frame.size.height);
        frame4 = CGRectMake(labelVIP.frame.origin.x, labelVIP.frame.origin.y + heightScale, labelVIP.frame.size.width, labelVIP.frame.size.height );
        frame5 = CGRectMake(labelCleaningStatus.frame.origin.x, labelCleaningStatus.frame.origin.y + heightScale, labelCleaningStatus.frame.size.width, labelCleaningStatus.frame.size.height );
        [self moveDropboxViewDown];
    }

    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelegate:self];

    [roomTableView setFrame:frame1];
    [labelRoom setFrame:frame2];
    [labelRMStatus setFrame:frame3];
    [labelVIP setFrame:frame4];
    [labelCleaningStatus setFrame:frame5];

    CGRect f = topBarView.frame;
    if(topBarView.alpha == 0.0f)
    {
        f.size.height = 60;
        [topBarView setFrame:f];
        [topBarView setAlpha:1.0f];
    }
    else
    {
        f.size.height = 0;
        [topBarView setFrame:f];
        [topBarView setAlpha:0.0f];
    }

    [UIView commitAnimations];

}

#pragma mark -
#pragma mark NavigationBarDelegate Methods
-(void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated{
    //NSLog(@"navigationcontroller willshowviewcontroller");
}

-(void) navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated{

}

#pragma mark -
#pragma mark TableViewDelegate Methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {

    //if(![UserManagerV2 isSupervisor]){
    if([self.roomAssignmentData count] > 0) {
        NSObject *object = [self.roomAssignmentData objectAtIndex:indexPath.row];
        if ([object isKindOfClass:[OtherActivityDetailsDisplay class]]) {
            return 50;
        } else {
            NSDictionary *rowData = (NSDictionary*)object;
            NSString *roomNo = (NSString *)[rowData objectForKey:kRoomNo];

            if (roomNo.length > 0) {
                RoomBlocking *curRoomBlocking = [[RoomManagerV2 sharedRoomManager] loadRoomBlockingByUserId:[UserManagerV2 sharedUserManager].currentUser.userId roomNumber:roomNo];
                NSUInteger row = [indexPath row];
                NSDictionary *rowData =(NSDictionary*)[self.roomAssignmentData objectAtIndex:row];
                NSString *guestArrivalTime = (NSString *)[rowData objectForKey:kRaGuestArrivalTime];
                NSString *guestDepartureTime = (NSString *)[rowData objectForKey:kRaGuestDepartureTime];
                NSString *guestName = [rowData objectForKey:kGuestName];
                int roomIndicatorGuest = [self getRoomIndicatorGuestByArrivalDate:guestArrivalTime departureDate:guestDepartureTime];
                isDueRoom = (roomIndicatorGuest == RoomIndicatorGuestDueIn) || (roomIndicatorGuest == RoomIndicatorGuestDueOut) || (roomIndicatorGuest == RoomIndicatorGuestBackToBack);
                [LogFileManager logDebugMode:@"isdue = %@", isDueRoom ? @"True" : @"False"];

                BOOL isHaveReasonRemark = (curRoomBlocking.roomblocking_reason.length > 0 || curRoomBlocking.roomblocking_oosdurations.length > 0) || (curRoomBlocking.roomblocking_remark_physical_check.length > 0);

                if (curRoomBlocking != nil
                    && curRoomBlocking.roomblocking_is_blocked > RoomBlockingStatus_Normal
                    && isHaveReasonRemark
                    && (isDueRoom || guestName.length > 0)) {

                    //Extend the height when room was blocked
                    return 133;
                } else if (curRoomBlocking != nil
                           && curRoomBlocking.roomblocking_is_blocked > RoomBlockingStatus_Normal
                           && isHaveReasonRemark
                           && !isDueRoom && guestName.length == 0) {

                    return 107;
                }
            }
        }
    }
    return 65;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    //NSLog(@"num section");
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{


    //check if no room assigned
    if ([roomAssignmentData count] <= 0) {
        return 1;
    }
    return [roomAssignmentData count];
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return @"";
}


- (void)addAccessoryButton:(UITableViewCell *)cell  {
    /*****************************************/
    UIImageView *accessoryButton = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 24, 24)];
    [accessoryButton setBackgroundColor:[UIColor clearColor]];
    //    [accessoryButton setImage:[UIImage imageNamed:@"gray_arrow_36x36.png"] forState:UIControlStateNormal];
    [accessoryButton setImage:[UIImage imageNamed:imgGrayArrow]];
    [cell setAccessoryView:accessoryButton];
    //    cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
    /*****************************************/

}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    if ([roomAssignmentData count] <= 0) {
        [labelRoom setHidden:YES];
        [labelRMStatus setHidden:YES];
        [labelVIP setHidden:YES];
        [labelCleaningStatus setHidden:YES];
        static NSString *sectionNoRoomAssigned = @"NoRoomAssigned";
        UITableViewCell *cellno = nil;
        cellno = [tableView dequeueReusableCellWithIdentifier:sectionNoRoomAssigned];
        if (cellno == nil) {
            cellno = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:sectionNoRoomAssigned] ;
            [cellno.textLabel setTextAlignment:NSTextAlignmentCenter];
            [cellno setSelectionStyle:UITableViewCellSelectionStyleNone];
        }
        cellno.textLabel.text = [[LanguageManagerV2 sharedLanguageManager] getNoRoomAssigned];
        [cellno.textLabel setTextColor:[UIColor grayColor]];
        [cellno setBackgroundColor:[UIColor clearColor]];
        [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        return cellno;
    }
    [labelRoom setHidden:NO];
    [labelRMStatus setHidden:NO];
    [labelVIP setHidden:NO];
    [labelCleaningStatus setHidden:NO];
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    static NSString *sectionIdentifier = @"RoomAssignmentIdentifier";
    UITableViewCell *cell;

    NSObject *object = [self.roomAssignmentData objectAtIndex:indexPath.row];
    if ([object isKindOfClass:[OtherActivityDetailsDisplay class]]) {

        OtherActivityDetailsDisplay *details = (OtherActivityDetailsDisplay*)object;

        OtherActivityCell *otherActivityCell = (OtherActivityCell *)[tableView dequeueReusableCellWithIdentifier:@"OtherActivityIdentifier"];
        if (otherActivityCell == nil) {
            NSArray *nibCell = [[NSBundle mainBundle] loadNibNamed:@"OtherActivityCell" owner:self options:nil];

            if ([nibCell count] > 0) {
                otherActivityCell = [nibCell objectAtIndex:0];
            }
        }
        [otherActivityCell setBackgroundColor:[UIColor colorWithRed:254/255.0f green:186/255.0f blue:8/255.0f alpha:1]];
        [otherActivityCell setAccessoryType:UITableViewCellAccessoryNone];
        otherActivityCell.lblActivityName.textColor = [UIColor colorWithRed:9/255.0f green:46/255.0f blue:122/255.0f alpha:1];
        otherActivityCell.lblActivityName.text = details.otherActivityString;
        return otherActivityCell;
    } else {

        cell = [tableView dequeueReusableCellWithIdentifier:sectionIdentifier];


        if (cell == nil) {

            NSArray *nibCell = [[NSBundle mainBundle] loadNibNamed:@"RoomAssignmentCellV2" owner:self options:nil];

            if ([nibCell count] > 0) {
                cell = [nibCell objectAtIndex:0];

                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            } else {
                if([LogFileManager isLogConsole])
                {
                    NSLog(@"failed to load RoomAssignmentCell nib file!");
                }
            }
        }

        if(!isDemoMode){
            //Hao Tran[20130513/Disable Touch Cell] - Disable touch cell if QR Code enabled
            if(![qrScanButton isHidden]
               && [qrScanButton isEnabled])
            {
                [cell setUserInteractionEnabled:NO];
            }
            //Hao Tran[20130513/Disable Touch Cell] - END

            if(!ENABLE_QRCODESCANNER){
                if(ENABLE_CHECK_ACCESS_RIGHT_QRCODESCANNER) {
                    if (QRCodeScanner.isActive) {
                        [cell setUserInteractionEnabled:NO];
                    } else {
                        [cell setUserInteractionEnabled:YES];
                    }
                } else {
                    [cell setUserInteractionEnabled:YES];
                }
            }
        }

        NSUInteger row = [indexPath row];
        NSDictionary *rowData =(NSDictionary*)[self.roomAssignmentData objectAtIndex:row];

        UILabel *lblRoomNo = (UILabel *)[cell viewWithTag:tagRoomNo];
        UILabel *lblRoomStatus = (UILabel *)[cell viewWithTag:tagRMStatus];
        UILabel *lblVipRoom = (UILabel *)[cell viewWithTag:tagVIP];
        UIImageView *imgFirstIconCleaningStatus = (UIImageView *)[cell viewWithTag:tagFirstIconCleaningStatus];
        UIImageView *imgSecondIconCleaningStatus = (UIImageView *)[cell viewWithTag:tagSecondIconCleaningStatus];
        UILabel *lblServiceLaterTime = (UILabel *)[cell viewWithTag:tagTimeServiceLater];
        UIImageView *imgRushOrQueueRoom = (UIImageView *)[cell viewWithTag:tagRushOrQueueRoom];
        UILabel *lblDueOutRoom = (UILabel *)[cell viewWithTag:tagDueOutRoom];
        UILabel *lblDepartureTime = (UILabel *)[cell viewWithTag:tagDepartureTime];
        UILabel *lblDueInRoom = (UILabel *)[cell viewWithTag:tagDueInRoom];
        UILabel *lblArrivalTime = (UILabel *)[cell viewWithTag:tagArrivalTime];
        UILabel *lblRoomType = (UILabel *)[cell viewWithTag:tagRoomType];
        UIImageView *imgProfileNotes = (UIImageView *)[cell viewWithTag:tagProfileNotes]; // CFG [20160928/CRF-00000827]
        UIImageView *imgStayOver = (UIImageView *)[cell viewWithTag:tagStayOver]; // CRF [20161106/CRF-00001293]
        //setting marqueelabel
        MarqueeLabel *lblGuestName = (MarqueeLabel *)[cell viewWithTag:tagGuestName];
        lblGuestName.textColor = [UIColor lightGrayColor];
        lblGuestName.marqueeType = MLContinuous;
        lblGuestName.continuousMarqueeExtraBuffer = 50.0f;
        lblGuestName.animationDelay = 0;
        lblGuestName.backgroundColor = [UIColor clearColor];
        [lblGuestName restartLabel];

        UILabel *lblRAName = (UILabel *)[cell viewWithTag:tagRAName];
        NSString *roomNo = (NSString *)[rowData objectForKey:kRoomNo];
        NSString *guestName = [rowData objectForKey:kGuestName];
        NSString *vip = (NSString *)[rowData objectForKey:kVIP];
        NSInteger cleaningStatus = [(NSString *)[rowData objectForKey:kCleaningStatusID] integerValue];
        NSString *roomStatus = (NSString *)[rowData objectForKey:kRMStatus];
        NSString *raFirstName = (NSString *)[rowData objectForKey:kRAFirstName];
        NSString *raLastName = (NSString *)[rowData objectForKey:kRALastName];
        //    NSString *guestFirstName = (NSString *)[rowData objectForKey:kGuestFirstName];
        //    NSString *guestLastName = (NSString *)[rowData objectForKey:kGuestLastName];
        NSString *guestArrivalTime = (NSString *)[rowData objectForKey:kRaGuestArrivalTime];
        NSString *guestDepartureTime = (NSString *)[rowData objectForKey:kRaGuestDepartureTime];
        NSInteger kindOfRoom = [(NSString *)[rowData objectForKey:kRaKindOfRoom] integerValue];
        NSInteger isMockRoom = [(NSString *)[rowData objectForKey:kRaIsMockRoom] integerValue];
        NSInteger isReassignedRoom = [(NSString *)[rowData objectForKey:kRaIsReassignedRoom] integerValue];
        NSData *cleaningStatusIcon = (NSData *)[rowData objectForKey:kCleaningStatus];
        NSString *roomAssignmentTime = (NSString *)[rowData objectForKey:kRoomAssignmentTime];
        NSString *roomType = (NSString *)[rowData objectForKey:kRoomType];

        [lblRoomType setText:roomType];
        [lblRoomNo setText:roomNo];
        [lblRoomStatus setText:roomStatus];
        [lblVipRoom setText:(vip == nil || [vip isEqualToString:@""] || [vip isEqualToString:@"0"])?@"-":vip];

        [lblDueOutRoom setText:@"[D/O]"];
        [lblArrivalTime setText:guestArrivalTime];
        [lblGuestName setText:(guestName == nil || [guestName isEqualToString:@""])?@"-":guestName];

        //20150108 - Show icon previous day
        UIImageView *imgPreviousDate = (UIImageView *)[cell viewWithTag:tagPreviousDate];
        int roomStatusID = [[rowData objectForKey:kRoomStatusId] intValue];
        if (roomStatusID == ENUM_ROOM_STATUS_OC || roomStatusID == ENUM_ROOM_STATUS_VC || roomStatusID == ENUM_ROOM_STATUS_OPU) {
            if ([[rowData objectForKey:kLastCleaningDate] length] > 0) {
                NSDateFormatter *dFormat = [[NSDateFormatter alloc] init];
                [dFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                //[dFormat setLocale:[NSLocale currentLocale]];
                //[dFormat setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];

                if([[rowData objectForKey:kLastCleaningDate] isEqualToString:@"1900-01-01 00:00:00"]) {

                    imgPreviousDate.hidden = YES;

                } else if ([DateTimeUtility compareDatetimeValue:[rowData objectForKey:kLastCleaningDate]] == DateComparison_LessThanCurrent) {
                    imgPreviousDate.hidden = NO;
                    [imgPreviousDate setFrame:CGRectMake(self.view.bounds.size.width - 30,
                                                         25,
                                                         imgPreviousDate.frame.size.width,
                                                         imgPreviousDate.frame.size.height)];
                } else {
                    imgPreviousDate.hidden = YES;
                }
            } else if([roomStatus isEqualToString:ROOM_STATUS_VD] || [roomStatus isEqualToString:ROOM_STATUS_OD]){
                imgPreviousDate.hidden = YES;
            }
            else {
                imgPreviousDate.hidden = YES;
            }
        } else {
            imgPreviousDate.hidden = YES;
        }
        //    if ([roomStatus integerValue] == ENUM_ROOM_STATUS_VC || [roomStatus integerValue] == ENUM_ROOM_STATUS_VD
        //        || [roomStatus integerValue] == ENUM_ROOM_STATUS_VI || [roomStatus integerValue] == ENUM_ROOM_STATUS_VPU
        //        || [roomStatus integerValue] == ENUM_ROOM_STATUS_VCB) {
        //        [lblDueOutRoom setText:@"[D/O]"];
        //        [lblArrivalTime setText:guestArrivalTime];
        //        [lblGuestName setText:(guestName == nil || [guestName isEqualToString:@""])?@"-":guestName];
        //    }
        //    else {
        //        [lblGuestName setText:(guestName == nil || [guestName isEqualToString:@""])?@"-":guestName];
        //    }
        //[CRF-994/20150108] - show rush/queue room on view
        //    if (kindOfRoom == ENUM_KIND_OF_ROOM_NORMAL || [UserManagerV2 isSupervisor]) {
        //        [imgRushOrQueueRoom setHidden:YES];
        //    }
        // CFG [20160913/CRF-00001439] - Changed to binary AND operation.
        if ((kindOfRoom & ENUM_KIND_OF_ROOM_RUSH) == ENUM_KIND_OF_ROOM_RUSH) {
            [imgRushOrQueueRoom setHidden:NO];
            if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                [imgRushOrQueueRoom setImage:[UIImage imageNamed:imgRushRoomFlat]];
            } else {
                [imgRushOrQueueRoom setImage:[UIImage imageNamed:imgRushRoom]];
            }
            if ([UserManagerV2 isSupervisor]) {
                [lblRAName setFrame:CGRectMake(lblRAName.frame.origin.x,
                                               lblRAName.frame.origin.y,
                                               98.0,
                                               lblRAName.frame.size.height)];
                [imgRushOrQueueRoom setFrame:CGRectMake(295.0,
                                                        imgRushOrQueueRoom.frame.origin.y,
                                                        imgRushOrQueueRoom.frame.size.width,
                                                        imgRushOrQueueRoom.frame.size.height)];
            }
        }
        else if ((kindOfRoom & ENUM_KIND_OF_ROOM_QUEUE) == ENUM_KIND_OF_ROOM_QUEUE) {
            [imgRushOrQueueRoom setHidden:NO];
            if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                [imgRushOrQueueRoom setImage:[UIImage imageNamed:imgQueueRoomFlat]];
            } else {
                [imgRushOrQueueRoom setImage:[UIImage imageNamed:imgQueueRoom]];
            }
            if ([UserManagerV2 isSupervisor]) {
                [lblRAName setFrame:CGRectMake(lblRAName.frame.origin.x,
                                               lblRAName.frame.origin.y,
                                               98.0,
                                               lblRAName.frame.size.height)];
                [imgRushOrQueueRoom setFrame:CGRectMake(295.0,
                                                        imgRushOrQueueRoom.frame.origin.y,
                                                        imgRushOrQueueRoom.frame.size.width,
                                                        imgRushOrQueueRoom.frame.size.height)];
            }
        }
        else {
            [imgRushOrQueueRoom setHidden:YES];
        }
        // CFG [20160913/CRF-00001439] - End.

        // CFG [20160928/CRF-00000827] - Show notes icon.
        if ((kindOfRoom & ENUM_KIND_OF_ROOM_PROFILE_NOTES_EXISTS) == ENUM_KIND_OF_ROOM_PROFILE_NOTES_EXISTS) {
            [imgProfileNotes setHidden:NO];
        }
        else {
            [imgProfileNotes setHidden:YES];
        }
        // CFG [20160928/CRF-00000827] - End.

        // CRF [20161106/CRF-00001293] - Show notes icon.
        if ((kindOfRoom & ENUM_KIND_OF_ROOM_STAY_OVER_EXISTS) == ENUM_KIND_OF_ROOM_STAY_OVER_EXISTS){
            [imgStayOver setHidden:NO];
            if(imgProfileNotes.isHidden == NO) {
                [imgStayOver setFrame:CGRectMake(imgProfileNotes.frame.origin.x - imgProfileNotes.frame.size.width, imgProfileNotes.frame.origin.y, imgProfileNotes.frame.size.width, imgProfileNotes.frame.size.height)];
            } else {
                [imgStayOver setFrame:imgProfileNotes.frame];
            }
        }
        else {
            [imgStayOver setHidden:YES];
        }
        // CRF [20161106/CRF-00001293] - End.
        //Change request for PRD 2.2R2
        //Checking Room Indicator Guest
        //- Due Out: departure = now & arrival time != now
        //- Due In: departure != now & arrival = now
        //- Back To Back: departure = now & arrival = now

        int roomIndicatorGuest = [self getRoomIndicatorGuestByArrivalDate:guestArrivalTime departureDate:guestDepartureTime];

        if(roomIndicatorGuest == RoomIndicatorGuestDueIn) { //light yellow green color
            [cell setBackgroundColor:[UIColor colorWithRed:253/255.0f green:255/255.0f blue:206/255.0f alpha:1]];

            [lblDueInRoom setHidden:YES];
            [lblArrivalTime setHidden:YES];

            //Warning! Temporary use label Due Out and label departure time for Arrival time
            //[lblDueOutRoom setFrame:lblDueOutFrame];//Hao Tran - Remove for uneccessary frame for cell in HomeViewV2

            //Change frames for Due Out Infor
            //Decreasing width of Guest Infor
            if(lblDepartureTime.isHidden /*&& lblDueOutRoom.isHidden*/) {
                CGRect frlblDueOut = lblDueOutRoom.frame;
                CGRect frlblDepartureTime = lblDepartureTime.frame;
                CGRect frlblGuestName = lblGuestName.frame;
                frlblGuestName.size.width -= (frlblDueOut.size.width + frlblDepartureTime.size.width + 2);
                frlblGuestName.origin.x += (frlblDueOut.size.width + frlblDepartureTime.size.width + 2);
                [lblGuestName setFrame:frlblGuestName];
                [lblDueOutRoom setHidden:NO];
                [lblDepartureTime setHidden:NO];
            }

            [lblDueOutRoom setText:@"[D/I]"];
            [lblDepartureTime setText:[ehkConvert DateToStringWithString:guestArrivalTime fromFormat:@"yyyy-MM-dd HH:mm:ss" toFormat:@"HH:mm"]];
            //[lblGuestName setFrame:lblGuestNameFrameInCase3];
            [lblGuestName setHidden:YES];

        } else if(roomIndicatorGuest == RoomIndicatorGuestDueOut) { //light green color
            [cell setBackgroundColor:[UIColor colorWithRed:172/255.0f green:255/255.0f blue:203/255.0f alpha:1]];

            if(guestName.length > 0) {
                [lblGuestName setHidden:NO];
                [lblGuestName setText:guestName];
            } else {
                //No need to check guest for displaying green background - matching with Android
                //[cell setBackgroundColor:[UIColor whiteColor]];
                [lblGuestName setHidden:YES];
            }

            [lblDueInRoom setHidden:YES];
            [lblArrivalTime setHidden:YES];

            //[lblDueOutRoom setFrame:lblDueOutFrame];//Hao Tran - Remove for uneccessary frame for cell in HomeViewV2

            //Change frames for Due Out Infor
            //Decreasing width of Guest Infor
            if(lblDepartureTime.isHidden /*&& lblDueOutRoom.isHidden*/) {
                CGRect frlblDueOut = lblDueOutRoom.frame;
                CGRect frlblDepartureTime = lblDepartureTime.frame;
                CGRect frlblGuestName = lblGuestName.frame;
                frlblGuestName.size.width -= (frlblDueOut.size.width + frlblDepartureTime.size.width + 2);
                frlblGuestName.origin.x += (frlblDueOut.size.width + frlblDepartureTime.size.width + 2);
                [lblGuestName setFrame:frlblGuestName];
                [lblDueOutRoom setHidden:NO];
                [lblDepartureTime setHidden:NO];
            }

            [lblDueOutRoom setText:@"[D/O]"];
            [lblDepartureTime setText:[ehkConvert DateToStringWithString:guestDepartureTime fromFormat:@"yyyy-MM-dd HH:mm:ss" toFormat:@"HH:mm"]];
            //[lblGuestName setFrame:lblGuestNameFrameInCase3];

        } else if(roomIndicatorGuest == RoomIndicatorGuestBackToBack) { //light green color
            [cell setBackgroundColor:[UIColor colorWithRed:172/255.0f green:255/255.0f blue:203/255.0f alpha:1]];

            //Change frames for Due Out Infor
            //Decreasing width of Guest Infor
            if(lblDepartureTime.isHidden /*&& lblDueOutRoom.isHidden*/) {
                CGRect frlblDueOut = lblDueOutRoom.frame;
                CGRect frlblDepartureTime = lblDepartureTime.frame;
                CGRect frlblGuestName = lblGuestName.frame;
                frlblGuestName.size.width -= (frlblDueOut.size.width + frlblDepartureTime.size.width + 2);
                frlblGuestName.origin.x += (frlblDueOut.size.width + frlblDepartureTime.size.width + 2);
                [lblGuestName setFrame:frlblGuestName];
                [lblDueOutRoom setHidden:NO];
                [lblDepartureTime setHidden:NO];
            }

            [lblDueOutRoom setText:@"[D/O]"];
            [lblDepartureTime setText:[ehkConvert DateToStringWithString:guestDepartureTime fromFormat:@"yyyy-MM-dd HH:mm:ss" toFormat:@"HH:mm"]];
            [lblDueInRoom setHidden:NO];
            [lblDueInRoom setText:@"[D/I]"];
            [lblArrivalTime setHidden:NO];
            [lblArrivalTime setText:[ehkConvert DateToStringWithString:guestArrivalTime fromFormat:@"yyyy-MM-dd HH:mm:ss" toFormat:@"HH:mm"]];

            //[lblGuestName setFrame:lblGuestNameFrameInCase3];
            if(guestName.length > 0) {
                [lblGuestName setHidden:NO];
                [lblGuestName setText:guestName];
            } else {
                [lblGuestName setHidden:YES];
            }

        } else { //Normal
            [cell setBackgroundColor:[UIColor whiteColor]];

            //Change frames for Due Out Infor
            //Increasing width of Guest Infor
            if(!lblDepartureTime.isHidden) {
                CGRect frlblDueOut = lblDueOutRoom.frame;
                CGRect frlblDepartureTime = lblDepartureTime.frame;
                CGRect frlblGuestName = lblGuestName.frame;
                frlblGuestName.size.width += (frlblDueOut.size.width + frlblDepartureTime.size.width + 2);
                frlblGuestName.origin.x -= (frlblDueOut.size.width + frlblDepartureTime.size.width + 2);
                [lblGuestName setFrame:frlblGuestName];
                [lblDepartureTime setHidden:YES];
                [lblDueOutRoom setHidden:YES];
            }

            [lblDueInRoom setHidden:YES];
            [lblArrivalTime setHidden:YES];

            if(guestName.length > 0) {
                [lblGuestName setHidden:NO];
                [lblGuestName setText:guestName];
            } else {
                [lblGuestName setHidden:YES];
                [lblDueOutRoom setText:@"-"];
            }
        }

        [cell setAccessoryType:UITableViewCellAccessoryNone];
        if ([UserManagerV2 isSupervisor]) {
            //        if(isRestriction)
            //        {
            //            [self addAccessoryButton:cell];
            //        }
            //        else {
            //            if([indexPath row] ==0)
            //                [self addAccessoryButton:cell];
            //            else {
            //                cell.accessoryType = UITableViewCellAccessoryNone;
            //                cell.accessoryView = nil;
            //            }
            //        }

            [imgFirstIconCleaningStatus setHidden:NO];
            [imgSecondIconCleaningStatus setHidden:NO];
            [lblServiceLaterTime setHidden:YES];
            [lblRAName setHidden:YES];
            [lblRAName setText:[NSString stringWithFormat:@"%@ %@", raFirstName, raLastName]];

            //Hao_Tran[20140507]-Comment to apply blocking status to Supervisor
            /*
             //Set hidden all things for OOS room
             UIImageView *blockRoomStatus = (UIImageView*)[cell.contentView viewWithTag:tagBlockingImage];
             UILabel *lblBlockReason = (UILabel*)[cell.contentView viewWithTag:tagBlockingReason];
             UILabel *lblBlockRemark = (UILabel*)[cell.contentView viewWithTag:tagBlockingRemark];

             [blockRoomStatus setHidden:YES];
             [lblBlockRemark setHidden:YES];
             [lblBlockReason setHidden:YES];
             [lblAddReason setHidden:YES];
             */
            //Hao_Tran[20140507]-END
            [imgSecondIconCleaningStatus setHidden:YES];
            [imgFirstIconCleaningStatus setImage:[UIImage imageWithData:cleaningStatusIcon]];
            if (cleaningStatus == ENUM_CLEANING_STATUS_SERVICE_LATER) {
                [lblServiceLaterTime setHidden:NO];
                [lblServiceLaterTime setFrame:lblServiceLaterTimeFrame];
                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                [dateFormat setLocale:[NSLocale currentLocale]];
                [dateFormat setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
                NSDate *assignedDate = [dateFormat dateFromString:roomAssignmentTime];
                [dateFormat setDateFormat:@"HH:mm"];
                [lblServiceLaterTime setText:[dateFormat stringFromDate:assignedDate]];
            }
            else {
                [lblServiceLaterTime setHidden:YES];
            }
        }
        else {
            //[self addAccessoryButton:cell];
            [imgFirstIconCleaningStatus setHidden:NO];
            [imgSecondIconCleaningStatus setHidden:NO];
            [lblServiceLaterTime setHidden:YES];
            [lblRAName setHidden:YES];

            if (isMockRoom == 1) {
                if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                    [imgFirstIconCleaningStatus setImage:[UIImage imageNamed:imgCompleteFlat]];
                    [imgSecondIconCleaningStatus setImage:[UIImage imageWithData:cleaningStatusIcon]];
                } else {
                    [imgFirstIconCleaningStatus setImage:[UIImage imageNamed:imgComplete]];
                    [imgSecondIconCleaningStatus setImage:[UIImage imageWithData:cleaningStatusIcon]];
                }
            }
            else if (isReassignedRoom == 1){
                if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                    [imgFirstIconCleaningStatus setImage:[UIImage imageNamed:imgPendingFlat]];
                    [imgSecondIconCleaningStatus setImage:[UIImage imageWithData:cleaningStatusIcon]];
                } else {
                    [imgFirstIconCleaningStatus setImage:[UIImage imageNamed:imgPending]];
                    //            if (cleaningStatus == ENUM_CLEANING_STATUS_PENDING_SECOND_TIME) {
                    //                [imgSecondIconCleaningStatus setImage:[UIImage imageNamed:@"icon_pending.png"]];
                    //            }
                    //            else {
                    //                [imgSecondIconCleaningStatus setImage:[UIImage imageWithData:cleaningStatusIcon]];
                    //            }
                    [imgSecondIconCleaningStatus setImage:[UIImage imageWithData:cleaningStatusIcon]];
                }
            }
            else {
                [imgSecondIconCleaningStatus setHidden:YES];
                [imgFirstIconCleaningStatus setImage:[UIImage imageWithData:cleaningStatusIcon]];
                if (cleaningStatus == ENUM_CLEANING_STATUS_SERVICE_LATER) {
                    [lblServiceLaterTime setHidden:NO];
                    [lblServiceLaterTime setFrame:lblServiceLaterTimeFrame];
                    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                    [dateFormat setLocale:[NSLocale currentLocale]];
                    [dateFormat setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
                    NSDate *assignedDate = [dateFormat dateFromString:roomAssignmentTime];
                    [dateFormat setDateFormat:@"HH:mm"];
                    [lblServiceLaterTime setText:[dateFormat stringFromDate:assignedDate]];
                }
                else {
                    [lblServiceLaterTime setHidden:YES];
                }
            }
        }

        //CRF-0000682: blocking room sign, remark and reason
        RoomBlocking *curRoomBlocking = [[RoomManagerV2 sharedRoomManager] loadRoomBlockingByUserId:[UserManagerV2 sharedUserManager].currentUser.userId roomNumber:roomNo];

        UIImageView *blockRoomStatus = (UIImageView*)[cell.contentView viewWithTag:tagBlockingImage];
        UILabel *lblBlockReason = (UILabel*)[cell.contentView viewWithTag:tagBlockingReason];
        UILabel *lblBlockRemark = (UILabel*)[cell.contentView viewWithTag:tagBlockingRemark];
        UILabel *lblAddReason = (UILabel*)[cell.contentView viewWithTag:tagAddition];
        UILabel *lblBlockDuration = (UILabel*)[cell.contentView viewWithTag:tagOOSDuration];

        BOOL isHaveReasonRemark = (curRoomBlocking.roomblocking_reason.length > 0 || curRoomBlocking.roomblocking_remark_physical_check.length > 0 || curRoomBlocking.roomblocking_oosdurations.length > 0);

        if(curRoomBlocking.roomblocking_is_blocked > 0 && !isDueRoom  && guestName.length == 0 )
        {
            if(curRoomBlocking.roomblocking_is_blocked == RoomBlockingStatus_Release) {
                [blockRoomStatus setImage:[UIImage imageNamed:imgReleaseRoom]];
            } else {
                [blockRoomStatus setImage:[UIImage imageNamed:imgBlockRoom]];
            }

            [blockRoomStatus setHidden:NO];

            if(isHaveReasonRemark){
                NSString *reason = [NSString stringWithFormat:@"%@: %@", [L_reason currentKeyToLanguage], curRoomBlocking.roomblocking_reason];
                NSString *remark = [NSString stringWithFormat:@"%@: %@",[L_remark_title currentKeyToLanguage], curRoomBlocking.roomblocking_remark_physical_check];
                NSString *blockingDuration =  [NSString stringWithFormat:@"%@: %@",[L_oos_duration currentKeyToLanguage], curRoomBlocking.roomblocking_oosdurations];

                [lblAddReason setHidden:NO];
                [lblBlockReason setHidden:NO];
                [lblBlockRemark setHidden:NO];
                [lblBlockDuration setHidden:YES];
                [lblAddReason setText:reason];
                [lblBlockReason setText:remark];
                [lblBlockRemark setText:blockingDuration];
            }
            else{
                [lblAddReason setHidden:YES];
                [lblBlockReason setHidden:YES];
                [lblBlockRemark setHidden:YES];
                [lblBlockDuration setHidden:YES];
            }
            [lblGuestName setHidden:YES];
        }
        else if (curRoomBlocking.roomblocking_is_blocked > 0 && (isDueRoom || guestName.length > 0)){

            if(curRoomBlocking.roomblocking_is_blocked == RoomBlockingStatus_Release) {
                [blockRoomStatus setImage:[UIImage imageNamed:imgReleaseRoom]];
            } else {
                [blockRoomStatus setImage:[UIImage imageNamed:imgBlockRoom]];
            }

            [blockRoomStatus setHidden:NO];

            if(isHaveReasonRemark){

                NSString *reason = [NSString stringWithFormat:@"%@: %@", [L_reason currentKeyToLanguage], curRoomBlocking.roomblocking_reason];
                NSString *remark = [NSString stringWithFormat:@"%@: %@",[L_remark_title currentKeyToLanguage], curRoomBlocking.roomblocking_remark_physical_check];
                NSString *blockingDuration =  [NSString stringWithFormat:@"%@: %@",[L_oos_duration currentKeyToLanguage], curRoomBlocking.roomblocking_oosdurations];

                [lblAddReason setHidden:YES];
                [lblBlockReason setHidden:NO];
                [lblBlockRemark setHidden:NO];
                [lblBlockDuration setHidden:NO];

                [lblBlockReason setText:reason];
                [lblBlockRemark setText:remark];
                [lblBlockDuration setText:blockingDuration];

            }
            else{
                [lblAddReason setHidden:YES];
                [lblBlockReason setHidden:YES];
                [lblBlockRemark setHidden:YES];
                [lblBlockDuration setHidden:YES];
            }
            [lblGuestName setHidden:NO];
        }
        else {
            [blockRoomStatus setHidden:YES];
            [lblBlockRemark setHidden:YES];
            [lblBlockReason setHidden:YES];
            [lblAddReason setHidden:YES];
            [lblBlockDuration setHidden:YES];
            [lblGuestName setHidden:NO];
        }

        [cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
        return cell;
    }
}




-(NSString *) getBackTextButton {
    NSDictionary *dic = [[LanguageManagerV2 sharedLanguageManager] getCurrentDictionaryLanguage];
    return [dic valueForKey:[NSString stringWithFormat:@"%@", L_BACK]];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    if (self.roomAssignmentData == nil || self.roomAssignmentData.count == 0) {
        return;
    }
    NSObject *object = [self.roomAssignmentData objectAtIndex:indexPath.row];

    if ([object isKindOfClass:[OtherActivityDetailsDisplay class]]) {

        OtherActivityViewController *viewController = [[OtherActivityViewController alloc] initWithNibName:@"OtherActivityViewController" bundle:nil];

        OtherActivityDetailsDisplay *model = (OtherActivityDetailsDisplay*)object;
        [viewController setActivityAssignmentDetail:model.otherActivityDetail];
        [viewController setOtherActivitySelected:model.otherActivityString];

        [self.navigationController pushViewController:viewController animated:YES];

        [self.navigationItem.backBarButtonItem setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK]];
    } else {
        //Hao Tran Remove
        //    if (!manualUpdateRoomStatus.isActive) {
        //        return;
        //    }

        if (restrictionAssignment.isActive) {
            if (([CommonVariable sharedCommonVariable].currentPauseRoom == noPauseRoom && indexPath.row != 0)
                ||([CommonVariable sharedCommonVariable].currentPauseRoom != greaterThanOnePauseRoom && indexPath.row != 0 && indexPath.row != 1)) {
                return;
            }
        }

        //    //ANLE rework Here
        //    if([indexPath row] !=0 && ![UserManagerV2 isSupervisor])
        //        return;

        if ([roomAssignmentData count] <= 0) {
            return;
        }

        //Enable Checklist button when go to Room Detail screen
        //    if ([UserManagerV2 isSupervisor]) {
        //        UIButton *butChecklist = (UIButton *)[[self.tabBarController tabBar] viewWithTag:tagOfCheckListButton];
        //        [butChecklist setEnabled:YES];
        //    }

        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        //[self drawUnderlinedLabel:(UILabel *)[cell viewWithTag:tagRMStatus] inTableViewCell:cell];

        // Show hud when get room detail and get info before go to room detail
        MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
        [hud setLabelText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_RA_LOADING_DATA_CONTENT]];
        [hud show:YES];
        [self.tabBarController.view addSubview:hud];

        NSDictionary *rowData = [self.roomAssignmentData objectAtIndex:indexPath.row];

        // CFG [20160913/CRF-00001439] - Check to validate special assignment, only check for room attendants.
        if ((([self validateSpecialAssignment:[(NSString *)[rowData objectForKey:kRaKindOfRoom] intValue]]) && (![UserManagerV2 isSupervisor])) || (([self validateInspectionCrossZone:[(NSString *)[rowData objectForKey:kRaKindOfRoom] intValue]]) && ([UserManagerV2 isSupervisor]))) { // CFG [20161221/CRF-00001559]

            //Hao Tran - Fix more than 1 room started
            //11/20/2014 - Fix Supervisor can't start room
            if(![UserManagerV2 isSupervisor]){
                if([CommonVariable sharedCommonVariable].currentStartedRoom > 0){
                    if([[rowData objectForKey:kCleaningStatusID] intValue] != ENUM_CLEANING_STATUS_START){
                        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:imgYesBtn], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_please_complete_room_started] delegate:nil cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getOK] otherButtonTitles: nil];
                        [alert show];

                        [hud setHidden:YES];
                        [hud removeFromSuperview];
                        return;
                    }
                }
            }
            if([UserManagerV2 isSupervisor]){
                //set current RoomAssignmentId use in ChecklistView
                [TasksManagerV2 setCurrentRoomAssignment:[[rowData objectForKey:kRoomAssignmentID] integerValue]];

                RoomAssignmentInfoViewController *viewController = [[RoomAssignmentInfoViewController alloc] initWithOption:IS_PUSHED_FROM_ROOM_ASSIGNMENT andRoomAssignmentData:rowData];
                //auto Start if using QRCode
                if(isUsingQRCode) {
                    viewController.isAutoStartOnLoad = YES;
                    isUsingQRCode = NO;
                }

                [hud setHidden:YES];
                [hud removeFromSuperview];

                [self.navigationController pushViewController:viewController animated:YES];
                //        [aRoomInfoAssignment loadingData];

                [self.navigationItem.backBarButtonItem setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK]];


            }
            else {

                //set current RoomAssignmentId use in ChecklistView
                [TasksManagerV2 setCurrentRoomAssignment:[[rowData objectForKey:kRoomAssignmentID] integerValue]];

                RoomAssignmentInfoViewController *viewController = [[RoomAssignmentInfoViewController alloc] initWithOption:IS_PUSHED_FROM_ROOM_ASSIGNMENT andRoomAssignmentData:rowData];
                //auto Start if using QRCode
                if(isUsingQRCode) {
                    viewController.isAutoStartOnLoad = YES;
                    isUsingQRCode = NO;
                } else {
                    //Auto start for room started
                    int cleaningStatusId = [(NSString *)[rowData objectForKey:kCleaningStatusID] intValue];
                    if(cleaningStatusId == ENUM_CLEANING_STATUS_START){
                        viewController.isAutoStartOnLoad = YES;
                    }
                }

                [hud setHidden:YES];
                [hud removeFromSuperview];
                [self.navigationController pushViewController:viewController animated:YES];
                //        [aRoomInfoAssignment loadingData];

                [self.navigationItem.backBarButtonItem setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK]];

            }

            [cell setSelected:NO animated:NO];
        }
        else {
            [hud setHidden:YES];
            [hud removeFromSuperview];
            [cell setSelected:NO animated:NO];
        }
    }
}

#pragma mark -
#pragma mark UITableView Editing rows
-(NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSIndexPath *rowToSelect = indexPath;
    //NSInteger section = indexPath.section;
    if (isEdit) {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        rowToSelect = nil;
    }
    return rowToSelect;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCellEditingStyle style = UITableViewCellEditingStyleNone;
    return  style;
}

#pragma mark -
#pragma mark UITableView Moving rows
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {

    BOOL canMove = NO;

    if (indexPath.section == 0) {

        NSObject *object = [roomAssignmentData objectAtIndex:indexPath.row];
        if ([object isKindOfClass:[NSDictionary class]]) {

            NSDictionary *rowData = (NSDictionary*)object;
            CleaningStatusModelV2 *csmodel = [[CleaningStatusModelV2 alloc] init];
            csmodel.cstat_Id = [[rowData objectForKey:kCleaningStatusID] intValue];
            [[RoomManagerV2 sharedRoomManager] loadCleaningStatus:csmodel];

            if ([csmodel.cstat_Name isEqualToString:@"DND"]) {
                return NO;
            }

            canMove = indexPath.row <= [roomAssignmentData count];
        }
    }
    return canMove;
}

- (NSIndexPath *)tableView:(UITableView *)tableView targetIndexPathForMoveFromRowAtIndexPath:(NSIndexPath *)sourceIndexPath toProposedIndexPath:(NSIndexPath *)proposedDestinationIndexPath {

    NSObject *object = [roomAssignmentData objectAtIndex:proposedDestinationIndexPath.row];
    if ([object isKindOfClass:[NSDictionary class]]) {

        NSDictionary *rowData = (NSDictionary*)object;
        CleaningStatusModelV2 *csmodel = [[CleaningStatusModelV2 alloc] init];
        csmodel.cstat_Id = [[rowData objectForKey:kCleaningStatusID] intValue];
        [[RoomManagerV2 sharedRoomManager] loadCleaningStatus:csmodel];

        if ([csmodel.cstat_Name caseInsensitiveCompare:@"dnd"] == NSOrderedSame) {
            return sourceIndexPath;
        }

        //    if(proposedDestinationIndexPath.row -1 != sourceIndexPath.row &&
        //       sourceIndexPath.row !=  proposedDestinationIndexPath.row+1){
        //
        //
        //        return sourceIndexPath;
        //    }
    }
    return proposedDestinationIndexPath;
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {

    if ([fromIndexPath isEqual:toIndexPath]) {
        return;
    }

    NSInteger rowFrom, rowTo;
    rowFrom = fromIndexPath.row;
    rowTo = toIndexPath.row;

    NSObject *objectFrom = [roomAssignmentData objectAtIndex:fromIndexPath.row];
    NSObject *objectTo = [roomAssignmentData objectAtIndex:toIndexPath.row];

    if ([objectFrom isKindOfClass:[NSDictionary class]]) {

        NSMutableDictionary *fromRowData = [[NSMutableDictionary alloc] initWithDictionary:(NSDictionary*)objectFrom];
        if ([objectTo isKindOfClass:[NSDictionary class]]) {

            NSDictionary *toRowData = (NSDictionary*)objectTo;

            NSInteger priorityFrom, priorityTo;
            priorityTo = [[toRowData valueForKey:kRaPrioritySortOrder] integerValue];
            priorityFrom = [[fromRowData valueForKey:kRaPrioritySortOrder] integerValue];

            [[RoomManagerV2 sharedRoomManager] changeRoomAssignmentSequenceFromrRoomAssignId:
             [[fromRowData valueForKey:kRoomAssignmentID] integerValue] toRoomAssignId:[[toRowData valueForKey:kRoomAssignmentID] integerValue]  forUserId:[UserManagerV2 sharedUserManager].currentUser.userId];

            //Change priorities
            if (rowFrom < rowTo) {
                // To Priority + 1
                priorityFrom = priorityTo + 1;
            } else if (rowFrom > rowTo) {
                // To Priority - 1
                priorityFrom = priorityTo - 1;
            }

            NSString *priorityFromStr = [[NSString alloc] initWithFormat:@"%d", (int)priorityFrom];

            [fromRowData setValue:priorityFromStr forKey:kRaPrioritySortOrder];

            [roomAssignmentData removeObjectAtIndex:fromIndexPath.row];

            [roomAssignmentData insertObject:fromRowData atIndex:toIndexPath.row];

            //update room assignment priority
            RoomAssignmentModelV2 *model = [[RoomAssignmentModelV2 alloc] init];

            NSInteger raID = [[fromRowData objectForKey:kRoomAssignmentID] integerValue];
            NSInteger priority = [[fromRowData objectForKey:kRaPrioritySortOrder] integerValue];
            model.roomAssignment_Id = (int)raID;
            model.roomAssignment_UserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
            [[RoomManagerV2 sharedRoomManager] load:model];
            model.roomAssignment_Priority_Sort_Order = priority;

            [[RoomManagerV2 sharedRoomManager] update:model];
        } else {
            [roomAssignmentData removeObjectAtIndex:fromIndexPath.row];

            [roomAssignmentData insertObject:fromRowData atIndex:toIndexPath.row];
        }
    }
}

- (BOOL)tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([indexPath section] == 0) {
        return NO;
    }
    return YES;
}

- (void)tableView:(UITableView *)tableView willBeginEditingRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([indexPath section] == 0) {
        //redraw underline label
        //UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        //[self drawUnderlinedLabel:(UILabel *)[cell viewWithTag:tagRMStatus] inTableViewCell:cell];
    }
}

-(void)tableView:(UITableView *)tableView didEndEditingRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([indexPath section] == 0) {
        [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationTop];
    }
}

#pragma mark -
#pragma mark SyncRoomAssignment Methods

//return enum type of RoomIndicatorGuest
-(int)getRoomIndicatorGuestByArrivalDate:(NSString*)arrivalDate departureDate:(NSString*)departureDate
{
    NSString *arrivalDateCompare = nil;
    NSString *departureDateCompare = nil;
    NSString *nowDate = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd"];
    if(!isDemoMode){
        if(arrivalDate.length > 0) {
            arrivalDateCompare = [ehkConvert DateToStringWithString:arrivalDate fromFormat:@"yyyy-MM-dd HH:mm:ss" toFormat:@"yyyy-MM-dd"];

        }

        if(departureDate.length > 0) {
            departureDateCompare = [ehkConvert DateToStringWithString:departureDate fromFormat:@"yyyy-MM-dd HH:mm:ss" toFormat:@"yyyy-MM-dd"];
        }

        if(arrivalDateCompare.length > 0 && departureDateCompare.length > 0) {
            if([arrivalDateCompare caseInsensitiveCompare:nowDate] == NSOrderedSame){
                //Checking Back To Back
                if([departureDateCompare caseInsensitiveCompare:nowDate] == NSOrderedSame) {
                    return RoomIndicatorGuestBackToBack;
                }
            }
        }

        //Checking Due In Guest
        if(arrivalDateCompare.length > 0) {
            if([arrivalDateCompare caseInsensitiveCompare:nowDate] == NSOrderedSame) {
                return RoomIndicatorGuestDueIn;
            }
        }

        //Checking Due Out Guest
        if(departureDateCompare.length > 0){
            if([departureDateCompare caseInsensitiveCompare:nowDate] == NSOrderedSame) {
                return RoomIndicatorGuestDueOut;
            }
        }
    }
    else{
        if(arrivalDate.length > 0 && departureDate.length > 0) {
            if([arrivalDate isEqualToString:@"2014-10-28 8:30:00"]){
                //Checking Back To Back
                if([departureDate isEqualToString:@"2014-10-28 15:30:00"]) {
                    return RoomIndicatorGuestBackToBack;
                }
            }
        }

        //Checking Due In Guest
        if(arrivalDate.length > 0) {
            if([arrivalDate isEqualToString:@"2014-10-28 15:30:00"]) {
                return RoomIndicatorGuestDueIn;
            }
        }

        //Checking Due Out Guest
        if([departureDate isEqualToString:@"2014-10-28 8:30:00"]){
            if([departureDateCompare caseInsensitiveCompare:nowDate] == NSOrderedSame) {
                return RoomIndicatorGuestDueOut;
            }
        }
    }

    return 0;
}

-(void) cancelSyncMethod{
    isSyncMethod = YES;
}

-(void) setTextSyncButtonWithText:(NSString *)text{
    UIButton *button = (UIButton *)[[self.tabBarController tabBar] viewWithTag:tagOfSyncNowButton];

    [button setTitle:text forState:UIControlStateNormal];
}

-(void) showSyncMessageAfterSync:(NSString *) returnSync {

    //set title again
    [self setCaptionsView];

    //Show alert new message
    NSString *willShowAlertMessage = [[UserManagerV2 sharedUserManager] getCurrentUserConfigValueByKey:USER_CONFIG_MESSAGE_IS_SHOW_NOTIFICATION];
    if(willShowAlertMessage.length > 0) {
        if([willShowAlertMessage caseInsensitiveCompare:@"true"] == NSOrderedSame
           || [willShowAlertMessage caseInsensitiveCompare:@"yes"] == NSOrderedSame) {

            [self announceNewMessage:nil];
        }
    }

    //if(isSynCompletedDialogShowing || isSynInCompletedDialogShowing) return;
    if ([returnSync caseInsensitiveCompare:@"syncincomplete"] == NSOrderedSame) {

        //        isSynInCompletedDialogShowing = YES;

        //        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[[LanguageManagerV2 sharedLanguageManager] getMsgSyncIncomplete] message:nil delegate:nil cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getOK] otherButtonTitles:nil];
        //        alert.tag = TagSynInCompletedDialogShowing;
        //        [alert setDelegate:self];
        //        [alert show];

        [[[[iToast makeText:[[LanguageManagerV2 sharedLanguageManager] getMsgSyncIncomplete]] setGravity:iToastGravityBottom] setDuration:iToastDurationNormal] show];

    } else {

        //        isSynCompletedDialogShowing = YES;

        //        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[[LanguageManagerV2 sharedLanguageManager] getMsgSyncMessage] message:nil delegate:nil cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getOK] otherButtonTitles:nil];
        //        alert.tag = TagSynCompletedDialogShowing;
        //        [alert setDelegate:self];
        //        [alert show];

        [[[[iToast makeText:[[LanguageManagerV2 sharedLanguageManager] getMsgSyncMessage]] setGravity:iToastGravityBottom] setDuration:iToastDurationNormal] show];
    }
}

-(void) syncMethod{
    @autoreleasepool {
        isSyncNow = NO;
        NSInteger countSecond = 0;

        //set default syncPeriod is 3 minutes
        //syncPeriod = 3 * 60;
        if(![UserManagerV2 isSupervisor]){
            syncPeriod = [[UserManagerV2 sharedUserManager].currentUserAccessRight syncRAInterval];
        } else {
            syncPeriod = [[UserManagerV2 sharedUserManager].currentUserAccessRight syncSUPInterval];
        }

        while (!isSyncMethod) {  // Loop until cancelled.
            countSecond = 0;
            // An autorelease pool to prevent the build-up of temporary objects.
            @autoreleasepool {

                if(![UserManagerV2 isSupervisor]){
                    syncPeriod = [[UserManagerV2 sharedUserManager].currentUserAccessRight syncRAInterval];
                } else {
                    syncPeriod = [[UserManagerV2 sharedUserManager].currentUserAccessRight syncSUPInterval];
                }

                NSDate *curtainTime = [[NSDate alloc] initWithTimeIntervalSinceNow:syncPeriod];
                NSDate *currentTime = [[NSDate alloc] init];

                // Wake up periodically to see if we've been cancelled.
                while ([currentTime compare:curtainTime] != NSOrderedDescending) {
                    @autoreleasepool {
                        //check if start sync method now is YES
                        countInThread++;
                        if (startSyncNow == START_SYNC_NOW) {
                            break;
                        }

                        // thread sleep syncPeriod time for next loop
                        [NSThread sleepForTimeInterval:1];

                        if (isLoginShowwing == NO) {
                            countSecond++;
                            if (countSecond >= syncPeriod) {
                                currentTime = [[NSDate alloc] init];
                            }
                        } else {
                            //reset count second when login is showwing
                            countSecond = 0;
                        }
                    }
                }
                //Beign sync here
                if([LogFileManager isLogConsole])
                {
                    NSLog(@"=== Begin sync ===");
                }

                //check if message is getting or is loading data, does not run sync method
                if (isGettingMessage == YES || isLoadingData == YES) {
                    startSyncNow = NON_START_SYNC;
                } else {

                    //load setting value
                    SettingModelV2 *smodel = [[SettingModelV2 alloc] init];
                    SettingAdapterV2 *sadapter = [[SettingAdapterV2 alloc] init];
                    [sadapter openDatabase];
                    [sadapter loadSettingModel:smodel];
                    [sadapter close];

                    if (smodel.settingsSynch == sManual) {
                        //Manual sync

                        if (isSyncNow == NO && isLoginShowwing == NO && startSyncNow == YES) {
                            //if user is logging out, does not update
                            [self performSync];

                            //sync HomeView on main thread
                            [self performSelectorOnMainThread:@selector(syncHomeView) withObject:nil waitUntilDone:NO];
                        }

                    } else {
                        if (isSyncNow == NO && isLoginShowwing == NO) {
                            //if user is logging out, does not update
                            [self performSync];

                            //sync HomeView on main thread
                            [self performSelectorOnMainThread:@selector(syncHomeView) withObject:nil waitUntilDone:NO];
                        }
                    }

                    //End sync here
                }

#ifdef DEBUG
                NSLog(@"=== End sync === %i",countInThread);
#endif
            }
        }

    }
}

//handle sync for application
-(void)performSync {


    //show dialog please wait syncing process
    [self performSelectorOnMainThread:@selector(showIsSyncing) withObject:nil waitUntilDone:YES];

    startSyncNow = NON_START_SYNC;

    //set text button SYNC is SYNCING..
    [self performSelectorOnMainThread:@selector(setTextSyncButtonWithText:) withObject:[[LanguageManagerV2 sharedLanguageManager] getMsgSyncRunningMessage] waitUntilDone:NO];

    //disable sync button
    //UIButton *syncButton = (UIButton *)[self.tabBarController.tabBar viewWithTag:tagOfSyncNowButton];
    UIButton *syncButton = (UIButton *)[self.slidingMenuBar buttonWithTag:tagOfSyncNowButton];
    [self performSelectorOnMainThread:@selector(disableButtonInTabbar:) withObject:syncButton waitUntilDone:NO];

    SyncManagerV2 *synManager = [[SyncManagerV2 alloc] init];

    isSyncNow = YES;

    SettingModelV2 *smodel = [[SettingModelV2 alloc] init];
    SettingAdapterV2 *sadapter = [[SettingAdapterV2 alloc] init];
    [sadapter openDatabase];
    [sadapter loadSettingModel:smodel];
    [sadapter close];

    //checking manual sync
    BOOL isManual = NO;
    if(smodel.settingsSynch == sManual || isTouchSync)
    {
        isManual = YES;
    }

    BOOL returnSync = [synManager update:isManual AndPercentView:waitingProgress];

    //invalid touch button sync
    isTouchSync = false;

    //assign non sync now
    isSyncNow = NO;

    //hide dialog please wait syncing process
    [self performSelectorOnMainThread:@selector(hideIsSyncing) withObject:nil waitUntilDone:YES];

    //enable sync button
    [self performSelectorOnMainThread:@selector(enableButtonInTabbar:) withObject:syncButton waitUntilDone:NO];

    //show alert when after sync
    if (returnSync == NO) {

        [self performSelectorOnMainThread:@selector(showSyncMessageAfterSync:) withObject:@"SyncIncomplete" waitUntilDone:NO];
        //set text button SYNC is SYNC NOW
        [self performSelectorOnMainThread:@selector(setTextSyncButtonWithText:) withObject:[[LanguageManagerV2 sharedLanguageManager] getBtnSyncNow] waitUntilDone:NO];

    } else if (returnSync == YES) {

        [self performSelectorOnMainThread:@selector(showSyncMessageAfterSync:) withObject:@"SyncComplete" waitUntilDone:NO];
        //set text button SYNC is SYNC NOW
        [self performSelectorOnMainThread:@selector(setTextSyncButtonWithText:) withObject:[[LanguageManagerV2 sharedLanguageManager] getBtnSyncNow] waitUntilDone:NO];

    }
}

#pragma mark -
#pragma mark syncHomeView Methods
-(void)syncHomeView{
    if([UserManagerV2 isSupervisor]){
        if ([[UserManagerV2 sharedUserManager].currentUserAccessRight isEnabledSupHomeFilter]) {
            [[RoomManagerV2 sharedRoomManager] getRoomInspectionByFilterType:(int)firstComboIndexSelected filterDetailId:(int)filterDetailId];
        } else {
            [[RoomManagerV2 sharedRoomManager] getRoomInspection];
        }
    } else{
        [[RoomManagerV2 sharedRoomManager] getRoomAssignments];
    }

    //self.roomAssignmentData = [NSMutableArray arrayWithArray:[[RoomManagerV2 sharedRoomManager] roomAssignmentList]];
    [self getOtherActivityAssignment];
    [self loadRoomAssignmentData];

    //Set badge number of Home
    [self setBadgeNumberTabbarWith:tagOfHomeButton number:[NSNumber numberWithInteger:[roomAssignmentData count]]];

    [roomTableView reloadData];
}

#pragma mark - Enable Button in Tabbar
-(void)enableButtonInTabbar:(UIButton *)button{
    [button setEnabled:YES];
}

-(void)disableButtonInTabbar:(UIButton *) button {
    [button setEnabled:NO];
}

#pragma mark - SyncButtonPressed
-(void)syncButtonPressed:(id)sender{
    //check wifi if wifi is not ok, will show popup
    if(!isDemoMode)
        if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp] == NO) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[[LanguageManagerV2 sharedLanguageManager] getNoWifi] message:nil delegate:nil cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getOK] otherButtonTitles:nil];
            [alert show];
            return;
        }

    if (isSyncNow == YES) {

    } else {
        [sender setEnabled:NO];
        isSyncNow = NO;

        //enable start sync now flag
        startSyncNow = START_SYNC_NOW;

        //checking touch button synch
        isTouchSync = YES;
    }

}

#pragma mark - SaveButtonPressed
-(void)saveButtonPressed:(id)sender{
    //disable save button 0.5s
    UIButton *saveBut = (UIButton *)sender;
    [saveBut setEnabled:NO];
    [self performSelector:@selector(enableButtonInTabbar:) withObject:saveBut afterDelay:0.0f];
}

#pragma mark - Message handle

//Get messages and show pop up (if any) from WS
//And update as read message to WS
-(void) getMessagePopupFromWS
{
    NSString *lastModified = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd"];
    NSMutableArray *results = [[RoomManagerV2 sharedRoomManager] getPopUpMessageFromWSByUserId:[UserManagerV2 sharedUserManager].currentUser.userId lastModified:lastModified];
    if(results.count > 0) {
        NSMutableArray *listMessage = [NSMutableArray array];
        NSMutableString *listIdRead = [[NSMutableString alloc] initWithString:@""];

        for(int i = 0; i < results.count; i ++) {
            PopupMessageItemModel *curMsgItem = [results objectAtIndex:i];
            [listMessage addObject:curMsgItem.popupMessage];
            [listIdRead appendString:[NSString stringWithFormat:@"%d",(int)curMsgItem.popupId]];
            if(i < results.count - 1){
                [listIdRead appendString:@","];
            }
        }

        if(listMessage.count > 0) {
            AlertTableViewViewController *alertView = [[AlertTableViewViewController alloc] initWithSuperView:self.tabBarController.view];
            alertView.listContents = listMessage;
            alertView.listIdsUpdateWS = listIdRead;
            [alertView show];

            [[CommonLib sharedCommonLib] playRingtone];
        }
    }
}

-(void) loadUserListByCurrentUser
{
    //------------------------------------------------------------------
    //HaoTran[20130607/Enhace Load User List] - Enhance Load User list
    //Check to load UserList with last modified
    int currentUserId = [UserManagerV2 sharedUserManager].currentUser.userId;

    //If user haven't login before then userLogData is NULL
    //So we will get all userlist on WS
    UserLogModelV2 *userLogData = [[UserManagerV2 sharedUserManager] getUserLogByUserId:currentUserId];
    NSString *lastModifiedDate = @"";
    if(userLogData){
        lastModifiedDate = [[UserManagerV2 sharedUserManager] getUserLastModifiedDate];
    }

    //Will log user login when get userlist success
    if([[AdHocMessageManagerV2 sharedAdHocMessageManagerV2] getUserListWith:currentUserId andHotelID:[UserManagerV2 sharedUserManager].currentUser.userHotelsId andLastModified:lastModifiedDate AndPercentView:nil])
    {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSString *loginDate = [formatter stringFromDate:[NSDate date]];

        if(!userLogData){
            userLogData = [[UserLogModelV2 alloc] init];
            userLogData.userId = currentUserId;
            userLogData.userName = [UserManagerV2 sharedUserManager].currentUser.userName;
            userLogData.userLastLoginDate = loginDate;
            [[UserManagerV2 sharedUserManager] insertUserLog:userLogData];
        }
        else{
            userLogData.userLastLoginDate = loginDate;
            [[UserManagerV2 sharedUserManager] updateUserLog:userLogData];
        }
    }
}
- (void)getMessageFromServer {
    if([LogFileManager isLogConsole])
    {
        NSLog(@"timer getMessage >>>");
    }
    //check if app is syncing or is loading data, does not start get message
    if (isSyncNow == YES || isLoadingData == YES) {
        return;
    }

    [NSThread detachNewThreadSelector:@selector(messageReciever) toTarget:self withObject:nil];
}

-(void) messageReciever
{
    //Show hud for get message
    [self messageReciever:YES callFrom:IS_PUSHED_FROM_HOME];
}

//if isShowHUD = true we will show HUD for loading message, otherwise don't show HUD for sync manager
-(void) messageReciever:(BOOL) isShowHUD callFrom:(int)callFrom
{

    //Check access right to get message from server
    if(messages.isActive || messages.isAllowedView || messages.isALlowedEdit){

    }
    else{
        return;
    }

    @autoreleasepool {
        //if isLoginShowwing is YES, do not get message from server
        if (self.isLoginShowwing == YES || isGettingMessage == YES) {
            return;
        }

#ifdef DEBUG
        NSLog(@"=== Begin get message ===");
#endif

        //message is getting
        isGettingMessage = YES;

        if(isShowHUD){
            //show dialog please wait getting message
            [self performSelectorOnMainThread:@selector(showMessageIsGetting) withObject:nil waitUntilDone:YES];
        }

        //Get message with last modified
        int messagePeriod = [[UserManagerV2 sharedUserManager].currentUserAccessRight getMessagePeriod];
        NSString *messageLastModified = [ehkConvert dateStringAgoWithPeriodDays:messagePeriod];
        NSString *dateRemoveTime = [ehkConvert commonDateRemoveTime:messageLastModified];

        /* DungPhan - discussion with FM on 20151007
         for new IPA and APK, will call new API: GetNoOfAdHocMessageRoutine
         - Old API GetNoOfAdHocMessage will not be called
         */
        [[AdHocMessageManagerV2 sharedAdHocMessageManagerV2] getNoOfAdHocMessageRoutineByUserId:[UserManagerV2 sharedUserManager].currentUser.userId groupMessageId:0 hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId lastModified:dateRemoveTime];
        // [[AdHocMessageManagerV2 sharedAdHocMessageManagerV2] loadWSMessageCountNumberByUserId:[UserManagerV2 sharedUserManager].currentUser.userId groupMessageId:0 hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId lastModified:dateRemoveTime];

        //Handle notify new message from sync or not
        if(callFrom == IS_PUSHED_FROM_MESSAGE_MAIN
           || callFrom == IS_PUSHED_FROM_MESSAGE_INBOX
           || slidingMenuBar.currentSelected == tagOfMessageButton) {

            //Will not show pop up message in message views
            [[UserManagerV2 sharedUserManager] setCurrentUserConfig:USER_CONFIG_MESSAGE_IS_SHOW_NOTIFICATION value:@"false"];
        }

        NSInteger numberOfMessage = [[[UserManagerV2 sharedUserManager] getCurrentUserConfigValueByKey:USER_CONFIG_MESSAGE_UNREAD_RECEIVED] intValue];
        [self performSelectorOnMainThread:@selector(showNumberOfNewMessages:) withObject:[NSNumber numberWithInteger:numberOfMessage] waitUntilDone:NO];

        //message finish getting
        isGettingMessage = NO;

        if(isShowHUD){
            //hide dialog please wait getting message
            [self performSelectorOnMainThread:@selector(hideMessageIsGetting) withObject:nil waitUntilDone:YES];
        }

#ifdef DEBUG
        NSLog(@"=== End get message ===");
#endif
    }
}


- (void) showPopupNewMessage: (NSNotification *) notif  {
    //NSString *msgNotification=[NSString stringWithFormat:@"%@ %@", [notif object] , @"New Message"];
    if(isDemoMode){
        NSString *unreadMessage = [NSString stringWithFormat:@"%d", [[[AdHocMessageManagerV2 alloc] init] countMessageInboxUnreadByOwnerId:[[UserManagerV2 sharedUserManager] currentUser].userId]];
        int countUnreadmessage = 0;
        if([unreadMessage intValue] > 0){
            countUnreadmessage = [unreadMessage intValue];
            NSString *newMessageValue = [NSString stringWithFormat:@"%d %@",countUnreadmessage, [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_MSG_NEW_MESSAGE]];
            UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:imgYesBtn], [UIImage imageNamed:imgNoBtn], nil] title:nil message:newMessageValue delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles:[[LanguageManagerV2 sharedLanguageManager] getNo], nil];
            [alert setTag:tagAlertNewMessage];
            [alert show];
        }
    }
    else{
        NSString *unreadMessage = [[UserManagerV2 sharedUserManager] getCurrentUserConfigValueByKey:USER_CONFIG_MESSAGE_UNREAD_RECEIVED];
        int countUnreadmessage = 0;
        if(unreadMessage.length > 0){
            countUnreadmessage = [unreadMessage intValue];

            //checking is showing pop up message or not
            NSString *isWaitingDismissPopup =  [[UserManagerV2 sharedUserManager] getCurrentUserConfigValueByKey:USER_CONFIG_MESSAGE_IS_WAITING_DISMISS_ALERT];
            if(isWaitingDismissPopup.length <= 0
               || [isWaitingDismissPopup caseInsensitiveCompare:@"false"] == NSOrderedSame
               || [isWaitingDismissPopup caseInsensitiveCompare:@"no"] == NSOrderedSame) {

                //flag to show or dismiss message
                [[UserManagerV2 sharedUserManager] setCurrentUserConfig:USER_CONFIG_MESSAGE_IS_WAITING_DISMISS_ALERT value:@"true"];
                NSString *newMessageValue = [NSString stringWithFormat:@"%d %@",countUnreadmessage, [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_MSG_NEW_MESSAGE]];
                UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:imgYesBtn], [UIImage imageNamed:imgNoBtn], nil] title:nil message:newMessageValue delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getView] otherButtonTitles:[[LanguageManagerV2 sharedLanguageManager] getCancel], nil];
                if(countUnreadmessage == 1) {
                    [alert setTag:tagAlertOneMessage];
                } else if(countUnreadmessage > 1) {
                    [alert setTag:tagAlertNewMessage];
                }
                [alert show];
            }
        }
    }

    //Hao Tran - Remove because already alert
    /*
     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:msgNotification message:nil delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getOK] otherButtonTitles:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_CANCEL], nil];
     [alert setTag:tagAlertNewMessage];
     [alert show];
     */
}

-(int)countUnreadMessageByUserId:(int)userId
{
    int numberOfUnReadMessage = 0;
    if(isDemoMode)
        numberOfUnReadMessage = [[[AdHocMessageManagerV2 alloc] init] countMessageInboxUnreadByOwnerId:[[UserManagerV2 sharedUserManager] currentUser].userId];
    else
        numberOfUnReadMessage = [[[UserManagerV2 sharedUserManager] getCurrentUserConfigValueByKey:USER_CONFIG_MESSAGE_UNREAD_RECEIVED] intValue];
    return numberOfUnReadMessage;
}

// Check database and update the unread message
- (void)updateUnreadMessage {
    @autoreleasepool {
        int numberOfUnReadMessage = [self countUnreadMessageByUserId:[[UserManagerV2 sharedUserManager] currentUser].userId];
        [self performSelectorOnMainThread:@selector(showNumberOfNewMessages:) withObject:[NSNumber numberWithInt:numberOfUnReadMessage] waitUntilDone:NO];
    }
}

// So unread message number on bottom bar
- (void)showNumberOfNewMessages:(NSNumber*)numberOfMsg {
    //Remove Button Tabbar For PRD 2.2 Release 2
    //UIButton *button = (UIButton *)[tabBarScrollView viewWithTag:tagOfMessageButton];

    UIButton *button = [slidingMenuBar buttonWithTag:tagOfMessageButton];
    for (UIView *subView in button.subviews) {
        if ([subView isKindOfClass:[CustomBadge class] ]) {
            [subView removeFromSuperview];
        }
    }

    if ([numberOfMsg intValue]>0) {

        UIColor *badgeFrameColor = nil;
        BOOL isShining = NO;
        if(SYSTEM_VERSION_LESS_THAN(@"7.0")){
            badgeFrameColor = [UIColor whiteColor];
            isShining = YES;
        } else {
            badgeFrameColor = [UIColor clearColor];
        }

        CustomBadge *customBadge1 = [CustomBadge customBadgeWithString:[numberOfMsg stringValue]
                                                       withStringColor:[UIColor whiteColor]
                                                        withInsetColor:[UIColor redColor]
                                                        withBadgeFrame:YES
                                                   withBadgeFrameColor:badgeFrameColor
                                                             withScale:1.0
                                                           withShining:isShining];
        [customBadge1 setFrame:CGRectMake(53 ,0, 27, 25)];
        [button addSubview:customBadge1];
    }
}

// Call when get new message
-(void)announceNewMessage:(NSNotification *) notif {
    SettingModelV2 *stModel=[[SettingModelV2 alloc] init];

    SettingAdapterV2 *stAdapter=[[SettingAdapterV2 alloc] init];
    [stAdapter openDatabase];
    [stAdapter resetSqlCommand];
    [stAdapter loadSettingModel:stModel];

    switch (stModel.settingsNotificationType)
    {
        case sPopup:
        {
            [self showPopupNewMessage: notif];
        }
            break;

        case sVibrate:
        {
            // case vibrate.
            [self vibrate];
            break;
        }


        case sRing:
        {
            // case sound
            [self loudTone];
            break;

        }
            break;

        default:
        {
            [self showPopupNewMessage: notif];
        }
            break;
    }
    [stAdapter close];
}


- (void)vibrate {

    for (int i = 1; i < 20; i++)
    {
        [self performSelector:@selector(vibe:) withObject:self afterDelay:i *.3f];
    }
}
// @ vibrate
-(void)vibe:(id)sender
{
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
}

// @ sound
- (void)loudTone {
    AudioServicesPlaySystemSound(1005);
}

#pragma mark -
#pragma mark BarButtonItem Methods
-(void) syncDataAfterEditTableView{
    for (NSDictionary *rowData in self.syncData) {
        [roomAssignmentData addObject:rowData];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[roomAssignmentData count]-1 inSection:0];
        [roomTableView beginUpdates];
        [roomTableView insertRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationBottom];
        [roomTableView endUpdates];
    }
    //NSLog(@"syncData: %d",[syncData count]);
    [self.syncData removeAllObjects];
}

- (void)butEditPressed:(id)sender{
    UIBarButtonItem *editBarButton = (UIBarButtonItem *)sender;
    if (isEdit == NO) { // is editing isEdit == NO
        [editBarButton setTitle:[self getTextForEditingButton]];
        [editBarButton setStyle:UIBarButtonItemStyleDone];
        isEdit = !isEdit;
        [roomTableView setEditing:isEdit animated:YES];
    } else { // exit edit isEdit == YES
        [self syncDataAfterEditTableView];
        isEdit = !isEdit;
        [roomTableView setEditing:isEdit animated:YES];
        [editBarButton setTitle:[self getTextForEditButton]];
        [editBarButton setStyle:UIBarButtonItemStylePlain];
    }
}


#pragma mark -
#pragma mark UIAlertView Delegate Methods
-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    switch (alertView.tag) {
        case tagAlertLogOut: // 10
        {
            // buttonIndex: 0-YES 1-NO
            //NSLog(@"buttonIndex: %d", buttonIndex);
            if (buttonIndex == 1) {
                //NO
                //Not do any action
            } else {
                //YES
                //reset all setting if needed

                //Call logout function
                [self logOutUser];
            }
            break;
        }
        case tagAlertNewMessage: // 11
        {
            //set false after show alert
            [[UserManagerV2 sharedUserManager] setCurrentUserConfig:USER_CONFIG_MESSAGE_IS_SHOW_NOTIFICATION value:@"false"];

            //set false after show alert
            [[UserManagerV2 sharedUserManager] setCurrentUserConfig:USER_CONFIG_MESSAGE_IS_WAITING_DISMISS_ALERT value:@"false"];

            if(buttonIndex == 0){
                //                self.isLoginShowwing = YES;
                if (buttonIndex == 0) {
                    // OK
                    UIButton *button = (UIButton *)[[self.tabBarController tabBar] viewWithTag:tagOfMessageButton];
                    [self handleTabbarEventWithTagOfButton:tagOfMessageButton];
                    [self butTabBarPressed:button];
                }
            }
            break;
        }
        case tagAlertOneMessage: // 111
        {
            //set false after show alert
            [[UserManagerV2 sharedUserManager] setCurrentUserConfig:USER_CONFIG_MESSAGE_IS_SHOW_NOTIFICATION value:@"false"];

            //set false after show alert
            [[UserManagerV2 sharedUserManager] setCurrentUserConfig:USER_CONFIG_MESSAGE_IS_WAITING_DISMISS_ALERT value:@"false"];

            if(buttonIndex == 0){
                //                self.isLoginShowwing = YES;
                if (buttonIndex == 0) {
                    AdhocMessageDetailV2 * adHocController = [[AdhocMessageDetailV2 alloc]init];
                    [[self navigationController] pushViewController:adHocController animated:YES];
                    //flagForOneMessage 1104
                    adHocController.isForOneMsg = TRUE;
                }
            }
            break;
        }

        case TagSynInCompletedDialogShowing:{
            isSynInCompletedDialogShowing = NO;
        } break;

        case TagSynCompletedDialogShowing:{
            isSynCompletedDialogShowing = NO;
        } break;

        case tagAlertForceUserLogout: //15
        {
            [self logOutUser];
        }
            break;
        default:
            break;
    }
}

/*
 #pragma mark - Set Captions View
 -(void) changeCaptionsTabbar {
 UIButton *button = nil;

 //home view
 button = (UIButton *)[tabBarScrollView viewWithTag:tagOfHomeButton];
 [button setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BTN_HOME] forState:UIControlStateNormal];

 //Sync
 button = (UIButton *)[tabBarScrollView viewWithTag:tagOfSyncNowButton];
 if ([button isEnabled] == NO) {
 [button setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_MSG_SYNC_RUNNING_MESSAGE] forState:UIControlStateNormal];
 } else
 [button setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BTN_SYNC_NOW] forState:UIControlStateNormal];

 //Save
 //    button = (UIButton *)[tabBarScrollView viewWithTag:tagOfSaveButton];
 //    [button setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BTN_SAVE] forState:UIControlStateNormal];
 //    [button setHidden:YES];

 //Posting
 button = (UIButton *)[tabBarScrollView viewWithTag:tagOfPostingButton];
 [button setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BTN_POSTING] forState:UIControlStateNormal];



 //Check list
 //    button = (UIButton *)[tabBarScrollView viewWithTag:tagOfCheckListButton];
 //    [button setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BTN_CHECK_LIST] forState:UIControlStateNormal];

 //Message
 button = (UIButton *)[tabBarScrollView viewWithTag:tagOfMessageButton];
 [button setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BTN_MESSAGE] forState:UIControlStateNormal];

 //Find
 button = (UIButton *)[tabBarScrollView viewWithTag:tagOfFindButton];
 [button setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BTN_FIND] forState:UIControlStateNormal];

 //Unassign
 button = (UIButton *)[tabBarScrollView viewWithTag:tagOfUnassignButton];
 [button setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BTN_UNASSIGN] forState:UIControlStateNormal];

 //Setting
 button = (UIButton *)[tabBarScrollView viewWithTag:tagOfSettingButton];
 [button setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BTN_SETTING] forState:UIControlStateNormal];

 //Logout
 button = (UIButton *)[tabBarScrollView viewWithTag:tagOfLogOutButton];
 [button setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BTN_LOGOUT] forState:UIControlStateNormal];
 //History----update code
 button = (UIButton *)[tabBarScrollView viewWithTag:tagOfHistoryButton];
 [button setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_HISTORY_BOTTOM_TABAR] forState:UIControlStateNormal];

 //Additional Job
 button = (UIButton *)[tabBarScrollView viewWithTag:tagOfAdditionalJob];
 [button setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BTN_JOB] forState:UIControlStateNormal];

 [self.tabBarController.view bringSubviewToFront:tabBarScrollView];
 }

 -(void) setCaptionsTabbar {
 //    NSArray *arrName = [self getNameOfTabBut];
 NSArray *arrImage = [self getNameImageTabButton];
 int size = [tabBarScrollView.subviews count];
 for (int temp = 0; temp < size ; temp ++) {

 int btnIndex = [[tabBarScrollView.subviews objectAtIndex:temp] tag] - 1;

 if([[tabBarScrollView.subviews objectAtIndex:temp] tag] == tagOfMessageButton)
 {
 btnIndex = 8;
 }
 else if([[tabBarScrollView.subviews objectAtIndex:temp] tag] == tagOfAdditionalJob)
 {
 btnIndex = 9;
 }

 //        textTemp = [arrName objectAtIndex:btnIndex];
 UIView *v = [tabBarScrollView.subviews objectAtIndex:temp];
 if([v isKindOfClass:[UIButton class]])
 {
 UIButton *butTabBar = (UIButton *)v;

 //        [butTabBar setTitle:textTemp forState:UIControlStateNormal];
 NSMutableString *strImage = nil;

 if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
 strImage =[[NSMutableString alloc] initWithFormat:@"menu2_%@.png",(NSString *)[arrImage objectAtIndex:btnIndex]];
 } else {
 strImage = [[NSMutableString alloc] initWithFormat:@"menu_%@.png",(NSString *)[arrImage objectAtIndex:btnIndex]];
 }
 [strImage setString:[strImage stringByReplacingOccurrencesOfString:@" " withString:@""]];
 [strImage setString:[strImage lowercaseString]];


 [butTabBar setBackgroundImage:[UIImage imageNamed:strImage] forState:UIControlStateNormal];
 }

 }

 //set captions tabbar
 [self changeCaptionsTabbar];
 }
 */

// set captions for view
-(void) setCaptionsView {
    NSString *userlang = [[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage];
    NSDictionary *dic = [[LanguageManagerV2 sharedLanguageManager] getCurrentDictionaryLanguageByCurrentUserLang:userlang];

    [labelRoom setText: [L_rm currentKeyToLanguage]];
    [labelRMStatus setText:[L_RM_STATUS currentKeyToLanguage]];
    [labelVIP setText:[L_VIP currentKeyToLanguage]];


    // RA intial :
    if (![UserManagerV2 isSupervisor]) {

        [[RoomManagerV2 sharedRoomManager] getTotalRoomComlete];
        NSInteger amount = [[RoomManagerV2 sharedRoomManager].roomAssignmentList count];
        NSString *title = nil;

        //Hao Tran - Remove for similar to android view
        /*
         if (amount == 0) {
         title = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_COMPLETED];
         }
         else {
         title = [NSString stringWithFormat:@"%@ (%i)",[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_COMPLETED],amount];
         }*/
        title = [NSString stringWithFormat:@"%@ (%i)", [L_BTN_VIEW_ASSIGNMENT currentKeyToLanguage], (int)amount];

        NSString *cleaningStatusString = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_CLEANING_STATUS];
        [labelCleaningStatus setText:cleaningStatusString];

        [butViewCleanedRooms setTitle:title forState:UIControlStateNormal];

    } else {

        [[RoomManagerV2 sharedRoomManager] getInspectedRoom];

        NSInteger amount = [[RoomManagerV2 sharedRoomManager].roomAssignmentList count];
        NSString *title = nil;
        if (amount == 0) {
            title = [dic valueForKey:[NSString stringWithFormat:@"%@", L_INSPECTED]];
        }
        else {
            title = [NSString stringWithFormat:@"%@ (%i)",[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_INSPECTED],(int)amount];
        }

        //        [labelCleaningStatus setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ios_ra_name]];
        [labelCleaningStatus setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_CLEANING_STATUS]];

        //[butViewCleanedRooms setTitle:title forState:UIControlStateNormal];

        //Hide inspection for supervisor
        _heightConstrainBtnComplete.constant = 0.0f;
        [butViewCleanedRooms setHidden:YES];
    }

    [labelHousekeeper setText:[dic valueForKey:[NSString stringWithFormat:@"%@", L_HOUSEKEEPER_NAME]]];
    [nameUserLabel setText:[dic valueForKey:[NSString stringWithFormat:@"%@", L_HOUSEKEEPER_NAME]]];

    SuperRoomModelV2 *modelSuper = [[SuperRoomModelV2 alloc] init];
    [topBarView refresh:modelSuper];
    //readd topbar view when view will appear
    [self.view addSubview:topBarView];

    //#warning dthanhson edit
    //    UIButton *labelTitle = (UIButton *)self.navigationItem.titleView;
    //
    //    if (![UserManagerV2 isSupervisor]){
    //        NSDictionary *dic = [[LanguageManagerV2 sharedLanguageManager] getCurrentDictionaryLanguage];
    //        [labelTitle setTitle:[dic valueForKey:[NSString stringWithFormat:@"%@", L_ASSIGNMENT]] forState:UIControlStateNormal];
    //    } else{
    //        //room_inspections
    //        [labelTitle setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_INSPECTION] forState:UIControlStateNormal];
    //    }
    [self populateDataIntoNavigationbar];
    //#warning end

    //set caption for sync button correct when syncing or not syncing
    UIButton *button = (UIButton *)[self.tabBarController.tabBar viewWithTag:tagOfLogOutButton];
    if (isSyncNow == YES) {
        [button setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_MSG_SYNC_RUNNING_MESSAGE] forState:UIControlStateNormal];
    } else {
        [button setTitle:[[LanguageManagerV2 sharedLanguageManager] getSYNCNOW] forState:UIControlStateNormal];
    }

    if (roomCompleted.isActive) {
        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
            [butViewCleanedRooms setBackgroundImage:[UIImage imageNamed:imgBtnCompletedAndInspectedFlat] forState:UIControlStateNormal];
        } else {
            [butViewCleanedRooms setBackgroundImage:[UIImage imageNamed:imgBtnCompletedAndInspected] forState:UIControlStateNormal];
        }
    }
    else {
        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
            [butViewCleanedRooms setBackgroundImage:[UIImage imageNamed:imgBtnCompletedAndInspectedDisableFlat] forState:UIControlStateNormal];
        } else {
            [butViewCleanedRooms setBackgroundImage:[UIImage imageNamed:imgBtnCompletedAndInspectedDisable] forState:UIControlStateNormal];
        }
    }
    [butViewCleanedRooms setBackgroundImage:[UIImage imageNamed:imgBtnCompletedAndInspectedClicked] forState:UIControlEventTouchUpInside];
    [butViewCleanedRooms setUserInteractionEnabled:roomCompleted.isActive];

    //Remove Button Tabbar for PRD V2.2 Release 2
    //[self setCaptionsTabbar];
}

#pragma mark - Update SyncPeriod Time
//update sync period time
-(void)updateSyncPeriodTime:(NSNotification *) notif {
    syncPeriod = [[notif object] integerValue];
}


#pragma mark - Show/Hide Wifi View
-(void)showWifiView {

    //Hao Tran add to fix wrong wifi signal in LoginView
    //If login view is showing we will not show wifi Viewcontroller
    if(self.loginView != nil && [self.loginView.view isDescendantOfView:self.tabBarController.view]){
        [self hideWifiView];
    } else {
        [wifiViewController.view removeFromSuperview];
        [self.tabBarController.view addSubview:wifiViewController.view];
        [wifiViewController setHidden:NO];
    }
}

-(void)hideWifiView {
    [wifiViewController setHidden:YES];
}


#pragma mark - === Get Room Statuses via WS ===
#pragma mark
-(void)getRoomStatusesWSWithPercentView:(MBProgressHUD*)percentView{
    RoomManagerV2 *manager = [[RoomManagerV2 alloc] init];
    // Hao Tran Enhance for get room status from WS
    /*
     NSMutableArray *roomstatuses = [manager loadAllRoomStatus];
     if (roomstatuses.count > 0) {
     NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"rstat_LastModified" ascending:NO];
     [roomstatuses sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];

     RoomStatusModelV2 *roomstatus = [roomstatuses objectAtIndex:0];

     [manager getAllRoomStatusWS:roomstatus.rstat_LastModified AndPercentView:percentView];
     } else {
     [manager getAllRoomStatusWS:nil AndPercentView:percentView];
     }*/

    //Enhance for get room status
    NSString *roomStatusLastModified = [[RoomManagerV2 sharedRoomManager] getRoomStatusLastModifiedDate];
    [manager getAllRoomStatusWS:roomStatusLastModified AndPercentView:percentView];

}

#pragma mark - Get Cleaning Statuses via WS
-(void)getCleaningStatusesWSWithPercentView:(MBProgressHUD*)percentView {
    //Hao Tran Enhance for get cleaning status
    NSString *cleaningStatusLastModified = [[RoomManagerV2 sharedRoomManager] getCleaningStatusLastModifiedDate];
    [[RoomManagerV2 sharedRoomManager] getAllCleaningStatusWS:cleaningStatusLastModified AndPercentView:percentView];
}

#pragma mark -
#pragma mark === Get Hotel info via WS ===
-(void)getHotelInfoWSWithPercentView:(MBProgressHUD*)percentView {
    int userID = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    int hotelID = [[[UserManagerV2 sharedUserManager] currentUser] userHotelsId];
    [[HotelManagerV2 sharedHotelManager] getHotelInfoOfUserID:[NSNumber numberWithInt:userID] andHotel:[NSNumber numberWithInt:hotelID] andlstModifier:nil AndPercentView:percentView];
}

#pragma mark - === Get message subjects ===
#pragma mark
-(void)getMessageSubjectWithPercentView:(MBProgressHUD *)percentView {
    NSInteger userId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    NSInteger hotelId = [[[UserManagerV2 sharedUserManager] currentUser] userHotelsId];

    NSMutableArray *category = [[AdHocMessageManagerV2 sharedAdHocMessageManagerV2] loadAllMsgSubject];
    if (category.count == 0) {
        [[AdHocMessageManagerV2 sharedAdHocMessageManagerV2] getSubjectListWith:userId andHotelID:hotelId andLastModified:nil AndPercentView:percentView];
    }
}

#pragma mark - === Update Record Sync ===
#pragma mark
-(void)updateRecordSyncWithNumber:(NSInteger)record {
    self.numberOfMustPostRecords = record;

    UIButton *button = (UIButton *)[self.tabBarController.tabBar viewWithTag:tagOfSyncNowButton];

    CustomBadge *customBadge = nil;

    customBadge = (CustomBadge *)[button viewWithTag:tagCustomeBadgeSync];

    if (record <= 0) {

        [customBadge setHidden:YES];

    } else {

        NSString *recordString = [NSString stringWithFormat:@"%d", (int)record];

        if (customBadge == nil) {

            UIColor *badgeFrameColor = nil;
            BOOL isShining = NO;
            if(SYSTEM_VERSION_LESS_THAN(@"7.0")){
                badgeFrameColor = [UIColor whiteColor];
                isShining = YES;
            } else {
                badgeFrameColor = [UIColor clearColor];
            }
            customBadge = [CustomBadge customBadgeWithString: recordString
                                             withStringColor:[UIColor whiteColor]
                                              withInsetColor:[UIColor redColor]
                                              withBadgeFrame:YES
                                         withBadgeFrameColor:badgeFrameColor
                                                   withScale:1.0
                                                 withShining:isShining];

        } else {

            [customBadge setBadgeText: recordString];
            [customBadge setNeedsDisplay];

        }

        [customBadge setTag:tagCustomeBadgeSync];
        [customBadge setFrame:CGRectMake(53 ,0, 27, 25)];
        [customBadge setHidden:NO];

        [button addSubview:customBadge];

    }
}
#pragma mark- Implement button job pressed
-(void)butJobPressed{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[L_alert currentKeyToLanguage] message:[L_job_pressed currentKeyToLanguage] delegate:nil cancelButtonTitle:[L_YES currentKeyToLanguage] otherButtonTitles:nil, nil];
    [alert show];
}

#pragma mark - === Temp Room Assignment Last Modified ===
#pragma mark
-(NSString *)getTempLastModifiedRoomAssignment {
    NSString *lastModified = [[RoomManagerV2 sharedRoomManager] getRoomAssignmentLastModifiedDateByUserId:[UserManagerV2 sharedUserManager].currentUser.userId];
    return lastModified;
}

#pragma mark - === Sync Status ===
#pragma mark
-(BOOL)isSyncing {
    return isSyncNow;
}

#pragma mark - === Show/Hide dialog message is getting ===
#pragma mark
-(void)showMessageIsGetting {
#ifdef DEBUG
    NSLog(@"=== Show HUD is getting messages ===");
#endif

    waitingProgress = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [waitingProgress setLabelText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_getting_message]];
    [self.tabBarController.view addSubview:waitingProgress];
    [waitingProgress show:YES];
}

-(void)hideMessageIsGetting {
#ifdef DEBUG
    NSLog(@"=== Hide HUD is getting messages ===");
#endif

    if (waitingProgress) {

#ifdef DEBUG
        NSLog(@"=== waiting messsage HUD != nil ===");
#endif

        [waitingProgress hide:YES];
        [waitingProgress removeFromSuperview];
        waitingProgress = nil;
    }
}

#pragma mark - === Show/Hide dialog is syncing ===
#pragma mark
-(void)showIsSyncing {
    waitingProgress = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [self.tabBarController.view addSubview:waitingProgress];
    [waitingProgress createCustomViewProgressBar:300];
    [waitingProgress show:YES];
    [waitingProgress setLabelText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_please_wait_syncing_progress]];

}

-(void)hideIsSyncing {
    if (waitingProgress) {
        [waitingProgress hide:YES];
        [waitingProgress removeFromSuperview];
        waitingProgress = nil;
    }
}

#pragma mark - === Set is loading data ===
#pragma mark
-(void) waitingLoadingData {
    isLoadingData = YES;
}

-(void) endWaitingLoadingData {
    isLoadingData = NO;
}

-(void)addButtonHandleShowHideTopbar {
    UIView *vBarButton = [[UIView alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
    UIButton *titleBarButtonFirst = [[UIButton alloc] initWithFrame:CGRectMake(0, -5, 200, 30)];
    [titleBarButtonFirst setTag:1];
    titleBarButtonFirst.titleLabel.backgroundColor = [UIColor clearColor];
    [titleBarButtonFirst.titleLabel setFont: FONT_BUTTON_TOPBAR];
    titleBarButtonFirst.titleLabel.textAlignment = NSTextAlignmentCenter;
    [titleBarButtonFirst.titleLabel setAdjustsFontSizeToFitWidth:YES];
    titleBarButtonFirst.titleLabel.textColor = COLOR_BUTTON_TOPBAR;

    [titleBarButtonFirst.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
    [titleBarButtonFirst.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
    [titleBarButtonFirst addTarget:self action:@selector(hiddenHeaderView) forControlEvents:UIControlEventTouchUpInside];
    [vBarButton addSubview:titleBarButtonFirst];

    UIButton *titleBarButtonSecond = [[UIButton alloc] initWithFrame:CGRectMake(0, 23, 200, 22)];
    [titleBarButtonSecond setTag:2];
    [titleBarButtonSecond setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [titleBarButtonSecond.titleLabel setFont:FONT_BUTTON_TOPBAR_SECOND];
    [titleBarButtonSecond addTarget:self action:@selector(hiddenHeaderView) forControlEvents:UIControlEventTouchUpInside];
    [vBarButton addSubview:titleBarButtonSecond];

    [self.navigationItem setTitleView:vBarButton];

    qrScanButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *img = [UIImage imageNamed:imgQRCode];
    [qrScanButton setBackgroundImage:img forState:UIControlStateNormal];
    [qrScanButton addTarget:self action:@selector(btnQRCodeScannerTouched:) forControlEvents:UIControlEventTouchUpInside];
    [qrScanButton setFrame:CGRectMake(0, 0, 50, 50)];
    UIView *backButtonView = [[UIView alloc] initWithFrame:qrScanButton.frame];

    //Move title to center vertical
    int moveLeftButton = 0;
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        moveLeftButton = 10;
    }

    backButtonView.bounds = CGRectMake(backButtonView.bounds.origin.x + moveLeftButton, 5, backButtonView.bounds.size.width, backButtonView.bounds.size.height);
    [backButtonView addSubview:qrScanButton];

    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButtonView];
    self.navigationItem.leftBarButtonItem = barButtonItem;

}

-(void) populateDataIntoNavigationbar
{
    if (topBarView != nil) {

        SuperRoomModelV2 *modelSuper = [[SuperRoomModelV2 alloc] init];
        NSString* propertyName = [[RoomManagerV2 sharedRoomManager] getPropertyName:modelSuper];
        NSString* propertyNameStr = [NSString stringWithFormat:@"%@",propertyName== nil ? @"":propertyName];

        UIView *viewNavigationBar = (UIView *)self.navigationItem.titleView;
        UIButton *btnFirstTitle = (UIButton *)[viewNavigationBar viewWithTag:1];
        UIButton *btnSecondTitle = (UIButton *)[viewNavigationBar viewWithTag:2];
        [btnFirstTitle setTitle:propertyNameStr forState:UIControlStateNormal];
        [btnSecondTitle setTitle:[[RoomManagerV2 sharedRoomManager] getHouseKeeperName] forState:UIControlStateNormal];
    }
}

-(void) addConfigPage
{
    SettingViewV2 *settingView = [[SettingViewV2 alloc] initForConfigAccount:YES with:self];
    //Hao Tran Remove - This code to hide tabbar made view wrong
    //[self hideTabBar:self.tabBarController];

    //We only use this code to hide tabbar bottom
    [settingView setHidesBottomBarWhenPushed:YES];

    [self.navigationController pushViewController:settingView animated:YES];
    [[HomeViewV2 shareHomeView].slidingMenuBar hideAll];
}

//Remove for PRD 2.2 release 2
/*
 - (void)hideTabBar:(UITabBarController *) tabbarcontroller
 {
 [UIView beginAnimations:nil context:NULL];
 [UIView setAnimationDuration:0.5];

 UIImageView *imgView = (UIImageView *)[[self.tabBarController tabBar] viewWithTag:numOfTabBut + 1];
 [imgView setHidden:YES];

 //Get Device Screen Kind
 int deviceKind = [DeviceManager getDeviceScreenKind];
 for(UIView *view in tabbarcontroller.view.subviews)
 {
 if([view isKindOfClass:[UITabBar class]])
 {
 [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y + HEIGHT_TAB_BAR, view.frame.size.width, view.frame.size.height)];
 }
 else
 {
 if(deviceKind == DeviceScreenKindRetina4_0)
 {
 [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, DeviceScreenSizeStandardHeight4_0)];
 }
 else
 {
 [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, DeviceScreenSizeStandardHeight3_5)];
 }

 }
 }

 [UIView commitAnimations];
 }

 - (void)showTabBar:(UITabBarController *) tabbarcontroller
 {
 [UIView beginAnimations:nil context:NULL];
 [UIView setAnimationDuration:0.5];
 for(UIView *view in tabbarcontroller.view.subviews)
 {
 if([view isKindOfClass:[UITabBar class]])
 {
 //Hao Tran[20130514/Fix layout] - Tab bar menu lost apart after login with config
 CGRect screenRect = [[UIScreen mainScreen] bounds];
 [view setFrame:CGRectMake(view.frame.origin.x, screenRect.size.height - HEIGHT_TAB_BAR, view.frame.size.width, view.frame.size.height)];
 //Hao Tran[20130514/Fix layout] - END
 }
 else
 {
 int heightHomeView = 431;
 int deviceKind = [DeviceManager getDeviceScreenKind];
 if (deviceKind == DeviceScreenKindRetina4_0) {
 heightHomeView = 519;
 }
 [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, heightHomeView)];
 }
 }
 [UIView commitAnimations];
 }
 */

-(void) btnSubmitClicked
{

    //Hao Tran Remove - No need to show tabbar because it not hide
    //[self showTabBar:self.tabBarController];

    if (loginView == nil) {
        loginView = [[LoginViewV2 alloc] initWithNibName:@"LoginViewV2" bundle:nil];
        [loginView setDelegate:self];

    }
    //Catch Frame
    //    CGRect frame = loginView.view.frame;
    //    int deviceKind = [DeviceManager getDeviceScreenKind];
    //    if(deviceKind == DeviceScreenKindRetina4_0)
    //    {
    //        frame.size.height = DeviceScreenSizeStandardHeight4_0;
    //    }
    //    else
    //    {
    //        frame.size.height = DeviceScreenSizeStandardHeight3_5;
    //    }
    //    frame.origin.y = 0;
    //    loginView.view.frame = frame;

    [loginView.view setFrame:self.tabBarController.view.bounds];

    //Clear information before Log out
    [self clearSubviewsContentInHomeView];

    //    [UIView transitionWithView:self.tabBarController.view duration:1
    //                       options:UIViewAnimationOptionTransitionCurlDown
    //                    animations:^ { [self.tabBarController.view addSubview:loginView.view]; }
    //                    completion:nil];

    [self.tabBarController.view addSubview:loginView.view];

    [loginView setCaptionsView];
}

-(void) loadAccessRights
{
    AccessRightModel *currentUserAccessRight = [[UserManagerV2 sharedUserManager] currentUserAccessRight];
    roomAssignment = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.roomAssignment];
    restrictionAssignment = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.restrictionAssignment];
    roomCompleted = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.completedRoom];
    manualUpdateRoomStatus = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.manualUpdateRoomStatus];
    QRCodeScanner = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.QRCodeScanner];
    settings = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.setting];
    messages = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.message];
    postingLostAndFound = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.postLostAndFound];
    postingEngineering = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.postEngineering];
    postingLaundry = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.postLaundry];
    postingMinibar = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.postMinibar];
    postingLinen = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.postLinen];
    postingAmenities = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.postAmenities];
    postingPhysicalCheck = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.postPhysicalCheck];
    findRoom = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.findRoom];
    findAttendant = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.findAttendantPendingCompleted];
    findInspection = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.findInspection];

    //Additionanl Job
    additionalJob = [[AccessRight alloc] init];
    additionalJob.isActive = [[[UserManagerV2 sharedUserManager] currentUserAccessRight] isAddJobActive];

}

#pragma mark - CleaningStatusPopup Delegator
-(void) btnDNDClicked
{
    if (cleaningStatusConfirmPopup == nil) {
        cleaningStatusConfirmPopup = [[CleaningStatusConfirmPopupView alloc] initWithDelegate:self andOption:CLEANING_STATUS_CONFIRM_POPUP_DND];
    }
    [cleaningStatusPopup removeFromSuperview];

    [cleaningStatusConfirmPopup setConfirmPopupType:CLEANING_STATUS_CONFIRM_POPUP_DND];
    [cleaningStatusConfirmPopup setRoomNo:(NSString *)[selectedRoomAssignment objectForKey:kRoomNo]];
    //[self.tabBarController.view addSubview:cleaningStatusConfirmPopup];
    [self.tabBarController addSubview:cleaningStatusConfirmPopup];
}
-(void) btnDoubleLockClicked
{
    if (cleaningStatusConfirmPopup == nil) {
        cleaningStatusConfirmPopup = [[CleaningStatusConfirmPopupView alloc] initWithDelegate:self andOption:CLEANING_STATUS_CONFIRM_POPUP_DOUBLE_LOCK];
    }
    [cleaningStatusPopup removeFromSuperview];

    [cleaningStatusConfirmPopup setConfirmPopupType:CLEANING_STATUS_CONFIRM_POPUP_DOUBLE_LOCK];
    [cleaningStatusConfirmPopup setRoomNo:(NSString *)[selectedRoomAssignment objectForKey:kRoomNo]];
    //[self.tabBarController.view addSubview:cleaningStatusConfirmPopup];
    [self.tabBarController addSubview:cleaningStatusConfirmPopup];
}
-(void) btnDeclinedServiceClicked
{
    if (cleaningStatusConfirmPopup == nil) {
        cleaningStatusConfirmPopup = [[CleaningStatusConfirmPopupView alloc] initWithDelegate:self andOption:CLEANING_STATUS_CONFIRM_POPUP_DECLINED_SERVICE];
    }
    [cleaningStatusPopup removeFromSuperview];

    [cleaningStatusConfirmPopup setConfirmPopupType:CLEANING_STATUS_CONFIRM_POPUP_DECLINED_SERVICE];
    [cleaningStatusConfirmPopup setRoomNo:(NSString *)[selectedRoomAssignment objectForKey:kRoomNo]];

    //[self.tabBarController.view addSubview:cleaningStatusConfirmPopup];
    [self.tabBarController addSubview:cleaningStatusConfirmPopup];
}

-(void) btnServiceLaterClicked
{
    if (cleaningStatusConfirmPopup == nil) {
        cleaningStatusConfirmPopup = [[CleaningStatusConfirmPopupView alloc] initWithDelegate:self andOption:CLEANING_STATUS_CONFIRM_POPUP_SERVICE_LATER];
    }
    [cleaningStatusPopup removeFromSuperview];

    [cleaningStatusConfirmPopup setConfirmPopupType:CLEANING_STATUS_CONFIRM_POPUP_SERVICE_LATER];
    [cleaningStatusConfirmPopup setRoomNo:(NSString *)[selectedRoomAssignment objectForKey:kRoomNo]];
    //[self.tabBarController.view addSubview:cleaningStatusConfirmPopup];
    [self.tabBarController addSubview:cleaningStatusConfirmPopup];
}

-(void) btnPendingClicked
{
    if (cleaningStatusConfirmPopup == nil) {
        cleaningStatusConfirmPopup = [[CleaningStatusConfirmPopupView alloc] initWithDelegate:self andOption:CLEANING_STATUS_CONFIRM_POPUP_PENDING];
    }
    [cleaningStatusPopup removeFromSuperview];

    [cleaningStatusConfirmPopup setConfirmPopupType:CLEANING_STATUS_CONFIRM_POPUP_PENDING];
    [cleaningStatusConfirmPopup setRoomNo:(NSString *)[selectedRoomAssignment objectForKey:kRoomNo]];
    //[self.tabBarController.view addSubview:cleaningStatusConfirmPopup];
    [self.tabBarController addSubview:cleaningStatusConfirmPopup];
}

#pragma mark - CleaningStatusConfirmPopup delegate

-(void)btnCompleteClicked
{

}

-(void)btnCancelClicked
{

}

-(void)btnCloseClicked
{

}

-(void) btnYesClicked
{
    RoomAssignmentModelV2 *roomAssignmentModel = [[RoomAssignmentModelV2 alloc] init];
    roomAssignmentModel.roomAssignment_Id = [(NSString *)[selectedRoomAssignment objectForKey:kRoomAssignmentID] intValue];
    roomAssignmentModel.roomAssignment_UserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    [[RoomManagerV2 sharedRoomManager] load:roomAssignmentModel];
    switch (cleaningStatusConfirmPopup.confirmPopupType) {
        case CLEANING_STATUS_CONFIRM_POPUP_DND:
        {
            if ([UserManagerV2 isSupervisor] && roomAssignmentModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_DND) {
                return;
            }

            roomAssignmentModel.roomAssignmentRoomCleaningStatusId = ENUM_CLEANING_STATUS_DND;

            roomAssignmentModel.roomAssignment_Priority_Sort_Order = [[RoomManagerV2 sharedRoomManager] loadMaxPrioprity] + 1;
            roomAssignmentModel.roomAssignment_PostStatus = (int)POST_STATUS_SAVED_UNPOSTED;

            NSDateFormatter *format = [[NSDateFormatter alloc] init];
            [format setDateFormat:@"yyyy-MM-dd"];
            NSDate *now = [NSDate date];
            NSString *dndAssignDate = [NSString stringWithFormat:@"%@ 23:58:00", [format stringFromDate:now]];
            roomAssignmentModel.roomAssignment_AssignedDate = dndAssignDate;

            [[RoomManagerV2 sharedRoomManager] update:roomAssignmentModel];

            MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
            [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getMsgSaveMessage]];
            [self.tabBarController.view addSubview:HUD];
            [HUD show:YES];
            SyncManagerV2 *synManager = [[SyncManagerV2 alloc] init];
            [synManager postRoomAssignmentWithUserID:[[UserManagerV2 sharedUserManager] currentUser].userId AndRaModel:roomAssignmentModel];

            [HUD hide:YES];
            [HUD removeFromSuperview];

            [cleaningStatusConfirmPopup removeFromSuperview];
        }
            break;
            // CRF [Huy][20161108/CRF-00001431]
        case CLEANING_STATUS_CONFIRM_POPUP_DOUBLE_LOCK:
        {
            if ([UserManagerV2 isSupervisor] && roomAssignmentModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_DOUBLE_LOCK) {
                return;
            }

            roomAssignmentModel.roomAssignmentRoomCleaningStatusId = ENUM_CLEANING_STATUS_DOUBLE_LOCK;

            roomAssignmentModel.roomAssignment_Priority_Sort_Order = [[RoomManagerV2 sharedRoomManager] loadMaxPrioprity] + 1;
            roomAssignmentModel.roomAssignment_PostStatus = (int)POST_STATUS_SAVED_UNPOSTED;

            NSDateFormatter *format = [[NSDateFormatter alloc] init];
            [format setDateFormat:@"yyyy-MM-dd"];
            NSDate *now = [NSDate date];
            NSString *dndAssignDate = [NSString stringWithFormat:@"%@ 23:58:00", [format stringFromDate:now]];
            roomAssignmentModel.roomAssignment_AssignedDate = dndAssignDate;

            [[RoomManagerV2 sharedRoomManager] update:roomAssignmentModel];

            MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
            [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getMsgSaveMessage]];
            [self.tabBarController.view addSubview:HUD];
            [HUD show:YES];
            SyncManagerV2 *synManager = [[SyncManagerV2 alloc] init];
            [synManager postRoomAssignmentWithUserID:[[UserManagerV2 sharedUserManager] currentUser].userId AndRaModel:roomAssignmentModel];

            [HUD hide:YES];
            [HUD removeFromSuperview];

            [cleaningStatusConfirmPopup removeFromSuperview];
        }
            break;
        case CLEANING_STATUS_CONFIRM_POPUP_COMPLETE:
            break;

        case CLEANING_STATUS_CONFIRM_POPUP_DECLINED_SERVICE:
        {
            if ([UserManagerV2 isSupervisor] && roomAssignmentModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_DECLINE_SERVICE) {
                return;
            }

            //update assign date for Decline Service to 11:59:00
            NSDateFormatter *format = [[NSDateFormatter alloc] init];
            [format setDateFormat:@"yyyy-MM-dd"];
            NSDate *now = [NSDate date];
            NSString *declineServiceAssignDate = [NSString stringWithFormat:@"%@ 23:59:00", [format stringFromDate:now]];
            roomAssignmentModel.roomAssignment_AssignedDate = declineServiceAssignDate;
            roomAssignmentModel.roomAssignmentRoomCleaningStatusId = ENUM_CLEANING_STATUS_DECLINE_SERVICE;
            roomAssignmentModel.roomAssignment_PostStatus = (int)POST_STATUS_SAVED_UNPOSTED;

            [[RoomManagerV2 sharedRoomManager] update:roomAssignmentModel];

            roomAssignmentModel.roomAssignment_AssignedDate = declineServiceAssignDate;

            MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
            [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getMsgSaveMessage]];
            [self.tabBarController.view addSubview:HUD];
            [HUD show:YES];
            SyncManagerV2 *synManager = [[SyncManagerV2 alloc] init];
            [synManager postRoomAssignmentWithUserID:[[UserManagerV2 sharedUserManager] currentUser].userId AndRaModel:roomAssignmentModel];

            [HUD hide:YES];
            [HUD removeFromSuperview];

            [cleaningStatusConfirmPopup removeFromSuperview];
        }
            break;

        case CLEANING_STATUS_CONFIRM_POPUP_PENDING:
            break;

        case CLEANING_STATUS_CONFIRM_POPUP_STOP:
            break;

        case CLEANING_STATUS_CONFIRM_POPUP_SERVICE_LATER:
        {
            if ([UserManagerV2 isSupervisor] && roomAssignmentModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_SERVICE_LATER) {
                return;
            }

            NSDateFormatter *_timeFomarter = [[NSDateFormatter alloc] init];
            [_timeFomarter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];

            RoomRecordModelV2 *roomRecord = [[RoomRecordModelV2 alloc] init];
            roomRecord.rrec_User_Id = [[UserManagerV2 sharedUserManager] currentUser].userId;
            roomRecord.rrec_room_assignment_id = roomAssignmentModel.roomAssignment_Id;
            roomRecord = [[RoomManagerV2 sharedRoomManager] loadRoomRecordDataByRoomIdAndUserId:roomRecord];

            RoomServiceLaterModelV2 *roomServicelater = [[RoomServiceLaterModelV2 alloc] init];
            roomServicelater.rsl_RecordId = roomRecord.rrec_Id;
            roomServicelater.rsl_Time = [_timeFomarter stringFromDate:cleaningStatusConfirmPopup.selectedDate];
            [[RoomManagerV2 sharedRoomManager] insertRoomServiceLaterData:roomServicelater];

            roomAssignmentModel.roomAssignmentRoomCleaningStatusId = ENUM_CLEANING_STATUS_SERVICE_LATER;
            roomAssignmentModel.roomAssignment_AssignedDate = [_timeFomarter stringFromDate:cleaningStatusConfirmPopup.selectedDate];
            roomAssignmentModel.roomAssignment_PostStatus = (int)POST_STATUS_SAVED_UNPOSTED;
            [[RoomManagerV2 sharedRoomManager] update:roomAssignmentModel];

            RoomModelV2 *roomModel = [[RoomModelV2 alloc] init];
            roomModel.room_PostStatus = (int)POST_STATUS_SAVED_UNPOSTED;
            [[RoomManagerV2 sharedRoomManager] updateRoomModel:roomModel];

            roomAssignmentModel.roomAssignment_PostStatus = (int)POST_STATUS_SAVED_UNPOSTED;
            MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
            [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getMsgSaveMessage]];
            [self.tabBarController.view addSubview:HUD];
            [HUD show:YES];
            SyncManagerV2 *synManager = [[SyncManagerV2 alloc] init];
            int responseCode = [synManager postRoomAssignmentWithUserID:[[UserManagerV2 sharedUserManager] currentUser].userId
                                                             AndRaModel:roomAssignmentModel];

            if(responseCode == RESPONSE_STATUS_NO_NETWORK) {

                UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:@"" message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_internet_exception] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getOK] otherButtonTitles: nil];
                [alert show];
                [HUD hide:YES];
                [HUD removeFromSuperview];
                return;

            } else if(responseCode == RESPONSE_STATUS_ERROR_REASSIGN_DATE_IN_THE_PAST) {

                UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:@"" message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_assign_date_can_not_be_past] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getOK] otherButtonTitles: nil];
                [alert show];
                [HUD hide:YES];
                [HUD removeFromSuperview];
                return;
            } else if(responseCode == RESPONSE_STATUS_ERROR_REASSIGN_OUT_OF_STAFF_WORKSHIFT) {

                UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:@"" message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_assignment_out_of_staff_workshift] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getOK] otherButtonTitles: nil];
                [alert show];
                [HUD hide:YES];
                [HUD removeFromSuperview];
                return;
            } else if(responseCode == RESPONSE_STATUS_ERROR_REASSIGN_OUT_OF_REMAINED_TIME) {

                UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:@"" message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_assignment_out_of_remained_time] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getOK] otherButtonTitles: nil];
                [alert show];
                [HUD hide:YES];
                [HUD removeFromSuperview];
                return;
            } else if(responseCode == RESPONSE_STATUS_ERROR_ASSIGNMENT_NOT_EXIST) {

                UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:@"" message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_assignment_not_exist] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getOK] otherButtonTitles: nil];
                [alert show];
                [HUD hide:YES];
                [HUD removeFromSuperview];
                return;
            } else if(responseCode == RESPONSE_STATUS_ERROR_STAFF_NOT_ASSIGNED) {

                UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:@"" message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_assignment_staff_not_assigned] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getOK] otherButtonTitles: nil];
                [alert show];
                [HUD hide:YES];
                [HUD removeFromSuperview];
                return;

            }  else if(responseCode == RESPONSE_STATUS_ERROR_END_CLEANING_TIME_EXCEED_CURRENT_DAY_ASSIGNMENT) {

                UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:@"" message:[L_end_cleaning_time_exceed_current_day_assignment currentKeyToLanguage] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getOK] otherButtonTitles: nil];
                [alert show];
                [HUD hide:YES];
                [HUD removeFromSuperview];
                return;
            }

            [HUD hide:YES];
            [HUD removeFromSuperview];
            [cleaningStatusConfirmPopup removeFromSuperview];
        }
            break;

        case CLEANING_STATUS_CONFIRM_POPUP_SERVICE_LATER_REMINDER:
            break;

        default:
            break;
    }

    [self syncHomeView];
}

#pragma mark - QR Code Button Touched
-(void) btnQRCodeScannerTouched:(id) sender
{
    ZXingWidgetController *widController = [[ZXingWidgetController alloc] initWithDelegate:self showCancel:YES OneDMode:NO showLicense:NO];
    QRCodeReader* qrcodeReader = [[QRCodeReader alloc] init];
    NSSet *readers = [[NSSet alloc ] initWithObjects:qrcodeReader,nil];
    widController.readers = readers;
    //    [readers release];
    //    NSBundle *mainBundle = [NSBundle mainBundle];
    //    widController.soundToPlay = [NSURL fileURLWithPath:[mainBundle pathForResource:@"beep-beep" ofType:@"aiff"] isDirectory:NO];
    [self presentViewController:widController animated:YES completion:nil];
}

#pragma mark - ZxingDelegate
- (void)zxingController:(ZXingWidgetController*)controller didScanResult:(NSString *)resultString {
    resultString = [STEncryptorDES convertText:resultString];
    [self dismissViewControllerAnimated:YES completion:nil];
    BOOL isWrong = YES;

    //Find room assignment existing
    if ([roomAssignmentData count] > 0) {
        for (int i = 0; i < [roomAssignmentData count]; i++) {
            NSIndexPath *currentIndexPath = [NSIndexPath indexPathForRow:i inSection:0];
            UITableViewCell *cell = [self tableView:roomTableView cellForRowAtIndexPath:currentIndexPath];
            UILabel *lblRoomNo = (UILabel *)[cell viewWithTag:tagRoomNo];

            //Navigate to RoomAssignmentInfo
            if([lblRoomNo.text isEqualToString:resultString])
            {
                isWrong = NO;

                //Using QR Code will set to NO in didSelectRowAtIndexPath
                isUsingQRCode = YES;
                [self tableView:roomTableView didSelectRowAtIndexPath:currentIndexPath];
                break;
            }
        }
    }

    if (isWrong) {

        NSString *alertContent = nil;
        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
            alertContent = [L_wrong_room currentKeyToLanguage];
        } else {
            alertContent = [NSString stringWithFormat:@"\n\n\n\n\n%@!", [L_wrong_room currentKeyToLanguage]];
        }

        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[L_alert currentKeyToLanguage] message:alertContent delegate:self cancelButtonTitle:[L_CLOSE currentKeyToLanguage] otherButtonTitles:nil];
        UIImageView *alertImage = [[UIImageView alloc] initWithFrame:CGRectMake(80, 35, 120, 120)];
        [alertImage setImage:[UIImage imageNamed:imgWrongRoom]];
        [alert addSubview:alertImage];
        [alert show];
    }
}

- (void)zxingControllerDidCancel:(ZXingWidgetController*)controller {
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - HUD delegate

-(void)hudWasHidden:(MBProgressHUD *)hud
{
    if(hud.tag == tagHudRemoveLoginView)
    {
        [roomTableView reloadData];
    }
}

#pragma mark - Scale Image
- (UIImage *)resizeImage:(UIImage*)image newSize:(CGSize)newSize {
    CGRect newRect = CGRectIntegral(CGRectMake(0, 0, newSize.width, newSize.height));
    CGImageRef imageRef = image.CGImage;

    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0);
    CGContextRef context = UIGraphicsGetCurrentContext();

    // Set the quality level to use when rescaling
    CGContextSetInterpolationQuality(context, kCGInterpolationHigh);
    CGAffineTransform flipVertical = CGAffineTransformMake(1, 0, 0, -1, 0, newSize.height);

    CGContextConcatCTM(context, flipVertical);
    // Draw into the context; this scales the image
    CGContextDrawImage(context, newRect, imageRef);

    // Get the resized image from the context and a UIImage
    CGImageRef newImageRef = CGBitmapContextCreateImage(context);
    UIImage *newImage = [UIImage imageWithCGImage:newImageRef];

    CGImageRelease(newImageRef);
    UIGraphicsEndImageContext();

    return newImage;
}

#pragma mark - Sliding Legend

-(void)addSlidingLegend
{
    if(!_slidingLegend){
        _slidingLegend = [[SlidingMenuBar alloc] initWithView:self.tabBarController.view
                                                     dockType:SlidingMenuBarDockLeft slidingType:SlidingType_Legend];
        _slidingLegend.slidingType = SlidingType_Legend;
    }
    [_slidingLegend showAll];
}

#pragma mark - Sliding Menu Bar

//Sliding Manu bar with
-(void)addSlidingMenuBar
{
    [LogFileManager logDebugMode:@"[Item 3] addSlidingMenuBar Stack: %@",[NSThread callStackSymbols]];

    //UIWindow *window = [[[UIApplication sharedApplication] windows] objectAtIndex:0];
    if(!slidingMenuBar){
        slidingMenuBar = [[SlidingMenuBar alloc] initWithView:self.tabBarController.view dockType:SlidingMenuBarDockRight slidingType:SlidingType_Menu];
    }

    //Delegate
    slidingMenuBar.delegate = self;

    //Panic
    if([[UserManagerV2 sharedUserManager].currentUserAccessRight isEnablePanicButton]) {
        [LogFileManager logDebugMode:@"[Item 3] addSlidingMenuBar: [UserManagerV2 sharedUserManager].currentUserAccessRight isEnablePanicButton] YES"];
        [slidingMenuBar createButtonWithTag:tagOfPanicButton atIndex:NSNotFound enabled:YES];
    }else{
        [LogFileManager logDebugMode:@"[Item 3] addSlidingMenuBar: [UserManagerV2 sharedUserManager].currentUserAccessRight isEnablePanicButton] NO"];
    }

    //Home
    [slidingMenuBar createButtonWithTag:tagOfHomeButton atIndex:NSNotFound enabled:YES];

    //Additional Job
    if(additionalJob.isActive || additionalJob.isALlowedEdit){
        [slidingMenuBar createButtonWithTag:tagOfAdditionalJob atIndex:NSNotFound enabled:YES];
    }

    [LogFileManager logDebugMode:@"[Item 3] addSlidingMenuBar: postingLostAndFound == nil", postingLostAndFound ? @"YES" : @"NO" ];

    //Posting
    if(postingLostAndFound.isActive || postingLostAndFound.isAllowedView || postingEngineering.isActive || postingEngineering.isAllowedView || postingLaundry.isActive || postingLaundry.isAllowedView ||
       postingMinibar.isActive || postingMinibar.isAllowedView ||
       postingLinen.isActive || postingLinen.isAllowedView ||
       postingAmenities.isActive || postingAmenities.isAllowedView ||
       postingPhysicalCheck.isActive || postingPhysicalCheck.isAllowedView)
    {
        [slidingMenuBar createButtonWithTag:tagOfPostingButton atIndex:NSNotFound enabled:YES];
    }

    //History
    [slidingMenuBar createButtonWithTag:tagOfHistoryButton atIndex:NSNotFound enabled:YES];

    //Guest Profile
    if ([[UserManagerV2 sharedUserManager].currentUserAccessRight isEnableGuestProfile]) {
        [slidingMenuBar createButtonWithTag:tagOfGuestProfileButton atIndex:NSNotFound enabled:YES];
    }

    //EditedBy-DungPhan-2015/03/03
    //Enable or disable find function base on useraccessright
    if ([UserManagerV2 isSupervisor]) {
        [LogFileManager logDebugMode:@"[Item 3] addSlidingMenuBar: findRoom == nil", findRoom ? @"YES" : @"NO" ];
        if(findRoom.isAllowedView || findRoom.isALlowedEdit || findRoom.isActive || findInspection.isAllowedView || findInspection.isALlowedEdit || findInspection.isActive || findAttendant.isAllowedView || findAttendant.isALlowedEdit || findAttendant.isActive)
        {
            [slidingMenuBar createButtonWithTag:tagOfFindButton atIndex:NSNotFound enabled:YES];
        }
    } else {
        if(findRoom.isAllowedView || findRoom.isALlowedEdit || findRoom.isActive || findInspection.isAllowedView || findInspection.isALlowedEdit || findInspection.isActive)
        {
            [slidingMenuBar createButtonWithTag:tagOfFindButton atIndex:NSNotFound enabled:YES];
        }
    }//End edit-2015/03/03

    //Unassign
    if([[[UserManagerV2 sharedUserManager] currentUserAccessRight] hasUnassignRoom]){
        [slidingMenuBar createButtonWithTag:tagOfUnassignButton atIndex:NSNotFound enabled:YES];
    }

    //Message
    if(messages.isAllowedView || messages.isActive || messages.isALlowedEdit){
        [slidingMenuBar createButtonWithTag:tagOfMessageButton atIndex:NSNotFound enabled:YES];
    }

    //Sync
    if(!isDemoMode){
        [slidingMenuBar createButtonWithTag:tagOfSyncNowButton atIndex:NSNotFound enabled:YES];
    }
    //PTT check if want to show
#if DEFAULT_ENABLE_PTT
    if(!isDemoMode){
        [slidingMenuBar createButtonWithTag:tagOfPTT atIndex:NSNotFound enabled:YES];
    }
#endif
    //Setting
    if(!isDemoMode){
        if(settings.isAllowedView || settings.isActive || settings.isALlowedEdit){
            [slidingMenuBar createButtonWithTag:tagOfSettingButton atIndex:NSNotFound enabled:YES];
        }
    }


    //Legend
    //[slidingMenuBar createButtonWithTag:tagOfLegendButton atIndex:NSNotFound enabled:YES];

    //Log out
    [slidingMenuBar createButtonWithTag:tagOfLogOutButton atIndex:NSNotFound enabled:YES];

    //Paint all menu buttons to view
    [slidingMenuBar paintAllButtons];
    [slidingMenuBar hide:NO];

    //Highlight home button
    [self setSelectedButton:tagOfHomeButton];

}

//Delegate of sliding menu bar button
-(void)didSelectedButtonIndex:(int)index tag:(int)tagOfButton
{
    //Re-use old method of previous bottom tabbar
    [self butTabBarPressed:[slidingMenuBar buttonWithTag:tagOfButton]];
    //[self handleTabbarEventWithTagOfButton:tagOfButton];
}

#pragma mark - Panic Cover Layout
-(void)addCoverPanicView
{
    CGRect tabbarFrame = self.tabBarController.view.frame;
    if(!coverPanicView){
        coverPanicView = [[UIView alloc] initWithFrame:tabbarFrame];
        coverPanicView.backgroundColor = [UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.75f];

        UIButton *buttonCover = [UIButton buttonWithType:UIButtonTypeCustom];
        [buttonCover setBackgroundImage:[UIImage imageNamed:imgPanicPressed] forState:UIControlStateNormal];
        [buttonCover addTarget:self action:@selector(touchedButtonCover) forControlEvents:UIControlEventTouchUpInside];
        CGFloat y = (tabbarFrame.size.height - tabbarFrame.size.width/2)/2;
        [buttonCover setFrame:CGRectMake(tabbarFrame.size.width/4 , y, tabbarFrame.size.width/2, tabbarFrame.size.width/2)];
        [coverPanicView addSubview:buttonCover];
    }

    [self.tabBarController.view addSubview:coverPanicView];
    //[self.tabBarController addSubview:coverPanicView];
}

//Begin: MHSKPIOS-223 - 20140421
//Add for fixing when Supervisor find room then back to Home view, rooms which don't belong to current Supervisor will appear - MHSKPIOS-223:
- (void)loadRoomAssignmentData {

    if ([UserManagerV2 isSupervisor]) {
        //Check if Find function is used. If not, no need to check flag ra_is_checked_ra_id in ROOM_ASSIGNMENT.
        if([[RoomManagerV2 sharedRoomManager] isFindRoom] == YES){
            NSMutableArray *listRoomAssignment = [[RoomManagerV2 sharedRoomManager] roomAssignmentList];

            self.roomAssignmentData = [[NSMutableArray alloc] init];
            for (NSDictionary *roomAssigment in listRoomAssignment) {
                //Check flag ra_is_checked_ra_id in ROOM_ASSIGNMENT: 0 means roomassignment's houseKeeperId is equal to one of list RA of current Supervisor.
                if ([[roomAssigment valueForKey:kraCheckedRa] isEqualToString:@"0"]) {
                    [self.roomAssignmentData addObject:roomAssigment];
                }
            }
            [RoomManagerV2 sharedRoomManager].isFindRoom = NO;
        } else {
            self.roomAssignmentData = [[RoomManagerV2 sharedRoomManager] roomAssignmentList];
        }
    } else {
        if ([[RoomManagerV2 sharedRoomManager] isFindRoom] == YES) {
            NSMutableArray *listRoomAssignment = [[RoomManagerV2 sharedRoomManager] roomAssignmentList];
            self.roomAssignmentData = [[NSMutableArray alloc] init];
            for(NSDictionary *roomAssigment in listRoomAssignment) {
                NSString *userID = [NSString stringWithFormat:@"%d",[[[UserManagerV2 sharedUserManager] currentUser] userId]];
                if([[roomAssigment valueForKey:kraHousekeeperID] isEqualToString: userID]) {
                    [self.roomAssignmentData addObject:roomAssigment];
                }
            }
            [RoomManagerV2 sharedRoomManager].isFindRoom = NO;
        } else {
            self.roomAssignmentData = [[RoomManagerV2 sharedRoomManager] roomAssignmentList];
        }
        if([self.roomAssignmentData count] > 0) {
            NSMutableArray *tempArray = [NSMutableArray new];
            for (int i=0;i<self.roomAssignmentData.count;i++) {
                NSDictionary *status = (NSDictionary*)self.roomAssignmentData[i];
                if([ status[@"RMStatus"] isEqualToString:@"OOS"]) {
                    [tempArray addObject:status];
                }
            }
            [self.roomAssignmentData removeObjectsInArray:tempArray];
        }
    }
    [self.roomAssignmentData addObjectsFromArray:listOtherActivityDetailDisplay];
    [self sortListRoomAssignmentData];
}
//End: MHSKPIOS-223 - 20140421

-(NSComparisonResult (^) (id obj1, id obj2))compareRoom{

    return [^(id obj1, id obj2)
            {
                NSDictionary *p1 = (NSDictionary*)obj1;
                NSDictionary *p2 = (NSDictionary*)obj2;

                NSString *cleanTM1 =  [p1 objectForKey:@"raLastCleaningDate"] ;
                NSString *cleanTM2 =  [p2 objectForKey:@"raLastCleaningDate"] ;
                NSInteger roomNo1 =  [[p1 objectForKey:@"RoomNo"] integerValue];
                NSInteger roomNo2 =  [[p2 objectForKey:@"RoomNo"] integerValue];


                NSComparisonResult comparisionresult = [cleanTM1 compare:cleanTM2];

                if (comparisionresult == NSOrderedSame){
                    if (roomNo1 > roomNo2) {
                        return (NSComparisonResult)NSOrderedDescending;
                    }

                    if (roomNo1 < roomNo2) {
                        return (NSComparisonResult)NSOrderedAscending;
                    }
                    return (NSComparisonResult)NSOrderedSame;

                }else{
                    return comparisionresult;
                }

            } copy];
}

-(NSComparisonResult (^) (id obj1, id obj2))compareByRoomNo{

    return [^(id obj1, id obj2)
            {

                NSInteger roomNo1 =  [obj1 integerValue];
                NSInteger roomNo2 =  [obj2 integerValue];

                if(roomNo1 > roomNo2) return (NSComparisonResult)NSOrderedDescending;
                else if(roomNo1 < roomNo2) return (NSComparisonResult)NSOrderedAscending;
                else return (NSComparisonResult)NSOrderedSame;
            } copy];
}

-(NSComparisonResult (^) (id obj1, id obj2))compareByCleaningTime{

    return [^(id obj1, id obj2)
            {
                //                NSDictionary *p1 = (NSDictionary*)obj1;
                //                NSDictionary *p2 = (NSDictionary*)obj2;
                NSString *cleanTM1 =  obj1 ;
                NSString *cleanTM2 = obj2 ;

                if([cleanTM1 isEqualToString:kDefaultValueRaCleanTm]){
                    return [cleanTM2 compare:kDefaultValueRaCleanTm];
                }else if([cleanTM2 isEqualToString:kDefaultValueRaCleanTm]){
                    return (NSComparisonResult)NSOrderedAscending;
                }else{
                    return [cleanTM1 compare:cleanTM2];
                }
            } copy];
}

-(NSComparisonResult (^) (id obj1, id obj2))compareByRoomType{

    return [^(id obj1, id obj2)
            {
                //                NSDictionary *p1 = (NSDictionary*)obj1;
                //                NSDictionary *p2 = (NSDictionary*)obj2;
                //
                NSInteger roomType1 =  [obj1 integerValue];
                NSInteger roomType2 =  [obj2 integerValue];

                if(roomType2 == ENUM_KIND_OF_ROOM_RUSH){
                    return roomType1 == ENUM_KIND_OF_ROOM_RUSH ? (NSComparisonResult)NSOrderedSame : (NSComparisonResult)NSOrderedDescending;
                }
                if(roomType1 == ENUM_KIND_OF_ROOM_RUSH){
                    return (NSComparisonResult)NSOrderedAscending;
                }
                return (NSComparisonResult)NSOrderedSame;
            } copy];
}




- (void)sortListRoomAssignmentData {

    if([UserManagerV2 isSupervisor]){
        // [20160927] Kha Tran edit code
        NSSortDescriptor *sortByRoomNo  = [[NSSortDescriptor alloc] initWithKey:@"RoomNo" ascending:YES  comparator:[self compareByRoomNo]];
        //        NSSortDescriptor *sortByCleanTm =[[NSSortDescriptor alloc] initWithKey:@"raCleanEndTm" ascending:YES comparator: [self compareByCleaningTime]];
        NSSortDescriptor *sortByCleanTm =[[NSSortDescriptor alloc] initWithKey:kLastCleaningDate ascending:YES comparator: [self compareByCleaningTime]];

        NSSortDescriptor *sortByRoomType =[[NSSortDescriptor alloc] initWithKey:@"raKindOfRoom" ascending:YES comparator: [self compareByRoomType]];
        NSArray *sortDescriptors = [NSArray arrayWithObjects:sortByRoomType, sortByCleanTm, sortByRoomNo,nil];
        //    NSLog(@"before");
        //    for (NSDictionary *lRoomAssignment in roomAssignmentData){
        //        NSLog(@"kind: %@ ,cl: %@ ,no: %@", [lRoomAssignment objectForKey:@"raKindOfRoom"], [lRoomAssignment objectForKey:@"raLastCleaningDate"] ,
        //              [lRoomAssignment objectForKey:@"RoomNo"]);
        //    }
        self.roomAssignmentData = [NSMutableArray arrayWithArray: [roomAssignmentData sortedArrayUsingDescriptors:sortDescriptors]];


        //    for (int i =0 ; i< [self.roomAssignmentData count]; i ++) {
        //        NSDictionary *i =self.roomAssignmentData dictionn:i]
        //        NSLog("kind: %@ ,cl: %@ ,no: %@", [self.roomAssignmentData objectAtIndex:i])
        //    }
        NSLog(@"after");
        for (NSDictionary *lRoomAssignment in self.roomAssignmentData){
            NSLog(@"kind: %@ ,cl: %@ ,no: %@", [lRoomAssignment objectForKey:@"raKindOfRoom"], [lRoomAssignment objectForKey:kLastCleaningDate] ,
                  [lRoomAssignment objectForKey:@"RoomNo"]);
        }
    }
    else{
        self.roomAssignmentData = [NSMutableArray arrayWithArray:[self.roomAssignmentData sortedArrayUsingComparator:^NSComparisonResult(NSObject *obj1, NSObject *obj2) {
            NSString *str1 = [NSString new];
            NSString *str2 = [NSString new];
            if ([obj1 isKindOfClass:[OtherActivityDetailsDisplay class]]) {
                str1 = ((OtherActivityDetailsDisplay*)obj1).otherActivityDetail.oaa_assign_date;
            } else {
                str1 = [((NSDictionary*)obj1) objectForKey:kRoomAssignmentTime];
            }
            if ([obj2 isKindOfClass:[OtherActivityDetailsDisplay class]]) {
                str2 = ((OtherActivityDetailsDisplay*)obj2).otherActivityDetail.oaa_assign_date;
            } else {
                str2 = [((NSDictionary*)obj2) objectForKey:kRoomAssignmentTime];
            }
            return [str1 compare:str2 options:NSCaseInsensitiveSearch];
        }]];

        // CFG [20160810/CRF-00001440] - Sorting options.
        NSMutableArray *rushList = [NSMutableArray array];
        NSMutableArray *tmpList = [NSMutableArray array];
        NSMutableArray *dndList = [NSMutableArray array];
        NSMutableArray *declinedServiceList = [NSMutableArray array];
        NSMutableArray *result=[NSMutableArray array];

        for (id obj in roomAssignmentData) {
            // Adam [20180112/App Crash] - Filter OtherActivity objects.
            if(![obj isKindOfClass:[OtherActivityDetailsDisplay class]]){
                NSDictionary *lRoomAssignment = obj;
                if (([[lRoomAssignment objectForKey:@"Cleaning Status ID"] integerValue] == ENUM_CLEANING_STATUS_DND)||[[lRoomAssignment objectForKey:@"Cleaning Status ID"] integerValue] == ENUM_CLEANING_STATUS_DOUBLE_LOCK) {
                    [dndList addObject:lRoomAssignment];
                }
                else if ([[lRoomAssignment objectForKey:@"Cleaning Status ID"] integerValue] == ENUM_CLEANING_STATUS_DECLINE_SERVICE) {
                    [declinedServiceList addObject:lRoomAssignment];
                }
                else if (([[lRoomAssignment objectForKey:@"raKindOfRoom"] integerValue] & ENUM_KIND_OF_ROOM_RUSH) == ENUM_KIND_OF_ROOM_RUSH) { // CFG [20160913/CRF-00001439]
                    [rushList addObject:lRoomAssignment];
                }
                else {
                    [tmpList addObject:lRoomAssignment];
                }
            }
        }
        //                [rushList sortUsingComparator: [self compareRoom]];
        //                [tmpList sortUsingComparator: [self compareRoom]];
        //                [dndList sortUsingComparator: [self compareRoom]];
        //                [declinedServiceList sortUsingComparator: [self compareRoom]];

        [result addObjectsFromArray:rushList];
        [result addObjectsFromArray:tmpList];
        [result addObjectsFromArray:dndList];
        [result addObjectsFromArray:declinedServiceList];

        self.roomAssignmentData = result;
        //             CFG [20160810/CRF-00001440] - End.
    }
    //
    //




}

-(void)touchedButtonCover{
    [self handleTabbarEventWithTagOfButton:tagOfPanicButton];
}

#pragma mark - Filter Room List At Home Screen
//CRF-1259 : Display Filter for supervisor Home Screen

-(void) resetComboboxes
{
    if (SYSTEM_VERSION_LESS_THAN(@"7.0")) {
        [_firstComboBox setBackgroundImage:[UIImage imageNamed:optionButLanguageImg] forState:UIControlStateNormal];
        [_secondComboBox setBackgroundImage:[UIImage imageNamed:optionButLanguageImg] forState:UIControlStateNormal];
    }

    firstComboIndexSelected = 0;
    secondComboIndexSelected = NSNotFound;
    filterDetailId = 0;
    [_firstComboBox setTitle:[L_filter_all currentKeyToLanguage] forState:UIControlStateNormal];
    [_secondComboBox setEnabled:NO];
    [_secondComboBox setAlpha:7.0f];
    [_secondComboBox setTitle:@"" forState:UIControlStateNormal];
}

-(void)moveDropboxViewDown
{
    _topSpaceDropboxView.constant = heightScale;
    [self.view layoutSubviews];
}

-(void)moveDropboxViewUp
{
    _topSpaceDropboxView.constant = 0.0f;
    [self.view layoutSubviews];
}

-(void) hideComboboxView
{
    [_dropboxView setHidden:YES];
    _heightDropboxView.constant = 0;
}

-(void) showComboboxView
{
    [_dropboxView setHidden:NO];
    _heightDropboxView.constant = 40.0f;
}


-(IBAction)firstComboPressed:(id)sender
{
    NSString *allOption = [L_filter_all currentKeyToLanguage];

    NSString *floorOption = [L_filter_floor currentKeyToLanguage];
    NSString *zoneOption = [L_filter_zone currentKeyToLanguage];
    NSString *roomStatusOption = [L_filter_room_status currentKeyToLanguage];
    NSString *roomTypeOption = [L_filter_room_type currentKeyToLanguage];

    NSMutableArray *listFirstFilter = [NSMutableArray arrayWithObjects:allOption, roomTypeOption, roomStatusOption, zoneOption, floorOption, nil];
    PickerViewV2 *picker = [[PickerViewV2 alloc] initPickerViewV2WithDatas:listFirstFilter AndSelectedData:nil AndIndex:nil];
    picker.delegate = self;
    picker.showFromObject = _firstComboBox;
    [picker showPickerViewV2];
}

-(IBAction)secondComboPressed:(id)sender
{
    if (firstComboIndexSelected == FilterOption_Floor) {
        PickerViewV2 *picker = [[PickerViewV2 alloc] initPickerViewV2WithDatas:listFloorTitles AndSelectedData:nil AndIndex:nil];
        picker.delegate = self;
        picker.showFromObject = _secondComboBox;
        [picker showPickerViewV2];
    } else if (firstComboIndexSelected == FilterOption_RoomStatus) {
        PickerViewV2 *picker = [[PickerViewV2 alloc] initPickerViewV2WithDatas:listRoomStatusTitles AndSelectedData:nil AndIndex:nil];
        picker.delegate = self;
        picker.showFromObject = _secondComboBox;
        [picker showPickerViewV2];
    } else if (firstComboIndexSelected == FilterOption_RoomType) {
        PickerViewV2 *picker = [[PickerViewV2 alloc] initPickerViewV2WithDatas:listRoomTypeTitles AndSelectedData:nil AndIndex:nil];
        picker.delegate = self;
        picker.showFromObject = _secondComboBox;
        [picker showPickerViewV2];
    } else if (firstComboIndexSelected == FilterOption_Zone) {
        PickerViewV2 *picker = [[PickerViewV2 alloc] initPickerViewV2WithDatas:listZoneTitles AndSelectedData:nil AndIndex:nil];
        picker.delegate = self;
        picker.showFromObject = _secondComboBox;
        [picker showPickerViewV2];
    }
}

-(void) reloadData{
}

-(void) didChooseFindRoomPickerViewV2WithPickerView:(PickerViewV2*)pickerView withText:(NSString *)data WithtIndex:(NSInteger)tIndex
{
    if (pickerView.showFromObject == _firstComboBox) {
        firstComboIndexSelected = tIndex;
        secondComboIndexSelected = 0;
        [_firstComboBox setTitle:data forState:UIControlStateNormal];
        if (tIndex == FilterOption_All) { //0
            [_secondComboBox setTitle:@"" forState:UIControlStateNormal];
            [_secondComboBox setEnabled:NO];
            [_secondComboBox setAlpha:7.0f];
        } else if(tIndex == FilterOption_Floor) {

            if(!listFloorOptions || !listFloorTitles) {
                listFloorOptions = [[RoomManagerV2 sharedRoomManager] loadSupervisorFiltersRoutineByUserId:[UserManagerV2 sharedUserManager].currentUser.userId typeId:FilterOption_Floor];
                listFloorTitles = [NSMutableArray array];

                for (int i = 0; i < listFloorOptions.count; i ++) {
                    SupervisorFilterDetail *curFloor = listFloorOptions[i];
                    if([[LanguageManagerV2 sharedLanguageManager] currentLanguageIsEqualTo:@"%@", ENGLISH_LANGUAGE]){
                        [listFloorTitles addObject:curFloor.filter_name];
                    } else {
                        [listFloorTitles addObject:curFloor.filter_lang];
                    }
                }
            }

            if ([listFloorTitles count] > 0) {
                [_secondComboBox setTitle:listFloorTitles[0] forState:UIControlStateNormal];
                [self selectSecondComboboxIndex:0 title:listFloorTitles[0]];
                [_secondComboBox setEnabled:YES];
                [_secondComboBox setAlpha:1.0f];
            } else {
                [_secondComboBox setTitle:@"" forState:UIControlStateNormal];
                [_secondComboBox setEnabled:NO];
                [_secondComboBox setAlpha:7.0f];
            }

        } else if(tIndex == FilterOption_RoomStatus) {
            [_secondComboBox setEnabled:YES];
            [_secondComboBox setAlpha:1.0f];

            if(!listRoomStatusOptions || !listRoomStatusTitles) {
                listRoomStatusOptions = [[RoomManagerV2 sharedRoomManager] loadSupervisorFiltersRoutineByUserId:[UserManagerV2 sharedUserManager].currentUser.userId typeId:FilterOption_RoomStatus];
                listRoomStatusTitles = [NSMutableArray array];

                for (int i = 0; i < listRoomStatusOptions.count; i ++) {
                    SupervisorFilterDetail *curStatus = listRoomStatusOptions[i];
                    if([[LanguageManagerV2 sharedLanguageManager] currentLanguageIsEqualTo:@"%@", ENGLISH_LANGUAGE]) {
                        [listRoomStatusTitles addObject:curStatus.filter_name];
                    } else {
                        [listRoomStatusTitles addObject:curStatus.filter_lang];
                    }
                }
            }

            if ([listRoomStatusTitles count] > 0) {
                [_secondComboBox setTitle:listRoomStatusTitles[0] forState:UIControlStateNormal];
                [self selectSecondComboboxIndex:0 title:listRoomStatusTitles[0]];
                [_secondComboBox setEnabled:YES];
                [_secondComboBox setAlpha:1.0f];
            } else {
                [_secondComboBox setTitle:@"" forState:UIControlStateNormal];
                [_secondComboBox setEnabled:NO];
                [_secondComboBox setAlpha:7.0f];
            }

        } else if(tIndex == FilterOption_RoomType) {
            [_secondComboBox setEnabled:YES];
            [_secondComboBox setAlpha:1.0f];
            if(!listRoomTypeOptions || !listRoomTypeTitles) {
                listRoomTypeOptions = [[RoomManagerV2 sharedRoomManager] loadSupervisorFiltersRoutineByUserId:[UserManagerV2 sharedUserManager].currentUser.userId typeId:FilterOption_RoomType];
                listRoomTypeTitles = [NSMutableArray array];

                for (int i = 0; i < listRoomTypeOptions.count; i ++) {
                    SupervisorFilterDetail *curRoomType = listRoomTypeOptions[i];
                    if([[LanguageManagerV2 sharedLanguageManager] currentLanguageIsEqualTo:@"%@", ENGLISH_LANGUAGE]) {
                        [listRoomTypeTitles addObject:curRoomType.filter_name];
                    } else {
                        [listRoomTypeTitles addObject:curRoomType.filter_lang];
                    }
                }
            }

            if ([listRoomTypeTitles count] > 0) {
                [_secondComboBox setTitle:listRoomTypeTitles[0] forState:UIControlStateNormal];
                [self selectSecondComboboxIndex:0 title:listRoomTypeTitles[0]];
                [_secondComboBox setEnabled:YES];
                [_secondComboBox setAlpha:1.0f];
            } else {
                [_secondComboBox setTitle:@"" forState:UIControlStateNormal];
                [_secondComboBox setEnabled:NO];
                [_secondComboBox setAlpha:7.0f];
            }

        } else if(tIndex == FilterOption_Zone) {
            [_secondComboBox setEnabled:YES];
            [_secondComboBox setAlpha:1.0f];
            if(!listZoneTitles || !listZoneOptions) {
                listZoneOptions = [[RoomManagerV2 sharedRoomManager] loadSupervisorFiltersRoutineByUserId:[UserManagerV2 sharedUserManager].currentUser.userId typeId:FilterOption_Zone];
                listZoneTitles = [NSMutableArray array];

                for (int i = 0; i < listZoneOptions.count; i ++) {
                    SupervisorFilterDetail *curRoomType = listZoneOptions[i];
                    if([[LanguageManagerV2 sharedLanguageManager] currentLanguageIsEqualTo:@"%@", ENGLISH_LANGUAGE]) {
                        [listZoneTitles addObject:curRoomType.filter_name];
                    } else {
                        [listZoneTitles addObject:curRoomType.filter_lang];
                    }
                }
            }

            if ([listZoneTitles count] > 0) {
                [_secondComboBox setTitle:listZoneTitles[0] forState:UIControlStateNormal];
                [self selectSecondComboboxIndex:0 title:listZoneTitles[0]];
                [_secondComboBox setEnabled:YES];
                [_secondComboBox setAlpha:1.0f];
            } else {
                [_secondComboBox setTitle:@"" forState:UIControlStateNormal];
                [_secondComboBox setEnabled:NO];
                [_secondComboBox setAlpha:7.0f];
            }
        }
        [self syncHomeView];

    } else if(pickerView.showFromObject == _secondComboBox){

        [self selectSecondComboboxIndex:(int)tIndex title:data];
    }
}

-(void) selectSecondComboboxIndex:(int)indexSecond title:(NSString*) title
{
    [_secondComboBox setTitle:title forState:UIControlStateNormal];
    switch (firstComboIndexSelected) {
        case FilterOption_All:
            filterDetailId = 0;
            break;
        case FilterOption_RoomStatus:
            filterDetailId = ((SupervisorFilterDetail*)listRoomStatusOptions[indexSecond]).filter_id;
            break;
        case FilterOption_RoomType:
            filterDetailId = ((SupervisorFilterDetail*)listRoomTypeOptions[indexSecond]).filter_id;
            break;
        case FilterOption_Zone:
            filterDetailId = ((SupervisorFilterDetail*)listZoneOptions[indexSecond]).filter_id;
            break;
        case FilterOption_Floor:
            filterDetailId = ((SupervisorFilterDetail*)listFloorOptions[indexSecond]).filter_id;
            break;
        default:
            break;
    }

    [self syncHomeView];
}
- (void)clickPTT:(id)sender{
    if (pttNavigationController == nil) {
        [self openPTT];
    }else{
        [self presentViewController:pttNavigationController animated:YES completion:nil];
    }
}
- (void)openPTT{
#if DEFAULT_ENABLE_PTT
    if (openedPTT) {
        return;
    }
    openedPTT = YES;
    eHouseKeepingAppDelegate *appDelegate = (eHouseKeepingAppDelegate*)[[UIApplication sharedApplication] delegate];
    //    HomeViewController *pttController = appDelegate.pttController;
    if (appDelegate.pttController == nil) {
        appDelegate.pttController =[[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:[Utility getPttLibraryBundle]];


    }
    [appDelegate.pttController initBasicInfo:[NSString stringWithFormat:@"%d", [UserManagerV2 sharedUserManager].currentUser.userId] withUserName:[UserManagerV2 sharedUserManager].currentUser.userName withLanguage:[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage]];
    GlobalObject* globalObject = [GlobalObject instance];
    globalObject.eConnectUserId = [NSString stringWithFormat:@"%d", [UserManagerV2 sharedUserManager].currentUser.userId];
    globalObject.currentLanguage = [[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage];
    globalObject.currentTheme = THEME_MHSKP;

    if (appDelegate.pttController.isLogged) {
        if (!pttNavigationController) {
            pttNavigationController = [[UINavigationController alloc] initWithRootViewController:appDelegate.pttController];
            pttNavigationController.navigationBar.translucent = NO;
        }
        [self presentViewController:pttNavigationController animated:YES completion:nil];
        openedPTT = NO;
    }else{
        [MBProgressHUD1 showHUDAddedTo:self.view animated:YES];
        [appDelegate.pttController checkLogin:^(NSString *errMsg) {
            openedPTT = NO;
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD1 hideHUDForView:self.view animated:YES];
            });

            if (errMsg == nil) {
                if (!pttNavigationController) {
                    pttNavigationController = [[UINavigationController alloc] initWithRootViewController:appDelegate.pttController];

                    pttNavigationController.navigationBar.translucent = NO;
                }

                [self presentViewController:pttNavigationController animated:YES completion:nil];
            }else{
                UIAlertView *alert = [[UIAlertView alloc]
                                      initWithTitle:@""
                                      message:errMsg
                                      delegate:nil
                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles:nil];
                [alert show];
            }
        }];
    }
#endif
}
- (void)releasePTT{
#if DEFAULT_ENABLE_PTT
    eHouseKeepingAppDelegate *delegateEconnect = (eHouseKeepingAppDelegate*)[[UIApplication sharedApplication] delegate];
    if (delegateEconnect.pttController) {
        [delegateEconnect.pttController releaseWhenLogout];
    }
    NotifUnregAll(self);
    [self exitPtt];
#endif
}

- (void)exitPtt {
#if DEFAULT_ENABLE_PTT
    eHouseKeepingAppDelegate *appDelegate = (eHouseKeepingAppDelegate*)[[UIApplication sharedApplication] delegate];
    appDelegate.pttController = nil;
    pttNavigationController= nil;
#endif
}

// CFG [20160913/CRF-00001439] - nRoomAssignmentSpecialAssignmentMethod : 0 = No special assignment, 1 = Primary RA only update, 2 = Anyone can update, but must be online.
- (BOOL)validateSpecialAssignment:(int) nKindOfRoom {
    bool bValidateSpecialAssignment = false;
    int nRoomAssignmentSpecialAssignmentMethod = ENUM_ROOM_ASSIGNMENT_SPECIAL_ASSIGNMENT_METHOD_DISABLED;

    @try {
        nRoomAssignmentSpecialAssignmentMethod = [[UserManagerV2 sharedUserManager].currentUserAccessRight getRoomAssignmentSpecialAssignmentMethod];

        if (nRoomAssignmentSpecialAssignmentMethod > ENUM_ROOM_ASSIGNMENT_SPECIAL_ASSIGNMENT_METHOD_DISABLED) { // Special assignment method is enabled.
            if ((nKindOfRoom & ENUM_KIND_OF_ROOM_SPECIAL_ASSIGNMENT) == ENUM_KIND_OF_ROOM_SPECIAL_ASSIGNMENT) {
                if (nRoomAssignmentSpecialAssignmentMethod == ENUM_ROOM_ASSIGNMENT_SPECIAL_ASSIGNMENT_METHOD_PRIMARY_RA_ONLY) { // There is no use for this line, because the default value is already bValidate = false.
                    bValidateSpecialAssignment = false;
                }
                else if ((nRoomAssignmentSpecialAssignmentMethod == ENUM_ROOM_ASSIGNMENT_SPECIAL_ASSIGNMENT_METHOD_BOTH_RA_MUST_BE_ONLINE)
                         && ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp] == YES)) {
                    bValidateSpecialAssignment = true;
                }
            }
            else {
                bValidateSpecialAssignment = true;
            }
        }
        else { // Special assignment method not enabled.
            bValidateSpecialAssignment = true;
        }

        if(!bValidateSpecialAssignment) {
            NSString* sAlertMessage;

            if (nRoomAssignmentSpecialAssignmentMethod == ENUM_ROOM_ASSIGNMENT_SPECIAL_ASSIGNMENT_METHOD_PRIMARY_RA_ONLY) {
                sAlertMessage = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_special_assignment_restriction_you_must_be_a_primary_room_attendant];
            }
            else if (nRoomAssignmentSpecialAssignmentMethod == ENUM_ROOM_ASSIGNMENT_SPECIAL_ASSIGNMENT_METHOD_BOTH_RA_MUST_BE_ONLINE) {
                sAlertMessage = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_special_assignment_restriction_you_must_online];
            }

            UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:imgYesBtn], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:sAlertMessage delegate:nil cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getOK] otherButtonTitles: nil];
            [alert show];
        }
    }
    @catch (NSException* ex) {
        NSLog(@"E>HomeViewV2.mm>validateSpecialAssignment>%@", ex.reason);
    }

    return bValidateSpecialAssignment;
}
// CFG [20160914/CRF-00001439] - End.

// CFG [20161221/CRF-00001557] - nInspectionCrossZoneMethod : 0 = No inspection cross zone, 1 = Primary RA only update, 2 = Anyone can update, but must be online.
- (BOOL)validateInspectionCrossZone:(int) nKindOfRoom {
    bool bValidateInspectionCrossZone = false;
    int nInspectionCrossZoneMethod = ENUM_ROOM_ASSIGNMENT_SPECIAL_ASSIGNMENT_METHOD_DISABLED;

    @try {
        nInspectionCrossZoneMethod = [[UserManagerV2 sharedUserManager].currentUserAccessRight getInspectionCrossZoneMethod];

        if (nInspectionCrossZoneMethod > ENUM_INSPECTION_CROSS_ZONE_METHOD_DISABLED) { // Inspection cross zone method is enabled.
            if ((nKindOfRoom & ENUM_KIND_OF_ROOM_INSPECTION_CROSS_ZONE) == ENUM_KIND_OF_ROOM_INSPECTION_CROSS_ZONE) {
                if (nInspectionCrossZoneMethod == ENUM_INSPECTION_CROSS_ZONE_METHOD_PRIMARY_SUP_ONLY) { // There is no use for this line, because the default value is already bValidate = false.
                    bValidateInspectionCrossZone = false;
                }
                else if ((nInspectionCrossZoneMethod == ENUM_INSPECTION_CROSS_ZONE_METHOD_BOTH_SUP_MUST_BE_ONLINE)
                         && ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp] == YES)) {
                    bValidateInspectionCrossZone = true;
                }
            }
            else {
                bValidateInspectionCrossZone = true;
            }
        }
        else { // Inspection cross zone method not enabled.
            bValidateInspectionCrossZone = true;
        }

        if(!bValidateInspectionCrossZone) {
            NSString* sAlertMessage;

            if (nInspectionCrossZoneMethod == ENUM_INSPECTION_CROSS_ZONE_METHOD_PRIMARY_SUP_ONLY) {
                sAlertMessage = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_inspection_cross_zone_restriction_you_must_be_a_primary_supervisor];
            }
            else if (nInspectionCrossZoneMethod == ENUM_INSPECTION_CROSS_ZONE_METHOD_BOTH_SUP_MUST_BE_ONLINE) {
                sAlertMessage = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_inspection_cross_zone_restriction_you_must_online];
            }

            UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:imgYesBtn], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:sAlertMessage delegate:nil cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getOK] otherButtonTitles: nil];
            [alert show];
        }
    }
    @catch (NSException* ex) {
        NSLog(@"E>HomeViewV2.mm>bValidateInspectionCrossZone>%@", ex.reason);
    }

    return bValidateInspectionCrossZone;
}
// CFG [20160914/CRF-00001439] - End.

@end
