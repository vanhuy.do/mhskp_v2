//
//  LoginViewV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LanguageViewV2.h"
#import "LanguageModel.h"
#import "MBProgressHUD.h"
#include <unistd.h>
#import "UserManagerV2.h"
#import "UserManagerV2Demo.h"
#import "RoomManagerV2.h"
#import "FindManagerV2.h"
#import "AdditionalJobManagerV2.h"
#import "HomeViewV2.h"

@protocol LoginViewDelegate <NSObject>

-(void) addConfigPage;

@end

@interface LoginViewV2 : UIViewController<LanguageViewV2Delegate,UITextFieldDelegate,MBProgressHUDDelegate, UITableViewDelegate, UITableViewDataSource> {
    IBOutlet UITextField *txtUserName;
    IBOutlet UITextField *txtPassWord;
    LanguageViewV2 *languagesView;
    IBOutlet UIButton *btnLanguage;
    BOOL isOpeningLanguageView;
    //RoomInfo *roomInfoView;
    UILabel *buttonLabel;
    MBProgressHUD			*HUD;
    UserManagerV2 *userManager;
    BOOL isExisUser;
    
    NSData *fileData;
    NSString *fileName;
    IBOutlet UILabel *labelLogin;
    IBOutlet UILabel *labelPassword;
    IBOutlet UIButton *buttonLogin;
    //variable to check load languagepack
    BOOL didLoadLanguagePack;
    BOOL languagereload;
    
    id<LoginViewDelegate> delegate;
    
    //Variable for debug mode
    NSMutableArray *listUserDebug;
    UITableView *tableDebug;
    
    IBOutlet UIImageView *bgImage;
    // FM - Percipia
    IBOutlet UIImageView *bgAppLogo;
    IBOutlet UIImageView *bgComLogo;
    
    IBOutlet UILabel *lblWelcomeMsg;
    
}

@property (nonatomic, readwrite) BOOL didLoadLanguagePack;
@property (nonatomic, strong) NSString *fileName;
@property (nonatomic, strong) NSData *fileData;
@property (nonatomic) BOOL isOpeningLanguageView;
@property (nonatomic,strong) IBOutlet UIButton *btnLanguage;
@property (nonatomic,strong) IBOutlet UITextField *txtUserName;
@property (nonatomic,strong) IBOutlet UITextField *txtPassWord;
@property (nonatomic, strong) LanguageViewV2 *languagesView;
@property (nonatomic,strong) UILabel *buttonlabel;
@property (nonatomic ,strong) UserManagerV2 *userManager;
//@property (nonatomic,retain) RoomInfo *roomInfoView;
@property (nonatomic, strong) IBOutlet UILabel *labelLogin;
@property (nonatomic, strong) IBOutlet UILabel *labelPassword;
@property (nonatomic, strong) IBOutlet UIButton *buttonLogin;
@property (strong, nonatomic) IBOutlet UILabel *lblVersion;
@property (nonatomic, readwrite) BOOL languagereload;

//-(IBAction)pressBtnEVC:(id)sender;

-(void)redirectToRoomAssignment;

-(void) setCaptionsView;

-(void) removeText;

-(void) setDelegate:(id<LoginViewDelegate>) delegate;

-(void)updateLanguageSystem;

@end
