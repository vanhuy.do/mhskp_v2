//
//  LoginViewV2.m
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LoginViewV2.h"
//#import "LoginEngine.h"
//#import "LanguageEngine.h"
#import "LanguageManager.h"
#import "NetworkCheck.h"
#import "ehkDefines.h"
#import "CheckMemory.h"
#import "CommonVariable.h"
#import "SettingModelV2.h"
#import "SettingAdapterV2.h"
#import "SettingViewV2.h"
#import "LogFileManager.h"
#import "DeviceManager.h"
#import "ehkDefinesV2.h"
#import "NSFileManager+DoNotBackup.h"
#import "eHouseKeepingAppDelegate.h"

//#define optionButLanguageImg @"a.png"

#define COLOR_LOGIN_BUTTON [UIColor colorWithRed:0.0f green:124/255.0f blue:140/255.0f alpha:1.0f]

@implementation LoginViewV2

//@synthesize roomInfoView;
@synthesize buttonlabel;
@synthesize txtUserName;
@synthesize txtPassWord;
@synthesize languagesView;
@synthesize btnLanguage;
@synthesize isOpeningLanguageView;
@synthesize userManager, fileData, fileName, labelLogin, labelPassword, buttonLogin, didLoadLanguagePack, languagereload;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}



- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

-(void)updateFileLangXMLWithURL:(NSString *)url {
    self.fileData = [NSMutableData data];
    
    NSArray *arr = [url componentsSeparatedByString:@"/"];
    self.fileName = [arr objectAtIndex:[arr count]-1];
    
    NSString *file = [NSString stringWithFormat:@"%@", url];
    NSURL *fileURL = [NSURL URLWithString:[file stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLRequest *req = [NSURLRequest requestWithURL:fileURL];
    NSURLResponse *res = nil;
    self.fileData = [NSURLConnection sendSynchronousRequest:req returningResponse:&res error:nil];
    
    //NSArray *dirArray = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSArray *dirArray = NSSearchPathForDirectoriesInDomains(DataDirectory, NSUserDomainMask, YES);
    
    NSString *path = [NSString stringWithFormat:@"%@/%@", [dirArray objectAtIndex:0], self.fileName];
    if(self.fileData != nil) {
        [[NSFileManager defaultManager] removeItemAtPath:path error:nil];
    }
    //if ([self.fileData writeToFile:path options:NSAtomicWrite error:nil] == NO) {
    if(![self.fileData writeToFile:path atomically:YES]){
        if([LogFileManager isLogConsole]) {
            NSLog(@"Fail to download language");
        }
    }
    else {
        if([LogFileManager isLogConsole]) {
            NSLog(@"success to download language");
        }
    }
}

#pragma mark - View lifecycle
-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    //hide wifi view
    [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationHideWifiView] object:nil];
    [self setCaptionsView];
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self performSelector:@selector(updateLanguageSystem) withObject:nil afterDelay:0.01];
// ADAM [20170929/Change]- To hide the debug table completely
//    if([LogFileManager isLogConsole])
//    {
//        if (tableDebug == nil) {
//            tableDebug = [[UITableView alloc] initWithFrame:CGRectMake(0, 20, 320, 180)];
//            tableDebug.dataSource = self;
//            tableDebug.delegate = self;
//            [tableDebug setBackgroundColor:[UIColor clearColor]];
//            [tableDebug setAlpha:0.6f];
//            [self.view addSubview:tableDebug];
//        }
//        
//        if(listUserDebug == nil){
//            listUserDebug = [[NSMutableArray alloc] init];
//            listUserDebug = [[UserManagerV2 sharedUserManager] loadAllUsers];
//            if([listUserDebug count] > 0)
//            {
//                UserModelV2 *curUser = [listUserDebug objectAtIndex:0];
//                [txtUserName setText:curUser.userName];
//                NSString *keyConfig = [NSString stringWithFormat:@"%@", DEBUG_USER_COMFIG_PASSWORD];
//                NSString *passWord = [[UserManagerV2 sharedUserManager] getConfigValueByUserID:curUser.userId AndKey:keyConfig];
//                [txtPassWord setText:passWord];
//            }
//        }
//    }
    
    //Create RoundRect Style in iOS 7 for button
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        [self loadFlatResource];
    }
    
    //hide wifi view
    [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationHideWifiView] object:nil];
    
    [[CommonVariable sharedCommonVariable] setSuperRoomModel:[[SuperRoomModelV2 alloc] init]];
    
    isExisUser = NO;
    
    txtUserName.tag=1;
    txtPassWord.tag=2;   
    
    [txtUserName setDelegate:self];
    [txtPassWord setDelegate:self];
    txtUserName.text = @"";
    [btnLanguage setTitleEdgeInsets:UIEdgeInsetsMake(0, 3, 0, 18)];
    [btnLanguage.titleLabel setAdjustsFontSizeToFitWidth:YES];
    
    
    //init variable didLoadLanguagePack
    didLoadLanguagePack = NO;
    
    self.isOpeningLanguageView = NO;
    
    if (languagereload != YES) {
        languagereload = NO;
    }
    
    if(![SettingModelV2 isAlreadySetDefaultSetting]) {
        //Set default setting on first load
        [SettingModelV2 setDefaultSetting];
    } else {
        SettingModelV2 *smodel = [[SettingModelV2 alloc] init];
        SettingAdapterV2 *sadapter = [[SettingAdapterV2 alloc] init];
        [sadapter openDatabase];
        [sadapter loadSettingModel:smodel];
        [sadapter close];
        [[LanguageManagerV2 sharedLanguageManager] setCurrentLanguage:smodel.settingsLang];
//        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
//        dispatch_async(queue, ^{
//            // Perform async operation
//            // Call your method/function here
//            
//            [[LanguageManagerV2 sharedLanguageManager] setCurrentLanguage:smodel.settingsLang];
//            dispatch_sync(dispatch_get_main_queue(), ^{
//                // Update UI
//                // Example:
//            });
//        });
        
        
    }
    
    //re-set captions view
    [self setCaptionsView];
}
//-(void)updateLanguageSystem
//{
//    BOOL isAlreadyLoadLanguage = [[NSUserDefaults standardUserDefaults] boolForKey:IS_LOAD_LANGUAGE];
//    if (isAlreadyLoadLanguage) {
//        return;
//    }
//
//    NSMutableArray *languageModels = [[LanguageManagerV2 sharedLanguageManager] loadAllLanguageModel];
//    if (languageModels.count > 0 && isAlreadyLoadLanguage) {
//
//    } else {
//        if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp] == TRUE) {
//
//            HUD = [[MBProgressHUD alloc] initWithView:self.view];
//            [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getRaLoadingData]];
//            HUD.delegate = self;
//            [self.view addSubview:HUD];
//            [HUD show:YES];
//
//            NSArray *arr = [LanguageManagerV2 updateLanguageList];
//            for (NSString *url in arr) {
//                [self updateFileLangXMLWithURL:url];
//            }
//            [[LanguageManagerV2 sharedLanguageManager] setCurrentLanguage:(NSString *)ENGLISH_LANGUAGE];
//            if ([arr count] > 0) {
//                self.languagereload = YES;
//            }
//            //set variable didLoadLanguagePack
//            didLoadLanguagePack = YES;
//
//            //Update UI homeView
//            [[HomeViewV2 shareHomeView] setCaptionsView];
//
//            [HUD hide:YES];
//
//        }
//    }
//}

-(void)updateLanguageSystem
{
    BOOL isAlreadyLoadLanguage = [[NSUserDefaults standardUserDefaults] boolForKey:IS_LOAD_LANGUAGE];
    if (isAlreadyLoadLanguage) {
        return;
    }
    
    NSMutableArray *languageModels = [[LanguageManagerV2 sharedLanguageManager] loadAllLanguageModel];
    if (languageModels.count > 0 && isAlreadyLoadLanguage) {
        
    } else {
        if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp] == TRUE) {
            NSString *wsURL = (NSString *)[[NSUserDefaults standardUserDefaults] objectForKey:KEY_WEBSERVICE_URL];
            if (wsURL.length <= 0) {
                wsURL = eHouseKeepingDefaultWebService;
            }
            
            NSURL *myURL = [NSURL URLWithString: wsURL];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL: myURL];
            request.timeoutInterval = LOGIN_TIMEOUT_CHECK;
            [request setHTTPMethod: @"HEAD"];
            dispatch_async( dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_BACKGROUND, NULL), ^{
                NSURLResponse *response;
                NSError *error;
                NSData *myData = [NSURLConnection sendSynchronousRequest: request returningResponse: &response error: &error];
                BOOL reachable;
                
                if (myData) {
                    // we are probably reachable, check the response
                    reachable=YES;
                } else {
                    // we are probably not reachable, check the error:
                    reachable=NO;
                }
                // now call ourselves back on the main thread
                dispatch_async( dispatch_get_main_queue(), ^{
                    if (reachable) {
                        [self loadLanguage];
                    }
                    
                });
            });
        }
    }
}

- (void)loadLanguage{
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getRaLoadingData]];
    HUD.delegate = self;
    [self.view addSubview:HUD];
    [HUD show:YES];
    
    NSArray *arr = [LanguageManagerV2 updateLanguageList];
    for (NSString *url in arr) {
        [self updateFileLangXMLWithURL:url];
    }
    [[LanguageManagerV2 sharedLanguageManager] setCurrentLanguage:(NSString *)ENGLISH_LANGUAGE];
    if ([arr count] > 0) {
        self.languagereload = YES;
    }
    //set variable didLoadLanguagePack
    didLoadLanguagePack = YES;
    
    //Update UI homeView
    [[HomeViewV2 shareHomeView] setCaptionsView];
    
    [HUD hide:YES];
}

- (IBAction)ChooseLanguage:(id)sender
{
    BOOL checkDidLoad = NO;
    if (self.languagereload == NO) {
        
        [self updateLanguageSystem];
        
//#warning dthanhson edit
//        if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp] == TRUE) {
//            HUD = [[MBProgressHUD alloc] initWithView:self.view];
//            [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getRaLoadingData]];
//            HUD.delegate = self;
//            [self.view addSubview:HUD];
//            [HUD show:YES];
//            
//            NSArray *arr = [LanguageManagerV2 updateLanguageList];
//            for (NSString *url in arr) {
//                [self updateFileLangXMLWithURL:url];
//            }
//            if ([arr count] > 0) {
//                self.languagereload = YES;
//            }
//            //set variable didLoadLanguagePack
//            didLoadLanguagePack = YES;
//            [HUD hide:YES];
//        }
//        
//        checkDidLoad = YES;
//#warning end
    }
    
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    HUD.delegate = self;
    [self.view addSubview:HUD];
    [HUD show:YES];
    
    BOOL languageViewAlloc = NO;
    if(self.languagesView == nil)
    {
        self.isOpeningLanguageView=YES;
        self.languagesView = [[LanguageViewV2 alloc] initWithNibName:@"LanguageViewV2" bundle:nil];
        [self.languagesView.view setFrame:self.view.frame];
        languagesView.delegate=self;
        //languagesView.view.hidden=YES;
        languageViewAlloc = YES;
    }
    
    //[[LanguageManagerV2 sharedLanguageManager] getCurrentDictionaryLanguage];
    
    [self.view addSubview:languagesView.view];

    //reload languageView if needed
    if (self.languagereload == YES && checkDidLoad == YES && languageViewAlloc == NO) {
        [languagesView viewDidLoad];
    }
    [languagesView viewWillAppear:NO];
    
    //set default language
    [languagesView setSelectedRowWithData:[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage]];
    
    // close add keyboad if it is opening
    [self.txtUserName resignFirstResponder];
    [self.txtPassWord resignFirstResponder];
    
    [HUD hide:YES]; 
}

-(void)languageViewController:(LanguageViewV2 *)controller didFinishSetupLanguageWithInfo:(LanguageModel *)languageInfo
{
    self.isOpeningLanguageView=NO;
    
    [btnLanguage setTitle:languageInfo.lang_name forState:UIControlStateNormal];

    [[LanguageManagerV2 sharedLanguageManager] setCurrentLanguage:languageInfo.lang_id];
    
    [self setCaptionsView];
    
    [[HomeViewV2 shareHomeView] setCaptionsView];
    
    //update language id for setting
    SettingModelV2 *smodel = [[SettingModelV2 alloc] init];
    SettingAdapterV2 *sadapter = [[SettingAdapterV2 alloc] init];
    [sadapter openDatabase];
    
    //get current setting
    [sadapter loadSettingModel:smodel];
    [sadapter close];
    
    //update new language id
    SettingModelV2 *smodel2 = [[SettingModelV2 alloc] init];
    smodel2.settingsLang = [NSString stringWithString:languageInfo.lang_id];
    smodel2.settingsLastSync = smodel.settingsLastSync;
    smodel2.settingsNotificationType = smodel.settingsNotificationType;
    smodel2.settingsPhoto = smodel.settingsPhoto;
    smodel2.settingsSynch = smodel.settingsSynch;
    smodel2.settingEnableLog = smodel.settingEnableLog;
    smodel2.row_Id = smodel.row_Id;
    smodel2.settingsWSURL = smodel.settingsWSURL;
    smodel2.settingsLogAmount = smodel.settingsLogAmount;
    smodel2.settingEnableRoomValidate = smodel.settingEnableRoomValidate;
    // CFG [20160927/CRF-00001432] - Load background mode values.
    smodel2.settingsBackgroundMode = smodel.settingsBackgroundMode;
    smodel2.settingsHeartbeatServer = smodel.settingsHeartbeatServer;
    smodel2.settingsHeartbeatPort = smodel.settingsHeartbeatPort;
    // CFG [20160927/CRF-00001432] - End.
    
    [sadapter openDatabase];
    [sadapter updateSettingModel:smodel2];
    [sadapter close];
}

-(void) resetManagers{
    [UserManagerV2Demo updateUserManagerInstance];
    [RoomManagerV2 updateRoomManagerInstance];
    [FindManagerV2 updateRoomManagerInstance];
    [UnassignManagerV2 updateUnassignManagerInstance];
    [AdditionalJobManagerV2 updateAdditionalJobManagerInstance];
}
- (IBAction)Login:(id)sender {
    if ([txtPassWord.text length] <= 0 || [txtUserName.text length] <= 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[[LanguageManagerV2 sharedLanguageManager] getError] 
                                                        message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_MSG_INCORRECT_LOGIN]
                                                       delegate:nil 
                                              cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getOK] 
                                              otherButtonTitles: nil];
        [alert setMessage:[[LanguageManagerV2 sharedLanguageManager] getMsgIncorrectLogin]];
        [alert setTitle:[[LanguageManagerV2 sharedLanguageManager] getError]];
        [alert show];
        return;
    }
    if ([txtPassWord.text isEqualToString:@"123456"] && ([txtUserName.text isEqualToString:@"demora"] || [txtUserName.text isEqualToString:@"demosup"])) {
        
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:IS_LOAD_LANGUAGE];
        [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:ENABLE_DEMO_MODE];

        NSString *selectedLanguage = [[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage];
        [((eHouseKeepingAppDelegate*)[[UIApplication sharedApplication] delegate]) createEditableCopyOfDatabaseIfNeeded];
        [((eHouseKeepingAppDelegate*)[[UIApplication sharedApplication] delegate]) resetAllLanguages];
        //Currently Demo mode only support 2 languages English or Chinese
        if(selectedLanguage.length > 0 && ![selectedLanguage isEqualToString:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]]) {
            [[LanguageManagerV2 sharedLanguageManager] setCurrentLanguage:[NSString stringWithFormat:@"%@", SIMPLIFIED_LANGUAGE]];
        } else {
            [[LanguageManagerV2 sharedLanguageManager] setCurrentLanguage:[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage]];
        }
        [self resetManagers];
    }
    else{
        [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:ENABLE_DEMO_MODE];
        [((eHouseKeepingAppDelegate*)[[UIApplication sharedApplication] delegate]) createEditableCopyOfDatabaseIfNeeded];
        [self resetManagers];
    }
    if ([txtPassWord.text isEqualToString:@"1234567"] && [txtUserName.text isEqualToString:@"config"]) {
        [txtPassWord setText:@""];
        if ([btnLanguage.titleLabel.text isEqualToString: @"English"])
            [[LanguageManagerV2 sharedLanguageManager] setCurrentLanguage:(NSString *)ENGLISH_LANGUAGE];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadCaptionHomeView" object:nil];
        
        [delegate addConfigPage];

        //        [UIView transitionWithView:self.view.superview duration:1
        //                           options:UIViewAnimationOptionTransitionCurlUp
        //                        animations:^ { [self.view removeFromSuperview]; }
        //                        completion:nil];
        
        [self.view removeFromSuperview];
        return;
    }
    
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    
    // Add HUD to screen
    [self.view addSubview:HUD];
    
    // set text Signing in...
    HUD.labelText = [[LanguageManagerV2 sharedLanguageManager] getMsgAuthenticating];
    [HUD show:YES];
    [HUD setDelegate:self];
    BOOL isAuthenticate ;
    if(isDemoMode)
    {
        [[UserManagerV2 sharedUserManager] updateUserData:[txtUserName text] userPassword:[txtPassWord text]];
    }
    else{
        if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
            //login via WS
            [[UserManagerV2 sharedUserManager] updateUserData:[txtUserName text] userPassword:[txtPassWord text]];
        }
    }
    
    //login via local
    isAuthenticate = [[UserManagerV2 sharedUserManager] signIn:[txtUserName text] userPassword:[txtPassWord text]];
    //isAuthenticate = YES;
    
    //CRF-1036: Check to Log out user if other use login same account with different device
    [[HomeViewV2 shareHomeView] startSchedulingUserInformation];
    
    if(isAuthenticate)
    {
        
        //Register push notifications when login success
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
        if ([btnLanguage.titleLabel.text isEqualToString: @"English"]) {
            if ([[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] length] <= 0) {
                [[LanguageManagerV2 sharedLanguageManager] setCurrentLanguage:(NSString *)ENGLISH_LANGUAGE];
            }
        }
        
        //Hao Tran - remove for crash because txtUserName is IBOutlet UITextView, it's not a NSString
        //[[NSUserDefaults standardUserDefaults] setValue:txtUserName forKey:CURRENT_USER_ID];
        //Hao Tran - END
        int isFollowOrder = (int)[[[UserManagerV2 sharedUserManager] currentUser] userAllowReorderRoom];
        [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithInt:isFollowOrder] forKey:IS_FOLLOW_ROOM_ASSIGN_ORDER];
        
        [self performSelector:@selector(redirectToRoomAssignment)  withObject:nil afterDelay:1.0];
    }
    else
    {
        if (![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
            //show no network
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_NO_WIFI] message:nil delegate:nil cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getOK] otherButtonTitles: nil];
            [alert show];
            [HUD hide:YES];
            return;
        }
        [self performSelector:@selector(showErrorMessage)  withObject:nil afterDelay:1.0];
    }
    
}

-(void)redirectToRoomAssignment
{
    [HUD hide:YES];
    
    //Remove for login without animation
    /*
    [UIView transitionWithView:self.view.superview duration:1
                       options:UIViewAnimationOptionTransitionCurlUp
                    animations:^ { [self.view removeFromSuperview]; }
                    completion:nil];
    */
    
    [self.view removeFromSuperview];
    if (isExisUser) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"removeLoginViewV2" object:@"UserExisting"];
    } else {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"removeLoginViewV2" object:@"NoUserExisting"];
    }
    [txtPassWord setText:@""];
}
-(void)showErrorMessage
{
    [HUD hide:YES];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[[LanguageManagerV2 sharedLanguageManager] getError]
                                                    message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_MSG_INCORRECT_LOGIN]
                                                   delegate:nil 
                                          cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getOK] 
                                          otherButtonTitles: nil];
    [alert setTitle:[[LanguageManagerV2 sharedLanguageManager] getError]];
    [alert setMessage:[[LanguageManagerV2 sharedLanguageManager] getMsgIncorrectLogin]];
    [txtPassWord setText:@""];
    [alert show];
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
#pragma mark
#pragma mark textfield delegate
-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    if(textField == txtUserName){
        if(textField.returnKeyType == UIReturnKeyNext) {
            [txtPassWord becomeFirstResponder];
        }
    } else if(textField == txtPassWord) {
        if(textField.returnKeyType == UIReturnKeyDone) {
            [textField resignFirstResponder];
            //login using key board done
            [self Login:txtPassWord];
        }
    }
    return TRUE;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{   
//    NSLog(@"begin editing memory = %@",[[CheckMemory sharedCheckMemory] getMemoryStatus]);
    if(self.isOpeningLanguageView)
    {
        self.isOpeningLanguageView=NO;
        [self.languagesView.view removeFromSuperview];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    
    return (newLength > 20) ? NO : YES;
}
#pragma mark -
#pragma mark MBProgressHUDDelegate methods

- (void)hudWasHidden {
    // Remove HUD from screen when the HUD was hidded
    if (HUD !=nil){
		[HUD removeFromSuperview];
	}
}

#pragma mark - Set Captions
-(void)setCaptionsView {
    [labelLogin setText:[[LanguageManagerV2 sharedLanguageManager] getTitleLogin]];
    [labelPassword setText:[[LanguageManagerV2 sharedLanguageManager] getTitlePassword]];
    [buttonLogin setTitle:[[LanguageManagerV2 sharedLanguageManager] getTitleLogin] forState:UIControlStateNormal];
    [txtUserName setPlaceholder:[[LanguageManagerV2 sharedLanguageManager] getUserID]];
    [txtPassWord setPlaceholder:[[LanguageManagerV2 sharedLanguageManager] getTitlePassword]];
    [lblWelcomeMsg setText:[[LanguageManagerV2 sharedLanguageManager] getTitleWelcomeMsg]];
    
    //hide wifi view
    [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationHideWifiView] object:nil];
    
    //set language name
    LanguageModelV2 *lmodel = [[LanguageModelV2 alloc] init];
    lmodel.lang_id = [[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage];
    [[LanguageManagerV2 sharedLanguageManager] loadLanguageModel:lmodel];
    [btnLanguage setTitle:lmodel.lang_name forState:UIControlStateNormal];
    
    NSDictionary *infoDictionary = [[NSBundle mainBundle]infoDictionary];
    NSString *version = infoDictionary[@"CFBundleShortVersionString"];
    NSString *build = infoDictionary[(NSString*)kCFBundleVersionKey];
    [self.lblVersion setText:[NSString stringWithFormat:@"%@.%@", version, build]];
    
    // Set fonts.
    labelLogin.font = [UIFont fontWithName:@"Montserrat-Medium" size:12];
    txtUserName.font = [UIFont fontWithName:@"Montserrat-Medium" size:12];
    labelPassword.font = [UIFont fontWithName:@"Montserrat-Medium" size:12];
    txtPassWord.font = [UIFont fontWithName:@"Montserrat-Medium" size:12];
    buttonLogin.font = [UIFont fontWithName:@"Montserrat-Medium" size:12];
    btnLanguage.font = [UIFont fontWithName:@"Montserrat-Medium" size:12];
    self.lblVersion.font = [UIFont fontWithName:@"Montserrat-Medium" size:12];
}

#pragma mark - Remove All Text
-(void)removeText {
    [self.txtPassWord setText:@""];
    [self.txtUserName setText:@""];
}

-(void) setDelegate:(id<LoginViewDelegate>) _delegate
{
    delegate = _delegate;
}

//MARK: Debug functions
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [listUserDebug count];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    int rowSelected = (int)indexPath.row;
    UserModelV2 *curUser = [listUserDebug objectAtIndex:rowSelected];
    
    [txtUserName setText:curUser.userName];
    NSString *keyConfig = [NSString stringWithFormat:@"%@", DEBUG_USER_COMFIG_PASSWORD];
    NSString *passWord = [[UserManagerV2 sharedUserManager] getConfigValueByUserID:curUser.userId AndKey:keyConfig];
    [txtPassWord setText:passWord];
    
    [self Login:nil];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellID = @"CELL_DEBUG";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if(cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellID];
    }
    
    UserModelV2 *curUser = [listUserDebug objectAtIndex:indexPath.row];
    NSString *keyConfig = [NSString stringWithFormat:@"%@", DEBUG_USER_COMFIG_PASSWORD];
    NSString *passWord = [[UserManagerV2 sharedUserManager] getConfigValueByUserID:curUser.userId AndKey:keyConfig];
    [cell.detailTextLabel setText:passWord];
    NSString *role = curUser.userSupervisorId > 0 ? @"Supervisor" : @"Maid";
    [cell.textLabel setText: [NSString stringWithFormat:@"%@ - %@", curUser.userName, role]];
    
    return cell;
}

#pragma mark - iOS 7 Resources
-(void)loadFlatResource
{
//    UIToolbar *blurView = [[UIToolbar alloc] initWithFrame:self.view.frame];
//    [blurView setAlpha:0.9f];
//    [blurView setTranslucent:YES];
//    blurView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
//    [self.view insertSubview:blurView atIndex:1]; //Index = 1 because imageview index = 0
    
    //ADAM [20170926/Bug] commented the code to avoid setting the background image
//    NSString *sBGImage;
//    sBGImage = [NSString stringWithFormat:@"%@", imgBgLoginFlat];
//    [bgImage setImage:[UIImage imageNamed:sBGImage]];
    [buttonLogin setBackgroundImage:[UIImage imageNamed:imgLoginButtonFlat] forState:UIControlStateNormal];
}

@end
