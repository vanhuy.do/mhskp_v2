//
//  LFCategoryHeaderView.h
//  mHouseKeeping
//
//  Created by TMS on 1/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LFCategoryHeaderView;

@protocol LFCategoryHeaderViewDelegate <NSObject>

@optional
-(void) sectionHeaderView:(LFCategoryHeaderView*) sectionheaderView sectionOpened:(NSInteger) section;
-(void) sectionHeaderView:(LFCategoryHeaderView*) sectionheaderView sectionClosed:(NSInteger) section;

@end

@interface LFCategoryHeaderView : UIView {
    UILabel *lblCategoryName;
    UIImageView *imgCategory;
    UIImageView *imgArrowSection;
    __unsafe_unretained id<LFCategoryHeaderViewDelegate> delegate;
    UIButton *btnToggle;
    BOOL isToggle;
    NSInteger section;
}

@property BOOL isToggle;
@property NSInteger section;
@property (nonatomic, strong) UILabel *lblCategoryName;
@property (nonatomic, strong) UIImageView *imgCategory;
@property (nonatomic, strong) UIImageView *imgArrowSection;
@property (nonatomic, assign) id<LFCategoryHeaderViewDelegate> delegate;

-(id)initWithSection:(NSInteger) sectionIndex contentName:(NSString *)content AndStatusArrow:(BOOL)isOpened;
-(void)toggleOpenWithUserAction:(BOOL) userAction;

@end
