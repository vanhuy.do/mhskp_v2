//
//  LFCategoryHeaderView.m
//  mHouseKeeping
//
//  Created by TMS on 1/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LFCategoryHeaderView.h"

@interface LFCategoryHeaderView (PrivateMethods)
-(void) toggleOpen:(id)sender;

@end

@implementation LFCategoryHeaderView

@synthesize lblCategoryName,imgCategory,imgArrowSection,delegate, section, isToggle;

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setFrame:CGRectMake(0, 0, 320, 50)];
        lblCategoryName = [[UILabel alloc] initWithFrame:CGRectMake(20, 4, 196, 39)];
        [lblCategoryName setBackgroundColor:[UIColor clearColor]];
        [lblCategoryName setTextColor:[UIColor colorWithRed:255 green:255 blue:255 alpha:1]];
        [lblCategoryName setFont:[UIFont fontWithName:@"Arial-BoldMT" size:15]];
        
        [self setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:imgBackgroundCategory]]];
        
        if(isToggle == YES) {
            imgArrowSection = [[UIImageView alloc]initWithImage:[UIImage imageNamed:imgRightRowOpen]];
        } else {
            imgArrowSection = [[UIImageView alloc]initWithImage:[UIImage imageNamed:imgRightRow]];
        }
        
        [imgArrowSection setFrame:CGRectMake(288, 12, 25, 25)];
        btnToggle = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnToggle setFrame:CGRectMake(0, 0, 320, 50)];
        [btnToggle addTarget:self action:@selector(toggleOpen:) forControlEvents:UIControlEventTouchUpInside];
        
        [self addSubview:lblCategoryName];
        [self addSubview:imgArrowSection];
        [self addSubview:btnToggle];
    }
    
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(id)initWithSection:(NSInteger)sectionIndex contentName:(NSString *)content AndStatusArrow:(BOOL)isOpened {
    self = [self init];
    if (self) {
        section = sectionIndex;
      //  lblContentName.text = content;
        isToggle = isOpened;
    }

    return self;
}

-(void)toggleOpen:(id)sender {
    [self toggleOpenWithUserAction:YES];
}

-(void)toggleOpenWithUserAction:(BOOL)userAction {
    if (userAction) {
        if (isToggle == NO) {
            if ([delegate respondsToSelector:@selector(sectionHeaderView:sectionOpened:)]) {
                isToggle = YES;
                [delegate sectionHeaderView:self sectionOpened:section];                
                [imgArrowSection setImage:[UIImage imageNamed:imgRightRowOpen]];
            }
        } else {
            if ([delegate respondsToSelector:@selector(sectionHeaderView:sectionClosed:)]) {
                isToggle = NO;
                [imgArrowSection setImage:[UIImage imageNamed:imgRightRow]];
                [delegate sectionHeaderView:self sectionClosed:section];
            }
        }
    }
}

@end
