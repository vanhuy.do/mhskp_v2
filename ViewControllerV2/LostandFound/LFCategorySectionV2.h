//
//  LFCategorySectionV2.h
//  mHouseKeeping
//
//  Created by TMS on 1/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LFCatergorySectionHeaderV2.h"
#import "LFCategoryModelV2.h"

@interface LFCategorySectionV2 : NSObject

@property BOOL open;
@property (nonatomic, strong) LFCategoryModelV2 *chkContentType;
@property (nonatomic, strong) LFCatergorySectionHeaderV2 *headerView;
@property (nonatomic, strong) NSMutableArray *rowHeights;

- (NSUInteger)countOfRowHeights;
- (id)objectInRowHeightsAtIndex:(NSUInteger)idx;
- (void)getRowHeights:(id __unsafe_unretained [])buffer range:(NSRange)inRange ;
- (void)insertObject:(id)anObject inRowHeightsAtIndex:(NSUInteger)idx;
- (void)removeObjectFromRowHeightsAtIndex:(NSUInteger)idx;
- (void)replaceObjectInRowHeightsAtIndex:(NSUInteger)idx withObject:(id)anObject;
- (void)insertRowHeights:(NSArray *)rowHeightArray atIndexes:(NSIndexSet *)indexes;
- (void)removeRowHeightsAtIndexes:(NSIndexSet *)indexes;
- (void)replaceRowHeightsAtIndexes:(NSIndexSet *)indexes withRowHeights:(NSArray *)rowHeightArray;
-(void) insertObjectToNextIndex:(id)anObject;

@end
