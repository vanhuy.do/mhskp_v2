//
//  LFCategorySectionV2.m
//  mHouseKeeping
//
//  Created by TMS on 1/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LFCategorySectionV2.h"

@implementation LFCategorySectionV2

@synthesize open, rowHeights,headerView,chkContentType;

- init {	
	self = [super init];
	if (self) {
		rowHeights = [[NSMutableArray alloc] init];
	}

	return self;
}

- (NSUInteger)countOfRowHeights {
	return [rowHeights count];
}

- (void)getRowHeights:(id __unsafe_unretained [])buffer range:(NSRange)inRange {
	[rowHeights getObjects:buffer range:inRange];
}

- (id)objectInRowHeightsAtIndex:(NSUInteger)idx {
	return [rowHeights objectAtIndex:idx];
}

- (void)insertObject:(id)anObject inRowHeightsAtIndex:(NSUInteger)idx {
	[rowHeights insertObject:anObject atIndex:idx];
}

- (void)insertRowHeights:(NSArray *)rowHeightArray atIndexes:(NSIndexSet *)indexes {
	[rowHeights insertObjects:rowHeightArray atIndexes:indexes];
}

- (void)removeObjectFromRowHeightsAtIndex:(NSUInteger)idx {
	[rowHeights removeObjectAtIndex:idx];
}

- (void)removeRowHeightsAtIndexes:(NSIndexSet *)indexes {
	[rowHeights removeObjectsAtIndexes:indexes];
}

- (void)replaceObjectInRowHeightsAtIndex:(NSUInteger)idx withObject:(id)anObject {
	[rowHeights replaceObjectAtIndex:idx withObject:anObject];
}

- (void)replaceRowHeightsAtIndexes:(NSIndexSet *)indexes withRowHeights:(NSArray *)rowHeightArray {
	[rowHeights replaceObjectsAtIndexes:indexes withObjects:rowHeightArray];
}

-(void)insertObjectToNextIndex:(id)anObject {
    [rowHeights addObject:anObject];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	return interfaceOrientation == UIInterfaceOrientationLandscapeLeft ||
    interfaceOrientation == UIInterfaceOrientationLandscapeRight;
}

@end
