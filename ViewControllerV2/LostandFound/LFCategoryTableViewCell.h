//
//  LFCategoryTableViewCell.h
//  mHouseKeeping
//
//  Created by TMS on 2/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol LFCategoryTableViewCellDelegate <NSObject>

@optional
- (void) btnQtyCategoryItemCellPressedWithIndexPath:(NSIndexPath *)indexpath
                                         AndSender:(UIButton *) sender;

@end

@interface LFCategoryTableViewCell : UITableViewCell {
    __unsafe_unretained id<LFCategoryTableViewCellDelegate> delegate;
    NSIndexPath *indexpath;
}

@property (assign, nonatomic) id<LFCategoryTableViewCellDelegate> delegate;
@property (strong, nonatomic) NSIndexPath *indexpath;
@property (retain, nonatomic) IBOutlet UIImageView *imgCategoryItemCell;
@property (retain, nonatomic) IBOutlet UILabel *lblNameCategoryItemCell;
@property (retain, nonatomic) IBOutlet UILabel *lblQtyCategoryItemCell;
@property (retain, nonatomic) IBOutlet UIButton *btnQtyCategoryItemCell;

- (IBAction)btnQtyCategoryItemCellPressed:(id)sender;

@end
