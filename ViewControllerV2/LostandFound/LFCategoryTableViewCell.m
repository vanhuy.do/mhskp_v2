//
//  LFCategoryTableViewCell.m
//  mHouseKeeping
//
//  Created by TMS on 2/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LFCategoryTableViewCell.h"

@implementation LFCategoryTableViewCell
@synthesize  delegate;
@synthesize indexpath;
@synthesize imgCategoryItemCell;
@synthesize lblNameCategoryItemCell;
@synthesize lblQtyCategoryItemCell;
@synthesize btnQtyCategoryItemCell;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)btnQtyCategoryItemCellPressed:(id)sender {
    if ([delegate respondsToSelector:@selector(btnQtyCategoryItemCellPressedWithIndexPath:AndSender:)]) {
        [delegate btnQtyCategoryItemCellPressedWithIndexPath:indexpath 
                                                   AndSender:sender];
    }
}

@end
