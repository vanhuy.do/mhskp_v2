//
//  LFCategoryViewV2.h
//  mHouseKeeping
//
//  Created by TMS on 2/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LFCategoryTableViewCell.h"
#import "LFCategorySectionV2.h"
#import "LFCatergorySectionHeaderV2.h"
#import "PickerViewV2.h"
#import "LFCategoryModelV2.h"
#import "LostandFoundManagerV2.h"
#import "TopbarViewV2.h"
#import "CommonVariable.h"

@protocol LFCategoryView2Delegate <NSObject>

@optional
-(void)callbackLostandFoundObject: (LostandFoundModelV2 *)lafObject atPosition:(NSInteger)selectedPosition;
@end

@interface LFCategoryViewV2 : UIViewController<UITableViewDataSource,UITableViewDelegate, LFCatergorySectionHeaderV2Delegate, PickerViewV2Delegate,LFCategoryTableViewCellDelegate, UISearchBarDelegate> {   
    IBOutlet UITableView *categoryTableView;
    TopbarViewV2 *topBarView;
    
    BOOL isListDown;
    NSMutableArray *sectionInfoArray;
    __unsafe_unretained id <LFCategoryView2Delegate> delegate;
    NSInteger selectedSectionIndex;
    LostandFoundModelV2 *existedLaFObject;  
    IBOutlet UISearchBar *search;
    UIView *backgroundSearchBar;
    UIButton *hideSearchBtn;

    NSMutableArray *listTemp;
}

// Control_Variables For View.
@property (nonatomic, strong) LostandFoundModelV2 *existedLaFObject;
@property (nonatomic, strong) TopbarViewV2 *topBarView;
@property NSInteger selectedSectionIndex;
@property (nonatomic, assign) id <LFCategoryView2Delegate> delegate;
@property (nonatomic, strong) NSMutableArray *sectionInfoArray;
@property (nonatomic, strong) IBOutlet UITableView *categoryTableView;
@property BOOL isListDown;

@property (nonatomic, strong) UIView *backgroundSearchBar;
@property (nonatomic, strong) IBOutlet UIImageView *imageview;
@property (nonatomic, strong) IBOutlet UISearchBar *search;
@property (nonatomic, strong) UIButton *hideSearchBtn;
-(void)loadDataFromDatabase;

@end
