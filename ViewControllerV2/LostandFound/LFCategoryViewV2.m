//
//  LFCategoryViewV2.m
//  mHouseKeeping
//
//  Created by TMS on 2/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LFCategoryViewV2.h"
#import "MyNavigationBarV2.h"
#import "LFItemModelV2.h"
#import "LogFileManager.h"
#import "DeviceManager.h"
#import "ehkDefines.h"
#import "LanguageManagerV2.h"
#import "UserManagerV2.h"
#import "HomeViewV2.h"
#import "CustomAlertViewV2.h"

#define sizeTitle 22
#define isDemoMode ([[NSUserDefaults standardUserDefaults] integerForKey:ENABLE_DEMO_MODE] == 1)

@interface LFCategoryViewV2(PrivateMethods)
-(void) loadTopbarView;
-(void) showTopbarView;
-(void) hideTopbarView;
-(void) setHiddenTopbarView;
-(void) adjustShowForViews;
-(void) adjustRemoveForViews;
-(void) addButtonHandleShowHideTopbar;
-(void) topbarTapped:(UIButton *) sender;
-(TopbarViewV2 *) getTopBarView;
@end

@implementation LFCategoryViewV2
@synthesize categoryTableView,topBarView,isListDown,sectionInfoArray,selectedSectionIndex;
@synthesize delegate;
@synthesize existedLaFObject;
@synthesize imageview;
@synthesize backgroundSearchBar;
@synthesize search;
@synthesize hideSearchBtn;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    
    return self;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle
- (void)viewDidLoad {
    NSInteger userId = [[UserManagerV2 sharedUserManager] currentUser].userId;
    NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
    if([LogFileManager isLogConsole])
    {
        NSLog(@"<%@><UserId:%d><L&F choose Item><ViewDidLoad>", time, (int)userId);
    }
    [super viewDidLoad];
    
    //Resize content views of current VC to fit with Navigation bar and tabbar
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    [self loadDataFromDatabase];

    [self performSelector:@selector(loadTopbarView)];

    [categoryTableView reloadData];
    //search bar
    [imageview setBackgroundColor:[UIColor grayColor]];
    //    [lbltitle setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_the_top_item]];
    
    NSArray *subviews = search.subviews;
    for (id subview in subviews) {
        if ([subview isKindOfClass:NSClassFromString(@"UISearchBarBackground")]) {
            UIView *sview = (UIView *) subview;
            UIView *bg = [[UIView alloc] initWithFrame:sview.frame];
            [bg setFrame:CGRectMake(sview.frame.origin.x, sview.frame.origin.y, 320, 44)];
            //              bg.backgroundColor = [UIColor colorWithRed:254.0/255.0 green:248/255.0 blue:215/255.0 alpha:1.0];
            bg.backgroundColor = [UIColor grayColor];
            [self.search insertSubview:bg aboveSubview:sview];
            self.backgroundSearchBar= bg;
            //            [bg release];
            [sview removeFromSuperview];
        }
    }
    
    search.placeholder = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_SEARCH];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated {
    //show wifi view
    if(!isDemoMode){
        [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationShowWifiView] object:nil];
    }
    
    [super viewWillAppear:YES];
    [categoryTableView reloadData];
}

#pragma mark - uisearchbar Methods
- (void)searchBar:(UISearchBar *)theSearchBar textDidChange:(NSString *)searchText {        
    [listTemp removeAllObjects];// remove all data that belongs to previous search
    
    if(searchText == nil || searchText.length== 0) {
        [listTemp removeAllObjects];
        
        [listTemp addObjectsFromArray:sectionInfoArray];
        isListDown = NO;
        
        @try{            
            [categoryTableView reloadData];
        }    
        @catch(NSException *e){
            
        }
        
        //        [m_searchBar resignFirstResponder];
        //        
        //        m_searchBar.text = @"";
        //        
        //        [subjectTableView reloadData];
        //        
        //        return;
    } else {
        isListDown = YES;
        NSInteger counter = 0;
        LFCategorySectionV2 *msSubjectResutl = [[LFCategorySectionV2 alloc] init];
        
        for (LFCategorySectionV2 *msSubjectSection in sectionInfoArray){
            NSMutableArray *itemArray=msSubjectSection.rowHeights;
            
            for (LFItemModelV2 *subjectItem in itemArray){
                //NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];
                
//                NSRange r = [[subjectItem.eni_name lowercaseString] rangeOfString:[searchText lowercaseString]];
                NSRange r = [[subjectItem.lfitName lowercaseString] rangeOfString:[searchText lowercaseString]];
                
                if(r.location != NSNotFound) {                    
                    if(r.location== 0) {
                        [msSubjectResutl insertObjectToNextIndex:subjectItem];
                    }
                }
                
                counter++;
                
                //                [pool release];
            }            
        }
        
        [listTemp addObject:msSubjectResutl];
        [categoryTableView reloadData];
    }
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [search resignFirstResponder];   
    
}

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    // only show the status bars cancel button while in edit mode
    self.hideSearchBtn=[[UIButton alloc] initWithFrame:CGRectMake(0, 0, 320,340)];
    [self.hideSearchBtn addTarget:self action:@selector(exitSearchBtn:) forControlEvents:UIControlEventTouchUpInside];
    [self.hideSearchBtn setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:hideSearchBtn];
    
    isListDown=YES;
    search.showsCancelButton = NO;
    search.autocorrectionType = UITextAutocorrectionTypeNo;
    
    // flush the previous search content
    
    //    [sectionDataSource removeAllObjects];
    
    
}

-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    if(searchBar.text.length == 0)
    {
        isListDown = NO;
        [categoryTableView reloadData];
    }
}

-(void)exitSearchBtn:(id)sender
{
    isListDown=NO;
//    isListDown = YES;
    
    [search setShowsCancelButton:NO animated:NO];
    [search resignFirstResponder];
    
    if (hideSearchBtn) {
//        [listTemp removeAllObjects];
        [hideSearchBtn removeFromSuperview];
    }
    
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - UITableViewDelegate Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
 
//    return [sectionInfoArray count];
    return [listTemp count];
    
    
    
    
//    if (isListDown) {
//        return 1;
//    }
//    return [sectionInfoArray count];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
//    if (isListDown) {
//        return [listTemp count];
//    }
    
//    LFCategorySectionV2 *sectioninfo = [sectionInfoArray objectAtIndex:section];
    LFCategorySectionV2 *sectioninfo = [listTemp objectAtIndex:section];//edit
    NSInteger numberRowInSection = sectioninfo.rowHeights.count;
    if (isListDown) {
        return numberRowInSection;
    }
    return sectioninfo.open ? numberRowInSection : 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    //edit
    if(isListDown) {
        return  0;
    }
    else {
        return 60;
    }
//    return 60;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {

    LFCategorySectionV2 *sectioninfo = [sectionInfoArray objectAtIndex:section];

    if (sectioninfo.headerView == nil) {
        
        LFCatergorySectionHeaderV2 *sview = [[LFCatergorySectionHeaderV2 alloc] initWithSection:section categoryName:[[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString:[NSString stringWithFormat:@"%@",ENGLISH_LANGUAGE]]?  sectioninfo.chkContentType.lfcName : sectioninfo.chkContentType.lfcLang categoryImage:sectioninfo.chkContentType.lfcImage AndStatusArrow:NO]  ;
        
        sectioninfo.headerView = sview;
        sview.delegate = self;
    }
    
    return sectioninfo.headerView;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *categoryRowIdentifier = @"categoryIdentifier";
    
    LFCategoryTableViewCell *cell = nil;
    cell =(LFCategoryTableViewCell*) [tableView dequeueReusableCellWithIdentifier:categoryRowIdentifier];   
    
    if (cell == nil)  {
        NSArray *array = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([LFCategoryTableViewCell class]) owner:self options:nil];
        cell = [array objectAtIndex:0];
        
        cell.btnQtyCategoryItemCell.layer.cornerRadius = 7.0f;
        cell.btnQtyCategoryItemCell.layer.borderWidth = 1.0f;
        cell.btnQtyCategoryItemCell.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    }
    
    LFCategorySectionV2 *sectionInfo; //= [sectionInfoArray objectAtIndex:indexPath.section];
//    LFItemModelV2 *model = [sectionInfo.rowHeights objectAtIndex:indexPath.row];
//    
//    [cell.imgCategoryItemCell setImage:[UIImage imageWithData:model.lfitImage]];
    //edit
    if(isListDown) {
        sectionInfo = [listTemp objectAtIndex:0];
    } else {
        sectionInfo = [sectionInfoArray objectAtIndex:indexPath.section];
    }
    LFItemModelV2 *model = [sectionInfo.rowHeights objectAtIndex:indexPath.row];
    
    [cell.imgCategoryItemCell setImage:[UIImage imageWithData:model.lfitImage]];
    //edit
    if ([[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString:[NSString stringWithFormat:@"%@",ENGLISH_LANGUAGE]]) {
        cell.lblNameCategoryItemCell.text = model.lfitName;
    } else {
        cell.lblNameCategoryItemCell.text = model.lfitLang;
    }
    
//    if(indexPath.section == 0 && indexPath.row == 0){
//        cell.lblQtyCategoryItemCell.text = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_QUANTITY];
//        [cell.btnQtyCategoryItemCell setFrame:CGRectMake(241, 24, 65, 40)];
//    }
//    else{
        cell.lblQtyCategoryItemCell.text = @"";
        [cell.btnQtyCategoryItemCell setFrame:CGRectMake(241, 15, 65, 40)];
//    }
    
    if(existedLaFObject && existedLaFObject.lafItemId == model.lfitID) {
        [cell.btnQtyCategoryItemCell setTitle:[NSString stringWithFormat:@"%ld",(long)existedLaFObject.laf_item_quantity] forState:UIControlStateNormal];
    } else {
        [cell.btnQtyCategoryItemCell setTitle:[NSString stringWithFormat:@"N/A"] forState:UIControlStateNormal];
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    //[cell setBackgroundColor:[UIColor clearColor]];
    [cell setDelegate:self];
    [cell setIndexpath:indexPath];    
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {    
//        if (sectionInfoArray &&[sectionInfoArray count]>indexPath.section) {
//             LFCategorySectionV2 *sectionDetail=[sectionInfoArray objectAtIndex:indexPath.section]; 
//            NSLog(@"index section is %d and index row is  %d and Lfcategorysection count is %d",indexPath.section,indexPath.row,sectionDetail.rowHeights.count);
//             LFItemModelV2 *selectedItem=[sectionDetail objectInRowHeightsAtIndex:indexPath.row];
//                LFCategoryModelV2 *selectedCategory=sectionDetail.chkContentType;
//            if ([delegate respondsToSelector:@selector(callbackLostandFoundObject:atPosition:)]) {
//                existedLaFObject.lafCategoryId=selectedCategory.lfcID;
//                existedLaFObject.lafItemId=selectedItem.lfitID;
//        
//                
//                [delegate callbackLostandFoundObject:existedLaFObject atPosition:selectedSectionIndex];
//                [self.navigationController popViewControllerAnimated:YES];
//            }                
//        }
    
}

#pragma mark - Section Header Delegate
 -(void)sectionHeaderView:(LFCatergorySectionHeaderV2 *)sectionheaderView sectionOpened:(NSInteger)section {
     LFCategorySectionV2 *sectionInfo = [sectionInfoArray objectAtIndex:section];
     sectionInfo.open = YES;
 
     NSInteger countOfRowsToInsert = [sectionInfo.rowHeights count];
     NSMutableArray *indexPathsToInsert = [[NSMutableArray alloc] init];
     
     if (countOfRowsToInsert == 0) {
         return;
     } else {
         for (NSInteger i = 0; i < countOfRowsToInsert; i++) {
             [indexPathsToInsert addObject:[NSIndexPath indexPathForRow:i
                                                              inSection:section]];
         }
         
         UITableViewRowAnimation insertAnimation;
         insertAnimation = UITableViewRowAnimationTop;
         
         [self.categoryTableView beginUpdates];         
         [self.categoryTableView insertRowsAtIndexPaths:indexPathsToInsert
                                       withRowAnimation:insertAnimation];
         [self.categoryTableView endUpdates];
         
         [self.categoryTableView scrollToRowAtIndexPath:[indexPathsToInsert objectAtIndex:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
     }
     
//     [indexPathsToInsert release];
}
 
-(void)sectionHeaderView:(LFCatergorySectionHeaderV2 *)sectionheaderView sectionClosed:(NSInteger)section {
    LFCategorySectionV2 *sectionInfo = [sectionInfoArray
                                         objectAtIndex:section];

    sectionInfo.open = NO;
    NSInteger countOfRowsToDelete = [self.categoryTableView
                                      numberOfRowsInSection:section];
 
    if (countOfRowsToDelete > 0) {
        NSMutableArray *indexPathsToDelete = [[NSMutableArray alloc] init];
        for (NSInteger i = 0; i < countOfRowsToDelete; i++) {
            [indexPathsToDelete addObject:[NSIndexPath indexPathForRow:i
                                                            inSection:section]];
        }
 
        [self.categoryTableView beginUpdates];
        [self.categoryTableView deleteRowsAtIndexPaths:indexPathsToDelete
                                        withRowAnimation:UITableViewRowAnimationTop];
        [self.categoryTableView endUpdates];
 
//            [indexPathsToDelete release];
    }
}

-(void)loadDataFromDatabase {
    //enable flag to announce, is loading data
    [[HomeViewV2 shareHomeView] waitingLoadingData];
    
    
    if (!self.sectionInfoArray) {
        self.sectionInfoArray = [[NSMutableArray alloc] init];
    }
    
    // load category and all items of its.
    NSMutableArray *categoryArray = [[LostandFoundManagerV2 sharedLostandFoundManagerV2] loadAllCategoryData];
    
    
//    //FOR TESTING//////////////////////////////////////////////////////
//    LFCategoryModelV2 *catTest1 = [[LFCategoryModelV2 alloc] init];
//    catTest1.lfcID = 10;
//    catTest1.lfcName = @"AA";
//    catTest1.lfcLang = @"BB";
//    
//    LFItemModelV2 *itemTest1 = [[LFItemModelV2 alloc] init];
//    itemTest1.lfitID = 101;
//    itemTest1.lfitName = @"A1";
//    itemTest1.lfitLang = @"B1";
//    itemTest1.lafi_category_id = 10;
//    catTest1.lfItem = itemTest1;
//    
//    [categoryArray addObject:catTest1];
//    
//    
//    
//    LFCategoryModelV2 *catTest2 = [[LFCategoryModelV2 alloc] init];
//    catTest2.lfcID = 11;
//    catTest2.lfcName = @"CC";
//    catTest2.lfcLang = @"DD";
//    
//    LFItemModelV2 *itemTest2 = [[LFItemModelV2 alloc] init];
//    itemTest2.lfitID = 111;
//    itemTest2.lfitName = @"C1";
//    itemTest2.lfitLang = @"D1";
//    itemTest2.lafi_category_id = 11;
//    catTest2.lfItem = itemTest2;
//    
//    [categoryArray addObject:catTest2];
    
//    NSInteger lfcId;
//    NSString *lfcName;
//    NSString *lfcLang;
//    NSData *lfcImage;
//    NSString *lfcLastModified;
//    
//    LFItemModelV2 *lfItem;
//    NSData *lfcColor;
    
    
//    NSInteger lfitID;
//    NSString *lfitName;
//    NSString *lfitLang;
//    NSInteger lafi_category_id;
//    NSData *lfitImage;
//    NSString *lfitLastModified;
    ///////////////////////////////////
    
    if ([categoryArray count] > 0) {
        
        for (int i = 0; i < [categoryArray count]; i++) {
            LFCategoryModelV2 *categoryObj = [categoryArray objectAtIndex:i];
            NSMutableArray *itemArray = [[LostandFoundManagerV2 sharedLostandFoundManagerV2] loadAllItemDataByCategoryId:(int)categoryObj.lfcID];
            
//            
//            ///////FOR TESTING
//            
//            LFItemModelV2 *itemTest1 = [[LFItemModelV2 alloc] init];
//            itemTest1.lfitID = 101;
//            itemTest1.lfitName = @"A1";
//            itemTest1.lfitLang = @"B1";
//            itemTest1.lafi_category_id = 10;
//            
//            [itemArray addObject:itemTest1];
//            
//            /////////////
            
            LFCategorySectionV2 *section1 = [[LFCategorySectionV2 alloc] init];
            section1.chkContentType = categoryObj;
//            listTemp = [[NSMutableArray alloc] init];
            
            for (int j = 0; j < [itemArray count]; j++) {
                LFItemModelV2 *itemObj = [itemArray objectAtIndex:j];
               
                
                
                [section1 insertObjectToNextIndex:itemObj];
//                [listTemp addObject:itemObj];
            }
            
            [sectionInfoArray addObject:section1];
        }        
        listTemp = [NSMutableArray arrayWithArray:sectionInfoArray];//add
    }
    
    
    //end loading data
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
}

#pragma mark - CategoryItemCellV2 Delegate Methods
- (void) btnQtyCategoryItemCellPressedWithIndexPath:(NSIndexPath *)indexpath
                                          AndSender:(UIButton *) sender{
    PickerViewV2 *picker = [[PickerViewV2 alloc] initPickerViewV2WithDatas:[NSArray arrayWithObjects:@"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9", @"10" ,nil] AndSelectedData:sender.titleLabel.text AndIndex:indexpath];
    picker.delegate = self;
    [picker showPickerViewV2];   
}

#pragma mark - PickerViewV2Delegate
-(void)didChoosePickerViewV2WithData:(NSString *) data AndIndexItemRow:(NSIndexPath *) index {

    if (isListDown) {
        
        LFCategorySectionV2 *sectionInfo = [listTemp objectAtIndex:index.section];
        LFItemModelV2 *selectedItem = [sectionInfo.rowHeights objectAtIndex:index.row];
        
        LFCategorySectionV2 *sectionToFind = nil;
        for (LFCategorySectionV2 *currentSection in sectionInfoArray) {
            for (LFItemModelV2 *currentCate in currentSection.rowHeights) {
                if (selectedItem == currentCate) {
                    sectionToFind = currentSection;
                    break;
                }
                if (sectionToFind != nil) {
                    break;
                }
            }
        }
        LFCategoryModelV2 *selectedCategory = sectionToFind.chkContentType;
        
        if ([delegate respondsToSelector:@selector(callbackLostandFoundObject:atPosition:)]) {
            
            LostandFoundModelV2 *lf = [[LostandFoundModelV2 alloc] init];
            
            if (existedLaFObject.lafItemId == selectedItem.lfitID && existedLaFObject.lafCategoryId == selectedCategory.lfcID && existedLaFObject.laf_item_quantity == [data integerValue]) {
                
            } 
            else {
                existedLaFObject.lafCategoryId = selectedCategory.lfcID;
                existedLaFObject.lafItemId = selectedItem.lfitID;
                existedLaFObject.lafCat = selectedCategory;
                existedLaFObject.lafItem = selectedItem;
                existedLaFObject.laf_item_quantity = [data integerValue];
                
                if (existedLaFObject.laf_Status == inDataBase) {
                    existedLaFObject.laf_Status = isNeedUpdate;
                }
                
                
                lf.lafCategoryId = existedLaFObject.lafCategoryId;//=0
                lf.lafItemId = existedLaFObject.lafItemId;
                lf.lafCat = existedLaFObject.lafCat;
                lf.lafItem = existedLaFObject.lafItem;
                lf.laf_item_quantity = existedLaFObject.laf_item_quantity;
                lf.laf_Status = existedLaFObject.laf_Status;
                lf.lafColor = existedLaFObject.lafColor;
                
            }
            
            [delegate callbackLostandFoundObject:lf atPosition:selectedSectionIndex];
        }
        [self.navigationController popViewControllerAnimated:YES];
        
    }
    else {
        
        LFCategorySectionV2 *sectionInfo = [sectionInfoArray objectAtIndex:index.section];
        LFCategoryModelV2 *selectedCategory = sectionInfo.chkContentType;
        LFItemModelV2 *selectedItem = [sectionInfo.rowHeights objectAtIndex:index.row];
        
        if ([delegate respondsToSelector:@selector(callbackLostandFoundObject:atPosition:)]) {
            
            LostandFoundModelV2 *lf = [[LostandFoundModelV2 alloc] init];
            
            if (existedLaFObject.lafItemId == selectedItem.lfitID && existedLaFObject.lafCategoryId == selectedCategory.lfcID && existedLaFObject.laf_item_quantity == [data integerValue]) {
                
            } 
            else {
                existedLaFObject.lafCategoryId = selectedCategory.lfcID;
                existedLaFObject.lafItemId = selectedItem.lfitID;
                existedLaFObject.lafCat = selectedCategory;
                existedLaFObject.lafItem = selectedItem;
                existedLaFObject.laf_item_quantity = [data integerValue];
                
                if (existedLaFObject.laf_Status == inDataBase) {
                    existedLaFObject.laf_Status = isNeedUpdate;
                }
                
                
                lf.lafCategoryId = existedLaFObject.lafCategoryId;
                lf.lafItemId = existedLaFObject.lafItemId;
                lf.lafCat = existedLaFObject.lafCat;
                lf.lafItem = existedLaFObject.lafItem;
                lf.laf_item_quantity = existedLaFObject.laf_item_quantity;
                lf.laf_Status = existedLaFObject.laf_Status;
                lf.lafColor = existedLaFObject.lafColor;
                
            }
            
            [delegate callbackLostandFoundObject:lf atPosition:selectedSectionIndex];
        }
        
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    // [categoryTableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:index] withRowAnimation:UITableViewRowAnimationFade];
}


#pragma mark - Handle Topbar Methods
-(void)loadTopbarView {
    TopbarViewV2 *topbar = [[TopbarViewV2 alloc] initViewWithSuperModel:[[CommonVariable sharedCommonVariable] superRoomModel]];
    [topbar setTag:tagTopbarView];
    
    [self.view addSubview:topbar];
    [topbar setHidden:YES];
    [topbar setAlpha:0.0];
    [self addButtonHandleShowHideTopbar];
}

-(void)showTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationWillStartSelector:@selector(setHiddenTopbarView)];
    
    f = topbar.frame;
    f.origin.y = 0;
    [topbar setFrame:f];
    [topbar setAlpha:1.0];
    
    [self adjustShowForViews];
    
    [UIView commitAnimations];
}

-(void)hideTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDidStopSelector:@selector(setHiddenTopbarView)];
    [UIView setAnimationDelegate:self];
    
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    [topbar setAlpha:0.0];
    
    [self adjustRemoveForViews];
    
    [UIView commitAnimations];
}

-(void)setHiddenTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    [topbar setHidden:!topbar.hidden];
}

-(TopbarViewV2 *)getTopBarView {
    return (TopbarViewV2 *)[self.view viewWithTag:tagTopbarView];
}

-(void)addButtonHandleShowHideTopbar {
    
    UIView *vBarButton = [[UIView alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
    
    //        UIButton *titleBarButton = [[UIButton alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
    UIButton *titleBarButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 200, 30)];
    titleBarButton.titleLabel.backgroundColor = [UIColor clearColor];
    titleBarButton.titleLabel.font = FONT_BUTTON_TOPBAR;
    titleBarButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    titleBarButton.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
    [titleBarButton.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
    [titleBarButton.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
    [titleBarButton addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    [titleBarButton setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_Counts] forState:UIControlStateNormal];
    //        self.navigationItem.titleView = titleBarButton;
    [vBarButton addSubview:titleBarButton];
    
    
    UIButton *titleBarButtonSecond = [[UIButton alloc] initWithFrame:CGRectMake(0, 28, 200, 22)];
    [titleBarButtonSecond setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [titleBarButtonSecond.titleLabel setFont:FONT_BUTTON_TOPBAR_SECOND];
    [titleBarButtonSecond setTitle:[L_LOST_FOUND currentKeyToLanguage] forState:UIControlStateNormal];
    [titleBarButtonSecond addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    [vBarButton addSubview:titleBarButtonSecond];
    
    self.navigationItem.titleView = vBarButton;
    
    //back button navigation
    self.navigationItem.leftBarButtonItem = [MyNavigationBarV2 createBackButtonWithTarget:self selector:@selector(btnBackPressed) forControlEvents:UIControlEventTouchUpInside];
    
    //Move title to center vertical
    CGRect titleViewBounds = self.navigationItem.titleView.bounds;
    int moveLeftTitle = 0;
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        moveLeftTitle = 10;
    }
    self.navigationItem.titleView.bounds = CGRectMake(titleViewBounds.origin.x + moveLeftTitle, Y_TOPBAR_TITLE, titleViewBounds.size.width, titleViewBounds.size.height);

}

-(void)btnBackPressed
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)topbarTapped:(UIButton *)sender {
    TopbarViewV2 *topbar = [self getTopBarView];
    [sender setEnabled:NO];
    if (topbar.isHidden) {
        [self showTopbarView];
    } else {
        [self hideTopbarView];
    }
    [self performSelector:@selector(topbarButtonEnable:) withObject:sender 
               afterDelay:0.5];
}

-(void) topbarButtonEnable:(UIButton *) topbarBtn {
    [topbarBtn setEnabled:YES];
}

-(void)adjustShowForViews {
    //adjust all views when show 
    //[categoryTableView setFrame:CGRectMake(0, 45, 320, 430)];
    
    TopbarViewV2 *topbar = [self getTopBarView];
    
    CGRect searchFrame = search.frame;
    CGRect tableFrame = categoryTableView.frame;
    
    searchFrame.origin.y = topbar.frame.size.height;
    tableFrame.origin.y = searchFrame.origin.y + searchFrame.size.height;
    tableFrame.size.height = tableFrame.size.height - topbar.frame.size.height;
    
    [search setFrame:searchFrame];
    [categoryTableView setFrame:tableFrame];
}

-(void)adjustRemoveForViews { 
    //adjust all views when hide
    //[categoryTableView setFrame:CGRectMake(0, 0, 320, 460)];
    
    TopbarViewV2 *topbar = [self getTopBarView];
    
    CGRect searchFrame = search.frame;
    CGRect tableFrame = categoryTableView.frame;
    
    searchFrame.origin.y = 0;
    tableFrame.origin.y = searchFrame.origin.y + searchFrame.size.height;
    tableFrame.size.height = tableFrame.size.height + topbar.frame.size.height;
    
    [search setFrame:searchFrame];
    [categoryTableView setFrame:tableFrame];
}

#pragma mark -  Hidden Header view when tap on navigationBar
#define heightScale 45
- (void) hiddenHeaderView {    
    BOOL isHidden = topBarView.hidden;
    [topBarView setHidden:!isHidden];
    
    CGRect frame1 = CGRectZero;
    
    if(!isHidden){
        frame1  = CGRectMake(categoryTableView.frame.origin.x, categoryTableView.frame.origin.y - heightScale, categoryTableView.frame.size.width, categoryTableView.frame.size.height+heightScale);
    } else {
        frame1  = CGRectMake(categoryTableView.frame.origin.x, categoryTableView.frame.origin.y +heightScale, categoryTableView.frame.size.width, categoryTableView.frame.size.height-heightScale);
    }
    
    [categoryTableView setFrame:frame1];
}

#pragma mark - === Handle Check Save Data ===
#pragma mark
-(BOOL) checkSaveDataWithTagEvent:(NSInteger) tagOfEvent {
    //can go to message screen
    if (tagOfEvent == tagOfMessageButton) {
        //set status room is not completed
        [[CommonVariable sharedCommonVariable] setRoomIsCompleted:0];
        
        return NO;
    }
    
    //show please complete the room
    UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_complete_room_to_leave] delegate:nil cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles: nil];   
    [alert show];
    
    return YES; //no need to save before leaving, but need to complete the room before leaving
}

@end
