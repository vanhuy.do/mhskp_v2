//
//  LFCatergorySectionHeaderV2.h
//  mHouseKeeping
//
//  Created by TMS on 1/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol LFCatergorySectionHeaderV2Delegate;

@interface LFCatergorySectionHeaderV2 : UIView {
    UILabel *lblCategoryName;
    UIImageView *imgBackgroundView;
    UIImageView *imgCategory;
    UIImageView *imgArrowSection;
    __unsafe_unretained id<LFCatergorySectionHeaderV2Delegate> delegate;
    UIButton *btnToggle;
    BOOL isToggle;
    NSInteger section;
}

@property (nonatomic, strong) UIImageView *imgBackgroundView;
@property (nonatomic, strong) UILabel *lblCategoryName;
@property (nonatomic, strong) UIImageView *imgCategory;
@property (nonatomic, strong) UIImageView *imgArrowSection;
@property (nonatomic, assign) id<LFCatergorySectionHeaderV2Delegate> delegate;
@property (nonatomic, strong) UIButton *btnToggle;
@property BOOL isToggle;
@property NSInteger section;

-(void)toggleOpenWithUserAction:(BOOL)userAction;
-(id)initWithSection:(NSInteger)sectionIndex categoryName:(NSString *)catName categoryImage: (NSData*)catImage AndStatusArrow:(BOOL)isOpened;
@end

@protocol LFCatergorySectionHeaderV2Delegate<NSObject>

@optional
-(void) sectionHeaderView:(LFCatergorySectionHeaderV2*) sectionheaderView sectionOpened:(NSInteger) section;
-(void) sectionHeaderView:(LFCatergorySectionHeaderV2*) sectionheaderView sectionClosed:(NSInteger) section;

@end
