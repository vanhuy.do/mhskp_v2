//
//  LFCatergorySectionHeaderV2.m
//  mHouseKeeping
//
//  Created by TMS on 1/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LFCatergorySectionHeaderV2.h"
#import "UIImage+Tint.h"
#import "ehkDefinesV2.h"

@implementation LFCatergorySectionHeaderV2

@synthesize lblCategoryName,isToggle,section,imgCategory,imgArrowSection,btnToggle,delegate,imgBackgroundView;

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setFrame:CGRectMake(0, 0, 320, 50)];
        imgCategory = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,75,60)];
        lblCategoryName = [[UILabel alloc] initWithFrame:CGRectMake(75, 10, 200, 39)];
        [lblCategoryName setBackgroundColor:[UIColor clearColor]];
        [lblCategoryName setTextColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:1]];
        [lblCategoryName setFont:[UIFont fontWithName:@"Arial-BoldMT" size:15]];
        [lblCategoryName setMinimumFontSize:15];
        lblCategoryName.textAlignment=NSTextAlignmentCenter ;
        
        [self setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageBeforeiOS7:imgBgBigBtn equaliOS7:imgBgBigBtnFlat]]];
        
        if(isToggle == YES) {
            imgArrowSection = [[UIImageView alloc]initWithImage:[UIImage imageBeforeiOS7:imgRightRowOpen equaliOS7:imgRightRowOpenFlat]];
        } else {
            imgArrowSection = [[UIImageView alloc]initWithImage:[UIImage imageBeforeiOS7:imgRightRow equaliOS7:imgRightRowFlat]];
        }
        
        [imgArrowSection setFrame:CGRectMake(288, 17, 25, 25)];
        btnToggle = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnToggle setFrame:CGRectMake(0, 0, 320, 50)];
        [btnToggle addTarget:self action:@selector(toggleOpen:) forControlEvents:UIControlEventTouchUpInside];
        
        [self addSubview:imgCategory];
        [self addSubview:lblCategoryName];
        [self addSubview:imgArrowSection];
        [self addSubview:btnToggle];
    }

    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(void)toggleOpen:(id)sender {
    [self toggleOpenWithUserAction:YES];
}

-(void)toggleOpenWithUserAction:(BOOL)userAction {
    if (userAction) {
        if (isToggle == NO) {
            if ([delegate respondsToSelector:@selector(sectionHeaderView:sectionOpened:)]) {
                isToggle = YES;
                [delegate sectionHeaderView:self sectionOpened:section];                
                [imgArrowSection setImage:[UIImage imageBeforeiOS7:imgRightRowOpen equaliOS7:imgRightRowOpenFlat]];
            }
        }else {
            if ([delegate respondsToSelector:@selector(sectionHeaderView:sectionClosed:)]) {
                isToggle = NO;
                [imgArrowSection setImage:[UIImage imageBeforeiOS7:imgRightRow equaliOS7:imgRightRowFlat]];
                [delegate sectionHeaderView:self sectionClosed:section];
            }
        }
    }
}

-(id)initWithSection:(NSInteger)sectionIndex categoryName:(NSString *)catName categoryImage: (NSData *)catImage AndStatusArrow:(BOOL)isOpened {
    self = [self init];
    if (self) {
        self.section = sectionIndex;
        self.lblCategoryName.text = catName;
        [self.imgCategory setImage:[UIImage imageWithData:catImage]];
        self.isToggle = isOpened;
    }

    return self;
}

@end
