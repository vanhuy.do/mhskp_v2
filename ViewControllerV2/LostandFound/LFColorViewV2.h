//
//  LFColorViewV2.h
//  mHouseKeeping
//
//  Created by TMS on 2/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LFColorModelV2.h"
#import "LostandFoundManagerV2.h"
#import "QuartzCore/QuartzCore.h"

@protocol LFColorViewV2Delegate <NSObject>

@optional
//-(void)transfer:(LostandFoundModelV2 *)lafData atPosition: (NSInteger) sectionIndex;//modify
-(void)transferTheValueOfTableView:(LFColorModelV2 *)lafData atPosition: (NSInteger) sectionIndex;

@end

@interface LFColorViewV2 : UIViewController {
    NSMutableArray *colorArray;
    __unsafe_unretained id <LFColorViewV2Delegate>delegate;
    LostandFoundModelV2 *existedLaFObject;
    NSInteger selectedSection;
}

@property (nonatomic, strong) LostandFoundModelV2 *existedLaFObject;
@property NSInteger selectedSection;
@property (nonatomic, assign) id <LFColorViewV2Delegate> delegate;
@property (nonatomic, strong) NSMutableArray *colorArray;

-(void)displayColorButtons;
-(void)selectColorButton:(id)sender;

@end
