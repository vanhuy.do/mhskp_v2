//
//  LFColorViewV2.m
//  mHouseKeeping
//
//  Created by TMS on 2/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LFColorViewV2.h"
#import "ehkDefines.h"
#import "MyNavigationBarV2.h"
#import "DeviceManager.h"
#import "CustomAlertViewV2.h"

#define STARLOCATIONX 13
#define STARLOCATIONY 110   //88
#define BUTTONSIZE 70
#define BUTTONCAP 5
#define YELLOWCOLOR @"yellow.png"
#define REDCOLOR @"Red.png"
#define PURPLECOLOR @"Purple.png"
#define WHITECOLOR @"white.png"
#define PINKCOLOR @"pink.png"
#define ORANGECOLOR @"orange.png"
#define LIGHTGREENCOLOR @"light_green.png"
#define GREENCOLOR @"green.png"
#define DARKBLUECOLOR @"dack_blue.png"
#define BLUECOLOR @"blue.png"
#define GRAYCOLOR @"gray.png"
#define BLACKCOLOR @"black.png"
#define COLORSELECTION @"Colour Selection"
#define UIColorFromRGB(rgbValue) \
[UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0x00FF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0x0000FF))/255.0 \
alpha:1.0]
#define isDemoMode ([[NSUserDefaults standardUserDefaults] integerForKey:ENABLE_DEMO_MODE] == 1)

@implementation LFColorViewV2
@synthesize colorArray;
@synthesize delegate;
@synthesize selectedSection;
@synthesize existedLaFObject;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //[self addButtonHandleShowHideTopbar];
    
    //Resize content views of current VC to fit with Navigation bar and tabbar
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    [self displayColorButtons];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[self navigationController] setNavigationBarHidden:YES];
    
    //show wifi view
    if(!isDemoMode){
        [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationShowWifiView] object:nil];
    }
    
    //back button navigation
    self.navigationItem.leftBarButtonItem = [MyNavigationBarV2 createBackButtonWithTarget:self selector:@selector(btnBackPressed) forControlEvents:UIControlEventTouchUpInside];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    //show wifi view
    if(!isDemoMode){
        [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationShowWifiView] object:nil];
    }
    [[self navigationController] setNavigationBarHidden:NO];
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void)btnBackPressed
{
    [self.navigationController popViewControllerAnimated:NO];
}

-(void)addButtonHandleShowHideTopbar {
    UIView *vBarButton = [[UIView alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
    UIButton *titleBarButtonFirst = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 200, 30)];
    
    titleBarButtonFirst.titleLabel.backgroundColor = [UIColor clearColor];
    [titleBarButtonFirst.titleLabel setFont: FONT_BUTTON_TOPBAR];
    titleBarButtonFirst.titleLabel.textAlignment = NSTextAlignmentCenter;
    titleBarButtonFirst.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
    
    [titleBarButtonFirst.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
    [titleBarButtonFirst.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
    [titleBarButtonFirst addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    [titleBarButtonFirst setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_Counts] forState:UIControlStateNormal];
    
    [vBarButton addSubview:titleBarButtonFirst];
    
    UIButton *titleBarButtonSecond = [[UIButton alloc] initWithFrame:CGRectMake(0, 28, 200, 22)];
    [titleBarButtonSecond setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [titleBarButtonSecond.titleLabel setFont:FONT_BUTTON_TOPBAR_SECOND];
    [titleBarButtonSecond setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_LOSTANDFOUND_CASE_TITLE] forState:UIControlStateNormal];
    [titleBarButtonSecond addTarget:self action:@selector(btnBackPressed) forControlEvents:UIControlEventTouchUpInside];
    [vBarButton addSubview:titleBarButtonSecond];
    
    self.navigationItem.titleView = vBarButton;
    
    //Move title to center vertical
    CGRect titleViewBounds = self.navigationItem.titleView.bounds;
    int moveLeftTitle = 0;
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        moveLeftTitle = 10;
    }
    self.navigationItem.titleView.bounds = CGRectMake(titleViewBounds.origin.x + moveLeftTitle, Y_TOPBAR_TITLE, titleViewBounds.size.width, titleViewBounds.size.height);
}

-(void)displayColorButtons {   
    //enable flag to announce, is loading data
    [[HomeViewV2 shareHomeView] waitingLoadingData];
    
    if (!colorArray) {
        self.colorArray = [[NSMutableArray alloc] init];
    }
    
    self.colorArray = [[LostandFoundManagerV2 sharedLostandFoundManagerV2] loadAllColors];
    
    UILabel *titleViewLabel = [[UILabel alloc] initWithFrame:CGRectMake(STARLOCATIONX,STARLOCATIONY - 43, 150, 25)];
    [titleViewLabel setBackgroundColor:[UIColor clearColor]];
    [titleViewLabel setTextColor:[UIColor whiteColor]];
    [titleViewLabel setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostanfound_colour_selection]];
    //[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:lostanfound_colour_selection]
    
    [self.view addSubview:titleViewLabel];
    
    //Room title
    UILabel *labelRoom = [[UILabel alloc] initWithFrame:CGRectMake(0, STARLOCATIONY - 68, self.view.bounds.size.width, 24)];
    [labelRoom setBackgroundColor:[UIColor lightGrayColor]];
    [labelRoom setTextColor:[UIColor whiteColor]];
    [labelRoom setText: [NSString stringWithFormat:@"   %@",[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOM_TITLE]]];
    [self.view addSubview:labelRoom];
    
    //Lost And Found Title
    UILabel *labelLostAndFound = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, self.view.bounds.size.width - 5, 34)];
    [labelLostAndFound setBackgroundColor:[UIColor clearColor]];
    [labelLostAndFound setTextColor:[UIColor whiteColor]];
    [labelLostAndFound setText: [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_LOSTANDFOUND_CASE_TITLE]];
    [labelLostAndFound setFont: FONT_BUTTON_TOPBAR];
    [self.view addSubview:labelLostAndFound];
    
    CGFloat xLocation = 0;
    CGFloat yLocation = 0;
    
    for (int i = 0; i < [colorArray count]; i++) {
        if (i%4 != 0) {            
            xLocation += BUTTONCAP + BUTTONSIZE;
        } else {
            if (i==0) {
                xLocation = STARLOCATIONX;
                yLocation = STARLOCATIONY;
            } else {
                xLocation = STARLOCATIONX;
                yLocation += BUTTONCAP + BUTTONSIZE;
            }            
        }
        
        LFColorModelV2 *colorData = [colorArray objectAtIndex:i];
        unsigned result = 0;
        NSScanner *scanner = [NSScanner scannerWithString:colorData.htmlCode];
        [scanner setScanLocation:1];
        
        // bypass '#' character
        [scanner scanHexInt:&result];
        
        UIColor *colorName = UIColorFromRGB(result);
        UIButton *colorButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [colorButton setFrame:CGRectMake(xLocation, yLocation, 
                                         BUTTONSIZE, BUTTONSIZE)];
        [colorButton setTag:i];
        [[colorButton layer] setCornerRadius:8.0f];
        [[colorButton layer] setBorderWidth:1.0f];
        [[colorButton layer] setBorderColor:[[UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:1] CGColor] ];
        [[colorButton layer] setMasksToBounds:YES];
        [[colorButton layer] setBackgroundColor:[colorName CGColor]];
        [[colorButton layer] setNeedsDisplay];   
        
        [colorButton addTarget:self action:@selector(selectColorButton:) forControlEvents:UIControlEventTouchUpInside];
        
        if (colorData.lfcID == existedLaFObject.lafColorId) {
            [colorButton setSelected:YES];
            [colorButton setHighlighted:YES];
            [[colorButton layer] setBounds:CGRectMake(xLocation, yLocation, 75, 75)];
            [[colorButton layer] setBorderWidth:8.0f];
            [[colorButton layer] setBorderColor:[[UIColor colorWithRed:127/255.0 green:192/255.0 blue:255/255.0 alpha:1] CGColor]];
        } else {
            [colorButton setSelected:NO];
            [colorButton setHighlighted:NO];
        }
        [self.view addSubview:colorButton];            
    }
    
    yLocation += BUTTONCAP + BUTTONSIZE;
    
    //Add close button
    UIButton *btnClose = [[UIButton alloc] initWithFrame:CGRectMake(40, yLocation + 40, self.view.bounds.size.width - 80, 30)];
    [btnClose setBackgroundImage:[UIImage imageNamed:@"bt_gray_299x58.png"] forState:UIControlStateNormal];
    [btnClose setTitle:[L_CLOSE currentKeyToLanguage] forState:UIControlStateNormal];
    [btnClose addTarget:self action:@selector(btnBackPressed) forControlEvents:UIControlEventTouchUpInside];
    [btnClose setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:btnClose];
    
    /*
    lblSubmit = [[UILabel alloc] initWithFrame:CGRectMake(110, 10, 163, 35)];
    lblSubmit.font = [UIFont boldSystemFontOfSize:36];//29
    lblSubmit.text = @"Submit";
    lblSubmit.tag = LABEL_SUBMIT_TAG;
    lblSubmit.backgroundColor = [UIColor clearColor];
    lblSubmit.textColor = [UIColor whiteColor];
    [cell.contentView addSubview:lblSubmit];
    (*/
    
    //[self.view setBackgroundColor:[UIColor colorWithRed:102.0f/255.0f green:102.0f/255.0f blue:102.0f/255.0f alpha:0.8]];
    
    //end loading data
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
}

-(void)selectColorButton:(id)sender {   
    UIButton *buttonColor = (UIButton *) sender;
    LFColorModelV2 *selectedColor = [colorArray objectAtIndex:buttonColor.tag];
    
      if (selectedColor) {
        if (selectedColor.lfcID == existedLaFObject.lafColorId) {
            
        } else {
            existedLaFObject.lafColorId = selectedColor.lfcID;
            existedLaFObject.lafColor = selectedColor;
            if (existedLaFObject.laf_Status == inDataBase) {
                 existedLaFObject.laf_Status = isNeedUpdate;
            }
        }
          
        if ([delegate respondsToSelector:@selector(transferTheValueOfTableView:atPosition:)]) {
//            [delegate transferTheValueOfTableView:existedLaFObject atPosition:selectedSection];
            [delegate transferTheValueOfTableView:selectedColor atPosition:selectedSection];
        }
    }
    
    [self.navigationController popViewControllerAnimated:NO];
}

#pragma mark - === Handle Check Save Data ===
#pragma mark
-(BOOL) checkSaveDataWithTagEvent:(NSInteger) tagOfEvent {
    //can go to message screen
    if (tagOfEvent == tagOfMessageButton) {
        //set status room is not completed
        [[CommonVariable sharedCommonVariable] setRoomIsCompleted:0];
        
        return NO;
    }
    
    //show please complete the room
    UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_complete_room_to_leave] delegate:nil cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles: nil];   
    [alert show];
    
    return YES; //no need to save before leaving, but need to complete the room before leaving
}

@end
