//
//  LFItemViewV2.h
//  mHouseKeeping
//
//  Created by TMS on 2/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LFCategoryTableViewCell.h"
#import "LFItemSectionV2.h"
#import "LFCatergorySectionHeaderV2.h"
#import "PickerViewV2.h"
#import "LFItemModelV2.h"
#import "LostandFoundManagerV2.h"
#import "TopbarViewV2.h"
#import "CommonVariable.h"

@protocol LFItemViewV2Delegate <NSObject>

@optional 
-(void)forwardhoseItem:(LostandFoundModelV2 *)lafData atselectedSection:(NSInteger) sectionIndex;

@end

@interface LFItemViewV2 : UIViewController<UITableViewDataSource, UITableViewDelegate,LFCatergorySectionHeaderV2Delegate, PickerViewV2Delegate,LFCategoryTableViewCellDelegate> { 
    IBOutlet UITableView *categoryTableView;
    TopbarViewV2 *topBarView;
    BOOL isListDown;
    NSMutableArray *sectionInfoArray;
    NSInteger selectedSectionIndex;
    __unsafe_unretained id <LFItemViewV2Delegate> delegate;
    LostandFoundModelV2 *existedLaFObject;    
}

// Control_Variables For View.
@property (nonatomic,strong)TopbarViewV2 *topBarView;
@property (nonatomic,strong)LostandFoundModelV2 *existedLaFObject;
@property NSInteger selectedSectionIndex;
@property (nonatomic,assign)id <LFItemViewV2Delegate> delegate;
@property (nonatomic,strong)NSMutableArray *sectionInfoArray;
@property (nonatomic,strong)IBOutlet IBOutlet UITableView *categoryTableView;
@property BOOL isListDown;

-(void)loadDataFromCategoryID;
@end
