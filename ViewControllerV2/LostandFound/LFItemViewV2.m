//
//  LFItemViewV2.m
//  mHouseKeeping
//
//  Created by TMS on 2/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LFItemViewV2.h"
#import "LFCategoryModelV2.h"
#import "LFItemModelV2.h"
#import "ehkDefines.h"
#import "CustomAlertViewV2.h"

#define ALLITEMS @"All Items"
#define sizeTitle 22
#define ALLITEMSIMG @"all_pic.png"
#define isDemoMode ([[NSUserDefaults standardUserDefaults] integerForKey:ENABLE_DEMO_MODE] == 1)

@interface LFItemViewV2(PrivateMethods)
-(void) loadTopbarView;
-(void) showTopbarView;
-(void) hideTopbarView;
-(void) setHiddenTopbarView;
-(void) adjustShowForViews;
-(void) adjustRemoveForViews;
-(void) addButtonHandleShowHideTopbar;
-(void) topbarTapped:(UIButton *) sender;
-(TopbarViewV2 *) getTopBarView;
@end

@implementation LFItemViewV2

@synthesize existedLaFObject;
@synthesize selectedSectionIndex;
@synthesize categoryTableView,topBarView,isListDown,sectionInfoArray;
@synthesize delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    
    return self;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadDataFromCategoryID];
    
    [self.categoryTableView setBackgroundColor:[UIColor clearColor]];

    
    [self performSelector:@selector(loadTopbarView)];

    [categoryTableView reloadData];

    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated {
    //show wifi view
    if(!isDemoMode){
        [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationShowWifiView] object:nil];
    }
    [super viewWillAppear:animated];
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - UITableViewDelegate Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [sectionInfoArray count];
}

-(NSInteger)tableView:(UITableView *)tableView 
    numberOfRowsInSection:(NSInteger)section {
    LFItemSectionV2 *sectioninfo = [sectionInfoArray objectAtIndex:section];
    NSInteger numberRowInSection = sectioninfo.rowHeights.count;

    return sectioninfo.open ? numberRowInSection : 0;
}

-(CGFloat)tableView:(UITableView *)tableView 
    heightForHeaderInSection:(NSInteger)section {
    return 60;
}

-(CGFloat)tableView:(UITableView *)tableView 
    heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    LFItemSectionV2 *sectioninfo = [sectionInfoArray objectAtIndex:section];
    
    if (sectioninfo.headerView == nil) {
        LFCatergorySectionHeaderV2 *sview;

        if (existedLaFObject.lafCategoryId == 1) {
            
            sview = [[LFCatergorySectionHeaderV2 alloc] initWithSection:section categoryName:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostanfound_all_item] categoryImage:UIImagePNGRepresentation([UIImage imageNamed:ALLITEMSIMG]) AndStatusArrow:YES] ;
            
        } else {
            
            sview = [[LFCatergorySectionHeaderV2 alloc] initWithSection:section categoryName:[[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString:[NSString stringWithFormat:@"%@",ENGLISH_LANGUAGE]]? sectioninfo.chkContentType.lfcName:sectioninfo.chkContentType.lfcLang categoryImage:sectioninfo.chkContentType.lfcImage AndStatusArrow:YES] ;
            
        }
                
        sectioninfo.headerView = sview;
        sview.delegate = self;
    }
    
    return sectioninfo.headerView;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *categoryRowIdentifier = @"categoryIdentifier";
   
    LFCategoryTableViewCell *cell = nil;
    cell = [tableView dequeueReusableCellWithIdentifier:categoryRowIdentifier];   
    
    if (cell == nil) {
        NSArray *array = [[NSBundle mainBundle] loadNibNamed:@"LFCategoryTableViewCell" owner:self options:nil];
        cell = [array objectAtIndex:0];
    }
    
    LFItemSectionV2 *sectionInfo = (LFItemSectionV2 *)[sectionInfoArray objectAtIndex:indexPath.section];
    LFItemModelV2 *model = [sectionInfo.rowHeights objectAtIndex:indexPath.row];
    
    [cell.imgCategoryItemCell setImage:[UIImage imageWithData:model.lfitImage]];
    cell.lblQtyCategoryItemCell.text = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_QUANTITY];
    
    if ([[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString:[NSString stringWithFormat:@"%@",ENGLISH_LANGUAGE]]) {
      cell.lblNameCategoryItemCell.text = model.lfitName;  
    } else{
        cell.lblNameCategoryItemCell.text = model.lfitLang;
    }
    

    
    if(existedLaFObject && existedLaFObject.lafItemId == model.lfitID) {
        [cell.btnQtyCategoryItemCell setTitle:[NSString stringWithFormat:@"%ld",(long)existedLaFObject.laf_item_quantity] forState:UIControlStateNormal];
    } else {
        [cell.btnQtyCategoryItemCell setTitle:[NSString stringWithFormat:@"%d", 0] forState:UIControlStateNormal];
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell setBackgroundColor:[UIColor clearColor]];
    [cell setDelegate:self];
    [cell setIndexpath:indexPath];    
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    if (sectionInfoArray) {
//        LFItemSectionV2 *selectedCategory=[sectionInfoArray objectAtIndex:indexPath.section];
//        LFItemModelV2 *selectedItem=[selectedCategory objectInRowHeightsAtIndex:indexPath.row];
//        if ([delegate respondsToSelector:@selector(forwardhoseItem: atselectedSection:)]) {
//            [delegate forwardhoseItem:selectedItem atselectedSection:selectedSectionIndex];
//        }
//    }
}

#pragma mark - Section Header Delegate
-(void)sectionHeaderView:(LFCatergorySectionHeaderV2 *)sectionheaderView 
           sectionOpened:(NSInteger)section {
    LFItemSectionV2 *sectionInfo = [sectionInfoArray objectAtIndex:section];
    sectionInfo.open = YES;
    
    NSInteger countOfRowsToInsert = [sectionInfo.rowHeights count];
    
    if (countOfRowsToInsert == 0) {
        return;
    }
    
    NSMutableArray *indexPathsToInsert = [[NSMutableArray alloc] init];
    
    for (NSInteger i = 0; i < countOfRowsToInsert; i++) {
        [indexPathsToInsert addObject:[NSIndexPath indexPathForRow:i
                                                         inSection:section]];
    }
    
    UITableViewRowAnimation insertAnimation;
    insertAnimation = UITableViewRowAnimationTop;
    
    [self.categoryTableView beginUpdates];
    [self.categoryTableView insertRowsAtIndexPaths:indexPathsToInsert
                                  withRowAnimation:insertAnimation];
    [self.categoryTableView endUpdates];
    
    [self.categoryTableView scrollToRowAtIndexPath:[indexPathsToInsert objectAtIndex:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

-(void)sectionHeaderView:(LFCatergorySectionHeaderV2 *)sectionheaderView sectionClosed:(NSInteger)section {
    LFItemSectionV2 *sectionInfo = [sectionInfoArray
                                        objectAtIndex:section];
    
    sectionInfo.open = NO;
    NSInteger countOfRowsToDelete = [self.categoryTableView
                                     numberOfRowsInSection:section];
    
    if (countOfRowsToDelete > 0) {
        NSMutableArray *indexPathsToDelete = [[NSMutableArray alloc] init];
        for (NSInteger i = 0; i < countOfRowsToDelete; i++) {
            [indexPathsToDelete addObject:[NSIndexPath indexPathForRow:i
                                                             inSection:section]];
        }
        
        [self.categoryTableView beginUpdates];
        [self.categoryTableView deleteRowsAtIndexPaths:indexPathsToDelete
                                      withRowAnimation:UITableViewRowAnimationTop];
        [self.categoryTableView endUpdates];        
    }
}

#pragma mark - Loaddata
-(void)loadDataFromCategoryID {
    //enable flag to announce, is loading data
    [[HomeViewV2 shareHomeView] waitingLoadingData];
    
    
    LFItemSectionV2 *section1 = [[LFItemSectionV2 alloc] init];
    if (!sectionInfoArray) {
        sectionInfoArray = [[NSMutableArray alloc] init];
    }
    
    NSMutableArray *itemArray;  
    
    LFCategoryModelV2 *categoryObj = [[LFCategoryModelV2 alloc] init];
    categoryObj.lfcID = existedLaFObject.lafCategoryId;
    
    if (existedLaFObject.lafCategoryId == 1) { 
//        || existedLaFObject.lafItemId == 16) {
        itemArray = [[LostandFoundManagerV2 sharedLostandFoundManagerV2] loadAllItemData];        
     } else {
         itemArray = [[LostandFoundManagerV2 sharedLostandFoundManagerV2] loadAllItemDataByCategoryId:(int)existedLaFObject.lafCategoryId];
         categoryObj = [[LostandFoundManagerV2 sharedLostandFoundManagerV2] loadCategoryData:categoryObj];
     }
    
     section1.chkContentType = categoryObj;
     
    for (LFItemModelV2 *itemData in itemArray) {
         [section1 insertObjectToNextIndex:itemData];
     }
    
    section1.open = YES;
    
     [sectionInfoArray addObject:section1];
    
    
    //end loading data
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
}
 
#pragma mark - CategoryItemCellV2 Delegate Methods
- (void) btnQtyCategoryItemCellPressedWithIndexPath:(NSIndexPath *)indexpath
                                          AndSender:(UIButton *) sender{
    PickerViewV2 *picker = [[PickerViewV2 alloc] initPickerViewV2WithDatas:[NSArray arrayWithObjects:@"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9", @"10",nil] AndSelectedData:sender.titleLabel.text AndIndex:indexpath];
    picker.delegate = self;
    [picker showPickerViewV2];   
}

#pragma mark - PickerViewV2 Delegate
-(void)didChoosePickerViewV2WithData:(NSString *) data 
                     AndIndexItemRow:(NSIndexPath *) index {
    LFItemSectionV2 *sectionInfo = [sectionInfoArray objectAtIndex:index.section];
    LFCategoryModelV2 *selectedCategory = sectionInfo.chkContentType;
    LFItemModelV2 *selectedItem = [sectionInfo.rowHeights objectAtIndex:index.row];

    if ([delegate respondsToSelector:@selector(forwardhoseItem:atselectedSection:)]) {
        if (existedLaFObject.lafItemId == selectedItem.lfitID && existedLaFObject.laf_item_quantity == [data integerValue]) {
           
        } else { 
            existedLaFObject.lafCategoryId = selectedItem.lafi_category_id;
            existedLaFObject.lafItemId = selectedItem.lfitID;
            
            selectedCategory.lfcID = selectedItem.lafi_category_id;
            [[LostandFoundManagerV2 sharedLostandFoundManagerV2] loadCategoryData:selectedCategory];
            
            existedLaFObject.lafCat = selectedCategory;
            existedLaFObject.lafItem = selectedItem;
            existedLaFObject.laf_item_quantity = [data integerValue];
            
            if (existedLaFObject.laf_Status == inDataBase) {
                existedLaFObject.laf_Status = isNeedUpdate;
            }
        }
        
        [delegate forwardhoseItem:existedLaFObject atselectedSection:selectedSectionIndex];
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    // [categoryTableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:index] withRowAnimation:UITableViewRowAnimationFade];
}

#pragma mark - Handle Topbar Methods
-(void)loadTopbarView {
    TopbarViewV2 *topbar = [[TopbarViewV2 alloc] initViewWithSuperModel:[[CommonVariable sharedCommonVariable] superRoomModel]];
    [topbar setTag:tagTopbarView];
    
    [self.view addSubview:topbar];
    [topbar setHidden:YES];
    [topbar setAlpha:0.0];
    [self addButtonHandleShowHideTopbar];
}

-(void)showTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationWillStartSelector:@selector(setHiddenTopbarView)];
    
    f = topbar.frame;
    f.origin.y = 0;
    [topbar setFrame:f];
    [topbar setAlpha:1.0];
    
    [self adjustShowForViews];
    
    [UIView commitAnimations];
}

-(void)hideTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDidStopSelector:@selector(setHiddenTopbarView)];
    [UIView setAnimationDelegate:self];
    
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    [topbar setAlpha:0.0];
    
    [self adjustRemoveForViews];
    
    [UIView commitAnimations];
}

-(void)setHiddenTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    [topbar setHidden:!topbar.hidden];
}

-(TopbarViewV2 *)getTopBarView {
    return (TopbarViewV2 *)[self.view viewWithTag:tagTopbarView];
}

-(void)addButtonHandleShowHideTopbar {
    UIButton *titleBarButton = [[UIButton alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
    
    titleBarButton.titleLabel.backgroundColor = [UIColor clearColor];
    titleBarButton.titleLabel.font = FONT_BUTTON_TOPBAR;
    titleBarButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    titleBarButton.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
    [titleBarButton.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
    [titleBarButton.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
    [titleBarButton addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    [titleBarButton setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_LOST_FOUND] forState:UIControlStateNormal];
    self.navigationItem.titleView = titleBarButton;
}

-(void)topbarTapped:(UIButton *)sender {
    TopbarViewV2 *topbar = [self getTopBarView];
    [sender setEnabled:NO];
    if (topbar.isHidden) {
        [self showTopbarView];
    } else {
        [self hideTopbarView];
    }
    [self performSelector:@selector(topbarButtonEnable:) withObject:sender 
               afterDelay:0.5];
}

-(void) topbarButtonEnable:(UIButton *) topbarBtn {
    [topbarBtn setEnabled:YES];
}

-(void)adjustShowForViews {
    //adjust all views when show 
    [categoryTableView setFrame:CGRectMake(0, 45, 320, 430)];
}

-(void)adjustRemoveForViews { 
    //adjust all views when hide
    [categoryTableView setFrame:CGRectMake(0, 0, 320, 460)];
}

#pragma mark -  Hidden Header view when tap on navigationBar
#define heightScale 45
- (void) hiddenHeaderView {    
    BOOL isHidden = topBarView.hidden;
    [topBarView setHidden:!isHidden];
    
    CGRect frame1 = CGRectZero;
   
    if(!isHidden){
        frame1  = CGRectMake(categoryTableView.frame.origin.x, categoryTableView.frame.origin.y - heightScale, categoryTableView.frame.size.width, categoryTableView.frame.size.height+heightScale);
    } else {
        frame1  = CGRectMake  (categoryTableView.frame.origin.x, categoryTableView.frame.origin.y +heightScale, categoryTableView.frame.size.width, categoryTableView.frame.size.height-heightScale);
    }
    
    [categoryTableView setFrame:frame1];
}

#pragma mark - === Handle Check Save Data ===
#pragma mark
-(BOOL) checkSaveDataWithTagEvent:(NSInteger) tagOfEvent {
    //can go to message screen
    if (tagOfEvent == tagOfMessageButton) {
        //set status room is not completed
        [[CommonVariable sharedCommonVariable] setRoomIsCompleted:0];
        
        return NO;
    }
    
    //show please complete the room
    UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_complete_room_to_leave] delegate:nil cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles: nil];   
    [alert show];
    
    return YES; //no need to save before leaving, but need to complete the room before leaving
}

@end
