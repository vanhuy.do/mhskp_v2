//
//  LFRemarkViewV2.h
//  mHouseKeeping
//
//  Created by TMS on 3/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QuartzCore/QuartzCore.h"

@protocol LFRemarkViewV2Delegate <NSObject>

@optional
-(void) remarkViewV2DoneWithText:(NSString *) text andController:(UIViewController*)controllerView;

@end

@interface LFRemarkViewV2 : UIViewController {
    __unsafe_unretained id<LFRemarkViewV2Delegate> delegate;
    NSString *textinRemark;
}
 
@property (nonatomic,strong) NSString *textinRemark;
@property (nonatomic, assign) id<LFRemarkViewV2Delegate> delegate;
@property (retain, nonatomic) IBOutlet UINavigationBar *nvgBar;
@property (retain, nonatomic) IBOutlet UITextView *txvRemark;
@property (retain, nonatomic) IBOutlet UIButton *btnCancel;

-(IBAction)ExitWithView:(id)sender;
-(void)setCaptionsView;
-(IBAction)ExitKeyBoard:(id)sender;
-(void)remarkSaveDidPressed;
@end

