//
//  LFRemarkViewV2.m
//  mHouseKeeping
//
//  Created by TMS on 3/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LFRemarkViewV2.h"
#import "ehkDefines.h"
#import "CustomAlertViewV2.h"

#define isDemoMode ([[NSUserDefaults standardUserDefaults] integerForKey:ENABLE_DEMO_MODE] == 1)

@implementation LFRemarkViewV2
@synthesize btnCancel;
@synthesize nvgBar;
@synthesize txvRemark;
@synthesize delegate;
@synthesize textinRemark;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        //hide wifi view
        [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationHideWifiView] object:nil];
    }
    
    return self;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle
-(void)viewWillAppear:(BOOL)animated {
    [self setCaptionsView];
    //show wifi view
    if(!isDemoMode){
        [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationShowWifiView] object:nil];
    }
    
    self.navigationController.navigationBarHidden = YES;
    
    [super viewWillAppear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    [self.txvRemark becomeFirstResponder];
    [self.view setBackgroundColor:[UIColor colorWithWhite:0.1 alpha:0.1]];
    self.navigationController.navigationBarHidden = YES;
    [self.navigationItem setHidesBackButton:YES animated:NO];    
    
    UIView *parentView = nil;    
    parentView = self.parentViewController.view;
    
    UIGraphicsBeginImageContext(parentView.bounds.size);
    CALayer *layer = parentView.layer;
    [layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *parentViewImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:self.view.bounds];
    imgView.image = parentViewImage;
    [self.view insertSubview:imgView atIndex:0];
    [txvRemark setText:textinRemark];
}

- (void)viewDidUnload {
    [super viewDidUnload];
//    [self setTxvRemark:nil];
//    [self setBtnCancel:nil];
//    [self setNvgBar:nil];

    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)ExitWithView:(id)sender {
    self.navigationController.navigationBarHidden = NO;
    [self.navigationController popViewControllerAnimated:YES];
    
    //[self.view removeFromSuperview];
}

- (IBAction)ExitKeyBoard:(id)sender {
    [txvRemark resignFirstResponder];
}

-(void)remarkSaveDidPressed {
    if ([delegate respondsToSelector:@selector(remarkViewV2DoneWithText::)]) {
        [delegate remarkViewV2DoneWithText:txvRemark.text andController:self];
    }
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ([text rangeOfCharacterFromSet:[NSCharacterSet newlineCharacterSet]].location==NSNotFound) {        
        return YES;
    }
    
    [txvRemark resignFirstResponder];
    //self.navigationController.navigationBarHidden = NO;
    //[self.navigationController popViewControllerAnimated:YES];
//    if ([delegate respondsToSelector:@selector(remarkViewV2DoneWithText::)]) {
//        [delegate remarkViewV2DoneWithText:txvRemark.text andController:self];
//    }
    
    return NO;
}

-(void)saveData{
    self.navigationController.navigationBarHidden = NO;
    if ([delegate respondsToSelector:@selector(remarkViewV2DoneWithText:andController:)]) {
        [delegate remarkViewV2DoneWithText:txvRemark.text andController:self];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void)setCaptionsView {
    [self.nvgBar.topItem setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_remark_title]];
    [self.btnCancel setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_CANCEL] forState:UIControlStateNormal];
}

-(BOOL) checkSaveDataWithTagEvent:(NSInteger) tagOfEvent {
    if (tagOfEvent != tagOfMessageButton) {
        //Show alert complete room
        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_complete_room_to_leave] delegate:nil cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles: nil]; 
        [alert show];
        
        return YES;
    }
    
    //can go to message screen
    if (tagOfEvent == tagOfMessageButton) {
        //set status room is not completed
        [[CommonVariable sharedCommonVariable] setRoomIsCompleted:0];
        
        return NO;
    }
    
    return NO;
}

@end
