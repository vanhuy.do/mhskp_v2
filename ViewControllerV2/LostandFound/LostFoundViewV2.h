//
//  LostFoundViewV2.h
//  mHouseKeeping
//
//  Created by TMS on 2/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
/*
 ----------Business Rule------------------------------
 First, load the lostandfoundDetail Object from database and store in lafDetailObject.
        load all the lostandfound objects based on lostandfoundDetail Id property from database and store in catDataSourceArray.
 
 Using a NSDictionnary to store all lostandfound objects with key is lostandfound Id property.
 There are 3 values of status property of lostandfound Object:
 isNeedUpdate 1;
 isNeedCreate 2;
 isNeedDelete 3: using for lostandfound with the value lafd_pos_status is no.
 Edit lostandfound object mode: choose 1 and forward to respective subView.
 Category subView;
 Item subView;
 Color subView;
 Receive this object from subViews.
 Category subView: if categoryID changes the status of lostandfoundobject is changed : isNeedUpdate.
 
 Create lostandfound object mode : isNeedCreate.
 Delete lostandfound object mode:   isNeedDelete.
 
 
 ------------------------------------------------------       
 */

#import <UIKit/UIKit.h>
#import <ZXingWidgetController.h>
#import "PhotoScrollViewV2.h"
#import "LFRemarkViewV2.h"
#import "LFColorModelV2.h"
#import "PhotoViewV2.h"
#import "AlertAdvancedSearch.h"
#import "LFColorViewV2.h"
#import "LFCategoryViewV2.h"
#import "LFItemViewV2.h"
#import "LostandFoundManagerV2.h"
#import "UserManagerV2.h"
#import "RoomManagerV2.h"
#import "TopbarViewV2.h"
#import "zoomPhotoViewV2.h"
#import "CommonVariable.h"
#import "QuartzCore/QuartzCore.h"
#import "MBProgressHUD.h"

@interface LostFoundViewV2 : UIViewController<UITableViewDataSource, UITableViewDelegate, PhotoViewV2Delegate, LFRemarkViewV2Delegate,UIGestureRecognizerDelegate, LFColorViewV2Delegate, LFCategoryView2Delegate,LFItemViewV2Delegate, UIAlertViewDelegate, MBProgressHUDDelegate, UITextFieldDelegate, UITextViewDelegate, ZXingDelegate, RemarkViewV2Delegate> {
    // Control_Variables For View.
    UITableView *lostfoundTableView;
    UILabel *locationLabel;
    UIButton *addButton;
    TopbarViewV2 * topBarView;
    
    // Variables for Data.
    LostandFoundDetailModelV2 *lafDetailObject;

    //data for template
    NSMutableArray *photoDeleteList;
    NSMutableArray *lafDeleteList;
    NSMutableArray *lafList;
    NSString *remarkText;

    NSInteger indexDelete;
    BOOL isFirstLoad;
    MBProgressHUD *HUD;
    
    BOOL isUpdatedText;
    BOOL isUpdatePhoto;
    BOOL isTapBackButton;
    
    UIButton *btnQR;
    BOOL isFromMainPosting;
    int countRow;
    
    NSInteger indexTemp;
    IBOutlet UIView *viewRoom;
    UIButton *btnSubmit;//edit
    UIButton *btnPostingHistory;
    
    AccessRight *QRCodeScanner;
    AccessRight *postingLostAndFound;
    AccessRight *actionLostAndFound;
    
    RemarkViewV2 *remarkView;
    
    IBOutlet UIImageView *imgHeader;
}

// Control_Variables For View.
@property BOOL isFirstLoad;

@property (nonatomic, strong) TopbarViewV2 *topBarView;
@property (nonatomic, strong) MBProgressHUD *HUD;
@property (nonatomic, strong) UIButton *addButton;
@property (nonatomic, strong) IBOutlet UITableView *lostfoundTableView;
@property (nonatomic, strong) IBOutlet UILabel *locationLabel;
@property (nonatomic, strong) IBOutlet UITextField *locationTextField;

// Variables for Data.
@property (nonatomic, strong) LostandFoundDetailModelV2 *lafDetailObject;

@property NSInteger indexDelete;
@property (nonatomic, strong) NSMutableArray *photoDeleteList;
@property (nonatomic, strong) NSMutableArray *lafDeleteList;
@property (nonatomic) int countRow;
@property (strong, nonatomic) IBOutlet UIButton *btnQR;
@property (nonatomic) BOOL isFromMainPosting;
@property (nonatomic, strong) NSMutableArray *lafList;

-(void)loadData;
-(void)saveData;
-(void)setHiddenForAddButton;
-(IBAction) btnQRCodeScannerTouched:(id) sender;
@end
