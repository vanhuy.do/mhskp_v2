//
//  LostFoundViewV2.m
//  mHouseKeeping
//
//  Created by TMS on 2/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LostFoundViewV2.h"
#import "ehkDefinesV2.h"
#import "LanguageManager.h"
#import "LFCategoryModelV2.h"
#import "LFItemModelV2.h"
#import "LFCategorySectionV2.h"
#import "CustomAlertViewV2.h"
#import "MBProgressHUD.h"
#import "NetworkCheck.h"
#import "RoomAssignmentInfoViewController.h"
#import "LostandFoundHistoryModel.h"
#import "LostandFoundHistoryManager.h"
#import "MyNavigationBarV2.h"
#import "QRCodeReader.h"
#import "eHousekeepingService.h"
#import "LogFileManager.h"
#import "DeviceManager.h"
#import "ehkDefinesV2.h"
#import "UIImage+Tint.h"
#import "LanguageManagerV2.h"
#import "HomeViewV2.h"
#import "iToast.h"
#import "DTAlertView.h"
#import "STEncryptorDES.h"
#import "NSString+Common.h"
#import "HistoryPosting.h"
#import "RemarkViewV2.h"

#define HEIGHT 60
#define WIDTH  85
#define WIDTHIMAGE 85
#define DELETEIMAGE @"icon_garbage.png"
#define PHOTOIMAGE @"photo_icon.png"
#define TESTIMAGE    @"Shampoo_pic.png"
#define ADDICONIMAGE @"add_icon.png"
#define NEXTIMAGE @"gray_arrow_36x36.png"
#define BACKGROUNDIMAGE @"bg.png"
#define FONTLABEL @"Arial-BoldMT"
#define FONTLABELSIZE 17
#define FONTTEXTSIZE 15
#define REDLABEL 6.0f/255.0f
#define GREENLABEL 62.0f/255.0f
#define BLUELABEL 127.0f/255.0f
#define REDTEXT 102.0f/255.0f
#define GREENTEXT 102.0f/255.0f
#define BLUETEXT 102.0f/255.0f
#define KTAGREMARK 2
#define KTAGCOLORBUTTON 3
#define sizeTitle 22

#define remarkTag 102
#define maxRowOfTableItem 5
//define section of table view
#define sectionTableItems 0
#define sectionRemark 1
#define sectionPhotoButton 2
#define sectionSubmitButton 3
#define tagOfSubmitButton 101
#define tagOfPostingButton 102
#define COLOR_BLACK_LAF_ID 0

#define tagDiscard 4
#define tagSaveLeaving 2
#define tagDeletePhoto 3
#define tagDeleteCategory 5
#define tagSaveSuccessful 31
#define tagAlertSubmit 32
#define tagRemarkCell 33
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0x00FF00) >> 8))/255.0 blue:((float)(rgbValue & 0x0000FF))/255.0 alpha:1.0]

#define heightScale 45

@interface ButtonColor : UIButton
@property (nonatomic, assign) int indexInTableRow;
@end

@implementation ButtonColor
@synthesize indexInTableRow;
@end

@interface LostFoundViewV2(PrivateMethods) <DTAlertViewDelegate>
-(void) loadTopbarView;
-(void) showTopbarView;
-(void) hideTopbarView;
-(void) setHiddenTopbarView;
-(void) adjustShowForViews;
-(void) adjustRemoveForViews;
-(void) addButtonHandleShowHideTopbar;
-(void) topbarTapped:(UIButton *) sender;
-(TopbarViewV2 *) getTopBarView;

-(BOOL) checkSaveData;

-(void) reloadLaFData;

-(void) popViewControllerAfterDelay;
-(void) gotoMessageAfterDelay;

@end

@implementation LostFoundViewV2

@synthesize lafDetailObject;
@synthesize photoDeleteList;
@synthesize indexDelete, isFirstLoad, lafDeleteList, HUD;
@synthesize isFromMainPosting, countRow, btnQR, lafList;
@synthesize locationTextField;
@synthesize topBarView;
@synthesize lostfoundTableView;
@synthesize locationLabel;
@synthesize addButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - Display Data in Views
- (void)setHiddenForAddButton {
    if ([lafDetailObject.lafArray count] < 6)
        [addButton setHidden:NO];
    else
        [addButton setHidden:YES];
}

#pragma mark - LongPress Response
-(void)responseLongPressButton:(UILongPressGestureRecognizer*)sender {
    if (sender.state == UIGestureRecognizerStateBegan) {
        self.indexDelete = sender.view.tag;
        
        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_delete_confirmation] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles:[[LanguageManagerV2 sharedLanguageManager] getNo], nil];
        [alert setTag:tagDeletePhoto];
        [alert show];
        //                [alert release];
    }
}

#pragma mark - Forward to SubViews Methods
-(void)forwardtoPhotoView {
    PhotoViewV2 *viewPhoto = [[PhotoViewV2 alloc] init];
    viewPhoto.delegate = self;
    [viewPhoto setHidesBottomBarWhenPushed:YES];
    [self.navigationController pushViewController:viewPhoto animated:YES];
    //    [viewPhoto release];
}

#pragma mark - Process buttonPhotoClick
-(void)selectPhotoButton {
    if ([lafDetailObject.lafPhoto count] == 6)
        return;
    else {
        PhotoViewV2 *viewPhoto = [[PhotoViewV2 alloc] init];
        viewPhoto.delegate = self;
        UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK] style:UIBarButtonItemStyleBordered target:nil action:nil];
        self.navigationItem.backBarButtonItem = backButton;
        [viewPhoto setTitle:[[LanguageManagerV2 sharedLanguageManager]
                             getLOSTANDFOUND]];
        [viewPhoto setHidesBottomBarWhenPushed:YES];
        [self.navigationController pushViewController:viewPhoto animated:NO];
        
    }
    if([LogFileManager isLogConsole])
    {
        NSInteger userId = [[UserManagerV2 sharedUserManager] currentUser].userId;
        NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSLog(@"<%@><UserId:%d><L&F><Click photo button>", time, (int)userId);
    }
}

-(void)tapImageToZoom:(id)sender {
    UIButton *zoomButton = (UIButton *)sender;
    zoomPhotoViewV2 *zoomedView = [[zoomPhotoViewV2 alloc] initWithNibName:@"zoomPhotoViewV2" bundle:nil];
    
    UINavigationController *navZoomPhoto=[[UINavigationController alloc] initWithRootViewController:zoomedView];
    [self presentViewController:navZoomPhoto animated:NO completion:nil];
//    [self presentModalViewController:navZoomPhoto animated:NO];
    
    if ([lafDetailObject.lafPhoto count]>zoomButton.tag) {
        LFImageModelV2 *zoomedImage = [lafDetailObject.lafPhoto objectAtIndex:zoomButton.tag];
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [zoomedView.zoomPhoto setImage:[UIImage imageWithData:
//                                            zoomedImage.lafImgImage]];
//        });
        zoomedView.imageData = zoomedImage.lafImgImage;
//        [zoomedView.zoomPhoto setImage:[UIImage imageWithData:
//                                        zoomedImage.lafImgImage]];
        
    }
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_DONE] style:UIBarButtonItemStyleDone target:self action:@selector(tapDoneButtonInZoomView:)];
    zoomedView.navigationItem.leftBarButtonItem = doneButton;
    navZoomPhoto.navigationBar.tintColor = [UIColor blackColor];
    navZoomPhoto.navigationBar.alpha = 0.7f;
    navZoomPhoto.navigationBar.translucent = YES;
    
}

#pragma mark- hidden after save.
-(void)hudWasHidden {
    if (HUD != nil) {
		[HUD removeFromSuperview];
    }
}

-(void)tapDoneButtonInZoomView:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
//    [self dismissModalViewControllerAnimated:YES];
}

#pragma  mark - Process Back button
-(void)tapBackButton:(id)sender {
    isTapBackButton = YES;
    
    BOOL result = [self checkSaveData];
    if (result == NO) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - Handle Topbar Methods
-(void)loadTopbarView {
    TopbarViewV2 *topbar = [[TopbarViewV2 alloc] initViewWithSuperModel:[[CommonVariable sharedCommonVariable] superRoomModel]];
    [topbar setTag:tagTopbarView];
    
    [self.view addSubview:topbar];
    [topbar setHidden:YES];
    [topbar setAlpha:0.0];
    [self addButtonHandleShowHideTopbar];
}

-(void)showTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationWillStartSelector:@selector(setHiddenTopbarView)];
    
    f = topbar.frame;
    f.origin.y = 0;
    [topbar setFrame:f];
    [topbar setAlpha:1.0];
    
    [self adjustShowForViews];
    
    [UIView commitAnimations];
}

-(void)hideTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDidStopSelector:@selector(setHiddenTopbarView)];
    [UIView setAnimationDelegate:self];
    
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    [topbar setAlpha:0.0];
    
    [self adjustRemoveForViews];
    
    [UIView commitAnimations];
}

-(void)setHiddenTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    [topbar setHidden:!topbar.hidden];
}

-(TopbarViewV2 *)getTopBarView {
    return (TopbarViewV2 *)[self.view viewWithTag:tagTopbarView];
}

//-(void)addButtonHandleShowHideTopbar {
//    UIButton *titleBarButton = [[UIButton alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
//
//    titleBarButton.titleLabel.backgroundColor = [UIColor clearColor];
//    titleBarButton.titleLabel.font = FONT_BUTTON_TOPBAR;
//    titleBarButton.titleLabel.textAlignment = NSTextAlignmentCenter;
//    titleBarButton.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
//    [titleBarButton.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
//    [titleBarButton.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
//    [titleBarButton addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
//    [titleBarButton setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_LOST_FOUND] forState:UIControlStateNormal];
//    self.navigationItem.titleView = titleBarButton;
//}

-(void)addButtonHandleShowHideTopbar {
    UIView *vBarButton = [[UIView alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
    UIButton *titleBarButtonFirst = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 200, 30)];
    
    titleBarButtonFirst.titleLabel.backgroundColor = [UIColor clearColor];
    [titleBarButtonFirst.titleLabel setFont: FONT_BUTTON_TOPBAR];
    titleBarButtonFirst.titleLabel.textAlignment = NSTextAlignmentCenter;
    titleBarButtonFirst.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
    
    [titleBarButtonFirst.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
    [titleBarButtonFirst.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
    [titleBarButtonFirst addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    if (isFromMainPosting){
        [titleBarButtonFirst setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_Counts] forState:UIControlStateNormal];
    } else {
        [titleBarButtonFirst setTitle:locationTextField.text forState:UIControlStateNormal];
    }
    
    [vBarButton addSubview:titleBarButtonFirst];
    
    
    UIButton *titleBarButtonSecond = [[UIButton alloc] initWithFrame:CGRectMake(0, 28, 200, 22)];
    [titleBarButtonSecond setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [titleBarButtonSecond.titleLabel setFont:FONT_BUTTON_TOPBAR_SECOND];
    [titleBarButtonSecond setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_LOSTANDFOUND_CASE_TITLE] forState:UIControlStateNormal];
    [titleBarButtonSecond addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    [vBarButton addSubview:titleBarButtonSecond];
    
    self.navigationItem.titleView = vBarButton;
    
    //Move title to center vertical
    CGRect titleViewBounds = self.navigationItem.titleView.bounds;
    int moveLeftTitle = 0;
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        moveLeftTitle = 10;
    }
    self.navigationItem.titleView.bounds = CGRectMake(titleViewBounds.origin.x + moveLeftTitle, Y_TOPBAR_TITLE, titleViewBounds.size.width, titleViewBounds.size.height);
}

-(void)topbarTapped:(UIButton *)sender {
    TopbarViewV2 *topbar = [self getTopBarView];
    [sender setEnabled:NO];
    if (topbar.isHidden) {
        [self showTopbarView];
    } else {
        [self hideTopbarView];
    }
    [self performSelector:@selector(topbarButtonEnable:) withObject:sender
               afterDelay:0.5];
}

-(void) topbarButtonEnable:(UIButton *) topbarBtn {
    [topbarBtn setEnabled:YES];
}

-(void)adjustShowForViews {
    //adjust all views when show
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    CGRect fRoomView = viewRoom.frame;
    [viewRoom setFrame:CGRectMake(0, f.size.height, 320, fRoomView.size.height)];
    //adjust all views when show
    //    [locationLabel setFrame:CGRectMake(20, 45, 88, 28)];
    //    [locationTextField setFrame:CGRectMake(152, 43, 160, 31)];
    
    int deviceKind = [DeviceManager getDeviceScreenKind];
    int moreHeight = 0;
    if(deviceKind == DeviceScreenKindRetina4_0){
        moreHeight = 81;
    }
    [lostfoundTableView setFrame:CGRectMake(0, 45 + f.size.height, 320, 260 + moreHeight)];
}

-(void)adjustRemoveForViews {
    //adjust all views when hide
    //    [locationLabel setFrame:CGRectMake(20, 15, 88, 28)];
    //    [locationTextField setFrame:CGRectMake(152, 13, 160, 31)];
    //    TopbarViewV2 *topbar = [self getTopBarView];
    //    CGRect f = topbar.frame;
    
    CGRect fRoomView = viewRoom.frame;
    [viewRoom setFrame:CGRectMake(0, 0, 320, fRoomView.size.height)];
    int deviceKind = [DeviceManager getDeviceScreenKind];
    int moreHeight = 0;
    if(deviceKind == DeviceScreenKindRetina4_0){
        moreHeight = 81;
    }
    [lostfoundTableView setFrame:CGRectMake(0, 45, 320, 320 + moreHeight)];
}

#pragma mark - View lifecycle
- (void)enableQRCode:(BOOL)isEnable{
    [btnQR setAlpha:isEnable ? 1.0f : 0.7f];
    [btnQR setEnabled:isEnable];
}
//CRF1619
- (void)loadAccessQRCode{
    if ([UserManagerV2 sharedUserManager].currentUserAccessRight.isPostingFunctionLostAndFoundQRCode == 1) {
        locationTextField.enabled = NO;
        [self enableQRCode:YES];
    }else if ([UserManagerV2 sharedUserManager].currentUserAccessRight.isPostingFunctionLostAndFoundQRCode == 0) {
        locationTextField.enabled = YES;
        [self enableQRCode:NO];
    }
}
- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        //Clear redundant view for header, footter
        CGRect frameHeaderFooter = lostfoundTableView.tableHeaderView.frame;
        frameHeaderFooter.size.height = 0.1f;
        UIView *headerView = [[UIView alloc] initWithFrame:frameHeaderFooter];
        [lostfoundTableView setTableHeaderView:headerView];
        
        frameHeaderFooter = lostfoundTableView.tableFooterView.frame;
        frameHeaderFooter.size.height = 0.1f;
        UIView *footerView = [[UIView alloc] initWithFrame:frameHeaderFooter];
        [lostfoundTableView setTableFooterView:footerView];
        
        [self loadFlatResource];
    }
    
    //Resize content views of current VC to fit with Navigation bar and tabbar
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    [self loadAccessRights];
    if(!ENABLE_QRCODESCANNER)
    {
        if(ENABLE_CHECK_ACCESS_RIGHT_QRCODESCANNER)
        {
            [btnQR setHidden:NO];
            [self enableQRCode:NO];
            
            if (QRCodeScanner.isActive && !isDemoMode) {
                [self enableQRCode:YES];
            }
            
        }
        else
        {
            [btnQR setHidden:YES];
        }
    }
    
    //Set Enable button submit
    [btnSubmit setEnabled:![self isOnlyViewedAccessRight]];
    
    if([LogFileManager isLogConsole])
    {
        NSInteger userId = [[UserManagerV2 sharedUserManager] currentUser].userId;
        NSString *date = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSLog(@"<%@><L&F><UserId:%d><viewDidLoad>",date,(int)userId);
    }
    
    if (!isFirstLoad) {
        [self performSelector:@selector(loadData) withObject:nil afterDelay:0.0f];
        
        isFirstLoad = YES;
        isUpdatedText = FALSE;
        //        isUpdatePhoto = FALSE;
    }
    
    lafList = [[NSMutableArray alloc] init];
    
    UIView *v = [self.navigationController.view
                 viewWithTag:tagViewWifi];  // add by chinh X.Bui
    [v setHidden:YES];
    
    //back button navigation
    self.navigationItem.leftBarButtonItem = [MyNavigationBarV2 createBackButtonWithTarget:self selector:@selector(tapBackButton:) forControlEvents:UIControlEventTouchUpInside];
    
    //set add button
    //    if (!addButton ) {
    //        addButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    //        [addButton setImage:[UIImage imageNamed:ADDICONIMAGE] forState:UIControlStateNormal];
    //        [addButton addTarget:self action:@selector(addCategorytoTableView) forControlEvents:UIControlEventTouchUpInside];
    //        [addButton setFrame:CGRectMake(15, 20, 35, 35)];
    //
    //        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:addButton];
    //    }
    
    //    UIColor *labelColor = [UIColor colorWithRed:REDLABEL green:GREENLABEL blue:BLUELABEL alpha:1];
    //    UIColor *textColor = [UIColor colorWithRed:REDTEXT green:GREENTEXT blue:BLUETEXT alpha:1];
    
    [locationLabel setTextColor:[UIColor whiteColor]];
    //    [locationLabel setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_EC_LOCATIONS]];
    [locationLabel setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOM_NO]];
    [locationTextField setTextColor:[UIColor blackColor]];
    [locationTextField setEnabled:NO];
    //    [self.locationTextField setText:[NSString stringWithFormat:@"%@ %d", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOM_TITLE], [RoomManagerV2 getCurrentRoomNo]]];
    [locationTextField setText:[RoomManagerV2 getCurrentRoomNo]];
    
    [lostfoundTableView setBackgroundColor:[UIColor clearColor]];
    
    countRow = 1;
    if(isFromMainPosting)
    {
        [locationTextField setText:@""];
        [locationTextField setEnabled:YES];
        [self loadAccessQRCode];
    }
    else
    {
        //        [locationTextField setText:[NSString stringWithFormat:@"%@ %d", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOM_TITLE], [RoomManagerV2 getCurrentRoomNo]]];
        [locationTextField setText:[RoomManagerV2 getCurrentRoomNo]];
        [locationTextField setTextColor:[UIColor colorWithRed:102.0f/255.0f green:102.0f/255.0f
                                                         blue:102.0f/255.0f alpha:1]];
        [locationTextField setEnabled:NO];
    }
    
    [self loadTopbarView];
    [self setEnableBtnAtionHistory:NO];
}

- (void) reachabilityDidChanged: (NSNotification* )note
{
    if(![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]){
        [btnSubmit setEnabled:NO];
        [self setEnableBtnAtionHistory:NO];
    }
    else{
        [btnSubmit setEnabled:YES];
        [self setEnableBtnAtionHistory:YES];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if(!isDemoMode){
        //show wifi view
        [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationShowWifiView] object:nil];
        if(![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]){
            [btnSubmit setEnabled:NO];
        }
        else{
            [btnSubmit setEnabled:YES];
        }
    }
    [[HomeViewV2 shareHomeView].slidingMenuBar showAll];
    if(isFromMainPosting){
        if(!isDemoMode){
            [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(reachabilityDidChanged:) name: kReachabilityChangedNotification object: nil];
        }
        [[HomeViewV2 shareHomeView] setSelectedButton:tagOfPostingButton];
    } else{
        [[HomeViewV2 shareHomeView] setSelectedButton:tagOfHomeButton];
    }
    
    //Fix lost navigation bar after choose picture from PhotoViewV2
    [self.navigationController setNavigationBarHidden:NO];
    
    [lostfoundTableView reloadData];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:sectionSubmitButton];
    UITableViewCell *cell = [lostfoundTableView cellForRowAtIndexPath:indexPath];
    btnSubmit = (UIButton *)[cell viewWithTag:tagOfSubmitButton];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    if(!isDemoMode){
        [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
    }
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - UITableViewDelegate Methods

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.0f;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    //    return [lafDetailObject.lafArray count] + 3;
    return 4;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //    int rowCount = 0;
    //    if (section >= [lafDetailObject.lafArray count])
    //        rowCount = 1;
    //    else
    //        rowCount = 3;
    //
    //    return rowCount;
    //    if(section >= [lafDetailObject.lafArray count])
    //        return 1;
    //    else
    //        return countRow;
    //---------------modify-----------
    
    
    if(section == sectionTableItems)
        return countRow;
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    float rowHeight = 0;
    if (indexPath.section == sectionPhotoButton) {
        rowHeight = 70;
    }else if (indexPath.section == sectionSubmitButton){
        if (![UserManagerV2 sharedUserManager].currentUserAccessRight.isShowActionHistoryPosting) {
            rowHeight = 70;
        }else
            rowHeight = 150;
    }
    else
        rowHeight = 50;
    
    return rowHeight;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *categoryRowIdentifier = @"categoryIdentifier";
    //    static NSString *itemRowIdentifier = @"itemIdentifier";
    //    static NSString *colorRowIdentifier = @"colorIdentifier";
    static NSString *remarkSectionIdentifier = @"remarkIdentifier";
    static NSString *photoSectionIdentifier=@"photoIdentifier";
    static NSString *submitSectionIdentifier = @"submitIdentifier";
    
    const NSInteger TITLE_TAG = 1201;
    const NSInteger SUBTITLE_TAG = 1202;
    const NSInteger BUTTON_TAG = 1203;
    //const NSInteger REMARK_TAG = 102;
    //const NSInteger SUBMIT_TAG = 101;
    const NSInteger PHOTO_TAG = 100;
    const NSInteger LABEL_SUBMIT_TAG = 105;
    
    UILabel *lblTitle;
    UILabel *lblSubTitle;
    ButtonColor *btnColor;
    //UITextView *txtRemark;
    UILabel *txtRemark;
    PhotoScrollViewV2 *photoViewScroll;
    //    UIButton *btnSubmit;
    UILabel *lblSubmit;
    
    UIColor *labelColor=[UIColor colorWithRed:REDLABEL green:GREENLABEL blue:BLUELABEL alpha:1];
    
    UIColor *textColor=[UIColor colorWithRed:REDTEXT green:GREENTEXT
                                        blue:BLUETEXT alpha:1];
    
    UIImage *nextImage=[UIImage imageNamed:NEXTIMAGE];
    UIImageView *nextImageView = [[UIImageView alloc] initWithImage:nextImage];
    nextImageView.frame = CGRectMake(260, 5, 25, 25);
    
    UITableViewCell *cell = nil;
    //    if (indexPath.section == [lafDetailObject.lafArray count]) {
    if(indexPath.section == sectionRemark) {
        //if (indexPath.row == 0) {
            cell = [tableView dequeueReusableCellWithIdentifier:remarkSectionIdentifier];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            //            txtRemark.text = @"";
        //}
        //    } else if (indexPath.section == [lafDetailObject.lafArray count] + 1) {
    }
    else if (indexPath.section == sectionPhotoButton) {
        if (indexPath.row == 0) {
            cell = [tableView dequeueReusableCellWithIdentifier: photoSectionIdentifier];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        }
        //    } else if (indexPath.section == [lafDetailObject.lafArray count] + 2) {
    }
    else if (indexPath.section == sectionSubmitButton) {
        if (indexPath.row == 0) {
            cell = [tableView dequeueReusableCellWithIdentifier: submitSectionIdentifier];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        }
    }
    else {
        cell = [tableView dequeueReusableCellWithIdentifier:categoryRowIdentifier];
    }
    
    
    
    if (cell == nil) {
        //        if (indexPath.section == [lafDetailObject.lafArray count]) {
        if (indexPath.section == sectionRemark) {
            if (indexPath.row == 0) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:
                        remarkSectionIdentifier];
                //Hao Tran - Remove for change layout like android
                /*
                CGRect frameRemark = CGRectMake(0, 0, 300, 60);
                txtRemark = [[UITextView alloc] initWithFrame:frameRemark];
                if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                    frameRemark.size.width = self.view.bounds.size.width;
                    frameRemark.size.height = 50;
                    [txtRemark setFrame:frameRemark];
                } else {
                    txtRemark.layer.cornerRadius = 10.0f;
                    txtRemark.layer.borderWidth = 1.0f;
                    txtRemark.layer.borderColor = [[UIColor grayColor] CGColor];
                }
                
                txtRemark.tag = REMARK_TAG;
                txtRemark.delegate = self;
                [cell setBackgroundColor:[UIColor clearColor]];
                cell.backgroundView = [[UIView alloc] initWithFrame:CGRectZero];
                [cell.contentView addSubview:txtRemark];*/
                
                
                [cell.textLabel setTextColor:[UIColor colorWithRed:6/255.0 green:62/255.0 blue:127/255.0 alpha:1]];
                [cell.textLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:15]];
                [cell.textLabel setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_EC_REMARK]];
                
                UILabel *lblRemarkContent = [[UILabel alloc] initWithFrame:CGRectMake(90, 0, tableView.bounds.size.width - 100, 60)];
                [lblRemarkContent setTag:tagRemarkCell];
                [lblRemarkContent setFont:[UIFont fontWithName:@"Helvetica-Bold" size:15]];
                [lblRemarkContent setAdjustsFontSizeToFitWidth:NO];
                [lblRemarkContent setNumberOfLines:2];
                [lblRemarkContent setLineBreakMode:NSLineBreakByTruncatingTail];
                [lblRemarkContent setBackgroundColor:[UIColor clearColor]];
                [lblRemarkContent setTextColor:[UIColor grayColor]];
                
                [cell addSubview:lblRemarkContent];
            }
            
            //        } else if(indexPath.section == [lafDetailObject.lafArray count] + 1) {
        }
        else if(indexPath.section == sectionPhotoButton) {
            if (indexPath.row == 0) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier: photoSectionIdentifier] ;
                
                photoViewScroll = [[PhotoScrollViewV2
                                    alloc] init];
                photoViewScroll.frame = CGRectMake(5, 5, 290, 60);
                photoViewScroll.backgroundColor = [UIColor clearColor];
                
                photoViewScroll.tag = PHOTO_TAG;
                [cell setBackgroundColor:[UIColor grayColor]];
                [cell.contentView addSubview:photoViewScroll];
            }
            //        } else if(indexPath.section == [lafDetailObject.lafArray count] + 2) {
        }
        else if(indexPath.section == sectionSubmitButton) {
            if (indexPath.row == 0) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:submitSectionIdentifier];
                
                btnSubmit = [[UIButton alloc] initWithFrame:CGRectMake(10, 0, 300, 60)];
                [btnSubmit setBackgroundImage:[UIImage imageBeforeiOS7:imgBtnSubmit equaliOS7:imgBtnSubmitFlat] forState:UIControlStateNormal];
                [btnSubmit addTarget:self action:@selector(btnSubmit_Clicked:) forControlEvents:UIControlEventTouchDown];
                //                    btnSubmit.tag = SUBMIT_TAG;
                btnSubmit.tag = tagOfSubmitButton;//edit
                btnSubmit.backgroundColor = [UIColor clearColor];
                [cell setBackgroundColor:[UIColor clearColor]];
                cell.backgroundView = [[UIView alloc] initWithFrame:CGRectZero];
                [cell.contentView addSubview:btnSubmit];
                
                lblSubmit = [[UILabel alloc] initWithFrame:CGRectMake(110, 10, 163, 35)];
                lblSubmit.font = [UIFont boldSystemFontOfSize:36];//29
//                lblSubmit.text = @"Submit";
                lblSubmit.text = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_SUBMIT];
                lblSubmit.tag = LABEL_SUBMIT_TAG;
                lblSubmit.backgroundColor = [UIColor clearColor];
                lblSubmit.textColor = [UIColor whiteColor];
                [cell.contentView addSubview:lblSubmit];
                
                if ([UserManagerV2 sharedUserManager].currentUserAccessRight.isShowActionHistoryPosting && self.isFromMainPosting) {
                    btnPostingHistory = [[UIButton alloc] initWithFrame:CGRectMake(10, CGRectGetHeight(btnSubmit.bounds) + 10, 300, 50)];
                    [btnPostingHistory setBackgroundImage:[UIImage imageBeforeiOS7:@"btn_bg-cleaning status & action.png" equaliOS7:@"btn_bg-cleaning status & action.png"] forState:UIControlStateNormal];
                    [btnPostingHistory addTarget:self action:@selector(btnPostingHistory_Clicked:) forControlEvents:UIControlEventTouchDown];
                    //                    btnSubmit.tag = SUBMIT_TAG;
                    btnPostingHistory.tag = tagOfPostingButton;//edit
                    btnPostingHistory.backgroundColor = [UIColor clearColor];
                    btnPostingHistory.titleLabel.font = [UIFont boldSystemFontOfSize:26];
                    [cell setBackgroundColor:[UIColor clearColor]];
                    cell.backgroundView = [[UIView alloc] initWithFrame:CGRectZero];
                    [cell.contentView addSubview:btnPostingHistory];
                }
                [self loadFlatResource];
            }
        }
        else {
            
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:categoryRowIdentifier];
            
            lblTitle = [[UILabel alloc] init];
            lblTitle.frame = CGRectMake(10, 7, 200, 20);
            lblTitle.font = [UIFont boldSystemFontOfSize:13];
            lblTitle.backgroundColor = [UIColor clearColor];
            lblTitle.tag = TITLE_TAG;
            [cell.contentView addSubview:lblTitle];
            
            lblSubTitle = [[UILabel alloc] init];
            lblSubTitle.frame = CGRectMake(10, 27, 200, 20);
            lblSubTitle.tag = SUBTITLE_TAG;
            lblSubTitle.font = [UIFont systemFontOfSize:13];
            lblSubTitle.backgroundColor = [UIColor clearColor];
            [cell.contentView addSubview:lblSubTitle];
            
            btnColor = [ButtonColor buttonWithType:UIButtonTypeCustom];
            [btnColor setImage:[UIImage imageNamed:@"no_color.png"] forState:UIControlStateNormal];
            btnColor.frame = CGRectMake(215, 5, 40, 40);
            btnColor.backgroundColor = [UIColor clearColor];
            btnColor.tag = BUTTON_TAG;
            btnColor.indexInTableRow = (int)indexPath.row;
            [btnColor addTarget:self action:@selector(btnColor_Clicked:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:btnColor];
            
            //            imgColorView = [[UIImageView alloc] init];
            //            imgColorView.frame = CGRectMake(100, 5, 40, 40);
            
        }
    }
    else
    {
        if (indexPath.section == sectionRemark)
        {
            if(indexPath.row == 0)
            {
                txtRemark = (UILabel*)[cell viewWithTag:tagRemarkCell];
            }
        }
        else if(indexPath.section == sectionPhotoButton)
        {
            if (indexPath.row == 0)
            {
                photoViewScroll = (PhotoScrollViewV2 *)[cell viewWithTag:PHOTO_TAG];
            }
        }
        else if(indexPath.section == sectionSubmitButton)
        {
            if (indexPath.row == 0)
            {
                //                btnSubmit = (UIButton *)[cell viewWithTag:SUBMIT_TAG];
                btnSubmit = (UIButton *)[cell viewWithTag:tagOfSubmitButton];
                lblSubmit = (UILabel *)[cell viewWithTag:LABEL_SUBMIT_TAG];
            }
        }
        else
        {
            
            lblTitle = (UILabel *)[cell viewWithTag:TITLE_TAG];
            lblSubTitle = (UILabel *)[cell viewWithTag:SUBTITLE_TAG];
            btnColor = (ButtonColor *)[cell viewWithTag:BUTTON_TAG];
        }
    }
    
    
    //add data to cell
    if (indexPath.section == sectionRemark) {
        //if (indexPath.row == 0) {
            //Remark row
            //            cell.textLabel.text = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_EC_REMARK];
            //            [cell.textLabel setTextColor:labelColor];
            //            [cell.detailTextLabel setFont:[UIFont fontWithName:FONTLABEL
            //                                                          size:FONTLABELSIZE]];
            //            [cell.detailTextLabel setTextColor:textColor];
            //
            //
            //            [cell.detailTextLabel setText:lafDetailObject.lafDetailRemark];
            
        //}
        txtRemark.text = lafDetailObject.lafDetailRemark;
    }
    else if(indexPath.section == sectionPhotoButton) {
        if (indexPath.row == 0) {
            //Photo
            photoViewScroll = (PhotoScrollViewV2 *) [cell.contentView viewWithTag:PHOTO_TAG];
            
            //            if (isUpdatePhoto) {
            if ([photoViewScroll.subviews count] > 0) {
                for (UIView* subview in photoViewScroll.subviews) {
                    [subview removeFromSuperview];
                }
            }
            
            lafDetailObject.lafDetailRoomId = [RoomManagerV2 getCurrentRoomNo];
            lafDetailObject.lafDetailUserId = [[UserManagerV2 sharedUserManager] currentUser].userId;
            [[LostandFoundManagerV2 sharedLostandFoundManagerV2] loadLaFDetailDataBy_UserIdand_RoomId:lafDetailObject];
            
            NSMutableArray *arrayPhoto = [NSMutableArray array];
            
            //load data in here
            if([lafDetailObject.lafPhoto count] <= 0) {
                if(isUpdatePhoto != YES) {
                    lafDetailObject.lafPhoto = [[LostandFoundManagerV2 sharedLostandFoundManagerV2] loadAllImageDataByLFDetailId:(int)lafDetailObject.lafDetailId];
                    arrayPhoto = [NSMutableArray arrayWithArray:lafDetailObject.lafPhoto];
                }
            }
            else {
                arrayPhoto = [NSMutableArray arrayWithArray:lafDetailObject.lafPhoto];
            }
            
            //first display the photo Button, and add more images if any. If the number of images=6 the photo Button is gray.
            CGFloat cxLocation = 0;
            if ([arrayPhoto count] == 0) {
                cxLocation += (photoViewScroll.frame.size.width - 80) / 2;
                UIImage *photoImage = [UIImage imageNamed:PHOTOIMAGE];
                //                    UIView *photoView=[[UIView alloc] initWithFrame:CGRectMake(cxLocation,5, WIDTH, HEIGHT)];
                UIButton *photoButton = [[UIButton alloc] initWithFrame:CGRectMake(cxLocation, 0, WIDTH, HEIGHT)];
                [photoButton setBackgroundImage:photoImage
                                       forState:UIControlStateNormal];
                [photoButton addTarget:self action:@selector(selectPhotoButton) forControlEvents:UIControlEventTouchDown];
                //                    [photoView addSubview:photoButton];
                [photoViewScroll addSubview:photoButton];
                //                    [photoButton release];
                //                    [photoView release];
            } else {
                if ([arrayPhoto count] > 0) {
                    if ([arrayPhoto count] < 6) {
                        cxLocation += 10;
                        UIImage *photoImage = [UIImage imageNamed:PHOTOIMAGE];
                        //                            UIView *photoView=[[UIView alloc] initWithFrame:CGRectMake(cxLocation,5, WIDTH, HEIGHT)];
                        UIButton *photoButton = [[UIButton alloc] initWithFrame:CGRectMake(cxLocation, 0, WIDTH, HEIGHT)];
                        [photoButton setBackgroundImage:photoImage forState:UIControlStateNormal];
                        [photoButton addTarget:self action:@selector(selectPhotoButton) forControlEvents:UIControlEventTouchDown];
                        //                            [photoView addSubview:photoButton];
                        [photoViewScroll addSubview:photoButton];
                        //                            [photoButton release];
                        //                            [photoView release];
                        cxLocation += WIDTH;
                    }
                    
                    for (int i = 0; i < [arrayPhoto count]; i++) {
                        LFImageModelV2 *photoImage = [arrayPhoto objectAtIndex:i];
                        cxLocation += 10;
                        //                                UIView *photoView=[[UIView alloc]initWithFrame:CGRectMake(cxLocation, 5, WIDTH, HEIGHT)];
                        UIButton *photoButton = [[UIButton alloc] initWithFrame:CGRectMake(cxLocation, 0, WIDTHIMAGE , HEIGHT)];
                        photoButton.tag = i;
                        [photoButton setBackgroundImage:[UIImage imageWithData:photoImage.lafImgImage]
                                               forState:UIControlStateNormal];
                        [photoButton addTarget:self action:@selector(tapImageToZoom:) forControlEvents:UIControlEventTouchUpInside];
                        UILongPressGestureRecognizer *longpressGesture=[[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(responseLongPressButton:)];
                        longpressGesture.delegate=self;
                        longpressGesture.minimumPressDuration=1;
                        [photoButton addGestureRecognizer:longpressGesture];
                        //                                [photoView addSubview:photoButton];
                        [photoViewScroll addSubview:photoButton];
                        cxLocation += WIDTH;
                        //                                [photoButton release];
                        //                                [photoView release];
                    }
                }
            }
            
            [photoViewScroll setContentSize:CGSizeMake(cxLocation, HEIGHT)];
            //
            //                isUpdatePhoto = NO;
            //            }
        }
    }
    //modify
    else if(indexPath.section == sectionSubmitButton) {
        if (indexPath.row == 0) {
            //                btnSubmit = (UIButton *)[cell viewWithTag:SUBMIT_TAG];
            btnSubmit = (UIButton *)[cell viewWithTag:tagOfSubmitButton];
            lblSubmit = (UILabel *)[cell viewWithTag:LABEL_SUBMIT_TAG];
        }
    }
    else {
        
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        UIImageView *indicatorView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_arrow (grey).png"]];
        [indicatorView setFrame:CGRectMake(0, 0, 26, 26)];
        [cell setAccessoryView:indicatorView];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [lblTitle setTextColor:labelColor];
        [lblTitle setFont:[UIFont fontWithName:FONTLABEL size:FONTLABELSIZE]];
        [lblSubTitle setFont:[UIFont fontWithName:FONTLABEL size:FONTTEXTSIZE]];
        [lblSubTitle setMinimumFontSize:FONTTEXTSIZE];
        [lblSubTitle setTextColor:textColor];
        lblTitle.text = @"";
        
        lblSubTitle.text = @"";
        
        if(indexPath.row < [lafDetailObject.lafArray count])
        {
            LostandFoundModelV2 *dataLaF = [lafDetailObject.lafArray objectAtIndex:indexPath.row];
            
            
            LFCategoryModelV2 *lafCategory = [[LFCategoryModelV2 alloc] init];
            lafCategory.lfcID = dataLaF.lafCategoryId;
            
            LFItemModelV2 *lafItem = [[LFItemModelV2 alloc] init];
            lafItem.lfitID = dataLaF.lafItemId;
            
            LFColorModelV2 *lafColor = [[LFColorModelV2 alloc] init];
            lafColor.lfcID = dataLaF.lafColor.lfcID;
            
            [[LostandFoundManagerV2 sharedLostandFoundManagerV2] loadCategoryData:lafCategory];
            [[LostandFoundManagerV2 sharedLostandFoundManagerV2] loadItemData:lafItem];
            [[LostandFoundManagerV2 sharedLostandFoundManagerV2] loadColorData:lafColor];
            
            if([[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString:[NSString stringWithFormat:@"%@",ENGLISH_LANGUAGE]]){
                lblTitle.text = lafCategory.lfcName;
                NSString *itemName = @"";
                if(lafItem.lfitID > 0)
                {
                    itemName = [NSString stringWithFormat:@"%@ x %d", lafItem.lfitName, (int)dataLaF.laf_item_quantity];
                    //-------Log action----
                    if([LogFileManager isLogConsole])
                    {
                        NSInteger userId = [[UserManagerV2 sharedUserManager] currentUser].userId;
                        NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
                        NSLog(@"<%@><L&F><UserId:%d><Input %@><ColorId:%d><at Index# %d>",time, (int)userId, itemName, (int)lafColor.lfcID, (int)indexPath.row);
                    }
                }
                lblSubTitle.text = itemName;
                
                if(lafColor.lfcID > 0)
                {
                    unsigned result = 0;
                    NSScanner *scanner = [NSScanner scannerWithString:lafColor.htmlCode];
                    [scanner setScanLocation:1];
                    
                    // bypass '#' character
                    [scanner scanHexInt:&result];
                    
                    UIColor *colorName = UIColorFromRGB(result);
                    [[btnColor layer] setBorderWidth:1.0f];
                    [[btnColor layer] setBorderColor:[[UIColor blackColor] CGColor]];
                    [[btnColor layer] setCornerRadius:8.0f];
                    [[btnColor layer] setMasksToBounds:YES];
                    [[btnColor layer] setBackgroundColor:[colorName CGColor]];
                    [[btnColor layer] setNeedsDisplay];
                    
                    CGRect rect = CGRectMake(0, 0, 40, 40);
                    UIGraphicsBeginImageContext(rect.size);
                    CGContextRef context = UIGraphicsGetCurrentContext();
                    CGContextSetFillColorWithColor(context, [colorName CGColor]);
                    CGContextFillRect(context, rect);
                    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
                    UIGraphicsEndImageContext();
                    [btnColor setImage:img forState:UIControlStateNormal];
                    
                    //                    [btnColor setImage:[UIImage imageNamed:@"btnSubmit.png"] forState:UIControlStateNormal];
                    //                    btnColor.frame = CGRectMake(100, 5, 40, 40);
                }
                else {
                    [btnColor setImage:[UIImage imageNamed:@"no_color.png"] forState:UIControlStateNormal];
                }
                
            }
            else{
                lblTitle.text = lafCategory.lfcLang;
                if(lafItem.lfitID > 0){
                    lblSubTitle.text = [NSString stringWithFormat:@"%@ x %d", lafItem.lfitLang, (int)dataLaF.laf_item_quantity];
                }
                //---Log action
                if([LogFileManager isLogConsole])
                {
                    NSInteger userId = [[UserManagerV2 sharedUserManager] currentUser].userId;
                    NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
                    NSLog(@"<%@><L&F><UserId:%d><%@>",time, (int)userId, lblSubTitle.text);
                }
            }
            
            
            
        }
        
        
        
        //       switch (indexPath.row) {
        //            case 0: {   //Category row
        //                cell.textLabel.text = [NSString stringWithFormat:@"%@ %d", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_category_title], indexPath.section + 1];
        
        
        //                cell.textLabel.text = @"";
        //                [cell.textLabel setTextColor:labelColor];
        //                [cell.textLabel setFont:[UIFont fontWithName:FONTLABEL size:FONTLABELSIZE]];
        //                [cell.detailTextLabel setFont:[UIFont fontWithName:FONTLABEL size:FONTTEXTSIZE]];
        //                [cell.detailTextLabel setMinimumFontSize:FONTTEXTSIZE];
        //                [cell.detailTextLabel setTextColor:textColor];
        
        
        //                if (dataLaF.lafCategoryId == -1) {
        //                    cell.detailTextLabel.text = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_undefined_value];
        //                } else {
        //                    cell.detailTextLabel.text = dataLaF.lafCat.lfcName;
        //                }
        
        //load lost and found category
        
        
        //                if(lafCategory.lfcID > 0)
        //                {
        //                lafCategory.lfcName = @"AA";
        //                lafCategory.lfcLang = @"BB";
        //                }
        //
        //                if(lafItem.lfitID > 0)
        //                {
        //                    lafItem.lfitName = @"A1";
        //                    lafItem.lfitLang = @"B1";
        //                }
        
        /////////////////////
        
        
        
        //            }
        //                break;
        //
        //           case 1: {   //Item row
        //                cell.textLabel.text = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ITEM];
        //                [cell.textLabel setTextColor:labelColor];
        //                [cell.textLabel setFont:[UIFont fontWithName:FONTLABEL size:FONTLABELSIZE]];
        //                [cell.detailTextLabel setFont:[UIFont fontWithName:FONTLABEL size:FONTTEXTSIZE]];
        //                [cell.detailTextLabel setMinimumFontSize:FONTTEXTSIZE];
        //                [cell.detailTextLabel setTextColor:textColor];
        //
        ////                if (dataLaF.lafItemId == -1) {
        ////                    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ x %d", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_undefined_value],0];
        ////                } else {
        ////                    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ x %d",dataLaF.lafItem.lfitName, dataLaF.laf_item_quantity];
        ////                }
        //
        //               //load laf item
        //               LFItemModelV2 *lafItem = [[LFItemModelV2 alloc] init];
        //               lafItem.lfitID = dataLaF.lafItemId;
        //               [[LostandFoundManagerV2 sharedLostandFoundManagerV2] loadItemData:lafItem];
        //
        //
        //               cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ x %d", [[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString:[NSString stringWithFormat:@"%@",ENGLISH_LANGUAGE]] ? (lafItem.lfitName != nil ? lafItem.lfitName : @"") : (lafItem.lfitLang != nil ? lafItem.lfitLang : @"") , dataLaF.laf_item_quantity];
        //            }
        //                break;
        //
        //            case 2: {   //Color row
        //                cell.textLabel.text = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_color_title];
        //                [cell.textLabel setTextColor:labelColor];
        //                [cell.textLabel setFont:[UIFont fontWithName:FONTLABEL size:FONTLABELSIZE]];
        //
        //                UIImageView *btnColor = (UIImageView *)[cell.contentView viewWithTag:KTAGCOLORBUTTON];
        //                unsigned result = 0;
        //                NSScanner *scanner = [NSScanner scannerWithString:
        //                                      dataLaF.lafColor.htmlCode == nil ? @"#000000" : dataLaF.lafColor.htmlCode];
        //                [scanner setScanLocation:1];
        //
        //                // bypass '#' character
        //                [scanner scanHexInt:&result];
        //
        //                UIColor *colorName = UIColorFromRGB(result);
        //                [[btnColor layer] setCornerRadius:8.0f];
        //                [[btnColor layer] setMasksToBounds:YES];
        //                [[btnColor layer] setBorderWidth:1.0f];
        //                [[btnColor layer] setBorderColor:[[UIColor grayColor] CGColor]];
        //                [[btnColor layer] setBackgroundColor:[colorName CGColor]];
        //
        ////                [colorData release];
        //            }
        //                break;
        //
        //            default:
        //                break;
        //        }
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
    [cell setSelected:NO animated:NO];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == sectionPhotoButton) {
        // process the Photo in here
    }
    else if (indexPath.section == sectionRemark) {
        [self showRemarkView];
    }
    else if (indexPath.section == sectionSubmitButton) {
        
    }
    else {
        indexTemp = indexPath.row;
        //        switch (indexPath.row) {
        //            case 0: {   //Category row
        LFCategoryViewV2 *vCategory = [[LFCategoryViewV2 alloc]initWithNibName:@"LFCategoryViewV2" bundle:Nil];
        
        UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK] style:UIBarButtonItemStyleBordered target:nil action:nil];
        self.navigationItem.backBarButtonItem = backButton;
        
        vCategory.existedLaFObject = [lafDetailObject.lafArray objectAtIndex:indexPath.row];
        vCategory.selectedSectionIndex = indexPath.row;
        vCategory.delegate = self;
        [self.navigationController pushViewController:vCategory animated:YES];
        //                [backButton release];
        //                [vCategory release];
        //            }
        //                break;
        //
        //            case 1: {   //Item row
        //                LFItemViewV2 *vItem = [[LFItemViewV2 alloc] initWithNibName:@"LFItemViewV2" bundle:Nil];
        //
        //                UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK] style:UIBarButtonItemStyleBordered target:nil action:nil];
        //                self.navigationItem.backBarButtonItem = backButton;
        //                [vItem setTitle:[[LanguageManagerV2 sharedLanguageManager]getItems]];
        //                vItem.existedLaFObject=[lafDetailObject.lafArray objectAtIndex:indexPath.section];
        //                vItem.selectedSectionIndex = indexPath.section;
        //                vItem.delegate = self;
        //
        //                [self.navigationController pushViewController:vItem animated:YES];
        ////                [backButton release];
        ////                [vItem release];
        //            }
        //                break;
        //
        //            case 2: {  //Color row
        //                LFColorViewV2 *vColor = [[LFColorViewV2 alloc]initWithNibName:@"LFColorViewV2" bundle:Nil ];
        //                vColor.existedLaFObject = [lafDetailObject.lafArray objectAtIndex:indexPath.section];
        //                vColor.selectedSection = indexPath.section;
        //                vColor.delegate = self;
        //
        //                UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK] style:UIBarButtonItemStyleBordered target:nil action:nil];
        //                self.navigationItem.backBarButtonItem = backButton;
        //                [vColor setTitle:[[LanguageManagerV2 sharedLanguageManager] getLOSTANDFOUND]];
        //                [self.navigationController pushViewController:vColor animated:YES];
        ////                [backButton release];
        ////                [vColor release];
        //            }
        //                break;
        //
        //            default:
        //                break;
        //        }
    }
//    UITableViewCell *_cell = [tableView cellForRowAtIndexPath:indexPath];
//    [_cell setSelected:NO animated:NO];
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section < [lafDetailObject.lafArray count] && indexPath.row == 0 && indexPath.section > 0)
        return UITableViewCellEditingStyleDelete;
    else
        return UITableViewCellEditingStyleNone;
}

- (void)tableView:(UITableView *)aTableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        if (indexPath.section < [lafDetailObject.lafArray count]) {
            self.indexDelete = indexPath.section;
            UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_delete_confirmation] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles:[[LanguageManagerV2 sharedLanguageManager] getNo], nil];
            [alert setTag:tagDeleteCategory];
            [alert show];
            //            [alert release];
        }
    }
}

#pragma mark - Load Data
-(void)reloadLaFData {
    //first of all, load the lostandfoundetail base Userid and RoomId in local database
    if (!self.lafDetailObject) {
        self.lafDetailObject = [[LostandFoundDetailModelV2 alloc] init];
    }
    [locationTextField setText:@""];
    countRow = 1;
    [lafDetailObject.lafArray removeAllObjects];
    [self initLFArray];
    //[tvRemark setText:@""];//get the UItext view from Log action
    
    NSMutableArray *arrayPhoto = [[LostandFoundManagerV2 sharedLostandFoundManagerV2] loadAllImageDataByLFDetailId:(int)lafDetailObject.lafDetailId];
    
    if([arrayPhoto count] > 0) {
        for(int i = 0; i < [arrayPhoto count]; i++) {
            LFImageModelV2 *image = [arrayPhoto objectAtIndex:i];
            [[LostandFoundManagerV2 sharedLostandFoundManagerV2] deleteImageData:image];
        }
    }
    
    lafDetailObject.lafDetailUserId = [[UserManagerV2 sharedUserManager]
                                       currentUser].userId;
    lafDetailObject.lafDetailRoomId = [RoomManagerV2 getCurrentRoomNo];
    [[LostandFoundManagerV2 sharedLostandFoundManagerV2] loadLaFDetailDataBy_UserIdand_RoomId:lafDetailObject];
    
    /*secondly, create the state of lostandfoundetail variable (isCreate if it doesn't exist in database), load all of lostandfound objects in local database*/
    if (!lafDetailObject.lafArray) {
        lafDetailObject.lafArray = [[NSMutableArray alloc] init];
    }
    else {
        [lafDetailObject.lafArray removeAllObjects];
    }
    
    // initalise the lostandfound Photos ind.
    if (!lafDetailObject.lafPhoto) {
        lafDetailObject.lafPhoto = [[NSMutableArray alloc] init];
    }
    else {
        [lafDetailObject.lafPhoto removeAllObjects];
    }
    
    if (lafDetailObject.lafDetailId != 0) {
        lafDetailObject.lafDetail_Status = isCreate;
    }
    else {
        lafDetailObject.lafDetail_Status = haveDataBase;
        [lafDetailObject.lafArray addObjectsFromArray:[[LostandFoundManagerV2 sharedLostandFoundManagerV2] loadAllLaFDataByLaFDetailId:(int)lafDetailObject.lafDetailId]];
        
        // in addition,  add the state of lostandfound variables.
        if ([lafDetailObject.lafArray count] > 0) {
            for(LostandFoundModelV2 *lafModel in lafDetailObject.lafArray) {
                lafModel.laf_Status = inDataBase;
                if (!lafModel.lafCat) {
                    lafModel.lafCat = [[LFCategoryModelV2 alloc] init];
                }
                
                lafModel.lafCat.lfcID = lafModel.lafCategoryId;
                [[LostandFoundManagerV2 sharedLostandFoundManagerV2] loadCategoryData:lafModel.lafCat];
                
                if (!lafModel.lafItem) {
                    lafModel.lafItem = [[LFItemModelV2 alloc] init];
                }
                
                lafModel.lafItem.lfitID = lafModel.lafItemId;
                [[LostandFoundManagerV2 sharedLostandFoundManagerV2] loadItemData:lafModel.lafItem];
                
                if (!lafModel.lafColor) {
                    lafModel.lafColor = [[LFColorModelV2 alloc] init];
                }
                
                lafModel.lafColor.lfcID = lafModel.lafColorId;
                [[LostandFoundManagerV2 sharedLostandFoundManagerV2] loadColorData:lafModel.lafColor];
            }
        }
        
        [lafDetailObject.lafPhoto addObjectsFromArray:[[LostandFoundManagerV2 sharedLostandFoundManagerV2] loadAllImageDataByLFDetailId:(int)lafDetailObject.lafDetailId]];
        
        if ([lafDetailObject.lafPhoto count] > 0) {
            for (LFImageModelV2 *imageModel in lafDetailObject.lafPhoto) {
                imageModel.lafStatus = inDataBaseI;
            }
        }
    }
    
    // check in  the case of no members in lostandfound array.
    if ([lafDetailObject.lafArray count] == 0) {
        
        LostandFoundModelV2 *lafTempObject = [[LostandFoundModelV2 alloc] init];
        //        lafTempObject.lafCategoryId = 1;
        lafTempObject.lafCategoryId = 0;
        lafTempObject.lafColorId = COLOR_BLACK_LAF_ID;
        //        lafTempObject.lafItemId = 16;
        lafTempObject.lafItemId = 0;
        
        lafTempObject.lafCat = [[LFCategoryModelV2 alloc] init];
        lafTempObject.lafCat.lfcID = lafTempObject.lafCategoryId;
        [[LostandFoundManagerV2 sharedLostandFoundManagerV2] loadCategoryData:lafTempObject.lafCat];
        
        lafTempObject.lafItem = [[LFItemModelV2 alloc] init];
        lafTempObject.lafItem.lfitID = lafTempObject.lafItemId;
        [[LostandFoundManagerV2 sharedLostandFoundManagerV2] loadItemData:lafTempObject.lafItem];
        
        lafTempObject.lafColor = [[LFColorModelV2 alloc] init];
        lafTempObject.lafColor.lfcID = lafTempObject.lafColorId;
        [[LostandFoundManagerV2 sharedLostandFoundManagerV2] loadColorData:lafTempObject.lafColor];
        
        lafTempObject.lafLaFDetailId = lafDetailObject.lafDetailId;
        lafTempObject.laf_Status = isNeedCreate;
        [lafDetailObject.lafArray addObject:lafTempObject];
        
        //set isneedcreate status for new laf
        lafDetailObject.lafDetail_Status = isCreate;
    }
    
    [self setHiddenForAddButton];
    
    //clear remark
    lafDetailObject.lafDetailRemark = @"";
    
    [lostfoundTableView reloadData];
}

-(void)loadData {
    //enable flag to announce, is loading data
    [[HomeViewV2 shareHomeView] waitingLoadingData];
    
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [hud setLabelText:[[LanguageManagerV2 sharedLanguageManager] getLoadingData]];
    [self.tabBarController.view addSubview:hud];
    [hud show:YES];
    
    /* TuanVo - Start Get Data LostAndFound Color, Category, Item from WS*/
    [[LostandFoundManagerV2 sharedLostandFoundManagerV2] getLaFColorFromWS:[NSNumber numberWithInt:[[UserManagerV2 sharedUserManager] currentUser].userId] andLastModifier:@""];
    //[[LostandFoundManagerV2 sharedLostandFoundManagerV2] getLaFCategoryFromWS:[NSNumber numberWithInt:[[UserManagerV2 sharedUserManager] currentUser].userId] andLastModifier:@""];
    NSString *hotelId = [NSString stringWithFormat:@"%d", [[[UserManagerV2 sharedUserManager] currentUser] userHotelsId]];
    [[LostandFoundManagerV2 sharedLostandFoundManagerV2] getLaFCategoryFromWS:[NSNumber numberWithInt:[[UserManagerV2 sharedUserManager] currentUser].userId] andLastModifier:@"" andHotelID:hotelId];
    
    //[[LostandFoundManagerV2 sharedLostandFoundManagerV2] getLaFItemFromWS:[NSNumber numberWithInt:[[UserManagerV2 sharedUserManager] currentUser].userId] andLastModifier:@""];
    [[LostandFoundManagerV2 sharedLostandFoundManagerV2] getLaFItemFromWS:[NSNumber numberWithInt:[[UserManagerV2 sharedUserManager] currentUser].userId] andLastModifier:@"" andHotelID:hotelId];
    /****************************** TuanVo End *********************************/
    
    //first of all, load the lostandfoundetail base Userid and RoomId in local database
    if (!self.lafDetailObject) {
        self.lafDetailObject = [[LostandFoundDetailModelV2 alloc] init];
    }
    
    lafDetailObject.lafDetailUserId = [[UserManagerV2 sharedUserManager] currentUser].userId;
    lafDetailObject.lafDetailRoomId = isFromMainPosting ? @"" :[RoomManagerV2 getCurrentRoomNo];
    [[LostandFoundManagerV2 sharedLostandFoundManagerV2] loadLaFDetailDataBy_UserIdand_RoomId:lafDetailObject];
    
    /*secondly, create the state of lostandfoundetail variable (isCreate if it doesn't exist in database), load all of lostandfound objects in local database*/
    if (!lafDetailObject.lafArray) {
        lafDetailObject.lafArray = [[NSMutableArray alloc] init];
    }
    else {
        [lafDetailObject.lafArray removeAllObjects];
    }
    
    // initalise the lostandfound Photos ind.
    if (!lafDetailObject.lafPhoto) {
        lafDetailObject.lafPhoto = [[NSMutableArray alloc] init];
    }
    
    if (lafDetailObject.lafDetailId == 0) {
        lafDetailObject.lafDetail_Status = isCreate;
    }
    else {
        lafDetailObject.lafDetail_Status = haveDataBase;
        [lafDetailObject.lafArray addObjectsFromArray:[[LostandFoundManagerV2 sharedLostandFoundManagerV2] loadAllLaFDataByLaFDetailId:(int)lafDetailObject.lafDetailId]];
        
        // in addition,  add the state of lostandfound variables.
        if ([lafDetailObject.lafArray count] > 0) {
            for(LostandFoundModelV2 *lafModel in lafDetailObject.lafArray) {
                lafModel.laf_Status = inDataBase;
                if (!lafModel.lafCat) {
                    lafModel.lafCat = [[LFCategoryModelV2 alloc] init];
                }
                
                lafModel.lafCat.lfcID = lafModel.lafCategoryId;
                [[LostandFoundManagerV2 sharedLostandFoundManagerV2] loadCategoryData:lafModel.lafCat];
                
                if (!lafModel.lafItem) {
                    lafModel.lafItem = [[LFItemModelV2 alloc] init];
                }
                
                lafModel.lafItem.lfitID = lafModel.lafItemId;
                [[LostandFoundManagerV2 sharedLostandFoundManagerV2] loadItemData:lafModel.lafItem];
                
                if (!lafModel.lafColor) {
                    lafModel.lafColor = [[LFColorModelV2 alloc] init];
                }
                
                lafModel.lafColor.lfcID = lafModel.lafColorId;
                [[LostandFoundManagerV2 sharedLostandFoundManagerV2] loadColorData:lafModel.lafColor];
            }
        }
        
        [lafDetailObject.lafPhoto addObjectsFromArray:[[LostandFoundManagerV2 sharedLostandFoundManagerV2] loadAllImageDataByLFDetailId:(int)lafDetailObject.lafDetailId]];
        
        if ([lafDetailObject.lafPhoto count] > 0) {
            for (LFImageModelV2 *imageModel in lafDetailObject.lafPhoto) {
                imageModel.lafStatus = inDataBaseI;
            }
        }
    }
    
    [self initLFArray];
    
    
    
    [self performSelector:@selector(hiddenHUDAfterSaved:) withObject:hud
               afterDelay:0.5];
    
    [self setHiddenForAddButton];
    
    [lostfoundTableView reloadData];
    
    
    //end loading data
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
}

-(void) initLFArray
{
    // check in  the case of no members in lostandfound array.
    if ([lafDetailObject.lafArray count] == 0) {
        
        LostandFoundModelV2 *lafTempObject = [[LostandFoundModelV2 alloc] init];
        //        lafTempObject.lafCategoryId = 1;
        lafTempObject.lafCategoryId = 0;
        lafTempObject.lafColorId = COLOR_BLACK_LAF_ID;
        //        lafTempObject.lafItemId = 16;
        lafTempObject.lafItemId = 0;
        
        lafTempObject.lafCat = [[LFCategoryModelV2 alloc] init];
        lafTempObject.lafCat.lfcID = lafTempObject.lafCategoryId;
        [[LostandFoundManagerV2 sharedLostandFoundManagerV2] loadCategoryData:lafTempObject.lafCat];
        
        lafTempObject.lafItem = [[LFItemModelV2 alloc] init];
        lafTempObject.lafItem.lfitID = lafTempObject.lafItemId;
        [[LostandFoundManagerV2 sharedLostandFoundManagerV2] loadItemData:lafTempObject.lafItem];
        
        lafTempObject.lafColor = [[LFColorModelV2 alloc] init];
        lafTempObject.lafColor.lfcID = lafTempObject.lafColorId;
        [[LostandFoundManagerV2 sharedLostandFoundManagerV2] loadColorData:lafTempObject.lafColor];
        
        lafTempObject.lafLaFDetailId = lafDetailObject.lafDetailId;
        lafTempObject.laf_Status = isNeedCreate;
        [lafDetailObject.lafArray addObject:lafTempObject];
        
        //set isneedcreate status for new laf
        lafDetailObject.lafDetail_Status = isCreate;
    }
}

-(void) hiddenHUDAfterSaved:(MBProgressHUD *)hud{
    [hud hide:YES];
    [hud removeFromSuperview];
}

#pragma mark - Save Data
-(void)saveData {
    
    //save all data as tapping Save Button
    //insert all lafDetail
    //check the status of lafDetail
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self.view];
    [hud setLabelText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_RA_SAVING_DATA]];
    [self.view addSubview:hud];
    [hud show:YES];
    
    //    lafDetailObject.lafDetailRoomId = [RoomManagerV2 getCurrentRoomNo];
    
    //Post Lost And Found with Room Number or Room Assignment Id Config
    if(ENABLE_POST_LOSTANDFOUND_WITH_ROOM_NUMBER)
    {
        if(!isFromMainPosting) {
            //Hao Tran Add to post lost and found with Room Assignment Id
            RoomAssignmentModelV2 *roomAssignment = [[RoomAssignmentModelV2 alloc] init];
            roomAssignment.roomAssignment_RoomId = locationTextField.text;
            roomAssignment.roomAssignment_UserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
            [[RoomManagerV2 sharedRoomManager] getRoomAssignmentByUserIDAndRoomID:roomAssignment];
            lafDetailObject.lafRoomAssignmentId = roomAssignment.roomAssignment_Id;
        } else {
            lafDetailObject.lafRoomAssignmentId = 0;
        }
        lafDetailObject.lafDetailRoomId = locationTextField.text;
    }
    else
    {
        //Hao Tran Add to post lost and found with Room Assignment Id
        RoomAssignmentModelV2 *roomAssignment = [[RoomAssignmentModelV2 alloc] init];
        roomAssignment.roomAssignment_RoomId = locationTextField.text;
        roomAssignment.roomAssignment_UserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
        [[RoomManagerV2 sharedRoomManager] getRoomAssignmentByUserIDAndRoomID:roomAssignment];
        lafDetailObject.lafDetailRoomId = [NSString stringWithFormat:@"%d", roomAssignment.roomAssignment_Id];
    }
    
    //Hold remark before load old lost and found data
    NSString *detailRemark = lafDetailObject.lafDetailRemark;
    lafDetailObject.lafDetailUserId = [[UserManagerV2 sharedUserManager] currentUser].userId;
    [[LostandFoundManagerV2 sharedLostandFoundManagerV2] loadLaFDetailDataBy_UserIdand_RoomId:lafDetailObject];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    //    [dateFormat setDateFormat:@"dd-MMM-yyyy HH:mm:ss"];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *now = [NSDate date];
    lafDetailObject.lafDetailDate = [dateFormat stringFromDate:now];
    //get remark text
    //NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:sectionRemark];
    //UITableViewCell *cell = [lostfoundTableView cellForRowAtIndexPath:indexPath];
    //UITextView *tvRemark = (UITextView *)[cell viewWithTag:remarkTag];
    //lafDetailObject.lafDetailRemark = [NSString stringWithFormat:@"%@", tvRemark.text];
    
    lafDetailObject.lafDetailRemark = detailRemark;
    if(lafDetailObject.lafDetailId != 0) {
        int numberResutl = [[LostandFoundManagerV2 sharedLostandFoundManagerV2] updateLaFDetailData:lafDetailObject];
        if (numberResutl == 1) {
            lafDetailObject.lafDetail_Status = haveDataBase;
        }
    }
    else if(lafDetailObject.lafDetailId == 0) {
        int numberResutl = [[LostandFoundManagerV2 sharedLostandFoundManagerV2] insertLaFDetailData:lafDetailObject];
        
        if (numberResutl == 1) {
            lafDetailObject.lafDetail_Status = haveDataBase;
        }
    }
    
    [[LostandFoundManagerV2 sharedLostandFoundManagerV2] loadLaFDetailDataBy_UserIdand_RoomId:lafDetailObject];//add
    
    if (lafDetailObject.lafDetail_Status == isUpdate) {
        int numberResutl = [[LostandFoundManagerV2 sharedLostandFoundManagerV2] updateLaFDetailData:lafDetailObject];
        if (numberResutl == 1) {
            lafDetailObject.lafDetail_Status = haveDataBase;
        }
    }
    else if(lafDetailObject.lafDetail_Status == isCreate) {
        [[LostandFoundManagerV2 sharedLostandFoundManagerV2] insertLaFDetailData:lafDetailObject];
    }
    
    //    lafDetailObject.lafDetailRoomId = isFromMainPosting?0:[RoomManagerV2 getCurrentRoomNo];
    //    lafDetailObject.lafDetailUserId = [[UserManagerV2 sharedUserManager] currentUser].userId;
    //    [[LostandFoundManagerV2 sharedLostandFoundManagerV2] loadLaFDetailDataBy_UserIdand_RoomId:lafDetailObject];
    
    //Process all photos of LostandFoundDetail
    if ([photoDeleteList count] > 0) {
        for ( LFImageModelV2 *deletePhoto in photoDeleteList) {
            [[LostandFoundManagerV2 sharedLostandFoundManagerV2] deleteImageData:deletePhoto];
        }
        
        [photoDeleteList removeAllObjects];
    }
    
    for (LFImageModelV2 *imageModel in lafDetailObject.lafPhoto) {
        if(imageModel.lafStatus == isNeedCreateI) {
            imageModel.lafDetailId = lafDetailObject.lafDetailId;
            int isSuccess = [[LostandFoundManagerV2 sharedLostandFoundManagerV2] insertImageData:imageModel];
            
            if(isSuccess == 0)
                [[LostandFoundManagerV2 sharedLostandFoundManagerV2] updateImageData:imageModel];
            
            imageModel.lafStatus = inDataBaseI;
        }
    }
    
    //Process all laf into lafdetail
    if ([lafDeleteList count] > 0) {
        for (LostandFoundModelV2  *deleteLaF in lafDeleteList) {
            [[LostandFoundManagerV2 sharedLostandFoundManagerV2] deleteLaFData:deleteLaF];
            [lafDeleteList removeObject:deleteLaF];
        }
    }
    if(!isDemoMode){
        NSInteger i = [lafDetailObject.lafArray count];
        [lafDetailObject.lafArray removeObjectAtIndex:(i - 1)];
    }
    
    
    for (LostandFoundModelV2 *lafModel in lafDetailObject.lafArray) {
        if (lafModel.laf_Status == isNeedCreate) {
            lafModel.lafLaFDetailId = lafDetailObject.lafDetailId;
            //update
            lafModel.lafColorId = lafModel.lafColor.lfcID;
            int result = [[LostandFoundManagerV2 sharedLostandFoundManagerV2] insertLaFData:lafModel];
            
            if (result == 1)
                lafModel.laf_Status = inDataBase;
        }
        else if(lafModel.laf_Status == isNeedUpdate) {
            int result = [[LostandFoundManagerV2 sharedLostandFoundManagerV2] updateLaFData:lafModel];
            
            if (result == 1)
                lafModel.laf_Status = inDataBase;
        }
    }
    
    lafDetailObject.lafPhoto = [[LostandFoundManagerV2 sharedLostandFoundManagerV2] loadAllImageDataByLFDetailId:(int)lafDetailObject.lafDetailId];
    
    //    lafDetailObject.lafArray = [[LostandFoundManagerV2 sharedLostandFoundManagerV2] loadAllLaFDataByLaFDetailId:lafDetailObject.lafDetailId];
    
    //-------------------------------Post WSLog
    //    int currentCountRow = [lafDetailObject.lafArray count]>maxRowOfTableItem ? (countRow):(countRow - 1);
    NSString *date = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSInteger userId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    
    int switchStatus = (int)[[NSUserDefaults standardUserDefaults] integerForKey:ENABLE_LOGWS];
    if  (switchStatus == 1){
        //        for(int i = 0;i< currentCountRow; i++){
        
        for (LostandFoundModelV2 *lafModel in lafDetailObject.lafArray) {
            if(lafModel.lafItemId > 0)
            {
                NSString *itemName = lafModel.lafItem.lfitName;
                int itemQty = (int)lafModel.laf_item_quantity;
                NSInteger colorId = lafModel.lafColor.lfcID;
                NSString *messageValue = [NSString stringWithFormat:@"Lost&Found post WSLog: Time:%@, UserId:%d, RoomId:%@, Remark:%@, Category:%@(Id:%d) x %d, ColorId:%d", date, (int)userId, locationTextField.text, lafDetailObject.lafDetailRemark, itemName, (int)lafModel.lafItemId, itemQty, (int)colorId];
                [[LostandFoundManagerV2 sharedLostandFoundManagerV2] postWSLog:userId Message:messageValue MessageType:1];
            }
            
        }
        //        }
    }
    
    
    int resultPost = 0;
    
    // post data in here
    // post photo in here
    if(isDemoMode){
        resultPost = RESPONSE_STATUS_OK;
    }
    else{
        if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp] == TRUE) {
            
            //CN-1274: Remove for new API in post lost and found
            /*
            for (LFImageModelV2 *imageModel in lafDetailObject.lafPhoto) {
                isSuccessPost = [[LostandFoundManagerV2 sharedLostandFoundManagerV2] postLaFPhoto:lafDetailObject andPhoto:imageModel];
            }
            
            if(isSuccessPost == TRUE || [lafDetailObject.lafPhoto count] <= 0) {
                for (int i = 0; i < [lafDetailObject.lafArray count]; i++) {
                    LostandFoundModelV2 *lafModel = [lafDetailObject.lafArray objectAtIndex:i];
                    resultPost = [[LostandFoundManagerV2 sharedLostandFoundManagerV2] postLostandFound:lafDetailObject LostAndFound:lafModel];
                }
            }*/
            
            resultPost = [[LostandFoundManagerV2 sharedLostandFoundManagerV2] postLostandFound:lafDetailObject];
        }
    }
    
    if (resultPost == RESPONSE_STATUS_OK ||resultPost == RESPONSE_STATUS_NEW_RECORD_ADDED) {
        [hud setLabelText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_save_success]];
        //reload view after post

        [self saveLafHistoryData];
        [self reloadLaFData];
        [[[[iToast makeText:[[LanguageManagerV2 sharedLanguageManager] getMsgSaveMessage]] setGravity:iToastGravityBottom] setDuration:iToastDurationNormal] show];
                if(isDemoMode){
                    [self.navigationController popViewControllerAnimated:YES];
                }
    } else {
        
    }
    
    
    [self hiddenHUDAfterSaved:hud];
}

-(void) saveLafHistoryData{
    
    LostandFoundHistoryModel *lafHistoryModel =[[LostandFoundHistoryModel alloc] init];
    lafHistoryModel.lafh_user_id = [[UserManagerV2 sharedUserManager] currentUser].userId;
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *now = [NSDate date];
    lafHistoryModel.lafh_date = [dateFormat stringFromDate:now];
    lafHistoryModel.lafh_room_id = locationTextField.text;
    lafDetailObject.lafDetailRoomId = [RoomManagerV2 getCurrentRoomNo];
    for (LostandFoundModelV2 *lafModel in lafDetailObject.lafArray) {
        lafHistoryModel.lafh_item_id = lafModel.lafItemId;
        lafHistoryModel.lafh_color_id = lafModel.lafColorId;
        lafHistoryModel.lafh_quantity = lafModel.laf_item_quantity;
        LostandFoundHistoryManager *lafhManager = [[LostandFoundHistoryManager alloc] init];
        [lafhManager insertLaFHistoryData:lafHistoryModel];
    }
    //    [lafDetailObject.lafArray removeAllObjects];
}


#pragma mark - === Handle Check Save Data ===
#pragma mark
-(BOOL) checkSaveDataWithTagEvent:(NSInteger) tagOfEvent {
    if ([self checkSaveDataWithOutAlert] == NO) {
        //NO
        
        //can go to message screen
        if (tagOfEvent == tagOfMessageButton) {
            //set status room is not completed
            [[CommonVariable sharedCommonVariable] setRoomIsCompleted:0];
            
            return NO;
        }
        
        //show please complete the room
        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_complete_room_to_leave] delegate:nil cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getOK] otherButtonTitles: nil];
        [alert show];
        
        return YES; //no need to save before leaving, but need to complete the room before leaving
    } else {
        //YES
        //need to save before leaving
        
        if (tagOfEvent == tagOfMessageButton) {
            UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ALERT_SAVEROOMDETAIL] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles:[[LanguageManagerV2 sharedLanguageManager] getNo], nil];
            alert.tag = tagSaveLeaving;
            [alert show];
            
        } else {
            //show please complete the room
            UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_complete_room_to_leave] delegate:nil cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getOK] otherButtonTitles: nil];
            [alert show];
        }
        
        
        return YES;
    }
}

-(BOOL) checkSaveDataWithOutAlert {
    if (lafDetailObject.lafDetail_Status == inDataBase) {
        for (LostandFoundModelV2 * lafModelObj in lafDetailObject.lafArray) {
            if (lafModelObj.laf_Status != inDataBase) {
                
                return YES;
            }
        }
        
        for (LFImageModelV2 *lafImageModel in lafDetailObject.lafPhoto) {
            if (lafImageModel.lafStatus != inDataBaseI) {
                
                return YES;
            }
        }
        
        if ([photoDeleteList count] == 0 && [lafDeleteList count] == 0) {
            
            return NO;
        } else {
            
            return YES;
        }
    } else {
        if (lafDetailObject.lafDetail_Status == isCreate) {
            return NO;
        }
        
        return YES;
    }
}

-(BOOL) checkSaveData {
    
    
    //////////////////////////////////////////////////////////////////////////
    if (lafDetailObject.lafDetail_Status == inDataBase) {
        for (LostandFoundModelV2 * lafModelObj in lafDetailObject.lafArray) {
            if (lafModelObj.laf_Status != inDataBase) {
                UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ALERT_SAVEROOMDETAIL] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles:[[LanguageManagerV2 sharedLanguageManager] getNo], nil];
                alert.tag = tagSaveLeaving;
                [alert show];
                return YES;
            }
        }
        
        for (LFImageModelV2 *lafImageModel in lafDetailObject.lafPhoto) {
            if (lafImageModel.lafStatus != inDataBaseI) {
                UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ALERT_SAVEROOMDETAIL] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles:[[LanguageManagerV2 sharedLanguageManager] getNo], nil];
                alert.tag = tagSaveLeaving;
                [alert show];
                return YES;
            }
        }
        
        if ([photoDeleteList count] == 0 && [lafDeleteList count] == 0) {
            
            return NO;
        } else {
            UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ALERT_SAVEROOMDETAIL] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles:[[LanguageManagerV2 sharedLanguageManager] getNo], nil];
            alert.tag = tagSaveLeaving;
            [alert show];
            return YES;
        }
    } else {
        if (lafDetailObject.lafDetail_Status == isCreate) {
            return NO;
        }
        
        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ALERT_SAVEROOMDETAIL] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles:[[LanguageManagerV2 sharedLanguageManager] getNo], nil];
        alert.tag = tagSaveLeaving;
        [alert show];
        return YES;
    }
    /////////////////////////////////////////////////////////////////////////
}

#pragma mark - Edit Methods
//process click add button
-(void)addCategorytoTableView {
    LostandFoundModelV2 *lafTempObject = [[LostandFoundModelV2 alloc] init];
    
    //    lafTempObject.lafCategoryId = 1;
    lafTempObject.lafCategoryId = 0;
    lafTempObject.lafColorId = COLOR_BLACK_LAF_ID;
    //    lafTempObject.lafItemId = 16;
    lafTempObject.lafItemId = 0;
    
    lafTempObject.lafCat = [[LFCategoryModelV2 alloc] init];
    lafTempObject.lafCat.lfcID = lafTempObject.lafCategoryId;
    [[LostandFoundManagerV2 sharedLostandFoundManagerV2] loadCategoryData:
     lafTempObject.lafCat];
    
    lafTempObject.lafItem = [[LFItemModelV2 alloc] init];
    lafTempObject.lafItem.lfitID = lafTempObject.lafItemId;
    [[LostandFoundManagerV2 sharedLostandFoundManagerV2] loadItemData:
     lafTempObject.lafItem];
    
    lafTempObject.lafColor = [[LFColorModelV2 alloc] init];
    lafTempObject.lafColor.lfcID = lafTempObject.lafColorId;
    lafTempObject.lafColor = [[LostandFoundManagerV2 sharedLostandFoundManagerV2] loadColorData:lafTempObject.lafColor];
    
    lafTempObject.lafLaFDetailId = lafDetailObject.lafDetailId;
    lafTempObject.laf_Status = isNeedCreate;
    
    [lafDetailObject.lafArray addObject:lafTempObject];
    
    //set to check save data
    lafDetailObject.lafDetail_Status = isUpdate;
    
    NSInteger tempIndex = [lafDetailObject.lafArray count];
    [lostfoundTableView beginUpdates];
    [lostfoundTableView insertSections:[NSIndexSet indexSetWithIndex:tempIndex - 1] withRowAnimation:UITableViewRowAnimationNone];
    [lostfoundTableView endUpdates];
    
    [self setHiddenForAddButton];
    
    [lostfoundTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:tempIndex - 1] atScrollPosition:UITableViewScrollPositionTop animated:NO];
    
    UITableViewCell *remarkCell = [lostfoundTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:tempIndex]];
    [remarkCell setSelected:NO animated:NO];
}

#pragma mark - Photoviewv2delegate
-(void)processPhoto:(UIImage *)photo {
    if (!lafDetailObject.lafPhoto) {
        self.lafDetailObject.lafPhoto = [[NSMutableArray alloc] init];
    }
    
    //create the  new Image object
    LFImageModelV2 *lfImageObject = [[LFImageModelV2 alloc] init];
    photo = [[LostandFoundManagerV2 sharedLostandFoundManagerV2] resizeImage:photo maxSize:1024.0f];
    lfImageObject.lafImgImage = UIImagePNGRepresentation(photo);
    lfImageObject.lafStatus = isNeedCreateI;
    [lafDetailObject.lafPhoto addObject:lfImageObject];
    isUpdatePhoto = YES;
    
    lafDetailObject.lafDetail_Status = isUpdate;
    
    [lostfoundTableView reloadData];
}

-(void) showRemarkView
{
    if (remarkView == nil) {
        remarkView = [[RemarkViewV2 alloc] initWithNibName:@"RemarkViewV2" bundle:nil];
    }
    UIView *view = remarkView.view;
    [remarkView setDelegate:self];
    [remarkView setTextinRemark:lafDetailObject.lafDetailRemark != nil ? lafDetailObject.lafDetailRemark : @""];
    [remarkView viewWillAppear:YES];
    remarkView.isAddSubview = YES;
    //[self.tabBarController.view addSubview:remarkView.view];
    [self.tabBarController addSubview:view];
}

#pragma mark - Remark Delegate
-(void) remarkViewV2DoneWithText:(NSString *)text andController:(UIViewController *)controllerView {
    //show wifi view for fix no wifi view
    if(!isDemoMode){
        [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationShowWifiView] object:nil];
    }
    
    lafDetailObject.lafDetailRemark = text;
    isUpdatedText = TRUE;
    if(remarkView) {
        [remarkView setDelegate:nil];
    }
    [lostfoundTableView reloadData];
}

#pragma mark - UIAlertView Delegate Methods
-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    switch (alertView.tag) {
        case tagDeletePhoto: {
            switch (buttonIndex) {
                case 0: {
                    //YES:
                    if ([lafDetailObject.lafPhoto count] > indexDelete) {
                        LFImageModelV2 *imageDelete = [lafDetailObject.lafPhoto objectAtIndex:indexDelete];
                        if (imageDelete.lafStatus == inDataBaseI) {
                            imageDelete.lafStatus = isNeedDeleteI;
                            
                            if (!self.photoDeleteList) {
                                self.photoDeleteList = [[NSMutableArray alloc] init];
                            }
                            
                            [self.photoDeleteList addObject:imageDelete];
                        }
                        
                        [lafDetailObject.lafPhoto removeObjectAtIndex:indexDelete];
                        isUpdatePhoto = YES;
                        [lostfoundTableView reloadData];
                    }
                }
                    break;
                    
                case 1: {
                    //NO
                }
                    
                default:
                    break;
            }
        }
            break;
            
        case tagAlertSubmit:
        {
            if(buttonIndex == 0)
            {
                //-------Log---------
                if([LogFileManager isLogConsole])
                {
                    NSInteger userId = [[UserManagerV2 sharedUserManager] currentUser].userId;
                    NSString *date = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
                    NSInteger roomId = [locationTextField.text integerValue];
                    
                    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:sectionRemark];
                    UITableViewCell *cell = [lostfoundTableView cellForRowAtIndexPath:indexPath];
                    UITextView *tvRemark = (UITextView *)[cell viewWithTag:remarkTag];
                    NSString *reMark = [NSString stringWithFormat:@"%@", tvRemark.text];
                    
                    for (LostandFoundModelV2 *lafModel in lafDetailObject.lafArray) {
                        if(lafModel.lafItemId > 0)
                        {
                            NSString *itemName = lafModel.lafItem.lfitName;
                            int itemQty = (int)lafModel.laf_item_quantity;
                            NSInteger colorId = lafModel.lafColor.lfcID;
                            
                            NSLog(@"Press button submit to post <%@><L&F><UserId:%d><Roomid:%d><Remark:%@><%@ (%d) x %d><ColorId:%d>",date,(int)userId,(int)roomId,reMark,itemName, (int)lafModel.lafItemId, itemQty, (int)colorId);
                        }
                    }
                }
                //-------Log---------
                [self saveData];
                if(!isFromMainPosting)
                {
                    NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
                    for (UIViewController *aViewController in allViewControllers) {
                        if ([aViewController isKindOfClass:[RoomAssignmentInfoViewController class]]) {
                            [self.navigationController popToViewController:aViewController animated:YES];
                        }
                    }
                }
            }
            else {
                //----------Log---------------
                if([LogFileManager isLogConsole])
                {
                    NSInteger userId = [[UserManagerV2 sharedUserManager] currentUser].userId;
                    NSString *date = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
                    NSInteger roomId = [locationTextField.text integerValue];
                    
                    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:sectionRemark];
                    UITableViewCell *cell = [lostfoundTableView cellForRowAtIndexPath:indexPath];
                    UITextView *tvRemark = (UITextView *)[cell viewWithTag:remarkTag];
                    NSString *reMark = [NSString stringWithFormat:@"%@", tvRemark.text];
                    int currentCountRow = ([lafDetailObject.lafArray count]>maxRowOfTableItem) ? countRow : (countRow - 1);
                    if (currentCountRow == 0) {
                        NSLog(@"Not confirm to submit <%@><L&F><UserId:%d><Input RoomId:%d><Remark:%@>", date, (int)userId, (int)roomId, reMark);
                    }
                    for(int i = 0;i< currentCountRow; i++){
                        for (LostandFoundModelV2 *lafModel in lafDetailObject.lafArray) {
                            if(lafModel.lafItemId > 0)
                            {
                                NSString *itemName = lafModel.lafItem.lfitName;
                                int itemQty = (int)lafModel.laf_item_quantity;
                                NSInteger colorId = lafModel.lafColor.lfcID;
                                NSLog(@"not confirm to submit <%@><L&F><UserId:%d><RoomId:%d><Remark:%@><%@ (%d) x %d><ColorId:%d>",date,(int)userId,(int)roomId,reMark,itemName, (int)lafModel.lafItemId, itemQty, (int)colorId);
                            }
                            
                        }
                    }
                }
                //------------Log---------------
            }
            break;
        }
            
        case tagDeleteCategory: {
            switch (buttonIndex) {
                case 0: {
                    //YES
                    LostandFoundModelV2 *lafModel = [lafDetailObject.lafArray objectAtIndex:indexDelete] ;
                    if (lafModel.laf_Status == inDataBase || lafModel.laf_Status == isNeedUpdate) {
                        lafModel.laf_Status = isNeedDelete;
                        
                        if (!self.lafDeleteList) {
                            self.lafDeleteList = [[NSMutableArray alloc] init];
                        }
                        
                        [self.lafDeleteList addObject:lafModel];
                    }
                    
                    [lafDetailObject.lafArray removeObjectAtIndex:indexDelete];
                    [self setHiddenForAddButton];
                    [lostfoundTableView reloadData];
                    
                }
                    break;
                    
                case 1: {
                    //NO
                }
                    
                default:
                    break;
            }
        }
            break;
            
        case tagSaveLeaving: {
            switch (buttonIndex) {
                case 0: {
                    //YES:
                    if(isFromMainPosting)
                    {
                        NSString *roomNo = [locationTextField text];
                        if(roomNo.length > 0 )
                        {
                            RoomAssignmentModelV2 *roomAssignment = [[RoomAssignmentModelV2 alloc] init];
                            roomAssignment.roomAssignment_RoomId = roomNo;
                            roomAssignment.roomAssignment_UserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
                            BOOL result = [[RoomManagerV2 sharedRoomManager] getRoomAssignmentByUserIDAndRoomID:roomAssignment];
                            if(result)
                            {
                                if([LogFileManager isLogConsole])
                                {
                                    NSInteger userId = [[UserManagerV2 sharedUserManager] currentUser].userId;
                                    NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
                                    NSLog(@"<%@><L&F><UserId:%d><User click back button and choose save data>",time, (int)userId);
                                }
                                [self saveData];
                            }
                            else
                            {
                                NSString *message = nil;
                                if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                                    message = [NSString stringWithFormat:@"%@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOM_INVALID]];
                                } else {
                                    message = [NSString stringWithFormat:@"\n\n\n\n\n%@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOM_INVALID]];
                                }
                                
                                UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObject:[UIImage imageNamed:@"yes_bt.png"]] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:message delegate:nil cancelButtonTitle:[L_CLOSE currentKeyToLanguage] otherButtonTitles: nil];
                                
                                if(!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                                    UIImageView *alertImage = [[UIImageView alloc] initWithFrame:CGRectMake(80, 35, 120, 120)];
                                    [alertImage setImage:[UIImage imageNamed:@"icon_wrong room.png"]];
                                    [alert addSubview:alertImage];
                                }
                                [alert show];
                            }
                        }
                        else
                        {
                            if([LogFileManager isLogConsole])
                            {
                                NSInteger userId = [[UserManagerV2 sharedUserManager] currentUser].userId;
                                NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
                                NSLog(@"<%@><L&F><UserId:%d><User click back button and choose save input data with a wrong room number>", time, (int)userId);
                            }
                            NSString *message = nil;
                            if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                                message = [NSString stringWithFormat:@"%@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOM_INVALID]];
                            } else {
                                message = [NSString stringWithFormat:@"\n\n\n\n\n%@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOM_INVALID]];
                            }
                            
                            UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObject:[UIImage imageNamed:@"yes_bt.png"]] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:message delegate:nil cancelButtonTitle:[L_CLOSE currentKeyToLanguage] otherButtonTitles: nil];
                            
                            if(!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                                UIImageView *alertImage = [[UIImageView alloc] initWithFrame:CGRectMake(80, 35, 120, 120)];
                                [alertImage setImage:[UIImage imageNamed:@"icon_wrong room.png"]];
                                [alert addSubview:alertImage];
                            }
                            [alert show];
                        }
                    }
                    if (isTapBackButton == YES) {
                        isTapBackButton = NO;
                        //                        [self.navigationController popViewControllerAnimated:YES];
                        [self performSelector:@selector(popViewControllerAfterDelay) withObject:nil afterDelay:0.5];
                    } else {
                        NSInteger tagEvent = [[NSUserDefaults standardUserDefaults] integerForKey:TAG_OF_EVENT];
                        
                        if (tagEvent == tagOfMessageButton) {
                            [self performSelector:@selector(gotoMessageAfterDelay) withObject:nil afterDelay:0.5];
                            
                            //                            [[CommonVariable sharedCommonVariable] setRoomIsCompleted:0];
                            //                            [[HomeViewV2 shareHomeView] handleTabbarEventWithTagOfButton:tagEvent];
                        }
                    }
                }
                    break;
                    
                case 1: {
                    //NO
                    if([LogFileManager isLogConsole])
                    {
                        NSInteger userId = [[UserManagerV2 sharedUserManager] currentUser].userId;
                        NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
                        NSLog(@"<%@><L&F><UserId:%d><User click back button when doesn't press submit yet and choose 'NO' save data>", time, (int)userId);
                    }
                    UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ALERT_DISCARDSAVECOUNTS] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles:[[LanguageManagerV2 sharedLanguageManager] getNo], nil];
                    alert.tag = tagDiscard;
                    [alert show];
                }
                    
                default:
                    break;
            }
        }
            break;
            
        case tagDiscard: {
            //            NSInteger userId = [[UserManagerV2 sharedUserManager] currentUser].userId;
            //            NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
            //            NSLog(@"<%@><UserId:%d><User doesn't discard input>", time, userId);
            switch (buttonIndex) {
                    
                case 0: {
                    //YES:
                    
                    if (isTapBackButton == YES) {
                        isTapBackButton = NO;
                        //                        [self.navigationController popViewControllerAnimated:YES];
                        [self performSelector:@selector(popViewControllerAfterDelay) withObject:nil afterDelay:0.5];
                    }
                    else {
                        
                        
                        NSInteger tagEvent = [[NSUserDefaults standardUserDefaults] integerForKey:TAG_OF_EVENT];
                        
                        if (tagEvent == tagOfMessageButton) {
                            
                            [self performSelector:@selector(gotoMessageAfterDelay) withObject:nil afterDelay:0.5];
                            
                            //                            [[CommonVariable sharedCommonVariable] setRoomIsCompleted:0];
                            //                            [[HomeViewV2 shareHomeView] handleTabbarEventWithTagOfButton:tagEvent];
                        } else {
                            [self reloadLaFData];
                        }
                        
                    }
                    
                }
                    break;
                    
                case 1: {
                    //NO
                    
                }
                    
                default:
                    break;
            }
        }
            break;
            
        case tagSaveSuccessful: {
            switch (buttonIndex) {
                case 0: {
                    //YES:
                    
                }
                    break;
                    
                case 1: {
                    //NO
                }
                    
                default:
                    break;
            }
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - LFCategoryViewV2Delegate
-(void)callbackLostandFoundObject: (LostandFoundModelV2 *)lafObject atPosition:(NSInteger)selectedPosition {
    //set to check save data
    lafDetailObject.lafDetail_Status = isUpdate;
    
    //    [lafDetailObject.lafArray replaceObjectAtIndex:selectedPosition withObject:lafObject];
    
    //    for(LostandFoundModelV2 *a in lafList)
    //    {
    //        NSLog(@"Before : qty = %d", a.laf_item_quantity);
    //    }
    
    LostandFoundModelV2 *lf = [[LostandFoundModelV2 alloc] init];
    lf.lafId = lafObject.lafId;
    
    lf.lafCategoryId = lafObject.lafCategoryId;
    lf.lafItemId = lafObject.lafItemId;
    lf.lafColorId = lafObject.lafColorId;
    lf.lafLaFDetailId = lafObject.lafLaFDetailId;
    lf.laf_item_quantity = lafObject.laf_item_quantity;
    lf.laf_Status = lafObject.laf_Status;
    lf.lafCat = lafObject.lafCat;
    lf.lafItem = lafObject.lafItem,
    lf.lafColor = lafObject.lafColor;
    
    if(indexTemp >= [lafList count])
    {
        [lafList addObject:lf];
    }
    else
    {
        [lafList removeObjectAtIndex:indexTemp];
        [lafList insertObject:lf atIndex:indexTemp];
    }
    
    //    for(LostandFoundModelV2 *a in lafList)
    //    {
    //        NSLog(@"After : qty = %d", a.laf_item_quantity);
    //    }
    
//    if(indexTemp >= [lafDetailObject.lafArray count])
//    {
//        //        [lafDetailObject.lafArray addObject:lafObject];
//        [lafDetailObject.lafArray insertObject:lf atIndex:indexTemp];
//    }
    if(indexTemp == [lafDetailObject.lafArray count] - 1)
    {
        [lafDetailObject.lafArray removeObjectAtIndex:indexTemp];
        [lafDetailObject.lafArray insertObject:lf atIndex:indexTemp];
        
        LostandFoundModelV2 *lafTempObject = [[LostandFoundModelV2 alloc] init];
        lafTempObject.lafCategoryId = 0;
        lafTempObject.lafColorId = COLOR_BLACK_LAF_ID;
        lafTempObject.lafItemId = 0;
        
        lafTempObject.lafCat = [[LFCategoryModelV2 alloc] init];
        lafTempObject.lafCat.lfcID = lafTempObject.lafCategoryId;
        [[LostandFoundManagerV2 sharedLostandFoundManagerV2] loadCategoryData:lafTempObject.lafCat];
        
        lafTempObject.lafItem = [[LFItemModelV2 alloc] init];
        lafTempObject.lafItem.lfitID = lafTempObject.lafItemId;
        [[LostandFoundManagerV2 sharedLostandFoundManagerV2] loadItemData:lafTempObject.lafItem];
        
        lafTempObject.lafColor = [[LFColorModelV2 alloc] init];
        lafTempObject.lafColor.lfcID = lafTempObject.lafColorId;
        [[LostandFoundManagerV2 sharedLostandFoundManagerV2] loadColorData:lafTempObject.lafColor];
        
        lafTempObject.lafLaFDetailId = lafDetailObject.lafDetailId;
        lafTempObject.laf_Status = isNeedCreate;
        [lafDetailObject.lafArray addObject:lafTempObject];
        
        lafDetailObject.lafDetail_Status = isCreate;
    }
    
    
    //    NSLog(@"selectedPosition = %d, countRow = %d, indexTemp = %d", selectedPosition, countRow, indexTemp);
    if(countRow < maxRowOfTableItem && indexTemp == countRow - 1)
        countRow++;
    [lostfoundTableView reloadData];
    
}

-(BOOL) isOnlyViewedAccessRight
{
    //Hao Tran[20130517/Check Access Right] - Check access right only view
    if(isFromMainPosting)
    {
        if (postingLostAndFound.isAllowedView)
        {
            return YES;
        }
    }
    else if(actionLostAndFound.isAllowedView)
    {
        return YES;
    }
    //Hao Tran[20130517/Check Access Right] - END
    
    return NO;
}

-(BOOL)isValidQty{
    if([self isOnlyViewedAccessRight]) {
        return NO;
    }
    
    //    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:sectionTableItems];
    //    UITableViewCell *cell = [lostfoundTableView cellForRowAtIndexPath:indexPath];
    if (countRow > 1) {
        /*
        if ([locationTextField.text isEqualToString:@""]) {
            return NO;
        }*/
        return YES;
         
    }
    return NO;
}

#pragma mark - LFItemViewV2Delegate
-(void)forwardhoseItem:(LostandFoundModelV2 *)lafData atselectedSection:(NSInteger)sectionIndex  {
    //set to check save data
    lafDetailObject.lafDetail_Status = isUpdate;
    
    [lafDetailObject.lafArray replaceObjectAtIndex:sectionIndex withObject:lafData];
    
    [lostfoundTableView reloadData];
}

#pragma mark - LFColorViewV2Delegate
-(void)transferTheValueOfTableView:(LFColorModelV2 *)lafData atPosition: (NSInteger) sectionIndex {
    //set to check save data
    lafDetailObject.lafDetail_Status = isUpdate;
    
    LostandFoundModelV2 *lf = (LostandFoundModelV2*)[lafDetailObject.lafArray objectAtIndex:indexTemp];
    lf.lafColor = lafData;
    
    //no need to replace object because it is same reference object
    //    [lafDetailObject.lafArray replaceObjectAtIndex:sectionIndex withObject:lafData];
    
    [lostfoundTableView reloadData];
}

#pragma mark - === Handle navigation view controller ===
#pragma mark
-(void)popViewControllerAfterDelay {
    NSInteger userId = [[UserManagerV2 sharedUserManager] currentUser].userId;
    NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
    if([LogFileManager isLogConsole])
    {
        NSLog(@"<%@><L&F><UserId:%d><User choose discard all input>", time, (int)userId);
    }
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)gotoMessageAfterDelay {
    [[HomeViewV2 shareHomeView] handleTabbarEventWithTagOfButton:[[NSUserDefaults standardUserDefaults] integerForKey:TAG_OF_EVENT]];
}

-(void)btnColor_Clicked:(id)sender
{
    if([LogFileManager isLogConsole])
    {
        NSInteger userId = [[UserManagerV2 sharedUserManager] currentUser].userId;
        NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
        int currentCountRow = ([lafDetailObject.lafArray count]>maxRowOfTableItem) ? countRow : (countRow - 1);
        if (currentCountRow == 0) {
            NSLog(@"<%@><UserId:%d><Click button color:index# %d>", time, (int)userId, currentCountRow);
        }
        else
            NSLog(@"<%@><UserId:%d><Click button color:index# %d>", time, (int)userId, currentCountRow - 1);
    }
    LFColorViewV2 *vColor = [[LFColorViewV2 alloc]initWithNibName:@"LFColorViewV2" bundle:Nil ];
    //    vColor.existedLaFObject = [lafDetailObject.lafArray objectAtIndex:indexPath.section];
    //    vColor.selectedSection = indexPath.section;
    vColor.delegate = self;
    indexTemp = ((ButtonColor*)sender).indexInTableRow;
    /*
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK] style:UIBarButtonItemStyleBordered target:nil action:nil];
    self.navigationItem.backBarButtonItem = backButton;
     */
    [vColor setHidesBottomBarWhenPushed:YES];
    [vColor setTitle:[[LanguageManagerV2 sharedLanguageManager] getLOSTANDFOUND]];
    [self.navigationController pushViewController:vColor animated:NO];
}

- (NSMutableString *) aggregateData {
    
    NSMutableString *confirmData = [NSMutableString string];
    for (LostandFoundModelV2 *lafModel in lafDetailObject.lafArray) {
        if(lafModel.lafItemId > 0) {
            if ([[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString:[NSString stringWithFormat:@"%@",ENGLISH_LANGUAGE]]){
                [confirmData appendString:[NSString stringWithFormat:@"\n%d - %@", (int)lafModel.laf_item_quantity, lafModel.lafItem.lfitName]];
            } else {
                [confirmData appendString:[NSString stringWithFormat:@"\n%d - %@", (int)lafModel.laf_item_quantity, lafModel.lafItem.lfitLang]];
            }
        }
    }
    return confirmData;
}

// DungPhan: CRF-00001277 > [Marriott] Confirmation Message before Submit Posting on Minibar / Engineering / Amenities / Lost & Found
- (void)alertView:(DTAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 0) {
        // YES
        //-------Log---------
        if([LogFileManager isLogConsole]) {
            NSInteger userId = [[UserManagerV2 sharedUserManager] currentUser].userId;
            NSString *date = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSInteger roomId = [locationTextField.text integerValue];
            
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:sectionRemark];
            UITableViewCell *cell = [lostfoundTableView cellForRowAtIndexPath:indexPath];
            UITextView *tvRemark = (UITextView *)[cell viewWithTag:remarkTag];
            NSString *reMark = [NSString stringWithFormat:@"%@", tvRemark.text];
            
            for (LostandFoundModelV2 *lafModel in lafDetailObject.lafArray) {
                if(lafModel.lafItemId > 0)
                {
                    NSString *itemName = lafModel.lafItem.lfitName;
                    int itemQty = (int)lafModel.laf_item_quantity;
                    NSInteger colorId = lafModel.lafColor.lfcID;
                    
                    NSLog(@"Press button submit to post <%@><L&F><UserId:%d><Roomid:%d><Remark:%@><%@ (%d) x %d><ColorId:%d>",date,(int)userId,(int)roomId,reMark,itemName, (int)lafModel.lafItemId, itemQty, (int)colorId);
                }
            }
        }
        //-------Log---------
        [self saveData];
        if(!isFromMainPosting) {
            NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
            for (UIViewController *aViewController in allViewControllers) {
                if ([aViewController isKindOfClass:[RoomAssignmentInfoViewController class]]) {
                    [self.navigationController popToViewController:aViewController animated:YES];
                }
            }
        }
        
        
    } else {
        // NO
        //----------Log---------------
        if([LogFileManager isLogConsole])
        {
            NSInteger userId = [[UserManagerV2 sharedUserManager] currentUser].userId;
            NSString *date = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSInteger roomId = [locationTextField.text integerValue];
            
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:sectionRemark];
            UITableViewCell *cell = [lostfoundTableView cellForRowAtIndexPath:indexPath];
            UITextView *tvRemark = (UITextView *)[cell viewWithTag:remarkTag];
            NSString *reMark = [NSString stringWithFormat:@"%@", tvRemark.text];
            int currentCountRow = ([lafDetailObject.lafArray count]>maxRowOfTableItem) ? countRow : (countRow - 1);
            if (currentCountRow == 0) {
                NSLog(@"Not confirm to submit <%@><L&F><UserId:%d><Input RoomId:%d><Remark:%@>", date, (int)userId, (int)roomId, reMark);
            }
            for(int i = 0;i< currentCountRow; i++){
                for (LostandFoundModelV2 *lafModel in lafDetailObject.lafArray) {
                    if(lafModel.lafItemId > 0)
                    {
                        NSString *itemName = lafModel.lafItem.lfitName;
                        int itemQty = (int)lafModel.laf_item_quantity;
                        NSInteger colorId = lafModel.lafColor.lfcID;
                        NSLog(@"not confirm to submit <%@><L&F><UserId:%d><RoomId:%d><Remark:%@><%@ (%d) x %d><ColorId:%d>",date,(int)userId,(int)roomId,reMark,itemName, (int)lafModel.lafItemId, itemQty, (int)colorId);
                    }
                    
                }
            }
        }
        //------------Log---------------
    }
}
-(IBAction)btnPostingHistory_Clicked:(id)sender{
    HistoryPosting *historyPosting = [[HistoryPosting alloc] initWithNibName:@"HistoryPosting" bundle:nil];
    //    historyPosting.isPushFrom = IS_PUSHED_FROM_PHYSICAL_CHECK;
    historyPosting.roomID = locationTextField.text;
    historyPosting.isFromPostingFunction = isFromMainPosting;
    historyPosting.selectedModule = ModuleHistory_LostAndFound;
    [self.navigationController pushViewController:historyPosting animated:YES];
    
}
-(IBAction)btnSubmit_Clicked:(id)sender
{
    NSString *roomNo = [locationTextField text];
    if(roomNo.length > 0 && [self isValidQty]) {
        RoomAssignmentModelV2 *roomAssignment = [[RoomAssignmentModelV2 alloc] init];
        roomAssignment.roomAssignment_RoomId = roomNo;
        roomAssignment.roomAssignment_UserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
        BOOL result = [[RoomManagerV2 sharedRoomManager] getRoomAssignmentByUserIDAndRoomID:roomAssignment];
        if([[UserManagerV2 sharedUserManager].currentUserAccessRight isValidateRoom] && !isDemoMode) {
            NSString *hotelId = [NSString stringWithFormat:@"%d", [[[UserManagerV2 sharedUserManager] currentUser] userHotelsId]];
            result = [[RoomManagerV2 sharedRoomManager] isValidRoomWSByUserId:[UserManagerV2 sharedUserManager].currentUser.userId roomNumber:roomNo hotelId:hotelId];
        } else {
            result = TRUE; //Force posting mini bar without checking validation room
        }
        if(result)
        {
            //            [RoomManagerV2 setCurrentRoomNo:[num integerValue]];
            //            [RoomManagerV2 setCurrentRoomNo:[locationTextField.text integerValue]];//modify
            //NSString *mess = [NSString stringWithFormat:@"Room %@ \n Submit L&F Items?", roomNo];
            NSString *mess = [NSString stringWithFormat:@"%@ %@ \n %@%@ \n %@", [L_room currentKeyToLanguage],roomNo,[L_submit_lost_found_items currentKeyToLanguage],@"?", [self aggregateData]];
            
            DTAlertView *alertView = [DTAlertView alertViewWithTitle:[L_confirm_title currentKeyToLanguage] message:mess delegate:self cancelButtonTitle:[L_YES currentKeyToLanguage] positiveButtonTitle:[L_NO currentKeyToLanguage]];
            [alertView show];
        }
        else
        {
            NSString *message = nil;
            if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                message = [NSString stringWithFormat:@"%@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOM_INVALID]];
            } else {
                message = [NSString stringWithFormat:@"\n\n\n\n\n%@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOM_INVALID]];
            }
            
            UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObject:[UIImage imageNamed:@"yes_bt.png"]] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:message delegate:nil cancelButtonTitle:[L_CLOSE currentKeyToLanguage] otherButtonTitles: nil];
            
            if(!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                UIImageView *alertImage = [[UIImageView alloc] initWithFrame:CGRectMake(80, 35, 120, 120)];
                [alertImage setImage:[UIImage imageNamed:@"icon_wrong room.png"]];
                [alert addSubview:alertImage];
            }
            [alert show];
            if([LogFileManager isLogConsole])
            {
                NSInteger userId = [[UserManagerV2 sharedUserManager] currentUser].userId;
                NSString *date = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
                NSInteger roomId = [locationTextField.text integerValue];
                NSLog(@"<%@><UserId:%d><Input RoomID:%d><Press button submit when enter invalid room number>", date, (int)userId, (int)roomId);
            }
        }
    } else {
        if (roomNo.length > 0) {
            NSString *message = nil;
            if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                message = [NSString stringWithFormat:@"%@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_INVALID_DATA]];
            } else {
                message = [NSString stringWithFormat:@"\n\n\n\n\n%@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_INVALID_DATA]];
            }
            
            UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObject:[UIImage imageNamed:@"yes_bt.png"]] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:message delegate:nil cancelButtonTitle:[L_CLOSE currentKeyToLanguage] otherButtonTitles: nil];
            
            if(!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                UIImageView *alertImage = [[UIImageView alloc] initWithFrame:CGRectMake(80, 35, 120, 120)];
                [alertImage setImage:[UIImage imageNamed:@"icon_wrong room.png"]];
                [alert addSubview:alertImage];
            }
            [alert show];
        } else {
            NSString *message = nil;
            if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                message = [NSString stringWithFormat:@"%@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOM_INVALID]];
            } else {
                message = [NSString stringWithFormat:@"\n\n\n\n\n%@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOM_INVALID]];
            }
            
            UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObject:[UIImage imageNamed:@"yes_bt.png"]] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:message delegate:nil cancelButtonTitle:[L_CLOSE currentKeyToLanguage] otherButtonTitles: nil];
            
            if(!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                UIImageView *alertImage = [[UIImageView alloc] initWithFrame:CGRectMake(80, 35, 120, 120)];
                [alertImage setImage:[UIImage imageNamed:@"icon_wrong room.png"]];
                [alert addSubview:alertImage];
            }
            [alert show];
            if([LogFileManager isLogConsole])
            {
                NSInteger userId = [[UserManagerV2 sharedUserManager] currentUser].userId;
                NSString *date = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
                NSLog(@"<%@><UserId:%d><Press button submit when the text field is empty>", date, (int)userId);
            }
        }
    }
}

/**
 * Name : shouldChangeCharactersInRange
 * Description : For hiding the keyboard. This must be implemented if you are working with UITextField
 * Parameter :
 * Return : BOOL
 */
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if([string isEqualToString:@"\n"])
    {
        [textField resignFirstResponder];
        return NO;
    }
    return YES;
}


/**
 * Name : textFieldShouldReturn
 * Description : Hiding the keyboard of UITextField
 * Parameter :
 * Return : BOOL
 */
- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    
    //get remark
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:sectionRemark];
    UITableViewCell *cell = [lostfoundTableView cellForRowAtIndexPath:indexPath];
    UITextView *tvRemark = (UITextView *)[cell viewWithTag:remarkTag];
    
    if([LogFileManager isLogConsole])
    {
        NSInteger userId = [[UserManagerV2 sharedUserManager] currentUser].userId;
        NSString *date = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSString *reMark = [NSString stringWithFormat:@"%@", tvRemark.text];
        NSLog(@"<%@><L&F><UserId:%d><Input RoomId:%@><Remark:%@>",date, (int)userId, textField.text, reMark);
    }
    
    NSIndexPath *indexPathSubmit = [NSIndexPath indexPathForRow:0 inSection:sectionSubmitButton];
    UITableViewCell *cellSubmit = [lostfoundTableView cellForRowAtIndexPath:indexPathSubmit];
    btnSubmit = (UIButton *)[cellSubmit viewWithTag:tagOfSubmitButton];
    //[btnSubmit setEnabled:[self isValidQty]];
    [textField resignFirstResponder];
    [self setEnableBtnAtionHistory:YES];
    return YES;
}

/**
 * Name : shouldChangeCharactersInRange
 * Description : For hiding the keyboard. This must be implemented if you are working with UITextField
 * Parameter :
 * Return : BOOL
 */
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)string
{
    
    if([string isEqualToString:@"\n"])
    {
        if([LogFileManager isLogConsole])
        {
            NSInteger userId = [[UserManagerV2 sharedUserManager] currentUser].userId;
            NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
            //        NSString *currentRoom = [locationTextField.text]
            NSLog(@"<%@><L&F><UserId:%d><RoomId:%@><Input Remark:%@>", time, (int)userId, locationTextField.text, textView.text);
        }
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}

/**
 * Name : textFieldShouldReturn
 * Description : Hiding the keyboard of UITextField
 * Parameter :
 * Return : BOOL
 */
- (BOOL) textViewShouldReturn:(UITextView *)textView
{
    
    [textView resignFirstResponder];
    return YES;
}


#pragma mark - Access Right
-(void) loadAccessRights
{
    AccessRightModel *currentUserAccessRight = [[UserManagerV2 sharedUserManager] currentUserAccessRight];
    QRCodeScanner = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.QRCodeScanner];
    postingLostAndFound = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.postLostAndFound];
    actionLostAndFound = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.actionLostAndFound];
}
#pragma mark - QR Code Button Touched
-(IBAction)btnQRCodeScannerTouched:(id) sender
{
    ZXingWidgetController *widController = [[ZXingWidgetController alloc] initWithDelegate:self showCancel:YES OneDMode:NO showLicense:NO];
    QRCodeReader* qrcodeReader = [[QRCodeReader alloc] init];
    NSSet *readers = [[NSSet alloc ] initWithObjects:qrcodeReader,nil];
    widController.readers = readers;
    [self presentViewController:widController animated:YES completion:nil];
}

#pragma mark - ZxingDelegate
- (void)zxingController:(ZXingWidgetController*)controller didScanResult:(NSString *)resultString {
    resultString = [STEncryptorDES convertText:resultString];
    [self dismissViewControllerAnimated:YES completion:nil];
    
    
    locationTextField.text = resultString;
    [self textFieldShouldReturn:locationTextField];
}

- (void)zxingControllerDidCancel:(ZXingWidgetController*)controller {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - iOS 7 Resources
-(void)loadFlatResource
{
    [imgHeader setImage:[UIImage imageNamed:imgGuestInfoActiveFlat]];
    [btnSubmit setImage:[UIImage imageNamed:imgBtnSubmitFlat] forState:UIControlStateNormal];
    [btnSubmit setImage:[UIImage imageNamed:imgBtnSubmitClickedFlat] forState:UIControlStateHighlighted];
    if (btnPostingHistory){
        [btnPostingHistory setBackgroundImage:[UIImage imageNamed:imgHistoryPostingBtnFlat] forState:UIControlStateHighlighted];
        [btnPostingHistory setBackgroundImage:[UIImage imageNamed:imgHistoryPostingBtnFlat] forState:UIControlStateNormal];
        [btnPostingHistory setTitle:[L_action_history currentKeyToLanguage] forState:UIControlStateNormal];
        [self setEnableBtnAtionHistory:NO];
    }
}
-(void) setEnableBtnAtionHistory:(BOOL) isEnable
{
    if(![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]){
        isEnable = NO;
    }
    [btnPostingHistory setUserInteractionEnabled:isEnable];
    [btnPostingHistory setBackgroundImage:[UIImage imageBeforeiOS7:(isEnable?imgCleaningStatusBtnAndActionBtn:imgCleaningStatusBtnAndActionBtnDisable)
                                                         equaliOS7:(isEnable?imgCleaningStatusBtnAndActionBtnFlat:imgCleaningStatusBtnAndActionBtnDisableFlat)]
                                 forState:UIControlStateNormal];
    [btnPostingHistory setBackgroundImage:[UIImage imageBeforeiOS7:(isEnable?imgCleaningStatusBtnAndActionBtnClicked:imgCleaningStatusBtnAndActionBtnDisable)
                                                         equaliOS7:(isEnable?imgCleaningStatusBtnAndActionBtnClickedFlat:imgCleaningStatusBtnAndActionBtnDisableFlat)]
                                 forState:UIControlEventTouchDown];
    [btnPostingHistory setTitle:[L_action_history currentKeyToLanguage] forState:UIControlStateNormal];
}

@end
