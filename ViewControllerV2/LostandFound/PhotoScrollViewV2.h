//
//  PhotoScrollViewV2.h
//  mHouseKeeping
//
//  Created by TMS on 2/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotoScrollViewV2 : UIScrollView {    
    NSMutableArray *photoArray;
}

@property (nonatomic, strong) NSMutableArray *photoArray;

-(void)displayPhotoArray;

@end
