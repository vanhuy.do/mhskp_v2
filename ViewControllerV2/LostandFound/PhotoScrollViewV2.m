//
//  selfV2.m
//  mHouseKeeping
//
//  Created by TMS on 2/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "PhotoScrollViewV2.h"
#import "PhotoViewV2.h"

#define HEIGHT 50
#define WIDTH  80
#define WIDTHIMAGE 80
#define SIZEDELETE 10
#define DELETEIMAGE @"icon_garbage.png"
#define PHOTOIMAGE @"photo_icon.png"
#define TESTIMAGE @"Shampoo_pic.png"

@implementation PhotoScrollViewV2
@synthesize photoArray;
//@synthesize delegate;

//- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
//{
//    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
//    if (self) {
//        // Custom initialization
//    }
//    return self;
//}
//
//- (void)didReceiveMemoryWarning
//{
//    // Releases the view if it doesn't have a superview.
//    [super didReceiveMemoryWarning];
//    
//    // Release any cached data, images, etc that aren't in use.
//}

#pragma mark - View lifecycle
- (void)viewDidLoad {
//    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
   // photoArray=[[NSMutableArray alloc] initWithObjects:[UIImage imageNamed:TESTIMAGE], [UIImage imageNamed:TESTIMAGE],[UIImage imageNamed:TESTIMAGE],[UIImage imageNamed:TESTIMAGE],[UIImage imageNamed:TESTIMAGE],nil];
    [self displayPhotoArray];
}

//- (void)viewDidUnload
//{
//    [super viewDidUnload];
//    // Release any retained subviews of the main view.
//    // e.g. self.myOutlet = nil;
//}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark ScrollViewDelegate
-(void)displayPhotoArray {       
    //first display the photo Button, and add more images if any. If the number of images=6 the photo Button is gray.
    CGFloat cxLocation = 0;
    if ([photoArray count] == 0) {
        cxLocation += (self.frame.size.width - 80) / 2;   
        UIImage *photoImage = [UIImage imageNamed:PHOTOIMAGE];
        UIView *photoView = [[UIView alloc] initWithFrame:CGRectMake(
                                        cxLocation, 5, WIDTH, HEIGHT)];
        UIButton *photoButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
        [photoButton setBackgroundImage:photoImage forState:UIControlStateNormal];        
        [photoButton addTarget:self action:@selector(selectPhotoButton) forControlEvents:UIControlEventTouchDown];
    
        [photoView addSubview:photoButton];
        [self addSubview:photoView];
   } else {
       if ([photoArray count] > 0 ) {
           UIImage *photoImage = [UIImage imageNamed:PHOTOIMAGE];
           UIView *photoView = [[UIView alloc] initWithFrame:CGRectMake(cxLocation + 5, 5, WIDTH, HEIGHT)];
           UIButton *photoButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0,WIDTH, HEIGHT)];
           [photoButton setBackgroundImage:photoImage forState:UIControlStateNormal];
           [photoButton addTarget:self action:@selector(selectPhotoButton) forControlEvents:UIControlEventTouchDown];
            
           if ([photoArray count] == 5) {
               //set the color of photo button is gray in here.  
           }
      
           [photoView addSubview:photoButton];
           [self addSubview:photoView];
                   
           cxLocation += WIDTH + 5;

           for (int i = 0; i < [photoArray count]; i++) {
//            UIImage *photoImage=[photoArray objectAtIndex:i];
               UIImage *photoImage = [UIImage imageNamed:PHOTOIMAGE];
               UIView *photoView = [[UIView alloc]initWithFrame:CGRectMake(cxLocation, 5, WIDTH, HEIGHT)];
               UIButton *photoButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0,WIDTHIMAGE, HEIGHT)];
               [photoButton setBackgroundImage:photoImage 
                                      forState:UIControlStateNormal];
            
               [photoView addSubview:photoButton];
               //[photoView addSubview:deleteButton];
               [self addSubview:photoView];
            
               cxLocation += WIDTH + 5;
           }
       }
    }
    
    [self setContentSize:CGSizeMake(cxLocation, HEIGHT)];        
}

@end
