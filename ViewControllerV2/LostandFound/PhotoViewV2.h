//
//  PhotoViewV2.h
//  mHouseKeeping
//
//  Created by TMS on 2/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "LFImageModelV2.h"

@protocol PhotoViewV2Delegate;

@interface PhotoViewV2 : UIViewController<UIImagePickerControllerDelegate, UINavigationControllerDelegate> {
    IBOutlet UIButton *takePhotoButton;
    IBOutlet UIButton *cancelButton;
    IBOutlet UIButton *choosePhotoButton;
    IBOutlet UIButton *photoButton;
    
//    LFImageModelV2 *selectedLFImage;
    UIImage *selectedImage;
    __unsafe_unretained id<PhotoViewV2Delegate> delegate;
}

@property (nonatomic, strong) IBOutlet UIButton *takePhotoButton;
@property (nonatomic, strong) IBOutlet UIButton *cancelButton;
@property (nonatomic, strong) IBOutlet UIButton *choosePhotoButton;
@property (nonatomic, strong) IBOutlet UIButton *photoButton;

//@property (nonatomic, strong) LFImageModelV2 *selectedLFImage;
@property (nonatomic, strong) UIImage *selectedImage;
@property (nonatomic, assign) id<PhotoViewV2Delegate> delegate;

-(IBAction)selectTakePhotoButton:(id)sender;
-(IBAction)selectCancelButton:(id)sender;
-(IBAction)selectChoosePhotoButton:(id)sender;
- (UIImage *)resizedImage:(CGSize)newSize transform:(CGAffineTransform)transform
           drawTransposed:(BOOL)transpose andImage: (UIImage *)imagePara
     interpolationQuality:(CGInterpolationQuality)quality;
- (CGAffineTransform)transformForOrientation:(CGSize)newSize 
                                    andImage:(UIImage *)imageF ;
@end

@protocol PhotoViewV2Delegate<NSObject>

@optional
-(void)processPhoto:(UIImage *)photo;

@end
