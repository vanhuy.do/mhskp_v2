//
//  PhotoViewV2.m
//  mHouseKeeping
//
//  Created by TMS on 2/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "PhotoViewV2.h"
#import "QuartzCore/QuartzCore.h"
#import "SettingModelV2.h"
#import "SettingAdapterV2.h"
#import <MobileCoreServices/UTCoreTypes.h>
#import "MyNavigationBarV2.h"
#import "LogFileManager.h"
#import "DeviceManager.h"
#import "ehkDefines.h"
#import "CustomAlertViewV2.h"

#define radians( degrees ) ( degrees * M_PI / 180 )
#define HEIGHTING_LOW 240
#define WIDTHING_LOW 180
#define HEIGHTING_MEDIUM 320
#define WIDTHING_MEDIUM 240
#define HEIGHTING_HIGH 360
#define WIDTHING_HIGH 480
#define isDemoMode ([[NSUserDefaults standardUserDefaults] integerForKey:ENABLE_DEMO_MODE] == 1)

@interface PhotoViewV2 (PrivateMethods)

- (UIImage*)imageByScalingImage:(UIImage*)sourceImage toSize:(CGSize)targetSize;
- (UIImage*)imageWithImage:(UIImage*)sourceImage scaledToSizeWithSameAspectRatio:(CGSize)targetSize;

@end

@implementation PhotoViewV2
@synthesize takePhotoButton,choosePhotoButton,cancelButton,selectedImage;
//@synthesize selectedLFImage;
@synthesize delegate;
@synthesize photoButton;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    
    return self;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //Hide navigation bar
    [self.navigationController setNavigationBarHidden:YES];
    [[[HomeViewV2 shareHomeView] slidingMenuBar] hideAll];
    //show wifi view for fix no wifi view
    if(!isDemoMode){
        [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationShowWifiView] object:nil];
    }
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES];   
    //back button navigation
    //self.navigationItem.leftBarButtonItem = [MyNavigationBarV2 createBackButtonWithTarget:self selector:@selector(btnBackPressed) forControlEvents:UIControlEventTouchUpInside];
}

-(void)viewWillDisappear:(BOOL)animated
{
    //[self.navigationController setNavigationBarHidden:NO];
    //show wifi view for fix no wifi view
    if(!isDemoMode){
        [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationShowWifiView] object:nil];
    }
    [super viewWillDisappear:animated];
}

-(void)btnBackPressed
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        [self loadFlatResource];
    }
    //[self.view setBackgroundColor:[UIColor colorWithWhite:0.1 alpha:0.5]];
    
    //Resize content views of current VC to fit with Navigation bar and tabbar
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    UIView *parentView = nil;
    
    parentView = self.parentViewController.view;
    
    UIGraphicsBeginImageContext(parentView.bounds.size);
     CALayer *layer = parentView.layer;
    [layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *parentViewImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:self.view.bounds];
    imgView.image = parentViewImage;
    
    [takePhotoButton setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_TAKE_PHOTO]  forState:UIControlStateNormal];
    [choosePhotoButton setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostanfound_choose_photo]  forState:UIControlStateNormal];
    [cancelButton setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_CANCEL]  forState:UIControlStateNormal];
    
    [photoButton setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostanfound_photo_title] forState:UIControlStateNormal];

    [self.view insertSubview:imgView atIndex:0];    
    // Do any additional setup after loading the view from its nib.
    
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma amrk - process buttons
-(IBAction)selectTakePhotoButton:(id)sender {
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        UIImagePickerController *pickerController = [[UIImagePickerController 
                                                  alloc] init];
        pickerController.delegate = self;
        pickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        [pickerController setCameraCaptureMode:UIImagePickerControllerCameraCaptureModePhoto];
        [self presentViewController:pickerController animated:YES completion:nil];
    } 
}

-(IBAction)selectCancelButton:(id)sender {
    if([LogFileManager isLogConsole])
    {
        NSInteger userId = [[UserManagerV2 sharedUserManager] currentUser].userId;
        NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSLog(@"<%@><UserId:%d><Photo screen><click Cancel button>", time, (int)userId);
    }
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationController popViewControllerAnimated:NO];
}

-(IBAction)selectChoosePhotoButton:(id)sender {
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum]) {
        if([LogFileManager isLogConsole])
        {
            NSInteger userId = [[UserManagerV2 sharedUserManager] currentUser].userId;
            NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSLog(@"<%@><UserId:%d><Photo screen><Click choose photo button>", time, (int)userId);
        }
        UIImagePickerController *pickerController = [[UIImagePickerController alloc] init];
        pickerController.delegate = self;
        pickerController.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        [pickerController setMediaTypes: [NSArray arrayWithObject:(NSString *)kUTTypeImage]];
        [pickerController setVideoQuality:UIImagePickerControllerQualityTypeMedium];
        
        [self presentViewController:pickerController animated:YES completion:nil];
      
        
    }
}

#pragma UIImagePickerControllerDelegate
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
    picker.delegate = nil;
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    [self.navigationController setNavigationBarHidden:NO];
    
    SettingModelV2 *smodel = [[SettingModelV2 alloc] init];
    SettingAdapterV2 *sadapter = [[SettingAdapterV2 alloc] init];
    [sadapter openDatabase];
    
    //get current setting
    [sadapter loadSettingModel:smodel];
    [sadapter close];
    
    CGSize size;
//    CGInterpolationQuality quality;
    
//    quality = kCGInterpolationMedium;
//
//    
    switch (smodel.settingsPhoto) {
        case PHOTO_RESOLUTION_240_180: {
            size = CGSizeMake(WIDTHING_LOW, HEIGHTING_LOW);
//            quality = kCGInterpolationLow;
        }
            break;
            
        case PHOTO_RESOLUTION_320_240: {
            size = CGSizeMake(WIDTHING_MEDIUM, HEIGHTING_MEDIUM);
//            quality = kCGInterpolationMedium;
        }
            break;
            
        case PHOTO_RESOLUTION_480_360: {
            size = CGSizeMake(WIDTHING_HIGH, HEIGHTING_HIGH);
//            quality = kCGInterpolationHigh;
        }
            break;
            
        default: {
            size = CGSizeMake(WIDTHING_MEDIUM, HEIGHTING_MEDIUM);
//            quality = kCGInterpolationMedium;
        }
            break;
    }
    
    if (picker.sourceType == UIImagePickerControllerSourceTypeCamera) {
        UIImage* image1 = [info objectForKey:UIImagePickerControllerOriginalImage];
        if([LogFileManager isLogConsole])
        {
            NSLog(@"image size width = %f x %f", image1.size.width, image1.size.height);
        }
        //resize image
//        self.selectedImage = [self imageWithImage:image1 scaledToSizeWithSameAspectRatio:size];
        self.selectedImage = [self imageByScalingImage:image1 toSize:size];
        if([LogFileManager isLogConsole])
        {
            NSLog(@"%@", NSStringFromCGSize(selectedImage.size));
        }
        
    } else if (picker.sourceType == UIImagePickerControllerSourceTypeSavedPhotosAlbum||picker.sourceType == UIImagePickerControllerSourceTypePhotoLibrary) {
        UIImage* image1 = [info objectForKey:UIImagePickerControllerOriginalImage];
        
        //resize image
//        self.selectedImage = [self imageWithImage:image1 scaledToSizeWithSameAspectRatio:size];
        self.selectedImage = [self imageByScalingImage:image1 toSize:size];
    }
   
    if ([delegate respondsToSelector:@selector(processPhoto:)]) {
        [delegate processPhoto:selectedImage];
    }
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Scale a image to a smaller size
-(UIImage*)imageByScalingImage:(UIImage*)sourceImage toSize:(CGSize)targetSize
{
	CGFloat targetWidth = targetSize.width;
	CGFloat targetHeight = targetSize.height;
    
	CGImageRef imageRef = [sourceImage CGImage];
	CGBitmapInfo bitmapInfo = CGImageGetBitmapInfo(imageRef);
	CGColorSpaceRef colorSpaceInfo = CGImageGetColorSpace(imageRef);
	
	if (bitmapInfo == kCGImageAlphaNone) {
		bitmapInfo = kCGImageAlphaNoneSkipLast;
	}
	
	CGContextRef bitmap;
	
	if (sourceImage.imageOrientation == UIImageOrientationUp || sourceImage.imageOrientation == UIImageOrientationDown) {
		bitmap = CGBitmapContextCreate(NULL, targetWidth, targetHeight, CGImageGetBitsPerComponent(imageRef), CGImageGetBytesPerRow(imageRef), colorSpaceInfo, bitmapInfo);
		
	} else {
		bitmap = CGBitmapContextCreate(NULL, targetHeight, targetWidth, CGImageGetBitsPerComponent(imageRef), CGImageGetBytesPerRow(imageRef), colorSpaceInfo, bitmapInfo);
		
	}	
	
	
	// In the right or left cases, we need to switch scaledWidth and scaledHeight,
	// and also the thumbnail point
	if (sourceImage.imageOrientation == UIImageOrientationLeft) {
		CGContextRotateCTM (bitmap, radians(90));
		CGContextTranslateCTM (bitmap, 0, -targetHeight);
		
	} else if (sourceImage.imageOrientation == UIImageOrientationRight) {
		CGContextRotateCTM (bitmap, radians(-90));
		CGContextTranslateCTM (bitmap, -targetWidth, 0);
		
	} else if (sourceImage.imageOrientation == UIImageOrientationUp) {
		// NOTHING
	} else if (sourceImage.imageOrientation == UIImageOrientationDown) {
		CGContextTranslateCTM (bitmap, targetWidth, targetHeight);
		CGContextRotateCTM (bitmap, radians(-180.));
	}
	
	CGContextDrawImage(bitmap, CGRectMake(0, 0, targetWidth, targetHeight), imageRef);
	CGImageRef ref = CGBitmapContextCreateImage(bitmap);
	UIImage* newImage = [UIImage imageWithCGImage:ref];
	
	CGContextRelease(bitmap);
	CGImageRelease(ref);
	
	return newImage; 
}

- (UIImage*)imageWithImage:(UIImage*)sourceImage scaledToSizeWithSameAspectRatio:(CGSize)targetSize
{  
    CGSize imageSize = sourceImage.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    CGFloat targetWidth = targetSize.width;
    CGFloat targetHeight = targetSize.height;
    CGFloat scaleFactor = 0.0;
    CGFloat scaledWidth = targetWidth;
    CGFloat scaledHeight = targetHeight;
    CGPoint thumbnailPoint = CGPointMake(0.0,0.0);
    
    if (CGSizeEqualToSize(imageSize, targetSize) == NO) {
        CGFloat widthFactor = targetWidth / width;
        CGFloat heightFactor = targetHeight / height;
        
        if (widthFactor > heightFactor) {
            scaleFactor = widthFactor; // scale to fit height
        }
        else {
            scaleFactor = heightFactor; // scale to fit width
        }
        
        scaledWidth  = width * scaleFactor;
        scaledHeight = height * scaleFactor;
        
        // center the image
        if (widthFactor > heightFactor) {
            thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5; 
        }
        else if (widthFactor < heightFactor) {
            thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
        }
    }     
    
    CGImageRef imageRef = [sourceImage CGImage];
    CGBitmapInfo bitmapInfo = CGImageGetBitmapInfo(imageRef);
    CGColorSpaceRef colorSpaceInfo = CGImageGetColorSpace(imageRef);
    
    if (bitmapInfo == kCGImageAlphaNone) {
        bitmapInfo = kCGImageAlphaNoneSkipLast;
    }
    
    CGContextRef bitmap;
    
    if (sourceImage.imageOrientation == UIImageOrientationUp || sourceImage.imageOrientation == UIImageOrientationDown) {
        bitmap = CGBitmapContextCreate(NULL, targetWidth, targetHeight, CGImageGetBitsPerComponent(imageRef), CGImageGetBytesPerRow(imageRef), colorSpaceInfo, bitmapInfo);
        
    } else {
        bitmap = CGBitmapContextCreate(NULL, targetHeight, targetWidth, CGImageGetBitsPerComponent(imageRef), CGImageGetBytesPerRow(imageRef), colorSpaceInfo, bitmapInfo);
        
    }   
    
    // In the right or left cases, we need to switch scaledWidth and scaledHeight,
    // and also the thumbnail point
    if (sourceImage.imageOrientation == UIImageOrientationLeft) {
        thumbnailPoint = CGPointMake(thumbnailPoint.y, thumbnailPoint.x);
        CGFloat oldScaledWidth = scaledWidth;
        scaledWidth = scaledHeight;
        scaledHeight = oldScaledWidth;
        
        CGContextRotateCTM (bitmap, radians(90));
        CGContextTranslateCTM (bitmap, 0, -targetHeight);
        
    } else if (sourceImage.imageOrientation == UIImageOrientationRight) {
        thumbnailPoint = CGPointMake(thumbnailPoint.y, thumbnailPoint.x);
        CGFloat oldScaledWidth = scaledWidth;
        scaledWidth = scaledHeight;
        scaledHeight = oldScaledWidth;
        
        CGContextRotateCTM (bitmap, radians(-90));
        CGContextTranslateCTM (bitmap, -targetWidth, 0);
        
    } else if (sourceImage.imageOrientation == UIImageOrientationUp) {
        // NOTHING
    } else if (sourceImage.imageOrientation == UIImageOrientationDown) {
        CGContextTranslateCTM (bitmap, targetWidth, targetHeight);
        CGContextRotateCTM (bitmap, radians(-180.));
    }
    
    CGContextDrawImage(bitmap, CGRectMake(thumbnailPoint.x, thumbnailPoint.y, scaledWidth, scaledHeight), imageRef);
    CGImageRef ref = CGBitmapContextCreateImage(bitmap);
    UIImage* newImage = [UIImage imageWithCGImage:ref];
    
    CGContextRelease(bitmap);
    CGImageRelease(ref);
    
    return newImage; 
}


//- (UIImage*)imageWithImage:(UIImage*)sourceImage scaledToSizeWithSameAspectRatio:(CGSize)targetSize {  
//    CGSize imageSize = sourceImage.size;
//    CGFloat width = imageSize.width;
//    CGFloat height = imageSize.height;
//   
//    CGFloat targetWidth = targetSize.width;
//    CGFloat targetHeight = targetSize.height;
//    
//    NSLog(@"source = %fx%f - target = %f x %f", width, height, targetWidth, targetHeight);
//    
//    CGFloat scaleFactor = 0.0;
//    CGFloat scaledWidth = targetWidth;
//    CGFloat scaledHeight = targetHeight;
//    CGPoint thumbnailPoint = CGPointMake(0.0, 0.0);
//    
//    if (CGSizeEqualToSize(imageSize, targetSize) == NO) {
//        CGFloat widthFactor = targetWidth / width;
//        CGFloat heightFactor = targetHeight / height;
//        
//        if (widthFactor > heightFactor) {
//            scaleFactor = widthFactor; // scale to fit height
//        } else {
//            scaleFactor = heightFactor; // scale to fit width
//        }
//        
//        scaledWidth  = width * scaleFactor;
//        scaledHeight = height * scaleFactor;
//        
//        // center the image
//        if (widthFactor > heightFactor) {
//            thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5; 
//        } else if (widthFactor < heightFactor) {
//            thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
//        }
//        NSLog(@"x,y = %f - %f - %f - %f", thumbnailPoint.x, thumbnailPoint.y, scaledWidth, scaledHeight);
//    }     
//    
//    CGImageRef imageRef = [sourceImage CGImage];
//    CGBitmapInfo bitmapInfo = CGImageGetBitmapInfo(imageRef);
//    CGColorSpaceRef colorSpaceInfo = CGImageGetColorSpace(imageRef);
//    
//    if (bitmapInfo == kCGImageAlphaNone) {
//        bitmapInfo = kCGImageAlphaNoneSkipLast;
//    }
//    
//    CGContextRef bitmap;
//    
//    if (sourceImage.imageOrientation == UIImageOrientationUp || sourceImage.imageOrientation == UIImageOrientationDown) {
//        bitmap = CGBitmapContextCreate(NULL, targetWidth, targetHeight, CGImageGetBitsPerComponent(imageRef), CGImageGetBytesPerRow(imageRef), colorSpaceInfo, bitmapInfo);        
//    } else {
//        bitmap = CGBitmapContextCreate(NULL, targetHeight, targetWidth, CGImageGetBitsPerComponent(imageRef), CGImageGetBytesPerRow(imageRef), colorSpaceInfo, bitmapInfo);
//    }   
//    
//    // In the right or left cases, we need to switch scaledWidth and scaledHeight,
//    // and also the thumbnail point
//    if (sourceImage.imageOrientation == UIImageOrientationLeft) {
//        thumbnailPoint = CGPointMake(thumbnailPoint.y, thumbnailPoint.x);
//        CGFloat oldScaledWidth = scaledWidth;
//        scaledWidth = scaledHeight;
//        scaledHeight = oldScaledWidth;
//        
//        CGContextRotateCTM (bitmap, radians(90));
//        CGContextTranslateCTM (bitmap, 0, -targetHeight);        
//    } else if (sourceImage.imageOrientation == UIImageOrientationRight) {
//        thumbnailPoint = CGPointMake(thumbnailPoint.y, thumbnailPoint.x);
//        CGFloat oldScaledWidth = scaledWidth;
//        scaledWidth = scaledHeight;
//        scaledHeight = oldScaledWidth;
//        
//        CGContextRotateCTM (bitmap, radians(-90));
//        CGContextTranslateCTM (bitmap, -targetWidth, 0);        
//    } else if (sourceImage.imageOrientation == UIImageOrientationUp) {
//        // NOTHING
//    } else if (sourceImage.imageOrientation == UIImageOrientationDown) {
//        CGContextTranslateCTM (bitmap, targetWidth, targetHeight);
//        CGContextRotateCTM (bitmap, radians(-180.));
//    }
//    
//    CGContextDrawImage(bitmap, CGRectMake(thumbnailPoint.x, thumbnailPoint.y, scaledWidth, scaledHeight), imageRef);
//    CGImageRef ref = CGBitmapContextCreateImage(bitmap);
//    UIImage* newImage = [UIImage imageWithCGImage:ref];
//    
//    CGContextRelease(bitmap);
//    CGImageRelease(ref);
//    
//    return newImage; 
//}

#pragma mark - ResizeImage
- (UIImage *)resizedImage:(CGSize)newSize
                transform:(CGAffineTransform)transform
           drawTransposed:(BOOL)transpose andImage: (UIImage *)imagePara
     interpolationQuality:(CGInterpolationQuality)quality {
    CGRect newRect = CGRectIntegral(CGRectMake(0, 0, newSize.width, newSize.height));
    CGRect transposedRect = CGRectMake(0, 0, newRect.size.height, 
                                       newRect.size.width);
    CGImageRef imageRef = imagePara.CGImage;

    // Build a context that's the same dimensions as the new size
    CGContextRef bitmap = CGBitmapContextCreate(NULL,
                                                newRect.size.width,
                                                newRect.size.height,
                                                CGImageGetBitsPerComponent(imageRef),
                                                0,
                                                CGImageGetColorSpace(imageRef),
                                                CGImageGetBitmapInfo(imageRef));
    
    // Rotate and/or flip the image if required by its orientation
    CGContextConcatCTM(bitmap, transform);
    
    // Set the quality level to use when rescaling
    CGContextSetInterpolationQuality(bitmap, quality);
    
    // Draw into the context; this scales the image
    CGContextDrawImage(bitmap, transpose ? transposedRect : newRect, imageRef);
    
    // Get the resized image from the context and a UIImage
    CGImageRef newImageRef = CGBitmapContextCreateImage(bitmap);
    UIImage *newImage = [UIImage imageWithCGImage:newImageRef];
    
    // Clean up
    CGContextRelease(bitmap);
    CGImageRelease(newImageRef);
    
    return newImage;
}


#pragma mark - TransfromForOrientation
- (CGAffineTransform)transformForOrientation:(CGSize)newSize 
                                    andImage:(UIImage *)imageF {
    CGAffineTransform transform = CGAffineTransformIdentity;
    
    switch (imageF.imageOrientation) {
        case UIImageOrientationDown: //EXIF = 3
        case UIImageOrientationDownMirrored: {  // EXIF = 4
            transform = CGAffineTransformTranslate(transform, newSize.width, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
        }
            break;
            
        case UIImageOrientationLeft:           // EXIF = 6
        case UIImageOrientationLeftMirrored: {  // EXIF = 5
            transform = CGAffineTransformTranslate(transform, newSize.width, 0);
            transform = CGAffineTransformRotate(transform, M_PI_2);
        }
            break;
            
        case UIImageOrientationUp: //EXIF = 1
        case UIImageOrientationUpMirrored: //EXIF = 2
        case UIImageOrientationRight:    // EXIF = 8
        case UIImageOrientationRightMirrored: { // EXIF = 7
            transform = CGAffineTransformTranslate(transform, 0, newSize.height);
            transform = CGAffineTransformRotate(transform, -M_PI_2);
        }
            break;
    }
    
    return transform;
}

#pragma mark - === Handle Check Save Data ===
#pragma mark
-(BOOL) checkSaveDataWithTagEvent:(NSInteger) tagOfEvent {
    
    //can go to message screen
    if (tagOfEvent == tagOfMessageButton) {
        //set status room is not completed
        [[CommonVariable sharedCommonVariable] setRoomIsCompleted:0];
        
        return NO;
    }
    
    //show please complete the room
    UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_complete_room_to_leave] delegate:nil cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles: nil];   
    [alert show];
    
    return YES; //no need to save before leaving, but need to complete the room before leaving
}

#pragma mark - iOS 7 Resources
-(void)loadFlatResource
{
    [photoButton setBackgroundImage:[UIImage imageNamed:imgTopGrayFlat] forState:UIControlStateNormal];
    [takePhotoButton setBackgroundImage:[UIImage imageNamed:imgPhotoButtonFlat] forState:UIControlStateNormal];
    [choosePhotoButton setBackgroundImage:[UIImage imageNamed:imgChoosePhotoButtonFlat] forState:UIControlStateNormal];
    [cancelButton setBackgroundImage:[UIImage imageNamed:imgCancelButtonFlat] forState:UIControlStateNormal];
    
}

@end
