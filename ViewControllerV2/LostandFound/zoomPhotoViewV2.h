//
//  zoomPhotoViewV2.h
//  mHouseKeeping
//
//  Created by TMS on 2/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface zoomPhotoViewV2 : UIViewController {
    IBOutlet UIImageView *zoomPhoto;
    NSData *imageData;
}

@property (nonatomic, strong) IBOutlet UIImageView *zoomPhoto;
@property (nonatomic, strong) NSData *imageData;

@end
