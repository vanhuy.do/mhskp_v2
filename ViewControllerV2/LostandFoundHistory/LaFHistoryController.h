//
//  LaFHistoryController.h
//  mHouseKeeping
//
//  Created by Quang Nguyen on 4/8/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LaFHistorySection.h"
#import "LaFHistorySectionInfo.h"

@interface LaFHistoryController : UIViewController<UITableViewDataSource, UITableViewDelegate,LaFHistorySectionDelegate>{
    NSMutableArray *sectionDatas;
    BOOL isFromLaFHistory;
}
@property (nonatomic, strong) NSMutableArray *sectionDatas;
@property (nonatomic, strong) IBOutlet UITableView *tbvLaFHistory;
@property (nonatomic) BOOL isFromLaFHistory;
@property (nonatomic) NSInteger openSectionIndex;
@end
