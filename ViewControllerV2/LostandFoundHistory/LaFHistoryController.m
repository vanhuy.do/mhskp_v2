//
//  LaFHistoryController.m
//  mHouseKeeping
//
//  Created by Quang Nguyen on 4/8/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "LaFHistoryController.h"
#import "MBProgressHUD.h"
#import "LostandFoundHistoryManager.h"
#import "LostandFoundHistoryModel.h"
#import "LaFHistorySectionInfo.h"
#import "LaFHistoryItemCell.h"
#import "LaFHistorySection.h"
#import "LFColorModelV2.h"
#import "MyNavigationBarV2.h"
#import "DeviceManager.h"
#import "ehkDefines.h"
#import "LanguageManagerV2.h"
#import "TopbarViewV2.h"
#import "UserManagerV2.h"
#import "CommonVariable.h"

@interface LaFHistoryController (privateMethods)
-(void) cartButtonPressed;

#define tagTopbarView 1234
//#define tagAlertSubmit 1235
//#define tagAlertNo 11
#define UIColorFromRGB(rgbValue) \
[UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0x00FF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0x0000FF))/255.0 \
alpha:1.0]
#define isDemoMode ([[NSUserDefaults standardUserDefaults] integerForKey:ENABLE_DEMO_MODE] == 1)

-(void) loadTopbarView;
-(void) showTopbarView;
-(void) hideTopbarView;
-(void) setHiddenTopbarView;
-(void) adjustShowForViews;
-(void) adjustRemoveForViews;
-(void) addButtonHandleShowHideTopbar;
-(void) topbarTapped:(UIButton *) sender;
-(TopbarViewV2 *) getTopBarView;
@end

@implementation LaFHistoryController
@synthesize sectionDatas,tbvLaFHistory,openSectionIndex;
@synthesize isFromLaFHistory;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Resize content views of current VC to fit with Navigation bar and tabbar
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    // Do any additional setup after loading the view from its nib.
    [tbvLaFHistory setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [tbvLaFHistory setBackgroundView:nil];
    [tbvLaFHistory setBackgroundColor:[UIColor clearColor]];
    [tbvLaFHistory setOpaque:YES];
    [self performSelector:@selector(loadTopbarView)];
    self.openSectionIndex = NSNotFound;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

#pragma mark - View lifecycle
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //    [self setCaptionsView];
    MBProgressHUD	*HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getRaLoadingData]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES];
    [self performSelector:@selector(loadDataAfterDelayWithHUD:) withObject:HUD afterDelay:0.1];
    
    //show wifi view
    if(!isDemoMode){
        [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationShowWifiView] object:nil];
    }
    self.openSectionIndex = NSNotFound;
    
    
}

#pragma mark - Load LaFHistory
-(void)loadingDatas {
    
    LostandFoundHistoryManager *lafhManager = [[LostandFoundHistoryManager alloc] init];
    
    sectionDatas = [[NSMutableArray alloc] init];
    NSInteger userId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    NSArray *listRoomId = [lafhManager loadRoomIdByUserId:userId];
    
    for (LostandFoundHistoryModel *LaFHModel in listRoomId) {
        
        LaFHistorySectionInfo *sectionInfo = [[LaFHistorySectionInfo alloc] init];
        sectionInfo.lafhRoomModel =LaFHModel;
        sectionInfo.open = false;
        NSMutableArray *listLaFHistory = [lafhManager loadAllLaFHistoryByUserId:userId AndRoomId:LaFHModel.lafh_room_id];
        
        for (LostandFoundHistoryModel *model in listLaFHistory){
            [sectionInfo insertObjectToNextIndex:model];
        }
        
        [sectionDatas addObject:sectionInfo];
        
    }
    if (sectionDatas.count == 0) {
        UILabel *lblAlert  =  [[UILabel alloc]init];
        lblAlert.frame     =  CGRectMake(15, 150, 290, 80);
        [lblAlert setText:[[LanguageManagerV2 sharedLanguageManager] getNoResultsFound]];
        lblAlert.textAlignment = NSTextAlignmentCenter;
        lblAlert.layer.cornerRadius = 10;
        [lblAlert setFont:[UIFont fontWithName:@"Arial-BoldMT" size:22]];
        lblAlert.numberOfLines = 2;
        [lblAlert setTextColor:[UIColor grayColor]];
        lblAlert.font = [UIFont boldSystemFontOfSize:20.0f];
        [tbvLaFHistory addSubview:lblAlert];
    }

    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void) loadDataAfterDelayWithHUD:(MBProgressHUD *) HUD {
    
    LostandFoundHistoryManager *manager = [[LostandFoundHistoryManager alloc] init];
    [manager deleteDatabaseAfterSomeDays];
    [self loadingDatas];
    [self performSelector:@selector(hiddenHUDAfterLoad:) withObject:HUD afterDelay:0.5];
}

#pragma didden after save.
-(void) hiddenHUDAfterLoad:(MBProgressHUD *)HUD{
    
    [HUD hide:YES];
    [HUD removeFromSuperview];
    [tbvLaFHistory reloadData];
}


#pragma mark - UITableView Datasource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return sectionDatas.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    LaFHistorySectionInfo *sectionInfo = [sectionDatas objectAtIndex:section];
    NSInteger numberRowInSection = sectionInfo.rowHeights.count;
    return sectionInfo.open ? numberRowInSection : 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifycell = @"LaFHistoryItemCell";
    LaFHistoryItemCell *cell = nil;
    
    cell = (LaFHistoryItemCell *)[tableView dequeueReusableCellWithIdentifier:identifycell];
    if (cell == nil) {
        NSArray *array = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([LaFHistoryItemCell class]) owner:self options:nil];
        cell = [array objectAtIndex:0];
    }
    
    //    cell.delegate = self;
    LaFHistorySectionInfo *sectionInfo = [sectionDatas objectAtIndex:indexPath.section];
    LostandFoundHistoryModel *model = [sectionInfo.rowHeights objectAtIndex:indexPath.row];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell.lblTitleName setText:[NSString stringWithFormat:@"%@",model.lafh_category_name]];
    [cell.lblSubTitleName setText:[NSString stringWithFormat:@"%@ x %d", model.lafh_item_name, (int)model.lafh_quantity]];
    [cell.lblUnderLine setBackgroundColor:[UIColor blackColor]];
    
    [cell.imgColor setFrame:CGRectMake(215, 5, 40, 40)];
    if (model.lafh_color_id > 0) {
        
        unsigned result = 0;
        NSScanner *scanner = [NSScanner scannerWithString:model.lafh_color_code];
        [scanner setScanLocation:1];
        // bypass '#' character
        [scanner scanHexInt:&result];
        UIColor *colorName = UIColorFromRGB(result);
        [[cell.imgColor layer] setCornerRadius:8.0f];
        [[cell.imgColor layer] setMasksToBounds:YES];
        [cell.imgColor setBackgroundColor:colorName];
        [cell.imgColor setNeedsDisplay];
        CGRect rect = CGRectMake(0, 0, 40, 40);
        UIGraphicsBeginImageContext(rect.size);
        CGContextRef context = UIGraphicsGetCurrentContext();
        CGContextSetFillColorWithColor(context, [colorName CGColor]);
        CGContextFillRect(context, rect);
        UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        [cell.imgColor setImage:img];
    }
    else {
        [cell.imgColor setBackgroundColor:[UIColor clearColor]];
        [cell.imgColor setImage:[UIImage imageNamed:@"no_color.png"]];
    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 65;//65//the number of value must equal the value of cell, if not the index will overwite
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 60;//60
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    LaFHistorySectionInfo *sectionInfo = [sectionDatas objectAtIndex:section];
    
    if (sectionInfo.headerView == nil) {
        
        LaFHistorySection *headerView = [[LaFHistorySection alloc] initWithSection:section LaFHistoryModel:sectionInfo.lafhRoomModel andOpenStatus:NO];
        sectionInfo.headerView = headerView;
        headerView.delegate = self;
    }
    return sectionInfo.headerView;
}

//update code
#pragma mark - Section Header Delegate
-(void)sectionHeaderView:(LaFHistorySection *)sectionheaderView sectionOpened:(NSInteger)section {
    
    LaFHistorySectionInfo *sectionInfo = [sectionDatas objectAtIndex:section];
    sectionInfo.open = YES;
    
    NSInteger countOfRowsToInsert = [sectionInfo.rowHeights count];
    NSMutableArray *indexPathsToInsert = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i < countOfRowsToInsert; i++) {
        [indexPathsToInsert addObject:[NSIndexPath indexPathForRow:i inSection:section]];
    }
    
    UITableViewRowAnimation insertAnimation;
    
    
    UITableViewRowAnimation deleteAnimation;
    
    
    NSMutableArray *indexPathsToDelete = [[NSMutableArray alloc] init];
    
    NSInteger previousOpenSectionIndex = self.openSectionIndex;
    
    if (openSectionIndex == -1) {
        
    }
    else {
        if (previousOpenSectionIndex != NSNotFound) {
            
            LaFHistorySectionInfo *previousOpenSection = [self.sectionDatas objectAtIndex:previousOpenSectionIndex];
            previousOpenSection.open = NO;
            [previousOpenSection.headerView toggleOpenWithUserAction:NO];
            
            NSInteger countOfRowsToDelete =[previousOpenSection.rowHeights count];
            for (NSInteger i = 0; i < countOfRowsToDelete; i++) {
                [indexPathsToDelete addObject:[NSIndexPath indexPathForRow:i inSection:previousOpenSectionIndex]];
            }
            
        }
    }
    
    if (previousOpenSectionIndex == NSNotFound || section < previousOpenSectionIndex) {
        insertAnimation = UITableViewRowAnimationTop;
        deleteAnimation = UITableViewRowAnimationBottom;
    }
    else {
        insertAnimation = UITableViewRowAnimationBottom;
        deleteAnimation = UITableViewRowAnimationTop;
    }
    
    
    [self.tbvLaFHistory beginUpdates];
    
    [self.tbvLaFHistory insertRowsAtIndexPaths:indexPathsToInsert withRowAnimation:insertAnimation];
    [self.tbvLaFHistory deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:deleteAnimation];
    [self.tbvLaFHistory endUpdates];
    
    if([indexPathsToInsert count] > 0){
        [self.tbvLaFHistory scrollToRowAtIndexPath:[indexPathsToInsert objectAtIndex:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    self.openSectionIndex = section;
    
}

-(void)sectionHeaderView:(LaFHistorySection *)sectionheaderView sectionClosed:(NSInteger)section {

    LaFHistorySectionInfo *sectionInfo = [sectionDatas objectAtIndex:section];
    sectionInfo.open = NO;
    NSInteger countOfRowsToDelete = [self.tbvLaFHistory numberOfRowsInSection:section];
    
    if (countOfRowsToDelete > 0) {
        NSMutableArray *indexPathsToDelete = [[NSMutableArray alloc] init];
        for (NSInteger i = 0; i < countOfRowsToDelete; i++) {
            [indexPathsToDelete addObject:[NSIndexPath indexPathForRow:i inSection:section]];
        }
        
        [tbvLaFHistory beginUpdates];
        [self.tbvLaFHistory deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:UITableViewRowAnimationAutomatic];
        [tbvLaFHistory endUpdates];
    }
    
    self.openSectionIndex = NSNotFound;
}


#pragma mark - Handle Topbar Methods
-(void)loadTopbarView {
    TopbarViewV2 *topbar = [[TopbarViewV2 alloc] initViewWithSuperModel:[[CommonVariable sharedCommonVariable] superRoomModel]];
    [topbar setTag:tagTopbarView];
    [self.view addSubview:topbar];
    [topbar setHidden:YES];
    
    [topbar setAlpha:0.0];
    [self addButtonHandleShowHideTopbar];
}

-(void)showTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationWillStartSelector:@selector(setHiddenTopbarView)];
    
    f = topbar.frame;
    f.origin.y = 0;
    [topbar setFrame:f];
    [topbar setAlpha:1.0];
    
    [self adjustShowForViews];
    
    [UIView commitAnimations];
}

-(void)hideTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDidStopSelector:@selector(setHiddenTopbarView)];
    [UIView setAnimationDelegate:self];
    
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    [topbar setAlpha:0.0];
    
    [self adjustRemoveForViews];
    
    [UIView commitAnimations];
}

-(void)setHiddenTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    [topbar setHidden:!topbar.hidden];
}

-(TopbarViewV2 *)getTopBarView {
    return (TopbarViewV2 *)[self.view viewWithTag:tagTopbarView];
}

-(void)addButtonHandleShowHideTopbar {
    UIView *vBarButton = [[UIView alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
    UIButton *titleBarButtonFirst = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 200, 30)];
    
    titleBarButtonFirst.titleLabel.backgroundColor = [UIColor clearColor];
    [titleBarButtonFirst.titleLabel setFont: FONT_BUTTON_TOPBAR];
    titleBarButtonFirst.titleLabel.textAlignment = NSTextAlignmentCenter;
    titleBarButtonFirst.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
    
    [titleBarButtonFirst.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
    [titleBarButtonFirst.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
    [titleBarButtonFirst addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    [titleBarButtonFirst setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_HISTORY] forState:UIControlStateNormal];
    
    [vBarButton addSubview:titleBarButtonFirst];
    
    
    UIButton *titleBarButtonSecond = [[UIButton alloc] initWithFrame:CGRectMake(0, 28, 200, 22)];
    [titleBarButtonSecond setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [titleBarButtonSecond.titleLabel setFont:FONT_BUTTON_TOPBAR_SECOND];
    [titleBarButtonSecond setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_LOSTANDFOUND_CASE_TITLE] forState:UIControlStateNormal];
    [titleBarButtonSecond addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    [vBarButton addSubview:titleBarButtonSecond];
    
    self.navigationItem.titleView = vBarButton;
    
    //Move title to center vertical
    CGRect titleViewBounds = self.navigationItem.titleView.bounds;
    int moveLeftTitle = 0;
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        moveLeftTitle = 10;
    }
    self.navigationItem.titleView.bounds = CGRectMake(titleViewBounds.origin.x + moveLeftTitle, Y_TOPBAR_TITLE, titleViewBounds.size.width, titleViewBounds.size.height);
    
    //back button navigation
    self.navigationItem.leftBarButtonItem = [MyNavigationBarV2 createBackButtonWithTarget:self selector:@selector(btnBackPressed) forControlEvents:UIControlEventTouchUpInside];
}

-(void)btnBackPressed
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)topbarTapped:(UIButton *)sender {
    TopbarViewV2 *topbar = [self getTopBarView];
    [sender setEnabled:NO];
    if (topbar.isHidden) {
        [self showTopbarView];
    } else {
        [self hideTopbarView];
    }
    [self performSelector:@selector(topbarButtonEnable:) withObject:sender afterDelay:0.5];
}

-(void) topbarButtonEnable:(UIButton *) topbarBtn {
    [topbarBtn setEnabled:YES];
}

-(void)adjustShowForViews {
    //adjust all views when show
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    CGRect fRoomView = tbvLaFHistory.frame;
    [tbvLaFHistory setFrame:CGRectMake(0, f.size.height, 320, fRoomView.size.height)];
    
}

-(void)adjustRemoveForViews {
    //adjust all views when hide
    //    TopbarViewV2 *topbar = [self getTopBarView];
    //    CGRect f = topbar.frame;
    
    CGRect fRoomView = tbvLaFHistory.frame;
    [tbvLaFHistory setFrame:CGRectMake(0, 0, 320, fRoomView.size.height)];
    //    [tbvEngineeringHistory setFrame:CGRectMake(0, 0, 3209, fRoomView.size.height + f.size.height)];
    
    //    CGRect ftbv = tbvLinen.frame;
    //    [tbvLinen setFrame:CGRectMake(0, fRoomView.size.height, 320, ftbv.size.height + f.size.height)];
    
}

@end
