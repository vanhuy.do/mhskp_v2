//
//  LaFHistoryItemCell.h
//  mHouseKeeping
//
//  Created by Quang Nguyen on 4/8/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LaFHistoryItemCell : UITableViewCell
@property (nonatomic, strong) IBOutlet UILabel *lblTitleName;
@property (nonatomic, strong) IBOutlet UILabel *lblSubTitleName;
@property (nonatomic, strong) IBOutlet UILabel *lblUnderLine;
@property (nonatomic, strong) IBOutlet UIImageView *imgColor;
//@property (nonatomic, strong) IBOutlet UILabel *lblColor;
@end
