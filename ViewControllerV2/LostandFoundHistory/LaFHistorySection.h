//
//  LaFHistorySection.h
//  mHouseKeeping
//
//  Created by Quang Nguyen on 4/8/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LostandFoundHistoryModel.h"
@class LaFHistorySection;

@protocol LaFHistorySectionDelegate <NSObject>

@optional
-(void) sectionHeaderView:(LaFHistorySection *) sectionheaderView sectionOpened:(NSInteger) section;
-(void) sectionHeaderView:(LaFHistorySection *) sectionheaderView sectionClosed:(NSInteger) section;

@end

@interface LaFHistorySection : UIView{
    __unsafe_unretained id<LaFHistorySectionDelegate> delegate;
    NSInteger section;
    BOOL isToggle;
}
@property (nonatomic, assign) NSInteger section;
@property (nonatomic, assign) id<LaFHistorySectionDelegate> delegate;
@property (strong, nonatomic) IBOutlet UIImageView *imgLaFHistory;
@property (strong, nonatomic) IBOutlet UILabel *lblLaFHistoryTitle;
@property (strong, nonatomic) IBOutlet UIImageView *imgDetail;

-(void) toggleOpenWithUserAction:(BOOL) userAction;
//- (IBAction)btnPhisicalCHSectionPressed:(id)sender;
-(id) initWithSection:(NSInteger) section LaFHistoryModel:(LostandFoundHistoryModel *) roomModel andOpenStatus:(BOOL) isOpen;
@end
