//
//  LaFHistorySection.m
//  mHouseKeeping
//
//  Created by Quang Nguyen on 4/8/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "LaFHistorySection.h"

@implementation LaFHistorySection

@synthesize section,delegate,imgDetail,imgLaFHistory,lblLaFHistoryTitle;

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void) initControls {
    [self setFrame:CGRectMake(0, 0, 320, 60)];
    
    UIImageView *bg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 60)];
    [bg setImage:[UIImage imageNamed:@"big_bt_640x111.png"]];
    [self addSubview:bg];
    
    imgLaFHistory = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 70, 60)];
    [imgLaFHistory setContentMode:UIViewContentModeScaleAspectFit];
    [self addSubview:imgLaFHistory];
    
    lblLaFHistoryTitle = [[UILabel alloc] initWithFrame:CGRectMake(70, 5, 220, 50)];
    [lblLaFHistoryTitle setFont:[UIFont fontWithName:@"Arial-BoldMT" size:17]];
    [lblLaFHistoryTitle setBackgroundColor:[UIColor clearColor]];
    [lblLaFHistoryTitle setTextColor:[UIColor colorWithRed:55.0/255.0 green:55.0/255.0 blue:55.0/255.0 alpha:1.0]];
    [lblLaFHistoryTitle setTextAlignment:NSTextAlignmentCenter];
    [self addSubview:lblLaFHistoryTitle];
    
    imgDetail = [[UIImageView alloc] initWithFrame:CGRectMake(285, 15, 30, 30)];
    [imgDetail setImage:[UIImage imageNamed:imgRightRow]];
    [self addSubview:imgDetail];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(0, 0, 320, 70)];
    [button addTarget:self action:@selector(btnPhysicalCHPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:button];
}

-(id)init {
    self = [super init];
    if (self) {
        [self initControls];
    }
    return self;
}

-(id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initControls];
    }
    return self;
}

- (void)dealloc {
    //    [imgLinenSection release];
    //    [lblLinenTitle release];
    //    [imgDetail release];
    //    [super dealloc];
}

-(void) toggleOpenWithUserAction:(BOOL) userAction {
    if (userAction) {
        if (isToggle == NO) {
            if ([delegate respondsToSelector:@selector(sectionHeaderView:sectionOpened:)]) {
                isToggle = YES;
                [delegate sectionHeaderView:self sectionOpened:section];
                
                [imgDetail setImage:[UIImage imageNamed:imgRightRowOpen]];
            }
        }
        else {
            if ([delegate respondsToSelector:@selector(sectionHeaderView:sectionClosed:)]) {
                isToggle = NO;
                [imgDetail setImage:[UIImage imageNamed:imgRightRow]];
                [delegate sectionHeaderView:self sectionClosed:section];
            }
        }
    }
    else
    {
        if (isToggle == NO) {
            isToggle = YES;
            [imgDetail setImage:[UIImage imageNamed:imgRightRowOpen]];
        }
        else {
            isToggle = NO;
            [imgDetail setImage:[UIImage imageNamed:imgRightRow]];
        }
    }
}

- (IBAction)btnPhysicalCHPressed:(id)sender {
    [self toggleOpenWithUserAction:YES];
}

-(id) initWithSection:(NSInteger) sectionIndex LaFHistoryModel:(LostandFoundHistoryModel *) roomModel andOpenStatus:(BOOL) isOpen {
    self = [self init];
    
    if (self) {
        section = sectionIndex;
//        lblLaFHistoryTitle.text = [NSString stringWithFormat:@"%d",model.room_id];
        lblLaFHistoryTitle.text = roomModel.lafh_room_id;
        isToggle = isOpen;
        //        [imgLinenSection setImage:[UIImage imageWithData:categoryModel.linenCategoryImage]];
        if (isOpen) {
            [imgDetail setImage:[UIImage imageNamed:imgRightRowOpen]];
        } else {
            [imgDetail setImage:[UIImage imageNamed:imgRightRow]];
        }
    }
    
    return self;
}


@end
