//
//  AdHocMessageBoxV2.h
//  mHouseKeeping
//
//  Created by TMS on 1/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MessageModelV2.h"
#import "AlertAdvancedSearch.h"
#import "AdHocMessageManagerV2.h"
#import "AdhocMessageDetailV2.h"
#import "TopbarViewV2.h"
#import "CommonVariable.h"
#import "MBProgressHUD.h"

@interface AdHocMessageBoxV2 : UIViewController<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate,UIAlertViewDelegate,MBProgressHUDDelegate>
{
    UISearchBar *searchBarMessage;
    UIButton *btnInBoxTab;
    UIButton *btnOutBoxTab;
    UILabel *lblInBox;
    UILabel *lblOutBox;
    UILabel *lblTime;
    UILabel *lblAddress;
    TopbarViewV2 * topBarView;
    UITableView *tbvContent;
    NSMutableArray *arrayDataSource;
	NSMutableArray	*arrayDataTable;
    BOOL isInBox;
    BOOL isOutBox;
    BOOL isAlreadySelectOutBox;
    BOOL isSearchBox;
    NSInteger selectedIndex;
    UIButton *hideSearchBtn;
    MBProgressHUD *HUD;
    //NSMutableArray* listOfUser;
    NSMutableDictionary *arrayReceivers;
    NSMutableDictionary *arrayReceiversTable;
}
@property(nonatomic,strong)MBProgressHUD *HUD;
@property (nonatomic,strong) UIButton *hideSearchBtn;
@property NSInteger selectedIndex;
@property BOOL  isInBox;
@property BOOL isOutBox;
@property (nonatomic,strong)TopbarViewV2 * topBarView;
@property (nonatomic,strong) IBOutlet UISearchBar *searchBarMessage;
@property (nonatomic,strong) IBOutlet UIButton *btnInBoxTab;
@property (nonatomic,strong) IBOutlet UIButton *btnOutBoxTab;
@property (nonatomic,strong) IBOutlet UILabel *lblInBox;
@property (nonatomic,strong) IBOutlet UILabel *lblOutBox;
@property (nonatomic,strong) IBOutlet UILabel *lblTime;
@property (nonatomic,strong) IBOutlet UILabel *lblAddress;
@property (nonatomic,strong) IBOutlet UITableView *tbvContent;
@property (nonatomic,strong) NSMutableArray *arrayDataSource;
@property (nonatomic,strong) NSMutableArray	*arrayDataTable;

- (void)showTitleBox;
-(IBAction)selectInboxTab:(id)sender;
-(IBAction)selectOutboxTab:(id)sender;
-(void)loadDataToOutBox;
-(void)loadDataToInBox;
@end
