//
//  AdHocMessageBoxV2.m
//  mHouseKeeping
//
//  Created by TMS on 1/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AdHocMessageBoxV2.h"
#import "AdhocMessageTableViewCell.h"
#import "UserManagerV2.h"
#import "CustomAlertViewV2.h"
#import "ehkConvert.h"
#import "MyNavigationBarV2.h"
#import "DeviceManager.h"
#import "ehkDefinesV2.h"
#import "UIImage+Tint.h"
#import "HomeViewV2.h"
#import "AdhocMessageDetailV2.h"

#define REDCELL 6.0f/255.0f
#define GREENCELL 62.0f/255.0f
#define BLUECELl 127.0f/255.0f
#define CELL_FONT_SIZE  14
#define HIGHLIGHT_COLOR [UIColor colorWithRed:REDCELL green:GREENCELL blue:BLUECELl alpha:1]
#define NORMAL_COLOR [UIColor colorWithRed:REDCELL green:GREENCELL blue:BLUECELl alpha:0.5]
//#define FROMTITLE @"From"
//#define TOTITLE @"To"
//#define DATETIMETITLE @"Date & Time"
//#define INBOXTITLE @"Inbox"
//#define OUTBOXTITLE @"Sent Items"
//#define TITLENAVI @"Message History"
#define sizeTitle           22
#define tagDeletePhoto 3
//#define DELETECONFIRM @"Please confirm to delete."
//#define ALERTTITLE @"Alert"
//#define YESTITLE @"Yes"
//#define NOTITLE @"No"
@interface AdHocMessageBoxV2 (PrivateMethods) <AdhocMEssageDetailV2Delegate>
- (void)highlight:(BOOL)highlight forRowAtIndexPath:(NSIndexPath*)indexPath;
- (void)tableView:(UITableView *)tableView highlight:(BOOL)highlight forRowAtIndexPath:(NSIndexPath*)indexPath;
- (void)stopWaiting;
- (void)updateUnreadMessage;
@end
    
@implementation AdHocMessageBoxV2

@synthesize hideSearchBtn;
@synthesize searchBarMessage;
@synthesize btnInBoxTab;
@synthesize btnOutBoxTab;
@synthesize lblInBox;
@synthesize lblOutBox;
@synthesize lblTime;
@synthesize lblAddress;
@synthesize tbvContent;
@synthesize arrayDataSource;
@synthesize arrayDataTable;
@synthesize isOutBox,isInBox; 
@synthesize selectedIndex,topBarView,HUD;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    //set selected tab message
    [[HomeViewV2 shareHomeView] setSelectedButton:tagOfMessageButton];
    [searchBarMessage setPlaceholder:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_SEARCH]];
    if(![searchBarMessage.text isEqualToString:@""]){
        [self searchBar:searchBarMessage textDidChange:searchBarMessage.text];
    }
    else{
        [self showTitleBox];
    }
    if(!isDemoMode){
        [self performSelector:@selector(loadData) withObject:nil afterDelay:0.0f];
    }
    
    [tbvContent reloadData];
}

// Switch inbox/outbox and load data for inbox/outbox
- (void)showTitleBox
{
    // Do any additional setup after loading the view from its nib.
    //config image for switchview use button
    @autoreleasepool {
        UIImage *imageActive = [UIImage imageBeforeiOS7:imgActiveMessage equaliOS7:imgActiveMessageFlat];
        UIImage *imgNoActive = [UIImage imageBeforeiOS7:imgDeactiveMessage equaliOS7:imgDeactiveMessageFlat];
        //NSMutableArray *inboxMsgTotal=[self loadDataForInbox];
        //NSMutableArray *outboxMsgTotal = [self loadDataForOutBox];
        
        int userId = [UserManagerV2 sharedUserManager].currentUser.userId;
        
        //Enhance count message
        if (isDemoMode) {
            if (arrayReceivers == nil) {
                arrayReceivers = [[NSMutableDictionary alloc] init];
            }
            if (arrayReceiversTable == nil) {
                arrayReceiversTable = [[NSMutableDictionary alloc] init];
            }
            NSMutableArray *messageOutBox = [[AdHocMessageManagerV2 sharedAdHocMessageManagerV2]  loadMessageSentByOwnerId:[UserManagerV2 sharedUserManager].currentUser.userId];
            
            for (MessageModelV2 *curMessage in messageOutBox) {
                NSMutableArray *listReceivers = [[[AdHocMessageManagerV2 alloc] init] loadMessageReceiversByOwnerId:[UserManagerV2 sharedUserManager].currentUser.userId groupId:(int)curMessage.message_group_id];
                if(listReceivers != nil){
                    if(![arrayReceiversTable objectForKey:[NSNumber numberWithInt:(int)curMessage.message_group_id]]){
                        [arrayReceiversTable setObject:listReceivers forKey:[NSNumber numberWithInt:(int)curMessage.message_group_id]];
                    }
                    if(![arrayReceivers objectForKey:[NSNumber numberWithInt:(int)curMessage.message_group_id]]){
                        [arrayReceivers setObject:listReceivers forKey:[NSNumber numberWithInt:(int)curMessage.message_group_id]];
                    }
                }
            }
        }
        int countMessageInbox = [[AdHocMessageManagerV2 sharedAdHocMessageManagerV2] countMessageInboxByOwnerId:userId];
        int countMessageOutBox = 0;
        if(isDemoMode) {
            countMessageOutBox = (int)[arrayReceiversTable count];
        } else {
            countMessageOutBox = [[AdHocMessageManagerV2 sharedAdHocMessageManagerV2] countMessageSentByOwnerId:userId];
        }
        
        [self.lblInBox setText:[NSString stringWithFormat:@"%@(%d)", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_adhocMsg_inbox_title] ,countMessageInbox]];
        [self.lblOutBox setText:[NSString stringWithFormat:@"%@(%d)", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_sent_item] ,countMessageOutBox]];
        lblTime.text= [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_date_and_time];
        if (isInBox) {
            [btnInBoxTab setBackgroundImage:imageActive forState:UIControlStateNormal];
            [btnOutBoxTab setBackgroundImage:imgNoActive forState:UIControlStateNormal];
            [lblOutBox setTextColor:[UIColor blackColor]];
            [lblInBox setTextColor:[UIColor grayColor]];
            lblAddress.text = [[LanguageManagerV2 sharedLanguageManager] getFrom];
            [self loadDataToInBox];
        } else if(isOutBox) {
            [btnInBoxTab setBackgroundImage:imgNoActive forState:UIControlStateNormal];
            [btnOutBoxTab setBackgroundImage:imageActive forState:UIControlStateNormal];
            [lblInBox setTextColor:[UIColor blackColor]];
            [lblOutBox setTextColor:[UIColor grayColor]];
            lblAddress.text= [NSString stringWithFormat:@"%@",[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_to]];
            
            //Load WS data first show
            if (!isAlreadySelectOutBox) {
                isAlreadySelectOutBox = YES;
            }
            
            [self loadDataToOutBox];
        }
        if (isDemoMode)
            [tbvContent reloadData];
        else
            [self performSelectorOnMainThread:@selector(stopWaiting) withObject:nil waitUntilDone:NO];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //Resize content views of current VC to fit with Navigation bar and tabbar
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    [tbvContent setBackgroundView:nil];
    [tbvContent setBackgroundColor:[UIColor clearColor]];
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        //Clear redundant view for header, footter
        CGRect frameHeaderFooter = tbvContent.tableHeaderView.frame;
        frameHeaderFooter.size.height = 0.1f;
        UIView *headerView = [[UIView alloc] initWithFrame:frameHeaderFooter];
        [tbvContent setTableHeaderView:headerView];
        
        frameHeaderFooter = tbvContent.tableFooterView.frame;
        frameHeaderFooter.size.height = 0.1f;
        UIView *footerView = [[UIView alloc] initWithFrame:frameHeaderFooter];
        [tbvContent setTableFooterView:footerView];
        [tbvContent setOpaque:YES];
        
        [self loadFlatResource];
    }
    isAlreadySelectOutBox = NO;
    if (topBarView == nil) {
        topBarView = [[TopbarViewV2 alloc] initViewWithSuperModel:[CommonVariable sharedCommonVariable].superRoomModel];
        [self.view addSubview:topBarView];
    }
    
//    UIButton *titleBarButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 230, 44)];
//    [searchBarMessage setPlaceholder:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_SEARCH]];
//    
//    titleBarButton.titleLabel.backgroundColor = [UIColor clearColor];
//    [titleBarButton.titleLabel setFont: FONT_BUTTON_TOPBAR];
//    titleBarButton.titleLabel.textAlignment = NSTextAlignmentCenter;
//    titleBarButton.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
//    [titleBarButton.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
//    [titleBarButton.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
//    [titleBarButton setTitle:  forState:UIControlStateNormal];
//    [titleBarButton addTarget:self action:@selector(hiddenHeaderView) forControlEvents:UIControlEventTouchUpInside];
//    self.navigationItem.titleView = titleBarButton;
////    if (titleBarButton.titleLabel.text.length >= 14) {
////        titleBarButton.titleLabel.font = FONT_SMALL_BUTTON_TOPBAR;
////    }
    
     self.navigationItem.titleView = [MyNavigationBarV2 createTitleBarWithFirstRow:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_MESSAGE] secondRow:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_message_history] target:self selector:@selector(hiddenHeaderView) forControlEvents:UIControlEventTouchUpInside];
    
    //Move title to center vertical
    CGRect titleViewBounds = self.navigationItem.titleView.bounds;
    int moveLeftTitle = 0;
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        moveLeftTitle = 10;
    }
    self.navigationItem.titleView.bounds = CGRectMake(titleViewBounds.origin.x + moveLeftTitle, Y_TOPBAR_TITLE, titleViewBounds.size.width, titleViewBounds.size.height);
    
    //back button navigation
    self.navigationItem.leftBarButtonItem = [MyNavigationBarV2 createBackButtonWithTarget:self selector:@selector(backBarPressed) forControlEvents:UIControlEventTouchUpInside];
}

-(void)loadData
{
    if(!HUD) {
        HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    }
    
    [self.tabBarController.view addSubview:HUD];
    HUD.delegate = self;
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getRaLoadingData]];
    [HUD show:YES];
    
    [[HomeViewV2 shareHomeView] waitingLoadingData];
    
    //Hao Tran - Load new Message from server
    BOOL isShowHud = NO;
    [[HomeViewV2 shareHomeView] messageReciever:isShowHud callFrom:IS_PUSHED_FROM_MESSAGE_INBOX];
    
    //Hao Tran Remove
    //[NSThread detachNewThreadSelector:@selector(showTitleBox) toTarget:self withObject:nil];
    
    //Delete old message
    int userId = [UserManagerV2 sharedUserManager].currentUser.userId;
    int messagePeriod = [[UserManagerV2 sharedUserManager].currentUserAccessRight getMessagePeriod];
    NSString *lastModified = [ehkConvert dateStringAgoWithPeriodDays:messagePeriod];
    NSString *lastModifiedDate = [ehkConvert commonDateRemoveTime:lastModified];
    [[AdHocMessageManagerV2 sharedAdHocMessageManagerV2] deleteMessageByOwnerId:userId];
    
    //Check and load userlist before getting message
    [[HomeViewV2 shareHomeView] loadUserListByCurrentUser];
    
    /* DungPhan - discussion with FM on 20151007
     for new IPA and APK, will call new API: GetReceivedAdHocMessageRoutine and GetSendAdHocMessageRoutine
     - Old API GetReceivedAdHocMessage and GetSentAdHocMessage will not be called
     */
    [[AdHocMessageManagerV2 sharedAdHocMessageManagerV2] getReceivedAdHocMessageRoutineByUserId:userId groupMessageId:0 hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId lastModified:lastModifiedDate];
    [[AdHocMessageManagerV2 sharedAdHocMessageManagerV2] getSendAdHocMessageRoutineByUserId:userId groupMessageId:0 hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId lastModified:lastModifiedDate];
    
    //    [[AdHocMessageManagerV2 sharedAdHocMessageManagerV2] loadWSMessageReceivedByUserId:userId groupMessageId:0 hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId lastModified:lastModifiedDate];
    //    [[AdHocMessageManagerV2 sharedAdHocMessageManagerV2] loadWSMessageSentByUserId:[UserManagerV2 sharedUserManager].currentUser.userId groupMessageId:0 hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId lastModified:lastModifiedDate];

    
    [self showTitleBox];
    
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
    
    [tbvContent setBackgroundColor:[UIColor clearColor]];
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:
                                   [UIImage imageNamed:imgBackground]]];
    
    //Hao Tran - Remove
    //listOfUser = [[AdHocMessageManagerV2 sharedAdHocMessageManagerV2] loadAllUserListDataByUserHotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId];
}

- (void)backBarPressed {
    [self.navigationController popViewControllerAnimated:YES];
}

// Stop waiting indicator and search if have keyword
- (void)stopWaiting {
    
    [HUD hide:YES];
    [HUD removeFromSuperview];
    
    //Repaint all views
    [self.view setNeedsDisplay];
    
    if (searchBarMessage.text.length>0) {
        [self searchBarSearchButtonClicked:searchBarMessage];
    }
    // Re-enable scroll
    tbvContent.scrollEnabled = YES;
    [tbvContent reloadData];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - UITableView Delegate Methods
- (void)tableView:(UITableView *)tableView highlight:(BOOL)highlight forRowAtIndexPath:(NSIndexPath*)indexPath {
    AdhocMessageTableViewCell *cell = (AdhocMessageTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    if (highlight) {
        [cell.lblMessageDate setTextColor:[UIColor blackColor]];
        [cell.lblMessageTo setTextColor:HIGHLIGHT_COLOR];
        [cell.lblMessageTopic setTextColor:[UIColor blackColor]];
    } else {
        [cell.lblMessageDate setTextColor:[UIColor lightGrayColor]];
        [cell.lblMessageTo setTextColor:NORMAL_COLOR];
        [cell.lblMessageTopic setTextColor:[UIColor lightGrayColor]];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60.0f;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(isDemoMode){
        if(isInBox){
            return [arrayDataTable count];
        }
        else{
            return [arrayReceiversTable count];
        }
    }
    else
        return [arrayDataTable count];
    
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    cell.backgroundColor = [UIColor colorWithRed:255/255.0 green:255/255.0 
                                            blue:255/255.0 alpha:1.0];
} 


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentify = @"cellMessageIndentify";
    AdhocMessageTableViewCell *cell = nil; 
    cell = (AdhocMessageTableViewCell *)[tableView dequeueReusableCellWithIdentifier:                                     cellIdentify];
    if (cell == nil) {
        NSArray *arrayCell = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([AdhocMessageTableViewCell class]) owner:self options:nil];
        cell = [arrayCell objectAtIndex:0];
    }
    UIColor *cellColor=[UIColor colorWithRed:REDCELL green:GREENCELL blue:BLUECELl alpha:1];
    [cell.lblMessageTo setTextColor:cellColor];
    
    bool isEnglish = [[LanguageManagerV2 sharedLanguageManager] currentLanguageIsEqualTo:@"%@",ENGLISH_LANGUAGE];
    MessageModelV2 *messageObject=[arrayDataTable objectAtIndex:indexPath.row];
    cell.lblMessageDate.text = [ehkConvert DateToStringWithString:messageObject.message_last_modified fromFormat:@"yyyy-MM-dd HH:mm:ss" toFormat:@"dd MMM yyyy HH:mm"];
    if (isInBox) {
        cell.lblMessageTo.text = messageObject.message_from_user_name;
        cell.lblMessageTopic.text = messageObject.message_topic;
    } else {
        NSMutableArray *listReceivers = nil;
        if(isDemoMode)
            listReceivers = [arrayReceiversTable objectForKey:[NSNumber numberWithInteger:messageObject.message_group_id]];
        else
            listReceivers = [arrayReceivers objectForKey:[NSNumber numberWithInteger:messageObject.message_id]];
        if ([listReceivers count] > 0) {
            UserListModelV2 * toObject = [listReceivers objectAtIndex:0];
            if (toObject.userListFullName.length > 0 && ![toObject.userListFullName isEqualToString:@"null"]) {
                if (listReceivers.count >1) {
                    if(isEnglish){
                        cell.lblMessageTo.text = [NSString stringWithFormat:@"%@...",toObject.userListFullName];
                    } else {
                        cell.lblMessageTo.text = [NSString stringWithFormat:@"%@...",toObject.userListFullNameLang];
                    }
                } else {
                    if(isEnglish){
                        cell.lblMessageTo.text = toObject.userListFullName;
                    } else {
                        cell.lblMessageTo.text = toObject.userListFullNameLang;
                    }
                }
            }
        }
        cell.lblMessageTopic.text = messageObject.message_topic;
    }
    
    if (messageObject.message_status == MESSAGE_READ) {
        // DON'T highlight
        [cell.lblMessageDate setTextColor:[UIColor lightGrayColor]];
        [cell.lblMessageTo setTextColor:NORMAL_COLOR];
        [cell.lblMessageTopic setTextColor:[UIColor lightGrayColor]];
    } else {
        // HIGHLIGHT with unread message
        [cell.lblMessageDate setTextColor:[UIColor blackColor]];
        [cell.lblMessageTo setTextColor:HIGHLIGHT_COLOR];
        [cell.lblMessageTopic setTextColor:[UIColor blackColor]];
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [[HomeViewV2 shareHomeView] waitingLoadingData];
    
    AdhocMessageDetailV2 *messageDetailView = [[AdhocMessageDetailV2 alloc] initWithNibName:@"AdhocMessageDetailV2" bundle:Nil];
    messageDetailView.delegate=self;
    messageDetailView.messageObject = (MessageModelV2 *)[self.arrayDataTable objectAtIndex:indexPath.row];
    if (isInBox) {
        messageDetailView.isSentMessage = NO;
        messageDetailView.isForwardMessage = NO;
        messageDetailView.isForOneMsg = TRUE;
        messageDetailView.messageObjectOfRePly = [arrayDataTable objectAtIndex:indexPath.row];
    } else if(isOutBox) {
        messageDetailView.isForwardMessage = NO;
        messageDetailView.isSentMessage = YES;
        NSMutableArray *messageReceiverList = nil;
        if(isDemoMode)
            messageReceiverList = [arrayReceiversTable objectForKey:[NSNumber numberWithInteger:messageDetailView.messageObject.message_group_id]];
        else
            messageReceiverList = [arrayReceivers objectForKey:[NSNumber numberWithInteger:messageDetailView.messageObject.message_id]];
        messageDetailView.messageReceiverList = messageReceiverList;
    }
    
    [self.navigationController pushViewController:messageDetailView animated:NO];
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    if(isDemoMode)
        [self updateUnreadMessage];
    else
        [NSThread detachNewThreadSelector:@selector(updateUnreadMessage) toTarget:self withObject:nil];

    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
}
- (void)updateUnreadMessage {
    [[HomeViewV2 shareHomeView] updateUnreadMessage];
}

- (void)tableView:(UITableView *)aTableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath 
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title: [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_delete_confirmation] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles:[[LanguageManagerV2 sharedLanguageManager] getNo], nil];   
         selectedIndex=indexPath.row;
        [alert setTag:tagDeletePhoto];
        [alert show];
    } 	
}

#pragma mark - UISearchBar Delegate Methods

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
  // only show the status bars cancel button while in edit mode
  
      self.hideSearchBtn=[[UIButton alloc] initWithFrame:CGRectMake(0, 0, 320,340)];
      [self.hideSearchBtn addTarget:self action:@selector(exitSearchBtn:) forControlEvents:UIControlEventTouchUpInside];
      [self.hideSearchBtn setBackgroundColor:[UIColor clearColor]];
      [self.view addSubview:hideSearchBtn];
      searchBarMessage.showsCancelButton = NO;
  
      searchBarMessage.autocorrectionType = UITextAutocorrectionTypeNo;
      isSearchBox = NO;
  }
  
  
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    [self.arrayDataTable removeAllObjects];// remove all data that belongs to previous search
    isSearchBox = YES;
    if(isDemoMode){
        arrayReceiversTable = [[NSMutableDictionary alloc] init];
    }
    if(searchText==nil || searchText.length==0)
    {
        if(isDemoMode){
            arrayReceiversTable = arrayReceivers;
            [tbvContent reloadData];
        }
        [self.arrayDataTable addObjectsFromArray:arrayDataSource];  
        [tbvContent reloadData];
        return;
    }
    
    if (isInBox) {
        // search in inbox
        for(MessageModelV2 *aMessage in self.arrayDataSource){
            BOOL find = FALSE;
            
            // Search in sent user
            NSArray* subNames = [aMessage.message_from_user_name componentsSeparatedByString:@" "];
            for (NSString* aSub in subNames) {
                NSRange r = [[aSub lowercaseString] rangeOfString:[searchText lowercaseString]];
                if (r.location != NSNotFound && r.location == 0) {
                    find = TRUE;
                    break;
                }
            }
            
            if(!find){
                // Search in login user
                NSArray* logNames = [aMessage.message_from_user_login_name componentsSeparatedByString:@" "];
                for (NSString* aSub in logNames) {
                    NSRange r = [[aSub lowercaseString] rangeOfString:[searchText lowercaseString]];
                    if (r.location != NSNotFound && r.location == 0) {
                        find = TRUE;
                        break;
                    }
                }
            }
            
            // Search in subject
            if (!find) {
                NSArray* subNames = [aMessage.message_topic componentsSeparatedByString:@" "];
                for (NSString* aSub in subNames) {
                    NSRange r = [[aSub lowercaseString] rangeOfString:[searchText lowercaseString]];
                    if (r.location != NSNotFound && r.location == 0) {
                        find = TRUE;
                        break;
                    }
                }
            }
            
            // Search in content
            if (!find) {
                NSArray* subNames = [aMessage.message_content componentsSeparatedByString:@" "];
                for (NSString* aSub in subNames) {
                    NSRange r = [[aSub lowercaseString] rangeOfString:[searchText lowercaseString]];
                    if (r.location != NSNotFound && r.location == 0) {
                        find = TRUE;
                        break;
                    }
                }
            }
            
            // Add to result if found
            if (find) {
                [arrayDataTable addObject:aMessage];
            }
        }
    }
    else {
        // Search in outbox
        for(MessageModelV2 *aMessage in self.arrayDataSource) {
            BOOL find = FALSE;
            NSMutableArray *listUserReceivers = nil;
            if(isDemoMode)
                listUserReceivers = [arrayReceivers objectForKey:[NSNumber numberWithInt:(int)aMessage.message_group_id]];
            else
                listUserReceivers = [arrayReceivers objectForKey:[NSNumber numberWithInt:(int)aMessage.message_id]];
            
            // Search in receiver list
            for (UserListModelV2* aReceiver in listUserReceivers) {
                NSArray* subNames = [aReceiver.userListFullName componentsSeparatedByString:@" "];
                for (NSString* aSub in subNames) {
                    NSRange r = [[aSub lowercaseString] rangeOfString:[searchText lowercaseString]];
                    if (r.location != NSNotFound && r.location == 0) {
                        find = TRUE;
                        break;
                    }
                }
                
            }
            
            // Search in subject
            if (!find) {
                NSArray* subNames = [aMessage.message_topic componentsSeparatedByString:@" "];
                for (NSString* aSub in subNames) {
                    NSRange r = [[aSub lowercaseString] rangeOfString:[searchText lowercaseString]];
                    if (r.location != NSNotFound && r.location == 0) {
                        find = TRUE;
                        break;
                    }
                }
            }
            
            // Search in content
            if (!find) {
                NSArray* subNames = [aMessage.message_content componentsSeparatedByString:@" "];
                for (NSString* aSub in subNames) {
                    NSRange r = [[aSub lowercaseString] rangeOfString:[searchText lowercaseString]];
                    if (r.location != NSNotFound && r.location == 0) {
                        find = TRUE;
                        break;
                    }
                }
            }
            
            // Add to result if found
            if (find) {
                if(!isDemoMode){
                    [arrayDataTable addObject:aMessage];
                }
                else{
                    if(![arrayReceiversTable objectForKey:[NSNumber numberWithInt:(int)aMessage.message_group_id]])
                    {
                        [arrayReceiversTable setObject:listUserReceivers forKey:[NSNumber numberWithInteger:aMessage.message_group_id]];
                        [arrayDataTable addObject:aMessage];
                    }
                }
            }
        }
    }
    [tbvContent reloadData]; 
}
  
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self.arrayDataTable removeAllObjects];
    isSearchBox=YES; 
    [searchBarMessage resignFirstResponder];
    if(searchBar.text==nil || searchBar.text.length==0)
    {
        [self.arrayDataTable removeAllObjects];
        [self.arrayDataTable addObjectsFromArray:arrayDataSource];
        [tbvContent reloadData];
    }
    if (isInBox) {
        // search in inbox
        for(MessageModelV2 *aMessage in self.arrayDataSource){
            BOOL find = FALSE;
            
            // Search in sent user
            NSArray* subNames = [aMessage.message_from_user_name componentsSeparatedByString:@" "];
            for (NSString* aSub in subNames) {
                NSRange r = [[aSub lowercaseString] rangeOfString:[searchBar.text lowercaseString]];
                if (r.location != NSNotFound && r.location == 0) {
                    find = TRUE;
                    break;
                }
            }
            
            if(!find){
                // Search in login user
                NSArray* logNames = [aMessage.message_from_user_login_name componentsSeparatedByString:@" "];
                for (NSString* aSub in logNames) {
                    NSRange r = [[aSub lowercaseString] rangeOfString:[searchBar.text lowercaseString]];
                    if (r.location != NSNotFound && r.location == 0) {
                        find = TRUE;
                        break;
                    }
                }
            }
            
            // Search in subject
            if (!find) {
                NSArray* subNames = [aMessage.message_topic componentsSeparatedByString:@" "];
                for (NSString* aSub in subNames) {
                    NSRange r = [[aSub lowercaseString] rangeOfString:[searchBar.text lowercaseString]];
                    if (r.location != NSNotFound && r.location == 0) {
                        find = TRUE;
                        break;
                    }
                }
            }
            
            // Search in content
            if (!find) {
                NSArray* subNames = [aMessage.message_content componentsSeparatedByString:@" "];
                for (NSString* aSub in subNames) {
                    NSRange r = [[aSub lowercaseString] rangeOfString:[searchBar.text lowercaseString]];
                    if (r.location != NSNotFound && r.location == 0) {
                        find = TRUE;
                        break;
                    }
                }
            }
            
            // Add to result if found
            if (find) {
                [arrayDataTable addObject:aMessage];
            }
        }
    }
    else {
        // Search in outbox
        for(MessageModelV2 *aMessage in self.arrayDataSource) {
            BOOL find = FALSE;
            NSMutableArray *listUserReceivers = nil;
            if(isDemoMode)
                listUserReceivers = [arrayReceivers objectForKey:[NSNumber numberWithInteger:aMessage.message_group_id]];
            else
                listUserReceivers = [arrayReceivers objectForKey:[NSNumber numberWithInteger:aMessage.message_id]];
            
            // Search in receiver list
            for (UserListModelV2* aReceiver in listUserReceivers) {
                NSArray* subNames = [aReceiver.userListFullName componentsSeparatedByString:@" "];
                for (NSString* aSub in subNames) {
                    NSRange r = [[aSub lowercaseString] rangeOfString:[searchBar.text lowercaseString]];
                    if (r.location != NSNotFound && r.location == 0) {
                        find = TRUE;
                        break;
                    }
                }
                
                if(!find){
                    subNames = [aReceiver.userListFullNameLang componentsSeparatedByString:@" "];
                    for (NSString* aSub in subNames) {
                        NSRange r = [[aSub lowercaseString] rangeOfString:[searchBar.text lowercaseString]];
                        if (r.location != NSNotFound && r.location == 0) {
                            find = TRUE;
                            break;
                        }
                    }
                }
                
                if(!find){
                    subNames = [aReceiver.userListName componentsSeparatedByString:@" "];
                    for (NSString* aSub in subNames) {
                        NSRange r = [[aSub lowercaseString] rangeOfString:[searchBar.text lowercaseString]];
                        if (r.location != NSNotFound && r.location == 0) {
                            find = TRUE;
                            break;
                        }
                    }
                }
            }
            
            // Search in subject
            if (!find) {
                NSArray* subNames = [aMessage.message_topic componentsSeparatedByString:@" "];
                for (NSString* aSub in subNames) {
                    NSRange r = [[aSub lowercaseString] rangeOfString:[searchBar.text lowercaseString]];
                    if (r.location != NSNotFound && r.location == 0) {
                        find = TRUE;
                        break;
                    }
                }
            }
            
            // Search in content
            if (!find) {
                NSArray* subNames = [aMessage.message_content componentsSeparatedByString:@" "];
                for (NSString* aSub in subNames) {
                    NSRange r = [[aSub lowercaseString] rangeOfString:[searchBar.text lowercaseString]];
                    if (r.location != NSNotFound && r.location == 0) {
                        find = TRUE;
                        break;
                    }
                }
            }
            
            // Add to result if found
            if (find) {
                [arrayDataTable addObject:aMessage];
            }
        }
    }
    
    
    [tbvContent reloadData];
    
    [searchBar setShowsCancelButton:NO animated:NO];
    [searchBar resignFirstResponder];
    if (hideSearchBtn) {
        [hideSearchBtn removeFromSuperview];
    }
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    // if a valid search was entered but the user wanted to cancel, bring back the main list content
    [self.arrayDataTable removeAllObjects];
    
    [self.arrayDataTable addObjectsFromArray:arrayDataSource];
    
    [tbvContent reloadData];
    [searchBarMessage setShowsCancelButton:NO animated:NO];
    [searchBarMessage resignFirstResponder];
    
    
    searchBarMessage.text = @"";
    
}

#pragma mark - Inbox/Outbox handler

-(IBAction)selectInboxTab:(id)sender
{
    // temporaty disable the table view to avoid crashing
//    tbvContent.scrollEnabled = NO;
    if (isInBox && sender)
        return;
    isOutBox=NO;
    isInBox=YES;
    if (!HUD) {
        HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    }
    [self.view.superview addSubview:HUD];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getRaLoadingData]];
    [HUD show:YES];
    //[NSThread detachNewThreadSelector:@selector(showTitleBox) toTarget:self withObject:nil];
    //Delete old message
    int userId = [UserManagerV2 sharedUserManager].currentUser.userId;
    int messagePeriod = [[UserManagerV2 sharedUserManager].currentUserAccessRight getMessagePeriod];
    NSString *lastModified = [ehkConvert dateStringAgoWithPeriodDays:messagePeriod];
    NSString *lastModifiedDate = [ehkConvert commonDateRemoveTime:lastModified];
    if (!isDemoMode)
        [[AdHocMessageManagerV2 sharedAdHocMessageManagerV2] deleteMessageByOwnerId:userId];
    
    //Check and load userlist before getting message
    [[HomeViewV2 shareHomeView] loadUserListByCurrentUser];
    
    // Call new API for get sent/received ad hoc message - discussion with FM on 20151007
    [[AdHocMessageManagerV2 sharedAdHocMessageManagerV2] getReceivedAdHocMessageRoutineByUserId:userId groupMessageId:0 hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId lastModified:lastModifiedDate];
    [[AdHocMessageManagerV2 sharedAdHocMessageManagerV2] getSendAdHocMessageRoutineByUserId:userId groupMessageId:0 hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId lastModified:lastModifiedDate];
    
    [self showTitleBox];
    
    [HUD hide:YES];
    [HUD removeFromSuperview];
}
-(IBAction)selectOutboxTab:(id)sender
{
    // temporaty disable the table view to avoid crashing
    if (isOutBox) {
        return;
    }
    
    isInBox = NO;
    isOutBox = YES;
    if (!HUD) {
        HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    }
    
    [self.view.superview addSubview:HUD];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getRaLoadingData]];
    [HUD show:YES];
    
    //Hao Tran Remove
    //[NSThread detachNewThreadSelector:@selector(showTitleBox) toTarget:self withObject:nil];
    //Delete old message
    int userId = [UserManagerV2 sharedUserManager].currentUser.userId;
    int messagePeriod = [[UserManagerV2 sharedUserManager].currentUserAccessRight getMessagePeriod];
    NSString *lastModified = [ehkConvert dateStringAgoWithPeriodDays:messagePeriod];
    NSString *lastModifiedDate = [ehkConvert commonDateRemoveTime:lastModified];
    if (!isDemoMode)
        [[AdHocMessageManagerV2 sharedAdHocMessageManagerV2] deleteMessageByOwnerId:userId];
    
    //Check and load userlist before getting message
    [[HomeViewV2 shareHomeView] loadUserListByCurrentUser];
    
    // Call new API for get sent/received ad hoc message - discussion with FM on 20151007
    [[AdHocMessageManagerV2 sharedAdHocMessageManagerV2] getReceivedAdHocMessageRoutineByUserId:userId groupMessageId:0 hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId lastModified:lastModifiedDate];
    [[AdHocMessageManagerV2 sharedAdHocMessageManagerV2] getSendAdHocMessageRoutineByUserId:userId groupMessageId:0 hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId lastModified:lastModifiedDate];

    [self showTitleBox];
    
    [HUD hide:YES];
    [HUD removeFromSuperview];
}

-(void)loadDataToOutBox
{
    if(self.arrayDataTable == nil) {
        self.arrayDataTable = [[NSMutableArray alloc] init];
    } else {
        [self.arrayDataTable removeAllObjects];
    }
    
    if(self.arrayDataSource == nil){
        self.arrayDataSource = [[NSMutableArray alloc] init];
    } else {
        [self.arrayDataSource removeAllObjects];
    }
    
    if(arrayReceivers == nil){
        arrayReceivers = [[NSMutableDictionary alloc] init];
    }
    else{
        [arrayReceivers removeAllObjects];
    }
    
    if(arrayReceiversTable == nil){
        arrayReceiversTable = [[NSMutableDictionary alloc] init];
    }
    else{
        [arrayReceiversTable removeAllObjects];
    }
    
    AdHocMessageManagerV2 *messageManager = [AdHocMessageManagerV2 sharedAdHocMessageManagerV2];
    
    self.arrayDataTable = [[NSMutableArray alloc] init];
    self.arrayDataSource = [[NSMutableArray alloc] init];
    
    NSMutableArray *messageOutBox = [[AdHocMessageManagerV2 sharedAdHocMessageManagerV2] loadMessageSentByOwnerId:[UserManagerV2 sharedUserManager].currentUser.userId];
    //Load User Detail of Message Receivers
    if (!isDemoMode) {
        for (MessageModelV2 *curMessage in messageOutBox) {
            NSMutableArray *listReceivers = [messageManager loadMessageReceiversByOwnerId:[UserManagerV2 sharedUserManager].currentUser.userId groupId:(int)curMessage.message_group_id];
            if (listReceivers != nil) {
                [arrayReceivers setObject:listReceivers forKey:[NSNumber numberWithInt:(int)curMessage.message_id]];
            }
        }
    } else {
        NSMutableArray *messageOutBox = [[AdHocMessageManagerV2 sharedAdHocMessageManagerV2]  loadMessageSentByOwnerId:[UserManagerV2 sharedUserManager].currentUser.userId];
        for (MessageModelV2 *curMessage in messageOutBox) {
            NSMutableArray *listReceivers = [[[AdHocMessageManagerV2 alloc] init] loadMessageReceiversByOwnerId:[UserManagerV2 sharedUserManager].currentUser.userId groupId:(int)curMessage.message_group_id];
            if(listReceivers != nil){
                if(![arrayReceiversTable objectForKey:[NSNumber numberWithInt:(int)curMessage.message_group_id]]){
                    [arrayReceiversTable setObject:listReceivers forKey:[NSNumber numberWithInt:(int)curMessage.message_group_id]];
                    [self.arrayDataSource addObject:curMessage];
                }
                if(![arrayReceivers objectForKey:[NSNumber numberWithInt:(int)curMessage.message_group_id]]){
                    [arrayReceivers setObject:listReceivers forKey:[NSNumber numberWithInt:(int)curMessage.message_group_id]];
                }
            }
        }
    }
    if (!isDemoMode) {
        [self.arrayDataSource addObjectsFromArray:messageOutBox];
    }
    [self.arrayDataTable addObjectsFromArray:arrayDataSource];
    
}

-(void)loadDataToInBox
{
    if(self.arrayDataTable == nil) {
        self.arrayDataTable = [[NSMutableArray alloc] init];
    } else {
        [self.arrayDataTable removeAllObjects];
    }
    
    if(self.arrayDataSource == nil){
        self.arrayDataSource = [[NSMutableArray alloc] init];
    } else {
        [self.arrayDataSource removeAllObjects];
    }
    
    NSMutableArray *messageInbox = [[AdHocMessageManagerV2 sharedAdHocMessageManagerV2] loadMessageReceivedByOwnerId:[UserManagerV2 sharedUserManager].currentUser.userId];
    
    [self.arrayDataSource addObjectsFromArray:messageInbox];
    [self.arrayDataTable addObjectsFromArray:arrayDataSource];
}

#pragma mark- hidden after save.
-(void)hudWasHidden
{
    if (HUD !=nil){
		[HUD removeFromSuperview];
    }
}

#pragma mark-AdhocMEssageDetailV2Delegate
-(void)backButtonProcess:(BOOL)isForwardMessage

{
    isInBox=isForwardMessage;
    isOutBox=!isForwardMessage;
//    [self showTitleBox];
    if (!isSearchBox)
    {
        [self showTitleBox];
    }
}
#pragma mark -  Hidden Header view when tap on navigationBar

// hidden header when tap navigationBar
#define heightScale 45
- (void) hiddenHeaderView {
    
    BOOL isHidden = topBarView.hidden;
    [topBarView setHidden:!isHidden];
    
    CGRect frame1 = CGRectZero;
    CGRect frame2 = CGRectZero;
    CGRect frame3 = CGRectZero;
    CGRect frame4 = CGRectZero;
    CGRect frame5 = CGRectZero;
    CGRect frame6 = CGRectZero;
    CGRect frame7 = CGRectZero;
    CGRect frame8 = CGRectZero;

    if(!isHidden){
        frame1  = CGRectMake(searchBarMessage.frame.origin.x, searchBarMessage.frame.origin.y - heightScale, searchBarMessage.frame.size.width, searchBarMessage.frame.size.height);
        frame2  = CGRectMake(btnInBoxTab.frame.origin.x, btnInBoxTab.frame.origin.y - heightScale, btnInBoxTab.frame.size.width, btnInBoxTab.frame.size.height);
        frame3  = CGRectMake(btnOutBoxTab.frame.origin.x, btnOutBoxTab.frame.origin.y - heightScale, btnOutBoxTab.frame.size.width, btnOutBoxTab.frame.size.height);
        frame4  = CGRectMake(lblInBox.frame.origin.x, lblInBox.frame.origin.y - heightScale, lblInBox.frame.size.width, lblInBox.frame.size.height);
        frame5  = CGRectMake(lblOutBox.frame.origin.x, lblOutBox.frame.origin.y - heightScale, lblOutBox.frame.size.width, lblOutBox.frame.size.height);
        frame6  = CGRectMake(lblTime.frame.origin.x, lblTime.frame.origin.y - heightScale, lblTime.frame.size.width, lblTime.frame.size.height);
        frame7  = CGRectMake(lblAddress.frame.origin.x, lblAddress.frame.origin.y - heightScale, lblAddress.frame.size.width, lblAddress.frame.size.height);
        frame8  = CGRectMake(tbvContent.frame.origin.x, tbvContent.frame.origin.y - heightScale, tbvContent.frame.size.width, tbvContent.frame.size.height+heightScale);
        
    } else
    {
        frame1  = CGRectMake(searchBarMessage.frame.origin.x, searchBarMessage.frame.origin.y + heightScale, searchBarMessage.frame.size.width, searchBarMessage.frame.size.height);
        frame2  = CGRectMake(btnInBoxTab.frame.origin.x, btnInBoxTab.frame.origin.y + heightScale, btnInBoxTab.frame.size.width, btnInBoxTab.frame.size.height);

        frame3  = CGRectMake(btnOutBoxTab.frame.origin.x, btnOutBoxTab.frame.origin.y + heightScale, btnOutBoxTab.frame.size.width, btnOutBoxTab.frame.size.height);
        frame4  = CGRectMake(lblInBox.frame.origin.x, lblInBox.frame.origin.y + heightScale, lblInBox.frame.size.width, lblInBox.frame.size.height);
        frame5  = CGRectMake(lblOutBox.frame.origin.x, lblOutBox.frame.origin.y + heightScale, lblOutBox.frame.size.width, lblOutBox.frame.size.height);
        frame6  = CGRectMake(lblTime.frame.origin.x, lblTime.frame.origin.y + heightScale, lblTime.frame.size.width, lblTime.frame.size.height);
        frame7  = CGRectMake(lblAddress.frame.origin.x, lblAddress.frame.origin.y + heightScale, lblAddress.frame.size.width, lblAddress.frame.size.height);
        frame8  = CGRectMake(tbvContent.frame.origin.x, tbvContent.frame.origin.y + heightScale, tbvContent.frame.size.width, tbvContent.frame.size.height-heightScale);

    }
    
    [searchBarMessage setFrame:frame1];
    [btnInBoxTab setFrame:frame2];
    [btnOutBoxTab setFrame:frame3];
    [lblInBox setFrame:frame4];
    [lblOutBox setFrame:frame5];
    [lblTime setFrame:frame6];
    [lblAddress setFrame:frame7];
    [tbvContent setFrame:frame8];
   
}
#pragma mark - UIAlertView Delegate Methods
-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    switch (alertView.tag) {
        case tagDeletePhoto:
        {
            switch (buttonIndex)
            {
                case 0:
                {
                    // YES
                    if ([arrayDataSource count] >= selectedIndex) {
                        
                        MessageModelV2 *deletedMessage=[arrayDataSource objectAtIndex:selectedIndex];
//                        BOOL isSucess = [[AdHocMessageManagerV2 sharedAdHocMessageManagerV2] updateWSMessageStatus:[UserManagerV2 sharedUserManager].currentUser.userId messageId:deletedMessage.message_id status:MESSAGE_DELETE];
                        BOOL isSucess = NO;
                        NSMutableArray *messageUpdateDetails = [[NSMutableArray alloc] init];
                        if (isInBox) {
                            [messageUpdateDetails addObject:deletedMessage];
                            isSucess = [[AdHocMessageManagerV2 sharedAdHocMessageManagerV2] postUpdateAdHocMessageRoutineWithUserId:[UserManagerV2 sharedUserManager].currentUser.userId messageUpdateDetails:messageUpdateDetails updateType:MessageUpdateType_ReceiverInbox];
                        } else if (isOutBox) {
                            messageUpdateDetails = [[AdHocMessageManagerV2 sharedAdHocMessageManagerV2] loadMessageSentByOwnerId:[UserManagerV2 sharedUserManager].currentUser.userId messageGroupId:(int)deletedMessage.message_group_id];
                            isSucess = [[AdHocMessageManagerV2 sharedAdHocMessageManagerV2] postUpdateAdHocMessageRoutineWithUserId:[UserManagerV2 sharedUserManager].currentUser.userId messageUpdateDetails:messageUpdateDetails updateType:MessageUpdateType_SenderOutbox];
                        }
                        
                        if (isSucess) {
                            [[AdHocMessageManagerV2 sharedAdHocMessageManagerV2] deleteMessageByOwnerId:[UserManagerV2 sharedUserManager].currentUser.userId messageGroupId:(int)deletedMessage.message_group_id];
                            [[AdHocMessageManagerV2 sharedAdHocMessageManagerV2] deleteMessageReceiverByOwnerId:[UserManagerV2 sharedUserManager].currentUser.userId messageGroupId:(int)deletedMessage.message_group_id];
                            [arrayDataSource removeObjectAtIndex:selectedIndex];
                            [arrayDataTable removeObjectAtIndex:selectedIndex];
                            if (isInBox) {
                                [self.lblInBox setText:[NSString stringWithFormat:@"%@(%d)", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_adhocMsg_inbox_title],(int)[arrayDataTable count]]];
                                if (deletedMessage.message_status != MESSAGE_READ) {
                                    int countMessage = [[[UserManagerV2 sharedUserManager] getCurrentUserConfigValueByKey:USER_CONFIG_MESSAGE_UNREAD_RECEIVED] intValue];
                                    if (countMessage > 0) {
                                        countMessage --;
                                    }
                                    [[UserManagerV2 sharedUserManager] setCurrentUserConfig:USER_CONFIG_MESSAGE_UNREAD_RECEIVED value:[NSString stringWithFormat:@"%d", countMessage]];
                                }
                                [[HomeViewV2 shareHomeView] updateUnreadMessage];
                            } else if(isOutBox) {
                                [self.lblOutBox setText:[NSString stringWithFormat:@"%@(%d)", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_sent_item],(int)[arrayDataTable count]]];
                            }
                        }
                    }
                }
                    break;
                case 1:
                {
                    //NO
                }
                default:
                    break;
            }
        }
            
            break;
        default:
            break;
    }
    [tbvContent reloadData];
}

-(void)exitSearchBtn:(id)sender
{
    [self.searchBarMessage setShowsCancelButton:NO animated:NO];
    [self.searchBarMessage resignFirstResponder];
    
    if (self.hideSearchBtn) {
        [self.hideSearchBtn removeFromSuperview];
    }
}

#pragma mark - iOS 7 Resources
-(void)loadFlatResource
{
    [btnInBoxTab setBackgroundImage:[UIImage imageNamed:imgActiveMessageFlat] forState:UIControlStateNormal];
    [btnOutBoxTab setBackgroundImage:[UIImage imageNamed:imgActiveMessageFlat] forState:UIControlStateNormal];
}

@end
