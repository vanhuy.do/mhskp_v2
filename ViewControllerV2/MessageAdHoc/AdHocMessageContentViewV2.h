//
//  AdHocMessageContentViewV2.h
//  mHouseKeeping
//
//  Created by TMS on 2/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIPlaceHolderTextView.h"

@protocol AdHocMessageContentViewV2Delegate <NSObject>
@optional
-(void) messageViewV2DoneWithText:(NSString *) text;

@end
@interface AdHocMessageContentViewV2 : UIViewController <UITextViewDelegate>
{
    __unsafe_unretained id<AdHocMessageContentViewV2Delegate>delegate;
    NSString *contentMessage;
    BOOL contentIsChanged;
}
@property (nonatomic,assign)  id<AdHocMessageContentViewV2Delegate>delegate;
@property (strong, nonatomic) IBOutlet UINavigationBar *nvgBar;
@property (strong, nonatomic) IBOutlet UIPlaceHolderTextView *txvMessage;
@property (strong, nonatomic) IBOutlet UIButton *btnOK;

@property (nonatomic,strong) NSString *contentMessage;
@property (nonatomic) BOOL isOnlyReadMessage;
@property (nonatomic) BOOL isFromContent;

- (IBAction)ExitWithView:(id)sender;
- (void) setCaptionsView;
- (IBAction)ExitKeyBoard:(id)sender;
-(void)messageSaveDidPressed;
@end
