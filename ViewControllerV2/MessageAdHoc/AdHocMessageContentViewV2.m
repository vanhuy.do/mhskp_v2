//
//  AdHocMessageContentViewV2.m
//  mHouseKeeping
//
//  Created by TMS on 2/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AdHocMessageContentViewV2.h"
#import "LanguageManager.h"
#import "QuartzCore/QuartzCore.h"
#import "ehkDefinesV2.h"
#import "UIImage+Tint.h"
#import "DeviceManager.h"

#define isDemoMode ([[NSUserDefaults standardUserDefaults] integerForKey:ENABLE_DEMO_MODE] == 1)

@implementation AdHocMessageContentViewV2
@synthesize delegate,txvMessage,btnOK,nvgBar;
@synthesize contentMessage;
@synthesize isOnlyReadMessage, isFromContent;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        isOnlyReadMessage = NO;
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle
-(void)viewWillAppear:(BOOL)animated
{
    //set selected tab message
    [[HomeViewV2 shareHomeView] setSelectedButton:tagOfMessageButton];
    
    //hide wifi view
    [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationHideWifiView] object:nil];
    
    [self setCaptionsView];
    [super viewWillAppear:animated];
    
}

-(void)keyboardDidHide:(NSNotification*)notification
{
    [self.navigationController popViewControllerAnimated:YES];
    if(!isOnlyReadMessage)
    {
        if (contentIsChanged && [delegate respondsToSelector:@selector(messageViewV2DoneWithText:)]) {
            [delegate messageViewV2DoneWithText:txvMessage.text];
        }
    }
}

- (void)viewDidLoad
{
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        [self loadFlatResource];
    }
    
    if(isOnlyReadMessage)
    {
        //[txvMessage setUserInteractionEnabled:NO];
        [txvMessage setEditable:NO];
        [txvMessage resignFirstResponder];
    }
    else
    {
        [self.txvMessage becomeFirstResponder];
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardDidHide:)name:UIKeyboardDidHideNotification object:nil];
    }
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.view setBackgroundColor:[UIColor colorWithWhite:0.1 alpha:0.1]];
    self.navigationController.navigationBarHidden = YES;
    [self.navigationItem setHidesBackButton:YES animated:NO];
     
     
    UIView *parentView = nil;
     
    parentView = self.parentViewController.view;
     
    UIGraphicsBeginImageContext(parentView.bounds.size);
    [parentView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *parentViewImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
     
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:self.view.bounds];
    imgView.image = parentViewImage;
    [self.view insertSubview:imgView atIndex:0];
    txvMessage.placeholder = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_adhocMsg_message_content];
    if (contentMessage && ![contentMessage isEqualToString:@""]) {
        txvMessage.text=contentMessage;
    }
    [btnOK setTitle:[L_OK currentKeyToLanguage] forState:UIControlStateNormal];
}

-(void)viewWillDisappear:(BOOL)animated {
    //show wifi view
    if(!isDemoMode){
        [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationShowWifiView] object:nil];
    }
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    self.navigationController.navigationBarHidden = NO;
    [super viewWillDisappear:animated];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
#pragma Methods Process
- (IBAction)ExitWithView:(id)sender {
    self.navigationController.navigationBarHidden = NO;
    
    [self.navigationController popViewControllerAnimated:YES];
    
    if(!isOnlyReadMessage)
    {
        if (contentIsChanged && [delegate respondsToSelector:@selector(messageViewV2DoneWithText:)]) {
            [delegate messageViewV2DoneWithText:txvMessage.text];
        }
    }
    //[self.view removeFromSuperview];
}

-(void)setCaptionsView {
//    [self.nvgBar.topItem setTitle:[[LanguageManagerV2 sharedLanguageManager] getMESSAGEDETAIL]];
    if(isFromContent == YES)
    {
        [self.nvgBar.topItem setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_adhocMsg_message_content_title_navigation]];
    }
    else{
        [self.nvgBar.topItem setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_adhocMsg_message_contacts_title_navigation]];
    }
}

-(void)messageSaveDidPressed
{

}

- (IBAction)ExitKeyBoard:(id)sender {
    [self.txvMessage resignFirstResponder];
}

-(void)remarkSaveDidPressed {
    if ([delegate respondsToSelector:@selector(messageViewV2DoneWithText:)]) {
        [delegate messageViewV2DoneWithText:txvMessage.text];
    }
}

- (void)textViewDidChange:(UITextView *)textView {
    contentIsChanged = TRUE;
}

//-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
//{
//    if ([text rangeOfCharacterFromSet:[NSCharacterSet newlineCharacterSet]].location==NSNotFound) {
//
//        return YES;
//    }
//    [txvMessage resignFirstResponder];
//    [self.navigationController popViewControllerAnimated:YES];
//    if ([delegate respondsToSelector:@selector(remarkViewV2DoneWithText:)]) {
//        [delegate remarkViewV2DoneWithText:txvMessage.text];
//    }
//    return NO;
//}

#pragma mark - iOS 7 Resources
-(void)loadFlatResource
{
    [btnOK setBackgroundImage:[UIImage imageNamed:imgYesBtnFlat] forState:UIControlStateNormal];
}

@end
