//
//  AdHocMessageSubViewV2.h
//  mHouseKeeping
//
//  Created by TMS on 3/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AdHocMessageSubjectHeaderV2.h"
#import "AdHocMessageSubjectSectionV2.h"
#import "AdHocSubjectTableViewCellV2.h"
#import "MessageSubjectModelV2.h"
#import "AdHocMessageManagerV2.h"
#import "TopbarViewV2.h"
#import "CommonVariable.h"
#import "AccessRight.h"
#import "MBProgressHUD.h"

@protocol AdHocMessageSubViewV2Delegate<NSObject>

@optional
-(void)selectItem: (MessageSubjectItemModelV2 *)itemTemp ;
@end
@interface AdHocMessageSubViewV2 : UIViewController<UITableViewDataSource,UITableViewDelegate,AdHocMessageSubjectHeaderV2Delegate,UISearchBarDelegate>
{
    
    IBOutlet UITableView *subjectTableView;
    IBOutlet UISearchBar * search;
    IBOutlet UILabel * searchTitle;
    IBOutlet UIImageView  *imageview ;
    UIView * backgroundSearchBar;
    TopbarViewV2 * topBarView;
    __unsafe_unretained id <AdHocMessageSubViewV2Delegate> delegate;
    NSMutableArray *sectionInfoArray;
    NSMutableArray *sectionDataSource;
       BOOL isListDown;
    MessageSubjectItemModelV2 *selectedItem;
    AccessRight *messages;
    
    MBProgressHUD *HUD;
    UIButton *hideSearchBtn;
}
@property (nonatomic,strong)UIButton *hideSearchBtn;
@property (nonatomic,strong)TopbarViewV2 * topBarView;
@property (nonatomic,strong) UIView * backgroundSearchBar;
@property (nonatomic,strong) IBOutlet UISearchBar * search;
@property (nonatomic,strong) IBOutlet UILabel * searchTitle;
@property (nonatomic,strong) IBOutlet UIImageView  *imageview;
@property (nonatomic,strong) IBOutlet UITableView *subjectTableView;
@property (nonatomic,assign)id <AdHocMessageSubViewV2Delegate> delegate;

@property (nonatomic,strong)NSMutableArray *sectionDataSource;
@property (nonatomic,strong)NSMutableArray *sectionInfoArray;
@property (nonatomic,strong)MessageSubjectItemModelV2 *selectedItem;
@property BOOL isListDown;
-(void)loadDataForTemplateCategory;
-(void)exitSearchBtn:(id)sender;

@end
