//
//  AdHocMessageSubViewV2.m
//  mHouseKeeping
//
//  Created by TMS on 3/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AdHocMessageSubViewV2.h"
#import "LanguageManagerV2.h"
#import "MyNavigationBarV2.h"
#import "DeviceManager.h"

#define BACKGROUNDIMAGE  @"bg.png"
#define sizeTitle           22
#define isDemoMode ([[NSUserDefaults standardUserDefaults] integerForKey:ENABLE_DEMO_MODE] == 1)

@implementation AdHocMessageSubViewV2
@synthesize subjectTableView,topBarView;
@synthesize isListDown;
@synthesize sectionInfoArray;
@synthesize delegate;
@synthesize selectedItem;
@synthesize search,searchTitle,imageview,backgroundSearchBar;
@synthesize sectionDataSource,hideSearchBtn;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - Load Access Right
-(void) loadAccessRights
{
    AccessRightModel *currentUserAccessRight = [[UserManagerV2 sharedUserManager] currentUserAccessRight];
    messages = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.message];
}

#pragma mark - View lifecycle
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    //set selected tab message
    [[HomeViewV2 shareHomeView] setSelectedButton:tagOfMessageButton];
    
    /*
    isListDown = NO;
    [search setPlaceholder:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_SEARCH]];
    [subjectTableView setBackgroundColor:[UIColor clearColor]];
    [subjectTableView reloadData];*/
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //Resize content views of current VC to fit with Navigation bar and tabbar
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    [self loadAccessRights];
    
    if (topBarView == nil) {
        topBarView = [[TopbarViewV2 alloc] initViewWithSuperModel:[CommonVariable sharedCommonVariable].superRoomModel];
        [self.view addSubview:topBarView];
    }
    UIButton *titleBarButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 230, 44)];
    titleBarButton.titleLabel.backgroundColor = [UIColor clearColor];
    [titleBarButton.titleLabel setFont: FONT_BUTTON_TOPBAR];
    titleBarButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    titleBarButton.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
    [titleBarButton.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
    [titleBarButton.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
    [titleBarButton setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_add_hoc_message] forState:UIControlStateNormal];
    [titleBarButton addTarget:self action:@selector(hiddenHeaderView) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.titleView = titleBarButton;
    if (titleBarButton.titleLabel.text.length >= 14) {
        titleBarButton.titleLabel.font = FONT_SMALL_BUTTON_TOPBAR;
    }
    
    //Move title to center vertical
    CGRect titleViewBounds = self.navigationItem.titleView.bounds;
    int moveLeftTitle = 0;
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        moveLeftTitle = 10;
        [self loadFlatResource];
    }
    
    self.navigationItem.titleView.bounds = CGRectMake(titleViewBounds.origin.x + moveLeftTitle, Y_TOPBAR_TITLE, titleViewBounds.size.width, titleViewBounds.size.height);
    
    //back button navigation
    self.navigationItem.leftBarButtonItem = [MyNavigationBarV2 createBackButtonWithTarget:self selector:@selector(backBarPressed) forControlEvents:UIControlEventTouchUpInside];
    
    HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [self.tabBarController.view addSubview:HUD];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getRaLoadingData]];
    [HUD show:YES];
    
    [self performSelector:@selector(loadData) withObject:nil afterDelay:0.0f];
}

-(void)loadData
{
    if(!isDemoMode)
        [[AdHocMessageManagerV2 sharedAdHocMessageManagerV2] getSubjectListWith:[UserManagerV2 sharedUserManager].currentUser.userId  andHotelID:[[[UserManagerV2 sharedUserManager] currentUser] userHotelsId] andLastModified:nil AndPercentView:nil];
    
    [self loadDataSuccess];
}

-(void)loadDataSuccess
{
    [HUD hide:YES];
    [HUD removeFromSuperview];
    
    // Do any additional setup after loading the view from its nib.
    UIColor *bgColor = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:BACKGROUNDIMAGE]];
    self.view.backgroundColor = bgColor;
    
    [imageview setBackgroundColor:[UIColor grayColor]];
    [searchTitle setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_adhocMsg_top_subject]];
    [search setPlaceholder:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_SEARCH]];
    NSArray *subviews = search.subviews;
    for (id subview in subviews) {
        if ([subview isKindOfClass:NSClassFromString(@"UISearchBarBackground")]) {
            UIView *sview = (UIView *) subview;
            UIView *bg = [[UIView alloc] initWithFrame:sview.frame];
            [bg setFrame:CGRectMake(sview.frame.origin.x, sview.frame.origin.y, 250, 44)];
            //  bg.backgroundColor = [UIColor colorWithRed:254.0/255.0 green:248/255.0 blue:215/255.0 alpha:1.0];
            bg.backgroundColor=[UIColor clearColor];
            [self.search insertSubview:bg aboveSubview:sview];
            self.backgroundSearchBar= bg;
            //            [bg release];
            [sview removeFromSuperview];
        }
    }
    
    subjectTableView.delegate=self;
    subjectTableView.dataSource=self;
    [self loadDataForTemplateCategory];
}

-(void)backBarPressed
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [sectionDataSource count];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    AdHocMessageSubjectSectionV2 *sectioninfo = [sectionDataSource objectAtIndex:section];
    NSInteger numberRowInSection = sectioninfo.rowHeights.count;
    //    return numberRowInSection;
    if (isListDown) {
        return numberRowInSection;
    }else{
        return sectioninfo.open ? numberRowInSection : 0;}
   
    
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (isListDown) {
        return 0;
    }
    else
    {
        return 60;
    }

}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70;
}


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
   
    if ([sectionDataSource count]<=section) {
        UIView *view1=[[UIView alloc] init];
        [view1 setBackgroundColor:[UIColor clearColor]];
        return view1;
    }
    else
    { 
        AdHocMessageSubjectSectionV2 *sectioninfo= [sectionDataSource objectAtIndex:section];
        
        if (sectioninfo.headerView == nil) {
            NSString *subjectName = nil;
            if ([[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString:(NSString *)ENGLISH_LANGUAGE]) {
                subjectName = sectioninfo.chkContentType.smcName;
            } else {
                subjectName = sectioninfo.chkContentType.smcNameLang;
            }
            
            AdHocMessageSubjectHeaderV2 *sview = [[AdHocMessageSubjectHeaderV2 alloc]initWithSection:section subjectName:subjectName subjectImage:sectioninfo.chkContentType.smcImage AndStatusArrow:NO] ;
            
            sectioninfo.headerView = sview;
            sview.delegate = self;
        }
        
        
        return sectioninfo.headerView;
    
    }
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *subjectRowIdentifier = @"subjectIdentifier";
    
    AdHocSubjectTableViewCellV2 *cell = nil;
    cell =(AdHocSubjectTableViewCellV2*) [tableView dequeueReusableCellWithIdentifier:subjectRowIdentifier];   
    
    if (cell==nil) 
    {
        NSArray *array = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([AdHocSubjectTableViewCellV2 class]) owner:self options:nil];
        cell = [array objectAtIndex:0];
        
    }
    AdHocMessageSubjectSectionV2 *sectionInfo;
    if (isListDown) {
          sectionInfo = [sectionDataSource objectAtIndex:0];
    } else {
        sectionInfo = [sectionDataSource objectAtIndex:indexPath.section];
    }
    
    MessageSubjectItemModelV2 *model = [sectionInfo.rowHeights objectAtIndex:indexPath.row];
    [cell.imgSubjectItemCell setImage:[UIImage imageWithData:model.smiImage]];
    
    if ([[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString:(NSString *)ENGLISH_LANGUAGE]) {
        cell.lblNameSubjectItemCell.text = model.smiName;
    } else {
        cell.lblNameSubjectItemCell.text = model.smiNameLang;
    }
    
    [cell setBackgroundColor:[UIColor clearColor]];
    [cell setIndexpath:indexPath]; 
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //Comment for uncheck access right
    //Check access right only view message
    //    if(messages.isAllowedView){
    //        return;
    //    }
    
    AdHocMessageSubjectSectionV2 *sectionInfo;
    if (isListDown) {
        sectionInfo = [sectionDataSource objectAtIndex:0];
    }else
    {
        sectionInfo = [sectionDataSource objectAtIndex:indexPath.section];
        
    }
    selectedItem = [sectionInfo.rowHeights objectAtIndex:indexPath.row];
    

    if ([self.delegate respondsToSelector:@selector(selectItem:)]) {
        [self.delegate selectItem:selectedItem];
    }
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - Section Header Delegate

-(void)sectionHeaderView:(AdHocMessageSubjectHeaderV2 *)sectionheaderView sectionOpened:(NSInteger)section
{
    if ([sectionDataSource count] <= section) {
        return;
    }
    else
    {
        AdHocMessageSubjectSectionV2 *sectionInfo = [sectionDataSource objectAtIndex:section];
        sectionInfo.open = YES;
        
        NSInteger countOfRowsToInsert = [sectionInfo.rowHeights count];
        NSMutableArray *indexPathsToInsert = [[NSMutableArray alloc] init];
        for (NSInteger i = 0; i < countOfRowsToInsert; i++) {
            [indexPathsToInsert addObject:[NSIndexPath indexPathForRow:i
                                                             inSection:section]];
        }
        
        UITableViewRowAnimation insertAnimation;
        insertAnimation = UITableViewRowAnimationTop;
        
        [self.subjectTableView beginUpdates];
        
        [self.subjectTableView insertRowsAtIndexPaths:indexPathsToInsert
                                     withRowAnimation:insertAnimation];
        [self.subjectTableView endUpdates];
        
        
        if (countOfRowsToInsert > 0) {
            [self.subjectTableView scrollToRowAtIndexPath:[indexPathsToInsert objectAtIndex:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
        }
        
        
        
//        [indexPathsToInsert release];
    }
    
}

-(void)sectionHeaderView:(AdHocMessageSubjectHeaderV2 *)sectionheaderView sectionClosed:(NSInteger)section
{
    if ([sectionDataSource count]<=section) {
        return;
    }
    else
    {
        AdHocMessageSubjectSectionV2 *sectionInfo = [sectionDataSource
                                                     objectAtIndex:section];
        
        sectionInfo.open = NO;
        NSInteger countOfRowsToDelete = [self.subjectTableView
                                         numberOfRowsInSection:section];
        
        if (countOfRowsToDelete > 0) {
            NSMutableArray *indexPathsToDelete = [[NSMutableArray alloc] init];
            for (NSInteger i = 0; i < countOfRowsToDelete; i++) {
                [indexPathsToDelete addObject:[NSIndexPath indexPathForRow:i
                                                                 inSection:section]];
            }
            
            [self.subjectTableView beginUpdates];
            [self.subjectTableView deleteRowsAtIndexPaths:indexPathsToDelete
                                         withRowAnimation:UITableViewRowAnimationTop];
            [self.subjectTableView endUpdates];
            
//            [indexPathsToDelete release];
            
        }

    }
    }
-(void)loadDataForTemplateCategory
{
    if (!sectionDataSource) {
        sectionDataSource=[[NSMutableArray alloc] init];
    }
    if (!sectionInfoArray) {
        sectionInfoArray=[[NSMutableArray alloc] init];
    }
    
    AdHocMessageManagerV2 *messageManager=[[AdHocMessageManagerV2 alloc] init];
    NSMutableArray *subjectArray=[messageManager loadAllMsgSubject];
    if ([subjectArray count]>0) {
        for (int i=0; i<[subjectArray count]; i++) {
            MessageSubjectModelV2 *subjectObj=[subjectArray objectAtIndex:i];
            NSMutableArray *itemArray=[messageManager loadAllMsgSubjectItemByMsgSubject:(int)subjectObj.smc];
            AdHocMessageSubjectSectionV2 *section1=[[AdHocMessageSubjectSectionV2 alloc] init];
            section1.chkContentType=subjectObj;
            for (int j=0; j<[itemArray count]; j++) {
                MessageSubjectItemModelV2 *itemObj=[itemArray objectAtIndex:j];
                [section1 insertObjectToNextIndex:itemObj];
            }
            [sectionInfoArray addObject:section1];
        }
        
    }else
    {
//        NSLog(@"do not thing");
    }
    [sectionDataSource addObjectsFromArray: sectionInfoArray];
    
    [subjectTableView reloadData];
}

#pragma mark - UISearchBar Delegate Methods

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    // only show the status bars cancel button while in edit mode
    self.hideSearchBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 320,340)];
    [self.hideSearchBtn addTarget:self action:@selector(exitSearchBtn:) forControlEvents:UIControlEventTouchUpInside];
    [self.hideSearchBtn setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:hideSearchBtn];
    
    isListDown = YES;
    search.showsCancelButton = NO;
    search.autocorrectionType = UITextAutocorrectionTypeNo;
    
    // flush the previous search content
    
//    [sectionDataSource removeAllObjects];
    
    
}


-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
    [sectionDataSource removeAllObjects];// remove all data that belongs to previous search
    
    if(searchText==nil || searchText.length==0)
    {
//        [sectionDataSource removeAllObjects];
        
        [sectionDataSource addObjectsFromArray:sectionInfoArray];
        isListDown=NO;
        @try{
            
            [subjectTableView reloadData];
            
        }
        
        @catch(NSException *e){
            
        }
        
        //[m_searchBar resignFirstResponder];
        
        //m_searchBar.text = @"";
        
//        [subjectTableView reloadData];
        
        //return;
        
    }else
    {
        isListDown=YES;
        AdHocMessageSubjectSectionV2 *msSubjectResutl=[[AdHocMessageSubjectSectionV2 alloc] init];
        for (AdHocMessageSubjectSectionV2 *msSubjectSection in sectionInfoArray) {
            NSMutableArray *itemArray=msSubjectSection.rowHeights;
            
            for( MessageSubjectItemModelV2*subjectItem in itemArray)
                
            {
                
//                NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];
                
                NSRange r = [[subjectItem.smiName lowercaseString] rangeOfString:[searchText lowercaseString]];
                
                if(r.location != NSNotFound)
                    
                {
                    
                    if(r.location== 0)//that is we are checking only the start of the names.
                        
                    {
                        
                        
                        [ msSubjectResutl insertObjectToNextIndex:subjectItem];
                        
                    }
                    
                }
                
                
//                [pool release];
                
            }
            
        }
        
        [sectionDataSource addObject:msSubjectResutl];
        [subjectTableView reloadData];
    }
      
    
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    
    // if a valid search was entered but the user wanted to cancel, bring back the main list content
    isListDown=NO;
    [sectionDataSource removeAllObjects];
    if(searchBar.text==nil || searchBar.text.length==0)
    {
        [sectionDataSource removeAllObjects];
        
        [sectionDataSource addObjectsFromArray:sectionInfoArray];
        isListDown=NO;
        @try{
            
            [subjectTableView reloadData];
            
        }
        
        @catch(NSException *e){
            
        }
        
        //[m_searchBar resignFirstResponder];
        
        //m_searchBar.text = @"";
        
        //        [subjectTableView reloadData];
        
        //return;
        
    }else
    {
        isListDown=YES;
        AdHocMessageSubjectSectionV2 *msSubjectResutl=[[AdHocMessageSubjectSectionV2 alloc] init];
        for (AdHocMessageSubjectSectionV2 *msSubjectSection in sectionInfoArray) {
            NSMutableArray *itemArray=msSubjectSection.rowHeights;
            
            for( MessageSubjectItemModelV2*subjectItem in itemArray)
                
            {
                
//                NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];
                
                NSRange r = [[subjectItem.smiName lowercaseString] rangeOfString:[searchBar.text lowercaseString]];
                
                if(r.location != NSNotFound)
                    
                {
                    
                    if(r.location== 0)//that is we are checking only the start of the names.
                        
                    {
                        
                        
                        [ msSubjectResutl insertObjectToNextIndex:subjectItem];
                        
                    }
                    
                }
                
            
                
//                [pool release];
                
            }
            
        }
        
        [sectionDataSource addObject:msSubjectResutl];
        [subjectTableView reloadData];
    }
    
    [search setShowsCancelButton:NO animated:NO];
    [search resignFirstResponder];
    if (hideSearchBtn) {
        [hideSearchBtn removeFromSuperview];
    }
    

    
}
-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    // if a valid search was entered but the user wanted to cancel, bring back the main list content
    isListDown=NO;
    [sectionDataSource removeAllObjects];
    
    [sectionDataSource addObjectsFromArray:sectionInfoArray];
    [subjectTableView reloadData];
    [search setShowsCancelButton:NO animated:NO];
    [search resignFirstResponder];
    
    
    search.text = @"";
}
#pragma mark -  Hidden Header view when tap on navigationBar

// hidden header when tap navigationBar
#define heightScale 45
- (void) hiddenHeaderView {
    
    BOOL isHidden = topBarView.hidden;
    [topBarView setHidden:!isHidden];
    
    CGRect frame1 = CGRectZero;
    CGRect frame2 = CGRectZero;
    CGRect frame3 = CGRectZero;
    CGRect frame4 = CGRectZero;
    if(!isHidden){
        frame1  = CGRectMake(subjectTableView.frame.origin.x, subjectTableView.frame.origin.y - heightScale, subjectTableView.frame.size.width, subjectTableView.frame.size.height+heightScale);
        frame2  = CGRectMake(search.frame.origin.x, search.frame.origin.y - heightScale, search.frame.size.width, search.frame.size.height);
        frame3  = CGRectMake(searchTitle.frame.origin.x, searchTitle.frame.origin.y - heightScale, searchTitle.frame.size.width, searchTitle.frame.size.height);
        frame4  = CGRectMake(imageview.frame.origin.x, imageview.frame.origin.y - heightScale, imageview.frame.size.width, imageview.frame.size.height);
    } else
    {
        frame1  = CGRectMake(subjectTableView.frame.origin.x, subjectTableView.frame.origin.y + heightScale, subjectTableView.frame.size.width, subjectTableView.frame.size.height-heightScale);
        frame2  = CGRectMake(search.frame.origin.x, search.frame.origin.y +heightScale, search.frame.size.width, search.frame.size.height);
        frame3  = CGRectMake(searchTitle.frame.origin.x, searchTitle.frame.origin.y + heightScale, searchTitle.frame.size.width, searchTitle.frame.size.height);
        frame4  = CGRectMake(imageview.frame.origin.x, imageview.frame.origin.y + heightScale, imageview.frame.size.width, imageview.frame.size.height);
        
    }
   

    [search setFrame:frame2];
    [searchTitle setFrame:frame3];
    [imageview setFrame:frame4];
    [subjectTableView setFrame:frame1];
   
    
}
-(void)exitSearchBtn:(id)sender
{
    isListDown=NO;
    
    [search setShowsCancelButton:NO animated:NO];
    [search resignFirstResponder];
    
    if (hideSearchBtn) {
        [hideSearchBtn removeFromSuperview];
    }
//    if ([sectionDataSource count]>0) {
//        return;    
//    }else
//    {
//        [sectionDataSource removeAllObjects];
//        
//        [sectionDataSource addObjectsFromArray:sectionInfoArray];
//        
//        @try{
//            
//            [subjectTableView reloadData];
//            
//        }
//        
//        @catch(NSException *e){
//            
//        }
//
//    }

}

#pragma mark - iOS 7 Resources
-(void)loadFlatResource
{
    [imageview setImage:[UIImage imageNamed:imgTopGrayFlat]];
}
@end
