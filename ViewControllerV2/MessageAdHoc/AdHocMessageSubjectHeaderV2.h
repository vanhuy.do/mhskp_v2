//
//  AdHocMessageSubjectHeaderV2.h
//  mHouseKeeping
//
//  Created by TMS on 2/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol AdHocMessageSubjectHeaderV2Delegate;
@interface AdHocMessageSubjectHeaderV2 : UIView

{
    UILabel *lblSubjectName;
    UIImageView *imgSubject;
    UIImageView *imgArrowSection;
    __unsafe_unretained id<AdHocMessageSubjectHeaderV2Delegate> delegate;
    UIButton *btnToggle;
    BOOL isToggle;
    NSInteger section;

}
@property (nonatomic,strong) UILabel *lblSubjectName;
@property (nonatomic,strong) UIImageView *imgSubject;
@property (nonatomic,strong) UIImageView *imgArrowSection;
@property (nonatomic,assign) id<AdHocMessageSubjectHeaderV2Delegate> delegate;
@property (nonatomic,strong) UIButton *btnToggle;
@property BOOL isToggle;
@property NSInteger section;
-(void)toggleOpenWithUserAction:(BOOL)userAction ;
-(id)initWithSection:(NSInteger)sectionIndex subjectName:(NSString *)subName subjectImage: (NSData*)subImage AndStatusArrow:(BOOL)isOpened;
@end

@protocol AdHocMessageSubjectHeaderV2Delegate<NSObject>
 @optional
 -(void) sectionHeaderView:(AdHocMessageSubjectHeaderV2*) sectionheaderView sectionOpened:(NSInteger) section;
 -(void) sectionHeaderView:(AdHocMessageSubjectHeaderV2*) sectionheaderView sectionClosed:(NSInteger) section;
 @end

