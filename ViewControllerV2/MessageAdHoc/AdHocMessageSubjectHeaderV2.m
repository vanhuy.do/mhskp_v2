//
//  AdHocMessageSubjectHeaderV2.m
//  mHouseKeeping
//
//  Created by TMS on 2/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AdHocMessageSubjectHeaderV2.h"
#import "ehkDefinesV2.h"
#import "UIImage+Tint.h"

@implementation AdHocMessageSubjectHeaderV2
@synthesize  lblSubjectName;
@synthesize imgSubject;
@synthesize imgArrowSection;
@synthesize delegate;
@synthesize btnToggle;
@synthesize isToggle;
@synthesize section;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self setFrame:CGRectMake(0, 0, 320, 50)];
        imgSubject=[[UIImageView alloc] initWithFrame:CGRectMake(0,5,64,50)];
        lblSubjectName = [[UILabel alloc] initWithFrame:CGRectMake(100, 10, 196, 39)];
        [lblSubjectName setBackgroundColor:[UIColor clearColor]];
        [lblSubjectName setTextColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:1]];
        [lblSubjectName setFont:[UIFont fontWithName:@"Arial-BoldMT" size:17]];
        [self setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageBeforeiOS7:imgBackgroundSection equaliOS7:imgBackgroundSectionFlat]]];
        if(isToggle == YES)
        {
            imgArrowSection = [[UIImageView alloc]initWithImage:[UIImage imageBeforeiOS7:imgRightRowOpen equaliOS7:imgRightRowOpenFlat]];
        }
        else
        {
            imgArrowSection = [[UIImageView alloc]initWithImage:[UIImage imageBeforeiOS7:imgRightRow equaliOS7:imgRightRowFlat]];
        }
        
        [imgArrowSection setFrame:CGRectMake(288, 12, 25, 25)];
        btnToggle = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnToggle setFrame:CGRectMake(0, 0, 320, 50)];
        [btnToggle addTarget:self action:@selector(toggleOpen:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:imgSubject];
        [self addSubview:lblSubjectName];
        [self addSubview:imgArrowSection];
        [self addSubview:btnToggle];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
-(void)toggleOpen:(id)sender {
    [self toggleOpenWithUserAction:YES];
}

-(void)toggleOpenWithUserAction:(BOOL)userAction {
    if (userAction) {
        if (isToggle == NO) {
            if ([delegate respondsToSelector:@selector(sectionHeaderView:sectionOpened:)]) {
                isToggle = YES;
                [delegate sectionHeaderView:self sectionOpened:section];
                
                [imgArrowSection setImage:[UIImage imageBeforeiOS7:imgRightRowOpen equaliOS7:imgRightRowOpenFlat]];
            }
        }else {
            if ([delegate respondsToSelector:@selector(sectionHeaderView:sectionClosed:)]) {
                isToggle = NO;
                [imgArrowSection setImage:[UIImage imageBeforeiOS7:imgRightRow equaliOS7:imgRightRowFlat]];
                [delegate sectionHeaderView:self sectionClosed:section];
            }
        }
    }
}
-(id)initWithSection:(NSInteger)sectionIndex subjectName:(NSString *)subName subjectImage: (NSData*)subImage AndStatusArrow:(BOOL)isOpened {
    self = [self init];
    if (self) {
        self.section = sectionIndex;
        self.lblSubjectName.text = subName;
        [self.imgSubject setImage:[UIImage imageWithData:subImage]];
        self.isToggle = isOpened;
    }
    return self;
}


@end
