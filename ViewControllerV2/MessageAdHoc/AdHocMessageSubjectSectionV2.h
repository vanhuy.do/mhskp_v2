//
//  AdHocMessageSubjectSectionV2.h
//  mHouseKeeping
//
//  Created by TMS on 2/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AdHocMessageSubjectHeaderV2.h"
#import "MessageSubjectModelV2.h"
@interface AdHocMessageSubjectSectionV2 : NSObject

 
 @property (nonatomic, strong) MessageSubjectModelV2 *chkContentType;
 @property  BOOL open;
 @property (nonatomic,strong) AdHocMessageSubjectHeaderV2* headerView;
 
 @property (nonatomic,strong) NSMutableArray *rowHeights;
 
 - (NSUInteger)countOfRowHeights;
 - (id)objectInRowHeightsAtIndex:(NSUInteger)idx;
 - (void)insertObject:(id)anObject inRowHeightsAtIndex:(NSUInteger)idx;
 - (void)removeObjectFromRowHeightsAtIndex:(NSUInteger)idx;
 - (void)replaceObjectInRowHeightsAtIndex:(NSUInteger)idx withObject:(id)anObject;
 - (void)getRowHeights:(id __unsafe_unretained [])buffer range:(NSRange)inRange ;
 - (void)insertRowHeights:(NSArray *)rowHeightArray atIndexes:(NSIndexSet *)indexes;
 - (void)removeRowHeightsAtIndexes:(NSIndexSet *)indexes;
 - (void)replaceRowHeightsAtIndexes:(NSIndexSet *)indexes withRowHeights:(NSArray *)rowHeightArray;
 -(void) insertObjectToNextIndex:(id)anObject;
 

@end
