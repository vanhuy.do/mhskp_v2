//
//  AdHocMessageToTableViewCellV2.h
//  mHouseKeeping
//
//  Created by TMS on 2/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AdHocMessageToTableViewCellV2 : UITableViewCell
{
    UILabel *lblPersonName;
   
    UIImageView *imvPersonPhoto;
}
@property (nonatomic,strong) IBOutlet UILabel *lblPersonName;

@property (nonatomic,strong) IBOutlet UIImageView *imvPersonPhoto;
@end
