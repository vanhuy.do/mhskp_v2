//
//  AdHocMessageToV2.h
//  mHouseKeeping
//
//  Created by TMS on 2/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MessageToModelV2.h"
#import "AdHocMessageToTableViewCellV2.h"
#import "TopbarViewV2.h"
#import "AdHocMessageManagerV2.h"
#import "CommonVariable.h"
#import "AccessRight.h"

@protocol AdHocMessageToV2Delegate <NSObject>
@optional
-(void)transferTheValueOfTableView:(NSMutableArray *)objectArray;

@end

@interface AdHocMessageToV2 : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    
    UITableView *tblToList;
    UIButton *allButton;
    UILabel *titleView;
    IBOutlet UIButton *btnSelect;
    IBOutlet UIImageView *bgImv;
    IBOutlet UISearchBar *searchBarTo;
    TopbarViewV2 * topBarView;
    
    __unsafe_unretained id <AdHocMessageToV2Delegate>delegate;

    NSMutableArray *toList;
    NSMutableArray *dataTable;
    NSMutableArray *dataTableSearch;
    NSMutableArray *tappedDataArray;
    int isSelectedAll; // 1 if user tap all selected all, otherwise 0
    AccessRight *messages;
}
@property (nonatomic,strong)TopbarViewV2 * topBarView;
@property (nonatomic,strong)  IBOutlet UIButton *btnSelect;
@property (nonatomic,strong) IBOutlet UIButton *allButton;
@property (nonatomic,strong) IBOutlet UILabel *titleView;
@property (nonatomic,strong) IBOutlet UITableView *tblToList;
@property (nonatomic,strong) IBOutlet  IBOutlet UIImageView *bgImv;
@property (nonatomic,assign) id <AdHocMessageToV2Delegate>delegate;

@property (nonatomic,strong) NSMutableArray *dataTable;
@property (nonatomic,strong) NSMutableArray *toList;
@property (nonatomic,strong) NSMutableArray *tappedDataArray;

-(void)loadData;
-(void)setCaptionView ;
-(IBAction)selectAll:(id)sender;
-(IBAction)tapSelectButton:(id)sender;
@end
