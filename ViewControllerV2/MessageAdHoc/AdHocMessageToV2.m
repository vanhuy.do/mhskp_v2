//
//  AdHocMessageToV2.m
//  mHouseKeeping
//
//  Created by TMS on 2/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AdHocMessageToV2.h"
#import "MyNavigationBarV2.h"
#import "DeviceManager.h"
#import "ehkDefinesV2.h"
#import "UIImage+Tint.h"
#import "AccessRightModel.h"

#define FONTLABEL @"Arial-BoldMT"
#define FONTLABELSIZE 17
#define FONTTEXTSIZE 15
#define BACKGROUNDIMAGE @"bg.png"
#define CHECKIMG @"click_box(checked).png"
#define UNCHECKEDIMG @"click_box.png"

#define REDCELL 6.0f/255.0f
#define GREENCELL 62.0f/255.0f
#define BLUECELl 127.0f/255.0f
#define sizeTitle           22
@implementation AdHocMessageToV2
@synthesize tblToList,toList,dataTable;
@synthesize delegate,allButton,titleView,tappedDataArray;
@synthesize btnSelect,topBarView,bgImv;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}


#pragma mark - Load Access Right

-(void) loadAccessRights
{
    AccessRightModel *currentUserAccessRight = [[UserManagerV2 sharedUserManager] currentUserAccessRight];
    messages = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.message];
}


#pragma mark - View lifecycle
-(void)tapBackButton
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //Resize content views of current VC to fit with Navigation bar and tabbar
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        //Clear redundant view for header, footter
        CGRect frameHeaderFooter = tblToList.tableHeaderView.frame;
        frameHeaderFooter.size.height = 0.1f;
        UIView *headerView = [[UIView alloc] initWithFrame:frameHeaderFooter];
        [tblToList setTableHeaderView:headerView];
        
        frameHeaderFooter = tblToList.tableFooterView.frame;
        frameHeaderFooter.size.height = 0.1f;
        UIView *footerView = [[UIView alloc] initWithFrame:frameHeaderFooter];
        [tblToList setTableFooterView:footerView];
        
        [self loadFlatResource];
    }
    
    [self loadAccessRights];
    
    UIColor *bgColor = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:BACKGROUNDIMAGE]];
    self.view.backgroundColor = bgColor;
    if (topBarView == nil) {
        topBarView = [[TopbarViewV2 alloc] initViewWithSuperModel:[CommonVariable sharedCommonVariable].superRoomModel];
        [self.view addSubview:topBarView];
    }
    
    //back button navigation
    self.navigationItem.leftBarButtonItem = [MyNavigationBarV2 createBackButtonWithTarget:self selector:@selector(tapBackButton) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *titleBarButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 230, 44)];
    titleBarButton.titleLabel.backgroundColor = [UIColor clearColor];
    [titleBarButton.titleLabel setFont: FONT_BUTTON_TOPBAR];
    titleBarButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    titleBarButton.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
    [titleBarButton.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
    [titleBarButton.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
    [titleBarButton setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_MESSAGE] forState:UIControlStateNormal];
    [titleBarButton addTarget:self action:@selector(hiddenHeaderView) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.titleView = titleBarButton;
    if (titleBarButton.titleLabel.text.length >= 14) {
        titleBarButton.titleLabel.font = FONT_SMALL_BUTTON_TOPBAR;
    }
    [self.btnSelect setTitleColor:[UIColor colorWithRed:REDCELL green:GREENCELL blue:BLUECELl alpha:1] forState:UIControlStateNormal];
    [self.btnSelect setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_select] forState:UIControlStateNormal];
    
    [self.titleView setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_adhocMsg_send_to]];
    
    //Move title to center vertical
    CGRect titleViewBounds = self.navigationItem.titleView.bounds;
    int moveLeftTitle = 0;
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        moveLeftTitle = 10;
    }
    [titleBarButton setContentEdgeInsets:UIEdgeInsetsMake(-10, -moveLeftTitle, 0, 0)];
    self.navigationItem.titleView.bounds = CGRectMake(titleViewBounds.origin.x + moveLeftTitle, Y_TOPBAR_TITLE, titleViewBounds.size.width, titleViewBounds.size.height);
    
    
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getRaLoadingData]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES];
    
    [self performSelector:@selector(loadWSUserList:) withObject:HUD afterDelay:0.0f];
    
    searchBarTo.placeholder = [L_SEARCH currentKeyToLanguage];
    [searchBarTo setValue:[L_CANCEL currentKeyToLanguage] forKey:@"_cancelButtonText"];
}

-(void)loadWSUserList:(MBProgressHUD*)HUD
{
    [[HomeViewV2 shareHomeView] loadUserListByCurrentUser];
    [self performSelector:@selector(loadDataSuccess:) withObject:HUD afterDelay:0.0f];
}

-(void)loadDataSuccess:(MBProgressHUD*)HUD
{
    [self loadData];
    
    if (messages.isALlowedEdit) {
        [btnSelect setUserInteractionEnabled:YES];
        [btnSelect setAlpha:1];
    } else {
        [btnSelect setUserInteractionEnabled:NO];
        [btnSelect setAlpha:0.5];
    }
    
    //set selected tab message
    [[HomeViewV2 shareHomeView] setSelectedButton:tagOfMessageButton];
    [tblToList setBackgroundColor:[UIColor clearColor]];
    [tblToList reloadData];
    
    [HUD hide:YES];
    [HUD removeFromSuperview];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    [btnSelect setUserInteractionEnabled:YES];
    [btnSelect setAlpha:1];
    
    //set selected tab message
    [[HomeViewV2 shareHomeView] setSelectedButton:tagOfMessageButton];
    [tblToList setBackgroundColor:[UIColor clearColor]];
    [tblToList reloadData];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - UITableViewDelegate Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(searchBarTo.text.length > 0){
        return [dataTableSearch count];
    }
    return [dataTable count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *sectionIdentified = @"cellIdentified";
    AdHocMessageToTableViewCellV2 *cell = nil;
    
    cell = [tableView dequeueReusableCellWithIdentifier:sectionIdentified];
    if (cell == nil) {
        NSArray *arrayCell = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([AdHocMessageToTableViewCellV2 class]) owner:self options:nil];
        cell = [arrayCell objectAtIndex:0];
        
    }
    //Load data to cells
    UserDetailsModelV2 *personi = nil;
    
    if(searchBarTo.text.length > 0) {
        personi = [dataTableSearch objectAtIndex:indexPath.row];
    } else {
        personi = [dataTable objectAtIndex:indexPath.row];
    }
    
    UIColor *cellColor=[UIColor colorWithRed:REDCELL green:GREENCELL blue:BLUECELl alpha:1];
    [cell.lblPersonName setTextColor:cellColor];
    cell.lblPersonName.text=personi.userDetailsFullName;
    if (!personi.isSelected) {
        [cell.imvPersonPhoto setImage:[UIImage imageNamed:UNCHECKEDIMG]];
        
    }else
    {
        [cell.imvPersonPhoto setImage:[UIImage imageNamed:CHECKIMG]];
        
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UserDetailsModelV2 *toObject = nil;
    if(searchBarTo.text.length > 0){
        toObject = [dataTableSearch objectAtIndex:indexPath.row];
    } else {
        toObject = [dataTable objectAtIndex:indexPath.row];
    }
    AdHocMessageToTableViewCellV2 *selectedCell=(AdHocMessageToTableViewCellV2*)[tableView cellForRowAtIndexPath:indexPath];
    if (toObject.isSelected) {
        [selectedCell.imvPersonPhoto setImage:[UIImage imageNamed:UNCHECKEDIMG]];
        //         NSLog(@"Add unchecked image in here !!!");
        toObject.isSelected=NO;
    }else
    {
        [selectedCell.imvPersonPhoto setImage:[UIImage imageNamed:CHECKIMG]];
        //         NSLog(@"Add checked image in here !!!");
        toObject.isSelected=YES;
    }
    
    if([self isSelectAll]){
        isSelectedAll = 1;
        [allButton setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_adhocMsg_Unselect_all] forState:UIControlStateNormal];
    } else {
        isSelectedAll = 0;
        [self.allButton setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_adhocMsg_select_all] forState:UIControlStateNormal];
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


#pragma mark - set Caption View
-(void)setCaptionView {
}
#pragma Load Template Data
-(void)loadData
{
    NSMutableArray *listAllUser = [[AdHocMessageManagerV2 sharedAdHocMessageManagerV2] loadAllUserDetailsData];
    if (isDemoMode){
        int hotelId = [[[UserManagerV2 sharedUserManager] currentUser] userHotelsId];
        NSMutableArray *localData = [[AdHocMessageManagerV2 sharedAdHocMessageManagerV2] loadAllUserListDataByUserHotelId:hotelId];
        listAllUser = [NSMutableArray new];
        for (UserListModelV2 *obj in localData) {
            UserDetailsModelV2 *detail = [[UserDetailsModelV2 alloc] init];
            detail.userId = obj.userListId;
            detail.userDetailsId = [NSString stringWithFormat:@"%ld", (long)obj.userListId];
            detail.userDetailsName = obj.userListName;
            detail.userDetailsHotelId = obj.userListHotelId;
            detail.userDetailsFullName = obj.userListFullName;
            detail.userDetailsFullNameLang = obj.userListFullNameLang;
            detail.userDetailsLastModified = obj.userListLastModified;
            detail.userSupervisorId = obj.userSupervisorId;
            detail.usrSupervisor = obj.usrSupervisor;
            detail.isSelected = obj.isSelected;
            [listAllUser addObject:detail];
        }
    }
    //Remove current user out of list user receive message
    
    NSArray *filteredArray = [listAllUser filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(UserDetailsModelV2 *object, NSDictionary *bindings) {
        if([object isKindOfClass:[UserDetailsModelV2 class]]){
            if([object.userDetailsId isEqualToString:[NSString stringWithFormat:@"%d",[UserManagerV2 sharedUserManager].currentUser.userId]]) {
                return NO;
            }
        }
        return YES;
    }]];
    
    listAllUser = [[NSMutableArray alloc] initWithArray:filteredArray];
    toList = [NSMutableArray arrayWithArray:listAllUser];
    if (!toList) {
        toList=[[NSMutableArray alloc] init];
    }
    if (!dataTable) {
        dataTable=[[NSMutableArray alloc] init];
    }
    for (int i=0;i< [toList count];i++) {
        if (tappedDataArray && [tappedDataArray count]>0) {
            for (int j=0;j<[tappedDataArray count];j++) {
                UserDetailsModelV2  *ultappedData=[tappedDataArray objectAtIndex:j];
                UserDetailsModelV2 *ultoList=[toList objectAtIndex:i];
                if ([ultappedData.userDetailsId isEqualToString:ultoList.userDetailsId]) {
                    ultoList.isSelected=YES;
                    [tappedDataArray replaceObjectAtIndex:j withObject:ultoList];
                    
                }
            }
            
        }
    }
    if (tappedDataArray && toList.count == tappedDataArray.count) {
        isSelectedAll = 1;
        [allButton setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_adhocMsg_Unselect_all] forState:UIControlStateNormal];
    } else {
        isSelectedAll = 0;
        [self.allButton setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_adhocMsg_select_all] forState:UIControlStateNormal];
    }
    [self.dataTable addObjectsFromArray:toList];
}

-(IBAction)selectAll:(id)sender
{
    if (isSelectedAll) {
        isSelectedAll = 0;
        [allButton setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_adhocMsg_select_all] forState:UIControlStateNormal];
        for (UserDetailsModelV2 *toModel in toList) {
            toModel.isSelected=NO;
        }
    } else {
        isSelectedAll = 1;
        [allButton setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_adhocMsg_Unselect_all] forState:UIControlStateNormal];
        for (UserDetailsModelV2 *toModel in toList) {
            toModel.isSelected=YES;
        }
    }
    
    [tblToList reloadData];
}

-(BOOL) isSelectAll
{
    BOOL isSelectAll = YES;
    for (UserDetailsModelV2 *toModel in toList) {
        if(!toModel.isSelected){
            isSelectAll = NO;
        }
    }
    return isSelectAll;
}

-(IBAction)tapSelectButton:(id)sender
{
    if (self.tappedDataArray) {
        
        [self.tappedDataArray removeAllObjects];
    }
    self.tappedDataArray=[[NSMutableArray alloc] init];
    for (int i=0; i<[dataTable count]; i++) {
        UserDetailsModelV2 *tempObject=[dataTable objectAtIndex:i];
        if (tempObject.isSelected) {
            [self.tappedDataArray addObject:tempObject];
            
        }
    }
    
    if ([delegate respondsToSelector:@selector(transferTheValueOfTableView:)])
        
    {
        [delegate transferTheValueOfTableView:self.tappedDataArray];
        [self.navigationController popViewControllerAnimated:YES];
        
    }
    
}

#pragma mark - UISearchBar Delegate Methods

-(void) searchUsers
{
    
    if(!dataTableSearch){
        dataTableSearch = [NSMutableArray array];
    }
    [dataTableSearch removeAllObjects];
    
    NSString *searchText = searchBarTo.text;
    if(searchText.length > 0) {
        BOOL isFound = NO;
        for (int i = 0; i < dataTable.count; i ++) {
            isFound = NO;
            UserDetailsModelV2 *curUser = [dataTable objectAtIndex:i];
            
            if (curUser.isSelected) {
                isFound = YES;
            }
            
            if(curUser.userDetailsFullName.length > 0 && !isFound) {
                NSRange r = [[curUser.userDetailsFullName lowercaseString] rangeOfString:[searchText lowercaseString]];
                if(r.location != NSNotFound){
                    isFound = YES;
                }
            }
            
            if(curUser.userDetailsFullNameLang.length > 0 && !isFound) {
                NSRange r = [[curUser.userDetailsFullNameLang lowercaseString] rangeOfString:[searchText lowercaseString]];
                if(r.location != NSNotFound){
                    isFound = YES;
                }
            }
            
            if(curUser.userDetailsName.length > 0 && !isFound) {
                NSRange r = [[curUser.userDetailsName lowercaseString] rangeOfString:[searchText lowercaseString]];
                if(r.location != NSNotFound){
                    isFound = YES;
                }
            }
            
            if(isFound){
                [dataTableSearch addObject:curUser];
            }
        }
    }
}

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    
    [searchBarTo setShowsCancelButton:YES];
    searchBarTo.autocorrectionType = UITextAutocorrectionTypeNo;
}


-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
    [self searchUsers];
    [searchBarTo setShowsCancelButton:YES];
    [tblToList reloadData];
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    
    [searchBarTo resignFirstResponder];
    
}
-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [searchBarTo setShowsCancelButton:NO];
    [searchBarTo resignFirstResponder];
    searchBarTo.text = @"";
}

#pragma mark -  Hidden Header view when tap on navigationBar

// hidden header when tap navigationBar
#define heightScale 45
- (void) hiddenHeaderView {
    
    BOOL isHidden = topBarView.hidden;
    [topBarView setHidden:!isHidden];
    
    CGRect frame1 = CGRectZero;
    CGRect frame2 = CGRectZero;
    CGRect frame3 = CGRectZero;
    CGRect frame4 = CGRectZero;
    CGRect frame5 = CGRectZero;
    
    if(!isHidden){
        frame1  = CGRectMake(tblToList.frame.origin.x, tblToList.frame.origin.y - heightScale, tblToList.frame.size.width, tblToList.frame.size.height+heightScale);
        frame2  = CGRectMake(allButton.frame.origin.x, allButton.frame.origin.y - heightScale, allButton.frame.size.width, allButton.frame.size.height);
        frame3  = CGRectMake(bgImv.frame.origin.x, bgImv.frame.origin.y - heightScale, bgImv.frame.size.width, bgImv.frame.size.height);
        frame4 = CGRectMake(titleView.frame.origin.x, titleView.frame.origin.y - heightScale, titleView.frame.size.width, titleView.frame.size.height);
        frame5 = CGRectMake(searchBarTo.frame.origin.x, searchBarTo.frame.origin.y - heightScale, searchBarTo.frame.size.width, searchBarTo.frame.size.height);
    } else
    {
        frame1  = CGRectMake(tblToList.frame.origin.x, tblToList.frame.origin.y + heightScale, tblToList.frame.size.width, tblToList.frame.size.height-heightScale);
        frame2  = CGRectMake(allButton.frame.origin.x, allButton.frame.origin.y +heightScale, allButton.frame.size.width, allButton.frame.size.height);
        frame3  = CGRectMake(bgImv.frame.origin.x, bgImv.frame.origin.y + heightScale, bgImv.frame.size.width, bgImv.frame.size.height);
        frame4  = CGRectMake(titleView.frame.origin.x, titleView.frame.origin.y + heightScale, titleView.frame.size.width, titleView.frame.size.height);
        frame5  = CGRectMake(searchBarTo.frame.origin.x, searchBarTo.frame.origin.y + heightScale, searchBarTo.frame.size.width, searchBarTo.frame.size.height);
    }
    
    [tblToList setFrame:frame1];
    [allButton setFrame:frame2];
    [bgImv setFrame:frame3];
    [titleView setFrame:frame4];
    [searchBarTo setFrame:frame5];
}

#pragma mark - iOS 7 Resources
-(void)loadFlatResource
{
    [bgImv setImage:[UIImage imageNamed:imgTopBtFlat]];
    [btnSelect setBackgroundImage:[UIImage imageNamed:imgBtnViewLogFlat] forState:UIControlStateNormal];
    [allButton setBackgroundImage:[UIImage imageNamed:imgSelectAllFlat] forState:UIControlStateNormal];
}

@end
