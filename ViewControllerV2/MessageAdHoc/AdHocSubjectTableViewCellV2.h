//
//  AdHocSubjectTableViewCellV2.h
//  mHouseKeeping
//
//  Created by TMS on 2/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface AdHocSubjectTableViewCellV2 : UITableViewCell


{

    NSIndexPath *indexpath;
}

@property (strong, nonatomic) NSIndexPath *indexpath;
@property (strong, nonatomic) IBOutlet UIImageView *imgSubjectItemCell;
@property (strong, nonatomic) IBOutlet UILabel *lblNameSubjectItemCell;


@end
