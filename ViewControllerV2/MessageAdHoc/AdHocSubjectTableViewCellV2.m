//
//  AdHocSubjectTableViewCellV2.m
//  mHouseKeeping
//
//  Created by TMS on 2/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AdHocSubjectTableViewCellV2.h"

@implementation AdHocSubjectTableViewCellV2
//@synthesize delegate;
@synthesize indexpath;
@synthesize imgSubjectItemCell;
@synthesize lblNameSubjectItemCell;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


@end
