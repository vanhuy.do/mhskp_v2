//
//  AdhocMessageDetailV2.h
//  mHouseKeeping
//
//  Created by TMS on 2/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LanguageManagerV2.h"
#import "ehkDefines.h"
#import "TopbarViewV2.h"
#import "PhotoZoomV2.h"
#import "PhotoScrollViewV2.h"
#import "MessageModelV2.h"
#import "AdHocMessageToV2.h"
#import "AdHocMessageContentViewV2.h"
#import "AlertAdvancedSearch.h"
#import "AdHocMessageManagerV2.h"
#import "AdHocMessageSubViewV2.h"
#import "PhotoViewV2.h"
#import "AdhocMessageMain.h"
#import "AccessRight.h"

@protocol AdhocMEssageDetailV2Delegate <NSObject>
@optional
-(void)backButtonProcess:(BOOL)isForwardMessage;
@end

@interface AdhocMessageDetailV2 : UIViewController<UITableViewDelegate,UITableViewDataSource,AdHocMessageToV2Delegate, AdHocMessageContentViewV2Delegate,AlertAdvancedSearchDelegate,UIAlertViewDelegate, AdHocMessageSubViewV2Delegate, PhotoViewV2Delegate, UIGestureRecognizerDelegate>
{
    IBOutlet UITableView *messageTableView;
    IBOutlet UIButton *messageButton;
    IBOutlet UIButton *replyButton;
    TopbarViewV2 * topBarView;
    UIButton *titleBarButton;
    __unsafe_unretained id<AdhocMEssageDetailV2Delegate>delegate;
    MessageModelV2 *messageObject;
    NSInteger indexDelete;
    BOOL isSentMessage;
    BOOL isForwardMessage;
    BOOL isReplyMessage;
    BOOL dataIsChanged;
    BOOL backIsPressed; // Back button is pressed or not
    BOOL messageIsCompleted; // Messase is sent and will not push sent item screen
    
    AccessRight *messages;
    MBProgressHUD *HUD;
    
    NSMutableArray *messageReceiverList;
}
@property (nonatomic,strong)MessageModelV2 *messageObject;
@property (nonatomic,strong) MessageModelV2 *messageObjectOfRePly;
@property (nonatomic,strong) NSMutableArray *photoList;
@property (nonatomic,strong)TopbarViewV2 * topBarView;
@property (nonatomic,strong)UIButton *titleBarButton;
@property (nonatomic,assign) id<AdhocMEssageDetailV2Delegate>delegate;
@property   NSInteger indexDelete;
@property BOOL isSentMessage;
@property BOOL isForwardMessage;
@property BOOL isForOneMsg;
@property BOOL isReplyMessage;
@property (nonatomic,strong) NSMutableArray *messageReceiverList;
@property (nonatomic,strong) NSMutableArray *messageForwardList;
@property (nonatomic,strong) IBOutlet UITableView *messageTableView;
@property (nonatomic,strong) IBOutlet UIButton *messageButton;
@property (nonatomic,strong) IBOutlet UIButton *replyButton;
-(void) setCaptionsView ;
-(IBAction)buttonProcess:(id)sender;
- (IBAction)buttonReply:(id)sender;
-(void)sendMessage;
-(void)forwardMessage;
-(void)changeTitleButton;
@end
