//
//  AdhocMessageDetailV2.m
//  mHouseKeeping
//
//  Created by TMS on 2/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AdhocMessageDetailV2.h"
#import "zoomPhotoViewV2.h"
#import "AdHocMessageManagerV2.h"
#import "CustomAlertViewV2.h"
#import "NetworkCheck.h"
#import "ehkConvert.h"
#import "UserManagerV2.h"
#import "MyNavigationBarV2.h"
#import "LogFileManager.h"
#import "DeviceManager.h"

#define NEXTIMAGE @"gray_arrow_36x36.png"
#define BACKGROUNDIMAGE  @"bg.png"
#define DETAIL_IMAGE_FRAME CGRectMake(260, 5, 25, 25)
#define RECEIVER_SUBJECT_FRAME CGRectMake(100, 3, 170, 40)
#define FONTLABEL @"Arial-BoldMT"
#define FONTLABELSIZE 17
#define FONTTEXTSIZE 15
#define REDLABEL 6.0f/255.0f
#define GREENLABEL 62.0f/255.0f
#define BLUELABEL 127.0f/255.0f
#define REDTEXT 102.0f/255.0f
#define GREENTEXT 102.0f/255.0f
#define BLUETEXT 102.0f/255.0f
#define KTAGMESSAGE 2
#define KTAGMESSAGETEXT 3
#define RECEIVER_SUBJECT_TAG 4
#define HEIGHT 46
#define WIDTH  74
#define WIDTHIMAGE 85
#define sizeTitle 22

#define tagDiscard 14
#define tagSaveLeaving 12
#define tagSaveSuccessful 15
#define tagSentSuccessful 17

@interface AdhocMessageDetailV2 (PrivateMethods)

-(void)selectPhotoButton;

@end

// Index for row of table
enum AdhocIndex {
    AdhocFromToIndex = 0,
    AdhocSubjectIndex,
    AdhocMessageContentIndex,
};

@implementation AdhocMessageDetailV2
@synthesize messageTableView;
@synthesize messageButton,replyButton;
@synthesize  topBarView,photoList,messageObject;
@synthesize isSentMessage,isForwardMessage,isReplyMessage;
@synthesize delegate,titleBarButton;
@synthesize indexDelete;
@synthesize messageReceiverList;
@synthesize messageForwardList;
@synthesize messageObjectOfRePly;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - Load Access Right

-(void) loadAccessRights
{
    AccessRightModel *currentUserAccessRight = [[UserManagerV2 sharedUserManager] currentUserAccessRight];
    messages = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.message];
}

#pragma mark - View lifecycle
/**
 * Status of AdhocMessageDetailV2
 * isForwardMessage = TRUE: message was tap forward and now is ready to send
 * isForwardMessage = FALSE: Message come from inbox and outbox, have forward button
 * isSentMessage = TRUE: message come from inbox
 * isSentMessage = FALSE: message come from sent item
 * in case message is uncomplete sent message, it will go through message detail
 * with isForwardMessage = TRUE and isSentMessage = TRUE
 **/


- (void)changeTitleButton
{
    // View for send message after tap forward
    if (isForwardMessage) {
        [titleBarButton setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_forward_message] forState:UIControlStateNormal];
       
        [messageButton setTitle:[[LanguageManagerV2 sharedLanguageManager] getSend] forState:UIControlStateNormal];
    }
    // View for send message after tap reply
    else if (isReplyMessage) {
        [titleBarButton setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_reply_message] forState:UIControlStateNormal];
        [messageButton setTitle:[[LanguageManagerV2 sharedLanguageManager] getSend] forState:UIControlStateNormal];
    }
    // Message is from "Sent items"
    else if(isSentMessage)
    {
        [titleBarButton setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_adhocMsg_sent_message_title] forState:UIControlStateNormal];
    
        [messageButton setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_forward] forState:UIControlStateNormal];
    }
    // Message is from "Inbox"
    else {
        [titleBarButton setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_adhocMsg_inbox_message_title] forState:UIControlStateNormal];
        
        [messageButton setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_forward] forState:UIControlStateNormal];
        
        [replyButton setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_reply] forState:UIControlStateNormal];
    }
    
    if(titleBarButton.titleLabel.text.length >= 14)
    {
        [titleBarButton.titleLabel setFont: FONT_SMALL_BUTTON_TOPBAR];
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    [messageTableView reloadData];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    //Resize content views of current VC to fit with Navigation bar and tabbar
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    replyButton.hidden = !self.isForOneMsg;
    if(self.isForOneMsg) {
        if (!HUD) {
            HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
        }
        [self.view.superview addSubview:HUD];
        [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getRaLoadingData]];
        [HUD show:YES];
        //[NSThread detachNewThreadSelector:@selector(showTitleBox) toTarget:self withObject:nil];
        //Delete old message
        int userId = [UserManagerV2 sharedUserManager].currentUser.userId;
        int messagePeriod = [[UserManagerV2 sharedUserManager].currentUserAccessRight getMessagePeriod];
        NSString *lastModified = [ehkConvert dateStringAgoWithPeriodDays:messagePeriod];
        NSString *lastModifiedDate = [ehkConvert commonDateRemoveTime:lastModified];
        if (!isDemoMode)
            [[AdHocMessageManagerV2 sharedAdHocMessageManagerV2] deleteMessageByOwnerId:userId];
        
        //Check and load userlist before getting message
        [[HomeViewV2 shareHomeView] loadUserListByCurrentUser];
        
        // Call new API for get sent/received ad hoc message - discussion with FM on 20151007
        [[AdHocMessageManagerV2 sharedAdHocMessageManagerV2] getReceivedAdHocMessageRoutineByUserId:userId groupMessageId:0 hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId lastModified:lastModifiedDate];
        [[AdHocMessageManagerV2 sharedAdHocMessageManagerV2] getSendAdHocMessageRoutineByUserId:userId groupMessageId:0 hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId lastModified:lastModifiedDate];
        
        NSMutableArray *messageInbox = [[AdHocMessageManagerV2 sharedAdHocMessageManagerV2] loadMessageReceivedByOwnerId:[UserManagerV2 sharedUserManager].currentUser.userId];
        
        for(MessageModelV2 *msgObject in messageInbox) {
            if(msgObject.message_status == 0){
                self.messageObject = msgObject;
                break;
            }
        }
        NSLog(@"%@",messageInbox);
    }
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        CGRect messageButtonFrame = messageButton.frame;
        messageButtonFrame.origin.y += 30;
        [messageButton setFrame: messageButtonFrame];
        CGRect replyButtonFrame = messageButton.frame;
        replyButtonFrame.origin.x -= 155;
        [replyButton setFrame: replyButtonFrame];
        [self loadFlatResource];
        [self loadFlatResource];
    }
    
    if(!HUD){
        HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    }
    
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_SENDING_DATA]];
    [self.view.superview addSubview:HUD];
    [HUD show:YES];
    
    [self loadAccessRights];
    [self.messageTableView setBackgroundColor:[UIColor clearColor]];
    UIColor *bgColor = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:BACKGROUNDIMAGE]];
    self.view.backgroundColor = bgColor;
    if (topBarView == nil) {
        topBarView = [[TopbarViewV2 alloc] initViewWithSuperModel:[CommonVariable sharedCommonVariable].superRoomModel];
        [self.view addSubview:topBarView];
    }
    titleBarButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 230, 44)];
    
    [titleBarButton.titleLabel setFont: FONT_BUTTON_TOPBAR];
    titleBarButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    [titleBarButton.titleLabel setAdjustsFontSizeToFitWidth:YES];
    titleBarButton.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
    
    [titleBarButton addTarget:self action:@selector(hiddenHeaderView) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.titleView = titleBarButton;
    
    //back button navigation
    self.navigationItem.leftBarButtonItem = [MyNavigationBarV2 createBackButtonWithTarget:self selector:@selector(tapBackButton:) forControlEvents:UIControlEventTouchUpInside];
    
    //Move title to center vertical
    CGRect titleViewBounds = self.navigationItem.titleView.bounds;
    int moveLeftTitle = 0;
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        moveLeftTitle = 10;
    }
    [titleBarButton setContentEdgeInsets:UIEdgeInsetsMake(-10, -moveLeftTitle, 0, 0)];
    self.navigationItem.titleView.bounds = CGRectMake(titleViewBounds.origin.x + moveLeftTitle, Y_TOPBAR_TITLE, titleViewBounds.size.width, titleViewBounds.size.height);
    [self changeTitleButton];
    [self setCaptionsView];
    
    AdHocMessageManagerV2 *messageManager = [AdHocMessageManagerV2 sharedAdHocMessageManagerV2];
    [messageManager getMessagePhotoWithUserID:[UserManagerV2 sharedUserManager].currentUser.userId andGroupId:messageObject.message_group_id];
    
    if(!isForwardMessage){
        photoList = [messageManager loadMessagePhotosByGroupId:(int)messageObject.message_group_id];
        
        if (!isSentMessage) {
            if(messageObject.message_status == MESSAGE_UNREAD){
                if(isDemoMode){
                    [[AdHocMessageManagerV2 sharedAdHocMessageManagerV2] updateMessageStatus:messageObject];
                    messageObject.message_status = MESSAGE_READ;
                    [[HomeViewV2 shareHomeView] updateUnreadMessage]; //update badge number for message
                }
                else{
                    BOOL isSucess = [messageManager updateWSMessageStatus:[UserManagerV2 sharedUserManager].currentUser.userId messageId:(int)messageObject.message_id status:MESSAGE_READ];
                    if(isSucess) {
                        int countMessage = [[[UserManagerV2 sharedUserManager] getCurrentUserConfigValueByKey:USER_CONFIG_MESSAGE_UNREAD_RECEIVED] intValue];
                        if(countMessage > 0){
                            countMessage --;
                        }
                        [[UserManagerV2 sharedUserManager] setCurrentUserConfig:USER_CONFIG_MESSAGE_UNREAD_RECEIVED value:[NSString stringWithFormat:@"%d", countMessage]];
                        [[HomeViewV2 shareHomeView] updateUnreadMessage]; //update badge number for message
                        
                        messageObject.message_status = MESSAGE_READ;
                        [messageManager updateMessage:messageObject];
                    }
                }
            }
        }
    }
    
    [HUD hide:YES];
    [HUD removeFromSuperview];

        // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[HomeViewV2 shareHomeView].slidingMenuBar showAll];
    //set selected tab message
    [[HomeViewV2 shareHomeView] setSelectedButton:tagOfMessageButton];
    
//    if(messages.isAllowedView)
//    {
//        [messageButton setUserInteractionEnabled:NO];
//        [messageButton setAlpha:0.5];
//    }
//    else
//    {
//        [messageButton setUserInteractionEnabled:YES];
//        [messageButton setAlpha:1];
//    }
    
    //Check for allow editMessage
    [self.navigationController setNavigationBarHidden:NO];
    if (!messages.isALlowedEdit) {
        [messageButton setHidden:YES];
        [replyButton setHidden:YES];
    } else {
        [messageButton setUserInteractionEnabled:YES];
        [messageButton setAlpha:1];
        [replyButton setUserInteractionEnabled:YES];
        [replyButton setAlpha:1];
    }
    
    
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
#pragma UITableView DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int rowinSection=0;
    if (section==0) {
         rowinSection=3;
    } else {
        rowinSection=1;
    }
   
    
    return rowinSection;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *toRowIdentifier = @"toIdentifier";
    static NSString *subjectRowIdentifier = @"subjectIdentifier";
    static NSString *photoSectionIdentifier=@"photoIdentifier";
    static NSString *messageRowIdentifier=@"messageIdentifier";
    
    UIColor *labelColor=[UIColor colorWithRed:REDLABEL green:GREENLABEL blue:BLUELABEL alpha:1];
    
    
    UIColor *textColor=[UIColor colorWithRed:REDTEXT green:GREENTEXT blue:BLUETEXT alpha:1];    
    UITableViewCell *cell=nil;
    if (indexPath.section==0) {
        switch (indexPath.row) {
            case AdhocFromToIndex:
            {   
                cell=[tableView dequeueReusableCellWithIdentifier:toRowIdentifier];    
                break;
            }
            case AdhocSubjectIndex:
            { 
                cell=[tableView dequeueReusableCellWithIdentifier:subjectRowIdentifier];
                break;
            }
            case AdhocMessageContentIndex:
            {
                cell=[tableView dequeueReusableCellWithIdentifier:messageRowIdentifier]; }
                break;
            default:
                break;
        }

    }else
    {
        if (indexPath.row==0) {
            cell=[tableView dequeueReusableCellWithIdentifier:photoSectionIdentifier];
        }
    }
    
    PhotoScrollViewV2 *photoViewScroll=[[PhotoScrollViewV2 alloc] init];      
    if (cell==nil) {
        if (indexPath.section==0) 
        {
            switch (indexPath.row) {
                case AdhocFromToIndex: {
                    cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:toRowIdentifier];
                    // Add receiver label
                    UILabel* receiverLabel = [[UILabel alloc] initWithFrame:RECEIVER_SUBJECT_FRAME];
                    [receiverLabel setFont:[UIFont fontWithName:FONTLABEL size:FONTLABELSIZE]];
                    [receiverLabel setTextColor:textColor];
                    [receiverLabel setBackgroundColor:[UIColor clearColor]];
                    receiverLabel.tag = RECEIVER_SUBJECT_TAG;
                    [cell addSubview:receiverLabel];
                    
                    // Add detail icon
                    if (isForwardMessage) {
                        UIImage *nextImage=[UIImage imageNamed:NEXTIMAGE];
                        UIImageView *nextImageView=[[UIImageView alloc] initWithImage:nextImage] ;
                        nextImageView.frame=DETAIL_IMAGE_FRAME;
                        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                        [cell setAccessoryView:nextImageView];
                    }
                    break;
                }     
                case AdhocSubjectIndex: {
                    cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:subjectRowIdentifier];  
                    // Add receiver label
                    UILabel* receiverLabel = [[UILabel alloc] initWithFrame:RECEIVER_SUBJECT_FRAME];
                    [receiverLabel setFont:[UIFont fontWithName:FONTLABEL size:FONTLABELSIZE]];
                    [receiverLabel setTextColor:textColor];
                    [receiverLabel setBackgroundColor:[UIColor clearColor]];
                    receiverLabel.tag = RECEIVER_SUBJECT_TAG;
                    [cell addSubview:receiverLabel];
                    break;
                }
                case AdhocMessageContentIndex:
                { 
                    cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:messageRowIdentifier];  
                    UILabel *messageLabel=[[UILabel alloc] initWithFrame:CGRectMake(10,0, 250,30 )];
                    
                    messageLabel.tag=KTAGMESSAGE;
                    [cell.contentView addSubview:messageLabel];
                    // Add next icon
                    UIImage *nextImage=[UIImage imageNamed:NEXTIMAGE];
                    UIImageView *nextImageView=[[UIImageView alloc] initWithImage:nextImage] ;
                    nextImageView.frame=DETAIL_IMAGE_FRAME;
                    
                    
                    [cell.contentView addSubview:nextImageView];
                }
                    break;
                default:
                    break;
            }
        }else
        {
            if (indexPath.row==0) {
                cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:photoSectionIdentifier];  
                photoViewScroll.frame=CGRectMake(0,0, 290, 70);
                photoViewScroll.backgroundColor=[UIColor clearColor];
                photoViewScroll.tag = 100;
                [cell.contentView addSubview:photoViewScroll];
                [cell setBackgroundColor:[UIColor colorWithWhite:0.5 alpha:0.5]];
            }
        }
    }
    //add data
    if (indexPath.section==0) {
        switch (indexPath.row) {
            case AdhocFromToIndex:
            {
                if (!isForwardMessage && !isSentMessage) {
                    if(isReplyMessage) {
                        cell.textLabel.text= [NSString stringWithFormat:@"%@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_to]];
                    } else {
                    cell.textLabel.text= [NSString stringWithFormat:@"%@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_FROM]];
                    }
                } else {
                    cell.textLabel.text= [NSString stringWithFormat:@"%@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_to]];
                }
                
                [cell.textLabel setTextColor:labelColor];
                
                NSMutableString *stringTo=[NSMutableString stringWithString:@""];
                bool isEnglishLang = [[LanguageManagerV2 sharedLanguageManager] currentLanguageIsEqualTo:@"%@", ENGLISH_LANGUAGE];
                
                //view each outbox message
                if(!isForwardMessage && isSentMessage){
                    if (messageReceiverList && [messageReceiverList count] > 0) {
                        if ([messageReceiverList count] > 1) {
                            if(isEnglishLang){
                                for (UserListModelV2 * toObject in messageReceiverList) {
                                    [stringTo appendFormat:@"%@; ",toObject.userListFullName];
                                }
                            } else {
                                for (UserListModelV2 * toObject in messageReceiverList) {
                                    [stringTo appendFormat:@"%@; ",toObject.userListFullNameLang];
                                }
                            }
                        } else {
                            UserListModelV2 * toObject = [messageReceiverList objectAtIndex:0];
                            if(isEnglishLang){
                                [stringTo appendFormat:@"%@",toObject.userListFullName];
                            } else {
                                [stringTo appendFormat:@"%@",toObject.userListFullNameLang];
                            }
                        }
                    }
                }
                
                //view each inbox message
                else if (!isForwardMessage && !isSentMessage){
                    if(messageObject != nil && messageObject.message_from_user_name != nil){
                        [stringTo appendFormat:@"%@", messageObject.message_from_user_name];
                    } else {
                        [stringTo appendFormat:@""];
                    }
                }
                //forward message
                else{
                    if(messageForwardList && [messageForwardList count] != 0)
                    {
                        if ([messageForwardList count] > 1) {
                            if(isEnglishLang){
                                for (UserDetailsModelV2 * toObject in messageForwardList) {
                                    [stringTo appendFormat:@"%@; ",toObject.userDetailsFullName];
                                }
                            } else {
                                for (UserDetailsModelV2 * toObject in messageForwardList) {
                                    [stringTo appendFormat:@"%@; ",toObject.userDetailsFullNameLang];
                                }
                            }
                        } else {
                            UserDetailsModelV2 * toObject = [messageForwardList objectAtIndex:0];
                            if(isEnglishLang){
                                [stringTo appendFormat:@"%@",toObject.userDetailsFullName];
                            } else {
                                [stringTo appendFormat:@"%@",toObject.userDetailsFullNameLang];
                            }
                        }
                    }
                }
                
                if (stringTo.length > 0 && ![stringTo isEqualToString:@"null"]) {
                    UILabel* receiverLabel = (UILabel*)[cell viewWithTag:RECEIVER_SUBJECT_TAG];
                    [receiverLabel setText:stringTo];
                }
                
                break;
            }     
            case AdhocSubjectIndex:
            {   
                cell.textLabel.text= [NSString stringWithFormat:@"%@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_subject]];
                [cell.textLabel setTextColor:labelColor];
                UILabel* receiverLabel = (UILabel*)[cell viewWithTag:RECEIVER_SUBJECT_TAG];
                if(messageObject.message_topic.length > 0)
                {
                    [receiverLabel setText:messageObject.message_topic];
                } else {
                    [receiverLabel setText:@""];
                }
            }
                break;
            case AdhocMessageContentIndex:
            {
                UILabel *lbMessage=(UILabel*)[cell.contentView viewWithTag:KTAGMESSAGE]; 
                [lbMessage setBackgroundColor:[UIColor clearColor]];
                [lbMessage setFont:[UIFont fontWithName:FONTLABEL size:FONTLABELSIZE]];
                [lbMessage setTextColor:textColor];
                NSString *Message = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_MESSAGE];
                if (messageObject.message_content.length > 0) {
                    [lbMessage setText:[NSString stringWithFormat:@"%@\n%@",Message,messageObject.message_content]];
                    NSMutableAttributedString *text =
                    [[NSMutableAttributedString alloc]
                     initWithAttributedString: lbMessage.attributedText];
                    
                    [text addAttribute:NSForegroundColorAttributeName
                                 value:labelColor
                                 range:NSMakeRange(0, 7)];
                    [lbMessage setAttributedText: text];
                } else {
                    [lbMessage setText:[NSString stringWithFormat:@"%@",Message]];
                    NSMutableAttributedString *text =
                    [[NSMutableAttributedString alloc]
                     initWithAttributedString: lbMessage.attributedText];
                    
                    [text addAttribute:NSForegroundColorAttributeName
                                 value:labelColor
                                 range:NSMakeRange(0, 7)];
                    [lbMessage setAttributedText: text];
                }
                
                CGSize maximumLabelSize = CGSizeMake(250, 110);
                
                CGSize expectedLabelSize = [lbMessage.text sizeWithFont:lbMessage.font 
                                                  constrainedToSize:maximumLabelSize 
                                                      lineBreakMode:NSLineBreakByTruncatingTail];
                
                //adjust the label the the new height.
                CGRect newFrame = lbMessage.frame;
                newFrame.size.height = expectedLabelSize.height;
                lbMessage.frame = newFrame;
                
                [lbMessage setNumberOfLines:5];

            }
                break;
            default:
                break;
        }

    }else
    {
        if (indexPath.row==0) {
            //Photo
            
            photoViewScroll = (PhotoScrollViewV2*)[cell.contentView viewWithTag:100];
            if ([photoViewScroll.subviews count]>0) {
                for (UIView* subview in photoViewScroll.subviews) {
                    [subview removeFromSuperview];
                }
            }
            //add data in here 
            
            //first display the photo Button, and add more images if any. If the number of images=6 the photo Button is gray.
            CGFloat cxLocation = 0;
            if ([photoList count]==0) {
                if (isForwardMessage || isReplyMessage) {
                    cxLocation+=(photoViewScroll.frame.size.width - 80)/2;   
                    UIImage *photoImage=[UIImage imageNamed:PHOTOIMAGE];
                    UIButton *photoButton=[[UIButton alloc] initWithFrame:CGRectMake(cxLocation,2,WIDTH, HEIGHT)];
                    [photoButton setBackgroundImage:photoImage forState:UIControlStateNormal];
                    
                    //Check access right for message
//                    if(!messages.isAllowedView)
//                    {
//                        [photoButton addTarget:self action:@selector(selectPhotoButton) forControlEvents:UIControlEventTouchDown];
//                    }
                    
                    [photoButton addTarget:self action:@selector(selectPhotoButton) forControlEvents:UIControlEventTouchDown];
                    
                    [photoViewScroll addSubview:photoButton];
                }
                
                
            } else {
                if ([photoList count]>0 ) {
                    if (([photoList count]<6 && isForwardMessage) || ([photoList count]<6 && isForwardMessage) || ([photoList count]<6 && isReplyMessage) || ([photoList count]<6 && isReplyMessage)) {
                        cxLocation +=10;
                        UIImage *photoImage=[UIImage imageNamed:PHOTOIMAGE];
                        UIButton *photoButton=[[UIButton alloc] initWithFrame:CGRectMake(cxLocation, 2,WIDTH, HEIGHT)];
                        [photoButton setBackgroundImage:photoImage forState:UIControlStateNormal];
                        
//                        if(!messages.isAllowedView)
//                        {
//                            [photoButton addTarget:self action:@selector(selectPhotoButton) forControlEvents:UIControlEventTouchDown];
//                        }
                        
                        [photoButton addTarget:self action:@selector(selectPhotoButton) forControlEvents:UIControlEventTouchDown];
                        
                        [photoViewScroll addSubview:photoButton];
                        
                        cxLocation+=WIDTH;
                        
                    }
                    for (int i=0; i<[photoList count]; i++) {
                        MessagePhotoModelV2 *photoImage=[photoList objectAtIndex:i];
                        cxLocation+=10;
                        UIView *photoView=[[UIView alloc]initWithFrame:CGRectMake(cxLocation, 2, WIDTH, HEIGHT)];
                        UIButton *photoButton=[[UIButton alloc] initWithFrame:CGRectMake(0,0,WIDTHIMAGE , HEIGHT)];
                        [photoButton setBackgroundImage:[UIImage imageWithData:photoImage.message_photo] forState:UIControlStateNormal];
                        photoButton.tag=i;
                        UILongPressGestureRecognizer *longpressGesture=[[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(responseLongPressButton:)];
                        longpressGesture.delegate=self;
                        longpressGesture.minimumPressDuration=1;
                        [photoButton addGestureRecognizer:longpressGesture];
                        
                        [photoButton addTarget:self action:@selector(tapImageToZoom:) forControlEvents:UIControlEventTouchUpInside];
                        [photoView addSubview:photoButton];
                        
                        [photoViewScroll addSubview:photoView];
                        cxLocation+=WIDTH;
                    }
                    
                }
            }  
            [photoViewScroll setContentSize:CGSizeMake(cxLocation, HEIGHT)];
        }
        
    }
        
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{   float rowHeight=0;
    if (indexPath.section==0) {
        switch (indexPath.row) {
            case 2:
                rowHeight=130;
                break;
                
            default:
                rowHeight=44;
                break;
        }   
    }else
    {
        rowHeight = 50;
    }
    return rowHeight;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==0) {
        switch (indexPath.row) {
            case AdhocFromToIndex:{
                if (isReplyMessage) {
                    break;
                }
                AdHocMessageToV2 *toMessageView=[[AdHocMessageToV2 alloc] initWithNibName:@"AdHocMessageToV2" bundle:Nil];
                if(isForwardMessage){
                    
                    toMessageView.tappedDataArray = messageForwardList;
                    [self.navigationController pushViewController:toMessageView animated:YES];
                    toMessageView.title= [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_add_hoc_message];
                    toMessageView.delegate=self;
                    
                    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK] style:UIBarButtonItemStyleBordered target:nil action:nil];
                    self.navigationItem.backBarButtonItem = backButton;
                }
                else {
                    NSMutableString *stringTo=[NSMutableString stringWithString:@""];
                    if (!isForwardMessage && isSentMessage)
                    {
                        if ([messageReceiverList count]>0) {
                            //toMessageView.tappedDataArray = messageReceiverList;
                            bool isEnglishLang = [[LanguageManagerV2 sharedLanguageManager] currentLanguageIsEqualTo:@"%@", ENGLISH_LANGUAGE];
                            if (messageReceiverList && [messageReceiverList count] > 0) {
                                if ([messageReceiverList count] > 1) {
                                    if(isEnglishLang){
                                        for (UserListModelV2 * toObject in messageReceiverList) {
                                            [stringTo appendFormat:@"%@; ",toObject.userListFullName];
                                        }
                                    } else {
                                        for (UserListModelV2 * toObject in messageReceiverList) {
                                            [stringTo appendFormat:@"%@; ",toObject.userListFullNameLang];
                                        }
                                    }
                                } else {
                                    UserListModelV2 * toObject = [messageReceiverList objectAtIndex:0];
                                    if(isEnglishLang){
                                        [stringTo appendFormat:@"%@",toObject.userListFullName];
                                    } else {
                                        [stringTo appendFormat:@"%@",toObject.userListFullNameLang];
                                    }
                                }
                            }
                            AdHocMessageContentViewV2 *messageReceiverListDetail=[[AdHocMessageContentViewV2 alloc] initWithNibName:@"AdHocMessageContentViewV2" bundle:Nil];
                            messageReceiverListDetail.delegate=self;
                            UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK] style:UIBarButtonItemStyleBordered target:nil action:nil];
                            self.navigationItem.backBarButtonItem = backButton;
                            messageReceiverListDetail.contentMessage = stringTo;
                            messageReceiverListDetail.isOnlyReadMessage = YES;
                            messageReceiverListDetail.isFromContent = NO;
                            [self.navigationController pushViewController:messageReceiverListDetail animated:YES];
                        }
                    }
                    else if (!isForwardMessage && !isSentMessage)
                    {
                        if(messageObject != nil && messageObject.message_from_user_name != nil){
                            [stringTo appendFormat:@"%@", messageObject.message_from_user_name];
                        } else {
                            [stringTo appendFormat:@""];
                        }
                        AdHocMessageContentViewV2 *messageSenderListDetail=[[AdHocMessageContentViewV2 alloc] initWithNibName:@"AdHocMessageContentViewV2" bundle:Nil];
                        messageSenderListDetail.delegate=self;
                        UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK] style:UIBarButtonItemStyleBordered target:nil action:nil];
                        self.navigationItem.backBarButtonItem = backButton;
                        messageSenderListDetail.contentMessage = stringTo;
                        messageSenderListDetail.isOnlyReadMessage = YES;
                        messageSenderListDetail.isFromContent = NO;
                        [self.navigationController pushViewController:messageSenderListDetail animated:YES];
                    }
                    
                }
                
            }
                break;
            case AdhocSubjectIndex:
                if (isForwardMessage && isSentMessage) {
                    AdHocMessageSubViewV2 *subjectView=[[AdHocMessageSubViewV2 alloc] initWithNibName:@"AdHocMessageSubViewV2" bundle:Nil];
                    subjectView.delegate=self;
                    
                    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK] style:UIBarButtonItemStyleBordered target:nil action:nil];
                    self.navigationItem.backBarButtonItem = backButton;
                    [self.navigationController pushViewController:subjectView animated:YES];
                }
                break;
            case AdhocMessageContentIndex:{
                
                //Check access right only view
                //                if(messages.isAllowedView){
                //                    return;
                //                }
                
                //Click text message
                if (isForwardMessage || isReplyMessage) {
                    AdHocMessageContentViewV2 *messageContentView=[[AdHocMessageContentViewV2 alloc] initWithNibName:@"AdHocMessageContentViewV2" bundle:Nil];
                    messageContentView.delegate=self;
                    messageContentView.txvMessage.text = @"";
                    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK] style:UIBarButtonItemStyleBordered target:nil action:nil];
                    self.navigationItem.backBarButtonItem = backButton;
                    messageContentView.contentMessage=messageObject.message_content;
                    messageContentView.isFromContent = YES;
                    [self.navigationController pushViewController:messageContentView animated:YES];
                }
                else {
                    AdHocMessageContentViewV2 *messageContentView = [[AdHocMessageContentViewV2 alloc] initWithNibName:@"AdHocMessageContentViewV2" bundle:Nil];
                    messageContentView.delegate=self;
                    messageContentView.txvMessage.text = @"";
                    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK] style:UIBarButtonItemStyleBordered target:nil action:nil];
                    self.navigationItem.backBarButtonItem = backButton;
                    messageContentView.contentMessage = messageObject.message_content;
                    messageContentView.isOnlyReadMessage = YES;
                    messageContentView.isFromContent = YES;
                    [self.navigationController pushViewController:messageContentView animated:YES];
                    [messageContentView.txvMessage setUserInteractionEnabled:NO];
                    [messageContentView.txvMessage resignFirstResponder];
                }
                
            }
        }
        [tableView deselectRowAtIndexPath:indexPath animated:NO];
    }
    else
    {
        if (indexPath.row==0) {
            //click Photo row
        }
    }
    
}
-(void) setCaptionsView {
  
    
}

#pragma mark - Message subject Item ViewV2Delegate
-(void)selectItem: (MessageSubjectItemModelV2 *)itemTemp
{
    dataIsChanged = TRUE;
    if (itemTemp) {
        messageObject.message_topic = itemTemp.smiName;
    }
    
    [messageTableView reloadData];
}

#pragma mark -
#pragma mark - Bottom bar handler
// Check data is saved or not
-(BOOL) checkSaveDataWithTagEvent:(NSInteger) tagOfEvent {
    if (!dataIsChanged) {
        [self.navigationController popViewControllerAnimated:NO];
        return NO;
    }else
    {
        messageIsCompleted = TRUE; // Set this to disable save success pop up when save.
        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ALERT_SAVEROOMDETAIL] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles:[[LanguageManagerV2 sharedLanguageManager] getNo], nil];   
        [alert setTag:tagSaveLeaving];
        [alert show];
        return YES;
        
    }
    
}

#pragma mark -Zoom Process
#pragma mark - LongPress Response
-(void)responseLongPressButton:(UILongPressGestureRecognizer*)sender
{   
    if (sender.state ==UIGestureRecognizerStateBegan){
        
        self.indexDelete=sender.view.tag;
        
        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_delete_confirmation] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles:[[LanguageManagerV2 sharedLanguageManager] getNo], nil];   
        [alert setTag:tagDeletePhoto];
        [alert show];
    }
    
}

-(void)tapImageToZoom:(id)sender
{
    UIButton *zoomButton=(UIButton *)sender;
    zoomPhotoViewV2 *zoomedView=[[zoomPhotoViewV2 alloc] initWithNibName:@"zoomPhotoViewV2" bundle:nil];
    
    if([LogFileManager isLogConsole])
    {
        NSLog(@"tag is %d",(int)zoomButton.tag);
    }
    
    UINavigationController *navZoomPhoto=[[UINavigationController alloc] initWithRootViewController:zoomedView];
    
    [self presentViewController:navZoomPhoto animated:NO completion:nil];
    if ([photoList count]>zoomButton.tag) {
        MessagePhotoModelV2 *zoomedImage=[photoList objectAtIndex:zoomButton.tag];
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [zoomedView.zoomPhoto setImage:[UIImage imageWithData:zoomedImage.message_photo]];
//        });
        zoomedView.imageData = zoomedImage.message_photo;
        //[zoomedView.zoomPhoto setImage:[UIImage imageWithData:zoomedImage.message_photo]];
    }
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_DONE] style:UIBarButtonItemStyleDone target:self action:@selector(tapDoneButtonInZoomView:)];
    zoomedView.navigationItem.leftBarButtonItem = doneButton;
    navZoomPhoto.navigationBar.tintColor = [UIColor blackColor];
    navZoomPhoto.navigationBar.alpha = 0.7f;
    navZoomPhoto.navigationBar.translucent = YES;
    
}
-(void)tapDoneButtonInZoomView:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark - AdHocMessageTodelegate
-(void) transferTheValueOfTableView:(NSMutableArray *)objectArray
{
    
    dataIsChanged = TRUE;
    
//    if ([messageReceiverList count]>0) {
//        [messageReceiverList removeAllObjects ];
//    }
    if(isForwardMessage)
    {
        messageForwardList = [NSMutableArray arrayWithArray:objectArray];
    }
//    else{
//        messageReceiverList = [NSMutableArray arrayWithArray:objectArray];
//    }
    
    [messageTableView reloadData];
}
#pragma mark - button Process
-(IBAction)buttonProcess:(id)sender
{
    // Tap forward button
    if (!isForwardMessage) {
        if(isReplyMessage) {
          return [self sendMessage];
        }
        AdhocMessageDetailV2 *messageDetailView=[[AdhocMessageDetailV2 alloc] initWithNibName:@"AdhocMessageDetailV2" bundle:nil];
        //messageDetailView.delegate=self;
        messageDetailView.isSentMessage=NO;
        messageDetailView.isForwardMessage=YES;
        
        messageDetailView.messageObject=[[MessageModelV2 alloc] init];
        messageDetailView.messageObject.message_topic = self.messageObject.message_topic;
        messageDetailView.messageObject.message_owner_id = [[UserManagerV2 sharedUserManager] currentUser].userId;
        messageDetailView.messageObject.message_content = self.messageObject.message_content;
        messageDetailView.messageObject.message_status = MESSAGE_READ;
        messageDetailView.messageReceiverList = messageReceiverList;
        messageDetailView.photoList = [self.photoList mutableCopy];
        
        //Get subject before change to send view
        UITableViewCell *cellSubject = [self tableView:messageTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:AdhocSubjectIndex inSection:0]];
        UILabel* receiverLabel = (UILabel*)[cellSubject viewWithTag:RECEIVER_SUBJECT_TAG];
        if(receiverLabel.text.length > 0){
            messageDetailView.messageObject.message_topic = receiverLabel.text;
        }
        
        [self.navigationController pushViewController:messageDetailView animated:YES];
    } else {
        // Send message
        [self sendMessage];
    }
    
//    [messageTableView reloadData];

    
}
-(IBAction)buttonReply:(id)sender
{
    if (!isReplyMessage) {
        AdhocMessageDetailV2 *messageDetailView=[[AdhocMessageDetailV2 alloc] initWithNibName:@"AdhocMessageDetailV2" bundle:nil];
        messageDetailView.isSentMessage=NO;
        messageDetailView.isReplyMessage=YES;
        messageDetailView.isForwardMessage=NO;
        messageDetailView.messageObject=[[MessageModelV2 alloc] init];
        messageDetailView.messageObject.message_topic = self.messageObject.message_topic;
        messageDetailView.messageObject.message_owner_id = [[UserManagerV2 sharedUserManager] currentUser].userId;
        messageDetailView.messageObject.message_from_user_name = self.messageObject.message_from_user_name;
        messageDetailView.messageObject.message_status = MESSAGE_READ;
        messageDetailView.messageReceiverList = [[NSMutableArray alloc] initWithObjects:self.messageObject, nil];
        messageDetailView.photoList = [self.photoList mutableCopy];
        int hotelId = [[[UserManagerV2 sharedUserManager] currentUser] userHotelsId];
        NSMutableArray *listAllUser = [[AdHocMessageManagerV2 sharedAdHocMessageManagerV2] loadAllUserListDataByUserHotelId:hotelId];
        if(!self.isForOneMsg) {
            if(messageObjectOfRePly) {
                NSMutableArray *toList = [[NSMutableArray alloc] init];
                for (UserListModelV2 *curUser in listAllUser) {
                    if([curUser.userListName isEqualToString:messageObject.message_from_user_login_name]) {
                        toList = [[NSMutableArray alloc]initWithObjects:curUser, nil];
                        break;
                    }
                }
                messageDetailView.messageForwardList = [[NSMutableArray alloc]  initWithArray:toList];
            }
        } else {
            if(messageObject) {
                NSMutableArray *toList = [[NSMutableArray alloc] init];
                for (UserListModelV2 *curUser in listAllUser) {
                    if([curUser.userListName isEqualToString:messageObject.message_from_user_login_name]) {
                        toList = [[NSMutableArray alloc]initWithObjects:curUser, nil];
                        break;
                    }
                }
                messageDetailView.messageForwardList = [[NSMutableArray alloc]  initWithArray:toList];
            }
        }
        [self.navigationController pushViewController:messageDetailView animated:YES];
        replyButton.hidden = !self.isForOneMsg;
    }
}
-(int) createMessageGroupId
{
    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
    timeFormatter.dateFormat = @"HHmmss";
    int groupId = [[timeFormatter stringFromDate:[NSDate date]] intValue];
    return groupId;
}

-(void)validateMessageView
 {
     NSString *remindMessage = nil;
     if (!messageForwardList || [messageForwardList count]==0) {
         remindMessage = [NSString stringWithString:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_adhocMsg_receiver]];
     }
     
     //validate the content of message.
     if (self.messageObject.message_content.length == 0) {
         if (remindMessage) {
             remindMessage = [remindMessage stringByAppendingFormat:@" & %@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_adhocMsg_message_content]];
         } else {
             remindMessage = [NSString stringWithString:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_adhocMsg_message_content]];
         }
     }
     
     if (remindMessage) {
         remindMessage = [NSString stringWithFormat:@"%@ %@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_adhocMsg_please_input], remindMessage];
         UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:remindMessage delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles: nil]; 
         [alert show];
         return;
     }
     
     ////////
     messageIsCompleted = TRUE;
     if(isDemoMode){
         NSDateFormatter *timeFomarter = [[NSDateFormatter alloc] init];
         [timeFomarter setDateFormat:[ehkConvert getyyyy_MM_dd_HH_mm_ss]];
         NSString *currentDateTime = [timeFomarter stringFromDate:[NSDate date]];
         MBProgressHUD *sendingIndicator = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
         [sendingIndicator setLabelText:[[LanguageManagerV2 sharedLanguageManager] getRaLoadingData]];
         [self.tabBarController.view addSubview:sendingIndicator];
         [sendingIndicator show:YES];
         [sendingIndicator hide:YES];
         [sendingIndicator removeFromSuperview];
         int groupId = [self createMessageGroupId];
         for(UserListModelV2 *userModel in messageForwardList)
         {
             MessageModelV2 *curMessage = [[MessageModelV2 alloc] init];
             curMessage.message_content = self.messageObject.message_content;
             curMessage.message_count_photo_attached = self.messageObject.message_count_photo_attached;
             curMessage.message_from_user_login_name = [[UserManagerV2 sharedUserManager] currentUser].userName;
             curMessage.message_from_user_name = [[UserManagerV2 sharedUserManager] currentUser].userFullName;
             curMessage.message_group_id = groupId;
             curMessage.message_kind = MESSAGE_KIND_INBOX;
             curMessage.message_last_modified = currentDateTime;
             curMessage.message_owner_id = userModel.userListId;
             curMessage.message_status = MESSAGE_UNREAD;
             curMessage.message_topic = self.messageObject.message_topic;
             [[AdHocMessageManagerV2 sharedAdHocMessageManagerV2] insertOfflineMessage:curMessage];
             
             MessageReceiverModelV2 *receiver = [[MessageReceiverModelV2 alloc] init];
             receiver.message_receiver_group_id = groupId;
             receiver.message_receiver_owner_id = [[UserManagerV2 sharedUserManager] currentUser].userId;
             receiver.message_receiver_id = userModel.userListId;
             
             receiver.message_receiver_last_modified = currentDateTime;
             [[AdHocMessageManagerV2 sharedAdHocMessageManagerV2] insertMessageReceiverData:receiver];
             
             MessageModelV2 *sentMessage = [[MessageModelV2 alloc] init];
             sentMessage.message_content = self.messageObject.message_content;
             sentMessage.message_count_photo_attached = self.messageObject.message_count_photo_attached;
             sentMessage.message_from_user_login_name = [[UserManagerV2 sharedUserManager] currentUser].userName;
             sentMessage.message_from_user_name = [[UserManagerV2 sharedUserManager] currentUser].userFullName;
             sentMessage.message_group_id = groupId;
             sentMessage.message_kind = MESSAGE_KIND_SENT;
             sentMessage.message_last_modified = currentDateTime;
             sentMessage.message_owner_id = [[UserManagerV2 sharedUserManager] currentUser].userId;
             sentMessage.message_status = MESSAGE_READ;
             sentMessage.message_topic = self.messageObject.message_topic;
             
             [[AdHocMessageManagerV2 sharedAdHocMessageManagerV2] insertOfflineMessage:sentMessage];
         }
         
         for(MessagePhotoModelV2 *messageImageObject in photoList)
         {
             messageImageObject.message_photo_group_id = groupId;
             [[AdHocMessageManagerV2 sharedAdHocMessageManagerV2] insertMsgPhotoData:messageImageObject];
         }
         
         UIAlertView* alert = [[UIAlertView alloc] initWithTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_MESSAGE_SENDING_SUCCESSFUL] message:nil delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getOK] otherButtonTitles: nil];
         alert.tag = tagSentSuccessful;
         [alert show];
         
         [messageTableView reloadData];
     }
     else{
         if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
             
             HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
             [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_SENDING_DATA]];
             [self.tabBarController.view addSubview:HUD];
             [HUD show:YES];
             int result = 0;
             if(isReplyMessage) {
               result = (int)[[AdHocMessageManagerV2 sharedAdHocMessageManagerV2] postMessage:self.messageObject receivers:messageForwardList photos:photoList];
             } else {
                result = (int)[[AdHocMessageManagerV2 sharedAdHocMessageManagerV2] postMessageDetailsModel:self.messageObject receivers:messageForwardList photos:photoList];
             }
             if (result) {
                 
                 [HUD hide:YES];
                 [HUD removeFromSuperview];
                 
                 UIAlertView* alert = [[UIAlertView alloc] initWithTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_MESSAGE_SENDING_SUCCESSFUL] message:nil delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getOK] otherButtonTitles: nil];
                 alert.tag = tagSentSuccessful;
                 [alert show];
                 
             } else {
                 UIAlertView* alert = [[UIAlertView alloc] initWithTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_SENDING_DATA_FAIL] message:nil delegate:nil cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getOK] otherButtonTitles: nil];
                 [alert show];
                 
                 [HUD hide:YES];
                 [HUD removeFromSuperview];
             }
         }
         messageIsCompleted = FALSE;
     }
 
 }

//-(void)saveData
// {
//     //only for ForwardMessage Type
//     if (isForwardMessage) {
//         BOOL result;
//         //update to MessageReceiver table
//         for (UserListModelV2 *receiverModel in messageObject.messageReceiverList) {
//             MessageReceiverModelV2 *msgReceiver=[[MessageReceiverModelV2 alloc] init];
//             msgReceiver.mr_receiver_id=receiverModel.userListId;
//             msgReceiver.mr_message_id=messageObject.msg_ID;
//             
//             int numberResutl =[[AdHocMessageManagerV2 sharedAdHocMessageManagerV2] updateMessageReceiverData:msgReceiver];
//             if (numberResutl==1) {
//                 result=YES;
//             }else
//             {
//                 result=NO;
//             }
//         }
//         
//         //update to Message table
//         
//         messageObject.msg_Last_Modified=[ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
//         
//         messageObject.msg_Sender_ID=[UserManagerV2 sharedUserManager].currentUser.userId;
//         int numberResult= [[AdHocMessageManagerV2 sharedAdHocMessageManagerV2] updateMessageData:self.messageObject];
//         if (numberResult==1) {
//             result=YES;
//         }else
//         {
//             result=NO;
//         }
//         
//         if (result) {
//             UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_save_success] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles: nil]; 
//             
//             alert.tag = tagSaveSuccessful;
//             [alert show];
//             //         isUpdate=NO;
//         }
//
//     }else
//     {return;}
//     
//  
// 
// }
-(void)sendMessage
{
    if(!isDemoMode){
        if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp] == NO) {
            
            UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:[[LanguageManagerV2 sharedLanguageManager] getNoWifi] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles: nil];
            
            [alert show];
            //        [alert release];
            return;
        }
    }
    
    [self validateMessageView];

    
}
-(void)forwardMessage
{   
    isForwardMessage=YES;
    isSentMessage=NO;
    
    [self changeTitleButton];
 
}

#pragma mark - Photoviewv2delegate
-(void)processPhoto:(UIImage *)photo
{
    if (!photoList) {
        photoList=[[NSMutableArray alloc] init];
    }
    //create the  new Image object
    MessagePhotoModelV2 *messageImageObject=[[MessagePhotoModelV2 alloc] init];
    messageImageObject.message_photo = UIImageJPEGRepresentation(photo,1.0);
    messageImageObject.message_photo_group_id = messageObject.message_group_id;
    [photoList addObject:messageImageObject];
}


#pragma mark - Process buttonPhotoClick
-(void)selectPhotoButton
{
    if ([photoList count] == 6) {
        return;
    } else { 
        PhotoViewV2 *viewPhoto = [[PhotoViewV2 alloc] initWithNibName:@"PhotoViewV2" bundle:Nil];
        viewPhoto.delegate = self;
        [viewPhoto setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_add_hoc_message]];
        UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK] style:UIBarButtonItemStyleBordered target:nil action:nil];
        self.navigationItem.backBarButtonItem = backButton;
        [self.navigationController pushViewController:viewPhoto animated:YES];
    }
}


#pragma mark - UIAlertView Delegate Methods
-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    switch (alertView.tag) {
        case tagSaveLeaving:
        {
            switch (buttonIndex)
            {
                case 0:
                {
                    //YES:
                    [self.navigationController popViewControllerAnimated:YES];
                }
                    break;
                case 1:
                {
                    //NO
                    UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ALERT_DISCARDSAVECOUNTS] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles:[[LanguageManagerV2 sharedLanguageManager] getNo], nil];   
                    alert.tag = tagDiscard;
                    [alert show];
                    
                    
                }
                default:
                    break;
            }
        }
            
            break;
        case tagDiscard:
        {
            switch (buttonIndex)
            {
                case 0:
                {
                    //YES: discard all change, do nothing and go away
                    if (backIsPressed) {
                        // press backbutton
                        [self.navigationController popViewControllerAnimated:YES];
                        backIsPressed = FALSE;
                    } else {
                        // other bottom bar button press
                        [[HomeViewV2 shareHomeView] handleTabbarEventWithTagOfButton:[[NSUserDefaults standardUserDefaults] integerForKey:TAG_OF_EVENT]];
                        
                    }
                }
                    break;
                case 1:
                {
                    // Reset the variable of messageIsCompleted which is set before
                    messageIsCompleted = FALSE;
                    //NO: do nothing, stay here
                    break;
                }
                default:
                    break;
            }
        }
            
            break;
        case tagSaveSuccessful:
        {
            switch (buttonIndex)
            {
                case 0:
                {
                    AdhocMessageMain* adhocScreen =  (AdhocMessageMain*)[self.navigationController.viewControllers objectAtIndex:0];
                    if (adhocScreen) {
                        [adhocScreen setAcceptShowOutbox:YES];
                    }
                    [self.navigationController popToRootViewControllerAnimated:NO];
                    break;
                }
                case 1:
                {
                    
                    //NO
                    
                }
                default:
                    break;
            }
        }
            
            break;
        case tagSentSuccessful: // When user sent successful, go to sent item
        {
            
            AdHocMessageBoxV2 *adhocScreen = nil;
            NSArray *arrayVC = self.navigationController.viewControllers;
            
            for (UIViewController *currentView in arrayVC) {
                if([currentView isKindOfClass:[AdHocMessageBoxV2 class]])
                {
                    adhocScreen = (AdHocMessageBoxV2*)currentView;
                    [self.navigationController popToViewController:currentView animated:YES];
                    break;
                }
            }
            
            break;
        }
            
        case tagDeletePhoto:
        {
            switch (buttonIndex)
            {
                case 0:
                {
                    //YES:
                    if ([photoList count]>indexDelete) {
                        [photoList removeObjectAtIndex:indexDelete];
                    }
                    
                }
                    break;
                case 1:
                {
                    //NO
                }
                default:
                    break;
            }
            break;
        }         
        default:
            break;
    }
    [self.messageTableView reloadData];
}

#pragma  mark - Process Back button
-(void)tapBackButton:(id)sender
{
    if(self.isForOneMsg) {
        replyButton.hidden = !self.isForOneMsg;
    }
    [self.navigationController popViewControllerAnimated:YES];
    
    //Temporary not saving data
    /*
    if (!dataIsChanged) {
        [self.navigationController popViewControllerAnimated:NO];
    }else
    {
        backIsPressed = TRUE;
        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ALERT_SAVEROOMDETAIL] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles:[[LanguageManagerV2 sharedLanguageManager] getNo], nil];   
        [alert setTag:tagSaveLeaving];
        [alert show];
    }*/
}
#pragma mark -  AdHocMessageContentViewV2Delegate 
-(void) messageViewV2DoneWithText:(NSString *) text
{ 
    if ([self.messageObject.message_content isEqualToString:text]) {
        dataIsChanged = TRUE;
    }
    self.messageObject.message_content = text;
    
    [messageTableView reloadData];
    
}
#pragma mark -  Hidden Header view when tap on navigationBar

// hidden header when tap navigationBar
#define heightScale 45
- (void) hiddenHeaderView {
    
    BOOL isHidden = topBarView.hidden;
    [topBarView setHidden:!isHidden];
    
    CGRect frame1 = CGRectZero;
    CGRect frame2 = CGRectZero;
    CGRect frame3 = CGRectZero;

    
    if(!isHidden){
        frame1  = CGRectMake(messageTableView.frame.origin.x, messageTableView.frame.origin.y - heightScale, messageTableView.frame.size.width, messageTableView.frame.size.height);
        frame2  = CGRectMake(messageButton.frame.origin.x,messageButton.frame.origin.y - heightScale, messageButton.frame.size.width, messageButton.frame.size.height);
        frame3  = CGRectMake(replyButton.frame.origin.x,replyButton.frame.origin.y - heightScale, replyButton.frame.size.width, replyButton.frame.size.height);
      
    } else
    {
        frame1  = CGRectMake(messageTableView.frame.origin.x, messageTableView.frame.origin.y + heightScale, messageTableView.frame.size.width, messageTableView.frame.size.height);
        frame2  = CGRectMake(messageButton.frame.origin.x,messageButton.frame.origin.y + heightScale, messageButton.frame.size.width, messageButton.frame.size.height);
        frame3  = CGRectMake(replyButton.frame.origin.x,replyButton.frame.origin.y + heightScale, replyButton.frame.size.width, replyButton.frame.size.height);

    }
    
    [messageTableView setFrame:frame1];
    [messageButton setFrame:frame2];
    [replyButton setFrame:frame3];
}

#pragma mark - iOS 7 Resources
-(void)loadFlatResource
{
    [messageButton setBackgroundImage:[UIImage imageNamed:ImageSendMessage_Flat] forState:UIControlStateNormal];
    [replyButton setBackgroundImage:[UIImage imageNamed:ImageSendMessage_Flat] forState:UIControlStateNormal];
}
@end
