//
//  AdhocMessageMain.h
//  mHouseKeeping
//
//  Created by TMS on 2/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
// This class is used for create a new message
/*
------------------------------------ Design GUI ------------------------------------ 
 in navigation bar : 1 "Back" button,1 "Ad Hoc Message" title, and Wifi status icon.
 the information about User : Hotel Name,Building Number ,Job title, Employee Name.
 Detail: (uitableview)
 To field : 1  "To" label ,1 Name text , 1 Accessory button ; It can be lead to List of other people View (The allowed people name) Default Value : Supervisor.
 
 Subject field : 1 "Subject" label, 1 Subject text , 1 Accessory button;
 As tapped, forward to The 50 list of Subject View( as same as the 50 list of Engineer Case View).
 Message field:
 Photo field : is the same as Photo in Lost and Found View.
 View Messags: 1 image and title "View Messages" and forward to outbox (on day messages).   
 Send button : 1 image and  title "Send".
 
 
 -------------------------------------------------------------------------------------

 ----------------------Business Rule------------------
 Precondition : There is no any new message in here.
                If any new message, back from inbox(Go inbox first).
 To: defauft value is Supervisor.
    Load list of other people who are allowed to receive message.
 Subject :   defauft value is ?
    Load list of Subjects.
 Message : Load the content of message.
 Save this message to Outbox if any field was change and highlight it in Outbox(highlight the "Send" button).
 2 AlertView "Please save before leaving." and "Please discard all inputs." view if tap the No button in the first view.
 If send successfull ?
 
 -----------------------------------------------------
 */
#import <UIKit/UIKit.h>
#import "PhotoScrollViewV2.h"
#import "PhotoViewV2.h"
#import "AdHocMessageToV2.h"
#import "AdHocMessageManagerV2.h"
#import "AdHocMessageSubViewV2.h"
#import "AdHocMessageBoxV2.h"
#import "AdHocMessageContentViewV2.h"
#import "TopbarViewV2.h"
#import "zoomPhotoViewV2.h"
#import "CommonVariable.h"
#import "MBProgressHUD.h"
//#import "HomeViewV2.h"
#import "AccessRight.h"

#define tagDeletePhoto 3

@interface AdhocMessageMain : UIViewController<UITableViewDataSource,UITableViewDelegate,PhotoViewV2Delegate,AdHocMessageToV2Delegate,AdHocMessageContentViewV2Delegate,UIGestureRecognizerDelegate,AdHocMessageSubViewV2Delegate,UIAlertViewDelegate>
{        
    IBOutlet UITableView *messagemainTableView;
    IBOutlet UIButton *sendMessageButton;
    IBOutlet UIButton *viewMessageButton;
    IBOutlet UILabel *lblSend;
    IBOutlet UILabel *lblViewMessage;
   
    NSInteger indexDelete;
    TopbarViewV2 * topBarView;
    MessageModelV2 *messageVariableObject;
    BOOL isOneView;
    UserListModelV2 *supperVisor;
    MBProgressHUD* HUD;
    BOOL acceptShowOutbox;
    BOOL messageIsCompleted; // Messase is sent and will not push sent item screen
    BOOL isBottomBarPressed;
    BOOL isAlreadyShowMessageBox;
    AccessRight *messages;
    
    NSMutableArray *messageReceiverList;
    NSMutableArray *photoList;
}

@property (nonatomic,strong) UserListModelV2 *supperVisor;
@property BOOL isOneView;
@property (nonatomic,strong) MessageModelV2 *messageVariableObject;
@property (nonatomic,strong)TopbarViewV2 * topBarView;
@property   NSInteger indexDelete;


@property (nonatomic,strong)  IBOutlet UIButton *sendMessageButton;
@property (nonatomic,strong) IBOutlet UIButton *viewMessageButton;

@property (nonatomic,strong) IBOutlet UITableView *messagemainTableView;
@property (nonatomic,strong) IBOutlet UILabel *lblSend;
@property (nonatomic,strong) IBOutlet UILabel *lblViewMessage;
@property (nonatomic) BOOL acceptShowOutbox;
//-(void) setCaptionsView ;
-(IBAction)viewMessages:(id)sender;
-(IBAction)sendMessage:(id)sender;
-(void)viewSentItem;

@end
