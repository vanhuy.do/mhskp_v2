//
//  AdhocMessageMain.m
//  mHouseKeeping
//
//  Created by TMS on 2/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AdhocMessageMain.h"
#import "LanguageManager.h"
#import "ehkConvert.h"
//#import "RoomManager.h"
#import "UserManagerV2.h"
#import "CustomAlertViewV2.h"
#import "NetworkCheck.h"
#import "ehkDefines.h"
#import "MyNavigationBarV2.h"
#import "LogFileManager.h"
#import "AccessRight.h"
#import "ehkDefinesV2.h"
#import "DeviceManager.h"

#define HEIGHT 55
#define WIDTH  85
#define WIDTHIMAGE 85
#define DELETEIMAGE @"icon_garbage.png"
#define NEXTIMAGE @"gray_arrow_36x36.png"
#define BACKGROUNDIMAGE  @"bg.png"
#define FONTLABEL @"Arial-BoldMT"
#define FONTLABELSIZE 17
#define FONTTEXTSIZE 15
#define REDLABEL 6.0f/255.0f
#define GREENLABEL 62.0f/255.0f
#define BLUELABEL 127.0f/255.0f
#define REDTEXT 102.0f/255.0f
#define GREENTEXT 102.0f/255.0f
#define BLUETEXT 102.0f/255.0f
#define KTAGMESSAGE 2
#define KTAGMESSAGETEXT 3
#define sizeTitle           22
#define tagDiscard 14
#define tagSaveLeaving 12
#define tagSaveSuccessful 15

// Private method
@interface AdhocMessageMain (PrivateMethods)
- (void)validateMessageView;

@end


@implementation AdhocMessageMain
@synthesize messagemainTableView;
@synthesize sendMessageButton;
@synthesize viewMessageButton;
@synthesize lblSend;
@synthesize lblViewMessage;
@synthesize indexDelete;
@synthesize topBarView;
@synthesize messageVariableObject,isOneView,supperVisor;
@synthesize acceptShowOutbox;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - Load Access Right

-(void) loadAccessRights
{
    AccessRightModel *currentUserAccessRight = [[UserManagerV2 sharedUserManager] currentUserAccessRight];
    messages = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.message];
}

#pragma mark - View lifecycle

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    int countUnreadMessage = [[HomeViewV2 shareHomeView] countUnreadMessageByUserId:[[UserManagerV2 sharedUserManager] currentUser].userId];
    if(countUnreadMessage > 0 && !isAlreadyShowMessageBox){
        isAlreadyShowMessageBox = YES;
        [self viewMessages:viewMessageButton];
    }
    [[HomeViewV2 shareHomeView].slidingMenuBar showAll];
    [self.navigationController setNavigationBarHidden:NO];
    [self.messagemainTableView reloadData];
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
   
    
    
    //    if(self.acceptShowOutbox){
    //        self.acceptShowOutbox = NO;
    //        [self viewSentItem];
    //    }
    
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Resize content views of current VC to fit with Navigation bar and tabbar
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    //Resize layouts for iOS 7
    /*
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        CGRect btnViewMessageFrame = viewMessageButton.frame;
        btnViewMessageFrame.origin.y += 20;
        [viewMessageButton setFrame:btnViewMessageFrame];
        
        CGRect btnSendMessageFrame = sendMessageButton.frame;
        btnSendMessageFrame.origin.y += 20;
        [sendMessageButton setFrame:btnSendMessageFrame];
        
        CGRect lblSendFrame = lblSend.frame;
        lblSendFrame.origin.y += 20;
        [lblSend setFrame:lblSendFrame];
        
        CGRect lblViewMessageFrame = lblViewMessage.frame;
        lblViewMessageFrame.origin.y += 20;
        [lblViewMessage setFrame:lblViewMessageFrame];
        
    }*/
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        //Clear redundant view for header, footter
        CGRect frameHeaderFooter = messagemainTableView.tableHeaderView.frame;
        frameHeaderFooter.size.height = 0.1f;
        UIView *headerView = [[UIView alloc] initWithFrame:frameHeaderFooter];
        [messagemainTableView setTableHeaderView:headerView];
        
        frameHeaderFooter = messagemainTableView.tableFooterView.frame;
        frameHeaderFooter.size.height = 0.1f;
        UIView *footerView = [[UIView alloc] initWithFrame:frameHeaderFooter];
        [messagemainTableView setTableFooterView:footerView];
        
        //Change images
        [viewMessageButton setBackgroundImage:[UIImage imageNamed:ImageViewMessage_Flat] forState:UIControlStateNormal];
        [sendMessageButton setBackgroundImage:[UIImage imageNamed:ImageSendMessage_Flat] forState:UIControlStateNormal];
    }
    
    isAlreadyShowMessageBox = NO;
    
    //Load message access right
    [self loadAccessRights];
    
    if (topBarView == nil) {
        topBarView = [[TopbarViewV2 alloc] initViewWithSuperModel:[CommonVariable sharedCommonVariable].superRoomModel];
        [self.view addSubview:topBarView];
    }
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self.lblViewMessage setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_view_message]];
    [self.lblSend setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ios_message_send]];
    
    UIView *titleViewBar = [[UIView alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
    UIButton *titleBarButtonFirst = [[UIButton alloc] initWithFrame:CGRectMake(-10, 8, FRAME_BUTTON_TOPBAR.size.width, 30)];
    [titleBarButtonFirst setTag:1];
    titleBarButtonFirst.titleLabel.backgroundColor = [UIColor clearColor];
    [titleBarButtonFirst.titleLabel setFont: FONT_BUTTON_TOPBAR];
    titleBarButtonFirst.titleLabel.textAlignment = NSTextAlignmentLeft;
    [titleBarButtonFirst.titleLabel setAdjustsFontSizeToFitWidth:YES];
    titleBarButtonFirst.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
    
    [titleBarButtonFirst.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
    [titleBarButtonFirst.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
    [titleBarButtonFirst setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_MESSAGE] forState:UIControlStateNormal];
    [titleBarButtonFirst addTarget:self action:@selector(hiddenHeaderView) forControlEvents:UIControlEventTouchUpInside];
    [titleViewBar addSubview:titleBarButtonFirst];
    
    self.navigationItem.titleView = titleViewBar;
    
    //Move title to center vertical
    CGRect titleViewBounds = self.navigationItem.titleView.bounds;
    int moveLeftTitle = 0;
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        moveLeftTitle = 10;
    }
    self.navigationItem.titleView.bounds = CGRectMake(titleViewBounds.origin.x + moveLeftTitle , Y_TOPBAR_TITLE, titleViewBounds.size.width, titleViewBounds.size.height);
    
    //back button navigation
    self.navigationItem.leftBarButtonItem = [MyNavigationBarV2 createBackButtonWithTarget:self selector:@selector(tapBackButton) forControlEvents:UIControlEventTouchUpInside];
    
    self.isOneView = YES;
    
    [self performSelector:@selector(loadData) withObject:nil afterDelay:0.0f];
    
}


- (void)tapBackButton {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)loadData
{
    if (!HUD) {
        HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
        //HUD.delegate=self;
    }
    
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES];
    
    //Update unread message
    [[HomeViewV2 shareHomeView] messageReciever:NO callFrom:IS_PUSHED_FROM_MESSAGE_MAIN];
    
    [self.messagemainTableView setScrollEnabled:NO];
    [self.messagemainTableView setBackgroundColor:[UIColor clearColor]];
    
    //    UIColor *bgColor = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:BACKGROUNDIMAGE]];
    //    self.view.backgroundColor = bgColor;
    
    [self.messagemainTableView setBackgroundColor:[UIColor clearColor]];
    
    [HUD hide:YES];
    [HUD removeFromSuperview];
    
    //HaoTran[20130531/Check Access right] - Check to set only view for access right
//    if(messages.isAllowedView)
//    {
//        [sendMessageButton setUserInteractionEnabled:NO];
//        [lblSend setAlpha:0.5];
//        [sendMessageButton setAlpha:0.5];
//    }
//    else
//    {
//        [sendMessageButton setUserInteractionEnabled:YES];
//        [lblSend setAlpha:1];
//        [sendMessageButton setAlpha:1];
//    }
    //HaoTran[20130531/Check Access right] - END
    
    [sendMessageButton setUserInteractionEnabled:YES];
    [lblSend setAlpha:1];
    [sendMessageButton setAlpha:1];
    
    //set selected tab message
    [[HomeViewV2 shareHomeView] setSelectedButton:tagOfMessageButton];
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    // [(UIButton *)self.navigationItem.titleView setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_add_hoc_message] forState:UIControlStateNormal];
    [self.lblViewMessage setText:[[LanguageManagerV2 sharedLanguageManager]  getStringLanguageByStringId:L_view_message]];
    [self.lblSend setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ios_message_send]];
    if (!self.isOneView) {
        self.isOneView=YES;
    }
    [topBarView refresh:[CommonVariable sharedCommonVariable].superRoomModel];
    //get AdHocmessage
    [[AdHocMessageManagerV2 sharedAdHocMessageManagerV2] getUserDetailsRoutine:[UserManagerV2 sharedUserManager].currentUser.userId withHotel:[UserManagerV2 sharedUserManager].currentUser.userHotelsId withTypeID:1 AndPercentView:nil];
    [self.messagemainTableView reloadData];
    
}

// Stop loading data
- (void)stopLoadingData {
    [HUD hide:YES];
    [HUD removeFromSuperview];
    [self.messagemainTableView reloadData];
}

// Remove the current message and create a new blank message object
-(void)clearDataAsLogout
{
    self.messageVariableObject = nil;
    self.messageVariableObject = [[MessageModelV2 alloc] init];
    self.messageVariableObject.message_owner_id = [UserManagerV2 sharedUserManager].currentUser.userId;
    if(messageReceiverList) {
        [messageReceiverList removeAllObjects];
    }
    
    if(photoList){
        [photoList removeAllObjects];
    }
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma UITableView DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int rowinSection=0;
    if (section==0) {
        rowinSection=2;
    }else
    {
        rowinSection=1;
    }
    return rowinSection;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *toRowIdentifier = @"toIdentifier";
    static NSString *subjectRowIdentifier = @"subjectIdentifier";
    static NSString *photoSectionIdentifier=@"photoIdentifier";
    static NSString *messageSectionIdentifier=@"messageIdentifier";
    
    UIColor *labelColor=[UIColor colorWithRed:REDLABEL green:GREENLABEL blue:BLUELABEL alpha:1];
    
    
    UIColor *textColor=[UIColor colorWithRed:REDTEXT green:GREENTEXT blue:BLUETEXT alpha:1];
    UIImage *nextImage=[UIImage imageNamed:NEXTIMAGE];
    UIImageView *nextImageView=[[UIImageView alloc] initWithImage:nextImage] ;
    nextImageView.frame=CGRectMake(260, 5, 25, 25);
    UITableViewCell *cell=nil;
    if (indexPath.section==0) {
        if (indexPath.row==0) {
            cell=[tableView dequeueReusableCellWithIdentifier:toRowIdentifier];
        } else
        {
            cell=[tableView dequeueReusableCellWithIdentifier:subjectRowIdentifier ];
        }
    } else if (indexPath.section==1) {
        if (indexPath.row==0) {
            cell=[tableView dequeueReusableCellWithIdentifier:messageSectionIdentifier];
        }
    }
    else
    {
        if (indexPath.row==0)
        {
            cell=[tableView dequeueReusableCellWithIdentifier:photoSectionIdentifier ];
        }
    }
    
    PhotoScrollViewV2 *photoViewScroll=[[PhotoScrollViewV2 alloc] init];
    if (cell==nil) {
        if (indexPath.section==0) {
            if (indexPath.row==0) {
                cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:toRowIdentifier];
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                [cell setAccessoryView:nextImageView];
            }else
            {
                cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:subjectRowIdentifier];
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                [cell setAccessoryView:nextImageView];
            }
        } else if (indexPath.section==1) {
            if (indexPath.row==0) {
                cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:messageSectionIdentifier];
                UILabel *messageLabel=[[UILabel alloc] initWithFrame:CGRectMake(10,0, 100,30 )];
                messageLabel.tag=KTAGMESSAGE;
                messageLabel.backgroundColor = [UIColor clearColor];
                UILabel *messageText=[[UILabel alloc] initWithFrame:CGRectMake(10,30 ,280, 30)];
                messageText.tag=KTAGMESSAGETEXT;
                messageText.backgroundColor = [UIColor clearColor];
                nextImageView.frame = CGRectMake(267,5, 25, 25);
                [cell.contentView addSubview:messageLabel];
                [cell.contentView addSubview:messageText];
                [cell setAccessoryView:nextImageView];
                
            }
        }
        else
        {
            if (indexPath.row==0)
            {
                cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:photoSectionIdentifier];
                
                
                photoViewScroll.delegate=self;
                photoViewScroll.frame=CGRectMake(5,5, 290, 70);
                photoViewScroll.backgroundColor=[UIColor clearColor];
                photoViewScroll.tag = 100;
                
                photoViewScroll.backgroundColor=[UIColor clearColor];
                [cell setBackgroundColor:[UIColor colorWithWhite:0.5 alpha:0.5]];
                [cell.contentView addSubview:photoViewScroll];
            }
            
        }
    }
    //add data
    if (indexPath.section==0) {
        // To field
        if (indexPath.row==0) {
            cell.textLabel.text= [NSString stringWithFormat:@"%@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_to]];
            [cell.textLabel setTextColor:labelColor];
            [cell.detailTextLabel setFont:[UIFont fontWithName:FONTLABEL size:FONTLABELSIZE]];
            [cell.detailTextLabel setTextColor:textColor];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            NSMutableString *stringTo=[NSMutableString stringWithString:@""];
            if ([messageReceiverList count]>0) {
                for (UserDetailsModelV2 * toObject in messageReceiverList) {
                    if ([messageReceiverList count]>1) {
                        [stringTo appendFormat:@"%@; ",toObject.userDetailsFullName];
                    } else {
                        [stringTo appendFormat:@"%@",toObject.userDetailsFullName];
                    }
                }
            }
            [cell.detailTextLabel setText:stringTo];
            // Subject field
        } else {
            cell.textLabel.text= [NSString stringWithFormat:@"%@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_subject]];
            [cell.textLabel setTextColor:labelColor];
            [cell.detailTextLabel setFont:[UIFont fontWithName:FONTLABEL size:FONTLABELSIZE]];
            [cell.detailTextLabel setTextColor:textColor];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            if (messageVariableObject.message_topic.length > 0) {
                [cell.detailTextLabel setText:messageVariableObject.message_topic];
            } else {
                [cell.detailTextLabel setText:@""];
            }
        }
    } else if (indexPath.section==1) {
        // Message content field
        if (indexPath.row==0) {
            UILabel *lbMessage=(UILabel*)[cell.contentView viewWithTag:KTAGMESSAGE];
            [lbMessage setFont:[UIFont fontWithName:FONTLABEL size:FONTLABELSIZE]];
            lbMessage.text= [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_MESSAGE];
            [lbMessage setTextColor:labelColor];
            UILabel *lbMessageText=(UILabel*)[cell.contentView viewWithTag:KTAGMESSAGETEXT];
            [lbMessageText setBackgroundColor:[UIColor clearColor]];
            if (messageVariableObject.message_content.length > 0) {
                lbMessageText.text = messageVariableObject.message_content;
            } else {
                lbMessageText.text = @"";
            }
            [lbMessageText setFont:[UIFont fontWithName:FONTLABEL size:FONTLABELSIZE]];
            [lbMessageText setTextColor:textColor];
        }
    }else
    {
        if (indexPath.row==0) {
            
            //Photo
            
            photoViewScroll = (PhotoScrollViewV2*)[cell.contentView viewWithTag:100];
            if ([photoViewScroll.subviews count]>0) {
                for (UIView* subview in photoViewScroll.subviews) {
                    [subview removeFromSuperview];
                }
            }
            //add data in here
            
            //first display the photo Button, and add more images if any. If the number of images=6 the photo Button is gray.
            CGFloat cxLocation = 0;
            if ([photoList count] == 0) {
                
                cxLocation+=(photoViewScroll.frame.size.width - 80)/2;
                UIImage *photoImage=[UIImage imageNamed:PHOTOIMAGE];
                UIButton *photoButton=[[UIButton alloc] initWithFrame:CGRectMake(cxLocation,5,WIDTH, HEIGHT)];
                [photoButton setBackgroundImage:photoImage forState:UIControlStateNormal];
                [photoButton addTarget:self action:@selector(selectPhotoButton) forControlEvents:UIControlEventTouchDown];
                [photoViewScroll addSubview:photoButton];
                
                
                
            }else
            {
                if ([photoList count] > 0 ) {
                    if ([photoList count] < 6) {
                        cxLocation +=10;
                        UIImage *photoImage=[UIImage imageNamed:PHOTOIMAGE];
                        UIButton *photoButton=[[UIButton alloc] initWithFrame:CGRectMake(cxLocation, 5,WIDTH, HEIGHT)];
                        [photoButton setBackgroundImage:photoImage forState:UIControlStateNormal];
                        [photoButton addTarget:self action:@selector(selectPhotoButton) forControlEvents:UIControlEventTouchDown];
                        
                        [photoViewScroll addSubview:photoButton];
                        
                        cxLocation+=WIDTH;
                        
                    }
                    
                    for (int i=0; i < [photoList count]; i++) {
                        MessagePhotoModelV2 *photoImage = [photoList objectAtIndex:i];
                        cxLocation +=10;
                        UIButton *photoButton=[[UIButton alloc] initWithFrame:CGRectMake(cxLocation,5,WIDTHIMAGE , HEIGHT)];
                        photoButton.tag=i;
                        [photoButton setBackgroundImage:[UIImage imageWithData:photoImage.message_photo] forState:UIControlStateNormal];
                        UILongPressGestureRecognizer *longpressGesture=[[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(responseLongPressButton:)];
                        longpressGesture.delegate=self;
                        longpressGesture.minimumPressDuration=1;
                        [photoButton addGestureRecognizer:longpressGesture];
                        [photoButton addTarget:self action:@selector(tapImageToZoom:) forControlEvents:UIControlEventTouchUpInside];
                        [photoViewScroll addSubview:photoButton];
                        cxLocation+=WIDTH;
                    }
                    
                }
                
                
            }
            [photoViewScroll setContentSize:CGSizeMake(cxLocation, HEIGHT)];
            
        }
    }
    
    return cell;
}
//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//    return 1;
//}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{   float rowHeight=0;
    if (indexPath.section==0) {
        rowHeight=44;
    }
    else if (indexPath.section==1)
    {
        rowHeight=70;
    }
    else
    {
        rowHeight=70;
    }
    return rowHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==0) {
        if (indexPath.row==0) {
            //click ToList
            AdHocMessageToV2 *toMessageView=[[AdHocMessageToV2 alloc] initWithNibName:@"AdHocMessageToV2" bundle:Nil];
            toMessageView.delegate=self;
            if ([messageReceiverList count] > 0) {
                toMessageView.tappedDataArray = messageReceiverList ;
            }
            
            UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK] style:UIBarButtonItemStyleBordered target:nil action:nil];
            self.navigationItem.backBarButtonItem = backButton;
            [self.navigationController pushViewController:toMessageView animated:YES];
            
        }else
        {
            //click Subject Lists
            AdHocMessageSubViewV2 *subjectView=[[AdHocMessageSubViewV2 alloc] initWithNibName:@"AdHocMessageSubViewV2" bundle:Nil];
            subjectView.delegate = self;
            
            UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK] style:UIBarButtonItemStyleBordered target:nil action:nil];
            self.navigationItem.backBarButtonItem = backButton;
            
            [self.navigationController pushViewController:subjectView animated:YES];
            
        }
    }
    else if (indexPath.section==1)
    {
        //Remove check isAllowedView
        //if (indexPath.row == 0 && !messages.isAllowedView) {
        if (indexPath.row == 0) {
            //Click text message
            AdHocMessageContentViewV2 *messageContentView=[[AdHocMessageContentViewV2 alloc] initWithNibName:@"AdHocMessageContentViewV2" bundle:Nil];
            messageContentView.delegate=self;
            UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK] style:UIBarButtonItemStyleBordered target:nil action:nil];
            self.navigationItem.backBarButtonItem = backButton;
            messageContentView.contentMessage = messageVariableObject.message_content;
            messageContentView.isFromContent = YES;
            [self.navigationController pushViewController:messageContentView animated:YES];
        }
    }
    else
    {
        if (indexPath.row==0) {
            //click Photo row
        }
    }
    
}

#pragma mark - Process buttonPhotoClick
-(void)selectPhotoButton
{
    //Check access right only view
//    if(messages.isAllowedView){
//        return;
//    }
    
    if ([photoList count] == 6) {
        return;
    }else
    {
        PhotoViewV2 *viewPhoto=[[PhotoViewV2 alloc] initWithNibName:@"PhotoViewV2" bundle:Nil];
        viewPhoto.delegate=self;
        [viewPhoto setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_add_hoc_message]];
        
        //UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK] style:UIBarButtonItemStyleBordered target:nil action:nil];
        //self.navigationItem.backBarButtonItem = backButton;
        
        [viewPhoto setHidesBottomBarWhenPushed:YES];
        [self.navigationController pushViewController:viewPhoto animated:NO];
        
    }
    
    
}

#pragma mark - Photoviewv2delegate
-(void)processPhoto:(UIImage *)photo
{
    if (!photoList) {
        photoList = [[NSMutableArray alloc] init];
    }
    //create the  new Image object
    MessagePhotoModelV2 *messageImageObject=[[MessagePhotoModelV2 alloc] init];
    messageImageObject.message_photo = UIImageJPEGRepresentation(photo,1.0);
    
    if(!isDemoMode){
        messageImageObject.message_photo_group_id = messageVariableObject.message_group_id;
    }
    [photoList addObject:messageImageObject];
}

#pragma mark - AdHocMessageTodelegate
-(void) transferTheValueOfTableView:(NSMutableArray *)objectArray
{
    if (messageReceiverList) {
        [messageReceiverList removeAllObjects];
    }
    
    //messageVariableObject.messageReceiverList=[[NSMutableArray alloc] init];
    messageReceiverList = objectArray;
    [messagemainTableView reloadData];
}

#pragma mark - Message subject Item ViewV2Delegate
-(void)selectItem: (MessageSubjectItemModelV2 *)itemTemp
{
    if (itemTemp) {
        if(messageVariableObject == nil){
            messageVariableObject = [[MessageModelV2 alloc] init];
            messageVariableObject.message_owner_id = [UserManagerV2 sharedUserManager].currentUser.userId;
        }
        
        if([[LanguageManagerV2 sharedLanguageManager] currentLanguageIsEqualTo:@"%@", ENGLISH_LANGUAGE]){
            messageVariableObject.message_topic = itemTemp.smiName;
        } else {
            messageVariableObject.message_topic = itemTemp.smiNameLang;
        }
    }
    
    [messagemainTableView reloadData];
}

#pragma mark - buttons Process
-(IBAction)viewMessages:(id)sender
{
    //self.acceptShowOutbox = NO;
    
    AdHocMessageBoxV2 *messageboxView=[[AdHocMessageBoxV2 alloc] initWithNibName:@"AdHocMessageBoxV2" bundle:nil];
    messageboxView.isInBox = YES;
    messageboxView.isOutBox = NO;
    //[messageboxView showTitleBox];
    self.isOneView = NO;
    
    [self.navigationController pushViewController:messageboxView animated:NO];
}
-(void)viewSentItem {
    AdHocMessageBoxV2 *messageboxView=[[AdHocMessageBoxV2 alloc] initWithNibName:@"AdHocMessageBoxV2" bundle:nil];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK] style:UIBarButtonItemStyleBordered target:nil action:nil];
    self.navigationItem.backBarButtonItem = backButton;
    messageboxView.isInBox = NO;
    messageboxView.isOutBox = YES;
    //[messageboxView showTitleBox];
    self.isOneView = NO;
    
    [self.navigationController pushViewController:messageboxView animated:YES];
}

-(int) createMessageGroupId
{
    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
    timeFormatter.dateFormat = @"HHmmss";
    int groupId = [[timeFormatter stringFromDate:[NSDate date]] intValue];
    return groupId;
}

-(void)validateMessageView
{
    NSString *remindMessage = nil;
    if (!messageReceiverList || [messageReceiverList count] == 0) {
        remindMessage = [NSString stringWithString:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_adhocMsg_receiver]];
    }
    
    // No need validate subject field
    //    //validate the subject field.
    //    if (!self.messageVariableObject.selectedSubjectItem && (self.messageVariableObject.msg_Topic.length == 0)) {
    //        if (remindMessage) {
    //            remindMessage = [remindMessage stringByAppendingFormat:@" & %@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_adhocMsg_subject]];
    //        } else {
    //            remindMessage = [NSString stringWithString:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_adhocMsg_subject]];
    //        }
    //
    //    }
    //validate the content of message.
    if (self.messageVariableObject.message_content.length==0) {
        if (remindMessage) {
            remindMessage = [remindMessage stringByAppendingFormat:@" & %@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_adhocMsg_message_content]];
        } else {
            remindMessage = [NSString stringWithString:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_adhocMsg_message_content]];
        }
    }
    
    if (remindMessage) {
        remindMessage = [NSString stringWithFormat:@"%@ %@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_adhocMsg_please_input], remindMessage];
        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:remindMessage delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles: nil];
        
        [alert show];
        
    } else {
        NSDateFormatter *timeFomarter = [[NSDateFormatter alloc] init];
        [timeFomarter setDateFormat:[ehkConvert getyyyy_MM_dd_HH_mm_ss]];
        NSString *currentDateTime = [timeFomarter stringFromDate:[NSDate date]];
        messageIsCompleted = TRUE;
        if(isDemoMode){
            int groupId = [self createMessageGroupId];
            for(UserDetailsModelV2 *userModel in messageReceiverList)
            {
                MessageModelV2 *curMessage = [[MessageModelV2 alloc] init];
                curMessage.message_content = messageVariableObject.message_content;
                curMessage.message_count_photo_attached = messageVariableObject.message_count_photo_attached;
                curMessage.message_from_user_login_name = [[UserManagerV2 sharedUserManager] currentUser].userName;
                curMessage.message_from_user_name = [[UserManagerV2 sharedUserManager] currentUser].userFullName;
                curMessage.message_group_id = groupId;
                curMessage.message_kind = MESSAGE_KIND_INBOX;
                curMessage.message_last_modified = currentDateTime;
                curMessage.message_owner_id = userModel.userDetailsId.integerValue;
                curMessage.message_status = MESSAGE_UNREAD;
                curMessage.message_topic = messageVariableObject.message_topic;
                [[[AdHocMessageManagerV2 alloc] init] insertOfflineMessage:curMessage];
                
                MessageReceiverModelV2 *receiver = [[MessageReceiverModelV2 alloc] init];
                receiver.message_receiver_group_id = groupId;
                receiver.message_receiver_owner_id = [[UserManagerV2 sharedUserManager] currentUser].userId;
                receiver.message_receiver_id = userModel.userDetailsId;
                
                receiver.message_receiver_last_modified = currentDateTime;
                [[[AdHocMessageManagerV2 alloc] init] insertMessageReceiverData:receiver];
                
                MessageModelV2 *sentMessage = [[MessageModelV2 alloc] init];
                sentMessage.message_content = messageVariableObject.message_content;
                sentMessage.message_count_photo_attached = messageVariableObject.message_count_photo_attached;
                sentMessage.message_from_user_login_name = [[UserManagerV2 sharedUserManager] currentUser].userName;
                sentMessage.message_from_user_name = [[UserManagerV2 sharedUserManager] currentUser].userFullName;
                sentMessage.message_group_id = groupId;
                sentMessage.message_kind = MESSAGE_KIND_SENT;
                sentMessage.message_last_modified = currentDateTime;
                sentMessage.message_owner_id = [[UserManagerV2 sharedUserManager] currentUser].userId;
                sentMessage.message_status = MESSAGE_READ;
                sentMessage.message_topic = messageVariableObject.message_topic;
                
                [[[AdHocMessageManagerV2 alloc] init] insertOfflineMessage:sentMessage];
            }
            
            for(MessagePhotoModelV2 *messageImageObject in photoList)
            {
                messageImageObject.message_photo_group_id = groupId;
                [[AdHocMessageManagerV2 sharedAdHocMessageManagerV2] insertMsgPhotoData:messageImageObject];
            }
            
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_MESSAGE_SENDING_SUCCESSFUL] message:nil delegate:nil cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getOK] otherButtonTitles: nil];
            [alert show];
            
            // Clear the data
            [self clearDataAsLogout];
        }
        else{
            if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]) {
                
                MBProgressHUD *sendingIndicator = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
                [sendingIndicator setLabelText:[[LanguageManagerV2 sharedLanguageManager] getRaLoadingData]];
                [self.tabBarController.view addSubview:sendingIndicator];
                [sendingIndicator show:YES];
                
                //Post message
                int result = (int)[[AdHocMessageManagerV2 sharedAdHocMessageManagerV2] postMessageDetailsModel:messageVariableObject receivers:messageReceiverList photos:photoList];
                
                [sendingIndicator hide:YES];
                [sendingIndicator removeFromSuperview];
                
                if (result > 0) {
                    // Message sent succesful
                    messageVariableObject.message_kind = MESSAGE_KIND_SENT;
                    messageVariableObject.message_id = result;
                    
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_MESSAGE_SENDING_SUCCESSFUL] message:nil delegate:nil cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getOK] otherButtonTitles: nil];
                    [alert show];
                    
                    // Clear the data
                    [self clearDataAsLogout];
                    
                } else {
                    // Message sent fail
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_SENDING_DATA_FAIL] message:nil delegate:nil cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getOK] otherButtonTitles: nil];
                    [alert show];
                }
            }
        }
        
        [messagemainTableView reloadData];
        
        messageIsCompleted = FALSE;
    }
    
}


-(IBAction)sendMessage:(id)sender
{
    //validate the to field.
    if(!isDemoMode){
        if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp] == NO) {
            
            UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:[[LanguageManagerV2 sharedLanguageManager] getNoWifi] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles: nil];
            
            [alert show];
            return;
        }
    }
    
    [self validateMessageView];
    
}

#pragma mark -  AdHocMessageContentViewV2Delegate
// Call when user complete input message content
-(void) messageViewV2DoneWithText:(NSString *) text
{
    if(messageVariableObject == nil){
        messageVariableObject = [[MessageModelV2 alloc] init];
        if(!isDemoMode)
            messageVariableObject.message_owner_id = [UserManagerV2 sharedUserManager].currentUser.userId;
    }
    
    self.messageVariableObject.message_content = text;
    [messagemainTableView reloadData];
    
}
#pragma mark - LongPress Response
-(void)responseLongPressButton:(UILongPressGestureRecognizer*)sender
{
    if (sender.state ==UIGestureRecognizerStateBegan){
        
        self.indexDelete=sender.view.tag;
        
        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_delete_confirmation] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles:[[LanguageManagerV2 sharedLanguageManager] getNo], nil];
        [alert setTag:tagDeletePhoto];
        [alert show];
    }
    
}

-(void)tapImageToZoom:(id)sender
{
    UIButton *zoomButton=(UIButton *)sender;
    zoomPhotoViewV2 *zoomedView=[[zoomPhotoViewV2 alloc] initWithNibName:@"zoomPhotoViewV2" bundle:nil];
    
    UINavigationController *navZoomPhoto=[[UINavigationController alloc] initWithRootViewController:zoomedView];
    
    [self presentViewController:navZoomPhoto animated:NO completion:nil];
    if ([photoList count] > zoomButton.tag) {
        MessagePhotoModelV2 *zoomedImage=[photoList objectAtIndex:zoomButton.tag];
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [zoomedView.zoomPhoto setImage:[UIImage imageWithData:zoomedImage.message_photo]];
//        });
        zoomedView.imageData = zoomedImage.message_photo;
        //[zoomedView.zoomPhoto setImage:[UIImage imageWithData:zoomedImage.message_photo]];
        
        
    }
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_DONE] style:UIBarButtonItemStyleDone target:self action:@selector(tapDoneButtonInZoomView:)];
    zoomedView.navigationItem.leftBarButtonItem = doneButton;
    navZoomPhoto.navigationBar.tintColor = [UIColor blackColor];
    navZoomPhoto.navigationBar.alpha = 0.7f;
    navZoomPhoto.navigationBar.translucent = YES;
    
}
-(void)tapDoneButtonInZoomView:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark -  Hidden Header view when tap on navigationBar

// hidden header when tap navigationBar
#define heightScale 45
- (void) hiddenHeaderView {
    
    //BOOL isHidden = topBarView.hidden;
    //[topBarView setHidden:!isHidden];
    
    CGRect frame1 = CGRectZero;
    CGRect frame2 = CGRectZero;
    CGRect frame3 = CGRectZero;
    CGRect frame4 = CGRectZero;
    CGRect frame5 = CGRectZero;
    
    BOOL isHidden = (topBarView.alpha == 0.0f);
    if(!isHidden){
        frame1  = CGRectMake(messagemainTableView.frame.origin.x, messagemainTableView.frame.origin.y - heightScale, messagemainTableView.frame.size.width, messagemainTableView.frame.size.height);
        frame2  = CGRectMake(viewMessageButton.frame.origin.x,viewMessageButton.frame.origin.y - heightScale, viewMessageButton.frame.size.width, viewMessageButton.frame.size.height);
        
        frame3  = CGRectMake(sendMessageButton.frame.origin.x,sendMessageButton.frame.origin.y - heightScale, sendMessageButton.frame.size.width, sendMessageButton.frame.size.height);
        frame4  = CGRectMake(lblViewMessage.frame.origin.x,lblViewMessage.frame.origin.y - heightScale, lblViewMessage.frame.size.width, lblViewMessage.frame.size.height);
        frame5  = CGRectMake(lblSend.frame.origin.x,lblSend.frame.origin.y - heightScale, lblSend.frame.size.width, lblSend.frame.size.height);
    } else {
        frame1  = CGRectMake(messagemainTableView.frame.origin.x, messagemainTableView.frame.origin.y + heightScale, messagemainTableView.frame.size.width, messagemainTableView.frame.size.height);
        frame2  = CGRectMake(viewMessageButton.frame.origin.x,viewMessageButton.frame.origin.y + heightScale, viewMessageButton.frame.size.width, viewMessageButton.frame.size.height);
        
        frame3  = CGRectMake(sendMessageButton.frame.origin.x,sendMessageButton.frame.origin.y + heightScale, sendMessageButton.frame.size.width, sendMessageButton.frame.size.height);
        frame4  = CGRectMake(lblViewMessage.frame.origin.x,lblViewMessage.frame.origin.y + heightScale, lblViewMessage.frame.size.width, lblViewMessage.frame.size.height);
        frame5  = CGRectMake(lblSend.frame.origin.x,lblSend.frame.origin.y + heightScale, lblSend.frame.size.width, lblSend.frame.size.height);
    }
    
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelegate:self];
    
    [messagemainTableView setFrame:frame1];
    [viewMessageButton setFrame:frame2];
    [sendMessageButton setFrame:frame3];
    [lblViewMessage setFrame:frame4];
    [lblSend setFrame:frame5];
    
    CGRect f = topBarView.frame;
    if(topBarView.alpha == 0.0f)
    {
        f.size.height = 60;
        [topBarView setFrame:f];
        [topBarView setAlpha:1.0f];
    }
    else
    {
        f.size.height = 0;
        [topBarView setFrame:f];
        [topBarView setAlpha:0.0f];
    }
    
    [UIView commitAnimations];
    
}

#pragma mark - UIAlertView Delegate Methods
-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    switch (alertView.tag) {
        case tagDeletePhoto:
        {
            switch (buttonIndex)
            {
                case 0:
                {
                    //YES:
                    if ([photoList count] > indexDelete) {
                        [photoList removeObjectAtIndex:indexDelete];
                    }
                    
                }
                    break;
                case 1:
                {
                    //NO
                }
                default:
                    break;
            }
        }
            
            break;
        case tagSaveLeaving:
        {
            switch (buttonIndex)
            {
                case 0:
                {
                    //YES
                    
                }
                    break;
                case 1:
                {
                    //NO
                    UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ALERT_DISCARDSAVECOUNTS] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles:[[LanguageManagerV2 sharedLanguageManager] getNo], nil];
                    alert.tag = tagDiscard;
                    [alert show];
                    
                    
                }
                default:
                    break;
            }
        }
            
            break;
        case tagDiscard:
        {
            switch (buttonIndex)
            {
                case 0:
                {
                    // Discard all thing and go out
                    self.isOneView=NO;
                    [self clearDataAsLogout];
                    [[HomeViewV2 shareHomeView] handleTabbarEventWithTagOfButton:[[NSUserDefaults standardUserDefaults] integerForKey:TAG_OF_EVENT]];
                }
                    break;
                case 1:
                {
                    // Do not discard and stay on the current screen
                    
                }
                default:
                    break;
            }
        }
            
            break;
        case tagSaveSuccessful:
        {
            switch (buttonIndex)
            {
                case 0:
                {
                    //YES:
                    [CommonVariable sharedCommonVariable].isSaveSuccessfull = true;
                    [self clearDataAsLogout];
                }
                    break;
                case 1:
                {
                    
                    //NO
                    
                }
                default:
                    break;
            }
        }
            
            break;
        default:
            break;
    }
    [messagemainTableView reloadData];
}
#pragma mark- hidden after save.
-(void) hiddenHUDAfterSaved:(MBProgressHUD *)hud{
    [hud hide:YES];
    [hud removeFromSuperview];
    //    [HUD release];
}

@end
