//
//  AdhocMessageTableViewCell.h
//  mHouseKeeping
//
//  Created by TMS on 2/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AdhocMessageTableViewCell : UITableViewCell
{
    UILabel *lblMessageDate;
    UILabel *lblMessageTopic;
    UILabel *lblMessageTo;
    UIButton *btnNext;

}
@property (nonatomic,strong) IBOutlet UILabel *lblMessageDate;
@property (nonatomic,strong) IBOutlet UILabel *lblMessageTopic;
@property (nonatomic,strong) IBOutlet UILabel *lblMessageTo;
@property (nonatomic,strong) IBOutlet UIButton *btnNext;


@end
