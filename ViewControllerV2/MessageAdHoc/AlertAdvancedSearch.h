//
//  AlertAdvancedSearch.h
//  iGuest
//
//  Created by ThuongNM on 1/13/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class AlertAdvancedSearch;
@protocol AlertAdvancedSearchDelegate <NSObject>
@optional
-(void) alertAdvancedOKButtonPressed;
-(void) alertAdvancedCancelButtonPressed;
-(void) alertAdvancedOKButtonPressed:(AlertAdvancedSearch *) alertView;
-(void) alertAdvancedCancelButtonPressed:(AlertAdvancedSearch *)alertView;

@end

@interface AlertAdvancedSearch : UIAlertView <UIAlertViewDelegate>{
  

    UIView * view;
    UIButton * btnOK;
    UIButton *btnCancel;
    UILabel *lblTitle;
    UILabel *lblContent;
    UIPopoverController *popoverReserStatus;
    AlertAdvancedSearch *advancedsearch;
    __unsafe_unretained id<AlertAdvancedSearchDelegate> delegate;
}
@property(nonatomic,assign)id<AlertAdvancedSearchDelegate> delegate;
- (id)alertAdvancedSearchInitWithFrame:(CGRect)frame title:(NSString *)titletext content:(NSString *)contenttext;
-(void) setCaptionsView;
@end

