//
//  MyNavigationBarV2.h
//  mHouseKeeping
//
//  Created by khanhnguyen on 2/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WifiIConViewController.h"

#define FONT_BACK_BUTTON 15
#define FONT_BACK_BUTTON_20 20

#define STANDARD_HEIGHT_NAVIGATION_BAR 44
#define Y_TOPBAR_TITLE  7
#define X_TOPBAR_TITLE  0
#define Y_BACK_BUTTON   5
#define HEIGHT_NAVIGATION_BAR 56
#define WIDTH_BACK_BUTTON 56 // 51
#define HEIGHT_BACK_BUTTON 41 // 31

#define WIDTH_BACK_ARROW 40
#define HEIGHT_BACK_ARROW 40

#define TagButtonFirstRow   876
#define TagButtonSecondRow  877

@interface MyNavigationBarV2 : UINavigationBar<UINavigationBarDelegate>{
//    WifiIConViewController * wifiStatus;
}
//@property(strong) WifiIConViewController * wifiStatus;
//-(void) hiddenConnectiveIcon;
//-(void) unHiddenConnectiveIcon;

+(UIBarButtonItem*)createBackButtonWithTarget:(id)target selector:(SEL)selector forControlEvents:(UIControlEvents)controlEvents;

//Create title bar with 2 row
+(UIView*)createTitleBarWithFirstRow:(NSString*)firstRowValue secondRow:(NSString*)secondRowValue target:(id)target selector:(SEL)selector forControlEvents:(UIControlEvents)controlEvents;

//Create frame for title bar in middle
+(CGRect)middleFrameForNavigationTitleView:(UIView*)titleView;

@end
