//
//  MyNavigationBarV2.m
//  mHouseKeeping
//
//  Created by khanhnguyen on 2/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MyNavigationBarV2.h"
#import "ehkDefines.h"
#import "LogFileManager.h"
#import "UIImage+Tint.h"
#import "DeviceManager.h"

#define navigationBarImage  @"NavigationBar.png"

@implementation MyNavigationBarV2

//@synthesize wifiStatus=_wifiStatus;
#pragma mark - View lifecycle

- (CGSize)sizeThatFits:(CGSize)size {
    CGRect frame = [UIScreen mainScreen].applicationFrame;
    CGSize newSize = CGSizeMake(frame.size.width , HEIGHT_NAVIGATION_BAR);
    return newSize;
}

- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        // Initialization code
//        wifiStatus = [[WifiIConViewController alloc] init];
//        [self addSubview:wifiStatus.view];
    }
    return self;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
}

- (void)drawRect:(CGRect)rect
{
//    if (wifiStatus == nil) {
//        wifiStatus = [[WifiIConViewController alloc] init];
//    }
//    [self addSubview:wifiStatus.view];
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        UIImage *imageBackground = [UIImage imageWithColor:COLOR_NAVIGATION_BAR];
        [imageBackground drawInRect:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    } else {
        // ColorSync manipulated image
        UIImage *imageBackground = [UIImage imageNamed: navigationBarImage];
        [imageBackground drawInRect: CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    }
}

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated{
    
}

- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated{
    
}

+(UIBarButtonItem*)createBackButtonWithTarget:(id)target selector:(SEL)selector forControlEvents:(UIControlEvents)controlEvents
{
    
    if(SYSTEM_VERSION_LESS_THAN(@"7.0")) {
        //Back button
        UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
        
        UIImage *img = [UIImage imageNamed:@"btn_back.png"];
        [btnBack setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK] forState:UIControlStateNormal];
        [btnBack setBackgroundImage:img forState:UIControlStateNormal];
        [btnBack addTarget:target action:selector forControlEvents:controlEvents];
        [btnBack setFrame:CGRectMake(0, 0, WIDTH_BACK_BUTTON , HEIGHT_BACK_BUTTON)];
        [btnBack.titleLabel setFont:[UIFont boldSystemFontOfSize:FONT_BACK_BUTTON]];
        [btnBack.titleLabel setMinimumFontSize:10];
        [btnBack setContentEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
        UIView *backButtonView = [[UIView alloc] initWithFrame:btnBack.frame];
        backButtonView.bounds = CGRectMake(backButtonView.bounds.origin.x, Y_BACK_BUTTON, backButtonView.bounds.size.width, backButtonView.bounds.size.height);
        [backButtonView addSubview:btnBack];
        
        UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButtonView];
        
        return barButtonItem;
        
    } else {
        //Back button
        //UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
        
        //[btnBack setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK] forState:UIControlStateNormal];
        //[btnBack addTarget:target action:selector forControlEvents:controlEvents];
        //[btnBack setFrame:CGRectMake(0, -5, WIDTH_BACK_BUTTON , HEIGHT_BACK_BUTTON)];
        //[btnBack.titleLabel setFont:[UIFont boldSystemFontOfSize:FONT_BACK_BUTTON_20]];
        //[btnBack.titleLabel setMinimumFontSize:10];
        //[btnBack setContentEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
        
        UIImage *img = [UIImage imageNamed:@"Back-Vector.png"];
        //UIImageView *leftArrow = [[UIImageView alloc] initWithImage:[img imageTintedWithColor:[UIColor whiteColor]]];
        //[leftArrow setFrame:CGRectMake(btnBack.frame.origin.x - (WIDTH_BACK_ARROW / 2) - 3, -5, WIDTH_BACK_ARROW, HEIGHT_BACK_ARROW)];
        
        UIButton *leftArrow = [UIButton buttonWithType:UIButtonTypeCustom];
        [leftArrow addTarget:target action:selector forControlEvents:controlEvents];
        [leftArrow setFrame:CGRectMake(- (WIDTH_BACK_ARROW / 2) - 3, -5, WIDTH_BACK_ARROW, HEIGHT_BACK_ARROW + 20)];
        [leftArrow setImage:[img imageTintedWithColor:[UIColor whiteColor]] forState:UIControlStateNormal];
        leftArrow.imageEdgeInsets = UIEdgeInsetsMake(43, 30, 57, 50);
        
        //UIView *backButtonView = [[UIView alloc] initWithFrame:leftArrow.frame];
        //backButtonView.bounds = CGRectMake(leftArrow.frame.origin.x + 12, 0, backButtonView.bounds.size.width, backButtonView.bounds.size.height);
        
        //[backButtonView addSubview:leftArrow];
        //[backButtonView addSubview:btnBack];
        
        UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftArrow];
        
        return barButtonItem;
    }
}

+(UIView*)createTitleBarWithFirstRow:(NSString*)firstRowValue secondRow:(NSString*)secondRowValue target:(id)target selector:(SEL)selector forControlEvents:(UIControlEvents)controlEvents
{
    UIView *vBarButton = [[UIView alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
    UIButton *titleBarButtonFirst = [[UIButton alloc] initWithFrame:CGRectMake(0, -5, 200, 30)];
    [titleBarButtonFirst setTag:TagButtonFirstRow];
    titleBarButtonFirst.titleLabel.backgroundColor = [UIColor clearColor];
    [titleBarButtonFirst.titleLabel setFont: FONT_BUTTON_TOPBAR];
    titleBarButtonFirst.titleLabel.textAlignment = NSTextAlignmentCenter;
    [titleBarButtonFirst.titleLabel setAdjustsFontSizeToFitWidth:YES];
    titleBarButtonFirst.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
    [titleBarButtonFirst.titleLabel setMinimumFontSize:14.0f];
    
    [titleBarButtonFirst.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
    [titleBarButtonFirst.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
    [titleBarButtonFirst addTarget:target action:selector forControlEvents:controlEvents];
    [titleBarButtonFirst setTitle:firstRowValue forState:UIControlStateNormal];
    [vBarButton addSubview:titleBarButtonFirst];
    
    UIButton *titleBarButtonSecond = [[UIButton alloc] initWithFrame:CGRectMake(0, 23, 200, 22)];
    [titleBarButtonSecond setTag:TagButtonSecondRow];
    [titleBarButtonSecond setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [titleBarButtonSecond.titleLabel setFont:FONT_BUTTON_TOPBAR_SECOND];
    [titleBarButtonSecond.titleLabel setMinimumFontSize:14.0f];
    [titleBarButtonSecond addTarget:target action:selector forControlEvents:controlEvents];
    [titleBarButtonSecond setTitle:secondRowValue forState:UIControlStateNormal];
    [vBarButton addSubview:titleBarButtonSecond];
    
    return vBarButton;
}

//Create frame for title bar in middle
+(CGRect)middleFrameForNavigationTitleView:(UIView*)titleView
{
    CGRect titleViewBounds = titleView.bounds;
    int moveLeftTitle = 0;
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        moveLeftTitle = 10;
    }
    return CGRectMake(titleViewBounds.origin.x + moveLeftTitle, Y_TOPBAR_TITLE, titleViewBounds.size.width, titleViewBounds.size.height);
}

//nmtan the code below not work
//-(void) hiddenConnectiveIcon{
//    [wifiStatus setHidden:YES];
//}
//-(void) unHiddenConnectiveIcon{
//    [wifiStatus setHidden:NO];
//}

@end
