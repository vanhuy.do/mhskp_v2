//
//  OtherActivityCell.h
//  mHouseKeeping
//
//  Created by DungPhan on 10/13/15.
//
//

#import <UIKit/UIKit.h>

@interface OtherActivityCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblActivityName;

@end
