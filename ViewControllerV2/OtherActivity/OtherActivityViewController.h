//
//  OtherActivityViewController.h
//  mHouseKeeping

#import <UIKit/UIKit.h>
#import "AdditionalJobManagerV2.h"
#import "RoomManagerV2.h"
#import "RoomModelV2.h"
#import "RoomAssignmentModelV2.h"
#import "GuestInfoViewV2.h"
#import "GuestInfoModelV2.h"
//MARK: re-use defined of "RoomAssignmentInfoViewController"
#import "RoomAssignmentInfoViewController.h"
#import "ehkDefinesV2.h"
#import "OtherActivityManager.h"

@interface OtherActivityViewController : UIViewController
<UIAlertViewDelegate, RemarkViewV2Delegate> {
    
    //View
    IBOutlet UIScrollView *scrContent;
    IBOutlet UIView *viewDetail;
    IBOutlet UIButton *btnStartStop;
    IBOutlet UIButton *btnComplete;
    IBOutlet UIButton *btnTimer;
    
    UIButton *titleBarButtonFirst;
    UIButton *titleBarButtonSecond;
    RemarkViewV2 *remarkView;
    //Variables for cleaning, inspecting
    enum TIMER_STATUS timerStatus;
    NSTimer *timer;
    NSInteger timeLeft;
    NSInteger duration;
    
    OtherActivityManager *otherActivityManager;
    bool isEnglishLang;
    bool isStartOnLoad;
    bool isAlreadyLayoutSubviewsFirstLoad;
}

@property (strong, nonatomic) IBOutlet UILabel *captionRemark;
@property (nonatomic, retain) IBOutlet UITextView *tvRemarkContent;
@property (nonatomic, assign) enum IS_PUSHED_FROM isPushFrom;
@property (nonatomic, strong) OtherActivityAssignmentModel *activityAssignmentDetail;
@property (nonatomic, strong) NSString *otherActivitySelected;

- (void)loadData;
- (void)setCaptionsView;
- (IBAction)btnCompleteClicked:(id)sender;
- (IBAction)btnStartStopClicked:(id)sender;

//Update timer in AppDelegate become foreground
- (void)countDownWithTimeDif:(NSInteger)timeDif;
// Update duration in AppDelegate become foreground
- (void)saveProgress;

@end
