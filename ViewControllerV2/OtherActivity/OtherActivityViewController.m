//
//  AdditionalJobByRoomViewV2.m
//  mHouseKeeping
//

#import "OtherActivityViewController.h"
#import "AdditionalJobRoomCellV2.h"
#import "RoomManagerV2.h"
#import "RoomModelV2.h"
#import "FloorModelV2.h"
#import "AddJobDetailModelV2.h"
#import "AdditionalJobManagerV2.h"
#import "MyNavigationBarV2.h"
#import "LogFileManager.h"
#import "UnassignManagerV2.h"
#import "DeviceManager.h"
#import "ActionPopupView.h"
#import "LanguageManagerV2.h"
#import "AdditionalJobListByFloorViewV2.h"
#import "ehkDefinesV2.h"
#import "UIImage+Tint.h"
#import "AddJobFloorModelV2.h"
#import "CustomAlertViewV2.h"

#define heightTaskDetailsCell    45.0f
#define heightAddJobCell    45.0f
#define heightHeaderTable   20.0f
#define heightFooterTable   4.0f
#define tagNoResult         12345
#define numberTableSection  2

#define heightTbvNoResult   240.0f
#define sizeNoResult        22.0f
#define sizeNormal          17.0f
#define sizeSmall           10.0f

#define colorAlpha          1.0
#define colorWhite          255/255.0
#define colorBlueR          105/255.0
#define colorBlueG          218/255.0
#define colorBlueB          222/255.0

#define tagConfirmDone      4812
#define tagConfirmStop      4813


@implementation OtherActivityViewController
@synthesize isPushFrom;
@synthesize activityAssignmentDetail;
@synthesize otherActivitySelected;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [self setCaptionsView];
    isEnglishLang = [[LanguageManagerV2 sharedLanguageManager] currentLanguageIsEqualTo:@"%@", ENGLISH_LANGUAGE];
    
    //    if(isPushFrom == IS_PUSHED_FROM_ADD_JOB_LIST) {
    //set selection find in bottom bar
    //        [[HomeViewV2 shareHomeView] setSelectedButton:tagOfAdditionalJob];
    //    } else {
    [[HomeViewV2 shareHomeView] setSelectedButton:tagOfHomeButton];
    //    }
    
}

- (void)viewDidAppear:(BOOL)animated {
    TopbarViewV2 *topbar = [self getTopBarView];
    BOOL isTopbarShowing = (topbar.isHidden != YES || topbar.alpha > 0.0f);
    
    [self modifyAllViews:isTopbarShowing];
    [super viewDidAppear:animated];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    //Fix bug content in table is lost apart on first load
    if (!isAlreadyLayoutSubviewsFirstLoad) {
        isAlreadyLayoutSubviewsFirstLoad = YES;
        TopbarViewV2 *topbar = [self getTopBarView];
        BOOL isTopbarShowing = (topbar.isHidden != YES || topbar.alpha > 0.0f);
        
        [self modifyAllViews:isTopbarShowing];
    }
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    //Fix bug content in table is lost apart on first load
    isAlreadyLayoutSubviewsFirstLoad = NO;
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        [self loadFlatResource];
    }
    
    [self setupViews];
    
    isEnglishLang = [[LanguageManagerV2 sharedLanguageManager] currentLanguageIsEqualTo:@"%@", ENGLISH_LANGUAGE];
    otherActivityManager = [OtherActivityManager sharedOtherActivityManager];
    
    [self performSelector:@selector(loadTopbarView)];
    
    [self addBackButton];
    [self.captionRemark setText:[L_remark_title currentKeyToLanguage]];
    
    //Will auto start after load data done
    isStartOnLoad = NO;
    
    // Load Data
    [self loadData];
}

- (void)viewDidUnload {
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Alert View Delegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    
    if (alertView.tag == tagConfirmDone) {
        //YES
        if (buttonIndex == 0) {
            [self completeCurrentActivity];
        }
    } else if (alertView.tag == tagConfirmStop) {
        //YES
        if (buttonIndex == 0) {
            [self stopCurrentActivity];
        }
    }
}

- (void)hiddenHUDAfterSaved:(MBProgressHUD *)HUD {
    [HUD hide:YES];
    [HUD removeFromSuperview];
    
}

#pragma mark - Delegate Remark View
- (void)remarkViewV2DoneWithText:(NSString *)text andController:(UIViewController *)controllerView {
    
}

//implement this for remove remark field with button cancel
- (void)remarkViewV2didDismissWithButtonIndex:(NSInteger)buttonIndex {
    
}

-(void)remarkViewV2DoneWithText:(NSString *)text {
    
}

#pragma mark - Load Data

- (void)loadData {
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getLoadingData]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES];
    [[HomeViewV2 shareHomeView] waitingLoadingData];
    
    int userId = [UserManagerV2 sharedUserManager].currentUser.userId;
    OtherActivityModel *otherActivityModel = [otherActivityManager loadOtherActivityById:activityAssignmentDetail.oaa_activities_id];
    
    if (isEnglishLang) {
        self.otherActivitySelected = otherActivityModel.oa_name;
    } else {
        self.otherActivitySelected = otherActivityModel.oa_name_lang;
    }
    
    //Reload local database Additional Job Detail
    OtherActivityAssignmentModel *newDetail = [otherActivityManager loadOtherActivityAssignmentByUserId:userId ondayOtherActivityId:activityAssignmentDetail.oaa_id];
    if (newDetail) {
        activityAssignmentDetail = newDetail;
    }
    
    [self.tvRemarkContent setText:activityAssignmentDetail.oaa_remark];
    timerStatus = TIMER_STATUS_NOT_START;
    timeLeft = ((activityAssignmentDetail.oaa_duration * 60) - [self calculateDurationSpentForStart]);
    [btnTimer setTitle:[self countTimeSpendForTask:timeLeft option:YES] forState:UIControlStateNormal];
    
    [HUD hide:YES];
    [HUD removeFromSuperview];
    
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
    
    //Start onload
    if (isStartOnLoad) {
        isStartOnLoad = NO;
        [self startCurrentActivity];
    } else if (activityAssignmentDetail.oaa_status_id == OtherActivityStatus_Start) {
        [self startCurrentActivity];
    }
}

- (void)setupViews {
    if (timerStatus == TIMER_STATUS_NOT_START) {
        [self.tvRemarkContent setEditable:NO];
        [self setEnableBtnComplete:NO];
        [btnStartStop setBackgroundImage:[UIImage imageBeforeiOS7:imgStartBtn equaliOS7:imgStartBtnFlat] forState:UIControlStateNormal];
        [btnStartStop setBackgroundImage:[UIImage imageBeforeiOS7:imgStartBtnClicked equaliOS7:imgStartBtnClickedFlat] forState:UIControlEventTouchDown];
    } else if(timerStatus == TIMER_STATUS_PAUSE) {
        [self.tvRemarkContent setEditable:NO];
        [self setEnableBtnComplete:NO];
        [btnStartStop setBackgroundImage:[UIImage imageBeforeiOS7:imgStartBtn equaliOS7:imgStartBtnFlat] forState:UIControlStateNormal];
        [btnStartStop setBackgroundImage:[UIImage imageBeforeiOS7:imgStartBtn equaliOS7:imgStartBtnFlat] forState:UIControlEventTouchDown];
    } else if (timerStatus == TIMER_STATUS_START) {
        [self.tvRemarkContent setEditable:YES];
        [self setEnableBtnComplete:YES];
        [btnStartStop setBackgroundImage:[UIImage imageBeforeiOS7:imgStopBtn equaliOS7:imgStopBtnFlat] forState:UIControlStateNormal];
        [btnStartStop setBackgroundImage:[UIImage imageBeforeiOS7:imgStopBtnClicked equaliOS7:imgStopBtnClickedFlat] forState:UIControlEventTouchDown];
    } else { //Timer finish
        
    }
}

#pragma mark - View Interaction

- (IBAction)btnStartStopClicked:(id)sender {
    //Current Timer Status
    switch (timerStatus) {
        case TIMER_STATUS_PAUSE:
        {
            [self startCurrentActivity];
            
        }
            break;
            
        case TIMER_STATUS_START:
        {
            UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_stop_this_other_activity] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles:[[LanguageManagerV2 sharedLanguageManager] getNo], nil];
            alert.delegate = self;
            [alert setTag:tagConfirmStop];
            [alert show];
        }
            break;
            
        case TIMER_STATUS_NOT_START:
        {
            [self startCurrentActivity];
        }
            break;
            
        default:
            break;
    }
    
}

- (IBAction)btnCompleteClicked:(id)sender {
    UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_complete_this_other_activity] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles:[[LanguageManagerV2 sharedLanguageManager] getNo], nil];
    alert.delegate = self;
    [alert setTag:tagConfirmDone];
    [alert show];
}

- (NSInteger)calculateDurationSpentForStart {
    
    int expectedDuration = activityAssignmentDetail.oaa_duration * 60;
    int spentDuration = activityAssignmentDetail.oaa_duration_spent;
    int localDuration = activityAssignmentDetail.oaa_local_duration;
    
    if (localDuration != 0 && spentDuration < localDuration) {
        spentDuration = localDuration;
    }
    if (spentDuration < expectedDuration) {
        return spentDuration;
    } else {
        return -spentDuration;
    }
}

- (NSInteger)calculateDurationSpentforStopAndComplete {
    
    int spentDuration = activityAssignmentDetail.oaa_duration_spent;
    int localDuration = activityAssignmentDetail.oaa_local_duration;
    
    if (localDuration != 0 && spentDuration < localDuration) {
        spentDuration = localDuration;
    }
    return (spentDuration + duration);
}

- (void)startCurrentActivity {
    
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getLoadingData]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES];
    [[HomeViewV2 shareHomeView] waitingLoadingData];
    
    if (timeLeft < 0) {
        [btnTimer setBackgroundImage:[UIImage imageBeforeiOS7:imgTimerOver equaliOS7:imgTimerOverFlat] forState:UIControlStateNormal];
    } else {
        [btnTimer setBackgroundImage:[UIImage imageBeforeiOS7:imgTimerPlay equaliOS7:imgTimerPlayFlat] forState:UIControlStateNormal];
    }
    
    [btnStartStop setBackgroundImage:[UIImage imageBeforeiOS7:imgStopBtn equaliOS7:imgStopBtnFlat] forState:UIControlStateNormal];
    [btnStartStop setBackgroundImage:[UIImage imageBeforeiOS7:imgStopBtnClicked equaliOS7:imgStopBtnClickedFlat] forState:UIControlEventTouchDown];
    
    timeLeft = ((activityAssignmentDetail.oaa_duration * 60) - [self calculateDurationSpentForStart]);
    
    NSString *strCurrentTime = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSString *curTime = [ehkConvert DateToStringWithString:strCurrentTime fromFormat:@"yyyy-MM-dd HH:mm:ss" toFormat:@"HH:mm:ss"];
    
    int userId = [UserManagerV2 sharedUserManager].currentUser.userId;
    // just record first time start
    if ([activityAssignmentDetail.oaa_user_start_time isEqualToString:@"00:00:00"]) {
        activityAssignmentDetail.oaa_user_start_time = curTime;
    }
    activityAssignmentDetail.oaa_status_id = OtherActivityStatus_Start;
    activityAssignmentDetail.oaa_duration_spent = (int)[self calculateDurationSpentForStart];
    activityAssignmentDetail.oaa_last_modified = strCurrentTime;
    
    [otherActivityManager updateOtherActivityAssignment:activityAssignmentDetail];
    
    OtherActivityRecordModel *detailRecord = [[OtherActivityRecordModel alloc] init];
    detailRecord.oar_onday_activity_id = activityAssignmentDetail.oaa_id;
    detailRecord.oar_operation = OtherActivityStatus_Start;
    detailRecord.oar_post_status = (int)POST_STATUS_SAVED_UNPOSTED;
    detailRecord.oar_time = activityAssignmentDetail.oaa_user_start_time;
    detailRecord.oar_remark = activityAssignmentDetail.oaa_remark;
    detailRecord.oar_user_id = userId;
    detailRecord.oar_duration = activityAssignmentDetail.oaa_duration_spent;
    detailRecord.oar_last_modified = strCurrentTime;
    [otherActivityManager insertOtherActivityRecord:detailRecord];
    
    // post start other activity
    bool isPostSucess = NO;
    if (isDemoMode) {
        isPostSucess = YES;
    } else {
        isPostSucess = [self postUpdateOtherActivityStatustoWSWithUserId:userId Data:detailRecord];
    }
    // update data record after post
    if (isPostSucess) {
        activityAssignmentDetail.oaa_local_duration = 0;
        [otherActivityManager updateOtherActivityAssignment:activityAssignmentDetail];
        detailRecord.oar_post_status = (int)POST_STATUS_POSTED;
        [otherActivityManager updateOtherActivityRecord:detailRecord];
    }
    
    [self changeTimerStatus:TIMER_STATUS_START];
    
    [self setupViews];
    
    [HUD hide:YES];
    [HUD removeFromSuperview];
    
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
}

- (void)stopCurrentActivity {
    
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getLoadingData]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES];
    [[HomeViewV2 shareHomeView] waitingLoadingData];
    
    [self changeTimerStatus:TIMER_STATUS_PAUSE];
    
    NSString *strCurrentTime = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *curTime = [ehkConvert DateToStringWithString:strCurrentTime fromFormat:@"yyyy-MM-dd HH:mm:ss" toFormat:@"HH:mm:ss"];
    
    int userId = [UserManagerV2 sharedUserManager].currentUser.userId;
    activityAssignmentDetail.oaa_user_stop_time = curTime;
    activityAssignmentDetail.oaa_status_id = OtherActivityStatus_Stop;
    activityAssignmentDetail.oaa_remark = self.tvRemarkContent.text;
    activityAssignmentDetail.oaa_duration_spent = (int)[self calculateDurationSpentforStopAndComplete];
    activityAssignmentDetail.oaa_local_duration = activityAssignmentDetail.oaa_duration_spent;
    activityAssignmentDetail.oaa_last_modified = strCurrentTime;
    
    [otherActivityManager updateOtherActivityAssignment:activityAssignmentDetail];
    
    OtherActivityRecordModel *detailRecord = [[OtherActivityRecordModel alloc] init];
    detailRecord.oar_onday_activity_id = activityAssignmentDetail.oaa_id;
    detailRecord.oar_operation = OtherActivityStatus_Stop;
    detailRecord.oar_post_status = (int)POST_STATUS_SAVED_UNPOSTED;
    detailRecord.oar_time = curTime;
    detailRecord.oar_remark = activityAssignmentDetail.oaa_remark;
    detailRecord.oar_user_id = userId;
    detailRecord.oar_duration = activityAssignmentDetail.oaa_duration_spent;
    detailRecord.oar_last_modified = strCurrentTime;
    [otherActivityManager insertOtherActivityRecord:detailRecord];
    
    // post stop other activity
    bool isPostSucess = NO;
    if (isDemoMode) {
        isPostSucess = YES;
    } else {
        isPostSucess = [self postUpdateOtherActivityStatustoWSWithUserId:userId Data:detailRecord];
    }
    
    // update data record after post
    if (isPostSucess) {
        activityAssignmentDetail.oaa_local_duration = 0;
        [otherActivityManager updateOtherActivityAssignment:activityAssignmentDetail];
        detailRecord.oar_post_status = (int)POST_STATUS_POSTED;
        [otherActivityManager updateOtherActivityRecord:detailRecord];
    }
    
    [self setupViews];
    [HUD hide:YES];
    [HUD removeFromSuperview];
    
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
    
    [self backBarPressed];
}

- (void)completeCurrentActivity {
    
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getLoadingData]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES];
    [[HomeViewV2 shareHomeView] waitingLoadingData];
    
    [self changeTimerStatus:TIMER_STATUS_FINISH];
    
    NSString *strCurrentTime = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *curTime = [ehkConvert DateToStringWithString:strCurrentTime fromFormat:@"yyyy-MM-dd HH:mm:ss" toFormat:@"HH:mm:ss"];
    
    int userId = [UserManagerV2 sharedUserManager].currentUser.userId;
    activityAssignmentDetail.oaa_end_time = curTime;
    activityAssignmentDetail.oaa_status_id = OtherActivityStatus_Complete;
    activityAssignmentDetail.oaa_remark = self.tvRemarkContent.text;
    activityAssignmentDetail.oaa_duration_spent = (int)[self calculateDurationSpentforStopAndComplete];
    activityAssignmentDetail.oaa_local_duration = activityAssignmentDetail.oaa_duration_spent;
    activityAssignmentDetail.oaa_last_modified = strCurrentTime;
    
    [otherActivityManager updateOtherActivityAssignment:activityAssignmentDetail];
    
    OtherActivityRecordModel *detailRecord = [[OtherActivityRecordModel alloc] init];
    detailRecord.oar_onday_activity_id = activityAssignmentDetail.oaa_id;
    detailRecord.oar_operation = OtherActivityStatus_Complete;
    detailRecord.oar_post_status = (int)POST_STATUS_SAVED_UNPOSTED;
    detailRecord.oar_time = curTime;
    detailRecord.oar_remark = activityAssignmentDetail.oaa_remark;
    detailRecord.oar_user_id = userId;
    detailRecord.oar_duration = activityAssignmentDetail.oaa_duration_spent;
    detailRecord.oar_last_modified = strCurrentTime;
    [otherActivityManager insertOtherActivityRecord:detailRecord];
    
    // post complete other activity
    bool isPostSucess = NO;
    if (isDemoMode) {
        isPostSucess = YES;
    } else {
        isPostSucess = [self postUpdateOtherActivityStatustoWSWithUserId:userId Data:detailRecord];
    }
    
    // update data record after post
    if (isPostSucess) {
        activityAssignmentDetail.oaa_local_duration = 0;
        [otherActivityManager updateOtherActivityAssignment:activityAssignmentDetail];
        detailRecord.oar_post_status = (int)POST_STATUS_POSTED;
        [otherActivityManager updateOtherActivityRecord:detailRecord];
        
        //        NSArray *listVC = self.navigationController.viewControllers;
        //        for (UIViewController *curVC in listVC) {
        //            if([curVC isKindOfClass:[AdditionalJobListByFloorViewV2 class]]){
        //                AdditionalJobListByFloorViewV2 *addJobFloorVC = (AdditionalJobListByFloorViewV2*)curVC;
        //                addJobFloorVC.isReloadDataOnShow = YES;//Reload data for complete pending additional Job
        //            }
        //        }
    }
    
    [HUD hide:YES];
    [HUD removeFromSuperview];
    
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
    
    [self backBarPressed];
}

- (BOOL)postUpdateOtherActivityStatustoWSWithUserId:(int)userId Data:(OtherActivityRecordModel*)data {
    
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getMsgSaveMessage]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES];
    
    [otherActivityManager postAllOtherActivityByUserId:userId];
    
    BOOL result = NO;
    result = [otherActivityManager postOtherActivityStatusByUserId:userId ondayOtherActivityId:data.oar_onday_activity_id otherActivityStatus:data.oar_operation time:data.oar_time remark:data.oar_remark durationSpent:data.oar_duration];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [HUD hide:YES];
        [HUD removeFromSuperview];
    });
    return result;
}

- (void)saveProgress {
    
    if (activityAssignmentDetail.oaa_status_id == OtherActivityStatus_Start) {
        
        NSString *strCurrentTime = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
        activityAssignmentDetail.oaa_remark = self.tvRemarkContent.text;
        activityAssignmentDetail.oaa_duration_spent = (int)[self calculateDurationSpentforStopAndComplete];
        activityAssignmentDetail.oaa_local_duration = activityAssignmentDetail.oaa_duration_spent;
        activityAssignmentDetail.oaa_last_modified = strCurrentTime;
        
        [otherActivityManager updateOtherActivityAssignment:activityAssignmentDetail];
    }
}

- (void)setEnableBtnComplete:(BOOL)isEnable {
    [btnComplete setUserInteractionEnabled:isEnable];
    [btnComplete setBackgroundImage:[UIImage imageBeforeiOS7:(isEnable?imgCompleteBtn:imgCompleteBtnDisable) equaliOS7:(isEnable?imgCompleteBtnFlat:imgCompleteBtnDisableFlat)] forState:UIControlStateNormal];
    [btnComplete setBackgroundImage:[UIImage imageBeforeiOS7:(isEnable?imgCompleteBtnClicked:imgCompleteBtnDisable) equaliOS7:(isEnable?imgCompleteBtnClickedFlat:imgCompleteBtnDisableFlat)] forState:UIControlEventTouchDown];
}

- (void)setEnableBtnStartStop:(BOOL)isEnable {
    [btnStartStop setUserInteractionEnabled:isEnable];
    if (timerStatus == TIMER_STATUS_START) {
        [btnStartStop setBackgroundImage:[UIImage imageBeforeiOS7:(isEnable?imgStartBtn:imgStartBtnDisable) equaliOS7:(isEnable?imgStartBtnFlat:imgStartBtnDisableFlat)] forState:UIControlStateNormal];
        [btnStartStop setBackgroundImage:[UIImage imageBeforeiOS7:(isEnable?imgStartBtnClicked:imgStartBtnDisable) equaliOS7:(isEnable?imgStartBtnClickedFlat:imgStartBtnDisableFlat)] forState:UIControlEventTouchDown];
    } else {
        [btnStartStop setBackgroundImage:[UIImage imageBeforeiOS7:(isEnable?imgStopBtn:imgStartBtnDisable) equaliOS7:(isEnable?imgStopBtnFlat:imgStartBtnDisableFlat)] forState:UIControlStateNormal];
        [btnStartStop setBackgroundImage:[UIImage imageBeforeiOS7:(isEnable?imgStopBtnClicked:imgStartBtnDisable) equaliOS7:(isEnable?imgStopBtnClickedFlat:imgStartBtnDisableFlat)] forState:UIControlEventTouchDown];
    }
}

#pragma mark - Timer method

- (void)changeTimerStatus:(enum TIMER_STATUS) status {
    timerStatus = status;
    [self runTimer];
}

- (void)runTimer {
    if (timerStatus == TIMER_STATUS_START) {
        [CommonVariable sharedCommonVariable].roomIsCompleted = 0;
        if (timer == nil) {
            timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(countDown) userInfo:nil repeats:YES];
        }
    } else {
        [CommonVariable sharedCommonVariable].roomIsCompleted = 1;
        [timer invalidate];
        timer = nil;
    }
}

- (void)countDown {
    [self countDownWithTimeDif:1];
}

- (void)countDownWithTimeDif:(NSInteger)timeDif {
    if (timerStatus == TIMER_STATUS_NOT_START || timerStatus == TIMER_STATUS_PAUSE) {
        return;
    }
    
    timeLeft -= timeDif;
    duration += timeDif;
    [btnTimer setTitle:[self countTimeSpendForTask:timeLeft option:YES] forState:UIControlStateNormal];
    if (timeLeft < 0) {
        [btnTimer setBackgroundImage:[UIImage imageBeforeiOS7:imgTimerOver equaliOS7:imgTimerOverFlat] forState:UIControlStateNormal];
    }
}

- (NSString *)countTimeSpendForTask:(NSInteger)expectedTimeForTask option:(BOOL)isAttachedSecond {
    
    NSString *secondsString;
    NSString *minutesString;
    NSString *hoursString;
    NSInteger secondsInt;
    NSInteger minutesInt;
    NSInteger hoursInt;
    NSString *timeDisplay;
    NSString *minus = @"-";
    if (expectedTimeForTask < 0) {
        hoursInt = -expectedTimeForTask/3600;
        minutesInt = -((expectedTimeForTask + (hoursInt*3600)) / 60);
        secondsInt = -(expectedTimeForTask % 60);
        minus = @"-";
    } else {
        hoursInt = expectedTimeForTask/3600;
        minutesInt = (expectedTimeForTask - (hoursInt*3600)) / 60;
        secondsInt = expectedTimeForTask % 60;
        minus = @"";
    }
    
    hoursString = [NSString stringWithFormat:@"%d",(int)hoursInt];
    minutesString = [NSString stringWithFormat:@"%d",(int)minutesInt];
    secondsString = [NSString stringWithFormat:@"%d",(int)secondsInt];
    
    if ([hoursString length] == 1) {
        hoursString = [NSString stringWithFormat:@"0%@",hoursString];
    }
    
    if ([minutesString length] == 1) {
        minutesString = [NSString stringWithFormat:@"0%@",minutesString];
    }
    
    if ([secondsString length] == 1) {
        secondsString = [NSString stringWithFormat:@"0%@",secondsString];
    }
    
    if (isAttachedSecond) {
        timeDisplay = [NSString stringWithFormat:@"%@%@:%@:%@",minus,hoursString,minutesString,secondsString];
    } else {
        timeDisplay = [NSString stringWithFormat:@"%@%@:%@",minus,hoursString,minutesString];
    }
    
    return timeDisplay;
}

#pragma mark  - Set Captions View
- (void)setCaptionsView {
    [self setTitle:[NSString stringWithFormat:@"%@ - %@ %@", [[LanguageManagerV2 sharedLanguageManager] getFind], [[LanguageManagerV2 sharedLanguageManager] getBy], [[LanguageManagerV2 sharedLanguageManager] getRoomTitle]]];
    
}

#pragma mark - === Handle Topbar Methods ===
#pragma mark

- (void)modifyAllViews:(BOOL)isTopBarShowing {
    TopbarViewV2 *topbar = [self getTopBarView];
    int heightTopbar = 0;
    if (isTopBarShowing) {
        heightTopbar = topbar.frame.size.height;
    }
    
    //View Detail
    CGRect fViewDetail = viewDetail.frame;
    [viewDetail setFrame:fViewDetail];
    
    [viewDetail setHidden:NO];
    
    //ScrollView Content
    CGRect fScrContent = scrContent.frame;
    if ([DeviceManager getScreenDeviceHeight] == DeviceScreenSizeStandardHeight4_0) {
        fScrContent.size.height = 416 - heightTopbar;
    } else {
        fScrContent.size.height = 328 - heightTopbar;
    }
    [scrContent setContentSize:CGSizeMake(fScrContent.size.width, fViewDetail.origin.y + fViewDetail.size.height + 5)];
    [scrContent setFrame:fScrContent];
    [scrContent setScrollEnabled:YES];
}

- (void)loadTopbarView {
    TopbarViewV2 *topbar = [[TopbarViewV2 alloc] initViewWithSuperModel:[[CommonVariable sharedCommonVariable] superRoomModel]];
    [topbar setTag:tagTopbarView];
    [self.view addSubview:topbar];
    [topbar setHidden:YES];
    [topbar setAlpha:0.0];
    [self addButtonHandleShowHideTopbar];
}

- (void)showTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationWillStartSelector:@selector(setHiddenTopbarView)];
    
    f = topbar.frame;
    f.origin.y = 0;
    [topbar setFrame:f];
    [topbar setAlpha:1.0];
    
    [self adjustShowForViews];
    
    [UIView commitAnimations];
    
}

- (void)hideTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDidStopSelector:@selector(setHiddenTopbarView)];
    [UIView setAnimationDelegate:self];
    
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    [topbar setAlpha:0.0];
    
    [self adjustRemoveForViews];
    
    [UIView commitAnimations];
}

- (void)setHiddenTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    [topbar setHidden:!topbar.hidden];
}

- (TopbarViewV2 *)getTopBarView {
    return (TopbarViewV2 *)[self.view viewWithTag:tagTopbarView];
}

- (void)addButtonHandleShowHideTopbar {
    UIView *vBarButton = [[UIView alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
    titleBarButtonFirst = [[UIButton alloc] initWithFrame:CGRectMake(0, -5, 200, 35)];
    
    titleBarButtonFirst.titleLabel.backgroundColor = [UIColor clearColor];
    [titleBarButtonFirst.titleLabel setFont: FONT_BUTTON_TOPBAR];
    titleBarButtonFirst.titleLabel.textAlignment = NSTextAlignmentCenter;
    titleBarButtonFirst.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
    
    [titleBarButtonFirst.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
    [titleBarButtonFirst.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
    [titleBarButtonFirst addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    [titleBarButtonFirst setTitle:otherActivitySelected forState:UIControlStateNormal];
    titleBarButtonFirst.titleLabel.adjustsFontSizeToFitWidth = YES;
    
    [vBarButton addSubview:titleBarButtonFirst];
    titleBarButtonSecond = [[UIButton alloc] initWithFrame:CGRectMake(0, 25, 200, 22)];
    [titleBarButtonSecond setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [titleBarButtonSecond.titleLabel setFont:FONT_BUTTON_TOPBAR_SECOND];
    [titleBarButtonSecond setTitle:[L_activity_detail currentKeyToLanguage] forState:UIControlStateNormal];
    [titleBarButtonSecond addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    [vBarButton addSubview:titleBarButtonSecond];
    
    self.navigationItem.titleView = vBarButton;
    
    //Move title to center vertical
    CGRect titleViewBounds = self.navigationItem.titleView.bounds;
    int moveLeftTitle = 0;
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        moveLeftTitle = 10;
    }
    self.navigationItem.titleView.bounds = CGRectMake(titleViewBounds.origin.x + moveLeftTitle, Y_TOPBAR_TITLE, titleViewBounds.size.width, titleViewBounds.size.height);
}

- (void)topbarTapped:(UIButton *)sender {
    TopbarViewV2 *topbar = [self getTopBarView];
    [titleBarButtonFirst setEnabled:NO];
    [titleBarButtonSecond setEnabled:NO];
    
    if (topbar.isHidden) {
        [self showTopbarView];
    } else {
        [self hideTopbarView];
    }
    
    [self performSelector:@selector(topbarButtonEnable:) withObject:sender afterDelay:0.6f];
}

- (void)topbarButtonEnable:(UIButton *)topbarBtn {
    [titleBarButtonSecond setEnabled:YES];
    [titleBarButtonFirst setEnabled:YES];
}

- (void)adjustShowForViews {
    TopbarViewV2 *topBarView = [self getTopBarView];
    CGRect fTopbarView = topBarView.frame;
    
    CGRect fViewDetail = viewDetail.frame;
    [viewDetail setFrame:fViewDetail];
    
    CGRect fScrViewContent = scrContent.frame;
    fScrViewContent.origin.y += fTopbarView.size.height;
    fScrViewContent.size.height -= fTopbarView.size.height;
    [scrContent setFrame:fScrViewContent];
}

- (void)adjustRemoveForViews {
    TopbarViewV2 *topBarView = [self getTopBarView];
    CGRect fTopbarView = topBarView.frame;
    
    CGRect fViewDetail = viewDetail.frame;
    [viewDetail setFrame:fViewDetail];
    
    CGRect fScrViewContent = scrContent.frame;
    fScrViewContent.origin.y -= fTopbarView.size.height;
    fScrViewContent.size.height += fTopbarView.size.height;
    [scrContent setFrame:fScrViewContent];
}

#pragma mark - Handle Back Button
- (void)backBarPressed {
    if (timerStatus == TIMER_STATUS_START) {
        NSString *message = [L_complete_the_other_activity_before_leaving currentKeyToLanguage];
        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:message delegate:nil cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_OK] otherButtonTitles:nil];
        [alert show];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)addBackButton {
    //back button navigation
    self.navigationItem.leftBarButtonItem = [MyNavigationBarV2 createBackButtonWithTarget:self selector:@selector(backBarPressed) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - Scroll View Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
}

#pragma mark - iOS 7 Resources
- (void)loadFlatResource {
    
}

@end
