//
//  PickerViewV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 1/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
@class PickerViewV2;

@protocol PickerViewV2Delegate <NSObject>

@optional
-(void) didChoosePickerViewV2WithData:(NSString *) data AndIndex:(NSIndexPath *) index;
-(void) didChoosePickerViewV2WithData:(NSString *) data AndIndex:(NSIndexPath *) index WithtIndex:(NSInteger) tIndex;
-(void) didChooseFindRoomPickerViewV2WithData:(NSString *) data AndIndex:(NSIndexPath *) index WithtIndex:(NSInteger) tIndex;
-(void) didChooseDemoModeFindRoomPickerViewV2WithData:(NSString *) data AndIndex:(NSIndexPath *) index WithtIndex:(NSInteger) tIndex;
-(void) didChoosePickerViewV2WithData:(NSString *) data AndIndexRow:(NSInteger) index;
-(void) didChoosePickerViewV2WithData:(NSString *) data AndIndexItemRow:(NSIndexPath *) index;
-(void) didChooseFindRoomPickerViewV2WithPickerView:(PickerViewV2*)pickerView withText:(NSString *)data WithtIndex:(NSInteger)tIndex;
-(void) didChooseDate:(PickerViewV2*)pickerView withDate:(NSDate*)dateSelected;

@end

@interface PickerViewV2 : UIView <UIPickerViewDelegate, UIPickerViewDataSource>{
    NSArray *datas;
    UIPickerView *pickerView;
    UIDatePicker *pickerDate;
@private id selectedData;
    __unsafe_unretained id<PickerViewV2Delegate> delegate;
    NSIndexPath *path;
    NSInteger tIndex;
}

@property (nonatomic, assign) NSInteger tIndex;
@property (nonatomic, assign) id<PickerViewV2Delegate> delegate;
@property (nonatomic, strong) id selectedData;
@property (nonatomic, strong) NSArray *datas;
@property (nonatomic, readonly, strong) NSIndexPath *path;
@property (nonatomic, strong) id showFromObject;
@property (nonatomic, assign) BOOL isShowDatePicker;

-(id) initPickerViewV2WithDatas:(NSArray *) data AndSelectedData:(id) currentSelectedData AndIndex:(NSIndexPath *)indexpath;
-(void) shouldShowDatePicker:(BOOL)shouldShowDatePicker;
-(void) showPickerViewV2;
-(void) hidePickerViewV2;
@end
