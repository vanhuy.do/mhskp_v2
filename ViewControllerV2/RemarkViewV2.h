//
//  RemarkViewV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QuartzCore/QuartzCore.h"

@protocol RemarkViewV2Delegate <NSObject>

@optional
-(void) remarkViewV2DoneWithText:(NSString *) text ;
-(void) remarkViewV2SubmitWithText:(NSString *) text andController:(UIViewController*)controllerView;
-(void) remarkViewV2DoneWithText:(NSString *) text andController:(UIViewController*)controllerView;
-(void) remarkViewV2RoomAssignDoneWithText:(NSString *) text andController:(UIViewController*)controllerView;
-(void) remarkViewV2didDismissWithButtonIndex:(NSInteger) buttonIndez;
-(void) remarkViewV2RoomAssigndidDismissWithButtonIndex:(NSInteger) buttonIndez;
@end

@interface RemarkViewV2 : UIViewController<UITextViewDelegate> {
//    __unsafe_unretained id<RemarkViewV2Delegate> delegate;
    NSString *textinRemark;
    BOOL isCancel;
}

@property (nonatomic, weak) id<RemarkViewV2Delegate> delegate;
@property (strong, nonatomic) IBOutlet UINavigationBar *nvgBar;
@property (strong, nonatomic) IBOutlet UITextView *txvRemark;
@property (strong, nonatomic) IBOutlet UIButton *btnCancel;
@property (strong, nonatomic) NSString *textinRemark;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;
@property (strong, nonatomic) IBOutlet UIView *vwBackground;
@property (strong, nonatomic) IBOutlet UIButton *btnBackground;

@property BOOL isViewOnly;
@property bool isAddSubview;
@property bool isClickFromFind;
-(IBAction) ExitWithView:(id)sender;
-(void) setCaptionsView;
-(IBAction) ExitKeyBoard:(id)sender;
-(void) remarkSaveDidPressed;
- (void)setTextinRemark:(NSString*)text;
//- (void)addNotify;
@end
