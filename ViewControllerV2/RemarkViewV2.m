//
//  RemarkViewV2.m
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "RemarkViewV2.h"
#import "DeviceManager.h"
#import "CustomAlertViewV2.h"

@implementation RemarkViewV2
@synthesize btnCancel;
@synthesize nvgBar;
@synthesize txvRemark;
@synthesize delegate;
@synthesize textinRemark;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        //hide wifi view
        [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationHideWifiView] object:nil];
    }
    return self;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
- (void)setTextinRemark:(NSString*)text{
    
    textinRemark = text;
    [txvRemark setText:textinRemark];
}
#pragma mark - View lifecycle
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setCaptionsView];
    //show wifi view
//    [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationShowWifiView] object:nil];
    [self.vwBackground setFrame:[[UIScreen mainScreen]bounds]];
    [self.btnBackground setFrame:[[UIScreen mainScreen]bounds]];

    if(_isViewOnly){
        [txvRemark setEditable:NO];
    }else{
        [txvRemark setEditable:YES];
        [txvRemark becomeFirstResponder];
    }
//    isCancel = NO;
    [txvRemark setText:textinRemark];
    //Hide the top navigation
    [self.navigationController setNavigationBarHidden:YES];
}

-(void) keyboardDidHide
{

//    if (isCancel) {
//        
//        return;
//    }
    //Push from Room detail
    if ([delegate respondsToSelector:@selector(remarkViewV2DoneWithText:andController:)]) {
        [delegate remarkViewV2DoneWithText:txvRemark.text andController:self];
//        [txvRemark resignFirstResponder];
//        [self.view removeFromSuperview];
    }
    
    //Push from Unassigned room
    if ([delegate respondsToSelector:@selector(remarkViewV2RoomAssignDoneWithText:andController:)])
    {
        [delegate remarkViewV2RoomAssignDoneWithText:txvRemark.text andController:self];
        [self.navigationController popViewControllerAnimated:YES];
    }
    if (delegate == nil) {
        if (self.isAddSubview) {
            [self.view removeFromSuperview];
        }
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        [self loadFlatResource];
    }
    isCancel = FALSE;
    
    // Do any additional setup after loading the view from its nib.
    //[self.txvRemark becomeFirstResponder];
    [self.view setBackgroundColor:[UIColor colorWithWhite:0.1 alpha:0.1]];
    
    [self.navigationItem setHidesBackButton:YES animated:NO];
        
    UIView *parentView = self.parentViewController.view;
    UIGraphicsBeginImageContext(parentView.bounds.size);
    CALayer *layer = parentView.layer;
    [layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *parentViewImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    //more 88 pixel for 4.0 inch
//    CGRect currentViewBounds = self.view.bounds;
//    if([DeviceManager getDeviceScreenKind] == DeviceScreenKindRetina4_0){
//        currentViewBounds.size.height += 88;
//        self.view.bounds = currentViewBounds;
//    }

    UIImageView *imgView = [[UIImageView alloc] initWithFrame:self.view.bounds];
    imgView.image = parentViewImage;
    [self.view insertSubview:imgView atIndex:0];
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide) name:UIKeyboardDidHideNotification object:nil];
    
    [self setCaptionsView];
    //show wifi view
//    [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationShowWifiView] object:nil];
    

    if (self.isClickFromFind) {
        //
        self.btnCancel.frame = CGRectMake(48, 140, 108, 37);
        self.btnSubmit.frame = CGRectMake(164, 140, 108, 37);
        self.btnSubmit.hidden = NO;
    }
}
//- (void)addNotify{
//}
-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillDisappear:animated];
    [txvRemark resignFirstResponder];
//    [self keyboardDidHide];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
- (IBAction)didClickSubmit:(id)sender {
    
    if([delegate respondsToSelector:@selector(remarkViewV2SubmitWithText:andController:)])
    {
        [self.view removeFromSuperview];
        [self keyboardDidHide];
        [delegate remarkViewV2SubmitWithText:txvRemark.text andController:self];
    }
}

- (IBAction)ExitWithView:(id)sender {
    isCancel = YES;
//    [self.navigationController popViewControllerAnimated:YES];
    
    //Additional Job Pages
    if([delegate respondsToSelector:@selector(remarkViewV2DoneWithText:)]) {
        [self.view removeFromSuperview];
        [self keyboardDidHide];
        [delegate remarkViewV2DoneWithText:txvRemark.text];
    } else
    //Push from Room detail
    if([delegate respondsToSelector:@selector(remarkViewV2didDismissWithButtonIndex:)])
    {
        [self.view removeFromSuperview];
        [self keyboardDidHide];
        [delegate remarkViewV2didDismissWithButtonIndex:2];
    } else
    //Push from Room detail
    if ([delegate respondsToSelector:@selector(remarkViewV2DoneWithText:andController:)]) {
//        [delegate remarkViewV2DoneWithText:txvRemark.text andController:self];
        [txvRemark resignFirstResponder];
        if (self.isAddSubview) {
            [self.view removeFromSuperview];
        }
        
        [self keyboardDidHide];
    } else
    //Push from unassigned room
    if([delegate respondsToSelector:@selector(remarkViewV2RoomAssigndidDismissWithButtonIndex:)])
    {
        [delegate remarkViewV2RoomAssigndidDismissWithButtonIndex:2];
        [self.navigationController popViewControllerAnimated:YES];
    }

    if (delegate == nil) {
        if (self.isAddSubview) {
            [self.view removeFromSuperview];
        }
    }
}

- (IBAction)ExitKeyBoard:(id)sender {
    [txvRemark resignFirstResponder];
    [self keyboardDidHide];
}

-(void)remarkSaveDidPressed {
    // save remark data
//    NSInteger countRoomRemark = [[RoomManagerV2 sharedRoomManager] countRoomRemarkData:self.roomRecord.rrec_Id];
//    countRoomRemark ++;
//    
//    self.roomRemarkModel.rm_Id = countRoomRemark;
//    self.roomRemarkModel.rm_RecordId = self.roomRecord.rrec_Id;
//    self.roomRemarkModel.rm_content  = text;
//    [[RoomManagerV2 sharedRoomManager] insertRoomRemarkData:self.roomRemarkModel];
    
//    if ([delegate respondsToSelector:@selector(remarkViewV2DoneWithText::)]) {
//        [delegate remarkViewV2DoneWithText:txvRemark.text andController:self];
//    }
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ([text rangeOfCharacterFromSet:[NSCharacterSet newlineCharacterSet]].location == NSNotFound || [text isEqualToString:@"\n"]) {
        return YES;
    }
    
    [txvRemark resignFirstResponder];
    [self keyboardDidHide];
    return NO;
}

-(void)saveData {
    if ([delegate respondsToSelector:@selector(remarkViewV2DoneWithText:andController:)]) {
        [delegate remarkViewV2DoneWithText:txvRemark.text andController:self];
        [self.view removeFromSuperview];
        [self keyboardDidHide];
    }
}

-(void)setCaptionsView {
    [self.nvgBar.topItem setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_remark_title]];
    [self.btnCancel setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_CANCEL] forState:UIControlStateNormal];
}

-(BOOL) checkSaveDataWithTagEvent:(NSInteger) tagOfEvent {
    if (tagOfEvent != tagOfMessageButton) {
        //Show alert complete room
        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_complete_room_to_leave] delegate:nil cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles: nil]; 
        [alert show];
        
        return YES;
    }
    
    return NO;
}

#pragma mark - iOS 7 Resources
-(void)loadFlatResource
{
    [btnCancel setBackgroundImage:[UIImage imageNamed:imgNoBtnFlat] forState:UIControlStateNormal];
    [btnCancel setBackgroundImage:[UIImage imageNamed:imgNoBtnFlat] forState:UIControlStateHighlighted];
}

@end
