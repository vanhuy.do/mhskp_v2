//
//  RoomCompletedView.h
//  eHouseKeeping
//
//  Created by tms on 5/9/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RoomInspectionViewV2.h"
#import "TopbarViewV2.h"
#import "MBProgressHUD.h"

@interface RoomCompletedViewV2 : UIViewController <UINavigationControllerDelegate, UITableViewDelegate, UITableViewDataSource>{
    
    IBOutlet UITableView *roomTableView;
    NSMutableArray *section1Data;
    NSMutableArray *section2Data;
    
    IBOutlet UITableViewCell *tvCell;
    IBOutlet UILabel *lblLocation;
    UIImageView *hotelLogo;
    IBOutlet UILabel *labelRoom;
    IBOutlet UILabel *labelRMStatus;
    IBOutlet UILabel *labelVIP;
    IBOutlet UILabel *labelDurationUsed;
    IBOutlet UILabel * lbltotalPassNumber;
    IBOutlet UILabel *lblTotalFailNumber;
    
    IBOutlet UILabel * lbltotalPassTitle;
    IBOutlet UILabel *lblTotalFailTitle;

    //With face image
    IBOutlet UIButton *totalPassButton;
    IBOutlet UIButton *TotalFailButton;
    
    //Without face image
    IBOutlet UIButton *totalPassButton2;
    IBOutlet UIButton *TotalFailButton2;
    
    RoomInspectionViewV2 *roomInspectionFailViewV2;
    RoomInspectionViewV2 *roomInspectionPassViewV2;
    TopbarViewV2 * topBarView;
    NSInteger numberPass;
    NSInteger numberfail;
    NSMutableArray* listOfPassedRoom;
    NSMutableArray* listOfFailedRoom;
    
    MBProgressHUD *HUD;
}
@property (nonatomic, strong) UITableView *roomTableView;
@property (nonatomic, strong) NSMutableArray *section1Data;
@property (nonatomic, strong) NSMutableArray *section2Data;
@property (nonatomic, strong) NSMutableArray *totalRoomCleaned;
@property (nonatomic, strong) UITableViewCell *tvCell;
@property (nonatomic, strong) IBOutlet UILabel *lblLocation;
@property (nonatomic, strong) IBOutlet UIImageView *hotelLogo;
@property (strong, nonatomic) IBOutlet UILabel *lblHouseKeeperName;

@property (strong, nonatomic) IBOutlet UILabel * lbltotalPassNumber;
@property (strong, nonatomic) IBOutlet UILabel *lblTotalFailNumber;
@property (strong, nonatomic) IBOutlet UILabel * lbltotalPassTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblTotalFailTitle;


@property (nonatomic, strong) UILabel *labelRoom;
@property (nonatomic, strong) UILabel *labelRMStatus;
@property (nonatomic, strong) UILabel *labelVIP;
@property (nonatomic, strong) UILabel *labelDurationUsed;
//@property (nonatomic, strong) WifiIConViewController * wifiViewController;
@property (nonatomic, strong) TopbarViewV2 * topBarView;
@property (nonatomic, strong) RoomInspectionViewV2 *roomInspectionFailViewV2;
@property (nonatomic, strong) RoomInspectionViewV2 *roomInspectionPassViewV2;
@property (nonatomic, strong) NSMutableArray *listOfPassedRoom;
@property (nonatomic, strong) NSMutableArray *listOfFailedRoom;

-(void)drawUnderlinedLabel:(UILabel *)label inTableViewCell:(UITableViewCell *)cell;

-(void) loadDataTable;
-(void) totalStatusRoom;
-(void) setCaptionsView;
@end
