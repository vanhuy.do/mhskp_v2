//
//  RoomCompletedView.m
//  eHouseKeeping
//
//  Created by tms on 5/9/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "RoomCompletedViewV2.h"
#import "SuperRoomModelV2.h"
#import "RoomAssignmentInfoViewController.h"
#import "ehkDefines.h"
#import "TasksManagerV2.h"
#import "CheckMemory.h"
#import "TopbarViewV2.h"
#import "RoomManagerV2.h"
#import "DateTimeUtility.h"
#import "MyNavigationBarV2.h"
#import "LogFileManager.h"
#import "DeviceManager.h"

//#define titleBar            @"Room Completed"
#define backgroundImage     @"bg.png"
#define sizeTitle           22
#define sizeSubtitle        17
#define tagRoomNo           1
#define tagRMStatus         2
#define tagVIP              3
#define tagDurationUsed     4
#define tagGuestNameCell        5
#define tagUnderline        6
#define tagGuestTitle       7
#define tagName             8
#define tagDetail           9
//#define tagRoomType         10
#define numofSection        2
#define primaryRed          60
#define primaryGreen        206
#define primaryBlue         229
#define secondaryRed        22
#define secondaryGreen      103
#define secondaryBlue       120
#define thirdaryRed         6
#define thirdaryGreen       62
#define thirdaryBlue        127
#define kRoomNo             @"RoomNo"
#define kRMStatus           @"RMStatus"
#define kVIP                @"VIP"
#define kGuestName          @"GuestName"
#define kLabel              @"Label"
#define kDetail             @"Detail"
#define kRoomAssignmentID   @"RoomAssignmentID"
#define kTotalTimeUsed      @"TotalTimeUsed"
#define kDurationUsed        @"DurationUsed"

@interface RoomCompletedViewV2 (PrivateMethods)

-(void) syncRoomCompletedView;
-(void) syncOnMainThread;
-(void)setDataTable;

@end
@implementation RoomCompletedViewV2
@synthesize hotelLogo;
@synthesize lblHouseKeeperName;
@synthesize roomTableView, section1Data, section2Data, totalRoomCleaned, tvCell,lblLocation, labelDurationUsed, labelVIP, labelRMStatus, labelRoom,lblTotalFailNumber,lbltotalPassNumber,lblTotalFailTitle,lbltotalPassTitle;
@synthesize roomInspectionFailViewV2;
@synthesize roomInspectionPassViewV2;
@synthesize topBarView=_topBarView;
@synthesize listOfPassedRoom;
@synthesize listOfFailedRoom;


-(void)drawUnderlinedLabel:(UILabel *)label inTableViewCell:(UITableViewCell *)cell{
    NSString *string = label.text;
    CGSize stringSize = [string sizeWithFont:label.font];
    CGRect rect = label.frame;
    CGRect labelFrame;
    //NSLog(@"%f %f",rect.origin.x,stringSize.width);
    if (stringSize.width > rect.size.width) {
        labelFrame = CGRectMake(rect.origin.x, stringSize.height - 4, rect.size.width, 2);
    } else {
        labelFrame = CGRectMake(rect.origin.x + (rect.size.width - stringSize.width)/2.0, stringSize.height - 4, stringSize.width, 2);
    }
    UILabel *lineLabel = (UILabel *)[cell viewWithTag:tagUnderline];
    lineLabel.frame = labelFrame;
    
    lineLabel.backgroundColor =[UIColor blackColor];
}

-(void)loadDataTable {
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getLoadingData]];
    [HUD show:YES];
    [self performSelector:@selector(setDataTable) withObject:nil afterDelay:0.2];
}

-(void)setDataTable{
    
    [[HomeViewV2 shareHomeView] waitingLoadingData];
    
    [[RoomManagerV2 sharedRoomManager] getRoomAssignmentComplete];
    self.section1Data = [RoomManagerV2 sharedRoomManager].roomAssignmentList;
    
    //Hao Tran - fix bug don't have room records
    //Refresh data from WS
    if (self.section1Data && [self.section1Data count] > 0) {
        for (NSDictionary *aRoom in self.section1Data) {
            int roomAssignmentId = [[aRoom objectForKey:kRoomAssignmentID] intValue];
            //Load WS Room Detail
            [[RoomManagerV2 sharedRoomManager] getRoomWSByUserID:[UserManagerV2 sharedUserManager].currentUser.userId  AndRaID:roomAssignmentId AndLastModified:nil shouldUpdateRoomRecord:YES];
        }
    }
    
    //Get Room Assignment again after get room records from WS
    
    [[RoomManagerV2 sharedRoomManager] getTotalRoomComlete];
    self.totalRoomCleaned = [RoomManagerV2 sharedRoomManager].roomAssignmentList;
    
    [[RoomManagerV2 sharedRoomManager] getRoomAssignmentComplete];
    self.section1Data = [RoomManagerV2 sharedRoomManager].roomAssignmentList;
    
    [[RoomManagerV2 sharedRoomManager] getRoomInspectedPassed];
    self.listOfPassedRoom = [RoomManagerV2 sharedRoomManager].roomAssignmentList;
    
    [[RoomManagerV2 sharedRoomManager] getRoomInspectedFailed];
    self.listOfFailedRoom = [RoomManagerV2 sharedRoomManager].roomAssignmentList;
    
    numberPass=0;
    numberfail=0;
    if (self.listOfPassedRoom) {
        numberPass = [self.listOfPassedRoom count];
    }
    if (self.listOfFailedRoom) {
        numberfail = [self.listOfFailedRoom count];
    }
    
    
    int totalTimeCleanUsed = 0;
    int  AverageTimeCleanUsed = 0;
    int numOfInspected = 0;
    if (self.section1Data && [self.section1Data count] > 0) {
        for (NSDictionary *aRoom in self.section1Data) {
            int time = [[aRoom objectForKey:kDurationUsed] intValue];
            //Hao Tran - Fix bug
            //totalTimeCleanUsed += (time - (time%60));
            totalTimeCleanUsed += time;
            if ([[aRoom objectForKey:KInspection_Status_id] intValue] == ENUM_INSPECTION_COMPLETED_PASS || [[aRoom objectForKey:KInspection_Status_id] intValue] == ENUM_INSPECTION_COMPLETED_FAIL) {
                //numOfInspected ++;
            }
        }
        AverageTimeCleanUsed=totalTimeCleanUsed / ([self.section1Data count]);
    }
       
    numOfInspected = (int)(numberfail + numberPass);
    NSDictionary *dic = [[LanguageManagerV2 sharedLanguageManager] getCurrentDictionaryLanguage];
    
    /****************** nmtan change ******************/
    NSDictionary *s2row1 =[[NSDictionary alloc] initWithObjectsAndKeys:[dic valueForKey:[NSString stringWithFormat:@"%@", L_TOTAL_ROOM_CLEANED]],kLabel,[NSString stringWithFormat:@"%lu", (unsigned long)[self.totalRoomCleaned count]],kDetail, nil];
    
    NSDictionary *s2row2 =[[NSDictionary alloc] initWithObjectsAndKeys:[dic valueForKey:[NSString stringWithFormat:@"%@", L_total_duration_used]],kLabel,[DateTimeUtility getMM_SSFromSecond:totalTimeCleanUsed],kDetail, nil];
    
    NSDictionary *s2row3 =[[NSDictionary alloc] initWithObjectsAndKeys:[dic valueForKey:[NSString stringWithFormat:@"%@", L_average_clean_time]],kLabel,[DateTimeUtility getMM_SSFromSecond:AverageTimeCleanUsed],kDetail, nil];
    
    // add termp code for demo
    NSDictionary *s2row4 =[[NSDictionary alloc] initWithObjectsAndKeys:[dic valueForKey:[NSString stringWithFormat:@"%@", L_TOTAL_ROOM_INSPECTED]],kLabel,[NSString stringWithFormat:@"%d",numOfInspected],kDetail, nil];
    /****************** nmtan end change ******************/
    
    NSMutableArray *array2 =[[NSMutableArray alloc] initWithObjects:s2row1,s2row2,s2row3,s2row4, nil];
    self.section2Data = array2;
    [self.roomTableView reloadData];
    [self totalStatusRoom];
    
    [HUD hide:YES];
    [HUD removeFromSuperview];
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(syncRoomCompletedView) name:@"syncRoomCompletedView" object:nil];
    }
    return self;
}



- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //    UIButton *butCheckList = (UIButton *) [[self.tabBarController tabBar] viewWithTag:tagOfCheckListButton];
    //    [butCheckList setEnabled:NO];
    
    self.navigationItem.titleView = [MyNavigationBarV2 createTitleBarWithFirstRow:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_completed_label] secondRow:[[RoomManagerV2 sharedRoomManager] getHouseKeeperName] target:self selector:@selector(hiddenHeaderView) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.titleView.bounds = [MyNavigationBarV2 middleFrameForNavigationTitleView:self.navigationItem.titleView];
    
    //set captions of view
    [self setCaptionsView];
    
    //set focus on home view
    [[HomeViewV2 shareHomeView] setSelectedButton:tagOfHomeButton];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //Resize content views of current VC to fit with Navigation bar and tabbar
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        //Clear redundant view for header, footter
        CGRect frameHeaderFooter = roomTableView.tableHeaderView.frame;
        frameHeaderFooter.size.height = 0.1f;
        UIView *headerView = [[UIView alloc] initWithFrame:frameHeaderFooter];
        [roomTableView setTableHeaderView:headerView];
        
        frameHeaderFooter = roomTableView.tableFooterView.frame;
        frameHeaderFooter.size.height = 0.1f;
        UIView *footerView = [[UIView alloc] initWithFrame:frameHeaderFooter];
        [roomTableView setTableFooterView:footerView];
        
        [self loadFlatResource];
    }
    
    
    SuperRoomModelV2 *modelSuper= [[SuperRoomModelV2 alloc] init];
    if (topBarView == nil) {
        topBarView = [[TopbarViewV2 alloc] initViewWithSuperModel:modelSuper];
        [self.view addSubview:topBarView];
    }
    
    // Do any additional setup after loading the view from its nib.
    roomTableView.backgroundColor =[UIColor clearColor];
    
    //Remove faces to like Android
    [totalPassButton setHidden:YES];
    [TotalFailButton setHidden:YES];
    
    UIColor *bgColor = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:backgroundImage]];
    self.view.backgroundColor = bgColor;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reLoadDataSyncDone:) name:[NSString stringWithFormat:@"%@", notificationReloadDataAllViewWhenSyncDone] object:nil];
    //set captions of view
    [self setCaptionsView];
    
    //Hao Tran - remove for change style of title bar view
    /*
    CGRect frame = CGRectMake(0, 0, 230, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont systemFontOfSize:sizeTitle];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor colorWithRed:47.0/255.0 green:144.0/255.0 blue:186.0/255.0 alpha:1.0];
    
    UIButton *titleBarButton = [[UIButton alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
    titleBarButton.frame = frame;
    titleBarButton.titleLabel.backgroundColor = [UIColor clearColor];
    titleBarButton.titleLabel.font = FONT_BUTTON_TOPBAR;
    [titleBarButton.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
    [titleBarButton.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
    titleBarButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    titleBarButton.titleLabel.textColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:1.0];
    [titleBarButton addTarget:self action:@selector(hiddenHeaderView) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.titleView = titleBarButton;
    
    //Move title to center vertical
    CGRect titleViewBounds = self.navigationItem.titleView.bounds;
    int moveLeftTitle = 0;
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        moveLeftTitle = 10;
        [titleBarButton setContentEdgeInsets:UIEdgeInsetsMake(-10, -10, 0, 0)];
    }
    self.navigationItem.titleView.bounds = CGRectMake(titleViewBounds.origin.x + moveLeftTitle, Y_TOPBAR_TITLE, titleViewBounds.size.width, titleViewBounds.size.height);
    */
    
    //Hao Tran - END
    
    //back button navigation
    self.navigationItem.leftBarButtonItem = [MyNavigationBarV2 createBackButtonWithTarget:self selector:@selector(btnBackPressed) forControlEvents:UIControlEventTouchUpInside];
    
    //Resize all views
    //[self modifyAllViews];
    
    //load Data for room completed after view did load
    [self loadDataTable];
}

-(void)btnBackPressed
{
    [self.navigationController popViewControllerAnimated:YES];
}

// hidden header when tap navigationBar
#define heightScale 45
- (void) hiddenHeaderView {
    if (![UserManagerV2 isSupervisor]){
        BOOL isHidden = topBarView.hidden;
        [topBarView setHidden:!isHidden];
        
        
        CGRect frame1 = CGRectZero;
        CGRect frame2 = CGRectZero;
        CGRect frame3 = CGRectZero;
        CGRect frame4 = CGRectZero;
        CGRect frame5 = CGRectZero;
        
        if(!isHidden){
            frame1  = CGRectMake(0, roomTableView.frame.origin.y - heightScale, roomTableView.frame.size.width, roomTableView.frame.size.height + heightScale);
            frame2 = CGRectMake(labelRoom.frame.origin.x, labelRoom.frame.origin.y - heightScale, labelRoom.frame.size.width, labelRoom.frame.size.height );
            frame3 = CGRectMake(labelRMStatus.frame.origin.x, labelRMStatus.frame.origin.y - heightScale, labelRMStatus.frame.size.width, labelRMStatus.frame.size.height );
            frame4 = CGRectMake(labelVIP.frame.origin.x, labelVIP.frame.origin.y - heightScale, labelVIP.frame.size.width, labelVIP.frame.size.height );
            frame5 = CGRectMake(labelDurationUsed.frame.origin.x, labelDurationUsed.frame.origin.y - heightScale, labelDurationUsed.frame.size.width, labelDurationUsed.frame.size.height);
        }
        else
        {
            frame1 = CGRectMake(0, roomTableView.frame.origin.y + heightScale, roomTableView.frame.size.width, roomTableView.frame.size.height - heightScale);
            frame2 = CGRectMake(labelRoom.frame.origin.x, labelRoom.frame.origin.y + heightScale, labelRoom.frame.size.width, labelRoom.frame.size.height);
            frame3 = CGRectMake(labelRMStatus.frame.origin.x, labelRMStatus.frame.origin.y + heightScale, labelRMStatus.frame.size.width, labelRMStatus.frame.size.height);
            frame4 = CGRectMake(labelVIP.frame.origin.x, labelVIP.frame.origin.y + heightScale, labelVIP.frame.size.width, labelVIP.frame.size.height );
            frame5 = CGRectMake(labelDurationUsed.frame.origin.x, labelDurationUsed.frame.origin.y + heightScale, labelDurationUsed.frame.size.width, labelDurationUsed.frame.size.height );
        }
        
        [roomTableView setFrame:frame1];
        [labelRoom setFrame:frame2];
        [labelRMStatus setFrame:frame3];
        [labelVIP setFrame:frame4];
        [labelDurationUsed setFrame:frame5];
        
        
    }
}

//@ reload data after sync done.
-(void)reLoadDataSyncDone:(NSNotification *)notificate
{
    [self setDataTable];
}
- (void)viewDidUnload
{
    
    roomTableView = nil;
    tvCell = nil;
    self.section1Data = nil;
    self.section2Data = nil;
    [self setHotelLogo:nil];
    totalPassButton = nil;
    TotalFailButton = nil;
    [self setLblHouseKeeperName:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark -
#pragma mark TableViewDelegate Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    //NSLog(@"numofsection %d",numofSection);
    return numofSection;
}
-(void) totalStatusRoom{
    lbltotalPassNumber.text=[NSString stringWithFormat:@"%d",(int)numberPass];
    lblTotalFailNumber.text=[NSString stringWithFormat:@"%d",(int)numberfail];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    switch (section) {
        case 0:
            if([self.section1Data count] <= 0){
                return 1;
            } else {
                return [self.section1Data count];
            }
            break;
        case 1:
            return [self.section2Data count];
            break;
            
        default:
            return 0;
            break;
    }
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return @"";
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.section) {
        case 0:
            return 44;
            break;
        case 1:
        {
            CGRect cgrDetailLabel = CGRectMake(175, 12, 130, 21);
            NSDictionary *rowData =[self.section2Data objectAtIndex:indexPath.row];
            NSString *tmp = [rowData objectForKey:kDetail];
            CGSize t = [tmp sizeWithFont:[UIFont systemFontOfSize:sizeSubtitle] constrainedToSize:CGSizeMake(130, 300) lineBreakMode:NSLineBreakByWordWrapping];
            if (t.height < 20) {
                cgrDetailLabel.origin.y = 0;
                break;
            } else
            {
                t.height += 24;
                cgrDetailLabel.size = t;
            }
            return cgrDetailLabel.size.height;
        }
            break;
        default:
            break;
    }
    return 44;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *sectionIdentifier = @"SectionsIdentifier";
    static NSString *sectionIdentifierNoData = @"SectionIdentifierNodata";
    static NSString *section2Identifier = @"Section2Identifier";
    NSUInteger section =[indexPath section];
    NSUInteger row = [indexPath row];
    UITableViewCell *cell = nil;
    
    if (section == 0) {
        if([self.section1Data count] <= 0){
            cell = [tableView dequeueReusableCellWithIdentifier:sectionIdentifierNoData];
        } else {
            cell = [tableView dequeueReusableCellWithIdentifier:sectionIdentifier];
        }
    } else if ( section == 1 )
    {
        cell = [tableView dequeueReusableCellWithIdentifier:section2Identifier];
    }
    if (cell == nil) {
        if ( section == 0 )
        {
            if([self.section1Data count] > 0) {
                NSArray *nibCell = [[NSBundle mainBundle] loadNibNamed:@"RoomCompletedCellV2" owner:self options:nil];
                
                if ([nibCell count] > 0) {
                    cell = self.tvCell;
                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                }
            } else {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:sectionIdentifierNoData];
            }
        } else if(section == 1) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:section2Identifier] ;
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        }
    }
    
    /******** section 0 ************/
    if (section == 0) {
        if([self.section1Data count] <= 0){
            
            cell.textLabel.textAlignment = NSTextAlignmentLeft;
            cell.textLabel.textColor =[UIColor colorWithRed:thirdaryRed/225.0f green:thirdaryGreen/225.0f blue:thirdaryBlue/225.0f alpha:1.0f];
            cell.textLabel.font = [UIFont boldSystemFontOfSize:sizeSubtitle];
            
            [cell.textLabel setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_NO_RESULT_FOUND]];
        } else {
            [cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
            
            NSDictionary *rowData =[self.section1Data objectAtIndex:row];
            UILabel *labelGuestTitle = (UILabel *)[cell viewWithTag:tagGuestTitle];
            [labelGuestTitle setHidden:YES];
            [labelGuestTitle setText:[NSString stringWithFormat:@"%@:",[[[LanguageManagerV2 sharedLanguageManager] getCurrentDictionaryLanguage] valueForKey:[NSString stringWithFormat:@"%@", L_GUEST_NAME]]]];
            
            UILabel *labelRoomNo = (UILabel *)[cell viewWithTag:tagRoomNo];
            labelRoomNo.text =[rowData objectForKey:kRoomNo];
            
            UILabel *lblRMStatus =(UILabel *)[cell viewWithTag:tagRMStatus];
            lblRMStatus.text =[rowData objectForKey:kRMStatus];
            
            UILabel *lblVIP =(UILabel *)[cell viewWithTag:tagVIP];
            NSString *vip = (NSString *)[rowData objectForKey:kVIP];
            [lblVIP setText:(vip == nil || [vip isEqualToString:@""] || [vip isEqualToString:@"0"])?@"-":vip];
            
            UILabel *lblDurationUsed =(UILabel *)[cell viewWithTag:tagDurationUsed];
            NSString* durationUsed = (NSString*)[rowData objectForKey:kDurationUsed];
            
            UIImageView *blockRoomStatus = (UIImageView*)[cell.contentView viewWithTag:tagBlockingImage];
            
            RoomBlocking *curRoomBlocking = [[RoomManagerV2 sharedRoomManager] loadRoomBlockingByUserId:[UserManagerV2 sharedUserManager].currentUser.userId roomNumber:[rowData objectForKey:kRoomNo]];
            
            if(curRoomBlocking.roomblocking_is_blocked == RoomBlockingStatus_Release) {
                [blockRoomStatus setHidden:NO];
                [blockRoomStatus setImage:[UIImage imageNamed:imgReleaseRoom]];
            } else if (curRoomBlocking.roomblocking_is_blocked == RoomBlockingStatus_Blocked){
                [blockRoomStatus setHidden:NO];
                [blockRoomStatus setImage:[UIImage imageNamed:imgBlockRoom]];
            } else {
                [blockRoomStatus setHidden:YES];
            }
            
            //calculate forward time or late cleaning time
            NSInteger durationInspected = [durationUsed integerValue];
            if (durationInspected < 0) {
                durationInspected = 0;
            }
            NSInteger expectedTime = [[rowData objectForKey:kExpected_Clean_time] integerValue] * 60;
            NSInteger differenceTime = 0;
            differenceTime = durationInspected;
            
            NSString *durationString = nil;
            durationString = [DateTimeUtility getMM_SSFromSecond:(int)differenceTime];
            if (durationInspected > expectedTime) {
                [lblDurationUsed setText:[NSString stringWithFormat:@"%@", durationString]];
            } else {
                [lblDurationUsed setText:[NSString stringWithFormat:@"%@", durationString]];
            }
            
            if ([durationUsed intValue] > [[rowData objectForKey:kExpected_Clean_time] intValue] * 60)  {
                [lblDurationUsed setTextColor: [UIColor redColor]];
            }
            else{
                [lblDurationUsed setTextColor: [UIColor greenColor]];
            }
            
            UILabel *labelGuestName = (UILabel *)[cell viewWithTag:tagGuestNameCell];
            labelGuestName.text = [rowData objectForKey:kGuestName];
            
            UILabel *lblRoomType = (UILabel *)[cell viewWithTag:tagRoomType];
            lblRoomType.text = [rowData objectForKey:kRoomType];
        }
    }
    
    /******** section 1 ************/
    else if (section == 1) {
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        NSDictionary *rowData =[self.section2Data objectAtIndex:row];
        BOOL canRelease = NO;
        CGRect cgrNameLabel = CGRectMake(15, 12, 270, 21);
        UILabel *lblName = (UILabel *)[cell viewWithTag:tagName];
        if (lblName == nil) {
            canRelease = YES;
            lblName = [[UILabel alloc] initWithFrame:cgrNameLabel];
        }
        
        [lblName setFrame:cgrNameLabel];
        lblName.textAlignment = NSTextAlignmentLeft;
        lblName.textColor =[UIColor colorWithRed:thirdaryRed/225.0f green:thirdaryGreen/225.0f blue:thirdaryBlue/225.0f alpha:1.0f];
        lblName.text = [rowData objectForKey:kLabel];
        lblName.font = [UIFont boldSystemFontOfSize:sizeSubtitle];
        [lblName setTag:tagName];
        
        
        CGRect cgrDetailLabel = CGRectMake(205, 12, 100, 21);
        NSInteger numOfRow = 1;
        NSString *tmp = [rowData objectForKey:kDetail];
        CGSize t = [tmp sizeWithFont:[UIFont systemFontOfSize:sizeSubtitle] constrainedToSize:CGSizeMake(130, 300) lineBreakMode:NSLineBreakByWordWrapping];
        if (t.height < 20) {
            cgrDetailLabel.origin.y = 10;
        } else
        {
            t.width = 100;
            t.height += 10;
            cgrDetailLabel.size = t;
            numOfRow = t.height / 20;
        }
        
        UILabel *lblDetail = (UILabel *)[cell viewWithTag:tagDetail];
        if (lblDetail == nil) {
            lblDetail = [[UILabel alloc] initWithFrame:cgrDetailLabel];
        }
        [lblDetail setFrame:cgrDetailLabel];
        lblDetail.textAlignment = NSTextAlignmentRight;
        lblDetail.textColor = [UIColor colorWithRed:51/225.0f green:51/225.0f blue:51/225.0f alpha:1.0f];
        lblDetail.text = [rowData objectForKey:kDetail];
        lblDetail.font = [UIFont systemFontOfSize:sizeSubtitle];
        //        lblDetail.tag = row + 1;
        [lblDetail setLineBreakMode:NSLineBreakByWordWrapping];
        [lblDetail setNumberOfLines:numOfRow];
        [lblDetail setTag:tagDetail];
        
        [cell setBackgroundColor:[UIColor whiteColor]];
        
        if (canRelease == YES) {
            [cell addSubview:lblName];
            [cell addSubview:lblDetail];
        }
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSInteger section = [indexPath section];
    if (section == 0) {
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        [self drawUnderlinedLabel:(UILabel *)[cell viewWithTag:tagRMStatus] inTableViewCell:cell];
        
        // Show hud when get room detail and get info before go to room detail
        MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
        [hud setLabelText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_RA_LOADING_DATA_CONTENT]];
        [hud show:YES];
        [self.tabBarController.view addSubview:hud];
        
        //        UIViewController *aRoomInfo = nil;
        
        NSDictionary *rowData =[self.section1Data objectAtIndex:indexPath.row];
        
        RoomAssignmentInfoViewController *roomAssignmentInfoController = [[RoomAssignmentInfoViewController alloc] initWithOption:IS_PUSHED_FROM_COMPLETE andRoomAssignmentData:rowData];
        
        [TasksManagerV2 setCurrentRoomAssignment:[[rowData objectForKey:kRoomAssignmentID] integerValue]];
        [self.navigationController pushViewController:roomAssignmentInfoController animated:YES];
        [self.navigationItem.backBarButtonItem setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK]];
        
//#warning dthanhson edit : remove
        ////        NSInteger userId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
        ////        NSInteger raId = [[rowData objectForKey:kRoomAssignmentID] integerValue];
        //        NSInteger roomId = [[rowData objectForKey:kRoomNo] integerValue];
        //
        //        RoomModelV2 *roomModel = [[RoomModelV2 alloc] init];
        //        roomModel.room_Id = roomId;
        ////
        ////        // load room model
        ////        roomModel.room_HotelId = -1;
        ////        [[RoomManagerV2 sharedRoomManager] loadRoomModel:roomModel];
        ////
        ////        if (roomModel.room_HotelId == -1) {
        ////            // Get room detail
        ////            [[RoomManagerV2 sharedRoomManager] getRoomWSByUserID:userId AndRaID:raId AndLastModified:nil AndLastCleaningDate:nil];
        ////        }
        ////
        ////        GuestInfoModelV2 *guestInfo = [[GuestInfoModelV2 alloc] init];
        ////        guestInfo.guestRoomId = roomId;
        ////        if (guestInfo.guestId == 0) {
        ////            // Get guest info
        ////            [[RoomManagerV2 sharedRoomManager] updateGuestInfo:userId WithRoomAssignID:raId];
        ////        }
        //
        ////        RoomModelV2 *roomModel = [[RoomModelV2 alloc] init];
        //        roomModel.room_Id = [[rowData objectForKey:kRoomNo] intValue];
        //        [[RoomManagerV2 sharedRoomManager] loadRoomModel:roomModel];
        //
        //        RoomAssignmentModelV2 *roomAssignmentModel = [[RoomAssignmentModelV2 alloc] init];
        //        roomAssignmentModel.roomAssignment_Id = [[rowData objectForKey:kRoomAssignmentID] intValue];
        //        roomAssignmentModel.roomAssignment_UserId = [UserManagerV2 sharedUserManager].currentUser.userId;
        //        [[RoomManagerV2 sharedRoomManager] load:roomAssignmentModel];
        //
        //
        //        //set current room no
        //        [RoomManagerV2 setCurrentRoomNo:roomModel.room_Id];
        //
        //        if (roomModel.room_expected_status_id == ENUM_INSPECTION_COMPLETED_PASS) {
        //            //pass
        //            aRoomInfo = [[RoomInfoCompletedPassedV2 alloc] initWithNibName:@"RoomInfoCompletedPassedV2" bundle:nil];
        //        } else
        //            if (roomModel.room_expected_status_id == ENUM_INSPECTION_COMPLETED_FAIL) {
        //                //fail
        //                aRoomInfo = [[RoomInfoCompletedFailedV2 alloc] initWithNibName:@"RoomInfoCompletedFailedV2" bundle:nil];
        //            } else {
        //                //not inspected
        //                aRoomInfo = [[RoomInfoCompletedNotInspectedV2 alloc] initWithNibName:@"RoomInfoCompletedNotInspectedV2" bundle:nil];
        //            }
        //
        //        GuestInfoModelV2 *guestInfoModel = [[GuestInfoModelV2 alloc] init];
        //        guestInfoModel.guestRoomId = roomModel.room_Id;
        //        [[RoomManagerV2 sharedRoomManager] loadByRoomIDGuestInfo:guestInfoModel];
        //
        //        [(id)aRoomInfo setRoomName: [rowData objectForKey:kRoomNo]];
        //        [(id)aRoomInfo setRoomModel : roomModel];
        //        [(id)aRoomInfo setGuestInfoModel : guestInfoModel];
        //        [(id)aRoomInfo setRaDataModel:roomAssignmentModel];
        //
        //        //set current RoomAssignmentId use in ChecklistView
        //        [TasksManagerV2 setCurrentRoomAssignment:[[rowData objectForKey:kRoomAssignmentID] integerValue]];
        //        [self.navigationController pushViewController:aRoomInfo animated:YES];
        //        [(id)aRoomInfo loadingData];
        //        [self.navigationItem.backBarButtonItem setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK]];
//#warning end
        
        [cell setSelected:NO animated:NO];
        
        // hide hud
        [hud setHidden:YES];
        [hud removeFromSuperview];
    }
    
}

#pragma mark -
#pragma mark syncMethods
// Method accept notify message from subthread
-(void)syncRoomCompletedView{
    [self performSelectorOnMainThread:@selector(syncOnMainThread) withObject:nil waitUntilDone:NO];
}

-(void)syncOnMainThread{
    //    NSLog(@"syn OnMainThread");
    NSDictionary *row =[[NSDictionary alloc] initWithObjectsAndKeys:@"10111",kRoomNo,@"OC",kRMStatus,@"1",kVIP,@"1",kDurationUsed,@"Ms. Joanna Lee",kGuestName, nil];
    [self.section1Data addObject:row];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[self.section1Data count]-1 inSection:0];
    
    [roomTableView insertRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationBottom];
}

-(void)setCaptionsView {
    NSDictionary *dic = [[LanguageManagerV2 sharedLanguageManager] getCurrentDictionaryLanguage];
    self.labelRoom.text = [dic valueForKey:[NSString stringWithFormat:@"%@", L_rm]];
    self.labelRMStatus.text = [dic valueForKey:[NSString stringWithFormat:@"%@", L_RM_STATUS]];
    self.labelVIP.text = [dic valueForKey:[NSString stringWithFormat:@"%@", L_VIP]];
    self.labelDurationUsed.text = [dic valueForKey:[NSString stringWithFormat:@"%@", L_DURATION_USED]];
    
    //Hao Tran - Remove for fix ui to match with android
    /*
    lblTotalFailTitle.text  = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_total];
    lbltotalPassTitle.text  = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_total];
    */
    
    //Label
    [lbltotalPassTitle setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_passed]];
    [lblTotalFailTitle setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_failed]];
    
    //Hao Tran - Remove for no need to change
    /*
     UIButton *label = (UIButton *)self.navigationItem.titleView;
    //check whether user is supervisor
    [label setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOMS_COMPLETED] forState:UIControlStateNormal];
    
    if (label.titleLabel.text.length >= 14) {
        label.titleLabel.font = FONT_SMALL_BUTTON_TOPBAR;
    }*/
    //refresh topview
    [topBarView refresh:[[SuperRoomModelV2 alloc] init]];
}

#pragma mark
#pragma mark room inspection
- (IBAction)totalPassDidSelect:(id)sender {
    
    //Remove check for similar to Android behaviour
    /*
    if (numberPass == 0) {
        return;
    }*/
    if(roomInspectionPassViewV2 == nil){
        roomInspectionPassViewV2 = [[RoomInspectionViewV2 alloc] initWithNibName:@"RoomInspectionViewV2" bundle:nil];
    }
    
    roomInspectionPassViewV2.isPass = YES;
    roomInspectionPassViewV2.section1Data = listOfPassedRoom;
    [self.navigationController pushViewController:roomInspectionPassViewV2 animated:YES];
    
    
}

- (IBAction)totalFailDidSelect:(id)sender {
    //Remove check for similar to Android
    /*
    if (numberfail == 0) {
        return;
    }*/
    if(roomInspectionFailViewV2 == nil){
        roomInspectionFailViewV2 = [[RoomInspectionViewV2 alloc] initWithNibName:@"RoomInspectionViewV2" bundle:nil];
    }
    
    roomInspectionFailViewV2.isPass = NO;
    roomInspectionFailViewV2.section1Data = listOfFailedRoom;
    [self.navigationController pushViewController:roomInspectionFailViewV2 animated:YES];
    
}

#pragma mark - Resize layout

//-(void)modifyAllViews
//{
//    int deviceKind = [DeviceManager getDeviceScreenKind];
//    
//    //move buttons at bottom of view for 4.0 inch screen
//    if(deviceKind == DeviceScreenKindRetina4_0){
//        CGRect flblTotalPassTitle = lbltotalPassTitle.frame;
//        CGRect flblTotalPassNumber = lbltotalPassNumber.frame;
//        CGRect ftotalFailButton = TotalFailButton.frame;
//        CGRect ftotalFailButton2 = TotalFailButton2.frame;
//        CGRect flbltotalFailTitle = lblTotalFailTitle.frame;
//        CGRect flblTotalFailNumber = lblTotalFailNumber.frame;
//        CGRect ftotalPassButton = totalPassButton.frame;
//        CGRect ftotalPassButton2 = totalPassButton2.frame;
//        
//        //Move down 80 px for 4.0 inch screen
//        flblTotalPassTitle.origin.y += 85;
//        flblTotalPassNumber.origin.y += 85;
//        ftotalFailButton.origin.y += 85;
//        ftotalFailButton2.origin.y += 85;
//        flbltotalFailTitle.origin.y += 85;
//        flblTotalFailNumber.origin.y += 85;
//        ftotalPassButton.origin.y += 85;
//        ftotalPassButton2.origin.y += 85;
//        
//        //Set frame again
//        [lbltotalPassTitle setFrame:flblTotalPassTitle];
//        [lbltotalPassNumber setFrame:flblTotalPassNumber];
//        [TotalFailButton setFrame:ftotalFailButton];
//        [TotalFailButton2 setFrame:ftotalFailButton2];
//        [lblTotalFailTitle setFrame:flbltotalFailTitle];
//        [lblTotalFailNumber setFrame:flblTotalFailNumber];
//        [totalPassButton setFrame:ftotalPassButton];
//        [totalPassButton2 setFrame:ftotalPassButton2];
//    }
//}

#pragma mark - iOS 7 Resources
-(void)loadFlatResource
{
    [TotalFailButton2 setBackgroundImage:[UIImage imageNamed:imgButtonFlat] forState:UIControlStateNormal];
    [TotalFailButton2 setBackgroundImage:[UIImage imageNamed:imgButtonFlat] forState:UIControlStateHighlighted];
    [totalPassButton2 setBackgroundImage:[UIImage imageNamed:imgButtonFlat] forState:UIControlStateNormal];
    [totalPassButton2 setBackgroundImage:[UIImage imageNamed:imgButtonFlat] forState:UIControlStateHighlighted];
}

@end
