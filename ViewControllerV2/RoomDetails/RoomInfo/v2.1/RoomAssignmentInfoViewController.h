//
//  RoomAssignmentInfoViewController.h
//  mHouseKeeping
//
//  Created by Khanh Phan on 1/25/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RoomModelV2.h"
#import "ActionPopupView.h"
#import "ConfirmPopupView.h"
#import "RemarkViewV2.h"
#import "CleaningStatusViewV2.h"
#import "GuestInfoViewV2.h"
#import "CountPopupReminderViewV2.h"
#import "CleaningStatusPopupView.h"
#import "CleaningStatusConfirmPopupView.h"
#import "AccessRight.h"
#import "TopbarViewV2.h"
#import "ehkDefinesV2.h"

@interface RoomAssignmentInfoViewController : UIViewController<UINavigationControllerDelegate, UITableViewDelegate, UITableViewDataSource, UIAlertViewDelegate,UIGestureRecognizerDelegate, ActionPopupDelegate, ConfirmPopupDelegate, CleaningStatusConfirmPopupDelegate, CleaningStatusPopupDelegate, RemarkViewV2Delegate, cleaningStatusViewDelegate, CountPopupReminderViewV2Delegate, MBProgressHUDDelegate, UIScrollViewDelegate>
{
    IBOutlet UIView *viewDetail;
    IBOutlet UIView *viewAction;
    IBOutlet UIView *viewChecklistOverall;
    IBOutlet UIImageView *imgBgCheckListOverall;
    
    IBOutlet UIButton *btnGuestInfo;
    IBOutlet UIButton *btnRoomAssignmentInfo;
    IBOutlet UIButton *btnStartStop;
    IBOutlet UIButton *btnCleaningStatus;
    IBOutlet UIButton *btnAction;
    IBOutlet UIButton *btnReassign;
    IBOutlet UIButton *btnPostingHistory;
    IBOutlet UIButton *btnChecklist;
    IBOutlet UIButton *btnComplete;
    IBOutlet UIButton *btnTimer;
    
    IBOutlet UIImageView *imgRushOrQueueRoom;
    IBOutlet UIImageView *imgRoomStatus;
    IBOutlet UIImageView *imgChecklistIcon;
    
    IBOutlet UILabel *lblRoomStatus;
    IBOutlet UILabel *lblChecklistResult;
    IBOutlet UILabel *lblOverrall;
    
    IBOutlet UIScrollView *scviewRoomAssignmentDetail;
    
    IBOutlet UILabel *lblGuestTitle;
    
    IBOutlet UIImageView *imgProfileNotes; // CFG [20160928/CRF-00000827]
    IBOutlet UIImageView *imgStayOver; // CRF [20161106/CRF-00001293]
    
    CleaningStatusPopupView *cleaningStatusPopup;
    CleaningStatusConfirmPopupView *cleaningStatusConfirmPopup;
    ActionPopupView *actionPopup;
    ConfirmPopupView *confirmPopup;
    RemarkViewV2 *remarkView;
    RemarkViewV2 *physicalCheckRemarkView;
    CleaningStatusViewV2 *roomStatusView;
    
    //GuestInfoViewV2 *guestInfo;
    //GuestInfoViewV2 *guestArrivalInfo;
    
    NSMutableArray *listGuestModels;
    NSMutableArray *listGuestViews;
    UIScrollView *guestScrollView;
    UIImageView *guestIndicatorLeft;
    UIImageView *guestIndicatorRight;
    
    CountPopupReminderViewV2 *countReminderPopup;
    TopbarViewV2 * topBarView;
    
    RoomModelV2 *roomModel;
    RoomAssignmentModelV2 *roomAssignmentModel;
    RoomRecordModelV2 *roomRecordModel;
    RoomRemarkModelV2 *roomRemarkModel;
    RoomRecordDetailModelV2 *roomRecordDetailModel;
    RoomStatusModelV2 *roomStatusModel;
    
    NSInteger duration;
    NSInteger timeLeft;
    NSTimer *timer;
    NSInteger roomStatusId;
    NSInteger roomStatusIdSelected;
    NSString *roomStatusTextSelected;
    UIImage *roomStatusImageSelected;
    int fromRemarkView;
    enum TIMER_STATUS timerStatus;
    
    BOOL isSaved;
    BOOL isStopButtonClicked;
    BOOL isSubmitCheckList;
    BOOL isCheckListPass;
    enum IS_PUSHED_FROM isPushFrom;
    NSInteger userId;
    
    AccessRight *actionLostAndFound;
    AccessRight *actionEngineering;
    AccessRight *actionLaundry;
    AccessRight *actionMinibar;
    AccessRight *actionLinen;
    AccessRight *actionAmenities;
    AccessRight *checklist;
    AccessRight *guideline;
    AccessRight *manualUpdateRoomStatus;
    
    CGSize contentRoomAssignmentSize;
    
    NSMutableArray *listAddJobs;
    BOOL isAlreadyLoadWSData;
    BOOL isFromGuestInfo;
    BOOL isAlreadyReassign;
}
@property (nonatomic, strong) NSMutableArray *listGuestViews;
@property (nonatomic, strong) IBOutlet UITableView *tbViewDetail;
@property (nonatomic, strong) NSDictionary *roomAssignment;
@property (nonatomic, assign) BOOL isAutoStartOnLoad;
@property (nonatomic, assign) enum IS_PUSHED_FROM isPushFrom;

//To check complete posting before complete room
@property (nonatomic, assign) BOOL isAlreadyPostedLinen;
@property (nonatomic, assign) BOOL isAlreadyPostedMiniBar;
@property (nonatomic, assign) BOOL isAlreadyPostedAmenities;
@property BOOL isLoadFromFind;
-(IBAction) btnStartStopClicked:(id)sender;
-(IBAction) btnCleaningStatusClicked:(id)sender;
-(IBAction) btnActionClicked:(id)sender;
-(IBAction) btnCompleteClicked:(id)sender;
-(IBAction) btnRoomAssignmentInfo:(id)sender;
-(IBAction) btnGuestInfo:(id)sender;
-(IBAction) btnReassignClicked:(id)sender;
-(IBAction) btnChecklistClicked:(id)sender;

-(id) initWithOption:(enum IS_PUSHED_FROM) isPushFrom andRoomAssignmentData:(NSDictionary *)roomAssignment;
-(void) countDownWithTimeDif:(NSInteger) timeDif;

@end
