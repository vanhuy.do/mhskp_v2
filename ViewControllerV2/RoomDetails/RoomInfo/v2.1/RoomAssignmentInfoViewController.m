 //
//  RoomAssignmentInfoViewController.m
//  mHouseKeeping
//
//  Created by Khanh Phan on 1/25/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "RoomAssignmentInfoViewController.h"
#import "RoomManagerV2.h"
#import "CleaningStatusViewV2.h"
#import "EngineeringCaseViewV2.h"
#import "NetworkCheck.h"
#import "GuidelineViewV2.h"
#import "CustomAlertViewV2.h"
#import "RoomManagerV2.h"
#import "CheckListViewV2.h"
#import "RoomReassignmentViewController.h"
#import "AccessRightModel.h"
#import "SuperRoomModelV2.h"
#import "NSString+Common.h"
//#import "HomeViewV2.h"
#import "MyNavigationBarV2.h"
#import "ehkConvert.h"
#import "LogFileManager.h"
#import "FindPendingViewV2.h"
#import "DeviceManager.h"
#import "AdditionalJobDetailsScreenV2.h"
#import "iToast.h"
#import "UIImage+Tint.h"
#import "LanguageManagerV2.h"
#import "HomeViewV2.h"
#import "NSString+Common.h"
#import "HistoryPosting.h"
#import "AdditionalJobDetailCell.h"
#import "CleaningTimeManager.h"
#import "RoomManagerV2.h"
#define tagFirstContent 1
#define tagSecondContent 2
#define tagThirthContent 3
#define tagForthContent 4
#define tagAdditionalJob 5
#define tagPhysicalCheckRemark 6

#define defaultDate                             @"1900-01-01 00:00:00"

#define spacing                                 8
#define bottomPadding                           5
#define viewButtonWidth                         300
#define tbViewDetailHeight                      360 //290 old
#define mediumButtonHeight                      45
#define largeButtonHeight                       55

#define scrollViewOriginalSize                  CGSizeMake(320,347)
#define tableViewOrigin                         CGPointMake(0, 0)
#define viewButtonOrigin                        CGPointMake(10, 300)
#define viewChecklistOverallFrame               CGRectMake(0, 0, 320, 50)

#define notSelectRoomStatusYet                  -1
#define keyAdditionalJobDetail                  @"ADDJOB_DETAIL"
#define keyAdditionalJobCategory                @"ADDJOB_CATEGORY"
#define tagHUDFirstLoadingData                  5647
//#define tagMessageBack                          1358

@interface RoomAssignmentInfoViewController () {
    RoomAssignmentModelV2 *ramodelController;
}
@end
@implementation RoomAssignmentInfoViewController
//@synthesize listGuestViews;
@synthesize roomAssignment;
@synthesize isAutoStartOnLoad;
@synthesize isPushFrom;
@synthesize isAlreadyPostedLinen;
@synthesize isAlreadyPostedMiniBar;
@synthesize isAlreadyPostedAmenities;

//Local tags
int tagConfirmDone = 1357;
int tagConfirmDiscrepantRoomStatus = 1358;
int tagConfirmDoneWithPosting = 1359;
int tagCompleteRoomByRoomStatus = 1360;
int tagForceCloseRoomDetail = 1361;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        isAutoStartOnLoad = NO;
    }
    return self;
}

-(id) initWithOption:(enum IS_PUSHED_FROM) _isPushFrom andRoomAssignmentData:(NSDictionary *)_roomAssignment;
{
    self = [super initWithNibName:@"RoomAssignmentInfoViewController" bundle:nil];
    if (self) {
        isPushFrom = _isPushFrom;
        isAutoStartOnLoad = NO;
        [self setRoomAssignment:_roomAssignment];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (![[UserManagerV2 sharedUserManager].currentUserAccessRight findRoomRemarkUpdate]) {
        self.isLoadFromFind = NO;
    }
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        //Clear redundant view for header, footter
        CGRect frameHeaderFooter = self.tbViewDetail.tableHeaderView.frame;
        frameHeaderFooter.size.height = 0.1f;
        UIView *headerView = [[UIView alloc] initWithFrame:frameHeaderFooter];
        [self.tbViewDetail setTableHeaderView:headerView];
        
        frameHeaderFooter = self.tbViewDetail.tableFooterView.frame;
        frameHeaderFooter.size.height = 0.1f;
        UIView *footerView = [[UIView alloc] initWithFrame:frameHeaderFooter];
        [self.tbViewDetail setTableFooterView:footerView];
    }
    
    [self loadAccessRights];
    
    //Resize content views of current VC to fit with Navigation bar and tabbar
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
//    [self initializeView];
    [self addButtonHandleShowHideTopbar];
    [self insertTopBarView];
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        [self loadFlatResource];
    }
    
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getLoadingData]];
    [self.tabBarController.view addSubview:HUD];
    [HUD setTag:tagHUDFirstLoadingData];
    HUD.delegate = self;
    
    isAlreadyLoadWSData = NO;
    [HUD showWhileExecuting:@selector(loadData) onTarget:self withObject:nil animated:YES];
    
    isAlreadyPostedLinen = NO;
    isAlreadyPostedMiniBar = NO;
    isAlreadyPostedAmenities = NO;
    
    roomStatusIdSelected = notSelectRoomStatusYet;
    //[self loadData];
    
}

-(void)hudWasHidden:(MBProgressHUD *)hud
{
    [hud removeFromSuperview];
    
    if(hud.tag == tagHUDFirstLoadingData){
        [self initViewWillAppear];
        [self initViewDidAppear];
    }
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    ramodelController = [[RoomAssignmentModelV2 alloc] init];
    ramodelController.roomAssignment_Id = roomAssignmentModel.roomAssignment_Id;
    ramodelController.roomAssignment_UserId = [UserManagerV2 sharedUserManager].currentUser.userId;
    [[RoomManagerV2 sharedRoomManager] load:ramodelController];
    [self initViewWillAppear];
}

-(void) initViewWillAppear
{
    //Checking whether showing hud loading data or not.
    //hudWasHidden: function will handle view lifecycle
    if(!isAlreadyLoadWSData){
        return;
    }
    
    [lblGuestTitle setText:[L_GUEST_INFO currentKeyToLanguage]];
    [lblOverrall setText:[L_OVERRAL currentKeyToLanguage]];
    
    //Set high light button menu
    if(isPushFrom == IS_PUSHED_FROM_FIND_BY_RA || isPushFrom == IS_PUSHED_FROM_FIND_BY_ROOM){
        [[HomeViewV2 shareHomeView] setSelectedButton:tagOfFindButton];
    } else if(isPushFrom == IS_PUSHED_FROM_ROOM_ASSIGNMENT || isPushFrom == IS_PUSHED_FROM_COMPLETE){
        [[HomeViewV2 shareHomeView] setSelectedButton:tagOfHomeButton];
    }
    
    [scviewRoomAssignmentDetail setHidden:YES];
    [viewAction setHidden:YES];
    [viewChecklistOverall setHidden:YES];
    
    
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getLoadingData]];
    [self.tabBarController.view addSubview:HUD];
    HUD.delegate = self;
    [HUD show:YES];
    
    //For showing room remark at the same as Room Detail
    if([[UserManagerV2 sharedUserManager].currentUserAccessRight isRemarkConsolidation]) {
        NSString *remarkWS = [[RoomManagerV2 sharedRoomManager] getRoomRemarksByUserId:[UserManagerV2 sharedUserManager].currentUser.userId hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId remarkType:RemarkType_ConsolidationRemark roomNumber:roomAssignmentModel.roomAssignment_RoomId];
        //[[UserManagerV2 sharedUserManager] setCommontUserConfig:[NSString stringWithFormat:@"%@%@", REMARK_CONSOLIDATE, roomAssignmentModel.roomAssignment_RoomId] value:remarkWS.length > 0 ? remarkWS : @""];
        [[UserManagerV2 sharedUserManager] setRemarkForCommonUserConfig:[NSString stringWithFormat:@"%@%@", REMARK_CONSOLIDATE, roomAssignmentModel.roomAssignment_RoomId] value:remarkWS.length > 0 ? remarkWS : @""];
    }
    
    listAddJobs = [NSMutableArray array];
    AdditionalJobManagerV2 *addJobManager = [AdditionalJobManagerV2 sharedAddionalJobManager];
    [addJobManager loadWSAddJobOfRoomByUserId:(int)userId hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId roomNumber:roomAssignmentModel.roomAssignment_RoomId roomAssignId:roomAssignmentModel.roomAssignment_Id];
    
    //Additional Job
    NSMutableArray *listAddJobDetails = [[AdditionalJobManagerV2 sharedAddionalJobManager] loadAddJobDetailsByUserId:(int)userId roomId:0 roomAssignId:roomAssignmentModel.roomAssignment_Id];
    bool isEnglishLang = [[LanguageManagerV2 sharedLanguageManager] currentLanguageIsEqualTo:@"%@", ENGLISH_LANGUAGE];
    for (AddJobDetailModelV2 *curDetail in listAddJobDetails) {
        NSMutableDictionary *curAddJobDetail = [[NSMutableDictionary alloc] init];
        [curAddJobDetail setObject:curDetail forKey:keyAdditionalJobDetail];
        AddJobCategoryModelV2 *category = [addJobManager loadAddJobCategoryByUserId:(int)userId categoryId:(int)curDetail.addjob_detail_category_id];
        NSString *categoryString = nil;
        if(isEnglishLang){
            categoryString = category.addjob_category_name;
        } else {
            categoryString = category.addjob_category_name_lang;
        }
        [curAddJobDetail setObject:categoryString forKey:keyAdditionalJobCategory];
        
        [listAddJobs addObject:curAddJobDetail];
    }
    
    [HUD removeFromSuperview];
}

-(void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    switch (timerStatus) {
        case TIMER_STATUS_PAUSE: //set duration = 0 if not finished yet
        {
            //Hao Tran - Remove for hold total time
            /*
            if([UserManagerV2 isSupervisor])
            {
                roomRecordModel.rrec_Total_Inspected_Time = @"0";
            }
            else
            {
                roomRecordModel.rrec_Total_Cleaning_Time = [NSString stringWithFormat:@"%d", duration ];
            }
             */
            //update local database of RoomRecord
            if(roomRecordModel.rrec_Id <= 0) {
                [[RoomManagerV2 sharedRoomManager] insertRoomRecordData:roomRecordModel];
            } else {
                [[RoomManagerV2 sharedRoomManager] updateRoomRecordData:roomRecordModel];
            }
        }
            break;
            
        default:
            break;
    }
}

-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self initViewDidAppear];
}

-(void) initViewDidAppear
{
    //Checking whether showing hud loading data or not.
    //hudWasHidden: function will handle view lifecycle
    if(!isAlreadyLoadWSData){
        return;
    }
    
    [self initializeView];
    [self populateData];
    [self.tbViewDetail reloadData];
    
    //Cached current room Assignment content size
    contentRoomAssignmentSize = scviewRoomAssignmentDetail.contentSize;
    
    if(isAutoStartOnLoad)
    {
        [self btnStartStopClicked:btnStartStop];
        isAutoStartOnLoad = NO;
    }
    
    //Fix bug - Trigger to show guest infor after view show again
    if(viewDetail.isHidden && self.tbViewDetail.isHidden){
        [self btnGuestInfo:btnGuestInfo];
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void) insertTopBarView
{
    if (topBarView == nil) {
        SuperRoomModelV2 *superModel = [[SuperRoomModelV2 alloc] init];
        topBarView = [[TopbarViewV2 alloc] initViewWithSuperModel:superModel];
        [self.view addSubview:topBarView];
        
        [topBarView setHidden:YES];
        [self hiddenHeaderView];
    } else {
        [topBarView refresh:[[SuperRoomModelV2 alloc] init]];
        [topBarView setHidden:YES];
    }
}

-(void)addButtonHandleShowHideTopbar {
    UIView *vBarButton = [[UIView alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
    UIButton *titleBarButtonFirst = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 200, 30)];
    [titleBarButtonFirst setTag:1];
    titleBarButtonFirst.titleLabel.backgroundColor = [UIColor clearColor];
    [titleBarButtonFirst.titleLabel setFont: FONT_BUTTON_TOPBAR];
    titleBarButtonFirst.titleLabel.textAlignment = NSTextAlignmentCenter;
    [titleBarButtonFirst.titleLabel setAdjustsFontSizeToFitWidth:YES];
    titleBarButtonFirst.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
    
    [titleBarButtonFirst.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
    [titleBarButtonFirst.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
    [titleBarButtonFirst addTarget:self action:@selector(hiddenHeaderView) forControlEvents:UIControlEventTouchUpInside];
    [titleBarButtonFirst setTitle:[roomAssignment objectForKey:kRoomNo] forState:UIControlStateNormal];
    
    [vBarButton addSubview:titleBarButtonFirst];
    
    UIButton *titleBarButtonSecond = [[UIButton alloc] initWithFrame:CGRectMake(0, 22, 200, 22)];
    [titleBarButtonSecond setTag:2];
    [titleBarButtonSecond setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [titleBarButtonSecond.titleLabel setFont:FONT_BUTTON_TOPBAR_SECOND];
    [titleBarButtonSecond addTarget:self action:@selector(hiddenHeaderView) forControlEvents:UIControlEventTouchUpInside];
    
    if (isPushFrom == IS_PUSHED_FROM_FIND_BY_RA) {
        [titleBarButtonSecond setTitle:[[LanguageManagerV2 sharedLanguageManager] getFind] forState:UIControlStateNormal];
    } else {
        [titleBarButtonSecond setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOM_DETAIL] forState:UIControlStateNormal];
    }
    [vBarButton addSubview:titleBarButtonSecond];
    
    [self.navigationItem setTitleView:vBarButton];
    
    //Move title to center vertical
    CGRect titleViewBounds = self.navigationItem.titleView.bounds;
    int moveLeftTitle = 0;
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        moveLeftTitle = 10;
    }
    self.navigationItem.titleView.bounds = CGRectMake(titleViewBounds.origin.x + moveLeftTitle, Y_TOPBAR_TITLE, titleViewBounds.size.width, titleViewBounds.size.height);
    
    //Back button
    self.navigationItem.leftBarButtonItem = [MyNavigationBarV2 createBackButtonWithTarget:self selector:@selector(btnBackPressed) forControlEvents:UIControlEventTouchUpInside];
}

- (void) hiddenHeaderView {      
    BOOL isHidden = topBarView.hidden;
    [topBarView setHidden:!isHidden];
    
    CGRect scviewRoomDetailFrame = scviewRoomAssignmentDetail.frame;
    CGRect viewActionFrame = viewAction.frame;
    CGRect checklistOverallFrame = viewChecklistOverall.frame;
    
    CGFloat scale;
    if (isHidden) {
        scale = 45;
    }
    else {
        scale = -45;
    }
    
    scviewRoomDetailFrame.origin.y += scale;
    scviewRoomDetailFrame.size.height -= scale;
    viewActionFrame.origin.y += scale;
    checklistOverallFrame.origin.y += scale;
    
    [scviewRoomAssignmentDetail setFrame:scviewRoomDetailFrame];
    [viewAction setFrame:viewActionFrame];
    [viewChecklistOverall setFrame:checklistOverallFrame];
}

-(void)showHUD:(MBProgressHUD*)HUD
{
    [HUD show:YES];
}

-(void)hideHUD:(MBProgressHUD*)HUD
{
    [HUD hide:YES];
    [HUD removeFromSuperview];
}

-(void) loadData
{
    if ([roomAssignment objectForKey:kRaUserId] == nil) {
        userId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    }
    else {
        userId = [(NSString *)[roomAssignment objectForKey:kRaUserId] integerValue];
    }
    
    roomAssignmentModel = [[RoomAssignmentModelV2 alloc] init];
    roomAssignmentModel.roomAssignment_Id = [[roomAssignment objectForKey:kRoomAssignmentID] intValue];
    roomAssignmentModel.roomAssignment_UserId = (int)userId;
    [[RoomManagerV2 sharedRoomManager] load:roomAssignmentModel];
    [RoomManagerV2 setCurrentRoomNo:roomAssignmentModel.roomAssignment_RoomId];

    //Load WS Room Detail
    if(!isDemoMode)
    {
        //Check is valid room status or not
        //If not valid, show alert message and force user to close room detail
        BOOL isValidRoomStatus = [self isValidRoomStatus];
        if (!isValidRoomStatus) {
            return;
        }
    }

    if([[UserManagerV2 sharedUserManager].currentUserAccessRight isEnableGuestInfo]) {
        //Removed because of changing WS for Guest
        //[[RoomManagerV2 sharedRoomManager] updateGuestInfo:[UserManagerV2 sharedUserManager].currentUser.userId WithRoomAssignID:roomAssignmentModel.roomAssignment_Id];
        if(listGuestModels.count <= 0) {
            [[RoomManagerV2 sharedRoomManager] updateGuestInfoByUserId:[UserManagerV2 sharedUserManager].currentUser.userId hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId roomNumber:roomAssignmentModel.roomAssignment_RoomId];
        }
    }
    
    roomModel = [[RoomModelV2 alloc] init];
    roomModel.room_Id = [roomAssignment objectForKey:kRoomNo];
    [[RoomManagerV2 sharedRoomManager] loadRoomModel:roomModel];
    
    //guestInfoModel = [[RoomManagerV2 sharedRoomManager] loadByRoomID:roomModel.room_Id];
    if(listGuestModels.count <= 0) {
        [self loadGuestData];
    }
    
    roomRecordModel = [[RoomRecordModelV2 alloc] init];
    roomRecordModel.rrec_room_assignment_id = [[roomAssignment objectForKey:kRoomAssignmentID] intValue];
    roomRecordModel.rrec_User_Id = (int)userId;
    [[RoomManagerV2 sharedRoomManager] loadRoomRecordDataByRoomIdAndUserId:roomRecordModel];
    
    if (roomAssignmentModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_START) {
        NSMutableArray* allRoomRecordDetail = [[RoomManagerV2 sharedRoomManager] loadAllRoomRecordDetailsByRoomAssignmentId:[[roomAssignment objectForKey:kRoomAssignmentID] intValue] andUserId:userId];
        int cleaningDuration = 0;
        if ([allRoomRecordDetail count] > 0) {
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            for (int i = 0; i < [allRoomRecordDetail count]; i++) {
                RoomRecordDetailModelV2 *detail = (RoomRecordDetailModelV2*)allRoomRecordDetail[i];
                NSDate *start = [dateFormat dateFromString:detail.recd_Start_Date];
                NSDate *stop = [dateFormat dateFromString:detail.recd_Stop_Date];
                NSTimeInterval interval = [stop timeIntervalSinceDate:start];
                cleaningDuration += interval;
                NSLog(@"clDuration: %d", cleaningDuration);
            }
        }
        roomRecordModel.rrec_Total_Cleaning_Time = cleaningDuration;
        [[RoomManagerV2 sharedRoomManager] updateRoomRecordData:roomRecordModel];
    }
    
    roomRecordDetailModel = [[RoomRecordDetailModelV2 alloc] init];
    roomRecordDetailModel.recd_Ra_Id = roomAssignmentModel.roomAssignment_Id;
    roomRecordDetailModel.recd_User_Id = userId;
    [[RoomManagerV2 sharedRoomManager] loadRoomRecordDataDetailByRoomAssignmentIdAndUserId:roomRecordDetailModel];
    ((CleaningTimeManager*)[CleaningTimeManager sharedManager]).isCleaning = NO;
    
    roomRemarkModel = [[RoomRemarkModelV2 alloc] init];
    roomRemarkModel.rm_Id = -1;
    roomRemarkModel.rm_roomassignment_roomId = roomAssignmentModel.roomAssignment_RoomId;
    roomRemarkModel.rm_RecordId = roomRecordModel.rrec_Id;
    [[RoomManagerV2 sharedRoomManager] loadRoomRemarkDataByRecordId:roomRemarkModel];
    
    roomStatusModel = [[RoomStatusModelV2 alloc] init];
    roomStatusModel.rstat_Id = (int)roomAssignmentModel.roomAssignmentRoomStatusId;
    [[RoomManagerV2 sharedRoomManager] loadRoomStatusData:roomStatusModel];
    
    //Hao Tran - Fix button startStop always show Stop Icon
    if(roomAssignmentModel.roomAssignmentRoomInspectionStatusId == ENUM_INSPECTION_STARTED){
        roomAssignmentModel.roomAssignmentRoomInspectionStatusId = ENUM_INSPECTION_PAUSE;
    }
    if(roomAssignmentModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_START){
        roomAssignmentModel.roomAssignmentRoomCleaningStatusId = ENUM_CLEANING_STATUS_PAUSE;
    }
    //Hao Tran - END Fix
    
    isSaved = YES;
    
    listAddJobs = [NSMutableArray array];
    
    AdditionalJobManagerV2 *addJobManager = [AdditionalJobManagerV2 sharedAddionalJobManager];
    //Delete old additional Job data
    if(!isDemoMode){
        [addJobManager deleteAllAddJobDataBeforeToday];
    }
//    [addJobManager loadWSAddJobOfRoomByUserId:userId hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId roomNumber:roomAssignmentModel.roomAssignment_RoomId roomAssignId:roomAssignmentModel.roomAssignment_Id];
//    
//    //Additional Job
//    NSMutableArray *listAddJobDetails = [[AdditionalJobManagerV2 sharedAddionalJobManager] loadAddJobDetailsByUserId:userId roomId:0 roomAssignId:roomAssignmentModel.roomAssignment_Id];
//    bool isEnglishLang = [[LanguageManagerV2 sharedLanguageManager] currentLanguageIsEqualTo:@"%@", ENGLISH_LANGUAGE];
//    for (AddJobDetailModelV2 *curDetail in listAddJobDetails) {
//        NSMutableDictionary *curAddJobDetail = [[NSMutableDictionary alloc] init];
//        [curAddJobDetail setObject:curDetail forKey:keyAdditionalJobDetail];
//        AddJobCategoryModelV2 *category = [addJobManager loadAddJobCategoryByUserId:userId categoryId:curDetail.addjob_detail_category_id];
//        NSString *categoryString = nil;
//        if(isEnglishLang){
//            categoryString = category.addjob_category_name;
//        } else {
//            categoryString = category.addjob_category_name_lang;
//        }
//        [curAddJobDetail setObject:categoryString forKey:keyAdditionalJobCategory];
//        
//        [listAddJobs addObject:curAddJobDetail];
//    }
    
    if (roomAssignmentModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_PAUSE || roomAssignmentModel.roomAssignmentRoomInspectionStatusId == ENUM_INSPECTION_PAUSE) {
        timerStatus = TIMER_STATUS_PAUSE;
        if ([UserManagerV2 isSupervisor]) {
//          AdamHong [20180125/Unreset timer] - Timer reseted in inspection
            //reset inspection status things if inspector come back Room Assignment
//            if(roomRecordModel.rrec_Total_Inspected_Time == 0)
//            {
//                timerStatus = TIMER_STATUS_NOT_START;
//            }
//            duration = 0; // [roomRecordModel.rrec_Inspection_Duration integerValue];

            if ([[UserManagerV2 sharedUserManager].currentUserAccessRight continueFromPausedTime]) {
                duration = roomRecordModel.rrec_Total_Inspected_Time;
            } else {
                duration = 0;
                roomRecordModel.rrec_Total_Inspected_Time = (int)duration;
            }
        }
        else {
            //HaoTran[20130529/Reset button timer] - Change requirement, timer will be reset to 0 in next time
            //duration = [roomRecordModel.rrec_Cleaning_Duration integerValue];
            
            //CRF-711: "Time Counter" for room cleaning should continue from where it stopped
            if ([[UserManagerV2 sharedUserManager].currentUserAccessRight continueFromPausedTime]) {
                duration = roomRecordModel.rrec_Total_Cleaning_Time;
            } else {
                duration = 0;
                roomRecordModel.rrec_Total_Cleaning_Time = (int)duration;
            }
            //HaoTran[20130529/Reset button timer] - END
        }
    }
    else if (roomAssignmentModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_START || roomAssignmentModel.roomAssignmentRoomInspectionStatusId == ENUM_INSPECTION_STARTED){
        isSaved = false;
        timerStatus = TIMER_STATUS_START;
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss "];
        NSDate *cleaningStartTime = nil;
        
        if ([UserManagerV2 isSupervisor]) {
            
            cleaningStartTime = [dateFormat dateFromString:roomRecordModel.rrec_Inspection_Start_Time];
        }
        else {
            cleaningStartTime = [dateFormat dateFromString:roomRecordModel.rrec_Cleaning_Start_Time];
        }
        
        NSCalendar *calender = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        NSDateComponents *components = [calender components:NSHourCalendarUnit|NSMinuteCalendarUnit|NSSecondCalendarUnit fromDate:cleaningStartTime toDate:[NSDate date] options:0];
        
        duration = components.hour*60*60 + components.minute*60 + components.second;
    }
    else if (roomAssignmentModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_PENDING_FIRST_TIME
             || roomAssignmentModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_PENDING_SECOND_TIME
             || roomAssignmentModel.roomAssignmentRoomInspectionStatusId == ENUM_INSPECTION_PENDING
             || ((roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OC || roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VC) && roomAssignmentModel.roomAssignmentRoomInspectionStatusId)) {
        timerStatus = TIMER_STATUS_NOT_START;
        duration = 0;
    }
    else {
        timerStatus = TIMER_STATUS_FINISH;
        duration = 0;
    }
    
    if ([UserManagerV2 isSupervisor]) {
        timeLeft = roomAssignmentModel.roomAssignmentRoomExpectedInspectionTime*60 - duration;
    }
    else {
        timeLeft = roomAssignmentModel.roomAssignmentRoomExpectedCleaningTime*60 - duration;
    }
//    roomStatusId = roomAssignmentModel.roomAssignmentRoomStatusId;
    roomStatusId = notSelectRoomStatusYet;
    
//    isSaved = YES;
    isStopButtonClicked = NO;
    
    [btnTimer setTitle:[self countTimeSpendForTask:timeLeft option:YES] forState:UIControlStateNormal];
    
    isAlreadyLoadWSData = YES;
}

-(void) initializeView
{
    [[HomeViewV2 shareHomeView] waitingLoadingData];
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getLoadingData]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES];
    
    //CRF-1229: Posting History online
    if (isPushFrom == IS_PUSHED_FROM_FIND_BY_ROOM || isPushFrom == IS_PUSHED_FROM_FIND_BY_RA) {
        if([[UserManagerV2 sharedUserManager].currentUserAccessRight isFindActionHistoryActive] || [[UserManagerV2 sharedUserManager].currentUserAccessRight isFindActionHistoryAllowedView]) {
            [btnPostingHistory setHidden:NO];
        } else {
            [btnPostingHistory setHidden:YES];
        }
    } else if (isPushFrom == IS_PUSHED_FROM_ROOM_ASSIGNMENT || isPushFrom == IS_PUSHED_FROM_PASSED_INSPECTION || isPushFrom == IS_PUSHED_FROM_FAILED_INSPECTION || isPushFrom == IS_PUSHED_FROM_COMPLETE) {
        if([[UserManagerV2 sharedUserManager].currentUserAccessRight isRoomDetailsActionHistoryActive] || [[UserManagerV2 sharedUserManager].currentUserAccessRight isRoomDetailsActionHistoryAllowedView]) {
            [btnPostingHistory setHidden:NO];
        } else {
            [btnPostingHistory setHidden:YES];
        }
    }
    
    CGRect frame;
    isSubmitCheckList = false;
    BOOL isCompletedInspected = ((roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OI || roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VI || roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OD || roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VD) && (roomAssignmentModel.roomAssignmentRoomInspectionStatusId == ENUM_INSPECTION_COMPLETED_FAIL || roomAssignmentModel.roomAssignmentRoomInspectionStatusId == ENUM_INSPECTION_COMPLETED_PASS));
    
    NSInteger score = -1;
    if ([UserManagerV2 isSupervisor]) {
        
        if (isCompletedInspected
            || ((roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OPU
                 || roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VPU)
                && (roomAssignmentModel.roomAssignmentRoomInspectionStatusId == ENUM_INSPECTION_PAUSE)))
        {
            if(isPushFrom == IS_PUSHED_FROM_FIND_BY_ROOM //also get checklist for find functions
               || isPushFrom == IS_PUSHED_FROM_FIND_BY_RA) {
                
                //no need get checklist data for inspection passed
//                if(!(roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OI
//                     || roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VI)) {
                    [self loadDataCheckListFormService];
//                }
            } else {
                //if already selected room status dirty we don't need load data check list while showing button reassign
                if(roomStatusId != ENUM_ROOM_STATUS_VD && roomStatusId != ENUM_ROOM_STATUS_OD){
                    [self loadDataCheckListFormService];
                }
            }
        }
        
        [self clearLocalCheckList];
        score = [self loadChecklistStatus];
        
        if (isPushFrom == IS_PUSHED_FROM_FIND_BY_RA || isPushFrom == IS_PUSHED_FROM_FIND_BY_ROOM) {
            if (roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VD 
                || roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OD) {
                if (roomAssignmentModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_DND
                    || roomAssignmentModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_PENDING_FIRST_TIME
                    || roomAssignmentModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_SERVICE_LATER
                    || roomAssignmentModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_DECLINE_SERVICE) {
                    [btnAction setHidden:YES];
                    [btnChecklist setHidden:YES];
                    [btnComplete setHidden:YES];
                    
                    [btnReassign setHidden: NO];//(isPushFrom == IS_PUSHED_FROM_FIND_BY_RA)];
                    [btnStartStop setHidden:YES];
                    [btnTimer setHidden:YES];
                    [viewChecklistOverall setHidden:YES];
                    
                    //20150504 - Compare with Android, set hidden Button Cleaning Status when use Find Function
                    //[btnCleaningStatus setHidden:NO];
                    [btnCleaningStatus setHidden:YES];
                    
//                    CGFloat nextPosistion = 0;
//                    if (isPushFrom == IS_PUSHED_FROM_FIND_BY_RA) {
//                        CGRect btnReassignFrame = btnReassign.frame;
//                        btnReassignFrame.origin.y = nextPosistion;
//                        [btnReassign setFrame:btnReassignFrame];
//                        nextPosistion = btnReassignFrame.origin.x + btnReassignFrame.size.height + spacing;
//                    }
//                    
//                    CGRect btnCleaningStatusFrame = btnCleaningStatus.frame;
//                    btnCleaningStatusFrame.origin.y = nextPosistion;
//                    [btnCleaningStatus setFrame:btnCleaningStatusFrame];
//                    
//                    CGRect viewDetailFrame = viewDetail.frame;
//                    viewDetailFrame.origin = viewButtonOrigin;
//                    viewDetailFrame.size.height = btnCleaningStatusFrame.origin.x + btnCleaningStatusFrame.size.height;
//                    [viewDetail setFrame:viewDetailFrame];
//                    
//                    frame = CGRectMake(0, 40, 320, viewDetailFrame.size.height + tbViewDetailHeight);
                }
                else {
                    [viewChecklistOverall setHidden:!isSubmitCheckList];
                    
                    [btnAction setHidden:YES];
                    [btnChecklist setHidden:!isSubmitCheckList];
                    [btnComplete setHidden:YES];
                    [btnReassign setHidden:NO];
                    [btnStartStop setHidden:YES];
                    [btnTimer setHidden:YES];
                    [btnCleaningStatus setHidden:YES];
                    
                    if (isSubmitCheckList) {
                        [lblChecklistResult setText:[NSString stringWithFormat:@"%i%%",(int)score]];
                    }
                    
//                    CGRect btnReassignFrame = btnReassign.frame;
//                    btnReassignFrame.origin.y = 0;
//                    [btnReassign setFrame:btnReassignFrame];
//                    
//                    CGFloat nextPosisiton = btnReassignFrame.origin.y + btnReassignFrame.size.height + spacing;
//                    CGRect btnChecklistFrame = btnChecklist.frame;
//                    btnChecklistFrame.origin.y = nextPosisiton;
//                    [btnChecklist setFrame:btnChecklistFrame];
//                    
//                    CGRect viewDetailFrame = viewDetail.frame;
//                    viewDetailFrame.origin = viewButtonOrigin;
//                    viewDetailFrame.size.height = btnChecklistFrame.origin.y + btnChecklistFrame.size.height;
//                    [viewDetail setFrame:viewDetailFrame];
//                    
//                    frame = CGRectMake(0, (isSubmitCheckList?viewChecklistOverallFrame.size.height:0) + 40, 320, viewDetailFrame.size.height + tbViewDetailHeight);
                }
            }
            else if (roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VC 
                     || roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OC
                     || roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OC_SO
                     || roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OOS) {
                [btnAction setHidden:NO];
                [btnChecklist setHidden:YES];
                [btnComplete setHidden:NO];
                [btnReassign setHidden:!(isPushFrom == IS_PUSHED_FROM_FIND_BY_ROOM && roomAssignmentModel.raIsMockRoom == 1 && roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OC)];

                //force show button reassign show for find by room
                if(isPushFrom == IS_PUSHED_FROM_FIND_BY_ROOM) {
                    if(roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OC
                       || roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OPU||
                       roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OI) {
                        
                        [btnReassign setHidden:NO];
                    }
                }
                [btnStartStop setHidden:NO];
                [btnTimer setHidden:NO];
                [viewChecklistOverall setHidden:!(isPushFrom == IS_PUSHED_FROM_FIND_BY_ROOM && isSubmitCheckList)];
                [btnCleaningStatus setHidden:YES];
                
            }
            else if (roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VI
                     || roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OI) {
                
                /*Hao Tran - 2015-04-03 Remove for show different score with RA
                //Show checklist for find room OI, VI
                if(roomAssignmentModel.roomAssignmentInspectedScore > 0){
                    isSubmitCheckList = YES;
                    isCheckListPass = YES;
                    score = roomAssignmentModel.roomAssignmentInspectedScore;
                } else {
                    if(!isSubmitCheckList && score <= 0)
                    {
                        //hide if no have score of checklist
                        isSubmitCheckList = NO;
                        isCheckListPass = YES;
                    }
                }*/
                
                [viewChecklistOverall setHidden:!isSubmitCheckList];
                
                [btnAction setHidden:YES];
                [btnChecklist setHidden:YES];
                [btnComplete setHidden:YES];
                [btnReassign setHidden:roomAssignmentModel.roomAssignmentRoomStatusId != ENUM_ROOM_STATUS_OI];
                [btnStartStop setHidden:YES];
                [btnTimer setHidden:YES];
                [btnCleaningStatus setHidden:YES];
                
                if (isSubmitCheckList) {
                    [lblChecklistResult setText:[NSString stringWithFormat:@"%i%%",(int)score]];
                }
            }
            else {
                [viewChecklistOverall setHidden:!isSubmitCheckList];
                [btnAction setHidden:(isCompletedInspected)];
                [btnChecklist setHidden:YES];
                [btnComplete setHidden:(isCompletedInspected)];
                [btnStartStop setHidden:(isCompletedInspected)];
                [btnTimer setHidden:(isCompletedInspected)];
                [btnCleaningStatus setHidden:YES];
                
                if (isSubmitCheckList) {
                    [lblChecklistResult setText:[NSString stringWithFormat:@"%i%%",(int)score]];
                }
                
                if (isCompletedInspected) {
                    [btnReassign setHidden:NO];
//                    CGRect btnReassignFrame = btnReassign.frame;
//                    btnReassignFrame.origin.y = nextPosition;
//                    [btnReassign setFrame:btnReassignFrame];
                    
//                    viewDetailFrame.size.height = btnReassignFrame.origin.y + btnReassignFrame.size.height;
                    
                    if (isCheckListPass) {
                        [imgChecklistIcon setImage:[UIImage imageBeforeiOS7:imgPassIcon equaliOS7:imgPassIconFlat]];
                    }
                    else {
                        [imgChecklistIcon setImage:[UIImage imageBeforeiOS7:imgFailIcon equaliOS7:imgFailIconFlat]];
                    }
                }
                else {
//                    CGRect btnStartStopFrame = btnStartStop.frame;
//                    btnStartStopFrame.origin.y = 0;
//                    [btnStartStop setFrame:btnStartStopFrame];
                    
//                    nextPosition = btnStartStopFrame.origin.y + btnStartStopFrame.size.height + spacing;
//                    CGRect btnActionFrame = btnAction.frame;
//                    btnActionFrame.origin.y = nextPosition;
//                    [btnAction setFrame:btnActionFrame];
//                    nextPosition = btnActionFrame.origin.y + btnActionFrame.size.height + spacing;
                    
                    if (isSubmitCheckList) {	
                        if (isCheckListPass) {
                            [btnReassign setHidden:YES];
                            [imgChecklistIcon setImage:[UIImage imageBeforeiOS7:imgPassIcon equaliOS7:imgPassIconFlat]];
                        }
                        else {
                            [btnReassign setHidden:NO];
//                            CGRect btnReassignFrame = btnReassign.frame;
//                            btnReassignFrame.origin.y = nextPosition;
//                            [btnReassign setFrame:btnReassignFrame];
//                            nextPosition = btnReassignFrame.origin.y + btnReassignFrame.size.height + spacing;
                            [imgChecklistIcon setImage:[UIImage imageBeforeiOS7:imgFailIcon equaliOS7:imgFailIconFlat]];
                        }
                    }
                    else {
                        [btnReassign setHidden:YES];
                    }
                    
//                    CGRect btnCompleteFrame = btnComplete.frame;
//                    btnCompleteFrame.origin.y = nextPosition;
//                    [btnComplete setFrame:btnCompleteFrame];
//                    nextPosition = btnCompleteFrame.origin.y + btnCompleteFrame.size.height + spacing;
//                    
//                    CGRect btnTimerFrame = btnTimer.frame;
//                    btnTimerFrame.origin.y = nextPosition;
//                    [btnTimer setFrame:btnTimerFrame];
//                    
//                    viewDetailFrame.size.height = btnTimerFrame.origin.y + btnTimerFrame.size.height;
                }
                
//                [viewDetail setFrame:viewDetailFrame];
                
//                frame = CGRectMake(0, (isSubmitCheckList?viewChecklistOverallFrame.size.height:0) + 40, 320, viewDetailFrame.size.height + tbViewDetailHeight);
            }
        }
        else if (isPushFrom == IS_PUSHED_FROM_FIND_BY_ROOM) {
        }else {
            [viewChecklistOverall setHidden:!isSubmitCheckList];
            [btnAction setHidden:(isCompletedInspected)];
            [btnChecklist setHidden:YES];
            [btnComplete setHidden:(isCompletedInspected)];
            [btnStartStop setHidden:(isCompletedInspected)];
            [btnTimer setHidden:(isCompletedInspected)];
            [btnCleaningStatus setHidden:YES];
            
//            CGRect viewDetailFrame = viewDetail.frame;
//            viewDetailFrame.origin = viewButtonOrigin;
            
            if (isSubmitCheckList) {
                [lblChecklistResult setText:[NSString stringWithFormat:@"%i%%",(int)score]];
                if (isCheckListPass) {
                    [imgChecklistIcon setImage:[UIImage imageBeforeiOS7:imgPassIcon equaliOS7:imgPassIconFlat]];
//                    if(roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VPU){
//                        [self setRoomStatusModel:ENUM_ROOM_STATUS_VI];
//                    }
//                    else if(roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OPU){
//                        [self setRoomStatusModel:ENUM_ROOM_STATUS_OI];
//                    }
                }
            }
            
//            CGFloat nextPosition = 0;
            if (isCompletedInspected) {
                [btnReassign setHidden:NO];
//                CGRect btnReassignFrame = btnReassign.frame;
//                btnReassignFrame.origin.y = nextPosition;
//                [btnReassign setFrame:btnReassignFrame];
//                
//                viewDetailFrame.size.height = btnReassignFrame.origin.y + btnReassignFrame.size.height;
                
                if (isCheckListPass) {
                    [imgChecklistIcon setImage:[UIImage imageBeforeiOS7:imgPassIcon equaliOS7:imgPassIconFlat]];
                }
                else {
                    [imgChecklistIcon setImage:[UIImage imageBeforeiOS7:imgFailIcon equaliOS7:imgFailIconFlat]];
                }
            }
            else {
//                CGRect btnStartStopFrame = btnStartStop.frame;
//                btnStartStopFrame.origin.y = 0;
//                [btnStartStop setFrame:btnStartStopFrame];
//                
//                nextPosition = btnStartStopFrame.origin.y + btnStartStopFrame.size.height + spacing;
//                CGRect btnActionFrame = btnAction.frame;
//                btnActionFrame.origin.y = nextPosition;
//                [btnAction setFrame:btnActionFrame];
//                nextPosition = btnActionFrame.origin.y + btnActionFrame.size.height + spacing;
                
                if (isSubmitCheckList) {	
                    if (isCheckListPass) {
                        [btnReassign setHidden:YES];
                        [imgChecklistIcon setImage:[UIImage imageBeforeiOS7:imgPassIcon equaliOS7:imgPassIconFlat]];
                    }
                    else {
                        [btnReassign setHidden:NO];
//                        CGRect btnReassignFrame = btnReassign.frame;
//                        btnReassignFrame.origin.y = nextPosition;
//                        [btnReassign setFrame:btnReassignFrame];
//                        nextPosition = btnReassignFrame.origin.y + btnReassignFrame.size.height + spacing;
                        [imgChecklistIcon setImage:[UIImage imageBeforeiOS7:imgFailIcon equaliOS7:imgFailIconFlat]];
                    }
                }
                else {
                    [btnReassign setHidden:YES];
                }
                
//                CGRect btnCompleteFrame = btnComplete.frame;
//                btnCompleteFrame.origin.y = nextPosition;
//                [btnComplete setFrame:btnCompleteFrame];
//                nextPosition = btnCompleteFrame.origin.y + btnCompleteFrame.size.height + spacing;
//                
//                CGRect btnTimerFrame = btnTimer.frame;
//                btnTimerFrame.origin.y = nextPosition;
//                [btnTimer setFrame:btnTimerFrame];
//                
//                viewDetailFrame.size.height = btnTimerFrame.origin.y + btnTimerFrame.size.height;
            }
            if (roomStatusIdSelected == ENUM_ROOM_STATUS_VD
                || roomStatusIdSelected == ENUM_ROOM_STATUS_OD) {
                [btnReassign setHidden:NO];
            }
            
            
//            [viewDetail setFrame:viewDetailFrame];
//            
//            frame = CGRectMake(0, (isSubmitCheckList?viewChecklistOverallFrame.size.height:0) + 40, 320, viewDetailFrame.size.height + tbViewDetailHeight);
        }
    }
    else {
        //Hao Tran - Remove for change requirement (for some rooms DND, DeclineService RA can start cleaning room as usual
        /*
        if (roomAssignmentModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_DND
            || roomAssignmentModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_DECLINE_SERVICE) {
            [btnAction setHidden:YES];
            [btnChecklist setHidden:YES];
            [btnComplete setHidden:YES];
            [btnReassign setHidden:YES];
            [btnStartStop setHidden:YES];
            [btnTimer setHidden:YES];
            [viewChecklistOverall setHidden:YES];
            [btnCleaningStatus setHidden:NO];
            
//            CGRect btnCleaningStatusFrame = btnCleaningStatus.frame;
//            btnCleaningStatusFrame.origin.y = 0;
//            [btnCleaningStatus setFrame:btnCleaningStatusFrame];
//            
//            CGRect viewDetailFrame = viewDetail.frame;
//            viewDetailFrame.origin.y = viewButtonOrigin.y - 2*45;
//            viewDetailFrame.size.height = mediumButtonHeight;
//            [viewDetail setFrame:viewDetailFrame];
//            
//            frame = CGRectMake(0, 40, 320, viewDetailFrame.size.height + tbViewDetailHeight - 2*45);
        }
        else { */
        if(isPushFrom == IS_PUSHED_FROM_FAILED_INSPECTION || isPushFrom == IS_PUSHED_FROM_PASSED_INSPECTION){
            [self loadDataCheckListFormService];
            [self clearLocalCheckList];
            //Load Checklist score and submitted status (if any)
            score = [self loadChecklistStatus];
        } else {

            //Get Checklist Score only for RA to review
            if(![UserManagerV2 isSupervisor]) {
                //20152901 - Fix: Incorrect checklist's status shown on checklist screen when supervisor checked.
                [self loadDataCheckListFormService];
                [self clearLocalCheckList];
                //Load Checklist score and submitted status (if any)
                score = [self loadChecklistStatus];
            }
        }
        
        if (isPushFrom == IS_PUSHED_FROM_FAILED_INSPECTION){
            [viewChecklistOverall setHidden:!isSubmitCheckList];
            [btnAction setHidden:YES];
            [btnChecklist setHidden:!isSubmitCheckList];
            [btnComplete setHidden:YES];
            [btnReassign setHidden:YES];
            [btnStartStop setHidden:YES];
            [btnTimer setHidden:YES];
            [btnCleaningStatus setHidden:YES];
            
            if (isSubmitCheckList) {
                [lblChecklistResult setText:[NSString stringWithFormat:@"%i%%",(int)score]];
            }
        } else {
            
            [btnAction setHidden:NO];
            [btnChecklist setHidden:YES];
            [btnComplete setHidden:NO];
            [btnReassign setHidden:YES];
            [btnStartStop setHidden:NO];
            [btnTimer setHidden:NO];
            [viewChecklistOverall setHidden:!isSubmitCheckList];
            [btnCleaningStatus setHidden:NO];
        }
//            CGRect btnStartStopFrame = btnStartStop.frame;
//            btnStartStopFrame.origin.y = 0;
//            [btnStartStop setFrame:btnStartStopFrame];
//            
//            CGFloat nextPosition = btnStartStopFrame.origin.y + btnStartStopFrame.size.height + spacing;
//            CGRect btnCleaningStatusFrame = btnCleaningStatus.frame;
//            btnCleaningStatusFrame.origin.y = nextPosition;
//            [btnCleaningStatus setFrame:btnCleaningStatusFrame];
//            
//            nextPosition = btnCleaningStatusFrame.origin.y + btnCleaningStatusFrame.size.height + spacing;
//            CGRect btnActionFrame = btnAction.frame;
//            btnActionFrame.origin.y = nextPosition;
//            [btnAction setFrame:btnActionFrame];
//            
//            nextPosition = btnActionFrame.origin.y + btnActionFrame.size.height + spacing;
//            CGRect btnCompleteFrame = btnComplete.frame;
//            btnCompleteFrame.origin.y = nextPosition;
//            [btnComplete setFrame:btnCompleteFrame];
//            
//            nextPosition = btnCompleteFrame.origin.y + btnCompleteFrame.size.height + spacing;
//            CGRect btnTimerFrame = btnTimer.frame;
//            btnTimerFrame.origin.y = nextPosition;
//            [btnTimer setFrame:btnTimerFrame];
//            
//            if (isPushFrom == IS_PUSHED_FROM_COMPLETE) {
//                frame = CGRectMake(0, 40, 320, tbViewDetailHeight - 2*45);
//            }
//            else {
//                CGRect viewDetailFrame = viewDetail.frame;
//                viewDetailFrame.origin = viewButtonOrigin;
//                viewDetailFrame.origin.y -= 2*45;
//                viewDetailFrame.size.height = btnTimerFrame.origin.y + btnTimerFrame.size.height;
//                [viewDetail setFrame:viewDetailFrame];
//                
//                frame = CGRectMake(0, 40, 320, viewDetailFrame.size.height + tbViewDetailHeight - 2*45);
//            }
        //}
    }
    
    if (![viewChecklistOverall isHidden]) {
        [lblChecklistResult setText:[NSString stringWithFormat:@"%i%%",(int)score]];
        if (isCheckListPass) {
            [imgChecklistIcon setImage:[UIImage imageBeforeiOS7:imgPassIcon equaliOS7:imgPassIconFlat]];
        }
        else {
            [imgChecklistIcon setImage:[UIImage imageBeforeiOS7:imgFailIcon equaliOS7:imgFailIconFlat]];
        }
    }
    
    CGFloat nextPosition = 0;
    
    //check to show button Reassign above others button
    if(isPushFrom == IS_PUSHED_FROM_FIND_BY_ROOM && (roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OC || roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OI))
    {
        if (![btnReassign isHidden]) {
            CGRect btnReassignFrame = btnReassign.frame;
            btnReassignFrame.origin.y = nextPosition;
            [btnReassign setFrame:btnReassignFrame];
            nextPosition = btnReassignFrame.origin.y + btnReassignFrame.size.height + spacing;
        }
        
        if (![btnStartStop isHidden]) {
            CGRect btnStartStopFrame = btnStartStop.frame;
            btnStartStopFrame.origin.y = nextPosition;
            [btnStartStop setFrame:btnStartStopFrame];
            nextPosition = btnStartStopFrame.origin.y + btnStartStopFrame.size.height + spacing;
        }
    }
    else
    {
        if (![btnStartStop isHidden]) {
            CGRect btnStartStopFrame = btnStartStop.frame;
            btnStartStopFrame.origin.y = nextPosition;
            [btnStartStop setFrame:btnStartStopFrame];
            nextPosition = btnStartStopFrame.origin.y + btnStartStopFrame.size.height + spacing;
        }
        
        if (![btnReassign isHidden]) {
            CGRect btnReassignFrame = btnReassign.frame;
            btnReassignFrame.origin.y = nextPosition;
            [btnReassign setFrame:btnReassignFrame];
            nextPosition = btnReassignFrame.origin.y + btnReassignFrame.size.height + spacing;
        }
    }
    
    if (![btnChecklist isHidden]) {
        CGRect btnChecklistFrame = btnChecklist.frame;
        btnChecklistFrame.origin.y = nextPosition;
        [btnChecklist setFrame:btnChecklistFrame];
        nextPosition = btnChecklistFrame.origin.y + btnChecklistFrame.size.height + spacing;
    }
    
    if (![btnCleaningStatus isHidden]) {
        CGRect btnCleaningStatusFrame = btnCleaningStatus.frame;
        btnCleaningStatusFrame.origin.y = nextPosition;
        [btnCleaningStatus setFrame:btnCleaningStatusFrame];
        nextPosition = btnCleaningStatusFrame.origin.y + btnCleaningStatusFrame.size.height + spacing;
    }
    
    if (![btnAction isHidden]) {
        CGRect btnActionFrame = btnAction.frame;
        btnActionFrame.origin.y = nextPosition;
        [btnAction setFrame:btnActionFrame];
        nextPosition = btnActionFrame.origin.y + btnActionFrame.size.height + spacing;
    }
    
    if (![btnComplete isHidden]) {
        CGRect btnCompleteFrame = btnComplete.frame;
        btnCompleteFrame.origin.y = nextPosition;
        [btnComplete setFrame:btnCompleteFrame];
        nextPosition = btnCompleteFrame.origin.y + btnCompleteFrame.size.height + spacing;
    }
    
    if (![btnPostingHistory isHidden]) {
        CGRect btnPostingHistoryFrame = btnPostingHistory.frame;
        btnPostingHistoryFrame.origin.y = nextPosition;
        [btnPostingHistory setFrame:btnPostingHistoryFrame];
        nextPosition = btnPostingHistoryFrame.origin.y + btnPostingHistoryFrame.size.height + spacing;
        [btnPostingHistory setTitle:[L_posting_history currentKeyToLanguage] forState:UIControlStateNormal];
    }
    
    if (![btnTimer isHidden]) {
        CGRect btnTimerFrame = btnTimer.frame;
        btnTimerFrame.origin.y = nextPosition;
        [btnTimer setFrame:btnTimerFrame];
        nextPosition = btnTimerFrame.origin.y + btnTimerFrame.size.height + spacing;
    }
    
    //Resize frame table content
    CGRect ftbViewDetail = self.tbViewDetail.frame;
    ftbViewDetail.size = self.tbViewDetail.contentSize;
    ftbViewDetail.size.height = [self totalHeightOfTableDetail];
    [self.tbViewDetail setFrame:ftbViewDetail];
    
    CGRect viewDetailFrame = viewDetail.frame;
    viewDetailFrame.origin = viewButtonOrigin;
    //move frame for controls view
    viewDetailFrame.origin.y = self.tbViewDetail.frame.size.height + 10;
    
    //viewDetailFrame.origin.y -= ([UserManagerV2 isSupervisor]?0: 2*45 + 20);
    //move frame for hidden room status cell
    //viewDetailFrame.origin.y += (listAddJobs.count > 0) ? listAddJobs.count * 45 : 45;
    
    viewDetailFrame.size.height = nextPosition;
    [viewDetail setFrame:viewDetailFrame];
    
    frame = CGRectMake(0, ([viewChecklistOverall isHidden]?0:viewChecklistOverallFrame.size.height) + 35, 320, viewDetailFrame.size.height + self.tbViewDetail.frame.size.height + ([viewChecklistOverall isHidden] ? viewChecklistOverallFrame.size.height : 0) + 15);
    
    [self.navigationItem setTitle:(NSString *)[roomAssignment objectForKey:kRoomNo]];
    
    if ([UserManagerV2 isSupervisor] || (isPushFrom != IS_PUSHED_FROM_COMPLETE && isPushFrom != IS_PUSHED_FROM_PASSED_INSPECTION)) {
        //View Detail will not show in complete view
        [scviewRoomAssignmentDetail addSubview:viewDetail];
    } else {
        //Decrease frame of Scroll view if view detail is not present
        frame.size.height -= viewDetailFrame.size.height;
    }
    
    CGRect scrollViewFame = scviewRoomAssignmentDetail.frame;
    scrollViewFame.size = scrollViewOriginalSize;
    if([DeviceManager getDeviceScreenKind] == DeviceScreenKindRetina4_0){
        scrollViewFame.size.height += 81; //more 81 standard pixel for 4 inch Screen
    }
    scrollViewFame.origin.y = frame.origin.y;
    
    if (isSubmitCheckList && ![viewChecklistOverall isHidden]) {
        scrollViewFame.size.height -= viewChecklistOverallFrame.size.height;
    }
    
    if(!btnReassign.isHidden){
        scrollViewFame.size.height -= btnReassign.frame.size.height;
    }
    
    if(!topBarView.isHidden)
    {
        scrollViewFame.origin.y += 45;
    }
    
    if (!(actionLostAndFound.isActive || actionEngineering.isActive || actionLaundry.isActive || actionMinibar.isActive || actionLinen.isActive || actionAmenities.isActive || checklist.isActive || guideline.isActive))
    {
        [self setEnableBtnAtion:NO];
    }
    
    [scviewRoomAssignmentDetail setFrame:scrollViewFame];
    [scviewRoomAssignmentDetail setContentSize:frame.size];
    
    [scviewRoomAssignmentDetail setHidden:NO];
    [viewAction setHidden:NO];
    
    [HUD setHidden:YES];
    [HUD removeFromSuperview];
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
}

-(void) populateData
{
    NSInteger kindOfRoom = [(NSString *)[roomAssignment objectForKey:kRaKindOfRoom] integerValue];
    // CFG [20160913/CRF-00001439] - Changed to binary AND method.
    if ((kindOfRoom & ENUM_KIND_OF_ROOM_RUSH) == ENUM_KIND_OF_ROOM_RUSH) {
        [imgRushOrQueueRoom setHidden:NO];
        [imgRushOrQueueRoom setImage:[UIImage imageBeforeiOS7:imgRushRoom equaliOS7:imgRushRoomFlat]];
    }
    else if ((kindOfRoom & ENUM_KIND_OF_ROOM_QUEUE) == ENUM_KIND_OF_ROOM_QUEUE) {
        [imgRushOrQueueRoom setHidden:NO];
        [imgRushOrQueueRoom setImage:[UIImage imageBeforeiOS7:imgQueueRoom equaliOS7:imgQueueRoomFlat]];
    }
    else {
//        if([LogFileManager isLogConsole])
//        {
//            NSLog(@"OUT OF RANGE");
//        }
        [imgRushOrQueueRoom setHidden:YES];
    }
    // CFG [20160913/CRF-00001439] - End.
    
    // CFG [20160928/CRF-00000827] - Show notes icon.
    if ((kindOfRoom & ENUM_KIND_OF_ROOM_PROFILE_NOTES_EXISTS) == ENUM_KIND_OF_ROOM_PROFILE_NOTES_EXISTS) {
        [imgProfileNotes setHidden:NO];
    }
    else {
        [imgProfileNotes setHidden:YES];
    }
    // CFG [20160928/CRF-00000827] - End.
    // CRF [20161106/CRF-00001293] - Show notes icon.
    if ((kindOfRoom & ENUM_KIND_OF_ROOM_STAY_OVER_EXISTS) == ENUM_KIND_OF_ROOM_STAY_OVER_EXISTS) {
        [imgStayOver setHidden:NO];
        if(imgProfileNotes.isHidden == NO) {
            [imgStayOver setFrame:CGRectMake(imgProfileNotes.frame.origin.x - imgProfileNotes.frame.size.width, imgProfileNotes.frame.origin.y, imgProfileNotes.frame.size.width, imgProfileNotes.frame.size.height)];
        } else {
            [imgStayOver setFrame:imgProfileNotes.frame];
        }
    }
    else {
        [imgStayOver setHidden:YES];
    }
    // CRF [20161106/CRF-00001293] - End.
    NSData *imageData = (NSData *)[roomAssignment objectForKey:kRoomStatusImage];
    UIImage *imageRoomStatus = [UIImage imageWithData:imageData];
    [imgRoomStatus setImage:imageRoomStatus];
    
    NSString *roomStatusName = (NSString *)[roomAssignment objectForKey:kRMStatus];
    [lblRoomStatus setText:roomStatusName];
    [lblRoomStatus setTextColor:[self colorForLabel:(int)roomAssignmentModel.roomAssignmentRoomStatusId]];
    
    [btnAction setBackgroundImage:[UIImage imageBeforeiOS7:imgCleaningStatusBtnAndActionBtnClicked equaliOS7:imgCleaningStatusBtnAndActionBtnClickedFlat] forState:UIControlEventTouchDown];
    [btnChecklist setBackgroundImage:[UIImage imageBeforeiOS7:imgCleaningStatusBtnAndActionBtnClicked equaliOS7:imgCleaningStatusBtnAndActionBtnClickedFlat] forState:UIControlEventTouchDown];
    [btnCleaningStatus setBackgroundImage:[UIImage imageBeforeiOS7:imgCleaningStatusBtnAndActionBtnClicked equaliOS7:imgCleaningStatusBtnAndActionBtnClickedFlat] forState:UIControlEventTouchDown];
    [btnComplete setBackgroundImage:[UIImage imageBeforeiOS7:imgCompleteBtnClicked equaliOS7:imgCompleteBtnClickedFlat] forState:UIControlEventTouchDown];
    [btnReassign setBackgroundImage:[UIImage imageBeforeiOS7:imgReassignBtnClicked equaliOS7:imgReassignBtnClickedFlat] forState:UIControlEventTouchDown];
    [btnStartStop setBackgroundImage:[UIImage imageBeforeiOS7:imgStartBtnClicked equaliOS7:imgStartBtnClickedFlat] forState:UIControlEventTouchDown];
    
    [btnAction setBackgroundImage:[UIImage imageBeforeiOS7:imgCleaningStatusBtnAndActionBtn equaliOS7:imgCleaningStatusBtnAndActionBtnFlat] forState:UIControlStateNormal];
    [btnChecklist setBackgroundImage:[UIImage imageBeforeiOS7:imgCleaningStatusBtnAndActionBtn equaliOS7:imgCleaningStatusBtnAndActionBtnFlat] forState:UIControlStateNormal];
    [btnCleaningStatus setBackgroundImage:[UIImage imageBeforeiOS7:imgCleaningStatusBtnAndActionBtn equaliOS7:imgCleaningStatusBtnAndActionBtnFlat] forState:UIControlStateNormal];
    [btnComplete setBackgroundImage:[UIImage imageBeforeiOS7:imgCompleteBtn equaliOS7:imgCompleteBtnFlat] forState:UIControlStateNormal];
    [btnReassign setBackgroundImage:[UIImage imageBeforeiOS7:imgReassignBtn equaliOS7:imgReassignBtnFlat] forState:UIControlStateNormal];
    [btnStartStop setBackgroundImage:[UIImage imageBeforeiOS7:imgStartBtn equaliOS7:imgStartBtnFlat] forState:UIControlStateNormal];
    
    if (timeLeft < 0) {
        [btnTimer setBackgroundImage:[UIImage imageBeforeiOS7:imgTimerOver equaliOS7:imgTimerOverFlat] forState:UIControlStateNormal];
    }
    else {
        if (roomAssignmentModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_PAUSE) {
            [btnTimer setBackgroundImage:[UIImage imageBeforeiOS7:imgTimerPause equaliOS7:imgTimerPauseFlat] forState:UIControlStateNormal];
        }
        else if (roomAssignmentModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_START){
            [btnTimer setBackgroundImage:[UIImage imageBeforeiOS7:imgTimerPlay equaliOS7:imgTimerPlayFlat] forState:UIControlStateNormal];
        }
        else {
            [btnTimer setBackgroundImage:[UIImage imageBeforeiOS7:imgTimerNotStart equaliOS7:imgTimerNotStartFlat] forState:UIControlStateNormal];
        }
    }
    
    //check to set text Assign for btnReassign
    if ([UserManagerV2 isSupervisor]
        && (roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VD
            || roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OD
            || roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OC)
        && roomAssignmentModel.raIsReassignedRoom == 0
        && roomAssignmentModel.roomAssignmentHousekeeperId <= 0
        && isPushFrom == IS_PUSHED_FROM_FIND_BY_ROOM
        && roomAssignmentModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_PENDING_FIRST_TIME){
        
        [btnReassign setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lbl_assign] forState:UIControlStateNormal];
        
    }
    else {
        [btnReassign setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lbl_reassign] forState:UIControlStateNormal];
    }
    
    
    if ([UserManagerV2 isSupervisor]) {
        BOOL btnReassignEnableCase = (roomAssignmentModel.raIsReassignedRoom == 0 && (roomAssignmentModel.roomAssignmentRoomInspectionStatusId == ENUM_INSPECTION_COMPLETED_FAIL || (roomAssignmentModel.roomAssignmentRoomInspectionStatusId != ENUM_INSPECTION_COMPLETED_PASS && !isCheckListPass)));
        
        [self setEnableBtnReassign:btnReassignEnableCase];
        
        if (isPushFrom == IS_PUSHED_FROM_FIND_BY_RA || isPushFrom == IS_PUSHED_FROM_FIND_BY_ROOM) {
            [self setEnableBtnChecklist:isSubmitCheckList];
            
            [self setEnableBtnCleaningStatus: (roomAssignmentModel.roomAssignmentHousekeeperId > 0)];
            if (roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VD 
                || roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OD) {
                
                if (roomAssignmentModel.roomAssignmentRoomInspectionStatusId == ENUM_INSPECTION_COMPLETED_FAIL) {
                    [self setEnableBtnChecklist:isSubmitCheckList];
                }
                else if (roomAssignmentModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_DND
                         || roomAssignmentModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_PENDING_FIRST_TIME
                         || roomAssignmentModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_SERVICE_LATER
                         || roomAssignmentModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_DECLINE_SERVICE) {
                    
                    //Hao Tran remove for PRD 2.1
                    //- Detail PRD:If Room Status is VD/OD, & cleaning status is DND/Declined service/ Service Later/ Pending, allow Supervisor to change the cleaning status (e.g. from DND to Pending)
                    //[self setEnableBtnCleaningStatus:(roomAssignmentModel.roomAssignmentRoomStatusId != ENUM_ROOM_STATUS_VD)];
                    [self setEnableBtnCleaningStatus:YES];
                    
                    //[self setEnableBtnReassign:(roomAssignmentModel.raIsReassignedRoom == 0 && isPushFrom == IS_PUSHED_FROM_FIND_BY_ROOM)];
                    
                    //always set btn Reassign enable when Find in VD, OD rooms
                    [self setEnableBtnReassign:YES];
                }
                else {
                    [self setEnableBtnReassign:(roomAssignmentModel.raIsReassignedRoom == 0 && (roomAssignmentModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_COMPLETED || (isPushFrom == IS_PUSHED_FROM_FIND_BY_ROOM && roomAssignmentModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_PENDING_FIRST_TIME)))];
                }
            }
            else if (roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VC 
                     || roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OC
                     || roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OC_SO
                     || roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OOS
                     || roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OPU
                     || roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VPU) {
                
                [self setEnableBtnReassign:(roomAssignmentModel.raIsReassignedRoom == 0 && isPushFrom == IS_PUSHED_FROM_FIND_BY_ROOM && roomAssignmentModel.raIsMockRoom == 1 && roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OC)];

   				//Force enable btnReassign for find room OC, OPU
                if(isPushFrom == IS_PUSHED_FROM_FIND_BY_ROOM) {
                    [self setEnableBtnReassign:(roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OC
                                                || roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OPU)];
                }
                
                //Hao Tran[20130521] - Fix missing button reassign when check list failed
                if(roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VPU ||roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OPU)
                {
                    if(isSubmitCheckList)
                    {
                        if(!isCheckListPass)
                        {
                            if(roomAssignmentModel.raIsReassignedRoom == 0
                            && roomAssignmentModel.roomAssignmentHousekeeperId <= 0
                            && isPushFrom == IS_PUSHED_FROM_FIND_BY_ROOM
                            && roomAssignmentModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_PENDING_FIRST_TIME)
                            {
                                [btnReassign setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lbl_assign] forState:UIControlStateNormal];
                                //20150501 - Remove for compare with Android. Shouldn't open reassign page after checklist failed
                                //[self btnReassignClicked:btnReassign];
                            }
                            else
                            {
                                [btnReassign setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lbl_reassign] forState:UIControlStateNormal];
                                //20150501 - Remove for compare with Android. Shouldn't open reassign page after checklist failed
                                //[self btnReassignClicked:btnReassign];
                            }
                            
                            [self setEnableBtnReassign:!isCheckListPass];
                        }
                    }
                }
                //Hao Tran[20130521] - END
                
                if (roomAssignmentModel.roomAssignmentRoomInspectionStatusId == ENUM_INSPECTION_PAUSE) {
                    [btnStartStop setBackgroundImage:[UIImage imageBeforeiOS7:imgStartBtn equaliOS7:imgStartBtnFlat] forState:UIControlStateNormal];
                    [btnStartStop setBackgroundImage:[UIImage imageBeforeiOS7:imgStartBtnClicked equaliOS7:imgStartBtnClickedFlat] forState:UIControlEventTouchDown];
                    [self setEnableBtnAtion:NO];
                    [self setEnableBtnComplete:NO];
                }
                else if (roomAssignmentModel.roomAssignmentRoomInspectionStatusId == ENUM_INSPECTION_STARTED){
                    [btnStartStop setBackgroundImage:[UIImage imageBeforeiOS7:imgStopBtn equaliOS7:imgStopBtnFlat] forState:UIControlStateNormal];
                    [btnStartStop setBackgroundImage:[UIImage imageBeforeiOS7:imgStopBtnClicked equaliOS7:imgStopBtnClickedFlat] forState:UIControlEventTouchDown];
                    [self setEnableBtnAtion:YES];
                    [self setEnableBtnComplete:YES];
                }
                else {
                    [btnStartStop setBackgroundImage:[UIImage imageBeforeiOS7:imgStartBtn equaliOS7:imgStartBtnFlat] forState:UIControlStateNormal];
                    [btnStartStop setBackgroundImage:[UIImage imageBeforeiOS7:imgStartBtnClicked equaliOS7:imgStartBtnClickedFlat] forState:UIControlEventTouchDown];
                    [self setEnableBtnAtion:NO];
                    [self setEnableBtnComplete:NO];
                }
            }
            else if (roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VI
                     || roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OI) {
                [self setEnableBtnReassign:(roomAssignmentModel.raIsReassignedRoom == 0 && isPushFrom == IS_PUSHED_FROM_FIND_BY_ROOM && roomAssignmentModel.raIsMockRoom == 1 && roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OI)];

				//force enable button reassign for find room
                if(isPushFrom == IS_PUSHED_FROM_FIND_BY_ROOM) {
                    [self setEnableBtnReassign:roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OI];
                }
                [self setEnableBtnChecklist:isSubmitCheckList];
            }
        }
        else if (isPushFrom == IS_PUSHED_FROM_FIND_BY_ROOM) {
            
        }
        else {
            if (roomAssignmentModel.roomAssignmentRoomInspectionStatusId == ENUM_INSPECTION_PAUSE) {
                [btnStartStop setBackgroundImage:[UIImage imageBeforeiOS7:imgStartBtn equaliOS7:imgStartBtnFlat] forState:UIControlStateNormal];
                [btnStartStop setBackgroundImage:[UIImage imageBeforeiOS7:imgStartBtnClicked equaliOS7:imgStartBtnClickedFlat] forState:UIControlEventTouchDown];
                [self setEnableBtnAtion:NO];
                [self setEnableBtnComplete:NO];
            }
            else if (roomAssignmentModel.roomAssignmentRoomInspectionStatusId == ENUM_INSPECTION_STARTED){
                [btnStartStop setBackgroundImage:[UIImage imageBeforeiOS7:imgStopBtn equaliOS7:imgStopBtnFlat] forState:UIControlStateNormal];
                [btnStartStop setBackgroundImage:[UIImage imageBeforeiOS7:imgStopBtnClicked equaliOS7:imgStopBtnClickedFlat] forState:UIControlEventTouchDown];
                [self setEnableBtnAtion:YES];
                [self setEnableBtnComplete:YES];
            }
            else {
                [btnStartStop setBackgroundImage:[UIImage imageBeforeiOS7:imgStartBtn equaliOS7:imgStartBtnFlat] forState:UIControlStateNormal];
                [btnStartStop setBackgroundImage:[UIImage imageBeforeiOS7:imgStartBtnClicked equaliOS7:imgStartBtnClickedFlat] forState:UIControlEventTouchDown];
                [self setEnableBtnAtion:NO];
                [self setEnableBtnComplete:NO];
            }
        }
    }
    else {
        if (roomAssignmentModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_PAUSE) {
            [btnStartStop setBackgroundImage:[UIImage imageBeforeiOS7:imgStartBtn equaliOS7:imgStartBtnFlat] forState:UIControlStateNormal];
            [btnStartStop setBackgroundImage:[UIImage imageBeforeiOS7:imgStartBtnClicked equaliOS7:imgStartBtnClickedFlat] forState:UIControlEventTouchDown];
            [self setEnableBtnCleaningStatus:NO];
            [self setEnableBtnAtion:NO];
            [self setEnableBtnComplete:NO];
        }
        else if (roomAssignmentModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_START){
            if([LogFileManager isLogConsole])
            {
                NSLog(@"currentPauseRoom = %d",(int)[CommonVariable sharedCommonVariable].currentPauseRoom);
            }
            //disable button start, stop if we have >= 1 pause room
            if ([CommonVariable sharedCommonVariable].currentPauseRoom != noPauseRoom
                && [CommonVariable sharedCommonVariable].currentPauseRoom != roomAssignmentModel.roomAssignment_Id)
            {
                [self setEnableBtnStartStop:NO];
            }
            else
            {
                [btnStartStop setBackgroundImage:[UIImage imageBeforeiOS7:imgStopBtn equaliOS7:imgStopBtnFlat] forState:UIControlStateNormal];
                [btnStartStop setBackgroundImage:[UIImage imageBeforeiOS7:imgStopBtnClicked equaliOS7:imgStopBtnClickedFlat] forState:UIControlEventTouchDown];
            }
            [self setEnableBtnCleaningStatus:YES];
            [self setEnableBtnAtion:YES];
            [self setEnableBtnComplete:YES];
        }
        else {
            [btnStartStop setBackgroundImage:[UIImage imageBeforeiOS7:imgStartBtn equaliOS7:imgStartBtnFlat] forState:UIControlStateNormal];
            [btnStartStop setBackgroundImage:[UIImage imageBeforeiOS7:imgStartBtnClicked equaliOS7:imgStartBtnClickedFlat] forState:UIControlEventTouchDown];
            [self setEnableBtnCleaningStatus:NO];
            [self setEnableBtnAtion:NO];
            [self setEnableBtnComplete:NO];
        }
        
        if (roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VD) {
            [self setEnableBtnCleaningStatus:NO];
        }
        else if (roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OD) {
            [self setEnableBtnCleaningStatus:YES];
        }
        else if (roomAssignmentModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_DECLINE_SERVICE
                 || roomAssignmentModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_DND) {
            [self setEnableBtnCleaningStatus:YES];
        }
    }
    
    /*
    if (roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VD
        || roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VC
        || roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VCB
        || roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VI
        || roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VPU) {
        [btnGuestInfo setUserInteractionEnabled:NO];
        [btnGuestInfo setBackgroundImage:[UIImage imageNamed:imgGuestInfoInactive] forState:UIControlStateNormal];
    }
    else if (roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OC
             || roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OD
             || roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OI
             || roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OOO
             || roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OOS
             || roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OPU
             || roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OC_SO) {
        */
        if([[UserManagerV2 sharedUserManager].currentUserAccessRight isEnableGuestInfo] && listGuestModels.count > 0){
            [btnGuestInfo setUserInteractionEnabled:YES];
            [btnGuestInfo setBackgroundImage:[UIImage imageBeforeiOS7:imgGuestInfoActive equaliOS7:imgGuestInfoRoomAssignmentActiveFlat] forState:UIControlStateNormal];
        } else {
            [btnGuestInfo setUserInteractionEnabled:NO];
            [btnGuestInfo setBackgroundImage:[UIImage imageBeforeiOS7:imgGuestInfoInactive equaliOS7:imgGuestInfoInactiveFlat] forState:UIControlStateNormal];
        }
        
        //Page Controller
        if(guestScrollView == nil) {
            guestScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, scviewRoomAssignmentDetail.frame.size.width, scviewRoomAssignmentDetail.frame.size.height)]; //Fitted parent view frame
            
            //Enable paging
            guestScrollView.pagingEnabled = YES;
            guestScrollView.showsHorizontalScrollIndicator = NO;
            guestScrollView.showsVerticalScrollIndicator = NO;
            guestScrollView.delegate = self;
            guestScrollView.directionalLockEnabled = YES;
            [guestScrollView setBounces:NO];
            [guestScrollView setHidden:YES];
        }
        
        if(listGuestModels.count > 0) {
            
            if(!_listGuestViews) {
                _listGuestViews = [NSMutableArray array];
            }
            
            if(_listGuestViews.count > 0) {
                for (UIViewController *curVC in _listGuestViews) {
                    [curVC.view removeFromSuperview];
                }
                [_listGuestViews removeAllObjects];
            }
            
            //Scroll View
            CGRect guestScrollViewFr = guestScrollView.frame;
            CGFloat maxHeightContent = guestScrollViewFr.size.height;
            
            for (int i = 0; i < listGuestModels.count; i ++) {
                GuestInfoModelV2 *curGuest = [listGuestModels objectAtIndex:i];
                
                GuestInfoViewV2 *guestInfoView = [[GuestInfoViewV2 alloc] initWithNibName:@"GuestInfoViewV2" bundle:nil];
                guestInfoView.parentVC = self;
                [guestInfoView setDataModel:curGuest];
                guestInfoView.roomNumber = roomAssignmentModel.roomAssignment_RoomId;
                [_listGuestViews addObject:guestInfoView];
                [guestScrollView addSubview:guestInfoView.view];
                
                [guestInfoView reloadView];
                
                //Set position for Guest View
                CGRect guestInfoViewFr = guestInfoView.view.frame;
                guestInfoViewFr.origin.x = guestScrollViewFr.size.width * i;
                guestInfoViewFr.origin.y = 0;
                [guestInfoView.view setFrame:guestInfoViewFr];
                
                //Find max height contain for scroll view
                if(guestInfoView.tbvGuestInfo.frame.size.height > maxHeightContent) {
                    maxHeightContent = guestInfoView.tbvGuestInfo.frame.size.height;
                }
            }
            
            //Change content size for Scrollview
            [guestScrollView setContentSize:CGSizeMake(guestScrollView.frame.size.width * listGuestModels.count, maxHeightContent)];
            [guestScrollView setFrame:CGRectMake(0, 0, guestScrollView.frame.size.width, maxHeightContent)];
            
            //Set up indicators highlight
            if(listGuestModels.count > 1) {
                guestIndicatorLeft = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 5, self.view.frame.size.height)];
                [guestIndicatorLeft setBackgroundColor:[UIColor clearColor]];
                [guestIndicatorLeft setHidden:YES];
                [guestIndicatorLeft setImage:[UIImage imageNamed:imgMenuLeft]];
                
                guestIndicatorRight = [[UIImageView alloc] initWithFrame:CGRectMake(scviewRoomAssignmentDetail.frame.size.width - 5, 0, 5, self.view.frame.size.height)];
                [guestIndicatorRight setImage:[UIImage imageNamed:imgMenuRight]];
                [guestIndicatorRight setBackgroundColor:[UIColor clearColor]];
                [guestIndicatorRight setHidden:YES];
                
                [self.view addSubview:guestIndicatorLeft];
                [self.view addSubview:guestIndicatorRight];
            }
        }
        
        //[scviewRoomAssignmentDetail addSubview:guestPageController.view];
        [scviewRoomAssignmentDetail addSubview:guestScrollView];
    //}
    
    if (!(actionLostAndFound.isActive || actionEngineering.isActive || actionLaundry.isActive || actionMinibar.isActive || actionLinen.isActive || actionAmenities.isActive || checklist.isActive || guideline.isActive))
    {
        [self setEnableBtnAtion:NO];
    }
    
    [btnTimer setTitle:[self countTimeSpendForTask:timeLeft option:YES] forState:UIControlStateNormal];
    [self runTimer];
}

-(void) changeTimerStatus:(enum TIMER_STATUS) status
{
    timerStatus = status;
    [self runTimer];
}

-(void) runTimer
{
    if (timerStatus == TIMER_STATUS_START) {
        [CommonVariable sharedCommonVariable].roomIsCompleted = 0;
        ((CleaningTimeManager*)[CleaningTimeManager sharedManager]).isCleaning = YES;
        if (timer == nil) {
            timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(countDown) userInfo:nil repeats:YES];
        }
    }
    else {
        [CommonVariable sharedCommonVariable].roomIsCompleted = 1;
        ((CleaningTimeManager*)[CleaningTimeManager sharedManager]).isCleaning = NO;
        [timer invalidate];
        timer = nil;
    }
}

#pragma mark - Table view data source

-(int)totalHeightOfTableDetail
{
    int height = 0;
    int totalNumberSection = (int)[self numberOfSectionsInTableView:self.tbViewDetail];
    
    for (int i = 0; i < totalNumberSection; i ++) {
        
        height += [self tableView:self.tbViewDetail heightForHeaderInSection:i];
        height += [self tableView:self.tbViewDetail heightForFooterInSection:i];
        int totalRowInSection = (int)[self tableView:self.tbViewDetail numberOfRowsInSection:i];
        for (int j = 0; j < totalRowInSection; j++ ) {
            height += [self tableView:self.tbViewDetail heightForRowAtIndexPath:[NSIndexPath indexPathForRow:j inSection:i]];
        }
    }
    height += 20;
    return height;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    //Section without label
    if(section == [self sectionIndexAttendant]
       || section == [self sectionIndexRoomStatus]){
        return nil;
    } else {
        
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceScreenSizeStandardWidth3_5, 45)];
        
        NSString *fontType = @"Helvetica-Bold";
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15, -12, DeviceScreenSizeStandardWidth3_5 - 30, 45)];
        [label setTextColor:[UIColor colorWithRed:6.0f/255.0f green:62.0f/255.0f blue:127.0f/255.0f alpha:1]];
        [label setFont:[UIFont fontWithName:fontType size:13]];
        [label setBackgroundColor:[UIColor clearColor]];
        
        if(section == [self sectionIndexAdditionalJob]) {
            [label setText:[L_ADDITIONAL_JOB currentKeyToLanguage]];
        } else if(section == [self sectionIndexRemark]) {
            [label setText:[L_remark_title currentKeyToLanguage]];
        } else if(section == [self sectionIndexPhysicalCheckRemark]) {
            [label setText:[L_physical_remark currentKeyToLanguage]];
        }
        [headerView addSubview:label];
        return headerView;
    }
}

-(CGFloat)tableView:(UITableView*)tableView heightForFooterInSection:(NSInteger)section
{
    return 4.0f;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    //Section without label
    if(section == [self sectionIndexAttendant] || section == [self sectionIndexRoomStatus]){
        return 0;
    } else {
        if(section == [self sectionIndexAdditionalJob]) {
            return 20;
        } else if(section == [self sectionIndexRemark]) {
            return 20;
        } else if(section == [self sectionIndexPhysicalCheckRemark]) {
            return 20;
        }
        return 0;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger sectionAttendant = ([self sectionIndexAttendant] == NSNotFound ? 0 : 1);
    NSInteger sectionAdditionalJob = ([self sectionIndexAdditionalJob] == NSNotFound ? 0 : 1);
    NSInteger sectionRemark = ([self sectionIndexRemark] == NSNotFound ? 0 : 1);
    NSInteger sectionRoomStatus = ([self sectionIndexRoomStatus] == NSNotFound ? 0 : 1);
    NSInteger sectionPhysicalCheckRemark = ([self sectionIndexPhysicalCheckRemark] == NSNotFound ? 0 : 1);
    
    return sectionAttendant + sectionAdditionalJob + sectionRemark + sectionPhysicalCheckRemark + sectionRoomStatus;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section == [self sectionIndexAttendant]){
        return 2;
    } else if(section == [self sectionIndexAdditionalJob]) {
        int countAdditionalJobs = (int)[listAddJobs count];
        if(countAdditionalJobs == 0) {
            countAdditionalJobs = 1;
        }
        
        return countAdditionalJobs;
    } else if(section == [self sectionIndexRemark]) {
        return 1;
    } else if(section == [self sectionIndexPhysicalCheckRemark]) {
        return 1;
    } else if (section == [self sectionIndexRoomStatus]) {
        return 1;
    } else {
        return 1;
    }
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.section == [self sectionIndexAttendant]){
        return 45;
    } else if(indexPath.section == [self sectionIndexAdditionalJob]) {
        return 45;
        //return [additionalJobs count];
    } else if(indexPath.section == [self sectionIndexRemark]) {
        return 60;
    } else if(indexPath.section == [self sectionIndexPhysicalCheckRemark]) {
        return 60;
    } else if (indexPath.section == [self sectionIndexRoomStatus]) {
        return 45;
    } else {
        return 45;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"DetailRoomAssingmentCell";
    static NSString *CellIdentifierAddJob = @"AddJobCellIdentify";
    static NSString *CellIdentifierRoomStatus = @"RoomStatusCellIdentify";
    static NSString *CellIdentifierRemark = @"RemarkCellIdentify";
    
    NSString *fontType = @"Helvetica-Bold";
    
    UITableViewCell *cell = nil;

    if(indexPath.section == [self sectionIndexAdditionalJob]){
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifierAddJob];
    }else if (indexPath.section == [self sectionIndexRoomStatus]){
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifierRoomStatus];
    }else if ([self sectionIndexRemark]){
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifierRemark];
    }else {
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    }
    
    if (![UserManagerV2 isSupervisor]) {
        if (indexPath.section == [self sectionIndexAttendant] && indexPath.row == [self rowIndexAttendant]) {
            NSString *raFirstName = (NSString *)[roomAssignment objectForKey:kRAFirstName];
            NSString *raLastName = (NSString *)[roomAssignment objectForKey:kRALastName];
            UILabel *lblRaName, *lblCleaningTime;
            UIImageView *imgFirstIcon, *imgSencondIcon;
            if (cell == nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
                
                lblRaName = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, 200, 45)];
                [lblRaName setFont:[UIFont fontWithName:fontType size:18]];
                //[lblRaName setMinimumFontSize:12];
                lblRaName.adjustsFontSizeToFitWidth = YES;
                [lblRaName setTag:tagFirstContent];
                [lblRaName setLineBreakMode:NSLineBreakByTruncatingTail];
                [lblRaName setAdjustsFontSizeToFitWidth:YES];
                [lblRaName setBackgroundColor:[UIColor clearColor]];
                
                lblCleaningTime = [[UILabel alloc] initWithFrame:CGRectMake(175, 0, 120, 45)];
                [lblCleaningTime setAdjustsFontSizeToFitWidth:YES];
                [lblCleaningTime setFont:[UIFont fontWithName:fontType size:18]];
                //[lblCleaningTime setMinimumFontSize:12];
                lblCleaningTime.adjustsFontSizeToFitWidth = YES;
                [lblCleaningTime setTag:tagSecondContent];
                [lblCleaningTime setTextAlignment:NSTextAlignmentRight];
                [lblCleaningTime setBackgroundColor:[UIColor clearColor]];
                [lblCleaningTime setAlpha:0.6];
                
                imgFirstIcon = [[UIImageView alloc] initWithFrame:CGRectMake(218, 4, 37, 37)];
                [imgFirstIcon setTag:tagThirthContent];
                
                imgSencondIcon = [[UIImageView alloc] initWithFrame:CGRectMake(263, 4, 37, 37)];
                [imgSencondIcon setTag:tagForthContent];
                
                [cell addSubview:lblRaName];
                [cell addSubview:lblCleaningTime];
                [cell addSubview:imgFirstIcon];
                [cell addSubview:imgSencondIcon];
            }
            else {
                lblRaName = (UILabel *)[cell viewWithTag:tagFirstContent];
                lblCleaningTime = (UILabel *)[cell viewWithTag:tagSecondContent];
                imgFirstIcon = (UIImageView *)[cell viewWithTag:tagThirthContent];
                imgSencondIcon = (UIImageView *)[cell viewWithTag:tagForthContent];
            }
            
            if(roomAssignmentModel.roomAssignmentHousekeeperId <=0)
            {
                [lblRaName setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_unassign]];
            }
            else
            {
                [lblRaName setText:[NSString stringWithFormat:@"%@ %@",raFirstName, raLastName]];
            }
            
            if (roomAssignmentModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_COMPLETED) {
                [lblCleaningTime setHidden:NO];
                [imgFirstIcon setHidden:YES];
                [imgSencondIcon setHidden:YES];
                
                [lblCleaningTime setText:[self countTimeSpendForTask:roomRecordModel.rrec_Total_Cleaning_Time option:NO]];
            }
            else {
                [lblCleaningTime setHidden:YES];
                [imgSencondIcon setHidden:NO];
                
                NSData *cleaningStatusIcon = (NSData *)[roomAssignment objectForKey:kCleaningStatus];
                if (roomAssignmentModel.raIsMockRoom == 1) {
                    [imgFirstIcon setHidden:NO];
                    [imgFirstIcon setImage:[UIImage imageBeforeiOS7:imgComplete equaliOS7:imgCompleteFlat]];
                    [imgSencondIcon setImage:[UIImage imageWithData:cleaningStatusIcon]];
                }
                else if (roomAssignmentModel.raIsReassignedRoom == 1) {
                    [imgFirstIcon setHidden:NO];
                    [imgFirstIcon setImage:[UIImage imageBeforeiOS7:imgPending equaliOS7:imgPendingFlat]];
                    [imgSencondIcon setImage:[UIImage imageBeforeiOS7:imgPending equaliOS7:imgPendingFlat]];
                }
                else if (roomAssignmentModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_SERVICE_LATER) {
                    [imgFirstIcon setHidden:YES];
                    [lblCleaningTime setHidden:NO];
                    
                    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                    [dateFormat setLocale:[NSLocale currentLocale]];
                    [dateFormat setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
                    
                    NSString *roomAssignmentTime = (NSString *)[roomAssignment objectForKey:kRoomAssignmentTime];
                    NSDate *assignedDate = [dateFormat dateFromString:roomAssignmentTime];
                    [dateFormat setDateFormat:@"HH:mm"];
                    
                    [lblCleaningTime setText:[dateFormat stringFromDate:assignedDate]];
                    [imgSencondIcon setImage:[UIImage imageWithData:cleaningStatusIcon]];
                }
                else {
                    [imgFirstIcon setHidden:YES];
                    [imgSencondIcon setImage:[UIImage imageWithData:cleaningStatusIcon]];
                }
            }
        }
        else if (indexPath.section == [self sectionIndexAttendant] && indexPath.row == [self rowIndexLastClean]) {
            UILabel *lblLastCleaningTime;
            if (cell == nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
                
                UILabel *lblLastCleaning = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, 165, 45)];
                [lblLastCleaning setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_last_cleaning_date]];
                [lblLastCleaning setFont:[UIFont fontWithName:fontType size:18]];
                //[lblLastCleaning setMinimumFontSize:12];
                lblLastCleaning.adjustsFontSizeToFitWidth = YES;
                [lblLastCleaning setTextColor:[UIColor colorWithRed:22/255.0f green:77/255.0f blue:147/255.0f alpha:1.0f]];
                [lblLastCleaning setBackgroundColor:[UIColor clearColor]];
                
                lblLastCleaningTime = [[UILabel alloc] initWithFrame:CGRectMake(180, 0, 120, 45)];
                [lblLastCleaningTime setTextAlignment:NSTextAlignmentRight];
                [lblLastCleaningTime setFont:[UIFont fontWithName:fontType size:18]];
                [lblLastCleaningTime setTag:tagFirstContent];
                [lblLastCleaningTime setAdjustsFontSizeToFitWidth:YES];
                [lblLastCleaningTime setBackgroundColor:[UIColor clearColor]];
                [lblLastCleaningTime setAlpha:0.6];
                
                [cell addSubview:lblLastCleaning];
                [cell addSubview:lblLastCleaningTime];
            }
            else {
                lblLastCleaningTime = (UILabel *)[cell viewWithTag:tagFirstContent];
            }
            
            //Hao Tran remove for show wrong cleaning end time
            //NSString *lastCleaningTime = roomRecordModel.rrec_Cleaning_Date;
            NSString *lastCleaningTime = roomRecordModel.rrec_Cleaning_End_Time;
            if (lastCleaningTime != nil) {
                NSDateFormatter *dateTimeFormat = [[NSDateFormatter alloc] init];
                [dateTimeFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                //[dateTimeFormat setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
                
                //Hao Tran remove
                //NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
                //[dateTimeFormat setLocale:usLocale];
                //NSString *dateString = [lastCleaningTime stringByReplacingOccurrencesOfString:@" " withString:@""];
                //NSDate* cleaningDate = [dateTimeFormat dateFromString:dateString];
                
                NSDate* cleaningDate = [dateTimeFormat dateFromString:lastCleaningTime];
                
                //Remove time from cleaning date
                unsigned int flags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit;
                NSCalendar* calendar = [NSCalendar currentCalendar];
                NSDateComponents* componentRemoved = [calendar components:flags fromDate:cleaningDate];
                NSDate* cleaningDateRemoveTime = [calendar dateFromComponents:componentRemoved];
                
                //Remove time from current date
                componentRemoved = [calendar components:flags fromDate:[NSDate date]];
                NSDate* currentDateRemoveTime = [calendar dateFromComponents:componentRemoved];
                
                [dateTimeFormat setDateFormat:@"dd / MM / yyyy"];
                lastCleaningTime = [dateTimeFormat stringFromDate:cleaningDate];
                
                NSCalendar *calender = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
                NSDateComponents *components = [calender components:NSDayCalendarUnit fromDate:cleaningDateRemoveTime toDate:currentDateRemoveTime options:0];
                
                if (components.day == 0) {
                    lastCleaningTime = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_today];
                }
                else if (components.day == 1) {
                    lastCleaningTime = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_yesterday];
                }
            }
            
            if (roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OD
                || roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VD
                || [lastCleaningTime caseInsensitiveCompare:@"01 / 01 / 1900"] == NSOrderedSame) {
                
                lastCleaningTime = @"N/A";
            }
            
            [lblLastCleaningTime setText:lastCleaningTime];
        }
        else if (indexPath.section == [self sectionIndexAdditionalJob]) {
            if (cell == nil) {
                NSArray *array = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([AdditionalJobDetailCell class]) owner:self options:nil];
                cell = [array objectAtIndex:0];
            }
            
            AdditionalJobDetailCell *addJobCell = (AdditionalJobDetailCell*)cell;
            //only refresh data if have add job detail
            if([listAddJobs count] > 0)
            {
                int row = (int)indexPath.row;
                NSMutableDictionary *curAddJobDetail = [listAddJobs objectAtIndex:row];
                AddJobDetailModelV2 *addJobDetail = [curAddJobDetail objectForKey:keyAdditionalJobDetail];
                NSString *categoryString = [curAddJobDetail objectForKey:keyAdditionalJobCategory];
                [addJobCell.lblAddJobTitle setText:categoryString];
                if ([[UserManagerV2 sharedUserManager].currentUserAccessRight isUseAdditionalJobStatusTable]) {
                    if (addJobDetail.addjob_detail_status == AddJobStatus_DND) {
                        [addJobCell.imgAddJobStatus setHidden:NO];
                        [addJobCell.lblTime setHidden:YES];
                        [cell setUserInteractionEnabled:YES];
                        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
                            [addJobCell.imgAddJobStatus setImage:[UIImage imageNamed:imgDNDFlat]];
                        } else {
                            [addJobCell.imgAddJobStatus setImage:[UIImage imageNamed:imgDND]];
                        }
                        
                    } else if (addJobDetail.addjob_detail_status == AddJobStatus_DoubleLock) {
                        [addJobCell.imgAddJobStatus setHidden:NO];
                        [addJobCell.lblTime setHidden:YES];
                        [cell setUserInteractionEnabled:YES];
                        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
                            [addJobCell.imgAddJobStatus setImage:[UIImage imageNamed:imgDoubleLockFlat]];
                        }
                    } else if(addJobDetail.addjob_detail_status == AddJobStatus_DeclinedService) {
                        [addJobCell.imgAddJobStatus setHidden:NO];
                        [addJobCell.lblTime setHidden:YES];
                        [cell setUserInteractionEnabled:YES];
                        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
                            [addJobCell.imgAddJobStatus setImage:[UIImage imageNamed:imgDeclinedServiceFlat]];
                        } else {
                            [addJobCell.imgAddJobStatus setImage:[UIImage imageNamed:imgDeclinedService]];
                        }
                        
                    } else if(addJobDetail.addjob_detail_status == AddJobStatus_ServiceLater) {
                        [addJobCell.imgAddJobStatus setHidden:NO];
                        [addJobCell.lblTime setHidden:NO];
                        [addJobCell.lblTime setText:[ehkConvert DateToStringWithString:addJobDetail.addjob_detail_assigned_date fromFormat:@"yyyy-MM-dd HH:mm:ss" toFormat:@"HH:mm"]];
                        [cell setUserInteractionEnabled:YES];
                        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
                            [addJobCell.imgAddJobStatus setImage:[UIImage imageNamed:imgServiceLaterFlat]];
                        } else {
                            [addJobCell.imgAddJobStatus setImage:[UIImage imageNamed:imgServiceLater]];
                        }
                        
                    } else if(addJobDetail.addjob_detail_status == AddJobStatus_Complete){
                        [addJobCell.imgAddJobStatus setHidden:YES];
                        [addJobCell.lblTime setHidden:YES];
                        [cell setUserInteractionEnabled:NO];
                        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                            [cell setAccessoryType:UITableViewCellAccessoryNone];
                        }
                        UIImageView *indicatorView = [[UIImageView alloc] initWithImage:[UIImage imageBeforeiOS7:imgYesIcon equaliOS7:imgYesIconFlat]];
                        [indicatorView setFrame:CGRectMake(0, 0, 26, 26)];
                        [cell setAccessoryView:indicatorView];
                        [cell setUserInteractionEnabled:NO];
                    } else {
                        [addJobCell.imgAddJobStatus setHidden:YES];
                        [addJobCell.lblTime setHidden:YES];
                        [cell setUserInteractionEnabled:YES];
                        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                            [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
                        } else {
                            [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
                            UIImageView *indicatorView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imgArrowGrey]];
                            [indicatorView setFrame:CGRectMake(0, 0, 26, 26)];
                            [cell setAccessoryView:indicatorView];
                        }
                    }
                } else {
                    if(addJobDetail.addjob_detail_status == 1){
                        [addJobCell.imgAddJobStatus setHidden:YES];
                        [addJobCell.lblTime setHidden:YES];
                        [cell setUserInteractionEnabled:NO];
                        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                            [cell setAccessoryType:UITableViewCellAccessoryNone];
                        }
                        UIImageView *indicatorView = [[UIImageView alloc] initWithImage:[UIImage imageBeforeiOS7:imgYesIcon equaliOS7:imgYesIconFlat]];
                        [indicatorView setFrame:CGRectMake(0, 0, 26, 26)];
                        [cell setAccessoryView:indicatorView];
                        [cell setUserInteractionEnabled:NO];
                    } else {
                        [addJobCell.imgAddJobStatus setHidden:YES];
                        [addJobCell.lblTime setHidden:YES];
                        [cell setUserInteractionEnabled:YES];
                        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                            [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
                        } else {
                            [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
                            UIImageView *indicatorView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imgArrowGrey]];
                            [indicatorView setFrame:CGRectMake(0, 0, 26, 26)];
                            [cell setAccessoryView:indicatorView];
                        }
                    }
                }
            }
            else
            {
                [addJobCell.imgAddJobStatus setHidden:YES];
                [addJobCell.lblTime setHidden:YES];
                
                //20150505 - Compare with Android and show string match with Android
                //[lblAddJobContent setText:[L_NO_RESULT_FOUND currentKeyToLanguage]/*@"No Additional Job"*/];
                //[cell setUserInteractionEnabled:NO];
                if (roomModel != nil && roomModel.room_AdditionalJob.length > 0) {
                    [addJobCell.lblAddJobTitle setText:roomModel.room_AdditionalJob];
                } else {
                    [addJobCell.lblAddJobTitle setText:@""];
                }
                
                [cell setUserInteractionEnabled:NO];
            }
            
        }
        else if (indexPath.section == [self sectionIndexRoomStatus]) {
            UIImageView *imgRoomStatusCell = nil;
            UILabel *lblRoomStatusCell = nil;
            if (cell == nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
                
                UILabel *lblRoomStt = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, 160, 45)];
                [lblRoomStt setFont:[UIFont fontWithName:fontType size:20]];
                [lblRoomStt setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_room_status]];
                [lblRoomStt setTextColor:[UIColor colorWithRed:6.0f/255.0f green:62.0f/255.0f blue:127.0f/255.0f alpha:1]];
                [lblRoomStt setBackgroundColor:[UIColor clearColor]];
                
                lblRoomStatusCell = [[UILabel alloc] initWithFrame:CGRectMake(215, 4, 60, 37)];
                [lblRoomStatusCell setFont:[UIFont fontWithName:fontType size:20]];
                [lblRoomStatusCell setAdjustsFontSizeToFitWidth:YES];
                [lblRoomStatusCell setTag:tagSecondContent];
                [lblRoomStatusCell setBackgroundColor:[UIColor clearColor]];
                
                imgRoomStatusCell = [[UIImageView alloc] initWithFrame:CGRectMake(175, 4, 37, 37)];
                [imgRoomStatusCell setTag:tagFirstContent];
                
                [cell addSubview:lblRoomStt];
                [cell addSubview:imgRoomStatusCell];
                [cell addSubview:lblRoomStatusCell];
            }
            else {
                imgRoomStatusCell = (UIImageView *)[cell viewWithTag:tagFirstContent];
                lblRoomStatusCell = (UILabel *)[cell viewWithTag:tagSecondContent];
            }
            
            //Room Status already changed by user
            if(roomStatusIdSelected > notSelectRoomStatusYet) {
                [imgRoomStatusCell setImage:roomStatusImageSelected];
                [lblRoomStatusCell setText:roomStatusTextSelected];
                [lblRoomStatusCell setTextColor:[self colorForLabel:(int)roomStatusIdSelected]];
            } else {
                UIImage *image = [UIImage imageWithData:[roomAssignment objectForKey:kRoomStatusImage]];
                if (image != nil) {
                    [imgRoomStatusCell setImage:image];
                }
                [lblRoomStatusCell setText:[roomAssignment objectForKey:kRMStatus]];
                [lblRoomStatusCell setTextColor:[self colorForLabel:(int)roomAssignmentModel.roomAssignmentRoomStatusId]];
            }
            
            if(manualUpdateRoomStatus.isActive && !manualUpdateRoomStatus.isAllowedView){
                [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
                UIImageView *indicatorView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imgArrowGrey]];
                [indicatorView setFrame:CGRectMake(0, 0, 26, 26)];
                [cell setAccessoryView:indicatorView];
            }
        }
        else if (indexPath.section == [self sectionIndexPhysicalCheckRemark]){
            UILabel *lblPhysicalCheckRemarkContent;
            NSString *remarkWS = [[RoomManagerV2 sharedRoomManager] getRoomRemarksByUserId:[UserManagerV2 sharedUserManager].currentUser.userId hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId remarkType:RemarkType_PhyshicalCheck roomNumber:roomAssignmentModel.roomAssignment_RoomId];
            if (cell == nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
                lblPhysicalCheckRemarkContent = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, tableView.bounds.size.width - 35, 60)];
                [lblPhysicalCheckRemarkContent setTag:tagPhysicalCheckRemark];
                [lblPhysicalCheckRemarkContent setFont:[UIFont fontWithName:fontType size:20]];
                [lblPhysicalCheckRemarkContent setAdjustsFontSizeToFitWidth:NO];
                [lblPhysicalCheckRemarkContent setNumberOfLines:2];
                [lblPhysicalCheckRemarkContent setLineBreakMode:NSLineBreakByTruncatingTail];
                [lblPhysicalCheckRemarkContent setBackgroundColor:[UIColor clearColor]];
                [lblPhysicalCheckRemarkContent setTextColor:[UIColor grayColor]];
                
                [cell addSubview:lblPhysicalCheckRemarkContent];
            }
            else {
                lblPhysicalCheckRemarkContent = (UILabel *)[cell viewWithTag:tagPhysicalCheckRemark];
            }
            if (remarkWS && remarkWS.length > 0) {
                [lblPhysicalCheckRemarkContent setText:remarkWS];
            }else{
                [lblPhysicalCheckRemarkContent setText:roomRemarkModel.rm_physical_content];
            }
            
        }else if ([self sectionIndexRemark]){
            UILabel *lblRemarkContent;
            if (cell == nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
                lblRemarkContent = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, tableView.bounds.size.width - 35, 60)];
                [lblRemarkContent setTag:tagFirstContent];
                [lblRemarkContent setFont:[UIFont fontWithName:fontType size:20]];
                [lblRemarkContent setAdjustsFontSizeToFitWidth:NO];
                [lblRemarkContent setNumberOfLines:2];
                [lblRemarkContent setLineBreakMode:NSLineBreakByTruncatingTail];
                [lblRemarkContent setBackgroundColor:[UIColor clearColor]];
                [lblRemarkContent setTextColor:[UIColor grayColor]];
                
                [cell addSubview:lblRemarkContent];
            }
            else {
                lblRemarkContent = (UILabel *)[cell viewWithTag:tagFirstContent];
            }
            
            if([[UserManagerV2 sharedUserManager].currentUserAccessRight isRemarkConsolidation]) {
                NSString *remarkConsolidateValue = [[UserManagerV2 sharedUserManager] getCommonUserConfigValueByKey:[NSString stringWithFormat:@"%@%@",REMARK_CONSOLIDATE,roomAssignmentModel.roomAssignment_RoomId]];
                [lblRemarkContent setText:remarkConsolidateValue];
            } else {
                [lblRemarkContent setText:roomRecordModel.rrec_Remark];
            }
        }
    }
    else {
        if (indexPath.section == [self sectionIndexAttendant] && indexPath.row == [self rowIndexAttendant]) {
            NSString *raFirstName = (NSString *)[roomAssignment objectForKey:kRAFirstName];
            NSString *raLastName = (NSString *)[roomAssignment objectForKey:kRALastName];
            UILabel *lblRaName, *lblCleaningTime;
            UIImageView *imgFirstIcon, *imgSencondIcon;
            if (cell == nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
                
                lblRaName = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, 200, 45)];
                [lblRaName setFont:[UIFont fontWithName:fontType size:18]];
                //[lblRaName setMinimumFontSize:12];
                lblRaName.adjustsFontSizeToFitWidth = YES;
                [lblRaName setTag:tagFirstContent];
                [lblRaName setLineBreakMode:NSLineBreakByTruncatingTail];
                [lblRaName setAdjustsFontSizeToFitWidth:YES];
                [lblRaName setBackgroundColor:[UIColor clearColor]];
                
                lblCleaningTime = [[UILabel alloc] initWithFrame:CGRectMake(175, 0, 120, 45)];
                [lblCleaningTime setAdjustsFontSizeToFitWidth:YES];
                [lblCleaningTime setFont:[UIFont fontWithName:fontType size:18]];
                //[lblCleaningTime setMinimumFontSize:12];
                lblCleaningTime.adjustsFontSizeToFitWidth = YES;
                [lblCleaningTime setTag:tagSecondContent];
                [lblCleaningTime setTextAlignment:NSTextAlignmentRight];
                [lblCleaningTime setBackgroundColor:[UIColor clearColor]];
                [lblCleaningTime setAlpha:0.6];
                
                imgFirstIcon = [[UIImageView alloc] initWithFrame:CGRectMake(218, 4, 37, 37)];
                [imgFirstIcon setTag:tagThirthContent];
                
                imgSencondIcon = [[UIImageView alloc] initWithFrame:CGRectMake(263, 4, 37, 37)];
                [imgSencondIcon setTag:tagForthContent];
                
                [cell addSubview:lblRaName];
                [cell addSubview:lblCleaningTime];
                [cell addSubview:imgFirstIcon];
                [cell addSubview:imgSencondIcon];
            }
            else {
                lblRaName = (UILabel *)[cell viewWithTag:tagFirstContent];
                lblCleaningTime = (UILabel *)[cell viewWithTag:tagSecondContent];
                imgFirstIcon = (UIImageView *)[cell viewWithTag:tagThirthContent];
                imgSencondIcon = (UIImageView *)[cell viewWithTag:tagForthContent];
            }
            
            if(roomAssignmentModel.roomAssignmentHousekeeperId <=0)
            {
                [lblRaName setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_unassign]];
            }
            else
            {
                [lblRaName setText:[NSString stringWithFormat:@"%@ %@",raFirstName, raLastName]];
            }
            
            if (roomAssignmentModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_COMPLETED || isPushFrom == IS_PUSHED_FROM_FIND_BY_ROOM || isPushFrom == IS_PUSHED_FROM_FIND_BY_RA) {
                [lblCleaningTime setHidden:NO];
                [imgFirstIcon setHidden:YES];
                [imgSencondIcon setHidden:YES];
                
                [lblCleaningTime setText:[self countTimeSpendForTask:roomRecordModel.rrec_Total_Cleaning_Time option:NO]];
            }
            else {
                [lblCleaningTime setHidden:YES];
                [imgSencondIcon setHidden:NO];
                
                NSData *cleaningStatusIcon = (NSData *)[roomAssignment objectForKey:kCleaningStatus];
                if (roomAssignmentModel.raIsMockRoom == 1) {
                    [imgFirstIcon setHidden:NO];
                    [imgFirstIcon setImage:[UIImage imageBeforeiOS7:imgComplete equaliOS7:imgCompleteFlat]];
                    [imgSencondIcon setImage:[UIImage imageWithData:cleaningStatusIcon]];
                }
                else if (roomAssignmentModel.raIsReassignedRoom == 1) {
                    [imgFirstIcon setHidden:NO];
                    [imgFirstIcon setImage:[UIImage imageBeforeiOS7:imgPending equaliOS7:imgPendingFlat]];
                    [imgSencondIcon setImage:[UIImage imageBeforeiOS7:imgPending equaliOS7:imgPendingFlat]];
                }
                else if (roomAssignmentModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_SERVICE_LATER) {
                    [imgFirstIcon setHidden:YES];
                    [lblCleaningTime setHidden:NO];
                    
                    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                    [dateFormat setLocale:[NSLocale currentLocale]];
                    [dateFormat setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
                    
                    NSString *roomAssignmentTime = (NSString *)[roomAssignment objectForKey:kRoomAssignmentTime];
                    NSDate *assignedDate = [dateFormat dateFromString:roomAssignmentTime];
                    [dateFormat setDateFormat:@"HH:mm"];
                    
                    [lblCleaningTime setText:[dateFormat stringFromDate:assignedDate]];
                    [imgSencondIcon setImage:[UIImage imageWithData:cleaningStatusIcon]];
                }
                else {
                    [imgFirstIcon setHidden:YES];
                    [imgSencondIcon setImage:[UIImage imageWithData:cleaningStatusIcon]];
                }
            }
        }
        else if (indexPath.section == [self sectionIndexAttendant] && indexPath.row == [self rowIndexLastClean]) {
            UILabel *lblLastCleaningTime;
            if (cell == nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
                
                UILabel *lblLastCleaning = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, 165, 45)];
                [lblLastCleaning setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_last_cleaning_date]];
                [lblLastCleaning setFont:[UIFont fontWithName:fontType size:18]];
                //[lblLastCleaning setMinimumFontSize:12];
                lblLastCleaning.adjustsFontSizeToFitWidth = YES;
                [lblLastCleaning setTextColor:[UIColor colorWithRed:22/255.0f green:77/255.0f blue:147/255.0f alpha:1.0f]];
                [lblLastCleaning setBackgroundColor:[UIColor clearColor]];
                
                lblLastCleaningTime = [[UILabel alloc] initWithFrame:CGRectMake(180, 0, 120, 45)];
                [lblLastCleaningTime setTextAlignment:NSTextAlignmentRight];
                [lblLastCleaningTime setFont:[UIFont fontWithName:fontType size:18]];
                [lblLastCleaningTime setTag:tagFirstContent];
                [lblLastCleaningTime setAdjustsFontSizeToFitWidth:YES];
                [lblLastCleaningTime setBackgroundColor:[UIColor clearColor]];
                [lblLastCleaningTime setAlpha:0.6];
                
                [cell addSubview:lblLastCleaning];
                [cell addSubview:lblLastCleaningTime];
            }
            else {
                lblLastCleaningTime = (UILabel *)[cell viewWithTag:tagFirstContent];
            }
            
            //Hao Tran remove for show wrong cleaning end time
            //NSString *lastCleaningTime = roomRecordModel.rrec_Cleaning_Date;
            NSString *lastCleaningTime = roomRecordModel.rrec_Cleaning_End_Time;
            if (lastCleaningTime != nil) {
                NSDateFormatter *dateTimeFormat = [[NSDateFormatter alloc] init];
                [dateTimeFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                //[dateTimeFormat setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
                
                //Hao Tran remove
                //NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
                //[dateTimeFormat setLocale:usLocale];
                //NSString *dateString = [lastCleaningTime stringByReplacingOccurrencesOfString:@" " withString:@""];
                //NSDate* cleaningDate = [dateTimeFormat dateFromString:dateString];
                
                NSDate* cleaningDate = [dateTimeFormat dateFromString:lastCleaningTime];
                
                //Remove time from cleaning date
                unsigned int flags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit;
                NSCalendar* calendar = [NSCalendar currentCalendar];
                NSDateComponents* componentRemoved = [calendar components:flags fromDate:cleaningDate];
                NSDate* cleaningDateRemoveTime = [calendar dateFromComponents:componentRemoved];
                
                //Remove time from current date
                componentRemoved = [calendar components:flags fromDate:[NSDate date]];
                NSDate* currentDateRemoveTime = [calendar dateFromComponents:componentRemoved];
                
                [dateTimeFormat setDateFormat:@"dd / MM / yyyy"];
                lastCleaningTime = [dateTimeFormat stringFromDate:cleaningDate];
                
                NSCalendar *calender = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
                NSDateComponents *components = [calender components:NSDayCalendarUnit fromDate:cleaningDateRemoveTime toDate:currentDateRemoveTime options:0];
                
                if (components.day == 0) {
                    lastCleaningTime = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_today];
                }
                else if (components.day == 1) {
                    lastCleaningTime = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_yesterday];
                }
            }
            
            if (roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OD
                || roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VD
                || [lastCleaningTime caseInsensitiveCompare:@"01 / 01 / 1900"] == NSOrderedSame) {
                
                lastCleaningTime = @"N/A";
            }
            
            [lblLastCleaningTime setText:lastCleaningTime];
        }
        else if (indexPath.section == [self sectionIndexAdditionalJob]) {
            if (cell == nil) {
                NSArray *array = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([AdditionalJobDetailCell class]) owner:self options:nil];
                cell = [array objectAtIndex:0];
                
//                if([listAddJobs count] <= 0) {
//                    
//                    //20150505 - Compare with Android and show string match with Android
//                    //[lblAddJobContent setText:[L_NO_RESULT_FOUND currentKeyToLanguage]/*@"No Additional Job"*/];
//                    //[cell setUserInteractionEnabled:NO];
//                    if (roomModel != nil && roomModel.room_AdditionalJob.length > 0) {
//                        [lblAddJobContent setText:roomModel.room_AdditionalJob];
//                    } else {
//                        [lblAddJobContent setText:@""];
//                    }
//                    
//
//                } else {
//                    
//                    /*
//                    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
//                    UIImageView *indicatorView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_arrow (grey).png"]];
//                    [indicatorView setFrame:CGRectMake(0, 0, 26, 26)];
//                    [cell setAccessoryView:indicatorView];*/
//                    
//                    //Supervisor is not allowed press additional job
//                    //[cell setUserInteractionEnabled:NO];
//                }
            }
            
            AdditionalJobDetailCell *addJobCell = (AdditionalJobDetailCell*)cell;
            
            //only refresh data if have add job detail
            if([listAddJobs count] > 0)
            {
                int row = (int)indexPath.row;
                NSMutableDictionary *curAddJobDetail = [listAddJobs objectAtIndex:row];
                AddJobDetailModelV2 *addJobDetail = [curAddJobDetail objectForKey:keyAdditionalJobDetail];
                NSString *categoryString = [curAddJobDetail objectForKey:keyAdditionalJobCategory];
                [addJobCell.lblAddJobTitle setText: categoryString];
                if ([[UserManagerV2 sharedUserManager].currentUserAccessRight isUseAdditionalJobStatusTable]) {
                    if (addJobDetail.addjob_detail_status == AddJobStatus_DND) {
                        [addJobCell.imgAddJobStatus setHidden:NO];
                        [addJobCell.lblTime setHidden:YES];
                        [cell setUserInteractionEnabled:YES];
                        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
                            [addJobCell.imgAddJobStatus setImage:[UIImage imageNamed:imgDNDFlat]];
                        } else {
                            [addJobCell.imgAddJobStatus setImage:[UIImage imageNamed:imgDND]];
                        }
                        
                    } else if (addJobDetail.addjob_detail_status == AddJobStatus_DoubleLock) {
                        [addJobCell.imgAddJobStatus setHidden:NO];
                        [addJobCell.lblTime setHidden:YES];
                        [cell setUserInteractionEnabled:YES];
                        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
                            [addJobCell.imgAddJobStatus setImage:[UIImage imageNamed:imgDoubleLockFlat]];
                        }
                    } else if(addJobDetail.addjob_detail_status == AddJobStatus_DeclinedService) {
                        [addJobCell.imgAddJobStatus setHidden:NO];
                        [addJobCell.lblTime setHidden:YES];
                        [cell setUserInteractionEnabled:YES];
                        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
                            [addJobCell.imgAddJobStatus setImage:[UIImage imageNamed:imgDeclinedServiceFlat]];
                        } else {
                            [addJobCell.imgAddJobStatus setImage:[UIImage imageNamed:imgDeclinedService]];
                        }
                        
                    } else if(addJobDetail.addjob_detail_status == AddJobStatus_ServiceLater) {
                        [addJobCell.imgAddJobStatus setHidden:NO];
                        [addJobCell.lblTime setHidden:NO];
                        [addJobCell.lblTime setText:[ehkConvert DateToStringWithString:addJobDetail.addjob_detail_assigned_date fromFormat:@"yyyy-MM-dd HH:mm:ss" toFormat:@"HH:mm"]];
                        [cell setUserInteractionEnabled:YES];
                        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
                            [addJobCell.imgAddJobStatus setImage:[UIImage imageNamed:imgServiceLaterFlat]];
                        } else {
                            [addJobCell.imgAddJobStatus setImage:[UIImage imageNamed:imgServiceLater]];
                        }
                        
                    } else if(addJobDetail.addjob_detail_status == AddJobStatus_Complete){
                        [addJobCell.imgAddJobStatus setHidden:YES];
                        [addJobCell.lblTime setHidden:YES];
                        [cell setUserInteractionEnabled:NO];
                        UIImageView *indicatorView = [[UIImageView alloc] initWithImage:[UIImage imageBeforeiOS7:imgYesIcon equaliOS7:imgYesIconFlat]];
                        [indicatorView setFrame:CGRectMake(0, 0, 26, 26)];
                        [cell setAccessoryView:indicatorView];
                        [cell setUserInteractionEnabled:NO];
                    }
                    else {
                        [addJobCell.imgAddJobStatus setHidden:YES];
                        [addJobCell.lblTime setHidden:YES];
                        [cell setUserInteractionEnabled:YES];
                        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                            [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
                        } else {
                            [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
                            UIImageView *indicatorView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imgArrowGrey]];
                            [indicatorView setFrame:CGRectMake(0, 0, 26, 26)];
                            [cell setAccessoryView:indicatorView];
                        }
                    }
                } else {
                    if(addJobDetail.addjob_detail_status == 1){
                        [addJobCell.imgAddJobStatus setHidden:YES];
                        [addJobCell.lblTime setHidden:YES];
                        [cell setUserInteractionEnabled:NO];
                        UIImageView *indicatorView = [[UIImageView alloc] initWithImage:[UIImage imageBeforeiOS7:imgYesIcon equaliOS7:imgYesIconFlat]];
                        [indicatorView setFrame:CGRectMake(0, 0, 26, 26)];
                        [cell setAccessoryView:indicatorView];
                        [cell setUserInteractionEnabled:NO];
                    }
                    else {
                        [addJobCell.imgAddJobStatus setHidden:YES];
                        [addJobCell.lblTime setHidden:YES];
                        [cell setUserInteractionEnabled:YES];
                        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                            [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
                        } else {
                            [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
                            UIImageView *indicatorView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imgArrowGrey]];
                            [indicatorView setFrame:CGRectMake(0, 0, 26, 26)];
                            [cell setAccessoryView:indicatorView];
                        }
                    }
                }
            }
            else
            {
                [addJobCell.imgAddJobStatus setHidden:YES];
                [addJobCell.lblTime setHidden:YES];
                //20150505 - Compare with Android and show string match with Android
                //[lblAddJobContent setText:[L_NO_RESULT_FOUND currentKeyToLanguage]/*@"No Additional Job"*/];
                //[cell setUserInteractionEnabled:NO];
                if (roomModel != nil && roomModel.room_AdditionalJob.length > 0) {
                    [addJobCell.lblAddJobTitle setText:roomModel.room_AdditionalJob];
                } else {
                    [addJobCell.lblAddJobTitle setText:@""];
                }
                [cell setUserInteractionEnabled:NO];
                
            }
            
            //Supervisor only view additional job
            //[cell setUserInteractionEnabled:NO];
        }
        else if (indexPath.section == [self sectionIndexRoomStatus]) {
            UIImageView *imgRoomStatusCell = nil;
            UILabel *lblRoomStatusCell = nil;
            if (cell == nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
                
                UILabel *lblRoomStt = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, 160, 45)];
                [lblRoomStt setFont:[UIFont fontWithName:fontType size:20]];
                [lblRoomStt setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_room_status]];
                [lblRoomStt setTextColor:[UIColor colorWithRed:6.0f/255.0f green:62.0f/255.0f blue:127.0f/255.0f alpha:1]];
                [lblRoomStt setBackgroundColor:[UIColor clearColor]];
                
                lblRoomStatusCell = [[UILabel alloc] initWithFrame:CGRectMake(215, 4, 60, 37)];
                [lblRoomStatusCell setFont:[UIFont fontWithName:fontType size:20]];
                [lblRoomStatusCell setAdjustsFontSizeToFitWidth:YES];
                [lblRoomStatusCell setTag:tagSecondContent];
                [lblRoomStatusCell setBackgroundColor:[UIColor clearColor]];
                
                imgRoomStatusCell = [[UIImageView alloc] initWithFrame:CGRectMake(175, 4, 37, 37)];
                [imgRoomStatusCell setTag:tagFirstContent];
                
                [cell addSubview:lblRoomStt];
                [cell addSubview:imgRoomStatusCell];
                [cell addSubview:lblRoomStatusCell];
                
            }
            else {
                imgRoomStatusCell = (UIImageView *)[cell viewWithTag:tagFirstContent];
                lblRoomStatusCell = (UILabel *)[cell viewWithTag:tagSecondContent];
            }
            
            //Room Status already changed by user
            if(roomStatusIdSelected > notSelectRoomStatusYet) {
                [imgRoomStatusCell setImage:roomStatusImageSelected];
                [lblRoomStatusCell setText:roomStatusTextSelected];
                [lblRoomStatusCell setTextColor:[self colorForLabel:(int)roomStatusIdSelected]];
            } else {
                UIImage *image = [UIImage imageWithData:[roomAssignment objectForKey:kRoomStatusImage]];
                [imgRoomStatusCell setImage:image];
                [lblRoomStatusCell setText:[roomAssignment objectForKey:kRMStatus]];
                [lblRoomStatusCell setTextColor:[self colorForLabel:(int)roomAssignmentModel.roomAssignmentRoomStatusId]];
            }
            
            if(manualUpdateRoomStatus.isActive && !manualUpdateRoomStatus.isAllowedView && !isAlreadyReassign){
                [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
                UIImageView *indicatorView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imgArrowGrey]];
                [indicatorView setFrame:CGRectMake(0, 0, 26, 26)];
                [cell setAccessoryView:indicatorView];
            }
        }
        else if (indexPath.section == [self sectionIndexPhysicalCheckRemark]){
            UILabel *lblPhysicalCheckRemarkContent;
            NSString *remarkWS = [[RoomManagerV2 sharedRoomManager] getRoomRemarksByUserId:[UserManagerV2 sharedUserManager].currentUser.userId hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId remarkType:RemarkType_PhyshicalCheck roomNumber:roomAssignmentModel.roomAssignment_RoomId];
            if (cell == nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
                lblPhysicalCheckRemarkContent = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, tableView.bounds.size.width - 35, 60)];
                [lblPhysicalCheckRemarkContent setTag:tagPhysicalCheckRemark];
                [lblPhysicalCheckRemarkContent setFont:[UIFont fontWithName:fontType size:20]];
                [lblPhysicalCheckRemarkContent setAdjustsFontSizeToFitWidth:NO];
                [lblPhysicalCheckRemarkContent setNumberOfLines:2];
                [lblPhysicalCheckRemarkContent setLineBreakMode:NSLineBreakByTruncatingTail];
                [lblPhysicalCheckRemarkContent setBackgroundColor:[UIColor clearColor]];
                [lblPhysicalCheckRemarkContent setTextColor:[UIColor grayColor]];
                
                [cell addSubview:lblPhysicalCheckRemarkContent];
            }
            else {
                lblPhysicalCheckRemarkContent = (UILabel *)[cell viewWithTag:tagPhysicalCheckRemark];
            }
            
            
            if (remarkWS && remarkWS.length > 0) {
                [lblPhysicalCheckRemarkContent setText:remarkWS];
            }else{
                [lblPhysicalCheckRemarkContent setText:roomRemarkModel.rm_physical_content];
            }
        }else if ([self sectionIndexRemark]){
            UILabel *lblRemarkContent;
            if (cell == nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
                
                lblRemarkContent = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, tableView.bounds.size.width - 35, 60)];
                [lblRemarkContent setTag:tagFirstContent];
                [lblRemarkContent setFont:[UIFont fontWithName:fontType size:20]];
                [lblRemarkContent setAdjustsFontSizeToFitWidth:NO];
                [lblRemarkContent setNumberOfLines:2];
                [lblRemarkContent setLineBreakMode:NSLineBreakByTruncatingTail];
                [lblRemarkContent setBackgroundColor:[UIColor clearColor]];
                [lblRemarkContent setTextColor:[UIColor grayColor]];
                [cell addSubview:lblRemarkContent];
            }
            else {
                lblRemarkContent = (UILabel *)[cell viewWithTag:tagFirstContent];
            }
            
            if([[UserManagerV2 sharedUserManager].currentUserAccessRight isRemarkConsolidation]) {
                NSString *remarkConsolidateValue = [[UserManagerV2 sharedUserManager] getCommonUserConfigValueByKey:[NSString stringWithFormat:@"%@%@",REMARK_CONSOLIDATE,roomAssignmentModel.roomAssignment_RoomId]];
                [lblRemarkContent setText:remarkConsolidateValue];
            } else {
                [lblRemarkContent setText:roomRecordModel.rrec_Remark];
            }
        }
    }
    
    [cell setAutoresizesSubviews:YES];
    [cell setContentMode:UIViewContentModeScaleToFill];
    [cell setSelectionStyle:UITableViewCellEditingStyleNone];
    return cell;
}

#pragma mark - Table view delegate
- (void)showRemarkView{
    if (remarkView == nil) {
        remarkView = [[RemarkViewV2 alloc] initWithNibName:@"RemarkViewV2" bundle:nil];
        remarkView.isClickFromFind = self.isLoadFromFind;
        remarkView.delegate = self;
    }
    fromRemarkView = 0;
    
    NSInteger row = [self rowIndexRemark];
    NSInteger section = [self sectionIndexRemark];
    
    NSIndexPath *indexpath = [NSIndexPath indexPathForRow:row inSection:section];
    UITableViewCell *cell = nil;
    
    cell = [self.tbViewDetail cellForRowAtIndexPath:indexpath];
    UILabel *label = (UILabel*)[cell viewWithTag:tagFirstContent];
    [remarkView setTextinRemark:label.text];
//    [remarkView setTextinRemark:roomRecordModel.rrec_Remark.length > 0 ? roomRecordModel.rrec_Remark : @""];
    [remarkView.txvRemark becomeFirstResponder];
    //[remarkView viewWillAppear:NO];
    remarkView.isAddSubview = YES;
    [self.tabBarController.view addSubview:remarkView.view];
//
//    [remarkView addNotify];
}
- (void)showPhysicalRemark{
    if (physicalCheckRemarkView == nil) {
        physicalCheckRemarkView = [[RemarkViewV2 alloc] initWithNibName:@"RemarkViewV2" bundle:nil];
        physicalCheckRemarkView.isClickFromFind = self.isLoadFromFind;
        physicalCheckRemarkView.delegate = self;
    }
    fromRemarkView = 1;
    
    
    NSInteger row = [self rowIndexPhysicalCheckRemark];
    NSInteger section = [self sectionIndexPhysicalCheckRemark];
    
    NSIndexPath *indexpath = [NSIndexPath indexPathForRow:row inSection:section];
    UITableViewCell *cell = nil;
    cell = [self.tbViewDetail cellForRowAtIndexPath:indexpath];
    UILabel *label = (UILabel*)[cell viewWithTag:tagPhysicalCheckRemark];
    
    [physicalCheckRemarkView setTextinRemark:label.text];
    //[physicalCheckRemarkView viewWillAppear:NO];
    //[self.tabBarController.view addSubview:physicalCheckRemarkView.view];
    [physicalCheckRemarkView.txvRemark becomeFirstResponder];
    physicalCheckRemarkView.isAddSubview = YES;
    [self.tabBarController addSubview:physicalCheckRemarkView.view];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([UserManagerV2 isSupervisor]) {
        if (indexPath.section == [self sectionIndexRoomStatus]) {
            
            if(isPushFrom == IS_PUSHED_FROM_ROOM_ASSIGNMENT)
            {
                if(!manualUpdateRoomStatus.isActive || manualUpdateRoomStatus.isAllowedView)
                {
                    return;
                }
            }
            
            if (timerStatus == TIMER_STATUS_NOT_START)
            {
                if(isPushFrom == IS_PUSHED_FROM_FIND_BY_ROOM|| isPushFrom == IS_PUSHED_FROM_FIND_BY_RA)
                {
                    if(!(roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VD
                         || roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OD))
                    {
                        return;
                    }
                }
                else
                {
                    return;
                }
            }
            if (isAlreadyReassign) {
                return;
            }
            if (roomStatusView == nil) {
                roomStatusView = [[CleaningStatusViewV2 alloc] initWithNibName:@"CleaningStatusViewV2" bundle:nil];
            }
            
            /*
            if(roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OC || roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OD || roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OOO || roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OI || roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OOS || roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OPU) {
                roomStatusView.isOccupied = YES;
            } else{
                roomStatusView.isOccupied = NO;
            }
            */
            
            //Fixed can't load room status for Supervisor & RA
            roomStatusView.isOccupied = NO;
            
            [roomStatusView setDelegate:self];
            roomStatusView.title = self.navigationItem.title;
            roomStatusView.typeStatus = ENUM_ROOM_STATUS;
            roomStatusView.isPushFrom = isPushFrom;
            
            if(roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VPU)
                roomStatusView.currentRoomStatus = ENUM_ROOM_STATUS_VC;
            else if(roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OPU)
                roomStatusView.currentRoomStatus = ENUM_ROOM_STATUS_OC;
            else
                roomStatusView.currentRoomStatus = roomAssignmentModel.roomAssignmentRoomStatusId;
            
            [self.navigationController pushViewController:roomStatusView animated:YES];
        }
        else if (indexPath.section == [self sectionIndexRemark]) {
            if (self.isLoadFromFind && [[UserManagerV2 sharedUserManager].currentUserAccessRight findRoomRemarkUpdate]) {
                [self showRemarkView];
            }else{
                if (timerStatus != TIMER_STATUS_START){
                    return;
                }
                [self showRemarkView];
            }
            
        }
        else if (indexPath.section == [self sectionIndexPhysicalCheckRemark]) {
            if (self.isLoadFromFind && [[UserManagerV2 sharedUserManager].currentUserAccessRight findRoomRemarkUpdate]) {
                [self showPhysicalRemark];
            }else{
                if (timerStatus != TIMER_STATUS_START){
                    return;
                }
                [self showPhysicalRemark];
            }
        }
        else if(indexPath.section == [self sectionIndexAdditionalJob])
        {
            //Checking Access Right - Additional job is only allow view
            if([[UserManagerV2 sharedUserManager].currentUserAccessRight isAddJobAllowedView]
               || ![[UserManagerV2 sharedUserManager].currentUserAccessRight isAddJobActive]) {
                
                [[[[iToast makeText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_access_right_access_denied]] setGravity:iToastGravityBottom] setDuration:iToastDurationNormal] show];
                return;
            }
            
            int rowSelected = (int)indexPath.row;
            NSMutableDictionary *curAddJobDetail = [listAddJobs objectAtIndex:rowSelected];
            AddJobDetailModelV2 *addJobDetail = [curAddJobDetail objectForKey:keyAdditionalJobDetail];
            AdditionalJobDetailsScreenV2 *addJobDetailView = [[AdditionalJobDetailsScreenV2 alloc] initWithNibName:@"AdditionalJobDetailsScreenV2" bundle:nil];
            
            //Create AddJobRoomModel for display
            AddJobRoomModelV2 *roomSelected = [[AddJobRoomModelV2 alloc] init];
            roomSelected.addjob_room_assign_id = roomAssignmentModel.roomAssignment_Id;
            if(isDemoMode){
                RoomModelV2 *rModel = [[RoomModelV2 alloc] init];
                rModel.room_Id = roomAssignmentModel.roomAssignment_RoomId;
                [[RoomManagerV2 sharedRoomManager] loadRoomModel:rModel];
                roomSelected.addjob_room_floor_id = rModel.room_floor_id;
            }
            else{
                roomSelected.addjob_room_floor_id = 0;
            }
            
            roomSelected.addjob_room_id = 0;
            roomSelected.addjob_room_is_due_out = 0;
            roomSelected.addjob_room_number = roomAssignmentModel.roomAssignment_RoomId;
            roomSelected.addjob_room_type_id = roomModel.room_TypeId;
            roomSelected.addjob_room_status_id = roomAssignmentModel.roomAssignmentRoomStatusId;
            roomSelected.addjob_room_user_id = userId;
            addJobDetailView.roomSelected = roomSelected;
            
            addJobDetailView.detailSelected = addJobDetail;
            addJobDetailView.isPushFrom = IS_PUSHED_FROM_ROOM_ASSIGNMENT;
            [self.navigationController pushViewController:addJobDetailView animated:YES];
        }
    }
    else {
        
        if (timerStatus != TIMER_STATUS_START)
            return;
        
        if (indexPath.section == [self sectionIndexRoomStatus]) {
            
            if(!manualUpdateRoomStatus.isActive || manualUpdateRoomStatus.isAllowedView)
            {
                return;
            }
            
            if (roomStatusView == nil) {
                roomStatusView = [[CleaningStatusViewV2 alloc] initWithNibName:@"CleaningStatusViewV2" bundle:nil];
            }
            
            /*
            if(roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OC || roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OD || roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OOO || roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OI || roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OOS || roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OPU) {        
                roomStatusView.isOccupied = YES;
            } else{
                roomStatusView.isOccupied = NO;
            }*/
            
            //Fixed can't load room status for Supervisor & RA
            roomStatusView.isOccupied = NO;
            
            [roomStatusView setDelegate:self];
            roomStatusView.title = self.navigationItem.title;
            roomStatusView.typeStatus = ENUM_ROOM_STATUS;
            roomStatusView.currentRoomStatus = roomAssignmentModel.roomAssignmentRoomStatusId;
            
            [self.navigationController pushViewController:roomStatusView animated:YES];
        }
        else if (indexPath.section == [self sectionIndexRemark]){
            if (remarkView == nil) {
                remarkView = [[RemarkViewV2 alloc] initWithNibName:@"RemarkViewV2" bundle:nil];
                remarkView.delegate = self;
            }
            fromRemarkView = 0;
            
            if([[UserManagerV2 sharedUserManager].currentUserAccessRight isRemarkConsolidation]) {
                NSString *consolidateRemark = [[UserManagerV2 sharedUserManager] getCommonUserConfigValueByKey:[NSString stringWithFormat:@"%@%@",REMARK_CONSOLIDATE,roomAssignmentModel.roomAssignment_RoomId]];
                [remarkView setTextinRemark:consolidateRemark.length > 0 ? consolidateRemark : @""];
            } else {
                [remarkView setTextinRemark:roomRecordModel.rrec_Remark != nil ? roomRecordModel.rrec_Remark : @""];
            }
            //[remarkView viewWillAppear:NO];
            //[self.tabBarController.view addSubview:remarkView.view];
            remarkView.isAddSubview = YES;
            [self.tabBarController addSubview:remarkView.view];
        }
        else if (indexPath.section == [self sectionIndexPhysicalCheckRemark]) {
            if (timerStatus != TIMER_STATUS_START)
                return;
            
            if (physicalCheckRemarkView == nil) {
                physicalCheckRemarkView = [[RemarkViewV2 alloc] initWithNibName:@"RemarkViewV2" bundle:nil];
                physicalCheckRemarkView.delegate = self;
            }
            fromRemarkView = 1;
            
            NSString *remarkWS = [[RoomManagerV2 sharedRoomManager] getRoomRemarksByUserId:[UserManagerV2 sharedUserManager].currentUser.userId hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId remarkType:RemarkType_PhyshicalCheck roomNumber:roomAssignmentModel.roomAssignment_RoomId];
            [physicalCheckRemarkView setTextinRemark:remarkWS != nil ? remarkWS : @""];
            //[physicalCheckRemarkView viewWillAppear:NO];
            //[self.tabBarController.view addSubview:physicalCheckRemarkView.view];
            physicalCheckRemarkView.isAddSubview = YES;
            [self.tabBarController addSubview:physicalCheckRemarkView.view];
        }
        else if(indexPath.section == [self sectionIndexAdditionalJob])
        {
            //Checking Access Right - Additional job is only allow view
            if([[UserManagerV2 sharedUserManager].currentUserAccessRight isAddJobAllowedView]
               || [[UserManagerV2 sharedUserManager].currentUserAccessRight isAddJobActive]) {
                int rowSelected = (int)indexPath.row;
                NSMutableDictionary *curAddJobDetail = [listAddJobs objectAtIndex:rowSelected];
                AddJobDetailModelV2 *addJobDetail = [curAddJobDetail objectForKey:keyAdditionalJobDetail];
                AdditionalJobDetailsScreenV2 *addJobDetailView = [[AdditionalJobDetailsScreenV2 alloc] initWithNibName:@"AdditionalJobDetailsScreenV2" bundle:nil];

                //Create AddJobRoomModel for display
                AddJobRoomModelV2 *roomSelected = [[AddJobRoomModelV2 alloc] init];
                roomSelected.addjob_room_assign_id = roomAssignmentModel.roomAssignment_Id;
                if(isDemoMode){
                    RoomModelV2 *rModel = [[RoomModelV2 alloc] init];
                    rModel.room_Id = roomAssignmentModel.roomAssignment_RoomId;
                    [[RoomManagerV2 sharedRoomManager] loadRoomModel:rModel];
                    roomSelected.addjob_room_floor_id = rModel.room_floor_id;
                }
                else{
                    roomSelected.addjob_room_floor_id = 0;
                }

                roomSelected.addjob_room_id = 0;
                roomSelected.addjob_room_is_due_out = 0;
                roomSelected.addjob_room_number = roomAssignmentModel.roomAssignment_RoomId;
                roomSelected.addjob_room_type_id = roomModel.room_TypeId;
                roomSelected.addjob_room_user_id = userId;
                roomSelected.addjob_room_status_id = roomAssignmentModel.roomAssignmentRoomStatusId;
                addJobDetailView.roomSelected = roomSelected;

                addJobDetailView.detailSelected = addJobDetail;
                addJobDetailView.isPushFrom = IS_PUSHED_FROM_ROOM_ASSIGNMENT;
                [self.navigationController pushViewController:addJobDetailView animated:YES];

            }else{
                [[[[iToast makeText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_access_right_access_denied]] setGravity:iToastGravityBottom] setDuration:iToastDurationNormal] show];
            }
        }
    }
}
#pragma mark - Index Table Rows

-(NSInteger) sectionIndexAttendant
{
    if([UserManagerV2 isSupervisor] || isPushFrom == IS_PUSHED_FROM_PASSED_INSPECTION) {
        return 0;
    }
    return NSNotFound;
}

-(NSInteger) sectionIndexAdditionalJob
{
    if([UserManagerV2 isSupervisor] || isPushFrom == IS_PUSHED_FROM_PASSED_INSPECTION){
        return 1;
    } else {
        return 0;
    }
}

-(NSInteger) sectionIndexRemark
{
    if([UserManagerV2 isSupervisor] || isPushFrom == IS_PUSHED_FROM_PASSED_INSPECTION){
        return 2;
    } else {
        return 1;
    }
}

-(NSInteger) sectionIndexPhysicalCheckRemark
{
    if([[UserManagerV2 sharedUserManager].currentUserAccessRight isRemarkConsolidation]) {
        return NSNotFound;
    }
    
    if([UserManagerV2 isSupervisor] || isPushFrom == IS_PUSHED_FROM_PASSED_INSPECTION) {
        return 3;
    } else {
        return 2;
    }
}

-(NSInteger) sectionIndexRoomStatus
{
    int countSectionPhysicalCheckRemark = 1;
    if([[UserManagerV2 sharedUserManager].currentUserAccessRight isRemarkConsolidation]) {
        //Use only 1 room remark
        countSectionPhysicalCheckRemark = 0;
    }
    
    if([UserManagerV2 isSupervisor] || isPushFrom == IS_PUSHED_FROM_PASSED_INSPECTION){
        return 3 + countSectionPhysicalCheckRemark;
    } else {
        return 2 + countSectionPhysicalCheckRemark;
    }
}

//return index of attendant row for supervisor
-(NSInteger) rowIndexAttendant
{
    return 0;
}

//return index of last clean row for supervisor
-(NSInteger) rowIndexLastClean
{
    return 1;
}

-(NSInteger) rowIndexAdditionJob
{
    return 1;
}

-(NSInteger) rowIndexRoomStatus
{
    return 0;
}

-(NSInteger) rowIndexRemark
{
    return 0;
}
-(NSInteger) rowIndexPhysicalCheckRemark
{
    return 0;
}

#pragma mark - Button clicked
-(void) btnBackPressed
{
    if (isFromGuestInfo) {
        isFromGuestInfo = NO;
        [self showRoomAssignmentDetail];
        return;
    }
    
    if (isSaved) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else {
        if(isPushFrom == IS_PUSHED_FROM_FIND_BY_ROOM || isPushFrom == IS_PUSHED_FROM_FIND_BY_RA){
            if([btnStartStop isHidden]) {
                [self.navigationController popViewControllerAnimated:YES];
                return;
            }
        }
        
        NSString *message = [L_complete_room_to_leave currentKeyToLanguage]; //@"Please complete the room before leaving";
        if ([CommonVariable sharedCommonVariable].currentPauseRoom == noPauseRoom || [CommonVariable sharedCommonVariable].currentPauseRoom == roomAssignmentModel.roomAssignment_Id) {
            message = [L_complete_room_to_leave currentKeyToLanguage];//@"Please complete the room before leaving";
        }
        
        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:message delegate:nil cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_OK] otherButtonTitles:nil];
        [alert show];
    }
}

-(IBAction)btnActionClicked:(id)sender
{
    NSMutableArray *buttons = [NSMutableArray array];
    if (actionLostAndFound.isActive || actionEngineering.isActive || actionLaundry.isActive
        || actionMinibar.isActive || actionLinen.isActive || actionAmenities.isActive) {
        [buttons addObject:[L_COUNTS currentKeyToLanguage]/*@"Posting"*/];
    }
    
    if(checklist.isActive){
        [buttons addObject:[L_CHECKLIST currentKeyToLanguage]/*@"Checklist"*/];
    }
    
    if (guideline.isActive) {
        [buttons addObject:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_Guideline]];
    }
    actionPopup = [[ActionPopupView alloc] initWithTitles:[NSArray arrayWithArray:buttons] delegate:(id<ActionPopupDelegate>)self option:YES];
    
    //[self.tabBarController.view addSubview:actionPopup];
    [self.tabBarController addSubview:actionPopup];
}

-(IBAction) btnCleaningStatusClicked:(id)sender
{
    if (cleaningStatusPopup == nil) {
        cleaningStatusPopup = [[CleaningStatusPopupView alloc] initWithDelegate:self andOption:(isPushFrom == IS_PUSHED_FROM_FIND_BY_RA || isPushFrom == IS_PUSHED_FROM_FIND_BY_ROOM)];
    }
    [cleaningStatusPopup setRoomNo:(NSString *)[roomAssignment objectForKey:kRoomNo]];
    //[self.tabBarController.view addSubview:cleaningStatusPopup];
    [self.tabBarController addSubview:cleaningStatusPopup];
}

-(IBAction)btnCompleteClicked:(id)sender
{
    if ([UserManagerV2 isSupervisor]) {
        if (roomStatusIdSelected > notSelectRoomStatusYet /*&& !isSubmitCheckList*/) {
            
            UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title: @"" message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_room_confirmchangeroomstatus] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles:[[LanguageManagerV2 sharedLanguageManager] getNo], nil];
            alert.tag = tagCompleteRoomByRoomStatus;
            alert.delegate = self;
            [alert show];
            return;
            
        } else if(!isSubmitCheckList) {
            UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_room_detail_input_checklist_room_status] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getOK] otherButtonTitles:nil];
            [alert show];
            return;
            
        }  else {
            UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_are_you_done_with_the_room_cleaning] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles:[[LanguageManagerV2 sharedLanguageManager] getNo], nil];
            alert.delegate = self;
            [alert setTag:tagConfirmDone];
            [alert show];
            return;
        }
    }
    else {
        if(countReminderPopup == nil){
            countReminderPopup = [[CountPopupReminderViewV2 alloc] initWithRoomId:roomAssignmentModel.roomAssignment_Id];
        }
        countReminderPopup.isAlreadyPostedLinen = self.isAlreadyPostedLinen;
        countReminderPopup.isAlreadyPostedMiniBar = self.isAlreadyPostedMiniBar;
        countReminderPopup.isAlreadyPostedAmenities = self.isAlreadyPostedAmenities;
        [countReminderPopup setDelegate:self];
        if(![countReminderPopup show]){
            NSString *msgbtnNo = [[LanguageManagerV2 sharedLanguageManager] getNo];
            NSString *msgbtnYes = [[LanguageManagerV2 sharedLanguageManager] getYes];
            
            UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_are_you_done_with_the_room_cleaning] delegate:self cancelButtonTitle:msgbtnYes otherButtonTitles:msgbtnNo, nil]; 
            alert.delegate = self;
            [alert setTag:tagConfirmDone];
            [alert show];
        }
    }
}

-(IBAction)btnStartStopClicked:(id)sender
{
    if ([UserManagerV2 isSupervisor]) {
        [self startAndStopInspectionRoomForSupervisor:nil];
    }
    else {
        MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
        [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getLoadingData]];
        [self.tabBarController.view addSubview:HUD];
        [HUD show:YES];
        
        
        [self performSelector:@selector(startAndStopCleaningRoomForRA:) withObject:HUD afterDelay:0.05f];
        
        //[self startAndStopCleaningRoomForRA];
    }

}
-(int)postCurrentInpsection
{
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_RA_SAVING_DATA]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES];
    RoomAssignmentModelV2 *ramodel = [[RoomAssignmentModelV2 alloc] init];
    ramodel.roomAssignment_Id = roomAssignmentModel.roomAssignment_Id;
    ramodel.roomAssignment_UserId = [UserManagerV2 sharedUserManager].currentUser.userId;
    [[RoomManagerV2 sharedRoomManager] load:ramodel];
    
    SyncManagerV2 *synManager = [[SyncManagerV2 alloc] init];
    
    roomModel.room_PostStatus = (int)POST_STATUS_SAVED_UNPOSTED;
    [[RoomManagerV2 sharedRoomManager] updateRoomModel:roomModel];
    if(ramodel.roomAssignmentChecklistRemark) {
    roomAssignmentModel.roomAssignmentChecklistRemark = ramodel.roomAssignmentChecklistRemark;
    } else {
        roomAssignmentModel.roomAssignmentChecklistRemark = @"";
    }
    roomAssignmentModel.roomAssignment_PostStatus = (int)POST_STATUS_SAVED_UNPOSTED;
    [[RoomManagerV2 sharedRoomManager] update:roomAssignmentModel];
    
    //----------------------Post WSLog-------
    int switchStatus = (int)[[NSUserDefaults standardUserDefaults] integerForKey:ENABLE_LOGWS];
    if (switchStatus == 1) {
        NSInteger UserID = [[[UserManagerV2 sharedUserManager] currentUser] userId];
        NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSString *messageValue = [NSString stringWithFormat:@"Room Assignment Post WSLog:Time:%@, userID:%d, RoomPostStatus:%d, RoomStatus:%d", time,(int) UserID, roomAssignmentModel.roomAssignment_PostStatus,(int)roomAssignmentModel.roomAssignmentRoomStatusId];
        [synManager postWSLogRoomAsgnMent:UserID Message:messageValue MessageType:1];
    }
    
    int responseStatus = [synManager postRoomInspectionWithUserID:[[UserManagerV2 sharedUserManager] currentUser].userId AndRaModel:roomAssignmentModel];
    
    [HUD hide:YES];
    [HUD removeFromSuperview];
    
    return responseStatus;
}



-(IBAction) btnRoomAssignmentInfo:(id)sender
{
    [self showRoomAssignmentDetail];
    isFromGuestInfo = NO;
}

-(IBAction) btnGuestInfo:(id)sender
{
    [viewDetail setHidden:YES];
    [self.tbViewDetail setHidden:YES];
    
    //[guestInfo reloadView];
    [guestScrollView setHidden:NO];
    [guestScrollView layoutSubviews];
    
    //Check to show indicators
    [self scrollViewDidScroll:guestScrollView];
    //[scviewRoomAssignmentDetail setScrollEnabled:YES];
    [scviewRoomAssignmentDetail setContentSize:guestScrollView.frame.size];
    
    isFromGuestInfo = YES;
}

-(IBAction) btnReassignClicked:(id)sender
{
    isAlreadyReassign = YES;
    if ((roomAssignmentModel.roomAssignmentHousekeeperId <= 0 && (isPushFrom != IS_PUSHED_FROM_FIND_BY_ROOM)))
    {
        [self showReassignViewController];
        return;
    }
    //
    int popUpState = [self confirmCompleteCleaningStatus];
    [self updateRoomStatus];
    if (popUpState == POPUP_STATE_HIDE) {
        [self showReassignViewController];
        return;
        
    } else if(popUpState == POPUP_STATE_HIDE_WITH_INVALID_ROOM_STATUS) {
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:[L_invalid_room_status currentKeyToLanguage] message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:[L_OK currentKeyToLanguage], nil];
        [alert show];
        return;
    }
}

-(void) showReassignViewController
{
    RoomReassignmentViewController *roomReassignment = [[RoomReassignmentViewController alloc] init];
    [roomReassignment setRoomAssignment:roomAssignment];
    [roomReassignment setRoomAssignmentModel:roomAssignmentModel];
    [self.navigationController pushViewController:roomReassignment animated:YES];
}

-(IBAction) btnChecklistClicked:(id)sender
{
    if ([[HomeViewV2 shareHomeView] isSyncing] == YES) {
        CustomAlertViewV2 *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:nil message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_please_wait_syncing_progress] delegate:nil cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_OK] otherButtonTitles:nil];
        [alert show];
        
        [[HomeViewV2 shareHomeView] setSelectedButton:tagOfHomeButton];
        return;
    }
    
    CheckListViewV2 *checklistView = [[CheckListViewV2 alloc] initWithNibName:NSStringFromClass([CheckListViewV2 class]) bundle:nil];
    [checklistView setRoomNo:roomAssignmentModel.roomAssignment_RoomId];
    
    [self.navigationController pushViewController:checklistView animated:YES];
}

-(IBAction)btnHistoryPostingClicked:(id)sender
{
    HistoryPosting *historyPosting = [[HistoryPosting alloc] initWithNibName:@"HistoryPosting" bundle:nil];
    historyPosting.isPushFrom = isPushFrom;
    [self.navigationController pushViewController:historyPosting animated:YES];
}

#pragma mark - CleaningStatusPopup Delegator

-(void)btnCancelClicked
{
    
}

-(void)btnCompleteClicked
{
    
}

-(void) btnDNDClicked 
{
    if (cleaningStatusConfirmPopup == nil) {
        cleaningStatusConfirmPopup = [[CleaningStatusConfirmPopupView alloc] initWithDelegate:self andOption:CLEANING_STATUS_CONFIRM_POPUP_DND];
    }
    [cleaningStatusPopup removeFromSuperview];
    
    [cleaningStatusConfirmPopup setConfirmPopupType:CLEANING_STATUS_CONFIRM_POPUP_DND];
    [cleaningStatusConfirmPopup setRoomNo:(NSString *)[roomAssignment objectForKey:kRoomNo]];
    //[self.tabBarController.view addSubview:cleaningStatusConfirmPopup];
    [self.tabBarController addSubview:cleaningStatusConfirmPopup];
}
-(void) btnDoubleLockClicked
{
    if (cleaningStatusConfirmPopup == nil) {
        cleaningStatusConfirmPopup = [[CleaningStatusConfirmPopupView alloc] initWithDelegate:self andOption:CLEANING_STATUS_CONFIRM_POPUP_DOUBLE_LOCK];
    }
    [cleaningStatusPopup removeFromSuperview];
    
    [cleaningStatusConfirmPopup setConfirmPopupType:CLEANING_STATUS_CONFIRM_POPUP_DOUBLE_LOCK];
    [cleaningStatusConfirmPopup setRoomNo:(NSString *)[roomAssignment objectForKey:kRoomNo]];
    [self.tabBarController addSubview:cleaningStatusConfirmPopup];
}
-(void) btnDeclinedServiceClicked
{
    if (cleaningStatusConfirmPopup == nil) {
        cleaningStatusConfirmPopup = [[CleaningStatusConfirmPopupView alloc] initWithDelegate:self andOption:CLEANING_STATUS_CONFIRM_POPUP_DECLINED_SERVICE];
    }
    [cleaningStatusPopup removeFromSuperview];
    
    [cleaningStatusConfirmPopup setConfirmPopupType:CLEANING_STATUS_CONFIRM_POPUP_DECLINED_SERVICE];
    [cleaningStatusConfirmPopup setRoomNo:(NSString *)[roomAssignment objectForKey:kRoomNo]];
    //[self.tabBarController.view addSubview:cleaningStatusConfirmPopup];
    [self.tabBarController addSubview:cleaningStatusConfirmPopup];
}

-(void) btnServiceLaterClicked
{
    if (cleaningStatusConfirmPopup == nil) {
        cleaningStatusConfirmPopup = [[CleaningStatusConfirmPopupView alloc] initWithDelegate:self andOption:CLEANING_STATUS_CONFIRM_POPUP_SERVICE_LATER];
    }
    [cleaningStatusPopup removeFromSuperview];
    
    [cleaningStatusConfirmPopup setConfirmPopupType:CLEANING_STATUS_CONFIRM_POPUP_SERVICE_LATER];
    [cleaningStatusConfirmPopup setRoomNo:(NSString *)[roomAssignment objectForKey:kRoomNo]];
    //[self.tabBarController.view addSubview:cleaningStatusConfirmPopup];
    [self.tabBarController addSubview:cleaningStatusConfirmPopup];
}

-(void) btnPendingClicked
{
    if (cleaningStatusConfirmPopup == nil) {
        cleaningStatusConfirmPopup = [[CleaningStatusConfirmPopupView alloc] initWithDelegate:self andOption:CLEANING_STATUS_CONFIRM_POPUP_PENDING];
    }
    [cleaningStatusPopup removeFromSuperview];
    
    [cleaningStatusConfirmPopup setConfirmPopupType:CLEANING_STATUS_CONFIRM_POPUP_PENDING];
    [cleaningStatusConfirmPopup setRoomNo:(NSString *)[roomAssignment objectForKey:kRoomNo]];
    //[self.tabBarController.view addSubview:cleaningStatusConfirmPopup];
    [self.tabBarController addSubview:cleaningStatusConfirmPopup];
}

#pragma mark - CleaningStatusConfirmPopup delegate
-(void) btnYesClicked
{
    [self changeTimerStatus:TIMER_STATUS_PAUSE];
    isSaved = YES;
    
    switch (cleaningStatusConfirmPopup.confirmPopupType) {
        case CLEANING_STATUS_CONFIRM_POPUP_DND:
        {
            [cleaningStatusConfirmPopup removeFromSuperview];
            if ([UserManagerV2 isSupervisor] && roomAssignmentModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_DND) {
                return;
            }
            roomAssignmentModel.roomAssignmentRoomCleaningStatusId = ENUM_CLEANING_STATUS_DND;
                      
//            roomAssignmentModel.roomAssignment_Priority_Sort_Order = [[RoomManagerV2 sharedRoomManager] loadMaxPrioprity] + 1;
//            
//            duration = 0;
//            
//            NSDateFormatter *format = [[NSDateFormatter alloc] init];
//            [format setDateFormat:@"yyyy-MM-dd"];
//            NSDate *now = [NSDate date];
//            NSString *dndAssignDate = [NSString stringWithFormat:@"%@ 23:58:00", [format stringFromDate:now]];
//            roomAssignmentModel.roomAssignment_AssignedDate = dndAssignDate;
            
            roomAssignmentModel.roomAssignment_PostStatus = (int)POST_STATUS_SAVED_UNPOSTED;
            [[RoomManagerV2 sharedRoomManager] update:roomAssignmentModel];
            
            MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
            [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getMsgSaveMessage]];
            [self.tabBarController.view addSubview:HUD];
            [HUD show:YES];
            SyncManagerV2 *synManager = [[SyncManagerV2 alloc] init];
//            [synManager postRoomAssignmentWithUserID:userId AndRaModel:roomAssignmentModel];
            if (isPushFrom == IS_PUSHED_FROM_FIND_BY_ROOM) {
//                [synManager postRoomCleaningStatusByUserId:roomAssignmentModel.roomAssignmentHousekeeperId withRoomAssignmentModel:roomAssignmentModel];
                [synManager postRoomCleaningStatusByUserId:[[[UserManagerV2 sharedUserManager] currentUser]userId] withRoomAssignmentModel:roomAssignmentModel];
            }
            else {
                [synManager postRoomCleaningStatusByUserId:roomAssignmentModel.roomAssignment_UserId withRoomAssignmentModel:roomAssignmentModel];
            }
            
            [HUD hide:YES];
            [HUD removeFromSuperview];
            
            [self.navigationController popViewControllerAnimated:YES];
//            [self removeCleaningStatusConfirmPopupFromSuperview];
        }
            break;
        // CRF [Huy][20161108/CRF-00001431]
        case CLEANING_STATUS_CONFIRM_POPUP_DOUBLE_LOCK:
        {
            [cleaningStatusConfirmPopup removeFromSuperview];
            if ([UserManagerV2 isSupervisor] && roomAssignmentModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_DOUBLE_LOCK) {
                return;
            }
            roomAssignmentModel.roomAssignmentRoomCleaningStatusId = ENUM_CLEANING_STATUS_DOUBLE_LOCK;
            roomAssignmentModel.roomAssignment_PostStatus = (int)POST_STATUS_SAVED_UNPOSTED;
            [[RoomManagerV2 sharedRoomManager] update:roomAssignmentModel];
            
            MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
            [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getMsgSaveMessage]];
            [self.tabBarController.view addSubview:HUD];
            [HUD show:YES];
            SyncManagerV2 *synManager = [[SyncManagerV2 alloc] init];
            if (isPushFrom == IS_PUSHED_FROM_FIND_BY_ROOM) {
                [synManager postRoomCleaningStatusByUserId:[[[UserManagerV2 sharedUserManager] currentUser]userId] withRoomAssignmentModel:roomAssignmentModel];
            }
            else {
                [synManager postRoomCleaningStatusByUserId:roomAssignmentModel.roomAssignment_UserId withRoomAssignmentModel:roomAssignmentModel];
            }
            
            [HUD hide:YES];
            [HUD removeFromSuperview];
            
            [self.navigationController popViewControllerAnimated:YES];
        }
            break;
        case CLEANING_STATUS_CONFIRM_POPUP_COMPLETE:
            break;
            
        case CLEANING_STATUS_CONFIRM_POPUP_DECLINED_SERVICE:
        {
            [cleaningStatusConfirmPopup removeFromSuperview];
            if ([UserManagerV2 isSupervisor] && roomAssignmentModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_DECLINE_SERVICE) {
                return;
            }
            
//            timeLeft = roomAssignmentModel.roomAssignmentRoomExpectedCleaningTime*60;
//            duration = 0;            
//            
//            //update assign date for Decline Service to 11:59:00
//            NSDateFormatter *format = [[NSDateFormatter alloc] init];
//            [format setDateFormat:@"yyyy-MM-dd"];
//            NSDate *now = [NSDate date];
//            NSString *declineServiceAssignDate = [NSString stringWithFormat:@"%@ 23:59:00", [format stringFromDate:now]];
//            roomAssignmentModel.roomAssignment_AssignedDate = declineServiceAssignDate;
            
            roomAssignmentModel.roomAssignmentRoomCleaningStatusId = ENUM_CLEANING_STATUS_DECLINE_SERVICE;
            roomAssignmentModel.roomAssignment_PostStatus = (int)POST_STATUS_SAVED_UNPOSTED;
            
            [[RoomManagerV2 sharedRoomManager] update:roomAssignmentModel];
            
            
            MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
            [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getMsgSaveMessage]];
            [self.tabBarController.view addSubview:HUD];
            [HUD show:YES];
            SyncManagerV2 *synManager = [[SyncManagerV2 alloc] init];
//            [synManager postRoomAssignmentWithUserID:userId AndRaModel:roomAssignmentModel];
            if (isPushFrom == IS_PUSHED_FROM_FIND_BY_ROOM) {
//                [synManager postRoomCleaningStatusByUserId:roomAssignmentModel.roomAssignmentHousekeeperId withRoomAssignmentModel:roomAssignmentModel];
                [synManager postRoomCleaningStatusByUserId:[[[UserManagerV2 sharedUserManager] currentUser]userId] withRoomAssignmentModel:roomAssignmentModel];
                
            }
            else {
                [synManager postRoomCleaningStatusByUserId:roomAssignmentModel.roomAssignment_UserId withRoomAssignmentModel:roomAssignmentModel];
            }
            
            [HUD hide:YES];
            [HUD removeFromSuperview];
            
            [self.navigationController popViewControllerAnimated:YES];
//            [self removeCleaningStatusConfirmPopupFromSuperview];
        }
            break;
            
        case CLEANING_STATUS_CONFIRM_POPUP_PENDING:
        {
            [cleaningStatusConfirmPopup removeFromSuperview];
            if ([UserManagerV2 isSupervisor] && roomAssignmentModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_PENDING_FIRST_TIME) {
                return;
            }
            
//            if (roomAssignmentModel.raIsReassignedRoom == 1) {
//                roomAssignmentModel.roomAssignmentRoomCleaningStatusId = ENUM_CLEANING_STATUS_PENDING_SECOND_TIME;
//            }
//            else {
//                roomAssignmentModel.roomAssignmentRoomCleaningStatusId = ENUM_CLEANING_STATUS_PENDING_FIRST_TIME;
//            }
            roomAssignmentModel.roomAssignmentRoomCleaningStatusId = ENUM_CLEANING_STATUS_PENDING_FIRST_TIME;
            
            roomAssignmentModel.roomAssignment_PostStatus = (int)POST_STATUS_SAVED_UNPOSTED;
            [[RoomManagerV2 sharedRoomManager] update:roomAssignmentModel];    
            
            MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
            [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getMsgSaveMessage]];
            [self.tabBarController.view addSubview:HUD];
            [HUD show:YES];
            SyncManagerV2 *synManager = [[SyncManagerV2 alloc] init];
//            [synManager postRoomAssignmentWithUserID:userId AndRaModel:roomAssignmentModel];
            if (isPushFrom == IS_PUSHED_FROM_FIND_BY_ROOM) {
//                [synManager postRoomCleaningStatusByUserId:roomAssignmentModel.roomAssignmentHousekeeperId withRoomAssignmentModel:roomAssignmentModel];
                [synManager postRoomCleaningStatusByUserId:[[[UserManagerV2 sharedUserManager] currentUser]userId] withRoomAssignmentModel:roomAssignmentModel];
            }
            else {
                [synManager postRoomCleaningStatusByUserId:roomAssignmentModel.roomAssignment_UserId withRoomAssignmentModel:roomAssignmentModel];
            }
            
            [HUD hide:YES];
            [HUD removeFromSuperview];
            
            [self.navigationController popViewControllerAnimated:YES];
//            [self removeCleaningStatusConfirmPopupFromSuperview];
        }
            break;
            
        case CLEANING_STATUS_CONFIRM_POPUP_STOP:
        {
            if([UserManagerV2 isSupervisor])
            {
                [cleaningStatusConfirmPopup removeFromSuperview];
                isSaved = YES;
                [btnStartStop setBackgroundImage:[UIImage imageBeforeiOS7:imgStartBtn equaliOS7:imgStartBtnFlat] forState:UIControlStateNormal];
                [btnStartStop setBackgroundImage:[UIImage imageBeforeiOS7:imgStartBtnClicked equaliOS7:imgStartBtnClickedFlat] forState:UIControlEventTouchDown];
                
                if ([self canUpdateStopRecord] == CAN_START_RECORD_DETAIL) {
                    
                } else {
                    //return;
                }
                
                [self changeTimerStatus:TIMER_STATUS_PAUSE];
                [self setEnableBtnAtion:NO];
                [self setEnableBtnComplete:NO];
                
                //Hao Tran - remove for unecessary
                //[CommonVariable sharedCommonVariable].roomIsCompleted = 2;
                
                roomAssignmentModel.roomAssignmentRoomInspectionStatusId = ENUM_INSPECTION_PAUSE;
                
                //update stop record
                [self updateStopRecord];
                int responseStatus = [self postCurrentInpsection];
                //Show Invalid room number
                if(responseStatus == RESPONSE_STATUS_INVALID_ROOM_STATUS) {
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:[L_invalid_room_status currentKeyToLanguage] message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:[L_OK currentKeyToLanguage], nil];
                    [alert show];
                    return;
                }
                
                [self.navigationController popViewControllerAnimated:YES];
            }
            else
            {
                [cleaningStatusConfirmPopup removeFromSuperview];
                //CRF-1170: RA can update room status to VPU
                //Force complete room if RA select room status VPU
                if (roomStatusIdSelected > notSelectRoomStatusYet && roomStatusIdSelected == ENUM_ROOM_STATUS_VPU) {
                    [self btnCompleteClicked:btnComplete];
                    timerStatus = TIMER_STATUS_START;
                    return;
                }
                if ([CommonVariable sharedCommonVariable].currentPauseRoom == noPauseRoom) {
                    [CommonVariable sharedCommonVariable].currentPauseRoom = roomAssignmentModel.roomAssignment_Id;
                }
                else if ([CommonVariable sharedCommonVariable].currentPauseRoom != greaterThanOnePauseRoom) {
                    [CommonVariable sharedCommonVariable].currentPauseRoom = greaterThanOnePauseRoom;
                }
                
                if (timeLeft < 0) {
                    [btnTimer setBackgroundImage:[UIImage imageBeforeiOS7:imgTimerOver equaliOS7:imgTimerOverFlat] forState:UIControlStateNormal];
                }
                else {
                    [btnTimer setBackgroundImage:[UIImage imageBeforeiOS7:imgTimerPause equaliOS7:imgTimerPauseFlat] forState:UIControlStateNormal];
                }
                
                [btnStartStop setBackgroundImage:[UIImage imageBeforeiOS7:imgStartBtn equaliOS7:imgStartBtnFlat] forState:UIControlStateNormal];
                [btnStartStop setBackgroundImage:[UIImage imageBeforeiOS7:imgStartBtnClicked equaliOS7:imgStartBtnClickedFlat] forState:UIControlEventTouchDown];
                isSaved = YES;
                
                //        if([self isDurationLessMin]){
                //            isMustResetTimer = FALSE;
                //        }else
                //        {
                //            isMustResetTimer = TRUE;
                //        }
                
                // REMOVE CAUSE WE DON'T NEED TO CHECK MAX RECORDS CLEANING TIME FOR MAID
                //            if([self isMaxRoomRecordDetail]){
                //                if (countReminderPopup) {
                //                    countReminderPopup = [[CountPopupReminderViewV2 alloc] initWithRoomId:[TasksManagerV2 getCurrentRoomAssignment]];
                //                    [countReminderPopup setDelegate:self];
                //
                //                }
                //
                //                if(![countReminderPopup show]){
                //                    NSString *msgbtnNo = [[LanguageManagerV2 sharedLanguageManager] getNo];
                //                    NSString *msgbtnYes = [[LanguageManagerV2 sharedLanguageManager] getYes];
                //
                //
                //                    UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_are_you_done_with_the_room_cleaning] delegate:self cancelButtonTitle:msgbtnYes otherButtonTitles:msgbtnNo, nil];
                //                    alert.delegate = self;
                //                    [alert show];
                //                }
                //
                //                return;
                //            }
                
                [CommonVariable sharedCommonVariable].roomIsCompleted = 2;
                
                NSDateFormatter *timeFormat = [[NSDateFormatter alloc] init];
                [timeFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                NSDate *now = [NSDate date];
                
                roomRecordDetailModel.recd_Ra_Id = [TasksManagerV2 getCurrentRoomAssignment];
                roomRecordDetailModel.recd_User_Id = [UserManagerV2 sharedUserManager].currentUser.userId;
                roomRecordDetailModel.recd_Stop_Date = [timeFormat stringFromDate:now];
                
                //set saved unposted before send update to WS
                roomAssignmentModel.roomAssignment_PostStatus = (int)POST_STATUS_SAVED_UNPOSTED;
                roomAssignmentModel.roomAssignmentRoomCleaningStatusId = ENUM_CLEANING_STATUS_PAUSE;
                roomRecordModel.rrec_cleaning_pause_time = [timeFormat stringFromDate:now]; //save stoop time
                roomRecordModel.rrec_PostStatus = (int)POST_STATUS_SAVED_UNPOSTED;
                roomRecordModel.rrec_Cleaning_End_Time = @"1900-01-01 00:00:00";//[timeFormat stringFromDate:now];
                NSInteger dur = duration - roomRecordModel.rrec_Total_Cleaning_Time;
                roomRecordModel.rrec_Last_Cleaning_Duration = [NSString stringWithFormat:@"%d", (int)dur];
                roomRecordModel.rrec_Total_Cleaning_Time += (int)dur;
                
                if(roomRecordModel.rrec_Id <= 0) {
                    [[RoomManagerV2 sharedRoomManager] insertRoomRecordData:roomRecordModel];
                } else {
                    [[RoomManagerV2 sharedRoomManager] updateRoomRecordData:roomRecordModel];
                }
                //HaoTran[20130529/Update RoomRecord Data] - END
                
                int responseStatus = (int)[self updateRoomStatusToWS];
                if(responseStatus == RESPONSE_STATUS_ROOM_HAVE_BEEN_DELETED) {
                    UIAlertView *alert =[[UIAlertView alloc]initWithTitle:[L_MSG_ROOM_REMOVE currentKeyToLanguage]/*@"Error - This room is removed"*/ message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:[L_OK currentKeyToLanguage], nil];
                    [alert show];
                    
                    return;
                } else if(responseStatus == RESPONSE_STATUS_INVALID_ROOM_STATUS) {
                    UIAlertView *alert =[[UIAlertView alloc]initWithTitle:[L_invalid_room_status currentKeyToLanguage] message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:[L_OK currentKeyToLanguage], nil];
                    [alert show];
                    
                    return;
                }
                
                [[RoomManagerV2 sharedRoomManager] update:roomAssignmentModel];
                
                // REMOVE CAUSE WE NO NEED UPDATE ROOM MODEL AND THIS MADE BADGE NUMBER ISSUE
                //            roomModel.room_PostStatus = POST_STATUS_SAVED_UNPOSTED;
                //            [[RoomManagerV2 sharedRoomManager] updateRoomModel:roomModel];
                [self.navigationController popViewControllerAnimated:YES];
                //            [self removeCleaningStatusConfirmPopupFromSuperview];
            }
        }
            break;
            
        case CLEANING_STATUS_CONFIRM_POPUP_SERVICE_LATER:
        {
            [cleaningStatusConfirmPopup removeFromSuperview];
            if ([UserManagerV2 isSupervisor] && roomAssignmentModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_SERVICE_LATER) {
                return;
            }
            
            MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
            [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getMsgSaveMessage]];
            [self.tabBarController.view addSubview:HUD];
            [HUD show:YES];
            
            NSDateFormatter *_timeFomarter = [[NSDateFormatter alloc] init];
            [_timeFomarter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            
            RoomServiceLaterModelV2 *roomServicelater = [[RoomServiceLaterModelV2 alloc] init];
            roomServicelater.rsl_RecordId = roomRecordModel.rrec_Id;
            roomServicelater.rsl_Time = [_timeFomarter stringFromDate:cleaningStatusConfirmPopup.selectedDate];    
            [[RoomManagerV2 sharedRoomManager] insertRoomServiceLaterData:roomServicelater];
            
            roomAssignmentModel.roomAssignmentRoomCleaningStatusId = ENUM_CLEANING_STATUS_SERVICE_LATER;
            roomAssignmentModel.roomAssignment_AssignedDate = [_timeFomarter stringFromDate:cleaningStatusConfirmPopup.selectedDate];    
            roomAssignmentModel.roomAssignment_PostStatus = (int)POST_STATUS_SAVED_UNPOSTED;
            [[RoomManagerV2 sharedRoomManager] update:roomAssignmentModel];
            
            roomModel.room_PostStatus = (int)POST_STATUS_SAVED_UNPOSTED;
            [[RoomManagerV2 sharedRoomManager] updateRoomModel:roomModel];
            
            timeLeft = roomAssignmentModel.roomAssignmentRoomExpectedCleaningTime*60;
            duration = 0;            
            
            roomAssignmentModel.roomAssignment_PostStatus = (int)POST_STATUS_SAVED_UNPOSTED;
            SyncManagerV2 *synManager = [[SyncManagerV2 alloc] init];
        
            int responseCode = RESPONSE_STATUS_NO_NETWORK;
            
            if (isPushFrom == IS_PUSHED_FROM_FIND_BY_ROOM) {
//                 [synManager postRoomAssignmentWithUserID:roomAssignmentModel.roomAssignmentHousekeeperId AndRaModel:roomAssignmentModel];
                responseCode = [synManager postRoomAssignmentWithUserID:[[[UserManagerV2 sharedUserManager] currentUser]userId] AndRaModel:roomAssignmentModel];
            }
            else {
                responseCode = [synManager postRoomAssignmentWithUserID:userId AndRaModel:roomAssignmentModel];
            }
            
            //[HUD hide:YES];
            //[HUD removeFromSuperview];
            
            if(responseCode == RESPONSE_STATUS_NO_NETWORK) {
                
                UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:@"" message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_internet_exception] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getOK] otherButtonTitles: nil];
                [alert show];
                [HUD hide:YES];
                [HUD removeFromSuperview];
                return;
                
            } else if(responseCode == RESPONSE_STATUS_ERROR_REASSIGN_DATE_IN_THE_PAST) {
                
                UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:@"" message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_assign_date_can_not_be_past] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getOK] otherButtonTitles: nil];
                [alert show];
                [HUD hide:YES];
                [HUD removeFromSuperview];
                return;
            } else if(responseCode == RESPONSE_STATUS_ERROR_REASSIGN_OUT_OF_STAFF_WORKSHIFT) {
                
                UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:@"" message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_assignment_out_of_staff_workshift] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getOK] otherButtonTitles: nil];
                [alert show];
                [HUD hide:YES];
                [HUD removeFromSuperview];
                return;
            } else if(responseCode == RESPONSE_STATUS_ERROR_REASSIGN_OUT_OF_REMAINED_TIME) {
                
                UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:@"" message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_assignment_out_of_remained_time] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getOK] otherButtonTitles: nil];
                [alert show];
                [HUD hide:YES];
                [HUD removeFromSuperview];
                return;
            } else if(responseCode == RESPONSE_STATUS_ERROR_ASSIGNMENT_NOT_EXIST) {
                
                UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:@"" message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_assignment_not_exist] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getOK] otherButtonTitles: nil];
                [alert show];
                [HUD hide:YES];
                [HUD removeFromSuperview];
                return;
            } else if(responseCode == RESPONSE_STATUS_ERROR_STAFF_NOT_ASSIGNED) {
                
                UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:@"" message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_assignment_staff_not_assigned] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getOK] otherButtonTitles: nil];
                [alert show];
                [HUD hide:YES];
                [HUD removeFromSuperview];
                return;
            } else if(responseCode == RESPONSE_STATUS_ERROR_END_CLEANING_TIME_EXCEED_CURRENT_DAY_ASSIGNMENT) {
                
                UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:@"" message:[L_end_cleaning_time_exceed_current_day_assignment currentKeyToLanguage] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getOK] otherButtonTitles: nil];
                [alert show];
                [HUD hide:YES];
                [HUD removeFromSuperview];
                return;
            }
            
            NSCalendar *calender = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
            NSDateComponents *components = [calender components:NSMinuteCalendarUnit|NSSecondCalendarUnit fromDate:[NSDate date] toDate:cleaningStatusConfirmPopup.selectedDate options:0];
            
            NSInteger time = components.minute*60 + components.second;
            if (time > 0 && time < 5*60) {
                [cleaningStatusConfirmPopup setConfirmPopupType:CLEANING_STATUS_CONFIRM_POPUP_SERVICE_LATER_REMINDER];
                [HUD hide:YES];
                [HUD removeFromSuperview];
                //[self.tabBarController.view addSubview:cleaningStatusConfirmPopup];
                [self.tabBarController addSubview:cleaningStatusConfirmPopup];
            }
            else {
//                [self removeCleaningStatusConfirmPopupFromSuperview];
                [HUD hide:YES];
                [HUD removeFromSuperview];
                [self.navigationController popViewControllerAnimated:YES];
            }
        }
            break;
            
        case CLEANING_STATUS_CONFIRM_POPUP_SERVICE_LATER_REMINDER:
            break;
            
        default:
            break;
    }
}

-(void) btnCloseClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Actionpopup Delegator
-(void) actionPopupViewDidDismissWithButtonIndex:(NSInteger)buttonIndex
{
    [actionPopup removeFromSuperview];
    
    if (actionLostAndFound.isActive || actionEngineering.isActive || actionLaundry.isActive
        || actionMinibar.isActive || actionLinen.isActive || actionAmenities.isActive) {
        if (!checklist.isActive) {
            if (buttonIndex != 0) {
                buttonIndex++;
            }
        }
    }
    else {
        if (checklist.isActive) {
            buttonIndex++;
        }
        else {
            buttonIndex += 2;
        }
    }
   
    
    if ([UserManagerV2 isSupervisor]) {
        switch (buttonIndex) {
            case 0:
            {
                CountViewV2 *countView = [[CountViewV2 alloc] initWithNibName:@"CountViewV2" bundle:nil];
                countView.isFromMainPosting = NO;
                [self.navigationController pushViewController:countView animated:YES];
            }
                break;
                
            case 1:
            {
                if ([[HomeViewV2 shareHomeView] isSyncing] == YES) {
                    CustomAlertViewV2 *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:nil message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_please_wait_syncing_progress] delegate:nil cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_OK] otherButtonTitles:nil];
                    [alert show];
                    
                    //set selected button in tabbar
                    [[HomeViewV2 shareHomeView] setSelectedButton:tagOfHomeButton];
                    return;
                }
                
                //            if (timerStatus == TIMER_STATUS_PAUSE || timerStatus == TIMER_STATUS_NOT_START) {
                //                [self startInspection];
                //            }
                
                //selected checklist view
//                [[HomeViewV2 shareHomeView] setSelectedButton:tagOfCheckListButton];                
//                if (isSaved == YES && (roomAssignmentModel.roomAssignmentRoomInspectionStatusId == ENUM_INSPECTION_COMPLETED_FAIL || roomAssignmentModel.roomAssignmentRoomInspectionStatusId == ENUM_INSPECTION_COMPLETED_PASS)) {
//                    CheckListForMaidViewV2 *checklistView = [[CheckListForMaidViewV2 alloc] initWithNibName:NSStringFromClass([CheckListForMaidViewV2 class]) bundle:nil];
//                    [checklistView setRoomNo:roomAssignmentModel.roomAssignment_RoomId];
//                    
//                    [self.navigationController pushViewController:checklistView animated:YES];
//                    return;
//                }
                
                CheckListViewV2 *checklistView = [[CheckListViewV2 alloc] initWithNibName:NSStringFromClass([CheckListViewV2 class]) bundle:nil];
                [checklistView setRoomNo:roomAssignmentModel.roomAssignment_RoomId];
                checklistView.roomType = roomModel.room_TypeId;
                
                [self.navigationController pushViewController:checklistView animated:YES];
            }
                break;
                
            case 2:
            {
                UIBarButtonItem *BackButtonRoomInfo = [[UIBarButtonItem alloc] initWithTitle: [self getBackString] style: UIBarButtonItemStyleBordered target: nil action: nil];    
                [[self navigationItem] setBackBarButtonItem: BackButtonRoomInfo];    
                
                GuidelineViewV2* aGuidelineView = [[GuidelineViewV2 alloc] initWithNibName:@"GuidelineViewV2" bundle:nil];
                aGuidelineView.isPushFrom = IS_PUSHED_FROM_ROOM_ASSIGNMENT;
                aGuidelineView.roomNo = roomModel.room_Id;
                aGuidelineView.roomTypeId = roomModel.room_TypeId;
                
                //set flag room is not completed
                [aGuidelineView setIsRoomCompleted:NO];
                
                [self.navigationController pushViewController:aGuidelineView animated:YES]; 
            }
                break;
            
            default:
                break;
        }
    }
    else {
        switch (buttonIndex) {
            case 0:
            {
                CountViewV2 *countView = [[CountViewV2 alloc] initWithNibName:@"CountViewV2" bundle:nil];
                countView.isFromMainPosting = NO;
                [self.navigationController pushViewController:countView animated:YES];
            }
                break;
                
                // Hao Tran[20130509/Check to show checklist] - Show checklist whether access right enabled or not
                /********************************
                 Description:
                 
                 Case 1 and Case 2 are used for Show CheckList or GuidelineView.
                 If checklist did not active in case 1, we will use case 2 to show Guideline view.
                 *******************************/
            case 1:
            {
                //if we have check list
                if(checklist.isActive)
                {
                    CheckListViewV2 *checklistView = [[CheckListViewV2 alloc] initWithNibName:NSStringFromClass([CheckListViewV2 class]) bundle:nil];
                    checklistView.roomType = roomModel.room_TypeId;
                    [checklistView setRoomNo:roomAssignmentModel.roomAssignment_RoomId];
                    
                    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK] style:UIBarButtonItemStyleBordered target:nil action:nil];
                    self.navigationItem.backBarButtonItem = backButton;
                    
                    [self.navigationController pushViewController:checklistView animated:YES];
                    
                    //break switch case to prevent jump to case 2
                    break;
                }
            }
                
            case 2:
            {
                UIBarButtonItem *BackButtonRoomInfo = [[UIBarButtonItem alloc] initWithTitle: [self getBackString] style: UIBarButtonItemStyleBordered target: nil action: nil];
                [[self navigationItem] setBackBarButtonItem: BackButtonRoomInfo];
                
                GuidelineViewV2* aGuidelineView = [[GuidelineViewV2 alloc] initWithNibName:@"GuidelineViewV2" bundle:nil];
                aGuidelineView.isPushFrom = IS_PUSHED_FROM_ROOM_ASSIGNMENT;
                aGuidelineView.roomNo = roomModel.room_Id;
                aGuidelineView.roomTypeId = roomModel.room_TypeId;
                
                //set flag room is not completed
                [aGuidelineView setIsRoomCompleted:NO];
                
                [self.navigationController pushViewController:aGuidelineView animated:YES];
            }
                break;
                
                // Hao Tran[20130509/Check to show checklist] - END
            default:
                break;
        }
    }
}
#pragma mark - ConfirmPopup Delegate
-(void) confirmPopupDismissWhenBtnYesClicked
{
    [confirmPopup removeFromSuperview];
}

-(void) confirmPopupDismissWhenBtnNoClicked
{
    [confirmPopup removeFromSuperview];
}

#pragma mark - RemarkView delegate
-(void) remarkViewV2SubmitWithText:(NSString *) text andController:(UIViewController*)controllerView{
    [self updateRemark:text andController:controllerView];
    int type = RemarkType_RoomRemark;
    
    if(fromRemarkView == 1){
        type = RemarkType_PhyshicalCheck;
        roomRemarkModel.rm_physical_content = text;
    }else{
        roomRemarkModel.rm_content = text;
        if([[UserManagerV2 sharedUserManager].currentUserAccessRight isRemarkConsolidation]) {
            type = RemarkType_ConsolidationRemark;
//            [[RoomManagerV2 sharedRoomManager] updateRoomRemarksByUserId:[UserManagerV2 sharedUserManager].currentUser.userId hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId remarkType:RemarkType_ConsolidationRemark roomNumber:roomAssignmentModel.roomAssignment_RoomId remarkValue:text];
        }
    }
    
    if (roomRemarkModel.rm_Id == -1) {
        roomRemarkModel.rm_PostStatus = (int)POST_STATUS_POSTED;
        [[RoomManagerV2 sharedRoomManager] insertRoomRemarkData:roomRemarkModel];
        [[RoomManagerV2 sharedRoomManager] loadRoomRemarkDataByRecordId:roomRemarkModel];
    }else{
        [[RoomManagerV2 sharedRoomManager] updateRoomRemarkData:roomRemarkModel];
    }
    int status = [[RoomManagerV2 sharedRoomManager] updateRoomRemarksByUserId:[UserManagerV2 sharedUserManager].currentUser.userId hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId remarkType:type roomNumber:roomAssignmentModel.roomAssignment_RoomId remarkValue:text];
    if (status != RESPONSE_STATUS_OK) {
        roomRemarkModel.rm_PostStatus = (int)POST_STATUS_SAVED_UNPOSTED;
        [[RoomManagerV2 sharedRoomManager] updateRoomRemarkData:roomRemarkModel];
        [[[[iToast makeText:[[LanguageManagerV2 sharedLanguageManager] getMsgPostFailMessage]] setGravity:iToastGravityBottom] setDuration:iToastDurationNormal] show];
    }
    
}
-(void)remarkViewV2DoneWithText:(NSString *)text andController:(UIViewController *)controllerView{
    if (self.isLoadFromFind) {
        return;
    }
    [self updateRemark:text andController:controllerView];
    if(fromRemarkView == 0){
        int type = RemarkType_RoomRemark;
        if([[UserManagerV2 sharedUserManager].currentUserAccessRight isRemarkConsolidation]) {
            type = RemarkType_ConsolidationRemark;
        }
        [[RoomManagerV2 sharedRoomManager] updateRoomRemarksByUserId:[UserManagerV2 sharedUserManager].currentUser.userId hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId remarkType:type roomNumber:roomAssignmentModel.roomAssignment_RoomId remarkValue:text];
    }else{
//        if(!isDemoMode && [[UserManagerV2 sharedUserManager].currentUserAccessRight isShowPhyshicalCheckRemark]) {
//            [[RoomManagerV2 sharedRoomManager] updateRoomRemarksByUserId:[UserManagerV2 sharedUserManager].currentUser.userId hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId remarkType:RemarkType_PhyshicalCheck roomNumber:roomAssignmentModel.roomAssignment_RoomId remarkValue:text];
//        }
        if(!isDemoMode ) {
            [[RoomManagerV2 sharedRoomManager] updateRoomRemarksByUserId:[UserManagerV2 sharedUserManager].currentUser.userId hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId remarkType:RemarkType_PhyshicalCheck roomNumber:roomAssignmentModel.roomAssignment_RoomId remarkValue:text];
        }
    }
    
}
- (void)updateRemark:(NSString*)text andController:(UIViewController*)controllerView{
    if(fromRemarkView == 0){
        NSInteger row = [self rowIndexRemark];
        NSInteger section = [self sectionIndexRemark];
        
        NSIndexPath *indexpath = [NSIndexPath indexPathForRow:row inSection:section];
        UITableViewCell *cell = nil;
        
        roomRecordModel.rrec_Remark = text;
        
        if(roomRecordModel.rrec_Id <= 0) {
            [[RoomManagerV2 sharedRoomManager] insertRoomRecordData:roomRecordModel];
        } else {
            [[RoomManagerV2 sharedRoomManager] updateRoomRecordData:roomRecordModel];
        }
        
        cell = [self.tbViewDetail cellForRowAtIndexPath:indexpath];
        UILabel *label = (UILabel*)[cell viewWithTag:tagFirstContent];
        [label setText:text];
        
        //Update for Remark consolidation
        
        
    }
    else if(fromRemarkView == 1){
        NSInteger row = [self rowIndexPhysicalCheckRemark];
        NSInteger section = [self sectionIndexPhysicalCheckRemark];
        
        NSIndexPath *indexpath = [NSIndexPath indexPathForRow:row inSection:section];
        UITableViewCell *cell = nil;
        
        
        
        cell = [self.tbViewDetail cellForRowAtIndexPath:indexpath];
        UILabel *label = (UILabel*)[cell viewWithTag:tagPhysicalCheckRemark];
        [label setText:text];
    }
//    remarkView.delegate = nil;
//    physicalCheckRemarkView.delegate = nil;
}

-(void)remarkViewV2didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
    }
    else {
    }
}

#pragma mark - CleaningStatusView delegate

- (void)cleaningStatus:(NSInteger)roomStatus roomStatusText:(NSString*)roomStatusStrValue imageRoomStatus:(UIImage*)imageRoomStatus withViewController:(UIViewController*)controller
{
    roomStatusIdSelected = roomStatus;
    roomStatusTextSelected = roomStatusStrValue;
    roomStatusImageSelected = imageRoomStatus;
    [self.tbViewDetail reloadData];
    [controller.navigationController popViewControllerAnimated:YES];
    //CRF-1170: RA can update room status to VPU
    //Force complete room if RA select room status VPU
    if (roomStatusIdSelected > notSelectRoomStatusYet && roomStatusIdSelected == ENUM_ROOM_STATUS_VPU) {
        [self btnCompleteClicked:btnComplete];
        timerStatus = TIMER_STATUS_START;
        return;
    }
}

-(void)cleaningStatus:(NSInteger)cleaningStatus isRoomStatus:(BOOL)isRoomStatus withTime:(NSDate *)date withViewController:(UIViewController *)controller{
    [controller.navigationController popViewControllerAnimated:NO];
    if(!isRoomStatus) {
        
    } 
}

#pragma mark - CountReminderPopup delegate
-(void) yesCountReminder
{
    //Hao Tran[20130530/Fix missing discrepant checking] - Missing discrepant checking when reminder pop up press yes
    [countReminderPopup hide];
    UIAlertView *pseudoAlertView = [[UIAlertView alloc] init];
    pseudoAlertView.tag = tagConfirmDone;
    //Reuse local delegate alertview
    //button index 0 is "YES" (agree to complete cleaning room)
    [self alertView:pseudoAlertView didDismissWithButtonIndex:0]; 
    //Hao Tran[20130530/Fix missing discrepant checking] - END
    
/*  HaoTran[20130530/Remove Clone Code]
        REMOVE CLONE CODE WITH FUNCTION -(void) alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
        TAKE ADVANTAGE OF USING THAT FUNCTION TO EASY MAINTENANCE
 */
//    isSaved = YES;
//    [self changeTimerStatus:TIMER_STATUS_FINISH];
//    
//    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.view];
//    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_RA_LOADING_DATA_CONTENT]];
//    [self.view.superview addSubview:HUD];
//    [HUD show:YES];  
//    
//    SyncManagerV2 *synManager = [[SyncManagerV2 alloc] init];
//    
//    [CommonVariable sharedCommonVariable].roomIsCompleted = 1;
//    if ([CommonVariable sharedCommonVariable].currentPauseRoom == roomAssignmentModel.roomAssignment_Id) {
//        [CommonVariable sharedCommonVariable].currentPauseRoom = noPauseRoom;
//    }
//    
//    if (roomStatusId != notSelectRoomStatusYet) {
//        [self setRoomStatusModel:roomStatusId];
//        
//        if(roomStatusId != ENUM_ROOM_STATUS_VD && roomStatusId != ENUM_ROOM_STATUS_OD){
//            roomAssignmentModel.roomAssignmentRoomCleaningStatusId = ENUM_CLEANING_STATUS_COMPLETED;
//        }
//        else {
//            roomAssignmentModel.roomAssignmentRoomCleaningStatusId = ENUM_CLEANING_STATUS_PENDING_FIRST_TIME;
//        }
//    }
//    else {
//        roomAssignmentModel.roomAssignmentRoomCleaningStatusId = ENUM_CLEANING_STATUS_COMPLETED;
//        
//        if(roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VD){
//            [self setRoomStatusModel:ENUM_ROOM_STATUS_VC];
//        }
//        
//        if(roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OD){
//            [self setRoomStatusModel:ENUM_ROOM_STATUS_OC];
//        }
//    }
//    
//    NSDateFormatter *timeFormat = [[NSDateFormatter alloc] init];
//    [timeFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
//    
//    // update duration time
//    NSDate *now = [NSDate date];   
//    roomRecordModel.rrec_Cleaning_End_Time = [timeFormat stringFromDate:now];  //... end time    
//    roomRecordModel.rrec_PostStatus = POST_STATUS_SAVED_UNPOSTED;
//    
//    duration += [roomRecordModel.rrec_Cleaning_Duration intValue];    
//    roomRecordModel.rrec_Cleaning_Duration = [NSString stringWithFormat:@"%d",duration];
//    
//    NSTimeInterval timeDifference = [now timeIntervalSinceDate:
//                                     [timeFormat dateFromString:roomRecordModel.rrec_Cleaning_Start_Time]];
//    
//    NSInteger _totalPauseTime = (NSInteger)timeDifference - duration;
//    roomRecordModel.rrec_Cleaning_Total_Pause_Time =[NSString stringWithFormat:@"%d",_totalPauseTime] ;  
//    [[RoomManagerV2 sharedRoomManager] updateRoomRecordData:roomRecordModel];
//    
//    roomAssignmentModel.roomAssignment_PostStatus = POST_STATUS_SAVED_UNPOSTED;    
//    [[RoomManagerV2 sharedRoomManager] update:roomAssignmentModel];
//    
//    roomModel.room_PostStatus = POST_STATUS_SAVED_UNPOSTED;
//    [[RoomManagerV2 sharedRoomManager] updateRoomModel:roomModel];
//    
//    [self changeTimerStatus:TIMER_STATUS_PAUSE];
//    
//    if (isPushFrom == IS_PUSHED_FROM_FIND_BY_RA || isPushFrom == IS_PUSHED_FROM_FIND_BY_ROOM) {
//        [synManager postRoomCleaningtoServerWithUSerID:[[UserManagerV2 sharedUserManager] currentUser].userId AndRoomAssigmentModel:roomAssignmentModel];
//    }
//    else {
//        [synManager postRoomAssignmentWithUserID:[[UserManagerV2 sharedUserManager] currentUser].userId AndRaModel:roomAssignmentModel];
//    }
//    
//    [HUD hide:YES];
//    [HUD removeFromSuperview];
//    
//    [countReminderPopup hide];
//    [self.navigationController popViewControllerAnimated:YES];
//    //HaoTran[20130530/Remove Clone Code] - END
}

-(void) noCountReminder
{
    isSaved = NO;
    
    CountViewV2 *countView = [[CountViewV2 alloc] initWithNibName:@"CountViewV2" bundle:nil];
    countView.isFromMainPosting = NO;
    [self.navigationController pushViewController:countView animated:YES];

    [countReminderPopup hide];
}

#pragma mark - UIAlertView delegate
-(void) alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 1)
        return;
    
    //Discrepant or complete confirm
    if (alertView.tag == tagConfirmDone || alertView.tag == tagConfirmDiscrepantRoomStatus) {
        
        if (buttonIndex == 0) {
            // DungPhan - 20150818 - Compare flow of change Room Status with Android: Check if Room Status not change for allowing to update to complete cleaning
            if (roomStatusIdSelected == roomAssignmentModel.roomAssignmentRoomStatusId) {
                roomStatusIdSelected = notSelectRoomStatusYet;
            }
            
            if(alertView.tag == tagConfirmDiscrepantRoomStatus) {
                if(roomStatusId <= 0) {
                    roomStatusId = roomStatusIdSelected;
                }
            } else { //tagconfirmDone
                if(roomStatusIdSelected > 0) {
                    //roomAssignmentModel.roomAssignmentRoomStatusId = roomStatusIdSelected;
                    roomStatusId = roomStatusIdSelected;
                }
            }
            //check different roomStatusId from local with WS.
            //If It's different together we will ask to complete room or not
            if([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]
               && alertView.tag != tagConfirmDiscrepantRoomStatus)
            {
                RoomManagerV2 *roomManager = [[RoomManagerV2 alloc] init];
                int currentUserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
                int statusIdFromWS = 0 ;
                if(roomStatusIdSelected > 0) {
                    statusIdFromWS = [roomManager reloadRoomStatusWSByUserId:currentUserId roomAssignmentId:roomAssignmentModel.roomAssignment_Id currentRoomStatusId:(int)roomStatusIdSelected];
                } else {
                    statusIdFromWS = [roomManager reloadRoomStatusWSByUserId:currentUserId roomAssignmentId:roomAssignmentModel.roomAssignment_Id currentRoomStatusId:(int)roomAssignmentModel.roomAssignmentRoomStatusId];
                }
                
                //Check the real room stauts can be updated from server side
                //Check if room stauts can change or not
                BOOL roomStatusInvalid = NO;
                if(statusIdFromWS > 0 && statusIdFromWS != NSNotFound) {
                    if(roomStatusIdSelected > 0) {
                        if(statusIdFromWS != roomStatusIdSelected) {
                            roomStatusInvalid = YES;
                        }
                    } else {
                        if(statusIdFromWS != roomAssignmentModel.roomAssignmentRoomStatusId) {
                            roomStatusInvalid = YES;
                        }
                    }
                }
                
                if(roomStatusInvalid)
                {
                    //To update local database match with statusIdFromWS
                    roomStatusId = statusIdFromWS;
                    
                    RoomStatusModelV2 *newRoomStatusModel = [[RoomStatusModelV2 alloc] init];
                    newRoomStatusModel.rstat_Id = statusIdFromWS;
                    [[RoomManagerV2 sharedRoomManager] loadRoomStatusData:newRoomStatusModel];
                    
                    NSString *messageAlert = nil;
                    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
                        messageAlert = [NSString stringWithFormat:@"Room %@ is %@. \n\t Complete?", roomAssignmentModel.roomAssignment_RoomId, newRoomStatusModel.rstat_Code];
                    } else {
                        messageAlert = [NSString stringWithFormat:@"\n\n\n\n\nRoom %@ is %@. \n\t Complete?", roomAssignmentModel.roomAssignment_RoomId,newRoomStatusModel.rstat_Code];
                    }
                    
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[L_confirm_title currentKeyToLanguage] message:messageAlert delegate:self cancelButtonTitle:[L_YES currentKeyToLanguage] otherButtonTitles:[L_NO currentKeyToLanguage], nil];
                    UIImageView *alertImage = [[UIImageView alloc] initWithFrame:CGRectMake(80, 35, 120, 120)];
                    [alertImage setImage:[UIImage imageNamed:@"icon_wrong room.png"]];
                    alert.tag = tagConfirmDiscrepantRoomStatus;
                    [alert addSubview:alertImage];
                    [alert show];
                    return;
                }
            }else if(alertView.tag == tagConfirmDiscrepantRoomStatus){
                roomStatusId = notSelectRoomStatusYet;
            }
            
            
            if ([UserManagerV2 isSupervisor]) {
                if ([self confirmCompleteCleaningStatus] == POPUP_STATE_HIDE) {
                    [self.navigationController popViewControllerAnimated:YES];
                }
                return;
            }else{
                //Complete and post data to WS
                int responseStatus = [self completeCurrentRoom];
                
                //Show Invalid room number
                if(responseStatus == RESPONSE_STATUS_INVALID_ROOM_STATUS) {
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:[L_invalid_room_status currentKeyToLanguage] message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:[L_OK currentKeyToLanguage], nil];
                    [alert show];
                    return;
                }
            }
            
            [self.navigationController popViewControllerAnimated:YES];
        }
    } else if(alertView.tag == tagCompleteRoomByRoomStatus) {
        [self handleComplete];
    } else if (alertView.tag == tagForceCloseRoomDetail) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}
- (void)handleComplete{
    roomStatusId = roomStatusIdSelected;
    [self completeRoomByRoomStatusUpdated];
}
#pragma mark - Timer method
-(void) countDownWithTimeDif:(NSInteger) timeDif
{
    //Hao Tran fix - Will not update timer if room didn't start or pause
    if(timerStatus == TIMER_STATUS_NOT_START || timerStatus == TIMER_STATUS_PAUSE){
        return;
    }
    
    timeLeft -= timeDif;
    duration += timeDif;
    [btnTimer setTitle:[self countTimeSpendForTask:timeLeft option:YES] forState:UIControlStateNormal];
    if (timeLeft < 0) {
        [btnTimer setBackgroundImage:[UIImage imageBeforeiOS7:imgTimerOver equaliOS7:imgTimerOverFlat] forState:UIControlStateNormal];
    }
}

-(void) countDown
{
    [self countDownWithTimeDif:1];
}

- (NSString *)countTimeSpendForTask:(NSInteger)expectedTimeForTask  option:(BOOL) isAttachedSecond{
    if (expectedTimeForTask == 0) {
        return @"";
    }
    NSString *secondsString;
    NSString *minutesString;
    NSString *hoursString;
    NSInteger secondsInt;
    NSInteger minutesInt;
    NSInteger hoursInt;
    NSString *timeDisplay;
    NSString *minus = @"-";
    if (expectedTimeForTask < 0) {
        hoursInt = -expectedTimeForTask/3600;
        minutesInt =-((expectedTimeForTask + (hoursInt*3600)) / 60);
        secondsInt =-(expectedTimeForTask % 60);
        minus = @"-";
    } 
    else {
        hoursInt = expectedTimeForTask/3600;
        minutesInt = (expectedTimeForTask - (hoursInt*3600)) / 60;
        secondsInt = expectedTimeForTask % 60;
        minus = @"";
    }
    
    hoursString = [NSString stringWithFormat:@"%d",(int)hoursInt];
    minutesString = [NSString stringWithFormat:@"%d",(int)minutesInt];
    secondsString = [NSString stringWithFormat:@"%d",(int)secondsInt];
    
    if([hoursString length]==1){
        hoursString = [NSString stringWithFormat:@"0%@",hoursString];
    }
    
    if([minutesString length]==1){
        minutesString = [NSString stringWithFormat:@"0%@",minutesString];
    }
    
    if([secondsString length]==1){
        secondsString = [NSString stringWithFormat:@"0%@",secondsString];
    }
    
//    if (isAttachedSecond) {
//        timeDisplay = [NSString stringWithFormat:@"%@%@:%@:%@",minus,hoursString,minutesString,secondsString];
//        
//    }
//    else {
//        timeDisplay = [NSString stringWithFormat:@"%@%@:%@",minus,hoursString,minutesString];
//    }
    timeDisplay = [NSString stringWithFormat:@"%@%@:%@:%@",minus,hoursString,minutesString, secondsString];
    
    return timeDisplay;
}

#pragma mark - Check saving before change tab event
-(BOOL) checkSaveDataWithTagEvent:(NSInteger) tagOfEvent 
{
    if (tagOfEvent == tagOfMessageButton) {
        return YES;
    }
    return isSaved;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if(scrollView == guestScrollView) {
        if(listGuestModels.count > 1) {
            // Switch the indicator when more than 50% of the previous/next page is visible
            CGFloat pageWidth = scrollView.frame.size.width;
            int page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
            if(page == 0) { //First Page
                [guestIndicatorLeft setHidden:YES];
                [guestIndicatorRight setHidden:NO];
            } else if(page == listGuestModels.count - 1) { //Last Page
                [guestIndicatorLeft setHidden:NO];
                [guestIndicatorRight setHidden:YES];
            } else {
                [guestIndicatorLeft setHidden:NO];
                [guestIndicatorRight setHidden:NO];
            }
        }
    }
}

#pragma mark - Private methods

//Check is valid room status or not
//If not valid, show alert message and force user to close room detail
-(BOOL)isValidRoomStatus
{
    [[RoomManagerV2 sharedRoomManager] getRoomWSByUserID:[UserManagerV2 sharedUserManager].currentUser.userId  AndRaID:roomAssignmentModel.roomAssignment_Id AndLastModified:nil shouldUpdateRoomRecord:YES];

    //[Hao Tran / 20150514] SG-71567 - Check to update room status & cleaning status from WS
    if (!roomStatusModel) {
        roomStatusModel = [[RoomStatusModelV2 alloc] init];
    }
    RoomAssignmentModelV2 *ramodel = [[RoomAssignmentModelV2 alloc] init];
    ramodel.roomAssignment_Id = roomAssignmentModel.roomAssignment_Id;
    ramodel.roomAssignment_UserId = [UserManagerV2 sharedUserManager].currentUser.userId;
    [[RoomManagerV2 sharedRoomManager] load:ramodel];
    
    if (ramodel.roomAssignmentRoomStatusId != roomAssignmentModel.roomAssignmentRoomStatusId) {
        if (ramodel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VD || ramodel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OD) {
            roomAssignmentModel.roomAssignmentRoomStatusId = ramodel.roomAssignmentRoomStatusId;
            [self setRoomStatusModel:(int)roomAssignmentModel.roomAssignmentRoomStatusId];
            [self.tbViewDetail reloadData];
        } else { //OI, VI, OC, VC, OOS ....
            [self showAlertForCloseRoomDetail];
            return NO;
        }
    }
    return YES;
}

-(void) showAlertForCloseRoomDetail
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:[L_message_deny_start_room currentKeyToLanguage] message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:[L_OK currentKeyToLanguage], nil];
    alert.tag = tagForceCloseRoomDetail;
    [alert show];
}

-(void) startAndStopInspectionRoomForSupervisor:(MBProgressHUD*) HUD
{
    if (timerStatus == TIMER_STATUS_PAUSE || timerStatus == TIMER_STATUS_NOT_START) {
        if ([[HomeViewV2 shareHomeView] isSyncing] == YES) {
            CustomAlertViewV2 *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:nil message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_please_wait_syncing_progress] delegate:nil cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_OK] otherButtonTitles:nil];
            [alert show];
            
            //set selected button in tabbar
            [[HomeViewV2 shareHomeView] setSelectedButton:tagOfHomeButton];
            return;
        }
    }
    
    switch (timerStatus) {
        case TIMER_STATUS_PAUSE:
        {
            isSaved = NO;
            [btnStartStop setBackgroundImage:[UIImage imageBeforeiOS7:imgStopBtn equaliOS7:imgStopBtnFlat] forState:UIControlStateNormal];
            [btnStartStop setBackgroundImage:[UIImage imageBeforeiOS7:imgStopBtnClicked equaliOS7:imgStopBtnClickedFlat] forState:UIControlEventTouchDown];
            
//            timeLeft = roomAssignmentModel.roomAssignmentRoomExpectedInspectionTime * 60;

            [self changeTimerStatus:TIMER_STATUS_START];
            [self setEnableBtnAtion:YES];
            [self setEnableBtnComplete:YES];
            
            roomAssignmentModel.roomAssignmentRoomInspectionStatusId = ENUM_INSPECTION_STARTED;
            
            STATUS_UPDATE_RECORD_DETAIL statusUpdateStart = [self canUpdateStartRecord];
            if (statusUpdateStart == CAN_START_RECORD_DETAIL) {
                
                if(roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VC){
                    [self setRoomStatusModel:ENUM_ROOM_STATUS_VPU];
                } else if(roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OC){
                    [self setRoomStatusModel:ENUM_ROOM_STATUS_OPU];
                    //Hao Tran Remove - New requirement, start inspecting room is OPU
                    //[self setRoomStatusModel:ENUM_ROOM_STATUS_OC];
                } else if(roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OPU){
                    [self setRoomStatusModel:ENUM_ROOM_STATUS_OPU];
                }else if(roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VPU){
                    [self setRoomStatusModel:ENUM_ROOM_STATUS_VPU];
                }
                
                [self updateStartRecord];
            }
            else {
                if (statusUpdateStart == CONTINUE_COUNT_INSPECTION) {
                    NSDateFormatter *f = [[NSDateFormatter alloc] init];
                    [f setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                    NSDate *startDate = [f dateFromString:roomRecordDetailModel.recd_Start_Date];
                    NSDate *stopDate = [f dateFromString:roomRecordDetailModel.recd_Stop_Date];
                    //reset stop date
                    roomRecordDetailModel.recd_Stop_Date = @"";
                    NSTimeInterval timeInterval = [stopDate timeIntervalSinceDate:startDate];
                    timeLeft = roomAssignmentModel.roomAssignmentRoomExpectedInspectionTime * 60 - (NSInteger) timeInterval;
                    duration = (NSInteger) timeInterval;
                    
                    //update start date by add pause time
                    NSDate *now = [NSDate date];
                    startDate = [startDate dateByAddingTimeInterval:[now timeIntervalSinceDate:stopDate]];
                    roomRecordDetailModel.recd_Start_Date = [f stringFromDate:startDate];
                }
            }
        }
            break;
            
        case TIMER_STATUS_START:
        {
            if(isAutoStartOnLoad)
            {
                return;
            }
            
            if (cleaningStatusConfirmPopup == nil) {
                cleaningStatusConfirmPopup = [[CleaningStatusConfirmPopupView alloc] initWithDelegate:self andOption:CLEANING_STATUS_CONFIRM_POPUP_STOP];
            }
            [cleaningStatusConfirmPopup setConfirmPopupType:CLEANING_STATUS_CONFIRM_POPUP_STOP];
            [cleaningStatusConfirmPopup setRoomNo:(NSString *)[roomAssignment objectForKey:kRoomNo]];
            //[self.tabBarController.view addSubview:cleaningStatusConfirmPopup];
            [self.tabBarController addSubview:cleaningStatusConfirmPopup];
            
            return;//Hao Tran - Waiting for confirm pop up
        }
            break;
            
        case TIMER_STATUS_NOT_START:
        {
            isSaved = NO;
            roomRecordModel.rrec_PostStatus = (int)POST_STATUS_SAVED_UNPOSTED;
            
            [btnStartStop setBackgroundImage:[UIImage imageBeforeiOS7:imgStopBtn equaliOS7:imgStopBtnFlat] forState:UIControlStateNormal];
            [btnStartStop setBackgroundImage:[UIImage imageBeforeiOS7:imgStopBtnClicked equaliOS7:imgStopBtnClickedFlat] forState:UIControlEventTouchDown];
            
            [self changeTimerStatus:TIMER_STATUS_START];
            [self setEnableBtnAtion:YES];
            [self setEnableBtnComplete:YES];
            
            [CommonVariable sharedCommonVariable].roomIsCompleted = 0;
            NSDateFormatter *timeFomarter = [[NSDateFormatter alloc] init];
            [timeFomarter setDateFormat:[ehkConvert getyyyy_MM_dd_HH_mm_ss]];
            
            //update start time if we don't have or supervisor come back this Room Assignment and touch start button
            if ([roomRecordModel.rrec_Inspection_Start_Time isEqualToString:@"1900-01-01 00:00:00"]
                ||(![roomRecordModel.rrec_Inspection_Start_Time isEqualToString:@"1900-01-01 00:00:00"]
                   && roomRecordModel.rrec_Total_Inspected_Time == 0))
            {
                NSDate *now = [NSDate date];
                roomRecordModel.rrec_Inspection_Start_Time = [timeFomarter stringFromDate:now];
            }
            
            //update local database of RoomRecord
            if(roomRecordModel.rrec_Id <= 0) {
                [[RoomManagerV2 sharedRoomManager] insertRoomRecordData:roomRecordModel];
            } else {
                [[RoomManagerV2 sharedRoomManager] updateRoomRecordData:roomRecordModel];
            }
            
            roomAssignmentModel.roomAssignmentRoomInspectionStatusId = ENUM_INSPECTION_STARTED;
            
            if(roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VC){
                [self setRoomStatusModel:ENUM_ROOM_STATUS_VPU];
            } else if(roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OC){
                [self setRoomStatusModel:ENUM_ROOM_STATUS_OPU];
            } else if(roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OPU){
                [self setRoomStatusModel:ENUM_ROOM_STATUS_OPU];
            }else if(roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VPU){
                [self setRoomStatusModel:ENUM_ROOM_STATUS_VPU];
            }
            
            if ([self canUpdateStartRecord] == CAN_START_RECORD_DETAIL
                //inspector come back to inspect again after stopped inspection
                || roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VPU)
            {
                [self updateStartRecord];
            }
        }
            
            break;
            
        default:
            break;
    }
    
    //Hao Tran - avoid clone code
    int responseStatus = [self postCurrentInpsection];
    //Show Invalid room number
    if(responseStatus == RESPONSE_STATUS_INVALID_ROOM_STATUS) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:[L_invalid_room_status currentKeyToLanguage] message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:[L_OK currentKeyToLanguage], nil];
        [alert show];
        return;
    }
    
    [self checkEnableButtonAction];
}

-(void) startAndStopCleaningRoomForRA:(MBProgressHUD *)HUD
{
    if (timerStatus == TIMER_STATUS_START) {
        if(isAutoStartOnLoad)
        {
            [HUD hide:YES];
            [HUD removeFromSuperview];
            return;
        }
        
        if (cleaningStatusConfirmPopup == nil) {
            cleaningStatusConfirmPopup = [[CleaningStatusConfirmPopupView alloc] initWithDelegate:self andOption:CLEANING_STATUS_CONFIRM_POPUP_STOP];
        }
        [cleaningStatusConfirmPopup setConfirmPopupType:CLEANING_STATUS_CONFIRM_POPUP_STOP];
        [cleaningStatusConfirmPopup setRoomNo:(NSString *)[roomAssignment objectForKey:kRoomNo]];
        [self.tabBarController addSubview:cleaningStatusConfirmPopup];
    }
    else if (timerStatus == TIMER_STATUS_PAUSE) {
        if (!isDemoMode) {
            //Check is valid room status or not
            //If not valid, show alert message and force user to close room detail
            BOOL isvalidRoomStatus = [self isValidRoomStatus];
            if (!isvalidRoomStatus) {
                [HUD hide:YES];
                [HUD removeFromSuperview];
                return;
            }
        }

        if (timeLeft < 0) {
            [btnTimer setBackgroundImage:[UIImage imageBeforeiOS7:imgTimerOver equaliOS7:imgTimerOverFlat] forState:UIControlStateNormal];
        }
        else {
            [btnTimer setBackgroundImage:[UIImage imageBeforeiOS7:imgTimerPlay equaliOS7:imgTimerPlayFlat] forState:UIControlStateNormal];
        }
        if ([CommonVariable sharedCommonVariable].currentPauseRoom == noPauseRoom || [CommonVariable sharedCommonVariable].currentPauseRoom == roomAssignmentModel.roomAssignment_Id) {
            [self setEnableBtnStartStop:YES];
        }
        else {
            [self setEnableBtnStartStop:NO];
        }
        
        [btnStartStop setBackgroundImage:[UIImage imageBeforeiOS7:imgStopBtn equaliOS7:imgStopBtnFlat] forState:UIControlStateNormal];
        [btnStartStop setBackgroundImage:[UIImage imageBeforeiOS7:imgStopBtnClicked equaliOS7:imgStopBtnClickedFlat] forState:UIControlEventTouchDown];
        
        [self changeTimerStatus:TIMER_STATUS_START];
        
        [self setEnableBtnAtion:YES];
        [self setEnableBtnComplete:YES];
        [self setEnableBtnCleaningStatus:YES];
        if (roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VD) {
            [self setEnableBtnCleaningStatus:NO];
        }
        
        isSaved = NO;
        isStopButtonClicked = NO;
        
        //        NSLog(@"isMustResetTimer %d" ,isMustResetTimer);
        //        if(isMustResetTimer){
        //            isMustResetTimer = NO;
        //            durationTime = 0;
        //            timeLeftCleaningStatus = raDataModel.roomAssignmentRoomExpectedCleaningTime*60;
        //        }
        
        NSDateFormatter *timeFormat = [[NSDateFormatter alloc] init];
        [timeFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        NSDate *now = [NSDate date];
        //        NSTimeInterval timeDiff = [now timeIntervalSinceDate:[timeFormat dateFromString:roomRecordDetailModel.recd_Stop_Date]];
        roomAssignmentModel.roomAssignmentRoomCleaningStatusId = ENUM_CLEANING_STATUS_START;
        
        //Set cleaning pause time to default when start room
        roomRecordModel.rrec_cleaning_pause_time = @"1900-01-01 00:00:00";
        roomRecordModel.rrec_Cleaning_End_Time = @"1900-01-01 00:00:00";
        roomRecordModel.rrec_Cleaning_Start_Time = [timeFormat stringFromDate:now];
        
        if(roomRecordModel.rrec_Id <= 0) {
            [[RoomManagerV2 sharedRoomManager] insertRoomRecordData:roomRecordModel];
        } else {
            [[RoomManagerV2 sharedRoomManager] updateRoomRecordData:roomRecordModel];
        }
        
        //Hao Tran [20141121/Fix start time null] - END
        
        int responseStatus = (int)[self updateRoomStatusToWS];
        if(responseStatus == RESPONSE_STATUS_ROOM_HAVE_BEEN_DELETED){
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:[L_MSG_ROOM_REMOVE currentKeyToLanguage]/*@"Error - This room is removed"*/ message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:[L_OK currentKeyToLanguage] /*@"OK"*/, nil];
            [alert show];
            [self changeTimerStatus:TIMER_STATUS_PAUSE];
            isSaved = YES;
            isStopButtonClicked = YES;
            
            [HUD hide:YES];
            [HUD removeFromSuperview];
            return;
            
        } else if(responseStatus == RESPONSE_STATUS_INVALID_ROOM_STATUS) {
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:[L_invalid_room_status currentKeyToLanguage] message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:[L_OK currentKeyToLanguage], nil];
            [alert show];
            [HUD hide:YES];
            [HUD removeFromSuperview];
            return;
        }
        
        //timeLeft = roomAssignmentModel.roomAssignmentRoomExpectedCleaningTime*60;
        
        if (!roomRecordDetailModel) {
            roomRecordDetailModel = [[RoomRecordDetailModelV2 alloc] init];
        }
        
        roomRecordDetailModel.recd_Ra_Id = [TasksManagerV2 getCurrentRoomAssignment];
        roomRecordDetailModel.recd_User_Id = [UserManagerV2 sharedUserManager].currentUser.userId;
        roomRecordDetailModel.recd_Start_Date = [timeFormat stringFromDate:now];
//        [[RoomManagerV2 sharedRoomManager] insertRoomRecordDetailData:roomRecordDetailModel];
        ((CleaningTimeManager*)[CleaningTimeManager sharedManager]).roomRecordDetailModel = roomRecordDetailModel;
        
        //HaoTran[20130529/Remove] - Change requirement, remove to match with Android
        //roomRecordModel.rrec_Cleaning_Start_Time = [timeFormat stringFromDate:now];
        //HaoTran[20130529/Remove] - END
        
        roomRecordModel.rrec_PostStatus = (int)POST_STATUS_SAVED_UNPOSTED;
        
        int extraDuration = (int)duration + roomRecordModel.rrec_Total_Cleaning_Time;
        roomRecordModel.rrec_Total_Cleaning_Time = extraDuration;
        //[[RoomManagerV2 sharedRoomManager] updateRoomRecordData:roomRecord];
        
        //update local database of RoomRecord
        if(roomRecordModel.rrec_Id <= 0) {
            [[RoomManagerV2 sharedRoomManager] insertRoomRecordData:roomRecordModel];
        } else {
            [[RoomManagerV2 sharedRoomManager] updateRoomRecordData:roomRecordModel];
        }
        roomAssignmentModel.roomAssignment_PostStatus = (int)POST_STATUS_SAVED_UNPOSTED;
        [[RoomManagerV2 sharedRoomManager] update:roomAssignmentModel];
        roomModel.room_PostStatus = (int)POST_STATUS_SAVED_UNPOSTED;
        [[RoomManagerV2 sharedRoomManager] updateRoomModel:roomModel];
        
        //            duration = 0;
    }
    else {
        if (!isDemoMode) {
            //Check is valid room status or not
            //If not valid, show alert message and force user to close room detail
            BOOL isvalidRoomStatus = [self isValidRoomStatus];
            if (!isvalidRoomStatus) {
                [HUD hide:YES];
                [HUD removeFromSuperview];
                return;
            }
        }
        [self setEnableBtnAtion:YES];
        [self setEnableBtnComplete:YES];
        [self setEnableBtnCleaningStatus:YES];
        if (roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VD) {
            [self setEnableBtnCleaningStatus:NO];
        }
        
        if (timeLeft < 0) {
            [btnTimer setBackgroundImage:[UIImage imageBeforeiOS7:imgTimerOver equaliOS7:imgTimerOverFlat] forState:UIControlStateNormal];
        }
        else {
            [btnTimer setBackgroundImage:[UIImage imageBeforeiOS7:imgTimerPlay equaliOS7:imgTimerPlayFlat] forState:UIControlStateNormal];
        }
        
        [btnStartStop setBackgroundImage:[UIImage imageBeforeiOS7:imgStopBtn equaliOS7:imgStopBtnFlat] forState:UIControlStateNormal];
        [btnStartStop setBackgroundImage:[UIImage imageBeforeiOS7:imgStopBtnClicked equaliOS7:imgStopBtnClickedFlat] forState:UIControlEventTouchDown];
        if ([CommonVariable sharedCommonVariable].currentPauseRoom == noPauseRoom || [CommonVariable sharedCommonVariable].currentPauseRoom == roomAssignmentModel.roomAssignment_Id) {
            [self setEnableBtnStartStop:YES];
        }
        else {
            [self setEnableBtnStartStop:NO];
        }
        
        [self changeTimerStatus:TIMER_STATUS_START];
        
        isSaved = NO;
        
        roomAssignmentModel.roomAssignmentRoomCleaningStatusId = ENUM_CLEANING_STATUS_START;
        roomAssignmentModel.roomAssignment_PostStatus = (int)POST_STATUS_SAVED_UNPOSTED;
        
        //Set cleaning pause time to default when start room
        roomRecordModel.rrec_cleaning_pause_time = @"1900-01-01 00:00:00";
        roomRecordModel.rrec_Cleaning_End_Time = @"1900-01-01 00:00:00";
        
        NSDateFormatter *timeFormat = [[NSDateFormatter alloc] init];
        [timeFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *now = [NSDate date];
        roomRecordModel.rrec_Cleaning_Start_Time = [timeFormat stringFromDate:now];
        
        if(roomRecordModel.rrec_Id <= 0) {
            [[RoomManagerV2 sharedRoomManager] insertRoomRecordData:roomRecordModel];
        } else {
            [[RoomManagerV2 sharedRoomManager] updateRoomRecordData:roomRecordModel];
        }
        
        int responseStatus = (int)[self updateRoomStatusToWS];
        if(responseStatus == RESPONSE_STATUS_ROOM_HAVE_BEEN_DELETED) {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:[L_MSG_ROOM_REMOVE currentKeyToLanguage]/*@"Error - This room is removed"*/ message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:[L_OK currentKeyToLanguage], nil];
            [alert show];
            [self changeTimerStatus:TIMER_STATUS_PAUSE];
            isSaved = YES;
            isStopButtonClicked = YES;
            [HUD hide:YES];
            [HUD removeFromSuperview];
            return;
            
        } else if(responseStatus == RESPONSE_STATUS_INVALID_ROOM_STATUS) {
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:[L_invalid_room_status currentKeyToLanguage] message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:[L_OK currentKeyToLanguage], nil];
            [alert show];
            [HUD hide:YES];
            [HUD removeFromSuperview];
            return;
        }
        
        [CommonVariable sharedCommonVariable].roomIsCompleted = 0;
        
        /*
         //Hao Tran - Fixed don't have cleaning start time
         if(roomRecordModel.rrec_Cleaning_Start_Time.length <= 0) {
         //if(![roomRecordModel.rrec_Cleaning_Start_Time isEqualToString:@""]){
         roomRecordModel.rrec_Cleaning_Start_Time = [timeFormat stringFromDate:now];
         
         //update local database of RoomRecord
         RoomManagerV2 *roomManager = [[RoomManagerV2 alloc] init];
         [roomManager updateRoomRecordData:roomRecordModel];
         }*/
        
        roomRecordDetailModel.recd_Ra_Id = [TasksManagerV2 getCurrentRoomAssignment];
        roomRecordDetailModel.recd_User_Id = [UserManagerV2 sharedUserManager].currentUser.userId;
        roomRecordDetailModel.recd_Start_Date = [timeFormat stringFromDate:now];
        
        roomAssignmentModel.roomAssignment_PostStatus = (int)POST_STATUS_SAVED_UNPOSTED;
        [[RoomManagerV2 sharedRoomManager] update:roomAssignmentModel];
        roomModel.room_PostStatus = (int)POST_STATUS_SAVED_UNPOSTED;
        [[RoomManagerV2 sharedRoomManager] updateRoomModel:roomModel];
    }
    
    [self checkEnableButtonAction];
    dispatch_async(dispatch_get_main_queue(), ^{
        [HUD hide:YES];
        [HUD removeFromSuperview];
    });
}

-(void) checkEnableButtonAction
{
    if (!(actionLostAndFound.isActive || actionEngineering.isActive || actionLaundry.isActive || actionMinibar.isActive || actionLinen.isActive || actionAmenities.isActive || checklist.isActive || guideline.isActive))
    {
        [self setEnableBtnAtion:NO];
    }
}
- (int)updateRoomStatus{
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getMsgSaveMessage]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES];
    
    //NSInteger result = [[RoomManagerV2 sharedRoomManager] updateRoomHaveBeenRemove:[UserManagerV2 sharedUserManager].currentUser.userId AndRaModel:roomAssignmentModel];
    
    int result = RESPONSE_STATUS_ERROR;
    result = [[RoomManagerV2 sharedRoomManager] updateRoomStatusByUserId:(int)userId roomNo:roomAssignmentModel.roomAssignment_RoomId statusId:roomStatusModel.rstat_Id cleaningStatusId:0];
    dispatch_async(dispatch_get_main_queue(), ^{
        [HUD hide:YES];
        [HUD removeFromSuperview];
    });
    
    
    return result;
}
-(NSInteger) updateRoomStatusToWS
{
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getMsgSaveMessage]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES];
    
	//NSInteger result = [[RoomManagerV2 sharedRoomManager] updateRoomHaveBeenRemove:[UserManagerV2 sharedUserManager].currentUser.userId AndRaModel:roomAssignmentModel];
    
    int result = RESPONSE_STATUS_ERROR;
    
    //Post previous all unpost room before post this current room
    SyncManagerV2 *syncManager = [[SyncManagerV2 alloc] init];
    [syncManager postAllUnpostRoomRecordHistoryWithUserId:userId AndPercentView:nil];
    
    if ([roomAssignmentModel roomAssignmentRoomCleaningStatusId] == ENUM_CLEANING_STATUS_START) {
        
        //MC-71484: Check to post log previous start room
        if ([CommonVariable sharedCommonVariable].prevStartedRoomID != 0 && ![[CommonVariable sharedCommonVariable].prevStartedRoomNo isEqualToString:@""]) {
            NSString *prevRoomId = [NSString stringWithFormat:@"%d",[CommonVariable sharedCommonVariable].prevStartedRoomID];
            NSString *prevRoomNo = [CommonVariable sharedCommonVariable].prevStartedRoomNo;
            NSString *userName = [[[UserManagerV2 sharedUserManager] currentUser] userName];
            NSString *newRoomId = [NSString stringWithFormat:@"%d",[roomAssignmentModel roomAssignment_Id]];
            NSString *newRoomNo = [roomAssignmentModel roomAssignment_RoomId];
    
            [[RoomManagerV2 sharedRoomManager] postLogInvalidStartRoomWithUserID:userId userName:userName prevRoomId:prevRoomId prevRoomNo:prevRoomNo newRoomId:newRoomId newRoomNo:newRoomNo];
        }
    }
    //HaoTran[20130529/Fix update RoomAssignment] - Update room assignment record on server side
    roomAssignmentModel.roomAssignment_PostStatus = (int)POST_STATUS_SAVED_UNPOSTED;
    result = [[RoomManagerV2 sharedRoomManager] postRoomAssignmentWSByUserID:userId AndRaModel:roomAssignmentModel];
    //HaoTran[20130529/Fix update RoomAssignment] - END
    
    //Hao Tran - Remove for redundant update cleaning status
    //[[RoomManagerV2 sharedRoomManager] updateCleaningStatusWSByUserId:userId withRoomAssignmentModel:roomAssignmentModel];
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [HUD hide:YES];
        [HUD removeFromSuperview];
    });
    
    
    return result;
}

-(NSString *) getBackString {
    NSDictionary *dic = [[LanguageManagerV2 sharedLanguageManager] getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_BACK]];
    return s;
}

-(UIColor *) colorForLabel:(enum ENUM_ROOM_STATUS) roomStatus{
    switch (roomStatus) {
        case ENUM_ROOM_STATUS_VD:
        case ENUM_ROOM_STATUS_OD:
            return [UIColor redColor];
            
        case ENUM_ROOM_STATUS_VPU:
        case ENUM_ROOM_STATUS_OPU:
            return [UIColor yellowColor];
            
        case ENUM_ROOM_STATUS_OOO:
        case ENUM_ROOM_STATUS_OOS:
            return [UIColor grayColor];
            
        case ENUM_ROOM_STATUS_VC:
        case ENUM_ROOM_STATUS_OC:
        case ENUM_ROOM_STATUS_OC_SO:
            return [UIColor colorWithRed:0.0f green:1.0f blue:1.0f alpha:1.0f];
            
        case ENUM_ROOM_STATUS_VI:
        case ENUM_ROOM_STATUS_OI:
            return [UIColor greenColor];
            
        default:
            return [UIColor blackColor];
    }
}

-(void) setRoomStatusModel:(enum ENUM_ROOM_STATUS) roomStatus
{
//    roomStatusId = roomStatus;
    roomAssignmentModel.roomAssignmentRoomStatusId = roomStatus;
    roomStatusModel.rstat_Id = roomStatus;
    [[RoomManagerV2 sharedRoomManager] loadRoomStatusData:roomStatusModel];
    
    //update records room
    [[RoomManagerV2 sharedRoomManager] update:roomAssignmentModel];
    [[RoomManagerV2 sharedRoomManager] updateRoomStatusData:roomStatusModel];
    
    //[roomAssignment setValue:roomStatusModel.rstat_Code forKey:kRMStatus];
    [roomAssignment setValue:roomStatusModel.rstat_Name forKey:kRMStatus];
    [roomAssignment setValue:roomStatusModel.rstat_Image forKey:kRoomStatusImage];
    
    //HaoTran[20130604/Fix Update Data] - update room status data in previous view
    [self updateRoomStatusData];
    //HaoTran[20130604/Fix Update Data] - END
    
    //[lblRoomStatus setText:roomStatusModel.rstat_Code];
    [lblRoomStatus setText:roomStatusModel.rstat_Name];
    [lblRoomStatus setTextColor:[self colorForLabel:roomStatus]];
    if (roomStatusModel.rstat_Image != nil) {
        [imgRoomStatus setImage:[UIImage imageWithData:roomStatusModel.rstat_Image]];
    }
    
    if([self sectionIndexRoomStatus] != NSNotFound)
    {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[self rowIndexRoomStatus] inSection:[self sectionIndexRoomStatus]];
        UITableViewCell *roomStatusCell = [self.tbViewDetail cellForRowAtIndexPath:indexPath];
        
        UILabel *lblRoomStatusCell = (UILabel *)[roomStatusCell viewWithTag:tagSecondContent];
        UIImageView *imgRoomStatusIconCell = (UIImageView *)[roomStatusCell viewWithTag:tagFirstContent];
        
        
        //[lblRoomStatusCell setText:roomStatusModel.rstat_Code];
        [lblRoomStatusCell setText:roomStatusModel.rstat_Name];
        [lblRoomStatusCell setTextColor:[self colorForLabel:roomStatus]];
        if (roomStatusModel.rstat_Image != nil) {
            [imgRoomStatusIconCell setImage:[UIImage imageWithData:roomStatusModel.rstat_Image]];
        }
    }
}

-(BOOL)isMaxRoomRecordDetail{
    RoomRecordDetailModelV2 *roomRecordDetail = [[RoomRecordDetailModelV2 alloc] init];
    roomRecordDetail.recd_Ra_Id = [TasksManagerV2 getCurrentRoomAssignment];
    roomRecordDetail.recd_User_Id = [UserManagerV2 sharedUserManager].currentUser.userId;
    [[RoomManagerV2 sharedRoomManager] loadRoomRecordDataDetailByRoomAssignmentIdAndUserId:roomRecordDetail];
    if([[RoomManagerV2 sharedRoomManager] numberOfRoomRecordDetails:roomRecordDetail] >=MAX_ROOM_RECORD_DETAIL - 1){
        return YES;
    } else{
        return NO;
    }
}

//Hao Tran - Remove for unused function
/*
-(BOOL) isNotSavedAndExit
{
    if(isSaved){
        
        if(roomAssignmentModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_COMPLETED
           || roomAssignmentModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_PAUSE){
            // when not post to server
            if(roomAssignmentModel.roomAssignment_PostStatus == POST_STATUS_SAVED_UNPOSTED){
                UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ALERT_SAVEROOMDETAIL] delegate:nil cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_OK] otherButtonTitles:nil];
                [alert show];
                
                return YES;
                
            }
            
        }        
        return NO;
    } else{
        if(roomAssignmentModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_SERVICE_LATER ||
           roomAssignmentModel.roomAssignmentRoomCleaningStatusId == ENUM_CLEANING_STATUS_DECLINE_SERVICE){
            UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ALERT_SAVEROOMDETAIL] delegate:nil cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_OK] otherButtonTitles:nil];
            [alert show];
            return YES;
            
        }
        
        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObject:[UIImage imageNamed:@"yes_bt.png"]] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_complete_room_to_leave] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getOK] otherButtonTitles:nil, nil]; 
        alert.delegate = self;
        alert.tag = 10000;
        [alert show];
        return YES;
        
    }
}*/

-(void) setEnableBtnStartStop:(BOOL) isEnable
{
    [btnStartStop setUserInteractionEnabled:isEnable];
    if (timerStatus == TIMER_STATUS_START) {
        [btnStartStop setBackgroundImage:[UIImage imageBeforeiOS7:(isEnable?imgStartBtn:imgStartBtnDisable) equaliOS7:(isEnable?imgStartBtnFlat:imgStartBtnDisableFlat)]
                                forState:UIControlStateNormal];
        [btnStartStop setBackgroundImage:[UIImage imageBeforeiOS7:(isEnable?imgStartBtnClicked:imgStartBtnDisable) equaliOS7:(isEnable?imgStartBtnClickedFlat:imgStartBtnDisableFlat)] forState:UIControlEventTouchDown];
    }
    else {
        [btnStartStop setBackgroundImage:[UIImage imageBeforeiOS7:(isEnable?imgStopBtn:imgStartBtnDisable) equaliOS7:(isEnable?imgStopBtnFlat:imgStartBtnDisableFlat)]
                                forState:UIControlStateNormal];
        [btnStartStop setBackgroundImage:[UIImage imageBeforeiOS7:(isEnable?imgStopBtnClicked:imgStartBtnDisable) equaliOS7:(isEnable?imgStopBtnClickedFlat:imgStartBtnDisableFlat)]
                                forState:UIControlEventTouchDown];
    }
}

-(void) setEnableBtnAtion:(BOOL) isEnable
{
    [btnAction setUserInteractionEnabled:isEnable];
    [btnAction setBackgroundImage:[UIImage imageBeforeiOS7:(isEnable?imgCleaningStatusBtnAndActionBtn:imgCleaningStatusBtnAndActionBtnDisable)
                                                 equaliOS7:(isEnable?imgCleaningStatusBtnAndActionBtnFlat:imgCleaningStatusBtnAndActionBtnDisableFlat)]
                         forState:UIControlStateNormal];
    [btnAction setBackgroundImage:[UIImage imageBeforeiOS7:(isEnable?imgCleaningStatusBtnAndActionBtnClicked:imgCleaningStatusBtnAndActionBtnDisable)
                                                 equaliOS7:(isEnable?imgCleaningStatusBtnAndActionBtnClickedFlat:imgCleaningStatusBtnAndActionBtnDisableFlat)]
                         forState:UIControlEventTouchDown];
    [btnAction setTitle:[L_BTN_ACTION currentKeyToLanguage] forState:UIControlStateNormal];
}

-(void) setEnableBtnCleaningStatus:(BOOL) isEnable
{
    [btnCleaningStatus setUserInteractionEnabled:isEnable];
    [btnCleaningStatus setBackgroundImage:[UIImage imageBeforeiOS7:(isEnable?imgCleaningStatusBtnAndActionBtn:imgCleaningStatusBtnAndActionBtnDisable)
                                                         equaliOS7:(isEnable?imgCleaningStatusBtnAndActionBtnFlat:imgCleaningStatusBtnAndActionBtnDisableFlat)]
                                 forState:UIControlStateNormal];
    [btnCleaningStatus setBackgroundImage:[UIImage imageBeforeiOS7:(isEnable?imgCleaningStatusBtnAndActionBtnClicked:imgCleaningStatusBtnAndActionBtnDisable)
                                                         equaliOS7:(isEnable?imgCleaningStatusBtnAndActionBtnClickedFlat:imgCleaningStatusBtnAndActionBtnDisableFlat)]
                                 forState:UIControlEventTouchDown];
    [btnCleaningStatus setTitle:[L_CLEANING_STATUS currentKeyToLanguage] forState:UIControlStateNormal];
}

-(void) setEnableBtnComplete:(BOOL) isEnable
{
    [btnComplete setUserInteractionEnabled:isEnable];
    [btnComplete setBackgroundImage:[UIImage imageBeforeiOS7:(isEnable?imgCompleteBtn:imgCompleteBtnDisable) equaliOS7:(isEnable?imgCompleteBtnFlat:imgCompleteBtnDisableFlat)] forState:UIControlStateNormal];
    [btnComplete setBackgroundImage:[UIImage imageBeforeiOS7:(isEnable?imgCompleteBtnClicked:imgCompleteBtnDisable) equaliOS7:(isEnable?imgCompleteBtnClickedFlat:imgCompleteBtnDisableFlat)] forState:UIControlEventTouchDown];
}

-(void) setEnableBtnReassign:(BOOL) isEnable
{
    [btnReassign setUserInteractionEnabled:isEnable];
    [btnReassign setBackgroundImage:[UIImage imageBeforeiOS7:(isEnable?imgReassignBtn:imgReassignBtnDisable) equaliOS7:(isEnable?imgReassignBtnFlat:imgReassignBtnDisableFlat)] forState:UIControlStateNormal];
    [btnReassign setBackgroundImage:[UIImage imageBeforeiOS7:(isEnable?imgReassignBtnClicked:imgReassignBtnDisable) equaliOS7:(isEnable?imgReassignBtnClickedFlat:imgReassignBtnDisableFlat)] forState:UIControlEventTouchDown];
}

-(void) setEnableBtnChecklist:(BOOL) isEnable
{
    [btnChecklist setUserInteractionEnabled:isEnable];
    [btnChecklist setBackgroundImage:[UIImage imageBeforeiOS7:(isEnable?imgCleaningStatusBtnAndActionBtn:imgCleaningStatusBtnAndActionBtnDisable) equaliOS7:(isEnable?imgCleaningStatusBtnAndActionBtnFlat:imgCleaningStatusBtnAndActionBtnDisableFlat)] forState:UIControlStateNormal];
    [btnChecklist setBackgroundImage:[UIImage imageBeforeiOS7:(isEnable?imgCleaningStatusBtnAndActionBtnClicked:imgCleaningStatusBtnAndActionBtnDisable) equaliOS7:(isEnable?imgCleaningStatusBtnAndActionBtnClickedFlat:imgCleaningStatusBtnAndActionBtnDisableFlat)] forState:UIControlEventTouchDown];
}

-(void) removeCleaningStatusConfirmPopupFromSuperview
{
    [cleaningStatusConfirmPopup removeFromSuperview];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) loadAccessRights
{
    AccessRightModel *currentUserAccessRight = [[UserManagerV2 sharedUserManager] currentUserAccessRight];
    checklist = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.actionCheckList];
    guideline = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.actionGuideLine];
    actionLostAndFound = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.actionLostAndFound];
    actionLaundry = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.actionLaundry];
    actionMinibar = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.actionMinibar];
    actionLinen = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.actionLinen];
    actionAmenities = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.actionAmenities];
    actionEngineering = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.actionEngineering];
    manualUpdateRoomStatus = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.manualUpdateRoomStatus];
}

-(void) loadGuestData
{
    NSMutableArray *listCurrentGuests = [[RoomManagerV2 sharedRoomManager] loadGuestsByUserId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId roomNumber:roomModel.room_Id guestType:GuestTypeCurrent];
    NSMutableArray *listArrivalGuests = [[RoomManagerV2 sharedRoomManager] loadGuestsByUserId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId roomNumber:roomModel.room_Id guestType:GuestTypeArrival];
    
    listGuestModels = [NSMutableArray array];
    [listGuestModels addObjectsFromArray:listCurrentGuests];
    [listGuestModels addObjectsFromArray:listArrivalGuests];
}

#pragma mark - Private methods for supervisor

-(void)updateRoomStatusData
{
    //HaoTran[20130604/Fix Update Room Status]
    //Update room status after changed in current view for FindPendingViewV2.
    //Because it didn't update Room Status in list room FindPendingViewV2 when navigation back
    if(isPushFrom == IS_PUSHED_FROM_FIND_BY_RA)
    {
        NSArray *arrayVC = [self.navigationController viewControllers];
        for (UIViewController *currentVC in arrayVC) {
            if([currentVC isKindOfClass:[FindPendingViewV2 class]]){
                FindPendingViewV2 *pendingViews = (FindPendingViewV2*)currentVC;
                if(pendingViews.selectedPendingRoom != nil){
                    
                    //Change room status in FindPendingViewV2
                    NSDictionary *roomData = [pendingViews.selectedPendingRoom objectForKey:keyRoomData];
                    [roomData setValue:roomStatusModel.rstat_Code forKey:kRMStatus];
                    
                    //Change room Status Id in FindPendingViewV2
                    RoomAssignmentModelV2 *roomAssignmenFindPendingView = [pendingViews.selectedPendingRoom objectForKey:keyRoomAssignmentModel];
                     roomAssignmenFindPendingView.roomAssignmentRoomStatusId = roomAssignmentModel.roomAssignmentRoomStatusId;
                }
                break;
            }
        }
    }
    //HaoTran[20130604/Fix Update Room Status] - END
}

-(STATUS_UPDATE_RECORD_DETAIL)canUpdateStartRecord
{
    //init room record detail if needed
    if (roomRecordDetailModel == nil) {
        roomRecordDetailModel = [[RoomRecordDetailModelV2 alloc] init];
        roomRecordDetailModel.recd_Ra_Id = roomAssignmentModel.roomAssignment_Id;
        roomRecordDetailModel.recd_User_Id = roomAssignmentModel.roomAssignment_UserId;
        return CAN_START_RECORD_DETAIL;
    }
    
    //current number of record details in current room assignment
    NSMutableArray *recordDetails = [[RoomManagerV2 sharedRoomManager] loadAllRoomRecordDetailsByRoomAssignmentId:roomAssignmentModel.roomAssignment_Id andUserId:roomAssignmentModel.roomAssignment_UserId];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    
    if (recordDetails.count > MAX_ROOM_RECORD_DETAIL) {
        return CAN_NOT_START_RECORD_DETAIL;
    }
    
    //check last record did stopped over 1 minute?
    if (recordDetails.count > 0) {
        RoomRecordDetailModelV2 *lastRecordDetail = [recordDetails objectAtIndex:recordDetails.count - 1];
        
        NSDate *startDate = [formatter dateFromString:lastRecordDetail.recd_Start_Date];
        NSDate *stopDate = [formatter dateFromString:lastRecordDetail.recd_Stop_Date];
        NSDate *now = [NSDate date];
        NSTimeInterval timeInterval = [now timeIntervalSinceDate:stopDate];
        if (timeInterval > 60) {
            return CAN_START_RECORD_DETAIL;
        } else {
            //delete last record
            [[RoomManagerV2 sharedRoomManager] deleteRoomRecordDetailData:lastRecordDetail];
            
            //subtract duration in room record
            NSInteger durationTime = roomRecordModel.rrec_Total_Inspected_Time;
            durationTime -= (NSInteger)[stopDate timeIntervalSinceDate:startDate];
            roomRecordModel.rrec_Total_Inspected_Time = (int)durationTime;
            [[RoomManagerV2 sharedRoomManager] updateRoomRecordData:roomRecordModel];
            
            return CONTINUE_COUNT_INSPECTION;
        }
    }

    if (roomRecordDetailModel.recd_Stop_Date != nil) {
        
        NSDate *stopDate = [formatter dateFromString:roomRecordDetailModel.recd_Stop_Date];
        if ([stopDate compare:[NSDate dateWithTimeIntervalSinceNow: - INTERVAL_START_STOP_RECORD_DETAIL]] == NSOrderedDescending) {
            return CAN_START_RECORD_DETAIL;
        }
    }
    
    if (recordDetails.count == 0) {
        return CAN_START_RECORD_DETAIL;
    }
    
    return CAN_NOT_START_RECORD_DETAIL;
}

-(void)updateStartRecord {
    // update start record detail, clear current stop record detail
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    roomRecordDetailModel.recd_Id = 0;
    roomRecordDetailModel.recd_Start_Date = [formatter stringFromDate:[NSDate date]];
    roomRecordDetailModel.recd_Stop_Date = @"";
}

-(STATUS_UPDATE_RECORD_DETAIL)canUpdateStopRecord {
    //check can update stop record
    
    //current number of record details in current room assignment
    NSMutableArray *recordDetails = [[RoomManagerV2 sharedRoomManager] loadAllRoomRecordDetailsByRoomAssignmentId:roomAssignmentModel.roomAssignment_Id andUserId:roomAssignmentModel.roomAssignment_UserId];
    
    if (recordDetails.count == MAX_ROOM_RECORD_DETAIL - 1) {
        //check input room status
        enum ENUM_ROOM_STATUS roomStatus = (int)roomAssignmentModel.roomAssignmentRoomStatusId;
        
        if (roomStatus != ENUM_ROOM_STATUS_OI && roomStatus != ENUM_ROOM_STATUS_VI && roomStatus != ENUM_ROOM_STATUS_OD && roomStatus != ENUM_ROOM_STATUS_VD && roomStatus != ENUM_ROOM_STATUS_OOO && roomStatus != ENUM_ROOM_STATUS_OOS) {
            //check input checklist
            CheckListManagerV2 *chkManager = [[CheckListManagerV2 alloc] init];
            if ([chkManager checkChecklistStatusWithRoomAssigmentId:roomAssignmentModel.roomAssignment_Id] == NO) {
                //show alert and return
                CustomAlertViewV2 *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:nil message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_room_detail_input_checklist_room_status] delegate:nil cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_OK] otherButtonTitles:nil];
                [alert show];
                
                return CAN_NOT_START_RECORD_DETAIL;
            }
        }
    }
    
    return CAN_START_RECORD_DETAIL;
}

-(void)updateStopRecord {
    //set stop record detail
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *now = [NSDate date];
    roomRecordDetailModel.recd_Stop_Date = [formatter stringFromDate:now];
    
    //update record detail to db
    if (roomRecordDetailModel.recd_Id != 0) {
        [[RoomManagerV2 sharedRoomManager] updateRoomRecordDetailData:roomRecordDetailModel];
    } else {
        [[RoomManagerV2 sharedRoomManager] insertRoomRecordDetailData:roomRecordDetailModel];
    }
    //Hao Tran - Remove for update total time
    /*
     //update record duration inspection
     NSDate *startDate = [formatter dateFromString:roomRecordDetailModel.recd_Start_Date];
     NSDate *stopDate = [formatter dateFromString:roomRecordDetailModel.recd_Stop_Date];
     NSTimeInterval timeDifference = [stopDate timeIntervalSinceDate:startDate];
     NSInteger durationTime = timeDifference + roomRecordModel.rrec_Total_Inspected_Time;
     roomRecordModel.rrec_Total_Inspected_Time = durationTime;
     */

    //update record duration inspection
    roomRecordModel.rrec_Inspection_Stop_Time =  [formatter stringFromDate:now]; //save stoop time
    roomRecordModel.rrec_PostStatus = (int)POST_STATUS_SAVED_UNPOSTED;
    roomRecordModel.rrec_Inspection_End_Time =  @"1900-01-01 00:00:00";
    NSInteger dur = duration - roomRecordModel.rrec_Total_Inspected_Time;
    roomRecordModel.rrec_Last_Inspected_Duration = [NSString stringWithFormat:@"%d", (int)dur];
    roomRecordModel.rrec_Total_Inspected_Time += (int)dur;

    //Hao Tran - Remove for unneccessary property
    /*
     //calculate total pause inspection time
     startDate = [formatter dateFromString:roomRecordModel.rrec_Inspection_Start_Time];
     if ([roomRecordModel.rrec_Inspection_End_Time isEqualToString:@"1900-01-01 00:00:00"]) {
     stopDate = [NSDate date];
     } else {
     stopDate = [formatter dateFromString:roomRecordModel.rrec_Inspection_End_Time];
     }
     
     timeDifference = [stopDate timeIntervalSinceDate:startDate];
     NSInteger totalPause = timeDifference - duration;
     roomRecordModel.rrec_Inspected_Total_Pause_Time = [NSString stringWithFormat:@"%d", totalPause];
     */
    
    [[RoomManagerV2 sharedRoomManager] updateRoomRecordData:roomRecordModel];
    
    NSMutableArray *recordDetails = [[RoomManagerV2 sharedRoomManager] loadAllRoomRecordDetailsByRoomAssignmentId:roomAssignmentModel.roomAssignment_Id andUserId:roomAssignmentModel.roomAssignment_UserId];
    
    if (recordDetails.count >= MAX_ROOM_RECORD_DETAIL
        && timerStatus != TIMER_STATUS_FINISH
        //we only set auto complete for maid
        && ![UserManagerV2 isSupervisor])
    {
        //must auto complete the room because maid started, stoped 5 times
        //parameter NO cause we don't need update stop records
        [self confirmCompleteCleaningStatus:NO];
    }
}

//For using complete room by update room status
- (void) completeRoomByRoomStatusUpdated
{
    NSInteger row = 1;
    if ([UserManagerV2 isSupervisor]) {
        row = 3;
    }
    
    RoomStatusModelV2 *roomStatusModelV2 = [[RoomStatusModelV2 alloc] init];
    [roomStatusModelV2 setRstat_Id:(int)roomStatusId];
    [[RoomManagerV2 sharedRoomManager] loadRoomStatusData:roomStatusModelV2];
    [self setRoomStatusModel:(int)roomStatusId];
    
    if(![UserManagerV2 isSupervisor]){
        int responseStatus = [self completeCurrentRoom];
        if (responseStatus == POPUP_STATE_HIDE_WITH_INVALID_ROOM_STATUS) {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:[L_invalid_room_status currentKeyToLanguage] message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:[L_OK currentKeyToLanguage], nil];
            [alert show];
            return;
        }
    } else {
        int popUpState = [self confirmCompleteCleaningStatus];
        if (popUpState == POPUP_STATE_HIDE_WITH_INVALID_ROOM_STATUS) {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:[L_invalid_room_status currentKeyToLanguage] message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:[L_OK currentKeyToLanguage], nil];
            [alert show];
            return;
        }
    }
    
    //Back to view before
    NSArray *arrayViews = self.navigationController.viewControllers;
    int indexVC = (int)([arrayViews count] - 3);
    if(indexVC >= 0 && roomStatusId != ENUM_ROOM_STATUS_OD && roomStatusId != ENUM_ROOM_STATUS_VD)
    {
        if(roomStatusId == ENUM_ROOM_STATUS_OI || roomStatusId == ENUM_ROOM_STATUS_VI || roomStatusId == ENUM_ROOM_STATUS_OOO) {
            [self.navigationController popViewControllerAnimated:YES];
        } else {
            UIViewController *controller = self.navigationController.viewControllers.lastObject;
            controller = [self.navigationController.viewControllers objectAtIndex:indexVC];
            [self.navigationController popToViewController:controller animated:YES];
        }
    }
    else if(isPushFrom == IS_PUSHED_FROM_FIND_BY_RA
            || isPushFrom == IS_PUSHED_FROM_FIND_BY_ROOM
            || isPushFrom == IS_PUSHED_FROM_ROOM_ASSIGNMENT)
    {
        //Showing reassing view after selected room status VD/OD
        if(roomStatusId == ENUM_ROOM_STATUS_VD || roomStatusId == ENUM_ROOM_STATUS_OD) {
            if ((roomAssignmentModel.roomAssignmentHousekeeperId <= 0 && (isPushFrom != IS_PUSHED_FROM_FIND_BY_ROOM))
                || [self confirmCompleteCleaningStatus] == POPUP_STATE_HIDE)
            {
                if(![UserManagerV2 isSupervisor]){
                    [self.navigationController popViewControllerAnimated:YES];
                }
                else{
                    [self initViewDidAppear];
                    RoomReassignmentViewController *roomReassignment = [[RoomReassignmentViewController alloc] init];
                    [roomAssignment setValue:[NSString stringWithFormat:@"%d", (int)roomAssignmentModel.roomAssignmentRoomStatusId] forKey:kCleaningStatusID];
                    [roomReassignment setRoomAssignment:roomAssignment];
                    [roomReassignment setRoomAssignmentModel:roomAssignmentModel];
                    roomReassignment.isPushFrom = IS_PUSHED_FROM_ROOM_STATUS;
                    [self.navigationController pushViewController:roomReassignment animated:YES];
                }
            }
        } else {
            
            NSArray *arrayVC = self.navigationController.viewControllers;
            if([arrayVC count] > 2){
                UIViewController *viewToShow = [arrayVC objectAtIndex:[arrayVC count] - 2];
                [self.navigationController popToViewController:viewToShow animated:YES];
            } else {
                [self.navigationController popToRootViewControllerAnimated:YES];
            }
        }
    }
}

-(int)completeCurrentRoom
{
    int responseStatus = RESPONSE_STATUS_OK;
    isSaved = YES;
    
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getMsgSaveMessage]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES];
    
    SyncManagerV2 *synManager = [[SyncManagerV2 alloc] init];
    
    [CommonVariable sharedCommonVariable].roomIsCompleted = 1;
    if ([CommonVariable sharedCommonVariable].currentPauseRoom == roomAssignmentModel.roomAssignment_Id) {
        [CommonVariable sharedCommonVariable].currentPauseRoom = noPauseRoom;
    }
    
    if (roomStatusId != notSelectRoomStatusYet) {
        [self setRoomStatusModel:(int)roomStatusId];
        
        if(roomStatusId != ENUM_ROOM_STATUS_VD && roomStatusId != ENUM_ROOM_STATUS_OD){
            roomAssignmentModel.roomAssignmentRoomCleaningStatusId = ENUM_CLEANING_STATUS_COMPLETED;
        }
        else {
            roomAssignmentModel.roomAssignmentRoomCleaningStatusId = ENUM_CLEANING_STATUS_PENDING_FIRST_TIME;
        }
    }
    else {
        if(isDemoMode)
        {
            roomAssignmentModel.roomAssignmentRoomCleaningStatusId = ENUM_CLEANING_STATUS_COMPLETED;
            roomAssignmentModel.roomAssignmentRoomInspectionStatusId = ENUM_INSPECTION_PENDING;
            if(roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OD){
                [self setRoomStatusModel:ENUM_ROOM_STATUS_OC];
            } else {
                [self setRoomStatusModel:ENUM_ROOM_STATUS_VC];
            }
        }
        else{
            roomAssignmentModel.roomAssignmentRoomCleaningStatusId = ENUM_CLEANING_STATUS_COMPLETED;
            
            if(roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OD){
                [self setRoomStatusModel:ENUM_ROOM_STATUS_OC];
            }
            
            if(roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VD){
                [self setRoomStatusModel:ENUM_ROOM_STATUS_VC];
            }
        }
    }
    
    NSDateFormatter *timeFormat = [[NSDateFormatter alloc] init];
    [timeFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    // update duration time
    NSDate *now = [NSDate date];
    roomRecordModel.rrec_Cleaning_End_Time = [timeFormat stringFromDate:now];  //... end time
    roomRecordModel.rrec_PostStatus = (int)POST_STATUS_SAVED_UNPOSTED;
    NSInteger dur = duration - roomRecordModel.rrec_Total_Cleaning_Time;
//    duration += roomRecordModel.rrec_Total_Cleaning_Time;
    roomRecordModel.rrec_Total_Cleaning_Time += (int)dur;
    
    //Hao Tran - Remove for unneccessary data
    //NSTimeInterval timeDifference = [now timeIntervalSinceDate:[timeFormat dateFromString:roomRecordModel.rrec_Cleaning_Start_Time]];
    //NSInteger _totalPauseTime = (NSInteger)timeDifference - duration;
    //roomRecordModel.rrec_Cleaning_Total_Pause_Time = [NSString stringWithFormat:@"%d",_totalPauseTime] ;
    
    roomRecordModel.rrec_Last_Cleaning_Duration = [NSString stringWithFormat:@"%d", (int)dur];
    
    if(roomRecordModel.rrec_Id <= 0) {
        [[RoomManagerV2 sharedRoomManager] insertRoomRecordData:roomRecordModel];
    } else {
        [[RoomManagerV2 sharedRoomManager] updateRoomRecordData:roomRecordModel];
    }
    
    if(isDemoMode)
    {
        roomAssignmentModel.roomAssignment_PostStatus = (int)POST_STATUS_UN_CHANGED;
    }
    else{
        roomAssignmentModel.roomAssignment_PostStatus = (int)POST_STATUS_SAVED_UNPOSTED;
    }
    
    [[RoomManagerV2 sharedRoomManager] update:roomAssignmentModel];
    roomModel.room_PostStatus = (int)POST_STATUS_SAVED_UNPOSTED;
    [[RoomManagerV2 sharedRoomManager] updateRoomModel:roomModel];
    
    [self changeTimerStatus:TIMER_STATUS_PAUSE];
    
    if (isPushFrom == IS_PUSHED_FROM_FIND_BY_RA || isPushFrom == IS_PUSHED_FROM_FIND_BY_ROOM) {
        [synManager postRoomCleaningtoServerWithUSerID:[[UserManagerV2 sharedUserManager] currentUser].userId AndRoomAssigmentModel:roomAssignmentModel];
    }
    else {
        //-------------Post WSLog--------
        int switchStatus = (int)[[NSUserDefaults standardUserDefaults] integerForKey:ENABLE_LOGWS];
        if (switchStatus == 1) {
            NSString *time = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSInteger UserID = [[[UserManagerV2 sharedUserManager] currentUser] userId];
            NSString *messageValue = [NSString stringWithFormat:@"Clean Post WSLog:Time:%@, userId:%d, CleaningDuration:%d", time, (int)UserID, roomRecordModel.rrec_Total_Cleaning_Time];
            [synManager postWSLogCheckListDetail:userId Message:messageValue MessageType:1];
        }
        //-------------End Post WSLog--------
        
        responseStatus = [synManager postRoomAssignmentWithUserID:[[UserManagerV2 sharedUserManager] currentUser].userId AndRaModel:roomAssignmentModel];
    }
    
    [HUD hide:YES];
    [HUD removeFromSuperview];
    
    return responseStatus;
}

//to avoid recursion in updateStopRecord function
- (enum POPUP_STATE)confirmCompleteCleaningStatus{
    return [self confirmCompleteCleaningStatus:YES];
}

//parameter YES: will update stop records
//parameter NO: will not update stop records cause it come from updateStopRecord function
- (enum POPUP_STATE)confirmCompleteCleaningStatus:(BOOL)isNeedUpdateStopRecord {
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_RA_SAVING_DATA]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES];
    
    SyncManagerV2 *synManager = [[SyncManagerV2 alloc] init];
    
    CheckListManagerV2 *chkManager = [[CheckListManagerV2 alloc] init];
    CheckListModelV2 *chkModel = [[CheckListModelV2 alloc] init];
    chkModel.chkListId = [TasksManagerV2 getCurrentRoomAssignment];
    chkModel.chkListUserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    [chkManager loadCheckListData:chkModel];
    
    //update room record end time
    NSDateFormatter *timeFomarter = [[NSDateFormatter alloc] init];
    [timeFomarter setDateFormat:[ehkConvert getyyyy_MM_dd_HH_mm_ss]];
    
    [CommonVariable sharedCommonVariable].roomIsCompleted = 1;
    NSDate *now = [NSDate date];
    roomRecordModel.rrec_Inspection_End_Time = [timeFomarter stringFromDate:now];
    roomRecordModel.rrec_Last_Inspected_Duration = [NSString stringWithFormat:@"%d", (int)duration];
    
    if(isNeedUpdateStopRecord) {
        [self updateStopRecord];
    }
    
    [self changeTimerStatus:TIMER_STATUS_FINISH];
    
    if (roomStatusId == notSelectRoomStatusYet && roomAssignmentModel.roomAssignmentRoomStatusId != ENUM_ROOM_STATUS_OI && roomAssignmentModel.roomAssignmentRoomStatusId != ENUM_ROOM_STATUS_VI && roomAssignmentModel.roomAssignmentRoomStatusId != ENUM_ROOM_STATUS_OD && roomAssignmentModel.roomAssignmentRoomStatusId != ENUM_ROOM_STATUS_VD && roomAssignmentModel.roomAssignmentRoomStatusId != ENUM_ROOM_STATUS_OOO && roomAssignmentModel.roomAssignmentRoomStatusId != ENUM_ROOM_STATUS_OOS) {
        
        if ([chkManager checkChecklistStatusWithRoomAssigmentId:roomAssignmentModel.roomAssignment_Id] == NO) {
            
//#warning conrad CR
            roomAssignmentModel.roomAssignmentRoomInspectionStatusId = ENUM_INSPECTION_COMPLETED_PASS;
            if (roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OPU) {
                [self setRoomStatusModel:ENUM_ROOM_STATUS_OI];
            }
            else if (roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VPU) {
                [self setRoomStatusModel:ENUM_ROOM_STATUS_VI];
            }
            
            roomModel.room_PostStatus = (int)POST_STATUS_SAVED_UNPOSTED;
            [[RoomManagerV2 sharedRoomManager] updateRoomModel:roomModel];
            
            roomAssignmentModel.roomAssignment_PostStatus = (int)POST_STATUS_SAVED_UNPOSTED;
            [[RoomManagerV2 sharedRoomManager] update:roomAssignmentModel];
            
            [synManager postRoomInspectionWithUserID:[[UserManagerV2 sharedUserManager] currentUser].userId AndRaModel:roomAssignmentModel];
            
            [HUD hide:YES];
            [HUD removeFromSuperview];
            
            isSaved = YES;
            
            return POPUP_STATE_HIDE;
//#warning end
        }
    }
    
    //implement complete the room without checklist for CR 5.0
    if (roomStatusId == notSelectRoomStatusYet) {
        if (chkModel.chkListStatus == chkStatusPass) {
            roomAssignmentModel.roomAssignmentRoomInspectionStatusId = ENUM_INSPECTION_COMPLETED_PASS;
            if (roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OPU) {
                [self setRoomStatusModel:ENUM_ROOM_STATUS_OI];
            }
            else if (roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VPU) {
                [self setRoomStatusModel:ENUM_ROOM_STATUS_VI];
            }
        }
        else if (chkModel.chkListStatus == chkStatusFail){
            roomAssignmentModel.roomAssignmentRoomInspectionStatusId = ENUM_INSPECTION_COMPLETED_FAIL;
            roomAssignmentModel.roomAssignment_PostStatus = (int)POST_STATUS_SAVED_UNPOSTED;
            if (roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OPU) {
                [self setRoomStatusModel:ENUM_ROOM_STATUS_OD];
                roomStatusId = ENUM_ROOM_STATUS_OD;
            }
            else if (roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VPU) {
                [self setRoomStatusModel:ENUM_ROOM_STATUS_VD];
                roomStatusId = ENUM_ROOM_STATUS_VD;
            }
            
            //Remove for unneccessary function
            //[self initViewDidAppear];
      
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                [synManager postRoomInspectionWithUserID:[[UserManagerV2 sharedUserManager] currentUser].userId AndRaModel:roomAssignmentModel];
                [self updateRoomStatusToWS];
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    RoomReassignmentViewController *roomReassignment = [[RoomReassignmentViewController alloc] init];
                    [roomAssignment setValue:[NSString stringWithFormat:@"%d", (int)roomAssignmentModel.roomAssignmentRoomStatusId] forKey:kCleaningStatusID];
                    [roomReassignment setRoomAssignment:roomAssignment];
                    [roomReassignment setRoomAssignmentModel:roomAssignmentModel];
                    roomReassignment.isPushFrom = IS_PUSHED_FROM_ROOM_ASSIGNMENT;
                    [self.navigationController pushViewController:roomReassignment animated:YES];
                    [HUD hide:YES];
                    [HUD removeFromSuperview];
                    isSaved = YES;
                });
            });
            
            return POPUP_STATE_SHOW;
        }
    }
    else if (roomStatusId == ENUM_ROOM_STATUS_OD || roomStatusId == ENUM_ROOM_STATUS_VD) {
        //inspection fail without checklist
        //clear checklist by CR 5.0
        chkManager = [[CheckListManagerV2 alloc] init];
        [chkManager clearDataChecklistWithRoomAssignmentId:roomAssignmentModel.roomAssignment_Id andInspectionStatus:ENUM_INSPECTION_COMPLETED_FAIL];
        
            //manual set inspection status to fail
            roomAssignmentModel.roomAssignmentRoomInspectionStatusId = ENUM_INSPECTION_COMPLETED_FAIL;
            roomAssignmentModel.roomAssignmentRoomCleaningStatusId = ENUM_CLEANING_STATUS_COMPLETED;
//        }
        //Hao Tran[20130513/Fix can't reassign] - END
    }
    else if (roomStatusId == ENUM_ROOM_STATUS_OI || roomStatusId == ENUM_ROOM_STATUS_VI || roomStatusId == ENUM_ROOM_STATUS_OOO || roomStatusId == ENUM_ROOM_STATUS_OOS) {
        //inspection pass without checklist
        //clear checklist by CR 5.0
        chkManager = [[CheckListManagerV2 alloc] init];
        [chkManager clearDataChecklistWithRoomAssignmentId:roomAssignmentModel.roomAssignment_Id andInspectionStatus:ENUM_INSPECTION_COMPLETED_PASS];
        
        roomAssignmentModel.roomAssignmentRoomInspectionStatusId = ENUM_INSPECTION_COMPLETED_PASS;
    }
    else if (roomStatusId == ENUM_ROOM_STATUS_OC || roomStatusId == ENUM_ROOM_STATUS_VC) {
        //        CheckListManagerV2 *chkManager = [[CheckListManagerV2 alloc] init];
        //        CheckListModelV2 *chklist = [[CheckListModelV2 alloc] init];
        //        chklist.chkListId = roomAssignmentModel.roomAssignment_Id;
        //        chklist.chkListUserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
        //        [chkManager loadCheckListData:chklist];
        //
        
        [chkManager clearDataChecklistWithRoomAssignmentId:roomAssignmentModel.roomAssignment_Id andInspectionStatus:ENUM_INSPECTION_COMPLETED_PASS];
        roomAssignmentModel.roomAssignmentRoomInspectionStatusId = ENUM_INSPECTION_PENDING;
    }
    
    if (roomStatusId != notSelectRoomStatusYet) {
        [self setRoomStatusModel:(int)roomStatusId];
    }
    
    roomModel.room_PostStatus = (int)POST_STATUS_SAVED_UNPOSTED;
    [[RoomManagerV2 sharedRoomManager] updateRoomModel:roomModel];
    
    roomAssignmentModel.roomAssignment_PostStatus = (int)POST_STATUS_SAVED_UNPOSTED;
    roomAssignmentModel.roomAssignmentChecklistRemark = ramodelController.roomAssignmentChecklistRemark;
    [[RoomManagerV2 sharedRoomManager] update:roomAssignmentModel];
    
    int responseStatus = [synManager postRoomInspectionWithUserID:[[UserManagerV2 sharedUserManager] currentUser].userId AndRaModel:roomAssignmentModel];
    
    [HUD hide:YES];
    [HUD removeFromSuperview];
    
    isSaved = YES;
    
    if(responseStatus == RESPONSE_STATUS_INVALID_ROOM_STATUS) {
        return POPUP_STATE_HIDE_WITH_INVALID_ROOM_STATUS;
    }
    return POPUP_STATE_HIDE;
}

#pragma mark - Checklist functions

//Return Score of checklist submitted
-(NSInteger) loadChecklistStatus
{
    NSInteger score = -1;
    CheckListManagerV2 *chkManager = [[CheckListManagerV2 alloc] init];
    NSMutableArray *arrayFormScore = [chkManager loadChecklistFormScoreByChkListId:roomAssignmentModel.roomAssignment_Id AndChkUserId:[UserManagerV2 sharedUserManager].currentUser.userId];
    
    for(ChecklistFormScoreModel *chkFormScore in arrayFormScore) {
        NSString *formScore = [NSString stringWithFormat:@"%d", (int)chkFormScore.dfScore];
        if (![formScore isEqualToString:@"-1"]) {
            isSubmitCheckList = true;
            score = chkFormScore.dfScore;
            
            //Get Checklist form by Id
            CheckListTypeModelV2 *chkListType = [[CheckListTypeModelV2 alloc] init];
            chkListType.chkTypeId = chkFormScore.dfFormId;
            chkListType = [chkManager loadCheckListForm:chkListType];
            
            isCheckListPass = (score >= chkListType.chkTypePassingScore);
            
            if([[UserManagerV2 sharedUserManager].currentUserAccessRight checklistMandatoryPass] > 0) {
                if (chkFormScore.dfIsMandatoryPass <= 0) {
                    isCheckListPass = NO;
                }
            }
            break;
        }
    }
    
    return score;
}

-(void) loadDataCheckListFormService
{
    SyncManagerV2 *synManager = [[SyncManagerV2 alloc] init];
//    NSInteger userId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    NSInteger roomAssignId = roomAssignmentModel.roomAssignment_Id;
    NSInteger hotelId = [[[UserManagerV2 sharedUserManager] currentUser] userHotelsId];
    NSString *dateLastModified = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd"];
    
    //load List CheckList FormScore by CheckListId
    CheckListManagerV2 *manager = [[CheckListManagerV2 alloc] init];
    CheckListModelV2 *chkModel = [[CheckListModelV2 alloc] init];
    chkModel.chkListId  = roomAssignId;
    chkModel.chkListUserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
    

    [manager loadCheckListData:chkModel];
    
    //if data checklist have not in database
    //insert checklist 
    if (chkModel.chkListRoomId.length <= 0) {
        chkModel = [[CheckListModelV2 alloc] init];
        chkModel.chkListId = [[roomAssignment objectForKey:kRoomAssignmentID] integerValue];
        chkModel.chkListRoomId = roomModel.room_Id;
        chkModel.chkListUserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
        
        int inspectionStatusId = (int)roomAssignmentModel.roomAssignmentRoomInspectionStatusId;
        if (inspectionStatusId == ENUM_INSPECTION_COMPLETED_PASS) {
            chkModel.chkListStatus = tChkPass;
        }
        else if (inspectionStatusId == ENUM_INSPECTION_COMPLETED_FAIL) {
            chkModel.chkListStatus = tChkFail;
        }
        else {
            chkModel.chkListStatus = tNotCompleted;
        }
        
        chkModel.chkPostStatus = POST_STATUS_UN_CHANGED;
        chkModel.chkCore = -1;
        [manager insertCheckList:chkModel];
        
        //Hao Tran - get all checklist data if didn't have
        [synManager getAllCheckListFormWithUserId:userId AndHotelId:hotelId];
        
        if(!isDemoMode)
        {
            NSMutableArray *arrayChkFrom = [manager loadAllCheckListForm];
            for (CheckListTypeModelV2 *chkTypeModel in arrayChkFrom) {
                
                [manager getCheckListCategoriesWSWithUserId:userId AndHotelId:hotelId AndChecklistId:(int)chkTypeModel.chkTypeId];
                [manager getAllCheckListItemWSWithUserId:userId AndHotelId:hotelId AndChecklistId:(int)chkTypeModel.chkTypeId];
                
            }
        }
    }

    // Hao Tran remove for new checklist WS
    //    CheckListContentTypeModelV2 *chkListCategories = [manager loadCheckListCategoriesLatest];
    //
    //    if (chkListCategories.chkContentId == 0) {
    //        [synManager getAllCheckListCategoriesWithUserId:userId AndHotelId:hotelId];
    //    }
    //    CheckListDetailContentModelV2 *chkListDetailContent = [manager loadCheckListItemLatest];
    //
    //    if (chkListDetailContent.chkDetailContentId == 0) {
    //        [synManager getAllCheckListItemWithUserId:userId AndHotelId:hotelId];
    //    }

    NSMutableArray *arrayChkFrom = [manager loadAllCheckListForm];
    
    for (CheckListTypeModelV2 *chkTypeModel in arrayChkFrom) {
        if((isDemoMode && [UserManagerV2 isSupervisor]) || !isDemoMode){
            ChecklistFormScoreModel *chkFormScore = [manager loadCheckListFormScoreByChkListId:chkModel.chkListId AndChkFormId:chkTypeModel.chkTypeId AndChkUserId:[UserManagerV2 sharedUserManager].currentUser.userId];
            
            if(chkFormScore.dfId == 0) {
                chkFormScore.dfFormId = chkTypeModel.chkTypeId;
                chkFormScore.dfChecklistId = chkModel.chkListId;
                chkFormScore.dfUserId = [UserManagerV2 sharedUserManager].currentUser.userId;
                chkFormScore.dfScore = -1;
                chkFormScore.dfPostStatus = POST_STATUS_SAVED_UNPOSTED;
                [manager insertCheckListFormScore:chkFormScore];
            }
        }
        else if(isDemoMode && ![UserManagerV2 isSupervisor]){
            ChecklistFormScoreModel *chkFormScore = [manager loadCheckListFormScoreByChkListId:chkModel.chkListId AndChkFormId:chkTypeModel.chkTypeId AndChkUserId:[UserManagerV2 sharedUserManager].currentUser.userId];
            if(chkFormScore.dfId == 0) {
                ChecklistFormScoreModel *chkFormScoreForMaid = [manager loadCheckListFormScoreByChkListId:chkModel.chkListId AndChkFormId:chkTypeModel.chkTypeId];
                if(chkFormScoreForMaid.dfUserId != [UserManagerV2 sharedUserManager].currentUser.userId){
                    chkFormScoreForMaid.dfUserId = [UserManagerV2 sharedUserManager].currentUser.userId;
                    [manager insertCheckListFormScore:chkFormScoreForMaid];
                }
            }
        }
    }
    
    NSInteger checklistResponseCode = RESPONSE_STATUS_OK;
    if(!isDemoMode){
        checklistResponseCode = [synManager getCheckListItemScoreForMaidWithUserId:userId DutyAssignId:roomAssignId AndLastModified:dateLastModified];
    }
    
    if (checklistResponseCode == RESPONSE_STATUS_OK) {
        
        arrayChkFrom = [[NSMutableArray alloc] init];
        
        chkModel = [[CheckListModelV2 alloc] init];
        chkModel.chkListId  = roomAssignmentModel.roomAssignment_Id;
        chkModel.chkListUserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
        [manager loadCheckListData:chkModel];
        
        arrayChkFrom = [manager loadAllCheckListForm];
        
        CGFloat totalChkListScore = 0;
        
        //add score for checklist
        NSMutableArray *arrayChkFormScore = [[NSMutableArray alloc] init];
        if((isDemoMode && [UserManagerV2 isSupervisor]) || !isDemoMode){
            arrayChkFormScore = [manager loadChecklistFormScoreByChkListId:chkModel.chkListId AndChkUserId:[UserManagerV2 sharedUserManager].currentUser.userId];
        }
        
        BOOL noCheckList = true;
        NSInteger passingScore = 90;
        NSInteger isMandatoryPass = 1;
        
        for (ChecklistFormScoreModel *formScoreModel in arrayChkFormScore) {
            CGFloat totalhkListFormScore = 0;
            CGFloat totalPossible = 0;
            CGFloat totalPercentFormScore = 0;
            
            //load checklist form
            CheckListTypeModelV2 *chkListForm = [[CheckListTypeModelV2 alloc] init];
            chkListForm.chkTypeId = formScoreModel.dfFormId;
            [manager loadCheckListForm:chkListForm];
            
            //set total possible
            totalPossible = chkListForm.chkTypePercentageOverall;
            
            //load checklist item score
            NSMutableArray *arrayItemScore = [manager loadCheckListItemCoreByChkListFormScoreIdData:formScoreModel.dfId];
            
            BOOL isUpdate = false;
            if ([arrayItemScore count] == 0) {
                formScoreModel.dfScore = -1;
                isUpdate = true;
            }
            else {
                int countTotalItems = 0;
                totalhkListFormScore = 0;
                isMandatoryPass = 1;
                for (CheckListItemCoreDBModelV2 *itemScoreModel in arrayItemScore) {
                    if (itemScoreModel.chkItemCore != -1 && itemScoreModel.chkPostStatus != POST_STATUS_SAVED_UNPOSTED) {
                        totalhkListFormScore += itemScoreModel.chkItemCore;
                        if(chkListForm.chkType == tCheckmark) {
                            countTotalItems ++;
                        } else { //Checklist type Rate
                            NSInteger maxScore = [manager getChecklistItemPassScoreByCheckListItemID:itemScoreModel.chkItemCoreDetailContentId];
                            countTotalItems += maxScore;
                        }
                    }
                    
                    NSInteger mandatoryScore = [manager getChecklistItemMandatoryScoreByCheckListItemID:itemScoreModel.chkItemCoreDetailContentId];
                    
                    if([[UserManagerV2 sharedUserManager].currentUserAccessRight checklistMandatoryPass] > 0) {
                        if (itemScoreModel.chkItemCore < mandatoryScore) {
                            isMandatoryPass = 0;
                        }
                    }
                    
                    if (itemScoreModel.chkPostStatus == POST_STATUS_POSTED) {
                        noCheckList = false;
                        isUpdate = true;
                    }
                }
                
                //totalPercentFormScore = roundf((1.0 * totalhkListFormScore / totalPossible)*100.0);
                totalPercentFormScore = roundf((1.0 * totalhkListFormScore / countTotalItems) * 100.0);
                formScoreModel.dfScore = totalPercentFormScore;
                formScoreModel.dfIsMandatoryPass = isMandatoryPass;
            }
            
            //update score for checklist form
            //if (isUpdate) {
            [manager updateCheckListFormScore:formScoreModel];
            //}
            
            //set score for checklist
            //totalChkListScore += (formScoreModel.dfScore == -1 ? 0 : formScoreModel.dfScore);
            totalChkListScore = formScoreModel.dfScore;
            if(totalChkListScore > -1) {
                passingScore = chkListForm.chkTypePassingScore;
                break;
            }
        }
        
        if (totalChkListScore >= 0) {
            CGFloat totalOveral = 0;
            if (noCheckList) {
                totalOveral = -1;
            }
            else {
                if (arrayChkFrom.count != 0) {
                    totalOveral = (totalChkListScore * 1.0);
                }
            }
            
            if (totalOveral < passingScore || isMandatoryPass <= 0) {
                chkModel.chkListStatus = chkStatusFail;
            }
            else {
                chkModel.chkListStatus = chkStatusPass;
            }
            
            chkModel.chkCore = totalOveral;
            chkModel.chkListUserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
            [manager updateCheckList:chkModel];
        }
    }
}


-(void) clearLocalCheckList
{
    //Hao Tran Remove - Only clear checklist data for VC Room because OC room will not change to OPU status for supervisor inspection
    //if (roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OC || roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VC) {
    
    //Hao Tran - [20150403/Fixed checklist data is not clear in OC room]
    if(roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_VC
       || roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OC) {
        CheckListManagerV2 *checkListManager = [[CheckListManagerV2 alloc] init];
        
        CheckListModelV2 *chklist = [[CheckListModelV2 alloc] init];
        //clear data checklist
        chklist.chkListId = roomAssignmentModel.roomAssignment_Id;
        chklist.chkListUserId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
        [checkListManager loadCheckListData:chklist];
        
        if (chklist.chkListUserId != 0) {
            //[RC issues / 2015-03-20] - Fixed failed room with checklist, room matrix still show OPU/VPU
            //chklist.chkListStatus = POST_STATUS_SAVED_UNPOSTED;
            chklist.chkPostStatus = POST_STATUS_SAVED_UNPOSTED;
            chklist.chkCore = - 1;
            [checkListManager updateCheckList:chklist];
            
            //clear data checklist form score
            NSMutableArray *arrayChklistFormScore = [checkListManager loadChecklistFormScoreByChkListId:chklist.chkListId AndChkUserId:[UserManagerV2 sharedUserManager].currentUser.userId];
            
            for (ChecklistFormScoreModel *formScore in arrayChklistFormScore) {
                formScore.dfScore = -1;
                [checkListManager updateCheckListFormScore:formScore];
                
                //clear data checklist item score
                NSMutableArray *arrayChklistItemScore = [checkListManager loadCheckListItemCoreByChkListFormScoreIdData:formScore.dfId];
                
                for (CheckListItemCoreDBModelV2 *itemScore in arrayChklistItemScore) {
                    itemScore.chkItemCore = -1;
                    itemScore.chkPostStatus = POST_STATUS_UN_CHANGED;
                    [checkListManager updateCheckListItemCoreByChkItemId:itemScore];
//                  [checkListManager deleteCheckListItemScoreByItemId:itemScore];
                }
            }
        }
    }
}

#pragma mark - iOS 7 Resources
-(void)loadFlatResource
{
    btnReassign.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    [imgBgCheckListOverall setImage:[UIImage imageNamed:imgTopBtFlat]];
    [btnRoomAssignmentInfo setBackgroundImage:[UIImage imageNamed:imgGuestInfoRoomAssignmentActiveFlat] forState:UIControlStateNormal];
    [btnRoomAssignmentInfo setBackgroundImage:[UIImage imageNamed:imgGuestInfoRoomAssignmentActiveFlat] forState:UIControlStateHighlighted];
    [btnPostingHistory setBackgroundImage:[UIImage imageNamed:imgHistoryPostingBtnFlat] forState:UIControlStateHighlighted];
    [btnPostingHistory setBackgroundImage:[UIImage imageNamed:imgHistoryPostingBtnFlat] forState:UIControlStateNormal];
}

-(void)showRoomAssignmentDetail {
    
    [viewDetail setHidden:NO];
    [self.tbViewDetail setHidden:NO];
    [scviewRoomAssignmentDetail setScrollEnabled:YES];
    
    //if (guestInfo != nil) {
    if(listGuestModels.count > 0) {
        
        //        CGSize size = tbViewDetail.frame.size;
        //        if ([UserManagerV2 isSupervisor]) {
        //            size.height = tbViewDetailHeight + ([UserManagerV2 isSupervisor] ? viewDetail.frame.size.height : 0);
        //        }
        //        else {
        //            size.height = tbViewDetailHeight - 2*45 + ([UserManagerV2 isSupervisor] ? viewDetail.frame.size.height : 0);
        //        }
        //
        //        if (isPushFrom != IS_PUSHED_FROM_COMPLETE) {
        //            CGRect frame = viewDetail.frame;
        //            size.height += frame.size.height;
        //        }
        
        [guestScrollView setHidden:YES];
        if(guestIndicatorLeft) {
            [guestIndicatorLeft setHidden:YES];
        }
        if(guestIndicatorRight) {
            [guestIndicatorRight setHidden:YES];
        }
        
        [guestScrollView setHidden:YES];
        [scviewRoomAssignmentDetail setContentSize:contentRoomAssignmentSize];
    }
}

@end
