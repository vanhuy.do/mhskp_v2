//
//  RoomReassignmentViewController.h
//  mHouseKeeping
//
//  Created by Khanh Phan on 2/19/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RemarkViewV2.h"
#import "MBProgressHUD.h"
#import "TopbarViewV2.h"
#import "CleaningStatusConfirmPopupView.h"

@interface RoomReassignmentViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, RemarkViewV2Delegate, MBProgressHUDDelegate, CleaningStatusConfirmPopupDelegate, UITextFieldDelegate>
{
    IBOutlet UIView *viewReassignment;
    IBOutlet UIView *viewSelection;
    IBOutlet UIScrollView *scviewReassignment;
    
    IBOutlet UITableView *tbViewRoomAttendant;
    IBOutlet UITableView *tbViewPriority;
    
    IBOutlet UILabel *lblCaptionAssignTo;
    __weak IBOutlet UILabel *lblCaptionCleaningCredit;
    IBOutlet UILabel *lblCaptionTime;
    IBOutlet UILabel *lblCaptionPriority;
    IBOutlet UILabel *lblCaptionRemark;
    IBOutlet UILabel *lblTitleSetTime;
    IBOutlet UILabel *lblYes;
    IBOutlet UILabel *lblNo;
    IBOutlet UILabel *lblAssignedRoomAttendant;
    IBOutlet UILabel *lblPriority;
    IBOutlet UILabel *lblRemark;
    IBOutlet UILabel *lblTime;
    __weak IBOutlet UITextField *tfCleaningCredit;
    __weak IBOutlet UIView *vCleaningCredit;
    __weak IBOutlet UIView *vRemarkFooter;
    
    IBOutlet UILabel *lblRoomAttendantName;
    IBOutlet UILabel *lblRoomStatus;
    IBOutlet UIImageView *imgRoomStatusIcon;
    
    IBOutlet UIButton *btnSetTime;
    IBOutlet UIButton *btnCancel;
    IBOutlet UIButton *btnRemark;
    IBOutlet UIView *viewTimePicker;
    IBOutlet UIDatePicker *datePicker;
    
    NSArray *priorities;
    NSMutableArray *userList;
    
    NSInteger selectedPriorityIndex;
    NSInteger selectedAssignedRoomAttendant;
    NSDate *selectedDate;
    
    RemarkViewV2 *remarkView;
    TopbarViewV2 *topBarView;
    
    CleaningStatusConfirmPopupView *alertViewPastTime;
}
@property (nonatomic, strong) IBOutlet UIImageView *imgTop;
@property (nonatomic, strong) IBOutlet UIButton *btnSubmit;
@property (nonatomic, strong) IBOutlet UIButton *btnAssignTo;
@property (nonatomic, strong) IBOutlet UIButton *btnTime;
@property (nonatomic, strong) IBOutlet UIButton *btnPriority;
@property (nonatomic, strong) NSDictionary *roomAssignment;
@property (nonatomic, strong) RoomAssignmentModelV2 *roomAssignmentModel;
@property (nonatomic, assign) int isPushFrom;

-(IBAction) btnSetTimeClicked:(id)sender;
-(IBAction) btnCancelClicked:(id)sender;
-(IBAction) btnSubmitClicked:(id)sender;
-(IBAction) btnOutsideClicked :(id)sender;
-(IBAction) btnSelectRoomAttendantClicked:(id)sender;
-(IBAction) btnSelectPriority:(id)sender;
-(IBAction) btnRemarkClicked:(id)sender;
-(IBAction) btnSelectTimer:(id)sender;

@end
