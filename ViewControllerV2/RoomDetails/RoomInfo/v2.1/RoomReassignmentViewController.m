//
//  RoomReassignmentViewController.m
//  mHouseKeeping
//
//  Created by Khanh Phan on 2/19/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#define tableViewRoomAttendantOriginalFrame         CGRectMake(0, 40, 320, 400)
#define tableViewRoomAttendantCellHeight            44
#define imgBtnYes                                   @"btn_yes.png"
#define imgBtnYesClicked                            @"btn_yes (onpress).png"
#define tagErrorPosting                             61

#import "RoomReassignmentViewController.h"
#import "MyNavigationBarV2.h"
#import "RoomAssignmentInfoViewController.h"
#import "FindByRoomViewV2.h"
#import "DeviceManager.h"
#import "AdHocMessageManagerV2.h"
#import "CustomAlertViewV2.h"

@interface RoomReassignmentViewController ()

@end

@implementation RoomReassignmentViewController

@synthesize roomAssignmentModel;
@synthesize roomAssignment;
@synthesize btnSubmit, btnAssignTo, btnTime, btnPriority;
@synthesize imgTop;
@synthesize isPushFrom;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self modifyAllViews];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Resize content views of current VC to fit with Navigation bar and tabbar
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        [btnRemark.layer setCornerRadius:8.0f];
        [btnRemark.layer setBorderColor:[[UIColor blackColor] CGColor]];
        [btnRemark.layer setBorderWidth:1.0f];
        [btnRemark setBackgroundColor:[UIColor whiteColor]];
        [self loadFlatResource];
    }
    
    selectedAssignedRoomAttendant = 0;
    
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getRaLoadingData]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES];
    
    [self performSelector:@selector(loadData:) withObject:HUD afterDelay:0.0f];
    
    if(roomAssignmentModel.roomAssignmentHousekeeperId <= 0) {
        [lblCaptionAssignTo setText:[L_assign_to currentKeyToLanguage]];
    } else {
        [lblCaptionAssignTo setText:[L_reassign_to currentKeyToLanguage]];
    }
    [lblCaptionPriority setText:[L_priority_lable currentKeyToLanguage]];
    [lblCaptionRemark setText:[L_remark currentKeyToLanguage]];
    [lblCaptionTime setText:[L_time currentKeyToLanguage]];
    [btnSubmit setTitle:[L_SUBMIT currentKeyToLanguage] forState:UIControlStateNormal];
    [lblTitleSetTime setText:[L_set_up currentKeyToLanguage]];
    [lblYes setText:[L_YES currentKeyToLanguage]];
    [lblNo setText:[L_NO currentKeyToLanguage]];
    [lblCaptionCleaningCredit setText:[L_cleaning_credit currentKeyToLanguage]];
    tfCleaningCredit.text = roomAssignmentModel.roomAssignmentCleaningCredit;
    
    if (![UserManagerV2 sharedUserManager].currentUserAccessRight.roomAssignmentManualCredit) {
        CGRect frame = vRemarkFooter.frame;
        frame.origin.y = vCleaningCredit.frame.origin.y;
        vRemarkFooter.frame = frame;
        vCleaningCredit.hidden = YES;
    }
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if ([string isEqualToString:@"."] && [textField.text containsString:@"."]) {
        return NO;
    }
    return YES;
}
- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - TableView Delegate
-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (![tbViewPriority isHidden]) {
        return [priorities count];
    }
    return [userList count];
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return tableViewRoomAttendantCellHeight;
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *roomAttendantCellIdentifier = @"RoomAttendNameCell";
    static NSString *priorityCellIdenfifier = @"PriorityCell";
    NSString *fontType = @"Helvetica-Bold";
    
    NSString *labelString = nil;
    UILabel *label = nil;
    
    if (![tbViewPriority isHidden]) {
        labelString = [priorities objectAtIndex:indexPath.row];
    }
    else if (![tbViewRoomAttendant isHidden]) {
        UserListModelV2 *user = [userList objectAtIndex:indexPath.row];
        labelString = user.userListFullName;
    }
    
    UITableViewCell *cell = nil;
    
    if (![tbViewPriority isHidden]) {
        cell = [tableView dequeueReusableCellWithIdentifier:priorityCellIdenfifier];
    }
    else {
        cell = [tableView dequeueReusableCellWithIdentifier:roomAttendantCellIdentifier];
    }
    
    if (cell == nil) {
        if (![tbViewPriority isHidden]) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:priorityCellIdenfifier];
        }
        else {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:roomAttendantCellIdentifier];
        }
        
        label = [[UILabel alloc] initWithFrame:CGRectMake(20, 3, 280, 38)];
        [label setTextColor:[UIColor blackColor]];
        [label setFont:[UIFont fontWithName:fontType size:22]];
        [label setAdjustsFontSizeToFitWidth:YES];
        [label setTextAlignment:NSTextAlignmentCenter];
        [label setTag:1];
        
        [cell addSubview:label];
    }
    else {
        label = (UILabel *)[cell viewWithTag:1];
    }
    
    [label setText:labelString];
    
    [label setText:labelString];
    return cell;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (![tbViewPriority isHidden]) {
        selectedPriorityIndex = indexPath.row;
        [lblPriority setText:[priorities objectAtIndex:indexPath.row]];
    }
    else if (![tbViewRoomAttendant isHidden]) {
        UserListModelV2 *user = [userList objectAtIndex:indexPath.row];
        selectedAssignedRoomAttendant = indexPath.row;
        [lblAssignedRoomAttendant setText:user.userListFullName];
        if (user.userListId <= 0) { //Unassign
            [self setEnableBtnTime:NO];
        } else {
            [self setEnableBtnTime:YES];
        }
    }
    
    [viewSelection removeFromSuperview];
}

#pragma mark - Remark Delegate
-(void)remarkViewV2DoneWithText:(NSString *)text andController:(UIViewController *)controllerView{
    [lblRemark setText:text];
}

-(void)remarkViewV2didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
    }
    else {
    }
}

#pragma mark - Button Methods
-(IBAction) btnRemarkClicked:(id)sender
{
    if (remarkView == nil) {
        remarkView = [[RemarkViewV2 alloc] initWithNibName:@"RemarkViewV2" bundle:nil];
        [remarkView setDelegate:self];
    }
    UIView *view = remarkView.view;
    [remarkView setTextinRemark:lblRemark.text];
    [remarkView viewWillAppear:NO];
    
    //[self.tabBarController.view addSubview:remarkView.view];
    remarkView.isAddSubview = YES;
    [self.tabBarController addSubview:view];
}

-(IBAction) btnOutsideClicked:(id)sender
{
    [viewSelection removeFromSuperview];
}

-(IBAction) btnSelectPriority:(id)sender
{
    [tbViewRoomAttendant setHidden:YES];
    [tbViewPriority setHidden:NO];
    [viewTimePicker setHidden:YES];
    [tbViewPriority reloadData];
    
    [viewSelection setFrame:[DeviceManager getScreendDeviceBounds]];
    //[self.tabBarController.view addSubview:viewSelection];
    [self.tabBarController addSubview:viewSelection];
}

-(IBAction) btnSelectRoomAttendantClicked:(id)sender
{
    if ([userList count] == 0) {
        return;
    }
    
    [tbViewRoomAttendant setHidden:NO];
    [tbViewPriority setHidden:YES];
    [viewTimePicker setHidden:YES];
    [tbViewRoomAttendant reloadData];
    [viewSelection setFrame:[DeviceManager getScreendDeviceBounds]];
    
    //[self.tabBarController.view addSubview:viewSelection];
    [self.tabBarController addSubview:viewSelection];
}

-(IBAction)btnSubmitClicked:(id)sender
{
    UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_please_confirm_to_assign_the_room] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles:[[LanguageManagerV2 sharedLanguageManager] getNo],  nil];
    alert.delegate = self;
    [alert show];
}

-(IBAction)btnSetTimeClicked:(id)sender {
    
    selectedDate = [datePicker date];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"hh:mm a"];
    [lblTime setText:[dateFormat stringFromDate:selectedDate]];
    
    [viewSelection removeFromSuperview];
}

-(IBAction)btnCancelClicked:(id)sender {
    
    [viewSelection removeFromSuperview];
}

-(IBAction) btnSelectTimer:(id)sender
{
    [tbViewRoomAttendant setHidden:YES];
    [tbViewPriority setHidden:YES];
    [viewTimePicker setHidden:NO];
    [viewSelection setFrame:[DeviceManager getScreendDeviceBounds]];
    //[self.tabBarController.view addSubview:viewSelection];
    [self.tabBarController addSubview:viewSelection];
}

#pragma mark - CleaningStatusConfirmPopup delegate
-(void) btnCloseClicked
{
    [alertViewPastTime removeFromSuperview];
    
    return;
}
-(void) btnYesClicked
{
    
}

#pragma mark - UIAlertView Delegate

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    
    //Clicked Button OK in popup view
    if(alertView.tag == tagErrorPosting){
        return;
    }
    
    switch (buttonIndex) {
        case 0:
            //YES
        {
            if ([userList count] == 0) {
                return;
            }
            
            MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
            [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_RA_SAVING_DATA]];
            [HUD setDelegate:self];
            [HUD show:YES];
            [self.tabBarController.view addSubview:HUD];
            
            NSDateFormatter *dateTimeFormat = [[NSDateFormatter alloc] init];
            [dateTimeFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSString *assignDateString = [dateTimeFormat stringFromDate:selectedDate];
            
            roomAssignmentModel.roomAssignment_AssignedDate = assignDateString;
            
            UserListModelV2 *selectedUserModel = [userList objectAtIndex:selectedAssignedRoomAttendant];
            
            //            roomAssignmentModel.roomAssignment_PostStatus = POST_STATUS_SAVED_UNPOSTED;
            //            roomAssignmentModel.raIsReassignedRoom = 1;
            if (roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OC || roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OI) {
                roomAssignmentModel.roomAssignmentRoomStatusId = ENUM_ROOM_STATUS_OD;
                
                RoomStatusModelV2 *roomStatusModel = [[RoomStatusModelV2 alloc] init];
                roomStatusModel.rstat_Id = ENUM_ROOM_STATUS_OD;
                [[RoomManagerV2 sharedRoomManager] loadRoomStatusData:roomStatusModel];
                
                [roomAssignment setValue:roomStatusModel.rstat_Code forKey:kRMStatus];
                [roomAssignment setValue:roomStatusModel.rstat_Image forKey:kRoomStatusImage];
            }
            if(isDemoMode)
            {
                roomAssignmentModel.roomAssignmentRoomCleaningStatusId = ENUM_CLEANING_STATUS_PENDING_SECOND_TIME;
                
            }
            else{
                roomAssignmentModel.roomAssignmentRoomCleaningStatusId = ENUM_CLEANING_STATUS_PENDING_FIRST_TIME;
            }
            
            //            [[RoomManagerV2 sharedRoomManager] update:roomAssignmentModel];
            
            //            ReassignRoomAssignmentModel *remodel = [[ReassignRoomAssignmentModel alloc] init];
            //            remodel.reassignRaId = roomAssignmentModel.roomAssignment_Id;
            //            remodel.reassignUserId = roomAssignmentModel.roomAssignment_UserId;
            //            remodel.reassignRoomId = roomAssignmentModel.roomAssignment_RoomId;
            //            remodel.reasignExpectedCleaningTime = roomAssignmentModel.roomAssignmentRoomExpectedCleaningTime;
            //            remodel.reassignRoomStatus = roomAssignmentModel.roomAssignmentRoomStatusId;
            //            remodel.reassignCleaningStatus = roomAssignmentModel.roomAssignmentRoomCleaningStatusId;
            //            if (roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OC || roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OI) {
            //                remodel.reassignRoomStatus = ENUM_ROOM_STATUS_OD;
            //            }
            //            remodel.reassignCleaningStatus = ENUM_CLEANING_STATUS_PENDING_FIRST_TIME;
            //            remodel.reassignDatetime = assignDateString;
            
            RoomModelV2 *roomModel = [[RoomModelV2 alloc] init];
            roomModel.room_Id = [roomAssignment objectForKey:kRoomNo];
            [[RoomManagerV2 sharedRoomManager] loadRoomModel:roomModel];
            
            //            remodel.reassignHousekeeperId = selectedUserModel.userListId;
            //            remodel.reassignRoomType = roomModel.room_TypeId;
            //            remodel.reassignFloorId = roomModel.room_floor_id;
            //            remodel.reassignGuestProfileId = roomAssignmentModel.roomAssignmentGuestProfileId;
            //            remodel.reassignHotelId = roomModel.room_HotelId;
            //            ReassignRoomAssignmentModel* dbRoomAssign = nil;
            //            dbRoomAssign = [[RoomManagerV2 sharedRoomManager] loadReassignRoomAssignmentModelByPrimaryKey:remodel];
            //            if(dbRoomAssign==nil){
            //                [[RoomManagerV2 sharedRoomManager] insertReassignRoomAssignmentModel:remodel];
            //            }
            //            else {
            //                [[RoomManagerV2 sharedRoomManager] updateReassignRoomAssignmentModel:remodel];
            //            }
            
            SyncManagerV2 *syncmanager = [[SyncManagerV2 alloc] init];
            int responseCode = 0;
            if(roomAssignmentModel.roomAssignmentHousekeeperId <= 0) {
                //set first assign property
                roomAssignmentModel.raIsReassignedRoom = 0;
                NSArray *arrayName = [selectedUserModel.userListFullName componentsSeparatedByString:@" "];
                NSString *firstName = nil;
                NSString *lastName = nil;
                
                if (arrayName.count > 2) {
                    firstName = [arrayName objectAtIndex:0];
                    lastName = [arrayName objectAtIndex:arrayName.count - 1];
                } else if (arrayName.count == 1) {
                    firstName = [arrayName objectAtIndex:0];
                }
                
                [roomAssignment setValue:firstName forKey:kRAFirstName];
                [roomAssignment setValue:lastName forKey:kRALastName];
                AssignRoomModelV2 *assignModel = [[AssignRoomModelV2 alloc] init];
                assignModel.assignRoomID = roomAssignmentModel.roomAssignment_RoomId;
                assignModel.assignRoom_assign_id = roomAssignmentModel.roomAssignment_Id;
                assignModel.assignRoom_HouseKeeper_Id = selectedUserModel.userListId;
                assignModel.assignRoom_Assign_Date = assignDateString;
                assignModel.assignRoom_Remark = lblRemark.text;
                assignModel.assignRoom_post_status = POST_STATUS_SAVED_UNPOSTED ;
                assignModel.assignRoom_supervisor_id = [[[UserManagerV2 sharedUserManager] currentUser] userId];
                responseCode = [syncmanager postAssignRoomWithAssignModel:assignModel];
            } else {
                //set assign property
                roomAssignmentModel.raIsReassignedRoom = 1;
                
                if(isDemoMode)
                {
                    responseCode = RESPONSE_STATUS_OK;
                    [[RoomManagerV2 sharedRoomManager] update:roomAssignmentModel];
                }
                else{
                    ReassignRoomAssignmentModel *remodel = [[ReassignRoomAssignmentModel alloc] init];
                    remodel.reassignRaId = roomAssignmentModel.roomAssignment_Id;
                    remodel.reassignUserId = roomAssignmentModel.roomAssignment_UserId;
                    remodel.reassignRoomId = roomAssignmentModel.roomAssignment_RoomId;
                    remodel.reasignExpectedCleaningTime = roomAssignmentModel.roomAssignmentRoomExpectedCleaningTime;
                    remodel.reassignRoomStatus = roomAssignmentModel.roomAssignmentRoomStatusId;
                    if (roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OC || roomAssignmentModel.roomAssignmentRoomStatusId == ENUM_ROOM_STATUS_OI) {
                        remodel.reassignRoomStatus = ENUM_ROOM_STATUS_OD;
                    }
                    remodel.reassignCleaningStatus = ENUM_CLEANING_STATUS_PENDING_FIRST_TIME;
                    remodel.reassignDatetime = assignDateString;
                    remodel.reassignHousekeeperId = selectedUserModel.userListId;
                    remodel.reassignRoomType = roomModel.room_TypeId;
                    remodel.reassignFloorId = roomModel.room_floor_id;
                    remodel.reassignGuestProfileId = roomAssignmentModel.roomAssignmentGuestProfileId;
                    remodel.reassignHotelId = roomModel.room_HotelId;
                    remodel.reassignRemark = lblRemark.text.length > 0 ? lblRemark.text : @"";
                    
                    ReassignRoomAssignmentModel* dbRoomAssign = [[RoomManagerV2 sharedRoomManager] loadReassignRoomAssignmentModelByPrimaryKey:remodel];
                    if ([UserManagerV2 sharedUserManager].currentUserAccessRight.roomAssignmentManualCredit) {
                        remodel.cleaningCredit = tfCleaningCredit.text;
                    }
                    if(dbRoomAssign == nil){
                        [[RoomManagerV2 sharedRoomManager] insertReassignRoomAssignmentModel:remodel];
                    } else {
                        [[RoomManagerV2 sharedRoomManager] updateReassignRoomAssignmentModel:remodel];
                    }
                    
                    responseCode = [syncmanager postReAssignRoomWithReAssignModel:remodel];
                }
            }
            
            if(responseCode == RESPONSE_STATUS_NO_NETWORK) {
                
                //                UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:@"" message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_internet_exception] delegate:nil cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getOK] otherButtonTitles: nil];
                //                [alert show];
                [HUD hide:YES];
                [HUD removeFromSuperview];
                
            } else if(responseCode == RESPONSE_STATUS_ERROR_REASSIGN_DATE_IN_THE_PAST) {
                
                //                UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:@"" message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_assign_date_can_not_be_past] delegate:nil cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getOK] otherButtonTitles: nil];
                //                [alert show];
                //                [HUD hide:YES];
                //                [HUD removeFromSuperview];
                
                if (alertViewPastTime == nil)
                {
                    alertViewPastTime = [[CleaningStatusConfirmPopupView alloc] initWithDelegate:self andOption:CLEANING_STATUS_ALERT_TIME_IN_PAST];
                }
                [alertViewPastTime setConfirmPopupType:CLEANING_STATUS_ALERT_TIME_IN_PAST];
                //[self.tabBarController.view addSubview:alertViewPastTime];
                [self.tabBarController addSubview:alertViewPastTime];
                [HUD hide:YES];
                [HUD removeFromSuperview];
                return;
            } else if(responseCode == RESPONSE_STATUS_ERROR_REASSIGN_OUT_OF_STAFF_WORKSHIFT) {
                
                UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:@"" message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_assignment_out_of_staff_workshift] delegate:nil cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getOK] otherButtonTitles: nil];
                [alert show];
                [HUD hide:YES];
                [HUD removeFromSuperview];
                return;
            } else if(responseCode == RESPONSE_STATUS_ERROR_REASSIGN_OUT_OF_REMAINED_TIME) {
                
                UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:@"" message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_assignment_out_of_remained_time] delegate:nil cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getOK] otherButtonTitles: nil];
                [alert show];
                [HUD hide:YES];
                [HUD removeFromSuperview];
                return;
            } else if(responseCode == RESPONSE_STATUS_ERROR_ASSIGNMENT_NOT_EXIST) {
                
                UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:@"" message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_assignment_not_exist] delegate:nil cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getOK] otherButtonTitles: nil];
                [alert show];
                [HUD hide:YES];
                [HUD removeFromSuperview];
                return;
            } else if(responseCode == RESPONSE_STATUS_ERROR_STAFF_NOT_ASSIGNED) {
                
                UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:@"" message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_assignment_staff_not_assigned] delegate:nil cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getOK] otherButtonTitles: nil];
                [alert show];
                [HUD hide:YES];
                [HUD removeFromSuperview];
                return;
            } else if (responseCode == RESPONSE_STATUS_ERROR_NOT_CURRENT_DAY_ROOM_ASSIGNMENT) {
                UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:@"" message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_assign_time_not_current_day] delegate:nil cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getOK] otherButtonTitles: nil];
                [alert show];
                [HUD hide:YES];
                [HUD removeFromSuperview];
                return;
            } else if(responseCode == RESPONSE_STATUS_ERROR_END_CLEANING_TIME_EXCEED_CURRENT_DAY_ASSIGNMENT) {
                
                UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:@"" message:[L_end_cleaning_time_exceed_current_day_assignment currentKeyToLanguage] delegate:nil cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getOK] otherButtonTitles: nil];
                [alert show];
                [HUD hide:YES];
                [HUD removeFromSuperview];
                return;
            }else {
                roomAssignmentModel.roomAssignmentHousekeeperId = selectedUserModel.userListId;
                [[RoomManagerV2 sharedRoomManager] update:roomAssignmentModel];
                [HUD hide:YES];
                [HUD removeFromSuperview];
            }
            
        }
            break;
            
        case 1:
            //No
            return;
            
        default:
            break;
    }
    
    //Accross Room Assignment Detail
    [self navigateToParentView];
}

#pragma mark - Private Methods

-(void)loadData:(MBProgressHUD *)HUD
{
    int currentUserId = [UserManagerV2 sharedUserManager].currentUser.userId;
    NSString *lastModifiedDate = [[UserManagerV2 sharedUserManager] getUserLastModifiedDate];
    [[AdHocMessageManagerV2 sharedAdHocMessageManagerV2] getUserListWith:currentUserId andHotelID:[UserManagerV2 sharedUserManager].currentUser.userHotelsId andLastModified:lastModifiedDate AndPercentView:nil];
    //[[AdHocMessageManagerV2 sharedAdHocMessageManagerV2] loadWSAttendantByUserId:[UserManagerV2 sharedUserManager].currentUser.userId hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId];
    
    //hide saving data.
    [self performSelector:@selector(loadDataSuccess:) withObject:HUD afterDelay:0.f];
}

-(void) loadDataSuccess:(MBProgressHUD *)HUD{
    
    [btnSetTime setBackgroundImage:[UIImage imageNamed:imgBtnYes] forState:UIControlStateNormal];
    [btnSetTime setBackgroundImage:[UIImage imageNamed:imgBtnYesClicked] forState:UIControlEventTouchUpInside];
    
    [tbViewPriority setScrollEnabled:NO];
    
    priorities = [[NSArray alloc] initWithObjects:[L_strhigh currentKeyToLanguage], [L_strlow currentKeyToLanguage], nil];
    //NSArray *temp = [[AdHocMessageManagerV2 sharedAdHocMessageManagerV2] loadAllUserListBySupervisorId:self.roomAssignmentModel.roomAssignment_UserId];
    int hotelId = [[[UserManagerV2 sharedUserManager] currentUser] userHotelsId];
    
    NSMutableArray *temp = [[AdHocMessageManagerV2 sharedAdHocMessageManagerV2] loadAllUserListDataByUserHotelId:hotelId];
    //Remove Supervisor out of list user receive message
    NSMutableArray *listToRemove = [[NSMutableArray alloc] init];
    for (UserListModelV2 *curUser in temp) {
        if(curUser.usrSupervisor != 0) {
            [listToRemove addObject:curUser];
        }
    }
    
    for(UserListModelV2 *curUser in listToRemove){
        [temp removeObject:curUser];
    }
    NSArray *sortedUserList = [temp sortedArrayUsingComparator:^NSComparisonResult(UserListModelV2 *user1, UserListModelV2 *user2) {
        NSString *userName1 = user1.userListFullName;
        NSString *userName2 = user2.userListFullName;
        return [userName1 compare:userName2];
    }];
    
    UserListModelV2 *unassignedUser = nil;
    //Only assign room has housekeeper id
    if(roomAssignmentModel.roomAssignmentHousekeeperId > 0) {
        //Add Unassigned - Temporary model for displaying unassign
        unassignedUser = [[UserListModelV2 alloc] init];
        unassignedUser.userListFullName = @"Unassigned";
        unassignedUser.userListFullNameLang = [L_title_unassigned currentKeyToLanguage];
        unassignedUser.userListName = @"Unassigned";
        unassignedUser.userListId = 0;
        
        [self setEnableBtnTime:NO];
    } else {
        [self setEnableBtnTime:YES];
    }
    
    if (sortedUserList.count > 0) {
        userList = [NSMutableArray arrayWithArray:sortedUserList];
        if (unassignedUser) {
            [userList insertObject:unassignedUser atIndex:0];
        }
    } else {
        userList = [NSMutableArray array];
        if (unassignedUser) {
            [userList addObject:unassignedUser];
        }
    }
    
    if ([userList count] > 9) {
        [tbViewRoomAttendant setScrollEnabled:YES];
    }
    else {
        [tbViewRoomAttendant setScrollEnabled:NO];
    }
    
    NSString *raFirstName = (NSString *)[roomAssignment objectForKey:kRAFirstName];
    NSString *raLastName = (NSString *)[roomAssignment objectForKey:kRALastName];
    NSString *raFullName = [NSString stringWithFormat:@"%@ %@", raFirstName, raLastName];
    
    selectedPriorityIndex = -1;
    selectedAssignedRoomAttendant = -1;
    NSInteger index = 0;
    for (UserListModelV2 *user in userList) {
        NSString *trimmedString = [[user.userListFullName stringByTrimmingCharactersInSet:
                                   [NSCharacterSet whitespaceCharacterSet]] lowercaseString];
        NSString *raName = [[raFullName stringByTrimmingCharactersInSet:
                           [NSCharacterSet whitespaceCharacterSet]] lowercaseString];
        if ([trimmedString isEqualToString:raName]) {
            selectedPriorityIndex = index;
            selectedAssignedRoomAttendant = index;
        }
        index++;
    }
    if (selectedPriorityIndex > 0) {
        [lblAssignedRoomAttendant setText:((UserListModelV2 *)[userList objectAtIndex:selectedAssignedRoomAttendant]).userListFullName];
//        [lblAssignedRoomAttendant setText:raFullName];
        [self setEnableBtnTime:YES];
    }else{
        selectedPriorityIndex = 0;
        selectedAssignedRoomAttendant = 0;
        [lblAssignedRoomAttendant setText:[L_title_unassigned currentKeyToLanguage]];
    }
    
    [lblRoomAttendantName setText:raFullName];
    
    
    NSString *roomStatusCode = (NSString *)[roomAssignment objectForKey:kRMStatus];
    [lblRoomStatus setText:roomStatusCode];
    [lblRoomStatus setTextColor:[self colorForLabel:(int)roomAssignmentModel.roomAssignmentRoomStatusId]];
    
    NSData *imageData = (NSData *)[roomAssignment objectForKey:kRoomStatusImage];
    [imgRoomStatusIcon setImage:[UIImage imageWithData:imageData]];
    
    selectedPriorityIndex = 0;
    [lblPriority setText:[priorities objectAtIndex:selectedPriorityIndex]];
    
//    if ([userList count] > 0) {
//        [lblAssignedRoomAttendant setText:((UserListModelV2 *)[userList objectAtIndex:selectedAssignedRoomAttendant]).userListFullName];
//    }
//    else {
//        [lblAssignedRoomAttendant setText:@""];
//    }
    
    
    selectedDate = [NSDate date];
    [datePicker setDate:selectedDate];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"hh:mm a"];
    
    [lblTime setText:[dateFormat stringFromDate:selectedDate]];
    [lblRemark setText:@""];
    
    [self insertTopBarView];
    [self addButtonHandleShowHideTopbar];
    [self reLayoutForSmallerUserTable];
    
    UIToolbar *backGroundPickerView = [[UIToolbar alloc] initWithFrame: datePicker.frame];
    backGroundPickerView.translucent = YES;
    [backGroundPickerView.layer setCornerRadius:8.0f];
    [viewTimePicker insertSubview:backGroundPickerView belowSubview:datePicker];
    
    [self modifyAllViews];
    
    [HUD hide:YES];
    [HUD removeFromSuperview];
}

-(void) insertTopBarView
{
    if (topBarView == nil) {
        SuperRoomModelV2 *superModel = [[SuperRoomModelV2 alloc] init];
        topBarView = [[TopbarViewV2 alloc] initViewWithSuperModel:superModel];
        [self.view addSubview:topBarView];
    } else {
        [topBarView refresh:[[SuperRoomModelV2 alloc] init]];
    }
    [topBarView setHidden:YES];
}

-(void)addButtonHandleShowHideTopbar {
    UIView *vBarButton = [[UIView alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
    UIButton *titleBarButtonFirst = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 200, 30)];
    [titleBarButtonFirst setTag:1];
    titleBarButtonFirst.titleLabel.backgroundColor = [UIColor clearColor];
    [titleBarButtonFirst.titleLabel setFont: FONT_BUTTON_TOPBAR];
    titleBarButtonFirst.titleLabel.textAlignment = NSTextAlignmentCenter;
    [titleBarButtonFirst.titleLabel setAdjustsFontSizeToFitWidth:YES];
    titleBarButtonFirst.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
    
    [titleBarButtonFirst.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
    [titleBarButtonFirst.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
    [titleBarButtonFirst addTarget:self action:@selector(hiddenHeaderView) forControlEvents:UIControlEventTouchUpInside];
    [titleBarButtonFirst setTitle:[roomAssignment objectForKey:kRoomNo] forState:UIControlStateNormal];
    
    [vBarButton addSubview:titleBarButtonFirst];
    
    UIButton *titleBarButtonSecond = [[UIButton alloc] initWithFrame:CGRectMake(0, 22, 200, 22)];
    [titleBarButtonSecond setTag:2];
    [titleBarButtonSecond setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [titleBarButtonSecond.titleLabel setFont:FONT_BUTTON_TOPBAR_SECOND];
    [titleBarButtonSecond addTarget:self action:@selector(hiddenHeaderView) forControlEvents:UIControlEventTouchUpInside];
    
    if(roomAssignmentModel.roomAssignmentHousekeeperId <= 0) {
        [titleBarButtonSecond setTitle:[L_ASSIGN_ROOM currentKeyToLanguage] forState:UIControlStateNormal];
    } else {
        [titleBarButtonSecond setTitle:[L_reassign_room currentKeyToLanguage] forState:UIControlStateNormal];
    }
    
    [vBarButton addSubview:titleBarButtonSecond];
    
    [self.navigationItem setTitleView:vBarButton];
    
    //Move title to center vertical
    CGRect titleViewBounds = self.navigationItem.titleView.bounds;
    int moveLeftTitle = 0;
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        moveLeftTitle = 10;
    }
    self.navigationItem.titleView.bounds = CGRectMake(titleViewBounds.origin.x + moveLeftTitle, Y_TOPBAR_TITLE, titleViewBounds.size.width, titleViewBounds.size.height);
    
    //back button navigation
    self.navigationItem.leftBarButtonItem = [MyNavigationBarV2 createBackButtonWithTarget:self selector:@selector(btnBackPressed) forControlEvents:UIControlEventTouchUpInside];
}

-(void)btnBackPressed
{
    //[self.navigationController popViewControllerAnimated:YES];
    //Skip Room Detail View
    UIViewController *viewController = nil;
    for (UIViewController *vc in self.navigationController.viewControllers) {
        if ([vc isKindOfClass:[RoomAssignmentInfoViewController class]]) {
            viewController = vc;
            break;
        }
    }
    if (viewController != nil) {
        [self.navigationController popToViewController:viewController animated:YES];
    }else{
        NSArray *arrayViews = self.navigationController.viewControllers;
        int indexVC = (int)([arrayViews count] - 3);
        
        if(isPushFrom == IS_PUSHED_FROM_ROOM_STATUS) {
            indexVC = indexVC - 1;
        }
        
        if(indexVC >= 0)
        {
            UIViewController *controller = controller = [self.navigationController.viewControllers objectAtIndex:indexVC];
            [self.navigationController popToViewController:controller animated:YES];
        } else {
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
    }
}

//Navigate accross RoomAssignmentInfoViewController
-(void)navigateToParentView
{
    //Check is from find room or not
    NSArray *arrayVC = self.navigationController.viewControllers;
    RoomAssignmentInfoViewController *roomAssignmentDetail;
    for (UIViewController *currentVC in arrayVC) {
        if([currentVC isKindOfClass:[RoomAssignmentInfoViewController class]])
        {
            roomAssignmentDetail = (RoomAssignmentInfoViewController*) currentVC;
            break;
        }
    }
    
    if(roomAssignmentDetail)
    {
//        if(roomAssignmentDetail.isPushFrom == IS_PUSHED_FROM_FIND_BY_ROOM)
//        {
//            //Find FindByRoom
//            for (UIViewController *currentVC in arrayVC) {
//                if([currentVC isKindOfClass:[FindByRoomViewV2 class]])
//                {
//                    [self.navigationController popToViewController:currentVC animated:YES];
//                    return;
//                }
//            }
//        }
        if(roomAssignmentDetail.isPushFrom == IS_PUSHED_FROM_ROOM_ASSIGNMENT)
        {
            if([arrayVC count]>=4){
                UIViewController *viewToShow = [arrayVC objectAtIndex:[arrayVC count] - 4];
                [self.navigationController popToViewController:viewToShow animated:YES];
            }
            else{
                [self.navigationController popToRootViewControllerAnimated:YES];
            }
        }
        else if(roomAssignmentDetail.isPushFrom == IS_PUSHED_FROM_ROOM_STATUS) {
            UIViewController *viewToShow = [arrayVC objectAtIndex:[arrayVC count] - 4];
            [self.navigationController popToViewController:viewToShow animated:YES];
        }
        else if(roomAssignmentDetail.isPushFrom == IS_PUSHED_FROM_FIND_BY_RA || roomAssignmentDetail.isPushFrom == IS_PUSHED_FROM_FIND_BY_ROOM) {
            UIViewController *viewToShow = [arrayVC objectAtIndex:[arrayVC count] - 4];
            [self.navigationController popToViewController:viewToShow animated:YES];
        }
        else {
            if([arrayVC count] > 2){
                UIViewController *viewToShow = [arrayVC objectAtIndex:[arrayVC count] - 2];
                [self.navigationController popToViewController:viewToShow animated:YES];
            }
            else
            {
                [self.navigationController popViewControllerAnimated:YES];
            }
            return;
        }
    }
    
//    [self.navigationController popViewControllerAnimated:YES];
}

- (void) hiddenHeaderView {
    BOOL isHidden = topBarView.hidden;
    [topBarView setHidden:!isHidden];
    
    CGRect scviewReassignFrame = scviewReassignment.frame;
    CGRect viewReassignFrame = viewReassignment.frame;
    
    CGFloat scale;
    CGFloat expandContentSize;
    if (isHidden) {
        scale = 45;
        expandContentSize = 15;
    }
    else {
        scale = -45;
        expandContentSize = -15;
    }
    
    scviewReassignFrame.origin.y += scale;
    scviewReassignFrame.size.height -= scale;
    
    viewReassignFrame.size.height += expandContentSize;
    
    [scviewReassignment setFrame:scviewReassignFrame];
    [scviewReassignment setContentSize:viewReassignFrame.size];
}

-(void) reLayoutForSmallerUserTable
{
    if ([userList count] >= 9) {
        return;
    }
    CGRect tableViewFrame = tbViewRoomAttendant.frame;
    CGFloat tableHeight = [userList count]*tableViewRoomAttendantCellHeight;
    tableViewFrame.origin.y = 240 - tableHeight/2;
    tableViewFrame.size.height = tableHeight + 20;
    
    [tbViewRoomAttendant setFrame:tableViewFrame];
    [tbViewRoomAttendant setScrollEnabled:YES];
}

-(UIColor *) colorForLabel:(enum ENUM_ROOM_STATUS) roomStatus{
    switch (roomStatus) {
        case ENUM_ROOM_STATUS_VD:
        case ENUM_ROOM_STATUS_OD:
            return [UIColor redColor];
            
        case ENUM_ROOM_STATUS_VPU:
        case ENUM_ROOM_STATUS_OPU:
            return [UIColor yellowColor];
            
        case ENUM_ROOM_STATUS_OOO:
        case ENUM_ROOM_STATUS_OOS:
            return [UIColor grayColor];
            
        case ENUM_ROOM_STATUS_VC:
        case ENUM_ROOM_STATUS_OC:
        case ENUM_ROOM_STATUS_OC_SO:
            return [UIColor colorWithRed:0.0f green:1.0f blue:1.0f alpha:1.0f];
            
        case ENUM_ROOM_STATUS_VI:
        case ENUM_ROOM_STATUS_OI:
            return [UIColor greenColor];
            
        default:
            return [UIColor blackColor];
    }
}

-(void) setEnableBtnTime:(BOOL) isEnable
{
    if (isEnable) {
        [btnTime setEnabled:YES];
        [lblTime setEnabled:YES];
        [btnTime setAlpha:1];
        [lblTime setAlpha:1];
    } else {
        [btnTime setEnabled:NO];
        [lblTime setEnabled:NO];
        [btnTime setAlpha:0.7];
        [lblTime setAlpha:0.3];
    }
}

#pragma mark - Resize All Views

//Re-locate all views
-(void)modifyAllViews
{
    BOOL isTopBarHidden = topBarView.hidden;
    
    CGRect scviewReassignFrame = scviewReassignment.frame;
    CGRect viewReassignFrame = viewReassignment.frame;
    
    CGFloat scale;
    CGFloat expandContentSize;
    int deviceKind = [DeviceManager getDeviceScreenKind];
    if (isTopBarHidden) {
        scviewReassignFrame.origin.y = 0;
        expandContentSize = 15;
        scale = 45;
    }
    else {
        scale = -45;
        scviewReassignFrame.origin.y = 45;
        expandContentSize = -15;
    }
    
    scviewReassignFrame.size.height -= scale;
    if(deviceKind != DeviceScreenKindRetina4_0){
        viewReassignFrame.size.height += expandContentSize;
    }
    [scviewReassignment setFrame:scviewReassignFrame];
    [scviewReassignment setContentSize:viewReassignFrame.size];
}
-(void)loadFlatResource
{
    [imgTop setImage:[UIImage imageNamed:imgGuestInfoActiveFlat]];
    [btnSubmit setBackgroundImage:[UIImage imageNamed:imgBtnSubmitFlat] forState:UIControlStateNormal];
    [btnAssignTo setBackgroundImage:[UIImage imageNamed:imgOptionSetting_Flat] forState:UIControlStateNormal];
    [btnTime setBackgroundImage:[UIImage imageNamed:imgOptionSetting_Flat] forState:UIControlStateNormal];
    [btnPriority setBackgroundImage:[UIImage imageNamed:imgOptionSetting_Flat] forState:UIControlStateNormal];
}
@end
