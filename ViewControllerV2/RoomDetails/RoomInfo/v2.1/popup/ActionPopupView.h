//
//  ActionPopupView.h
//  mHouseKeeping
//
//  Created by Khanh Phan on 1/29/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ActionPopupDelegate <NSObject>

-(void)actionPopupViewDidDismissWithButtonIndex:(NSInteger)buttonIndex;

@end

@interface ActionPopupView : UIView
{
    IBOutlet UIImageView *imgTop;
    IBOutlet UIImageView *imgBody;
    IBOutlet UIImageView *imgBottomAction;
    IBOutlet UILabel *lblHeader;
    
    IBOutlet UIView *viewPopup;
    
    NSArray *btnTitles;
    BOOL haveCancelButton;
}

-(id) initWithTitles:(NSArray *)buttonTitles delegate:(id<ActionPopupDelegate>)delegator option:(BOOL)withCancelButton;

@property (nonatomic, strong) id<ActionPopupDelegate> delegator;

@end
