//
//  ActionPopupView.m
//  mHouseKeeping
//
//  Created by Khanh Phan on 1/29/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "ActionPopupView.h"
#import "DeviceManager.h"
#import "ehkDefinesV2.h"
#import "UIImage+Tint.h"

#define buttonHeight                    45
#define buttonWidth                     280
#define space                           10
#define spaceBeforeCancelButton         10

#define popupWidth                      300
#define popupMargin                     10

#define topPopupFrame                   CGRectMake(0, 0, 300, 50);
#define bodyPopupOrigin                 CGPointMake(0,50)
#define bottomPopupSize                 CGSizeMake(300,15)

@implementation ActionPopupView

@synthesize delegator;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
    }
    return self;
}

-(id) initWithTitles:(NSArray *)buttonTitles delegate:(id<ActionPopupDelegate>)_delegator option:(BOOL)withCancelButton{
    
    int frameHeight = DeviceScreenSizeStandardHeight3_5;
    int deviceKind = [DeviceManager getDeviceScreenKind];
    if(deviceKind == DeviceScreenKindRetina4_0){
        frameHeight = DeviceScreenSizeStandardHeight4_0;
    }
    self = [super initWithFrame:CGRectMake(0, 0, 320, frameHeight)];
    [self setBackgroundColor:[UIColor clearColor]];
    if (self) {
        NSArray *nibViews = [[NSBundle mainBundle] loadNibNamed:@"ActionPopupView" owner:self options:nil];
        UIView *actionPopupView = [nibViews objectAtIndex:0];
        [actionPopupView setFrame:self.frame];
        [self addSubview:actionPopupView];
        btnTitles = buttonTitles;
        haveCancelButton = withCancelButton;
        [self setDelegator:_delegator];
        
        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
            [self loadFlatResource];
        }
    }
    return self;
}

-(void)willMoveToSuperview:(UIView *)newSuperview {
    [super willMoveToSuperview:newSuperview];
    
    [lblHeader setText:[L_action currentKeyToLanguage]];
    CGRect frame;
    
    NSInteger buttonCount = [btnTitles count] + (haveCancelButton?1:0);
    NSInteger bodyPopupHeight = buttonCount*53 + (haveCancelButton?spaceBeforeCancelButton:space);
    
    frame.size = CGSizeMake(popupWidth, bodyPopupHeight + 50 + 15);
    frame.origin = CGPointMake(10, 240.0f - frame.size.height/2.0f);
    [viewPopup setFrame:frame];
    
    CGRect bodyFrame = imgBody.frame;
    bodyFrame.size.height = bodyPopupHeight;
    [imgBody setFrame:bodyFrame];
    
    CGRect bottomFrame = imgBottomAction.frame;
    bottomFrame.origin.y = frame.size.height - 15;
    [imgBottomAction setFrame:bottomFrame];
    
    CGPoint startPoint = CGPointMake(10, 50);
    int tagButton = 0;
    for (NSString *btnTitle in btnTitles) {
        startPoint.y += space;
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(startPoint.x, startPoint.y, buttonWidth, buttonHeight)];
        [button setBackgroundImage:[UIImage imageBeforeiOS7:imgBtnSelectionPopup equaliOS7:imgBtnSelectionPopupFlat] forState:UIControlStateNormal];
        [button setTitle:btnTitle forState:UIControlStateNormal];
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [button.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:22]];
        
        [button addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        startPoint.y += buttonHeight;
        
        [button setTag:tagButton];
        tagButton++;
        
        [viewPopup addSubview:button];
    }
    
    if (haveCancelButton) {
        startPoint.y += spaceBeforeCancelButton;
        UIButton *btnCancel = [[UIButton alloc] initWithFrame:CGRectMake(startPoint.x, startPoint.y, buttonWidth, buttonHeight)];
        
        [btnCancel setBackgroundImage:[UIImage imageBeforeiOS7:imgBtnCancelPopup equaliOS7:imgBtnCancelPopupFlat] forState:UIControlStateNormal];
        [btnCancel setTitle:[L_CANCEL currentKeyToLanguage] forState:UIControlStateNormal];
        [btnCancel setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btnCancel.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:22]];
        [btnCancel addTarget:self action:@selector(dismissPopup) forControlEvents:UIControlEventTouchUpInside];
        [btnCancel setTag:buttonCount];
        
        [viewPopup addSubview:btnCancel];
    }

}

- (void)drawRect:(CGRect)rect
{
    // Drawing code
}

-(void) buttonClicked:(id) sender
{
    [delegator actionPopupViewDidDismissWithButtonIndex:[sender tag]];
}

-(void) dismissPopup
{
    //UIButton *btnCancel = (UIButton *)[self viewWithTag:[btnTitles count]];
    //[btnCancel setBackgroundImage:[UIImage imageNamed:imgBtnCancelPopupClicked] forState:UIControlStateNormal];
    [self removeFromSuperview];
}

#pragma mark - iOS 7 Resources
-(void)loadFlatResource
{
    
    [imgTop setImage:[UIImage imageNamed:imgBGPopupTopFlat]];
    [imgBody setImage:[UIImage imageNamed:imgBGPopupMidFlat]];
    [imgBottomAction setImage:[UIImage imageNamed:imgBgPopupBottomFlat]];
    [lblHeader setTextColor:[UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.7f]];
    
    
}

@end
