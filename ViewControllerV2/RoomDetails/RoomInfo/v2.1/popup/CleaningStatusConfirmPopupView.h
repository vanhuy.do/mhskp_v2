//
//  CleaningStatusConfirmPopupView.h
//  mHouseKeeping
//
//  Created by Khanh Phan on 2/12/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

enum CLEANING_STATUS_CONFIRM_POPUP
{
    CLEANING_STATUS_CONFIRM_POPUP_DND = 1,
    CLEANING_STATUS_CONFIRM_POPUP_DECLINED_SERVICE = 2,
    CLEANING_STATUS_CONFIRM_POPUP_SERVICE_LATER = 3,
    CLEANING_STATUS_CONFIRM_POPUP_SERVICE_LATER_REMINDER = 4,
    CLEANING_STATUS_CONFIRM_POPUP_COMPLETE = 6,
    CLEANING_STATUS_CONFIRM_POPUP_PENDING = 5,
    CLEANING_STATUS_CONFIRM_POPUP_STOP = 7,
    CLEANING_STATUS_ALERT_TIME_IN_PAST = 8,
    CLEANING_STATUS_CONFIRM_POPUP_DOUBLE_LOCK = 9
    
};

@protocol CleaningStatusConfirmPopupDelegate <NSObject>

//-(void) btnNoClicked;
-(void) btnCloseClicked;
-(void) btnYesClicked;

@end

@interface CleaningStatusConfirmPopupView : UIView
{
    IBOutlet UILabel *lblCaption;
    IBOutlet UIButton *btnYes;
    IBOutlet UIButton *btnNo;
    IBOutlet UIButton *btnClose;
    
    IBOutlet UIView *viewPopup;
    
    IBOutlet UIImageView *imgviewPopupIcon;
    IBOutlet UIImageView *imgHeaderPopup;
    IBOutlet UIImageView *imgBodyPopup;
    IBOutlet UIImageView *imgBottomPopup;
    
    IBOutlet UILabel *lblRoomNo;
    IBOutlet UILabel *lblServiceLaterTime;
    IBOutlet UILabel *lblMessage;
    IBOutlet UILabel *lblAlertPastTime;
    IBOutlet UILabel *lblBtnYes;
    IBOutlet UILabel *lblBtnNo;
    IBOutlet UILabel *lblCleaningAt;
    
    IBOutlet UIDatePicker *datePicker;
    
    UIView *serviceLaterBackgroundTime;
    id<CleaningStatusConfirmPopupDelegate> delegate;
}

extern const int expandHeight;

@property (nonatomic, strong) NSDate *selectedDate;
@property (nonatomic, assign) enum CLEANING_STATUS_CONFIRM_POPUP confirmPopupType;
@property (nonatomic, strong) NSString *roomNo;

-(id) initWithDelegate:(id<CleaningStatusConfirmPopupDelegate>) delegator andOption:(enum CLEANING_STATUS_CONFIRM_POPUP)confirmPopupType;

-(IBAction) btnYesClicked:(id)sender;
-(IBAction) btnNoClicked:(id)sender;
-(IBAction) btnCloseClicked:(id)sender;

@end
