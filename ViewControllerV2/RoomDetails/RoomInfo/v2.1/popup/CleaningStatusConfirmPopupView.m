//
//  CleaningStatusConfirmPopupView.m
//  mHouseKeeping
//
//  Created by Khanh Phan on 2/12/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "CleaningStatusConfirmPopupView.h"
#import "LogFileManager.h"
#import "DeviceManager.h"
#import "ehkConvert.h"
#import "LanguageManagerV2.h"

#define timeFormat                                          @"hh:mm a"
#define timePickerFrame                                     CGRectMake(53,120,195,216)

const int expandHeight = 220;

@implementation CleaningStatusConfirmPopupView

@synthesize confirmPopupType;
@synthesize selectedDate;
@synthesize roomNo;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(id) initWithDelegate:(id<CleaningStatusConfirmPopupDelegate>) delegator andOption:(enum CLEANING_STATUS_CONFIRM_POPUP)confirmType
{
    self = [super initWithFrame:CGRectMake(0, 0, 320, 480)];
    [self setBackgroundColor:[UIColor clearColor]];
    if (self) {
        NSArray *nibViews = [[NSBundle mainBundle] loadNibNamed:@"CleaningStatusConfirmPopupView" owner:self options:nil];
        
        UIView *cleaningStatusConfirmView = [nibViews objectAtIndex:0];
        
        //Check to set frame
        if([DeviceManager getDeviceScreenKind] == DeviceScreenKindRetina4_0){
            [cleaningStatusConfirmView setFrame:CGRectMake(0, 0, 320, DeviceScreenSizeStandardHeight4_0)];
        } else {
            [cleaningStatusConfirmView setFrame:CGRectMake(0, 0, 320, DeviceScreenSizeStandardHeight3_5)];
        }
        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
            [self loadFlatResource];
        }
        [self addSubview:cleaningStatusConfirmView];
        delegate = delegator;
        [self setConfirmPopupType:confirmType];
    }
    return self;
}

-(void) willMoveToSuperview:(UIView *)newSuperview
{
    NSString *message = nil;
    NSString *imgIcon = nil;
    switch (confirmPopupType) {
        case CLEANING_STATUS_CONFIRM_POPUP_DND:
        {
            message = [NSString stringWithFormat:@"%@ %@", [L_strdnd currentKeyToLanguage], @"?"];
            if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                imgIcon = imgDNDFlat;
                [lblCaption setTextColor:[UIColor darkGrayColor]];
                [lblMessage setTextColor:[UIColor blackColor]];
            } else {
                imgIcon = imgDND;
            }
            
            [btnNo setHidden:NO];
            [lblBtnNo setHidden:NO];
            [btnYes setHidden:NO];
            [lblBtnYes setHidden:NO];
            [lblMessage setHidden:NO];
            [datePicker setHidden:YES];
            [btnClose setHidden:YES];
            [lblCleaningAt setHidden:YES];
            [lblServiceLaterTime setHidden:YES];
            [serviceLaterBackgroundTime setHidden:YES];
            [lblAlertPastTime setHidden:YES];
        }
            break;
        // CRF [Huy][20161108/CRF-00001431]
        case CLEANING_STATUS_CONFIRM_POPUP_DOUBLE_LOCK:
        {
            message = [NSString stringWithFormat:@"%@ %@", [L_strdouble_lock currentKeyToLanguage], @"?"];
            if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                imgIcon = imgDoubleLockFlat;
                [lblCaption setTextColor:[UIColor darkGrayColor]];
                [lblMessage setTextColor:[UIColor blackColor]];
            } else {
                imgIcon = imgDoubleLockFlat;
            }
            
            [btnNo setHidden:NO];
            [lblBtnNo setHidden:NO];
            [btnYes setHidden:NO];
            [lblBtnYes setHidden:NO];
            [lblMessage setHidden:NO];
            [datePicker setHidden:YES];
            [btnClose setHidden:YES];
            [lblCleaningAt setHidden:YES];
            [lblServiceLaterTime setHidden:YES];
            [serviceLaterBackgroundTime setHidden:YES];
            [lblAlertPastTime setHidden:YES];
            break;
        }
        case CLEANING_STATUS_CONFIRM_POPUP_PENDING:
        {
            message = [NSString stringWithFormat:@"%@ %@", [L_strpending currentKeyToLanguage], @"?"];
            
            if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                imgIcon = imgPendingFlat;
                [lblCaption setTextColor:[UIColor darkGrayColor]];
                [lblMessage setTextColor:[UIColor blackColor]];
            } else {
                imgIcon = imgPending;
            }
            
            [btnNo setHidden:NO];
            [lblBtnNo setHidden:NO];
            [btnYes setHidden:NO];
            [lblBtnYes setHidden:NO];
            [lblMessage setHidden:NO];
            [datePicker setHidden:YES];
            [btnClose setHidden:YES];
            [lblCleaningAt setHidden:YES];
            [lblServiceLaterTime setHidden:YES];
            [serviceLaterBackgroundTime setHidden:YES];
            [lblAlertPastTime setHidden:YES];
        }
            break;
            
        case CLEANING_STATUS_CONFIRM_POPUP_COMPLETE:
        {
            message = [NSString stringWithFormat:@"%@ %@", [L_strcomplete currentKeyToLanguage], @"?"];
            if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                imgIcon = imgCompleteFlat;
                [lblCaption setTextColor:[UIColor darkGrayColor]];
                [lblMessage setTextColor:[UIColor blackColor]];
            } else {
                imgIcon = imgComplete;
            }
            
            [btnNo setHidden:NO];
            [lblBtnNo setHidden:NO];
            [btnYes setHidden:NO];
            [lblBtnYes setHidden:NO];
            [lblMessage setHidden:NO];
            [datePicker setHidden:YES];
            [btnClose setHidden:YES];
            [lblCleaningAt setHidden:YES];
            [lblServiceLaterTime setHidden:YES];
            [serviceLaterBackgroundTime setHidden:YES];
            [lblAlertPastTime setHidden:YES];
        }
            break;
            
        case CLEANING_STATUS_CONFIRM_POPUP_SERVICE_LATER:
        {
//            message = CleaningStatusConfirmMessageServiceLater;
            message = [NSString stringWithFormat:@"%@ %@", [L_service_later currentKeyToLanguage], @"?"];
            if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                imgIcon = imgServiceLaterFlat;
                [lblCaption setTextColor:[UIColor darkGrayColor]];
                [lblMessage setTextColor:[UIColor blackColor]];
            } else {
                imgIcon = imgServiceLater;
            }
            
            [datePicker setDate:[NSDate date]];
            [btnNo setHidden:NO];
            [lblBtnNo setHidden:NO];
            [btnYes setHidden:NO];
            [lblBtnYes setHidden:NO];
            [lblMessage setHidden:NO];
            [datePicker setHidden:NO];
            [btnClose setHidden:YES];
            [lblCleaningAt setHidden:YES];
            [lblServiceLaterTime setHidden:YES];
            [lblAlertPastTime setHidden:YES];
            
            [datePicker setFrame:timePickerFrame];
            
            if(!serviceLaterBackgroundTime) {
                serviceLaterBackgroundTime = [[UIView alloc] initWithFrame:timePickerFrame];
                [serviceLaterBackgroundTime setBackgroundColor:[UIColor whiteColor]];
                [serviceLaterBackgroundTime setAlpha:0.8f];
                [serviceLaterBackgroundTime.layer setCornerRadius:10.0f];
            }
            [serviceLaterBackgroundTime setHidden:NO];
            [datePicker.superview insertSubview:serviceLaterBackgroundTime belowSubview:datePicker];
        }
            break;
            
        case CLEANING_STATUS_CONFIRM_POPUP_DECLINED_SERVICE:
        {
//            message = CleaningStatusConfirmMessageDeclinedService;
            message = [NSString stringWithFormat:@"%@ %@", [L_declined_service currentKeyToLanguage], @"?"];
            if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                imgIcon = imgDeclinedServiceFlat;
                [lblCaption setTextColor:[UIColor darkGrayColor]];
                [lblMessage setTextColor:[UIColor blackColor]];
            } else {
                imgIcon = imgDeclinedService;
            }
            
            [btnNo setHidden:NO];
            [lblBtnNo setHidden:NO];
            [btnYes setHidden:NO];
            [lblBtnYes setHidden:NO];
            [lblMessage setHidden:NO];
            [datePicker setHidden:YES];
            [btnClose setHidden:YES];
            [lblCleaningAt setHidden:YES];
            [lblServiceLaterTime setHidden:YES];
            [serviceLaterBackgroundTime setHidden:YES];
            [lblAlertPastTime setHidden:YES];
        }
            break;
            
        case CLEANING_STATUS_CONFIRM_POPUP_SERVICE_LATER_REMINDER:
        {
            message = @"";
            if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                imgIcon = imgServiceLaterFlat;
                [lblCleaningAt setTextColor:[UIColor blackColor]];
                [lblCaption setTextColor:[UIColor darkGrayColor]];
            } else {
                imgIcon = imgServiceLater;
            }
            [btnNo setHidden:YES];
            [lblBtnNo setHidden:YES];
            [btnYes setHidden:YES];
            [lblBtnYes setHidden:YES];
            [lblMessage setHidden:YES];
            [datePicker setHidden:YES];
            [btnClose setHidden:NO];
            [lblCleaningAt setHidden:NO];
            [lblServiceLaterTime setHidden:NO];
            [serviceLaterBackgroundTime setHidden:YES];
            [lblAlertPastTime setHidden:YES];
        }
            break;
            
        case CLEANING_STATUS_CONFIRM_POPUP_STOP:
        {
            //message = CleaningStatisConfirmMessageStop;
            message = [NSString stringWithFormat:@"%@ %@", [L_stop currentKeyToLanguage], @"?"];
            
            if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                imgIcon = imgStopFlat;
                [lblCaption setTextColor:[UIColor darkGrayColor]];
                [lblMessage setTextColor:[UIColor blackColor]];
            } else {
                imgIcon = imgStop;
            }
            
            [btnNo setHidden:NO];
            [lblBtnNo setHidden:NO];
            [btnYes setHidden:NO];
            [lblBtnYes setHidden:NO];
            [lblMessage setHidden:NO];
            [datePicker setHidden:YES];
            [btnClose setHidden:YES];
            [lblCleaningAt setHidden:YES];
            [lblServiceLaterTime setHidden:YES];
            [serviceLaterBackgroundTime setHidden:YES];
            [lblAlertPastTime setHidden:YES];
        }
            break;
        case CLEANING_STATUS_ALERT_TIME_IN_PAST:
        {
            message = [L_assign_date_can_not_be_past currentKeyToLanguage];
            [lblCaption setText:[L_alert currentKeyToLanguage]];
            if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                [lblCaption setTextColor:[UIColor darkGrayColor]];
                [lblAlertPastTime setTextColor:[UIColor colorWithRed:255.0f/255.0f green:99.0f/255.0f blue:71.0f/255.0f alpha:1.0f]];
            }
            [lblAlertPastTime setHidden:NO];
            [lblRoomNo setHidden:YES];
            [btnNo setHidden:YES];
            [lblBtnNo setHidden:YES];
            [btnYes setHidden:YES];
            [lblBtnYes setHidden:YES];
            [lblMessage setHidden:YES];
            [datePicker setHidden:YES];
            [btnClose setHidden:NO];
            [lblCleaningAt setHidden:YES];
            [lblServiceLaterTime setHidden:YES];
            [serviceLaterBackgroundTime setHidden:YES];
            [lblAlertPastTime setText:message];
        }
            break;
        default:
            break;
    }
    
    [lblMessage setText:message];
    if(roomNo != nil)
    {
//        [lblRoomNo setText:[NSString stringWithFormat:@"%@ %@",CleaningStatusConfirmMessageRoom, roomNo]];
        [lblRoomNo setText:[NSString stringWithFormat:@"%@ %@",[L_room currentKeyToLanguage], roomNo]];
    }
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        [lblRoomNo setTextColor:[UIColor blackColor]];
    }

    if (selectedDate != nil) {
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:timeFormat];
        [lblServiceLaterTime setText:[dateFormat stringFromDate:selectedDate]];
    }
    [imgviewPopupIcon setImage:[UIImage imageNamed:imgIcon]]; 
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}

-(void) setConfirmPopupType:(enum CLEANING_STATUS_CONFIRM_POPUP) confirmType
{
    if (confirmType == CLEANING_STATUS_CONFIRM_POPUP_SERVICE_LATER && confirmPopupType != CLEANING_STATUS_CONFIRM_POPUP_SERVICE_LATER) {
        [self layoutPopup:YES];
    }
    else if (confirmType != CLEANING_STATUS_CONFIRM_POPUP_SERVICE_LATER && confirmPopupType == CLEANING_STATUS_CONFIRM_POPUP_SERVICE_LATER) {
        [self layoutPopup:NO];
        if (confirmType == CLEANING_STATUS_CONFIRM_POPUP_SERVICE_LATER_REMINDER) {
            [self removeFromSuperview];
        }
    }
    confirmPopupType = confirmType;
}

#pragma mark - Button Clicked
-(IBAction) btnCloseClicked:(id)sender
{
    [self removeFromSuperview];
    [delegate btnCloseClicked];
}

-(IBAction) btnNoClicked:(id)sender
{
    [self removeFromSuperview];
}

-(IBAction)btnYesClicked:(id)sender
{
    if (confirmPopupType == CLEANING_STATUS_CONFIRM_POPUP_SERVICE_LATER) {
//        NSCalendar *calender = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
//        NSDateComponents *components = [calender components:NSHourCalendarUnit|NSMinuteCalendarUnit fromDate:[NSDate date] toDate:[datePicker date] options:0];
//        NSInteger time = components.hour*60 + components.minute;
//        
//        if (time < 0) {
//            [datePicker setDate:[NSDate date]];
//            return;
//        }
        
        selectedDate = [datePicker date];
        BOOL isValidDate = [ehkConvert isDateTimeInPast:selectedDate];
        if(isValidDate) {
            [lblMessage setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_assign_date_can_not_be_past]];
            return;
        }
    }
    [delegate btnYesClicked];
}

#pragma mark - Private method
-(void) layoutPopup:(BOOL) isExpanded{
    int expand;
    if (isExpanded) {
        expand = expandHeight;
    }
    else {
        expand = -expandHeight;
    }
    CGRect frame = viewPopup.frame;
    frame.origin.y += -expand/2;
    frame.size.height += expand;
    [viewPopup setFrame:frame];
    
    frame = imgBodyPopup.frame;
    frame.size.height += expand;
    [imgBodyPopup setFrame:frame];
    
    frame = imgBottomPopup.frame;
    frame.origin.y += expand;
    [imgBottomPopup setFrame:frame];
    
    frame = btnNo.frame;
    frame.origin.y += expand;
    [btnNo setFrame:frame];
    
    frame = btnYes.frame;
    frame.origin.y += expand;
    [btnYes setFrame:frame];
    
    frame = lblBtnNo.frame;
    frame.origin.y += expand;
    [lblBtnNo setFrame:frame];
    
    frame = lblBtnYes.frame;
    frame.origin.y += expand;
    [lblBtnYes setFrame:frame];
    
    frame = btnClose.frame;
    frame.origin.y += expand;
    [btnClose setFrame:frame];
}

#pragma mark - iOS 7 Resources
-(void)loadFlatResource
{
    [imgHeaderPopup setImage:[UIImage imageNamed:imgBGPopupTopFlat]];
    [imgBodyPopup setImage:[UIImage imageNamed:imgBGPopupMidFlat]];
    [imgBottomPopup setImage:[UIImage imageNamed:imgBgPopupBottomFlat]];
    [btnYes setBackgroundImage:[UIImage imageNamed:imgBtnSelectionPopupFlat] forState:UIControlStateNormal];
    [btnYes setBackgroundImage:[UIImage imageNamed:imgBtnSelectionPopupFlat] forState:UIControlStateHighlighted];
    [btnNo setBackgroundImage:[UIImage imageNamed:imgBtnSelectionPopupFlat] forState:UIControlStateNormal];
    [btnNo setBackgroundImage:[UIImage imageNamed:imgBtnSelectionPopupFlat] forState:UIControlStateHighlighted];
    [btnClose setBackgroundImage:[UIImage imageNamed:imgBtnSelectionPopupFlat] forState:UIControlStateNormal];
    [btnClose setBackgroundImage:[UIImage imageNamed:imgBtnSelectionPopupFlat] forState:UIControlStateHighlighted];
    
    [lblCaption setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_confirm_title]];
    
    [btnYes setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_YES] forState:UIControlStateNormal];
    [btnYes setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_YES] forState:UIControlStateHighlighted];
    [lblBtnYes setText:@""];
    
    [btnNo setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_NO] forState:UIControlStateNormal];
    [btnNo setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_NO] forState:UIControlStateHighlighted];
    [lblBtnNo setText:@""];
    
    [btnClose setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_CLOSE] forState:UIControlStateNormal];
    [btnClose setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_CLOSE] forState:UIControlStateHighlighted];
    
}

@end
