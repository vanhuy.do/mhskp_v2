//
//  CleaningStatusPopup.h
//  mHouseKeeping
//
//  Created by Khanh Phan on 1/26/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CleaningStatusPopup : UIAlertView
{
    NSMutableArray *buttonStrings;
    BOOL didLayout;
}

@property (nonatomic, strong) NSMutableArray *buttonStrings;

-(id)initWithArrayButtons:(NSArray *) images title:(NSString *)title message:(NSString *)message delegate:(id)delegate cancelButtonTitle:(NSString *)cancelButtonTitle otherButtonTitles:(NSString *)otherButtonTitles, ...;

@end
