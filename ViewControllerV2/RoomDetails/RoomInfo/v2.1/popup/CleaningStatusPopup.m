//
//  CleaningStatusPopup.m
//  mHouseKeeping
//
//  Created by Khanh Phan on 1/26/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "CleaningStatusPopup.h"
#import "LogFileManager.h"

#define tagImage 111

@implementation CleaningStatusPopup

@synthesize buttonStrings;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(id) initWithArrayButtons:(NSArray *)images title:(NSString *)title message:(NSString *)message delegate:(id)delegate cancelButtonTitle:(NSString *)cancelButtonTitle otherButtonTitles:(NSString *)otherButtonTitles, ...
{
//    self = [super initWithTitle:title message:message delegate:delegate cancelButtonTitle:cancelButtonTitle otherButtonTitles: nil];
    self = [super initWithFrame:CGRectMake(0, 0, 320, 400)];
    
    if (self != nil) {
        va_list args;
        va_start(args, otherButtonTitles);
        buttonStrings = [NSMutableArray array];
        [buttonStrings addObject:cancelButtonTitle == nil ? @"" : cancelButtonTitle];
        for (NSString *anOtherButtonTitles = otherButtonTitles; anOtherButtonTitles != nil; anOtherButtonTitles = va_arg(args, NSString*)){
            [buttonStrings addObject:anOtherButtonTitles];
            [self addButtonWithTitle:anOtherButtonTitles];
        }
        
        NSArray *subviews = self.subviews;
        NSInteger countButton = 0;
        for (id subview in subviews) {
            
            if([LogFileManager isLogConsole])
            {
                NSLog(@"%@",subview);
            }
            
            if (![subview isKindOfClass:[UILabel class]] && ![subview isKindOfClass:[UIImageView class]]) {
                UIImage *img = nil;
                img = [images objectAtIndex:countButton];
                UIImageView *imgView = [[UIImageView alloc] initWithImage:img];
                [imgView setFrame:CGRectMake(-5, -5, img.size.width / 2.0, img.size.height / 2.0)];
                [imgView setTag:tagImage];
                [subview insertSubview:imgView atIndex:1];
                [subview sendSubviewToBack:imgView];
                
                if ([subview isKindOfClass:NSClassFromString(@"UIThreePartButton")] ) {
                    UILabel *theTitle = [[UILabel alloc] init];
                    [theTitle setFont:[UIFont fontWithName:@"Helvetica-Bold" size:18.0]];
                    [theTitle setTextColor:[UIColor whiteColor]];
                    [theTitle setShadowColor:[UIColor blackColor]];
                    [theTitle setShadowOffset:CGSizeMake(0, -1)];
                    
                    CGRect newFrame = theTitle.frame;
                    newFrame.origin.x = 0 + 5;
                    newFrame.origin.y = 0;
                    
                    if (countButton == 1) {
                        newFrame.origin.x += 5;
                    }
                    
                    newFrame.size.height = imgView.frame.size.height;
                    newFrame.size.width = imgView.frame.size.width;
                    [theTitle setFrame:newFrame];
                    
                    [theTitle setBackgroundColor:[UIColor clearColor]];             
                    [theTitle setTextAlignment:NSTextAlignmentCenter];
                    [imgView addSubview:theTitle];
                    [theTitle setText:[buttonStrings objectAtIndex:countButton]];
                    
                } else {
                    
                    if ([[[subview class] description] isEqualToString:@"UIAlertButton"]) {
                        UILabel *theTitle = [[UILabel alloc] init];
                        [theTitle setFont:[UIFont fontWithName:@"Helvetica-Bold" size:18.0]];
                        [theTitle setTextColor:[UIColor whiteColor]];
                        [theTitle setShadowColor:[UIColor blackColor]];
                        [theTitle setShadowOffset:CGSizeMake(0, -1)];
                        
                        CGRect newFrame = theTitle.frame;
                        newFrame.origin.x = 0 + 5;
                        
                        if (countButton == 1) {
                            newFrame.origin.x += 5;
                        }
                        
                        newFrame.origin.y = 0;
                        newFrame.size.height = imgView.frame.size.height;
                        newFrame.size.width = imgView.frame.size.width;
                        [theTitle setFrame:newFrame];
                        
                        [theTitle setBackgroundColor:[UIColor clearColor]];             
                        [theTitle setTextAlignment:NSTextAlignmentCenter];
                        [imgView addSubview:theTitle];
                        [theTitle setText:[buttonStrings objectAtIndex:countButton]];
                        
                        [subview bringSubviewToFront:imgView];
                    }
                }
                
                countButton ++;
            }
        }
        didLayout = NO;
    }

    return self;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGRect frame = [self frame];
    frame.size = CGSizeMake(320, 180);
    self.frame = frame;
    
    CGRect activeBounds = [self bounds];
    CGFloat cornerRadius = 10.0f;
    CGFloat inset = 26.5f;
    CGFloat originX = activeBounds.origin.x + inset;
    CGFloat originY = activeBounds.origin.y +inset;
    CGFloat width = activeBounds.size.width - (inset * 2.0f);
    CGFloat height = activeBounds.size.height - (inset * 2.0f);
    CGRect bPathFrame = CGRectMake(originX, originY, width, height);
    CGPathRef path = [UIBezierPath bezierPathWithRoundedRect:bPathFrame cornerRadius:cornerRadius].CGPath;
    
    CGContextAddPath(context, path);
    CGContextAddRect(context, frame);
    CGContextSetFillColorWithColor(context, [UIColor blueColor].CGColor);
    CGContextSetShadowWithColor(context, CGSizeMake(0.0f, 1.0f), 6.0f, [UIColor redColor].CGColor);
    CGContextDrawPath(context, kCGPathFill);
}

-(void) layoutSubviews
{
    if (didLayout == NO) {
        didLayout = YES;
        CGRect frame = [self frame];
        frame.size = CGSizeMake(320, 400);
//        frame.origin = CGPointMake(0, 32);
        self.frame = frame;
        NSArray *subviews = self.subviews;
        for (id subview in subviews) {
            if([LogFileManager isLogConsole])
            {
                NSLog(@">>> %@",subview);
            }
            if (![subview isKindOfClass:[UILabel class]] && ![subview isKindOfClass:[UIImageView class]]) {
                if (buttonStrings.count == 2) {
                    CGRect frame = [subview frame];
                    frame.origin = CGPointMake(11, 120);
//                    [subview setFrame:frame];
                    UIImageView *imgView = (UIImageView *)[subview viewWithTag:tagImage];
                    CGRect f = imgView.frame;
                    f.origin.x = -3;
                    f.origin.y = -3;
                    [imgView setFrame:f];
                    continue;
                }
                if (buttonStrings.count > 2) {
                    continue;
                }
                UIImageView *imgView = (UIImageView *)[subview viewWithTag:tagImage];
                CGRect frame = [(id)subview frame];
                [subview setFrame:CGRectMake(([(id)subview superview].frame.size.width - imgView.frame.size.width)/2.0 /*- frame.origin.x*/, frame.origin.y, imgView.frame.size.width - 7, imgView.frame.size.height - 10)];
            }
            else {
                CGRect frame = [subview frame];
                frame.size = CGSizeMake(300, 120);
                [subview setFrame:frame];
            }
        }
    } else {
        //do nothing
    }
}

@end
