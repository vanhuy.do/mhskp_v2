//
//  CleaningStatusPopupView.h
//  mHouseKeeping
//
//  Created by Khanh Phan on 2/12/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CleaningStatusPopupViewController.h"

//@protocol CleaningStatusPopupDelegate <NSObject>
//
////-(void) btnCancelClicked;
//-(void) btnDNDClicked;
//-(void) btnServiceLaterClicked;
//-(void) btnDeclinedServiceClicked;
//-(void) btnPendingClicked;
////-(void) btnCompleteClicked;
//
//@end

@interface CleaningStatusPopupView : UIView
{
    IBOutlet UIView *viewCleaningPopup;
    IBOutlet UIView *viewBtnComplete;
    IBOutlet UIView *viewBtnPending;
    
    IBOutlet UIButton *btnDoubleLock;
    IBOutlet UIImageView *imgDoubleLock;
    IBOutlet UIImageView *imgBodyPopup;
    IBOutlet UIImageView *imgBottomPopup;
    IBOutlet UIImageView *imgTop;
    IBOutlet UILabel *lblHeader;
    IBOutlet UIView *viewPopupBody;
    
    IBOutlet UIButton *btnCancel;
    IBOutlet UILabel *lblDoubleLock;
    
    id<CleaningStatusPopupDelegate> delegate;
    
    BOOL isPushedFromFindView;
    IBOutlet UIButton *btnDND, *btnDeclinceService, *btnServiceLater, *btnPending, *btnComplete;
    IBOutlet UIImageView *imageDND, *imageDeclinceService, *imageServiceLater, *imagePending, *imageComplete;
}

-(id) initWithDelegate:(id<CleaningStatusPopupDelegate>)delegate andOption:(BOOL) isPushedFromFindView;

@property (nonatomic, strong) NSString *roomNo;
@property (nonatomic, assign) BOOL isPushedFromAddJobDetail;

-(IBAction) btnCancelClicked:(id)sender;
-(IBAction) btnDNDClicked:(id)sender;
-(IBAction) btnServiceLaterClicked:(id)sender;
-(IBAction) btnDeclinedServiceClicked:(id)sender;
-(IBAction) btnPendingClicked:(id)sender;
-(IBAction) btnCompleteClicked:(id)sender;
- (IBAction)btnDoubleLockClicked:(id)sender;

@end
