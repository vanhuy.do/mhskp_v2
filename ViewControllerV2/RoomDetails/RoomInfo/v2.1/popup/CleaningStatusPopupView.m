//
//  CleaningStatusPopupView.m
//  mHouseKeeping
//
//  Created by Khanh Phan on 2/12/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "CleaningStatusPopupView.h"
#import "DeviceManager.h"
#import "ehkDefinesV2.h"
#import "UIImage+Tint.h"

@implementation CleaningStatusPopupView

@synthesize roomNo;
@synthesize isPushedFromAddJobDetail;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(id) initWithDelegate:(id<CleaningStatusPopupDelegate>)_delegate andOption:(BOOL)_isPushedFromFindView
{
    int frameHeight = 480;
    int deviceKind = [DeviceManager getDeviceScreenKind];
    if(deviceKind == DeviceScreenKindRetina4_0){
        frameHeight = 568;
    }
    
    self = [super initWithFrame:CGRectMake(0, 0, 320, frameHeight)];
    [self setBackgroundColor:[UIColor clearColor]];
    if (self) {
        NSArray *nibViews = [[NSBundle mainBundle] loadNibNamed:@"CleaningStatusPopupView" owner:self options:nil];
        UIView *cleaningStatusPopupView = [nibViews objectAtIndex:0];
        [cleaningStatusPopupView setFrame:self.frame];
        [self addSubview:cleaningStatusPopupView];
        delegate = _delegate;
        isPushedFromFindView = _isPushedFromFindView;
        
        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
            [self loadFlatResource];
        }
    }
    return self;
}

-(void) willMoveToSuperview:(UIView *)newSuperview
{
    
    [btnCancel setTitle:[L_CANCEL currentKeyToLanguage] forState:UIControlStateNormal];
    [btnComplete setTitle:[L_completed_label currentKeyToLanguage] forState:UIControlStateNormal];
    [btnDeclinceService setTitle:[L_declined_service currentKeyToLanguage] forState:UIControlStateNormal];
    [btnDND setTitle:[L_dnd_service currentKeyToLanguage] forState:UIControlStateNormal];
    [lblDoubleLock setText:[L_double_lock_service currentKeyToLanguage]];
    [btnServiceLater setTitle:[L_service_later currentKeyToLanguage] forState:UIControlStateNormal];
    [btnPending setTitle:[L_strpending currentKeyToLanguage] forState:UIControlStateNormal];
    
    if (isPushedFromAddJobDetail) {
        [lblHeader setText:[L_job_status currentKeyToLanguage]];
    } else {
        [lblHeader setText:[L_CLEANING_STATUS currentKeyToLanguage]];
    }
    
    if (isPushedFromFindView) {
        [viewBtnComplete setHidden:YES];
        [viewBtnPending setHidden:NO];
        
        CGRect popupFrame = viewCleaningPopup.frame;
        popupFrame.size.height = 312;
        popupFrame.origin.y = 84;
        [viewCleaningPopup setFrame:popupFrame];
        
        CGRect popupBodyFrame = viewPopupBody.frame;
        popupFrame.size.height = 262;
        [viewPopupBody setFrame:popupBodyFrame];
        
        CGRect imgBodyPopupFrame = imgBodyPopup.frame;
        imgBodyPopupFrame.size.height = 247;
        [imgBodyPopup setFrame:imgBodyPopupFrame];
        
        CGRect imgBottomPopupFrame = imgBottomPopup.frame;
        imgBottomPopupFrame.origin.y = 247;
        [imgBottomPopup setFrame:imgBottomPopupFrame];
        
        CGRect btnCancelFrame = btnCancel.frame;
        btnCancelFrame.origin.y = 208;
        [btnCancel setFrame:btnCancelFrame];
    }
    else {
        [viewBtnComplete setHidden:YES];
        [viewBtnPending setHidden:YES];
        
        CGRect popupFrame = viewCleaningPopup.frame;
        popupFrame.size.height = 312;
        popupFrame.origin.y = 108;
        [viewCleaningPopup setFrame:popupFrame];
        
        CGRect popupBodyFrame = viewPopupBody.frame;
        popupFrame.size.height = 262;
        [viewPopupBody setFrame:popupBodyFrame];
        
        CGRect imgBodyPopupFrame = imgBodyPopup.frame;
        imgBodyPopupFrame.size.height = 248;
        [imgBodyPopup setFrame:imgBodyPopupFrame];
        
        CGRect imgBottomPopupFrame = imgBottomPopup.frame;
        imgBottomPopupFrame.origin.y = 248;
        [imgBottomPopup setFrame:imgBottomPopupFrame];
        
        CGRect btnCancelFrame = btnCancel.frame;
        btnCancelFrame.origin.y = 208;
        [btnCancel setFrame:btnCancelFrame];
    }
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
}

-(IBAction) btnCancelClicked:(id)sender
{
    [self removeFromSuperview];
}

-(IBAction) btnCompleteClicked:(id)sender
{
//    [delegate btnCompleteClicked];
}

- (IBAction)btnDoubleLockClicked:(id)sender {
    [delegate btnDoubleLockClicked];
}

-(IBAction) btnDeclinedServiceClicked:(id)sender
{
    [delegate btnDeclinedServiceClicked];
}

-(IBAction) btnDNDClicked:(id)sender
{
    [delegate btnDNDClicked];
}

-(IBAction) btnPendingClicked:(id)sender
{
    [delegate btnPendingClicked];
}

-(IBAction) btnServiceLaterClicked:(id)sender
{
    [delegate btnServiceLaterClicked];
}

#pragma mark - iOS 7 Resources
-(void)loadFlatResource
{
    [imgTop setImage:[UIImage imageNamed:imgBGPopupTopFlat]];
    [imgBodyPopup setImage:[UIImage imageNamed:imgBGPopupMidFlat]];
    [imgBottomPopup setImage:[UIImage imageNamed:imgBgPopupBottomFlat]];
    
    [imageDND setImage:[UIImage imageNamed:imgDNDFlat]];
    [imgDoubleLock setImage:[UIImage imageNamed:imgDoubleLockFlat]];
    [imageServiceLater setImage:[UIImage imageNamed:imgServiceLaterFlat]];
    [imageDeclinceService setImage:[UIImage imageNamed:imgDeclinedServiceFlat]];
    [imagePending setImage:[UIImage imageNamed:imgPendingFlat]];
    [imageComplete setImage:[UIImage imageNamed:imgCompleteFlat]];
    [lblHeader setTextColor:[UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.7f]];
    
    [btnDND setBackgroundImage:[UIImage imageNamed:imgBtnSelectionPopupFlat] forState:UIControlStateNormal];
    [btnDoubleLock setBackgroundImage:[UIImage imageNamed:imgBtnSelectionPopupFlat] forState:UIControlStateNormal];
    [btnComplete setBackgroundImage:[UIImage imageNamed:imgBtnSelectionPopupFlat] forState:UIControlStateNormal];
    [btnDeclinceService setBackgroundImage:[UIImage imageNamed:imgBtnSelectionPopupFlat] forState:UIControlStateNormal];
    [btnPending setBackgroundImage:[UIImage imageNamed:imgBtnSelectionPopupFlat] forState:UIControlStateNormal];
    [btnServiceLater setBackgroundImage:[UIImage imageNamed:imgBtnSelectionPopupFlat] forState:UIControlStateNormal];
    [btnCancel setBackgroundImage:[UIImage imageNamed:imgBtnCancelPopupFlat] forState:UIControlStateNormal];
}

@end
