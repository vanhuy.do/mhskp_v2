//
//  PopupViewController.h
//  mHouseKeeping
//
//  Created by Khanh Phan on 1/27/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CleaningStatusPopupDelegate <NSObject>

-(void) btnCancelClicked;
-(void) btnDNDClicked;
-(void) btnDoubleLockClicked;
-(void) btnServiceLaterClicked;
-(void) btnDeclinedServiceClicked;
-(void) btnPendingClicked;
-(void) btnCompleteClicked;

@end

@interface CleaningStatusPopupViewController : UIViewController
{
    
    IBOutlet UIView *viewCleaningPopup;
    IBOutlet UIView *viewBtnComplete;
    IBOutlet UIView *viewBtnPending;

    IBOutlet UIImageView *imgBodyPopup;
    IBOutlet UIImageView *imgBottomPopup;
    IBOutlet UIImageView *imgTop;
    
    IBOutlet UIView *viewPopupBody;
    
    id<CleaningStatusPopupDelegate> delegate;
    
    BOOL isPushedFromFindView;
    
    IBOutlet UIButton *btnDoubeLock;
    IBOutlet UIButton *btnDND, *btnDeclinceService, *btnServiceLater, *btnPending, *btnComplete, *btnCancel;
    IBOutlet UIImageView *imageDND, *imageDeclinceService, *imageServiceLater, *imagePending, *imageComplete;
    IBOutlet UILabel *lblCleaningStatus;
    IBOutlet UIImageView *imgDoubleLock;
}

-(id) initWithDelegate:(id<CleaningStatusPopupDelegate>) delegate andOption:(BOOL) isPushedFromFindView;

@property (weak, nonatomic) IBOutlet UILabel *lblDoubleLock;
@property (nonatomic, strong) NSString *roomNo;
- (IBAction)btnDoubleLockClicked:(id)sender;

-(IBAction) btnCancelClicked:(id)sender;
-(IBAction) btnDNDClicked:(id)sender;
-(IBAction) btnServiceLaterClicked:(id)sender;
-(IBAction) btnDeclinedServiceClicked:(id)sender;
-(IBAction) btnPendingClicked:(id)sender;
-(IBAction) btnCompleteClicked:(id)sender;

@end
