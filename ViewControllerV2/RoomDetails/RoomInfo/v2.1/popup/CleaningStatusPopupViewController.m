//
//  PopupViewController.m
//  mHouseKeeping
//
//  Created by Khanh Phan on 1/27/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "CleaningStatusPopupViewController.h"
#import "ehkDefinesV2.h"
#import "UIImage+Tint.h"
#import "DeviceManager.h"

@interface CleaningStatusPopupViewController ()

@end

@implementation CleaningStatusPopupViewController

@synthesize roomNo;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(id) initWithDelegate:(id<CleaningStatusPopupDelegate>) delegator andOption:(BOOL)_isPushedFromFindView
{
    self = [super initWithNibName:@"CleaningStatusPopupViewController" bundle:nil];
    if (self) {
        delegate = delegator;
        isPushedFromFindView = _isPushedFromFindView;
        
        [btnCancel setTitle:[L_CANCEL currentKeyToLanguage] forState:UIControlStateNormal];
        [btnComplete setTitle:[L_completed_label currentKeyToLanguage] forState:UIControlStateNormal];
        [btnDeclinceService setTitle:[L_declined_service currentKeyToLanguage] forState:UIControlStateNormal];
        [btnDND setTitle:[L_dnd_service currentKeyToLanguage] forState:UIControlStateNormal];
        [_lblDoubleLock setText:[L_double_lock_service currentKeyToLanguage]];
        [btnServiceLater setTitle:[L_service_later currentKeyToLanguage] forState:UIControlStateNormal];
        [btnPending setTitle:[L_strpending currentKeyToLanguage] forState:UIControlStateNormal];
        [lblCleaningStatus setText:[L_CLEANING_STATUS currentKeyToLanguage]];
        
        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
            [self loadFlatResource];
        }
        
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(IBAction) btnDNDClicked:(id)sender
{
    [delegate btnDNDClicked];
}
- (IBAction)btnDoubleLockClicked:(id)sender {
    [delegate btnDoubleLockClicked];
}
-(IBAction) btnDeclinedServiceClicked:(id)sender
{
    [delegate btnDeclinedServiceClicked];
}

-(IBAction) btnServiceLaterClicked:(id)sender
{
    [delegate btnServiceLaterClicked];
}



-(IBAction) btnCancelClicked:(id)sender
{
    [self.view removeFromSuperview];
//    [delegate btnCancelClicked];
}

-(IBAction) btnPendingClicked:(id)sender
{
    [delegate btnPendingClicked];
}

-(IBAction) btnCompleteClicked:(id)sender
{
//    [delegate btnCompleteClicked];
}

#pragma mark - Private Methods
-(void) layoutPopupFromFind
{
    
}

#pragma mark - iOS 7 Resources
-(void)loadFlatResource
{
    [imgTop setImage:[UIImage imageNamed:imgBGPopupTopFlat]];
    [imgBodyPopup setImage:[UIImage imageNamed:imgBGPopupMidFlat]];
    [imgBottomPopup setImage:[UIImage imageNamed:imgBgPopupBottomFlat]];
    
    [imageDND setImage:[UIImage imageNamed:imgDNDFlat]];
    [imgDoubleLock setImage:[UIImage imageNamed:imgDoubleLockFlat]];
    [imageServiceLater setImage:[UIImage imageNamed:imgServiceLaterFlat]];
    [imageDeclinceService setImage:[UIImage imageNamed:imgDeclinedServiceFlat]];
    [imagePending setImage:[UIImage imageNamed:imgPendingFlat]];
    [imageComplete setImage:[UIImage imageNamed:imgCompleteFlat]];
    
    [btnDoubeLock setImage:[UIImage imageNamed:imgBtnSelectionPopupFlat] forState:UIControlStateNormal];
    [btnDND setImage:[UIImage imageNamed:imgBtnSelectionPopupFlat] forState:UIControlStateNormal];
    [btnComplete setImage:[UIImage imageNamed:imgBtnSelectionPopupFlat] forState:UIControlStateNormal];
    [btnDeclinceService setImage:[UIImage imageNamed:imgBtnSelectionPopupFlat] forState:UIControlStateNormal];
    [btnPending setImage:[UIImage imageNamed:imgBtnSelectionPopupFlat] forState:UIControlStateNormal];
    [btnServiceLater setImage:[UIImage imageNamed:imgBtnSelectionPopupFlat] forState:UIControlStateNormal];
    [btnCancel setImage:[UIImage imageNamed:imgBtnCancelPopupFlat] forState:UIControlStateNormal];
}

@end
