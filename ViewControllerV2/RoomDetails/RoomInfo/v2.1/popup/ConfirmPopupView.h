//
//  ConfirmPopupView.h
//  mHouseKeeping
//
//  Created by Khanh Phan on 1/30/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ConfirmPopupDelegate <NSObject>

-(void) confirmPopupDismissWhenBtnYesClicked;
-(void) confirmPopupDismissWhenBtnNoClicked;

@end

@interface ConfirmPopupView : UIView
{
    IBOutlet UILabel *lblRoomNo;
    IBOutlet UILabel *lblMessage;
    IBOutlet UILabel *lblYes;
    IBOutlet UILabel *lblNo;
}

@property (nonatomic, strong) id<ConfirmPopupDelegate> delegator;
@property (nonatomic, strong) NSString *roomNo;
@property (nonatomic, strong) NSString *postingType;

-(IBAction) btnYesClicked:(id)sender;
-(IBAction) btnNoClicked:(id)sender;

-(id) initWithDelegator:(id<ConfirmPopupDelegate>)delegator;

@end
