//
//  ConfirmPopupView.m
//  mHouseKeeping
//
//  Created by Khanh Phan on 1/30/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "ConfirmPopupView.h"

@implementation ConfirmPopupView

@synthesize delegator, roomNo, postingType;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(id) initWithDelegator:(id<ConfirmPopupDelegate>)_delegator
{
    self = [super initWithFrame:CGRectMake(0, 0, 320, 480)];
    [self setBackgroundColor:[UIColor clearColor]];
    if (self) {
        NSArray *nibViews = [[NSBundle mainBundle] loadNibNamed:@"ConfirmPopupView" owner:self options:nil];
        [self addSubview:[nibViews objectAtIndex:0]];
        [self setDelegator:_delegator];
    }
    return self;
}

-(void) willMoveToSuperview:(UIView *)newSuperview
{
    [lblRoomNo setText:[NSString stringWithFormat:@"%@ %@?", [L_room currentKeyToLanguage] ,roomNo]];
    [lblMessage setText:[NSString stringWithFormat:@"%@ %@ %@?", [L_SUBMIT currentKeyToLanguage],postingType, [L_ITEM currentKeyToLanguage]]];
    [lblYes setText:[L_YES currentKeyToLanguage]];
    [lblNo setText:[L_NO currentKeyToLanguage]];
}

-(IBAction) btnYesClicked:(id)sender
{
    [delegator confirmPopupDismissWhenBtnYesClicked];
}

-(IBAction) btnNoClicked:(id)sender
{
    [delegator confirmPopupDismissWhenBtnNoClicked];
}
@end
