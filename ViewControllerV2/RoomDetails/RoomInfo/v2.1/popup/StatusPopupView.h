//
//  CleaningStatusPopupView.h
//  mHouseKeeping
//
//  Created by Khanh Phan on 2/12/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CleaningStatusPopupViewController.h"
#import "StatusTableViewCell.h"
@protocol StatusPopupViewDelegate <NSObject>

- (void)didClickItemAtIndex:(id)object withData:(NSDictionary*)data;

@end

@interface StatusPopupView : UIView<UITableViewDataSource, UITabBarDelegate, StatusTableViewCellDelegate>
{
    IBOutlet UIView *viewCleaningPopup;
//    IBOutlet UIView *viewBtnComplete;
//    IBOutlet UIView *viewBtnPending;
    
    IBOutlet UIImageView *imgBodyPopup;
    IBOutlet UIImageView *imgBottomPopup;
    IBOutlet UIImageView *imgTop;
    IBOutlet UILabel *lblHeader;
    IBOutlet UIView *viewPopupBody;
    
    IBOutlet UIButton *btnCancel;
    
    BOOL isPushedFromFindView;
    IBOutlet UIButton *btnDND, *btnDeclinceService, *btnServiceLater, *btnPending, *btnComplete, *btnDoubleLock;
    IBOutlet UIImageView *imageDND, *imageDeclinceService, *imageServiceLater, *imagePending, *imageComplete, *imageDoubleLock;
    __weak IBOutlet UITableView *tbvContent;
}

-(id) initWithDelegate:(id<StatusPopupViewDelegate>)_delegate withTitle:(NSString*)title withData:(NSArray*)data;
@property (nonatomic, strong) NSMutableArray *listData;
@property (nonatomic, strong) NSString *roomNo;
@property (nonatomic, assign) BOOL isPushedFromAddJobDetail;
@property (nonatomic, assign) id<StatusPopupViewDelegate> delegate;
@property (nonatomic, strong) NSDictionary *data;
-(IBAction) btnCancelClicked:(id)sender;

//-(IBAction) btnDNDClicked:(id)sender;
//-(IBAction) btnServiceLaterClicked:(id)sender;
//-(IBAction) btnDeclinedServiceClicked:(id)sender;
//-(IBAction) btnPendingClicked:(id)sender;
//-(IBAction) btnCompleteClicked:(id)sender;

@end
