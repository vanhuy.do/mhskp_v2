//
//  CleaningStatusPopupView.m
//  mHouseKeeping
//
//  Created by Khanh Phan on 2/12/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "StatusPopupView.h"
#import "DeviceManager.h"
#import "ehkDefinesV2.h"
#import "UIImage+Tint.h"
#import "StatusTableViewCell.h"

@implementation StatusPopupView

@synthesize roomNo;
@synthesize isPushedFromAddJobDetail;
@synthesize delegate;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(id) initWithDelegate:(id<StatusPopupViewDelegate>)_delegate withTitle:(NSString*)title withData:(NSArray*)data
{
    int frameHeight = 480;
    int deviceKind = [DeviceManager getDeviceScreenKind];
    if(deviceKind == DeviceScreenKindRetina4_0){
        frameHeight = 568;
    }
    frameHeight = [[UIScreen mainScreen] bounds].size.height;
    self = [super initWithFrame:CGRectMake(0, 0, 320, frameHeight)];
    isPushedFromAddJobDetail = NO;
    [self setBackgroundColor:[UIColor clearColor]];
    if (self) {
        self.listData = [[NSMutableArray alloc] initWithArray:data];
        NSArray *nibViews = [[NSBundle mainBundle] loadNibNamed:@"StatusPopupView" owner:self options:nil];
        UIView *cleaningStatusPopupView = [nibViews objectAtIndex:0];
        [cleaningStatusPopupView setFrame:self.frame];
        [self addSubview:cleaningStatusPopupView];
        delegate = _delegate;
        [lblHeader setText:title];
        
        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
            [self loadFlatResource];
        }
        [tbvContent registerNib:[UINib nibWithNibName:@"StatusTableViewCell" bundle:nil] forCellReuseIdentifier:@"StatusTableViewCell"];
    }
    return self;
}

-(void) willMoveToSuperview:(UIView *)newSuperview
{
    
    [btnCancel setTitle:[L_CANCEL currentKeyToLanguage] forState:UIControlStateNormal];
    [btnComplete setTitle:[L_completed_label currentKeyToLanguage] forState:UIControlStateNormal];
    [btnDeclinceService setTitle:[L_declined_service currentKeyToLanguage] forState:UIControlStateNormal];
    [btnDND setTitle:[L_dnd_service currentKeyToLanguage] forState:UIControlStateNormal];
    [btnDoubleLock setTitle:[L_double_lock_service currentKeyToLanguage] forState:UIControlStateNormal];
    [btnServiceLater setTitle:[L_service_later currentKeyToLanguage] forState:UIControlStateNormal];
    [btnPending setTitle:[L_strpending currentKeyToLanguage] forState:UIControlStateNormal];
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
}

-(IBAction) btnCancelClicked:(id)sender
{
    [self removeFromSuperview];
}

#pragma mark - iOS 7 Resources
-(void)loadFlatResource
{
    [imgTop setImage:[UIImage imageNamed:imgBGPopupTopFlat]];
    [imgBodyPopup setImage:[UIImage imageNamed:imgBGPopupMidFlat]];
    [imgBottomPopup setImage:[UIImage imageNamed:imgBgPopupBottomFlat]];
    
    [imageDND setImage:[UIImage imageNamed:imgDNDFlat]];
    [imageDoubleLock setImage:[UIImage imageNamed:imgDoubleLockFlat]];
    [imageServiceLater setImage:[UIImage imageNamed:imgServiceLaterFlat]];
    [imageDeclinceService setImage:[UIImage imageNamed:imgDeclinedServiceFlat]];
    [imagePending setImage:[UIImage imageNamed:imgPendingFlat]];
    [imageComplete setImage:[UIImage imageNamed:imgCompleteFlat]];
    [lblHeader setTextColor:[UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.7f]];
    
    [btnDND setBackgroundImage:[UIImage imageNamed:imgBtnSelectionPopupFlat] forState:UIControlStateNormal];
    [btnDoubleLock setBackgroundImage:[UIImage imageNamed:imgBtnSelectionPopupFlat] forState:UIControlStateNormal];
    [btnComplete setBackgroundImage:[UIImage imageNamed:imgBtnSelectionPopupFlat] forState:UIControlStateNormal];
    [btnDeclinceService setBackgroundImage:[UIImage imageNamed:imgBtnSelectionPopupFlat] forState:UIControlStateNormal];
    [btnPending setBackgroundImage:[UIImage imageNamed:imgBtnSelectionPopupFlat] forState:UIControlStateNormal];
    [btnServiceLater setBackgroundImage:[UIImage imageNamed:imgBtnSelectionPopupFlat] forState:UIControlStateNormal];
    [btnCancel setBackgroundImage:[UIImage imageNamed:imgBtnCancelPopupFlat] forState:UIControlStateNormal];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.listData.count;
}

//NSMutableArray *cleaningStatusList = [[RoomManagerV2 sharedRoomManager] loadAllCleaningStatus];
//listCleaningStatus = [NSMutableArray new];
//for (int index = 0; index < [cleaningStatusList count]; index++) {
//    CleaningStatusModelV2 *model = (CleaningStatusModelV2 *) [cleaningStatusList objectAtIndex:index];
//    [listCleaningStatus addObject:model];
//}
//NSMutableArray *roomStatusList = [[RoomManagerV2 sharedRoomManager] loadAllRoomStatus];
//listRoomStatus = [NSMutableArray new];
//for (int index = 0; index < [roomStatusList count]; index++) {
//    RoomStatusModelV2 *roomModel = [roomStatusList objectAtIndex:index];
//    [listRoomStatus addObject:roomModel];
//}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    StatusTableViewCell *cell =  [tableView dequeueReusableCellWithIdentifier:@"StatusTableViewCell"];
    NSString *title = @"";
    UIImage *icon = [UIImage new];
    id object = self.listData[indexPath.row];
    if ([object isKindOfClass:[CleaningStatusModelV2 class]]) {
        CleaningStatusModelV2 *data = (CleaningStatusModelV2*)object;
        title = data.cstat_Name;
        if (data.cstat_image) {
            icon =  [[UIImage alloc] initWithData:data.cstat_image];
        }
        
    }else if ([object isKindOfClass:[RoomStatusModelV2 class]]) {
        RoomStatusModelV2 *data = (RoomStatusModelV2*)object;
        title = data.rstat_Name;
        if (data.rstat_Image) {
            icon =  [[UIImage alloc] initWithData:data.rstat_Image];
        }
    }
    [cell.imageIcon setImage:icon];
    [cell.btnTitle setTitle:title forState:UIControlStateNormal];
    cell.delegate = self;
    cell.currentIndex = indexPath.row;
    return cell;
}
- (void)didClickItemAtIndex:(int)index{
    if ([delegate respondsToSelector:@selector(didClickItemAtIndex:withData:)]) {
        [delegate didClickItemAtIndex:self.listData[index] withData:self.data];
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([delegate respondsToSelector:@selector(didClickItemAtIndex:withData:)]) {
        [delegate didClickItemAtIndex:self.listData[indexPath.row] withData:self.data];
    }
}
@end
