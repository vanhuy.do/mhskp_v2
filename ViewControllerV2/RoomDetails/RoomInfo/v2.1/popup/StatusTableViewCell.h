//
//  StatusTableViewCell.h
//  mHouseKeeping
//
//  Created by Nguyen Minh Tuan on 7/7/16.
//
//

#import <UIKit/UIKit.h>
@protocol StatusTableViewCellDelegate <NSObject>

- (void)didClickItemAtIndex:(int)index;

@end
@interface StatusTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageIcon;
@property (weak, nonatomic) IBOutlet UIButton *btnTitle;
@property (nonatomic) NSInteger currentIndex;
- (IBAction)didClickButton:(UIButton *)sender;
@property (nonatomic, assign) id<StatusTableViewCellDelegate> delegate;
@end
