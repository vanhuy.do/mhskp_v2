//
//  StatusTableViewCell.m
//  mHouseKeeping
//
//  Created by Nguyen Minh Tuan on 7/7/16.
//
//

#import "StatusTableViewCell.h"

@implementation StatusTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)didClickButton:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(didClickItemAtIndex:)]) {
        [self.delegate didClickItemAtIndex:(int)self.currentIndex];
    }
}
@end
