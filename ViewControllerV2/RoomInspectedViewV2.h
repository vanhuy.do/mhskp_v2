//
//  RoomInspectedViewV2.h
//  mHouseKeeping
//
//  Created by TMS on 4/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RoomInspectionViewV2.h"

@interface RoomInspectedViewV2 : UIViewController<UINavigationControllerDelegate,UITableViewDataSource,UITableViewDelegate>
{
    IBOutlet UITableView *roomTableView;
    NSMutableArray *section1Data;
    NSMutableArray *section2Data;

    IBOutlet UITableViewCell *tvCell;
    IBOutlet UILabel *lblLocation;
    UIImageView *hotelLogo;
    IBOutlet UILabel *labelRoom;
    IBOutlet UILabel *labelRMStatus;
    IBOutlet UILabel *labelVIP;
    IBOutlet UILabel *labelDurationUsed;
    
    IBOutlet UILabel * lbltotalPassNumber;
    IBOutlet UILabel *lblTotalFailNumber;
    
    IBOutlet UILabel * lbltotalPassTitle;
    IBOutlet UILabel *lblTotalFailTitle;

    //With face image
    IBOutlet UIButton *totalPassButton;
    IBOutlet UIButton *TotalFailButton;
    
    //Without face image
    IBOutlet UIButton *totalPassButton2;
    IBOutlet UIButton *TotalFailButton2;
    
    RoomInspectionViewV2 *roomInspectionFailViewV2;
    RoomInspectionViewV2 *roomInspectionPassViewV2;
    TopbarViewV2 * topBarView;
    NSInteger numberPass;
    NSInteger numberfail;
    NSMutableArray* listOfPassedRoom;
    NSMutableArray* listOfFailedRoom;
}
@property (nonatomic, strong) UITableView *roomTableView;
@property (nonatomic, strong) NSMutableArray *section1Data;
@property (nonatomic, strong) NSMutableArray *section2Data;
@property (nonatomic, strong) UITableViewCell *tvCell;
@property (nonatomic, strong) IBOutlet UILabel *lblLocation;
@property (nonatomic, strong) IBOutlet UIImageView *hotelLogo;
@property (strong, nonatomic) IBOutlet UILabel *lblHouseKeeperName;
@property (strong, nonatomic) IBOutlet UILabel * lbltotalPassNumber;
@property (strong, nonatomic) IBOutlet UILabel *lblTotalFailNumber;
@property (strong, nonatomic) IBOutlet UILabel * lbltotalPassTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblTotalFailTitle;

@property (nonatomic, strong) UILabel *labelRoom;
@property (nonatomic, strong) UILabel *labelRMStatus;
@property (nonatomic, strong) UILabel *labelVIP;
@property (nonatomic, strong) UILabel *labelDurationUsed;

@property (nonatomic, strong) TopbarViewV2 * topBarView;
@property (nonatomic, strong) RoomInspectionViewV2 *roomInspectionFailViewV2;
@property (nonatomic, strong) RoomInspectionViewV2 *roomInspectionPassViewV2;
@property (nonatomic, strong) NSMutableArray *listOfPassedRoom;
@property (nonatomic, strong) NSMutableArray *listOfFailedRoom;

-(void)drawUnderlinedLabel:(UILabel *)label inTableViewCell:(UITableViewCell *)cell;
-(void)setDataTable;
-(void) totalStatusRoom;
-(void) setCaptionsView;

@end
