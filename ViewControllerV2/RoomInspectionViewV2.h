//
//  RoomCompletedView.h
//  eHouseKeeping
//
//  Created by tms on 5/9/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TopbarViewV2.h"
/**room completed pass or fail**/
@interface RoomInspectionViewV2 : UIViewController <UINavigationControllerDelegate, UITableViewDelegate, UITableViewDataSource>{
    
    IBOutlet UITableView *roomTableView;
    NSMutableArray *section1Data;
    NSMutableArray *section2Data;
    
    IBOutlet UITableViewCell *tvCell;
    IBOutlet UILabel *lblLocation;
    UIImageView *hotelLogo;
    IBOutlet UILabel *labelRoom;
    IBOutlet UILabel *labelRMStatus;
    IBOutlet UILabel *labelVIP;
    IBOutlet UILabel *labelDurationUsed;
    UILabel *lblHouseKeeperName;
    TopbarViewV2 * topBarView;    //
    BOOL isPass; //  
}
@property (nonatomic, retain) UITableView *roomTableView;
@property (nonatomic, retain) NSMutableArray *section1Data;
@property (nonatomic, retain) NSMutableArray *section2Data;
@property (nonatomic, retain) UITableViewCell *tvCell;
@property (nonatomic, retain) IBOutlet UILabel *lblLocation;
@property (nonatomic, retain) IBOutlet UIImageView *hotelLogo;
@property (nonatomic, retain) UILabel *labelRoom;
@property (nonatomic, retain) UILabel *labelRMStatus;
@property (nonatomic, retain) UILabel *labelVIP;
@property (nonatomic, retain) UILabel *labelDurationUsed;
@property (retain, nonatomic) IBOutlet UILabel *lblHouseKeeperName;
@property (nonatomic, strong) TopbarViewV2 * topBarView;    //
@property (nonatomic )  BOOL isPass; //  
-(void)drawUnderlinedLabel:(UILabel *)label inTableViewCell:(UITableViewCell *)cell;
-(void)setDataTable;

-(void) setCaptionsView;

@end
