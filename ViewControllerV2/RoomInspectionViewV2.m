//
//  RoomCompletedView.m
//  eHouseKeeping
//
//  Created by tms on 5/9/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "RoomInspectionViewV2.h"
#import "ehkDefines.h"
#import "TasksManagerV2.h"
#import "CheckMemory.h"
#import "MyNavigationBarV2.h"
#import "RoomAssignmentInfoViewController.h"
#import "LogFileManager.h"
#import "DeviceManager.h"

#define backgroundImage     @"bg.png"

#define sizeTitle           22
#define sizeSubtitle        15
#define tagRoomNo           1
#define tagRMStatus         2
#define tagVIP              3
#define tagDurationUsed     4
#define tagGuestNameCell        5
#define tagUnderline        6
#define tagGuestTitle       7
#define tagName             8
#define tagDetail           9
#define numofSection        1
#define primaryRed          60
#define primaryGreen        206
#define primaryBlue         229
#define secondaryRed        107
#define secondaryGreen      227
#define secondaryBlue       223
#define thirdaryRed         22
#define thirdaryGreen       103
#define thirdaryBlue        120
#define kRoomNo             @"RoomNo"
#define kRMStatus           @"RMStatus"
#define kDurationUsed        @"DurationUsed"
#define kVIP                @"VIP"
#define kGuestName          @"GuestName"
#define kLabel              @"Label"
#define kDetail             @"Detail"
#define kRoomAssignmentID   @"RoomAssignmentID"
#define kTotalTimeUsed      @"TotalTimeUsed"

@interface RoomInspectionViewV2 (PrivateMethods)

-(void) syncRoomCompletedView;
-(void) syncOnMainThread;
-(void) backBarPressed;
-(void) addBackButton;

@end
@implementation RoomInspectionViewV2
@synthesize lblHouseKeeperName;
@synthesize hotelLogo;
@synthesize isPass;

@synthesize roomTableView, section1Data, section2Data, tvCell,lblLocation, labelDurationUsed, labelVIP, labelRMStatus, labelRoom;
@synthesize topBarView;

-(void)drawUnderlinedLabel:(UILabel *)label inTableViewCell:(UITableViewCell *)cell{
    NSString *string = label.text;
    CGSize stringSize = [string sizeWithFont:label.font];
    CGRect rect = label.frame;
    CGRect labelFrame;
    //NSLog(@"%f %f",rect.origin.x,stringSize.width);
    if (stringSize.width > rect.size.width) {
        labelFrame = CGRectMake(rect.origin.x, stringSize.height - 4, rect.size.width, 2);
    } else {
        labelFrame = CGRectMake(rect.origin.x + (rect.size.width - stringSize.width)/2.0, stringSize.height - 4, stringSize.width, 2);
    }
    //NSLog(@"%f %f %f %f", labelFrame.origin.x, labelFrame.origin.y, labelFrame.size.width, labelFrame.size.height);
    UILabel *lineLabel = (UILabel *)[cell viewWithTag:tagUnderline];
    lineLabel.frame = labelFrame;
    
    //UILabel *lineLabel = [[UILabel alloc] initWithFrame:labelFrame];
    lineLabel.backgroundColor =[UIColor blackColor];
    //[cell addSubview:lineLabel];
    //[lineLabel release];
}

// Get room from database that inspected with result is pass or fail
-(void)setDataTable{
    //    [[RoomManagerV2 sharedRoomManager] getRoomAssignmentComplete];
    //    self.section1Data = [RoomManagerV2 sharedRoomManager].roomAssignmentList;
    //    if (self.section1Data && [self.section1Data count] > 0) {
    //        for (int i = 0; i<[self.section1Data count]; i++) {
    //            NSDictionary *aRoom = (NSDictionary*) [self.section1Data objectAtIndex:i];
    //            int inspectedStatus = [[aRoom objectForKey:KInspection_Status_id] intValue];
    //            if (inspectedStatus == ENUM_INSPECTION_COMPLETED_FAIL ) {
    //                if (self.isPass) {
    //                    [self.section1Data removeObjectAtIndex:i];
    //                }
    //            } else if (inspectedStatus == ENUM_INSPECTION_COMPLETED_PASS) {
    //                if (!self.isPass) {
    //                    [self.section1Data removeObjectAtIndex:i];
    //                }
    //            } else {
    //                [self.section1Data removeObjectAtIndex:i];
    //            }
    //        }
    //    }
    [self.roomTableView reloadData];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        //add Observer for sync notify from thread homeview
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(syncRoomCompletedView) name:@"syncRoomCompletedView" object:nil];
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //    UIButton *butCheckList = (UIButton *) [[self.tabBarController tabBar] viewWithTag:tagOfCheckListButton];
    //    [butCheckList setEnabled:NO];
    
    [self setDataTable];
    
    //set captions of view
    [self setCaptionsView];
    
    //set forcus on home view
    [[HomeViewV2 shareHomeView] setSelectedButton:tagOfHomeButton];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Resize content views of current VC to fit with Navigation bar and tabbar
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        //Clear redundant view for header, footter
        CGRect frameHeaderFooter = roomTableView.tableHeaderView.frame;
        frameHeaderFooter.size.height = 0.1f;
        UIView *headerView = [[UIView alloc] initWithFrame:frameHeaderFooter];
        [roomTableView setTableHeaderView:headerView];
        
        frameHeaderFooter = roomTableView.tableFooterView.frame;
        frameHeaderFooter.size.height = 0.1f;
        UIView *footerView = [[UIView alloc] initWithFrame:frameHeaderFooter];
        [roomTableView setTableFooterView:footerView];
    }
    
    SuperRoomModelV2 *modelSuper= [[SuperRoomModelV2 alloc] init];
    if (topBarView == nil) {
        topBarView = [[TopbarViewV2 alloc] initViewWithSuperModel:modelSuper];
        [self.view addSubview:topBarView];
    }
    // Do any additional setup after loading the view from its nib.
    roomTableView.backgroundColor =[UIColor clearColor];
    
    //    UIColor *bgColor = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:backgroundImage]];
    //    self.view.backgroundColor = bgColor;
    ////    [bgColor release];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reLoadDataSyncDone:) name:[NSString stringWithFormat:@"%@", notificationReloadDataAllViewWhenSyncDone] object:nil];
    //set captions of view
    [self setCaptionsView];
    
    NSDictionary *dic = [[LanguageManagerV2 sharedLanguageManager] getCurrentDictionaryLanguage];
    
    NSString *firstTitleRow = nil;
    if (self.isPass) {
        firstTitleRow = [dic valueForKey:[NSString stringWithFormat:@"%@", L_PASSED_INSPECTED]];
    } else {
        firstTitleRow = [dic valueForKey:[NSString stringWithFormat:@"%@", L_FAILED_INSPECTED]];
    }
    //create title bar
    self.navigationItem.titleView = [MyNavigationBarV2 createTitleBarWithFirstRow:firstTitleRow secondRow:[[RoomManagerV2 sharedRoomManager] getHouseKeeperName] target:self selector:@selector(hiddenHeaderView) forControlEvents:UIControlEventTouchUpInside];
    //Move to middle of view
    self.navigationItem.titleView.bounds = [MyNavigationBarV2 middleFrameForNavigationTitleView:self.navigationItem.titleView];
    
    //add back button
    [self addBackButton];
}


// hidden header when tap navigationBar
#define heightScale 45

- (void) hiddenHeaderView {
    //    if (![UserManagerV2 isSupervisor]){
    BOOL isHidden = topBarView.hidden;
    [topBarView setHidden:!isHidden];
    
    
    CGRect frame1 = CGRectZero;
    CGRect frame2 = CGRectZero;
    CGRect frame3 = CGRectZero;
    CGRect frame4 = CGRectZero;
    CGRect frame5 = CGRectZero;
    
    if(!isHidden){
        frame1  = CGRectMake(0, roomTableView.frame.origin.y - heightScale, roomTableView.frame.size.width, roomTableView.frame.size.height + heightScale);
        frame2 = CGRectMake(labelRoom.frame.origin.x, labelRoom.frame.origin.y - heightScale, labelRoom.frame.size.width, labelRoom.frame.size.height );
        frame3 = CGRectMake(labelRMStatus.frame.origin.x, labelRMStatus.frame.origin.y - heightScale, labelRMStatus.frame.size.width, labelRMStatus.frame.size.height );
        frame4 = CGRectMake(labelVIP.frame.origin.x, labelVIP.frame.origin.y - heightScale, labelVIP.frame.size.width, labelVIP.frame.size.height );
        frame5 = CGRectMake(labelDurationUsed.frame.origin.x, labelDurationUsed.frame.origin.y - heightScale, labelDurationUsed.frame.size.width, labelDurationUsed.frame.size.height);
    } else
    {
        frame1 = CGRectMake(0, roomTableView.frame.origin.y + heightScale, roomTableView.frame.size.width, roomTableView.frame.size.height - heightScale);
        frame2 = CGRectMake(labelRoom.frame.origin.x, labelRoom.frame.origin.y + heightScale, labelRoom.frame.size.width, labelRoom.frame.size.height);
        frame3 = CGRectMake(labelRMStatus.frame.origin.x, labelRMStatus.frame.origin.y + heightScale, labelRMStatus.frame.size.width, labelRMStatus.frame.size.height);
        frame4 = CGRectMake(labelVIP.frame.origin.x, labelVIP.frame.origin.y + heightScale, labelVIP.frame.size.width, labelVIP.frame.size.height );
        frame5 = CGRectMake(labelDurationUsed.frame.origin.x, labelDurationUsed.frame.origin.y + heightScale, labelDurationUsed.frame.size.width, labelDurationUsed.frame.size.height );
    }
    
    [roomTableView setFrame:frame1];
    [labelRoom setFrame:frame2];
    [labelRMStatus setFrame:frame3];
    [labelVIP setFrame:frame4];
    [labelDurationUsed setFrame:frame5];
    //    }
}


//@ reload data after sync done.
-(void)reLoadDataSyncDone:(NSNotification *)notificate
{
    [self setDataTable];
}
//- (void)viewDidUnload
//{
//    [roomTableView release];
//    roomTableView = nil;
//    [tvCell release];
//    tvCell = nil;
//    [section1Data release];
//    self.section1Data = nil;
//    [section2Data release];
//    self.section2Data = nil;
//    [self setHotelLogo:nil];
//    [self setLblHouseKeeperName:nil];
//    [super viewDidUnload];
//    // Release any retained subviews of the main view.
//    // e.g. self.myOutlet = nil;
//}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark -
#pragma mark TableViewDelegate Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return numofSection;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if([self.section1Data count] > 0) {
        return [self.section1Data count];
    }
    
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([self.section1Data count] <= 0) {
        static NSString *sectionNoRoomRoomRecord = @"NoRoomRoomRecord";
        UITableViewCell *cellno = nil;
        cellno = [tableView dequeueReusableCellWithIdentifier:sectionNoRoomRoomRecord];
        if (cellno == nil) {
            cellno = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:sectionNoRoomRoomRecord] ;
            [cellno.textLabel setTextAlignment:NSTextAlignmentCenter];
            [cellno setSelectionStyle:UITableViewCellSelectionStyleNone];
        }
        if(isPass) {
            cellno.textLabel.text = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_title_no_passed_room];
        } else {
            cellno.textLabel.text = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_title_no_failed_room];
        }
        [cellno.textLabel setTextColor:[UIColor grayColor]];
        [cellno setBackgroundColor:[UIColor clearColor]];
        [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        return cellno;
    }
    
    static NSString *sectionIdentifier = @"SectionsIdentifier";
    static NSString *section2Identifier = @"Section2Identifier";
    NSUInteger section = [indexPath section];
    NSUInteger row = [indexPath row];
    UITableViewCell *cell = nil;
    if (section == 0) {
        cell = [tableView dequeueReusableCellWithIdentifier:sectionIdentifier];
    } else if ( section == 1 )
    {
        cell = [tableView dequeueReusableCellWithIdentifier:section2Identifier];
    }
    if (cell == nil) {
        if ( section == 0 )
        {
            NSArray *nibCell = [[NSBundle mainBundle] loadNibNamed:@"RoomCompletedCellV2" owner:self options:nil];
            
            if ([nibCell count] > 0) {
                cell = self.tvCell;
                
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            } else {
                if([LogFileManager isLogConsole])
                {
                    NSLog(@"failed to load RoomAssignmentCell nib file!");
                }
            }
        }
    }
    
    NSDictionary *rowData =[self.section1Data objectAtIndex:row];
    
    UILabel *labelGuestTitle = (UILabel *)[cell viewWithTag:tagGuestTitle];
    [labelGuestTitle setText:[NSString stringWithFormat:@"%@:",[[[LanguageManagerV2 sharedLanguageManager] getCurrentDictionaryLanguage] valueForKey:[NSString stringWithFormat:@"%@", L_GUEST_NAME]]]];
    
    UILabel *labelRoomNo = (UILabel *)[cell viewWithTag:tagRoomNo];
    labelRoomNo.text =[rowData objectForKey:kRoomNo];
    
    UILabel *lblRMStatus =(UILabel *)[cell viewWithTag:tagRMStatus];
    lblRMStatus.text =[rowData objectForKey:kRMStatus];
    
    UILabel *lblVIP =(UILabel *)[cell viewWithTag:tagVIP];
    lblVIP.text= [rowData objectForKey:kVIP];
    
    UILabel *lblDurationUsed =(UILabel *)[cell viewWithTag:tagDurationUsed];
    NSString* durationUsed = (NSString*)[rowData objectForKey:kDurationUsed];
    
    //calculate forward time or late cleaning time
    NSInteger durationInspected = [durationUsed integerValue];
    if (durationInspected < 0) {
        durationInspected = 0;
    }
    NSInteger expectedTime = [[rowData objectForKey:kExpected_Clean_time] integerValue] * 60;
    NSInteger differenceTime = 0;
    //    if (durationInspected > expectedTime) {
    //        differenceTime = durationInspected - expectedTime;
    //    } else {
    //        differenceTime = durationInspected;
    //    }
    differenceTime = durationInspected;
    
    NSString *durationString = nil;
    durationString = [DateTimeUtility getHH_MMFromSecond:(int)differenceTime];
    if (durationInspected > expectedTime) {
        [lblDurationUsed setText:[NSString stringWithFormat:@"-%@", durationString]];
    } else {
        [lblDurationUsed setText:[NSString stringWithFormat:@"+%@", durationString]];
    }
    
    if ([durationUsed intValue] > [[rowData objectForKey:kExpected_Clean_time] integerValue] * 60)  {
        [lblDurationUsed setTextColor: [UIColor redColor]];
    } else {
        [lblDurationUsed setTextColor: [UIColor greenColor]];
    }
    
    UILabel *labelGuestName = (UILabel *)[cell viewWithTag:tagGuestNameCell];
    labelGuestName.text = [rowData objectForKey:kGuestName];
    
    if (section == 0) {
        [cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
    } else {
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    
    UILabel *lblRoomType = (UILabel *)[cell viewWithTag:tagRoomType];
    lblRoomType.text = [rowData objectForKey:kRoomType];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger section = [indexPath section];
    if (section == 0) {
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        [self drawUnderlinedLabel:(UILabel *)[cell viewWithTag:tagRMStatus] inTableViewCell:cell];
        
        // Show hud when get room detail and get info before go to room detail
        MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
        [hud setLabelText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_RA_LOADING_DATA_CONTENT]];
        [hud show:YES];
        [self.tabBarController.view addSubview:hud];
        
        NSDictionary *rowData = [self.section1Data objectAtIndex:indexPath.row];
        
        //        NSInteger userId = [[[UserManagerV2 sharedUserManager] currentUser] userId];
        //        NSInteger raId = [[rowData objectForKey:kRoomAssignmentID] integerValue];
        //        NSInteger roomId = [[rowData objectForKey:kRoomNo] integerValue];
        //
        //        RoomModelV2 *roomModel = [[RoomModelV2 alloc] init];
        //        roomModel.room_Id = roomId;
        //
        //        // load room model
        //        roomModel.room_HotelId = -1;
        //        [[RoomManagerV2 sharedRoomManager] loadRoomModel:roomModel];
        //
        //        if (roomModel.room_HotelId == -1) {
        //            // Get room detail
        //            [[RoomManagerV2 sharedRoomManager] getRoomWSByUserID:userId AndRaID:raId AndLastModified:nil AndLastCleaningDate:nil];
        //        }
        //
        //        GuestInfoModelV2 *guestInfo = [[GuestInfoModelV2 alloc] init];
        //        guestInfo.guestRoomId = roomId;
        //        if (guestInfo.guestId == 0) {
        //            // Get guest info
        //            [[RoomManagerV2 sharedRoomManager] updateGuestInfo:userId WithRoomAssignID:raId];
        //        }
        
        RoomAssignmentInfoViewController *roomAssignmentInfoViewController = [[RoomAssignmentInfoViewController alloc] initWithOption:IS_PUSHED_FROM_COMPLETE andRoomAssignmentData:rowData];
        
        [TasksManagerV2 setCurrentRoomAssignment:[[rowData objectForKey:kRoomAssignmentID] integerValue]];
        
        if(self.isPass){
            roomAssignmentInfoViewController.isPushFrom = IS_PUSHED_FROM_PASSED_INSPECTION;
        } else {
            roomAssignmentInfoViewController.isPushFrom = IS_PUSHED_FROM_FAILED_INSPECTION;
        }
        
        [self.navigationController pushViewController:roomAssignmentInfoViewController animated:YES];
        [self.navigationItem.backBarButtonItem setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK]];
        
        //        if([UserManagerV2 isSupervisor]){
        //            if (isPass) {
        //                RoomInfoInspectedPassedV2 *aRoomInfo = [[RoomInfoInspectedPassedV2 alloc] initWithNibName:@"RoomInfoInspectedPassedV2" bundle:nil];
        //
        //                RoomModelV2 *roomModel = [[RoomModelV2 alloc] init];
        //                roomModel.room_Id = [[rowData objectForKey:kRoomNo] intValue];
        //                [[RoomManagerV2 sharedRoomManager] loadRoomModel:roomModel];
        //
        //                //set current room no
        //                [RoomManagerV2 setCurrentRoomNo:roomModel.room_Id];
        //
        //                GuestInfoModelV2 *guestInfoModel = [[GuestInfoModelV2 alloc] init];
        //                guestInfoModel.guestRoomId = roomModel.room_Id;
        //                [[RoomManagerV2 sharedRoomManager] loadByRoomIDGuestInfo:guestInfoModel];
        //
        //                RoomAssignmentModelV2 *roomAssignmentModel = [[RoomAssignmentModelV2 alloc] init];
        //                roomAssignmentModel.roomAssignment_Id = [[rowData objectForKey:kRoomAssignmentID] intValue];
        //                roomAssignmentModel.roomAssignment_UserId = [UserManagerV2 sharedUserManager].currentUser.userId;
        //                [[RoomManagerV2 sharedRoomManager] load:roomAssignmentModel];
        //
        //                aRoomInfo.roomName = [rowData objectForKey:kRoomNo];
        //                aRoomInfo.roomModel = roomModel;
        //                aRoomInfo.guestInfoModel = guestInfoModel;
        //                aRoomInfo.raDataModel = roomAssignmentModel;
        //
        //                //set current RoomAssignmentId use in ChecklistView
        //                [TasksManagerV2 setCurrentRoomAssignment:[[rowData objectForKey:kRoomAssignmentID] integerValue]];
        //                [self.navigationController pushViewController:aRoomInfo animated:YES];
        //                [aRoomInfo loadingData];
        //
        //            } else {
        //                RoomInfoInspectedFailedV2 *aRoomInfo = [[RoomInfoInspectedFailedV2 alloc] initWithNibName:@"RoomInfoInspectedFailedV2" bundle:nil];
        //                NSDictionary *rowData =[self.section1Data objectAtIndex:indexPath.row];
        //
        //                RoomModelV2 *roomModel = [[RoomModelV2 alloc] init];
        //                roomModel.room_Id = [[rowData objectForKey:kRoomNo] intValue];
        //                [[RoomManagerV2 sharedRoomManager] loadRoomModel:roomModel];
        //
        //                //set current room no
        //                [RoomManagerV2 setCurrentRoomNo:roomModel.room_Id];
        //
        //                GuestInfoModelV2 *guestInfoModel = [[GuestInfoModelV2 alloc] init];
        //                guestInfoModel.guestRoomId = roomModel.room_Id;
        //                [[RoomManagerV2 sharedRoomManager] loadByRoomIDGuestInfo:guestInfoModel];
        //
        //
        //                RoomAssignmentModelV2 *roomAssignmentModel = [[RoomAssignmentModelV2 alloc] init];
        //                roomAssignmentModel.roomAssignment_Id = [[rowData objectForKey:kRoomAssignmentID] intValue];
        //                roomAssignmentModel.roomAssignment_UserId = [UserManagerV2 sharedUserManager].currentUser.userId;
        //                [[RoomManagerV2 sharedRoomManager] load:roomAssignmentModel];
        //
        //                aRoomInfo.roomName = [rowData objectForKey:kRoomNo];
        //                aRoomInfo.roomModel = roomModel;
        //                aRoomInfo.guestInfoModel = guestInfoModel;
        //                aRoomInfo.raDataModel = roomAssignmentModel;
        //
        //                //set current RoomAssignmentId use in ChecklistView
        //                [TasksManagerV2 setCurrentRoomAssignment:[[rowData objectForKey:kRoomAssignmentID] integerValue]];
        //                [self.navigationController pushViewController:aRoomInfo animated:YES];
        //                [aRoomInfo loadingData];
        //            }
        //
        //            [self.navigationItem.backBarButtonItem setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK]];
        //        } else{
        //            NSDictionary *rowData =[self.section1Data objectAtIndex:indexPath.row];
        //            RoomAssignmentModelV2 *roomAssignmentModel = [[RoomAssignmentModelV2 alloc] init];
        //            roomAssignmentModel.roomAssignment_Id = [[rowData objectForKey:kRoomAssignmentID] intValue];
        //            roomAssignmentModel.roomAssignment_UserId = [UserManagerV2 sharedUserManager].currentUser.userId;
        //            [[RoomManagerV2 sharedRoomManager] load:roomAssignmentModel];
        //
        //            if (roomAssignmentModel.roomAssignmentRoomInspectionStatusId == ENUM_INSPECTION_COMPLETED_PASS) {
        //                RoomInfoCompletedPassedV2 *aRoomInfo = [[RoomInfoCompletedPassedV2 alloc] initWithNibName:@"RoomInfoCompletedPassedV2" bundle:nil];
        //                //NSDictionary *rowData =[self.section1Data objectAtIndex:indexPath.row];
        //
        //                RoomModelV2 *roomModel = [[RoomModelV2 alloc] init];
        //                roomModel.room_Id = [[rowData objectForKey:kRoomNo] intValue];
        //                [[RoomManagerV2 sharedRoomManager] loadRoomModel:roomModel];
        //
        //                //set current room no
        //                [RoomManagerV2 setCurrentRoomNo:roomModel.room_Id];
        //
        //                GuestInfoModelV2 *guestInfoModel = [[GuestInfoModelV2 alloc] init];
        //                guestInfoModel.guestRoomId = roomModel.room_Id;
        //                [[RoomManagerV2 sharedRoomManager] loadByRoomIDGuestInfo:guestInfoModel];
        //
        //
        //                aRoomInfo.roomName = [rowData objectForKey:kRoomNo];
        //                aRoomInfo.roomModel = roomModel;
        //                aRoomInfo.guestInfoModel = guestInfoModel;
        //                aRoomInfo.raDataModel = roomAssignmentModel;
        //                //set current RoomAssignmentId use in ChecklistView
        //                [TasksManagerV2 setCurrentRoomAssignment:[[rowData objectForKey:kRoomAssignmentID] integerValue]];
        //                [self.navigationController pushViewController:aRoomInfo animated:YES];
        //                [aRoomInfo loadingData];
        //                [self.navigationItem.backBarButtonItem setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK]];
        //            } else{
        //                RoomInfoCompletedFailedV2 *aRoomInfo = [[RoomInfoCompletedFailedV2 alloc] initWithNibName:@"RoomInfoCompletedFailedV2" bundle:nil];
        //                //NSDictionary *rowData =[self.section1Data objectAtIndex:indexPath.row];
        //
        //                RoomModelV2 *roomModel = [[RoomModelV2 alloc] init];
        //                roomModel.room_Id = [[rowData objectForKey:kRoomNo] intValue];
        //                [[RoomManagerV2 sharedRoomManager] loadRoomModel:roomModel];
        //
        //                //set current room no
        //                [RoomManagerV2 setCurrentRoomNo:roomModel.room_Id];
        //
        //                GuestInfoModelV2 *guestInfoModel = [[GuestInfoModelV2 alloc] init];
        //                guestInfoModel.guestRoomId = roomModel.room_Id;
        //                [[RoomManagerV2 sharedRoomManager] loadByRoomIDGuestInfo:guestInfoModel];
        //
        //
        //                aRoomInfo.roomName = [rowData objectForKey:kRoomNo];
        //                aRoomInfo.roomModel = roomModel;
        //                aRoomInfo.guestInfoModel = guestInfoModel;
        //                aRoomInfo.raDataModel = roomAssignmentModel;
        //                //set current RoomAssignmentId use in ChecklistView
        //                [TasksManagerV2 setCurrentRoomAssignment:[[rowData objectForKey:kRoomAssignmentID] integerValue]];
        //                [self.navigationController pushViewController:aRoomInfo animated:YES];
        //                [aRoomInfo loadingData];
        //                [self.navigationItem.backBarButtonItem setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK]];
        //            }
        //        }
        
        [cell setSelected:NO animated:NO];
        
        // hide hud
        [hud setHidden:YES];
        [hud removeFromSuperview];
    }
}

-(void)setCaptionsView {
    NSDictionary *dic = [[LanguageManagerV2 sharedLanguageManager] getCurrentDictionaryLanguage];
    self.labelRoom.text = [dic valueForKey:[NSString stringWithFormat:@"%@", L_rm]];
    self.labelRMStatus.text = [dic valueForKey:[NSString stringWithFormat:@"%@", L_RM_STATUS]];
    self.labelVIP.text = [dic valueForKey:[NSString stringWithFormat:@"%@", L_VIP]];
    self.labelDurationUsed.text = [dic valueForKey:[NSString stringWithFormat:@"%@", L_DURATION_USED]];
    
    /*
    UIButton *label = (UIButton *)self.navigationItem.titleView;
    if (self.isPass) {
        [label.titleLabel setText:[dic valueForKey:[NSString stringWithFormat:@"%@", L_PASSED_INSPECTED]]];
    } else {
        [label.titleLabel setText:[dic valueForKey:[NSString stringWithFormat:@"%@", L_FAILED_INSPECTED]]];
    }*/
    
    //refresh topview
    [topBarView refresh:[[SuperRoomModelV2 alloc] init]];
}

#pragma mark - === Handle Back Button ===
#pragma mark

-(void)backBarPressed {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)addBackButton {
    
    //back button navigation
    self.navigationItem.leftBarButtonItem = [MyNavigationBarV2 createBackButtonWithTarget:self selector:@selector(backBarPressed) forControlEvents:UIControlEventTouchUpInside];
}

@end









