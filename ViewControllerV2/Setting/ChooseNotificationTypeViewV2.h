//
//  ChooseNotificationTypeViewV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
#import <UIKit/UIKit.h>


@class ChooseNotificationTypeViewV2;

@protocol ChooseNotificationTypeViewV2Delegate <NSObject>

@optional

-(void)ChooseNotificationTypeViewV2Controller:(ChooseNotificationTypeViewV2 *)controller didFinishSetupWithInfo:(NSString *)info;

@end
//@ 

@interface ChooseNotificationTypeViewV2 : UIViewController<UIPickerViewDelegate,UIPickerViewDataSource> {
    
    IBOutlet UIPickerView *pickerView;
    IBOutlet UIButton *btnExit;
    __unsafe_unretained id<ChooseNotificationTypeViewV2Delegate> delegate;  
    NSMutableArray *dataNotifications;
    NSInteger rowSelected;
    IBOutlet UIBarButtonItem *labelCancel;
    IBOutlet UIBarButtonItem *labelDone;
    
    NSString *sNotificationPopup;
    NSString *sNotificationVibrate;
    NSString *sNotificationRing;
    
    BOOL isAlreadyLayoutSubviews;
}

- (IBAction)cancel:(id)sender;
- (IBAction)done:(id)sender;
- (IBAction)ExitFromThisView:(id)sender;
@property (nonatomic, assign) id<ChooseNotificationTypeViewV2Delegate> delegate;
@property (nonatomic ,strong) UIButton *btnExit;
@property (nonatomic ,strong) NSMutableArray *dataNotifications;
@property (nonatomic ,strong) NSString *sNotificationPopup;
@property (nonatomic ,strong) NSString *sNotificationVibrate;
@property (nonatomic ,strong) NSString *sNotificationRing;

-(void) setCaptionsView;
-(void) setSelectedRowWithData:(NSString *) currentData;
@end
