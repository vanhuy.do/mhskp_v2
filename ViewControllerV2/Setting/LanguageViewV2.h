//
//  LanguageViewV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "LanguageModelV2.h"
#import "LanguageManager.h"
//#import "UserManager.h"
@class LanguageViewV2;

@protocol LanguageViewV2Delegate <NSObject>

@optional

-(void)languageViewController:(LanguageViewV2 *)controller didFinishSetupLanguageWithInfo:(LanguageModelV2 *)languageInfo;

@end

@interface LanguageViewV2 : UIViewController<UIPickerViewDelegate,UIPickerViewDataSource> 
{
    __unsafe_unretained id<LanguageViewV2Delegate> delegate;   
    LanguageModelV2 *languageInfo;
    NSMutableArray *arrayLanguages;
    IBOutlet UIPickerView *pickerView;
    LanguageModelV2 *currentLanguage;
    Boolean selectedLanguage;
    IBOutlet UIBarButtonItem *buttonDone;
    IBOutlet UIBarButtonItem *buttonCancel;
    IBOutlet UINavigationItem *navigationItemLanguage;
    BOOL isSetting;
}

@property (nonatomic, strong ) LanguageModelV2  *languageInfo;
@property (nonatomic, strong) NSMutableArray  *arrayLanguages;
@property (nonatomic) Boolean selectedLanguage;
@property (nonatomic, readwrite) BOOL isSetting;
-(IBAction)done:(id) sender;
- (IBAction)cancel:(id)sender;

@property (nonatomic, assign) id<LanguageViewV2Delegate> delegate;

-(void) setCaptionsView;
-(void) setSelectedRowWithData:(NSString *) currentData;
@end
