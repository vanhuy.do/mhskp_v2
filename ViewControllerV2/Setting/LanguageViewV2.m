//
//  LanguageViewV2.m
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LanguageViewV2.h"
#import "LanguageModel.h"
#import "DeviceManager.h"

@implementation LanguageViewV2

@synthesize arrayLanguages;
@synthesize selectedLanguage;
@synthesize delegate;
@synthesize languageInfo;
@synthesize isSetting;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    //set captions for view
    [self setCaptionsView];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.arrayLanguages = [[LanguageManagerV2 sharedLanguageManager] loadAllLanguageModel];
    self.selectedLanguage = NO;
    [pickerView reloadAllComponents];
    [pickerView reloadInputViews];
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        CGRect pickerViewFrame = pickerView.frame;
        UIToolbar *blurView =  [[UIToolbar alloc] initWithFrame:pickerViewFrame];
        //translucentView.tintColor = [UIColor colorWithRed:0.169 green:0 blue:0.192 alpha:0.2];
        blurView.translucent = YES;
        [self.view insertSubview:blurView belowSubview:pickerView];
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


-(IBAction)done:(id) sender
{
    if (arrayLanguages == nil || [arrayLanguages count] == 0) {
        [self.view removeFromSuperview];
        return;
    }
    
    int index = (int)[pickerView selectedRowInComponent:0];        
    if (index >= 0)
        languageInfo = [arrayLanguages objectAtIndex:index];

    //isSetting is NO, languageView in Login
    //isSetting is YES, languageView in Setting
    if (isSetting == NO) {
        if ([languageInfo.lang_id isEqual:ENGLISH_LANGUAGE]) {
            [[LanguageManagerV2 sharedLanguageManager] setCurrentLanguage:languageInfo.lang_id];
        }

    }
    
    [self.view removeFromSuperview];
    // call method didFinishSetupLanguageWithInfo in LogIn class and return LogInView. 
    if ([delegate respondsToSelector:@selector(languageViewController:didFinishSetupLanguageWithInfo:)]) 
    {        
        [delegate languageViewController:self didFinishSetupLanguageWithInfo:languageInfo];
    }
    
}

- (IBAction)cancel:(id)sender {
    [self.view removeFromSuperview];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
#pragma mark
#pragma mark  pickerview datasource/delegate
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [arrayLanguages count];
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    LanguageModelV2 *lang=[arrayLanguages objectAtIndex:row];    
    return lang.lang_name;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    self.selectedLanguage=YES;
}

//set caption for view base on user lang
-(void)setCaptionsView {

    [buttonDone setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_DONE]];
    [buttonCancel setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_CANCEL]];
    [navigationItemLanguage setTitle:[[LanguageManagerV2 sharedLanguageManager] getLanguage]];
}

-(void)setSelectedRowWithData:(NSString *)currentData {
    
    for (LanguageModelV2 *model in arrayLanguages) {
        if ([currentData isEqualToString:model.lang_id] == YES) {
            [pickerView selectRow:[arrayLanguages indexOfObject:model] inComponent:0 animated:NO];
        }
    }
}

@end
