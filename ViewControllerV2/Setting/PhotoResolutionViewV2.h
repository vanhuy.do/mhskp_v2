//
//  PhotoResolutionViewV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LanguageManagerV2.h"
#import "ehkDefines.h"

@class PhotoResolutionViewV2;

@protocol PhotoResolutionViewV2Delegate <NSObject>

@optional

-(void)photoResolutionViewV2Controller:(PhotoResolutionViewV2 *)controller didSelectResolutionAt:(NSInteger)index;

@end

@interface PhotoResolutionViewV2 : UIViewController<UIPickerViewDelegate,UIPickerViewDataSource> {
    
    IBOutlet UIPickerView *pickerView;
    IBOutlet UIButton *btnExit;
    NSMutableArray *dataPhotoResolution;
    NSInteger rowSelected;
    IBOutlet UIBarButtonItem *labelCancel;
    IBOutlet UIBarButtonItem *labelDone;
    NSString *sPhotoLow;
    NSString *sPhotoMedium;
    NSString *sPhotoHeight;
    BOOL isAlreadyLayoutSubviews;
}

- (IBAction)cancel:(id)sender;
- (IBAction)done:(id)sender;
- (IBAction)ExitFromThisView:(id)sender;
@property (nonatomic, assign) id<PhotoResolutionViewV2Delegate> delegate;
@property (nonatomic ,strong ) UIButton *btnExit;
@property (nonatomic ,strong) NSMutableArray *dataPhotoResolution;
@property (nonatomic ,strong) NSString *sPhotoLow;
@property (nonatomic ,strong) NSString *sPhotoMedium;
@property (nonatomic ,strong) NSString *sPhotoHeight;

-(void) setCaptionsView;
-(void) setSelectedRowWithData:(NSString *) currentData;

@end
