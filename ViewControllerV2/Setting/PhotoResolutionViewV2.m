//
//  PhotoResolutionViewV2.m
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "PhotoResolutionViewV2.h"
#import "DeviceManager.h"

@implementation PhotoResolutionViewV2

@synthesize delegate,btnExit,dataPhotoResolution;
@synthesize sPhotoLow, sPhotoMedium, sPhotoHeight;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    isAlreadyLayoutSubviews = NO;
    self.dataPhotoResolution = [NSMutableArray array]; 
    //default  choose row 0
    rowSelected=0;
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

//Call after set frames serveral subviews of this view
-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0") && !isAlreadyLayoutSubviews){
        isAlreadyLayoutSubviews = YES;
        CGRect framePickerView = pickerView.frame;
        UIToolbar *blurView = [[UIToolbar alloc] initWithFrame:framePickerView];
        blurView.translucent = YES;
        [self.view insertSubview:blurView belowSubview:pickerView];
    }
}

#pragma mark
#pragma mark  pickerview datasource/delegate
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return [self.dataPhotoResolution count];
}
-(NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return [self.dataPhotoResolution objectAtIndex:row];
}
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    rowSelected = row;
}
- (IBAction)cancel:(id)sender {
    [self.view removeFromSuperview];
}
- (IBAction)done:(id)sender {
    [delegate photoResolutionViewV2Controller:self  didSelectResolutionAt:rowSelected];
    [self.view removeFromSuperview];
}
- (IBAction)ExitFromThisView:(id)sender {
    [self.view removeFromSuperview];
   
    
}
#pragma mark - set captions of view
-(void)setCaptionsView {    
    [labelCancel setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_CANCEL]];
    [labelDone setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_DONE]];
    
//    sPhotoLow = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_resolution_72DPI];
//    sPhotoMedium = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_resolution_82DPI];
//    sPhotoHeight = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_resolution_92DPI];
   
    sPhotoLow = SETTING_240_180;
    sPhotoMedium = SETTING_320_240;
    sPhotoHeight = SETTING_480_360;
    [dataPhotoResolution removeAllObjects];
    
    [dataPhotoResolution addObject:sPhotoLow];
    [dataPhotoResolution addObject:sPhotoMedium];
    [dataPhotoResolution addObject:sPhotoHeight];
    
    [pickerView reloadAllComponents];
}

-(void)setSelectedRowWithData:(NSString *)currentData
{
    if ([currentData isEqualToString:sPhotoLow] == YES) {
        rowSelected = [dataPhotoResolution indexOfObject:sPhotoLow];
        [pickerView selectRow:rowSelected inComponent:0 animated:NO];
    }
    if ([currentData isEqualToString:sPhotoMedium] == YES) {
        rowSelected = [dataPhotoResolution indexOfObject:sPhotoMedium];
        [pickerView selectRow:rowSelected inComponent:0 animated:NO];
    }
    if ([currentData isEqualToString:sPhotoHeight] == YES) {
        rowSelected = [dataPhotoResolution indexOfObject:sPhotoHeight];
        [pickerView selectRow:rowSelected inComponent:0 animated:NO];
    }
}

@end
