//
//  PickerView.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
@class PickerData;

@protocol PickerViewDelegate <NSObject>

@optional
-(void)didSelectData:(PickerData*)data index:(int)indexSelected typeOperation:(int)typeOperation;
@end

@interface PickerData : NSObject
@property (nonatomic) int valueData;
@property (nonatomic, strong) NSString *displayData;

@end

@interface PickerView : UIViewController<UIPickerViewDelegate,UIPickerViewDataSource> {
    
    IBOutlet UIPickerView *pickerView;
    IBOutlet UIButton *btnExit;
    IBOutlet UIBarButtonItem *labelCancel;
    IBOutlet UIBarButtonItem *labelDone;
    int rowSelected;
    BOOL isAlreadyLayoutSubviews;
}

@property (nonatomic, assign) id<PickerViewDelegate> delegate;
@property (nonatomic, strong) UIButton *btnExit;
@property (nonatomic, strong) NSMutableArray *listPickerData;
@property (nonatomic) int typeOfOperation;

- (IBAction)cancel:(id)sender;
- (IBAction)done:(id)sender;
- (IBAction)ExitFromThisView:(id)sender;
-(void)setSelectedIndex:(int)indexSelected;

@end
