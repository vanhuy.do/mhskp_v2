//
//  PickerView.m
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "PickerView.h"
#import "LanguageManager.h"
#import "DeviceManager.h"

@implementation PickerData
@synthesize valueData, displayData;
@end

@implementation PickerView

@synthesize delegate, btnExit, listPickerData, typeOfOperation;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}


#pragma mark - View lifecycle

//Call after set frames serveral subviews of this view
-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0") && !isAlreadyLayoutSubviews){
        isAlreadyLayoutSubviews = YES;
        CGRect framePickerView = pickerView.frame;
        UIToolbar *blurView = [[UIToolbar alloc] initWithFrame:framePickerView];
        blurView.translucent = YES;
        [self.view insertSubview:blurView belowSubview:pickerView];
    }
}

- (void)viewDidLoad
{
    isAlreadyLayoutSubviews = NO;
    //default  choose row 0
    rowSelected = 0;
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setCaptionsView];
}

- (void)viewDidUnload
{    
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark
#pragma mark  pickerview datasource/delegate
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return [listPickerData count];
}
-(NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    PickerData *curPickDate = [listPickerData objectAtIndex:row];
    return curPickDate.displayData;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    rowSelected = (int)row;
}

- (IBAction)cancel:(id)sender {
    [self.view removeFromSuperview];
}
- (IBAction)done:(id)sender {
    [self.view removeFromSuperview];
    
    PickerData *dataPicked = [listPickerData objectAtIndex:rowSelected];
    if([delegate respondsToSelector:@selector(didSelectData:index:typeOperation:)]){
        [delegate didSelectData:dataPicked index:rowSelected typeOperation:typeOfOperation];
    }
}

- (IBAction)ExitFromThisView:(id)sender {
    [self.view removeFromSuperview];
}
#pragma mark - set captions of view
-(void)setCaptionsView {   
    [labelCancel setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_CANCEL]];
    [labelDone setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_DONE]];
    [pickerView reloadAllComponents];
}

-(void)setSelectedIndex:(int)indexSelected
{
    if ([listPickerData count] > 0){
        if (indexSelected < 0){
            indexSelected = 0;
        }
        
        if(indexSelected > [listPickerData count] - 1){
            indexSelected = 0;
        }
        
        rowSelected = indexSelected;
        [pickerView selectRow:rowSelected inComponent:0 animated:NO];
    }
}

@end
