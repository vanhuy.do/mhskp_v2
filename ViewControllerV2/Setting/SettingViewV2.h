//
//  SettingViewV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChooseNotificationTypeViewV2.h"
#import "LanguageViewV2.h"
#import "PhotoResolutionViewV2.h"
#import "SynchViewV2.h"
#import "SettingModelV2.h"
#import "RemarkViewV2.h"
#import "ehkConvert.h"
#import "ehkDefinesV2.h"
#import "TopbarViewV2.h"
#import "DayPickerView.h"
#import "LogViewController.h"
#import "AccessRight.h"
#import "PickerView.h"
#import "MBSwitch.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

@protocol SettingViewDelegate <NSObject>

-(void) btnSubmitClicked;

@end

@interface SettingViewV2 : UIViewController<UINavigationControllerDelegate, UIAlertViewDelegate,UITextFieldDelegate,ChooseNotificationTypeViewV2Delegate,LanguageViewV2Delegate, SynchViewV2Delegate,PhotoResolutionViewV2Delegate, DayPickerViewDelegate, PickerViewDelegate, MFMailComposeViewControllerDelegate>{
    IBOutlet UIButton *btnRemarkView;
    IBOutlet UITextField *txtService;
    IBOutlet UITextField *txtSynPeriod;
    IBOutlet UIButton *optionNotification;
    IBOutlet UIButton *optionLanguage;
    IBOutlet UIButton *optionSynch;
    IBOutlet UIButton *optionPhotoResolution;
    IBOutlet UILabel *labelSyncPeriod;
    IBOutlet UILabel *labelNotification;
    IBOutlet UILabel *labelLanguage;
    IBOutlet UILabel *labelPhotoResolution;
    
    //IBOutlet UILabel *lblDemoMode;
    IBOutlet UILabel *lblRoomValidate;
    IBOutlet UILabel *lblLog;
    IBOutlet UILabel *lblWSLog;
    IBOutlet UILabel *lblLogNo;
    IBOutlet UILabel *lblDefaultTimeOut;
    IBOutlet UILabel *lblRetrialTimes;
    IBOutlet UITextField *txtDefaultTimeOut;
    IBOutlet UITextField *txtRetrialTimes;
    
    IBOutlet UIButton *btnSubmit;
    
    //Switch old style (for iOS 6 and older versions)
    IBOutlet UISwitch *switchLogWS;
    IBOutlet UISwitch *switchIsLog;
    
    
    __weak IBOutlet UISwitch *switchWifi;
    __weak IBOutlet UISwitch *switch3G;
    MBSwitch *switchWifi2;
    MBSwitch *switch3G2;
    //IBOutlet UISwitch *switchDemoMode;
    IBOutlet UISwitch *switchValidateRoom;
    
    //Switch new style (for iOS 7 display)
    MBSwitch *switchIsLog2;
    MBSwitch *switchLogWS2;
    //MBSwitch *switchDemoMode2;
    MBSwitch *switchValidateRoom2;
    IBOutlet UILabel *messagePeriodLabel;
    IBOutlet UIButton *messagePeriodButton;
    
    /** dthanhson start edit */
    
    IBOutlet UITextField *txtLogNumber;
    IBOutlet UIScrollView *scrollView;
    
    UITextField *selectedTextField;
    BOOL _keyboardIsVisible;
    
    CGFloat prevYOffset;
    /** end edit */
    
    IBOutlet UILabel *lblWSURL;
    IBOutlet UITextField *txtWSURL;
    
    IBOutlet UIImageView *imgviewBackground;
    
    ChooseNotificationTypeViewV2 *chooseNotificationView;
    LanguageViewV2 *languagesView;
    PhotoResolutionViewV2 *photoResolutionViewV2;
    SynchViewV2 *synchViewV2;
    SettingModelV2 *saveSettingModel;
    DayPickerView *dayPicker;
    LogViewController *logView;
    
    NSString *sNotificationPopup;
    NSString *sNotificationVibrate;
    NSString *sNotificationRing;
    NSString *sSynchManual;
    NSString *sSynchAutomatic;
    NSString *sPhotoLow;
    NSString *sPhotoMedium;
    NSString *sPhotoHeight;
    NSString *currentLanguage;
    NSInteger originMode;
    
    BOOL isConfigAccount;
    
    //isResetDatabase = True if WS URL Changed
    //false if WS URL not changed
    BOOL isClearedDatabase;
    id<SettingViewDelegate> delegate;
    
    AccessRight *settings;
    
    IBOutlet UIButton *btnViewLog;
    IBOutlet UIButton *btnResetDatabase;
    IBOutlet UIButton *btnExportDatabase;
    __weak IBOutlet UIButton *btnSendEmail;
    __weak IBOutlet UIView *viewLastItem;
    __weak IBOutlet UIButton *btnPTTConfig;
    __weak IBOutlet UIScrollView *scroll;
    //IBOutlet UIButton *btnReset;
    
    __weak IBOutlet UISwitch *switchBackgroundMode; // CFG [20160927/CRF-00001432]
    MBSwitch *switchBackgroundMode2; // CFG [20160927/CRF-00001432]
    
    IBOutlet UITextField *txtHeartbeatServer; // CFG [20160927/CRF-00001432]
    IBOutlet UITextField *txtHeartbeatPort; // CFG [20160927/CRF-00001432]
}
- (IBAction)btnRemarkView:(id)sender;
- (IBAction)butSetServicePressed:(id)sender;
- (IBAction)ChooseTypeNotification:(id)sender;
- (IBAction)ChooseLanguage:(id)sender;
- (IBAction)ChooseSynch:(id)sender;
- (IBAction)ChoosePhotoResolution:(id)sender;
- (IBAction)btnResetDatabaseClick:(id)sender;
- (IBAction)btnExportDatabaseClick:(id)sender;
- (IBAction)btnViewLogClick:(id)sender;
- (IBAction)btnResetClick:(id)sender;
- (IBAction)btnReloadSystemDataClick:(id)sender;
- (IBAction)enabelLog:(id)sender;
- (IBAction)btnSendEmailClick:(id)sender;
- (IBAction) btnSubmitClicked:(id) sender;
- (IBAction)enableLogWS:(id)sender;
- (IBAction) validationRoomChanged:(id)sender;
- (IBAction)switchConnection:(id)sender;
- (IBAction)btnPTTConfigClick:(UIButton *)sender;
- (IBAction)switchBackgroundModeOnChange:(id)sender; // CFG [20160927/CRF-00001432]

@property (nonatomic,strong) ChooseNotificationTypeViewV2 *chooseNotificationView;
@property (nonatomic ,strong) LanguageViewV2 *languagesView;
@property (nonatomic ,strong) SettingModelV2 *saveSettingModel;
@property (nonatomic ,strong) PhotoResolutionViewV2 *photoResolutionViewV2;
@property (nonatomic ,strong) SynchViewV2 *synchViewV2;
@property (nonatomic, strong) DayPickerView *dayPicker;
@property (nonatomic, strong) LogViewController *logView;
@property (nonatomic, strong) PickerView *pickerMessagePeriod;
@property (nonatomic, strong) PickerView *pickerLog;

@property (strong, nonatomic) IBOutlet UIView *vSetting;
@property (nonatomic ,strong) NSString *sNotificationPopup;
@property (nonatomic ,strong) NSString *sNotificationVibrate;
@property (nonatomic ,strong) NSString *sNotificationRing;
@property (nonatomic ,strong) NSString *sSynchManual;
@property (nonatomic ,strong) NSString *sSynchAutomatic;
@property (nonatomic ,strong) NSString *sPhotoLow;
@property (nonatomic ,strong) NSString *sPhotoMedium;
@property (nonatomic ,strong) NSString *sPhotoHeight;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *lblSyncPeriod;
@property (unsafe_unretained, nonatomic) IBOutlet UITextField *txtSyncPeriod;
@property (nonatomic, strong) MFMailComposeViewController *mailViewController;

-(void)saveSetting:(NSNotification*)nt;
-(void)loadSettingData;
-(void) setCaptionsView;
-(id) initForConfigAccount:(BOOL) isConfigAccount with:(id<SettingViewDelegate>) delegate;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil;

@end
