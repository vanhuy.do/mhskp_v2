//
//  SettingViewV2.m
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SettingViewV2.h"
#import "ehkDefines.h"
#import "SettingModelV2.h"
#import "SettingAdapterV2.h"
#import "MBProgressHUD.h"
#import "NetworkCheck.h"
#import "CommonVariable.h"
//#import "HomeViewV2.h"
#import "LogFileManager.h"
#import "ehkDefines.h"
#import "eHouseKeepingAppDelegate.h"
#import "iToast.h"
#import "AccessRight.h"
#import "MyNavigationBarV2.h"
#import "DeviceManager.h"
#import "ehkDefines.h"
#import "HomeViewV2.h"
#import "CustomAlertViewV2.h"
#import "RoomManagerV2.h"
#import "RoomManagerV2Demo.h"
#import "NSFileManager+DoNotBackup.h"
#import "UserManagerV2Demo.h"
#if DEFAULT_ENABLE_PTT
#import <PTTLibrary/SettingViewController.h>
#import <PTTLibrary/userdefault.h>
#endif

#define tagClearData 88
//#define tagConfirmDemoMode 24
#define tagConfirmResetDatabase 25
#define OperationPickMessageData 111
#define OperationPickLogData 112
#define isDemoMode ([[NSUserDefaults standardUserDefaults] integerForKey:ENABLE_DEMO_MODE] == 1)

@interface SettingViewV2 (PrivateMethods)

#define tagTopbarView 1234

-(void) loadTopbarView;
-(void) showTopbarView;
-(void) hideTopbarView;
-(void) setHiddenTopbarView;
-(void) adjustShowForViews;
-(void) adjustRemoveForViews;
-(void) addButtonHandleShowHideTopbar;
-(void) topbarTapped:(UIButton *) sender;
-(TopbarViewV2 *) getTopBarView;
-(void) keyboardWillShow;
-(void) keyboardWillHide;

@end

@implementation SettingViewV2

@synthesize lblSyncPeriod;
@synthesize txtSyncPeriod;
@synthesize vSetting;
@synthesize chooseNotificationView,languagesView,saveSettingModel,photoResolutionViewV2,synchViewV2;
@synthesize sNotificationPopup, sNotificationVibrate, sNotificationRing, sSynchAutomatic, sSynchManual;
@synthesize sPhotoLow, sPhotoMedium, sPhotoHeight;
@synthesize dayPicker;
@synthesize logView;
@synthesize pickerMessagePeriod;
@synthesize pickerLog;
-(void) saveData {
    [self saveSetting:nil];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(id) initForConfigAccount:(BOOL) _isConfigAccount with:(id<SettingViewDelegate>) _delegate
{
    self = [super initWithNibName:@"SettingViewV2" bundle:nil];
    if (self) {
        isConfigAccount = _isConfigAccount;
        delegate = _delegate;
    }
    return self;
}
- (IBAction)btnResetClick:(id)sender{
    
}
- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - Load Access Right
-(void) loadAccessRights
{
    AccessRightModel *currentUserAccessRight = [[UserManagerV2 sharedUserManager] currentUserAccessRight];
    settings = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.setting];
}

-(void) disableAllControls
{
    [txtDefaultTimeOut setUserInteractionEnabled:NO];
    [txtRetrialTimes setUserInteractionEnabled:NO];
    [btnSubmit setUserInteractionEnabled:NO];
    [optionNotification setUserInteractionEnabled:NO];
    [optionLanguage setUserInteractionEnabled:NO];
    [optionSynch setUserInteractionEnabled:NO];
    [optionPhotoResolution setUserInteractionEnabled:NO];
    [switchLogWS setUserInteractionEnabled:NO];
    [switchLogWS2 setUserInteractionEnabled:NO];
    [txtLogNumber setUserInteractionEnabled:NO];
    [switchIsLog setUserInteractionEnabled:NO];
    [switchIsLog2 setUserInteractionEnabled:NO];
//    [switchDemoMode setUserInteractionEnabled:NO];
//    [switchDemoMode2 setUserInteractionEnabled:NO];
    [txtWSURL setUserInteractionEnabled:NO];
    [switchValidateRoom setUserInteractionEnabled:NO];
    [switchValidateRoom2 setUserInteractionEnabled:NO];
    [messagePeriodButton setUserInteractionEnabled:NO];
    [switch3G setUserInteractionEnabled:NO];
    [switch3G2 setUserInteractionEnabled:NO];
    [switchWifi setUserInteractionEnabled:NO];
    [switchWifi2 setUserInteractionEnabled:NO];
    [switchBackgroundMode setUserInteractionEnabled:NO]; // CFG [20160927/CRF-00001432]
    [switchBackgroundMode2 setUserInteractionEnabled:NO]; // CFG [20160927/CRF-00001432]
}

-(void) enableAllControls
{
    [txtDefaultTimeOut setUserInteractionEnabled:YES];
    [txtRetrialTimes setUserInteractionEnabled:YES];
    [btnSubmit setUserInteractionEnabled:YES];
    [optionNotification setUserInteractionEnabled:YES];
    [optionLanguage setUserInteractionEnabled:YES];
    [optionSynch setUserInteractionEnabled:YES];
    [optionPhotoResolution setUserInteractionEnabled:YES];
    [switchLogWS setUserInteractionEnabled:YES];
    [switchLogWS2 setUserInteractionEnabled:YES];
    [txtLogNumber setUserInteractionEnabled:YES];
    [switchIsLog setUserInteractionEnabled:YES];
    [switchIsLog2 setUserInteractionEnabled:YES];
//    [switchDemoMode setUserInteractionEnabled:YES];
//    [switchDemoMode2 setUserInteractionEnabled:YES];
    [txtWSURL setUserInteractionEnabled:YES];
    [switchValidateRoom setUserInteractionEnabled:YES];
    [switchValidateRoom2 setUserInteractionEnabled:YES];
    [messagePeriodButton setUserInteractionEnabled:YES];
    [switch3G setUserInteractionEnabled:YES];
    [switch3G2 setUserInteractionEnabled:YES];
    [switchWifi setUserInteractionEnabled:YES];
    [switchWifi2 setUserInteractionEnabled:YES];
    [switchBackgroundMode setUserInteractionEnabled:YES]; // CFG [20160927/CRF-00001432]
    [switchBackgroundMode2 setUserInteractionEnabled:YES]; // CFG [20160927/CRF-00001432]
}

#pragma mark - View lifecycle
-(void)viewWillDisappear:(BOOL)animated {
    //UIButton *butFind = (UIButton *)[[self.tabBarController tabBar] viewWithTag:tagOfFindButton];
    //[butFind setEnabled:YES];
    
    [super viewWillDisappear:animated];
}

-(void)viewWillAppear:(BOOL)animated {
        //show wifi view
    [super viewWillAppear:animated];
    
    [btnViewLog setTitle:[L_View_Log currentKeyToLanguage] forState:UIControlStateNormal];
    [lblWSLog setText:[L_Log_WS currentKeyToLanguage]];
    [lblLogNo setText:[L_Log_No currentKeyToLanguage]];
    [btnSubmit setTitle:[L_SUBMIT currentKeyToLanguage] forState:UIControlStateNormal];
    
    [self loadAccessRights];
//    if(isDemoMode)
//        originMode = 1;
//    else
//        originMode = 0;
    if(!isDemoMode){
        [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationShowWifiView] object:nil];
    }
//    [[self.tabBarController tabBar] setHidden:NO];
    
    if (isConfigAccount) {
        [self.navigationItem setHidesBackButton:YES];
        
        //Hao Tran - Remove, because viewDidLayoutSubviews handle this
        //[self initializeViewForConfigAccount];
    }
    
    //Check access right only view
    if(!isConfigAccount)
    {
        if(settings.isAllowedView)
        {
            [self disableAllControls];
        }
        else
        {
            [self enableAllControls];
        }
    }
    else
    {
        [self enableAllControls];
    }
    
    //Hao Tran - Remove
    //[self setCaptionsView];
    
    //Move to view didappear
    //[self loadSettingData];
    
    //back button navigation
    self.navigationItem.leftBarButtonItem = [MyNavigationBarV2 createBackButtonWithTarget:self selector:@selector(navigateBack) forControlEvents:UIControlEventTouchUpInside];
}

-(void) showAllSwitchBelongToOSVersion
{
//    [switchDemoMode2 setHidden:!isConfigAccount];
//    CGRect switchDemoModeFrame = switchDemoMode.frame;
//    switchDemoMode2 = [[MBSwitch alloc] initWithFrame:switchDemoModeFrame];//get frame was made by IBOutLet
//    [switchDemoMode2 setTintColor:[UIColor redColor]];
//    [switchDemoMode2 setOffTintColor:[UIColor redColor]];
//    [switchDemoMode2 setOnTintColor:[UIColor greenColor]];
//    [switchDemoMode2 addTarget:self action:@selector(enableDemoMode:) forControlEvents:UIControlEventValueChanged];
//    [vSetting addSubview:switchDemoMode2];
    
     CGRect switchValidateRoomFrame = switchValidateRoom.frame;
     //switchValidateRoomFrame.origin.x += 27; //right edge for ios 7
     switchValidateRoom2 = [[MBSwitch alloc] initWithFrame:switchValidateRoomFrame]; //get frame was made by IBOutLet
     [switchValidateRoom2 setTintColor:[UIColor redColor]];
     [switchValidateRoom2 setOffTintColor:[UIColor redColor]];
     [switchValidateRoom2 setOnTintColor:[UIColor greenColor]];
     [switchValidateRoom2 addTarget:self action:@selector(validationRoomChanged:) forControlEvents:UIControlEventValueChanged];
     [vSetting addSubview:switchValidateRoom2];
    
    
    CGRect switchIsLogFrame = switchIsLog.frame;
    //switchIsLogFrame.origin.x += 27; //right edge for ios 7
    switchIsLog2 = [[MBSwitch alloc] initWithFrame:switchIsLogFrame];//get frame was made by IBOutLet
    [switchIsLog2 setTintColor:[UIColor redColor]];
    [switchIsLog2 setOffTintColor:[UIColor redColor]];
    [switchIsLog2 setOnTintColor:[UIColor greenColor]];
    [switchIsLog2 addTarget:self action:@selector(enabelLog:) forControlEvents:UIControlEventValueChanged];
    [vSetting addSubview:switchIsLog2];
    
    CGRect switchLogWSFrame = switchLogWS.frame;
    //switchLogWSFrame.origin.x += 27; //right edge for ios 7
    switchLogWS2 = [[MBSwitch alloc] initWithFrame:switchLogWSFrame];//get frame was made by IBOutLet
    [switchLogWS2 setTintColor:[UIColor redColor]];
    [switchLogWS2 setOffTintColor:[UIColor redColor]];
    [switchLogWS2 setOnTintColor:[UIColor greenColor]];
    [switchLogWS2 addTarget:self action:@selector(enableLogWS:) forControlEvents:UIControlEventValueChanged];
    [vSetting addSubview:switchLogWS2];
    
    //IBOutLet Switchs
    [switchIsLog setHidden:YES];
    [switchLogWS setHidden:YES];
    [switchValidateRoom setHidden:YES];
    [switchBackgroundMode setHidden:YES]; // CFG [20160927/CRF-00001432]
//    [switchDemoMode setHidden:YES];
    
//    [btnReset setHidden:((!switchDemoMode2.isOn && isConfigAccount) || !isConfigAccount)];
//    [txtWSURL setHidden: (switchDemoMode2.isOn && isConfigAccount) || (!isDemoMode && !isConfigAccount)];
    
    
    CGRect switchWifiFrame = switchWifi.frame;
    //switchLogWSFrame.origin.x += 27; //right edge for ios 7
    switchWifi2 = [[MBSwitch alloc] initWithFrame:switchWifiFrame];//get frame was made by IBOutLet
    [switchWifi2 setTintColor:[UIColor redColor]];
    [switchWifi2 setOffTintColor:[UIColor redColor]];
    [switchWifi2 setOnTintColor:[UIColor greenColor]];
    [switchWifi2 addTarget:self action:@selector(onSwitchWifiValueChanged:) forControlEvents:UIControlEventValueChanged];
    [vSetting addSubview:switchWifi2];
    
    
    CGRect switch3GFrame = switch3G.frame;
    //switchLogWSFrame.origin.x += 27; //right edge for ios 7
    switch3G2 = [[MBSwitch alloc] initWithFrame:switch3GFrame];//get frame was made by IBOutLet
    [switch3G2 setTintColor:[UIColor redColor]];
    [switch3G2 setOffTintColor:[UIColor redColor]];
    [switch3G2 setOnTintColor:[UIColor greenColor]];
    [switch3G2 addTarget:self action:@selector(onSwitch3GValueChanged:) forControlEvents:UIControlEventValueChanged];
    [vSetting addSubview:switch3G2];
    
    // CFG [20160927/CRF-00001432] - Set background mode switch style.
    CGRect switchBackgroundModeFrame = switchBackgroundMode.frame;
    //switchLogWSFrame.origin.x += 27; //right edge for ios 7
    switchBackgroundMode2 = [[MBSwitch alloc] initWithFrame:switchBackgroundModeFrame];
    [switchBackgroundMode2 setTintColor:[UIColor redColor]];
    [switchBackgroundMode2 setOffTintColor:[UIColor redColor]];
    [switchBackgroundMode2 setOnTintColor:[UIColor greenColor]];
    [switchBackgroundMode2 addTarget:self action:@selector(switchBackgroundModeOnChange:) forControlEvents:UIControlEventValueChanged];
    [vSetting addSubview:switchBackgroundMode2];
    // CFG [20160927/CRF-00001432] - End.
}

-(void)syncAllSwitchValue
{
    [switchValidateRoom2 setOn:switchValidateRoom.isOn];
    [switchLogWS2 setOn:switchLogWS.isOn];
    [switchIsLog2 setOn:switchIsLog.isOn];
//    [switchDemoMode2 setOn:switchDemoMode.isOn];
    [switchBackgroundMode2 setOn:switchBackgroundMode.isOn]; // CRF [20160927/CRF-00001432]
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    isClearedDatabase = NO;
    //Resize content views of current VC to fit with Navigation bar and tabbar
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    //[self showAllSwitchBelongToOSVersion];
    
    prevYOffset = 0;
    _keyboardIsVisible = NO;
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardDidShow:)name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardDidHide:)name:UIKeyboardDidHideNotification object:nil];
    
    // Do any additional setup after loading the view from its nib.
    int retryTimes = (int)[[NSUserDefaults standardUserDefaults] integerForKey:RETRY_TIMES];
    if(retryTimes <= 0) {
        retryTimes = 2;
        [[NSUserDefaults standardUserDefaults] setInteger:2 forKey:RETRY_TIMES];
    }
    [txtRetrialTimes setText:[NSString stringWithFormat:@"%d", retryTimes]];
    
    txtService.tag=1;
    txtSynPeriod.tag=2;
    
//    CGRect screenRect = [[UIScreen mainScreen] bounds];
//    CGRect switchFrame = [switchValidateRoom frame];
//    CGFloat xFloat = screenRect.size.width - switchFrame.size.width - 8;
//    if(!isConfigAccount){
//        [switchDemoMode setHidden:YES];
//        [btnReset setHidden:YES];
//        [lblRetrialTimes setHidden:YES];
//        [lblDemoMode setText:@"Validation Room Number"];
//        [lblRoomValidate setText:@"Log"];
//        [lblLog setText:@"WSLog"];
//        [lblWSLog setText:@"Log No"];
//        [lblLogNo setText:@"Default Timeout"];
//        [lblDefaultTimeOut setText:@"Retrial Times"];
//        [switchValidateRoom setFrame:CGRectMake(xFloat, 244, 51, 31)];
//        [switchIsLog setFrame:CGRectMake(xFloat, 283, 51, 31)];
//        [switchLogWS setFrame:CGRectMake(xFloat, 322, 51, 31)];
//        [txtLogNumber setFrame:CGRectMake(165, 361, 146, 31)];
//        [txtDefaultTimeOut setFrame:CGRectMake(165, 396, 146, 31)];
//        [txtRetrialTimes setFrame:CGRectMake(165, 431, 146, 31)];
//        [btnViewLog setFrame:CGRectMake(21, 474, 280, 32)];
//        
//        if(isDemoMode){
//            [txtWSURL setHidden:YES];
//            [btnSubmit setFrame:CGRectMake(11, 514, 300, 55)];
//        }
//        else{
//            [txtWSURL setHidden:NO];
//            [txtWSURL setFrame:CGRectMake(11, 514, 302, 31)];
//            [btnSubmit setFrame:CGRectMake(11, 554, 300, 55)];
//        }
//        
//    }
//    else{
//        [switchDemoMode setHidden:NO];
//        [lblRetrialTimes setHidden:NO];
//        [lblDemoMode setText:@"Demo Mode"];
//        [lblRoomValidate setText:@"Validate Room Number"];
//        [lblLog setText:@"Log"];
//        [lblWSLog setText:@"WSLog"];
//        [lblLogNo setText:@"Log No"];
//        [lblDefaultTimeOut setText:@"Default Timeout"];
//        [lblRetrialTimes setText:@"Retrial Times"];
//        
//        [switchDemoMode setFrame:CGRectMake(xFloat, 244, 51, 31)];
//        [switchValidateRoom setFrame:CGRectMake(xFloat, 283, 51, 31)];
//        [switchIsLog setFrame:CGRectMake(xFloat, 322, 51, 31)];
//        [switchLogWS setFrame:CGRectMake(xFloat, 361, 51, 31)];
//        [txtLogNumber setFrame:CGRectMake(165, 400, 146, 31)];
//        [txtDefaultTimeOut setFrame:CGRectMake(165, 435, 146, 31)];
//        [txtRetrialTimes setFrame:CGRectMake(165, 470, 146, 31)];
//        if(isDemoMode)
//        {
//            [txtWSURL setHidden:YES];
//            [btnReset setHidden:NO];
//            
//            [btnReset setFrame:CGRectMake(21, 513, 280, 32)];
//            [btnViewLog setFrame:CGRectMake(21, 553, 280, 32)];
//            [btnSubmit setFrame:CGRectMake(11, 593, 300, 55)];
//        }
//        else{
//            [txtWSURL setHidden:NO];
//            [btnReset setHidden:YES];
//            
//            [btnViewLog setFrame:CGRectMake(21, 513, 280, 32)];
//            [txtWSURL setFrame: CGRectMake(9, 553, 302, 31)];
//            [btnSubmit setFrame:CGRectMake(11, 593, 300, 55)];
//        }
//    }
    
    
//    if(isDemoMode){
//        [btnResetDatabase setHidden:YES];
//        [txtWSURL setHidden:YES];
//        [btnExportDatabase setFrame:CGRectMake(20, 510, 280, 32)];
//        [btnSubmit setFrame:CGRectMake(9, 550, 302, 55)];
//    }
//    else{
//        [btnResetDatabase setHidden:NO];
//        [txtWSURL setHidden:NO];
//        [btnResetDatabase setFrame:CGRectMake(20, 510, 280, 32)];
//        [btnExportDatabase setFrame:CGRectMake(20, 550, 280, 32)];
//        [txtWSURL setFrame:CGRectMake(9, 590, 302, 31)];
//        [btnSubmit setFrame:CGRectMake(9, 629, 302, 55)];
//    }
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        [self loadFlatResource];
    } else {
        
        [optionNotification setBackgroundImage:[UIImage imageNamed:optionButLanguageImg] forState:UIControlStateNormal];
        [optionNotification setBackgroundImage:[UIImage imageNamed:optionBackgroundGray] forState:UIControlStateHighlighted];
        
        [optionLanguage  setBackgroundImage:[UIImage imageNamed:optionButLanguageImg] forState:UIControlStateNormal];
        [optionLanguage setBackgroundImage:[UIImage imageNamed:optionBackgroundGray] forState:UIControlStateHighlighted];
        optionLanguage.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;//NSLineBreakByTruncatingTail;
        
        [optionSynch setBackgroundImage:[UIImage imageNamed:optionButLanguageImg]
                               forState:UIControlStateNormal];
        [optionSynch setBackgroundImage:[UIImage imageNamed:optionBackgroundGray] forState:UIControlStateHighlighted];
        
        [optionPhotoResolution setBackgroundImage:[UIImage imageNamed:optionButLanguageImg]forState:UIControlStateNormal];
        [optionPhotoResolution setBackgroundImage:[UIImage imageNamed:optionBackgroundGray] forState:UIControlStateHighlighted];
        
        [optionLanguage setTitleEdgeInsets:UIEdgeInsetsMake(0, 3, 0, 29)];
        
        [optionNotification setTitleEdgeInsets:UIEdgeInsetsMake(0, 3, 0, 20)];
        [optionNotification.titleLabel setAdjustsFontSizeToFitWidth:YES];
    }
    
    self.saveSettingModel = [[SettingModelV2 alloc] init];
    
    //set captions of view
    [self setCaptionsView];
//    if(!isDemoMode){
//        [txtWSURL setText:[[NSUserDefaults standardUserDefaults] objectForKey:KEY_WEBSERVICE_URL]];
//    }
    
    [txtWSURL setText:[[NSUserDefaults standardUserDefaults] objectForKey:KEY_WEBSERVICE_URL]];
    if (!isConfigAccount) {
        [self performSelector:@selector(loadTopbarView)];
    }
    else {
        
        UIView *titleViewBar = [[UIView alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
        UIButton *titleBarButton = [[UIButton alloc] initWithFrame:CGRectMake(-5, 8, FRAME_BUTTON_TOPBAR.size.width, 30)];
        titleBarButton.titleLabel.backgroundColor = [UIColor clearColor];
        titleBarButton.titleLabel.font = FONT_BUTTON_TOPBAR;
        titleBarButton.titleLabel.textAlignment = NSTextAlignmentLeft;
        titleBarButton.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
        [titleBarButton.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
        [titleBarButton.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
        [titleBarButton setTitle:[[LanguageManagerV2 sharedLanguageManager] getBtnSetting] forState:UIControlStateNormal];
        [titleViewBar addSubview:titleBarButton];
        self.navigationItem.titleView = titleViewBar;
        
        //Move title to center vertical
        CGRect titleViewBounds = self.navigationItem.titleView.bounds;
        int moveLeftTitle = 0;
        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
            moveLeftTitle = 10;
        }
        self.navigationItem.titleView.bounds = CGRectMake(titleViewBounds.origin.x + moveLeftTitle, Y_TOPBAR_TITLE, titleViewBounds.size.width, titleViewBounds.size.height);
    }
#if !DEFAULT_ENABLE_PTT
    
        CGRect frame = viewLastItem.frame;
        frame.origin.y -= 40;
        viewLastItem.frame = frame;
        frame = vSetting.frame;
        frame.size.height -= 40;
        vSetting.frame = frame;
        btnPTTConfig.hidden = YES;
#endif
    [scroll setContentSize:CGSizeMake(vSetting.frame.size.width, vSetting.frame.size.height)];
    [scrollView setContentSize:CGSizeMake(vSetting.frame.size.width, vSetting.frame.size.height)];
    [scrollView setBackgroundColor:[UIColor clearColor]];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self loadSettingData];
}

// @ display setting data to view in Screen.
-(void)loadSettingData
{    
    //enable flag to announce, is loading data
    [[HomeViewV2 shareHomeView] waitingLoadingData];
    
    SettingAdapterV2 *stAdapter=[[SettingAdapterV2 alloc] init];
    [stAdapter openDatabase];
    [stAdapter resetSqlCommand];
    if (saveSettingModel == nil) {
        saveSettingModel = [[SettingModelV2 alloc] init];
    }
    [stAdapter loadSettingModel:self.saveSettingModel];
    
    
    if (self.saveSettingModel.row_Id) 
    {

        switch (self.saveSettingModel.settingsNotificationType) 
        {
            case sPopup:
            {
                [optionNotification setTitle:sNotificationPopup forState:UIControlStateNormal];
            }
                break;
                
            case sVibrate:
            {
                [optionNotification setTitle:sNotificationVibrate forState:UIControlStateNormal];
            }
                break;
                
            case sRing:
            {
                [optionNotification setTitle:sNotificationRing forState:UIControlStateNormal];
            }
                break;
                
            default:
            {
                [optionNotification setTitle:sNotificationPopup forState:UIControlStateNormal];
            }
                break;
        }
        
        switch (self.saveSettingModel.settingsSynch) 
        {
            case sManual:
            {
                [optionSynch setTitle:sSynchManual forState:UIControlStateNormal];
            }
                break;
                
            case sAutomatic:
            {
                [optionSynch setTitle:sSynchAutomatic forState:UIControlStateNormal];
            }
                break;
                
            default:
            {
                [optionSynch setTitle:sSynchManual forState:UIControlStateNormal];

            }
                break;
        }

        switch (self.saveSettingModel.settingsPhoto) {
            case PHOTO_RESOLUTION_240_180:
            {
                [optionPhotoResolution setTitle:SETTING_240_180 forState:UIControlStateNormal];
            }
                break;
                
            case PHOTO_RESOLUTION_320_240:
            {
                [optionPhotoResolution setTitle:SETTING_320_240 forState:UIControlStateNormal];
            }
                break;
                
            case PHOTO_RESOLUTION_480_360:
            {
                [optionPhotoResolution setTitle:SETTING_480_360 forState:UIControlStateNormal];
            }
                break;
                
            default:
            {
                [optionPhotoResolution setTitle:SETTING_240_180 forState:UIControlStateNormal];
            }
                break;
        }
        
        switchIsLog.on = saveSettingModel.settingEnableLog;
        switchValidateRoom.on = saveSettingModel.settingEnableRoomValidate;
        if (switchIsLog.on) {
            [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithInt:1] forKey:ENABLE_LOG];
        }
        else {
            [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithInt:0] forKey:ENABLE_LOG];
        }
        
        int logWSvalue = (int)[[NSUserDefaults standardUserDefaults] integerForKey:ENABLE_LOGWS];
        if (logWSvalue == 1) {
            [switchLogWS setOn:YES];
        }
        else {
            [switchLogWS setOn:NO];
        }
        
        //switch Validation Room Number
//        [switchValidateRoom setOn:[[UserManagerV2 sharedUserManager].currentUserAccessRight isValidateRoom]];
        //switch Demo Mode
//        if(isConfigAccount){
//            [switchDemoMode setOn:isDemoMode];
//        }
//        if(!isDemoMode){
//            [txtWSURL setText:saveSettingModel.settingsWSURL];
//        }
        [txtWSURL setText:saveSettingModel.settingsWSURL];
        
        if(saveSettingModel.settingsWSURL == nil || [saveSettingModel.settingsWSURL isEqualToString:@""]){
            [txtWSURL setText:[[NSUserDefaults standardUserDefaults] objectForKey:KEY_WEBSERVICE_URL]];
        }
        [txtLogNumber setText:[NSString stringWithFormat:@"%i",(int)saveSettingModel.settingsLogAmount]];
        
        int messagePeriod = [[UserManagerV2 sharedUserManager].currentUserAccessRight getMessagePeriod];
        if(messagePeriod == 7){
            [messagePeriodButton setTitle:[[LanguageManagerV2 sharedLanguageManager] getMessagePeriod7Days] forState:UIControlStateNormal];
        } else { //30
            [messagePeriodButton setTitle:[[LanguageManagerV2 sharedLanguageManager] getMessagePeriod30Days] forState:UIControlStateNormal];
        }
        
        int defaultTimeout = (int)[[NSUserDefaults standardUserDefaults] integerForKey:DEFAULT_TIMEOUT];
        if (defaultTimeout == 0) {
            [txtDefaultTimeOut setText:@"45"];
        }
        else {
            [txtDefaultTimeOut setText:[NSString stringWithFormat:@"%d", defaultTimeout]];
        }
//        [txtDefaultTimeOut setText:[NSString stringWithFormat:@"%d", defaultTimeout]];
        
        LanguageModelV2 *lModel = [[LanguageModelV2 alloc] init];
        lModel.lang_id = self.saveSettingModel.settingsLang;
        
        //Hold current language
        if(!currentLanguage) {
            currentLanguage = self.saveSettingModel.settingsLang;
        }
        
        [[LanguageManagerV2 sharedLanguageManager] loadLanguageModel:lModel];
        
        [optionLanguage setTitle:lModel.lang_name forState:UIControlStateNormal];
        
        // CFG [20160927/CRF-00001432] - Save background mode settings to SQLite.
        switchBackgroundMode.on = saveSettingModel.settingsBackgroundMode;
        if (saveSettingModel.settingsHeartbeatServer != nil && ![saveSettingModel.settingsHeartbeatServer isEqualToString:@""]) {
            txtHeartbeatServer.text = saveSettingModel.settingsHeartbeatServer;
        }
        else {
            txtHeartbeatServer.text = @"";
        }
        if (saveSettingModel.settingsHeartbeatPort != nil && ![saveSettingModel.settingsHeartbeatPort isEqualToString:@""]) {
            txtHeartbeatPort.text = saveSettingModel.settingsHeartbeatPort;
        }
        else {
            txtHeartbeatPort.text = @"";
        }
        // CFG [20160927/CRF-00001432] - End.

    }
    else {
        switchIsLog.enabled = YES;
    }
    
    //@ close database.
    [stAdapter close]; 
    
    NSInteger enable3G = [[NSUserDefaults standardUserDefaults] integerForKey:ENABLE_3G];
    [self set3G:((enable3G == 1) ? YES : NO)];


    BOOL enableWifi =  [[NSUserDefaults standardUserDefaults] boolForKey:ENABLE_WIFI];
    [switchWifi setOn:enableWifi animated:YES];
    [switchWifi2 setOn:enableWifi animated:YES];

    //Sync all switch value to right value on view
    [self syncAllSwitchValue];
    
    //load webservice url
//    [txtWSURL setText:[[NSUserDefaults standardUserDefaults] objectForKey:KEY_WEBSERVICE_URL]];
    
    //end loading data
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
}
- (void)set3G:(BOOL)value{
    [switch3G setOn:value animated:YES];
    [switch3G2 setOn:value animated:YES];
//    [switchWifi setOn:!value animated:YES];
//    [switchWifi2 setOn:!value animated:YES];
}
-(void)saveSetting:(NSNotification*)nt
{
    int retryTimes = [txtRetrialTimes.text intValue];
    if(retryTimes <= 0) {
        retryTimes = 2;
    }
    [[NSUserDefaults standardUserDefaults] setInteger:retryTimes  forKey:RETRY_TIMES];
    
    if ([txtSynPeriod.text length] > 0) {
        NSInteger period = [txtSynPeriod.text integerValue];
        if (period < 5 || period > 60) {
            NSString *msg = [NSString stringWithFormat:@"%@ %@", [[LanguageManagerV2 sharedLanguageManager] getMsgPleaseChoose], [[LanguageManagerV2 sharedLanguageManager] getMsgSyncPeriodSetting]];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:msg message:nil delegate:nil cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getOK] otherButtonTitles: nil];
            [alert show];
            return;
        }
    }
    
//    if ([txtDefaultTimeOut.text length] > 1 && [txtDefaultTimeOut.text integerValue] >= 30)
    if ([txtDefaultTimeOut.text integerValue] >= 45){
        int defaultTimeOut = [txtDefaultTimeOut.text intValue];
        [[NSUserDefaults standardUserDefaults] setInteger:defaultTimeOut  forKey:DEFAULT_TIMEOUT];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[L_please_input_time_out currentKeyToLanguage] message:nil delegate:nil cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getOK] otherButtonTitles: nil];
        [alert show];
        return;
    }
    
    // @ show loading data .
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getMsgSaveMessage]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES]; 
    
    isClearedDatabase = NO;
    //save key webserice url
//    if (!isDemoMode && txtWSURL.text != nil) {
        if (txtWSURL.text != nil) {
        //HaoTran[20130509/Check Clear Database] - check to clean all database if webservice changed
        NSString *urlWebservice = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_WEBSERVICE_URL];
        if([txtWSURL.text caseInsensitiveCompare:urlWebservice] != NSOrderedSame) {
            
            //Remove : before we cleared database, we will show a confirm pop up first
            /*
            [((eHouseKeepingAppDelegate*)[[UIApplication sharedApplication] delegate]) resetLocalDatabase];
            [[NSUserDefaults standardUserDefaults] setObject:txtWSURL.text forKey:KEY_WEBSERVICE_URL];
            [[NSUserDefaults standardUserDefaults] synchronize];*/
            
            isClearedDatabase = YES;
            /*
            if([LogFileManager isLogConsole]) {
                NSLog(@"%@ %@", [[NSUserDefaults standardUserDefaults] objectForKey:KEY_WEBSERVICE_URL], txtWSURL.text);
            }
            self.saveSettingModel.settingsWSURL = txtWSURL.text;
             */
            //End Remove:
        }
        //HaoTran[20130509/Check Clear Database] - END
    }
    
    // saving data :
    SettingAdapterV2 *stAdapter = [[SettingAdapterV2 alloc] init];
    [stAdapter openDatabase];
    [stAdapter resetSqlCommand];
    
    // assign info for notification type
    if ([optionNotification.titleLabel.text isEqualToString:sNotificationPopup]) {
        //Popup
        saveSettingModel.settingsNotificationType = sPopup;
    } 
    if ([optionNotification.titleLabel.text isEqualToString:sNotificationVibrate]) {
        //Vibrate
        saveSettingModel.settingsNotificationType = sVibrate;
    } 
    if ([optionNotification.titleLabel.text isEqualToString:sNotificationRing]) {
        //Ring
        saveSettingModel.settingsNotificationType = sRing;
    } 
    
    // assign info for synch
    if ([optionSynch.titleLabel.text isEqualToString:sSynchAutomatic]) {
        //automactic
        saveSettingModel.settingsSynch = sAutomatic;
    } else {
        //manual
        saveSettingModel.settingsSynch = sManual;
    }
    
    // assign info for photo resolution
//    if ([optionPhotoResolution.titleLabel.text isEqualToString:sPhotoLow]) {
//        //Low
//        saveSettingModel.settingsPhoto = sLow;
//    } 
//    if ([optionPhotoResolution.titleLabel.text isEqualToString:sPhotoMedium]) {
//        //Medium
//        saveSettingModel.settingsPhoto = sMedium;
//    } 
//    if ([optionPhotoResolution.titleLabel.text isEqualToString:sPhotoHeight]) {
//        //Height
//        saveSettingModel.settingsPhoto = sHeight;
//    } 
    
    saveSettingModel.settingEnableLog = switchIsLog.on;
    saveSettingModel.settingEnableRoomValidate = switchValidateRoom.on;
    
    int switchStatus = switchLogWS.on ? 1:0;
    [[NSUserDefaults standardUserDefaults] setInteger:switchStatus forKey:ENABLE_LOGWS];
    
    int logAmount = 0;
    if (txtLogNumber.text != nil && ![txtLogNumber.text isEqualToString:@""]) {
        logAmount = [txtLogNumber.text intValue];
    }
    saveSettingModel.settingsLogAmount = logAmount;
    
    // CFG [20160927/CRF-00001432] - Save background mode settings to SQLite.
    saveSettingModel.settingsBackgroundMode = switchBackgroundMode.on;
    if (txtHeartbeatServer.text != nil && ![txtHeartbeatServer.text isEqualToString:@""]) {
        saveSettingModel.settingsHeartbeatServer = txtHeartbeatServer.text;
    }
    else {
        saveSettingModel.settingsHeartbeatServer = @"";
    }
    if (txtHeartbeatPort.text != nil && ![txtHeartbeatPort.text isEqualToString:@""]) {
        saveSettingModel.settingsHeartbeatPort = txtHeartbeatPort.text;
    }
    else {
        saveSettingModel.settingsHeartbeatPort = @"";
    }
    // CFG [20160927/CRF-00001432] - End.
    
    
    if(self.saveSettingModel.row_Id)
    {
        [stAdapter updateSettingModel:self.saveSettingModel];
    }
    else
    {
        [stAdapter insertSettingModel:self.saveSettingModel];
    }
    
    [stAdapter close];
    
    //hide saving data.
    [self performSelector:@selector(hiddenHUDAfterSaved:) withObject:HUD afterDelay:0.5];
    
    // set current language when user save language;
    if (self.saveSettingModel.settingsLang != nil) {
        
        
        [[LanguageManagerV2 sharedLanguageManager] setCurrentLanguage:self.saveSettingModel.settingsLang];
        
        //reload screen setting when user save setting by post notification change language tabbar
        //[[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationChangeLanguageTabbar] object:nil];
        [[HomeViewV2 shareHomeView].slidingMenuBar reloadAllTitleButtons];
        [[HomeViewV2 shareHomeView].slidingLegend reloadAllLegendDescriptions];
        
        //reload captions of view
        [self setCaptionsView];
        
        //Hao Tran - Only reload setting data without change URL of WS
        if(!isClearedDatabase) {
            //reload loading data
            [self loadSettingData];
        }
        
        //Hao Tran [20141120/To update view for language] - Pop to root view for updating views for languages
        if (currentLanguage.length > 0
            && ![currentLanguage isEqualToString:self.saveSettingModel.settingsLang]) {
            
            [[HomeViewV2 shareHomeView] setCaptionsView];
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
    }
}

#pragma didden after save.

-(void) hiddenHUDAfterSaved:(MBProgressHUD *)HUD{
    [HUD hide:YES];
    [HUD removeFromSuperview];
//    [HUD release];
}


- (void)viewDidUnload
{

    [self setLblSyncPeriod:nil];
    [self setTxtSyncPeriod:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
- (IBAction)butSetServicePressed:(id)sender {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[[LanguageManagerV2 sharedLanguageManager] getMsgLogout] message:@"" delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getNo] otherButtonTitles:[[LanguageManagerV2 sharedLanguageManager] getYes], nil];
    [alert show];
//    [alert release];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
//    if (textField == txtSyncPeriod) {
//        if ([textField.text integerValue] >= 5 && [textField.text integerValue] <= 60) {
//            
//        } else {
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_MSG_SYNC_PERIOD_SETTING] message:nil delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getOK] otherButtonTitles:nil];
//            [alert show];
//            
//            return TRUE;
//        }
//    }
    
    [textField resignFirstResponder];
    return TRUE;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
//    if (textField == txtSyncPeriod) {
//        
//        if (range.length == 1 && [string isEqualToString:@""]) {
//            return YES;
//        }
//        
//        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
//        for (int i = 0; i < [string length]; i++) {
//            unichar c = [string characterAtIndex:i];
//            if ([myCharSet characterIsMember:c]) {
//                return YES;
//            }
//        }
//        
//        return NO;
//    }
    
    if ([string isEqualToString:@"\n"]) {
        [textField resignFirstResponder];
        return NO;
    }
    return YES;
}

#pragma mark - UIAlertDelegate Methods
-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    
//    if(alertView.tag == tagConfirmDemoMode){
//        switch (buttonIndex) {
//            case 1:{ //NO - Revert enable DemoMode
//                if(isDemoMode){
//                    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:ENABLE_DEMO_MODE];
//                    [self resetManagers];
//                    [((eHouseKeepingAppDelegate*)[[UIApplication sharedApplication] delegate]) createEditableCopyOfDatabaseIfNeeded];
//                    [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationShowWifiView] object:nil];
//                }
//                else{
//                    [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:ENABLE_DEMO_MODE];
//                    [self resetManagers];
//                    [((eHouseKeepingAppDelegate*)[[UIApplication sharedApplication] delegate]) createEditableCopyOfDatabaseIfNeeded];
//                    [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationHideWifiView] object:nil];
//                }
//            }
//                break;
//            case 0: //YES
//                break;
//        }
//        
//        [self saveData];
//        if(!isClearedDatabase){
//            [[[[iToast makeText:[[LanguageManagerV2 sharedLanguageManager] getSavesuccessfully]] setGravity:iToastGravityBottom] setDuration:iToastDurationNormal] show];
//            [[HomeViewV2 shareHomeView] logOutUser];
//        }
//        else{
//            UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:imgYesBtn], [UIImage imageNamed:imgNoBtn], nil] title:@"" message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_message_clear_data_base] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_CANCEL], nil];
//            alert.tag = tagClearData;
//            [alert show];
//            
//            return;
//        }
//    }
//    else {
//        //others cases
//        switch (buttonIndex) {
//            case 0:
//                // NO
//                break;
//            case 1:
//                // YES
//                [[NSUserDefaults standardUserDefaults] setObject:txtService.text forKey:KEY_WEBSERVICE_URL];
//                
//                break;
//            default:
//                break;
//        }
//    }
    if(alertView.tag == tagClearData) {
        if(buttonIndex == 0){
            
            [((eHouseKeepingAppDelegate*)[[UIApplication sharedApplication] delegate]) resetLocalDatabase];
            
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            //Save url setting
            //if(!isDemoMode){
                [[NSUserDefaults standardUserDefaults] setObject:txtWSURL.text forKey:KEY_WEBSERVICE_URL];
                self.saveSettingModel.settingsWSURL = txtWSURL.text;
            //}
            SettingAdapterV2 *stAdapter = [[SettingAdapterV2 alloc] init];
            [stAdapter openDatabase];
            [stAdapter resetSqlCommand];
            
            if(self.saveSettingModel.row_Id) {
                [stAdapter updateSettingModel:self.saveSettingModel];
            } else {
                [stAdapter insertSettingModel:self.saveSettingModel];
            }
            [stAdapter close];
            
            [[[[iToast makeText:[[LanguageManagerV2 sharedLanguageManager] getSavesuccessfully]] setGravity:iToastGravityBottom] setDuration:iToastDurationNormal] show];
            [[HomeViewV2 shareHomeView] logOutUser];
        } else if(buttonIndex == 1) {
            [[HomeViewV2 shareHomeView] logOutUser];
        }
    }
    else if (alertView.tag == tagConfirmResetDatabase){
        if(buttonIndex == 0){
            
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:IS_LOAD_LANGUAGE];
            [((eHouseKeepingAppDelegate*)[[UIApplication sharedApplication] delegate]) resetLocalDatabase];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            //Save url setting
            //if(!isDemoMode){
            [[NSUserDefaults standardUserDefaults] setObject:txtWSURL.text forKey:KEY_WEBSERVICE_URL];
            self.saveSettingModel.settingsWSURL = txtWSURL.text;
            //}
            SettingAdapterV2 *stAdapter = [[SettingAdapterV2 alloc] init];
            [stAdapter openDatabase];
            [stAdapter resetSqlCommand];
            
            if(self.saveSettingModel.row_Id) {
                [stAdapter updateSettingModel:self.saveSettingModel];
            } else {
                [stAdapter insertSettingModel:self.saveSettingModel];
            }
            [stAdapter close];
            
            // FM [20160329] - Delete and Update language file
            [self updateFileLangXMLWithURL:txtWSURL.text];
            
            [[[[iToast makeText:[[LanguageManagerV2 sharedLanguageManager] getMessageResetDb]] setGravity:iToastGravityBottom] setDuration:iToastDurationNormal] show];
            [[HomeViewV2 shareHomeView] logOutUser];
        }
        else if(buttonIndex == 1){
            return;
        }
    }
    else {
        //others cases
        switch (buttonIndex) {
            case 0:
                // NO
                break;
            case 1:
                // YES
                [[NSUserDefaults standardUserDefaults] setObject:txtService.text forKey:KEY_WEBSERVICE_URL];
                
                break;
            default:
                break;
        }
    }
}
- (IBAction)ChooseTypeNotification:(id)sender
{
    
    if(self.chooseNotificationView == nil){
        self.chooseNotificationView=[[ChooseNotificationTypeViewV2 alloc] initWithNibName:@"ChooseNotificationTypeViewV2" bundle:nil];
        if([DeviceManager getDeviceScreenKind] == DeviceScreenKindRetina4_0){
            CGRect pickerFrame = chooseNotificationView.view.frame;
            pickerFrame.size.height += 88;
            [chooseNotificationView.view setFrame:pickerFrame];
        }
    }
    CGRect frame = chooseNotificationView.view.frame;
    frame.origin.y = 0;
    chooseNotificationView.view.frame = frame;
    
    self.chooseNotificationView.delegate = self;
    [self.chooseNotificationView.view setBackgroundColor:[UIColor clearColor]];
    
    //[self.tabBarController.view addSubview:self.chooseNotificationView.view];
    [self.tabBarController addSubview:self.chooseNotificationView.view];
    
    //reload captions of view choosenotificationview
    [self.chooseNotificationView setCaptionsView];
    
    [chooseNotificationView setSelectedRowWithData:optionNotification.titleLabel.text];
}

-(void)updateFileLangXMLWithURL:(NSString *)url {
    NSData *fileData = nil;
    
    NSArray *arr = [url componentsSeparatedByString:@"/"];
    NSString *fileName = [arr objectAtIndex:[arr count]-1];
    
    NSString *file = [NSString stringWithFormat:@"%@", url];
    NSURL *fileURL = [NSURL URLWithString:[file stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLRequest *req = [NSURLRequest requestWithURL:fileURL];
    NSURLResponse *res = nil;
    fileData = [NSURLConnection sendSynchronousRequest:req returningResponse:&res error:nil];
    
    //NSArray *dirArray = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,    NSUserDomainMask, YES);
    NSArray *dirArray = NSSearchPathForDirectoriesInDomains(DataDirectory, NSUserDomainMask, YES);
    
    NSString *path = [NSString stringWithFormat:@"%@/%@", [dirArray objectAtIndex:0], fileName];
    if(fileData != nil) {
        [[NSFileManager defaultManager] removeItemAtPath:path error:nil];
    }
    
    //if ([fileData writeToFile:path options:NSAtomicWrite error:nil] == NO) {
    if([fileData writeToFile:path atomically:YES]){
        NSLog(@"Fail to download language");
    } else {
        NSLog(@"success to download language");
    }
}


- (IBAction)ChooseLanguage:(id)sender {
    NSMutableArray *languageModels = [[LanguageManagerV2 sharedLanguageManager] loadAllLanguageModel];
    BOOL isAlreadyLoadLanguage = [[NSUserDefaults standardUserDefaults] boolForKey:IS_LOAD_LANGUAGE];
    
    if (languageModels.count > 0 && isAlreadyLoadLanguage) {
        
    } else {
        if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp] == TRUE) {
            
            MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
            [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getRaLoadingData]];
            [self.tabBarController.view addSubview:HUD];
            [HUD show:YES];
            
            NSArray *arr = [LanguageManagerV2 updateLanguageList];
            for (NSString *url in arr) {
                [self updateFileLangXMLWithURL:url];
            }
            
            [HUD hide:YES];
            [HUD removeFromSuperview];
        }
    }
    
    if(self.languagesView == nil){
        self.languagesView = [[LanguageViewV2 alloc] initWithNibName:NSStringFromClass([LanguageViewV2 class]) bundle:nil];
        if([DeviceManager getDeviceScreenKind] == DeviceScreenKindRetina4_0){
            CGRect languagesFrame = languagesView.view.frame;
            languagesFrame.size.height += 88;
            [languagesView.view setFrame:languagesFrame];
        }
    }
    
    CGRect frame = languagesView.view.frame;
    frame.origin.y = 0;
    languagesView.view.frame = frame;
    
    self.languagesView.delegate = self;
    
    //[self.tabBarController.view addSubview:self.languagesView.view];
    [self.tabBarController addSubview:self.languagesView.view];
    
    //reload captions of view languagesView
    [self.languagesView viewWillAppear:NO];
    
    LanguageModelV2 *lmodel = [[LanguageModelV2 alloc] init];
    lmodel.lang_name = optionLanguage.titleLabel.text;
    [[LanguageManagerV2 sharedLanguageManager] loadLanguageModelByLanguageName:lmodel];
    
    [languagesView setSelectedRowWithData:lmodel.lang_id];
    
    [languagesView setIsSetting:YES];
}

- (IBAction)ChooseSynch:(id)sender{
   
    if(self.synchViewV2 == nil)
    {
        synchViewV2 = [[SynchViewV2 alloc]initWithNibName:@"SynchViewV2" bundle:nil];
        if([DeviceManager getDeviceScreenKind] == DeviceScreenKindRetina4_0){
            CGRect pickerFrame = synchViewV2.view.frame;
            pickerFrame.size.height += 88;
            [synchViewV2.view setFrame:pickerFrame];
        }
    }
    CGRect frame = synchViewV2.view.frame;
    frame.origin.y = 0;
    synchViewV2.view.frame = frame;
    self.synchViewV2.delegate = self;
    //[self.tabBarController.view addSubview:self.synchViewV2.view];
    [self.tabBarController addSubview:self.synchViewV2.view];
    [synchViewV2 setCaptionsView];
    
     [synchViewV2 setSelectedRowWithData:optionSynch.titleLabel.text];
}

- (IBAction)ChoosePhotoResolution:(id)sender{
    
    if(self.photoResolutionViewV2 == nil){
        photoResolutionViewV2 = [[PhotoResolutionViewV2 alloc] initWithNibName:@"PhotoResolutionViewV2" bundle:nil];
        if([DeviceManager getDeviceScreenKind] == DeviceScreenKindRetina4_0){
            CGRect pickerFrame = photoResolutionViewV2.view.frame;
            pickerFrame.size.height += 88;
            [photoResolutionViewV2.view setFrame:pickerFrame];
        }
    }
    CGRect frame = photoResolutionViewV2.view.frame;
    frame.origin.y = 0;
    photoResolutionViewV2.view.frame = frame;
    self.photoResolutionViewV2.delegate=self;
    //[self.tabBarController.view addSubview:self.photoResolutionViewV2.view];
    [self.tabBarController addSubview:self.photoResolutionViewV2.view];
    
    [photoResolutionViewV2 setCaptionsView];
    
    [photoResolutionViewV2 setSelectedRowWithData:optionPhotoResolution.titleLabel.text];
}
-(void)ChooseNotificationTypeViewV2Controller:(ChooseNotificationTypeViewV2 *)controller didFinishSetupWithInfo:(NSString *)info
{
    switch ([info intValue]) {
        case 0 :
        {
            [optionNotification setTitle:sNotificationPopup forState:UIControlStateNormal];
            self.saveSettingModel.settingsNotificationType=sPopup;
            
        }
            break;
        case 1 :
        {
            [optionNotification setTitle:sNotificationVibrate forState:UIControlStateNormal];
            self.saveSettingModel.settingsNotificationType=sVibrate;
        }
            break;
            
        case 2 :
        {
            [optionNotification setTitle:sNotificationRing forState:UIControlStateNormal];
            self.saveSettingModel.settingsNotificationType=sRing;
        }
            break;
            
        default:
        {
            [optionNotification setTitle:sNotificationPopup forState:UIControlStateNormal];
            self.saveSettingModel.settingsNotificationType=sPopup;
        }
            break;
    }
    
    
}

-(void)languageViewController:(LanguageViewV2 *)controller didFinishSetupLanguageWithInfo:(LanguageModelV2 *)languageInfo
{
    
    [optionLanguage setTitle:languageInfo.lang_name forState:UIControlStateNormal];
    self.saveSettingModel.settingsLang = languageInfo.lang_id;
}

-(void)didSelectData:(PickerData*)data index:(int)indexSelected typeOperation:(int)typeOperation
{
    if(typeOperation == OperationPickLogData){
        eHouseKeepingAppDelegate *appDelegate = (eHouseKeepingAppDelegate*)[UIApplication sharedApplication].delegate;
        [appDelegate sendEmail:indexSelected];
    }else if(typeOperation == OperationPickMessageData){
        if(data.valueData == 7){
            [messagePeriodButton setTitle:[[LanguageManagerV2 sharedLanguageManager] getMessagePeriod7Days]  forState:UIControlStateNormal];
        } else { //30 days
            [messagePeriodButton setTitle:[[LanguageManagerV2 sharedLanguageManager] getMessagePeriod30Days]  forState:UIControlStateNormal];
        }
        
        [[UserManagerV2 sharedUserManager].currentUserAccessRight setMessagePeriod:data.valueData];
    }
}

#pragma mark - set captions of view
-(void) setCaptionsView {
    
    //set language setting
    sNotificationPopup = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_notification_toast];
    sNotificationVibrate = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_notification_vibrate];
    sNotificationRing = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_notification_ring];
    sSynchManual = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_synch_manual];
    sSynchAutomatic = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_synch_automatically];
//    sPhotoLow = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_resolution_72DPI];
//    sPhotoMedium = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_resolution_82DPI];
//    sPhotoHeight = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_resolution_92DPI];
    
    sPhotoLow = SETTING_240_180;
    sPhotoMedium = SETTING_320_240;
    sPhotoHeight = SETTING_480_360;

    [labelLanguage setText:[[LanguageManagerV2 sharedLanguageManager] getLanguage]];
    
    //UIButton *button = (UIButton *)self.navigationItem.titleView;
    //[button setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BTN_SETTING] forState:UIControlStateNormal];
    
    [labelNotification setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_notifycation_type_setting]];
    [labelPhotoResolution setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_photo_resolution_setting]];
    [labelSyncPeriod setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_synch_setting]];
    [lblWSURL setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_web_service_url]];
    
    [lblSyncPeriod setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_sync_period]];
    
    [messagePeriodLabel setText:[L_message_period currentKeyToLanguage]];
    [lblRoomValidate setText:[L_room_no_valid currentKeyToLanguage]];

    
    [lblLog setText:[L_log currentKeyToLanguage]];
    [lblDefaultTimeOut setText:[L_log currentKeyToLanguage]];
    [lblRetrialTimes setText:[L_retrial_times currentKeyToLanguage]];
    
    TopbarViewV2 *topbar = [self getTopBarView];
    [topbar refresh:[[SuperRoomModelV2 alloc] init]];
}

#pragma mark - PhotoResolution View Delegate Methods
-(void)photoResolutionViewV2Controller:(PhotoResolutionViewV2 *)controller didSelectResolutionAt:(NSInteger)index {
    switch (index) {
        case PHOTO_RESOLUTION_240_180:
        {
            [optionPhotoResolution setTitle:SETTING_240_180 forState:UIControlStateNormal];
        }
            break;
            
        case PHOTO_RESOLUTION_320_240:
        {
            [optionPhotoResolution setTitle:SETTING_320_240 forState:UIControlStateNormal];
        }
            break;
            
        case PHOTO_RESOLUTION_480_360:
        {
            [optionPhotoResolution setTitle:SETTING_480_360 forState:UIControlStateNormal];
        }
            break;
            
        default:
        {
            [optionPhotoResolution setTitle:SETTING_240_180 forState:UIControlStateNormal];
        }
            break;
    }
   
    self.saveSettingModel.settingsPhoto = index;
}
#pragma mark - Synch View Delegate Methods
-(void)SynchViewV2Controller:(SynchViewV2 *)controller didFinishSetupWithInfo:(NSString *)info{
    [optionSynch setTitle:[NSString stringWithFormat:@"%@",info] forState:UIControlStateNormal];
}
-(IBAction)btnRemarkView:(id)sender{
    RemarkViewV2 *remark = [[RemarkViewV2 alloc] initWithNibName:NSStringFromClass([RemarkViewV2 class]) bundle:nil];
    [self.navigationController pushViewController:remark animated:YES];
//    [remark release];
}

#pragma mark - Handle Topbar Methods

//handle back button pressed
-(void)navigateBack
{
    //Log in with config account
    if (isConfigAccount) {
        //This delegate to show LoginView
        [delegate btnSubmitClicked];
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        //Remove because we don't use tabbar index
        //int homeTabbarIndex = 0;
        //[[HomeViewV2 shareHomeView] setTabBarSelectedIndex:homeTabbarIndex willAnimate:YES];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void)loadTopbarView {
    TopbarViewV2 *topbar = [[TopbarViewV2 alloc] initViewWithSuperModel:[[CommonVariable sharedCommonVariable] superRoomModel]];
    [topbar setTag:tagTopbarView];
    [self.view addSubview:topbar];
    [topbar setHidden:YES];
    
    [topbar setAlpha:0.0];
    [self addButtonHandleShowHideTopbar];
}

-(void)showTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationWillStartSelector:@selector(setHiddenTopbarView)];
    
    f = topbar.frame;
    f.origin.y = 0;
    [topbar setFrame:f];
    [topbar setAlpha:1.0];
    
    [self adjustShowForViews];
    
    [UIView commitAnimations];
}

-(void)hideTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDidStopSelector:@selector(setHiddenTopbarView)];
    [UIView setAnimationDelegate:self];
    
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    [topbar setAlpha:0.0];
    
    [self adjustRemoveForViews];
    
    [UIView commitAnimations];
}

-(void)setHiddenTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    [topbar setHidden:!topbar.hidden];
}

-(TopbarViewV2 *)getTopBarView {
    return (TopbarViewV2 *)[self.view viewWithTag:tagTopbarView];
}

-(void)addButtonHandleShowHideTopbar {
    
    UIView *titleViewBar = [[UIView alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
    UIButton *titleBarButton = [[UIButton alloc] initWithFrame:CGRectMake(-5, 8, FRAME_BUTTON_TOPBAR.size.width, 30)];
    
    titleBarButton.titleLabel.backgroundColor = [UIColor clearColor];
    titleBarButton.titleLabel.font = FONT_BUTTON_TOPBAR;
    titleBarButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    titleBarButton.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
    [titleBarButton.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
    [titleBarButton.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
    [titleBarButton addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    [titleBarButton setTitle:[[LanguageManagerV2 sharedLanguageManager] getBtnSetting] forState:UIControlStateNormal];
    [titleViewBar addSubview:titleBarButton];
    self.navigationItem.titleView = titleViewBar;
    
    //Move title to center vertical
    CGRect titleViewBounds = self.navigationItem.titleView.bounds;
    int moveLeftTitle = 0;
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        moveLeftTitle = 10;
    }
    self.navigationItem.titleView.bounds = CGRectMake(titleViewBounds.origin.x + moveLeftTitle, Y_TOPBAR_TITLE, titleViewBounds.size.width, titleViewBounds.size.height);
}

-(void)topbarTapped:(UIButton *)sender {
    TopbarViewV2 *topbar = [self getTopBarView];
    [sender setEnabled:NO];
    if (topbar.isHidden) {
        [self showTopbarView];
    } else {
        [self hideTopbarView];
    }
    [self performSelector:@selector(topbarButtonEnable:) withObject:sender afterDelay:0.5];
}

-(void) topbarButtonEnable:(UIButton *) topbarBtn {
    [topbarBtn setEnabled:YES];
}

-(void)adjustShowForViews {
    //adjust all views when show 
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;

    CGRect ftbv = scrollView.frame;
    [scrollView setFrame:CGRectMake(0, f.size.height, 320, ftbv.size.height - f.size.height)];
//    CGRect ftbv = vSetting.frame;
//    [vSetting setFrame:CGRectMake(0, f.size.height, 320, ftbv.size.height - f.size.height)];
}

-(void)adjustRemoveForViews { 
    //adjust all views when hide
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    CGRect ftbv = scrollView.frame;
    [scrollView setFrame:CGRectMake(0, 0, 320, ftbv.size.height + f.size.height)];
//    CGRect ftbv = vSetting.frame;
//    [vSetting setFrame:CGRectMake(0, 0, 320, ftbv.size.height + f.size.height)];
}

/** dthanhson start editting */

- (void) textFieldDidBeginEditing:(UITextField *)textField{
    
    selectedTextField = textField;   
}

-(void) keyboardDidShow:(NSNotification*)notif
{
    [scrollView setScrollEnabled:NO];
    if(_keyboardIsVisible)
    {
        return;
    }
        
    CGPoint offset = scrollView.contentOffset;
    
    CGRect keyboardFrame = [[[notif userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue];
    
    if(selectedTextField == txtLogNumber)
    {
        prevYOffset = offset.y;
        CGFloat move = offset.y + keyboardFrame.size.height;
        if (move > 30 + keyboardFrame.size.height) {
            move = 30 + keyboardFrame.size.height;
        }
        scrollView.contentOffset = CGPointMake(offset.x, move);
    }
    else if(selectedTextField == txtDefaultTimeOut) {
        prevYOffset = offset.y;
        CGFloat move = offset.y + keyboardFrame.size.height;
        if (move > 45 + keyboardFrame.size.height) {
            move = 45 + keyboardFrame.size.height;
        }
        scrollView.contentOffset = CGPointMake(offset.x, move);
    }
    else if(selectedTextField == txtWSURL) {
        prevYOffset = offset.y;
        CGFloat move = offset.y + keyboardFrame.size.height;
        int deviceScreenKind = [DeviceManager getDeviceScreenKind];
        if(deviceScreenKind == DeviceScreenKindRetina3_5 || deviceScreenKind == DeviceScreenKindStandard3_5) {
            if (move > 202 + keyboardFrame.size.height) {
                move = 202 + keyboardFrame.size.height;
            }
        } else { //DeviceScreenKindRetina4_0
            if (move > 115 + keyboardFrame.size.height) {
                move = 115 + keyboardFrame.size.height;
            }
        }
        scrollView.contentOffset = CGPointMake(offset.x, move);
    } else if (selectedTextField == txtRetrialTimes) {
        prevYOffset = offset.y;
        CGFloat move = offset.y + keyboardFrame.size.height;
        int deviceScreenKind = [DeviceManager getDeviceScreenKind];
        if(deviceScreenKind == DeviceScreenKindRetina3_5 || deviceScreenKind == DeviceScreenKindStandard3_5) {
            if (move > 135 + keyboardFrame.size.height) {
                move = 135 + keyboardFrame.size.height;
            }
        } else { //DeviceScreenKindRetina4_0
            if (move > 95 + keyboardFrame.size.height) {
                move = 95 + keyboardFrame.size.height;
            }
        }
        scrollView.contentOffset = CGPointMake(offset.x, move);
    }
    
    
    _keyboardIsVisible = YES;
}

-(void) keyboardDidHide:(NSNotification*)notif
{
    [scrollView setScrollEnabled:YES];
    if (!_keyboardIsVisible) {
        return;
    }
    
    CGPoint offset = scrollView.contentOffset;
    
    scrollView.contentOffset = CGPointMake(offset.x, prevYOffset);
    _keyboardIsVisible = NO;
}

-(IBAction)btnViewLogClick:(id)sender
{
    if([LogFileManager isLogConsole])
    {
        NSLog(@"ViewLog Button Clicked");
    }
    if (dayPicker == nil) {
        dayPicker = [[DayPickerView alloc] initWithNibName:@"DayPickerView" bundle:nil];
        if([DeviceManager getDeviceScreenKind] == DeviceScreenKindRetina4_0){
            CGRect pickerFrame = dayPicker.view.frame;
            pickerFrame.size.height += 88;
            [dayPicker.view setFrame:pickerFrame];
        }
    }
    
    [txtLogNumber resignFirstResponder];
    
    CGRect frame = dayPicker.view.frame;
    frame.origin.y = 0;
    dayPicker.view.frame = frame;
    self.dayPicker.delegate=self;
    //[self.tabBarController.view addSubview:self.dayPicker.view];
    [self.tabBarController addSubview:self.dayPicker.view];
}

-(IBAction)btnResetDatabaseClick:(id)sender{
    UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:imgYesBtn], [UIImage imageNamed:imgNoBtn], nil] title:@"" message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_message_reset_data_base] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_CANCEL], nil];
    alert.tag = tagConfirmResetDatabase;
    [alert show];
    
    return;
}

- (IBAction)btnExportDatabaseClick:(id)sender{
    if([MFMailComposeViewController canSendMail]){
        _mailViewController = [[MFMailComposeViewController alloc] init];
        _mailViewController.mailComposeDelegate = self;
        [_mailViewController setSubject:@"Send Database"];
        
        NSString *databaseName = [[NSString alloc] initWithFormat:@"%@", DATABASE_NAME];
        NSArray *paths = NSSearchPathForDirectoriesInDomains(DataDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *defaultDBPath = [documentsDirectory stringByAppendingPathComponent:databaseName];
        NSData *myData = [NSData dataWithContentsOfFile:defaultDBPath];
        
        [_mailViewController addAttachmentData:myData mimeType:@"application/x-sqlite3" fileName:defaultDBPath];
        [self presentViewController:_mailViewController animated:YES completion:nil];
    }
    else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[L_no_email_account currentKeyToLanguage] message:nil delegate:nil cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getOK] otherButtonTitles: nil];
        [alert show];
    }
}

#pragma mark - Mail Compose delegate
-(void)mailComposeController:(MFMailComposeViewController *)controller
         didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    if(![[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[[LanguageManagerV2 sharedLanguageManager] getNoWifi] message:nil delegate:nil cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getOK] otherButtonTitles: nil];
        [alert show];
    }
    else{
        if (result == MFMailComposeResultSent) {
            [[[[iToast makeText:[[LanguageManagerV2 sharedLanguageManager] getSentsuccessfully]] setGravity:iToastGravityBottom] setDuration:iToastDurationNormal] show];
            NSLog(@"Result : %d",(int)result);
        }
        if (error || result == MFMailComposeResultFailed) {
            [[[[iToast makeText:[[LanguageManagerV2 sharedLanguageManager] getErrorSent]] setGravity:iToastGravityBottom] setDuration:iToastDurationNormal] show];
            NSLog(@"Error : %@",error);
        }
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

//-(IBAction)btnResetClick:(id)sender{
//    [((eHouseKeepingAppDelegate*)[[UIApplication sharedApplication] delegate]) resetDemoDatabase];
//    [[[[iToast makeText:[[LanguageManagerV2 sharedLanguageManager] getMessageResetDb]] setGravity:iToastGravityBottom] setDuration:iToastDurationNormal] show];
//}

-(IBAction)btnMessagePeriodClick:(id)sender
{
    if(self.pickerMessagePeriod == nil)
    {
        pickerMessagePeriod = [[PickerView alloc]initWithNibName:@"PickerView" bundle:nil];
        pickerMessagePeriod.typeOfOperation = OperationPickMessageData;
        NSMutableArray *listMessagePeriod = [NSMutableArray array];
        
        PickerData *pickerData = [[PickerData alloc] init];
        pickerData.valueData = 7; //Recent 7 days
        pickerData.displayData = [[LanguageManagerV2 sharedLanguageManager] getMessagePeriod7Days];
        [listMessagePeriod addObject:pickerData];
        
        pickerData = [[PickerData alloc] init];
        //Recent 30 days is MAX message period.
        //Messages older MAX_MESSAGE_PERIOD will be deleted automatically on View Message Inbox View
        pickerData.valueData = MAX_MESSAGE_PERIOD; 
        pickerData.displayData = [[LanguageManagerV2 sharedLanguageManager] getMessagePeriod30Days];
        [listMessagePeriod addObject:pickerData];
        
        pickerMessagePeriod.listPickerData = listMessagePeriod;
        
        if([DeviceManager getDeviceScreenKind] == DeviceScreenKindRetina4_0){
            CGRect pickerFrame = pickerMessagePeriod.view.frame;
            pickerFrame.size.height += 88;
            [pickerMessagePeriod.view setFrame:pickerFrame];
        }
    }
    
    int messagePeriod = [[UserManagerV2 sharedUserManager].currentUserAccessRight getMessagePeriod];
    if(messagePeriod == 7){
        [pickerMessagePeriod setSelectedIndex:0];
    } else { // 30
        [pickerMessagePeriod setSelectedIndex:1];
    }
    
    CGRect frame = pickerMessagePeriod.view.frame;
    frame.origin.y = 0;
    pickerMessagePeriod.view.frame = frame;
    pickerMessagePeriod.delegate = self;
    //[self.tabBarController.view addSubview:pickerMessagePeriod.view];
    [self.tabBarController addSubview:pickerMessagePeriod.view];
}

-(IBAction)btnReloadSystemDataClick:(id)sender
{
    [self loadSettingData];
}

-(void)dayPickerViewController:(DayPickerView *)controller didSelectDayLogAt:(NSInteger)index
{
    if (logView == nil) {
        logView = [[LogViewController alloc] initWithNibName:@"LogViewController" bundle:nil];
    }
    
    LogFileManager *manager = [[LogFileManager alloc] init];
    [logView setLogData:[manager getLogContent:(int)index inLimit:[txtLogNumber.text intValue]]];
    
    [logView setLabelStr:[dayPicker getDataOfSelectedRow]];
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK] style:UIBarButtonItemStyleBordered target:nil action:nil];
    self.navigationItem.backBarButtonItem = backButtonItem;
    
    [self.navigationController pushViewController:logView animated:NO];
}

-(IBAction)enabelLog:(id)sender
{
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        [switchIsLog setOn:switchIsLog2.isOn];
    }
    
    if (switchIsLog.on) {
        [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithInt:1] forKey:ENABLE_LOG];
    }
    else {
        [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithInt:0] forKey:ENABLE_LOG];
    }
}
- (NSInteger)getLastDayOfMonth{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    
    NSDateComponents *componentsCurrent = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
    
    // Set your year and month here
    [components setYear:componentsCurrent.year];
    [components setMonth:componentsCurrent.month];
    
    NSDate *date = [calendar dateFromComponents:components];
    NSRange range = [calendar rangeOfUnit:NSCalendarUnitDay inUnit:NSCalendarUnitMonth forDate:date];
    
//    NSLog(@"%d", (int)range.length);
    return range.length;
}
- (IBAction)btnSendEmailClick:(id)sender {
    if(self.pickerLog == nil)
    {
        pickerLog = [[PickerView alloc]initWithNibName:@"PickerView" bundle:nil];
        pickerLog.typeOfOperation = OperationPickLogData;
        NSMutableArray *listMessagePeriod = [NSMutableArray array];
        
        NSInteger lastDate = [self getLastDayOfMonth];
        for (int i=0; i<=lastDate; i++) {
            PickerData *pickerData = [[PickerData alloc] init];
            pickerData.valueData = i; //Recent 7 days
            pickerData.displayData = [NSString stringWithFormat:@"%d", i];
            [listMessagePeriod addObject:pickerData];
        }
        
        pickerLog.listPickerData = listMessagePeriod;
        
        if([DeviceManager getDeviceScreenKind] == DeviceScreenKindRetina4_0){
            CGRect pickerFrame = pickerLog.view.frame;
            pickerFrame.size.height += 88;
            [pickerLog.view setFrame:pickerFrame];
        }
    }
    
    int messagePeriod = [[UserManagerV2 sharedUserManager].currentUserAccessRight getMessagePeriod];
    NSDateComponents *componentsCurrent = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
    [pickerLog setSelectedIndex:(int)componentsCurrent.day];
    
    CGRect frame = pickerLog.view.frame;
    frame.origin.y = 0;
    pickerLog.view.frame = frame;
    pickerLog.delegate = self;
    [self.tabBarController addSubview:pickerLog.view];
}

- (IBAction) enableLogWS:(id)sender{
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        [switchLogWS setOn:switchLogWS2.isOn];
    }
    
    if (switchLogWS.on){
        [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:ENABLE_LOGWS];
    }
    else {
        [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:ENABLE_LOGWS];
    }
}

-(void) resetManagers{
    [UserManagerV2Demo updateUserManagerInstance];
    [RoomManagerV2 updateRoomManagerInstance];
    [FindManagerV2 updateRoomManagerInstance];
    [UnassignManagerV2 updateUnassignManagerInstance];
}

//- (IBAction) enableDemoMode:(id)sender{
//    
//    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
//        [switchDemoMode2 setHidden:!isConfigAccount];
//        [switchDemoMode setOn:switchDemoMode2.isOn];
//    }
//    else{
//        [switchDemoMode setHidden:!isConfigAccount];
//    }
//    
//    if (isConfigAccount && !isDemoMode && switchDemoMode.on){
//        [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:ENABLE_DEMO_MODE];
//        [((eHouseKeepingAppDelegate*)[[UIApplication sharedApplication] delegate]) createEditableCopyOfDatabaseIfNeeded];
//        [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationHideWifiView] object:nil];
//        [txtWSURL setHidden:YES];
//        [btnReset setHidden:NO];
//        [btnViewLog setFrame:CGRectMake(21, 553, 280, 32)];
//        [btnSubmit setFrame:CGRectMake(11, 593, 300, 55)];
//        [self resetManagers];
//    }
//    else if (isConfigAccount && isDemoMode && !switchDemoMode.on){
//        [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:ENABLE_DEMO_MODE];
//        [((eHouseKeepingAppDelegate*)[[UIApplication sharedApplication] delegate]) createEditableCopyOfDatabaseIfNeeded];
//        [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationShowWifiView] object:nil];
//        [txtWSURL setHidden:NO];
//        [btnReset setHidden:YES];
//        [txtWSURL setText:saveSettingModel.settingsWSURL];
//        [btnViewLog setFrame:CGRectMake(21, 513, 280, 32)];
//        [txtWSURL setFrame: CGRectMake(9, 553, 302, 31)];
//        [btnSubmit setFrame:CGRectMake(11, 593, 300, 55)];
//        [self resetManagers];
//    }
//}

//In current this function only apply for change Switch validate room
- (IBAction) validationRoomChanged:(id)sender
{
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        [switchValidateRoom setOn:switchValidateRoom2.isOn];
    }
    //UISwitch *switchRoomValidate = (UISwitch*)sender;
    [[UserManagerV2 sharedUserManager].currentUserAccessRight setIsValidateRoomValue:switchValidateRoom.on];
}
- (IBAction)onSwitch3GValueChanged:(UISwitch *)sender {
    [[NSUserDefaults standardUserDefaults] setInteger:[[NSNumber numberWithBool:sender.isOn] integerValue] forKey:ENABLE_3G];
//    [self set3G:sender.enabled];
    [[NSNotificationCenter defaultCenter] postNotificationName:kReachabilityUpdateNotification object:nil];
}

- (IBAction)onSwitchWifiValueChanged:(UISwitch *)sender {
    [[NSUserDefaults standardUserDefaults] setBool:sender.isOn forKey:ENABLE_WIFI];

//        [switchWifi setOn:sender.enabled animated:YES];
//        [switchWifi2 setOn:sender.enabled animated:YES];
    [[NSNotificationCenter defaultCenter] postNotificationName:kReachabilityUpdateNotification object:nil];
}


- (IBAction)switchConnection:(id)sender {
    MBSwitch *switchSender = (MBSwitch*)sender;
    NSInteger enable3G = [[NSUserDefaults standardUserDefaults] integerForKey:ENABLE_3G];
    if (sender == switch3G2) {
        enable3G = switchSender.isOn ? 1 : 0;
    }else if (sender == switchWifi2){
        enable3G = switchSender.isOn ? 0 : 1;
    }
    
    [self set3G:((enable3G == 1) ? YES : NO)];
    [[NSUserDefaults standardUserDefaults] setInteger:enable3G forKey:ENABLE_3G];

    
    [[NSNotificationCenter defaultCenter] postNotificationName:kReachabilityUpdateNotification object:nil];
}

- (IBAction)btnPTTConfigClick:(UIButton *)sender {
#pragma mark - PTT REMOVE END
#if DEFAULT_ENABLE_PTT
    SettingViewController *settingController = [[SettingViewController alloc] initWithNibName:@"SettingViewController" bundle:[Utility getPttLibraryBundle]];
    settingController.selectedLanguageName = @"English";
    [self presentViewController:settingController animated:YES completion:nil];
#endif
#pragma mark - PTT REMOVE END
}

// CFG [20160927/CRF-00001432] - Click validation.
- (IBAction)switchBackgroundModeOnChange:(id)sender {
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        [switchBackgroundMode setOn:switchBackgroundMode2.isOn];
    }
//    if (switchBackgroundMode.on){
//        [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:ENABLE_BACKGROUND_MODE];
//    }
//    else {
//        [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:ENABLE_BACKGROUND_MODE];
//    }
}
// CFG [20160927/CRF-00001432] - End.

- (IBAction) btnSubmitClicked:(id) sender
{
//    NSString *msgContent = @"";
//    if(originMode == 1 && !isDemoMode){
//        msgContent = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_message_change_to_online_mode];
//        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:msgContent delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles:[[LanguageManagerV2 sharedLanguageManager] getNo], nil];
//        alert.delegate = self;
//        [alert setTag:tagConfirmDemoMode];
//        [alert show];
//        return;
//    }
//    else if(originMode == 0 && isDemoMode){
//        msgContent = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_message_change_to_demo_mode];
//        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:msgContent delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles:[[LanguageManagerV2 sharedLanguageManager] getNo], nil];
//        alert.delegate = self;
//        [alert setTag:tagConfirmDemoMode];
//        [alert show];
//        return;
//    }
//    else{
        [self saveData];
        //HaoTran[20130531/Force Log out] - Check to log out current user if database was cleared
        if(isClearedDatabase){
            
            UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:imgYesBtn], [UIImage imageNamed:imgNoBtn], nil] title:@"" message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_message_clear_data_base] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_CANCEL], nil];
            alert.tag = tagClearData;
            [alert show];
            
            return;
        }
        //HaoTran[20130531/Force Log out] - END
        
        //Log in with config account
        if (isConfigAccount) {
            //This delegate to show LoginView
            [delegate btnSubmitClicked];
            [self.navigationController popViewControllerAnimated:YES];
        }
//    }
}

-(void) initializeViewForConfigAccount
{
    CGRect viewFrame = self.view.frame;
    viewFrame.size.height = 436;
    
    CGRect imgBackgroundFrame = imgviewBackground.frame;
    imgBackgroundFrame.size.height = 436;
    
    CGRect scrollViewFrame = scrollView.frame;
    scrollViewFrame.origin.y = 0;
    scrollViewFrame.size.height = 436;
    
    //Check to resize layout for all iphone screen
    int deviceKind = [DeviceManager getDeviceScreenKind];
    if(deviceKind == DeviceScreenKindRetina4_0){
        viewFrame.size.height = 524;
        imgBackgroundFrame.size.height = 524;
        scrollViewFrame.size.height = 524;
    }
    
    [self.view setFrame:viewFrame];
    [imgviewBackground setFrame:imgBackgroundFrame];
    [scrollView setFrame:scrollViewFrame];
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
}

#pragma mark - iOS 7 Resources
-(void)loadFlatResource
{
    //Switch for iOS 7
    [self showAllSwitchBelongToOSVersion];
    
    [optionNotification setBackgroundImage:[UIImage imageNamed:imgOptionSetting_Flat] forState:UIControlStateNormal];
    [optionNotification setBackgroundImage:[UIImage imageNamed:imgOptionSettingGray_Flat] forState:UIControlStateHighlighted];
    [optionLanguage  setBackgroundImage:[UIImage imageNamed:imgOptionSetting_Flat] forState:UIControlStateNormal];
    [optionLanguage setBackgroundImage:[UIImage imageNamed:imgOptionSettingGray_Flat] forState:UIControlStateHighlighted];
    optionLanguage.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;//NSLineBreakByTruncatingTail;
    [optionSynch setBackgroundImage:[UIImage imageNamed:imgOptionSetting_Flat] forState:UIControlStateNormal];
    [optionSynch setBackgroundImage:[UIImage imageNamed:imgOptionSettingGray_Flat] forState:UIControlStateHighlighted];
    [optionPhotoResolution setBackgroundImage:[UIImage imageNamed:imgOptionSetting_Flat]forState:UIControlStateNormal];
    [optionPhotoResolution setBackgroundImage:[UIImage imageNamed:imgOptionSettingGray_Flat] forState:UIControlStateHighlighted];
    [optionLanguage setTitleEdgeInsets:UIEdgeInsetsMake(0, 3, 0, 29)];
    [optionNotification setTitleEdgeInsets:UIEdgeInsetsMake(0, 3, 0, 20)];
    [optionNotification.titleLabel setAdjustsFontSizeToFitWidth:YES];
    [messagePeriodButton setBackgroundImage:[UIImage imageNamed:imgOptionSetting_Flat] forState:UIControlStateNormal];
    [messagePeriodButton setBackgroundImage:[UIImage imageNamed:imgOptionSettingGray_Flat] forState:UIControlStateHighlighted];
    [btnSubmit setBackgroundImage:[UIImage imageNamed:imgBtnSubmitFlat] forState:UIControlStateNormal];
    [btnSubmit setBackgroundImage:[UIImage imageNamed:imgBtnSubmitDisableFlat] forState:UIControlStateHighlighted];
    
    [btnViewLog setBackgroundImage:[UIImage imageNamed:imgBtnViewLogFlat] forState:UIControlStateNormal];
    [btnViewLog setBackgroundImage:[UIImage imageNamed:imgBtnViewLogFlat] forState:UIControlStateHighlighted];
    
    [btnResetDatabase setBackgroundImage:[UIImage imageNamed:imgBtnViewLogFlat] forState:UIControlStateNormal];
    [btnResetDatabase setBackgroundImage:[UIImage imageNamed:imgBtnViewLogFlat] forState:UIControlStateHighlighted];
    
    [btnExportDatabase setBackgroundImage:[UIImage imageNamed:imgBtnViewLogFlat] forState:UIControlStateNormal];
    [btnExportDatabase setBackgroundImage:[UIImage imageNamed:imgBtnViewLogFlat] forState:UIControlStateHighlighted];
    
    [btnExportDatabase setTitle:[L_btn_export_database currentKeyToLanguage] forState:UIControlStateNormal];
    [btnResetDatabase setTitle:[L_btn_reset_database currentKeyToLanguage] forState:UIControlStateNormal];
    
    [btnPTTConfig setBackgroundImage:[UIImage imageNamed:imgBtnViewLogFlat] forState:UIControlStateNormal];
    [btnPTTConfig setBackgroundImage:[UIImage imageNamed:imgBtnViewLogFlat] forState:UIControlStateHighlighted];
    
    [btnSendEmail setBackgroundImage:[UIImage imageNamed:imgBtnViewLogFlat] forState:UIControlStateNormal];
    [btnSendEmail setBackgroundImage:[UIImage imageNamed:imgBtnViewLogFlat] forState:UIControlStateHighlighted];
    
//    [btnReset setBackgroundImage:[UIImage imageNamed:imgBtnViewLogFlat] forState:UIControlStateNormal];
//    [btnReset setBackgroundImage:[UIImage imageNamed:imgBtnViewLogFlat] forState:UIControlStateHighlighted];
}

@end
