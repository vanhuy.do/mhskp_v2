//
//  SynchViewV2.h
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SynchViewV2;

@protocol SynchViewV2Delegate <NSObject>

@optional

-(void)SynchViewV2Controller:(SynchViewV2 *)controller didFinishSetupWithInfo:(NSString *)info;

@end

@interface SynchViewV2 : UIViewController<UIPickerViewDelegate,UIPickerViewDataSource> {
    
    IBOutlet UIPickerView *pickerView;
    IBOutlet UIButton *btnExit;
    NSMutableArray *dataSynch;
    NSInteger rowSelected;
    IBOutlet UIBarButtonItem *labelCancel;
    IBOutlet UIBarButtonItem *labelDone;
    NSString *sSynchManual;
    NSString *sSynchAutomatic;
    BOOL isAlreadyLayoutSubviews;
}

- (IBAction)cancel:(id)sender;
- (IBAction)done:(id)sender;
- (IBAction)ExitFromThisView:(id)sender;
@property (nonatomic, assign) id<SynchViewV2Delegate> delegate;
@property (nonatomic ,strong ) UIButton *btnExit;
@property (nonatomic ,strong) NSMutableArray *dataSynch;
@property (nonatomic ,strong) NSString *sSynchManual;
@property (nonatomic ,strong) NSString *sSynchAutomatic;

-(void) setCaptionsView;
-(void) setSelectedRowWithData:(NSString *) currentData;

@end
