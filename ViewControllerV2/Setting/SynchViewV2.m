//
//  SynchViewV2.m
//  mHouseKeeping
//
//  Created by Thuong Nguyen Manh on 2/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SynchViewV2.h"
#import "LanguageManager.h"
#import "DeviceManager.h"

@implementation SynchViewV2

@synthesize delegate,btnExit,dataSynch;
@synthesize sSynchAutomatic, sSynchManual;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}


#pragma mark - View lifecycle

//Call after set frames serveral subviews of this view
-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0") && !isAlreadyLayoutSubviews){
        isAlreadyLayoutSubviews = YES;
        CGRect framePickerView = pickerView.frame;
        UIToolbar *blurView = [[UIToolbar alloc] initWithFrame:framePickerView];
        blurView.translucent = YES;
        [self.view insertSubview:blurView belowSubview:pickerView];
    }
}

- (void)viewDidLoad
{
    isAlreadyLayoutSubviews = NO;
    self.dataSynch = [NSMutableArray array];
    //default  choose row 0
    rowSelected=0;
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{    
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark
#pragma mark  pickerview datasource/delegate
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return [self.dataSynch count];
}
-(NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return [self.dataSynch objectAtIndex:row];
}
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    rowSelected = row;
}

- (IBAction)cancel:(id)sender {
    [self.view removeFromSuperview];
}
- (IBAction)done:(id)sender {
    [self.view removeFromSuperview];
    [delegate SynchViewV2Controller:self didFinishSetupWithInfo:[NSString stringWithFormat:@"%@",[dataSynch objectAtIndex:rowSelected]]];
}
- (IBAction)ExitFromThisView:(id)sender {
    [self.view removeFromSuperview];
    
    
}
#pragma mark - set captions of view
-(void)setCaptionsView {   
    [labelCancel setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_CANCEL]];
    [labelDone setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_DONE]];
    
    //set language synch
    sSynchManual = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_synch_manual];
    sSynchAutomatic = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_synch_automatically];
    
    [dataSynch removeAllObjects];
    
    [dataSynch addObject:sSynchManual];
    [dataSynch addObject:sSynchAutomatic];
    
    [pickerView reloadAllComponents];
}

-(void)setSelectedRowWithData:(NSString *)currentData
{
    if ([currentData isEqualToString:sSynchManual] == YES) {
        rowSelected = [dataSynch indexOfObject:sSynchManual];
        [pickerView selectRow:rowSelected inComponent:0 animated:NO];
    }
    if ([currentData isEqualToString:sSynchAutomatic] == YES) {
        rowSelected = [dataSynch indexOfObject:sSynchAutomatic];
        [pickerView selectRow:rowSelected inComponent:0 animated:NO];
    }
}

@end
