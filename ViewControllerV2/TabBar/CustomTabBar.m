//
//  CustomTabBar.m
//  mHouseKeeping
//
//  Created by vinhnguyen on 4/15/13.
//
//

#import "CustomTabBar.h"

@implementation CustomTabBar

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (CGSize)sizeThatFits:(CGSize)size {
    CGRect frame = [UIScreen mainScreen].applicationFrame;
    CGSize newSize = CGSizeMake(frame.size.width , HEIGHT_TAB_BAR);
    return newSize;
}

@end
