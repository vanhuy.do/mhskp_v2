//
//  TopbarViewV2.h
//  mHouseKeeping
//
//  Created by khanhnguyen on 2/14/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SuperRoomModelV2.h"
#import "MarqueeLabel.h"

@interface TopbarViewV2 : UIView{
//    UILabel  * hotelName;
    MarqueeLabel * hotelName;
    UILabel * houseKeeperTitle;
    UILabel * houseKeeperName;
    UILabel *buildingNameLabel;
    UIImageView * hotelLogo;
}
//@property(strong) UILabel  * hotelName;
@property(strong) MarqueeLabel  * hotelName;
@property(strong) UILabel  * houseKeeperTitle;
@property(strong) UILabel  * houseKeeperName;
@property(strong) UIImageView  * hotelLogo;
@property (strong) UILabel *buildingNameLabel;
- (id)initViewWithSuperModel:(SuperRoomModelV2 *) modelSuper;
-(void)refresh:(SuperRoomModelV2 *) modelSuper;
-(void) setCaptionsView;

@end
