//
//  TopbarViewV2.m
//  mHouseKeeping
//
//  Created by khanhnguyen on 2/14/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "TopbarViewV2.h"
#import "SuperRoomModelV2.h"
#import "RoomManagerV2.h"

#define topbarFrame CGRectMake(0, 0, 320, 60)
#define hotelLogoFrame CGRectMake(10, 5, 40, 40)

#define hotelNameFrame CGRectMake(55, 0, 120 + 130 + 5, 28)

//This frame will apply with hotel logo is null (no image hotel logo)
//#define hotelNameNoLogoFrame CGRectMake(5, 0, 120 + 130 + 5 + 55, 28)

#define buildingNameFrame CGRectMake(180, 0, 130, 28)

//#define houseKeeperNameFrame CGRectMake(180, 24, 130, 21)
#define houseKeeperNameFrame CGRectMake(55, 24, 250, 21)
#define houseKeeperTitleFrame CGRectMake(55, 24, 110 + 130 + 15, 21)
#define textSize       19
#define textSizeHouseKeeper       16
#define textFontSubTitle        @"Arial-BoldMT"
#define textFont    @"ArialMT"

@implementation TopbarViewV2

@synthesize hotelLogo =_hotelLogo;
@synthesize houseKeeperName =_houseKeeperName;
@synthesize houseKeeperTitle=_houseKeeperTitle;
@synthesize hotelName =_hotelName;
@synthesize buildingNameLabel = _buildingNameLabel;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(id)initViewWithSuperModel:(SuperRoomModelV2 *) modelSuper
{
    self = [super initWithFrame:topbarFrame];
    if (self) {
        // Initialization code
        hotelLogo = [[UIImageView alloc] initWithFrame:hotelLogoFrame];
        [hotelLogo setBackgroundColor:[UIColor clearColor]];
        
//        houseKeeperTitle = [[UILabel alloc] initWithFrame:houseKeeperTitleFrame];
//        [houseKeeperTitle setBackgroundColor:[UIColor clearColor]];
//        [houseKeeperTitle setFont:[UIFont fontWithName:textFont size:textSize]];
//        [houseKeeperTitle setTextColor:[UIColor darkGrayColor]];
//        [houseKeeperTitle setMinimumFontSize:10];
//        [houseKeeperTitle setAdjustsFontSizeToFitWidth:YES];
        
        houseKeeperName = [[UILabel alloc] initWithFrame:houseKeeperNameFrame];
        [houseKeeperName setBackgroundColor:[UIColor clearColor]];
         [houseKeeperName setFont:[UIFont fontWithName:textFont size:textSizeHouseKeeper]];
        [houseKeeperName setTextColor:[UIColor darkGrayColor]];
        houseKeeperName.textAlignment = NSTextAlignmentLeft;//NSTextAlignmentCenter;
        
//        hotelName = [[UILabel alloc] initWithFrame:hotelNameFrame];
//        [hotelName setBackgroundColor:[UIColor clearColor]];
//         [hotelName setFont:[UIFont fontWithName:textFontSubTitle size:textSize]];
//        [hotelName setTextColor:[UIColor blackColor]];
        
        hotelName = [[MarqueeLabel alloc] initWithFrame:hotelNameFrame];
        [hotelName setBackgroundColor:[UIColor clearColor]];
        [hotelName setFont:[UIFont fontWithName:textFontSubTitle size:textSize]];
        [hotelName setTextColor:[UIColor blackColor]];
        //hotelName.textAlignment = NSTextAlignmentCenter;
        hotelName.textAlignment = NSTextAlignmentLeft;
        hotelName.marqueeType = MLContinuous;
        hotelName.continuousMarqueeExtraBuffer = 80.0f;
        hotelName.animationDelay = 0;
        
        buildingNameLabel = [[UILabel alloc] initWithFrame:buildingNameFrame];
        [buildingNameLabel setBackgroundColor:[UIColor clearColor]];
        [buildingNameLabel setFont:[UIFont fontWithName:textFontSubTitle size:textSize]];
        [buildingNameLabel setTextColor:[UIColor blackColor]];

        
//        NSDictionary *dic = [[LanguageManagerV2 sharedLanguageManager] getCurrentDictionaryLanguage];
//        if ([UserManagerV2 isSupervisor]) {
//            houseKeeperTitle.text = [dic valueForKey:[NSString stringWithFormat:@"%@", L_SUPERVISOR_NAME]];
//        } else {
//            houseKeeperTitle.text = [dic valueForKey:[NSString stringWithFormat:@"%@", L_HOUSEKEEPER_NAME]];
//        }
        
        NSString * houseKeeperNameStr = [[RoomManagerV2 sharedRoomManager] getHouseKeeperName];
        houseKeeperName.text = houseKeeperNameStr;
        
        //combine title and name
//        [houseKeeperTitle setText:[NSString stringWithFormat:@"%@ %@", houseKeeperTitle.text, houseKeeperNameStr]];
        
        NSString* propertyName = [[RoomManagerV2 sharedRoomManager] getPropertyName:modelSuper];
        NSString* buildingName = [[RoomManagerV2 sharedRoomManager] getbuildingName:modelSuper];
        NSString* hotelNameStr = [NSString stringWithFormat:@"%@",buildingName == nil ? @"":buildingName ];
        NSString* propertyNameStr = [NSString stringWithFormat:@"%@",propertyName== nil ? @"":propertyName];
        
        hotelName.text = [NSString stringWithFormat:@"%@ %@", hotelNameStr, propertyNameStr];
        //hotelName.text = [NSString stringWithFormat:@"%@", hotelNameStr];
        
        buildingNameLabel.text =  propertyNameStr;
        
        NSData* logoData = [[RoomManagerV2 sharedRoomManager] getHotelLogo:modelSuper];
        if (logoData != nil) {
            hotelLogo.image = [UIImage imageWithData:logoData];
  
            //set frame if has logo image
            //[hotelName setFrame:hotelNameFrame];
        }
        
        [self addSubview:hotelLogo];
        [self addSubview:hotelName];
        [self addSubview:houseKeeperName];
//        [self addSubview:houseKeeperTitle];
        [self addSubview:buildingNameLabel];
        
        //disable building name because add text to hotel name
        [buildingNameLabel setHidden:YES];
        
        [self setBackgroundColor:[UIColor clearColor]];
    }
    return self;
}

-(void)refresh:(SuperRoomModelV2 *) modelSuper{
  
    
//    NSDictionary *dic = [[LanguageManagerV2 sharedLanguageManager] getCurrentDictionaryLanguage];
//    if ([UserManagerV2 isSupervisor]) {
//        houseKeeperTitle.text = [dic valueForKey:[NSString stringWithFormat:@"%@", L_SUPERVISOR_NAME]];
//    } else {
//        houseKeeperTitle.text = [dic valueForKey:[NSString stringWithFormat:@"%@", L_HOUSEKEEPER_NAME]];
//    }
    NSString * houseKeeperNameStr = [[RoomManagerV2 sharedRoomManager] getHouseKeeperName];
    houseKeeperName.text= houseKeeperNameStr;
    
    //combine title and name
//    [houseKeeperTitle setText:[NSString stringWithFormat:@"%@ %@", houseKeeperTitle.text, houseKeeperNameStr]];
    
    NSString* propertyName = [[RoomManagerV2 sharedRoomManager] getPropertyName:modelSuper];
    NSString* buildingName = [[RoomManagerV2 sharedRoomManager] getbuildingName:modelSuper];
    
    NSString* hotelNameStr = [NSString stringWithFormat:@"%@",buildingName == nil ? @"":buildingName ];
    NSString* propertyNameStr = [NSString stringWithFormat:@"%@",propertyName== nil ? @"":propertyName];
    
    hotelName.text = [NSString stringWithFormat:@"%@ %@", hotelNameStr, propertyNameStr];
    //hotelName.text = [NSString stringWithFormat:@"%@", hotelNameStr];
    
    buildingNameLabel.text =  propertyNameStr;
    
    NSData* logoData = [[RoomManagerV2 sharedRoomManager] getHotelLogo:modelSuper];
    if (logoData != nil)
    {
        hotelLogo.image = [UIImage imageWithData:logoData];
        
        //set frame if has logo image
        //[hotelName setFrame:hotelNameFrame];
    }

}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
       /******************* nmtan end change *******************/
    

}
-(void) bindingData{
    
}

#pragma mark - === Set Captions ===
#pragma mark

-(void)setCaptionsView {
    [self refresh:[[SuperRoomModelV2 alloc] init]];
}


@end
