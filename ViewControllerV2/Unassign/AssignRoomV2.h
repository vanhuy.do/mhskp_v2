//
//  AssignRoomV2.h
//  mHouseKeeping
//
//  Created by TMS TRIBE on 2/23/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ehkDefinesV2.h"
#import "RemarkViewV2.h"
#import "TopbarViewV2.h"
#import "UserManagerV2.h"
//#import "RoomManagerV2.h"
#import "DateTimePicker.h"
#import "AdHocMessageManagerV2.h"
#import "UserListPickerViewV2.h"
#import "ehkDefines.h"
//#import "UnassignManagerV2.h"


@interface AssignRoomV2 : UIViewController<UITableViewDataSource,UITableViewDelegate,RemarkViewV2Delegate,DateTimePickerViewV2Delegate,UserListPickerViewV2Delegate,UIAlertViewDelegate>{
    
    UILabel *assignToTitleLabel;
    UILabel *assignDateToTitleLabel;
    UILabel *assignTimeAtTitleLabel;
    UILabel *expectedCleaningTimeTitleLabel;
    
    UILabel *assignToLabel;
    UILabel *assignDateToLabel;
    UILabel *expectedCleaningTimelabel;
    UIButton *expectedCleaningTimeButton;
    UIButton *assignTimeAtButton;
    
    NSString *assignToTitle;
    NSString *assignDateToTitle;
    NSString *assignTimeAtTitle;
    NSString *expectedCleaningTimeTitle;
    UITableView *tableViewRemark;
    UIButton *assignToButton;
    UIButton *assignDateToButton;
    
    UIImageView *nextImageView;
    UIImage *img;
    TopbarViewV2 * topBarView;
    
    RoomModelV2 *roomModel;
    UnassignModelV2 *unassignModel;
    UIButton *roomNoButton;
    UIButton *roomStatusImage;
    UIButton *roomStatusButton;
    UIButton *guestNameButton;

    UIDatePicker *datePicker;
    NSDate *selectedDate;
    NSMutableArray *userList;
    AssignRoomModelV2 *asignRoomModel;
    UserListModelV2 *userListSelected;
    
    BOOL isAssigned;

}
@property (strong, nonatomic) NSString *msgbtnYes;
@property (strong, nonatomic) NSString *msgbtnNo;
@property (strong, nonatomic) NSString *msgSaveAssign;
@property (strong, nonatomic) NSString *msgDiscardCount;

@property (nonatomic, strong) IBOutlet UILabel *assignToTitleLabel;
@property (nonatomic, strong) IBOutlet UILabel *assignDateToTitleLabel;
@property (nonatomic, strong) IBOutlet UILabel *assignTimeAtTitleLabel;
@property (nonatomic, strong) IBOutlet UILabel *expectedCleaningTimeTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *submitLabel;


@property (nonatomic, strong) IBOutlet UILabel *assignToLabel;

//@property (nonatomic, strong) IBOutlet UILabel *assignDateToLabel;
@property (nonatomic, strong) IBOutlet UILabel *expectedCleaningTimelabel;
@property (nonatomic, strong) IBOutlet UIButton *assignTimeAtButton;

@property (nonatomic, strong) NSString *assignToTitle;
@property (nonatomic, strong) NSString *assignDateToTitle;
@property (nonatomic, strong) NSString *assignTimeAtTitle;
@property (nonatomic, strong) NSString *expectedCleaningTimeTitle;

@property (nonatomic, strong) IBOutlet UITableView *tableViewRemark;
@property (nonatomic, strong) IBOutlet UIButton *assignToButton;
@property (nonatomic, strong) IBOutlet UIButton *assignDateToButton;
@property (nonatomic, strong) RoomModelV2 *roomModel;
@property (nonatomic, strong) UnassignModelV2 *unassignModel;

@property (nonatomic, strong) IBOutlet UIButton *roomNoButton;
@property (nonatomic, strong) IBOutlet UIButton *roomStatusImage;
@property (nonatomic, strong) IBOutlet UIButton *roomStatusButton;
@property (nonatomic, strong) IBOutlet UIButton *guestNameButton;
@property (nonatomic, strong) IBOutlet UIButton *expectedCleaningTimeButton;

@property (nonatomic, strong) IBOutlet UILabel *remarkLabelView;
@property (nonatomic, strong) IBOutlet UILabel *remarkLabelTitle;

@property (nonatomic, strong) AssignRoomModelV2 *asignRoomModel;
@property (nonatomic, strong) IBOutlet UIView *vAssignRoom;


@property (nonatomic) BOOL isAssigned;

@property (nonatomic,strong) NSDate *selectedDate;
@property (nonatomic,strong)  NSMutableArray *userList;
- (void) setCaptionView;
- (void)loadData;
- (IBAction)assignToButtonDidSelect:(id)sender;
- (IBAction)assignDateToButtonDidSelect:(id)sender;
- (IBAction)assignTimeAtButtonDidSelect:(id)sender;
- (IBAction)btnSubmit_Clicked:(id)sender;

- (IBAction)remarkDidSelect:(id)sender;
@end
