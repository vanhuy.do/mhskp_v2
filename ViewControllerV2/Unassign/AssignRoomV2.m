//
//  AssignRoomV2.m
//  mHouseKeeping
//
//  Created by TMS TRIBE on 2/23/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "AssignRoomV2.h"
#import "CustomAlertViewV2.h"
#import "NetworkCheck.h"
#import "MyNavigationBarV2.h"
#import "LogFileManager.h"
#import "DeviceManager.h"

@interface AssignRoomV2 (PrivateMethods)

#define tagTopbarView 1234

-(void) loadTopbarView;
-(void) showTopbarView;
-(void) hideTopbarView;
-(void) setHiddenTopbarView;
-(void) adjustShowForViews;
-(void) adjustRemoveForViews;
-(void) addButtonHandleShowHideTopbar;
-(void) topbarTapped:(UIButton *) sender;
-(TopbarViewV2 *) getTopBarView;
-(void) keyboardWillShow;
-(void) keyboardWillHide;

@end


@implementation AssignRoomV2

@synthesize assignToTitleLabel;
@synthesize assignDateToTitleLabel;
@synthesize assignTimeAtTitleLabel;
@synthesize expectedCleaningTimeTitleLabel;

@synthesize assignToLabel;
//@synthesize assignDateToLabel;
@synthesize expectedCleaningTimelabel;
@synthesize assignTimeAtButton;

@synthesize assignDateToTitle;
@synthesize assignTimeAtTitle;
@synthesize assignToTitle;
@synthesize expectedCleaningTimeTitle;

@synthesize tableViewRemark;
@synthesize assignToButton;
@synthesize assignDateToButton;
@synthesize roomModel;
@synthesize roomNoButton;
@synthesize roomStatusImage;
@synthesize roomStatusButton;
@synthesize guestNameButton;

@synthesize selectedDate;

@synthesize expectedCleaningTimeButton;

@synthesize userList;

@synthesize msgbtnNo,msgbtnYes,msgSaveAssign,msgDiscardCount;
@synthesize unassignModel;
@synthesize isAssigned;
@synthesize remarkLabelView;
@synthesize vAssignRoom;
@synthesize asignRoomModel;
@synthesize remarkLabelTitle;

#define sizeTitleLabel 16
#define fontTitleLabel @"Arial-BoldMT"
#define NEXTIMAGE @"gray_arrow_36x36.png"
#define KTAGREMARK 1
#define KTAGREMARKTEXT 2
#define imageColorLabel @"tab_small.png"

#define backGroundButton @"setting_bt.png"

#define kInsetsButton UIEdgeInsetsMake(0, 10, 0, 30)
#define kInsetsButton1 UIEdgeInsetsMake(0, 10, 0, 0)

#define kDateTimeFormat @"dd MMM yyyy"

#define kTagAssignDateTo 12
#define ktagAssignTimeAt 13
#define tagAlertBack 10
#define tagAlertNo 11
#define tagTopbarView 1234
#define kTimeFormat @"yyyy-MM-dd HH:mm:ss"

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

-(void)viewDidAppear:(BOOL)animated{
    [self setCaptionView];
    [super viewDidAppear:animated];
}

#pragma mark
#pragma mark - save data

-(void)showAlert{
    
    UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:msgSaveAssign delegate:self cancelButtonTitle:msgbtnYes otherButtonTitles:msgbtnNo, nil]; 
    alert.delegate = self;
    alert.tag = tagAlertBack;
    [alert show];
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
//    
//    if(buttonIndex == 0){
//        asignRoomModel.assignRoom_assign_id = unassignModel.unassignroom_id;
//        asignRoomModel.assignRoomID = unassignModel.unassignroom_id;
//        asignRoomModel.assignRoom_HouseKeeper_Id = userListSelected.userListId;
//
//        [UnassignManagerV2 insertAssignRoom:asignRoomModel];
//        
//    }
}

-(void) saveData{
    [self showAlert];
}



//-(void) alertAdvancedOKButtonPressed:(AlertAdvancedSearch *) alertView{
//    
//}
//
//-(void) alertAdvancedCancelButtonPressed:(AlertAdvancedSearch *)alertView{
//
//}


#pragma mark - View lifecycle
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //show wifi view
    if(!isDemoMode){
        [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", notificationShowWifiView] object:nil];
    }
    [[HomeViewV2 shareHomeView] setSelectedButton:tagOfUnassignButton];
}


- (void)redrawDate
{
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:kDateTimeFormat];
    
	NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
	[timeFormatter setTimeStyle:NSDateFormatterShortStyle];
	

    [assignDateToButton setTitle:[NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:selectedDate]]
                        forState:UIControlStateNormal];
    [assignTimeAtButton setTitle:[NSString stringWithFormat:@"%@",[timeFormatter stringFromDate:selectedDate]] 
                        forState:UIControlStateNormal];
    
	
    roomModel.room_Id = unassignModel.unassignroom_room_id;
    [[RoomManagerV2 sharedRoomManager] loadRoomModel:roomModel];
    /*
    [expectedCleaningTimeButton setTitle:roomModel.room_expected_inspection_time forState:UIControlStateNormal];
     */
}

- (void)viewDidLoad
{
    isAssigned = NO;
    asignRoomModel = [[AssignRoomModelV2 alloc] init];

    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:imgBackground]]];
    
    // title of label 
    assignToTitle = [NSString stringWithFormat:@"%@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_assign_to]];
    assignDateToTitle = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_assign_date_to];
    assignTimeAtTitle = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_assign_time_at];
    expectedCleaningTimeTitle = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_EXPECTED_CLEANING_TIME];
    
    
    /********************************/
     assignToTitleLabel.font = [UIFont fontWithName:fontTitleLabel size:sizeTitleLabel];
     assignDateToTitleLabel.font = [UIFont fontWithName:fontTitleLabel size:sizeTitleLabel];
     assignTimeAtTitleLabel.font = [UIFont fontWithName:fontTitleLabel size:sizeTitleLabel];
     expectedCleaningTimeTitleLabel.font = [UIFont fontWithName:fontTitleLabel size:sizeTitleLabel];
     
    assignToTitleLabel.text = assignToTitle;
    assignDateToTitleLabel.text = assignDateToTitle;
    assignTimeAtTitleLabel.text = assignTimeAtTitle;
    expectedCleaningTimeTitleLabel.text = expectedCleaningTimeTitle;
    
    
    
    img  = [UIImage imageNamed:imageColorLabel];
    

    /********************************/
    
    UIImage *nextImage=[UIImage imageNamed:NEXTIMAGE];
    nextImageView=[[UIImageView alloc] initWithImage:nextImage] ;
    
    [tableViewRemark setBackgroundColor:[UIColor clearColor]];

    
    
    [assignTimeAtButton setBackgroundImage:[UIImage imageNamed:backGroundButton] forState:UIControlStateNormal];
    
    // allignment content of button 
    [assignDateToButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [assignToButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [assignTimeAtButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    
    [assignToButton setBackgroundImage:[UIImage imageNamed:backGroundButton] forState:UIControlStateNormal];
    [assignDateToButton setBackgroundImage:[UIImage imageNamed:backGroundButton] forState:UIControlStateNormal];

    
    [assignToButton.titleLabel setAdjustsFontSizeToFitWidth:YES];
    [expectedCleaningTimeButton.titleLabel setAdjustsFontSizeToFitWidth:YES];
    [guestNameButton.titleLabel setAdjustsFontSizeToFitWidth:YES];
    [assignDateToButton.titleLabel setAdjustsFontSizeToFitWidth:YES];
    [assignTimeAtButton.titleLabel setAdjustsFontSizeToFitWidth:YES];
    [roomNoButton.titleLabel setAdjustsFontSizeToFitWidth:YES];
    
    assignToButton.contentEdgeInsets =kInsetsButton;
    assignDateToButton.contentEdgeInsets = kInsetsButton;
    assignTimeAtButton.contentEdgeInsets = kInsetsButton; 
    expectedCleaningTimeButton.contentEdgeInsets = kInsetsButton;     
    guestNameButton.contentEdgeInsets  = kInsetsButton1;
    roomNoButton.contentEdgeInsets = kInsetsButton1;     
    
    selectedDate = [[NSDate alloc] init];
    
    [[HomeViewV2 shareHomeView] waitingLoadingData];
    [self loadData];
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
    
    
    [self redrawDate];
    
    [self loadTopbarView];
    
    [super viewDidLoad];
    //Resize content views of current VC to fit with Navigation bar and tabbar
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    // Do any additional setup after loading the view from its nib.
}

-(void)loadData{
    // setcaption
    [roomNoButton setTitle:[NSString stringWithFormat:@"%@ %@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_room_no_assign_room], unassignModel.unassignroom_room_id] forState:UIControlStateNormal];
    
    /*****load cleaning Status******/
   RoomStatusModelV2 *roomStatusModel = [[RoomStatusModelV2 alloc] init];
   roomStatusModel.rstat_Id=(int)unassignModel.unassignroom_room_status_id;
    [[RoomManagerV2 sharedRoomManager] loadRoomStatusData:roomStatusModel];
    
    [roomStatusImage setBackgroundImage:[UIImage imageWithData:roomStatusModel.rstat_Image] forState:UIControlStateNormal];
    
    [roomStatusButton setTitle:roomStatusModel.rstat_Name forState:UIControlStateNormal];
    /*****end load room Status******/
    
    [guestNameButton setTitle:[NSString stringWithFormat:@"- %@ %@",unassignModel.unassignroom_guest_first_name == nil ? @"":unassignModel.unassignroom_guest_first_name , unassignModel.unassignroom_guest_last_name == nil ? @"":unassignModel.unassignroom_guest_last_name] forState:UIControlStateNormal];
    
    AdHocMessageManagerV2 *adhocMsgManager = [[AdHocMessageManagerV2 alloc] init];
    userList  = [adhocMsgManager loadAllUserListData];
    
    if([userList count]){
    // set default of user list 
    UserListModelV2 *item = (UserListModelV2*)[userList objectAtIndex:0];
    
    
        
    if ([[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString: (NSString *)ENGLISH_LANGUAGE] == NO) {
        [assignToButton setTitle:item.userListFullNameLang forState:UIControlStateNormal];  
    } else{
        [assignToButton setTitle:item.userListFullName forState:UIControlStateNormal];  
    }
    userListSelected = item;
    }

}



- (IBAction)assignToButtonDidSelect:(id)sender {
   
 
    
    UserListPickerViewV2 *userListPicker = [[UserListPickerViewV2 alloc] initPickerViewV2WithDatas:userList AndSelectedData:nil AndIndex:nil];
    [userListPicker setDelegate:self];
    [userListPicker showPickerViewV2];
}


- (IBAction)assignDateToButtonDidSelect:(id)sender {
       
	DateTimePicker *dateTimePicker = [[DateTimePicker alloc] initWithUIDateTimeModel:UIDatePickerModeDate andDefaultDate:selectedDate];
	dateTimePicker.delegate = self;

	dateTimePicker.tag = kTagAssignDateTo;
    [dateTimePicker showPickerViewV2];


}

- (IBAction)assignTimeAtButtonDidSelect:(id)sender {
    // Initialization code
  	DateTimePicker *dateTimePicker = [[DateTimePicker alloc] initWithUIDateTimeModel:UIDatePickerModeTime andDefaultDate:selectedDate];
	dateTimePicker.delegate = self;
	dateTimePicker.tag = ktagAssignTimeAt;
    [dateTimePicker showPickerViewV2];

}
#pragma mark - UIAlertView Delegate Methods
-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    switch (alertView.tag) {
        case tagAlertBack:
        {
            switch (buttonIndex)
            {
                case 0:
                {
                    // @ show loading data .
                    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
                    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getRaSavingData]];
                    [self.tabBarController.view addSubview:HUD];
                    [HUD show:YES];  
                    
                    asignRoomModel.assignRoom_assign_id = unassignModel.unassignroom_id;
                    asignRoomModel.assignRoomID = unassignModel.unassignroom_room_id;
                    asignRoomModel.assignRoom_HouseKeeper_Id = userListSelected.userListId; 
                    asignRoomModel.assignRoom_post_status = POST_STATUS_SAVED_UNPOSTED;
                    asignRoomModel.assignRoom_supervisor_id = [UserManagerV2 sharedUserManager].currentUser.userId;
                    NSDateFormatter *timeFormartter = [[NSDateFormatter alloc] init];;
                    [timeFormartter setDateFormat:kTimeFormat];
                    if([LogFileManager isLogConsole])
                    {
                        NSLog(@"%@",[timeFormartter stringFromDate:self.selectedDate]);
                    }
                    asignRoomModel.assignRoom_Assign_Date = [timeFormartter stringFromDate:self.selectedDate];
                    [[UnassignManagerV2 sharedUnassignManager] insertAssignRoom:asignRoomModel];
                    
                    if ([[NetworkCheck sharedNetworkCheck] isConnectNetworkSuccessfulNoPopUp] ==YES){
                        [[UnassignManagerV2 sharedUnassignManager] postAssignRoom:asignRoomModel];
                    }
                    
                    isAssigned = YES;
                    
                    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getMsgSaveMessage]];
                    //hide saving data.
                    [self performSelector:@selector(hiddenHUDAfterSaved:) withObject:HUD afterDelay:0.5];

                    
                    [self.navigationController popViewControllerAnimated:YES];                   
                    
                }
                    break;
                    
                default:
                    break;
            }
        }
            break;
           }
}



#pragma didden after save.
-(void) hiddenHUDAfterSaved:(MBProgressHUD *)HUD{
    [HUD hide:YES];
    [HUD removeFromSuperview];
}


#pragma mark
#pragma mark tableView DataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = nil;
    static NSString *cellIdentifier = @"remark";
    
    cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if(cell == nil){
         cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
    }
    
    [nextImageView setFrame:CGRectMake(0, 0, 24, 24)];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    [cell setAccessoryView:nextImageView];

    UILabel *remarkLabel=[[UILabel alloc] initWithFrame:CGRectMake(5,0, 80,30 )];
    remarkLabel.tag=KTAGREMARK;
    [remarkLabel setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_remark_title]];
    [remarkLabel setTextColor:[UIColor colorWithRed:6.0f/255.0f green:62.0f/255.0f blue:127.0f/255.0f alpha:1]];
    [remarkLabel setFont:[UIFont fontWithName:fontTitleLabel size:sizeTitleLabel]];
    [remarkLabel setBackgroundColor:[UIColor clearColor]];                 
    
    UILabel *remarkText=[[UILabel alloc] initWithFrame:CGRectMake(75,-19,130, 60)];
    remarkText.tag=KTAGREMARKTEXT;

    remarkText.numberOfLines = 3;
    [remarkText setBackgroundColor:[UIColor clearColor]];
    
    [cell.contentView addSubview:remarkLabel];
    [cell.contentView addSubview:remarkText];
    
    
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 80.0;
}

#pragma mark 
#pragma mark UserList Picker

-(void) didChoosePickerViewV2WithData:(id) data AndIndexRow:(NSInteger) index{
    
    if ([[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString: (NSString *)ENGLISH_LANGUAGE] == NO) {
        [assignToButton setTitle:((UserListModelV2*)data).userListFullNameLang forState:UIControlStateNormal];  
    } else{
        [assignToButton setTitle:((UserListModelV2*)data).userListFullName forState:UIControlStateNormal];  
    }
    
    //[assignToButton setTitle:((UserListModelV2*)data).userListName forState:UIControlStateNormal];  
    userListSelected = (UserListModelV2*)data;
}


#pragma mark
#pragma mark tableView Delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    RemarkViewV2 *remarkView = [[RemarkViewV2 alloc] initWithNibName:NSStringFromClass([RemarkViewV2 class]) bundle:nil];
    remarkLabelView.text = asignRoomModel.assignRoom_Remark;
    [remarkView setDelegate:self];
    [self.navigationController pushViewController:remarkView animated:YES];

}

-(void)remarkViewV2RoomAssigndidDismissWithButtonIndex:(NSInteger)buttonIndex
{
//    if (buttonIndex == 1) {
//    }
//    else {
//    }
}

- (IBAction)remarkDidSelect:(id)sender{
    RemarkViewV2 *remarkView = [[RemarkViewV2 alloc] initWithNibName:NSStringFromClass([RemarkViewV2 class]) bundle:nil];
    remarkView.textinRemark = asignRoomModel.assignRoom_Remark;
    [remarkView setDelegate:self];
    [self.navigationController pushViewController:remarkView animated:YES];

}

-(void)remarkViewV2RoomAssignDoneWithText:(NSString *)text andController:(UIViewController *)controllerView
{
    remarkLabelView.text = text;
    
    //adjust label topleft
    //[remarkLabelView sizeToFitFixedWidth:193];
    CGRect frame = remarkLabelView.frame;
    if (frame.size.height > 73) {
        frame.size.height = 73;
        [remarkLabelView setFrame:frame];
        [remarkLabelView setLineBreakMode:NSLineBreakByTruncatingTail];
        [remarkLabelView setNumberOfLines:4];
    }
    asignRoomModel.assignRoom_Remark = text;
}


//-(void)remarkViewV2DoneWithText:(NSString *)text andController:(UIViewController *)controllerView{
////    NSIndexPath *indexPath =[NSIndexPath indexPathForRow:0 inSection:0];
////    UITableViewCell *cell = [tableViewRemark cellForRowAtIndexPath:indexPath];
////    UILabel *remarkText =  (UILabel*)[cell.contentView viewWithTag:KTAGREMARKTEXT];
////    [remarkText setText:text];
//    
//    remarkLabelView.text = text;
//    
//    //adjust label topleft
//    [remarkLabelView sizeToFitFixedWidth:193];
//    CGRect frame = remarkLabelView.frame;
//    if (frame.size.height > 73) {
//        frame.size.height = 73;
//        [remarkLabelView setFrame:frame];
//        [remarkLabelView setLineBreakMode:NSLineBreakByTruncatingTail];
//        [remarkLabelView setNumberOfLines:4];
//    }
//
//    
//    asignRoomModel.assignRoom_Remark = text;
//    
//}
//////////////////////////////////////////////////////////////////////

#pragma mark
#pragma mark date picker delegate
- (void)dateTimePickerOK:(DateTimePicker *)controller didPickDate:(NSDate *)date
{
    self.selectedDate = date;
	[self redrawDate];
    
    if(controller.tag == kTagAssignDateTo){
        
    } else{
        
    }
    NSDateFormatter *timeFormartter = [[NSDateFormatter alloc] init];;
    [timeFormartter setDateFormat:kTimeFormat];
    
     asignRoomModel.assignRoom_Assign_Date = [timeFormartter stringFromDate:self.selectedDate];
}

- (void)dateTimePickerCancel:(DateTimePicker *)controller
{
	
}


- (void)viewDidUnload
{
    [self setAssignToTitleLabel:nil];
    [self setAssignDateToTitleLabel:nil];
    [self setAssignTimeAtTitleLabel:nil];
    [self setExpectedCleaningTimeTitleLabel:nil];
    [self setAssignToLabel:nil];
//    [self setAssignDateToLabel:nil];

    [self setExpectedCleaningTimelabel:nil];
    tableViewRemark = nil;
    [self setAssignToButton:nil];
    [self setAssignDateToButton:nil];
    [self setRoomNoButton:nil];
    [self setRoomStatusImage:nil];
    [self setRoomStatusButton:nil];
    [self setGuestNameButton:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void) setCaptionView{
    [self setTitle:[[LanguageManagerV2 sharedLanguageManager] getAssignedRoom]];

    
    NSString *assignTitle = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ASSIGN_ASSIGNTO];
    
    assignToTitleLabel.text =  assignTitle;
    assignDateToTitleLabel.text = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ASSIGN_ASSIGNDATETO]  ;
    assignTimeAtTitleLabel.text =  [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ASSIGN_ASSIGNTIMEAT] ;
    expectedCleaningTimeTitleLabel.text =  [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ASSIGN_EXPECTEDCLEANINGTIME] ;
    
    msgbtnNo = [[LanguageManagerV2 sharedLanguageManager] getNo];
    msgbtnYes = [[LanguageManagerV2 sharedLanguageManager] getYes];
    msgSaveAssign = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_please_confirm_to_assign_the_room];
    
    remarkLabelTitle.text = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_remark_title];
    [_submitLabel setText:[L_SUBMIT currentKeyToLanguage]];
}

#pragma mark - Handle Topbar Methods
-(void)loadTopbarView {
    TopbarViewV2 *topbar = [[TopbarViewV2 alloc] initViewWithSuperModel:[[CommonVariable sharedCommonVariable] superRoomModel]];
    [topbar setTag:tagTopbarView];
    [self.view addSubview:topbar];
    [topbar setHidden:YES];
    
    [topbar setAlpha:0.0];
    [self addButtonHandleShowHideTopbar];
}

-(void)showTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationWillStartSelector:@selector(setHiddenTopbarView)];
    
    f = topbar.frame;
    f.origin.y = 0;
    [topbar setFrame:f];
    [topbar setAlpha:1.0];
    
    [self adjustShowForViews];
    
    [UIView commitAnimations];
}

-(void)hideTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDidStopSelector:@selector(setHiddenTopbarView)];
    [UIView setAnimationDelegate:self];
    
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    [topbar setAlpha:0.0];
    
    [self adjustRemoveForViews];
    
    [UIView commitAnimations];
}

-(void)setHiddenTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    [topbar setHidden:!topbar.hidden];
}

-(TopbarViewV2 *)getTopBarView {
    return (TopbarViewV2 *)[self.view viewWithTag:tagTopbarView];
}

-(void)addButtonHandleShowHideTopbar {
    UIButton *titleBarButton = [[UIButton alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
    
    titleBarButton.titleLabel.backgroundColor = [UIColor clearColor];
    titleBarButton.titleLabel.font = FONT_BUTTON_TOPBAR;
    titleBarButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    titleBarButton.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
    [titleBarButton.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
    [titleBarButton.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
    [titleBarButton addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    [titleBarButton setTitle:[[LanguageManagerV2 sharedLanguageManager] getAssignedRoom] forState:UIControlStateNormal];
    self.navigationItem.titleView = titleBarButton;
    //Move title to center vertical
    CGRect titleViewBounds = self.navigationItem.titleView.bounds;
    int moveLeftTitle = 0;
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        moveLeftTitle = 10;
    }
    self.navigationItem.titleView.bounds = CGRectMake(titleViewBounds.origin.x + moveLeftTitle, Y_TOPBAR_TITLE, titleViewBounds.size.width, titleViewBounds.size.height);
    
    //back button navigation
    self.navigationItem.leftBarButtonItem = [MyNavigationBarV2 createBackButtonWithTarget:self selector:@selector(btnBackPressed) forControlEvents:UIControlEventTouchUpInside];
    
}

-(void)btnBackPressed
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)topbarTapped:(UIButton *)sender {
    TopbarViewV2 *topbar = [self getTopBarView];
    [sender setEnabled:NO];
    if (topbar.isHidden) {
        [self showTopbarView];
    } else {
        [self hideTopbarView];
    }
    [self performSelector:@selector(topbarButtonEnable:) withObject:sender afterDelay:0.5];
}

-(void) topbarButtonEnable:(UIButton *) topbarBtn {
    [topbarBtn setEnabled:YES];
}

-(void)adjustShowForViews {
    //adjust all views when show 
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    CGRect ftbv = vAssignRoom.frame;
    [vAssignRoom setFrame:CGRectMake(0, f.size.height, 320, ftbv.size.height - f.size.height)];
}

-(void)adjustRemoveForViews { 
    //adjust all views when hide
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    CGRect ftbv = vAssignRoom.frame;
    [vAssignRoom setFrame:CGRectMake(0, 0, 320, ftbv.size.height + f.size.height)];
}

- (IBAction)btnSubmit_Clicked:(id)sender
{
    [self saveData];
}

@end
