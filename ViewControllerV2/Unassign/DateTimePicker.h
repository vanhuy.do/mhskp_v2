//
//  DateTimePicker.h
//

#import <UIKit/UIKit.h>
@class DateTimePicker;

@protocol DateTimePickerViewV2Delegate <NSObject>

@optional
- (void)dateTimePickerOK:(DateTimePicker *)controller didPickDate:(NSDate *)date;
- (void)dateTimePickerCancel:(DateTimePicker *)controller;
@end

@interface DateTimePicker : UIView {
    NSArray *datas;
@private UIDatePicker *pickerView;
@private id selectedData;
   __unsafe_unretained id<DateTimePickerViewV2Delegate> delegate;
    NSIndexPath *path;
    NSInteger tIndex;
//    NSDate *defaultDate;
}

@property (nonatomic, assign) NSInteger tIndex;
@property (nonatomic, assign) id<DateTimePickerViewV2Delegate> delegate;
@property (nonatomic, strong) id selectedData;
@property (nonatomic, strong) NSArray *datas;
@property (nonatomic, readonly, strong) NSIndexPath *path;
//@property (nonatomic, strong) NSDate *defaultDate;

-(id)initWithUIDateTimeModel:(UIDatePickerMode)dateTimteModel andDefaultDate:(NSDate*)defaultDate;
-(void) showPickerViewV2;
-(void) hidePickerViewV2;
@end
