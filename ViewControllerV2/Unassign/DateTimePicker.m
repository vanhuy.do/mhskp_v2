//
//  DateTimePicker.m
//

#import "DateTimePicker.h"
#import "eHouseKeepingAppDelegate.h"
#import "DeviceManager.h"

@interface DateTimePicker (PrivateMethods)

-(void)cancelButtonPressed;
-(void)doneButtonPressed;

@end

@implementation DateTimePicker
@synthesize datas, selectedData, delegate, path, tIndex;
//@synthesize defaultDate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

-(id)initWithUIDateTimeModel:(UIDatePickerMode)dateTimteModel andDefaultDate:(NSDate*)defaultDate{
    self = [super init];
    if (self) {
        CGRect f = self.frame;
        f.origin.y = DeviceScreenSizeStandardHeight3_5;
        f.origin.x = 0;
        f.size.width = 320;
        f.size.height = DeviceScreenSizeStandardHeight3_5;
        int moreY = 0;
        int moreHeight = 0;
        if([DeviceManager getDeviceScreenKind] == DeviceScreenKindRetina4_0){
            moreY = 88;
            moreHeight = 82;
            f.origin.y = DeviceScreenSizeStandardHeight4_0;
            f.size.height = DeviceScreenSizeStandardHeight4_0;
        }
        [self setFrame:f];
        
        NSDateFormatter *_timeFomarter = [[NSDateFormatter alloc] init];
        [_timeFomarter setDateFormat:@"yyyy-MM-dd hh:mm:ss a"];
        NSDate *now = [[NSDate alloc] init];
        pickerView = [[UIDatePicker alloc] init];
        [pickerView setFrame:CGRectMake(0, 268 + moreY, 320, 216)];
        [pickerView setDatePickerMode:dateTimteModel];
        [pickerView setDate:defaultDate];
        
        [pickerView setMinimumDate:now];
        
        NSDateComponents *components = [[NSDateComponents alloc] init] ;
        components.month = 1;
        NSDate *oneMonthFromNow = [[NSCalendar currentCalendar] dateByAddingComponents:components toDate:[NSDate date] options:0];
        
        [pickerView setMaximumDate:oneMonthFromNow];
        /*
        NSInteger indexSelect = 0;
        for (NSInteger index = 0; index < datas.count; index++) {
            if ([[datas objectAtIndex:index] isEqual:selectedData]) {
                indexSelect = index;
                break;
            }
        }
        */
        [self addSubview:pickerView];
        
        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
            UIToolbar *blurView = [[UIToolbar alloc] initWithFrame:pickerView.frame];
            blurView.translucent = YES;
            [self insertSubview:blurView belowSubview:pickerView];
        }
        
        UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 224 + moreY, 320, 44)];
        UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_CANCEL] style:UIBarButtonItemStyleBordered target:self action:@selector(cancelButtonPressed)];
        UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_DONE] style:UIBarButtonItemStyleDone target:self action:@selector(doneButtonPressed)];
        UIBarButtonItem *flexiablebar = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        [toolBar setItems:[NSArray arrayWithObjects:cancelButton, flexiablebar, doneButton, nil]];
        [self addSubview:toolBar];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setFrame:CGRectMake(0, 0, 320, 224 + moreHeight)];
        [button addTarget:self action:@selector(hidePickerViewV2) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:button];
        
       
    }
    return self;
}


#pragma mark - UIPickerView Datasource


#pragma mark - BarButton Methods
-(void)cancelButtonPressed {
    [self hidePickerViewV2];
}

-(void)doneButtonPressed {
    [self hidePickerViewV2];
    if (delegate && [delegate respondsToSelector:@selector(dateTimePickerOK:didPickDate:)]) 
	{
		[delegate dateTimePickerOK:self didPickDate:pickerView.date];
	}

}

#pragma mark - Show/Hide PickerViewV2
-(void) removePickerViewV2 {
    [self removeFromSuperview];
}

-(void)hidePickerViewV2 {
    [UIView beginAnimations:@"animationView" context:nil];
    [UIView setAnimationDuration:0.5];
    
    CGRect f = self.frame;
    f.origin.y = 480;
    [self setFrame:f];
    
    [UIView commitAnimations];
    
    [self performSelector:@selector(removePickerViewV2) withObject:nil afterDelay:0.5];
}

-(void)showPickerViewV2 {
    [[[[UIApplication sharedApplication] delegate] window] addSubview:self];
    
    [UIView beginAnimations:@"animationView" context:nil];
    [UIView setAnimationDuration:0.5];
    
    CGRect f = self.frame;
    f.origin.y = 0;
    [self setFrame:f];
    
    [UIView commitAnimations];
}



@end
