//
//  UnassignFloorCellV2.h
//  mHouseKeeping
//
//  Created by Giang Le on 2/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UnassignFloorCellV2 : UITableViewCell{
//    IBOutlet UIButton *disclosureButton;
    UILabel *floorLabel;
    UILabel *numOfRoomLabel;
}


@property (nonatomic,strong ) IBOutlet UILabel *floorLabel;
@property (nonatomic,strong ) IBOutlet UILabel *numOfRoomLabel;

@end
