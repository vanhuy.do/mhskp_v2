//
//  UnassignFloorViewV2.h
//  mHouseKeeping
//
//  Created by Giang Le on 2/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UnassignZoneViewV2.h"
#import "ehkDefines.h"
@interface UnassignFloorViewV2 : UIViewController<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate, MBProgressHUDDelegate>{
    UITableView *tbvContent;
    UITableView *tbvNoResult;
    NSMutableArray *dataSource;

    UISearchBar *searchBar;
    NSMutableArray *listTempData;
    MBProgressHUD *HUD;
    CGRect frameTableView;
}

@property (nonatomic, strong) IBOutlet UITableView *tbvContent;
@property (nonatomic, strong) IBOutlet UITableView *tbvNoResult;
@property (nonatomic, strong) NSMutableArray *dataSource;

@property (nonatomic, strong) IBOutlet UISearchBar *searchBar;

- (NSString *) getBackString;
- (void)setCaptionsView ;
@property (strong, nonatomic) IBOutlet UIView *vUnassignFloor;
@end

