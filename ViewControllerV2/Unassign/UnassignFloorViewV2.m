//
//  UnassignFloorViewV2.m
//  mHouseKeeping
//
//  Created by Giang Le on 2/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UnassignFloorViewV2.h"
#import "UnassignFloorCellV2.h"
#import "MyNavigationBarV2.h"
#import "DeviceManager.h"
@interface UnassignFloorViewV2 (PrivateMethods)

//#define tagTopbarView 12345


#define colorAlpha          1.0
#define colorWhite          255/255.0
#define colorBlueR          105/255.0
#define colorBlueG          218/255.0
#define colorBlueB          222/255.0
#define colorGreenR         77/255.0
#define colorGreenG         211/255.0
#define colorGreenB         100/255.0
#define colorRedR           223/255.0
#define colorRedG           81/255.0
#define colorRedB           59/255.0


-(void) loadTopbarView;
-(void) showTopbarView;
-(void) hideTopbarView;
-(void) setHiddenTopbarView;
-(void) adjustShowForViews;
-(void) adjustRemoveForViews;
-(void) addButtonHandleShowHideTopbar;
-(void) topbarTapped:(UIButton *) sender;
-(TopbarViewV2 *) getTopBarView;
-(void) keyboardWillShow;
-(void) keyboardWillHide;

@end

@implementation UnassignFloorViewV2

#define kFloorValueKey @"kFloorValueKey"
#define kFloorIDKey @"kFloorIDKey"
#define tagNoResult         12345
#define sizeNoResult        22.0f

#define heightTbvContent    44.0f
#define heightTbvNoResult   240.0f
#define font1                @"Arial-BoldMT"

@synthesize tbvContent;
@synthesize dataSource;
@synthesize searchBar;
@synthesize tbvNoResult;
@synthesize vUnassignFloor;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle
-(void)loaddata{
//    if (!HUD) {
//        HUD = [[MBProgressHUD alloc] initWithView:self.view];
//        
//        //HUD.delegate=self;
//    }
//    [self.view addSubview:HUD];
//    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getRaLoadingData]];
//    [HUD show:YES];
//   [[HomeViewV2 shareHomeView] waitingLoadingData]; 
   
    //Hao Tran Enhance for get floor
    NSString *floorLastModified = [[UnassignManagerV2 sharedUnassignManager] getFloorLastModifiedDate];
    [[UnassignManagerV2 sharedUnassignManager] getFloorListFromWSWith:[UserManagerV2 sharedUserManager].currentUser andLastModified:floorLastModified AndPercentView:nil];
    
    [[UnassignManagerV2 sharedUnassignManager] getUnassignRoomListWithUser:[UserManagerV2 sharedUserManager].currentUser
                                   andLastModified:nil];
    
    /**********load data*************/
//    listTempData = [UnassignManagerV2 loadFloors];
    listTempData = [[UnassignManagerV2 sharedUnassignManager] loadFloorsHaveUnAssignRoom];
    dataSource = [[NSMutableArray alloc] initWithArray:listTempData];
    
    
    [tbvContent setBackgroundView:nil];
    //[tbvContent setBackgroundColor:[UIColor clearColor]];
    [tbvContent setOpaque:YES];
    //[self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:imgBackground]]];
    
    
    
    [tbvContent reloadData];
    /********************************/
    
//    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
    [HUD hide:YES];
//    [HUD removeFromSuperview];

}

//- (void) setCaptionView{
//    [self.navigationItem.backBarButtonItem setTitle:[[LanguageManagerV2 sharedLanguageManager]  getStringLanguageByStringId:L_BACK]];
//    
//    
//}
#define tagBackButton 12
- (void)viewDidLoad
{
    [super viewDidLoad];
    //Resize content views of current VC to fit with Navigation bar and tabbar
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    [tbvContent setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [tbvNoResult setSeparatorStyle:UITableViewCellSeparatorStyleNone];

    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_RA_LOADING_DATA]];
    [HUD setDelegate:self];
    [self.view addSubview:HUD];
    [HUD show:YES];
    
    [[HomeViewV2 shareHomeView] waitingLoadingData];
    
    [self performSelectorOnMainThread:@selector(loaddata) withObject:nil waitUntilDone:NO];
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
    
    [self setCaptionsView];
    
    [searchBar setTintColor:[UIColor colorWithRed:colorBlueR green:colorBlueG  blue:colorBlueB alpha:colorAlpha]];
   
    //back button navigation
    self.navigationItem.leftBarButtonItem = [MyNavigationBarV2 createBackButtonWithTarget:self selector:@selector(backBarPressed) forControlEvents:UIControlEventTouchUpInside];
    
    // Do any additional setup after loading the view from its nib.
    [self performSelector:@selector(loadTopbarView)];
    
    frameTableView = tbvContent.frame;
    
}

-(void)backBarPressed{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewDidAppear:(BOOL)animated{
    [self setCaptionsView];
    [super viewDidAppear:animated];
}

-(void)viewWillAppear:(BOOL)animated{
//    if (!HUD) {
//        HUD = [[MBProgressHUD alloc] initWithView:self.view];
//        [self.view addSubview:HUD];
//        //HUD.delegate=self;
//    }
    
    [tbvContent reloadData];
//    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getRaLoadingData]];
//    [HUD show:YES];
    
    [super viewWillAppear:animated];
    [[HomeViewV2 shareHomeView] setSelectedButton:tagOfUnassignButton];
    
    UIView *v= [self.navigationController.view viewWithTag:tagViewWifi];  // add by chinh X.Bui
    [v setHidden:NO];
    [self setCaptionsView];

//    [HUD hide:YES];
//    [HUD removeFromSuperview];

}
#pragma mark table view delegate
-(CGFloat)tableView:(UITableView *)tableView 
heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(tableView == tbvContent)
        return heightTbvContent;
    else if(tableView == tbvNoResult)
        return heightTbvNoResult;
    
    return 0;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  
    if(tableView == tbvContent)
        return [dataSource count];
    
    return 1;

}



-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *IdentifyCell = @"IdentifyCell";
    
    static NSString *cellIdentify = @"cellIdentify";
    static NSString *cellNoIndentify = @"cellNoIndentify";
    
    UITableViewCell *cell = nil;
    
    if(tableView == tbvNoResult) {
        cell = [tableView dequeueReusableCellWithIdentifier:cellNoIndentify];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentify] ;
            
            UILabel *titleNoResult = [[UILabel alloc] 
                                      initWithFrame:CGRectMake(0, 0, 320, 240)];
            [titleNoResult setTextColor:[UIColor grayColor]];
            [titleNoResult setBackgroundColor:[UIColor clearColor]];
            [titleNoResult setTextAlignment:NSTextAlignmentCenter];
            [titleNoResult setTag:tagNoResult];
            [titleNoResult setFont:[UIFont fontWithName:font1 size:sizeNoResult]];
            
            [cell.contentView addSubview:titleNoResult];
   
        }
        
        UILabel *lblNoResult = (UILabel *)[cell viewWithTag:tagNoResult];
        [lblNoResult setText:[[LanguageManagerV2 sharedLanguageManager] getNoResultsFound]];
        
        return cell;
    }
    
    
    
    if(tableView == tbvContent){
    UnassignFloorCellV2 *cell = nil;
    cell = [tableView dequeueReusableCellWithIdentifier:IdentifyCell];
    if(cell == nil){
        NSArray *array = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([UnassignFloorCellV2 class]) owner:self options:nil];
        cell = [array objectAtIndex:0];
    }
    
    FloorModelV2 *floorModel = (FloorModelV2*)[dataSource objectAtIndex:indexPath.row];
    
    cell.floorLabel.text = [NSString stringWithFormat:@"%@ %@",[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ios_unassign_floor], floorModel.floor_name];
    
    int numberOfUnAssignRoom = (int)[[UnassignManagerV2 sharedUnassignManager] numberOfUnAssignRoomOnFloor:floorModel.floor_id];
    cell.numOfRoomLabel.text = [NSString stringWithFormat:@"(%d)", numberOfUnAssignRoom];
        
    //[cell setSelectionStyle:UITableViewCellSelectionStyleNone ] ;
    [cell.contentView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"big_bt_640x111.png"]]];
    [cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
        
        return cell;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView == tbvNoResult) return;
    
    if([dataSource count] > 0)
    {
        UnassignZoneViewV2 *unassignZoneView = [[UnassignZoneViewV2 alloc] initWithNibName:NSStringFromClass([UnassignZoneViewV2 class]) bundle:nil];
        
        
        FloorModelV2 *floorModel = (FloorModelV2*)[dataSource objectAtIndex:indexPath.row];
        unassignZoneView.floorID = floorModel.floor_id;
        
        UIBarButtonItem *BackButtonRoomInfo = [[UIBarButtonItem alloc] initWithTitle: [self getBackString] style: UIBarButtonItemStyleBordered target: nil action: nil];
        [[self navigationItem] setBackBarButtonItem: BackButtonRoomInfo];
        [self.navigationItem.backBarButtonItem setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK]];
        
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        [cell setSelected:NO animated:NO];
        [self.navigationController pushViewController:unassignZoneView animated:YES];
    }
    

}


- (void)viewDidUnload
{
    [self setSearchBar:nil];
    [self setSearchBar:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}



-(NSString *) getBackString {
    NSDictionary *dic = [[LanguageManagerV2 sharedLanguageManager] getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_BACK]];
    return s;
}

#pragma mark - UISearchBar Delegate Methods

-(NSString *) getCancelString {
    NSDictionary *dic = [[LanguageManagerV2 sharedLanguageManager] getCurrentDictionaryLanguage];
    return [dic valueForKey:[NSString stringWithFormat:@"%@", L_CANCEL]];
}
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBarA {
    [searchBarA setShowsCancelButton:NO animated:NO];
    [searchBarA resignFirstResponder];
     [tbvContent setFrame:CGRectMake(0, 44, 320,350)];
   
}

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBarA {    
    [searchBarA setShowsCancelButton:YES animated:NO];

    for(UIView *subView in searchBarA.subviews){
        if([subView isKindOfClass:UIButton.class])
        {
            UIButton *cancelButton = (UIButton*)subView;
//            [cancelButton setBackgroundColor:[UIColor whiteColor]];
            [cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [cancelButton setTintColor:COLOR_CANCEL_SEARCH_BAR];
            
            [cancelButton setTitle:[self getCancelString] forState:UIControlStateNormal];
            
        }
    }

    

}


-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBarA {
    [searchBarA setShowsCancelButton:NO animated:NO];
    [searchBarA resignFirstResponder];
    

    searchBarA.text = @"";
    [self searchBar:searchBarA textDidChange:@""];
    [tbvContent setFrame:frameTableView];
}


-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {    
    [tbvContent setFrame:CGRectMake(0, 44, 320,210)];
    
    if ([searchText isEqualToString:@""] || [searchText isEqual:nil]) {
        [dataSource removeAllObjects];
        [dataSource addObjectsFromArray:listTempData];
        [tbvNoResult setHidden:YES];
        [tbvContent setHidden:NO];
    } else {
        [dataSource removeAllObjects];
        
        for (FloorModelV2 *rowData in listTempData) { 
            
            NSString *floorName = rowData.floor_name;
            NSRange floor = [floorName rangeOfString:searchText 
                                      options:NSCaseInsensitiveSearch];
            
            
            if(floor.location!=NSNotFound) {
                [dataSource addObject:rowData];
                [tbvNoResult setHidden:YES];
                [tbvContent setHidden:NO];
            } 
        }        
    }
    
    if([dataSource count] <= 0) {
        [tbvContent setHidden:YES];
        [tbvNoResult setHidden:NO];
    }
    
    if([dataSource count]>1) {
        for(int i = 0; i < [dataSource count]; i++) {
            for(int j = i + 1; j < [dataSource count] ; j++) {            
                FloorModelV2 *floorItem1 = (FloorModelV2*)[dataSource  objectAtIndex:i];
                NSString *floorname1 = floorItem1.floor_name;
                
                FloorModelV2 *floorItem2 = (FloorModelV2*)[dataSource  objectAtIndex:j];
                NSString *floorname2 = floorItem2.floor_name;
                
                if([floorname1 isEqualToString:floorname2]) {
                    [dataSource removeObjectAtIndex:j];
                }
            }
        }
    }
    
    [tbvContent reloadData];
   
}


#pragma mark
#pragma mark Set Caption
-(void)setCaptionsView {
    
    UIButton *buttonTitle = (UIButton *)self.navigationItem.titleView;
    [buttonTitle setTitle:[[LanguageManagerV2 sharedLanguageManager] getUnassignedRoom] forState:UIControlStateNormal];

    
    
    [self setTitle:[[LanguageManagerV2 sharedLanguageManager] getUnassignedRoom]];
    searchBar.placeholder = [[LanguageManagerV2 sharedLanguageManager] getSearch];
    
    for(UIView *subView in searchBar.subviews){
        if([subView isKindOfClass:UIButton.class])
        {
            UIButton *cancelButton = (UIButton*)subView;
            //[cancelButton setBackgroundColor:[UIColor whiteColor]];
            [cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [cancelButton setTitle:[self getCancelString] forState:UIControlStateNormal];
            
        }
    }

    self.navigationItem.leftBarButtonItem.title = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK];
    
    UIButton *button = (UIButton*)[self.navigationItem.leftBarButtonItem.customView viewWithTag:tagBackButton ];
    if(button != nil){
        [button setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK] forState:UIControlStateNormal];
    }
    
}

#pragma mark - Handle Topbar Methods
-(void)loadTopbarView {
    TopbarViewV2 *topbar = [[TopbarViewV2 alloc] initViewWithSuperModel:[[CommonVariable sharedCommonVariable] superRoomModel]];
    [topbar setTag:tagTopbarView];
    [self.view addSubview:topbar];
    [topbar setHidden:YES];
    
    [topbar setAlpha:0.0];
    [self addButtonHandleShowHideTopbar];
}

-(void)showTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationWillStartSelector:@selector(setHiddenTopbarView)];
    
    f = topbar.frame;
    f.origin.y = 0;
    [topbar setFrame:f];
    [topbar setAlpha:1.0];
    
    [self adjustShowForViews];
    
    [UIView commitAnimations];
}

-(void)hideTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDidStopSelector:@selector(setHiddenTopbarView)];
    [UIView setAnimationDelegate:self];
    
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    [topbar setAlpha:0.0];
    
    [self adjustRemoveForViews];
    
    [UIView commitAnimations];
}

-(void)setHiddenTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    [topbar setHidden:!topbar.hidden];
}

-(TopbarViewV2 *)getTopBarView {
    return (TopbarViewV2 *)[self.view viewWithTag:tagTopbarView];
}

-(void)addButtonHandleShowHideTopbar {
    NSDictionary *dic = [[LanguageManagerV2 sharedLanguageManager] getCurrentDictionaryLanguage];
    
    UIButton *titleBarButton = [[UIButton alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
    titleBarButton.bounds = CGRectOffset(titleBarButton.bounds, -10, 0);
    titleBarButton.titleLabel.backgroundColor = [UIColor clearColor];
    titleBarButton.titleLabel.font = FONT_BUTTON_TOPBAR;
    titleBarButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    titleBarButton.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
    [titleBarButton.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
    [titleBarButton.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
    [titleBarButton addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    [titleBarButton setTitle:[dic valueForKey:[NSString stringWithFormat:@"%@", L_UNASSIGNED_ROOM]] forState:UIControlStateNormal];
    
    if (titleBarButton.titleLabel.text.length >= 14) {
        titleBarButton.titleLabel.font = FONT_SMALL_BUTTON_TOPBAR;
    }
    
    self.navigationItem.titleView = titleBarButton;
    //Move title to center vertical
    CGRect titleViewBounds = self.navigationItem.titleView.bounds;
    int moveLeftTitle = 0;
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        moveLeftTitle = 10;
    }
    self.navigationItem.titleView.bounds = CGRectMake(titleViewBounds.origin.x + moveLeftTitle, Y_TOPBAR_TITLE, titleViewBounds.size.width, titleViewBounds.size.height);
    
}

-(void)topbarTapped:(UIButton *)sender {
    TopbarViewV2 *topbar = [self getTopBarView];
    [sender setEnabled:NO];
    if (topbar.isHidden) {
        [self showTopbarView];
    } else {
        [self hideTopbarView];
    }
    [self performSelector:@selector(topbarButtonEnable:) withObject:sender afterDelay:0.5];
}

-(void) topbarButtonEnable:(UIButton *) topbarBtn {
    [topbarBtn setEnabled:YES];
}

-(void)adjustShowForViews {
    //adjust all views when show 
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    CGRect ftbv = vUnassignFloor.frame;
    [vUnassignFloor setFrame:CGRectMake(0, f.size.height, 320, ftbv.size.height - f.size.height)];
}

-(void)adjustRemoveForViews { 
    //adjust all views when hide
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    CGRect ftbv = vUnassignFloor.frame;
    [vUnassignFloor setFrame:CGRectMake(0, 0, 320, ftbv.size.height + f.size.height)];
}



@end
