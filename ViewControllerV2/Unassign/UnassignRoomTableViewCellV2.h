//
//  UnassignRoomTableViewCellV2.h
//  mHouseKeeping
//
//  Created by Giang Le on 2/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol UnassignRoomTableViewCellV2Delegate <NSObject>
@optional
- (void) btnAddRoomPressedWithIndexPath:(NSIndexPath *)indexpath
                                          AndSender:(UIButton *) sender;

@end

@interface UnassignRoomTableViewCellV2 : UITableViewCell{
   __unsafe_unretained  id<UnassignRoomTableViewCellV2Delegate> delegate;
}
@property (assign, nonatomic) id<UnassignRoomTableViewCellV2Delegate> delegate;

@property (strong, nonatomic) NSIndexPath *indexpath;

@property (strong, nonatomic) IBOutlet UILabel *roomNoLabel;
@property (strong, nonatomic) IBOutlet UILabel *vipStatusLabel;
@property (strong, nonatomic) IBOutlet UIButton *addRoomButton;
@property (strong, nonatomic) IBOutlet UILabel *roomStatusLabel;
@property (strong, nonatomic) IBOutlet UILabel *guestLabel;

@property (strong, nonatomic)  UILabel *roomNoTitleLabel;
@property (strong, nonatomic)  UILabel *vipStatusTitleLabel;
@property (strong, nonatomic)  UILabel *addRoomTitleLabel;
@property (strong, nonatomic)  UILabel *roomStatusTitleLabel;

-(void)addLabel:(NSIndexPath*)_indexPath;

- (IBAction)btnAddRoomPressed:(id)sender;

@end
