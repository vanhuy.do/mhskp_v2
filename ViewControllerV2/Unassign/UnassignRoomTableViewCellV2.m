//
//  UnassignRoomTableViewCellV2.m
//  mHouseKeeping
//
//  Created by Giang Le on 2/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UnassignRoomTableViewCellV2.h"

@implementation UnassignRoomTableViewCellV2
@synthesize roomNoLabel;
@synthesize vipStatusLabel;
@synthesize addRoomButton;
@synthesize roomStatusLabel;
@synthesize indexpath;


@synthesize roomNoTitleLabel;
@synthesize vipStatusTitleLabel;
@synthesize addRoomTitleLabel;
@synthesize roomStatusTitleLabel;
@synthesize guestLabel;

@synthesize delegate;


#define kImageColorLabel @"tab_small.png"
#define fontTitleBold                @"Arial-BoldMT"

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

#define y -30
#define widthLabel 100
-(void)addLabel:(NSIndexPath*)_indexPath{
    if(_indexPath.row==0){
        roomNoTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(roomNoLabel.frame.origin.x,y, widthLabel, roomNoLabel.frame.size.height)];
        
        vipStatusTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(vipStatusLabel.frame.origin.x,y, widthLabel, roomNoLabel.frame.size.height)];
        
        addRoomTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(addRoomButton.frame.origin.x,y, widthLabel, roomNoLabel.frame.size.height)];
        
        roomStatusTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(roomStatusLabel.frame.origin.x,y, widthLabel, roomNoLabel.frame.size.height)];
        
        [roomNoTitleLabel setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ROOM_NO]];
        [vipStatusTitleLabel  setText:[[LanguageManagerV2 sharedLanguageManager] getVIP]];
        [addRoomTitleLabel setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_add]];
        [roomStatusTitleLabel setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_RM_STATUS]];
        
        UIColor *color = roomNoLabel.textColor;
        
        [roomNoTitleLabel setBackgroundColor:[UIColor clearColor]];
        [vipStatusTitleLabel setBackgroundColor:[UIColor clearColor]];
        [addRoomTitleLabel setBackgroundColor:[UIColor clearColor]];
        [roomStatusTitleLabel  setBackgroundColor:[UIColor clearColor]];
        
        UIImage *img=[UIImage imageNamed:kImageColorLabel];
       
        
        [roomNoLabel setTextColor:color];
        [vipStatusLabel setTextColor:color];
        [addRoomButton setTitleColor:[UIColor colorWithPatternImage:img] forState:UIControlStateNormal];
        [roomStatusLabel setTextColor:color];
        
        
        [roomNoTitleLabel setFont:[UIFont fontWithName:fontTitleBold size:15]];
        [vipStatusTitleLabel setFont:[UIFont fontWithName:fontTitleBold size:15]];
        [addRoomTitleLabel setFont:[UIFont fontWithName:fontTitleBold size:15]];
        [roomStatusTitleLabel setFont:[UIFont fontWithName:fontTitleBold size:15]];
        
        
        
        [roomNoTitleLabel setTextColor:color];
        [vipStatusTitleLabel setTextColor:color];
        [addRoomTitleLabel setTextColor:color];
        [roomStatusTitleLabel setTextColor:color];
        
        
        [roomStatusTitleLabel setTextAlignment:NSTextAlignmentCenter];
        [self addSubview:roomNoTitleLabel];
        [self addSubview:vipStatusTitleLabel];
        [self addSubview:addRoomTitleLabel];
        [self addSubview:roomStatusTitleLabel];
        
    }
}

- (IBAction)btnAddRoomPressed:(id)sender{
    if ([delegate respondsToSelector:@selector(btnAddRoomPressedWithIndexPath:AndSender:)]) {
        [delegate btnAddRoomPressedWithIndexPath:indexpath AndSender:sender];
    }

}
@end
