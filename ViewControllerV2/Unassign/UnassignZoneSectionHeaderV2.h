//
//  UnassignZoneSectionHeaderV2.h
//  mHouseKeeping
//
//  Created by Giang Le on 2/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol UnassignZoneSectionHeaderV2Delegate;

@interface UnassignZoneSectionHeaderV2 : UIView{
    UILabel *lblZoneName;
    UIImageView *imgBackgroundView;
    UIImageView *imgbgview;
    UIImageView *imgArrowSection;
    __unsafe_unretained id<UnassignZoneSectionHeaderV2Delegate> delegate;
    UIButton *btnToggle;
    BOOL isToggle;
    NSInteger section;
    

}


@property (nonatomic,strong) UIImageView *imgBackgroundView;
@property(nonatomic,strong)UILabel *lblZoneName;
@property(nonatomic, strong)UILabel *lblNumbOfRoom;
@property(nonatomic,strong)UIImageView *imgbgview;
@property(nonatomic,strong)UIImageView *imgArrowSection;
@property (nonatomic,assign)id<UnassignZoneSectionHeaderV2Delegate> delegate;
@property(nonatomic,strong)UIButton *btnToggle;
@property BOOL isToggle;
@property NSInteger section;
-(void)toggleOpenWithUserAction:(BOOL)userAction ;

-(id)initWithSection:(NSInteger)sectionIndex unassignZoneId:(NSInteger)zoneID unassignZoneName: (NSString*)zoneName AndStatusArrow:(BOOL)isOpened;
//-(id)initWithSection:(NSInteger)sectionIndex unassignZoneId:(NSInteger)zoneID unassignZoneName: (NSString*)zoneName AndStatusArrow:(BOOL)isOpened AndNumOfRow:(NSInteger)num;
@end
@protocol UnassignZoneSectionHeaderV2Delegate<NSObject>
@optional
-(void) sectionHeaderView:(UnassignZoneSectionHeaderV2*) sectionheaderView sectionOpened:(NSInteger) section;
-(void) sectionHeaderView:(UnassignZoneSectionHeaderV2*) sectionheaderView sectionClosed:(NSInteger) section;

@end
