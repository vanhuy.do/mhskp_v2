//
//  UnassignZoneSectionHeaderV2.m
//  mHouseKeeping
//
//  Created by Giang Le on 2/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UnassignZoneSectionHeaderV2.h"
#import "UnassignManagerV2.h"

#define BACKGROUNDIMAGE @"unassigned_room_zone_bt.png"

@implementation UnassignZoneSectionHeaderV2
@synthesize lblZoneName,isToggle,section,imgArrowSection,btnToggle,delegate,imgBackgroundView,imgbgview;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setFrame:CGRectMake(0, 0, 320, 60)];
        imgbgview=[[UIImageView alloc] initWithFrame:CGRectMake(0,0,320,60)];
        [imgbgview setImage:[UIImage imageNamed:BACKGROUNDIMAGE]];
        
        lblZoneName = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 196, 39)];
        [lblZoneName setBackgroundColor:[UIColor clearColor]];
        [lblZoneName setTextColor:[UIColor whiteColor]];
        [lblZoneName setFont:[UIFont fontWithName:@"Arial-BoldMT" size:17]];
        
        if(isToggle == YES)
        {
            imgArrowSection = [[UIImageView alloc]initWithImage:[UIImage imageNamed:imgRightRowOpen]];
        }
        else
        {
            imgArrowSection = [[UIImageView alloc]initWithImage:[UIImage imageNamed:imgRightRow]];
        }
        
        [imgArrowSection setFrame:CGRectMake(288, 12, 25, 25)];
        btnToggle = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnToggle setFrame:CGRectMake(0, 0, 320, 50)];
        [btnToggle addTarget:self action:@selector(toggleOpen:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:imgbgview];
        [self addSubview:lblZoneName];
        [self addSubview:imgArrowSection];
        [self addSubview:btnToggle];
    }
    
    return self;
}

-(void) expandCell{
    if(isToggle == YES)
    {
        [self setFrame:CGRectMake(0, 0, 320, 60)];
    }
    else
    {
          [self setFrame:CGRectMake(0, 0, 320, 70)];
    }
    
}
/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */
-(void)toggleOpen:(id)sender {
    [self toggleOpenWithUserAction:YES];
}

-(void)toggleOpenWithUserAction:(BOOL)userAction {
    isToggle = !isToggle;
    if (userAction) {
        if (isToggle ) {
            if ([delegate respondsToSelector:@selector(sectionHeaderView:sectionOpened:)]) {
//                isToggle = YES;
                [delegate sectionHeaderView:self sectionOpened:section];
                
                [imgArrowSection setImage:[UIImage imageNamed:imgRightRowOpen]];
//                NSLog(@"imgRightRowOpen");
            }
        }else {
            if ([delegate respondsToSelector:@selector(sectionHeaderView:sectionClosed:)]) {
//                isToggle = NO;
                [imgArrowSection setImage:[UIImage imageNamed:imgRightRow]];
                [delegate sectionHeaderView:self sectionClosed:section];
//                NSLog(@"imgRightRow");
            }
        }
    } else {
        if(isToggle){
            [imgArrowSection setImage:[UIImage imageNamed:imgRightRowOpen]];
        } else {
            [imgArrowSection setImage:[UIImage imageNamed:imgRightRow]];
        }
    }
}

-(id)initWithSection:(NSInteger)sectionIndex unassignZoneId:(NSInteger)zoneID unassignZoneName: (NSString*)zoneName AndStatusArrow:(BOOL)isOpened{
    self = [self init];
    if (self) {
        self.section = sectionIndex;
//        self.lblZoneName.text = zoneName;
        int numOfRoom = (int)[[UnassignManagerV2 sharedUnassignManager] numberOfUnAssignRoomOnZone:zoneID];
        self.lblZoneName.text = [NSString stringWithFormat:@"%@   (%d)", zoneName, numOfRoom];
        self.isToggle = isOpened;
    }
    return self;
}

//-(id)initWithSection:(NSInteger)sectionIndex unassignZoneId:(NSInteger)zoneID unassignZoneName: (NSString*)zoneName AndStatusArrow:(BOOL)isOpened AndNumOfRow:(NSInteger)num
//{
//    self = [self init];
//    if (self) {
//        self.section = sectionIndex;
//        self.lblZoneName.text = zoneName;
//        self.lblNumbOfRoom.text = [NSString stringWithFormat:@"(%d)", num];
//        self.isToggle = isOpened;
//    }
//    return self;
//}

-(id)initWithSection:(NSInteger)sectionIndex categoryName:(NSString *)catName categoryImage: (NSData *)catImage AndStatusArrow:(BOOL)isOpened {
    self = [self init];
    if (self) {
        self.section = sectionIndex;
        self.lblZoneName.text = catName;
//        [self.imgCategory setImage:[UIImage imageWithData:catImage]];
        self.isToggle = isOpened;
    }
    return self;
}

@end
