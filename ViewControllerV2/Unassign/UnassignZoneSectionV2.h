//
//  UnassignZoneSectionV2.h
//  mHouseKeeping
//
//  Created by Giang Le on 2/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UnassignZoneSectionHeaderV2.h"
#import "UnassignZoneModelV2.h"
#import "UnassignRoomModelV2.h"

@interface UnassignZoneSectionV2 : NSObject

@property (nonatomic, strong) UnassignZoneModelV2 *chkContentType;
@property  BOOL open;
@property (nonatomic,strong) UnassignZoneSectionHeaderV2* headerView;

@property (nonatomic,strong) NSMutableArray *rowHeights;

- (NSUInteger)countOfRowHeights;
- (id)objectInRowHeightsAtIndex:(NSUInteger)idx;
- (void)insertObject:(id)anObject inRowHeightsAtIndex:(NSUInteger)idx;
- (void)removeObjectFromRowHeightsAtIndex:(NSUInteger)idx;
- (void)replaceObjectInRowHeightsAtIndex:(NSUInteger)idx withObject:(id)anObject;
- (void)getRowHeights: (id __unsafe_unretained [])buffer range:(NSRange)inRange ;

- (void)insertRowHeights:(NSArray *)rowHeightArray atIndexes:(NSIndexSet *)indexes;
- (void)removeRowHeightsAtIndexes:(NSIndexSet *)indexes;
- (void)replaceRowHeightsAtIndexes:(NSIndexSet *)indexes withRowHeights:(NSArray *)rowHeightArray;
-(void) insertObjectToNextIndex:(id)anObject;

@end
