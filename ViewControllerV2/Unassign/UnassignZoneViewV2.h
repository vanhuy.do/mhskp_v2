//
//  UnassignZoneViewV2.h
//  mHouseKeeping
//
//  Created by Giang Le on 2/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UnassignZoneSectionHeaderV2.h"
#import "UnassignZoneSectionV2.h"
#import "UnassignRoomTableViewCellV2.h"
#import "AssignRoomV2.h"
#import "PickerViewV2.h"
#import "MBProgressHUD.h"
//#import "UnassignManagerV2.h"
//#import "RoomStatusModelV2.h"
//#import "RoomManagerV2.h"



@interface UnassignZoneViewV2 : UIViewController<UnassignZoneSectionHeaderV2Delegate,UnassignRoomTableViewCellV2Delegate,UISearchBarDelegate,PickerViewV2Delegate,MBProgressHUDDelegate>{
    NSInteger floorID;
    
    UITableView *tbvNoResult;
    UITableView *tblContent;
    UITableView *tblListRoom;
    
    NSMutableArray *sectionInfoArray;
    NSMutableArray *sectionInfoArrayListRoom;
    NSMutableArray *listTempData;
    NSMutableArray *sectionIndexListRoom;
    NSInteger currentIndexInSectionInfo;
    NSInteger openSectionIndex;
    IBOutlet UIButton *btnSearchCondition;
    IBOutlet UIButton *btnTitle;
    IBOutlet UISearchBar *searchBar;
    enum ENUM_SEARCH_TYPE searchStatus;
    UIImage *img;
    UILabel *bgLabel;
    FloorModelV2 *floorModel;
    BOOL isFirstTime;
    MBProgressHUD *HUD;
    AssignRoomV2 *assignRoomView;
    UIToolbar *bgToolBar;
    
    BOOL isRoomNo;//Add
}
@property (nonatomic, strong) IBOutlet UILabel *bgLabel;

@property (nonatomic,strong) NSMutableArray *sectionInfoArray;
@property (nonatomic,strong) NSMutableArray *sectionInfoArrayListRoom;
@property (nonatomic,strong) NSMutableArray *listAllRooms;
@property (nonatomic) NSInteger floorID;
@property (nonatomic, strong) IBOutlet UITableView *tbvNoResult;
@property (nonatomic,strong) IBOutlet UITableView *tblContent;
@property (nonatomic,strong) IBOutlet UITableView *tblListRoom;
@property (nonatomic) NSInteger openSectionIndex;
@property (nonatomic, strong) IBOutlet UIButton *btnSearchCondition;
@property (nonatomic, strong) IBOutlet UISearchBar *searchBar;
@property (nonatomic) enum ENUM_SEARCH_TYPE searchStatus;
@property (nonatomic,strong) IBOutlet UIToolbar *bgToolBar;

@property (strong, nonatomic) IBOutlet UIView *vUnassignZone;
@property (strong, nonatomic) IBOutlet UIButton *topBackGround;
- (IBAction)filterCondition:(id)sender;

- (void) setCaptionView;
- (NSString *) getBackString;
-(NSString *) getCancelString;
@end
