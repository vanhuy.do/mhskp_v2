//
//  UnassignZoneViewV2.m
//  mHouseKeeping
//
//  Created by Giang Le on 2/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UnassignZoneViewV2.h"
#import "MyNavigationBarV2.h"
#import "LogFileManager.h"
#import "DeviceManager.h"

@interface UnassignZoneViewV2 (PrivateMethods)

#define tagTopbarView 1234

-(void) loadTopbarView;
-(void) showTopbarView;
-(void) hideTopbarView;
-(void) setHiddenTopbarView;
-(void) adjustShowForViews;
-(void) adjustRemoveForViews;
-(void) addButtonHandleShowHideTopbar;
-(void) topbarTapped:(UIButton *) sender;
-(TopbarViewV2 *) getTopBarView;
-(void) keyboardWillShow;
-(void) keyboardWillHide;
-(BOOL)isSearching;
@end

#define colorAlpha          1.0
#define colorWhite          255/255.0
#define colorBlueR          105/255.0
#define colorBlueG          218/255.0
#define colorBlueB          222/255.0
#define colorGreenR         77/255.0
#define colorGreenG         211/255.0
#define colorGreenB         100/255.0
#define colorRedR           223/255.0
#define colorRedG           81/255.0
#define colorRedB           59/255.0
#define fontTitleBold                @"Arial-BoldMT"

#define defaultTime @"1900-01-01"

@implementation UnassignZoneViewV2
@synthesize floorID;
@synthesize tbvNoResult;
@synthesize tblContent;
@synthesize tblListRoom;
@synthesize bgLabel;
@synthesize sectionInfoArray;
@synthesize sectionInfoArrayListRoom;

@synthesize openSectionIndex;
@synthesize btnSearchCondition;
@synthesize searchBar;
@synthesize searchStatus;
@synthesize vUnassignZone;
@synthesize topBackGround;
@synthesize bgToolBar;
@synthesize listAllRooms;

//#define roomNumber @"Room No."
//#define roomStatus @"Room Status"
//#define vipCode @"VIP Code"

#define headerFontSize 17
#define buttonFontSize 13
#define heightTableViewNoResult 240.0f

#define tagNoResult 300
#define kImageColorLabel @"tab_small.png"
#define LastModified @"LastModified"
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

-(void)loadData{
    
}

- (void)loadingData {
    
  isFirstTime = YES;
    // get unassign 
    
    NSDateFormatter *_timeFomarter = [[NSDateFormatter alloc] init];
   [_timeFomarter setDateFormat:@"yyyy-MM-dd"];
    NSDate *now = [[NSDate alloc] init];   
    
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *lastModifiedString = [prefs stringForKey:[NSString stringWithFormat:@"%@%d",LastModified,[UserManagerV2 sharedUserManager].currentUser.userId]];  
    if([LogFileManager isLogConsole]) {
        NSLog(@"before %@",lastModifiedString);
    }
    if([lastModifiedString isEqualToString:@""] || lastModifiedString == nil){
        [prefs setValue:defaultTime forKey:[NSString stringWithFormat:@"%@%d",LastModified,[UserManagerV2 sharedUserManager].currentUser.userId]];

    }
    
    lastModifiedString = [prefs stringForKey:[NSString stringWithFormat:@"%@%d",LastModified,[UserManagerV2 sharedUserManager].currentUser.userId]];
    if([LogFileManager isLogConsole]) {
        NSLog(@"before  call webservice %@",lastModifiedString);
    }
    
    [[UnassignManagerV2 sharedUnassignManager] getUnassignRoomListWithUser:[UserManagerV2 sharedUserManager].currentUser
                                   andLastModified:lastModifiedString];
    
    //Hao Tran Enhance with last modified date
    NSString *zoneLastModified = [[UnassignManagerV2 sharedUnassignManager] getZoneLastModifiedDate];
    [[UnassignManagerV2 sharedUnassignManager] getZoneRoomListWithUser:[UserManagerV2 sharedUserManager].currentUser
                               andlastModified:zoneLastModified AndPercentView:nil];
    
    [prefs setValue:[_timeFomarter stringFromDate:now] forKey:[NSString stringWithFormat:@"%@%d",LastModified,[UserManagerV2 sharedUserManager].currentUser.userId]];

   
    lastModifiedString = [prefs stringForKey:[NSString stringWithFormat:@"%@%d",LastModified,[UserManagerV2 sharedUserManager].currentUser.userId]];
    if([LogFileManager isLogConsole]) {
        NSLog(@"after call webservice %@",lastModifiedString);
    }
    
    ZoneModelV2 *zoneModel = [[ZoneModelV2 alloc] init];
    zoneModel.zone_floor_id = floorID;
    NSMutableArray *zoneList = [[UnassignManagerV2 sharedUnassignManager] loadZonesByFloorId:zoneModel];
    
    sectionInfoArray=[[NSMutableArray alloc] init];
    sectionInfoArrayListRoom = [[NSMutableArray alloc] init];
    listTempData = [[NSMutableArray alloc] init];
    sectionIndexListRoom = [[NSMutableArray alloc] init];
    listAllRooms = [[NSMutableArray alloc] init];
    currentIndexInSectionInfo = 0;
    
    for (int index = 0; index<[zoneList count]; index++) {
        ZoneModelV2* zoneItem = (ZoneModelV2*)[zoneList objectAtIndex:index];
        
        // load room on earch item
        NSMutableArray *roomList = [[UnassignManagerV2 sharedUnassignManager] loadRoombyZoneId:zoneItem];
        
        UnassignZoneSectionV2 *unassignSection=[[UnassignZoneSectionV2 alloc] init];
        UnassignZoneModelV2 *unassignZone=[[UnassignZoneModelV2 alloc] init];
        
        
        if([roomList count]){
            // assign zone :
            unassignZone.unAssignZoneId = zoneItem.zone_id;
            unassignZone.unAssignZone_Floor_Id = zoneItem.zone_floor_id;
            unassignZone.unAssignZoneName = [zoneItem.zone_name uppercaseString];
            unassignZone.unAssignZoneName_Lang = zoneItem.zone_name_lang;
            unassignZone.unAssignZoneLast_Modified = zoneItem.zone_last_modified;
            unassignSection.chkContentType = unassignZone;
        }        
        
        for (int indexRoom=0; indexRoom < [roomList count]; indexRoom++) {
            UnassignModelV2 *unassignRoomModelItem = (UnassignModelV2*)[roomList objectAtIndex:indexRoom];
            [unassignSection insertObjectToNextIndex:unassignRoomModelItem];
            
            [listAllRooms addObject:unassignRoomModelItem];
            [sectionIndexListRoom addObject:[NSString stringWithFormat:@"%d,%d",index,indexRoom]];
        }
        
        if([roomList count]){
         [listTempData addObject:unassignSection];    
        }
        roomList = nil;
        
    }
    
    sectionInfoArray = [[NSMutableArray alloc] initWithArray:listTempData];
    sectionInfoArrayListRoom = [[NSMutableArray alloc] initWithArray:listTempData];
    [tblContent reloadData];
    
    [HUD hide:YES];

}
- (void)viewDidLoad
{
    [super viewDidLoad];
    //Resize content views of current VC to fit with Navigation bar and tabbar
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    //default show all rooms on load
    isRoomNo = YES;
    [tblContent setHidden:NO];
    [tblListRoom setHidden:YES];
    
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_RA_LOADING_DATA]];
    [HUD setDelegate:self];
    [self.view addSubview:HUD];
    [HUD show:YES];
    
    [[HomeViewV2 shareHomeView] waitingLoadingData];
    
    [self performSelectorOnMainThread:@selector(loadingData) withObject:nil waitUntilDone:NO];
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
    
    [btnSearchCondition.titleLabel setFont:[UIFont fontWithName:fontName size:buttonFontSize]];
    [btnSearchCondition setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_room_no_unassign_room] forState:UIControlStateNormal];
    
    self.searchStatus = ENUM_SEARCH_ROOM_NUMBER;

    [tbvNoResult setHidden:YES]; // hidden tableview no result
    [tbvNoResult setBackgroundColor:[UIColor clearColor]];
    
    [tblListRoom setHidden:YES];
    [tblListRoom setBackgroundColor:[UIColor clearColor]];
    
    [tblContent setBackgroundColor:[UIColor clearColor]];
    self.openSectionIndex = NSNotFound;
    
    img=[UIImage imageNamed:kImageColorLabel];
    [searchBar setTintColor:[UIColor colorWithRed:colorBlueR green:colorBlueG blue:colorBlueB alpha:colorAlpha]];

    [bgLabel setBackgroundColor:[UIColor colorWithRed:colorBlueR green:colorBlueG 
                                                 blue:colorBlueB alpha:colorAlpha]];
    

    UILabel* blueBackground = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    [blueBackground  setBackgroundColor:[UIColor colorWithRed:colorBlueR green:colorBlueG 
                                                        blue:colorBlueB alpha:colorAlpha]];
    [searchBar insertSubview:blueBackground atIndex:1];

    [self setCaptionView];
    [self performSelector:@selector(loadTopbarView)];
        // Do any additional setup after loading the view from its nib.

//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(KeyboardDidShow) name:UIKeyboardDidShowNotification object:nil];
    
}

//- (void)KeyboardDidShow{
//    [listAllRooms removeAllObjects];
//    
//}

-(void)viewWillAppear:(BOOL)animated{
    
    [[HomeViewV2 shareHomeView] setSelectedButton:tagOfUnassignButton];
    
    floorModel = [[FloorModelV2 alloc] init];
    floorModel.floor_id = floorID;
    [[UnassignManagerV2 sharedUnassignManager] loadFloorsByFloorId:floorModel];
    
    
    NSString *titleString=[NSString stringWithFormat:@"%@ %@", [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ios_unassign_floor], floorModel.floor_name];
    
    titleString=[[[titleString substringToIndex:1] uppercaseString] stringByAppendingString:[titleString substringFromIndex:1]];

    btnTitle.titleLabel.font = [UIFont fontWithName:@"Arial-BoldMT" size:17];
    [btnTitle setTitle:titleString forState:UIControlStateNormal];
    btnTitle.titleLabel.textColor = [UIColor blackColor];

    
    if(!isFirstTime && assignRoomView.isAssigned){
       
        self.openSectionIndex = NSNotFound;
        ZoneModelV2 *zoneModel = [[ZoneModelV2 alloc] init];
        zoneModel.zone_floor_id = floorID;
        NSMutableArray *zoneList = [[UnassignManagerV2 sharedUnassignManager] loadZonesByFloorId:zoneModel];
        
        [sectionInfoArray removeAllObjects];
        [listTempData removeAllObjects];
        
        for (int index = 0; index<[zoneList count]; index++) {
            ZoneModelV2* zoneItem = (ZoneModelV2*)[zoneList objectAtIndex:index];
            
            // load room on earch item
            NSMutableArray *roomList = [[UnassignManagerV2 sharedUnassignManager] loadRoombyZoneId:zoneItem];
            
            UnassignZoneSectionV2 *unassignSection=[[UnassignZoneSectionV2 alloc] init];
            UnassignZoneModelV2 *unassignZone=[[UnassignZoneModelV2 alloc] init];
            
            if([roomList count]){
                // assign zone :
                unassignZone.unAssignZoneId = zoneItem.zone_id;
                unassignZone.unAssignZone_Floor_Id = zoneItem.zone_floor_id;
                unassignZone.unAssignZoneName = [zoneItem.zone_name uppercaseString];
                unassignZone.unAssignZoneName_Lang = zoneItem.zone_name_lang;
                unassignZone.unAssignZoneLast_Modified = zoneItem.zone_last_modified;
                unassignSection.chkContentType = unassignZone;
            }        
            
            for (int indexRoom=0; indexRoom < [roomList count]; indexRoom++) {
                UnassignModelV2 *unassignRoomModelItem = (UnassignModelV2*)[roomList objectAtIndex:indexRoom];
                [unassignSection insertObjectToNextIndex:unassignRoomModelItem];
            }
            
            if([roomList count]){
                [listTempData addObject:unassignSection];    
            }
            roomList = nil;
            
        }
        
        sectionInfoArray = [[NSMutableArray alloc] initWithArray:listTempData];
       
        [tblContent reloadData];

    }
    
    [super viewWillAppear:animated];
}
- (void)viewDidUnload
{

    tblContent = nil;
    [self setBtnSearchCondition:nil];

    btnTitle = nil;

    searchBar = nil;
    [self setTbvNoResult:nil];
    [self setBgLabel:nil];
    [self setTblListRoom:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark- hidden after save.
-(void)hudWasHidden
{
    if (HUD !=nil){
		[HUD removeFromSuperview];
    }
    //    [HUD release];
}


#pragma mark - UITableViewDelegate Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    if(isRoomNo){
        return 1;
    }
    else if([self isSearching]){
        return 1;
    }
    
    else if(tableView == tbvNoResult){
        return 1;
    } else{
        return [sectionInfoArray count];
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(isRoomNo &&  ![self isSearching]) //using filter room number
    {
        return [listAllRooms count];
    }
    
    if([self isSearching]){
        //sectionInfoArrayListRoom
        NSInteger numberRowInSection = 0;
        UnassignZoneSectionV2 *sectioninfo = nil;
        for (int index = 0; index < [sectionInfoArrayListRoom count]; index ++) {
            sectioninfo = [sectionInfoArrayListRoom objectAtIndex:index];
            numberRowInSection += sectioninfo.rowHeights.count;
        }
        
        sectioninfo.open = YES;
        //    return numberRowInSection;
        return sectioninfo.open ? numberRowInSection : 0;
        
    } else{
        if(tableView==tbvNoResult){
            return 1;
        } else {
            UnassignZoneSectionV2 *sectioninfo = [sectionInfoArray objectAtIndex:section];
            NSInteger numberRowInSection = sectioninfo.rowHeights.count;
            return sectioninfo.open ? numberRowInSection : 0;
        }
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    if([self isSearching]  || isRoomNo){
        return 60.0;
    }
    else if(tableView == tbvNoResult ){
        return 0;
    } else{
        UnassignZoneSectionV2 *sectioninfo = [sectionInfoArray objectAtIndex:section];
            if(sectioninfo.headerView.isToggle)
            {
                return 90.0;
            } 
            else
            {
                return 60.0;
            }
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if([self isSearching] || isRoomNo){
        return 50;
    }
    else if(tableView==tbvNoResult){
        return heightTableViewNoResult;
    } else {
        return 50;
    }
    
}


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if([self isSearching])
    {
        return nil;
    }
    else if(isRoomNo)
    {
        return nil;
    }
    else if(tableView == tbvNoResult )
    {
        return nil;
    }
    else
    {
        UnassignZoneSectionV2 *sectioninfo = [sectionInfoArray objectAtIndex:section];
        
        if (sectioninfo.headerView == nil) {
            UnassignZoneSectionHeaderV2 *sview = [[UnassignZoneSectionHeaderV2 alloc] initWithSection:section unassignZoneId:sectioninfo.chkContentType.unAssignZoneId unassignZoneName:sectioninfo.chkContentType.unAssignZoneName AndStatusArrow:NO] ;
            
            sectioninfo.headerView = sview;
            sview.delegate = self;
        }
        
        if(searchBar.text.length > 0){
            [sectioninfo.headerView setHidden:YES];
        }
        return sectioninfo.headerView;
    }
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *categoryRowIdentifier = @"categoryIdentifier";
     static NSString *categoryRowIdentifier1 = @"categoryIdentifier1";
    
    if([self isSearching] || isRoomNo){
        UnassignRoomTableViewCellV2 *cell = nil;
        cell = [tableView dequeueReusableCellWithIdentifier:categoryRowIdentifier];
        
        if (cell==nil) 
        {
            NSArray *array = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([UnassignRoomTableViewCellV2 class]) owner:self options:nil];
            cell = [array objectAtIndex:0];
        }
        
        UnassignModelV2 *model = nil;
        
        if ([self isSearching]) {
            NSArray *sectionAndRow = [[sectionIndexListRoom objectAtIndex:indexPath.row]  componentsSeparatedByString:@","];
            NSInteger section = [[sectionAndRow objectAtIndex:0] intValue];
            NSInteger row = [[sectionAndRow objectAtIndex:1] intValue];
            
            UnassignZoneSectionV2 *sectionInfo = [sectionInfoArrayListRoom objectAtIndex:section];
            if([LogFileManager isLogConsole]) {
                NSLog(@"%@ %d %d",sectionInfo.rowHeights,(int)section,(int)row);
            }

            //UnassignRoomModelV2 *model = [sectionInfo.rowHeights objectAtIndex:indexPath.row];
            model = [sectionInfo.rowHeights objectAtIndex:row] ;
            indexPath = [NSIndexPath indexPathForRow:row inSection:section];
        }
        else { //room NO chose
            model = [listAllRooms objectAtIndex:indexPath.row];
            indexPath = [NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section];
        }

        cell.roomNoLabel.text = model.unassignroom_room_id;
        [cell.roomNoLabel setFont:[UIFont fontWithName:fontTitleBold size:17]];
        
        RoomStatusModelV2 *rmStatus = [[RoomStatusModelV2 alloc] init];
        rmStatus.rstat_Id = (int)model.unassignroom_room_status_id;
        rmStatus = [[RoomManagerV2 sharedRoomManager] loadRoomStatusData:rmStatus];
        
        cell.roomStatusLabel.text = [NSString stringWithFormat:@"%@",rmStatus.rstat_Code];
        [cell.roomStatusLabel setFont:[UIFont fontWithName:fontTitleBold size:17]];
        
        
        cell.vipStatusLabel.text = [NSString stringWithFormat:@"%d",(int)model.unassignroom_room_kind] ;
        [cell.vipStatusLabel setFont:[UIFont fontWithName:fontTitleBold size:17]];
        
        
        cell.guestLabel.text =[NSString stringWithFormat:@"%@ %@",model.unassignroom_guest_first_name == nil ? @"" : model.unassignroom_guest_first_name ,model.unassignroom_guest_last_name == nil ? @"" : model.unassignroom_guest_last_name];
        [cell.guestLabel setFont:[UIFont fontWithName:fontTitleBold size:17]];
        [cell addLabel:indexPath];
        
        /***************************************************/
        [cell.addRoomButton setTitleColor:[UIColor colorWithPatternImage:img] forState:UIControlStateNormal];
        /***************************************************/
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell setBackgroundColor:[UIColor whiteColor]];
        [cell setDelegate:self];
        
        [cell setIndexpath:indexPath]; 
        
        return cell;
    }
    else if(tableView == tbvNoResult){
        UITableViewCell *cell = nil;
        cell = [tableView dequeueReusableCellWithIdentifier:categoryRowIdentifier1];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:categoryRowIdentifier1] ;
            
            UILabel *titleNoResult = [[UILabel alloc]
                                      initWithFrame:CGRectMake(0, 0, 320, 240)];
            [titleNoResult setTextColor:[UIColor grayColor]];
            [titleNoResult setBackgroundColor:[UIColor clearColor]];
            [titleNoResult setTextAlignment:NSTextAlignmentCenter];
            [titleNoResult setTag:tagNoResult];
            [titleNoResult setFont:[UIFont fontWithName:fontName size:13]];
            
            [cell.contentView addSubview:titleNoResult];
            
        }
        
        UILabel *lblNoResult = (UILabel *)[cell viewWithTag:tagNoResult];
        [lblNoResult setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_NO_RESULT_FOUND]];
        
        return cell;
    } else {
        
        UnassignRoomTableViewCellV2 *cell = nil;
        cell = [tableView dequeueReusableCellWithIdentifier:categoryRowIdentifier];
        
        if (cell==nil)
        {
            NSArray *array = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([UnassignRoomTableViewCellV2 class]) owner:self options:nil];
            cell = [array objectAtIndex:0];
            
        }
        
        UnassignZoneSectionV2 *sectionInfo = [sectionInfoArray objectAtIndex:indexPath.section];
        //UnassignRoomModelV2 *model = [sectionInfo.rowHeights objectAtIndex:indexPath.row];
        UnassignModelV2 *model = [sectionInfo.rowHeights objectAtIndex:indexPath.row];
        cell.roomNoLabel.text = model.unassignroom_room_id;
        [cell.roomNoLabel setFont:[UIFont fontWithName:fontTitleBold size:17]];
        
        RoomStatusModelV2 *rmStatus = [[RoomStatusModelV2 alloc] init];
        rmStatus.rstat_Id = (int)model.unassignroom_room_status_id;
        rmStatus = [[RoomManagerV2 sharedRoomManager] loadRoomStatusData:rmStatus];
        
        cell.roomStatusLabel.text = [NSString stringWithFormat:@"%@",rmStatus.rstat_Name];
        [cell.roomStatusLabel setFont:[UIFont fontWithName:fontTitleBold size:17]];
        
        
        cell.vipStatusLabel.text = [NSString stringWithFormat:@"%d",(int)model.unassignroom_room_kind] ;
        
        //    int numberOfUnAssignRoom = [UnassignManagerV2 numberOfUnAssignRoomOnZone:model.zon;
        //        cell.numOfRoomLabel.text = [NSString stringWithFormat:@"(%d)", numberOfUnAssignRoom];
        [cell.vipStatusLabel setFont:[UIFont fontWithName:fontTitleBold size:17]];
        
        
        cell.guestLabel.text =[NSString stringWithFormat:@"%@ %@",model.unassignroom_guest_first_name == nil ? @"" : model.unassignroom_guest_first_name ,model.unassignroom_guest_last_name == nil ? @"" : model.unassignroom_guest_last_name];
        [cell.guestLabel setFont:[UIFont fontWithName:fontTitleBold size:17]];
        [cell addLabel:indexPath];
        
        /***************************************************/
        //[cell.roomNoLabel setTextColor:[UIColor colorWithPatternImage:img]];
        //[cell.vipStatusLabel setTextColor:[UIColor colorWithPatternImage:img]];
        [cell.addRoomButton setTitleColor:[UIColor colorWithPatternImage:img] forState:UIControlStateNormal];
        //[cell.roomStatusLabel setTextColor:[UIColor colorWithPatternImage:img]];
        /***************************************************/
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell setBackgroundColor:[UIColor whiteColor]];
        [cell setDelegate:self];
        [cell setIndexpath:indexPath];
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self btnAddRoomPressedWithIndexPath:indexPath AndSender:nil];
}

#pragma mark - Section Header Delegate
-(void)sectionHeaderView:(UnassignZoneSectionHeaderV2 *)sectionheaderView sectionOpened:(NSInteger)section{
    UnassignZoneSectionV2 *sectionInfo = [sectionInfoArray objectAtIndex:section];
    sectionInfo.open = YES;
    
    NSInteger countOfRowsToInsert = [sectionInfo.rowHeights count];
    NSMutableArray *indexPathsToInsert = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i < countOfRowsToInsert; i++) {
        [indexPathsToInsert addObject:[NSIndexPath indexPathForRow:i
                                                         inSection:section]];
    }
    
    UITableViewRowAnimation insertAnimation;
    
    
    UITableViewRowAnimation deleteAnimation;
    
    
    NSMutableArray *indexPathsToDelete = [[NSMutableArray alloc] init];
    
    NSInteger previousOpenSectionIndex = self.openSectionIndex;
    
    if (openSectionIndex == -1) {
        
    } else {
        if (previousOpenSectionIndex != NSNotFound) {
            
            UnassignZoneSectionV2 *previousOpenSection = [self.sectionInfoArray objectAtIndex:previousOpenSectionIndex];
            previousOpenSection.open = NO;
            [previousOpenSection.headerView toggleOpenWithUserAction:NO];
            NSInteger countOfRowsToDelete =[previousOpenSection.rowHeights count];
            for (NSInteger i = 0; i < countOfRowsToDelete; i++) {
                [indexPathsToDelete addObject:[NSIndexPath indexPathForRow:i inSection:previousOpenSectionIndex]];
            }
        }
    }

    if (previousOpenSectionIndex == NSNotFound || section < previousOpenSectionIndex) {
        insertAnimation = UITableViewRowAnimationTop;
        deleteAnimation = UITableViewRowAnimationBottom;
    }
    else {
        insertAnimation = UITableViewRowAnimationBottom;
        deleteAnimation = UITableViewRowAnimationTop;
    }
    
    
    [self.tblContent beginUpdates];
    
    [self.tblContent insertRowsAtIndexPaths:indexPathsToInsert
                                  withRowAnimation:insertAnimation];
    [self.tblContent deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:deleteAnimation];
    [self.tblContent endUpdates];
    
    [self.tblContent scrollToRowAtIndexPath:[indexPathsToInsert objectAtIndex:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];    
    self.openSectionIndex = section;
}

-(void)sectionHeaderView:(UnassignZoneSectionHeaderV2 *)sectionheaderView sectionClosed:(NSInteger)section
{
    UnassignZoneSectionV2 *sectionInfo = [sectionInfoArray
                                        objectAtIndex:section];
    
    sectionInfo.open = NO;
    NSInteger countOfRowsToDelete = [self.tblContent
                                     numberOfRowsInSection:section];
    
    if (countOfRowsToDelete > 0) {
        NSMutableArray *indexPathsToDelete = [[NSMutableArray alloc] init];
        for (NSInteger i = 0; i < countOfRowsToDelete; i++) {
            [indexPathsToDelete addObject:[NSIndexPath indexPathForRow:i
                                                             inSection:section]];
        }
        
        [self.tblContent beginUpdates];
        [self.tblContent deleteRowsAtIndexPaths:indexPathsToDelete
                                      withRowAnimation:UITableViewRowAnimationTop];
        [self.tblContent endUpdates];
     }
    self.openSectionIndex = NSNotFound;
}

-(void)btnAddRoomPressedWithIndexPath:(NSIndexPath *)indexpath AndSender:(UIButton *)sender{
     isFirstTime = NO;
    
    UnassignZoneSectionV2 *sectionItem = nil; 
    UnassignModelV2 *roomItem = nil;
    
    if(isRoomNo)
    {
        roomItem = [listAllRooms objectAtIndex:indexpath.row];
    }
    else {
        sectionItem = [sectionInfoArray objectAtIndex:indexpath.section];
        roomItem = [sectionItem.rowHeights objectAtIndex:indexpath.row];
    }
    assignRoomView = [[AssignRoomV2 alloc] initWithNibName:NSStringFromClass([AssignRoomV2 class]) bundle:nil];
    
    
    UIBarButtonItem *BackButtonRoomInfo = [[UIBarButtonItem alloc] initWithTitle: [self getBackString] style: UIBarButtonItemStyleBordered target: nil action: nil];    
    [[self navigationItem] setBackBarButtonItem: BackButtonRoomInfo];    
    
    [self.navigationItem.backBarButtonItem setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BACK]];
    
    
    assignRoomView.unassignModel = roomItem;
    
    [self.navigationController pushViewController:assignRoomView animated:YES];
}

- (IBAction)filterCondition:(id)sender {
    UIButton *button = (UIButton *) sender;
    
    [searchBar resignFirstResponder];
    
    PickerViewV2 *picker = [[PickerViewV2 alloc] initPickerViewV2WithDatas:[NSArray arrayWithObjects:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_room_no_unassign_room], [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_zone_no], nil] AndSelectedData:button.titleLabel.text AndIndex:nil];
    picker.delegate = self;
    [picker showPickerViewV2];  
}

- (void) setCaptionView{
    [self setTitle:[[LanguageManagerV2 sharedLanguageManager] getUnassignedRoom]];
    searchBar.placeholder = [[LanguageManagerV2 sharedLanguageManager] getSearch];
    
    for(UIView *subView in searchBar.subviews){
        if([subView isKindOfClass:UIButton.class])
        {
            UIButton *cancelButton = (UIButton*)subView;
            [cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [cancelButton setTitle:[self getCancelString] forState:UIControlStateNormal];
        }
    }

}




#pragma mark 
#pragma mark  pickerView delegate

-(void)didChoosePickerViewV2WithData:(NSString *)data AndIndexRow:(NSInteger)index{
    
    //disable search if chose filter room or zone
    [self searchBarCancelButtonClicked:searchBar];
    
    switch (index) {
        case 0:
        {
            [btnSearchCondition setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_room_no_unassign_room] forState:UIControlStateNormal];
            //                searchStatus = ENUM_SEARCH_ROOM_NUMBER;
            isRoomNo = YES;
            [tblContent reloadData];
            [tblListRoom reloadData];
        }
            break;
        case 1:
        {
            [btnSearchCondition setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_zone_no] forState:UIControlStateNormal];
            //                searchStatus = ENUM_SEARCH_ZONE_NO;
            isRoomNo = NO;
            
            [tblListRoom reloadData];
            [tblContent reloadData];
        }
            break;
            
        default:
            break;
    }
    
    [self searchBar:self.searchBar textDidChange:self.searchBar.text];
}

#pragma mark - UISearchBar Delegate Methods

-(BOOL)isSearching{
    if(searchBar.text != nil && ![searchBar.text isEqualToString:@""] ){
        return YES;
    } else {
        return NO;
    }

}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBarA {
    [searchBarA setShowsCancelButton:NO animated:NO];
    [searchBarA resignFirstResponder];
}

-(NSString *) getCancelString {
    NSDictionary *dic = [[LanguageManagerV2 sharedLanguageManager] getCurrentDictionaryLanguage];
    return [dic valueForKey:[NSString stringWithFormat:@"%@", L_CANCEL]];
}

-(NSString *) getBackString {
    NSDictionary *dic = [[LanguageManagerV2 sharedLanguageManager] getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_BACK]];
    return s;
}

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBarA {   
    
    [searchBarA setShowsCancelButton:YES animated:NO];

    for(UIView *subView in searchBarA.subviews){
        if([subView isKindOfClass:UIButton.class])
        {
            UIButton *cancelButton = (UIButton*)subView;
            [cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            UIColor *tintColor = COLOR_CANCEL_SEARCH_BAR;
            [cancelButton setTintColor:tintColor];
             [cancelButton setTitle:[self getCancelString] forState:UIControlStateNormal];
        }
    }

    
}


-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBarA {
    
    [searchBarA setShowsCancelButton:NO animated:NO];
    [searchBarA resignFirstResponder];
    
    searchBarA.text = @"";
    [self searchBar:searchBarA textDidChange:@""];
    
    
}



-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {  
    
    if (searchText.length == 0) {
        [sectionInfoArrayListRoom removeAllObjects];
        [sectionInfoArrayListRoom addObjectsFromArray:listTempData];
        
        [tbvNoResult setHidden:YES];
        [tblContent setHidden:YES];
        [tblListRoom setHidden:NO];
        [btnTitle setHidden:NO];
        
        //Hao Tran[20130520/Fix empty data] - Fix empty data after search
        [tblContent reloadData];
        [tblListRoom reloadData];
        //Hao Tran[20130520/Fix empty data] - END
        
    } else {
        [sectionInfoArrayListRoom removeAllObjects];
        for (int zonid=0;zonid<[listTempData count];zonid++) {
       // for (UnassignZoneSectionV2 *zoneSectionItem in listTempData) {
            UnassignZoneSectionV2 *zoneSectionItem =[listTempData objectAtIndex:zonid];

            //HaoTran[20130523/Fix Layout] - Fix layout view after searching completed
            zoneSectionItem.headerView = nil;
            //HaoTran[20130523/Fix layout] - END
            UnassignZoneSectionV2 *unassignZoneTemp = [[UnassignZoneSectionV2 alloc] init];
            
            for (int index = 0 ; index < [zoneSectionItem countOfRowHeights] ;index++) {
                zoneSectionItem.open = NO;
                
                UnassignModelV2 *unassigRoomItem = (UnassignModelV2*)[zoneSectionItem.rowHeights objectAtIndex:index];
                
                UnassignModelV2 *unassignRoom=unassigRoomItem;

                NSString *roomNo = unassigRoomItem.unassignroom_room_id;
                NSRange roomRange = [roomNo rangeOfString:searchText options:NSCaseInsensitiveSearch];
                
                RoomStatusModelV2 *rmStatus = [[RoomStatusModelV2 alloc] init];
                rmStatus.rstat_Id = (int)unassigRoomItem.unassignroom_room_status_id;
                rmStatus = [[RoomManagerV2 sharedRoomManager] loadRoomStatusData:rmStatus];

                
                ZoneModelV2 *zoneModel = [[ZoneModelV2 alloc] init];
                zoneModel.zone_id =  unassigRoomItem.unassignroom_zone_id;
                zoneModel = [[UnassignManagerV2 sharedUnassignManager] loadZonesByZoneId:zoneModel];
                
                
                NSString *zoneNoValue = [NSString stringWithFormat:@"%@",zoneModel.zone_name] ; 
                NSRange zoneNoRange = [zoneNoValue rangeOfString:searchText options:NSCaseInsensitiveSearch];
                
                if([LogFileManager isLogConsole]) {
                    NSLog(@"%@ %@ %d",zoneModel.zone_name,searchText,(int)zoneNoRange.location);
                }
                
                NSInteger checkindex =-1;
//                switch (searchStatus) {
//                    case ENUM_SEARCH_ROOM_NUMBER:{
                        if(roomRange.location!=NSNotFound){
                            checkindex++;
                            [unassignZoneTemp insertObjectToNextIndex:unassignRoom];
                            [sectionIndexListRoom addObject:[NSString stringWithFormat:@"%d,%d",zonid,(int)checkindex]];
                        }
//                    }break;
//                     
//                    case ENUM_SEARCH_ZONE_NO:{
//                        if(zoneNoRange.location!=NSNotFound){
//                             checkindex++;
//                            [unassignZoneTemp insertObjectToNextIndex:unassignRoom];
//                           // [sectionIndexListRoom addObject:[NSString stringWithFormat:@"%d,%d",zonid,checkindex]];
//                        }
//
//                    } break;
//                        
//                    default:{
//                        if(roomRange.location!=NSNotFound){
//                            checkindex++;
//                            [unassignZoneTemp insertObjectToNextIndex:unassignRoom];
//                          //  [sectionIndexListRoom addObject:[NSString stringWithFormat:@"%d,%d",zonid,checkindex]];
//                        }
//
//                    }
//                        break;
//                }
               
                if([unassignZoneTemp countOfRowHeights]>0){
                    unassignZoneTemp.chkContentType = zoneSectionItem.chkContentType;
                }
            }
            
             
            // add zone to array:
            if([unassignZoneTemp countOfRowHeights]>0){
                [sectionInfoArrayListRoom addObject:unassignZoneTemp];
                [sectionIndexListRoom removeAllObjects];
            }
           
        }        
    }
    if([sectionInfoArrayListRoom count] > 0) {
        for(int indexZoneId=0;indexZoneId<[sectionInfoArrayListRoom count];indexZoneId++){
            UnassignZoneSectionV2 *zoneSectionItem =[sectionInfoArrayListRoom objectAtIndex:indexZoneId];
            for(int indexRom=0;indexRom<[zoneSectionItem countOfRowHeights];indexRom++){
                [sectionIndexListRoom addObject:[NSString stringWithFormat:@"%d,%d",indexZoneId,indexRom]];
            }
            
        }
     }
    
    if(![self isSearching]){
        if([sectionInfoArrayListRoom count] <= 0) {
            [tblContent setHidden:YES];            
            [tblListRoom setHidden:YES];            
            [btnTitle setHidden:YES];
            [tbvNoResult setHidden:NO];
        } else{
            [tbvNoResult setHidden:YES];            
            [tblListRoom setHidden:YES];
            [tblContent setHidden:NO];
            [btnTitle setHidden:NO];
            
        }
    } else{
        if([sectionInfoArrayListRoom count] <= 0) {
            [tblContent setHidden:YES];            
            [tblListRoom setHidden:YES];
            [btnTitle setHidden:YES];
            [tbvNoResult setHidden:NO];
        } else{
            [tbvNoResult setHidden:YES];
            [tblContent setHidden:YES];            
            [btnTitle setHidden:YES];
            [tblListRoom setHidden:NO];
            [tblListRoom reloadData];
            
        }
    }
        
    /********************************/
    self.openSectionIndex = -1;
    
    //[tblContent reloadData];
    [tblListRoom reloadData];
    
}



#pragma mark - Handle Topbar Methods
-(void)loadTopbarView {
    TopbarViewV2 *topbar = [[TopbarViewV2 alloc] initViewWithSuperModel:[[CommonVariable sharedCommonVariable] superRoomModel]];
    [topbar setTag:tagTopbarView];
    [self.view addSubview:topbar];
    [topbar setHidden:YES];
    
    [topbar setAlpha:0.0];
    [self addButtonHandleShowHideTopbar];
}

-(void)showTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationWillStartSelector:@selector(setHiddenTopbarView)];
    
    f = topbar.frame;
    f.origin.y = 0;
    [topbar setFrame:f];
    [topbar setAlpha:1.0];
    
    [self adjustShowForViews];
    
    [UIView commitAnimations];
}

-(void)hideTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDidStopSelector:@selector(setHiddenTopbarView)];
    [UIView setAnimationDelegate:self];
    
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    [topbar setAlpha:0.0];
    
    [self adjustRemoveForViews];
    
    [UIView commitAnimations];
}

-(void)setHiddenTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    [topbar setHidden:!topbar.hidden];
}

-(TopbarViewV2 *)getTopBarView {
    return (TopbarViewV2 *)[self.view viewWithTag:tagTopbarView];
}

-(void)addButtonHandleShowHideTopbar {
    
    NSDictionary *dic = [[LanguageManagerV2 sharedLanguageManager] getCurrentDictionaryLanguage];
    
    UIButton *titleBarButton = [[UIButton alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
    
    titleBarButton.titleLabel.backgroundColor = [UIColor clearColor];
    titleBarButton.titleLabel.font = FONT_SMALL_BUTTON_TOPBAR;
    titleBarButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    titleBarButton.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
    [titleBarButton.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
    [titleBarButton.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
    [titleBarButton addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    [titleBarButton setTitle:[dic valueForKey:[NSString stringWithFormat:@"%@", L_UNASSIGNED_ROOM]] forState:UIControlStateNormal];
    self.navigationItem.titleView = titleBarButton;
    //Move title to center vertical
    CGRect titleViewBounds = self.navigationItem.titleView.bounds;
    int moveLeftTitle = 0;
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        moveLeftTitle = 10;
    }
    self.navigationItem.titleView.bounds = CGRectMake(titleViewBounds.origin.x + moveLeftTitle, Y_TOPBAR_TITLE, titleViewBounds.size.width, titleViewBounds.size.height);
    
    //back button navigation
    self.navigationItem.leftBarButtonItem = [MyNavigationBarV2 createBackButtonWithTarget:self selector:@selector(btnBackPressed) forControlEvents:UIControlEventTouchUpInside];
}

-(void)btnBackPressed
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)topbarTapped:(UIButton *)sender {
    TopbarViewV2 *topbar = [self getTopBarView];
    [sender setEnabled:NO];
    if (topbar.isHidden) {
        [self showTopbarView];
    } else {
        [self hideTopbarView];
    }
    [self performSelector:@selector(topbarButtonEnable:) withObject:sender afterDelay:0.5];
}

-(void) topbarButtonEnable:(UIButton *) topbarBtn {
    [topbarBtn setEnabled:YES];
}

-(void)adjustShowForViews {
    //adjust all views when show 
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    CGRect ftbv = vUnassignZone.frame;
    [vUnassignZone setFrame:CGRectMake(0, f.size.height, 320, ftbv.size.height - f.size.height)];
}

-(void)adjustRemoveForViews { 
    //adjust all views when hide
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    CGRect ftbv = vUnassignZone.frame;
    [vUnassignZone setFrame:CGRectMake(0, 0, 320, ftbv.size.height + f.size.height)];
}

@end
