//
//  UserListPickerViewV2.h
//  mHouseKeeping
//
//  Created by TMS TRIBE on 3/16/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//



#import <UIKit/UIKit.h>
#import "UserListModelV2.h"
@protocol UserListPickerViewV2Delegate <NSObject>

@optional
-(void) didChoosePickerViewV2WithData:(NSString *) data AndIndex:(NSIndexPath *) index;
-(void) didChoosePickerViewV2WithData:(NSString *) data AndIndex:(NSIndexPath *) index WithtIndex:(NSInteger) tIndex;
//-(void) didChoosePickerViewV2WithData:(NSString *) data AndIndexRow:(NSInteger) index;
-(void)didChoosePickerViewV2WithData:(NSString *) data AndIndexItemRow:(NSIndexPath *) index;
-(void) didChoosePickerViewV2WithData:(id) data AndIndexRow:(NSInteger) index;
@end

@interface UserListPickerViewV2 : UIView <UIPickerViewDelegate, UIPickerViewDataSource>{
    NSArray *datas;
@private UIPickerView *pickerView;
@private id selectedData;
   __unsafe_unretained id<UserListPickerViewV2Delegate> delegate;
    NSIndexPath *path;
    NSInteger tIndex;
}

@property (nonatomic, assign) NSInteger tIndex;
@property (nonatomic, assign) id<UserListPickerViewV2Delegate> delegate;
@property (nonatomic, strong) id selectedData;
@property (nonatomic, strong) NSArray *datas;
@property (nonatomic, readonly, strong) NSIndexPath *path;

-(id) initPickerViewV2WithDatas:(NSArray *) data AndSelectedData:(id) currentSelectedData AndIndex:(NSIndexPath *)indexpath;
-(void) showPickerViewV2;
-(void) hidePickerViewV2;
@end
