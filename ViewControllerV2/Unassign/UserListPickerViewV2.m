//
//  UserListPickerViewV2.m
//  mHouseKeeping
//
//  Created by TMS TRIBE on 3/16/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "UserListPickerViewV2.h"
#import "eHouseKeepingAppDelegate.h"
#import "DeviceManager.h"

@interface UserListPickerViewV2 (PrivateMethods)

-(void)cancelButtonPressed;
-(void)doneButtonPressed;

@end

@implementation UserListPickerViewV2
@synthesize datas, selectedData, delegate, path, tIndex;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

-(id)initPickerViewV2WithDatas:(NSArray *)data AndSelectedData:(id)currentSelectedData AndIndex:(NSIndexPath *)indexpath{
    self = [super init];
    if (self) {
        CGRect f = self.frame;
        f.origin.y = DeviceScreenSizeStandardHeight3_5;
        f.origin.x = 0;
        f.size.width = 320;
        f.size.height = DeviceScreenSizeStandardHeight3_5;
        
        if([DeviceManager getDeviceScreenKind] == DeviceScreenKindRetina4_0){
            f.origin.y = DeviceScreenSizeStandardHeight4_0;
            f.size.height = DeviceScreenSizeStandardHeight4_0;
        }
        [self setFrame:f];
        
        self.datas = data;
        selectedData = currentSelectedData;
        
        pickerView = [[UIPickerView alloc] init];
        int moreY = 0;
        if([DeviceManager getDeviceScreenKind] == DeviceScreenKindRetina4_0){
            moreY = 88;
        }
        [pickerView setFrame:CGRectMake(0, 268 + moreY, 320, 216)];
        [pickerView setDelegate:self];
        [pickerView setShowsSelectionIndicator:YES];
        
        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
            UIToolbar  *blurView = [[UIToolbar alloc] initWithFrame:pickerView.frame];
            blurView.translucent = YES;
            [self insertSubview:blurView belowSubview:pickerView];
        }
        
        NSInteger indexSelect = 0;
        for (NSInteger index = 0; index < datas.count; index++) {
            if ([[datas objectAtIndex:index] isEqual:selectedData]) {
                indexSelect = index;
                break;
            }
        }
        [pickerView selectRow:indexSelect inComponent:0 animated:NO];
        [self addSubview:pickerView];
        
        UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 224 + moreY, 320, 44)];
        UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_CANCEL] style:UIBarButtonItemStyleBordered target:self action:@selector(cancelButtonPressed)];
        UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_DONE] style:UIBarButtonItemStyleDone target:self action:@selector(doneButtonPressed)];
        UIBarButtonItem *flexiablebar = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        [toolBar setItems:[NSArray arrayWithObjects:cancelButton, flexiablebar, doneButton, nil]];
        [self addSubview:toolBar];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setFrame:CGRectMake(0, 0, 320, 224)];
        [button addTarget:self action:@selector(hidePickerViewV2) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:button];
        
        path = indexpath;
    }
    return self;
}

#pragma mark - UIPickerView Datasource
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return datas.count;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    selectedData = [datas objectAtIndex:row];
}

-(UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
    UILabel *lblText = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 45)];
    
    
    if ([[[LanguageManagerV2 sharedLanguageManager] getCurrentLanguage] isEqualToString: (NSString *)ENGLISH_LANGUAGE] == NO) {
        [lblText setText:((UserListModelV2*)([datas objectAtIndex:row])).userListFullNameLang];
    } else{
        [lblText setText:((UserListModelV2*)([datas objectAtIndex:row])).userListFullName];
    }
    [lblText setTextAlignment:NSTextAlignmentCenter];
    [lblText setBackgroundColor:[UIColor clearColor]];
    [lblText setFont:[UIFont fontWithName:@"Arial-BoldMT" size:17.0]];
    return lblText;
}

#pragma mark - BarButton Methods
-(void)cancelButtonPressed {
    [self hidePickerViewV2];
}

-(void)doneButtonPressed {
    if ([delegate respondsToSelector:@selector(didChoosePickerViewV2WithData:AndIndex:)]) {
        
        [delegate didChoosePickerViewV2WithData:[datas objectAtIndex:[pickerView selectedRowInComponent:0]] AndIndex:path];
    }
    
    if ([delegate respondsToSelector:@selector(didChoosePickerViewV2WithData:AndIndex:WithtIndex:)]) {
        [delegate didChoosePickerViewV2WithData:[datas objectAtIndex:[pickerView selectedRowInComponent:0]] AndIndex:path WithtIndex:tIndex];
    }
    

    if([delegate respondsToSelector:@selector(didChoosePickerViewV2WithData:AndIndexRow:)]){
        [delegate didChoosePickerViewV2WithData:[datas objectAtIndex:[pickerView selectedRowInComponent:0]] AndIndexRow:[pickerView selectedRowInComponent:0]];
    }
    if ([delegate respondsToSelector:@selector(didChoosePickerViewV2WithData:AndIndexItemRow:)]) {
        
        [delegate didChoosePickerViewV2WithData:[datas objectAtIndex:[pickerView selectedRowInComponent:0]] AndIndexItemRow:path];
        
    }
    [self hidePickerViewV2];
}

#pragma mark - Show/Hide PickerViewV2
-(void) removePickerViewV2 {
    [self removeFromSuperview];
}

-(void)hidePickerViewV2 {
    [UIView beginAnimations:@"animationView" context:nil];
    [UIView setAnimationDuration:0.5];
    
    CGRect f = self.frame;
    f.origin.y = 480;
    [self setFrame:f];
    
    [UIView commitAnimations];
    
    [self performSelector:@selector(removePickerViewV2) withObject:nil afterDelay:0.5];
}

-(void)showPickerViewV2 {
    [[[[UIApplication sharedApplication] delegate] window] addSubview:self];
    
    [UIView beginAnimations:@"animationView" context:nil];
    [UIView setAnimationDuration:0.5];
    
    CGRect f = self.frame;
    f.origin.y = 0;
    [self setFrame:f];
    
    [UIView commitAnimations];
}


@end
