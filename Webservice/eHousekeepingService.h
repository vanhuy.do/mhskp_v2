#import <Foundation/Foundation.h>
#import "USAdditions.h"
#import <libxml/tree.h>
#import "USGlobals.h"
#import <objc/runtime.h>
#import "ServiceDefines.h"
#import "MLog.h"

@class eHousekeepingService_GetZoneList;
@class eHousekeepingService_GetZoneListResponse;
@class eHousekeepingService_ZnLst;
@class eHousekeepingService_ResponseStatus;
@class eHousekeepingService_ArrayOfZoneList;
@class eHousekeepingService_ZoneList;
@class eHousekeepingService_GetUnassignRoomList;
@class eHousekeepingService_GetUnassignRoomListResponse;
@class eHousekeepingService_UnAssgnLst;
@class eHousekeepingService_ArrayOfUnassignRoomList;
@class eHousekeepingService_UnassignRoomList;
@class eHousekeepingService_FindAttendant;
@class eHousekeepingService_FindAttendantResponse;
@class eHousekeepingService_AttdantList;
@class eHousekeepingService_ArrayOfAttendantList;
@class eHousekeepingService_AttendantList;
@class eHousekeepingService_GetZoneRoomList;
@class eHousekeepingService_GetZoneRoomListResponse;
@class eHousekeepingService_ZnRmLst;
@class eHousekeepingService_ArrayOfZoneRoomList;
@class eHousekeepingService_ZoneRoomList;
@class eHousekeepingService_GetCheckListItemScoreForMaid;
@class eHousekeepingService_GetCheckListItemScoreForMaidResponse;
@class eHousekeepingService_ChkListScoreForMaid;
@class eHousekeepingService_ArrayOfCheckListScoreForMaid;
@class eHousekeepingService_CheckListScoreForMaid;
@class eHousekeepingService_GetInspectionStatus;
@class eHousekeepingService_GetInspectionStatusResponse;
@class eHousekeepingService_InspStatus;
@class eHousekeepingService_ArrayOfInspectionStatusList;
@class eHousekeepingService_InspectionStatusList;
@class eHousekeepingService_PostWSLog;
@class eHousekeepingService_PostWSLogResponse;
@class eHousekeepingService_ValidateRoomNo;
@class eHousekeepingService_ValidateRoomNoResponse;
@class eHousekeepingService_GetAdditionalJobFloorList;
@class eHousekeepingService_GetAdditionalJobFloorListResponse;
@class eHousekeepingService_AdditionalJobFloorList;
@class eHousekeepingService_ArrayOfAdditionalJobFloor;
@class eHousekeepingService_AdditionalJobFloor;
@class eHousekeepingService_GetAdditionalJobTask;
@class eHousekeepingService_GetAdditionalJobTaskResponse;
@class eHousekeepingService_JobTaskList;
@class eHousekeepingService_ArrayOfJobTask;
@class eHousekeepingService_JobTask;
@class eHousekeepingService_GetMessageCategoryList2;
@class eHousekeepingService_GetMessageCategoryList2Response;
@class eHousekeepingService_MsgCatList2;
@class eHousekeepingService_ArrayOfMessageCategoryList2;
@class eHousekeepingService_MessageCategoryList2;
@class eHousekeepingService_GetMessageItemList2;
@class eHousekeepingService_GetMessageItemList2Response;
@class eHousekeepingService_MsgItmList2;
@class eHousekeepingService_ArrayOfMessageItemList2;
@class eHousekeepingService_MessageItemList2;
@class eHousekeepingService_GetNoOfAdHocMessage;
@class eHousekeepingService_GetNoOfAdHocMessageResponse;
@class eHousekeepingService_AdHocMessageCounter;
@class eHousekeepingService_GetSentAdHocMessage;
@class eHousekeepingService_GetSentAdHocMessageResponse;
@class eHousekeepingService_AdHocMsgList2;
@class eHousekeepingService_ArrayOfAdHocMessageList2;
@class eHousekeepingService_AdHocMessageList2;
@class eHousekeepingService_GetReceivedAdHocMessage;
@class eHousekeepingService_GetReceivedAdHocMessageResponse;
@class eHousekeepingService_UpdateAdHocMessageStatus;
@class eHousekeepingService_UpdateAdHocMessageStatusResponse;
@class eHousekeepingService_GetAdditionalJobRoomByFloor;
@class eHousekeepingService_GetAdditionalJobRoomByFloorResponse;
@class eHousekeepingService_AdditionalJobRoomList;
@class eHousekeepingService_ArrayOfAdditionalJobRoom;
@class eHousekeepingService_AdditionalJobRoom;
@class eHousekeepingService_ArrayOfAdditionalJob;
@class eHousekeepingService_AdditionalJob;
@class eHousekeepingService_GetAdditionalJobDetail;
@class eHousekeepingService_GetAdditionalJobDetailResponse;
@class eHousekeepingService_AdditionalJobDetail;
@class eHousekeepingService_CompleteAdditionalJob;
@class eHousekeepingService_CompleteAdditionalJobResponse;
@class eHousekeepingService_GetAdditionalJobCount;
@class eHousekeepingService_GetAdditionalJobCountResponse;
@class eHousekeepingService_AdditionalJobCount;
@class eHousekeepingService_UpdateAdditionalJobStatus;
@class eHousekeepingService_UpdateAdditionalJobStatusResponse;
@class eHousekeepingService_GetProfileNote;
@class eHousekeepingService_GetProfileNoteResponse;
@class eHousekeepingService_ProfileNoteTypeList;
@class eHousekeepingService_ArrayOfProfileNoteType;
@class eHousekeepingService_ProfileNoteType;
@class eHousekeepingService_ArrayOfProfileNote;
@class eHousekeepingService_ProfileNote;
@class eHousekeepingService_StartAdditionalJob;
@class eHousekeepingService_StartAdditionalJobResponse;
@class eHousekeepingService_StopAdditionalJob;
@class eHousekeepingService_StopAdditionalJobResponse;
@class eHousekeepingService_GetAdditionalJobOfRoom;
@class eHousekeepingService_GetAdditionalJobOfRoomResponse;
@class eHousekeepingService_AdditionalJobRoomDetail;
@class eHousekeepingService_GetGuestInfoByRoomID;
@class eHousekeepingService_GetGuestInfoByRoomIDResponse;
@class eHousekeepingService_GuestOfRoomInfo;
@class eHousekeepingService_GuestOfRoom;
@class eHousekeepingService_GetNumberOfGuest;
@class eHousekeepingService_GetNumberOfGuestResponse;
@class eHousekeepingService_NumberOfGuestInfo;
@class eHousekeepingService_UpdateAdditionalJobRemark;
@class eHousekeepingService_UpdateAdditionalJobRemarkResponse;
@class eHousekeepingService_GetAdditionalJobByRoomNo;
@class eHousekeepingService_GetAdditionalJobByRoomNoResponse;
@class eHousekeepingService_AdditionalJobOfRoom;
@class eHousekeepingService_FindRoomByRoomNo;
@class eHousekeepingService_FindRoomByRoomNoResponse;
@class eHousekeepingService_RoomAssignmentList;
@class eHousekeepingService_ArrayOfRoomAssign;
@class eHousekeepingService_RoomAssign;
@class eHousekeepingService_PostAmenitiesItemsList;
@class eHousekeepingService_ArrayOfAmenitiesItem;
@class eHousekeepingService_AmenitiesItem;
@class eHousekeepingService_UniqueRoom;
@class eHousekeepingService_PostAmenitiesItemsListResponse;
@class eHousekeepingService_PostAmenitiesItems;
@class eHousekeepingService_ArrayOfInt;
@class eHousekeepingService_GetPanicAlertConfig;
@class eHousekeepingService_GetPanicAlertConfigResponse;
@class eHousekeepingService_PanicAlertConfig;
@class eHousekeepingService_ArrayOfReceiver;
@class eHousekeepingService_Receiver;
@class eHousekeepingService_PostingPanicAlert;
@class eHousekeepingService_PostingPanicAlertResponse;
@class eHousekeepingService_GetGuestInformation;
@class eHousekeepingService_GetGuestInformationResponse;
@class eHousekeepingService_GuestInformationResponse;
@class eHousekeepingService_GuestRoomInformation;
@class eHousekeepingService_ArrayOfGuestInformation;
@class eHousekeepingService_GuestInformation;
@class eHousekeepingService_ArrayOfGuestProfileNoteDetail;
@class eHousekeepingService_GuestProfileNoteDetail;
@class eHousekeepingService_GetVersion;
@class eHousekeepingService_GetVersionResponse;
@class eHousekeepingService_Version;
@class eHousekeepingService_GetRoomRemarks;
@class eHousekeepingService_GetRoomRemarksResponse;
@class eHousekeepingService_GetRoomRemarksList;
@class eHousekeepingService_UpdateRoomRemarks;
@class eHousekeepingService_UpdateRoomRemarksResponse;
@class eHousekeepingService_GetOOSBlockRoomsList;
@class eHousekeepingService_GetOOSBlockRoomsListResponse;
@class eHousekeepingService_OOSBlockRoomList;
@class eHousekeepingService_ArrayOfBlockRoomDetails;
@class eHousekeepingService_BlockRoomDetails;
@class eHousekeepingService_GetReleaseRoomsList;
@class eHousekeepingService_GetReleaseRoomsListResponse;
@class eHousekeepingService_ReleaseRoomList;
@class eHousekeepingService_ArrayOfReleaseRoomDetails;
@class eHousekeepingService_ReleaseRoomDetails;
@class eHousekeepingService_GetFindFloorDetailsList;
@class eHousekeepingService_GetFindFloorDetailsRequest;
@class eHousekeepingService_GetFindFloorDetailsListResponse;
@class eHousekeepingService_GetFindFloorDetailsResponse;
@class eHousekeepingService_ArrayOfFloorDetails;
@class eHousekeepingService_FloorDetails;
@class eHousekeepingService_GetFindRoomDetailsList;
@class eHousekeepingService_GetFindRoomDetailsRequest;
@class eHousekeepingService_GetFindRoomDetailsListResponse;
@class eHousekeepingService_GetFindRoomDetailsResponse;
@class eHousekeepingService_ArrayOfRoomAssignmentDetails;
@class eHousekeepingService_RoomAssignmentDetails;
@class eHousekeepingService_GetStillLoggedIn;
@class eHousekeepingService_GetStillLoggedInResponse;
@class eHousekeepingService_PostLogout;
@class eHousekeepingService_PostLogoutResponse;
@class eHousekeepingService_LogInvalidStartRoom;
@class eHousekeepingService_LogInvalidStartRoomResponse;
@class eHousekeepingService_AddReassignRoom;
@class eHousekeepingService_AddReassignRoomResponse;
@class eHousekeepingService_AssignRoom;
@class eHousekeepingService_AssignRoomResponse;
@class eHousekeepingService_GetSupervisorFiltersRoutine;
@class eHousekeepingService_GetSupervisorFiltersRoutineResponse;
@class eHousekeepingService_GetSupervisorFiltersResponse;
@class eHousekeepingService_ArrayOfSupervisorFiltersDetails;
@class eHousekeepingService_SupervisorFiltersDetails;
@class eHousekeepingService_GetPostingHistoryRoutine;
@class eHousekeepingService_GetPostingHistoryRoutineResponse;
@class eHousekeepingService_GetPostingHistoryResponse;
@class eHousekeepingService_ArrayOfPostingHistoryDetails;
@class eHousekeepingService_PostingHistoryDetails;
@class eHousekeepingService_PostPostingHistoryRoutine;
@class eHousekeepingService_PostPostingHistoryRoutineResponse;
@class eHousekeepingService_PostPostingHistoryResponse;
@class eHousekeepingService_PostLostAndFoundRoutine;
@class eHousekeepingService_ArrayOfLostAndFoundItemDetails;
@class eHousekeepingService_LostAndFoundItemDetails;
@class eHousekeepingService_ArrayOfImageAttachmentDetails;
@class eHousekeepingService_ImageAttachmentDetails;
@class eHousekeepingService_PostLostAndFoundRoutineResponse;
@class eHousekeepingService_PostLostAndFoundResponse;
@class eHousekeepingService_GetAdditionalJobStatusesRoutine;
@class eHousekeepingService_GetAdditionalJobStatusesRoutineResponse;
@class eHousekeepingService_GetAdditionalJobStatusesResponse;
@class eHousekeepingService_ArrayOfAdditionalJobStatusDetails;
@class eHousekeepingService_AdditionalJobStatusDetails;
@class eHousekeepingService_PostAdditionalJobStatusUpdateRoutine;
@class eHousekeepingService_PostAdditionalJobStatusUpdateRoutineResponse;
@class eHousekeepingService_PostAdditionalJobStatusUpdateResponse;
@class eHousekeepingService_FindGuestStayedHistoryRoutine;
@class eHousekeepingService_FindGuestStayedHistoryRoutineResponse;
@class eHousekeepingService_FindGuestStayHistoryResponse;
@class eHousekeepingService_ArrayOfFindGuestInformation;
@class eHousekeepingService_FindGuestInformation;
@class eHousekeepingService_PostUnassignRoomRoutine;
@class eHousekeepingService_PostUnassignRoomRoutineResponse;
@class eHousekeepingService_PostUpdateRoomDetailsRoutine;
@class eHousekeepingService_PostUpdateRoomDetailsRoutineResponse;
@class eHousekeepingService_PostPhysicalCheck;
@class eHousekeepingService_GetRoomTypeInventoryRoutine;
@class eHousekeepingService_GetRoomTypeInventoryRoutineResponse;
@class eHousekeepingService_RoomTypeInventoryResponse;
@class eHousekeepingService_ArrayOfRoomTypeInventoryDetails;
@class eHousekeepingService_RoomTypeInventoryDetails;
@class eHousekeepingService_GetRoomTypeByRoomNoRoutine;
@class eHousekeepingService_GetRoomTypeByRoomNoRoutineResponse;
@class eHousekeepingService_RoomTypeList;
@class eHousekeepingService_ArrayOfRoomType;
@class eHousekeepingService_RoomType;
@class eHousekeepingService_PostUpdateAdHocMessageRoutine;
@class eHousekeepingService_ArrayOfMessageUpdateDetails;
@class eHousekeepingService_MessageUpdateDetails;
@class eHousekeepingService_PostUpdateAdHocMessageRoutineResponse;
@class eHousekeepingService_UpdateAdHocMessageResponse;
@class eHousekeepingService_GetNoOfAdHocMessageRoutine;
@class eHousekeepingService_GetNoOfAdHocMessageRoutineResponse;
@class eHousekeepingService_GetReceivedAdHocMessageRoutine;
@class eHousekeepingService_GetReceivedAdHocMessageRoutineResponse;
@class eHousekeepingService_GetSendAdHocMessageRoutine;
@class eHousekeepingService_GetSendAdHocMessageRoutineResponse;
@class eHousekeepingService_GetOtherActivityAssignmentRoutine;
@class eHousekeepingService_GetOtherActivityAssignmentRoutineResponse;
@class eHousekeepingService_OtherActivityDetailsList;
@class eHousekeepingService_ArrayOfOtherActivityDetails;
@class eHousekeepingService_OtherActivityDetails;
@class eHousekeepingService_GetOtherActivitiesStatusRoutine;
@class eHousekeepingService_GetOtherActivitiesStatusRoutineResponse;
@class eHousekeepingService_OtherActivityStatusList;
@class eHousekeepingService_ArrayOfOtherActivityStatus;
@class eHousekeepingService_OtherActivityStatus;
@class eHousekeepingService_GetOtherActivitiesLocationRoutine;
@class eHousekeepingService_GetOtherActivitiesLocationRoutineResponse;
@class eHousekeepingService_OtherActivityLocationList;
@class eHousekeepingService_ArrayOfOtherActivityLocation;
@class eHousekeepingService_OtherActivityLocation;
@class eHousekeepingService_PostUpdateOtherActivityStatusRoutine;
@class eHousekeepingService_PostUpdateOtherActivityStatusRoutineResponse;
@class eHousekeepingService_PostOtherActivityStatusUpdateResponse;
@class eHousekeepingService_GetLanguageList;
@class eHousekeepingService_GetLanguageListResponse;
@class eHousekeepingService_LanguageList;
@class eHousekeepingService_ArrayOfLanguageItem;
@class eHousekeepingService_LanguageItem;
@class eHousekeepingService_GetRoomStatusList;
@class eHousekeepingService_GetRoomStatusListResponse;
@class eHousekeepingService_RoomStatusList;
@class eHousekeepingService_ArrayOfRoomStatus;
@class eHousekeepingService_RoomStatus;
@class eHousekeepingService_GetCleaningStatusList;
@class eHousekeepingService_GetCleaningStatusListResponse;
@class eHousekeepingService_CleaningStatusList;
@class eHousekeepingService_ArrayOfCleaningStatus;
@class eHousekeepingService_CleaningStatus;
@class eHousekeepingService_GetRoomTypeList;
@class eHousekeepingService_GetRoomTypeListResponse;
@class eHousekeepingService_GetBuildingList;
@class eHousekeepingService_GetBuildingListResponse;
@class eHousekeepingService_BuildingList;
@class eHousekeepingService_ArrayOfBuilding;
@class eHousekeepingService_Building;
@class eHousekeepingService_GetFloorList;
@class eHousekeepingService_GetFloorListResponse;
@class eHousekeepingService_FloorList;
@class eHousekeepingService_ArrayOfFloor;
@class eHousekeepingService_Floor;
@class eHousekeepingService_AuthenticateUser;
@class eHousekeepingService_AuthenticateUserResponse;
@class eHousekeepingService_EHSKPUser;
@class eHousekeepingService_UserDetail;
@class eHousekeepingService_GetHotelInfo;
@class eHousekeepingService_GetHotelInfoResponse;
@class eHousekeepingService_HotelInfo;
@class eHousekeepingService_Hotel;
@class eHousekeepingService_GetRoomAssignmentList;
@class eHousekeepingService_GetRoomAssignmentListResponse;
@class eHousekeepingService_GetPrevRoomAssignmentList;
@class eHousekeepingService_GetPrevRoomAssignmentListResponse;
@class eHousekeepingService_GetFindRoomAssignmentList;
@class eHousekeepingService_GetFindRoomAssignmentListResponse;
@class eHousekeepingService_GetFindRoomAssignmentLists;
@class eHousekeepingService_GetFindRoomAssignmentListsResponse;
@class eHousekeepingService_GetSupervisorFindFloorList;
@class eHousekeepingService_GetSupervisorFindFloorListResponse;
@class eHousekeepingService_GetSupervisorFindRoomAssignmentLists;
@class eHousekeepingService_GetSupervisorFindRoomAssignmentListsResponse;
@class eHousekeepingService_GetGuestInfo;
@class eHousekeepingService_GetGuestInfoResponse;
@class eHousekeepingService_GuestInfo;
@class eHousekeepingService_Guest;
@class eHousekeepingService_GetRoomDetail;
@class eHousekeepingService_GetRoomDetailResponse;
@class eHousekeepingService_RoomDetail;
@class eHousekeepingService_UpdateGuestInfo;
@class eHousekeepingService_UpdateGuestInfoResponse;
@class eHousekeepingService_UpdateRoomAssignment;
@class eHousekeepingService_UpdateRoomAssignmentResponse;
@class eHousekeepingService_GetDiscrepantRoomStatus;
@class eHousekeepingService_GetDiscrepantRoomStatusResponse;
@class eHousekeepingService_GetDiscrepantRoomStatusList;
@class eHousekeepingService_UpdateRoomCleaningStatus;
@class eHousekeepingService_UpdateRoomCleaningStatusResponse;
@class eHousekeepingService_UpdateRoomStatus;
@class eHousekeepingService_UpdateRoomStatusResponse;
@class eHousekeepingService_UpdateRoomDetails;
@class eHousekeepingService_UpdateRoomDetailsResponse;
@class eHousekeepingService_UpdateCleaningStatus;
@class eHousekeepingService_UpdateCleaningStatusResponse;
@class eHousekeepingService_GetAccessRights;
@class eHousekeepingService_GetAccessRightsResponse;
@class eHousekeepingService_AccessRightsList;
@class eHousekeepingService_ArrayOfRights;
@class eHousekeepingService_Rights;
@class eHousekeepingService_GetCommonConfigurations;
@class eHousekeepingService_GetCommonConfigurationsResponse;
@class eHousekeepingService_CommonConfigurationsList;
@class eHousekeepingService_ArrayOfWSSettings;
@class eHousekeepingService_WSSettings;
@class eHousekeepingService_GetLaundryServiceList;
@class eHousekeepingService_GetLaundryServiceListResponse;
@class eHousekeepingService_LdrySrvList;
@class eHousekeepingService_ArrayOfLaundryList;
@class eHousekeepingService_LaundryList;
@class eHousekeepingService_GetLaundryCategoryList;
@class eHousekeepingService_GetLaundryCategoryListResponse;
@class eHousekeepingService_LdryTypList;
@class eHousekeepingService_ArrayOfLaundryTypeList;
@class eHousekeepingService_LaundryTypeList;
@class eHousekeepingService_GetLaundryItemList;
@class eHousekeepingService_GetLaundryItemListResponse;
@class eHousekeepingService_LdryItmList;
@class eHousekeepingService_ArrayOfLaundryItemList;
@class eHousekeepingService_LaundryItemList;
@class eHousekeepingService_RemoveLaundryItem;
@class eHousekeepingService_RemoveLaundryItemResponse;
@class eHousekeepingService_PostLaundryItem;
@class eHousekeepingService_PostLaundryItemResponse;
@class eHousekeepingService_GetRoomSetGuideByRoomType;
@class eHousekeepingService_GetRoomSetGuideByRoomTypeResponse;
@class eHousekeepingService_RoomStpList;
@class eHousekeepingService_ArrayOfRoomSetupList;
@class eHousekeepingService_RoomSetupList;
@class eHousekeepingService_ArrayOfBase64Binary;
@class eHousekeepingService_GetRoomSetGuideList;
@class eHousekeepingService_GetRoomSetGuideListResponse;
@class eHousekeepingService_GetMessageTemplateList;
@class eHousekeepingService_GetMessageTemplateListResponse;
@class eHousekeepingService_MsgTempList;
@class eHousekeepingService_ArrayOfMessageTemplateList;
@class eHousekeepingService_MessageTemplateList;
@class eHousekeepingService_GetNewMessage;
@class eHousekeepingService_GetNewMessageResponse;
@class eHousekeepingService_AdHocMsgList;
@class eHousekeepingService_ArrayOfAdHocMessageList;
@class eHousekeepingService_AdHocMessageList;
@class eHousekeepingService_GetMessageCategoryList;
@class eHousekeepingService_GetMessageCategoryListResponse;
@class eHousekeepingService_MsgCatList;
@class eHousekeepingService_ArrayOfMessageCategoryList;
@class eHousekeepingService_MessageCategoryList;
@class eHousekeepingService_GetMessageItemList;
@class eHousekeepingService_GetMessageItemListResponse;
@class eHousekeepingService_MsgItmList;
@class eHousekeepingService_ArrayOfMessageItemList;
@class eHousekeepingService_MessageItemList;
@class eHousekeepingService_GetPopupMsgList;
@class eHousekeepingService_GetPopupMsgListResponse;
@class eHousekeepingService_PopupMsgList;
@class eHousekeepingService_ArrayOfPopupMsgItem;
@class eHousekeepingService_PopupMsgItem;
@class eHousekeepingService_UpdatePopupMsgStatus;
@class eHousekeepingService_UpdatePopupMsgStatusResponse;
@class eHousekeepingService_PostMessage;
@class eHousekeepingService_PostMessageResponse;
@class eHousekeepingService_GrpAdHocList;
@class eHousekeepingService_ArrayOfGroupAdHocMessage;
@class eHousekeepingService_GroupAdHocMessage;
@class eHousekeepingService_PostEngineeringCase;
@class eHousekeepingService_PostEngineeringCaseResponse;
@class eHousekeepingService_EngCase;
@class eHousekeepingService_PosteCnJob;
@class eHousekeepingService_PosteCnJobResponse;
@class eHousekeepingService_PosteCnJobList;
@class eHousekeepingService_PosteCnAhMsg;
@class eHousekeepingService_PosteCnAhMsgResponse;
@class eHousekeepingService_PosteCnAhMsgList;
@class eHousekeepingService_PostECAttachPhoto;
@class eHousekeepingService_PostECAttachPhotoResponse;
@class eHousekeepingService_GetLFCategoryList;
@class eHousekeepingService_GetLFCategoryListResponse;
@class eHousekeepingService_LostNFoundCat;
@class eHousekeepingService_ArrayOfLostAndFoundCategory;
@class eHousekeepingService_LostAndFoundCategory;
@class eHousekeepingService_GetLFItemList;
@class eHousekeepingService_GetLFItemListResponse;
@class eHousekeepingService_LostNFoundItmTypList;
@class eHousekeepingService_ArrayOfLostAndFoundItemTypeList;
@class eHousekeepingService_LostAndFoundItemTypeList;
@class eHousekeepingService_GetLFColorList;
@class eHousekeepingService_GetLFColorListResponse;
@class eHousekeepingService_LostNFoundColorList;
@class eHousekeepingService_ArrayOfLostAndFoundColorList;
@class eHousekeepingService_LostAndFoundColorList;
@class eHousekeepingService_PostLostFound;
@class eHousekeepingService_PostLostFoundResponse;
@class eHousekeepingService_LostFnd;
@class eHousekeepingService_PostLFAttachPhoto;
@class eHousekeepingService_PostLFAttachPhotoResponse;
@class eHousekeepingService_GetLocationList;
@class eHousekeepingService_GetLocationListResponse;
@class eHousekeepingService_LocList;
@class eHousekeepingService_ArrayOfLocationList;
@class eHousekeepingService_LocationList;
@class eHousekeepingService_GetUserList;
@class eHousekeepingService_GetUserListResponse;
@class eHousekeepingService_UserList;
@class eHousekeepingService_ArrayOfUserDetail;
@class eHousekeepingService_GetRoomSectionList;
@class eHousekeepingService_GetRoomSectionListResponse;
@class eHousekeepingService_RmSecList;
@class eHousekeepingService_ArrayOfRoomSectionList;
@class eHousekeepingService_RoomSectionList;
@class eHousekeepingService_GetEngineeringItem;
@class eHousekeepingService_GetEngineeringItemResponse;
@class eHousekeepingService_EngItmList;
@class eHousekeepingService_ArrayOfEngineeringItemList;
@class eHousekeepingService_EngineeringItemList;
@class eHousekeepingService_GetEngineeringCategoryList;
@class eHousekeepingService_GetEngineeringCategoryListResponse;
@class eHousekeepingService_EngCatList;
@class eHousekeepingService_ArrayOfEngineeringCategoryList;
@class eHousekeepingService_EngineeringCategoryList;
@class eHousekeepingService_GetLaundrySpecialInstructionList;
@class eHousekeepingService_GetLaundrySpecialInstructionListResponse;
@class eHousekeepingService_LdryInstList;
@class eHousekeepingService_ArrayOfLaundryInstructionList;
@class eHousekeepingService_LaundryInstructionList;
@class eHousekeepingService_GetLaundryItemPriceList;
@class eHousekeepingService_GetLaundryItemPriceListResponse;
@class eHousekeepingService_LdryItmPriceList;
@class eHousekeepingService_ArrayOfLaundryItmPriceList;
@class eHousekeepingService_LaundryItmPriceList;
@class eHousekeepingService_GetMinibarCategoryList;
@class eHousekeepingService_GetMinibarCategoryListResponse;
@class eHousekeepingService_MinibarCatList;
@class eHousekeepingService_ArrayOfMinibarCategoryList;
@class eHousekeepingService_MinibarCategoryList;
@class eHousekeepingService_GetLinenCategoryList;
@class eHousekeepingService_GetLinenCategoryListResponse;
@class eHousekeepingService_LinenCatList;
@class eHousekeepingService_ArrayOfLinenCategoryList;
@class eHousekeepingService_LinenCategoryList;
@class eHousekeepingService_GetAmenitiesCategoryList;
@class eHousekeepingService_GetAmenitiesCategoryListResponse;
@class eHousekeepingService_AmenitiesCatList;
@class eHousekeepingService_ArrayOfAmenitiesCategoryList;
@class eHousekeepingService_AmenitiesCategoryList;
@class eHousekeepingService_GetMinibarItemList;
@class eHousekeepingService_GetMinibarItemListResponse;
@class eHousekeepingService_MinibarItmList;
@class eHousekeepingService_ArrayOfMinibarItemList;
@class eHousekeepingService_MinibarItemList;
@class eHousekeepingService_GetLinenItemList;
@class eHousekeepingService_GetLinenItemListResponse;
@class eHousekeepingService_LinenItmList;
@class eHousekeepingService_ArrayOfLinenItemList;
@class eHousekeepingService_LinenItemList;
@class eHousekeepingService_GetAmenitiesItemList;
@class eHousekeepingService_GetAmenitiesItemListResponse;
@class eHousekeepingService_AmenitiesItmList;
@class eHousekeepingService_ArrayOfAmenitiesItemList;
@class eHousekeepingService_AmenitiesItemList;
@class eHousekeepingService_GetAmenitiesLocationList;
@class eHousekeepingService_GetAmenitiesLocationListResponse;
@class eHousekeepingService_AmenitiesLocList;
@class eHousekeepingService_ArrayOfAmenitiesLocationList;
@class eHousekeepingService_AmenitiesLocationList;
@class eHousekeepingService_PostLaundryOrder;
@class eHousekeepingService_PostLaundryOrderResponse;
@class eHousekeepingService_PostLaundrySpecialInstruction;
@class eHousekeepingService_PostLaundrySpecialInstructionResponse;
@class eHousekeepingService_PostMinibarItem;
@class eHousekeepingService_PostMinibarItemResponse;
@class eHousekeepingService_PostMinibarList;
@class eHousekeepingService_PostLinenItem;
@class eHousekeepingService_PostLinenItemResponse;
@class eHousekeepingService_PostLinenList;
@class eHousekeepingService_PostMinibarItems;
@class eHousekeepingService_ArrayOfPostItem;
@class eHousekeepingService_PostItem;
@class eHousekeepingService_PostMinibarItemsResponse;
@class eHousekeepingService_PostMinibarItemsList;
@class eHousekeepingService_ArrayOfPostedItem;
@class eHousekeepingService_PostedItem;
@class eHousekeepingService_PostLinenItems;
@class eHousekeepingService_PostLinenItemsResponse;
@class eHousekeepingService_PostLinenItemsList;
@class eHousekeepingService_PostChecklistItems;
@class eHousekeepingService_ArrayOfPostChecklistItem;
@class eHousekeepingService_PostChecklistItem;
@class eHousekeepingService_PostChecklistItemsResponse;
@class eHousekeepingService_PostChecklistItemsList;
@class eHousekeepingService_PostingLinenItems;
@class eHousekeepingService_PostingLinenItemsResponse;
@class eHousekeepingService_PostingLinenItemsList;
@class eHousekeepingService_PostAmenitiesItem;
@class eHousekeepingService_PostAmenitiesItemResponse;
@class eHousekeepingService_PostAmenitiesList;
@class eHousekeepingService_GetChecklistCategoryList;
@class eHousekeepingService_GetChecklistCategoryListResponse;
@class eHousekeepingService_ChkListCategoryLst;
@class eHousekeepingService_ArrayOfCheckListCategory;
@class eHousekeepingService_CheckListCategory;
@class eHousekeepingService_PostMinibarOrder;
@class eHousekeepingService_PostMinibarOrderResponse;
@class eHousekeepingService_GetAllChecklist;
@class eHousekeepingService_GetAllChecklistResponse;
@class eHousekeepingService_SprChkList;
@class eHousekeepingService_ArrayOfSupervisorCheckList;
@class eHousekeepingService_SupervisorCheckList;
@class eHousekeepingService_GetChecklistItemList;
@class eHousekeepingService_GetChecklistItemListResponse;
@class eHousekeepingService_ChkListItm;
@class eHousekeepingService_ArrayOfCheckListItem;
@class eHousekeepingService_CheckListItem;
@class eHousekeepingService_GetChecklistRoomType;
@class eHousekeepingService_GetChecklistRoomTypeResponse;
@class eHousekeepingService_ChkListRmTyp;
@class eHousekeepingService_ArrayOfCheckListRoomType;
@class eHousekeepingService_CheckListRoomType;
@class eHousekeepingService_PostChecklistItemResponse;
@class eHousekeepingService_UpdateInspection;
@class eHousekeepingService_UpdateInspectionResponse;
@class eHousekeepingService_ChangeRoomAssignmentSequence;
@class eHousekeepingService_ChangeRoomAssignmentSequenceResponse;
@class eHousekeepingService_GetMessageAttachPhoto;
@class eHousekeepingService_GetMessageAttachPhotoResponse;
@class eHousekeepingService_AdHocMsgPhoto;
@class eHousekeepingService_ArrayOfAdHocPhoto;
@class eHousekeepingService_AdHocPhoto;
@class eHousekeepingService_PostMessageAttachPhoto;
@class eHousekeepingService_PostMessageAttachPhotoResponse;
@interface eHousekeepingService_GetZoneList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intHotelID;
	NSString * strLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetZoneList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intHotelID;
@property (nonatomic, retain) NSString * strLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ResponseStatus : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * respCode;
	NSString * respMsg;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ResponseStatus *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * respCode;
@property (nonatomic, retain) NSString * respMsg;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ZoneList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * znID;
	NSString * znName;
	NSString * znLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ZoneList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * znID;
@property (nonatomic, retain) NSString * znName;
@property (nonatomic, retain) NSString * znLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfZoneList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *ZoneList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfZoneList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addZoneList:(eHousekeepingService_ZoneList *)toAdd;
@property (nonatomic, readonly) NSMutableArray * ZoneList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ZnLst : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_ArrayOfZoneList * ZnLst;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ZnLst *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_ArrayOfZoneList * ZnLst;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetZoneListResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ZnLst * GetZoneListResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetZoneListResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ZnLst * GetZoneListResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetUnassignRoomList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSNumber * intHotelID;
	NSString * strLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetUnassignRoomList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSNumber * intHotelID;
@property (nonatomic, retain) NSString * strLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_UnassignRoomList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * uarRoomAssignID;
	NSString * uarRoomNo;
	NSNumber * uarHotelID;
	NSNumber * uarZoneID;
	NSNumber * uarFloorID;
	NSNumber * uarRoomStatusID;
	NSNumber * uarCleaningStatusID;
	NSString * uarVIP;
	NSString * uarGuestFirstName;
	NSString * uarGuestLastName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_UnassignRoomList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * uarRoomAssignID;
@property (nonatomic, retain) NSString * uarRoomNo;
@property (nonatomic, retain) NSNumber * uarHotelID;
@property (nonatomic, retain) NSNumber * uarZoneID;
@property (nonatomic, retain) NSNumber * uarFloorID;
@property (nonatomic, retain) NSNumber * uarRoomStatusID;
@property (nonatomic, retain) NSNumber * uarCleaningStatusID;
@property (nonatomic, retain) NSString * uarVIP;
@property (nonatomic, retain) NSString * uarGuestFirstName;
@property (nonatomic, retain) NSString * uarGuestLastName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfUnassignRoomList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *UnassignRoomList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfUnassignRoomList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addUnassignRoomList:(eHousekeepingService_UnassignRoomList *)toAdd;
@property (nonatomic, readonly) NSMutableArray * UnassignRoomList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_UnAssgnLst : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_ArrayOfUnassignRoomList * UnAssgnLst;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_UnAssgnLst *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_ArrayOfUnassignRoomList * UnAssgnLst;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetUnassignRoomListResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_UnAssgnLst * GetUnassignRoomListResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetUnassignRoomListResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_UnAssgnLst * GetUnassignRoomListResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_FindAttendant : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSNumber * intHotelID;
	NSString * strAttendantName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_FindAttendant *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSNumber * intHotelID;
@property (nonatomic, retain) NSString * strAttendantName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_AttendantList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * atdName;
	NSString * atdFullName;
	NSNumber * atdID;
	NSNumber * atdCurrentStatus;
	NSString * atdRoomNo;
	NSNumber * atdRoomAssignID;
	NSString * atdCurrentScheduleTime;
	NSString * atdStatusName;
	NSString * atdStatusLang;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_AttendantList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * atdName;
@property (nonatomic, retain) NSString * atdFullName;
@property (nonatomic, retain) NSNumber * atdID;
@property (nonatomic, retain) NSNumber * atdCurrentStatus;
@property (nonatomic, retain) NSString * atdRoomNo;
@property (nonatomic, retain) NSNumber * atdRoomAssignID;
@property (nonatomic, retain) NSString * atdCurrentScheduleTime;
@property (nonatomic, retain) NSString * atdStatusName;
@property (nonatomic, retain) NSString * atdStatusLang;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfAttendantList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *AttendantList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfAttendantList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addAttendantList:(eHousekeepingService_AttendantList *)toAdd;
@property (nonatomic, readonly) NSMutableArray * AttendantList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_AttdantList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_ArrayOfAttendantList * AttendantList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_AttdantList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_ArrayOfAttendantList * AttendantList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_FindAttendantResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_AttdantList * FindAttendantResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_FindAttendantResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_AttdantList * FindAttendantResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetZoneRoomList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intHotelID;
	NSString * strLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetZoneRoomList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intHotelID;
@property (nonatomic, retain) NSString * strLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ZoneRoomList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * znrRoomID;
	NSNumber * znrZoneID;
	NSNumber * znrFloorID;
	NSString * znrLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ZoneRoomList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * znrRoomID;
@property (nonatomic, retain) NSNumber * znrZoneID;
@property (nonatomic, retain) NSNumber * znrFloorID;
@property (nonatomic, retain) NSString * znrLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfZoneRoomList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *ZoneRoomList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfZoneRoomList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addZoneRoomList:(eHousekeepingService_ZoneRoomList *)toAdd;
@property (nonatomic, readonly) NSMutableArray * ZoneRoomList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ZnRmLst : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_ArrayOfZoneRoomList * ZnRoomLst;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ZnRmLst *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_ArrayOfZoneRoomList * ZnRoomLst;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetZoneRoomListResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ZnRmLst * GetZoneRoomListResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetZoneRoomListResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ZnRmLst * GetZoneRoomListResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetCheckListItemScoreForMaid : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSNumber * intDutyAssignID;
	NSString * strLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetCheckListItemScoreForMaid *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSNumber * intDutyAssignID;
@property (nonatomic, retain) NSString * strLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_CheckListScoreForMaid : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * clsCheckListItemID;
	NSNumber * clsInspectedBy;
	NSNumber * clsScore;
	NSString * clsLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_CheckListScoreForMaid *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * clsCheckListItemID;
@property (nonatomic, retain) NSNumber * clsInspectedBy;
@property (nonatomic, retain) NSNumber * clsScore;
@property (nonatomic, retain) NSString * clsLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfCheckListScoreForMaid : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *CheckListScoreForMaid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfCheckListScoreForMaid *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addCheckListScoreForMaid:(eHousekeepingService_CheckListScoreForMaid *)toAdd;
@property (nonatomic, readonly) NSMutableArray * CheckListScoreForMaid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ChkListScoreForMaid : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_ArrayOfCheckListScoreForMaid * ChkLstScoreForMaid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ChkListScoreForMaid *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_ArrayOfCheckListScoreForMaid * ChkLstScoreForMaid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetCheckListItemScoreForMaidResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ChkListScoreForMaid * GetCheckListItemScoreForMaidResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetCheckListItemScoreForMaidResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ChkListScoreForMaid * GetCheckListItemScoreForMaidResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetInspectionStatus : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSString * strLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetInspectionStatus *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSString * strLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_InspectionStatusList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * inspID;
	NSString * inspName;
	NSString * inspLang;
	NSData * inspPicture;
	NSString * inspLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_InspectionStatusList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * inspID;
@property (nonatomic, retain) NSString * inspName;
@property (nonatomic, retain) NSString * inspLang;
@property (nonatomic, retain) NSData * inspPicture;
@property (nonatomic, retain) NSString * inspLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfInspectionStatusList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *InspectionStatusList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfInspectionStatusList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addInspectionStatusList:(eHousekeepingService_InspectionStatusList *)toAdd;
@property (nonatomic, readonly) NSMutableArray * InspectionStatusList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_InspStatus : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_ArrayOfInspectionStatusList * InspStatusLst;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_InspStatus *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_ArrayOfInspectionStatusList * InspStatusLst;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetInspectionStatusResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_InspStatus * GetInspectionStatusResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetInspectionStatusResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_InspStatus * GetInspectionStatusResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostWSLog : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSString * strMessage;
	NSNumber * intMessageType;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostWSLog *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSString * strMessage;
@property (nonatomic, retain) NSNumber * intMessageType;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostWSLogResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * PostWSLogResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostWSLogResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * PostWSLogResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ValidateRoomNo : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSString * strRoomNo;
	NSString * strHotel_ID;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ValidateRoomNo *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSString * strRoomNo;
@property (nonatomic, retain) NSString * strHotel_ID;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ValidateRoomNoResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ValidateRoomNoResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ValidateRoomNoResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ValidateRoomNoResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetAdditionalJobFloorList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intHotelID;
	NSNumber * intUserId;
	NSString * taskIDs;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetAdditionalJobFloorList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intHotelID;
@property (nonatomic, retain) NSNumber * intUserId;
@property (nonatomic, retain) NSString * taskIDs;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_AdditionalJobFloor : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * floorID;
	NSString * floorName;
	NSString * floorLang;
	NSNumber * numberOfJob;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_AdditionalJobFloor *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * floorID;
@property (nonatomic, retain) NSString * floorName;
@property (nonatomic, retain) NSString * floorLang;
@property (nonatomic, retain) NSNumber * numberOfJob;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfAdditionalJobFloor : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *AdditionalJobFloor;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfAdditionalJobFloor *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addAdditionalJobFloor:(eHousekeepingService_AdditionalJobFloor *)toAdd;
@property (nonatomic, readonly) NSMutableArray * AdditionalJobFloor;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_AdditionalJobFloorList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_ArrayOfAdditionalJobFloor * AdditionalJobFloorList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_AdditionalJobFloorList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_ArrayOfAdditionalJobFloor * AdditionalJobFloorList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetAdditionalJobFloorListResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_AdditionalJobFloorList * GetAdditionalJobFloorListResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetAdditionalJobFloorListResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_AdditionalJobFloorList * GetAdditionalJobFloorListResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetAdditionalJobTask : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intHotelID;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetAdditionalJobTask *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intHotelID;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_JobTask : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * taskID;
	NSString * taskName;
	NSString * taskLang;
	NSString * taskLang2;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_JobTask *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * taskID;
@property (nonatomic, retain) NSString * taskName;
@property (nonatomic, retain) NSString * taskLang;
@property (nonatomic, retain) NSString * taskLang2;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfJobTask : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *JobTask;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfJobTask *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addJobTask:(eHousekeepingService_JobTask *)toAdd;
@property (nonatomic, readonly) NSMutableArray * JobTask;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_JobTaskList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_ArrayOfJobTask * JobTaskList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_JobTaskList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_ArrayOfJobTask * JobTaskList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetAdditionalJobTaskResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_JobTaskList * GetAdditionalJobTaskResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetAdditionalJobTaskResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_JobTaskList * GetAdditionalJobTaskResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetMessageCategoryList2 : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSNumber * intHotelID;
	NSString * strLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetMessageCategoryList2 *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSNumber * intHotelID;
@property (nonatomic, retain) NSString * strLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_MessageCategoryList2 : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * msgCatID;
	NSString * msgCatName;
	NSString * msgCatDesc;
	NSString * msgCatNameLang;
	NSString * msgCatNameLang2;
	NSString * msgCatDescLang;
	NSString * msgCatDescLang2;
	NSData * msgCatPhoto;
	NSString * msgCatLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_MessageCategoryList2 *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * msgCatID;
@property (nonatomic, retain) NSString * msgCatName;
@property (nonatomic, retain) NSString * msgCatDesc;
@property (nonatomic, retain) NSString * msgCatNameLang;
@property (nonatomic, retain) NSString * msgCatNameLang2;
@property (nonatomic, retain) NSString * msgCatDescLang;
@property (nonatomic, retain) NSString * msgCatDescLang2;
@property (nonatomic, retain) NSData * msgCatPhoto;
@property (nonatomic, retain) NSString * msgCatLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfMessageCategoryList2 : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *MessageCategoryList2;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfMessageCategoryList2 *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addMessageCategoryList2:(eHousekeepingService_MessageCategoryList2 *)toAdd;
@property (nonatomic, readonly) NSMutableArray * MessageCategoryList2;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_MsgCatList2 : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_ArrayOfMessageCategoryList2 * MessageCatList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_MsgCatList2 *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_ArrayOfMessageCategoryList2 * MessageCatList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetMessageCategoryList2Response : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_MsgCatList2 * GetMessageCategoryList2Result;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetMessageCategoryList2Response *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_MsgCatList2 * GetMessageCategoryList2Result;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetMessageItemList2 : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSString * strLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetMessageItemList2 *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSString * strLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_MessageItemList2 : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * msgItmID;
	NSNumber * msgItmSeq;
	NSNumber * msgItmCatID;
	NSString * msgItmName;
	NSString * msgItmNameLang;
	NSString * msgItmNameLang2;
	NSString * msgItmDesc;
	NSString * msgItmDescLang;
	NSString * msgItmDescLang2;
	NSData * msgItmPhoto;
	NSString * msgItmLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_MessageItemList2 *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * msgItmID;
@property (nonatomic, retain) NSNumber * msgItmSeq;
@property (nonatomic, retain) NSNumber * msgItmCatID;
@property (nonatomic, retain) NSString * msgItmName;
@property (nonatomic, retain) NSString * msgItmNameLang;
@property (nonatomic, retain) NSString * msgItmNameLang2;
@property (nonatomic, retain) NSString * msgItmDesc;
@property (nonatomic, retain) NSString * msgItmDescLang;
@property (nonatomic, retain) NSString * msgItmDescLang2;
@property (nonatomic, retain) NSData * msgItmPhoto;
@property (nonatomic, retain) NSString * msgItmLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfMessageItemList2 : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *MessageItemList2;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfMessageItemList2 *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addMessageItemList2:(eHousekeepingService_MessageItemList2 *)toAdd;
@property (nonatomic, readonly) NSMutableArray * MessageItemList2;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_MsgItmList2 : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_ArrayOfMessageItemList2 * MessageItmList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_MsgItmList2 *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_ArrayOfMessageItemList2 * MessageItmList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetMessageItemList2Response : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_MsgItmList2 * GetMessageItemList2Result;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetMessageItemList2Response *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_MsgItmList2 * GetMessageItemList2Result;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetNoOfAdHocMessage : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSString * intGrpMessageID;
	NSNumber * intHotelID;
	NSString * strLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetNoOfAdHocMessage *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSString * intGrpMessageID;
@property (nonatomic, retain) NSNumber * intHotelID;
@property (nonatomic, retain) NSString * strLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_AdHocMessageCounter : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	NSNumber * intSent;
	NSNumber * intReadReceived;
	NSNumber * intUnreadReceive;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_AdHocMessageCounter *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) NSNumber * intSent;
@property (nonatomic, retain) NSNumber * intReadReceived;
@property (nonatomic, retain) NSNumber * intUnreadReceive;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetNoOfAdHocMessageResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_AdHocMessageCounter * GetNoOfAdHocMessageResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetNoOfAdHocMessageResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_AdHocMessageCounter * GetNoOfAdHocMessageResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetSentAdHocMessage : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSString * intGrpMessageID;
	NSNumber * intHotelID;
	NSString * strLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetSentAdHocMessage *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSString * intGrpMessageID;
@property (nonatomic, retain) NSNumber * intHotelID;
@property (nonatomic, retain) NSString * strLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_AdHocMessageList2 : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * amsgID;
	NSNumber * amsgGrpID;
	NSString * amsgSendFrom;
	NSString * amsgSendFromLang;
	NSString * amsgTopic;
	NSString * amsgContent;
	NSString * amsgLastModified;
	NSNumber * amsgPhotoAttached;
	NSString * amsgReceivedIDs;
	NSNumber * amsgStatus;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_AdHocMessageList2 *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * amsgID;
@property (nonatomic, retain) NSNumber * amsgGrpID;
@property (nonatomic, retain) NSString * amsgSendFrom;
@property (nonatomic, retain) NSString * amsgSendFromLang;
@property (nonatomic, retain) NSString * amsgTopic;
@property (nonatomic, retain) NSString * amsgContent;
@property (nonatomic, retain) NSString * amsgLastModified;
@property (nonatomic, retain) NSNumber * amsgPhotoAttached;
@property (nonatomic, retain) NSString * amsgReceivedIDs;
@property (nonatomic, retain) NSNumber * amsgStatus;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfAdHocMessageList2 : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *AdHocMessageList2;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfAdHocMessageList2 *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addAdHocMessageList2:(eHousekeepingService_AdHocMessageList2 *)toAdd;
@property (nonatomic, readonly) NSMutableArray * AdHocMessageList2;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_AdHocMsgList2 : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_ArrayOfAdHocMessageList2 * AdHocMessageList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_AdHocMsgList2 *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_ArrayOfAdHocMessageList2 * AdHocMessageList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetSentAdHocMessageResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_AdHocMsgList2 * GetSentAdHocMessageResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetSentAdHocMessageResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_AdHocMsgList2 * GetSentAdHocMessageResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetReceivedAdHocMessage : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSString * intGrpMessageID;
	NSNumber * intHotelID;
	NSString * strLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetReceivedAdHocMessage *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSString * intGrpMessageID;
@property (nonatomic, retain) NSNumber * intHotelID;
@property (nonatomic, retain) NSString * strLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetReceivedAdHocMessageResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_AdHocMsgList2 * GetReceivedAdHocMessageResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetReceivedAdHocMessageResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_AdHocMsgList2 * GetReceivedAdHocMessageResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_UpdateAdHocMessageStatus : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSNumber * intMsgID;
	NSNumber * intStatus;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_UpdateAdHocMessageStatus *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSNumber * intMsgID;
@property (nonatomic, retain) NSNumber * intStatus;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_UpdateAdHocMessageStatusResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * UpdateAdHocMessageStatusResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_UpdateAdHocMessageStatusResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * UpdateAdHocMessageStatusResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetAdditionalJobRoomByFloor : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intHotelID;
	NSNumber * intUserID;
	NSNumber * intFloorID;
	NSString * taskIds;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetAdditionalJobRoomByFloor *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intHotelID;
@property (nonatomic, retain) NSNumber * intUserID;
@property (nonatomic, retain) NSNumber * intFloorID;
@property (nonatomic, retain) NSString * taskIds;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_AdditionalJob : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * ondayAdditionalJobId;
	NSNumber * additionalJobId;
	NSString * additionalJobName;
	NSString * additionalJobNamend;
	NSString * additionalJobNamerd;
	NSNumber * additionalJobStatus;
	NSString * additionalJobRemark;
	NSString * additionalJobAssignedDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_AdditionalJob *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * ondayAdditionalJobId;
@property (nonatomic, retain) NSNumber * additionalJobId;
@property (nonatomic, retain) NSString * additionalJobName;
@property (nonatomic, retain) NSString * additionalJobNamend;
@property (nonatomic, retain) NSString * additionalJobNamerd;
@property (nonatomic, retain) NSNumber * additionalJobStatus;
@property (nonatomic, retain) NSString * additionalJobRemark;
@property (nonatomic, retain) NSString * additionalJobAssignedDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfAdditionalJob : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *AdditionalJob;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfAdditionalJob *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addAdditionalJob:(eHousekeepingService_AdditionalJob *)toAdd;
@property (nonatomic, readonly) NSMutableArray * AdditionalJob;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_AdditionalJobRoom : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * roomId;
	NSString * roomNo;
	USBoolean * isDueOutRoom;
	NSNumber * roomStatusID;
	NSString * remark;
	eHousekeepingService_ArrayOfAdditionalJob * pendingAdditionalJobList;
	NSNumber * totalPendingAdditionalJobList;
	NSNumber * roomTypeID;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_AdditionalJobRoom *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * roomId;
@property (nonatomic, retain) NSString * roomNo;
@property (nonatomic, retain) USBoolean * isDueOutRoom;
@property (nonatomic, retain) NSNumber * roomStatusID;
@property (nonatomic, retain) NSString * remark;
@property (nonatomic, retain) eHousekeepingService_ArrayOfAdditionalJob * pendingAdditionalJobList;
@property (nonatomic, retain) NSNumber * totalPendingAdditionalJobList;
@property (nonatomic, retain) NSNumber * roomTypeID;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfAdditionalJobRoom : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *AdditionalJobRoom;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfAdditionalJobRoom *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addAdditionalJobRoom:(eHousekeepingService_AdditionalJobRoom *)toAdd;
@property (nonatomic, readonly) NSMutableArray * AdditionalJobRoom;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_AdditionalJobRoomList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_ArrayOfAdditionalJobRoom * AdditionalJobRoomList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_AdditionalJobRoomList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_ArrayOfAdditionalJobRoom * AdditionalJobRoomList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetAdditionalJobRoomByFloorResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_AdditionalJobRoomList * GetAdditionalJobRoomByFloorResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetAdditionalJobRoomByFloorResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_AdditionalJobRoomList * GetAdditionalJobRoomByFloorResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetAdditionalJobDetail : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * ondayAdditionalJobId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetAdditionalJobDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * ondayAdditionalJobId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_AdditionalJobDetail : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	NSNumber * expectedCleaningTime;
	eHousekeepingService_ArrayOfJobTask * JobTaskList;
	NSString * jobRemark;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_AdditionalJobDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) NSNumber * expectedCleaningTime;
@property (nonatomic, retain) eHousekeepingService_ArrayOfJobTask * JobTaskList;
@property (nonatomic, retain) NSString * jobRemark;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetAdditionalJobDetailResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_AdditionalJobDetail * GetAdditionalJobDetailResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetAdditionalJobDetailResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_AdditionalJobDetail * GetAdditionalJobDetailResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_CompleteAdditionalJob : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * ondayAdditionalJobId;
	NSString * completedDateTime;
	NSString * UserID;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_CompleteAdditionalJob *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * ondayAdditionalJobId;
@property (nonatomic, retain) NSString * completedDateTime;
@property (nonatomic, retain) NSString * UserID;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_CompleteAdditionalJobResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * CompleteAdditionalJobResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_CompleteAdditionalJobResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * CompleteAdditionalJobResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetAdditionalJobCount : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intHotelID;
	NSNumber * intUserID;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetAdditionalJobCount *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intHotelID;
@property (nonatomic, retain) NSNumber * intUserID;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_AdditionalJobCount : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	NSNumber * AdditionalJobCount;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_AdditionalJobCount *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) NSNumber * AdditionalJobCount;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetAdditionalJobCountResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_AdditionalJobCount * GetAdditionalJobCountResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetAdditionalJobCountResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_AdditionalJobCount * GetAdditionalJobCountResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_UpdateAdditionalJobStatus : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * ondayAdditionalJobId;
	NSNumber * ondayAdditionalJobStatus;
	NSString * completedDateTime;
	NSString * strUserID;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_UpdateAdditionalJobStatus *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * ondayAdditionalJobId;
@property (nonatomic, retain) NSNumber * ondayAdditionalJobStatus;
@property (nonatomic, retain) NSString * completedDateTime;
@property (nonatomic, retain) NSString * strUserID;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_UpdateAdditionalJobStatusResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * UpdateAdditionalJobStatusResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_UpdateAdditionalJobStatusResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * UpdateAdditionalJobStatusResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetProfileNote : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * sRoomNo;
	NSNumber * intUserID;
	NSNumber * intHotelID;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetProfileNote *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * sRoomNo;
@property (nonatomic, retain) NSNumber * intUserID;
@property (nonatomic, retain) NSNumber * intHotelID;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ProfileNote : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * NoteDescription;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ProfileNote *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * NoteDescription;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfProfileNote : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *ProfileNote;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfProfileNote *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addProfileNote:(eHousekeepingService_ProfileNote *)toAdd;
@property (nonatomic, readonly) NSMutableArray * ProfileNote;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ProfileNoteType : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * typeID;
	NSString * typeCode;
	NSString * typeDescription;
	eHousekeepingService_ArrayOfProfileNote * ProfileNoteList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ProfileNoteType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * typeID;
@property (nonatomic, retain) NSString * typeCode;
@property (nonatomic, retain) NSString * typeDescription;
@property (nonatomic, retain) eHousekeepingService_ArrayOfProfileNote * ProfileNoteList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfProfileNoteType : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *ProfileNoteType;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfProfileNoteType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addProfileNoteType:(eHousekeepingService_ProfileNoteType *)toAdd;
@property (nonatomic, readonly) NSMutableArray * ProfileNoteType;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ProfileNoteTypeList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_ArrayOfProfileNoteType * ProfileNoteTypeList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ProfileNoteTypeList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_ArrayOfProfileNoteType * ProfileNoteTypeList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetProfileNoteResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ProfileNoteTypeList * GetProfileNoteResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetProfileNoteResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ProfileNoteTypeList * GetProfileNoteResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_StartAdditionalJob : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * ondayAdditionalJobId;
	NSString * startedDateTime;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_StartAdditionalJob *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * ondayAdditionalJobId;
@property (nonatomic, retain) NSString * startedDateTime;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_StartAdditionalJobResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * StartAdditionalJobResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_StartAdditionalJobResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * StartAdditionalJobResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_StopAdditionalJob : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * ondayAdditionalJobId;
	NSString * stopDateTime;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_StopAdditionalJob *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * ondayAdditionalJobId;
@property (nonatomic, retain) NSString * stopDateTime;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_StopAdditionalJobResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * StopAdditionalJobResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_StopAdditionalJobResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * StopAdditionalJobResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetAdditionalJobOfRoom : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intHotelID;
	NSNumber * intUserID;
	NSNumber * intDutyAssignID;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetAdditionalJobOfRoom *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intHotelID;
@property (nonatomic, retain) NSNumber * intUserID;
@property (nonatomic, retain) NSNumber * intDutyAssignID;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_AdditionalJobRoomDetail : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_ArrayOfAdditionalJob * AdditionalJobList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_AdditionalJobRoomDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_ArrayOfAdditionalJob * AdditionalJobList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetAdditionalJobOfRoomResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_AdditionalJobRoomDetail * GetAdditionalJobOfRoomResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetAdditionalJobOfRoomResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_AdditionalJobRoomDetail * GetAdditionalJobOfRoomResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetGuestInfoByRoomID : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSNumber * intRoomID;
	NSString * strLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetGuestInfoByRoomID *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSNumber * intRoomID;
@property (nonatomic, retain) NSString * strLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GuestOfRoom : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * roomID;
	NSString * gpTitle;
	NSString * gpName;
	NSString * gpHousekeepingName;
	NSString * gpLangPref;
	NSString * gpCheckInDt;
	NSString * gpCheckOutDt;
	NSString * gpPreferenceDesc;
	NSString * gpVIP;
	NSString * gpLastModified;
	NSData * gpPhoto;
	NSString * gpSpecialService;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GuestOfRoom *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * roomID;
@property (nonatomic, retain) NSString * gpTitle;
@property (nonatomic, retain) NSString * gpName;
@property (nonatomic, retain) NSString * gpHousekeepingName;
@property (nonatomic, retain) NSString * gpLangPref;
@property (nonatomic, retain) NSString * gpCheckInDt;
@property (nonatomic, retain) NSString * gpCheckOutDt;
@property (nonatomic, retain) NSString * gpPreferenceDesc;
@property (nonatomic, retain) NSString * gpVIP;
@property (nonatomic, retain) NSString * gpLastModified;
@property (nonatomic, retain) NSData * gpPhoto;
@property (nonatomic, retain) NSString * gpSpecialService;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GuestOfRoomInfo : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_GuestOfRoom * GuestInfo;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GuestOfRoomInfo *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_GuestOfRoom * GuestInfo;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetGuestInfoByRoomIDResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_GuestOfRoomInfo * GetGuestInfoByRoomIDResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetGuestInfoByRoomIDResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_GuestOfRoomInfo * GetGuestInfoByRoomIDResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetNumberOfGuest : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSNumber * intHotelID;
	NSString * sRoomNo;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetNumberOfGuest *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSNumber * intHotelID;
@property (nonatomic, retain) NSString * sRoomNo;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_NumberOfGuestInfo : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	NSNumber * NumberOfGuest;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_NumberOfGuestInfo *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) NSNumber * NumberOfGuest;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetNumberOfGuestResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_NumberOfGuestInfo * GetNumberOfGuestResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetNumberOfGuestResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_NumberOfGuestInfo * GetNumberOfGuestResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_UpdateAdditionalJobRemark : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSNumber * intOnDayJobID;
	NSString * sRemark;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_UpdateAdditionalJobRemark *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSNumber * intOnDayJobID;
@property (nonatomic, retain) NSString * sRemark;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_UpdateAdditionalJobRemarkResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * UpdateAdditionalJobRemarkResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_UpdateAdditionalJobRemarkResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * UpdateAdditionalJobRemarkResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetAdditionalJobByRoomNo : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intHotelID;
	NSNumber * intUserID;
	NSString * sRoomNo;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetAdditionalJobByRoomNo *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intHotelID;
@property (nonatomic, retain) NSNumber * intUserID;
@property (nonatomic, retain) NSString * sRoomNo;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_AdditionalJobOfRoom : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_AdditionalJobRoom * AddtionalJobRoom;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_AdditionalJobOfRoom *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_AdditionalJobRoom * AddtionalJobRoom;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetAdditionalJobByRoomNoResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_AdditionalJobOfRoom * GetAdditionalJobByRoomNoResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetAdditionalJobByRoomNoResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_AdditionalJobOfRoom * GetAdditionalJobByRoomNoResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_FindRoomByRoomNo : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intStaffId;
	NSString * strRoomNo;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_FindRoomByRoomNo *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intStaffId;
@property (nonatomic, retain) NSString * strRoomNo;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_RoomAssign : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * raID;
	NSString * raHousekeeperID;
	NSString * raRoomNo;
	NSString * raRoomStatusID;
	NSString * raCleaningStatusID;
	NSString * raRoomTypeID;
	NSString * raFloorID;
	NSNumber * raBuildingID;
	NSString * raBuildingName;
	NSString * rabuildingLang;
	NSString * raPriority;
	NSString * raGuestName;
	NSString * raVIP;
	NSString * raExpectedCleaningTime;
	NSString * raGuestPreference;
	NSNumber * raPrioritySortOrder;
	NSNumber * raInspectedStatus;
	NSString * raLastModified;
	NSString * raCleanStartTm;
	NSString * raCleanEndTm;
	NSNumber * raExpectedInspectTime;
	NSString * raAssignedTime;
	NSString * raGuestFirstName;
	NSString * raGuestLastName;
	NSString * raHotelID;
	NSString * raInspectStartTm;
	NSString * raInspectEndTm;
	NSString * raAdditionalJob;
	NSString * raRemark;
	NSString * raInspectedScore;
	NSNumber * raGuestProfileID;
	NSNumber * raTotalCleaningTime;
	NSNumber * raToTalInspectionTime;
	NSString * raFullName;
	NSString * raLastRoomCleaningDate;
	NSString * raGuestArrivalTime;
	NSString * raGuestDepartureTime;
	NSNumber * raKindOfRoom;
	NSNumber * raIsMockRoom;
    NSString * raCleaningCredit;
    NSString * raChecklistRemarks;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_RoomAssign *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * raID;
@property (nonatomic, retain) NSString * raHousekeeperID;
@property (nonatomic, retain) NSString * raRoomNo;
@property (nonatomic, retain) NSString * raRoomStatusID;
@property (nonatomic, retain) NSString * raCleaningStatusID;
@property (nonatomic, retain) NSString * raRoomTypeID;
@property (nonatomic, retain) NSString * raFloorID;
@property (nonatomic, retain) NSNumber * raBuildingID;
@property (nonatomic, retain) NSString * raBuildingName;
@property (nonatomic, retain) NSString * rabuildingLang;
@property (nonatomic, retain) NSString * raPriority;
@property (nonatomic, retain) NSString * raGuestName;
@property (nonatomic, retain) NSString * raVIP;
@property (nonatomic, retain) NSString * raExpectedCleaningTime;
@property (nonatomic, retain) NSString * raGuestPreference;
@property (nonatomic, retain) NSNumber * raPrioritySortOrder;
@property (nonatomic, retain) NSNumber * raInspectedStatus;
@property (nonatomic, retain) NSString * raLastModified;
@property (nonatomic, retain) NSString * raCleanStartTm;
@property (nonatomic, retain) NSString * raCleanEndTm;
@property (nonatomic, retain) NSNumber * raExpectedInspectTime;
@property (nonatomic, retain) NSString * raAssignedTime;
@property (nonatomic, retain) NSString * raGuestFirstName;
@property (nonatomic, retain) NSString * raGuestLastName;
@property (nonatomic, retain) NSString * raHotelID;
@property (nonatomic, retain) NSString * raInspectStartTm;
@property (nonatomic, retain) NSString * raInspectEndTm;
@property (nonatomic, retain) NSString * raAdditionalJob;
@property (nonatomic, retain) NSString * raRemark;
@property (nonatomic, retain) NSString * raInspectedScore;
@property (nonatomic, retain) NSNumber * raGuestProfileID;
@property (nonatomic, retain) NSNumber * raTotalCleaningTime;
@property (nonatomic, retain) NSNumber * raToTalInspectionTime;
@property (nonatomic, retain) NSString * raFullName;
@property (nonatomic, retain) NSString * raLastRoomCleaningDate;
@property (nonatomic, retain) NSString * raGuestArrivalTime;
@property (nonatomic, retain) NSString * raGuestDepartureTime;
@property (nonatomic, retain) NSNumber * raKindOfRoom;
@property (nonatomic, retain) NSNumber * raIsMockRoom;
@property (nonatomic, retain) NSString * raCleaningCredit;
@property (nonatomic, retain) NSString * raChecklistRemarks;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfRoomAssign : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *RoomAssign;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfRoomAssign *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addRoomAssign:(eHousekeepingService_RoomAssign *)toAdd;
@property (nonatomic, readonly) NSMutableArray * RoomAssign;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_RoomAssignmentList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_ArrayOfRoomAssign * RoomAssignmentList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_RoomAssignmentList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_ArrayOfRoomAssign * RoomAssignmentList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_FindRoomByRoomNoResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_RoomAssignmentList * FindRoomByRoomNoResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_FindRoomByRoomNoResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_RoomAssignmentList * FindRoomByRoomNoResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_UniqueRoom : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * strRoomNumber;
	NSNumber * intHotelId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_UniqueRoom *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * strRoomNumber;
@property (nonatomic, retain) NSNumber * intHotelId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_AmenitiesItem : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intDutyAssignmentId;
	eHousekeepingService_UniqueRoom * urRoom;
	NSNumber * intAmenitiesItemId;
	NSNumber * intNewQuantity;
	NSString * strTransactionTime;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_AmenitiesItem *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intDutyAssignmentId;
@property (nonatomic, retain) eHousekeepingService_UniqueRoom * urRoom;
@property (nonatomic, retain) NSNumber * intAmenitiesItemId;
@property (nonatomic, retain) NSNumber * intNewQuantity;
@property (nonatomic, retain) NSString * strTransactionTime;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfAmenitiesItem : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *AmenitiesItem;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfAmenitiesItem *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addAmenitiesItem:(eHousekeepingService_AmenitiesItem *)toAdd;
@property (nonatomic, readonly) NSMutableArray * AmenitiesItem;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostAmenitiesItemsList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUserId;
	eHousekeepingService_ArrayOfAmenitiesItem * amenitiesItemsList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostAmenitiesItemsList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUserId;
@property (nonatomic, retain) eHousekeepingService_ArrayOfAmenitiesItem * amenitiesItemsList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfInt : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *int_;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfInt *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addInt_:(NSNumber *)toAdd;
@property (nonatomic, readonly) NSMutableArray * int_;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostAmenitiesItems : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_ArrayOfInt * amenitiesItemsList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostAmenitiesItems *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_ArrayOfInt * amenitiesItemsList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostAmenitiesItemsListResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_PostAmenitiesItems * PostAmenitiesItemsListResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostAmenitiesItemsListResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_PostAmenitiesItems * PostAmenitiesItemsListResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetPanicAlertConfig : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSNumber * intHotelID;
	NSString * strLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetPanicAlertConfig *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSNumber * intHotelID;
@property (nonatomic, retain) NSString * strLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_Receiver : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * staffID;
	NSString * staffName;
	NSString * email;
	NSString * mobile;
	USBoolean * isDirectCall;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_Receiver *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * staffID;
@property (nonatomic, retain) NSString * staffName;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * mobile;
@property (nonatomic, retain) USBoolean * isDirectCall;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfReceiver : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *Receiver;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfReceiver *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addReceiver:(eHousekeepingService_Receiver *)toAdd;
@property (nonatomic, readonly) NSMutableArray * Receiver;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PanicAlertConfig : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	NSString * raPhone;
	NSString * supName;
	NSString * supStaffID;
	USBoolean * isAdhocMsg;
	USBoolean * isSMS;
	USBoolean * isEmail;
	USBoolean * isAlarmSound;
	USBoolean * isEConnectJob;
	NSString * msgSubject;
	NSString * msgContent;
	USBoolean * isDirectCall;
	eHousekeepingService_ArrayOfReceiver * receivers;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PanicAlertConfig *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) NSString * raPhone;
@property (nonatomic, retain) NSString * supName;
@property (nonatomic, retain) NSString * supStaffID;
@property (nonatomic, retain) USBoolean * isAdhocMsg;
@property (nonatomic, retain) USBoolean * isSMS;
@property (nonatomic, retain) USBoolean * isEmail;
@property (nonatomic, retain) USBoolean * isAlarmSound;
@property (nonatomic, retain) USBoolean * isEConnectJob;
@property (nonatomic, retain) NSString * msgSubject;
@property (nonatomic, retain) NSString * msgContent;
@property (nonatomic, retain) USBoolean * isDirectCall;
@property (nonatomic, retain) eHousekeepingService_ArrayOfReceiver * receivers;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetPanicAlertConfigResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_PanicAlertConfig * GetPanicAlertConfigResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetPanicAlertConfigResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_PanicAlertConfig * GetPanicAlertConfigResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostingPanicAlert : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSNumber * intHotelID;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostingPanicAlert *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSNumber * intHotelID;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostingPanicAlertResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_PanicAlertConfig * PostingPanicAlertResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostingPanicAlertResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_PanicAlertConfig * PostingPanicAlertResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetGuestInformation : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intHotelId;
	NSString * strRoomNumber;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetGuestInformation *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intHotelId;
@property (nonatomic, retain) NSString * strRoomNumber;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GuestRoomInformation : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * strHousekeeperName;
	NSNumber * intNoOfCurrentGuest;
	NSNumber * intNoOfArrivalGuest;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GuestRoomInformation *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * strHousekeeperName;
@property (nonatomic, retain) NSNumber * intNoOfCurrentGuest;
@property (nonatomic, retain) NSNumber * intNoOfArrivalGuest;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GuestProfileNoteDetail : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * strTypeCode;
	NSString * strTypeDescription;
	NSString * strNoteDescription;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GuestProfileNoteDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * strTypeCode;
@property (nonatomic, retain) NSString * strTypeDescription;
@property (nonatomic, retain) NSString * strNoteDescription;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfGuestProfileNoteDetail : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *GuestProfileNoteDetail;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfGuestProfileNoteDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addGuestProfileNoteDetail:(eHousekeepingService_GuestProfileNoteDetail *)toAdd;
@property (nonatomic, readonly) NSMutableArray * GuestProfileNoteDetail;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GuestInformation : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * strTitle;
	NSString * strName;
	NSString * strLangPreference;
	NSString * strCheckInDate;
	NSString * strCheckOutDate;
	NSString * strVIP;
	NSData * binaryPhoto;
	NSString * strLastModified;
	NSString * strSpecialService;
	NSString * strPreferenceCodes;
	eHousekeepingService_ArrayOfGuestProfileNoteDetail * mGuestProfileNoteList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GuestInformation *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * strTitle;
@property (nonatomic, retain) NSString * strName;
@property (nonatomic, retain) NSString * strLangPreference;
@property (nonatomic, retain) NSString * strCheckInDate;
@property (nonatomic, retain) NSString * strCheckOutDate;
@property (nonatomic, retain) NSString * strVIP;
@property (nonatomic, retain) NSData * binaryPhoto;
@property (nonatomic, retain) NSString * strLastModified;
@property (nonatomic, retain) NSString * strSpecialService;
@property (nonatomic, retain) NSString * strPreferenceCodes;
@property (nonatomic, retain) eHousekeepingService_ArrayOfGuestProfileNoteDetail * mGuestProfileNoteList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfGuestInformation : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *GuestInformation;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfGuestInformation *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addGuestInformation:(eHousekeepingService_GuestInformation *)toAdd;
@property (nonatomic, readonly) NSMutableArray * GuestInformation;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GuestInformationResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * mResponseStatus;
	eHousekeepingService_GuestRoomInformation * mGuestRoomInformation;
	eHousekeepingService_ArrayOfGuestInformation * mCurrentGuestList;
	eHousekeepingService_ArrayOfGuestInformation * mArrivalGuestList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GuestInformationResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * mResponseStatus;
@property (nonatomic, retain) eHousekeepingService_GuestRoomInformation * mGuestRoomInformation;
@property (nonatomic, retain) eHousekeepingService_ArrayOfGuestInformation * mCurrentGuestList;
@property (nonatomic, retain) eHousekeepingService_ArrayOfGuestInformation * mArrivalGuestList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetGuestInformationResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_GuestInformationResponse * GetGuestInformationResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetGuestInformationResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_GuestInformationResponse * GetGuestInformationResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetVersion : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetVersion *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_Version : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	NSString * WebServicesVersionNo;
	NSString * MinimumAndroidVersionNos;
	NSString * MinimumiOSVersionNos;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_Version *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) NSString * WebServicesVersionNo;
@property (nonatomic, retain) NSString * MinimumAndroidVersionNos;
@property (nonatomic, retain) NSString * MinimumiOSVersionNos;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetVersionResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_Version * GetVersionResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetVersionResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_Version * GetVersionResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetRoomRemarks : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSString * strRoomNo;
	NSNumber * intHTID;
	NSNumber * intRemarksType;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetRoomRemarks *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSString * strRoomNo;
@property (nonatomic, retain) NSNumber * intHTID;
@property (nonatomic, retain) NSNumber * intRemarksType;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetRoomRemarksList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	NSString * RoomNo;
	NSNumber * HTID;
	NSString * Remarks;
	NSString * LastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetRoomRemarksList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) NSString * RoomNo;
@property (nonatomic, retain) NSNumber * HTID;
@property (nonatomic, retain) NSString * Remarks;
@property (nonatomic, retain) NSString * LastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetRoomRemarksResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_GetRoomRemarksList * GetRoomRemarksResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetRoomRemarksResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_GetRoomRemarksList * GetRoomRemarksResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_UpdateRoomRemarks : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSString * strRoomNo;
	NSNumber * intHTID;
	NSNumber * intRemarksType;
	NSString * strRemarks;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_UpdateRoomRemarks *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSString * strRoomNo;
@property (nonatomic, retain) NSNumber * intHTID;
@property (nonatomic, retain) NSNumber * intRemarksType;
@property (nonatomic, retain) NSString * strRemarks;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_UpdateRoomRemarksResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * UpdateRoomRemarksResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_UpdateRoomRemarksResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * UpdateRoomRemarksResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetOOSBlockRoomsList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSNumber * intHTID;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetOOSBlockRoomsList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSNumber * intHTID;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_BlockRoomDetails : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * strRoomNo;
	NSNumber * intHTID;
	NSNumber * intRoomStatus;
	NSString * strOOSRemarks;
	NSString * strOOSReason;
	NSString * strOOSDuration;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_BlockRoomDetails *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * strRoomNo;
@property (nonatomic, retain) NSNumber * intHTID;
@property (nonatomic, retain) NSNumber * intRoomStatus;
@property (nonatomic, retain) NSString * strOOSRemarks;
@property (nonatomic, retain) NSString * strOOSReason;
@property (nonatomic, retain) NSString * strOOSDuration;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfBlockRoomDetails : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *BlockRoomDetails;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfBlockRoomDetails *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addBlockRoomDetails:(eHousekeepingService_BlockRoomDetails *)toAdd;
@property (nonatomic, readonly) NSMutableArray * BlockRoomDetails;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_OOSBlockRoomList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_ArrayOfBlockRoomDetails * BlockRoomList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_OOSBlockRoomList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_ArrayOfBlockRoomDetails * BlockRoomList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetOOSBlockRoomsListResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_OOSBlockRoomList * GetOOSBlockRoomsListResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetOOSBlockRoomsListResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_OOSBlockRoomList * GetOOSBlockRoomsListResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetReleaseRoomsList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSNumber * intHTID;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetReleaseRoomsList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSNumber * intHTID;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ReleaseRoomDetails : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * strRoomNo;
	NSNumber * intHTID;
	NSNumber * intRoomStatus;
	NSString * strRemarks;
	NSString * strReason;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ReleaseRoomDetails *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * strRoomNo;
@property (nonatomic, retain) NSNumber * intHTID;
@property (nonatomic, retain) NSNumber * intRoomStatus;
@property (nonatomic, retain) NSString * strRemarks;
@property (nonatomic, retain) NSString * strReason;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfReleaseRoomDetails : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *ReleaseRoomDetails;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfReleaseRoomDetails *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addReleaseRoomDetails:(eHousekeepingService_ReleaseRoomDetails *)toAdd;
@property (nonatomic, readonly) NSMutableArray * ReleaseRoomDetails;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ReleaseRoomList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_ArrayOfReleaseRoomDetails * ReleaseRoomsList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ReleaseRoomList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_ArrayOfReleaseRoomDetails * ReleaseRoomsList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetReleaseRoomsListResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ReleaseRoomList * GetReleaseRoomsListResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetReleaseRoomsListResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ReleaseRoomList * GetReleaseRoomsListResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetFindFloorDetailsRequest : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * UserID;
	NSNumber * BuildingID;
	NSNumber * HTID;
	NSNumber * FilterType;
	NSString * GeneralFilter;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetFindFloorDetailsRequest *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * UserID;
@property (nonatomic, retain) NSNumber * BuildingID;
@property (nonatomic, retain) NSNumber * HTID;
@property (nonatomic, retain) NSNumber * FilterType;
@property (nonatomic, retain) NSString * GeneralFilter;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetFindFloorDetailsList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_GetFindFloorDetailsRequest * GetFindFloorDetailsRequest;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetFindFloorDetailsList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_GetFindFloorDetailsRequest * GetFindFloorDetailsRequest;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_FloorDetails : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * ID_;
	NSString * Name;
	NSString * Lang;
	NSNumber * BuildingID;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_FloorDetails *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * ID_;
@property (nonatomic, retain) NSString * Name;
@property (nonatomic, retain) NSString * Lang;
@property (nonatomic, retain) NSNumber * BuildingID;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfFloorDetails : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *FloorDetails;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfFloorDetails *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addFloorDetails:(eHousekeepingService_FloorDetails *)toAdd;
@property (nonatomic, readonly) NSMutableArray * FloorDetails;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetFindFloorDetailsResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_ArrayOfFloorDetails * FloorListing;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetFindFloorDetailsResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_ArrayOfFloorDetails * FloorListing;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetFindFloorDetailsListResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_GetFindFloorDetailsResponse * GetFindFloorDetailsListResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetFindFloorDetailsListResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_GetFindFloorDetailsResponse * GetFindFloorDetailsListResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetFindRoomDetailsRequest : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * UserID;
	NSString * FloorID;
	NSNumber * BuildingID;
	NSNumber * HTID;
	NSNumber * FilterType;
	NSString * GeneralFilter;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetFindRoomDetailsRequest *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * UserID;
@property (nonatomic, retain) NSString * FloorID;
@property (nonatomic, retain) NSNumber * BuildingID;
@property (nonatomic, retain) NSNumber * HTID;
@property (nonatomic, retain) NSNumber * FilterType;
@property (nonatomic, retain) NSString * GeneralFilter;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetFindRoomDetailsList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_GetFindRoomDetailsRequest * GetFindRoomDetailsRequest;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetFindRoomDetailsList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_GetFindRoomDetailsRequest * GetFindRoomDetailsRequest;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_RoomAssignmentDetails : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * ID_;
	NSString * RoomNo;
	NSString * FloorID;
	NSNumber * BuildingID;
	NSString * BuildingName;
	NSString * BuildingLang;
	NSNumber * HTID;
	NSNumber * RoomStatusID;
	NSNumber * CleaningStatusID;
	NSNumber * RoomTypeID;
	NSNumber * HousekeeperID;
	NSString * HousekeeperFullname;
	NSString * AssignedTime;
	NSString * CleaningStartTime;
	NSString * CleaningEndTime;
	NSString * ExpectedCleaningTime;
    NSString * CleaningCredit;
    NSString * ChecklistRemarks;
	NSNumber * TotalCleaningTime;
	NSNumber * InspectionStatusID;
	NSString * InspectionStartTime;
	NSString * InspectionEndTime;
	NSString * InspectionScore;
	NSNumber * ExpectedInspectionTime;
	NSNumber * TotalInspectionTime;
	NSString * LastRoomCleaningDate;
	NSString * LastModified;
	NSNumber * KindOfRoom;
	NSNumber * IsMockRoom;
	NSNumber * GuestProfileID;
	NSString * GuestFirstName;
	NSString * GuestLastName;
	NSString * GuestVIPCode;
	NSString * GuestArrivalTime;
	NSString * GuestDepartureTime;
	NSString * GuestPreference;
	NSString * GuestName;
	NSString * Priority;
	NSString * Remark;
	NSNumber * PrioritySortOrder;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_RoomAssignmentDetails *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * ID_;
@property (nonatomic, retain) NSString * RoomNo;
@property (nonatomic, retain) NSString * FloorID;
@property (nonatomic, retain) NSNumber * BuildingID;
@property (nonatomic, retain) NSString * BuildingName;
@property (nonatomic, retain) NSString * BuildingLang;
@property (nonatomic, retain) NSNumber * HTID;
@property (nonatomic, retain) NSNumber * RoomStatusID;
@property (nonatomic, retain) NSNumber * CleaningStatusID;
@property (nonatomic, retain) NSNumber * RoomTypeID;
@property (nonatomic, retain) NSNumber * HousekeeperID;
@property (nonatomic, retain) NSString * HousekeeperFullname;
@property (nonatomic, retain) NSString * AssignedTime;
@property (nonatomic, retain) NSString * CleaningStartTime;
@property (nonatomic, retain) NSString * CleaningEndTime;
@property (nonatomic, retain) NSString * ExpectedCleaningTime;
@property (nonatomic, retain) NSString * CleaningCredit;
@property (nonatomic, retain) NSString * ChecklistRemarks;
@property (nonatomic, retain) NSNumber * TotalCleaningTime;
@property (nonatomic, retain) NSNumber * InspectionStatusID;
@property (nonatomic, retain) NSString * InspectionStartTime;
@property (nonatomic, retain) NSString * InspectionEndTime;
@property (nonatomic, retain) NSString * InspectionScore;
@property (nonatomic, retain) NSNumber * ExpectedInspectionTime;
@property (nonatomic, retain) NSNumber * TotalInspectionTime;
@property (nonatomic, retain) NSString * LastRoomCleaningDate;
@property (nonatomic, retain) NSString * LastModified;
@property (nonatomic, retain) NSNumber * KindOfRoom;
@property (nonatomic, retain) NSNumber * IsMockRoom;
@property (nonatomic, retain) NSNumber * GuestProfileID;
@property (nonatomic, retain) NSString * GuestFirstName;
@property (nonatomic, retain) NSString * GuestLastName;
@property (nonatomic, retain) NSString * GuestVIPCode;
@property (nonatomic, retain) NSString * GuestArrivalTime;
@property (nonatomic, retain) NSString * GuestDepartureTime;
@property (nonatomic, retain) NSString * GuestPreference;
@property (nonatomic, retain) NSString * GuestName;
@property (nonatomic, retain) NSString * Priority;
@property (nonatomic, retain) NSString * Remark;
@property (nonatomic, retain) NSNumber * PrioritySortOrder;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfRoomAssignmentDetails : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *RoomAssignmentDetails;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfRoomAssignmentDetails *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addRoomAssignmentDetails:(eHousekeepingService_RoomAssignmentDetails *)toAdd;
@property (nonatomic, readonly) NSMutableArray * RoomAssignmentDetails;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetFindRoomDetailsResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_ArrayOfRoomAssignmentDetails * RoomAssignmentListing;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetFindRoomDetailsResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_ArrayOfRoomAssignmentDetails * RoomAssignmentListing;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetFindRoomDetailsListResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_GetFindRoomDetailsResponse * GetFindRoomDetailsListResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetFindRoomDetailsListResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_GetFindRoomDetailsResponse * GetFindRoomDetailsListResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetStillLoggedIn : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSString * strDeviceNo;
	NSNumber * intDeviceType;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetStillLoggedIn *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSString * strDeviceNo;
@property (nonatomic, retain) NSNumber * intDeviceType;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetStillLoggedInResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * GetStillLoggedInResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetStillLoggedInResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * GetStillLoggedInResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostLogout : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostLogout *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostLogoutResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * PostLogoutResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostLogoutResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * PostLogoutResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_LogInvalidStartRoom : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intPrevious_DAID;
	NSNumber * intNew_DAID;
	NSString * strPrevious_RoomNo;
	NSString * strNew_RoomNo;
	NSString * strUser_ID;
	NSString * strUser_Name;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_LogInvalidStartRoom *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intPrevious_DAID;
@property (nonatomic, retain) NSNumber * intNew_DAID;
@property (nonatomic, retain) NSString * strPrevious_RoomNo;
@property (nonatomic, retain) NSString * strNew_RoomNo;
@property (nonatomic, retain) NSString * strUser_ID;
@property (nonatomic, retain) NSString * strUser_Name;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_LogInvalidStartRoomResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * LogInvalidStartRoomResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_LogInvalidStartRoomResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * LogInvalidStartRoomResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_AddReassignRoom : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSString * strRoomNo;
	NSString * strAssignDateTime;
	NSNumber * intExpectedCleaningTime;
	NSNumber * intRoomStatus;
	NSNumber * intCleaningStatus;
	NSNumber * intHousekeeperID;
	NSNumber * intRoomTypeID;
	NSNumber * intFloorID;
	NSNumber * intGuestProfileID;
	NSNumber * intHotelID;
	NSNumber * intDutyAssignmentID;
	NSString * strRemark;
    NSString * CleaningCredit;
    NSString * ChecklistRemarks;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_AddReassignRoom *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSString * strRoomNo;
@property (nonatomic, retain) NSString * strAssignDateTime;
@property (nonatomic, retain) NSNumber * intExpectedCleaningTime;
@property (nonatomic, retain) NSNumber * intRoomStatus;
@property (nonatomic, retain) NSNumber * intCleaningStatus;
@property (nonatomic, retain) NSNumber * intHousekeeperID;
@property (nonatomic, retain) NSNumber * intRoomTypeID;
@property (nonatomic, retain) NSNumber * intFloorID;
@property (nonatomic, retain) NSNumber * intGuestProfileID;
@property (nonatomic, retain) NSNumber * intHotelID;
@property (nonatomic, retain) NSNumber * intDutyAssignmentID;
@property (nonatomic, retain) NSString * strRemark;
@property (nonatomic, retain) NSString * CleaningCredit;
@property (nonatomic, retain) NSString * ChecklistRemarks;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_AddReassignRoomResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * AddReassignRoomResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_AddReassignRoomResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * AddReassignRoomResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_AssignRoom : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intHousekeeperID;
	NSNumber * intSupervisorID;
	NSNumber * intDutyAssignID;
	NSString * strAssignDateTime;
	NSString * strRemark;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_AssignRoom *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intHousekeeperID;
@property (nonatomic, retain) NSNumber * intSupervisorID;
@property (nonatomic, retain) NSNumber * intDutyAssignID;
@property (nonatomic, retain) NSString * strAssignDateTime;
@property (nonatomic, retain) NSString * strRemark;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_AssignRoomResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * AssignRoomResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_AssignRoomResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * AssignRoomResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetSupervisorFiltersRoutine : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * UserID;
	NSString * BuildingID;
	NSString * HTID;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetSupervisorFiltersRoutine *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * UserID;
@property (nonatomic, retain) NSString * BuildingID;
@property (nonatomic, retain) NSString * HTID;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_SupervisorFiltersDetails : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * Type;
	NSString * ID_;
	NSString * Name;
	NSString * Lang;
	NSString * RoomNo;
	NSString * HotelID;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_SupervisorFiltersDetails *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * Type;
@property (nonatomic, retain) NSString * ID_;
@property (nonatomic, retain) NSString * Name;
@property (nonatomic, retain) NSString * Lang;
@property (nonatomic, retain) NSString * RoomNo;
@property (nonatomic, retain) NSString * HotelID;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfSupervisorFiltersDetails : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *SupervisorFiltersDetails;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfSupervisorFiltersDetails *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addSupervisorFiltersDetails:(eHousekeepingService_SupervisorFiltersDetails *)toAdd;
@property (nonatomic, readonly) NSMutableArray * SupervisorFiltersDetails;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetSupervisorFiltersResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_ArrayOfSupervisorFiltersDetails * SupervisorFiltersListing;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetSupervisorFiltersResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_ArrayOfSupervisorFiltersDetails * SupervisorFiltersListing;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetSupervisorFiltersRoutineResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_GetSupervisorFiltersResponse * GetSupervisorFiltersRoutineResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetSupervisorFiltersRoutineResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_GetSupervisorFiltersResponse * GetSupervisorFiltersRoutineResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetPostingHistoryRoutine : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * UserID;
	NSString * RoomNo;
	NSString * HTID;
	NSString * ModuleID;
	NSString * DateFilter;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetPostingHistoryRoutine *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * UserID;
@property (nonatomic, retain) NSString * RoomNo;
@property (nonatomic, retain) NSString * HTID;
@property (nonatomic, retain) NSString * ModuleID;
@property (nonatomic, retain) NSString * DateFilter;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostingHistoryDetails : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * UserID;
	NSString * RoomNo;
	NSString * HotelID;
	NSString * ModuleID;
	NSString * CategoryDescription;
	NSString * ItemDescription;
	NSString * NewQuantity;
	NSString * UsedQuantity;
	NSString * TransactionDateTime;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostingHistoryDetails *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * UserID;
@property (nonatomic, retain) NSString * RoomNo;
@property (nonatomic, retain) NSString * HotelID;
@property (nonatomic, retain) NSString * ModuleID;
@property (nonatomic, retain) NSString * CategoryDescription;
@property (nonatomic, retain) NSString * UserName;
@property (nonatomic, retain) NSString * ItemDescription;
@property (nonatomic, retain) NSString * NewQuantity;
@property (nonatomic, retain) NSString * UsedQuantity;
@property (nonatomic, retain) NSString * TransactionDateTime;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfPostingHistoryDetails : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *PostingHistoryDetails;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfPostingHistoryDetails *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addPostingHistoryDetails:(eHousekeepingService_PostingHistoryDetails *)toAdd;
@property (nonatomic, readonly) NSMutableArray * PostingHistoryDetails;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetPostingHistoryResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_ArrayOfPostingHistoryDetails * PostingHistoryListing;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetPostingHistoryResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_ArrayOfPostingHistoryDetails * PostingHistoryListing;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetPostingHistoryRoutineResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_GetPostingHistoryResponse * GetPostingHistoryRoutineResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetPostingHistoryRoutineResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_GetPostingHistoryResponse * GetPostingHistoryRoutineResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostPostingHistoryRoutine : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ArrayOfPostingHistoryDetails * PostingHistoryListing;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostPostingHistoryRoutine *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ArrayOfPostingHistoryDetails * PostingHistoryListing;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostPostingHistoryResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostPostingHistoryResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostPostingHistoryRoutineResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_PostPostingHistoryResponse * PostPostingHistoryRoutineResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostPostingHistoryRoutineResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_PostPostingHistoryResponse * PostPostingHistoryRoutineResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ImageAttachmentDetails : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * Content;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ImageAttachmentDetails *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * Content;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfImageAttachmentDetails : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *ImageAttachmentDetails;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfImageAttachmentDetails *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addImageAttachmentDetails:(eHousekeepingService_ImageAttachmentDetails *)toAdd;
@property (nonatomic, readonly) NSMutableArray * ImageAttachmentDetails;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_LostAndFoundItemDetails : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * UserID;
	NSString * RoomNo;
	NSString * HotelID;
	NSString * DutyAssignmentID;
	NSString * CategoryID;
	NSString * ItemID;
	NSString * ColorID;
	NSString * Quantity;
	NSString * FoundDateTime;
	NSString * Remarks;
	eHousekeepingService_ArrayOfImageAttachmentDetails * ImageAttachmentListing;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_LostAndFoundItemDetails *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * UserID;
@property (nonatomic, retain) NSString * RoomNo;
@property (nonatomic, retain) NSString * HotelID;
@property (nonatomic, retain) NSString * DutyAssignmentID;
@property (nonatomic, retain) NSString * CategoryID;
@property (nonatomic, retain) NSString * ItemID;
@property (nonatomic, retain) NSString * ColorID;
@property (nonatomic, retain) NSString * Quantity;
@property (nonatomic, retain) NSString * FoundDateTime;
@property (nonatomic, retain) NSString * Remarks;
@property (nonatomic, retain) eHousekeepingService_ArrayOfImageAttachmentDetails * ImageAttachmentListing;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfLostAndFoundItemDetails : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *LostAndFoundItemDetails;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfLostAndFoundItemDetails *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addLostAndFoundItemDetails:(eHousekeepingService_LostAndFoundItemDetails *)toAdd;
@property (nonatomic, readonly) NSMutableArray * LostAndFoundItemDetails;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostLostAndFoundRoutine : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ArrayOfLostAndFoundItemDetails * LostAndFoundItemsListing;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostLostAndFoundRoutine *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ArrayOfLostAndFoundItemDetails * LostAndFoundItemsListing;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostLostAndFoundResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostLostAndFoundResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostLostAndFoundRoutineResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_PostLostAndFoundResponse * PostLostAndFoundRoutineResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostLostAndFoundRoutineResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_PostLostAndFoundResponse * PostLostAndFoundRoutineResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetAdditionalJobStatusesRoutine : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetAdditionalJobStatusesRoutine *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_AdditionalJobStatusDetails : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * ID_;
	NSString * Name;
	NSString * Lang;
	NSString * Image;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_AdditionalJobStatusDetails *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * ID_;
@property (nonatomic, retain) NSString * Name;
@property (nonatomic, retain) NSString * Lang;
@property (nonatomic, retain) NSString * Image;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfAdditionalJobStatusDetails : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *AdditionalJobStatusDetails;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfAdditionalJobStatusDetails *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addAdditionalJobStatusDetails:(eHousekeepingService_AdditionalJobStatusDetails *)toAdd;
@property (nonatomic, readonly) NSMutableArray * AdditionalJobStatusDetails;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetAdditionalJobStatusesResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_ArrayOfAdditionalJobStatusDetails * AdditionalJobStatusListing;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetAdditionalJobStatusesResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_ArrayOfAdditionalJobStatusDetails * AdditionalJobStatusListing;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetAdditionalJobStatusesRoutineResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_GetAdditionalJobStatusesResponse * GetAdditionalJobStatusesRoutineResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetAdditionalJobStatusesRoutineResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_GetAdditionalJobStatusesResponse * GetAdditionalJobStatusesRoutineResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostAdditionalJobStatusUpdateRoutine : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * UserID;
	NSString * JobID;
	NSString * StatusID;
	NSString * ServiceLaterDateTime;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostAdditionalJobStatusUpdateRoutine *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * UserID;
@property (nonatomic, retain) NSString * JobID;
@property (nonatomic, retain) NSString * StatusID;
@property (nonatomic, retain) NSString * ServiceLaterDateTime;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostAdditionalJobStatusUpdateResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostAdditionalJobStatusUpdateResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostAdditionalJobStatusUpdateRoutineResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_PostAdditionalJobStatusUpdateResponse * PostAdditionalJobStatusUpdateRoutineResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostAdditionalJobStatusUpdateRoutineResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_PostAdditionalJobStatusUpdateResponse * PostAdditionalJobStatusUpdateRoutineResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_FindGuestStayedHistoryRoutine : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * UserID;
	NSString * Search;
	NSString * PropertyID;
	NSString * BuildingID;
	NSString * FilterStatus;
	NSString * FilterRange;
	NSString * PageNumber;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_FindGuestStayedHistoryRoutine *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * UserID;
@property (nonatomic, retain) NSString * Search;
@property (nonatomic, retain) NSString * PropertyID;
@property (nonatomic, retain) NSString * BuildingID;
@property (nonatomic, retain) NSString * FilterStatus;
@property (nonatomic, retain) NSString * FilterRange;
@property (nonatomic, retain) NSString * PageNumber;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_FindGuestInformation : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * RoomNo;
	NSString * RoomStatus;
	NSString * HousekeeperName;
	NSString * NoOfGuest;
	NSString * Title;
	NSString * Name;
	NSString * LangPreference;
	NSString * CheckInDate;
	NSString * CheckOutDate;
	NSString * VIP;
	NSString * SpecialService;
	NSString * PreferenceCodes;
	eHousekeepingService_ArrayOfGuestProfileNoteDetail * mGuestProfileNoteList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_FindGuestInformation *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * RoomNo;
@property (nonatomic, retain) NSString * RoomStatus;
@property (nonatomic, retain) NSString * HousekeeperName;
@property (nonatomic, retain) NSString * NoOfGuest;
@property (nonatomic, retain) NSString * Title;
@property (nonatomic, retain) NSString * Name;
@property (nonatomic, retain) NSString * LangPreference;
@property (nonatomic, retain) NSString * CheckInDate;
@property (nonatomic, retain) NSString * CheckOutDate;
@property (nonatomic, retain) NSString * VIP;
@property (nonatomic, retain) NSString * SpecialService;
@property (nonatomic, retain) NSString * PreferenceCodes;
@property (nonatomic, retain) eHousekeepingService_ArrayOfGuestProfileNoteDetail * mGuestProfileNoteList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfFindGuestInformation : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *FindGuestInformation;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfFindGuestInformation *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addFindGuestInformation:(eHousekeepingService_FindGuestInformation *)toAdd;
@property (nonatomic, readonly) NSMutableArray * FindGuestInformation;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_FindGuestStayHistoryResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatuses;
	eHousekeepingService_ArrayOfFindGuestInformation * FindGuestInformationList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_FindGuestStayHistoryResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatuses;
@property (nonatomic, retain) eHousekeepingService_ArrayOfFindGuestInformation * FindGuestInformationList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_FindGuestStayedHistoryRoutineResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_FindGuestStayHistoryResponse * FindGuestStayedHistoryRoutineResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_FindGuestStayedHistoryRoutineResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_FindGuestStayHistoryResponse * FindGuestStayedHistoryRoutineResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostUnassignRoomRoutine : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * UserID;
	NSString * DutyAssignmentID;
	NSString * PropertyID;
	NSString * CleaningStatus;
	NSString * Remark;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostUnassignRoomRoutine *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * UserID;
@property (nonatomic, retain) NSString * DutyAssignmentID;
@property (nonatomic, retain) NSString * PropertyID;
@property (nonatomic, retain) NSString * CleaningStatus;
@property (nonatomic, retain) NSString * Remark;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostUnassignRoomRoutineResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * PostUnassignRoomRoutineResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostUnassignRoomRoutineResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * PostUnassignRoomRoutineResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostUpdateRoomDetailsRoutine : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * UserID;
	NSString * RoomNo;
	NSString * PropertyID;
	NSString * UpdateType;
	NSString * RoomStatusID;
	NSString * CleaningStatusID;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostUpdateRoomDetailsRoutine *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * UserID;
@property (nonatomic, retain) NSString * RoomNo;
@property (nonatomic, retain) NSString * PropertyID;
@property (nonatomic, retain) NSString * UpdateType;
@property (nonatomic, retain) NSString * RoomStatusID;
@property (nonatomic, retain) NSString * CleaningStatusID;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostPhysicalCheck : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatuses;
	eHousekeepingService_ArrayOfFloorDetails * FloorInfo;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostPhysicalCheck *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatuses;
@property (nonatomic, retain) eHousekeepingService_ArrayOfFloorDetails * FloorInfo;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostUpdateRoomDetailsRoutineResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_PostPhysicalCheck * PostUpdateRoomDetailsRoutineResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostUpdateRoomDetailsRoutineResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_PostPhysicalCheck * PostUpdateRoomDetailsRoutineResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetRoomTypeInventoryRoutine : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * UserID;
	NSString * PropertyID;
	NSString * ServiceID;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetRoomTypeInventoryRoutine *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * UserID;
@property (nonatomic, retain) NSString * PropertyID;
@property (nonatomic, retain) NSString * ServiceID;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_RoomTypeInventoryDetails : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * ServiceItem;
	NSString * InventoryID;
	NSString * CategoryID;
	NSString * RoomTypeID;
	NSString * BuildingID;
	NSString * SectionID;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_RoomTypeInventoryDetails *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * ServiceItem;
@property (nonatomic, retain) NSString * InventoryID;
@property (nonatomic, retain) NSString * CategoryID;
@property (nonatomic, retain) NSString * RoomTypeID;
@property (nonatomic, retain) NSString * BuildingID;
@property (nonatomic, retain) NSString * SectionID;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfRoomTypeInventoryDetails : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *RoomTypeInventoryDetails;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfRoomTypeInventoryDetails *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addRoomTypeInventoryDetails:(eHousekeepingService_RoomTypeInventoryDetails *)toAdd;
@property (nonatomic, readonly) NSMutableArray * RoomTypeInventoryDetails;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_RoomTypeInventoryResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatuses;
	eHousekeepingService_ArrayOfRoomTypeInventoryDetails * RoomTypeInventoryListing;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_RoomTypeInventoryResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatuses;
@property (nonatomic, retain) eHousekeepingService_ArrayOfRoomTypeInventoryDetails * RoomTypeInventoryListing;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetRoomTypeInventoryRoutineResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_RoomTypeInventoryResponse * GetRoomTypeInventoryRoutineResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetRoomTypeInventoryRoutineResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_RoomTypeInventoryResponse * GetRoomTypeInventoryRoutineResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetRoomTypeByRoomNoRoutine : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * UserID;
	NSString * RoomNo;
	NSString * PropertyID;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetRoomTypeByRoomNoRoutine *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * UserID;
@property (nonatomic, retain) NSString * RoomNo;
@property (nonatomic, retain) NSString * PropertyID;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_RoomType : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * rtID;
	NSString * rtName;
	NSString * rtLang;
	NSString * rtLastModifed;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_RoomType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * rtID;
@property (nonatomic, retain) NSString * rtName;
@property (nonatomic, retain) NSString * rtLang;
@property (nonatomic, retain) NSString * rtLastModifed;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfRoomType : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *RoomType;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfRoomType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addRoomType:(eHousekeepingService_RoomType *)toAdd;
@property (nonatomic, readonly) NSMutableArray * RoomType;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_RoomTypeList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_ArrayOfRoomType * RoomTypeList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_RoomTypeList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_ArrayOfRoomType * RoomTypeList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetRoomTypeByRoomNoRoutineResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_RoomTypeList * GetRoomTypeByRoomNoRoutineResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetRoomTypeByRoomNoRoutineResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_RoomTypeList * GetRoomTypeByRoomNoRoutineResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_MessageUpdateDetails : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * UserID;
	NSString * MessageID;
	NSString * PropertyID;
	NSString * UpdateType;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_MessageUpdateDetails *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * UserID;
@property (nonatomic, retain) NSString * MessageID;
@property (nonatomic, retain) NSString * PropertyID;
@property (nonatomic, retain) NSString * UpdateType;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfMessageUpdateDetails : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *MessageUpdateDetails;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfMessageUpdateDetails *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addMessageUpdateDetails:(eHousekeepingService_MessageUpdateDetails *)toAdd;
@property (nonatomic, readonly) NSMutableArray * MessageUpdateDetails;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostUpdateAdHocMessageRoutine : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ArrayOfMessageUpdateDetails * MessageUpdateListing;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostUpdateAdHocMessageRoutine *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ArrayOfMessageUpdateDetails * MessageUpdateListing;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_UpdateAdHocMessageResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_UpdateAdHocMessageResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostUpdateAdHocMessageRoutineResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_UpdateAdHocMessageResponse * PostUpdateAdHocMessageRoutineResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostUpdateAdHocMessageRoutineResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_UpdateAdHocMessageResponse * PostUpdateAdHocMessageRoutineResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetNoOfAdHocMessageRoutine : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * UserID;
	NSString * PropertyID;
	NSString * GroupMessageID;
	NSString * LastModifiedDateTime;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetNoOfAdHocMessageRoutine *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * UserID;
@property (nonatomic, retain) NSString * PropertyID;
@property (nonatomic, retain) NSString * GroupMessageID;
@property (nonatomic, retain) NSString * LastModifiedDateTime;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetNoOfAdHocMessageRoutineResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_AdHocMessageCounter * GetNoOfAdHocMessageRoutineResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetNoOfAdHocMessageRoutineResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_AdHocMessageCounter * GetNoOfAdHocMessageRoutineResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetReceivedAdHocMessageRoutine : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * UserID;
	NSString * GroupMessageID;
	NSString * PropertyID;
	NSString * LastModifiedDateTime;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetReceivedAdHocMessageRoutine *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * UserID;
@property (nonatomic, retain) NSString * GroupMessageID;
@property (nonatomic, retain) NSString * PropertyID;
@property (nonatomic, retain) NSString * LastModifiedDateTime;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetReceivedAdHocMessageRoutineResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_AdHocMsgList2 * GetReceivedAdHocMessageRoutineResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetReceivedAdHocMessageRoutineResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_AdHocMsgList2 * GetReceivedAdHocMessageRoutineResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetSendAdHocMessageRoutine : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * UserID;
	NSString * GroupMessageID;
	NSString * PropertyID;
	NSString * LastModifiedDateTime;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetSendAdHocMessageRoutine *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * UserID;
@property (nonatomic, retain) NSString * GroupMessageID;
@property (nonatomic, retain) NSString * PropertyID;
@property (nonatomic, retain) NSString * LastModifiedDateTime;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetSendAdHocMessageRoutineResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_AdHocMsgList2 * GetSendAdHocMessageRoutineResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetSendAdHocMessageRoutineResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_AdHocMsgList2 * GetSendAdHocMessageRoutineResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetOtherActivityAssignmentRoutine : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * UserID;
	NSString * PropertyID;
	NSString * BuildingID;
	NSString * DateTime;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetOtherActivityAssignmentRoutine *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * UserID;
@property (nonatomic, retain) NSString * PropertyID;
@property (nonatomic, retain) NSString * BuildingID;
@property (nonatomic, retain) NSString * DateTime;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_OtherActivityDetails : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * OnDayID;
	NSNumber * OnDayActivityID;
	NSNumber * OnDayActivitiesID;
	NSString * OnDayActivitiesName;
	NSString * OnDayActivitiesNameLang;
	NSString * OnDayActivitiesCode;
	NSString * OnDayActivitiesDesc;
	NSString * OnDayActivitiesDescLang;
	NSNumber * OnDayLocationID;
	NSNumber * OnDayStatusID;
	NSString * OnDayRemark;
	NSNumber * OnDayDuration;
	NSNumber * OnDayReminder;
	NSNumber * OnDayRemind;
	NSString * OnDayAssignDate;
	NSString * OnDayStartTime;
	NSString * OnDayEndTime;
	NSString * OnDayStaffStartTime;
	NSString * OnDayStaffEndTime;
	NSString * OnDayStaffStopTime;
	NSString * OnDayDeclineServiceTime;
	NSNumber * OnDayStaffID;
	NSNumber * OnDayIsSingle;
	NSNumber * OnDayDeleted;
	NSNumber * OnDayDurationSpent;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_OtherActivityDetails *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * OnDayID;
@property (nonatomic, retain) NSNumber * OnDayActivityID;
@property (nonatomic, retain) NSNumber * OnDayActivitiesID;
@property (nonatomic, retain) NSString * OnDayActivitiesName;
@property (nonatomic, retain) NSString * OnDayActivitiesNameLang;
@property (nonatomic, retain) NSString * OnDayActivitiesCode;
@property (nonatomic, retain) NSString * OnDayActivitiesDesc;
@property (nonatomic, retain) NSString * OnDayActivitiesDescLang;
@property (nonatomic, retain) NSNumber * OnDayLocationID;
@property (nonatomic, retain) NSNumber * OnDayStatusID;
@property (nonatomic, retain) NSString * OnDayRemark;
@property (nonatomic, retain) NSNumber * OnDayDuration;
@property (nonatomic, retain) NSNumber * OnDayReminder;
@property (nonatomic, retain) NSNumber * OnDayRemind;
@property (nonatomic, retain) NSString * OnDayAssignDate;
@property (nonatomic, retain) NSString * OnDayStartTime;
@property (nonatomic, retain) NSString * OnDayEndTime;
@property (nonatomic, retain) NSString * OnDayStaffStartTime;
@property (nonatomic, retain) NSString * OnDayStaffEndTime;
@property (nonatomic, retain) NSString * OnDayStaffStopTime;
@property (nonatomic, retain) NSString * OnDayDeclineServiceTime;
@property (nonatomic, retain) NSNumber * OnDayStaffID;
@property (nonatomic, retain) NSNumber * OnDayIsSingle;
@property (nonatomic, retain) NSNumber * OnDayDeleted;
@property (nonatomic, retain) NSNumber * OnDayDurationSpent;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfOtherActivityDetails : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *OtherActivityDetails;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfOtherActivityDetails *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addOtherActivityDetails:(eHousekeepingService_OtherActivityDetails *)toAdd;
@property (nonatomic, readonly) NSMutableArray * OtherActivityDetails;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_OtherActivityDetailsList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * Response;
	eHousekeepingService_ArrayOfOtherActivityDetails * OtherActvityDetailsListing;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_OtherActivityDetailsList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * Response;
@property (nonatomic, retain) eHousekeepingService_ArrayOfOtherActivityDetails * OtherActvityDetailsListing;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetOtherActivityAssignmentRoutineResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_OtherActivityDetailsList * GetOtherActivityAssignmentRoutineResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetOtherActivityAssignmentRoutineResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_OtherActivityDetailsList * GetOtherActivityAssignmentRoutineResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetOtherActivitiesStatusRoutine : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * UserID;
	NSString * LastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetOtherActivitiesStatusRoutine *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * UserID;
@property (nonatomic, retain) NSString * LastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_OtherActivityStatus : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * StatusID;
	NSString * StatusName;
	NSString * StatusLang;
	NSData * StatusImage;
	NSString * StatusLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_OtherActivityStatus *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * StatusID;
@property (nonatomic, retain) NSString * StatusName;
@property (nonatomic, retain) NSString * StatusLang;
@property (nonatomic, retain) NSData * StatusImage;
@property (nonatomic, retain) NSString * StatusLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfOtherActivityStatus : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *OtherActivityStatus;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfOtherActivityStatus *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addOtherActivityStatus:(eHousekeepingService_OtherActivityStatus *)toAdd;
@property (nonatomic, readonly) NSMutableArray * OtherActivityStatus;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_OtherActivityStatusList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * Response;
	eHousekeepingService_ArrayOfOtherActivityStatus * ActivityStatusList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_OtherActivityStatusList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * Response;
@property (nonatomic, retain) eHousekeepingService_ArrayOfOtherActivityStatus * ActivityStatusList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetOtherActivitiesStatusRoutineResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_OtherActivityStatusList * GetOtherActivitiesStatusRoutineResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetOtherActivitiesStatusRoutineResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_OtherActivityStatusList * GetOtherActivitiesStatusRoutineResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetOtherActivitiesLocationRoutine : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * UserID;
	NSString * PropertyID;
	NSString * LastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetOtherActivitiesLocationRoutine *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * UserID;
@property (nonatomic, retain) NSString * PropertyID;
@property (nonatomic, retain) NSString * LastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_OtherActivityLocation : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * LocationID;
	NSString * LocationName;
	NSString * LocationNameLang;
	NSString * LocationCode;
	NSString * LocationDesc;
	NSString * LocationDescLang;
	NSString * LocationHotelID;
	NSString * LocationLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_OtherActivityLocation *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * LocationID;
@property (nonatomic, retain) NSString * LocationName;
@property (nonatomic, retain) NSString * LocationNameLang;
@property (nonatomic, retain) NSString * LocationCode;
@property (nonatomic, retain) NSString * LocationDesc;
@property (nonatomic, retain) NSString * LocationDescLang;
@property (nonatomic, retain) NSString * LocationHotelID;
@property (nonatomic, retain) NSString * LocationLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfOtherActivityLocation : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *OtherActivityLocation;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfOtherActivityLocation *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addOtherActivityLocation:(eHousekeepingService_OtherActivityLocation *)toAdd;
@property (nonatomic, readonly) NSMutableArray * OtherActivityLocation;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_OtherActivityLocationList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * Response;
	eHousekeepingService_ArrayOfOtherActivityLocation * ActivityLocationList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_OtherActivityLocationList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * Response;
@property (nonatomic, retain) eHousekeepingService_ArrayOfOtherActivityLocation * ActivityLocationList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetOtherActivitiesLocationRoutineResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_OtherActivityLocationList * GetOtherActivitiesLocationRoutineResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetOtherActivitiesLocationRoutineResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_OtherActivityLocationList * GetOtherActivitiesLocationRoutineResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostUpdateOtherActivityStatusRoutine : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * UserID;
	NSString * ActivityID;
	NSString * StatusID;
	NSString * Time;
	NSString * Remark;
	NSString * DurationSpent;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostUpdateOtherActivityStatusRoutine *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * UserID;
@property (nonatomic, retain) NSString * ActivityID;
@property (nonatomic, retain) NSString * StatusID;
@property (nonatomic, retain) NSString * Time;
@property (nonatomic, retain) NSString * Remark;
@property (nonatomic, retain) NSString * DurationSpent;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostOtherActivityStatusUpdateResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostOtherActivityStatusUpdateResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostUpdateOtherActivityStatusRoutineResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_PostOtherActivityStatusUpdateResponse * PostUpdateOtherActivityStatusRoutineResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostUpdateOtherActivityStatusRoutineResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_PostOtherActivityStatusUpdateResponse * PostUpdateOtherActivityStatusRoutineResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetLanguageList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * strLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetLanguageList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * strLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_LanguageItem : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * lgCode;
	NSString * lgName;
	NSString * lgXmlUrl;
	NSString * lgLastModified;
	NSString * lgIsActive;
	NSString * lgCurrencySymbol;
	NSString * lgCurrencyNoDigitAfterDecimal;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_LanguageItem *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * lgCode;
@property (nonatomic, retain) NSString * lgName;
@property (nonatomic, retain) NSString * lgXmlUrl;
@property (nonatomic, retain) NSString * lgLastModified;
@property (nonatomic, retain) NSString * lgIsActive;
@property (nonatomic, retain) NSString * lgCurrencySymbol;
@property (nonatomic, retain) NSString * lgCurrencyNoDigitAfterDecimal;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfLanguageItem : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *LanguageItem;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfLanguageItem *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addLanguageItem:(eHousekeepingService_LanguageItem *)toAdd;
@property (nonatomic, readonly) NSMutableArray * LanguageItem;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_LanguageList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_ArrayOfLanguageItem * LanguageList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_LanguageList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_ArrayOfLanguageItem * LanguageList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetLanguageListResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_LanguageList * GetLanguageListResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetLanguageListResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_LanguageList * GetLanguageListResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetRoomStatusList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * strLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetRoomStatusList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * strLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_RoomStatus : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * rsID;
	NSString * rsCode;
	NSString * rsName;
	NSString * rsLang;
	NSData * rsIconImage;
	NSString * rsLastModifed;
	NSString * rsPhysicalCheck;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_RoomStatus *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * rsID;
@property (nonatomic, retain) NSString * rsCode;
@property (nonatomic, retain) NSString * rsName;
@property (nonatomic, retain) NSString * rsLang;
@property (nonatomic, retain) NSData * rsIconImage;
@property (nonatomic, retain) NSString * rsLastModifed;
@property (nonatomic, retain) NSString * rsPhysicalCheck;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfRoomStatus : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *RoomStatus;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfRoomStatus *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addRoomStatus:(eHousekeepingService_RoomStatus *)toAdd;
@property (nonatomic, readonly) NSMutableArray * RoomStatus;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_RoomStatusList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_ArrayOfRoomStatus * RoomStatusList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_RoomStatusList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_ArrayOfRoomStatus * RoomStatusList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetRoomStatusListResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_RoomStatusList * GetRoomStatusListResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetRoomStatusListResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_RoomStatusList * GetRoomStatusListResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetCleaningStatusList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * strLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetCleaningStatusList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * strLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_CleaningStatus : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * csID;
	NSString * csName;
	NSString * csLang;
	NSData * csIcon;
	NSString * csLastModifed;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_CleaningStatus *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * csID;
@property (nonatomic, retain) NSString * csName;
@property (nonatomic, retain) NSString * csLang;
@property (nonatomic, retain) NSData * csIcon;
@property (nonatomic, retain) NSString * csLastModifed;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfCleaningStatus : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *CleaningStatus;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfCleaningStatus *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addCleaningStatus:(eHousekeepingService_CleaningStatus *)toAdd;
@property (nonatomic, readonly) NSMutableArray * CleaningStatus;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_CleaningStatusList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_ArrayOfCleaningStatus * CleaningStatusList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_CleaningStatusList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_ArrayOfCleaningStatus * CleaningStatusList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetCleaningStatusListResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_CleaningStatusList * GetCleaningStatusListResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetCleaningStatusListResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_CleaningStatusList * GetCleaningStatusListResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetRoomTypeList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intHotelID;
	NSString * strLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetRoomTypeList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intHotelID;
@property (nonatomic, retain) NSString * strLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetRoomTypeListResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_RoomTypeList * GetRoomTypeListResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetRoomTypeListResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_RoomTypeList * GetRoomTypeListResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetBuildingList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intHotelID;
	NSString * strLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetBuildingList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intHotelID;
@property (nonatomic, retain) NSString * strLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_Building : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * bdID;
	NSString * bdName;
	NSString * bdLang;
	NSString * bdHotelID;
	NSString * bdLastModifed;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_Building *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * bdID;
@property (nonatomic, retain) NSString * bdName;
@property (nonatomic, retain) NSString * bdLang;
@property (nonatomic, retain) NSString * bdHotelID;
@property (nonatomic, retain) NSString * bdLastModifed;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfBuilding : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *Building;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfBuilding *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addBuilding:(eHousekeepingService_Building *)toAdd;
@property (nonatomic, readonly) NSMutableArray * Building;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_BuildingList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_ArrayOfBuilding * BuildingList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_BuildingList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_ArrayOfBuilding * BuildingList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetBuildingListResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_BuildingList * GetBuildingListResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetBuildingListResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_BuildingList * GetBuildingListResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetFloorList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intBuildingID;
	NSNumber * intHotelID;
	NSString * strLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetFloorList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intBuildingID;
@property (nonatomic, retain) NSNumber * intHotelID;
@property (nonatomic, retain) NSString * strLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_Floor : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * flID;
	NSString * flName;
	NSString * flLang;
	NSString * flBuildingID;
	NSString * flLastModifed;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_Floor *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * flID;
@property (nonatomic, retain) NSString * flName;
@property (nonatomic, retain) NSString * flLang;
@property (nonatomic, retain) NSString * flBuildingID;
@property (nonatomic, retain) NSString * flLastModifed;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfFloor : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *Floor;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfFloor *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addFloor:(eHousekeepingService_Floor *)toAdd;
@property (nonatomic, readonly) NSMutableArray * Floor;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_FloorList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_ArrayOfFloor * FloorList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_FloorList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_ArrayOfFloor * FloorList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetFloorListResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_FloorList * GetFloorListResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetFloorListResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_FloorList * GetFloorListResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_AuthenticateUser : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * strUsrName;
	NSString * strUsrPassword;
	NSString * strDeviceNo;
	NSNumber * intDeviceType;
	NSString * strLastModified;
	NSString * intHotelID;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_AuthenticateUser *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * strUsrName;
@property (nonatomic, retain) NSString * strUsrPassword;
@property (nonatomic, retain) NSString * strDeviceNo;
@property (nonatomic, retain) NSNumber * intDeviceType;
@property (nonatomic, retain) NSString * strLastModified;
@property (nonatomic, retain) NSString * intHotelID;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_UserDetail : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * usrID;
	NSString * usrName;
	NSString * usrFullName;
	NSString * usrLang;
	NSString * usrPassword;
	NSString * usrTitle;
	NSString * usrIsSupervisor;
	NSString * usrIsAdmin;
	NSString * usrDeptID;
	NSString * usrDept;
	NSString * usrLangCode;
	NSString * usrHotelID;
	NSString * usrIsActive;
	NSString * usrLastModified;
	NSString * usrInitial;
	NSString * intAllowReorderRoomAssignment;
	NSString * intAllowBlockVacantRoom;
	NSNumber * usrBuilding;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_UserDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * usrID;
@property (nonatomic, retain) NSString * usrName;
@property (nonatomic, retain) NSString * usrFullName;
@property (nonatomic, retain) NSString * usrLang;
@property (nonatomic, retain) NSString * usrPassword;
@property (nonatomic, retain) NSString * usrTitle;
@property (nonatomic, retain) NSString * usrIsSupervisor;
@property (nonatomic, retain) NSString * usrIsAdmin;
@property (nonatomic, retain) NSString * usrDeptID;
@property (nonatomic, retain) NSString * usrDept;
@property (nonatomic, retain) NSString * usrLangCode;
@property (nonatomic, retain) NSString * usrHotelID;
@property (nonatomic, retain) NSString * usrIsActive;
@property (nonatomic, retain) NSString * usrLastModified;
@property (nonatomic, retain) NSString * usrInitial;
@property (nonatomic, retain) NSString * intAllowReorderRoomAssignment;
@property (nonatomic, retain) NSString * intAllowBlockVacantRoom;
@property (nonatomic, retain) NSNumber * usrBuilding;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_EHSKPUser : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_UserDetail * UserDetail;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_EHSKPUser *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_UserDetail * UserDetail;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_AuthenticateUserResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_EHSKPUser * AuthenticateUserResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_AuthenticateUserResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_EHSKPUser * AuthenticateUserResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetHotelInfo : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSNumber * intHotelID;
	NSString * strLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetHotelInfo *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSNumber * intHotelID;
@property (nonatomic, retain) NSString * strLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_Hotel : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * htID;
	NSString * htName;
	NSString * htLang;
	NSData * htLogo;
	NSString * htLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_Hotel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * htID;
@property (nonatomic, retain) NSString * htName;
@property (nonatomic, retain) NSString * htLang;
@property (nonatomic, retain) NSData * htLogo;
@property (nonatomic, retain) NSString * htLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_HotelInfo : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_Hotel * Hotel;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_HotelInfo *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_Hotel * Hotel;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetHotelInfoResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_HotelInfo * GetHotelInfoResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetHotelInfoResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_HotelInfo * GetHotelInfoResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetRoomAssignmentList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSString * strLastModified;
	NSString * intInspectionStatus;
	NSString * intRoomStatus;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetRoomAssignmentList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSString * strLastModified;
@property (nonatomic, retain) NSString * intInspectionStatus;
@property (nonatomic, retain) NSString * intRoomStatus;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetRoomAssignmentListResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_RoomAssignmentList * GetRoomAssignmentListResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetRoomAssignmentListResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_RoomAssignmentList * GetRoomAssignmentListResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetPrevRoomAssignmentList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSString * strLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetPrevRoomAssignmentList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSString * strLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetPrevRoomAssignmentListResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_RoomAssignmentList * GetPrevRoomAssignmentListResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetPrevRoomAssignmentListResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_RoomAssignmentList * GetPrevRoomAssignmentListResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetFindRoomAssignmentList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intSupervisorID;
	NSNumber * intRoomStatusID;
	NSString * strLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetFindRoomAssignmentList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intSupervisorID;
@property (nonatomic, retain) NSNumber * intRoomStatusID;
@property (nonatomic, retain) NSString * strLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetFindRoomAssignmentListResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_RoomAssignmentList * GetFindRoomAssignmentListResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetFindRoomAssignmentListResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_RoomAssignmentList * GetFindRoomAssignmentListResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetFindRoomAssignmentLists : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intSupervisorID;
	NSNumber * intStatusID;
	NSNumber * intFilterType;
	NSString * strLastModified;
	NSString * strHotel_ID;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetFindRoomAssignmentLists *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intSupervisorID;
@property (nonatomic, retain) NSNumber * intStatusID;
@property (nonatomic, retain) NSNumber * intFilterType;
@property (nonatomic, retain) NSString * strLastModified;
@property (nonatomic, retain) NSString * strHotel_ID;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetFindRoomAssignmentListsResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_RoomAssignmentList * GetFindRoomAssignmentListsResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetFindRoomAssignmentListsResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_RoomAssignmentList * GetFindRoomAssignmentListsResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetSupervisorFindFloorList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intSupervisorID;
	NSNumber * intBuildingID;
	NSNumber * intHotelID;
	NSNumber * intStatusID;
	NSNumber * intFilterType;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetSupervisorFindFloorList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intSupervisorID;
@property (nonatomic, retain) NSNumber * intBuildingID;
@property (nonatomic, retain) NSNumber * intHotelID;
@property (nonatomic, retain) NSNumber * intStatusID;
@property (nonatomic, retain) NSNumber * intFilterType;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetSupervisorFindFloorListResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_FloorList * GetSupervisorFindFloorListResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetSupervisorFindFloorListResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_FloorList * GetSupervisorFindFloorListResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetSupervisorFindRoomAssignmentLists : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intSupervisorID;
	NSNumber * intFloorID;
	NSNumber * intStatusID;
	NSNumber * intFilterType;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetSupervisorFindRoomAssignmentLists *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intSupervisorID;
@property (nonatomic, retain) NSNumber * intFloorID;
@property (nonatomic, retain) NSNumber * intStatusID;
@property (nonatomic, retain) NSNumber * intFilterType;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetSupervisorFindRoomAssignmentListsResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_RoomAssignmentList * GetSupervisorFindRoomAssignmentListsResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetSupervisorFindRoomAssignmentListsResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_RoomAssignmentList * GetSupervisorFindRoomAssignmentListsResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetGuestInfo : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSNumber * intRoomAssignID;
	NSString * strLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetGuestInfo *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSNumber * intRoomAssignID;
@property (nonatomic, retain) NSString * strLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_Guest : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * raID;
	NSString * gpTitle;
	NSString * gpName;
	NSString * gpHousekeepingName;
	NSString * gpLangPref;
	NSString * gpCheckInDt;
	NSString * gpCheckOutDt;
	NSString * gpPreferenceDesc;
	NSString * gpVIP;
	NSString * gpLastModified;
	NSData * gpPhoto;
	NSString * gpSpecialService;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_Guest *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * raID;
@property (nonatomic, retain) NSString * gpTitle;
@property (nonatomic, retain) NSString * gpName;
@property (nonatomic, retain) NSString * gpHousekeepingName;
@property (nonatomic, retain) NSString * gpLangPref;
@property (nonatomic, retain) NSString * gpCheckInDt;
@property (nonatomic, retain) NSString * gpCheckOutDt;
@property (nonatomic, retain) NSString * gpPreferenceDesc;
@property (nonatomic, retain) NSString * gpVIP;
@property (nonatomic, retain) NSString * gpLastModified;
@property (nonatomic, retain) NSData * gpPhoto;
@property (nonatomic, retain) NSString * gpSpecialService;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GuestInfo : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_Guest * GuestInfo;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GuestInfo *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_Guest * GuestInfo;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetGuestInfoResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_GuestInfo * GetGuestInfoResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetGuestInfoResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_GuestInfo * GetGuestInfoResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetRoomDetail : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSNumber * intRoomAssignID;
	NSString * strLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetRoomDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSNumber * intRoomAssignID;
@property (nonatomic, retain) NSString * strLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_RoomDetail : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_RoomAssign * RoomDetail;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_RoomDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_RoomAssign * RoomDetail;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetRoomDetailResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_RoomDetail * GetRoomDetailResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetRoomDetailResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_RoomDetail * GetRoomDetailResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_UpdateGuestInfo : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSNumber * intRoomAssignID;
	NSString * strGuestPref;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_UpdateGuestInfo *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSNumber * intRoomAssignID;
@property (nonatomic, retain) NSString * strGuestPref;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_UpdateGuestInfoResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * UpdateGuestInfoResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_UpdateGuestInfoResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * UpdateGuestInfoResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_UpdateRoomAssignment : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSNumber * intRoomAssignID;
	NSNumber * intRoomStatusID;
	NSNumber * intCleaningStatusID;
	NSString * strCleanStartTm;
	NSString * strCleanEndTm;
	NSString * raRemark;
	NSString * intTotalCleaningTime;
	NSString * strPauseTime;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_UpdateRoomAssignment *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSNumber * intRoomAssignID;
@property (nonatomic, retain) NSNumber * intRoomStatusID;
@property (nonatomic, retain) NSNumber * intCleaningStatusID;
@property (nonatomic, retain) NSString * strCleanStartTm;
@property (nonatomic, retain) NSString * strCleanEndTm;
@property (nonatomic, retain) NSString * raRemark;
@property (nonatomic, retain) NSString * intTotalCleaningTime;
@property (nonatomic, retain) NSString * strPauseTime;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_UpdateRoomAssignmentResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * UpdateRoomAssignmentResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_UpdateRoomAssignmentResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * UpdateRoomAssignmentResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetDiscrepantRoomStatus : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSNumber * intRoomAssignID;
	NSNumber * intRoomStatusID;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetDiscrepantRoomStatus *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSNumber * intRoomAssignID;
@property (nonatomic, retain) NSNumber * intRoomStatusID;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetDiscrepantRoomStatusList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	NSNumber * intDAID;
	NSNumber * intCurrentRoomStatus;
	NSNumber * intNewRoomStatus;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetDiscrepantRoomStatusList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) NSNumber * intDAID;
@property (nonatomic, retain) NSNumber * intCurrentRoomStatus;
@property (nonatomic, retain) NSNumber * intNewRoomStatus;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetDiscrepantRoomStatusResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_GetDiscrepantRoomStatusList * GetDiscrepantRoomStatusResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetDiscrepantRoomStatusResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_GetDiscrepantRoomStatusList * GetDiscrepantRoomStatusResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_UpdateRoomCleaningStatus : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intSupervisorID;
	NSNumber * intRoomAssignID;
	NSNumber * intCleaningStatusID;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_UpdateRoomCleaningStatus *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intSupervisorID;
@property (nonatomic, retain) NSNumber * intRoomAssignID;
@property (nonatomic, retain) NSNumber * intCleaningStatusID;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_UpdateRoomCleaningStatusResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * UpdateRoomCleaningStatusResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_UpdateRoomCleaningStatusResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * UpdateRoomCleaningStatusResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_UpdateRoomStatus : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUserID;
	NSString * strRoomNo;
	NSNumber * intStatusID;
	NSNumber * intCleaningStatusID;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_UpdateRoomStatus *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUserID;
@property (nonatomic, retain) NSString * strRoomNo;
@property (nonatomic, retain) NSNumber * intStatusID;
@property (nonatomic, retain) NSNumber * intCleaningStatusID;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_UpdateRoomStatusResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * UpdateRoomStatusResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_UpdateRoomStatusResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * UpdateRoomStatusResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_UpdateRoomDetails : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUserID;
	NSString * strRoomNo;
	NSNumber * intHTID;
	NSNumber * intUpdateType;
	NSNumber * intStatusID;
	NSNumber * intCleaningStatusID;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_UpdateRoomDetails *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUserID;
@property (nonatomic, retain) NSString * strRoomNo;
@property (nonatomic, retain) NSNumber * intHTID;
@property (nonatomic, retain) NSNumber * intUpdateType;
@property (nonatomic, retain) NSNumber * intStatusID;
@property (nonatomic, retain) NSNumber * intCleaningStatusID;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_UpdateRoomDetailsResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * UpdateRoomDetailsResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_UpdateRoomDetailsResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * UpdateRoomDetailsResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_UpdateCleaningStatus : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUserID;
	NSNumber * intRoomAssignmentID;
	NSNumber * intStatusID;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_UpdateCleaningStatus *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUserID;
@property (nonatomic, retain) NSNumber * intRoomAssignmentID;
@property (nonatomic, retain) NSNumber * intStatusID;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_UpdateCleaningStatusResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * UpdateCleaningStatusResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_UpdateCleaningStatusResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * UpdateCleaningStatusResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetAccessRights : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetAccessRights *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_Rights : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * arModule;
	NSNumber * arView;
	NSNumber * arEdit;
	NSNumber * arAdd;
	NSNumber * arRemove;
	NSNumber * arExport;
	NSNumber * arAccess;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_Rights *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * arModule;
@property (nonatomic, retain) NSNumber * arView;
@property (nonatomic, retain) NSNumber * arEdit;
@property (nonatomic, retain) NSNumber * arAdd;
@property (nonatomic, retain) NSNumber * arRemove;
@property (nonatomic, retain) NSNumber * arExport;
@property (nonatomic, retain) NSNumber * arAccess;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfRights : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *Rights;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfRights *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addRights:(eHousekeepingService_Rights *)toAdd;
@property (nonatomic, readonly) NSMutableArray * Rights;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_AccessRightsList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_ArrayOfRights * AccessRightsList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_AccessRightsList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_ArrayOfRights * AccessRightsList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetAccessRightsResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_AccessRightsList * GetAccessRightsResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetAccessRightsResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_AccessRightsList * GetAccessRightsResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetCommonConfigurations : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetCommonConfigurations *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_WSSettings : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * ccModule;
	NSString * ccValue;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_WSSettings *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * ccModule;
@property (nonatomic, retain) NSString * ccValue;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfWSSettings : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *WSSettings;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfWSSettings *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addWSSettings:(eHousekeepingService_WSSettings *)toAdd;
@property (nonatomic, readonly) NSMutableArray * WSSettings;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_CommonConfigurationsList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_ArrayOfWSSettings * CommonConfigurationsList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_CommonConfigurationsList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_ArrayOfWSSettings * CommonConfigurationsList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetCommonConfigurationsResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_CommonConfigurationsList * GetCommonConfigurationsResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetCommonConfigurationsResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_CommonConfigurationsList * GetCommonConfigurationsResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetLaundryServiceList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSNumber * intHotelID;
	NSString * strLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetLaundryServiceList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSNumber * intHotelID;
@property (nonatomic, retain) NSString * strLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_LaundryList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * ldryLstID;
	NSString * ldryLstName;
	NSString * ldryLstLang;
	NSString * ldryLstLastModified;
	NSData * ldryLstPicture;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_LaundryList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * ldryLstID;
@property (nonatomic, retain) NSString * ldryLstName;
@property (nonatomic, retain) NSString * ldryLstLang;
@property (nonatomic, retain) NSString * ldryLstLastModified;
@property (nonatomic, retain) NSData * ldryLstPicture;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfLaundryList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *LaundryList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfLaundryList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addLaundryList:(eHousekeepingService_LaundryList *)toAdd;
@property (nonatomic, readonly) NSMutableArray * LaundryList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_LdrySrvList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_ArrayOfLaundryList * LaundrySrvList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_LdrySrvList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_ArrayOfLaundryList * LaundrySrvList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetLaundryServiceListResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_LdrySrvList * GetLaundryServiceListResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetLaundryServiceListResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_LdrySrvList * GetLaundryServiceListResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetLaundryCategoryList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSNumber * intHotelID;
	NSString * strLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetLaundryCategoryList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSNumber * intHotelID;
@property (nonatomic, retain) NSString * strLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_LaundryTypeList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * ldryCatID;
	NSString * ldryCatName;
	NSString * ldryCatLang;
	NSData * ldryCatPicture;
	NSString * ldryTypLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_LaundryTypeList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * ldryCatID;
@property (nonatomic, retain) NSString * ldryCatName;
@property (nonatomic, retain) NSString * ldryCatLang;
@property (nonatomic, retain) NSData * ldryCatPicture;
@property (nonatomic, retain) NSString * ldryTypLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfLaundryTypeList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *LaundryTypeList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfLaundryTypeList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addLaundryTypeList:(eHousekeepingService_LaundryTypeList *)toAdd;
@property (nonatomic, readonly) NSMutableArray * LaundryTypeList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_LdryTypList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_ArrayOfLaundryTypeList * LaundryTypList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_LdryTypList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_ArrayOfLaundryTypeList * LaundryTypList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetLaundryCategoryListResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_LdryTypList * GetLaundryCategoryListResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetLaundryCategoryListResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_LdryTypList * GetLaundryCategoryListResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetLaundryItemList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSString * strLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetLaundryItemList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSString * strLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_LaundryItemList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * ldryItmID;
	NSString * ldryItmName;
	NSString * ldryItmLang;
	NSNumber * ldryItmGender;
	NSString * ldryItmLastModified;
	NSData * ldryPicture;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_LaundryItemList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * ldryItmID;
@property (nonatomic, retain) NSString * ldryItmName;
@property (nonatomic, retain) NSString * ldryItmLang;
@property (nonatomic, retain) NSNumber * ldryItmGender;
@property (nonatomic, retain) NSString * ldryItmLastModified;
@property (nonatomic, retain) NSData * ldryPicture;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfLaundryItemList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *LaundryItemList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfLaundryItemList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addLaundryItemList:(eHousekeepingService_LaundryItemList *)toAdd;
@property (nonatomic, readonly) NSMutableArray * LaundryItemList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_LdryItmList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_ArrayOfLaundryItemList * LaundryItmList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_LdryItmList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_ArrayOfLaundryItemList * LaundryItmList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetLaundryItemListResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_LdryItmList * GetLaundryItemListResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetLaundryItemListResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_LdryItmList * GetLaundryItemListResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_RemoveLaundryItem : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSNumber * intDutyAssignID;
	NSNumber * intLdryLstID;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_RemoveLaundryItem *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSNumber * intDutyAssignID;
@property (nonatomic, retain) NSNumber * intLdryLstID;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_RemoveLaundryItemResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * RemoveLaundryItemResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_RemoveLaundryItemResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * RemoveLaundryItemResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostLaundryItem : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSNumber * intLaundryOrderID;
	NSNumber * intLaundryItemID;
	NSNumber * intLaundryCategoryID;
	NSNumber * intLaundryServiceID;
	NSNumber * intQuantity;
	NSString * dblTotalPrice;
	NSString * strTransactionTime;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostLaundryItem *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSNumber * intLaundryOrderID;
@property (nonatomic, retain) NSNumber * intLaundryItemID;
@property (nonatomic, retain) NSNumber * intLaundryCategoryID;
@property (nonatomic, retain) NSNumber * intLaundryServiceID;
@property (nonatomic, retain) NSNumber * intQuantity;
@property (nonatomic, retain) NSString * dblTotalPrice;
@property (nonatomic, retain) NSString * strTransactionTime;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostLaundryItemResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * PostLaundryItemResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostLaundryItemResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * PostLaundryItemResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetRoomSetGuideByRoomType : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSString * intRoomTypeID;
	NSNumber * intHotelID;
	NSString * strLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetRoomSetGuideByRoomType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSString * intRoomTypeID;
@property (nonatomic, retain) NSNumber * intHotelID;
@property (nonatomic, retain) NSString * strLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfBase64Binary : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *base64Binary;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfBase64Binary *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addBase64Binary:(NSData *)toAdd;
@property (nonatomic, readonly) NSMutableArray * base64Binary;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_RoomSetupList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * rsgID;
	NSNumber * rsgRoomSectionID;
	NSString * rsgRoomSectionName;
	NSString * rsgRoomSectionLang;
	NSString * rsgDesc;
	NSString * rsgLang;
	NSString * rsgLastModified;
	NSData * rsgPicture;
	NSData * rsgImageUrl;
	NSString * sgc_Name;
	NSString * sgc_Lang;
	eHousekeepingService_ArrayOfBase64Binary * rsgImages;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_RoomSetupList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * rsgID;
@property (nonatomic, retain) NSNumber * rsgRoomSectionID;
@property (nonatomic, retain) NSString * rsgRoomSectionName;
@property (nonatomic, retain) NSString * rsgRoomSectionLang;
@property (nonatomic, retain) NSString * rsgDesc;
@property (nonatomic, retain) NSString * rsgLang;
@property (nonatomic, retain) NSString * rsgLastModified;
@property (nonatomic, retain) NSData * rsgPicture;
@property (nonatomic, retain) NSData * rsgImageUrl;
@property (nonatomic, retain) NSString * sgc_Name;
@property (nonatomic, retain) NSString * sgc_Lang;
@property (nonatomic, retain) eHousekeepingService_ArrayOfBase64Binary * rsgImages;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfRoomSetupList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *RoomSetupList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfRoomSetupList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addRoomSetupList:(eHousekeepingService_RoomSetupList *)toAdd;
@property (nonatomic, readonly) NSMutableArray * RoomSetupList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_RoomStpList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_ArrayOfRoomSetupList * RoomSetupList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_RoomStpList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_ArrayOfRoomSetupList * RoomSetupList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetRoomSetGuideByRoomTypeResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_RoomStpList * GetRoomSetGuideByRoomTypeResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetRoomSetGuideByRoomTypeResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_RoomStpList * GetRoomSetGuideByRoomTypeResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetRoomSetGuideList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intHotelID;
	NSNumber * intCategoryID;
	NSString * strLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetRoomSetGuideList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intHotelID;
@property (nonatomic, retain) NSNumber * intCategoryID;
@property (nonatomic, retain) NSString * strLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetRoomSetGuideListResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_RoomStpList * GetRoomSetGuideListResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetRoomSetGuideListResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_RoomStpList * GetRoomSetGuideListResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetMessageTemplateList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSString * strLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetMessageTemplateList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSString * strLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_MessageTemplateList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * msgteID;
	NSString * msgteName;
	NSString * msgteContent;
	NSString * msgtcLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_MessageTemplateList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * msgteID;
@property (nonatomic, retain) NSString * msgteName;
@property (nonatomic, retain) NSString * msgteContent;
@property (nonatomic, retain) NSString * msgtcLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfMessageTemplateList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *MessageTemplateList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfMessageTemplateList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addMessageTemplateList:(eHousekeepingService_MessageTemplateList *)toAdd;
@property (nonatomic, readonly) NSMutableArray * MessageTemplateList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_MsgTempList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_ArrayOfMessageTemplateList * MessageTemplateList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_MsgTempList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_ArrayOfMessageTemplateList * MessageTemplateList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetMessageTemplateListResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_MsgTempList * GetMessageTemplateListResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetMessageTemplateListResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_MsgTempList * GetMessageTemplateListResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetNewMessage : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSString * intGrpMessageID;
	NSNumber * intHotelID;
	NSString * strLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetNewMessage *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSString * intGrpMessageID;
@property (nonatomic, retain) NSNumber * intHotelID;
@property (nonatomic, retain) NSString * strLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_AdHocMessageList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * amsgID;
	NSNumber * amsgGrpID;
	NSString * amsgSendFrom;
	NSString * amsgSendFromLang;
	NSString * amsgTopic;
	NSString * amsgContent;
	NSString * amsgLastModified;
	NSNumber * amsgPhotoAttached;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_AdHocMessageList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * amsgID;
@property (nonatomic, retain) NSNumber * amsgGrpID;
@property (nonatomic, retain) NSString * amsgSendFrom;
@property (nonatomic, retain) NSString * amsgSendFromLang;
@property (nonatomic, retain) NSString * amsgTopic;
@property (nonatomic, retain) NSString * amsgContent;
@property (nonatomic, retain) NSString * amsgLastModified;
@property (nonatomic, retain) NSNumber * amsgPhotoAttached;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfAdHocMessageList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *AdHocMessageList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfAdHocMessageList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addAdHocMessageList:(eHousekeepingService_AdHocMessageList *)toAdd;
@property (nonatomic, readonly) NSMutableArray * AdHocMessageList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_AdHocMsgList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_ArrayOfAdHocMessageList * AdHocMessageList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_AdHocMsgList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_ArrayOfAdHocMessageList * AdHocMessageList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetNewMessageResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_AdHocMsgList * GetNewMessageResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetNewMessageResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_AdHocMsgList * GetNewMessageResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetMessageCategoryList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSNumber * intHotelID;
	NSString * strLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetMessageCategoryList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSNumber * intHotelID;
@property (nonatomic, retain) NSString * strLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_MessageCategoryList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * msgCatID;
	NSString * msgCatName;
	NSString * msgCatDesc;
	NSData * msgCatPhoto;
	NSString * msgCatLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_MessageCategoryList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * msgCatID;
@property (nonatomic, retain) NSString * msgCatName;
@property (nonatomic, retain) NSString * msgCatDesc;
@property (nonatomic, retain) NSData * msgCatPhoto;
@property (nonatomic, retain) NSString * msgCatLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfMessageCategoryList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *MessageCategoryList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfMessageCategoryList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addMessageCategoryList:(eHousekeepingService_MessageCategoryList *)toAdd;
@property (nonatomic, readonly) NSMutableArray * MessageCategoryList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_MsgCatList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_ArrayOfMessageCategoryList * MessageCatList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_MsgCatList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_ArrayOfMessageCategoryList * MessageCatList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetMessageCategoryListResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_MsgCatList * GetMessageCategoryListResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetMessageCategoryListResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_MsgCatList * GetMessageCategoryListResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetMessageItemList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSString * strLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetMessageItemList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSString * strLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_MessageItemList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * msgItmID;
	NSNumber * msgItmSeq;
	NSNumber * msgItmCatID;
	NSString * msgItmName;
	NSString * msgItmDesc;
	NSData * msgItmPhoto;
	NSString * msgItmLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_MessageItemList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * msgItmID;
@property (nonatomic, retain) NSNumber * msgItmSeq;
@property (nonatomic, retain) NSNumber * msgItmCatID;
@property (nonatomic, retain) NSString * msgItmName;
@property (nonatomic, retain) NSString * msgItmDesc;
@property (nonatomic, retain) NSData * msgItmPhoto;
@property (nonatomic, retain) NSString * msgItmLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfMessageItemList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *MessageItemList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfMessageItemList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addMessageItemList:(eHousekeepingService_MessageItemList *)toAdd;
@property (nonatomic, readonly) NSMutableArray * MessageItemList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_MsgItmList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_ArrayOfMessageItemList * MessageItmList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_MsgItmList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_ArrayOfMessageItemList * MessageItmList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetMessageItemListResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_MsgItmList * GetMessageItemListResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetMessageItemListResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_MsgItmList * GetMessageItemListResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetPopupMsgList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSString * strLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetPopupMsgList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSString * strLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PopupMsgItem : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * popupID;
	NSString * popupMsg;
	NSString * popupDateTime;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PopupMsgItem *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * popupID;
@property (nonatomic, retain) NSString * popupMsg;
@property (nonatomic, retain) NSString * popupDateTime;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfPopupMsgItem : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *PopupMsgItem;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfPopupMsgItem *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addPopupMsgItem:(eHousekeepingService_PopupMsgItem *)toAdd;
@property (nonatomic, readonly) NSMutableArray * PopupMsgItem;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PopupMsgList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_ArrayOfPopupMsgItem * PopupMsgItems;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PopupMsgList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_ArrayOfPopupMsgItem * PopupMsgItems;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetPopupMsgListResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_PopupMsgList * GetPopupMsgListResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetPopupMsgListResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_PopupMsgList * GetPopupMsgListResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_UpdatePopupMsgStatus : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSString * intListPopupMsgID;
	NSNumber * intStatus;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_UpdatePopupMsgStatus *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSString * intListPopupMsgID;
@property (nonatomic, retain) NSNumber * intStatus;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_UpdatePopupMsgStatusResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * UpdatePopupMsgStatusResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_UpdatePopupMsgStatusResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * UpdatePopupMsgStatusResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostMessage : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSString * strSendToID;
	NSString * strTopic;
	NSString * strContent;
	NSNumber * intHotelId;
	NSString * strTransactionTime;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostMessage *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSString * strSendToID;
@property (nonatomic, retain) NSString * strTopic;
@property (nonatomic, retain) NSString * strContent;
@property (nonatomic, retain) NSNumber * intHotelId;
@property (nonatomic, retain) NSString * strTransactionTime;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GroupAdHocMessage : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * gadGrpID;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GroupAdHocMessage *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * gadGrpID;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfGroupAdHocMessage : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *GroupAdHocMessage;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfGroupAdHocMessage *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addGroupAdHocMessage:(eHousekeepingService_GroupAdHocMessage *)toAdd;
@property (nonatomic, readonly) NSMutableArray * GroupAdHocMessage;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GrpAdHocList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_ArrayOfGroupAdHocMessage * GrpAdHocMsg;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GrpAdHocList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_ArrayOfGroupAdHocMessage * GrpAdHocMsg;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostMessageResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_GrpAdHocList * PostMessageResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostMessageResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_GrpAdHocList * PostMessageResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostEngineeringCase : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * pSender;
	NSString * pAction;
	NSString * pUsername;
	NSString * pPassword;
	NSString * pPropertyID;
	NSString * pServiceType;
	NSString * pRoomNo;
	NSString * pItemCode;
	NSString * pLocationCode;
	NSString * pRemark;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostEngineeringCase *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * pSender;
@property (nonatomic, retain) NSString * pAction;
@property (nonatomic, retain) NSString * pUsername;
@property (nonatomic, retain) NSString * pPassword;
@property (nonatomic, retain) NSString * pPropertyID;
@property (nonatomic, retain) NSString * pServiceType;
@property (nonatomic, retain) NSString * pRoomNo;
@property (nonatomic, retain) NSString * pItemCode;
@property (nonatomic, retain) NSString * pLocationCode;
@property (nonatomic, retain) NSString * pRemark;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_EngCase : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	NSNumber * ecID;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_EngCase *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) NSNumber * ecID;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostEngineeringCaseResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_EngCase * PostEngineeringCaseResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostEngineeringCaseResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_EngCase * PostEngineeringCaseResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PosteCnJob : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * strSender;
	NSString * strAction;
	NSString * strUsername;
	NSString * strPassword;
	NSString * strPropertyID;
	NSString * strServiceType;
	NSString * strRoomNo;
	NSString * strItemCode;
	NSString * strLocationCode;
	NSString * strRemark;
	NSData * binImage;
	NSString * strImageExtension;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PosteCnJob *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * strSender;
@property (nonatomic, retain) NSString * strAction;
@property (nonatomic, retain) NSString * strUsername;
@property (nonatomic, retain) NSString * strPassword;
@property (nonatomic, retain) NSString * strPropertyID;
@property (nonatomic, retain) NSString * strServiceType;
@property (nonatomic, retain) NSString * strRoomNo;
@property (nonatomic, retain) NSString * strItemCode;
@property (nonatomic, retain) NSString * strLocationCode;
@property (nonatomic, retain) NSString * strRemark;
@property (nonatomic, retain) NSData * binImage;
@property (nonatomic, retain) NSString * strImageExtension;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PosteCnJobList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	NSNumber * JobID;
	NSString * WorkorderNo;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PosteCnJobList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) NSNumber * JobID;
@property (nonatomic, retain) NSString * WorkorderNo;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PosteCnJobResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_PosteCnJobList * PosteCnJobResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PosteCnJobResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_PosteCnJobList * PosteCnJobResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PosteCnAhMsg : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * strSender;
	NSString * strUsername;
	NSString * strPassword;
	NSString * strPropertyID;
	NSString * strRunnerID;
	NSString * strRunnerName;
	NSString * strGroupID;
	NSString * strGroupName;
	NSString * strDeviceType;
	NSString * strDeviceNo;
	NSString * strMessage;
	NSString * strRoomNo;
	NSString * strLocationCode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PosteCnAhMsg *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * strSender;
@property (nonatomic, retain) NSString * strUsername;
@property (nonatomic, retain) NSString * strPassword;
@property (nonatomic, retain) NSString * strPropertyID;
@property (nonatomic, retain) NSString * strRunnerID;
@property (nonatomic, retain) NSString * strRunnerName;
@property (nonatomic, retain) NSString * strGroupID;
@property (nonatomic, retain) NSString * strGroupName;
@property (nonatomic, retain) NSString * strDeviceType;
@property (nonatomic, retain) NSString * strDeviceNo;
@property (nonatomic, retain) NSString * strMessage;
@property (nonatomic, retain) NSString * strRoomNo;
@property (nonatomic, retain) NSString * strLocationCode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PosteCnAhMsgList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	NSNumber * AdhocID;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PosteCnAhMsgList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) NSNumber * AdhocID;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PosteCnAhMsgResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_PosteCnAhMsgList * PosteCnAhMsgResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PosteCnAhMsgResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_PosteCnAhMsgList * PosteCnAhMsgResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostECAttachPhoto : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSNumber * intRoomAssignID;
	NSData * binPhoto;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostECAttachPhoto *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSNumber * intRoomAssignID;
@property (nonatomic, retain) NSData * binPhoto;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostECAttachPhotoResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * PostECAttachPhotoResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostECAttachPhotoResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * PostECAttachPhotoResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetLFCategoryList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSString * strLastModified;
	NSString * strHotel_ID;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetLFCategoryList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSString * strLastModified;
@property (nonatomic, retain) NSString * strHotel_ID;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_LostAndFoundCategory : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * lfcID;
	NSNumber * lfcHotelID;
	NSString * lfcName;
	NSString * lfcLang;
	NSData * lfcPicture;
	NSString * lfcLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_LostAndFoundCategory *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * lfcID;
@property (nonatomic, retain) NSNumber * lfcHotelID;
@property (nonatomic, retain) NSString * lfcName;
@property (nonatomic, retain) NSString * lfcLang;
@property (nonatomic, retain) NSData * lfcPicture;
@property (nonatomic, retain) NSString * lfcLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfLostAndFoundCategory : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *LostAndFoundCategory;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfLostAndFoundCategory *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addLostAndFoundCategory:(eHousekeepingService_LostAndFoundCategory *)toAdd;
@property (nonatomic, readonly) NSMutableArray * LostAndFoundCategory;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_LostNFoundCat : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_ArrayOfLostAndFoundCategory * LostAndFoundCategory;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_LostNFoundCat *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_ArrayOfLostAndFoundCategory * LostAndFoundCategory;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetLFCategoryListResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_LostNFoundCat * GetLFCategoryListResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetLFCategoryListResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_LostNFoundCat * GetLFCategoryListResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetLFItemList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSString * strLastModified;
	NSString * strHotel_ID;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetLFItemList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSString * strLastModified;
@property (nonatomic, retain) NSString * strHotel_ID;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_LostAndFoundItemTypeList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * lfitID;
	NSString * lfitName;
	NSString * lfitLang;
	NSString * lfcLastModified;
	NSData * lfcPicture;
	NSNumber * lfitCategoryID;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_LostAndFoundItemTypeList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * lfitID;
@property (nonatomic, retain) NSString * lfitName;
@property (nonatomic, retain) NSString * lfitLang;
@property (nonatomic, retain) NSString * lfcLastModified;
@property (nonatomic, retain) NSData * lfcPicture;
@property (nonatomic, retain) NSNumber * lfitCategoryID;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfLostAndFoundItemTypeList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *LostAndFoundItemTypeList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfLostAndFoundItemTypeList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addLostAndFoundItemTypeList:(eHousekeepingService_LostAndFoundItemTypeList *)toAdd;
@property (nonatomic, readonly) NSMutableArray * LostAndFoundItemTypeList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_LostNFoundItmTypList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_ArrayOfLostAndFoundItemTypeList * LostAndFoundItemTypeList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_LostNFoundItmTypList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_ArrayOfLostAndFoundItemTypeList * LostAndFoundItemTypeList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetLFItemListResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_LostNFoundItmTypList * GetLFItemListResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetLFItemListResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_LostNFoundItmTypList * GetLFItemListResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetLFColorList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSString * strLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetLFColorList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSString * strLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_LostAndFoundColorList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * lfcID;
	NSString * lfcName;
	NSString * lfcLang;
	NSString * lfcHTMLColorCode;
	NSString * lfcLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_LostAndFoundColorList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * lfcID;
@property (nonatomic, retain) NSString * lfcName;
@property (nonatomic, retain) NSString * lfcLang;
@property (nonatomic, retain) NSString * lfcHTMLColorCode;
@property (nonatomic, retain) NSString * lfcLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfLostAndFoundColorList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *LostAndFoundColorList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfLostAndFoundColorList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addLostAndFoundColorList:(eHousekeepingService_LostAndFoundColorList *)toAdd;
@property (nonatomic, readonly) NSMutableArray * LostAndFoundColorList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_LostNFoundColorList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_ArrayOfLostAndFoundColorList * LostNFoundColorList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_LostNFoundColorList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_ArrayOfLostAndFoundColorList * LostNFoundColorList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetLFColorListResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_LostNFoundColorList * GetLFColorListResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetLFColorListResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_LostNFoundColorList * GetLFColorListResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostLostFound : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSNumber * intLFCategoryID;
	NSNumber * intLFItemID;
	NSNumber * intLFColorID;
	NSNumber * intQuantity;
	NSString * strFoundTime;
	NSString * strRemark;
	NSNumber * intRoomAssignID;
	eHousekeepingService_UniqueRoom * urRoom;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostLostFound *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSNumber * intLFCategoryID;
@property (nonatomic, retain) NSNumber * intLFItemID;
@property (nonatomic, retain) NSNumber * intLFColorID;
@property (nonatomic, retain) NSNumber * intQuantity;
@property (nonatomic, retain) NSString * strFoundTime;
@property (nonatomic, retain) NSString * strRemark;
@property (nonatomic, retain) NSNumber * intRoomAssignID;
@property (nonatomic, retain) eHousekeepingService_UniqueRoom * urRoom;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_LostFnd : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	NSNumber * lfID;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_LostFnd *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) NSNumber * lfID;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostLostFoundResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_LostFnd * PostLostFoundResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostLostFoundResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_LostFnd * PostLostFoundResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostLFAttachPhoto : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSNumber * intRoomAssignID;
	NSData * binPhoto;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostLFAttachPhoto *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSNumber * intRoomAssignID;
@property (nonatomic, retain) NSData * binPhoto;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostLFAttachPhotoResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * PostLFAttachPhotoResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostLFAttachPhotoResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * PostLFAttachPhotoResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetLocationList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intHotelID;
	NSString * strLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetLocationList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intHotelID;
@property (nonatomic, retain) NSString * strLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_LocationList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * lcID;
	NSString * lcCode;
	NSString * lcDesc;
	NSString * lcLang;
	NSString * lcRoomNo;
	NSString * lcType;
	NSString * lcLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_LocationList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * lcID;
@property (nonatomic, retain) NSString * lcCode;
@property (nonatomic, retain) NSString * lcDesc;
@property (nonatomic, retain) NSString * lcLang;
@property (nonatomic, retain) NSString * lcRoomNo;
@property (nonatomic, retain) NSString * lcType;
@property (nonatomic, retain) NSString * lcLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfLocationList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *LocationList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfLocationList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addLocationList:(eHousekeepingService_LocationList *)toAdd;
@property (nonatomic, readonly) NSMutableArray * LocationList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_LocList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_ArrayOfLocationList * LocationList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_LocList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_ArrayOfLocationList * LocationList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetLocationListResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_LocList * GetLocationListResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetLocationListResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_LocList * GetLocationListResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetUserList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSNumber * intHotelID;
	NSString * strLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetUserList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSNumber * intHotelID;
@property (nonatomic, retain) NSString * strLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfUserDetail : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *UserDetail;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfUserDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addUserDetail:(eHousekeepingService_UserDetail *)toAdd;
@property (nonatomic, readonly) NSMutableArray * UserDetail;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_UserList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_ArrayOfUserDetail * UserDetailList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_UserList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_ArrayOfUserDetail * UserDetailList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetUserListResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_UserList * GetUserListResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetUserListResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_UserList * GetUserListResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetRoomSectionList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intHotelID;
	NSNumber * intRoomType;
	NSNumber * intBuildingID;
	NSString * strLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetRoomSectionList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intHotelID;
@property (nonatomic, retain) NSNumber * intRoomType;
@property (nonatomic, retain) NSNumber * intBuildingID;
@property (nonatomic, retain) NSString * strLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_RoomSectionList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * rsetID;
	NSString * rsetName;
	NSString * rsetLang;
	NSNumber * rsetRoomTypeID;
	NSData * rsetPicture;
	NSString * rsetLastModified;
	NSNumber * sgc_CategoryID;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_RoomSectionList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * rsetID;
@property (nonatomic, retain) NSString * rsetName;
@property (nonatomic, retain) NSString * rsetLang;
@property (nonatomic, retain) NSNumber * rsetRoomTypeID;
@property (nonatomic, retain) NSData * rsetPicture;
@property (nonatomic, retain) NSString * rsetLastModified;
@property (nonatomic, retain) NSNumber * sgc_CategoryID;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfRoomSectionList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *RoomSectionList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfRoomSectionList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addRoomSectionList:(eHousekeepingService_RoomSectionList *)toAdd;
@property (nonatomic, readonly) NSMutableArray * RoomSectionList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_RmSecList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_ArrayOfRoomSectionList * RmSec;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_RmSecList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_ArrayOfRoomSectionList * RmSec;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetRoomSectionListResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_RmSecList * GetRoomSectionListResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetRoomSectionListResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_RmSecList * GetRoomSectionListResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetEngineeringItem : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSNumber * intHotelID;
	NSString * strLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetEngineeringItem *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSNumber * intHotelID;
@property (nonatomic, retain) NSString * strLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_EngineeringItemList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * engID;
	NSString * engName;
	NSString * engLang;
	NSNumber * engCategoryID;
	NSNumber * engRankInList;
	NSString * engLastModified;
	NSString * engServiceItemCode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_EngineeringItemList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * engID;
@property (nonatomic, retain) NSString * engName;
@property (nonatomic, retain) NSString * engLang;
@property (nonatomic, retain) NSNumber * engCategoryID;
@property (nonatomic, retain) NSNumber * engRankInList;
@property (nonatomic, retain) NSString * engLastModified;
@property (nonatomic, retain) NSString * engServiceItemCode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfEngineeringItemList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *EngineeringItemList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfEngineeringItemList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addEngineeringItemList:(eHousekeepingService_EngineeringItemList *)toAdd;
@property (nonatomic, readonly) NSMutableArray * EngineeringItemList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_EngItmList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_ArrayOfEngineeringItemList * EngineeringItmList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_EngItmList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_ArrayOfEngineeringItemList * EngineeringItmList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetEngineeringItemResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_EngItmList * GetEngineeringItemResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetEngineeringItemResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_EngItmList * GetEngineeringItemResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetEngineeringCategoryList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSString * strLastModified;
	NSString * strHotel_ID;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetEngineeringCategoryList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSString * strLastModified;
@property (nonatomic, retain) NSString * strHotel_ID;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_EngineeringCategoryList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * encCatID;
	NSString * encCatName;
	NSString * encCatLang;
	NSData * encPicture;
	NSString * encCatLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_EngineeringCategoryList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * encCatID;
@property (nonatomic, retain) NSString * encCatName;
@property (nonatomic, retain) NSString * encCatLang;
@property (nonatomic, retain) NSData * encPicture;
@property (nonatomic, retain) NSString * encCatLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfEngineeringCategoryList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *EngineeringCategoryList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfEngineeringCategoryList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addEngineeringCategoryList:(eHousekeepingService_EngineeringCategoryList *)toAdd;
@property (nonatomic, readonly) NSMutableArray * EngineeringCategoryList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_EngCatList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_ArrayOfEngineeringCategoryList * EngineeringCatList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_EngCatList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_ArrayOfEngineeringCategoryList * EngineeringCatList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetEngineeringCategoryListResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_EngCatList * GetEngineeringCategoryListResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetEngineeringCategoryListResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_EngCatList * GetEngineeringCategoryListResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetLaundrySpecialInstructionList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSString * strLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetLaundrySpecialInstructionList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSString * strLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_LaundryInstructionList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * ldrySpcInstructionID;
	NSString * ldrySpcInstructionName;
	NSString * ldrySpcInstructionLang;
	NSString * ldrySpcLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_LaundryInstructionList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * ldrySpcInstructionID;
@property (nonatomic, retain) NSString * ldrySpcInstructionName;
@property (nonatomic, retain) NSString * ldrySpcInstructionLang;
@property (nonatomic, retain) NSString * ldrySpcLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfLaundryInstructionList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *LaundryInstructionList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfLaundryInstructionList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addLaundryInstructionList:(eHousekeepingService_LaundryInstructionList *)toAdd;
@property (nonatomic, readonly) NSMutableArray * LaundryInstructionList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_LdryInstList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_ArrayOfLaundryInstructionList * LdryInstructionList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_LdryInstList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_ArrayOfLaundryInstructionList * LdryInstructionList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetLaundrySpecialInstructionListResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_LdryInstList * GetLaundrySpecialInstructionListResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetLaundrySpecialInstructionListResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_LdryInstList * GetLaundrySpecialInstructionListResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetLaundryItemPriceList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSString * strLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetLaundryItemPriceList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSString * strLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_LaundryItmPriceList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * ldryPriID;
	NSNumber * ldryPriLaundryID;
	NSNumber * ldryLaundryCategoryID;
	NSNumber * ldryPrice;
	NSString * ldryLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_LaundryItmPriceList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * ldryPriID;
@property (nonatomic, retain) NSNumber * ldryPriLaundryID;
@property (nonatomic, retain) NSNumber * ldryLaundryCategoryID;
@property (nonatomic, retain) NSNumber * ldryPrice;
@property (nonatomic, retain) NSString * ldryLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfLaundryItmPriceList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *LaundryItmPriceList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfLaundryItmPriceList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addLaundryItmPriceList:(eHousekeepingService_LaundryItmPriceList *)toAdd;
@property (nonatomic, readonly) NSMutableArray * LaundryItmPriceList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_LdryItmPriceList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_ArrayOfLaundryItmPriceList * LdryItmPriceList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_LdryItmPriceList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_ArrayOfLaundryItmPriceList * LdryItmPriceList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetLaundryItemPriceListResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_LdryItmPriceList * GetLaundryItemPriceListResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetLaundryItemPriceListResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_LdryItmPriceList * GetLaundryItemPriceListResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetMinibarCategoryList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSNumber * intHotelID;
	NSString * strLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetMinibarCategoryList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSNumber * intHotelID;
@property (nonatomic, retain) NSString * strLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_MinibarCategoryList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * mbcID;
	NSString * mbcName;
	NSString * mbcLang;
	NSData * mbcPicture;
	NSString * mbcLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_MinibarCategoryList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * mbcID;
@property (nonatomic, retain) NSString * mbcName;
@property (nonatomic, retain) NSString * mbcLang;
@property (nonatomic, retain) NSData * mbcPicture;
@property (nonatomic, retain) NSString * mbcLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfMinibarCategoryList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *MinibarCategoryList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfMinibarCategoryList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addMinibarCategoryList:(eHousekeepingService_MinibarCategoryList *)toAdd;
@property (nonatomic, readonly) NSMutableArray * MinibarCategoryList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_MinibarCatList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_ArrayOfMinibarCategoryList * MinibarCat;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_MinibarCatList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_ArrayOfMinibarCategoryList * MinibarCat;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetMinibarCategoryListResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_MinibarCatList * GetMinibarCategoryListResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetMinibarCategoryListResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_MinibarCatList * GetMinibarCategoryListResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetLinenCategoryList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSNumber * intHotelID;
	NSString * strLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetLinenCategoryList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSNumber * intHotelID;
@property (nonatomic, retain) NSString * strLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_LinenCategoryList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * lncID;
	NSString * lncName;
	NSString * lncLang;
	NSData * lncPicture;
	NSString * lncLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_LinenCategoryList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * lncID;
@property (nonatomic, retain) NSString * lncName;
@property (nonatomic, retain) NSString * lncLang;
@property (nonatomic, retain) NSData * lncPicture;
@property (nonatomic, retain) NSString * lncLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfLinenCategoryList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *LinenCategoryList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfLinenCategoryList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addLinenCategoryList:(eHousekeepingService_LinenCategoryList *)toAdd;
@property (nonatomic, readonly) NSMutableArray * LinenCategoryList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_LinenCatList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_ArrayOfLinenCategoryList * LinenCat;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_LinenCatList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_ArrayOfLinenCategoryList * LinenCat;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetLinenCategoryListResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_LinenCatList * GetLinenCategoryListResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetLinenCategoryListResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_LinenCatList * GetLinenCategoryListResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetAmenitiesCategoryList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSNumber * intHotelID;
	NSString * strLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetAmenitiesCategoryList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSNumber * intHotelID;
@property (nonatomic, retain) NSString * strLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_AmenitiesCategoryList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * amcID;
	NSString * amcName;
	NSString * amcLang;
	NSData * amcPicture;
	NSString * amcLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_AmenitiesCategoryList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * amcID;
@property (nonatomic, retain) NSString * amcName;
@property (nonatomic, retain) NSString * amcLang;
@property (nonatomic, retain) NSData * amcPicture;
@property (nonatomic, retain) NSString * amcLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfAmenitiesCategoryList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *AmenitiesCategoryList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfAmenitiesCategoryList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addAmenitiesCategoryList:(eHousekeepingService_AmenitiesCategoryList *)toAdd;
@property (nonatomic, readonly) NSMutableArray * AmenitiesCategoryList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_AmenitiesCatList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_ArrayOfAmenitiesCategoryList * AmenitiesCat;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_AmenitiesCatList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_ArrayOfAmenitiesCategoryList * AmenitiesCat;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetAmenitiesCategoryListResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_AmenitiesCatList * GetAmenitiesCategoryListResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetAmenitiesCategoryListResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_AmenitiesCatList * GetAmenitiesCategoryListResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetMinibarItemList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSNumber * intHotelID;
	NSString * intCategoryID;
	NSString * strLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetMinibarItemList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSNumber * intHotelID;
@property (nonatomic, retain) NSString * intCategoryID;
@property (nonatomic, retain) NSString * strLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_MinibarItemList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * mbiID;
	NSString * mbiName;
	NSString * mbiLang;
	NSNumber * mbiCategoryID;
	NSNumber * mbiPrice;
	NSData * mbiPicture;
	NSString * mbiLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_MinibarItemList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * mbiID;
@property (nonatomic, retain) NSString * mbiName;
@property (nonatomic, retain) NSString * mbiLang;
@property (nonatomic, retain) NSNumber * mbiCategoryID;
@property (nonatomic, retain) NSNumber * mbiPrice;
@property (nonatomic, retain) NSData * mbiPicture;
@property (nonatomic, retain) NSString * mbiLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfMinibarItemList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *MinibarItemList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfMinibarItemList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addMinibarItemList:(eHousekeepingService_MinibarItemList *)toAdd;
@property (nonatomic, readonly) NSMutableArray * MinibarItemList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_MinibarItmList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_ArrayOfMinibarItemList * MinibarItm;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_MinibarItmList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_ArrayOfMinibarItemList * MinibarItm;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetMinibarItemListResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_MinibarItmList * GetMinibarItemListResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetMinibarItemListResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_MinibarItmList * GetMinibarItemListResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetLinenItemList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSNumber * intHotelID;
	NSString * intCategoryID;
	NSString * strLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetLinenItemList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSNumber * intHotelID;
@property (nonatomic, retain) NSString * intCategoryID;
@property (nonatomic, retain) NSString * strLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_LinenItemList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * lniID;
	NSString * lniName;
	NSString * lniLang;
	NSNumber * lniCategoryID;
	NSData * lniPicture;
	NSString * lniLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_LinenItemList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * lniID;
@property (nonatomic, retain) NSString * lniName;
@property (nonatomic, retain) NSString * lniLang;
@property (nonatomic, retain) NSNumber * lniCategoryID;
@property (nonatomic, retain) NSData * lniPicture;
@property (nonatomic, retain) NSString * lniLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfLinenItemList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *LinenItemList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfLinenItemList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addLinenItemList:(eHousekeepingService_LinenItemList *)toAdd;
@property (nonatomic, readonly) NSMutableArray * LinenItemList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_LinenItmList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_ArrayOfLinenItemList * LinenItm;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_LinenItmList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_ArrayOfLinenItemList * LinenItm;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetLinenItemListResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_LinenItmList * GetLinenItemListResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetLinenItemListResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_LinenItmList * GetLinenItemListResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetAmenitiesItemList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSNumber * intHotelID;
	NSString * intCategoryID;
	NSString * strLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetAmenitiesItemList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSNumber * intHotelID;
@property (nonatomic, retain) NSString * intCategoryID;
@property (nonatomic, retain) NSString * strLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_AmenitiesItemList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * amiID;
	NSString * amiName;
	NSString * amiLang;
	NSNumber * amiCategoryID;
	NSNumber * amiRoomTypeID;
	NSNumber * amiInventoryLocationID;
	NSData * amiPicture;
	NSString * amiLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_AmenitiesItemList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * amiID;
@property (nonatomic, retain) NSString * amiName;
@property (nonatomic, retain) NSString * amiLang;
@property (nonatomic, retain) NSNumber * amiCategoryID;
@property (nonatomic, retain) NSNumber * amiRoomTypeID;
@property (nonatomic, retain) NSNumber * amiInventoryLocationID;
@property (nonatomic, retain) NSData * amiPicture;
@property (nonatomic, retain) NSString * amiLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfAmenitiesItemList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *AmenitiesItemList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfAmenitiesItemList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addAmenitiesItemList:(eHousekeepingService_AmenitiesItemList *)toAdd;
@property (nonatomic, readonly) NSMutableArray * AmenitiesItemList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_AmenitiesItmList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_ArrayOfAmenitiesItemList * AmenitiesItm;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_AmenitiesItmList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_ArrayOfAmenitiesItemList * AmenitiesItm;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetAmenitiesItemListResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_AmenitiesItmList * GetAmenitiesItemListResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetAmenitiesItemListResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_AmenitiesItmList * GetAmenitiesItemListResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetAmenitiesLocationList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSString * strLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetAmenitiesLocationList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSString * strLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_AmenitiesLocationList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * amlID;
	NSString * amlLocationName;
	NSString * amlLocationLang;
	NSString * amlLocationLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_AmenitiesLocationList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * amlID;
@property (nonatomic, retain) NSString * amlLocationName;
@property (nonatomic, retain) NSString * amlLocationLang;
@property (nonatomic, retain) NSString * amlLocationLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfAmenitiesLocationList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *AmenitiesLocationList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfAmenitiesLocationList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addAmenitiesLocationList:(eHousekeepingService_AmenitiesLocationList *)toAdd;
@property (nonatomic, readonly) NSMutableArray * AmenitiesLocationList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_AmenitiesLocList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_ArrayOfAmenitiesLocationList * AmenitiesLocLst;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_AmenitiesLocList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_ArrayOfAmenitiesLocationList * AmenitiesLocLst;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetAmenitiesLocationListResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_AmenitiesLocList * GetAmenitiesLocationListResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetAmenitiesLocationListResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_AmenitiesLocList * GetAmenitiesLocationListResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostLaundryOrder : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSNumber * intRoomAssignID;
	NSNumber * intLaundryServiceID;
	NSNumber * intTotalQuantity;
	NSNumber * dblServiceCharge;
	NSNumber * dblSubTotal;
	NSString * strTransactionTime;
	NSNumber * intHotelID;
	NSString * strLaundryRmk;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostLaundryOrder *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSNumber * intRoomAssignID;
@property (nonatomic, retain) NSNumber * intLaundryServiceID;
@property (nonatomic, retain) NSNumber * intTotalQuantity;
@property (nonatomic, retain) NSNumber * dblServiceCharge;
@property (nonatomic, retain) NSNumber * dblSubTotal;
@property (nonatomic, retain) NSString * strTransactionTime;
@property (nonatomic, retain) NSNumber * intHotelID;
@property (nonatomic, retain) NSString * strLaundryRmk;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostLaundryOrderResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * PostLaundryOrderResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostLaundryOrderResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * PostLaundryOrderResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostLaundrySpecialInstruction : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSNumber * intLaundryOrderID;
	NSNumber * intLaundrySpcialInstructionID;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostLaundrySpecialInstruction *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSNumber * intLaundryOrderID;
@property (nonatomic, retain) NSNumber * intLaundrySpcialInstructionID;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostLaundrySpecialInstructionResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * PostLaundrySpecialInstructionResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostLaundrySpecialInstructionResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * PostLaundrySpecialInstructionResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostMinibarItem : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSNumber * intRoomAssignID;
	NSNumber * intMinibarOrderID;
	NSNumber * intMinibarItemID;
	NSNumber * intNewQuantity;
	NSNumber * intUsedQuantity;
	NSString * strTransactionTime;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostMinibarItem *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSNumber * intRoomAssignID;
@property (nonatomic, retain) NSNumber * intMinibarOrderID;
@property (nonatomic, retain) NSNumber * intMinibarItemID;
@property (nonatomic, retain) NSNumber * intNewQuantity;
@property (nonatomic, retain) NSNumber * intUsedQuantity;
@property (nonatomic, retain) NSString * strTransactionTime;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostMinibarList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	NSNumber * mbID;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostMinibarList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) NSNumber * mbID;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostMinibarItemResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_PostMinibarList * PostMinibarItemResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostMinibarItemResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_PostMinibarList * PostMinibarItemResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostLinenItem : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSNumber * intRoomAssignID;
	NSNumber * intLinenItemID;
	NSNumber * intNewQuantity;
	NSNumber * intUsedQuantity;
	NSString * strTransactionTime;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostLinenItem *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSNumber * intRoomAssignID;
@property (nonatomic, retain) NSNumber * intLinenItemID;
@property (nonatomic, retain) NSNumber * intNewQuantity;
@property (nonatomic, retain) NSNumber * intUsedQuantity;
@property (nonatomic, retain) NSString * strTransactionTime;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostLinenList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	NSNumber * lnID;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostLinenList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) NSNumber * lnID;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostLinenItemResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_PostLinenList * PostLinenItemResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostLinenItemResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_PostLinenList * PostLinenItemResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostItem : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intItemID;
	NSNumber * intNewQuantity;
	NSNumber * intUsedQuantity;
	NSString * strTransactionTime;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostItem *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intItemID;
@property (nonatomic, retain) NSNumber * intNewQuantity;
@property (nonatomic, retain) NSNumber * intUsedQuantity;
@property (nonatomic, retain) NSString * strTransactionTime;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfPostItem : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *PostItem;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfPostItem *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addPostItem:(eHousekeepingService_PostItem *)toAdd;
@property (nonatomic, readonly) NSMutableArray * PostItem;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostMinibarItems : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUserID;
	NSNumber * intRoomAssignID;
	NSNumber * intMinibarOrderID;
	eHousekeepingService_ArrayOfPostItem * PostItems;
	eHousekeepingService_UniqueRoom * urRoom;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostMinibarItems *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUserID;
@property (nonatomic, retain) NSNumber * intRoomAssignID;
@property (nonatomic, retain) NSNumber * intMinibarOrderID;
@property (nonatomic, retain) eHousekeepingService_ArrayOfPostItem * PostItems;
@property (nonatomic, retain) eHousekeepingService_UniqueRoom * urRoom;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostedItem : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * ID_;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostedItem *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * ID_;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfPostedItem : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *PostedItem;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfPostedItem *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addPostedItem:(eHousekeepingService_PostedItem *)toAdd;
@property (nonatomic, readonly) NSMutableArray * PostedItem;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostMinibarItemsList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_ArrayOfPostedItem * MinibarItems;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostMinibarItemsList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_ArrayOfPostedItem * MinibarItems;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostMinibarItemsResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_PostMinibarItemsList * PostMinibarItemsResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostMinibarItemsResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_PostMinibarItemsList * PostMinibarItemsResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostLinenItems : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUserID;
	NSNumber * intRoomAssignID;
	eHousekeepingService_ArrayOfPostItem * PostItems;
	eHousekeepingService_UniqueRoom * urRoom;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostLinenItems *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUserID;
@property (nonatomic, retain) NSNumber * intRoomAssignID;
@property (nonatomic, retain) eHousekeepingService_ArrayOfPostItem * PostItems;
@property (nonatomic, retain) eHousekeepingService_UniqueRoom * urRoom;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostLinenItemsList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_ArrayOfPostedItem * LinenItems;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostLinenItemsList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_ArrayOfPostedItem * LinenItems;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostLinenItemsResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_PostLinenItemsList * PostLinenItemsResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostLinenItemsResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_PostLinenItemsList * PostLinenItemsResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostChecklistItem : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intChecklistItemID;
	NSNumber * intScoreValue;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostChecklistItem *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intChecklistItemID;
@property (nonatomic, retain) NSNumber * intScoreValue;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfPostChecklistItem : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *PostChecklistItem;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfPostChecklistItem *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addPostChecklistItem:(eHousekeepingService_PostChecklistItem *)toAdd;
@property (nonatomic, readonly) NSMutableArray * PostChecklistItem;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostChecklistItems : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUserID;
	NSNumber * intRoomAssignID;
	NSNumber * intChecklistID;
	eHousekeepingService_ArrayOfPostChecklistItem * PostChkLstItems;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostChecklistItems *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUserID;
@property (nonatomic, retain) NSNumber * intRoomAssignID;
@property (nonatomic, retain) NSNumber * intChecklistID;
@property (nonatomic, retain) eHousekeepingService_ArrayOfPostChecklistItem * PostChkLstItems;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostChecklistItemsList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_ArrayOfPostedItem * ChecklistItems;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostChecklistItemsList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_ArrayOfPostedItem * ChecklistItems;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostChecklistItemsResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_PostChecklistItemsList * PostChecklistItemsResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostChecklistItemsResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_PostChecklistItemsList * PostChecklistItemsResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostingLinenItems : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUserID;
	NSNumber * intRoomAssignID;
	NSString * strRoomNo;
	eHousekeepingService_ArrayOfPostItem * PostItems;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostingLinenItems *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUserID;
@property (nonatomic, retain) NSNumber * intRoomAssignID;
@property (nonatomic, retain) NSString * strRoomNo;
@property (nonatomic, retain) eHousekeepingService_ArrayOfPostItem * PostItems;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostingLinenItemsList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_ArrayOfPostedItem * LinenItems;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostingLinenItemsList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_ArrayOfPostedItem * LinenItems;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostingLinenItemsResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_PostingLinenItemsList * PostingLinenItemsResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostingLinenItemsResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_PostingLinenItemsList * PostingLinenItemsResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostAmenitiesItem : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSNumber * intRoomAssignID;
	NSNumber * intAmenitiesItemID;
	NSNumber * intNewQuantity;
	NSString * strTransactionTime;
	eHousekeepingService_UniqueRoom * urRoom;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostAmenitiesItem *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSNumber * intRoomAssignID;
@property (nonatomic, retain) NSNumber * intAmenitiesItemID;
@property (nonatomic, retain) NSNumber * intNewQuantity;
@property (nonatomic, retain) NSString * strTransactionTime;
@property (nonatomic, retain) eHousekeepingService_UniqueRoom * urRoom;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostAmenitiesList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	NSNumber * AmnID;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostAmenitiesList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) NSNumber * AmnID;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostAmenitiesItemResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_PostAmenitiesList * PostAmenitiesItemResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostAmenitiesItemResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_PostAmenitiesList * PostAmenitiesItemResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetChecklistCategoryList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSNumber * intHotelID;
	NSNumber * intCheckListID;
	NSString * strLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetChecklistCategoryList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSNumber * intHotelID;
@property (nonatomic, retain) NSNumber * intCheckListID;
@property (nonatomic, retain) NSString * strLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_CheckListCategory : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * clcID;
	NSString * clcName;
	NSString * clcLang;
	NSString * clcLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_CheckListCategory *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * clcID;
@property (nonatomic, retain) NSString * clcName;
@property (nonatomic, retain) NSString * clcLang;
@property (nonatomic, retain) NSString * clcLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfCheckListCategory : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *CheckListCategory;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfCheckListCategory *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addCheckListCategory:(eHousekeepingService_CheckListCategory *)toAdd;
@property (nonatomic, readonly) NSMutableArray * CheckListCategory;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ChkListCategoryLst : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_ArrayOfCheckListCategory * ChkListCategoryLst;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ChkListCategoryLst *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_ArrayOfCheckListCategory * ChkListCategoryLst;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetChecklistCategoryListResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ChkListCategoryLst * GetChecklistCategoryListResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetChecklistCategoryListResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ChkListCategoryLst * GetChecklistCategoryListResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostMinibarOrder : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSNumber * intRoomAssignID;
	NSNumber * intTotalQuantity;
	NSNumber * dblServiceCharge;
	NSNumber * dblSubTotal;
	NSString * strTransactionTime;
	NSNumber * intHotelID;
	NSString * strRoomNo;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostMinibarOrder *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSNumber * intRoomAssignID;
@property (nonatomic, retain) NSNumber * intTotalQuantity;
@property (nonatomic, retain) NSNumber * dblServiceCharge;
@property (nonatomic, retain) NSNumber * dblSubTotal;
@property (nonatomic, retain) NSString * strTransactionTime;
@property (nonatomic, retain) NSNumber * intHotelID;
@property (nonatomic, retain) NSString * strRoomNo;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostMinibarOrderResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * PostMinibarOrderResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostMinibarOrderResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * PostMinibarOrderResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetAllChecklist : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSNumber * intHotelID;
	NSString * strLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetAllChecklist *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSNumber * intHotelID;
@property (nonatomic, retain) NSString * strLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_SupervisorCheckList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * cltID;
	NSString * cltName;
	NSString * cltLang;
	NSNumber * cltChecklistType;
	NSNumber * cltTotalPointPossible;
	NSNumber * cltPassingScore;
	NSString * cltLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_SupervisorCheckList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * cltID;
@property (nonatomic, retain) NSString * cltName;
@property (nonatomic, retain) NSString * cltLang;
@property (nonatomic, retain) NSNumber * cltChecklistType;
@property (nonatomic, retain) NSNumber * cltTotalPointPossible;
@property (nonatomic, retain) NSNumber * cltPassingScore;
@property (nonatomic, retain) NSString * cltLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfSupervisorCheckList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *SupervisorCheckList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfSupervisorCheckList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addSupervisorCheckList:(eHousekeepingService_SupervisorCheckList *)toAdd;
@property (nonatomic, readonly) NSMutableArray * SupervisorCheckList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_SprChkList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_ArrayOfSupervisorCheckList * SprChkList;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_SprChkList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_ArrayOfSupervisorCheckList * SprChkList;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetAllChecklistResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_SprChkList * GetAllChecklistResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetAllChecklistResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_SprChkList * GetAllChecklistResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetChecklistItemList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSNumber * intHotelID;
	NSNumber * intCheckListID;
	NSString * strLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetChecklistItemList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSNumber * intHotelID;
@property (nonatomic, retain) NSNumber * intCheckListID;
@property (nonatomic, retain) NSString * strLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_CheckListItem : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * cliID;
	NSString * cliName;
	NSString * cliLang;
	NSNumber * cliChecklistID;
	NSNumber * cliCategoryID;
	NSNumber * cliRatingValue;
	NSNumber * cliSortOrder;
	NSString * cliLastModified;
	NSNumber * cliMandatoryPass;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_CheckListItem *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * cliID;
@property (nonatomic, retain) NSString * cliName;
@property (nonatomic, retain) NSString * cliLang;
@property (nonatomic, retain) NSNumber * cliChecklistID;
@property (nonatomic, retain) NSNumber * cliCategoryID;
@property (nonatomic, retain) NSNumber * cliRatingValue;
@property (nonatomic, retain) NSNumber * cliSortOrder;
@property (nonatomic, retain) NSString * cliLastModified;
@property (nonatomic, retain) NSNumber * cliMandatoryPass;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfCheckListItem : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *CheckListItem;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfCheckListItem *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addCheckListItem:(eHousekeepingService_CheckListItem *)toAdd;
@property (nonatomic, readonly) NSMutableArray * CheckListItem;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ChkListItm : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_ArrayOfCheckListItem * ChkLstItm;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ChkListItm *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_ArrayOfCheckListItem * ChkLstItm;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetChecklistItemListResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ChkListItm * GetChecklistItemListResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetChecklistItemListResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ChkListItm * GetChecklistItemListResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetChecklistRoomType : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSNumber * intHotelID;
	NSString * strLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetChecklistRoomType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSNumber * intHotelID;
@property (nonatomic, retain) NSString * strLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_CheckListRoomType : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * clrtID;
	NSNumber * clrtRoomTypeID;
	NSNumber * clrtChecklistID;
	NSString * clrtLastModified;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_CheckListRoomType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * clrtID;
@property (nonatomic, retain) NSNumber * clrtRoomTypeID;
@property (nonatomic, retain) NSNumber * clrtChecklistID;
@property (nonatomic, retain) NSString * clrtLastModified;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfCheckListRoomType : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *CheckListRoomType;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfCheckListRoomType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addCheckListRoomType:(eHousekeepingService_CheckListRoomType *)toAdd;
@property (nonatomic, readonly) NSMutableArray * CheckListRoomType;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ChkListRmTyp : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_ArrayOfCheckListRoomType * ChkListRmTyp;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ChkListRmTyp *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_ArrayOfCheckListRoomType * ChkListRmTyp;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetChecklistRoomTypeResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ChkListRmTyp * GetChecklistRoomTypeResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetChecklistRoomTypeResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ChkListRmTyp * GetChecklistRoomTypeResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostChecklistItemResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * PostChecklistItemResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostChecklistItemResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * PostChecklistItemResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_UpdateInspection : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSNumber * intRoomAssignID;
	NSNumber * intOverallScore;
	NSString * strInspectedStartTime;
	NSString * strInspectedEndTime;
	NSNumber * intTotalInspectTime;
	NSNumber * intInspectStatus;
	NSNumber * intRoomStatus;
	NSString * strInspectRemark;
    NSString * strChecklistRemarks;
	NSString * strPauseTime;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_UpdateInspection *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSNumber * intRoomAssignID;
@property (nonatomic, retain) NSNumber * intOverallScore;
@property (nonatomic, retain) NSString * strInspectedStartTime;
@property (nonatomic, retain) NSString * strInspectedEndTime;
@property (nonatomic, retain) NSNumber * intTotalInspectTime;
@property (nonatomic, retain) NSNumber * intInspectStatus;
@property (nonatomic, retain) NSNumber * intRoomStatus;
@property (nonatomic, retain) NSString * strInspectRemark;
@property (nonatomic, retain) NSString * strChecklistRemarks;
@property (nonatomic, retain) NSString * strPauseTime;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_UpdateInspectionResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * UpdateInspectionResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_UpdateInspectionResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * UpdateInspectionResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ChangeRoomAssignmentSequence : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSNumber * intRoomAssignID;
	NSNumber * intNextRoomAssignID;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ChangeRoomAssignmentSequence *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSNumber * intRoomAssignID;
@property (nonatomic, retain) NSNumber * intNextRoomAssignID;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ChangeRoomAssignmentSequenceResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ChangeRoomAssignmentSequenceResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ChangeRoomAssignmentSequenceResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ChangeRoomAssignmentSequenceResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetMessageAttachPhoto : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSNumber * intGrpMsgID;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetMessageAttachPhoto *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSNumber * intGrpMsgID;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_AdHocPhoto : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * amaID;
	NSData * amaPhoto;
	NSNumber * amaGrpID;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_AdHocPhoto *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * amaID;
@property (nonatomic, retain) NSData * amaPhoto;
@property (nonatomic, retain) NSNumber * amaGrpID;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_ArrayOfAdHocPhoto : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSMutableArray *AdHocPhoto;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_ArrayOfAdHocPhoto *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
- (void)addAdHocPhoto:(eHousekeepingService_AdHocPhoto *)toAdd;
@property (nonatomic, readonly) NSMutableArray * AdHocPhoto;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_AdHocMsgPhoto : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * ResponseStatus;
	eHousekeepingService_ArrayOfAdHocPhoto * AdHocMessagePhoto;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_AdHocMsgPhoto *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * ResponseStatus;
@property (nonatomic, retain) eHousekeepingService_ArrayOfAdHocPhoto * AdHocMessagePhoto;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_GetMessageAttachPhotoResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_AdHocMsgPhoto * GetMessageAttachPhotoResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_GetMessageAttachPhotoResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_AdHocMsgPhoto * GetMessageAttachPhotoResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostMessageAttachPhoto : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSNumber * intUsrID;
	NSData * binPhoto;
	NSNumber * intGrpMsgID;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostMessageAttachPhoto *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSNumber * intUsrID;
@property (nonatomic, retain) NSData * binPhoto;
@property (nonatomic, retain) NSNumber * intGrpMsgID;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface eHousekeepingService_PostMessageAttachPhotoResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	eHousekeepingService_ResponseStatus * PostMessageAttachPhotoResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (eHousekeepingService_PostMessageAttachPhotoResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) eHousekeepingService_ResponseStatus * PostMessageAttachPhotoResult;
/* attributes */
- (NSDictionary *)attributes;
@end
/* Cookies handling provided by http://en.wikibooks.org/wiki/Programming:WebObjects/Web_Services/Web_Service_Provider */
#import <libxml/parser.h>
#import "xsd.h"
#import "eHousekeepingService.h"
@class eHousekeepingServiceSoapBinding;
@class eHousekeepingServiceSoap12Binding;
@interface eHousekeepingService : NSObject {
	
}
+ (eHousekeepingServiceSoapBinding *)eHousekeepingServiceSoapBinding;
+ (eHousekeepingServiceSoap12Binding *)eHousekeepingServiceSoap12Binding;
@end
@class eHousekeepingServiceSoapBindingResponse;
@class eHousekeepingServiceSoapBindingOperation;
@protocol eHousekeepingServiceSoapBindingResponseDelegate <NSObject>
- (void) operation:(eHousekeepingServiceSoapBindingOperation *)operation completedWithResponse:(eHousekeepingServiceSoapBindingResponse *)response;
@end
#define kServerAnchorCertificates   @"kServerAnchorCertificates"
#define kServerAnchorsOnly          @"kServerAnchorsOnly"
#define kClientIdentity             @"kClientIdentity"
#define kClientCertificates         @"kClientCertificates"
#define kClientUsername             @"kClientUsername"
#define kClientPassword             @"kClientPassword"
#define kNSURLCredentialPersistence @"kNSURLCredentialPersistence"
#define kValidateResult             @"kValidateResult"
@interface eHousekeepingServiceSoapBinding : NSObject <eHousekeepingServiceSoapBindingResponseDelegate> {
	NSURL *address;
	NSTimeInterval timeout;
	NSMutableArray *cookies;
	NSMutableDictionary *customHeaders;
	BOOL logXMLInOut;
	BOOL ignoreEmptyResponse;
	BOOL synchronousOperationComplete;
	id<SSLCredentialsManaging> sslManager;
	SOAPSigner *soapSigner;
}
@property (nonatomic, copy) NSURL *address;
@property (nonatomic) BOOL logXMLInOut;
@property (nonatomic) BOOL ignoreEmptyResponse;
@property (nonatomic) NSTimeInterval timeout;
@property (nonatomic, retain) NSMutableArray *cookies;
@property (nonatomic, retain) NSMutableDictionary *customHeaders;
@property (nonatomic, retain) id<SSLCredentialsManaging> sslManager;
@property (nonatomic, retain) SOAPSigner *soapSigner;
+ (NSTimeInterval) defaultTimeout;
- (id)initWithAddress:(NSString *)anAddress;
- (void)sendHTTPCallUsingBody:(NSString *)body soapAction:(NSString *)soapAction forOperation:(eHousekeepingServiceSoapBindingOperation *)operation;
//Hao Tran - For Retry request many times
- (void) retryWith:(eHousekeepingServiceSoapBindingOperation *)operation;
- (void)addCookie:(NSHTTPCookie *)toAdd;
- (NSString *)MIMEType;
- (eHousekeepingServiceSoapBindingResponse *)GetZoneListUsingParameters:(eHousekeepingService_GetZoneList *)aParameters ;
- (void)GetZoneListAsyncUsingParameters:(eHousekeepingService_GetZoneList *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetUnassignRoomListUsingParameters:(eHousekeepingService_GetUnassignRoomList *)aParameters ;
- (void)GetUnassignRoomListAsyncUsingParameters:(eHousekeepingService_GetUnassignRoomList *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)FindAttendantUsingParameters:(eHousekeepingService_FindAttendant *)aParameters ;
- (void)FindAttendantAsyncUsingParameters:(eHousekeepingService_FindAttendant *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetZoneRoomListUsingParameters:(eHousekeepingService_GetZoneRoomList *)aParameters ;
- (void)GetZoneRoomListAsyncUsingParameters:(eHousekeepingService_GetZoneRoomList *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetCheckListItemScoreForMaidUsingParameters:(eHousekeepingService_GetCheckListItemScoreForMaid *)aParameters ;
- (void)GetCheckListItemScoreForMaidAsyncUsingParameters:(eHousekeepingService_GetCheckListItemScoreForMaid *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetInspectionStatusUsingParameters:(eHousekeepingService_GetInspectionStatus *)aParameters ;
- (void)GetInspectionStatusAsyncUsingParameters:(eHousekeepingService_GetInspectionStatus *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)PostWSLogUsingParameters:(eHousekeepingService_PostWSLog *)aParameters ;
- (void)PostWSLogAsyncUsingParameters:(eHousekeepingService_PostWSLog *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)ValidateRoomNoUsingParameters:(eHousekeepingService_ValidateRoomNo *)aParameters ;
- (void)ValidateRoomNoAsyncUsingParameters:(eHousekeepingService_ValidateRoomNo *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetAdditionalJobFloorListUsingParameters:(eHousekeepingService_GetAdditionalJobFloorList *)aParameters ;
- (void)GetAdditionalJobFloorListAsyncUsingParameters:(eHousekeepingService_GetAdditionalJobFloorList *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetAdditionalJobTaskUsingParameters:(eHousekeepingService_GetAdditionalJobTask *)aParameters ;
- (void)GetAdditionalJobTaskAsyncUsingParameters:(eHousekeepingService_GetAdditionalJobTask *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetMessageCategoryList2UsingParameters:(eHousekeepingService_GetMessageCategoryList2 *)aParameters ;
- (void)GetMessageCategoryList2AsyncUsingParameters:(eHousekeepingService_GetMessageCategoryList2 *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetMessageItemList2UsingParameters:(eHousekeepingService_GetMessageItemList2 *)aParameters ;
- (void)GetMessageItemList2AsyncUsingParameters:(eHousekeepingService_GetMessageItemList2 *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetNoOfAdHocMessageUsingParameters:(eHousekeepingService_GetNoOfAdHocMessage *)aParameters ;
- (void)GetNoOfAdHocMessageAsyncUsingParameters:(eHousekeepingService_GetNoOfAdHocMessage *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetSentAdHocMessageUsingParameters:(eHousekeepingService_GetSentAdHocMessage *)aParameters ;
- (void)GetSentAdHocMessageAsyncUsingParameters:(eHousekeepingService_GetSentAdHocMessage *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetReceivedAdHocMessageUsingParameters:(eHousekeepingService_GetReceivedAdHocMessage *)aParameters ;
- (void)GetReceivedAdHocMessageAsyncUsingParameters:(eHousekeepingService_GetReceivedAdHocMessage *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)UpdateAdHocMessageStatusUsingParameters:(eHousekeepingService_UpdateAdHocMessageStatus *)aParameters ;
- (void)UpdateAdHocMessageStatusAsyncUsingParameters:(eHousekeepingService_UpdateAdHocMessageStatus *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetAdditionalJobRoomByFloorUsingParameters:(eHousekeepingService_GetAdditionalJobRoomByFloor *)aParameters ;
- (void)GetAdditionalJobRoomByFloorAsyncUsingParameters:(eHousekeepingService_GetAdditionalJobRoomByFloor *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetAdditionalJobDetailUsingParameters:(eHousekeepingService_GetAdditionalJobDetail *)aParameters ;
- (void)GetAdditionalJobDetailAsyncUsingParameters:(eHousekeepingService_GetAdditionalJobDetail *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)CompleteAdditionalJobUsingParameters:(eHousekeepingService_CompleteAdditionalJob *)aParameters ;
- (void)CompleteAdditionalJobAsyncUsingParameters:(eHousekeepingService_CompleteAdditionalJob *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetAdditionalJobCountUsingParameters:(eHousekeepingService_GetAdditionalJobCount *)aParameters ;
- (void)GetAdditionalJobCountAsyncUsingParameters:(eHousekeepingService_GetAdditionalJobCount *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)UpdateAdditionalJobStatusUsingParameters:(eHousekeepingService_UpdateAdditionalJobStatus *)aParameters ;
- (void)UpdateAdditionalJobStatusAsyncUsingParameters:(eHousekeepingService_UpdateAdditionalJobStatus *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetProfileNoteUsingParameters:(eHousekeepingService_GetProfileNote *)aParameters ;
- (void)GetProfileNoteAsyncUsingParameters:(eHousekeepingService_GetProfileNote *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)StartAdditionalJobUsingParameters:(eHousekeepingService_StartAdditionalJob *)aParameters ;
- (void)StartAdditionalJobAsyncUsingParameters:(eHousekeepingService_StartAdditionalJob *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)StopAdditionalJobUsingParameters:(eHousekeepingService_StopAdditionalJob *)aParameters ;
- (void)StopAdditionalJobAsyncUsingParameters:(eHousekeepingService_StopAdditionalJob *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetAdditionalJobOfRoomUsingParameters:(eHousekeepingService_GetAdditionalJobOfRoom *)aParameters ;
- (void)GetAdditionalJobOfRoomAsyncUsingParameters:(eHousekeepingService_GetAdditionalJobOfRoom *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetGuestInfoByRoomIDUsingParameters:(eHousekeepingService_GetGuestInfoByRoomID *)aParameters ;
- (void)GetGuestInfoByRoomIDAsyncUsingParameters:(eHousekeepingService_GetGuestInfoByRoomID *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetNumberOfGuestUsingParameters:(eHousekeepingService_GetNumberOfGuest *)aParameters ;
- (void)GetNumberOfGuestAsyncUsingParameters:(eHousekeepingService_GetNumberOfGuest *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)UpdateAdditionalJobRemarkUsingParameters:(eHousekeepingService_UpdateAdditionalJobRemark *)aParameters ;
- (void)UpdateAdditionalJobRemarkAsyncUsingParameters:(eHousekeepingService_UpdateAdditionalJobRemark *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetAdditionalJobByRoomNoUsingParameters:(eHousekeepingService_GetAdditionalJobByRoomNo *)aParameters ;
- (void)GetAdditionalJobByRoomNoAsyncUsingParameters:(eHousekeepingService_GetAdditionalJobByRoomNo *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)FindRoomByRoomNoUsingParameters:(eHousekeepingService_FindRoomByRoomNo *)aParameters ;
- (void)FindRoomByRoomNoAsyncUsingParameters:(eHousekeepingService_FindRoomByRoomNo *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)PostAmenitiesItemsListUsingParameters:(eHousekeepingService_PostAmenitiesItemsList *)aParameters ;
- (void)PostAmenitiesItemsListAsyncUsingParameters:(eHousekeepingService_PostAmenitiesItemsList *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetPanicAlertConfigUsingParameters:(eHousekeepingService_GetPanicAlertConfig *)aParameters ;
- (void)GetPanicAlertConfigAsyncUsingParameters:(eHousekeepingService_GetPanicAlertConfig *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)PostingPanicAlertUsingParameters:(eHousekeepingService_PostingPanicAlert *)aParameters ;
- (void)PostingPanicAlertAsyncUsingParameters:(eHousekeepingService_PostingPanicAlert *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetGuestInformationUsingParameters:(eHousekeepingService_GetGuestInformation *)aParameters ;
- (void)GetGuestInformationAsyncUsingParameters:(eHousekeepingService_GetGuestInformation *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetVersionUsingParameters:(eHousekeepingService_GetVersion *)aParameters ;
- (void)GetVersionAsyncUsingParameters:(eHousekeepingService_GetVersion *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetRoomRemarksUsingParameters:(eHousekeepingService_GetRoomRemarks *)aParameters ;
- (void)GetRoomRemarksAsyncUsingParameters:(eHousekeepingService_GetRoomRemarks *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)UpdateRoomRemarksUsingParameters:(eHousekeepingService_UpdateRoomRemarks *)aParameters ;
- (void)UpdateRoomRemarksAsyncUsingParameters:(eHousekeepingService_UpdateRoomRemarks *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetOOSBlockRoomsListUsingParameters:(eHousekeepingService_GetOOSBlockRoomsList *)aParameters ;
- (void)GetOOSBlockRoomsListAsyncUsingParameters:(eHousekeepingService_GetOOSBlockRoomsList *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetReleaseRoomsListUsingParameters:(eHousekeepingService_GetReleaseRoomsList *)aParameters ;
- (void)GetReleaseRoomsListAsyncUsingParameters:(eHousekeepingService_GetReleaseRoomsList *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetFindFloorDetailsListUsingParameters:(eHousekeepingService_GetFindFloorDetailsList *)aParameters ;
- (void)GetFindFloorDetailsListAsyncUsingParameters:(eHousekeepingService_GetFindFloorDetailsList *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetFindRoomDetailsListUsingParameters:(eHousekeepingService_GetFindRoomDetailsList *)aParameters ;
- (void)GetFindRoomDetailsListAsyncUsingParameters:(eHousekeepingService_GetFindRoomDetailsList *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetStillLoggedInUsingParameters:(eHousekeepingService_GetStillLoggedIn *)aParameters ;
- (void)GetStillLoggedInAsyncUsingParameters:(eHousekeepingService_GetStillLoggedIn *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)PostLogoutUsingParameters:(eHousekeepingService_PostLogout *)aParameters ;
- (void)PostLogoutAsyncUsingParameters:(eHousekeepingService_PostLogout *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)LogInvalidStartRoomUsingParameters:(eHousekeepingService_LogInvalidStartRoom *)aParameters ;
- (void)LogInvalidStartRoomAsyncUsingParameters:(eHousekeepingService_LogInvalidStartRoom *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)AddReassignRoomUsingParameters:(eHousekeepingService_AddReassignRoom *)aParameters ;
- (void)AddReassignRoomAsyncUsingParameters:(eHousekeepingService_AddReassignRoom *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)AssignRoomUsingParameters:(eHousekeepingService_AssignRoom *)aParameters ;
- (void)AssignRoomAsyncUsingParameters:(eHousekeepingService_AssignRoom *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetSupervisorFiltersRoutineUsingParameters:(eHousekeepingService_GetSupervisorFiltersRoutine *)aParameters ;
- (void)GetSupervisorFiltersRoutineAsyncUsingParameters:(eHousekeepingService_GetSupervisorFiltersRoutine *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetPostingHistoryRoutineUsingParameters:(eHousekeepingService_GetPostingHistoryRoutine *)aParameters ;
- (void)GetPostingHistoryRoutineAsyncUsingParameters:(eHousekeepingService_GetPostingHistoryRoutine *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)PostPostingHistoryRoutineUsingParameters:(eHousekeepingService_PostPostingHistoryRoutine *)aParameters ;
- (void)PostPostingHistoryRoutineAsyncUsingParameters:(eHousekeepingService_PostPostingHistoryRoutine *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)PostLostAndFoundRoutineUsingParameters:(eHousekeepingService_PostLostAndFoundRoutine *)aParameters ;
- (void)PostLostAndFoundRoutineAsyncUsingParameters:(eHousekeepingService_PostLostAndFoundRoutine *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetAdditionalJobStatusesRoutineUsingParameters:(eHousekeepingService_GetAdditionalJobStatusesRoutine *)aParameters ;
- (void)GetAdditionalJobStatusesRoutineAsyncUsingParameters:(eHousekeepingService_GetAdditionalJobStatusesRoutine *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)PostAdditionalJobStatusUpdateRoutineUsingParameters:(eHousekeepingService_PostAdditionalJobStatusUpdateRoutine *)aParameters ;
- (void)PostAdditionalJobStatusUpdateRoutineAsyncUsingParameters:(eHousekeepingService_PostAdditionalJobStatusUpdateRoutine *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)FindGuestStayedHistoryRoutineUsingParameters:(eHousekeepingService_FindGuestStayedHistoryRoutine *)aParameters ;
- (void)FindGuestStayedHistoryRoutineAsyncUsingParameters:(eHousekeepingService_FindGuestStayedHistoryRoutine *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)PostUnassignRoomRoutineUsingParameters:(eHousekeepingService_PostUnassignRoomRoutine *)aParameters ;
- (void)PostUnassignRoomRoutineAsyncUsingParameters:(eHousekeepingService_PostUnassignRoomRoutine *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)PostUpdateRoomDetailsRoutineUsingParameters:(eHousekeepingService_PostUpdateRoomDetailsRoutine *)aParameters ;
- (void)PostUpdateRoomDetailsRoutineAsyncUsingParameters:(eHousekeepingService_PostUpdateRoomDetailsRoutine *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetRoomTypeInventoryRoutineUsingParameters:(eHousekeepingService_GetRoomTypeInventoryRoutine *)aParameters ;
- (void)GetRoomTypeInventoryRoutineAsyncUsingParameters:(eHousekeepingService_GetRoomTypeInventoryRoutine *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetRoomTypeByRoomNoRoutineUsingParameters:(eHousekeepingService_GetRoomTypeByRoomNoRoutine *)aParameters ;
- (void)GetRoomTypeByRoomNoRoutineAsyncUsingParameters:(eHousekeepingService_GetRoomTypeByRoomNoRoutine *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)PostUpdateAdHocMessageRoutineUsingParameters:(eHousekeepingService_PostUpdateAdHocMessageRoutine *)aParameters ;
- (void)PostUpdateAdHocMessageRoutineAsyncUsingParameters:(eHousekeepingService_PostUpdateAdHocMessageRoutine *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetNoOfAdHocMessageRoutineUsingParameters:(eHousekeepingService_GetNoOfAdHocMessageRoutine *)aParameters ;
- (void)GetNoOfAdHocMessageRoutineAsyncUsingParameters:(eHousekeepingService_GetNoOfAdHocMessageRoutine *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetReceivedAdHocMessageRoutineUsingParameters:(eHousekeepingService_GetReceivedAdHocMessageRoutine *)aParameters ;
- (void)GetReceivedAdHocMessageRoutineAsyncUsingParameters:(eHousekeepingService_GetReceivedAdHocMessageRoutine *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetSendAdHocMessageRoutineUsingParameters:(eHousekeepingService_GetSendAdHocMessageRoutine *)aParameters ;
- (void)GetSendAdHocMessageRoutineAsyncUsingParameters:(eHousekeepingService_GetSendAdHocMessageRoutine *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetOtherActivityAssignmentRoutineUsingParameters:(eHousekeepingService_GetOtherActivityAssignmentRoutine *)aParameters ;
- (void)GetOtherActivityAssignmentRoutineAsyncUsingParameters:(eHousekeepingService_GetOtherActivityAssignmentRoutine *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetOtherActivitiesStatusRoutineUsingParameters:(eHousekeepingService_GetOtherActivitiesStatusRoutine *)aParameters ;
- (void)GetOtherActivitiesStatusRoutineAsyncUsingParameters:(eHousekeepingService_GetOtherActivitiesStatusRoutine *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetOtherActivitiesLocationRoutineUsingParameters:(eHousekeepingService_GetOtherActivitiesLocationRoutine *)aParameters ;
- (void)GetOtherActivitiesLocationRoutineAsyncUsingParameters:(eHousekeepingService_GetOtherActivitiesLocationRoutine *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)PostUpdateOtherActivityStatusRoutineUsingParameters:(eHousekeepingService_PostUpdateOtherActivityStatusRoutine *)aParameters ;
- (void)PostUpdateOtherActivityStatusRoutineAsyncUsingParameters:(eHousekeepingService_PostUpdateOtherActivityStatusRoutine *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetLanguageListUsingParameters:(eHousekeepingService_GetLanguageList *)aParameters ;
- (void)GetLanguageListAsyncUsingParameters:(eHousekeepingService_GetLanguageList *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetRoomStatusListUsingParameters:(eHousekeepingService_GetRoomStatusList *)aParameters ;
- (void)GetRoomStatusListAsyncUsingParameters:(eHousekeepingService_GetRoomStatusList *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetCleaningStatusListUsingParameters:(eHousekeepingService_GetCleaningStatusList *)aParameters ;
- (void)GetCleaningStatusListAsyncUsingParameters:(eHousekeepingService_GetCleaningStatusList *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetRoomTypeListUsingParameters:(eHousekeepingService_GetRoomTypeList *)aParameters ;
- (void)GetRoomTypeListAsyncUsingParameters:(eHousekeepingService_GetRoomTypeList *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetBuildingListUsingParameters:(eHousekeepingService_GetBuildingList *)aParameters ;
- (void)GetBuildingListAsyncUsingParameters:(eHousekeepingService_GetBuildingList *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetFloorListUsingParameters:(eHousekeepingService_GetFloorList *)aParameters ;
- (void)GetFloorListAsyncUsingParameters:(eHousekeepingService_GetFloorList *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)AuthenticateUserUsingParameters:(eHousekeepingService_AuthenticateUser *)aParameters ;
- (void)AuthenticateUserAsyncUsingParameters:(eHousekeepingService_AuthenticateUser *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetHotelInfoUsingParameters:(eHousekeepingService_GetHotelInfo *)aParameters ;
- (void)GetHotelInfoAsyncUsingParameters:(eHousekeepingService_GetHotelInfo *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetRoomAssignmentListUsingParameters:(eHousekeepingService_GetRoomAssignmentList *)aParameters ;
- (void)GetRoomAssignmentListAsyncUsingParameters:(eHousekeepingService_GetRoomAssignmentList *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetPrevRoomAssignmentListUsingParameters:(eHousekeepingService_GetPrevRoomAssignmentList *)aParameters ;
- (void)GetPrevRoomAssignmentListAsyncUsingParameters:(eHousekeepingService_GetPrevRoomAssignmentList *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetFindRoomAssignmentListUsingParameters:(eHousekeepingService_GetFindRoomAssignmentList *)aParameters ;
- (void)GetFindRoomAssignmentListAsyncUsingParameters:(eHousekeepingService_GetFindRoomAssignmentList *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetFindRoomAssignmentListsUsingParameters:(eHousekeepingService_GetFindRoomAssignmentLists *)aParameters ;
- (void)GetFindRoomAssignmentListsAsyncUsingParameters:(eHousekeepingService_GetFindRoomAssignmentLists *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetSupervisorFindFloorListUsingParameters:(eHousekeepingService_GetSupervisorFindFloorList *)aParameters ;
- (void)GetSupervisorFindFloorListAsyncUsingParameters:(eHousekeepingService_GetSupervisorFindFloorList *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetSupervisorFindRoomAssignmentListsUsingParameters:(eHousekeepingService_GetSupervisorFindRoomAssignmentLists *)aParameters ;
- (void)GetSupervisorFindRoomAssignmentListsAsyncUsingParameters:(eHousekeepingService_GetSupervisorFindRoomAssignmentLists *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetGuestInfoUsingParameters:(eHousekeepingService_GetGuestInfo *)aParameters ;
- (void)GetGuestInfoAsyncUsingParameters:(eHousekeepingService_GetGuestInfo *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetRoomDetailUsingParameters:(eHousekeepingService_GetRoomDetail *)aParameters ;
- (void)GetRoomDetailAsyncUsingParameters:(eHousekeepingService_GetRoomDetail *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)UpdateGuestInfoUsingParameters:(eHousekeepingService_UpdateGuestInfo *)aParameters ;
- (void)UpdateGuestInfoAsyncUsingParameters:(eHousekeepingService_UpdateGuestInfo *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)UpdateRoomAssignmentUsingParameters:(eHousekeepingService_UpdateRoomAssignment *)aParameters ;
- (void)UpdateRoomAssignmentAsyncUsingParameters:(eHousekeepingService_UpdateRoomAssignment *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetDiscrepantRoomStatusUsingParameters:(eHousekeepingService_GetDiscrepantRoomStatus *)aParameters ;
- (void)GetDiscrepantRoomStatusAsyncUsingParameters:(eHousekeepingService_GetDiscrepantRoomStatus *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)UpdateRoomCleaningStatusUsingParameters:(eHousekeepingService_UpdateRoomCleaningStatus *)aParameters ;
- (void)UpdateRoomCleaningStatusAsyncUsingParameters:(eHousekeepingService_UpdateRoomCleaningStatus *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)UpdateRoomStatusUsingParameters:(eHousekeepingService_UpdateRoomStatus *)aParameters ;
- (void)UpdateRoomStatusAsyncUsingParameters:(eHousekeepingService_UpdateRoomStatus *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)UpdateRoomDetailsUsingParameters:(eHousekeepingService_UpdateRoomDetails *)aParameters ;
- (void)UpdateRoomDetailsAsyncUsingParameters:(eHousekeepingService_UpdateRoomDetails *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)UpdateCleaningStatusUsingParameters:(eHousekeepingService_UpdateCleaningStatus *)aParameters ;
- (void)UpdateCleaningStatusAsyncUsingParameters:(eHousekeepingService_UpdateCleaningStatus *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetAccessRightsUsingParameters:(eHousekeepingService_GetAccessRights *)aParameters ;
- (void)GetAccessRightsAsyncUsingParameters:(eHousekeepingService_GetAccessRights *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetCommonConfigurationsUsingParameters:(eHousekeepingService_GetCommonConfigurations *)aParameters ;
- (void)GetCommonConfigurationsAsyncUsingParameters:(eHousekeepingService_GetCommonConfigurations *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetLaundryServiceListUsingParameters:(eHousekeepingService_GetLaundryServiceList *)aParameters ;
- (void)GetLaundryServiceListAsyncUsingParameters:(eHousekeepingService_GetLaundryServiceList *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetLaundryCategoryListUsingParameters:(eHousekeepingService_GetLaundryCategoryList *)aParameters ;
- (void)GetLaundryCategoryListAsyncUsingParameters:(eHousekeepingService_GetLaundryCategoryList *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetLaundryItemListUsingParameters:(eHousekeepingService_GetLaundryItemList *)aParameters ;
- (void)GetLaundryItemListAsyncUsingParameters:(eHousekeepingService_GetLaundryItemList *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)RemoveLaundryItemUsingParameters:(eHousekeepingService_RemoveLaundryItem *)aParameters ;
- (void)RemoveLaundryItemAsyncUsingParameters:(eHousekeepingService_RemoveLaundryItem *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)PostLaundryItemUsingParameters:(eHousekeepingService_PostLaundryItem *)aParameters ;
- (void)PostLaundryItemAsyncUsingParameters:(eHousekeepingService_PostLaundryItem *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetRoomSetGuideByRoomTypeUsingParameters:(eHousekeepingService_GetRoomSetGuideByRoomType *)aParameters ;
- (void)GetRoomSetGuideByRoomTypeAsyncUsingParameters:(eHousekeepingService_GetRoomSetGuideByRoomType *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetRoomSetGuideListUsingParameters:(eHousekeepingService_GetRoomSetGuideList *)aParameters ;
- (void)GetRoomSetGuideListAsyncUsingParameters:(eHousekeepingService_GetRoomSetGuideList *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetMessageTemplateListUsingParameters:(eHousekeepingService_GetMessageTemplateList *)aParameters ;
- (void)GetMessageTemplateListAsyncUsingParameters:(eHousekeepingService_GetMessageTemplateList *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetNewMessageUsingParameters:(eHousekeepingService_GetNewMessage *)aParameters ;
- (void)GetNewMessageAsyncUsingParameters:(eHousekeepingService_GetNewMessage *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetMessageCategoryListUsingParameters:(eHousekeepingService_GetMessageCategoryList *)aParameters ;
- (void)GetMessageCategoryListAsyncUsingParameters:(eHousekeepingService_GetMessageCategoryList *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetMessageItemListUsingParameters:(eHousekeepingService_GetMessageItemList *)aParameters ;
- (void)GetMessageItemListAsyncUsingParameters:(eHousekeepingService_GetMessageItemList *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetPopupMsgListUsingParameters:(eHousekeepingService_GetPopupMsgList *)aParameters ;
- (void)GetPopupMsgListAsyncUsingParameters:(eHousekeepingService_GetPopupMsgList *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)UpdatePopupMsgStatusUsingParameters:(eHousekeepingService_UpdatePopupMsgStatus *)aParameters ;
- (void)UpdatePopupMsgStatusAsyncUsingParameters:(eHousekeepingService_UpdatePopupMsgStatus *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)PostMessageUsingParameters:(eHousekeepingService_PostMessage *)aParameters ;
- (void)PostMessageAsyncUsingParameters:(eHousekeepingService_PostMessage *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)PostEngineeringCaseUsingParameters:(eHousekeepingService_PostEngineeringCase *)aParameters ;
- (void)PostEngineeringCaseAsyncUsingParameters:(eHousekeepingService_PostEngineeringCase *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)PosteCnJobUsingParameters:(eHousekeepingService_PosteCnJob *)aParameters ;
- (void)PosteCnJobAsyncUsingParameters:(eHousekeepingService_PosteCnJob *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)PosteCnAhMsgUsingParameters:(eHousekeepingService_PosteCnAhMsg *)aParameters ;
- (void)PosteCnAhMsgAsyncUsingParameters:(eHousekeepingService_PosteCnAhMsg *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)PostECAttachPhotoUsingParameters:(eHousekeepingService_PostECAttachPhoto *)aParameters ;
- (void)PostECAttachPhotoAsyncUsingParameters:(eHousekeepingService_PostECAttachPhoto *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetLFCategoryListUsingParameters:(eHousekeepingService_GetLFCategoryList *)aParameters ;
- (void)GetLFCategoryListAsyncUsingParameters:(eHousekeepingService_GetLFCategoryList *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetLFItemListUsingParameters:(eHousekeepingService_GetLFItemList *)aParameters ;
- (void)GetLFItemListAsyncUsingParameters:(eHousekeepingService_GetLFItemList *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetLFColorListUsingParameters:(eHousekeepingService_GetLFColorList *)aParameters ;
- (void)GetLFColorListAsyncUsingParameters:(eHousekeepingService_GetLFColorList *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)PostLostFoundUsingParameters:(eHousekeepingService_PostLostFound *)aParameters ;
- (void)PostLostFoundAsyncUsingParameters:(eHousekeepingService_PostLostFound *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)PostLFAttachPhotoUsingParameters:(eHousekeepingService_PostLFAttachPhoto *)aParameters ;
- (void)PostLFAttachPhotoAsyncUsingParameters:(eHousekeepingService_PostLFAttachPhoto *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetLocationListUsingParameters:(eHousekeepingService_GetLocationList *)aParameters ;
- (void)GetLocationListAsyncUsingParameters:(eHousekeepingService_GetLocationList *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetUserListUsingParameters:(eHousekeepingService_GetUserList *)aParameters ;
- (void)GetUserListAsyncUsingParameters:(eHousekeepingService_GetUserList *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetRoomSectionListUsingParameters:(eHousekeepingService_GetRoomSectionList *)aParameters ;
- (void)GetRoomSectionListAsyncUsingParameters:(eHousekeepingService_GetRoomSectionList *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetEngineeringItemUsingParameters:(eHousekeepingService_GetEngineeringItem *)aParameters ;
- (void)GetEngineeringItemAsyncUsingParameters:(eHousekeepingService_GetEngineeringItem *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetEngineeringCategoryListUsingParameters:(eHousekeepingService_GetEngineeringCategoryList *)aParameters ;
- (void)GetEngineeringCategoryListAsyncUsingParameters:(eHousekeepingService_GetEngineeringCategoryList *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetLaundrySpecialInstructionListUsingParameters:(eHousekeepingService_GetLaundrySpecialInstructionList *)aParameters ;
- (void)GetLaundrySpecialInstructionListAsyncUsingParameters:(eHousekeepingService_GetLaundrySpecialInstructionList *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetLaundryItemPriceListUsingParameters:(eHousekeepingService_GetLaundryItemPriceList *)aParameters ;
- (void)GetLaundryItemPriceListAsyncUsingParameters:(eHousekeepingService_GetLaundryItemPriceList *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetMinibarCategoryListUsingParameters:(eHousekeepingService_GetMinibarCategoryList *)aParameters ;
- (void)GetMinibarCategoryListAsyncUsingParameters:(eHousekeepingService_GetMinibarCategoryList *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetLinenCategoryListUsingParameters:(eHousekeepingService_GetLinenCategoryList *)aParameters ;
- (void)GetLinenCategoryListAsyncUsingParameters:(eHousekeepingService_GetLinenCategoryList *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetAmenitiesCategoryListUsingParameters:(eHousekeepingService_GetAmenitiesCategoryList *)aParameters ;
- (void)GetAmenitiesCategoryListAsyncUsingParameters:(eHousekeepingService_GetAmenitiesCategoryList *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetMinibarItemListUsingParameters:(eHousekeepingService_GetMinibarItemList *)aParameters ;
- (void)GetMinibarItemListAsyncUsingParameters:(eHousekeepingService_GetMinibarItemList *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetLinenItemListUsingParameters:(eHousekeepingService_GetLinenItemList *)aParameters ;
- (void)GetLinenItemListAsyncUsingParameters:(eHousekeepingService_GetLinenItemList *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetAmenitiesItemListUsingParameters:(eHousekeepingService_GetAmenitiesItemList *)aParameters ;
- (void)GetAmenitiesItemListAsyncUsingParameters:(eHousekeepingService_GetAmenitiesItemList *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetAmenitiesLocationListUsingParameters:(eHousekeepingService_GetAmenitiesLocationList *)aParameters ;
- (void)GetAmenitiesLocationListAsyncUsingParameters:(eHousekeepingService_GetAmenitiesLocationList *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)PostLaundryOrderUsingParameters:(eHousekeepingService_PostLaundryOrder *)aParameters ;
- (void)PostLaundryOrderAsyncUsingParameters:(eHousekeepingService_PostLaundryOrder *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)PostLaundrySpecialInstructionUsingParameters:(eHousekeepingService_PostLaundrySpecialInstruction *)aParameters ;
- (void)PostLaundrySpecialInstructionAsyncUsingParameters:(eHousekeepingService_PostLaundrySpecialInstruction *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)PostMinibarItemUsingParameters:(eHousekeepingService_PostMinibarItem *)aParameters ;
- (void)PostMinibarItemAsyncUsingParameters:(eHousekeepingService_PostMinibarItem *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)PostLinenItemUsingParameters:(eHousekeepingService_PostLinenItem *)aParameters ;
- (void)PostLinenItemAsyncUsingParameters:(eHousekeepingService_PostLinenItem *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)PostMinibarItemsUsingParameters:(eHousekeepingService_PostMinibarItems *)aParameters ;
- (void)PostMinibarItemsAsyncUsingParameters:(eHousekeepingService_PostMinibarItems *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)PostLinenItemsUsingParameters:(eHousekeepingService_PostLinenItems *)aParameters ;
- (void)PostLinenItemsAsyncUsingParameters:(eHousekeepingService_PostLinenItems *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)PostChecklistItemsUsingParameters:(eHousekeepingService_PostChecklistItems *)aParameters ;
- (void)PostChecklistItemsAsyncUsingParameters:(eHousekeepingService_PostChecklistItems *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)PostingLinenItemsUsingParameters:(eHousekeepingService_PostingLinenItems *)aParameters ;
- (void)PostingLinenItemsAsyncUsingParameters:(eHousekeepingService_PostingLinenItems *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)PostAmenitiesItemUsingParameters:(eHousekeepingService_PostAmenitiesItem *)aParameters ;
- (void)PostAmenitiesItemAsyncUsingParameters:(eHousekeepingService_PostAmenitiesItem *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetChecklistCategoryListUsingParameters:(eHousekeepingService_GetChecklistCategoryList *)aParameters ;
- (void)GetChecklistCategoryListAsyncUsingParameters:(eHousekeepingService_GetChecklistCategoryList *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)PostMinibarOrderUsingParameters:(eHousekeepingService_PostMinibarOrder *)aParameters ;
- (void)PostMinibarOrderAsyncUsingParameters:(eHousekeepingService_PostMinibarOrder *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetAllChecklistUsingParameters:(eHousekeepingService_GetAllChecklist *)aParameters ;
- (void)GetAllChecklistAsyncUsingParameters:(eHousekeepingService_GetAllChecklist *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetChecklistItemListUsingParameters:(eHousekeepingService_GetChecklistItemList *)aParameters ;
- (void)GetChecklistItemListAsyncUsingParameters:(eHousekeepingService_GetChecklistItemList *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetChecklistRoomTypeUsingParameters:(eHousekeepingService_GetChecklistRoomType *)aParameters ;
- (void)GetChecklistRoomTypeAsyncUsingParameters:(eHousekeepingService_GetChecklistRoomType *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)PostChecklistItemUsingParameters:(eHousekeepingService_PostChecklistItem *)aParameters ;
- (void)PostChecklistItemAsyncUsingParameters:(eHousekeepingService_PostChecklistItem *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)UpdateInspectionUsingParameters:(eHousekeepingService_UpdateInspection *)aParameters ;
- (void)UpdateInspectionAsyncUsingParameters:(eHousekeepingService_UpdateInspection *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)ChangeRoomAssignmentSequenceUsingParameters:(eHousekeepingService_ChangeRoomAssignmentSequence *)aParameters ;
- (void)ChangeRoomAssignmentSequenceAsyncUsingParameters:(eHousekeepingService_ChangeRoomAssignmentSequence *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)GetMessageAttachPhotoUsingParameters:(eHousekeepingService_GetMessageAttachPhoto *)aParameters ;
- (void)GetMessageAttachPhotoAsyncUsingParameters:(eHousekeepingService_GetMessageAttachPhoto *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoapBindingResponse *)PostMessageAttachPhotoUsingParameters:(eHousekeepingService_PostMessageAttachPhoto *)aParameters ;
- (void)PostMessageAttachPhotoAsyncUsingParameters:(eHousekeepingService_PostMessageAttachPhoto *)aParameters  delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)responseDelegate;
@end
@interface eHousekeepingServiceSoapBindingOperation : NSOperation {
	eHousekeepingServiceSoapBinding *binding;
	eHousekeepingServiceSoapBindingResponse *response;
	id<eHousekeepingServiceSoapBindingResponseDelegate> delegate;
	NSMutableData *responseData;
	NSURLConnection *urlConnection;
	NSMutableURLRequest *requestRetry;//Hao Tran - For trying to request many times
}
@property (nonatomic, retain) eHousekeepingServiceSoapBinding *binding;
@property (nonatomic, /*readonly*/) eHousekeepingServiceSoapBindingResponse *response;//Hao Tran - Remove for retry request many times
@property (nonatomic, assign) id<eHousekeepingServiceSoapBindingResponseDelegate> delegate;
@property (nonatomic, retain) NSMutableData *responseData;
@property (nonatomic, retain) NSURLConnection *urlConnection;
@property (nonatomic, retain) NSMutableURLRequest *requestRetry;//Hao Tran - For trying to request many times
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate;
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error;
@end
@interface eHousekeepingServiceSoapBinding_GetZoneList : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetZoneList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetZoneList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetZoneList *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetUnassignRoomList : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetUnassignRoomList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetUnassignRoomList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetUnassignRoomList *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_FindAttendant : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_FindAttendant * parameters;
}
@property (nonatomic, retain) eHousekeepingService_FindAttendant * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_FindAttendant *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetZoneRoomList : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetZoneRoomList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetZoneRoomList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetZoneRoomList *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetCheckListItemScoreForMaid : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetCheckListItemScoreForMaid * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetCheckListItemScoreForMaid * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetCheckListItemScoreForMaid *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetInspectionStatus : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetInspectionStatus * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetInspectionStatus * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetInspectionStatus *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_PostWSLog : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_PostWSLog * parameters;
}
@property (nonatomic, retain) eHousekeepingService_PostWSLog * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_PostWSLog *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_ValidateRoomNo : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_ValidateRoomNo * parameters;
}
@property (nonatomic, retain) eHousekeepingService_ValidateRoomNo * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_ValidateRoomNo *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetAdditionalJobFloorList : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetAdditionalJobFloorList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetAdditionalJobFloorList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetAdditionalJobFloorList *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetAdditionalJobTask : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetAdditionalJobTask * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetAdditionalJobTask * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetAdditionalJobTask *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetMessageCategoryList2 : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetMessageCategoryList2 * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetMessageCategoryList2 * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetMessageCategoryList2 *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetMessageItemList2 : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetMessageItemList2 * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetMessageItemList2 * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetMessageItemList2 *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetNoOfAdHocMessage : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetNoOfAdHocMessage * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetNoOfAdHocMessage * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetNoOfAdHocMessage *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetSentAdHocMessage : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetSentAdHocMessage * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetSentAdHocMessage * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetSentAdHocMessage *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetReceivedAdHocMessage : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetReceivedAdHocMessage * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetReceivedAdHocMessage * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetReceivedAdHocMessage *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_UpdateAdHocMessageStatus : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_UpdateAdHocMessageStatus * parameters;
}
@property (nonatomic, retain) eHousekeepingService_UpdateAdHocMessageStatus * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_UpdateAdHocMessageStatus *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetAdditionalJobRoomByFloor : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetAdditionalJobRoomByFloor * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetAdditionalJobRoomByFloor * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetAdditionalJobRoomByFloor *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetAdditionalJobDetail : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetAdditionalJobDetail * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetAdditionalJobDetail * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetAdditionalJobDetail *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_CompleteAdditionalJob : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_CompleteAdditionalJob * parameters;
}
@property (nonatomic, retain) eHousekeepingService_CompleteAdditionalJob * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_CompleteAdditionalJob *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetAdditionalJobCount : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetAdditionalJobCount * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetAdditionalJobCount * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetAdditionalJobCount *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_UpdateAdditionalJobStatus : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_UpdateAdditionalJobStatus * parameters;
}
@property (nonatomic, retain) eHousekeepingService_UpdateAdditionalJobStatus * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_UpdateAdditionalJobStatus *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetProfileNote : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetProfileNote * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetProfileNote * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetProfileNote *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_StartAdditionalJob : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_StartAdditionalJob * parameters;
}
@property (nonatomic, retain) eHousekeepingService_StartAdditionalJob * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_StartAdditionalJob *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_StopAdditionalJob : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_StopAdditionalJob * parameters;
}
@property (nonatomic, retain) eHousekeepingService_StopAdditionalJob * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_StopAdditionalJob *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetAdditionalJobOfRoom : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetAdditionalJobOfRoom * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetAdditionalJobOfRoom * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetAdditionalJobOfRoom *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetGuestInfoByRoomID : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetGuestInfoByRoomID * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetGuestInfoByRoomID * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetGuestInfoByRoomID *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetNumberOfGuest : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetNumberOfGuest * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetNumberOfGuest * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetNumberOfGuest *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_UpdateAdditionalJobRemark : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_UpdateAdditionalJobRemark * parameters;
}
@property (nonatomic, retain) eHousekeepingService_UpdateAdditionalJobRemark * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_UpdateAdditionalJobRemark *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetAdditionalJobByRoomNo : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetAdditionalJobByRoomNo * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetAdditionalJobByRoomNo * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetAdditionalJobByRoomNo *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_FindRoomByRoomNo : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_FindRoomByRoomNo * parameters;
}
@property (nonatomic, retain) eHousekeepingService_FindRoomByRoomNo * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_FindRoomByRoomNo *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_PostAmenitiesItemsList : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_PostAmenitiesItemsList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_PostAmenitiesItemsList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_PostAmenitiesItemsList *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetPanicAlertConfig : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetPanicAlertConfig * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetPanicAlertConfig * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetPanicAlertConfig *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_PostingPanicAlert : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_PostingPanicAlert * parameters;
}
@property (nonatomic, retain) eHousekeepingService_PostingPanicAlert * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_PostingPanicAlert *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetGuestInformation : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetGuestInformation * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetGuestInformation * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetGuestInformation *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetVersion : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetVersion * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetVersion * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetVersion *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetRoomRemarks : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetRoomRemarks * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetRoomRemarks * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetRoomRemarks *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_UpdateRoomRemarks : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_UpdateRoomRemarks * parameters;
}
@property (nonatomic, retain) eHousekeepingService_UpdateRoomRemarks * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_UpdateRoomRemarks *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetOOSBlockRoomsList : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetOOSBlockRoomsList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetOOSBlockRoomsList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetOOSBlockRoomsList *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetReleaseRoomsList : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetReleaseRoomsList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetReleaseRoomsList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetReleaseRoomsList *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetFindFloorDetailsList : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetFindFloorDetailsList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetFindFloorDetailsList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetFindFloorDetailsList *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetFindRoomDetailsList : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetFindRoomDetailsList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetFindRoomDetailsList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetFindRoomDetailsList *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetStillLoggedIn : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetStillLoggedIn * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetStillLoggedIn * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetStillLoggedIn *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_PostLogout : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_PostLogout * parameters;
}
@property (nonatomic, retain) eHousekeepingService_PostLogout * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_PostLogout *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_LogInvalidStartRoom : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_LogInvalidStartRoom * parameters;
}
@property (nonatomic, retain) eHousekeepingService_LogInvalidStartRoom * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_LogInvalidStartRoom *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_AddReassignRoom : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_AddReassignRoom * parameters;
}
@property (nonatomic, retain) eHousekeepingService_AddReassignRoom * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_AddReassignRoom *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_AssignRoom : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_AssignRoom * parameters;
}
@property (nonatomic, retain) eHousekeepingService_AssignRoom * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_AssignRoom *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetSupervisorFiltersRoutine : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetSupervisorFiltersRoutine * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetSupervisorFiltersRoutine * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetSupervisorFiltersRoutine *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetPostingHistoryRoutine : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetPostingHistoryRoutine * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetPostingHistoryRoutine * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetPostingHistoryRoutine *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_PostPostingHistoryRoutine : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_PostPostingHistoryRoutine * parameters;
}
@property (nonatomic, retain) eHousekeepingService_PostPostingHistoryRoutine * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_PostPostingHistoryRoutine *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_PostLostAndFoundRoutine : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_PostLostAndFoundRoutine * parameters;
}
@property (nonatomic, retain) eHousekeepingService_PostLostAndFoundRoutine * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_PostLostAndFoundRoutine *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetAdditionalJobStatusesRoutine : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetAdditionalJobStatusesRoutine * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetAdditionalJobStatusesRoutine * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetAdditionalJobStatusesRoutine *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_PostAdditionalJobStatusUpdateRoutine : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_PostAdditionalJobStatusUpdateRoutine * parameters;
}
@property (nonatomic, retain) eHousekeepingService_PostAdditionalJobStatusUpdateRoutine * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_PostAdditionalJobStatusUpdateRoutine *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_FindGuestStayedHistoryRoutine : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_FindGuestStayedHistoryRoutine * parameters;
}
@property (nonatomic, retain) eHousekeepingService_FindGuestStayedHistoryRoutine * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_FindGuestStayedHistoryRoutine *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_PostUnassignRoomRoutine : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_PostUnassignRoomRoutine * parameters;
}
@property (nonatomic, retain) eHousekeepingService_PostUnassignRoomRoutine * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_PostUnassignRoomRoutine *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_PostUpdateRoomDetailsRoutine : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_PostUpdateRoomDetailsRoutine * parameters;
}
@property (nonatomic, retain) eHousekeepingService_PostUpdateRoomDetailsRoutine * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_PostUpdateRoomDetailsRoutine *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetRoomTypeInventoryRoutine : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetRoomTypeInventoryRoutine * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetRoomTypeInventoryRoutine * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetRoomTypeInventoryRoutine *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetRoomTypeByRoomNoRoutine : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetRoomTypeByRoomNoRoutine * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetRoomTypeByRoomNoRoutine * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetRoomTypeByRoomNoRoutine *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_PostUpdateAdHocMessageRoutine : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_PostUpdateAdHocMessageRoutine * parameters;
}
@property (nonatomic, retain) eHousekeepingService_PostUpdateAdHocMessageRoutine * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_PostUpdateAdHocMessageRoutine *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetNoOfAdHocMessageRoutine : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetNoOfAdHocMessageRoutine * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetNoOfAdHocMessageRoutine * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetNoOfAdHocMessageRoutine *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetReceivedAdHocMessageRoutine : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetReceivedAdHocMessageRoutine * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetReceivedAdHocMessageRoutine * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetReceivedAdHocMessageRoutine *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetSendAdHocMessageRoutine : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetSendAdHocMessageRoutine * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetSendAdHocMessageRoutine * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetSendAdHocMessageRoutine *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetOtherActivityAssignmentRoutine : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetOtherActivityAssignmentRoutine * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetOtherActivityAssignmentRoutine * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetOtherActivityAssignmentRoutine *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetOtherActivitiesStatusRoutine : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetOtherActivitiesStatusRoutine * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetOtherActivitiesStatusRoutine * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetOtherActivitiesStatusRoutine *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetOtherActivitiesLocationRoutine : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetOtherActivitiesLocationRoutine * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetOtherActivitiesLocationRoutine * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetOtherActivitiesLocationRoutine *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_PostUpdateOtherActivityStatusRoutine : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_PostUpdateOtherActivityStatusRoutine * parameters;
}
@property (nonatomic, retain) eHousekeepingService_PostUpdateOtherActivityStatusRoutine * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_PostUpdateOtherActivityStatusRoutine *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetLanguageList : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetLanguageList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetLanguageList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetLanguageList *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetRoomStatusList : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetRoomStatusList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetRoomStatusList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetRoomStatusList *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetCleaningStatusList : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetCleaningStatusList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetCleaningStatusList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetCleaningStatusList *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetRoomTypeList : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetRoomTypeList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetRoomTypeList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetRoomTypeList *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetBuildingList : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetBuildingList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetBuildingList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetBuildingList *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetFloorList : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetFloorList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetFloorList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetFloorList *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_AuthenticateUser : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_AuthenticateUser * parameters;
}
@property (nonatomic, retain) eHousekeepingService_AuthenticateUser * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_AuthenticateUser *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetHotelInfo : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetHotelInfo * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetHotelInfo * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetHotelInfo *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetRoomAssignmentList : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetRoomAssignmentList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetRoomAssignmentList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetRoomAssignmentList *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetPrevRoomAssignmentList : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetPrevRoomAssignmentList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetPrevRoomAssignmentList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetPrevRoomAssignmentList *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetFindRoomAssignmentList : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetFindRoomAssignmentList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetFindRoomAssignmentList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetFindRoomAssignmentList *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetFindRoomAssignmentLists : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetFindRoomAssignmentLists * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetFindRoomAssignmentLists * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetFindRoomAssignmentLists *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetSupervisorFindFloorList : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetSupervisorFindFloorList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetSupervisorFindFloorList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetSupervisorFindFloorList *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetSupervisorFindRoomAssignmentLists : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetSupervisorFindRoomAssignmentLists * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetSupervisorFindRoomAssignmentLists * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetSupervisorFindRoomAssignmentLists *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetGuestInfo : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetGuestInfo * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetGuestInfo * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetGuestInfo *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetRoomDetail : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetRoomDetail * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetRoomDetail * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetRoomDetail *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_UpdateGuestInfo : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_UpdateGuestInfo * parameters;
}
@property (nonatomic, retain) eHousekeepingService_UpdateGuestInfo * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_UpdateGuestInfo *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_UpdateRoomAssignment : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_UpdateRoomAssignment * parameters;
}
@property (nonatomic, retain) eHousekeepingService_UpdateRoomAssignment * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_UpdateRoomAssignment *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetDiscrepantRoomStatus : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetDiscrepantRoomStatus * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetDiscrepantRoomStatus * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetDiscrepantRoomStatus *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_UpdateRoomCleaningStatus : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_UpdateRoomCleaningStatus * parameters;
}
@property (nonatomic, retain) eHousekeepingService_UpdateRoomCleaningStatus * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_UpdateRoomCleaningStatus *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_UpdateRoomStatus : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_UpdateRoomStatus * parameters;
}
@property (nonatomic, retain) eHousekeepingService_UpdateRoomStatus * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_UpdateRoomStatus *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_UpdateRoomDetails : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_UpdateRoomDetails * parameters;
}
@property (nonatomic, retain) eHousekeepingService_UpdateRoomDetails * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_UpdateRoomDetails *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_UpdateCleaningStatus : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_UpdateCleaningStatus * parameters;
}
@property (nonatomic, retain) eHousekeepingService_UpdateCleaningStatus * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_UpdateCleaningStatus *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetAccessRights : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetAccessRights * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetAccessRights * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetAccessRights *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetCommonConfigurations : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetCommonConfigurations * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetCommonConfigurations * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetCommonConfigurations *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetLaundryServiceList : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetLaundryServiceList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetLaundryServiceList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetLaundryServiceList *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetLaundryCategoryList : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetLaundryCategoryList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetLaundryCategoryList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetLaundryCategoryList *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetLaundryItemList : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetLaundryItemList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetLaundryItemList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetLaundryItemList *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_RemoveLaundryItem : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_RemoveLaundryItem * parameters;
}
@property (nonatomic, retain) eHousekeepingService_RemoveLaundryItem * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_RemoveLaundryItem *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_PostLaundryItem : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_PostLaundryItem * parameters;
}
@property (nonatomic, retain) eHousekeepingService_PostLaundryItem * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_PostLaundryItem *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetRoomSetGuideByRoomType : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetRoomSetGuideByRoomType * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetRoomSetGuideByRoomType * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetRoomSetGuideByRoomType *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetRoomSetGuideList : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetRoomSetGuideList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetRoomSetGuideList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetRoomSetGuideList *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetMessageTemplateList : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetMessageTemplateList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetMessageTemplateList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetMessageTemplateList *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetNewMessage : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetNewMessage * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetNewMessage * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetNewMessage *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetMessageCategoryList : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetMessageCategoryList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetMessageCategoryList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetMessageCategoryList *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetMessageItemList : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetMessageItemList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetMessageItemList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetMessageItemList *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetPopupMsgList : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetPopupMsgList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetPopupMsgList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetPopupMsgList *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_UpdatePopupMsgStatus : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_UpdatePopupMsgStatus * parameters;
}
@property (nonatomic, retain) eHousekeepingService_UpdatePopupMsgStatus * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_UpdatePopupMsgStatus *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_PostMessage : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_PostMessage * parameters;
}
@property (nonatomic, retain) eHousekeepingService_PostMessage * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_PostMessage *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_PostEngineeringCase : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_PostEngineeringCase * parameters;
}
@property (nonatomic, retain) eHousekeepingService_PostEngineeringCase * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_PostEngineeringCase *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_PosteCnJob : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_PosteCnJob * parameters;
}
@property (nonatomic, retain) eHousekeepingService_PosteCnJob * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_PosteCnJob *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_PosteCnAhMsg : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_PosteCnAhMsg * parameters;
}
@property (nonatomic, retain) eHousekeepingService_PosteCnAhMsg * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_PosteCnAhMsg *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_PostECAttachPhoto : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_PostECAttachPhoto * parameters;
}
@property (nonatomic, retain) eHousekeepingService_PostECAttachPhoto * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_PostECAttachPhoto *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetLFCategoryList : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetLFCategoryList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetLFCategoryList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetLFCategoryList *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetLFItemList : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetLFItemList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetLFItemList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetLFItemList *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetLFColorList : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetLFColorList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetLFColorList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetLFColorList *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_PostLostFound : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_PostLostFound * parameters;
}
@property (nonatomic, retain) eHousekeepingService_PostLostFound * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_PostLostFound *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_PostLFAttachPhoto : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_PostLFAttachPhoto * parameters;
}
@property (nonatomic, retain) eHousekeepingService_PostLFAttachPhoto * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_PostLFAttachPhoto *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetLocationList : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetLocationList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetLocationList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetLocationList *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetUserList : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetUserList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetUserList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetUserList *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetRoomSectionList : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetRoomSectionList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetRoomSectionList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetRoomSectionList *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetEngineeringItem : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetEngineeringItem * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetEngineeringItem * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetEngineeringItem *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetEngineeringCategoryList : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetEngineeringCategoryList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetEngineeringCategoryList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetEngineeringCategoryList *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetLaundrySpecialInstructionList : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetLaundrySpecialInstructionList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetLaundrySpecialInstructionList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetLaundrySpecialInstructionList *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetLaundryItemPriceList : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetLaundryItemPriceList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetLaundryItemPriceList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetLaundryItemPriceList *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetMinibarCategoryList : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetMinibarCategoryList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetMinibarCategoryList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetMinibarCategoryList *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetLinenCategoryList : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetLinenCategoryList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetLinenCategoryList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetLinenCategoryList *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetAmenitiesCategoryList : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetAmenitiesCategoryList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetAmenitiesCategoryList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetAmenitiesCategoryList *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetMinibarItemList : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetMinibarItemList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetMinibarItemList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetMinibarItemList *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetLinenItemList : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetLinenItemList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetLinenItemList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetLinenItemList *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetAmenitiesItemList : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetAmenitiesItemList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetAmenitiesItemList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetAmenitiesItemList *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetAmenitiesLocationList : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetAmenitiesLocationList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetAmenitiesLocationList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetAmenitiesLocationList *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_PostLaundryOrder : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_PostLaundryOrder * parameters;
}
@property (nonatomic, retain) eHousekeepingService_PostLaundryOrder * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_PostLaundryOrder *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_PostLaundrySpecialInstruction : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_PostLaundrySpecialInstruction * parameters;
}
@property (nonatomic, retain) eHousekeepingService_PostLaundrySpecialInstruction * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_PostLaundrySpecialInstruction *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_PostMinibarItem : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_PostMinibarItem * parameters;
}
@property (nonatomic, retain) eHousekeepingService_PostMinibarItem * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_PostMinibarItem *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_PostLinenItem : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_PostLinenItem * parameters;
}
@property (nonatomic, retain) eHousekeepingService_PostLinenItem * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_PostLinenItem *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_PostMinibarItems : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_PostMinibarItems * parameters;
}
@property (nonatomic, retain) eHousekeepingService_PostMinibarItems * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_PostMinibarItems *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_PostLinenItems : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_PostLinenItems * parameters;
}
@property (nonatomic, retain) eHousekeepingService_PostLinenItems * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_PostLinenItems *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_PostChecklistItems : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_PostChecklistItems * parameters;
}
@property (nonatomic, retain) eHousekeepingService_PostChecklistItems * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_PostChecklistItems *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_PostingLinenItems : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_PostingLinenItems * parameters;
}
@property (nonatomic, retain) eHousekeepingService_PostingLinenItems * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_PostingLinenItems *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_PostAmenitiesItem : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_PostAmenitiesItem * parameters;
}
@property (nonatomic, retain) eHousekeepingService_PostAmenitiesItem * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_PostAmenitiesItem *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetChecklistCategoryList : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetChecklistCategoryList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetChecklistCategoryList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetChecklistCategoryList *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_PostMinibarOrder : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_PostMinibarOrder * parameters;
}
@property (nonatomic, retain) eHousekeepingService_PostMinibarOrder * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_PostMinibarOrder *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetAllChecklist : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetAllChecklist * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetAllChecklist * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetAllChecklist *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetChecklistItemList : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetChecklistItemList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetChecklistItemList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetChecklistItemList *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetChecklistRoomType : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetChecklistRoomType * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetChecklistRoomType * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetChecklistRoomType *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_PostChecklistItem : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_PostChecklistItem * parameters;
}
@property (nonatomic, retain) eHousekeepingService_PostChecklistItem * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_PostChecklistItem *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_UpdateInspection : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_UpdateInspection * parameters;
}
@property (nonatomic, retain) eHousekeepingService_UpdateInspection * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_UpdateInspection *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_ChangeRoomAssignmentSequence : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_ChangeRoomAssignmentSequence * parameters;
}
@property (nonatomic, retain) eHousekeepingService_ChangeRoomAssignmentSequence * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_ChangeRoomAssignmentSequence *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_GetMessageAttachPhoto : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_GetMessageAttachPhoto * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetMessageAttachPhoto * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetMessageAttachPhoto *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_PostMessageAttachPhoto : eHousekeepingServiceSoapBindingOperation {
	eHousekeepingService_PostMessageAttachPhoto * parameters;
}
@property (nonatomic, retain) eHousekeepingService_PostMessageAttachPhoto * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoapBinding *)aBinding delegate:(id<eHousekeepingServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_PostMessageAttachPhoto *)aParameters
;
@end
@interface eHousekeepingServiceSoapBinding_envelope : NSObject {
}
+ (eHousekeepingServiceSoapBinding_envelope *)sharedInstance;
- (NSString *)serializedFormUsingHeaderElements:(NSDictionary *)headerElements bodyElements:(NSDictionary *)bodyElements bodyKeys:(NSArray *)bodyKeys;
@end
@interface eHousekeepingServiceSoapBindingResponse : NSObject {
	NSArray *headers;
	NSArray *bodyParts;
	NSError *error;
}
@property (nonatomic, retain) NSArray *headers;
@property (nonatomic, retain) NSArray *bodyParts;
@property (nonatomic, retain) NSError *error;
@end
@class eHousekeepingServiceSoap12BindingResponse;
@class eHousekeepingServiceSoap12BindingOperation;
@protocol eHousekeepingServiceSoap12BindingResponseDelegate <NSObject>
- (void) operation:(eHousekeepingServiceSoap12BindingOperation *)operation completedWithResponse:(eHousekeepingServiceSoap12BindingResponse *)response;
@end
#define kServerAnchorCertificates   @"kServerAnchorCertificates"
#define kServerAnchorsOnly          @"kServerAnchorsOnly"
#define kClientIdentity             @"kClientIdentity"
#define kClientCertificates         @"kClientCertificates"
#define kClientUsername             @"kClientUsername"
#define kClientPassword             @"kClientPassword"
#define kNSURLCredentialPersistence @"kNSURLCredentialPersistence"
#define kValidateResult             @"kValidateResult"
@interface eHousekeepingServiceSoap12Binding : NSObject <eHousekeepingServiceSoap12BindingResponseDelegate> {
	NSURL *address;
	NSTimeInterval timeout;
	NSMutableArray *cookies;
	NSMutableDictionary *customHeaders;
	BOOL logXMLInOut;
	BOOL ignoreEmptyResponse;
	BOOL synchronousOperationComplete;
	id<SSLCredentialsManaging> sslManager;
	SOAPSigner *soapSigner;
}
@property (nonatomic, copy) NSURL *address;
@property (nonatomic) BOOL logXMLInOut;
@property (nonatomic) BOOL ignoreEmptyResponse;
@property (nonatomic) NSTimeInterval timeout;
@property (nonatomic, retain) NSMutableArray *cookies;
@property (nonatomic, retain) NSMutableDictionary *customHeaders;
@property (nonatomic, retain) id<SSLCredentialsManaging> sslManager;
@property (nonatomic, retain) SOAPSigner *soapSigner;
+ (NSTimeInterval) defaultTimeout;
- (id)initWithAddress:(NSString *)anAddress;
- (void)sendHTTPCallUsingBody:(NSString *)body soapAction:(NSString *)soapAction forOperation:(eHousekeepingServiceSoap12BindingOperation *)operation;
//Hao Tran - For Retry request many times
- (void) retryWith:(eHousekeepingServiceSoap12BindingOperation *)operation;
- (void)addCookie:(NSHTTPCookie *)toAdd;
- (NSString *)MIMEType;
- (eHousekeepingServiceSoap12BindingResponse *)GetZoneListUsingParameters:(eHousekeepingService_GetZoneList *)aParameters ;
- (void)GetZoneListAsyncUsingParameters:(eHousekeepingService_GetZoneList *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetUnassignRoomListUsingParameters:(eHousekeepingService_GetUnassignRoomList *)aParameters ;
- (void)GetUnassignRoomListAsyncUsingParameters:(eHousekeepingService_GetUnassignRoomList *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)FindAttendantUsingParameters:(eHousekeepingService_FindAttendant *)aParameters ;
- (void)FindAttendantAsyncUsingParameters:(eHousekeepingService_FindAttendant *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetZoneRoomListUsingParameters:(eHousekeepingService_GetZoneRoomList *)aParameters ;
- (void)GetZoneRoomListAsyncUsingParameters:(eHousekeepingService_GetZoneRoomList *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetCheckListItemScoreForMaidUsingParameters:(eHousekeepingService_GetCheckListItemScoreForMaid *)aParameters ;
- (void)GetCheckListItemScoreForMaidAsyncUsingParameters:(eHousekeepingService_GetCheckListItemScoreForMaid *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetInspectionStatusUsingParameters:(eHousekeepingService_GetInspectionStatus *)aParameters ;
- (void)GetInspectionStatusAsyncUsingParameters:(eHousekeepingService_GetInspectionStatus *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)PostWSLogUsingParameters:(eHousekeepingService_PostWSLog *)aParameters ;
- (void)PostWSLogAsyncUsingParameters:(eHousekeepingService_PostWSLog *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)ValidateRoomNoUsingParameters:(eHousekeepingService_ValidateRoomNo *)aParameters ;
- (void)ValidateRoomNoAsyncUsingParameters:(eHousekeepingService_ValidateRoomNo *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetAdditionalJobFloorListUsingParameters:(eHousekeepingService_GetAdditionalJobFloorList *)aParameters ;
- (void)GetAdditionalJobFloorListAsyncUsingParameters:(eHousekeepingService_GetAdditionalJobFloorList *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetAdditionalJobTaskUsingParameters:(eHousekeepingService_GetAdditionalJobTask *)aParameters ;
- (void)GetAdditionalJobTaskAsyncUsingParameters:(eHousekeepingService_GetAdditionalJobTask *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetMessageCategoryList2UsingParameters:(eHousekeepingService_GetMessageCategoryList2 *)aParameters ;
- (void)GetMessageCategoryList2AsyncUsingParameters:(eHousekeepingService_GetMessageCategoryList2 *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetMessageItemList2UsingParameters:(eHousekeepingService_GetMessageItemList2 *)aParameters ;
- (void)GetMessageItemList2AsyncUsingParameters:(eHousekeepingService_GetMessageItemList2 *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetNoOfAdHocMessageUsingParameters:(eHousekeepingService_GetNoOfAdHocMessage *)aParameters ;
- (void)GetNoOfAdHocMessageAsyncUsingParameters:(eHousekeepingService_GetNoOfAdHocMessage *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetSentAdHocMessageUsingParameters:(eHousekeepingService_GetSentAdHocMessage *)aParameters ;
- (void)GetSentAdHocMessageAsyncUsingParameters:(eHousekeepingService_GetSentAdHocMessage *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetReceivedAdHocMessageUsingParameters:(eHousekeepingService_GetReceivedAdHocMessage *)aParameters ;
- (void)GetReceivedAdHocMessageAsyncUsingParameters:(eHousekeepingService_GetReceivedAdHocMessage *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)UpdateAdHocMessageStatusUsingParameters:(eHousekeepingService_UpdateAdHocMessageStatus *)aParameters ;
- (void)UpdateAdHocMessageStatusAsyncUsingParameters:(eHousekeepingService_UpdateAdHocMessageStatus *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetAdditionalJobRoomByFloorUsingParameters:(eHousekeepingService_GetAdditionalJobRoomByFloor *)aParameters ;
- (void)GetAdditionalJobRoomByFloorAsyncUsingParameters:(eHousekeepingService_GetAdditionalJobRoomByFloor *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetAdditionalJobDetailUsingParameters:(eHousekeepingService_GetAdditionalJobDetail *)aParameters ;
- (void)GetAdditionalJobDetailAsyncUsingParameters:(eHousekeepingService_GetAdditionalJobDetail *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)CompleteAdditionalJobUsingParameters:(eHousekeepingService_CompleteAdditionalJob *)aParameters ;
- (void)CompleteAdditionalJobAsyncUsingParameters:(eHousekeepingService_CompleteAdditionalJob *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetAdditionalJobCountUsingParameters:(eHousekeepingService_GetAdditionalJobCount *)aParameters ;
- (void)GetAdditionalJobCountAsyncUsingParameters:(eHousekeepingService_GetAdditionalJobCount *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)UpdateAdditionalJobStatusUsingParameters:(eHousekeepingService_UpdateAdditionalJobStatus *)aParameters ;
- (void)UpdateAdditionalJobStatusAsyncUsingParameters:(eHousekeepingService_UpdateAdditionalJobStatus *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetProfileNoteUsingParameters:(eHousekeepingService_GetProfileNote *)aParameters ;
- (void)GetProfileNoteAsyncUsingParameters:(eHousekeepingService_GetProfileNote *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)StartAdditionalJobUsingParameters:(eHousekeepingService_StartAdditionalJob *)aParameters ;
- (void)StartAdditionalJobAsyncUsingParameters:(eHousekeepingService_StartAdditionalJob *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)StopAdditionalJobUsingParameters:(eHousekeepingService_StopAdditionalJob *)aParameters ;
- (void)StopAdditionalJobAsyncUsingParameters:(eHousekeepingService_StopAdditionalJob *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetAdditionalJobOfRoomUsingParameters:(eHousekeepingService_GetAdditionalJobOfRoom *)aParameters ;
- (void)GetAdditionalJobOfRoomAsyncUsingParameters:(eHousekeepingService_GetAdditionalJobOfRoom *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetGuestInfoByRoomIDUsingParameters:(eHousekeepingService_GetGuestInfoByRoomID *)aParameters ;
- (void)GetGuestInfoByRoomIDAsyncUsingParameters:(eHousekeepingService_GetGuestInfoByRoomID *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetNumberOfGuestUsingParameters:(eHousekeepingService_GetNumberOfGuest *)aParameters ;
- (void)GetNumberOfGuestAsyncUsingParameters:(eHousekeepingService_GetNumberOfGuest *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)UpdateAdditionalJobRemarkUsingParameters:(eHousekeepingService_UpdateAdditionalJobRemark *)aParameters ;
- (void)UpdateAdditionalJobRemarkAsyncUsingParameters:(eHousekeepingService_UpdateAdditionalJobRemark *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetAdditionalJobByRoomNoUsingParameters:(eHousekeepingService_GetAdditionalJobByRoomNo *)aParameters ;
- (void)GetAdditionalJobByRoomNoAsyncUsingParameters:(eHousekeepingService_GetAdditionalJobByRoomNo *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)FindRoomByRoomNoUsingParameters:(eHousekeepingService_FindRoomByRoomNo *)aParameters ;
- (void)FindRoomByRoomNoAsyncUsingParameters:(eHousekeepingService_FindRoomByRoomNo *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)PostAmenitiesItemsListUsingParameters:(eHousekeepingService_PostAmenitiesItemsList *)aParameters ;
- (void)PostAmenitiesItemsListAsyncUsingParameters:(eHousekeepingService_PostAmenitiesItemsList *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetPanicAlertConfigUsingParameters:(eHousekeepingService_GetPanicAlertConfig *)aParameters ;
- (void)GetPanicAlertConfigAsyncUsingParameters:(eHousekeepingService_GetPanicAlertConfig *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)PostingPanicAlertUsingParameters:(eHousekeepingService_PostingPanicAlert *)aParameters ;
- (void)PostingPanicAlertAsyncUsingParameters:(eHousekeepingService_PostingPanicAlert *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetGuestInformationUsingParameters:(eHousekeepingService_GetGuestInformation *)aParameters ;
- (void)GetGuestInformationAsyncUsingParameters:(eHousekeepingService_GetGuestInformation *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetVersionUsingParameters:(eHousekeepingService_GetVersion *)aParameters ;
- (void)GetVersionAsyncUsingParameters:(eHousekeepingService_GetVersion *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetRoomRemarksUsingParameters:(eHousekeepingService_GetRoomRemarks *)aParameters ;
- (void)GetRoomRemarksAsyncUsingParameters:(eHousekeepingService_GetRoomRemarks *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)UpdateRoomRemarksUsingParameters:(eHousekeepingService_UpdateRoomRemarks *)aParameters ;
- (void)UpdateRoomRemarksAsyncUsingParameters:(eHousekeepingService_UpdateRoomRemarks *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetOOSBlockRoomsListUsingParameters:(eHousekeepingService_GetOOSBlockRoomsList *)aParameters ;
- (void)GetOOSBlockRoomsListAsyncUsingParameters:(eHousekeepingService_GetOOSBlockRoomsList *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetReleaseRoomsListUsingParameters:(eHousekeepingService_GetReleaseRoomsList *)aParameters ;
- (void)GetReleaseRoomsListAsyncUsingParameters:(eHousekeepingService_GetReleaseRoomsList *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetFindFloorDetailsListUsingParameters:(eHousekeepingService_GetFindFloorDetailsList *)aParameters ;
- (void)GetFindFloorDetailsListAsyncUsingParameters:(eHousekeepingService_GetFindFloorDetailsList *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetFindRoomDetailsListUsingParameters:(eHousekeepingService_GetFindRoomDetailsList *)aParameters ;
- (void)GetFindRoomDetailsListAsyncUsingParameters:(eHousekeepingService_GetFindRoomDetailsList *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetStillLoggedInUsingParameters:(eHousekeepingService_GetStillLoggedIn *)aParameters ;
- (void)GetStillLoggedInAsyncUsingParameters:(eHousekeepingService_GetStillLoggedIn *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)PostLogoutUsingParameters:(eHousekeepingService_PostLogout *)aParameters ;
- (void)PostLogoutAsyncUsingParameters:(eHousekeepingService_PostLogout *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)LogInvalidStartRoomUsingParameters:(eHousekeepingService_LogInvalidStartRoom *)aParameters ;
- (void)LogInvalidStartRoomAsyncUsingParameters:(eHousekeepingService_LogInvalidStartRoom *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)AddReassignRoomUsingParameters:(eHousekeepingService_AddReassignRoom *)aParameters ;
- (void)AddReassignRoomAsyncUsingParameters:(eHousekeepingService_AddReassignRoom *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)AssignRoomUsingParameters:(eHousekeepingService_AssignRoom *)aParameters ;
- (void)AssignRoomAsyncUsingParameters:(eHousekeepingService_AssignRoom *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetSupervisorFiltersRoutineUsingParameters:(eHousekeepingService_GetSupervisorFiltersRoutine *)aParameters ;
- (void)GetSupervisorFiltersRoutineAsyncUsingParameters:(eHousekeepingService_GetSupervisorFiltersRoutine *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetPostingHistoryRoutineUsingParameters:(eHousekeepingService_GetPostingHistoryRoutine *)aParameters ;
- (void)GetPostingHistoryRoutineAsyncUsingParameters:(eHousekeepingService_GetPostingHistoryRoutine *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)PostPostingHistoryRoutineUsingParameters:(eHousekeepingService_PostPostingHistoryRoutine *)aParameters ;
- (void)PostPostingHistoryRoutineAsyncUsingParameters:(eHousekeepingService_PostPostingHistoryRoutine *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)PostLostAndFoundRoutineUsingParameters:(eHousekeepingService_PostLostAndFoundRoutine *)aParameters ;
- (void)PostLostAndFoundRoutineAsyncUsingParameters:(eHousekeepingService_PostLostAndFoundRoutine *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetAdditionalJobStatusesRoutineUsingParameters:(eHousekeepingService_GetAdditionalJobStatusesRoutine *)aParameters ;
- (void)GetAdditionalJobStatusesRoutineAsyncUsingParameters:(eHousekeepingService_GetAdditionalJobStatusesRoutine *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)PostAdditionalJobStatusUpdateRoutineUsingParameters:(eHousekeepingService_PostAdditionalJobStatusUpdateRoutine *)aParameters ;
- (void)PostAdditionalJobStatusUpdateRoutineAsyncUsingParameters:(eHousekeepingService_PostAdditionalJobStatusUpdateRoutine *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)FindGuestStayedHistoryRoutineUsingParameters:(eHousekeepingService_FindGuestStayedHistoryRoutine *)aParameters ;
- (void)FindGuestStayedHistoryRoutineAsyncUsingParameters:(eHousekeepingService_FindGuestStayedHistoryRoutine *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)PostUnassignRoomRoutineUsingParameters:(eHousekeepingService_PostUnassignRoomRoutine *)aParameters ;
- (void)PostUnassignRoomRoutineAsyncUsingParameters:(eHousekeepingService_PostUnassignRoomRoutine *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)PostUpdateRoomDetailsRoutineUsingParameters:(eHousekeepingService_PostUpdateRoomDetailsRoutine *)aParameters ;
- (void)PostUpdateRoomDetailsRoutineAsyncUsingParameters:(eHousekeepingService_PostUpdateRoomDetailsRoutine *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetRoomTypeInventoryRoutineUsingParameters:(eHousekeepingService_GetRoomTypeInventoryRoutine *)aParameters ;
- (void)GetRoomTypeInventoryRoutineAsyncUsingParameters:(eHousekeepingService_GetRoomTypeInventoryRoutine *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetRoomTypeByRoomNoRoutineUsingParameters:(eHousekeepingService_GetRoomTypeByRoomNoRoutine *)aParameters ;
- (void)GetRoomTypeByRoomNoRoutineAsyncUsingParameters:(eHousekeepingService_GetRoomTypeByRoomNoRoutine *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)PostUpdateAdHocMessageRoutineUsingParameters:(eHousekeepingService_PostUpdateAdHocMessageRoutine *)aParameters ;
- (void)PostUpdateAdHocMessageRoutineAsyncUsingParameters:(eHousekeepingService_PostUpdateAdHocMessageRoutine *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetNoOfAdHocMessageRoutineUsingParameters:(eHousekeepingService_GetNoOfAdHocMessageRoutine *)aParameters ;
- (void)GetNoOfAdHocMessageRoutineAsyncUsingParameters:(eHousekeepingService_GetNoOfAdHocMessageRoutine *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetReceivedAdHocMessageRoutineUsingParameters:(eHousekeepingService_GetReceivedAdHocMessageRoutine *)aParameters ;
- (void)GetReceivedAdHocMessageRoutineAsyncUsingParameters:(eHousekeepingService_GetReceivedAdHocMessageRoutine *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetSendAdHocMessageRoutineUsingParameters:(eHousekeepingService_GetSendAdHocMessageRoutine *)aParameters ;
- (void)GetSendAdHocMessageRoutineAsyncUsingParameters:(eHousekeepingService_GetSendAdHocMessageRoutine *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetOtherActivityAssignmentRoutineUsingParameters:(eHousekeepingService_GetOtherActivityAssignmentRoutine *)aParameters ;
- (void)GetOtherActivityAssignmentRoutineAsyncUsingParameters:(eHousekeepingService_GetOtherActivityAssignmentRoutine *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetOtherActivitiesStatusRoutineUsingParameters:(eHousekeepingService_GetOtherActivitiesStatusRoutine *)aParameters ;
- (void)GetOtherActivitiesStatusRoutineAsyncUsingParameters:(eHousekeepingService_GetOtherActivitiesStatusRoutine *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetOtherActivitiesLocationRoutineUsingParameters:(eHousekeepingService_GetOtherActivitiesLocationRoutine *)aParameters ;
- (void)GetOtherActivitiesLocationRoutineAsyncUsingParameters:(eHousekeepingService_GetOtherActivitiesLocationRoutine *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)PostUpdateOtherActivityStatusRoutineUsingParameters:(eHousekeepingService_PostUpdateOtherActivityStatusRoutine *)aParameters ;
- (void)PostUpdateOtherActivityStatusRoutineAsyncUsingParameters:(eHousekeepingService_PostUpdateOtherActivityStatusRoutine *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetLanguageListUsingParameters:(eHousekeepingService_GetLanguageList *)aParameters ;
- (void)GetLanguageListAsyncUsingParameters:(eHousekeepingService_GetLanguageList *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetRoomStatusListUsingParameters:(eHousekeepingService_GetRoomStatusList *)aParameters ;
- (void)GetRoomStatusListAsyncUsingParameters:(eHousekeepingService_GetRoomStatusList *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetCleaningStatusListUsingParameters:(eHousekeepingService_GetCleaningStatusList *)aParameters ;
- (void)GetCleaningStatusListAsyncUsingParameters:(eHousekeepingService_GetCleaningStatusList *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetRoomTypeListUsingParameters:(eHousekeepingService_GetRoomTypeList *)aParameters ;
- (void)GetRoomTypeListAsyncUsingParameters:(eHousekeepingService_GetRoomTypeList *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetBuildingListUsingParameters:(eHousekeepingService_GetBuildingList *)aParameters ;
- (void)GetBuildingListAsyncUsingParameters:(eHousekeepingService_GetBuildingList *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetFloorListUsingParameters:(eHousekeepingService_GetFloorList *)aParameters ;
- (void)GetFloorListAsyncUsingParameters:(eHousekeepingService_GetFloorList *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)AuthenticateUserUsingParameters:(eHousekeepingService_AuthenticateUser *)aParameters ;
- (void)AuthenticateUserAsyncUsingParameters:(eHousekeepingService_AuthenticateUser *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetHotelInfoUsingParameters:(eHousekeepingService_GetHotelInfo *)aParameters ;
- (void)GetHotelInfoAsyncUsingParameters:(eHousekeepingService_GetHotelInfo *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetRoomAssignmentListUsingParameters:(eHousekeepingService_GetRoomAssignmentList *)aParameters ;
- (void)GetRoomAssignmentListAsyncUsingParameters:(eHousekeepingService_GetRoomAssignmentList *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetPrevRoomAssignmentListUsingParameters:(eHousekeepingService_GetPrevRoomAssignmentList *)aParameters ;
- (void)GetPrevRoomAssignmentListAsyncUsingParameters:(eHousekeepingService_GetPrevRoomAssignmentList *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetFindRoomAssignmentListUsingParameters:(eHousekeepingService_GetFindRoomAssignmentList *)aParameters ;
- (void)GetFindRoomAssignmentListAsyncUsingParameters:(eHousekeepingService_GetFindRoomAssignmentList *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetFindRoomAssignmentListsUsingParameters:(eHousekeepingService_GetFindRoomAssignmentLists *)aParameters ;
- (void)GetFindRoomAssignmentListsAsyncUsingParameters:(eHousekeepingService_GetFindRoomAssignmentLists *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetSupervisorFindFloorListUsingParameters:(eHousekeepingService_GetSupervisorFindFloorList *)aParameters ;
- (void)GetSupervisorFindFloorListAsyncUsingParameters:(eHousekeepingService_GetSupervisorFindFloorList *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetSupervisorFindRoomAssignmentListsUsingParameters:(eHousekeepingService_GetSupervisorFindRoomAssignmentLists *)aParameters ;
- (void)GetSupervisorFindRoomAssignmentListsAsyncUsingParameters:(eHousekeepingService_GetSupervisorFindRoomAssignmentLists *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetGuestInfoUsingParameters:(eHousekeepingService_GetGuestInfo *)aParameters ;
- (void)GetGuestInfoAsyncUsingParameters:(eHousekeepingService_GetGuestInfo *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetRoomDetailUsingParameters:(eHousekeepingService_GetRoomDetail *)aParameters ;
- (void)GetRoomDetailAsyncUsingParameters:(eHousekeepingService_GetRoomDetail *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)UpdateGuestInfoUsingParameters:(eHousekeepingService_UpdateGuestInfo *)aParameters ;
- (void)UpdateGuestInfoAsyncUsingParameters:(eHousekeepingService_UpdateGuestInfo *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)UpdateRoomAssignmentUsingParameters:(eHousekeepingService_UpdateRoomAssignment *)aParameters ;
- (void)UpdateRoomAssignmentAsyncUsingParameters:(eHousekeepingService_UpdateRoomAssignment *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetDiscrepantRoomStatusUsingParameters:(eHousekeepingService_GetDiscrepantRoomStatus *)aParameters ;
- (void)GetDiscrepantRoomStatusAsyncUsingParameters:(eHousekeepingService_GetDiscrepantRoomStatus *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)UpdateRoomCleaningStatusUsingParameters:(eHousekeepingService_UpdateRoomCleaningStatus *)aParameters ;
- (void)UpdateRoomCleaningStatusAsyncUsingParameters:(eHousekeepingService_UpdateRoomCleaningStatus *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)UpdateRoomStatusUsingParameters:(eHousekeepingService_UpdateRoomStatus *)aParameters ;
- (void)UpdateRoomStatusAsyncUsingParameters:(eHousekeepingService_UpdateRoomStatus *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)UpdateRoomDetailsUsingParameters:(eHousekeepingService_UpdateRoomDetails *)aParameters ;
- (void)UpdateRoomDetailsAsyncUsingParameters:(eHousekeepingService_UpdateRoomDetails *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)UpdateCleaningStatusUsingParameters:(eHousekeepingService_UpdateCleaningStatus *)aParameters ;
- (void)UpdateCleaningStatusAsyncUsingParameters:(eHousekeepingService_UpdateCleaningStatus *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetAccessRightsUsingParameters:(eHousekeepingService_GetAccessRights *)aParameters ;
- (void)GetAccessRightsAsyncUsingParameters:(eHousekeepingService_GetAccessRights *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetCommonConfigurationsUsingParameters:(eHousekeepingService_GetCommonConfigurations *)aParameters ;
- (void)GetCommonConfigurationsAsyncUsingParameters:(eHousekeepingService_GetCommonConfigurations *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetLaundryServiceListUsingParameters:(eHousekeepingService_GetLaundryServiceList *)aParameters ;
- (void)GetLaundryServiceListAsyncUsingParameters:(eHousekeepingService_GetLaundryServiceList *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetLaundryCategoryListUsingParameters:(eHousekeepingService_GetLaundryCategoryList *)aParameters ;
- (void)GetLaundryCategoryListAsyncUsingParameters:(eHousekeepingService_GetLaundryCategoryList *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetLaundryItemListUsingParameters:(eHousekeepingService_GetLaundryItemList *)aParameters ;
- (void)GetLaundryItemListAsyncUsingParameters:(eHousekeepingService_GetLaundryItemList *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)RemoveLaundryItemUsingParameters:(eHousekeepingService_RemoveLaundryItem *)aParameters ;
- (void)RemoveLaundryItemAsyncUsingParameters:(eHousekeepingService_RemoveLaundryItem *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)PostLaundryItemUsingParameters:(eHousekeepingService_PostLaundryItem *)aParameters ;
- (void)PostLaundryItemAsyncUsingParameters:(eHousekeepingService_PostLaundryItem *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetRoomSetGuideByRoomTypeUsingParameters:(eHousekeepingService_GetRoomSetGuideByRoomType *)aParameters ;
- (void)GetRoomSetGuideByRoomTypeAsyncUsingParameters:(eHousekeepingService_GetRoomSetGuideByRoomType *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetRoomSetGuideListUsingParameters:(eHousekeepingService_GetRoomSetGuideList *)aParameters ;
- (void)GetRoomSetGuideListAsyncUsingParameters:(eHousekeepingService_GetRoomSetGuideList *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetMessageTemplateListUsingParameters:(eHousekeepingService_GetMessageTemplateList *)aParameters ;
- (void)GetMessageTemplateListAsyncUsingParameters:(eHousekeepingService_GetMessageTemplateList *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetNewMessageUsingParameters:(eHousekeepingService_GetNewMessage *)aParameters ;
- (void)GetNewMessageAsyncUsingParameters:(eHousekeepingService_GetNewMessage *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetMessageCategoryListUsingParameters:(eHousekeepingService_GetMessageCategoryList *)aParameters ;
- (void)GetMessageCategoryListAsyncUsingParameters:(eHousekeepingService_GetMessageCategoryList *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetMessageItemListUsingParameters:(eHousekeepingService_GetMessageItemList *)aParameters ;
- (void)GetMessageItemListAsyncUsingParameters:(eHousekeepingService_GetMessageItemList *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetPopupMsgListUsingParameters:(eHousekeepingService_GetPopupMsgList *)aParameters ;
- (void)GetPopupMsgListAsyncUsingParameters:(eHousekeepingService_GetPopupMsgList *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)UpdatePopupMsgStatusUsingParameters:(eHousekeepingService_UpdatePopupMsgStatus *)aParameters ;
- (void)UpdatePopupMsgStatusAsyncUsingParameters:(eHousekeepingService_UpdatePopupMsgStatus *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)PostMessageUsingParameters:(eHousekeepingService_PostMessage *)aParameters ;
- (void)PostMessageAsyncUsingParameters:(eHousekeepingService_PostMessage *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)PostEngineeringCaseUsingParameters:(eHousekeepingService_PostEngineeringCase *)aParameters ;
- (void)PostEngineeringCaseAsyncUsingParameters:(eHousekeepingService_PostEngineeringCase *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)PosteCnJobUsingParameters:(eHousekeepingService_PosteCnJob *)aParameters ;
- (void)PosteCnJobAsyncUsingParameters:(eHousekeepingService_PosteCnJob *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)PosteCnAhMsgUsingParameters:(eHousekeepingService_PosteCnAhMsg *)aParameters ;
- (void)PosteCnAhMsgAsyncUsingParameters:(eHousekeepingService_PosteCnAhMsg *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)PostECAttachPhotoUsingParameters:(eHousekeepingService_PostECAttachPhoto *)aParameters ;
- (void)PostECAttachPhotoAsyncUsingParameters:(eHousekeepingService_PostECAttachPhoto *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetLFCategoryListUsingParameters:(eHousekeepingService_GetLFCategoryList *)aParameters ;
- (void)GetLFCategoryListAsyncUsingParameters:(eHousekeepingService_GetLFCategoryList *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetLFItemListUsingParameters:(eHousekeepingService_GetLFItemList *)aParameters ;
- (void)GetLFItemListAsyncUsingParameters:(eHousekeepingService_GetLFItemList *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetLFColorListUsingParameters:(eHousekeepingService_GetLFColorList *)aParameters ;
- (void)GetLFColorListAsyncUsingParameters:(eHousekeepingService_GetLFColorList *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)PostLostFoundUsingParameters:(eHousekeepingService_PostLostFound *)aParameters ;
- (void)PostLostFoundAsyncUsingParameters:(eHousekeepingService_PostLostFound *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)PostLFAttachPhotoUsingParameters:(eHousekeepingService_PostLFAttachPhoto *)aParameters ;
- (void)PostLFAttachPhotoAsyncUsingParameters:(eHousekeepingService_PostLFAttachPhoto *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetLocationListUsingParameters:(eHousekeepingService_GetLocationList *)aParameters ;
- (void)GetLocationListAsyncUsingParameters:(eHousekeepingService_GetLocationList *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetUserListUsingParameters:(eHousekeepingService_GetUserList *)aParameters ;
- (void)GetUserListAsyncUsingParameters:(eHousekeepingService_GetUserList *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetRoomSectionListUsingParameters:(eHousekeepingService_GetRoomSectionList *)aParameters ;
- (void)GetRoomSectionListAsyncUsingParameters:(eHousekeepingService_GetRoomSectionList *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetEngineeringItemUsingParameters:(eHousekeepingService_GetEngineeringItem *)aParameters ;
- (void)GetEngineeringItemAsyncUsingParameters:(eHousekeepingService_GetEngineeringItem *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetEngineeringCategoryListUsingParameters:(eHousekeepingService_GetEngineeringCategoryList *)aParameters ;
- (void)GetEngineeringCategoryListAsyncUsingParameters:(eHousekeepingService_GetEngineeringCategoryList *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetLaundrySpecialInstructionListUsingParameters:(eHousekeepingService_GetLaundrySpecialInstructionList *)aParameters ;
- (void)GetLaundrySpecialInstructionListAsyncUsingParameters:(eHousekeepingService_GetLaundrySpecialInstructionList *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetLaundryItemPriceListUsingParameters:(eHousekeepingService_GetLaundryItemPriceList *)aParameters ;
- (void)GetLaundryItemPriceListAsyncUsingParameters:(eHousekeepingService_GetLaundryItemPriceList *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetMinibarCategoryListUsingParameters:(eHousekeepingService_GetMinibarCategoryList *)aParameters ;
- (void)GetMinibarCategoryListAsyncUsingParameters:(eHousekeepingService_GetMinibarCategoryList *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetLinenCategoryListUsingParameters:(eHousekeepingService_GetLinenCategoryList *)aParameters ;
- (void)GetLinenCategoryListAsyncUsingParameters:(eHousekeepingService_GetLinenCategoryList *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetAmenitiesCategoryListUsingParameters:(eHousekeepingService_GetAmenitiesCategoryList *)aParameters ;
- (void)GetAmenitiesCategoryListAsyncUsingParameters:(eHousekeepingService_GetAmenitiesCategoryList *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetMinibarItemListUsingParameters:(eHousekeepingService_GetMinibarItemList *)aParameters ;
- (void)GetMinibarItemListAsyncUsingParameters:(eHousekeepingService_GetMinibarItemList *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetLinenItemListUsingParameters:(eHousekeepingService_GetLinenItemList *)aParameters ;
- (void)GetLinenItemListAsyncUsingParameters:(eHousekeepingService_GetLinenItemList *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetAmenitiesItemListUsingParameters:(eHousekeepingService_GetAmenitiesItemList *)aParameters ;
- (void)GetAmenitiesItemListAsyncUsingParameters:(eHousekeepingService_GetAmenitiesItemList *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetAmenitiesLocationListUsingParameters:(eHousekeepingService_GetAmenitiesLocationList *)aParameters ;
- (void)GetAmenitiesLocationListAsyncUsingParameters:(eHousekeepingService_GetAmenitiesLocationList *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)PostLaundryOrderUsingParameters:(eHousekeepingService_PostLaundryOrder *)aParameters ;
- (void)PostLaundryOrderAsyncUsingParameters:(eHousekeepingService_PostLaundryOrder *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)PostLaundrySpecialInstructionUsingParameters:(eHousekeepingService_PostLaundrySpecialInstruction *)aParameters ;
- (void)PostLaundrySpecialInstructionAsyncUsingParameters:(eHousekeepingService_PostLaundrySpecialInstruction *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)PostMinibarItemUsingParameters:(eHousekeepingService_PostMinibarItem *)aParameters ;
- (void)PostMinibarItemAsyncUsingParameters:(eHousekeepingService_PostMinibarItem *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)PostLinenItemUsingParameters:(eHousekeepingService_PostLinenItem *)aParameters ;
- (void)PostLinenItemAsyncUsingParameters:(eHousekeepingService_PostLinenItem *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)PostMinibarItemsUsingParameters:(eHousekeepingService_PostMinibarItems *)aParameters ;
- (void)PostMinibarItemsAsyncUsingParameters:(eHousekeepingService_PostMinibarItems *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)PostLinenItemsUsingParameters:(eHousekeepingService_PostLinenItems *)aParameters ;
- (void)PostLinenItemsAsyncUsingParameters:(eHousekeepingService_PostLinenItems *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)PostChecklistItemsUsingParameters:(eHousekeepingService_PostChecklistItems *)aParameters ;
- (void)PostChecklistItemsAsyncUsingParameters:(eHousekeepingService_PostChecklistItems *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)PostingLinenItemsUsingParameters:(eHousekeepingService_PostingLinenItems *)aParameters ;
- (void)PostingLinenItemsAsyncUsingParameters:(eHousekeepingService_PostingLinenItems *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)PostAmenitiesItemUsingParameters:(eHousekeepingService_PostAmenitiesItem *)aParameters ;
- (void)PostAmenitiesItemAsyncUsingParameters:(eHousekeepingService_PostAmenitiesItem *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetChecklistCategoryListUsingParameters:(eHousekeepingService_GetChecklistCategoryList *)aParameters ;
- (void)GetChecklistCategoryListAsyncUsingParameters:(eHousekeepingService_GetChecklistCategoryList *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)PostMinibarOrderUsingParameters:(eHousekeepingService_PostMinibarOrder *)aParameters ;
- (void)PostMinibarOrderAsyncUsingParameters:(eHousekeepingService_PostMinibarOrder *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetAllChecklistUsingParameters:(eHousekeepingService_GetAllChecklist *)aParameters ;
- (void)GetAllChecklistAsyncUsingParameters:(eHousekeepingService_GetAllChecklist *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetChecklistItemListUsingParameters:(eHousekeepingService_GetChecklistItemList *)aParameters ;
- (void)GetChecklistItemListAsyncUsingParameters:(eHousekeepingService_GetChecklistItemList *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetChecklistRoomTypeUsingParameters:(eHousekeepingService_GetChecklistRoomType *)aParameters ;
- (void)GetChecklistRoomTypeAsyncUsingParameters:(eHousekeepingService_GetChecklistRoomType *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)PostChecklistItemUsingParameters:(eHousekeepingService_PostChecklistItem *)aParameters ;
- (void)PostChecklistItemAsyncUsingParameters:(eHousekeepingService_PostChecklistItem *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)UpdateInspectionUsingParameters:(eHousekeepingService_UpdateInspection *)aParameters ;
- (void)UpdateInspectionAsyncUsingParameters:(eHousekeepingService_UpdateInspection *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)ChangeRoomAssignmentSequenceUsingParameters:(eHousekeepingService_ChangeRoomAssignmentSequence *)aParameters ;
- (void)ChangeRoomAssignmentSequenceAsyncUsingParameters:(eHousekeepingService_ChangeRoomAssignmentSequence *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)GetMessageAttachPhotoUsingParameters:(eHousekeepingService_GetMessageAttachPhoto *)aParameters ;
- (void)GetMessageAttachPhotoAsyncUsingParameters:(eHousekeepingService_GetMessageAttachPhoto *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
- (eHousekeepingServiceSoap12BindingResponse *)PostMessageAttachPhotoUsingParameters:(eHousekeepingService_PostMessageAttachPhoto *)aParameters ;
- (void)PostMessageAttachPhotoAsyncUsingParameters:(eHousekeepingService_PostMessageAttachPhoto *)aParameters  delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)responseDelegate;
@end
@interface eHousekeepingServiceSoap12BindingOperation : NSOperation {
	eHousekeepingServiceSoap12Binding *binding;
	eHousekeepingServiceSoap12BindingResponse *response;
	id<eHousekeepingServiceSoap12BindingResponseDelegate> delegate;
	NSMutableData *responseData;
	NSURLConnection *urlConnection;
	NSMutableURLRequest *requestRetry;//Hao Tran - For trying to request many times
}
@property (nonatomic, retain) eHousekeepingServiceSoap12Binding *binding;
@property (nonatomic, /*readonly*/) eHousekeepingServiceSoap12BindingResponse *response;//Hao Tran - Remove for retry request many times
@property (nonatomic, assign) id<eHousekeepingServiceSoap12BindingResponseDelegate> delegate;
@property (nonatomic, retain) NSMutableData *responseData;
@property (nonatomic, retain) NSURLConnection *urlConnection;
@property (nonatomic, retain) NSMutableURLRequest *requestRetry;//Hao Tran - For trying to request many times
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate;
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error;
@end
@interface eHousekeepingServiceSoap12Binding_GetZoneList : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetZoneList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetZoneList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetZoneList *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetUnassignRoomList : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetUnassignRoomList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetUnassignRoomList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetUnassignRoomList *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_FindAttendant : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_FindAttendant * parameters;
}
@property (nonatomic, retain) eHousekeepingService_FindAttendant * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_FindAttendant *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetZoneRoomList : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetZoneRoomList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetZoneRoomList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetZoneRoomList *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetCheckListItemScoreForMaid : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetCheckListItemScoreForMaid * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetCheckListItemScoreForMaid * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetCheckListItemScoreForMaid *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetInspectionStatus : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetInspectionStatus * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetInspectionStatus * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetInspectionStatus *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_PostWSLog : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_PostWSLog * parameters;
}
@property (nonatomic, retain) eHousekeepingService_PostWSLog * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_PostWSLog *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_ValidateRoomNo : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_ValidateRoomNo * parameters;
}
@property (nonatomic, retain) eHousekeepingService_ValidateRoomNo * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_ValidateRoomNo *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetAdditionalJobFloorList : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetAdditionalJobFloorList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetAdditionalJobFloorList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetAdditionalJobFloorList *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetAdditionalJobTask : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetAdditionalJobTask * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetAdditionalJobTask * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetAdditionalJobTask *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetMessageCategoryList2 : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetMessageCategoryList2 * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetMessageCategoryList2 * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetMessageCategoryList2 *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetMessageItemList2 : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetMessageItemList2 * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetMessageItemList2 * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetMessageItemList2 *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetNoOfAdHocMessage : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetNoOfAdHocMessage * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetNoOfAdHocMessage * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetNoOfAdHocMessage *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetSentAdHocMessage : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetSentAdHocMessage * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetSentAdHocMessage * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetSentAdHocMessage *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetReceivedAdHocMessage : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetReceivedAdHocMessage * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetReceivedAdHocMessage * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetReceivedAdHocMessage *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_UpdateAdHocMessageStatus : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_UpdateAdHocMessageStatus * parameters;
}
@property (nonatomic, retain) eHousekeepingService_UpdateAdHocMessageStatus * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_UpdateAdHocMessageStatus *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetAdditionalJobRoomByFloor : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetAdditionalJobRoomByFloor * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetAdditionalJobRoomByFloor * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetAdditionalJobRoomByFloor *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetAdditionalJobDetail : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetAdditionalJobDetail * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetAdditionalJobDetail * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetAdditionalJobDetail *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_CompleteAdditionalJob : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_CompleteAdditionalJob * parameters;
}
@property (nonatomic, retain) eHousekeepingService_CompleteAdditionalJob * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_CompleteAdditionalJob *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetAdditionalJobCount : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetAdditionalJobCount * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetAdditionalJobCount * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetAdditionalJobCount *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_UpdateAdditionalJobStatus : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_UpdateAdditionalJobStatus * parameters;
}
@property (nonatomic, retain) eHousekeepingService_UpdateAdditionalJobStatus * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_UpdateAdditionalJobStatus *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetProfileNote : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetProfileNote * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetProfileNote * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetProfileNote *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_StartAdditionalJob : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_StartAdditionalJob * parameters;
}
@property (nonatomic, retain) eHousekeepingService_StartAdditionalJob * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_StartAdditionalJob *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_StopAdditionalJob : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_StopAdditionalJob * parameters;
}
@property (nonatomic, retain) eHousekeepingService_StopAdditionalJob * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_StopAdditionalJob *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetAdditionalJobOfRoom : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetAdditionalJobOfRoom * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetAdditionalJobOfRoom * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetAdditionalJobOfRoom *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetGuestInfoByRoomID : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetGuestInfoByRoomID * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetGuestInfoByRoomID * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetGuestInfoByRoomID *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetNumberOfGuest : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetNumberOfGuest * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetNumberOfGuest * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetNumberOfGuest *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_UpdateAdditionalJobRemark : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_UpdateAdditionalJobRemark * parameters;
}
@property (nonatomic, retain) eHousekeepingService_UpdateAdditionalJobRemark * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_UpdateAdditionalJobRemark *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetAdditionalJobByRoomNo : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetAdditionalJobByRoomNo * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetAdditionalJobByRoomNo * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetAdditionalJobByRoomNo *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_FindRoomByRoomNo : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_FindRoomByRoomNo * parameters;
}
@property (nonatomic, retain) eHousekeepingService_FindRoomByRoomNo * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_FindRoomByRoomNo *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_PostAmenitiesItemsList : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_PostAmenitiesItemsList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_PostAmenitiesItemsList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_PostAmenitiesItemsList *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetPanicAlertConfig : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetPanicAlertConfig * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetPanicAlertConfig * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetPanicAlertConfig *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_PostingPanicAlert : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_PostingPanicAlert * parameters;
}
@property (nonatomic, retain) eHousekeepingService_PostingPanicAlert * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_PostingPanicAlert *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetGuestInformation : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetGuestInformation * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetGuestInformation * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetGuestInformation *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetVersion : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetVersion * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetVersion * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetVersion *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetRoomRemarks : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetRoomRemarks * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetRoomRemarks * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetRoomRemarks *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_UpdateRoomRemarks : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_UpdateRoomRemarks * parameters;
}
@property (nonatomic, retain) eHousekeepingService_UpdateRoomRemarks * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_UpdateRoomRemarks *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetOOSBlockRoomsList : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetOOSBlockRoomsList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetOOSBlockRoomsList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetOOSBlockRoomsList *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetReleaseRoomsList : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetReleaseRoomsList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetReleaseRoomsList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetReleaseRoomsList *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetFindFloorDetailsList : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetFindFloorDetailsList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetFindFloorDetailsList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetFindFloorDetailsList *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetFindRoomDetailsList : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetFindRoomDetailsList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetFindRoomDetailsList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetFindRoomDetailsList *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetStillLoggedIn : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetStillLoggedIn * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetStillLoggedIn * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetStillLoggedIn *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_PostLogout : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_PostLogout * parameters;
}
@property (nonatomic, retain) eHousekeepingService_PostLogout * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_PostLogout *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_LogInvalidStartRoom : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_LogInvalidStartRoom * parameters;
}
@property (nonatomic, retain) eHousekeepingService_LogInvalidStartRoom * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_LogInvalidStartRoom *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_AddReassignRoom : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_AddReassignRoom * parameters;
}
@property (nonatomic, retain) eHousekeepingService_AddReassignRoom * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_AddReassignRoom *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_AssignRoom : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_AssignRoom * parameters;
}
@property (nonatomic, retain) eHousekeepingService_AssignRoom * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_AssignRoom *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetSupervisorFiltersRoutine : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetSupervisorFiltersRoutine * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetSupervisorFiltersRoutine * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetSupervisorFiltersRoutine *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetPostingHistoryRoutine : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetPostingHistoryRoutine * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetPostingHistoryRoutine * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetPostingHistoryRoutine *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_PostPostingHistoryRoutine : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_PostPostingHistoryRoutine * parameters;
}
@property (nonatomic, retain) eHousekeepingService_PostPostingHistoryRoutine * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_PostPostingHistoryRoutine *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_PostLostAndFoundRoutine : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_PostLostAndFoundRoutine * parameters;
}
@property (nonatomic, retain) eHousekeepingService_PostLostAndFoundRoutine * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_PostLostAndFoundRoutine *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetAdditionalJobStatusesRoutine : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetAdditionalJobStatusesRoutine * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetAdditionalJobStatusesRoutine * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetAdditionalJobStatusesRoutine *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_PostAdditionalJobStatusUpdateRoutine : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_PostAdditionalJobStatusUpdateRoutine * parameters;
}
@property (nonatomic, retain) eHousekeepingService_PostAdditionalJobStatusUpdateRoutine * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_PostAdditionalJobStatusUpdateRoutine *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_FindGuestStayedHistoryRoutine : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_FindGuestStayedHistoryRoutine * parameters;
}
@property (nonatomic, retain) eHousekeepingService_FindGuestStayedHistoryRoutine * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_FindGuestStayedHistoryRoutine *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_PostUnassignRoomRoutine : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_PostUnassignRoomRoutine * parameters;
}
@property (nonatomic, retain) eHousekeepingService_PostUnassignRoomRoutine * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_PostUnassignRoomRoutine *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_PostUpdateRoomDetailsRoutine : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_PostUpdateRoomDetailsRoutine * parameters;
}
@property (nonatomic, retain) eHousekeepingService_PostUpdateRoomDetailsRoutine * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_PostUpdateRoomDetailsRoutine *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetRoomTypeInventoryRoutine : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetRoomTypeInventoryRoutine * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetRoomTypeInventoryRoutine * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetRoomTypeInventoryRoutine *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetRoomTypeByRoomNoRoutine : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetRoomTypeByRoomNoRoutine * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetRoomTypeByRoomNoRoutine * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetRoomTypeByRoomNoRoutine *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_PostUpdateAdHocMessageRoutine : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_PostUpdateAdHocMessageRoutine * parameters;
}
@property (nonatomic, retain) eHousekeepingService_PostUpdateAdHocMessageRoutine * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_PostUpdateAdHocMessageRoutine *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetNoOfAdHocMessageRoutine : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetNoOfAdHocMessageRoutine * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetNoOfAdHocMessageRoutine * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetNoOfAdHocMessageRoutine *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetReceivedAdHocMessageRoutine : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetReceivedAdHocMessageRoutine * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetReceivedAdHocMessageRoutine * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetReceivedAdHocMessageRoutine *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetSendAdHocMessageRoutine : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetSendAdHocMessageRoutine * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetSendAdHocMessageRoutine * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetSendAdHocMessageRoutine *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetOtherActivityAssignmentRoutine : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetOtherActivityAssignmentRoutine * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetOtherActivityAssignmentRoutine * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetOtherActivityAssignmentRoutine *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetOtherActivitiesStatusRoutine : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetOtherActivitiesStatusRoutine * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetOtherActivitiesStatusRoutine * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetOtherActivitiesStatusRoutine *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetOtherActivitiesLocationRoutine : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetOtherActivitiesLocationRoutine * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetOtherActivitiesLocationRoutine * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetOtherActivitiesLocationRoutine *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_PostUpdateOtherActivityStatusRoutine : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_PostUpdateOtherActivityStatusRoutine * parameters;
}
@property (nonatomic, retain) eHousekeepingService_PostUpdateOtherActivityStatusRoutine * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_PostUpdateOtherActivityStatusRoutine *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetLanguageList : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetLanguageList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetLanguageList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetLanguageList *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetRoomStatusList : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetRoomStatusList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetRoomStatusList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetRoomStatusList *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetCleaningStatusList : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetCleaningStatusList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetCleaningStatusList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetCleaningStatusList *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetRoomTypeList : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetRoomTypeList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetRoomTypeList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetRoomTypeList *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetBuildingList : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetBuildingList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetBuildingList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetBuildingList *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetFloorList : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetFloorList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetFloorList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetFloorList *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_AuthenticateUser : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_AuthenticateUser * parameters;
}
@property (nonatomic, retain) eHousekeepingService_AuthenticateUser * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_AuthenticateUser *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetHotelInfo : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetHotelInfo * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetHotelInfo * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetHotelInfo *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetRoomAssignmentList : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetRoomAssignmentList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetRoomAssignmentList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetRoomAssignmentList *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetPrevRoomAssignmentList : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetPrevRoomAssignmentList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetPrevRoomAssignmentList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetPrevRoomAssignmentList *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetFindRoomAssignmentList : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetFindRoomAssignmentList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetFindRoomAssignmentList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetFindRoomAssignmentList *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetFindRoomAssignmentLists : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetFindRoomAssignmentLists * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetFindRoomAssignmentLists * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetFindRoomAssignmentLists *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetSupervisorFindFloorList : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetSupervisorFindFloorList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetSupervisorFindFloorList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetSupervisorFindFloorList *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetSupervisorFindRoomAssignmentLists : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetSupervisorFindRoomAssignmentLists * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetSupervisorFindRoomAssignmentLists * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetSupervisorFindRoomAssignmentLists *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetGuestInfo : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetGuestInfo * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetGuestInfo * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetGuestInfo *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetRoomDetail : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetRoomDetail * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetRoomDetail * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetRoomDetail *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_UpdateGuestInfo : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_UpdateGuestInfo * parameters;
}
@property (nonatomic, retain) eHousekeepingService_UpdateGuestInfo * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_UpdateGuestInfo *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_UpdateRoomAssignment : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_UpdateRoomAssignment * parameters;
}
@property (nonatomic, retain) eHousekeepingService_UpdateRoomAssignment * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_UpdateRoomAssignment *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetDiscrepantRoomStatus : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetDiscrepantRoomStatus * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetDiscrepantRoomStatus * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetDiscrepantRoomStatus *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_UpdateRoomCleaningStatus : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_UpdateRoomCleaningStatus * parameters;
}
@property (nonatomic, retain) eHousekeepingService_UpdateRoomCleaningStatus * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_UpdateRoomCleaningStatus *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_UpdateRoomStatus : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_UpdateRoomStatus * parameters;
}
@property (nonatomic, retain) eHousekeepingService_UpdateRoomStatus * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_UpdateRoomStatus *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_UpdateRoomDetails : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_UpdateRoomDetails * parameters;
}
@property (nonatomic, retain) eHousekeepingService_UpdateRoomDetails * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_UpdateRoomDetails *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_UpdateCleaningStatus : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_UpdateCleaningStatus * parameters;
}
@property (nonatomic, retain) eHousekeepingService_UpdateCleaningStatus * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_UpdateCleaningStatus *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetAccessRights : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetAccessRights * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetAccessRights * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetAccessRights *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetCommonConfigurations : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetCommonConfigurations * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetCommonConfigurations * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetCommonConfigurations *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetLaundryServiceList : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetLaundryServiceList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetLaundryServiceList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetLaundryServiceList *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetLaundryCategoryList : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetLaundryCategoryList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetLaundryCategoryList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetLaundryCategoryList *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetLaundryItemList : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetLaundryItemList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetLaundryItemList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetLaundryItemList *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_RemoveLaundryItem : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_RemoveLaundryItem * parameters;
}
@property (nonatomic, retain) eHousekeepingService_RemoveLaundryItem * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_RemoveLaundryItem *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_PostLaundryItem : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_PostLaundryItem * parameters;
}
@property (nonatomic, retain) eHousekeepingService_PostLaundryItem * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_PostLaundryItem *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetRoomSetGuideByRoomType : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetRoomSetGuideByRoomType * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetRoomSetGuideByRoomType * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetRoomSetGuideByRoomType *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetRoomSetGuideList : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetRoomSetGuideList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetRoomSetGuideList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetRoomSetGuideList *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetMessageTemplateList : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetMessageTemplateList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetMessageTemplateList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetMessageTemplateList *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetNewMessage : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetNewMessage * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetNewMessage * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetNewMessage *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetMessageCategoryList : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetMessageCategoryList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetMessageCategoryList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetMessageCategoryList *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetMessageItemList : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetMessageItemList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetMessageItemList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetMessageItemList *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetPopupMsgList : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetPopupMsgList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetPopupMsgList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetPopupMsgList *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_UpdatePopupMsgStatus : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_UpdatePopupMsgStatus * parameters;
}
@property (nonatomic, retain) eHousekeepingService_UpdatePopupMsgStatus * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_UpdatePopupMsgStatus *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_PostMessage : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_PostMessage * parameters;
}
@property (nonatomic, retain) eHousekeepingService_PostMessage * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_PostMessage *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_PostEngineeringCase : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_PostEngineeringCase * parameters;
}
@property (nonatomic, retain) eHousekeepingService_PostEngineeringCase * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_PostEngineeringCase *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_PosteCnJob : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_PosteCnJob * parameters;
}
@property (nonatomic, retain) eHousekeepingService_PosteCnJob * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_PosteCnJob *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_PosteCnAhMsg : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_PosteCnAhMsg * parameters;
}
@property (nonatomic, retain) eHousekeepingService_PosteCnAhMsg * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_PosteCnAhMsg *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_PostECAttachPhoto : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_PostECAttachPhoto * parameters;
}
@property (nonatomic, retain) eHousekeepingService_PostECAttachPhoto * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_PostECAttachPhoto *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetLFCategoryList : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetLFCategoryList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetLFCategoryList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetLFCategoryList *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetLFItemList : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetLFItemList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetLFItemList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetLFItemList *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetLFColorList : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetLFColorList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetLFColorList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetLFColorList *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_PostLostFound : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_PostLostFound * parameters;
}
@property (nonatomic, retain) eHousekeepingService_PostLostFound * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_PostLostFound *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_PostLFAttachPhoto : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_PostLFAttachPhoto * parameters;
}
@property (nonatomic, retain) eHousekeepingService_PostLFAttachPhoto * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_PostLFAttachPhoto *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetLocationList : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetLocationList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetLocationList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetLocationList *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetUserList : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetUserList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetUserList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetUserList *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetRoomSectionList : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetRoomSectionList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetRoomSectionList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetRoomSectionList *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetEngineeringItem : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetEngineeringItem * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetEngineeringItem * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetEngineeringItem *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetEngineeringCategoryList : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetEngineeringCategoryList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetEngineeringCategoryList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetEngineeringCategoryList *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetLaundrySpecialInstructionList : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetLaundrySpecialInstructionList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetLaundrySpecialInstructionList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetLaundrySpecialInstructionList *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetLaundryItemPriceList : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetLaundryItemPriceList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetLaundryItemPriceList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetLaundryItemPriceList *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetMinibarCategoryList : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetMinibarCategoryList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetMinibarCategoryList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetMinibarCategoryList *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetLinenCategoryList : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetLinenCategoryList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetLinenCategoryList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetLinenCategoryList *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetAmenitiesCategoryList : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetAmenitiesCategoryList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetAmenitiesCategoryList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetAmenitiesCategoryList *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetMinibarItemList : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetMinibarItemList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetMinibarItemList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetMinibarItemList *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetLinenItemList : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetLinenItemList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetLinenItemList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetLinenItemList *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetAmenitiesItemList : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetAmenitiesItemList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetAmenitiesItemList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetAmenitiesItemList *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetAmenitiesLocationList : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetAmenitiesLocationList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetAmenitiesLocationList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetAmenitiesLocationList *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_PostLaundryOrder : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_PostLaundryOrder * parameters;
}
@property (nonatomic, retain) eHousekeepingService_PostLaundryOrder * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_PostLaundryOrder *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_PostLaundrySpecialInstruction : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_PostLaundrySpecialInstruction * parameters;
}
@property (nonatomic, retain) eHousekeepingService_PostLaundrySpecialInstruction * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_PostLaundrySpecialInstruction *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_PostMinibarItem : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_PostMinibarItem * parameters;
}
@property (nonatomic, retain) eHousekeepingService_PostMinibarItem * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_PostMinibarItem *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_PostLinenItem : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_PostLinenItem * parameters;
}
@property (nonatomic, retain) eHousekeepingService_PostLinenItem * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_PostLinenItem *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_PostMinibarItems : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_PostMinibarItems * parameters;
}
@property (nonatomic, retain) eHousekeepingService_PostMinibarItems * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_PostMinibarItems *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_PostLinenItems : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_PostLinenItems * parameters;
}
@property (nonatomic, retain) eHousekeepingService_PostLinenItems * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_PostLinenItems *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_PostChecklistItems : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_PostChecklistItems * parameters;
}
@property (nonatomic, retain) eHousekeepingService_PostChecklistItems * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_PostChecklistItems *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_PostingLinenItems : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_PostingLinenItems * parameters;
}
@property (nonatomic, retain) eHousekeepingService_PostingLinenItems * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_PostingLinenItems *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_PostAmenitiesItem : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_PostAmenitiesItem * parameters;
}
@property (nonatomic, retain) eHousekeepingService_PostAmenitiesItem * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_PostAmenitiesItem *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetChecklistCategoryList : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetChecklistCategoryList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetChecklistCategoryList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetChecklistCategoryList *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_PostMinibarOrder : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_PostMinibarOrder * parameters;
}
@property (nonatomic, retain) eHousekeepingService_PostMinibarOrder * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_PostMinibarOrder *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetAllChecklist : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetAllChecklist * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetAllChecklist * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetAllChecklist *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetChecklistItemList : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetChecklistItemList * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetChecklistItemList * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetChecklistItemList *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetChecklistRoomType : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetChecklistRoomType * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetChecklistRoomType * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetChecklistRoomType *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_PostChecklistItem : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_PostChecklistItem * parameters;
}
@property (nonatomic, retain) eHousekeepingService_PostChecklistItem * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_PostChecklistItem *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_UpdateInspection : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_UpdateInspection * parameters;
}
@property (nonatomic, retain) eHousekeepingService_UpdateInspection * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_UpdateInspection *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_ChangeRoomAssignmentSequence : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_ChangeRoomAssignmentSequence * parameters;
}
@property (nonatomic, retain) eHousekeepingService_ChangeRoomAssignmentSequence * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_ChangeRoomAssignmentSequence *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_GetMessageAttachPhoto : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_GetMessageAttachPhoto * parameters;
}
@property (nonatomic, retain) eHousekeepingService_GetMessageAttachPhoto * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_GetMessageAttachPhoto *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_PostMessageAttachPhoto : eHousekeepingServiceSoap12BindingOperation {
	eHousekeepingService_PostMessageAttachPhoto * parameters;
}
@property (nonatomic, retain) eHousekeepingService_PostMessageAttachPhoto * parameters;
- (id)initWithBinding:(eHousekeepingServiceSoap12Binding *)aBinding delegate:(id<eHousekeepingServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(eHousekeepingService_PostMessageAttachPhoto *)aParameters
;
@end
@interface eHousekeepingServiceSoap12Binding_envelope : NSObject {
}
+ (eHousekeepingServiceSoap12Binding_envelope *)sharedInstance;
- (NSString *)serializedFormUsingHeaderElements:(NSDictionary *)headerElements bodyElements:(NSDictionary *)bodyElements bodyKeys:(NSArray *)bodyKeys;
@end
@interface eHousekeepingServiceSoap12BindingResponse : NSObject {
	NSArray *headers;
	NSArray *bodyParts;
	NSError *error;
}
@property (nonatomic, retain) NSArray *headers;
@property (nonatomic, retain) NSArray *bodyParts;
@property (nonatomic, retain) NSError *error;
@end
