#import <Foundation/Foundation.h>
#import <libxml/tree.h>
#import <objc/runtime.h>

#import "USAdditions.h"
#import "USGlobals.h"

@class Web_x0020_Service_AccessRightsList;
@class Web_x0020_Service_AdHocMessageCounter;
@class Web_x0020_Service_AdHocMessageList;
@class Web_x0020_Service_AdHocMessageList2;
@class Web_x0020_Service_AdHocMsgList;
@class Web_x0020_Service_AdHocMsgList2;
@class Web_x0020_Service_AdHocMsgPhoto;
@class Web_x0020_Service_AdHocPhoto;
@class Web_x0020_Service_AdditionalJob;
@class Web_x0020_Service_AdditionalJobCount;
@class Web_x0020_Service_AdditionalJobDetail;
@class Web_x0020_Service_AdditionalJobFloor;
@class Web_x0020_Service_AdditionalJobFloorList;
@class Web_x0020_Service_AdditionalJobOfRoom;
@class Web_x0020_Service_AdditionalJobRoom;
@class Web_x0020_Service_AdditionalJobRoomDetail;
@class Web_x0020_Service_AdditionalJobRoomList;
@class Web_x0020_Service_AdditionalJobStatusDetails;
@class Web_x0020_Service_AmenitiesCatList;
@class Web_x0020_Service_AmenitiesCategoryList;
@class Web_x0020_Service_AmenitiesItem;
@class Web_x0020_Service_AmenitiesItemList;
@class Web_x0020_Service_AmenitiesItmList;
@class Web_x0020_Service_AmenitiesLocList;
@class Web_x0020_Service_AmenitiesLocationList;
@class Web_x0020_Service_ArrayOfAdHocMessageList;
@class Web_x0020_Service_ArrayOfAdHocMessageList2;
@class Web_x0020_Service_ArrayOfAdHocPhoto;
@class Web_x0020_Service_ArrayOfAdditionalJob;
@class Web_x0020_Service_ArrayOfAdditionalJobFloor;
@class Web_x0020_Service_ArrayOfAdditionalJobRoom;
@class Web_x0020_Service_ArrayOfAdditionalJobStatusDetails;
@class Web_x0020_Service_ArrayOfAmenitiesCategoryList;
@class Web_x0020_Service_ArrayOfAmenitiesItem;
@class Web_x0020_Service_ArrayOfAmenitiesItemList;
@class Web_x0020_Service_ArrayOfAmenitiesLocationList;
@class Web_x0020_Service_ArrayOfAttendantList;
@class Web_x0020_Service_ArrayOfBase64Binary;
@class Web_x0020_Service_ArrayOfBlockRoomDetails;
@class Web_x0020_Service_ArrayOfBuilding;
@class Web_x0020_Service_ArrayOfCheckListCategory;
@class Web_x0020_Service_ArrayOfCheckListItem;
@class Web_x0020_Service_ArrayOfCheckListRoomType;
@class Web_x0020_Service_ArrayOfCheckListScoreForMaid;
@class Web_x0020_Service_ArrayOfCleaningStatus;
@class Web_x0020_Service_ArrayOfEngineeringCategoryList;
@class Web_x0020_Service_ArrayOfEngineeringItemList;
@class Web_x0020_Service_ArrayOfFindGuestInformation;
@class Web_x0020_Service_ArrayOfFloor;
@class Web_x0020_Service_ArrayOfFloorDetails;
@class Web_x0020_Service_ArrayOfGroupAdHocMessage;
@class Web_x0020_Service_ArrayOfGuestInformation;
@class Web_x0020_Service_ArrayOfGuestProfileNoteDetail;
@class Web_x0020_Service_ArrayOfImageAttachmentDetails;
@class Web_x0020_Service_ArrayOfInspectionStatusList;
@class Web_x0020_Service_ArrayOfInt;
@class Web_x0020_Service_ArrayOfJobTask;
@class Web_x0020_Service_ArrayOfLanguageItem;
@class Web_x0020_Service_ArrayOfLaundryInstructionList;
@class Web_x0020_Service_ArrayOfLaundryItemList;
@class Web_x0020_Service_ArrayOfLaundryItmPriceList;
@class Web_x0020_Service_ArrayOfLaundryList;
@class Web_x0020_Service_ArrayOfLaundryTypeList;
@class Web_x0020_Service_ArrayOfLinenCategoryList;
@class Web_x0020_Service_ArrayOfLinenItemList;
@class Web_x0020_Service_ArrayOfLocationList;
@class Web_x0020_Service_ArrayOfLostAndFoundCategory;
@class Web_x0020_Service_ArrayOfLostAndFoundColorList;
@class Web_x0020_Service_ArrayOfLostAndFoundItemDetails;
@class Web_x0020_Service_ArrayOfLostAndFoundItemTypeList;
@class Web_x0020_Service_ArrayOfMessageCategoryList;
@class Web_x0020_Service_ArrayOfMessageCategoryList2;
@class Web_x0020_Service_ArrayOfMessageItemList;
@class Web_x0020_Service_ArrayOfMessageItemList2;
@class Web_x0020_Service_ArrayOfMessageTemplateList;
@class Web_x0020_Service_ArrayOfMessageUpdateDetails;
@class Web_x0020_Service_ArrayOfMinibarCategoryList;
@class Web_x0020_Service_ArrayOfMinibarItemList;
@class Web_x0020_Service_ArrayOfOtherActivityDetails;
@class Web_x0020_Service_ArrayOfOtherActivityLocation;
@class Web_x0020_Service_ArrayOfOtherActivityStatus;
@class Web_x0020_Service_ArrayOfPopupMsgItem;
@class Web_x0020_Service_ArrayOfPostChecklistItem;
@class Web_x0020_Service_ArrayOfPostItem;
@class Web_x0020_Service_ArrayOfPostedItem;
@class Web_x0020_Service_ArrayOfPostingHistoryDetails;
@class Web_x0020_Service_ArrayOfProfileNote;
@class Web_x0020_Service_ArrayOfProfileNoteType;
@class Web_x0020_Service_ArrayOfReceiver;
@class Web_x0020_Service_ArrayOfReleaseRoomDetails;
@class Web_x0020_Service_ArrayOfRights;
@class Web_x0020_Service_ArrayOfRoomAssign;
@class Web_x0020_Service_ArrayOfRoomAssignmentDetails;
@class Web_x0020_Service_ArrayOfRoomDetailsRequestDetails;
@class Web_x0020_Service_ArrayOfRoomDetailsResponseDetails;
@class Web_x0020_Service_ArrayOfRoomSectionList;
@class Web_x0020_Service_ArrayOfRoomSetupList;
@class Web_x0020_Service_ArrayOfRoomStatus;
@class Web_x0020_Service_ArrayOfRoomType;
@class Web_x0020_Service_ArrayOfRoomTypeInventoryDetails;
@class Web_x0020_Service_ArrayOfSupervisorCheckList;
@class Web_x0020_Service_ArrayOfSupervisorFiltersDetails;
@class Web_x0020_Service_ArrayOfUnassignRoomList;
@class Web_x0020_Service_ArrayOfUserDetail;
@class Web_x0020_Service_ArrayOfUserDetailsReponseDetails;
@class Web_x0020_Service_ArrayOfWSSettings;
@class Web_x0020_Service_ArrayOfZoneList;
@class Web_x0020_Service_ArrayOfZoneRoomList;
@class Web_x0020_Service_AttdantList;
@class Web_x0020_Service_AttendantList;
@class Web_x0020_Service_BlockRoomDetails;
@class Web_x0020_Service_Building;
@class Web_x0020_Service_BuildingList;
@class Web_x0020_Service_CheckListCategory;
@class Web_x0020_Service_CheckListItem;
@class Web_x0020_Service_CheckListRoomType;
@class Web_x0020_Service_CheckListScoreForMaid;
@class Web_x0020_Service_ChkListCategoryLst;
@class Web_x0020_Service_ChkListItm;
@class Web_x0020_Service_ChkListRmTyp;
@class Web_x0020_Service_ChkListScoreForMaid;
@class Web_x0020_Service_CleaningStatus;
@class Web_x0020_Service_CleaningStatusList;
@class Web_x0020_Service_CommonConfigurationsList;
@class Web_x0020_Service_EHSKPUser;
@class Web_x0020_Service_ElementAddReassignRoom;
@class Web_x0020_Service_ElementAddReassignRoomResponse;
@class Web_x0020_Service_ElementAssignRoom;
@class Web_x0020_Service_ElementAssignRoomResponse;
@class Web_x0020_Service_ElementAuthenticateUser;
@class Web_x0020_Service_ElementAuthenticateUserResponse;
@class Web_x0020_Service_ElementChangeRoomAssignmentSequence;
@class Web_x0020_Service_ElementChangeRoomAssignmentSequenceResponse;
@class Web_x0020_Service_ElementCompleteAdditionalJob;
@class Web_x0020_Service_ElementCompleteAdditionalJobResponse;
@class Web_x0020_Service_ElementFindAttendant;
@class Web_x0020_Service_ElementFindAttendantResponse;
@class Web_x0020_Service_ElementFindGuestStayedHistoryRoutine;
@class Web_x0020_Service_ElementFindGuestStayedHistoryRoutineResponse;
@class Web_x0020_Service_ElementFindRoomByRoomNo;
@class Web_x0020_Service_ElementFindRoomByRoomNoResponse;
@class Web_x0020_Service_ElementGetAccessRights;
@class Web_x0020_Service_ElementGetAccessRightsResponse;
@class Web_x0020_Service_ElementGetAdditionalJobByRoomNo;
@class Web_x0020_Service_ElementGetAdditionalJobByRoomNoResponse;
@class Web_x0020_Service_ElementGetAdditionalJobCount;
@class Web_x0020_Service_ElementGetAdditionalJobCountResponse;
@class Web_x0020_Service_ElementGetAdditionalJobDetail;
@class Web_x0020_Service_ElementGetAdditionalJobDetailResponse;
@class Web_x0020_Service_ElementGetAdditionalJobFloorList;
@class Web_x0020_Service_ElementGetAdditionalJobFloorListResponse;
@class Web_x0020_Service_ElementGetAdditionalJobOfRoom;
@class Web_x0020_Service_ElementGetAdditionalJobOfRoomResponse;
@class Web_x0020_Service_ElementGetAdditionalJobRoomByFloor;
@class Web_x0020_Service_ElementGetAdditionalJobRoomByFloorResponse;
@class Web_x0020_Service_ElementGetAdditionalJobStatusesRoutine;
@class Web_x0020_Service_ElementGetAdditionalJobStatusesRoutineResponse;
@class Web_x0020_Service_ElementGetAdditionalJobTask;
@class Web_x0020_Service_ElementGetAdditionalJobTaskResponse;
@class Web_x0020_Service_ElementGetAllChecklist;
@class Web_x0020_Service_ElementGetAllChecklistResponse;
@class Web_x0020_Service_ElementGetAmenitiesCategoryList;
@class Web_x0020_Service_ElementGetAmenitiesCategoryListResponse;
@class Web_x0020_Service_ElementGetAmenitiesItemList;
@class Web_x0020_Service_ElementGetAmenitiesItemListResponse;
@class Web_x0020_Service_ElementGetAmenitiesLocationList;
@class Web_x0020_Service_ElementGetAmenitiesLocationListResponse;
@class Web_x0020_Service_ElementGetBuildingList;
@class Web_x0020_Service_ElementGetBuildingListResponse;
@class Web_x0020_Service_ElementGetCheckListItemScoreForMaid;
@class Web_x0020_Service_ElementGetCheckListItemScoreForMaidResponse;
@class Web_x0020_Service_ElementGetChecklistCategoryList;
@class Web_x0020_Service_ElementGetChecklistCategoryListResponse;
@class Web_x0020_Service_ElementGetChecklistItemList;
@class Web_x0020_Service_ElementGetChecklistItemListResponse;
@class Web_x0020_Service_ElementGetChecklistRoomType;
@class Web_x0020_Service_ElementGetChecklistRoomTypeResponse;
@class Web_x0020_Service_ElementGetCleaningStatusList;
@class Web_x0020_Service_ElementGetCleaningStatusListResponse;
@class Web_x0020_Service_ElementGetCommonConfigurations;
@class Web_x0020_Service_ElementGetCommonConfigurationsResponse;
@class Web_x0020_Service_ElementGetDiscrepantRoomStatus;
@class Web_x0020_Service_ElementGetDiscrepantRoomStatusResponse;
@class Web_x0020_Service_ElementGetEngineeringCategoryList;
@class Web_x0020_Service_ElementGetEngineeringCategoryListResponse;
@class Web_x0020_Service_ElementGetEngineeringItem;
@class Web_x0020_Service_ElementGetEngineeringItemResponse;
@class Web_x0020_Service_ElementGetFindFloorDetailsList;
@class Web_x0020_Service_ElementGetFindFloorDetailsListResponse;
@class Web_x0020_Service_ElementGetFindRoomAssignmentList;
@class Web_x0020_Service_ElementGetFindRoomAssignmentListResponse;
@class Web_x0020_Service_ElementGetFindRoomAssignmentLists;
@class Web_x0020_Service_ElementGetFindRoomAssignmentListsResponse;
@class Web_x0020_Service_ElementGetFindRoomDetailsList;
@class Web_x0020_Service_ElementGetFindRoomDetailsListResponse;
@class Web_x0020_Service_ElementGetFloorList;
@class Web_x0020_Service_ElementGetFloorListResponse;
@class Web_x0020_Service_ElementGetGuestInfo;
@class Web_x0020_Service_ElementGetGuestInfoByRoomID;
@class Web_x0020_Service_ElementGetGuestInfoByRoomIDResponse;
@class Web_x0020_Service_ElementGetGuestInfoResponse;
@class Web_x0020_Service_ElementGetGuestInformation;
@class Web_x0020_Service_ElementGetGuestInformationResponse;
@class Web_x0020_Service_ElementGetHotelInfo;
@class Web_x0020_Service_ElementGetHotelInfoResponse;
@class Web_x0020_Service_ElementGetInspectionStatus;
@class Web_x0020_Service_ElementGetInspectionStatusResponse;
@class Web_x0020_Service_ElementGetLFCategoryList;
@class Web_x0020_Service_ElementGetLFCategoryListResponse;
@class Web_x0020_Service_ElementGetLFColorList;
@class Web_x0020_Service_ElementGetLFColorListResponse;
@class Web_x0020_Service_ElementGetLFItemList;
@class Web_x0020_Service_ElementGetLFItemListResponse;
@class Web_x0020_Service_ElementGetLanguageList;
@class Web_x0020_Service_ElementGetLanguageListResponse;
@class Web_x0020_Service_ElementGetLaundryCategoryList;
@class Web_x0020_Service_ElementGetLaundryCategoryListResponse;
@class Web_x0020_Service_ElementGetLaundryItemList;
@class Web_x0020_Service_ElementGetLaundryItemListResponse;
@class Web_x0020_Service_ElementGetLaundryItemPriceList;
@class Web_x0020_Service_ElementGetLaundryItemPriceListResponse;
@class Web_x0020_Service_ElementGetLaundryServiceList;
@class Web_x0020_Service_ElementGetLaundryServiceListResponse;
@class Web_x0020_Service_ElementGetLaundrySpecialInstructionList;
@class Web_x0020_Service_ElementGetLaundrySpecialInstructionListResponse;
@class Web_x0020_Service_ElementGetLinenCategoryList;
@class Web_x0020_Service_ElementGetLinenCategoryListResponse;
@class Web_x0020_Service_ElementGetLinenItemList;
@class Web_x0020_Service_ElementGetLinenItemListResponse;
@class Web_x0020_Service_ElementGetLocationList;
@class Web_x0020_Service_ElementGetLocationListResponse;
@class Web_x0020_Service_ElementGetMessageAttachPhoto;
@class Web_x0020_Service_ElementGetMessageAttachPhotoResponse;
@class Web_x0020_Service_ElementGetMessageCategoryList;
@class Web_x0020_Service_ElementGetMessageCategoryList2;
@class Web_x0020_Service_ElementGetMessageCategoryList2Response;
@class Web_x0020_Service_ElementGetMessageCategoryListResponse;
@class Web_x0020_Service_ElementGetMessageItemList;
@class Web_x0020_Service_ElementGetMessageItemList2;
@class Web_x0020_Service_ElementGetMessageItemList2Response;
@class Web_x0020_Service_ElementGetMessageItemListResponse;
@class Web_x0020_Service_ElementGetMessageTemplateList;
@class Web_x0020_Service_ElementGetMessageTemplateListResponse;
@class Web_x0020_Service_ElementGetMinibarCategoryList;
@class Web_x0020_Service_ElementGetMinibarCategoryListResponse;
@class Web_x0020_Service_ElementGetMinibarItemList;
@class Web_x0020_Service_ElementGetMinibarItemListResponse;
@class Web_x0020_Service_ElementGetNewMessage;
@class Web_x0020_Service_ElementGetNewMessageResponse;
@class Web_x0020_Service_ElementGetNoOfAdHocMessage;
@class Web_x0020_Service_ElementGetNoOfAdHocMessageResponse;
@class Web_x0020_Service_ElementGetNoOfAdHocMessageRoutine;
@class Web_x0020_Service_ElementGetNoOfAdHocMessageRoutineResponse;
@class Web_x0020_Service_ElementGetNumberOfGuest;
@class Web_x0020_Service_ElementGetNumberOfGuestResponse;
@class Web_x0020_Service_ElementGetOOSBlockRoomsList;
@class Web_x0020_Service_ElementGetOOSBlockRoomsListResponse;
@class Web_x0020_Service_ElementGetOtherActivitiesLocationRoutine;
@class Web_x0020_Service_ElementGetOtherActivitiesLocationRoutineResponse;
@class Web_x0020_Service_ElementGetOtherActivitiesStatusRoutine;
@class Web_x0020_Service_ElementGetOtherActivitiesStatusRoutineResponse;
@class Web_x0020_Service_ElementGetOtherActivityAssignmentRoutine;
@class Web_x0020_Service_ElementGetOtherActivityAssignmentRoutineResponse;
@class Web_x0020_Service_ElementGetPanicAlertConfig;
@class Web_x0020_Service_ElementGetPanicAlertConfigResponse;
@class Web_x0020_Service_ElementGetPopupMsgList;
@class Web_x0020_Service_ElementGetPopupMsgListResponse;
@class Web_x0020_Service_ElementGetPostingHistoryRoutine;
@class Web_x0020_Service_ElementGetPostingHistoryRoutineResponse;
@class Web_x0020_Service_ElementGetPrevRoomAssignmentList;
@class Web_x0020_Service_ElementGetPrevRoomAssignmentListResponse;
@class Web_x0020_Service_ElementGetProfileNote;
@class Web_x0020_Service_ElementGetProfileNoteResponse;
@class Web_x0020_Service_ElementGetReceivedAdHocMessage;
@class Web_x0020_Service_ElementGetReceivedAdHocMessageResponse;
@class Web_x0020_Service_ElementGetReceivedAdHocMessageRoutine;
@class Web_x0020_Service_ElementGetReceivedAdHocMessageRoutineResponse;
@class Web_x0020_Service_ElementGetReleaseRoomsList;
@class Web_x0020_Service_ElementGetReleaseRoomsListResponse;
@class Web_x0020_Service_ElementGetRoomAssignmentList;
@class Web_x0020_Service_ElementGetRoomAssignmentListResponse;
@class Web_x0020_Service_ElementGetRoomDetail;
@class Web_x0020_Service_ElementGetRoomDetailResponse;
@class Web_x0020_Service_ElementGetRoomDetailsRoutine;
@class Web_x0020_Service_ElementGetRoomDetailsRoutineResponse;
@class Web_x0020_Service_ElementGetRoomRemarks;
@class Web_x0020_Service_ElementGetRoomRemarksResponse;
@class Web_x0020_Service_ElementGetRoomSectionList;
@class Web_x0020_Service_ElementGetRoomSectionListResponse;
@class Web_x0020_Service_ElementGetRoomSetGuideByRoomType;
@class Web_x0020_Service_ElementGetRoomSetGuideByRoomTypeResponse;
@class Web_x0020_Service_ElementGetRoomSetGuideList;
@class Web_x0020_Service_ElementGetRoomSetGuideListResponse;
@class Web_x0020_Service_ElementGetRoomStatusList;
@class Web_x0020_Service_ElementGetRoomStatusListResponse;
@class Web_x0020_Service_ElementGetRoomTypeByRoomNoRoutine;
@class Web_x0020_Service_ElementGetRoomTypeByRoomNoRoutineResponse;
@class Web_x0020_Service_ElementGetRoomTypeInventoryRoutine;
@class Web_x0020_Service_ElementGetRoomTypeInventoryRoutineResponse;
@class Web_x0020_Service_ElementGetRoomTypeList;
@class Web_x0020_Service_ElementGetRoomTypeListResponse;
@class Web_x0020_Service_ElementGetSecretsRoutine;
@class Web_x0020_Service_ElementGetSecretsRoutineResponse;
@class Web_x0020_Service_ElementGetSendAdHocMessageRoutine;
@class Web_x0020_Service_ElementGetSendAdHocMessageRoutineResponse;
@class Web_x0020_Service_ElementGetSentAdHocMessage;
@class Web_x0020_Service_ElementGetSentAdHocMessageResponse;
@class Web_x0020_Service_ElementGetStillLoggedIn;
@class Web_x0020_Service_ElementGetStillLoggedInResponse;
@class Web_x0020_Service_ElementGetSupervisorFiltersRoutine;
@class Web_x0020_Service_ElementGetSupervisorFiltersRoutineResponse;
@class Web_x0020_Service_ElementGetSupervisorFindFloorList;
@class Web_x0020_Service_ElementGetSupervisorFindFloorListResponse;
@class Web_x0020_Service_ElementGetSupervisorFindRoomAssignmentLists;
@class Web_x0020_Service_ElementGetSupervisorFindRoomAssignmentListsResponse;
@class Web_x0020_Service_ElementGetUnassignRoomList;
@class Web_x0020_Service_ElementGetUnassignRoomListResponse;
@class Web_x0020_Service_ElementGetUserDetailsRoutine;
@class Web_x0020_Service_ElementGetUserDetailsRoutineResponse;
@class Web_x0020_Service_ElementGetUserList;
@class Web_x0020_Service_ElementGetUserListResponse;
@class Web_x0020_Service_ElementGetVersion;
@class Web_x0020_Service_ElementGetVersionResponse;
@class Web_x0020_Service_ElementGetZoneList;
@class Web_x0020_Service_ElementGetZoneListResponse;
@class Web_x0020_Service_ElementGetZoneRoomList;
@class Web_x0020_Service_ElementGetZoneRoomListResponse;
@class Web_x0020_Service_ElementLogInvalidStartRoom;
@class Web_x0020_Service_ElementLogInvalidStartRoomResponse;
@class Web_x0020_Service_ElementPostAdditionalJobStatusUpdateRoutine;
@class Web_x0020_Service_ElementPostAdditionalJobStatusUpdateRoutineResponse;
@class Web_x0020_Service_ElementPostAmenitiesItem;
@class Web_x0020_Service_ElementPostAmenitiesItemResponse;
@class Web_x0020_Service_ElementPostAmenitiesItemsList;
@class Web_x0020_Service_ElementPostAmenitiesItemsListResponse;
@class Web_x0020_Service_ElementPostChecklistItem;
@class Web_x0020_Service_ElementPostChecklistItemResponse;
@class Web_x0020_Service_ElementPostChecklistItems;
@class Web_x0020_Service_ElementPostChecklistItemsResponse;
@class Web_x0020_Service_ElementPostECAttachPhoto;
@class Web_x0020_Service_ElementPostECAttachPhotoResponse;
@class Web_x0020_Service_ElementPostEngineeringCase;
@class Web_x0020_Service_ElementPostEngineeringCaseResponse;
@class Web_x0020_Service_ElementPostLFAttachPhoto;
@class Web_x0020_Service_ElementPostLFAttachPhotoResponse;
@class Web_x0020_Service_ElementPostLaundryItem;
@class Web_x0020_Service_ElementPostLaundryItemResponse;
@class Web_x0020_Service_ElementPostLaundryOrder;
@class Web_x0020_Service_ElementPostLaundryOrderResponse;
@class Web_x0020_Service_ElementPostLaundrySpecialInstruction;
@class Web_x0020_Service_ElementPostLaundrySpecialInstructionResponse;
@class Web_x0020_Service_ElementPostLinenItem;
@class Web_x0020_Service_ElementPostLinenItemResponse;
@class Web_x0020_Service_ElementPostLinenItems;
@class Web_x0020_Service_ElementPostLinenItemsResponse;
@class Web_x0020_Service_ElementPostLogout;
@class Web_x0020_Service_ElementPostLogoutResponse;
@class Web_x0020_Service_ElementPostLostAndFoundRoutine;
@class Web_x0020_Service_ElementPostLostAndFoundRoutineResponse;
@class Web_x0020_Service_ElementPostLostFound;
@class Web_x0020_Service_ElementPostLostFoundResponse;
@class Web_x0020_Service_ElementPostMessage;
@class Web_x0020_Service_ElementPostMessageAttachPhoto;
@class Web_x0020_Service_ElementPostMessageAttachPhotoResponse;
@class Web_x0020_Service_ElementPostMessageResponse;
@class Web_x0020_Service_ElementPostMinibarItem;
@class Web_x0020_Service_ElementPostMinibarItemResponse;
@class Web_x0020_Service_ElementPostMinibarItems;
@class Web_x0020_Service_ElementPostMinibarItemsResponse;
@class Web_x0020_Service_ElementPostMinibarOrder;
@class Web_x0020_Service_ElementPostMinibarOrderResponse;
@class Web_x0020_Service_ElementPostPostingHistoryRoutine;
@class Web_x0020_Service_ElementPostPostingHistoryRoutineResponse;
@class Web_x0020_Service_ElementPostUnassignRoomRoutine;
@class Web_x0020_Service_ElementPostUnassignRoomRoutineResponse;
@class Web_x0020_Service_ElementPostUpdateAdHocMessageRoutine;
@class Web_x0020_Service_ElementPostUpdateAdHocMessageRoutineResponse;
@class Web_x0020_Service_ElementPostUpdateDeviceTokenRoutine;
@class Web_x0020_Service_ElementPostUpdateDeviceTokenRoutineResponse;
@class Web_x0020_Service_ElementPostUpdateOtherActivityStatusRoutine;
@class Web_x0020_Service_ElementPostUpdateOtherActivityStatusRoutineResponse;
@class Web_x0020_Service_ElementPostUpdateRoomDetailsRoutine;
@class Web_x0020_Service_ElementPostUpdateRoomDetailsRoutineResponse;
@class Web_x0020_Service_ElementPostWSLog;
@class Web_x0020_Service_ElementPostWSLogResponse;
@class Web_x0020_Service_ElementPosteCnAhMsg;
@class Web_x0020_Service_ElementPosteCnAhMsgResponse;
@class Web_x0020_Service_ElementPosteCnJob;
@class Web_x0020_Service_ElementPosteCnJobResponse;
@class Web_x0020_Service_ElementPostingLinenItems;
@class Web_x0020_Service_ElementPostingLinenItemsResponse;
@class Web_x0020_Service_ElementPostingPanicAlert;
@class Web_x0020_Service_ElementPostingPanicAlertResponse;
@class Web_x0020_Service_ElementRemoveLaundryItem;
@class Web_x0020_Service_ElementRemoveLaundryItemResponse;
@class Web_x0020_Service_ElementStartAdditionalJob;
@class Web_x0020_Service_ElementStartAdditionalJobResponse;
@class Web_x0020_Service_ElementStopAdditionalJob;
@class Web_x0020_Service_ElementStopAdditionalJobResponse;
@class Web_x0020_Service_ElementUpdateAdHocMessageStatus;
@class Web_x0020_Service_ElementUpdateAdHocMessageStatusResponse;
@class Web_x0020_Service_ElementUpdateAdditionalJobRemark;
@class Web_x0020_Service_ElementUpdateAdditionalJobRemarkResponse;
@class Web_x0020_Service_ElementUpdateAdditionalJobStatus;
@class Web_x0020_Service_ElementUpdateAdditionalJobStatusResponse;
@class Web_x0020_Service_ElementUpdateCleaningStatus;
@class Web_x0020_Service_ElementUpdateCleaningStatusResponse;
@class Web_x0020_Service_ElementUpdateGuestInfo;
@class Web_x0020_Service_ElementUpdateGuestInfoResponse;
@class Web_x0020_Service_ElementUpdateInspection;
@class Web_x0020_Service_ElementUpdateInspectionResponse;
@class Web_x0020_Service_ElementUpdatePopupMsgStatus;
@class Web_x0020_Service_ElementUpdatePopupMsgStatusResponse;
@class Web_x0020_Service_ElementUpdateRoomAssignment;
@class Web_x0020_Service_ElementUpdateRoomAssignmentResponse;
@class Web_x0020_Service_ElementUpdateRoomCleaningStatus;
@class Web_x0020_Service_ElementUpdateRoomCleaningStatusResponse;
@class Web_x0020_Service_ElementUpdateRoomDetails;
@class Web_x0020_Service_ElementUpdateRoomDetailsResponse;
@class Web_x0020_Service_ElementUpdateRoomRemarks;
@class Web_x0020_Service_ElementUpdateRoomRemarksResponse;
@class Web_x0020_Service_ElementUpdateRoomStatus;
@class Web_x0020_Service_ElementUpdateRoomStatusResponse;
@class Web_x0020_Service_ElementValidateRoomNo;
@class Web_x0020_Service_ElementValidateRoomNoResponse;
@class Web_x0020_Service_EngCase;
@class Web_x0020_Service_EngCatList;
@class Web_x0020_Service_EngItmList;
@class Web_x0020_Service_EngineeringCategoryList;
@class Web_x0020_Service_EngineeringItemList;
@class Web_x0020_Service_FindGuestInformation;
@class Web_x0020_Service_FindGuestStayHistoryResponse;
@class Web_x0020_Service_Floor;
@class Web_x0020_Service_FloorDetails;
@class Web_x0020_Service_FloorList;
@class Web_x0020_Service_GetAdditionalJobStatusesResponse;
@class Web_x0020_Service_GetDiscrepantRoomStatusList;
@class Web_x0020_Service_GetFindFloorDetailsRequest;
@class Web_x0020_Service_GetFindFloorDetailsResponse;
@class Web_x0020_Service_GetFindRoomDetailsRequest;
@class Web_x0020_Service_GetFindRoomDetailsResponse;
@class Web_x0020_Service_GetPostingHistoryResponse;
@class Web_x0020_Service_GetRoomDetailsResponse;
@class Web_x0020_Service_GetRoomRemarksList;
@class Web_x0020_Service_GetSecretsRequest;
@class Web_x0020_Service_GetSecretsResponse;
@class Web_x0020_Service_GetSupervisorFiltersResponse;
@class Web_x0020_Service_GetUserDetailsResponse;
@class Web_x0020_Service_GroupAdHocMessage;
@class Web_x0020_Service_GrpAdHocList;
@class Web_x0020_Service_Guest;
@class Web_x0020_Service_GuestInfo;
@class Web_x0020_Service_GuestInformation;
@class Web_x0020_Service_GuestInformationResponse;
@class Web_x0020_Service_GuestOfRoom;
@class Web_x0020_Service_GuestOfRoomInfo;
@class Web_x0020_Service_GuestProfileNoteDetail;
@class Web_x0020_Service_GuestRoomInformation;
@class Web_x0020_Service_Hotel;
@class Web_x0020_Service_HotelInfo;
@class Web_x0020_Service_ImageAttachmentDetails;
@class Web_x0020_Service_InspStatus;
@class Web_x0020_Service_InspectionStatusList;
@class Web_x0020_Service_JobTask;
@class Web_x0020_Service_JobTaskList;
@class Web_x0020_Service_LanguageItem;
@class Web_x0020_Service_LanguageList;
@class Web_x0020_Service_LaundryInstructionList;
@class Web_x0020_Service_LaundryItemList;
@class Web_x0020_Service_LaundryItmPriceList;
@class Web_x0020_Service_LaundryList;
@class Web_x0020_Service_LaundryTypeList;
@class Web_x0020_Service_LdryInstList;
@class Web_x0020_Service_LdryItmList;
@class Web_x0020_Service_LdryItmPriceList;
@class Web_x0020_Service_LdrySrvList;
@class Web_x0020_Service_LdryTypList;
@class Web_x0020_Service_LinenCatList;
@class Web_x0020_Service_LinenCategoryList;
@class Web_x0020_Service_LinenItemList;
@class Web_x0020_Service_LinenItmList;
@class Web_x0020_Service_LocList;
@class Web_x0020_Service_LocationList;
@class Web_x0020_Service_LostAndFoundCategory;
@class Web_x0020_Service_LostAndFoundColorList;
@class Web_x0020_Service_LostAndFoundItemDetails;
@class Web_x0020_Service_LostAndFoundItemTypeList;
@class Web_x0020_Service_LostFnd;
@class Web_x0020_Service_LostNFoundCat;
@class Web_x0020_Service_LostNFoundColorList;
@class Web_x0020_Service_LostNFoundItmTypList;
@class Web_x0020_Service_MessageCategoryList;
@class Web_x0020_Service_MessageCategoryList2;
@class Web_x0020_Service_MessageItemList;
@class Web_x0020_Service_MessageItemList2;
@class Web_x0020_Service_MessageTemplateList;
@class Web_x0020_Service_MessageUpdateDetails;
@class Web_x0020_Service_MinibarCatList;
@class Web_x0020_Service_MinibarCategoryList;
@class Web_x0020_Service_MinibarItemList;
@class Web_x0020_Service_MinibarItmList;
@class Web_x0020_Service_MsgCatList;
@class Web_x0020_Service_MsgCatList2;
@class Web_x0020_Service_MsgItmList;
@class Web_x0020_Service_MsgItmList2;
@class Web_x0020_Service_MsgTempList;
@class Web_x0020_Service_NumberOfGuestInfo;
@class Web_x0020_Service_OOSBlockRoomList;
@class Web_x0020_Service_OtherActivityDetails;
@class Web_x0020_Service_OtherActivityDetailsList;
@class Web_x0020_Service_OtherActivityLocation;
@class Web_x0020_Service_OtherActivityLocationList;
@class Web_x0020_Service_OtherActivityStatus;
@class Web_x0020_Service_OtherActivityStatusList;
@class Web_x0020_Service_PanicAlertConfig;
@class Web_x0020_Service_PopupMsgItem;
@class Web_x0020_Service_PopupMsgList;
@class Web_x0020_Service_PostAdditionalJobStatusUpdateResponse;
@class Web_x0020_Service_PostAmenitiesItems;
@class Web_x0020_Service_PostAmenitiesList;
@class Web_x0020_Service_PostChecklistItem;
@class Web_x0020_Service_PostChecklistItemsList;
@class Web_x0020_Service_PostItem;
@class Web_x0020_Service_PostLinenItemsList;
@class Web_x0020_Service_PostLinenList;
@class Web_x0020_Service_PostLostAndFoundResponse;
@class Web_x0020_Service_PostMinibarItemsList;
@class Web_x0020_Service_PostMinibarList;
@class Web_x0020_Service_PostOtherActivityStatusUpdateResponse;
@class Web_x0020_Service_PostPhysicalCheck;
@class Web_x0020_Service_PostPostingHistoryResponse;
@class Web_x0020_Service_PostUpdateDeviceTokenRequest;
@class Web_x0020_Service_PostUpdateDeviceTokenResponse;
@class Web_x0020_Service_PosteCnAhMsgList;
@class Web_x0020_Service_PosteCnJobList;
@class Web_x0020_Service_PostedItem;
@class Web_x0020_Service_PostingHistoryDetails;
@class Web_x0020_Service_PostingLinenItemsList;
@class Web_x0020_Service_ProfileNote;
@class Web_x0020_Service_ProfileNoteType;
@class Web_x0020_Service_ProfileNoteTypeList;
@class Web_x0020_Service_Receiver;
@class Web_x0020_Service_ReleaseRoomDetails;
@class Web_x0020_Service_ReleaseRoomList;
@class Web_x0020_Service_ResponseStatus;
@class Web_x0020_Service_Rights;
@class Web_x0020_Service_RmSecList;
@class Web_x0020_Service_RoomAssign;
@class Web_x0020_Service_RoomAssignmentDetails;
@class Web_x0020_Service_RoomAssignmentList;
@class Web_x0020_Service_RoomDetail;
@class Web_x0020_Service_RoomDetailsRequestDetails;
@class Web_x0020_Service_RoomDetailsResponseDetails;
@class Web_x0020_Service_RoomSectionList;
@class Web_x0020_Service_RoomSetupList;
@class Web_x0020_Service_RoomStatus;
@class Web_x0020_Service_RoomStatusList;
@class Web_x0020_Service_RoomStpList;
@class Web_x0020_Service_RoomType;
@class Web_x0020_Service_RoomTypeInventoryDetails;
@class Web_x0020_Service_RoomTypeInventoryResponse;
@class Web_x0020_Service_RoomTypeList;
@class Web_x0020_Service_SprChkList;
@class Web_x0020_Service_SupervisorCheckList;
@class Web_x0020_Service_SupervisorFiltersDetails;
@class Web_x0020_Service_UnAssgnLst;
@class Web_x0020_Service_UnassignRoomList;
@class Web_x0020_Service_UniqueRoom;
@class Web_x0020_Service_UpdateAdHocMessageResponse;
@class Web_x0020_Service_UserDetail;
@class Web_x0020_Service_UserDetailsReponseDetails;
@class Web_x0020_Service_UserList;
@class Web_x0020_Service_Version;
@class Web_x0020_Service_WSSettings;
@class Web_x0020_Service_ZnLst;
@class Web_x0020_Service_ZnRmLst;
@class Web_x0020_Service_ZoneList;
@class Web_x0020_Service_ZoneRoomList;

@interface Web_x0020_Service_ElementGetMessageTemplateList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetMessageTemplateList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSString * strLastModified;
@end
@interface Web_x0020_Service_ElementGetMessageItemList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetMessageItemList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSString * strLastModified;
@end
@interface Web_x0020_Service_ArrayOfMessageItemList : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_MinibarCategoryList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_MinibarCategoryList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * mbcID;
@property (nonatomic, strong) NSString * mbcName;
@property (nonatomic, strong) NSString * mbcLang;
@property (nonatomic, strong) NSData * mbcPicture;
@property (nonatomic, strong) NSString * mbcLastModified;
@end
@interface Web_x0020_Service_PostAmenitiesList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_PostAmenitiesList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSNumber * AmnID;
@end
@interface Web_x0020_Service_ElementGetAdditionalJobRoomByFloorResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetAdditionalJobRoomByFloorResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_AdditionalJobRoomList * GetAdditionalJobRoomByFloorResult;
@end
@interface Web_x0020_Service_ArrayOfAmenitiesLocationList : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_ArrayOfPostItem : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_ElementPostMinibarItems : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementPostMinibarItems *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUserID;
@property (nonatomic, strong) NSNumber * intRoomAssignID;
@property (nonatomic, strong) NSNumber * intMinibarOrderID;
@property (nonatomic, strong) NSArray * PostItems;
@property (nonatomic, strong) Web_x0020_Service_UniqueRoom * urRoom;
@end
@interface Web_x0020_Service_ArrayOfLanguageItem : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_LanguageList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_LanguageList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSArray * LanguageList;
@end
@interface Web_x0020_Service_GroupAdHocMessage : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_GroupAdHocMessage *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * gadGrpID;
@end
@interface Web_x0020_Service_ArrayOfCheckListRoomType : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_ChkListRmTyp : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ChkListRmTyp *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSArray * ChkListRmTyp;
@end
@interface Web_x0020_Service_PosteCnJobList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_PosteCnJobList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSNumber * JobID;
@property (nonatomic, strong) NSString * WorkorderNo;
@end
@interface Web_x0020_Service_ArrayOfPostedItem : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_MessageCategoryList2 : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_MessageCategoryList2 *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * msgCatID;
@property (nonatomic, strong) NSString * msgCatName;
@property (nonatomic, strong) NSString * msgCatDesc;
@property (nonatomic, strong) NSString * msgCatNameLang;
@property (nonatomic, strong) NSString * msgCatNameLang2;
@property (nonatomic, strong) NSString * msgCatDescLang;
@property (nonatomic, strong) NSString * msgCatDescLang2;
@property (nonatomic, strong) NSData * msgCatPhoto;
@property (nonatomic, strong) NSString * msgCatLastModified;
@end
@interface Web_x0020_Service_ArrayOfAdditionalJobFloor : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_MessageItemList2 : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_MessageItemList2 *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * msgItmID;
@property (nonatomic, strong) NSNumber * msgItmSeq;
@property (nonatomic, strong) NSNumber * msgItmCatID;
@property (nonatomic, strong) NSString * msgItmName;
@property (nonatomic, strong) NSString * msgItmNameLang;
@property (nonatomic, strong) NSString * msgItmNameLang2;
@property (nonatomic, strong) NSString * msgItmDesc;
@property (nonatomic, strong) NSString * msgItmDescLang;
@property (nonatomic, strong) NSString * msgItmDescLang2;
@property (nonatomic, strong) NSData * msgItmPhoto;
@property (nonatomic, strong) NSString * msgItmLastModified;
@end
@interface Web_x0020_Service_ElementGetUserListResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetUserListResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_UserList * GetUserListResult;
@end
@interface Web_x0020_Service_ElementGetEngineeringCategoryListResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetEngineeringCategoryListResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_EngCatList * GetEngineeringCategoryListResult;
@end
@interface Web_x0020_Service_ElementGetGuestInformation : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetGuestInformation *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intHotelId;
@property (nonatomic, strong) NSString * strRoomNumber;
@end
@interface Web_x0020_Service_ArrayOfMessageCategoryList2 : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_MsgCatList2 : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_MsgCatList2 *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSArray * MessageCatList;
@end
@interface Web_x0020_Service_ElementUpdateInspection : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementUpdateInspection *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSNumber * intRoomAssignID;
@property (nonatomic, strong) NSNumber * intOverallScore;
@property (nonatomic, strong) NSString * strInspectedStartTime;
@property (nonatomic, strong) NSString * strInspectedEndTime;
@property (nonatomic, strong) NSNumber * intTotalInspectTime;
@property (nonatomic, strong) NSNumber * intInspectStatus;
@property (nonatomic, strong) NSNumber * intRoomStatus;
@property (nonatomic, strong) NSString * strInspectRemark;
@property (nonatomic, strong) NSString * strPauseTime;
@property (nonatomic, strong) NSString * strChecklistRemarks;
@end
@interface Web_x0020_Service_ElementLogInvalidStartRoomResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementLogInvalidStartRoomResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * LogInvalidStartRoomResult;
@end
@interface Web_x0020_Service_CheckListScoreForMaid : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_CheckListScoreForMaid *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * clsCheckListItemID;
@property (nonatomic, strong) NSNumber * clsInspectedBy;
@property (nonatomic, strong) NSNumber * clsScore;
@property (nonatomic, strong) NSString * clsLastModified;
@end
@interface Web_x0020_Service_ArrayOfRoomSectionList : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_RmSecList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_RmSecList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSArray * RmSec;
@end
@interface Web_x0020_Service_ElementGetRoomDetailsRoutineResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetRoomDetailsRoutineResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_GetRoomDetailsResponse * GetRoomDetailsRoutineResult;
@end
@interface Web_x0020_Service_Receiver : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_Receiver *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * staffID;
@property (nonatomic, strong) NSString * staffName;
@property (nonatomic, strong) NSString * email;
@property (nonatomic, strong) NSString * mobile;
@property (nonatomic, strong) NSNumber * isDirectCall;
@end
@interface Web_x0020_Service_ArrayOfZoneRoomList : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_ElementPostAmenitiesItemsListResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementPostAmenitiesItemsListResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_PostAmenitiesItems * PostAmenitiesItemsListResult;
@end
@interface Web_x0020_Service_RoomDetailsResponseDetails : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_RoomDetailsResponseDetails *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSString * RoomNo;
@property (nonatomic, strong) NSString * HotelID;
@property (nonatomic, strong) NSString * RemarksForType1;
@property (nonatomic, strong) NSString * RemarksForType2;
@property (nonatomic, strong) NSString * RemarksForType3;
@end
@interface Web_x0020_Service_ElementPostLogout : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementPostLogout *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@end
@interface Web_x0020_Service_ElementGetChecklistRoomType : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetChecklistRoomType *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSNumber * intHotelID;
@property (nonatomic, strong) NSString * strLastModified;
@end
@interface Web_x0020_Service_ArrayOfAmenitiesCategoryList : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_AmenitiesCatList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_AmenitiesCatList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSArray * AmenitiesCat;
@end
@interface Web_x0020_Service_ElementUpdateGuestInfo : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementUpdateGuestInfo *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSNumber * intRoomAssignID;
@property (nonatomic, strong) NSString * strGuestPref;
@end
@interface Web_x0020_Service_ElementGetFindFloorDetailsListResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetFindFloorDetailsListResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_GetFindFloorDetailsResponse * GetFindFloorDetailsListResult;
@end
@interface Web_x0020_Service_ElementGetLFItemListResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetLFItemListResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_LostNFoundItmTypList * GetLFItemListResult;
@end
@interface Web_x0020_Service_ArrayOfCheckListCategory : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_ChkListCategoryLst : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ChkListCategoryLst *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSArray * ChkListCategoryLst;
@end
@interface Web_x0020_Service_ElementGetLFCategoryList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetLFCategoryList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSString * strLastModified;
@property (nonatomic, strong) NSString * strHotel_ID;
@end
@interface Web_x0020_Service_ElementPostingLinenItems : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementPostingLinenItems *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUserID;
@property (nonatomic, strong) NSNumber * intRoomAssignID;
@property (nonatomic, strong) NSString * strRoomNo;
@property (nonatomic, strong) NSArray * PostItems;
@end
@interface Web_x0020_Service_ArrayOfReceiver : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_PanicAlertConfig : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_PanicAlertConfig *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSString * raPhone;
@property (nonatomic, strong) NSString * supName;
@property (nonatomic, strong) NSString * supStaffID;
@property (nonatomic, strong) NSNumber * isAdhocMsg;
@property (nonatomic, strong) NSNumber * isSMS;
@property (nonatomic, strong) NSNumber * isEmail;
@property (nonatomic, strong) NSNumber * isAlarmSound;
@property (nonatomic, strong) NSNumber * isEConnectJob;
@property (nonatomic, strong) NSString * msgSubject;
@property (nonatomic, strong) NSString * msgContent;
@property (nonatomic, strong) NSNumber * isDirectCall;
@property (nonatomic, strong) NSArray * receivers;
@end
@interface Web_x0020_Service_ElementChangeRoomAssignmentSequenceResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementChangeRoomAssignmentSequenceResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ChangeRoomAssignmentSequenceResult;
@end
@interface Web_x0020_Service_ElementStartAdditionalJobResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementStartAdditionalJobResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * StartAdditionalJobResult;
@end
@interface Web_x0020_Service_ArrayOfBuilding : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_BuildingList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_BuildingList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSArray * BuildingList;
@end
@interface Web_x0020_Service_ElementGetRoomSetGuideByRoomType : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetRoomSetGuideByRoomType *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSString * intRoomTypeID;
@property (nonatomic, strong) NSNumber * intHotelID;
@property (nonatomic, strong) NSString * strLastModified;
@end
@interface Web_x0020_Service_ElementPostUpdateOtherActivityStatusRoutineResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementPostUpdateOtherActivityStatusRoutineResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_PostOtherActivityStatusUpdateResponse * PostUpdateOtherActivityStatusRoutineResult;
@end
@interface Web_x0020_Service_AdditionalJobOfRoom : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_AdditionalJobOfRoom *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) Web_x0020_Service_AdditionalJobRoom * AddtionalJobRoom;
@end
@interface Web_x0020_Service_ArrayOfFloor : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_ElementGetZoneList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetZoneList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intHotelID;
@property (nonatomic, strong) NSString * strLastModified;
@end
@interface Web_x0020_Service_ArrayOfReleaseRoomDetails : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_ElementChangeRoomAssignmentSequence : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementChangeRoomAssignmentSequence *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSNumber * intRoomAssignID;
@property (nonatomic, strong) NSNumber * intNextRoomAssignID;
@end
@interface Web_x0020_Service_ArrayOfLostAndFoundItemDetails : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_ElementPostLostAndFoundRoutine : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementPostLostAndFoundRoutine *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSArray * LostAndFoundItemsListing;
@end
@interface Web_x0020_Service_ElementGetLaundryItemPriceListResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetLaundryItemPriceListResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_LdryItmPriceList * GetLaundryItemPriceListResult;
@end
@interface Web_x0020_Service_ElementPostLaundrySpecialInstruction : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementPostLaundrySpecialInstruction *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSNumber * intLaundryOrderID;
@property (nonatomic, strong) NSNumber * intLaundrySpcialInstructionID;
@end
@interface Web_x0020_Service_LaundryTypeList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_LaundryTypeList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * ldryCatID;
@property (nonatomic, strong) NSString * ldryCatName;
@property (nonatomic, strong) NSString * ldryCatLang;
@property (nonatomic, strong) NSData * ldryCatPicture;
@property (nonatomic, strong) NSString * ldryTypLastModified;
@end
@interface Web_x0020_Service_MessageUpdateDetails : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_MessageUpdateDetails *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSString * UserID;
@property (nonatomic, strong) NSString * MessageID;
@property (nonatomic, strong) NSString * PropertyID;
@property (nonatomic, strong) NSString * UpdateType;
@end
@interface Web_x0020_Service_ElementGetAdditionalJobStatusesRoutineResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetAdditionalJobStatusesRoutineResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_GetAdditionalJobStatusesResponse * GetAdditionalJobStatusesRoutineResult;
@end
@interface Web_x0020_Service_ArrayOfLinenItemList : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_ElementGetAdditionalJobOfRoom : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetAdditionalJobOfRoom *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intHotelID;
@property (nonatomic, strong) NSNumber * intUserID;
@property (nonatomic, strong) NSNumber * intDutyAssignID;
@end
@interface Web_x0020_Service_ElementPostUpdateRoomDetailsRoutine : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementPostUpdateRoomDetailsRoutine *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSString * UserID;
@property (nonatomic, strong) NSString * RoomNo;
@property (nonatomic, strong) NSString * PropertyID;
@property (nonatomic, strong) NSString * UpdateType;
@property (nonatomic, strong) NSString * RoomStatusID;
@property (nonatomic, strong) NSString * CleaningStatusID;
@end
@interface Web_x0020_Service_ArrayOfLostAndFoundItemTypeList : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_ElementGetAdditionalJobByRoomNo : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetAdditionalJobByRoomNo *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intHotelID;
@property (nonatomic, strong) NSNumber * intUserID;
@property (nonatomic, strong) NSString * sRoomNo;
@end
@interface Web_x0020_Service_ElementGetUnassignRoomListResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetUnassignRoomListResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_UnAssgnLst * GetUnassignRoomListResult;
@end
@interface Web_x0020_Service_ArrayOfProfileNote : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_ArrayOfWSSettings : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_ArrayOfFloorDetails : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_GetFindFloorDetailsResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_GetFindFloorDetailsResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSArray * FloorListing;
@end
@interface Web_x0020_Service_PostUpdateDeviceTokenResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_PostUpdateDeviceTokenResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@end
@interface Web_x0020_Service_ElementGetOOSBlockRoomsListResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetOOSBlockRoomsListResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_OOSBlockRoomList * GetOOSBlockRoomsListResult;
@end
@interface Web_x0020_Service_ElementGetPanicAlertConfigResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetPanicAlertConfigResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_PanicAlertConfig * GetPanicAlertConfigResult;
@end
@interface Web_x0020_Service_ArrayOfRoomAssignmentDetails : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_GetFindRoomDetailsResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_GetFindRoomDetailsResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSArray * RoomAssignmentListing;
@end
@interface Web_x0020_Service_ElementGetMinibarCategoryListResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetMinibarCategoryListResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_MinibarCatList * GetMinibarCategoryListResult;
@end
@interface Web_x0020_Service_ArrayOfLinenCategoryList : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_PostChecklistItem : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_PostChecklistItem *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intChecklistItemID;
@property (nonatomic, strong) NSNumber * intScoreValue;
@end
@interface Web_x0020_Service_ElementGetCleaningStatusList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetCleaningStatusList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSString * strLastModified;
@end
@interface Web_x0020_Service_ElementGetAmenitiesItemList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetAmenitiesItemList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSNumber * intHotelID;
@property (nonatomic, strong) NSString * intCategoryID;
@property (nonatomic, strong) NSString * strLastModified;
@end
@interface Web_x0020_Service_MessageCategoryList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_MessageCategoryList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * msgCatID;
@property (nonatomic, strong) NSString * msgCatName;
@property (nonatomic, strong) NSString * msgCatDesc;
@property (nonatomic, strong) NSData * msgCatPhoto;
@property (nonatomic, strong) NSString * msgCatLastModified;
@end
@interface Web_x0020_Service_ElementGetFindRoomAssignmentListResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetFindRoomAssignmentListResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_RoomAssignmentList * GetFindRoomAssignmentListResult;
@end
@interface Web_x0020_Service_ElementGetMessageItemList2Response : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetMessageItemList2Response *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_MsgItmList2 * GetMessageItemList2Result;
@end
@interface Web_x0020_Service_ElementGetPrevRoomAssignmentListResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetPrevRoomAssignmentListResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_RoomAssignmentList * GetPrevRoomAssignmentListResult;
@end
@interface Web_x0020_Service_ArrayOfGuestProfileNoteDetail : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_GuestInformation : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_GuestInformation *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSString * strTitle;
@property (nonatomic, strong) NSString * strName;
@property (nonatomic, strong) NSString * strLangPreference;
@property (nonatomic, strong) NSString * strCheckInDate;
@property (nonatomic, strong) NSString * strCheckOutDate;
@property (nonatomic, strong) NSString * strVIP;
@property (nonatomic, strong) NSData * binaryPhoto;
@property (nonatomic, strong) NSString * strLastModified;
@property (nonatomic, strong) NSString * strSpecialService;
@property (nonatomic, strong) NSString * strPreferenceCodes;
@property (nonatomic, strong) NSArray * mGuestProfileNoteList;
@property (nonatomic, strong) NSString * strFirstName;
@property (nonatomic, strong) NSString * strLastName;
@end
@interface Web_x0020_Service_ElementUpdateRoomAssignmentResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementUpdateRoomAssignmentResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * UpdateRoomAssignmentResult;
@end
@interface Web_x0020_Service_ElementPostAmenitiesItemResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementPostAmenitiesItemResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_PostAmenitiesList * PostAmenitiesItemResult;
@end
@interface Web_x0020_Service_ElementGetNoOfAdHocMessageResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetNoOfAdHocMessageResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_AdHocMessageCounter * GetNoOfAdHocMessageResult;
@end
@interface Web_x0020_Service_ElementGetRoomTypeInventoryRoutine : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetRoomTypeInventoryRoutine *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSString * UserID;
@property (nonatomic, strong) NSString * PropertyID;
@property (nonatomic, strong) NSString * ServiceID;
@end
@interface Web_x0020_Service_ElementGetReceivedAdHocMessageRoutineResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetReceivedAdHocMessageRoutineResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_AdHocMsgList2 * GetReceivedAdHocMessageRoutineResult;
@end
@interface Web_x0020_Service_ElementPostECAttachPhotoResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementPostECAttachPhotoResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * PostECAttachPhotoResult;
@end
@interface Web_x0020_Service_ElementUpdateRoomCleaningStatus : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementUpdateRoomCleaningStatus *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intSupervisorID;
@property (nonatomic, strong) NSNumber * intRoomAssignID;
@property (nonatomic, strong) NSNumber * intCleaningStatusID;
@end
@interface Web_x0020_Service_ElementPostAdditionalJobStatusUpdateRoutineResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementPostAdditionalJobStatusUpdateRoutineResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_PostAdditionalJobStatusUpdateResponse * PostAdditionalJobStatusUpdateRoutineResult;
@end
@interface Web_x0020_Service_PopupMsgItem : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_PopupMsgItem *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * popupID;
@property (nonatomic, strong) NSString * popupMsg;
@property (nonatomic, strong) NSString * popupDateTime;
@end
@interface Web_x0020_Service_ArrayOfRights : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_ElementGetAmenitiesCategoryListResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetAmenitiesCategoryListResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_AmenitiesCatList * GetAmenitiesCategoryListResult;
@end
@interface Web_x0020_Service_ElementGetLinenCategoryListResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetLinenCategoryListResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_LinenCatList * GetLinenCategoryListResult;
@end
@interface Web_x0020_Service_ArrayOfEngineeringItemList : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_EngItmList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_EngItmList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSArray * EngineeringItmList;
@end
@interface Web_x0020_Service_ElementGetSecretsRoutineResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetSecretsRoutineResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_GetSecretsResponse * GetSecretsRoutineResult;
@end
@interface Web_x0020_Service_CheckListRoomType : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_CheckListRoomType *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * clrtID;
@property (nonatomic, strong) NSNumber * clrtRoomTypeID;
@property (nonatomic, strong) NSNumber * clrtChecklistID;
@property (nonatomic, strong) NSString * clrtLastModified;
@end
@interface Web_x0020_Service_ArrayOfLostAndFoundCategory : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_ArrayOfImageAttachmentDetails : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_LostAndFoundItemDetails : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_LostAndFoundItemDetails *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSString * UserID;
@property (nonatomic, strong) NSString * RoomNo;
@property (nonatomic, strong) NSString * HotelID;
@property (nonatomic, strong) NSString * DutyAssignmentID;
@property (nonatomic, strong) NSString * CategoryID;
@property (nonatomic, strong) NSString * ItemID;
@property (nonatomic, strong) NSString * ColorID;
@property (nonatomic, strong) NSString * Quantity;
@property (nonatomic, strong) NSString * FoundDateTime;
@property (nonatomic, strong) NSString * Remarks;
@property (nonatomic, strong) NSArray * ImageAttachmentListing;
@end
@interface Web_x0020_Service_ElementGetSendAdHocMessageRoutineResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetSendAdHocMessageRoutineResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_AdHocMsgList2 * GetSendAdHocMessageRoutineResult;
@end
@interface Web_x0020_Service_ElementPostLFAttachPhoto : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementPostLFAttachPhoto *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSNumber * intRoomAssignID;
@property (nonatomic, strong) NSData * binPhoto;
@end
@interface Web_x0020_Service_ElementGetRoomDetail : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetRoomDetail *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSNumber * intRoomAssignID;
@property (nonatomic, strong) NSString * strLastModified;
@end
@interface Web_x0020_Service_ArrayOfRoomTypeInventoryDetails : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_ElementGetRoomStatusList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetRoomStatusList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSString * strLastModified;
@end
@interface Web_x0020_Service_AdHocMessageList2 : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_AdHocMessageList2 *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * amsgID;
@property (nonatomic, strong) NSNumber * amsgGrpID;
@property (nonatomic, strong) NSString * amsgSendFrom;
@property (nonatomic, strong) NSString * amsgSendFromLang;
@property (nonatomic, strong) NSString * amsgTopic;
@property (nonatomic, strong) NSString * amsgContent;
@property (nonatomic, strong) NSString * amsgLastModified;
@property (nonatomic, strong) NSNumber * amsgPhotoAttached;
@property (nonatomic, strong) NSString * amsgReceivedIDs;
@property (nonatomic, strong) NSNumber * amsgStatus;
@end
@interface Web_x0020_Service_ElementGetRoomStatusListResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetRoomStatusListResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_RoomStatusList * GetRoomStatusListResult;
@end
@interface Web_x0020_Service_ElementGetRoomSetGuideListResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetRoomSetGuideListResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_RoomStpList * GetRoomSetGuideListResult;
@end
@interface Web_x0020_Service_ElementGetLaundryItemPriceList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetLaundryItemPriceList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSString * strLastModified;
@end
@interface Web_x0020_Service_ElementGetOOSBlockRoomsList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetOOSBlockRoomsList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSNumber * intHTID;
@end
@interface Web_x0020_Service_CleaningStatus : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_CleaningStatus *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSString * csID;
@property (nonatomic, strong) NSString * csName;
@property (nonatomic, strong) NSString * csLang;
@property (nonatomic, strong) NSData * csIcon;
@property (nonatomic, strong) NSString * csLastModifed;
@end
@interface Web_x0020_Service_ElementGetAmenitiesLocationList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetAmenitiesLocationList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSString * strLastModified;
@end
@interface Web_x0020_Service_PostLinenList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_PostLinenList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSNumber * lnID;
@end
@interface Web_x0020_Service_ElementGetPrevRoomAssignmentList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetPrevRoomAssignmentList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSString * strLastModified;
@end
@interface Web_x0020_Service_ElementGetLaundryCategoryListResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetLaundryCategoryListResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_LdryTypList * GetLaundryCategoryListResult;
@end
@interface Web_x0020_Service_GuestProfileNoteDetail : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_GuestProfileNoteDetail *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSString * strTypeCode;
@property (nonatomic, strong) NSString * strTypeDescription;
@property (nonatomic, strong) NSString * strNoteDescription;
@end
@interface Web_x0020_Service_GetRoomRemarksList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_GetRoomRemarksList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSString * RoomNo;
@property (nonatomic, strong) NSNumber * HTID;
@property (nonatomic, strong) NSString * Remarks;
@property (nonatomic, strong) NSString * LastModified;
@end
@interface Web_x0020_Service_ElementGetFindRoomAssignmentListsResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetFindRoomAssignmentListsResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_RoomAssignmentList * GetFindRoomAssignmentListsResult;
@end
@interface Web_x0020_Service_MsgItmList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_MsgItmList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSArray * MessageItmList;
@end
@interface Web_x0020_Service_LinenItmList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_LinenItmList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSArray * LinenItm;
@end
@interface Web_x0020_Service_ElementGetLFCategoryListResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetLFCategoryListResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_LostNFoundCat * GetLFCategoryListResult;
@end
@interface Web_x0020_Service_ArrayOfOtherActivityDetails : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_LaundryItmPriceList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_LaundryItmPriceList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * ldryPriID;
@property (nonatomic, strong) NSNumber * ldryPriLaundryID;
@property (nonatomic, strong) NSNumber * ldryLaundryCategoryID;
@property (nonatomic, strong) NSNumber * ldryPrice;
@property (nonatomic, strong) NSString * ldryLastModified;
@end
@interface Web_x0020_Service_ArrayOfAttendantList : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_ElementGetBuildingList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetBuildingList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intHotelID;
@property (nonatomic, strong) NSString * strLastModified;
@end
@interface Web_x0020_Service_ElementGetSupervisorFindFloorListResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetSupervisorFindFloorListResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_FloorList * GetSupervisorFindFloorListResult;
@end
@interface Web_x0020_Service_ElementPostMinibarOrder : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementPostMinibarOrder *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSNumber * intRoomAssignID;
@property (nonatomic, strong) NSNumber * intTotalQuantity;
@property (nonatomic, strong) NSNumber * dblServiceCharge;
@property (nonatomic, strong) NSNumber * dblSubTotal;
@property (nonatomic, strong) NSString * strTransactionTime;
@property (nonatomic, strong) NSNumber * intHotelID;
@property (nonatomic, strong) NSString * strRoomNo;
@end
@interface Web_x0020_Service_ArrayOfAdHocPhoto : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_ArrayOfCheckListItem : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_LostAndFoundCategory : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_LostAndFoundCategory *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * lfcID;
@property (nonatomic, strong) NSNumber * lfcHotelID;
@property (nonatomic, strong) NSString * lfcName;
@property (nonatomic, strong) NSString * lfcLang;
@property (nonatomic, strong) NSData * lfcPicture;
@property (nonatomic, strong) NSString * lfcLastModified;
@end
@interface Web_x0020_Service_ElementGetFindRoomDetailsList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetFindRoomDetailsList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_GetFindRoomDetailsRequest * GetFindRoomDetailsRequest;
@end
@interface Web_x0020_Service_ArrayOfRoomDetailsRequestDetails : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_ElementGetRoomDetailsRoutine : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetRoomDetailsRoutine *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSString * UserID;
@property (nonatomic, strong) NSArray * RoomDetailsRequestListing;
@end
@interface Web_x0020_Service_FindGuestInformation : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_FindGuestInformation *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSString * RoomNo;
@property (nonatomic, strong) NSString * RoomStatus;
@property (nonatomic, strong) NSString * HousekeeperName;
@property (nonatomic, strong) NSString * NoOfGuest;
@property (nonatomic, strong) NSString * Title;
@property (nonatomic, strong) NSString * Name;
@property (nonatomic, strong) NSString * LangPreference;
@property (nonatomic, strong) NSString * CheckInDate;
@property (nonatomic, strong) NSString * CheckOutDate;
@property (nonatomic, strong) NSString * VIP;
@property (nonatomic, strong) NSString * SpecialService;
@property (nonatomic, strong) NSString * PreferenceCodes;
@property (nonatomic, strong) NSArray * mGuestProfileNoteList;
@end
@interface Web_x0020_Service_EngineeringCategoryList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_EngineeringCategoryList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * encCatID;
@property (nonatomic, strong) NSString * encCatName;
@property (nonatomic, strong) NSString * encCatLang;
@property (nonatomic, strong) NSData * encPicture;
@property (nonatomic, strong) NSString * encCatLastModified;
@end
@interface Web_x0020_Service_ElementGetPopupMsgList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetPopupMsgList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSString * strLastModified;
@end
@interface Web_x0020_Service_ArrayOfFindGuestInformation : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_ElementGetAdditionalJobTaskResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetAdditionalJobTaskResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_JobTaskList * GetAdditionalJobTaskResult;
@end
@interface Web_x0020_Service_ElementPostUpdateRoomDetailsRoutineResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementPostUpdateRoomDetailsRoutineResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_PostPhysicalCheck * PostUpdateRoomDetailsRoutineResult;
@end
@interface Web_x0020_Service_ElementUpdateRoomRemarksResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementUpdateRoomRemarksResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * UpdateRoomRemarksResult;
@end
@interface Web_x0020_Service_ZnRmLst : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ZnRmLst *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSArray * ZnRoomLst;
@end
@interface Web_x0020_Service_ArrayOfLostAndFoundColorList : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_AmenitiesItemList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_AmenitiesItemList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * amiID;
@property (nonatomic, strong) NSString * amiName;
@property (nonatomic, strong) NSString * amiLang;
@property (nonatomic, strong) NSNumber * amiCategoryID;
@property (nonatomic, strong) NSNumber * amiRoomTypeID;
@property (nonatomic, strong) NSNumber * amiInventoryLocationID;
@property (nonatomic, strong) NSData * amiPicture;
@property (nonatomic, strong) NSString * amiLastModified;
@end
@interface Web_x0020_Service_UnassignRoomList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_UnassignRoomList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * uarRoomAssignID;
@property (nonatomic, strong) NSString * uarRoomNo;
@property (nonatomic, strong) NSNumber * uarHotelID;
@property (nonatomic, strong) NSNumber * uarZoneID;
@property (nonatomic, strong) NSNumber * uarFloorID;
@property (nonatomic, strong) NSNumber * uarRoomStatusID;
@property (nonatomic, strong) NSNumber * uarCleaningStatusID;
@property (nonatomic, strong) NSString * uarVIP;
@property (nonatomic, strong) NSString * uarGuestFirstName;
@property (nonatomic, strong) NSString * uarGuestLastName;
@end
@interface Web_x0020_Service_ElementStopAdditionalJobResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementStopAdditionalJobResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * StopAdditionalJobResult;
@end
@interface Web_x0020_Service_ElementPostLinenItems : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementPostLinenItems *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUserID;
@property (nonatomic, strong) NSNumber * intRoomAssignID;
@property (nonatomic, strong) NSArray * PostItems;
@property (nonatomic, strong) Web_x0020_Service_UniqueRoom * urRoom;
@end
@interface Web_x0020_Service_LostNFoundItmTypList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_LostNFoundItmTypList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSArray * LostAndFoundItemTypeList;
@end
@interface Web_x0020_Service_MinibarItemList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_MinibarItemList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * mbiID;
@property (nonatomic, strong) NSString * mbiName;
@property (nonatomic, strong) NSString * mbiLang;
@property (nonatomic, strong) NSNumber * mbiCategoryID;
@property (nonatomic, strong) NSNumber * mbiPrice;
@property (nonatomic, strong) NSData * mbiPicture;
@property (nonatomic, strong) NSString * mbiLastModified;
@end
@interface Web_x0020_Service_ElementUpdateInspectionResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementUpdateInspectionResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * UpdateInspectionResult;
@end
@interface Web_x0020_Service_ElementGetOtherActivitiesLocationRoutineResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetOtherActivitiesLocationRoutineResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_OtherActivityLocationList * GetOtherActivitiesLocationRoutineResult;
@end
@interface Web_x0020_Service_ElementGetLFColorList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetLFColorList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSString * strLastModified;
@end
@interface Web_x0020_Service_ArrayOfBlockRoomDetails : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_OOSBlockRoomList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_OOSBlockRoomList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSArray * BlockRoomList;
@end
@interface Web_x0020_Service_ArrayOfGroupAdHocMessage : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_GrpAdHocList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_GrpAdHocList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSArray * GrpAdHocMsg;
@end
@interface Web_x0020_Service_PosteCnAhMsgList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_PosteCnAhMsgList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSNumber * AdhocID;
@end
@interface Web_x0020_Service_ArrayOfLaundryInstructionList : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_ElementGetProfileNoteResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetProfileNoteResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ProfileNoteTypeList * GetProfileNoteResult;
@end
@interface Web_x0020_Service_AdHocMsgPhoto : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_AdHocMsgPhoto *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSArray * AdHocMessagePhoto;
@end
@interface Web_x0020_Service_LinenCategoryList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_LinenCategoryList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * lncID;
@property (nonatomic, strong) NSString * lncName;
@property (nonatomic, strong) NSString * lncLang;
@property (nonatomic, strong) NSData * lncPicture;
@property (nonatomic, strong) NSString * lncLastModified;
@end
@interface Web_x0020_Service_ElementPostLostFound : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementPostLostFound *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSNumber * intLFCategoryID;
@property (nonatomic, strong) NSNumber * intLFItemID;
@property (nonatomic, strong) NSNumber * intLFColorID;
@property (nonatomic, strong) NSNumber * intQuantity;
@property (nonatomic, strong) NSString * strFoundTime;
@property (nonatomic, strong) NSString * strRemark;
@property (nonatomic, strong) NSNumber * intRoomAssignID;
@property (nonatomic, strong) Web_x0020_Service_UniqueRoom * urRoom;
@end
@interface Web_x0020_Service_ElementCompleteAdditionalJob : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementCompleteAdditionalJob *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * ondayAdditionalJobId;
@property (nonatomic, strong) NSString * completedDateTime;
@property (nonatomic, strong) NSString * UserID;
@end
@interface Web_x0020_Service_ElementGetCommonConfigurations : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetCommonConfigurations *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@end
@interface Web_x0020_Service_Rights : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_Rights *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSString * arModule;
@property (nonatomic, strong) NSNumber * arView;
@property (nonatomic, strong) NSNumber * arEdit;
@property (nonatomic, strong) NSNumber * arAdd;
@property (nonatomic, strong) NSNumber * arRemove;
@property (nonatomic, strong) NSNumber * arExport;
@property (nonatomic, strong) NSNumber * arAccess;
@end
@interface Web_x0020_Service_ArrayOfAdHocMessageList2 : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_AdHocMsgList2 : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_AdHocMsgList2 *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSArray * AdHocMessageList;
@end
@interface Web_x0020_Service_ElementGetNoOfAdHocMessage : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetNoOfAdHocMessage *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSString * intGrpMessageID;
@property (nonatomic, strong) NSNumber * intHotelID;
@property (nonatomic, strong) NSString * strLastModified;
@end
@interface Web_x0020_Service_ElementPostAdditionalJobStatusUpdateRoutine : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementPostAdditionalJobStatusUpdateRoutine *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSString * UserID;
@property (nonatomic, strong) NSString * JobID;
@property (nonatomic, strong) NSString * StatusID;
@property (nonatomic, strong) NSString * ServiceLaterDateTime;
@end
@interface Web_x0020_Service_ElementFindGuestStayedHistoryRoutineResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementFindGuestStayedHistoryRoutineResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_FindGuestStayHistoryResponse * FindGuestStayedHistoryRoutineResult;
@end
@interface Web_x0020_Service_ElementGetZoneListResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetZoneListResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ZnLst * GetZoneListResult;
@end
@interface Web_x0020_Service_ElementGetEngineeringItemResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetEngineeringItemResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_EngItmList * GetEngineeringItemResult;
@end
@interface Web_x0020_Service_ElementPostMinibarItem : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementPostMinibarItem *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSNumber * intRoomAssignID;
@property (nonatomic, strong) NSNumber * intMinibarOrderID;
@property (nonatomic, strong) NSNumber * intMinibarItemID;
@property (nonatomic, strong) NSNumber * intNewQuantity;
@property (nonatomic, strong) NSNumber * intUsedQuantity;
@property (nonatomic, strong) NSString * strTransactionTime;
@end
@interface Web_x0020_Service_ElementGetSendAdHocMessageRoutine : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetSendAdHocMessageRoutine *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSString * UserID;
@property (nonatomic, strong) NSString * GroupMessageID;
@property (nonatomic, strong) NSString * PropertyID;
@property (nonatomic, strong) NSString * LastModifiedDateTime;
@end
@interface Web_x0020_Service_ElementPosteCnJobResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementPosteCnJobResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_PosteCnJobList * PosteCnJobResult;
@end
@interface Web_x0020_Service_ArrayOfMessageCategoryList : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_ElementGetCheckListItemScoreForMaid : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetCheckListItemScoreForMaid *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSNumber * intDutyAssignID;
@property (nonatomic, strong) NSString * strLastModified;
@end
@interface Web_x0020_Service_ElementFindGuestStayedHistoryRoutine : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementFindGuestStayedHistoryRoutine *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSString * UserID;
@property (nonatomic, strong) NSString * Search;
@property (nonatomic, strong) NSString * PropertyID;
@property (nonatomic, strong) NSString * BuildingID;
@property (nonatomic, strong) NSString * FilterStatus;
@property (nonatomic, strong) NSString * FilterRange;
@property (nonatomic, strong) NSString * PageNumber;
@end
@interface Web_x0020_Service_ElementGetHotelInfo : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetHotelInfo *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSNumber * intHotelID;
@property (nonatomic, strong) NSString * strLastModified;
@end
@interface Web_x0020_Service_ElementFindAttendant : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementFindAttendant *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSNumber * intHotelID;
@property (nonatomic, strong) NSString * strAttendantName;
@end
@interface Web_x0020_Service_RoomAssign : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_RoomAssign *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSString * raID;
@property (nonatomic, strong) NSString * raHousekeeperID;
@property (nonatomic, strong) NSString * raRoomNo;
@property (nonatomic, strong) NSString * raRoomStatusID;
@property (nonatomic, strong) NSString * raCleaningStatusID;
@property (nonatomic, strong) NSString * raRoomTypeID;
@property (nonatomic, strong) NSString * raFloorID;
@property (nonatomic, strong) NSNumber * raBuildingID;
@property (nonatomic, strong) NSString * raBuildingName;
@property (nonatomic, strong) NSString * rabuildingLang;
@property (nonatomic, strong) NSString * raPriority;
@property (nonatomic, strong) NSString * raGuestName;
@property (nonatomic, strong) NSString * raVIP;
@property (nonatomic, strong) NSString * raExpectedCleaningTime;
@property (nonatomic, strong) NSString * raGuestPreference;
@property (nonatomic, strong) NSNumber * raPrioritySortOrder;
@property (nonatomic, strong) NSNumber * raInspectedStatus;
@property (nonatomic, strong) NSString * raLastModified;
@property (nonatomic, strong) NSString * raCleanStartTm;
@property (nonatomic, strong) NSString * raCleanEndTm;
@property (nonatomic, strong) NSNumber * raExpectedInspectTime;
@property (nonatomic, strong) NSString * raAssignedTime;
@property (nonatomic, strong) NSString * raGuestFirstName;
@property (nonatomic, strong) NSString * raGuestLastName;
@property (nonatomic, strong) NSString * raHotelID;
@property (nonatomic, strong) NSString * raInspectStartTm;
@property (nonatomic, strong) NSString * raInspectEndTm;
@property (nonatomic, strong) NSString * raAdditionalJob;
@property (nonatomic, strong) NSString * raRemark;
@property (nonatomic, strong) NSString * raInspectedScore;
@property (nonatomic, strong) NSNumber * raGuestProfileID;
@property (nonatomic, strong) NSNumber * raTotalCleaningTime;
@property (nonatomic, strong) NSNumber * raToTalInspectionTime;
@property (nonatomic, strong) NSString * raFullName;
@property (nonatomic, strong) NSString * raLastRoomCleaningDate;
@property (nonatomic, strong) NSString * raGuestArrivalTime;
@property (nonatomic, strong) NSString * raGuestDepartureTime;
@property (nonatomic, strong) NSNumber * raKindOfRoom;
@property (nonatomic, strong) NSNumber * raIsMockRoom;
@property (nonatomic, strong) NSNumber * raCleaningCredit;
@property (nonatomic, strong) NSString * raChecklistRemarks;
@end
@interface Web_x0020_Service_ElementGetAdditionalJobByRoomNoResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetAdditionalJobByRoomNoResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_AdditionalJobOfRoom * GetAdditionalJobByRoomNoResult;
@end
@interface Web_x0020_Service_ElementGetLaundryItemListResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetLaundryItemListResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_LdryItmList * GetLaundryItemListResult;
@end
@interface Web_x0020_Service_ElementAssignRoom : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementAssignRoom *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intHousekeeperID;
@property (nonatomic, strong) NSNumber * intSupervisorID;
@property (nonatomic, strong) NSNumber * intDutyAssignID;
@property (nonatomic, strong) NSString * strAssignDateTime;
@property (nonatomic, strong) NSString * strRemark;
@end
@interface Web_x0020_Service_AdHocMessageCounter : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_AdHocMessageCounter *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSNumber * intSent;
@property (nonatomic, strong) NSNumber * intReadReceived;
@property (nonatomic, strong) NSNumber * intUnreadReceive;
@end
@interface Web_x0020_Service_ElementGetCleaningStatusListResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetCleaningStatusListResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_CleaningStatusList * GetCleaningStatusListResult;
@end
@interface Web_x0020_Service_LaundryInstructionList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_LaundryInstructionList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * ldrySpcInstructionID;
@property (nonatomic, strong) NSString * ldrySpcInstructionName;
@property (nonatomic, strong) NSString * ldrySpcInstructionLang;
@property (nonatomic, strong) NSString * ldrySpcLastModified;
@end
@interface Web_x0020_Service_ElementGetSupervisorFindRoomAssignmentListsResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetSupervisorFindRoomAssignmentListsResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_RoomAssignmentList * GetSupervisorFindRoomAssignmentListsResult;
@end
@interface Web_x0020_Service_ArrayOfMinibarItemList : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_PostUpdateDeviceTokenRequest : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_PostUpdateDeviceTokenRequest *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSString * UserName;
@property (nonatomic, strong) NSString * Password;
@property (nonatomic, strong) NSString * DeviceToken;
@property (nonatomic, strong) NSString * DeviceTokenType;
@end
@interface Web_x0020_Service_UniqueRoom : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_UniqueRoom *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSString * strRoomNumber;
@property (nonatomic, strong) NSNumber * intHotelId;
@end
@interface Web_x0020_Service_ElementGetMessageCategoryList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetMessageCategoryList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSNumber * intHotelID;
@property (nonatomic, strong) NSString * strLastModified;
@end
@interface Web_x0020_Service_Hotel : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_Hotel *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSString * htID;
@property (nonatomic, strong) NSString * htName;
@property (nonatomic, strong) NSString * htLang;
@property (nonatomic, strong) NSData * htLogo;
@property (nonatomic, strong) NSString * htLastModified;
@end
@interface Web_x0020_Service_ArrayOfCleaningStatus : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_ElementGetUserDetailsRoutine : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetUserDetailsRoutine *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSString * UserID;
@property (nonatomic, strong) NSString * HTID;
@property (nonatomic, strong) NSString * TypeID;
@end
@interface Web_x0020_Service_AdditionalJobFloorList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_AdditionalJobFloorList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSArray * AdditionalJobFloorList;
@end
@interface Web_x0020_Service_ArrayOfLocationList : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_EngineeringItemList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_EngineeringItemList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * engID;
@property (nonatomic, strong) NSString * engName;
@property (nonatomic, strong) NSString * engLang;
@property (nonatomic, strong) NSNumber * engCategoryID;
@property (nonatomic, strong) NSNumber * engRankInList;
@property (nonatomic, strong) NSString * engLastModified;
@property (nonatomic, strong) NSString * engServiceItemCode;
@end
@interface Web_x0020_Service_ElementGetZoneRoomList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetZoneRoomList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intHotelID;
@property (nonatomic, strong) NSString * strLastModified;
@end
@interface Web_x0020_Service_ElementGetZoneRoomListResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetZoneRoomListResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ZnRmLst * GetZoneRoomListResult;
@end
@interface Web_x0020_Service_LaundryItemList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_LaundryItemList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * ldryItmID;
@property (nonatomic, strong) NSString * ldryItmName;
@property (nonatomic, strong) NSString * ldryItmLang;
@property (nonatomic, strong) NSNumber * ldryItmGender;
@property (nonatomic, strong) NSString * ldryItmLastModified;
@property (nonatomic, strong) NSData * ldryPicture;
@end
@interface Web_x0020_Service_ElementPostingPanicAlertResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementPostingPanicAlertResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_PanicAlertConfig * PostingPanicAlertResult;
@end
@interface Web_x0020_Service_ElementGetPanicAlertConfig : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetPanicAlertConfig *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSNumber * intHotelID;
@property (nonatomic, strong) NSString * strLastModified;
@end
@interface Web_x0020_Service_ElementGetOtherActivityAssignmentRoutine : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetOtherActivityAssignmentRoutine *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSString * UserID;
@property (nonatomic, strong) NSString * PropertyID;
@property (nonatomic, strong) NSString * BuildingID;
@property (nonatomic, strong) NSString * DateTime;
@end
@interface Web_x0020_Service_GetFindFloorDetailsRequest : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_GetFindFloorDetailsRequest *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * UserID;
@property (nonatomic, strong) NSNumber * BuildingID;
@property (nonatomic, strong) NSNumber * HTID;
@property (nonatomic, strong) NSNumber * FilterType;
@property (nonatomic, strong) NSString * GeneralFilter;
@end
@interface Web_x0020_Service_ElementUpdateRoomAssignment : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementUpdateRoomAssignment *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSNumber * intRoomAssignID;
@property (nonatomic, strong) NSNumber * intRoomStatusID;
@property (nonatomic, strong) NSNumber * intCleaningStatusID;
@property (nonatomic, strong) NSString * strCleanStartTm;
@property (nonatomic, strong) NSString * strCleanEndTm;
@property (nonatomic, strong) NSString * raRemark;
@property (nonatomic, strong) NSString * intTotalCleaningTime;
@property (nonatomic, strong) NSString * strPauseTime;
@end
@interface Web_x0020_Service_UserDetail : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_UserDetail *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * usrID;
@property (nonatomic, strong) NSString * usrName;
@property (nonatomic, strong) NSString * usrFullName;
@property (nonatomic, strong) NSString * usrLang;
@property (nonatomic, strong) NSString * usrPassword;
@property (nonatomic, strong) NSString * usrTitle;
@property (nonatomic, strong) NSString * usrIsSupervisor;
@property (nonatomic, strong) NSString * usrIsAdmin;
@property (nonatomic, strong) NSString * usrDeptID;
@property (nonatomic, strong) NSString * usrDept;
@property (nonatomic, strong) NSString * usrLangCode;
@property (nonatomic, strong) NSString * usrHotelID;
@property (nonatomic, strong) NSString * usrIsActive;
@property (nonatomic, strong) NSString * usrLastModified;
@property (nonatomic, strong) NSString * usrInitial;
@property (nonatomic, strong) NSString * intAllowReorderRoomAssignment;
@property (nonatomic, strong) NSString * intAllowBlockVacantRoom;
@property (nonatomic, strong) NSNumber * usrBuilding;
@end
@interface Web_x0020_Service_ElementGetNoOfAdHocMessageRoutine : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetNoOfAdHocMessageRoutine *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSString * UserID;
@property (nonatomic, strong) NSString * PropertyID;
@property (nonatomic, strong) NSString * GroupMessageID;
@property (nonatomic, strong) NSString * LastModifiedDateTime;
@end
@interface Web_x0020_Service_ArrayOfAdditionalJob : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_AdditionalJobRoomDetail : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_AdditionalJobRoomDetail *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSArray * AdditionalJobList;
@end
@interface Web_x0020_Service_ElementPostUnassignRoomRoutine : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementPostUnassignRoomRoutine *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSString * UserID;
@property (nonatomic, strong) NSString * DutyAssignmentID;
@property (nonatomic, strong) NSString * PropertyID;
@property (nonatomic, strong) NSString * CleaningStatus;
@property (nonatomic, strong) NSString * Remark;
@end
@interface Web_x0020_Service_ArrayOfUserDetail : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_UserList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_UserList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSArray * UserDetailList;
@end
@interface Web_x0020_Service_ElementPostMessageAttachPhoto : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementPostMessageAttachPhoto *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSData * binPhoto;
@property (nonatomic, strong) NSNumber * intGrpMsgID;
@end
@interface Web_x0020_Service_CheckListItem : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_CheckListItem *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * cliID;
@property (nonatomic, strong) NSString * cliName;
@property (nonatomic, strong) NSString * cliLang;
@property (nonatomic, strong) NSNumber * cliChecklistID;
@property (nonatomic, strong) NSNumber * cliCategoryID;
@property (nonatomic, strong) NSNumber * cliRatingValue;
@property (nonatomic, strong) NSNumber * cliSortOrder;
@property (nonatomic, strong) NSString * cliLastModified;
@property (nonatomic, strong) NSNumber * cliMandatoryPass;
@end
@interface Web_x0020_Service_RoomTypeInventoryResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_RoomTypeInventoryResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatuses;
@property (nonatomic, strong) NSArray * RoomTypeInventoryListing;
@end
@interface Web_x0020_Service_ElementUpdateAdditionalJobRemarkResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementUpdateAdditionalJobRemarkResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * UpdateAdditionalJobRemarkResult;
@end
@interface Web_x0020_Service_AttendantList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_AttendantList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSString * atdName;
@property (nonatomic, strong) NSString * atdFullName;
@property (nonatomic, strong) NSNumber * atdID;
@property (nonatomic, strong) NSNumber * atdCurrentStatus;
@property (nonatomic, strong) NSString * atdRoomNo;
@property (nonatomic, strong) NSNumber * atdRoomAssignID;
@property (nonatomic, strong) NSString * atdCurrentScheduleTime;
@property (nonatomic, strong) NSString * atdStatusName;
@property (nonatomic, strong) NSString * atdStatusLang;
@end
@interface Web_x0020_Service_ElementGetMinibarCategoryList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetMinibarCategoryList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSNumber * intHotelID;
@property (nonatomic, strong) NSString * strLastModified;
@end
@interface Web_x0020_Service_ArrayOfLaundryTypeList : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_LdryTypList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_LdryTypList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSArray * LaundryTypList;
@end
@interface Web_x0020_Service_ElementGetLocationListResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetLocationListResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_LocList * GetLocationListResult;
@end
@interface Web_x0020_Service_ElementGetAllChecklist : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetAllChecklist *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSNumber * intHotelID;
@property (nonatomic, strong) NSString * strLastModified;
@end
@interface Web_x0020_Service_OtherActivityLocation : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_OtherActivityLocation *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * LocationID;
@property (nonatomic, strong) NSString * LocationName;
@property (nonatomic, strong) NSString * LocationNameLang;
@property (nonatomic, strong) NSString * LocationCode;
@property (nonatomic, strong) NSString * LocationDesc;
@property (nonatomic, strong) NSString * LocationDescLang;
@property (nonatomic, strong) NSString * LocationHotelID;
@property (nonatomic, strong) NSString * LocationLastModified;
@end
@interface Web_x0020_Service_ElementPostAmenitiesItem : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementPostAmenitiesItem *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSNumber * intRoomAssignID;
@property (nonatomic, strong) NSNumber * intAmenitiesItemID;
@property (nonatomic, strong) NSNumber * intNewQuantity;
@property (nonatomic, strong) NSString * strTransactionTime;
@property (nonatomic, strong) Web_x0020_Service_UniqueRoom * urRoom;
@end
@interface Web_x0020_Service_ElementGetLanguageList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetLanguageList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSString * strLastModified;
@end
@interface Web_x0020_Service_ArrayOfAmenitiesItem : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_ArrayOfLaundryList : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_ElementPostUnassignRoomRoutineResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementPostUnassignRoomRoutineResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * PostUnassignRoomRoutineResult;
@end
@interface Web_x0020_Service_GuestOfRoom : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_GuestOfRoom *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSString * roomID;
@property (nonatomic, strong) NSString * gpTitle;
@property (nonatomic, strong) NSString * gpName;
@property (nonatomic, strong) NSString * gpHousekeepingName;
@property (nonatomic, strong) NSString * gpLangPref;
@property (nonatomic, strong) NSString * gpCheckInDt;
@property (nonatomic, strong) NSString * gpCheckOutDt;
@property (nonatomic, strong) NSString * gpPreferenceDesc;
@property (nonatomic, strong) NSString * gpVIP;
@property (nonatomic, strong) NSString * gpLastModified;
@property (nonatomic, strong) NSData * gpPhoto;
@property (nonatomic, strong) NSString * gpSpecialService;
@end
@interface Web_x0020_Service_ArrayOfRoomType : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_RoomTypeList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_RoomTypeList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSArray * RoomTypeList;
@end
@interface Web_x0020_Service_ArrayOfAmenitiesItemList : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_AmenitiesItmList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_AmenitiesItmList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSArray * AmenitiesItm;
@end
@interface Web_x0020_Service_ElementGetRoomDetailResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetRoomDetailResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_RoomDetail * GetRoomDetailResult;
@end
@interface Web_x0020_Service_NumberOfGuestInfo : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_NumberOfGuestInfo *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSNumber * NumberOfGuest;
@end
@interface Web_x0020_Service_ElementGetStillLoggedIn : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetStillLoggedIn *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSString * strDeviceNo;
@property (nonatomic, strong) NSNumber * intDeviceType;
@end
@interface Web_x0020_Service_ElementGetRoomTypeListResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetRoomTypeListResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_RoomTypeList * GetRoomTypeListResult;
@end
@interface Web_x0020_Service_ElementGetPostingHistoryRoutineResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetPostingHistoryRoutineResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_GetPostingHistoryResponse * GetPostingHistoryRoutineResult;
@end
@interface Web_x0020_Service_ElementGetChecklistCategoryList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetChecklistCategoryList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSNumber * intHotelID;
@property (nonatomic, strong) NSNumber * intCheckListID;
@property (nonatomic, strong) NSString * strLastModified;
@end
@interface Web_x0020_Service_ArrayOfMessageItemList2 : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_ElementUpdateRoomDetails : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementUpdateRoomDetails *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUserID;
@property (nonatomic, strong) NSString * strRoomNo;
@property (nonatomic, strong) NSNumber * intHTID;
@property (nonatomic, strong) NSNumber * intUpdateType;
@property (nonatomic, strong) NSNumber * intStatusID;
@property (nonatomic, strong) NSNumber * intCleaningStatusID;
@end
@interface Web_x0020_Service_ElementGetEngineeringCategoryList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetEngineeringCategoryList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSString * strLastModified;
@property (nonatomic, strong) NSString * strHotel_ID;
@end
@interface Web_x0020_Service_Version : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_Version *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSString * WebServicesVersionNo;
@property (nonatomic, strong) NSString * MinimumAndroidVersionNos;
@property (nonatomic, strong) NSString * MinimumiOSVersionNos;
@end
@interface Web_x0020_Service_ElementGetDiscrepantRoomStatusResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetDiscrepantRoomStatusResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_GetDiscrepantRoomStatusList * GetDiscrepantRoomStatusResult;
@end
@interface Web_x0020_Service_ArrayOfMinibarCategoryList : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_PostedItem : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_PostedItem *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * ID;
@end
@interface Web_x0020_Service_ArrayOfRoomAssign : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_RoomAssignmentList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_RoomAssignmentList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSArray * RoomAssignmentList;
@end
@interface Web_x0020_Service_ElementStopAdditionalJob : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementStopAdditionalJob *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * ondayAdditionalJobId;
@property (nonatomic, strong) NSString * stopDateTime;
@end
@interface Web_x0020_Service_ElementAssignRoomResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementAssignRoomResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * AssignRoomResult;
@end
@interface Web_x0020_Service_ElementLogInvalidStartRoom : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementLogInvalidStartRoom *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intPrevious_DAID;
@property (nonatomic, strong) NSNumber * intNew_DAID;
@property (nonatomic, strong) NSString * strPrevious_RoomNo;
@property (nonatomic, strong) NSString * strNew_RoomNo;
@property (nonatomic, strong) NSString * strUser_ID;
@property (nonatomic, strong) NSString * strUser_Name;
@end
@interface Web_x0020_Service_ElementPostEngineeringCase : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementPostEngineeringCase *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSString * pSender;
@property (nonatomic, strong) NSString * pAction;
@property (nonatomic, strong) NSString * pUsername;
@property (nonatomic, strong) NSString * pPassword;
@property (nonatomic, strong) NSString * pPropertyID;
@property (nonatomic, strong) NSString * pServiceType;
@property (nonatomic, strong) NSString * pRoomNo;
@property (nonatomic, strong) NSString * pItemCode;
@property (nonatomic, strong) NSString * pLocationCode;
@property (nonatomic, strong) NSString * pRemark;
@end
@interface Web_x0020_Service_LostNFoundCat : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_LostNFoundCat *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSArray * LostAndFoundCategory;
@end
@interface Web_x0020_Service_ElementGetMinibarItemListResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetMinibarItemListResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_MinibarItmList * GetMinibarItemListResult;
@end
@interface Web_x0020_Service_ElementGetChecklistRoomTypeResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetChecklistRoomTypeResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ChkListRmTyp * GetChecklistRoomTypeResult;
@end
@interface Web_x0020_Service_ElementGetFloorListResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetFloorListResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_FloorList * GetFloorListResult;
@end
@interface Web_x0020_Service_ElementGetFindRoomAssignmentLists : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetFindRoomAssignmentLists *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intSupervisorID;
@property (nonatomic, strong) NSNumber * intStatusID;
@property (nonatomic, strong) NSNumber * intFilterType;
@property (nonatomic, strong) NSString * strLastModified;
@property (nonatomic, strong) NSString * strHotel_ID;
@end
@interface Web_x0020_Service_RoomAssignmentDetails : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_RoomAssignmentDetails *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * ID;
@property (nonatomic, strong) NSString * RoomNo;
@property (nonatomic, strong) NSString * FloorID;
@property (nonatomic, strong) NSNumber * BuildingID;
@property (nonatomic, strong) NSString * BuildingName;
@property (nonatomic, strong) NSString * BuildingLang;
@property (nonatomic, strong) NSNumber * HTID;
@property (nonatomic, strong) NSNumber * RoomStatusID;
@property (nonatomic, strong) NSNumber * CleaningStatusID;
@property (nonatomic, strong) NSNumber * RoomTypeID;
@property (nonatomic, strong) NSNumber * HousekeeperID;
@property (nonatomic, strong) NSString * HousekeeperFullname;
@property (nonatomic, strong) NSString * AssignedTime;
@property (nonatomic, strong) NSString * CleaningStartTime;
@property (nonatomic, strong) NSString * CleaningEndTime;
@property (nonatomic, strong) NSString * ExpectedCleaningTime;
@property (nonatomic, strong) NSNumber * TotalCleaningTime;
@property (nonatomic, strong) NSNumber * InspectionStatusID;
@property (nonatomic, strong) NSString * InspectionStartTime;
@property (nonatomic, strong) NSString * InspectionEndTime;
@property (nonatomic, strong) NSString * InspectionScore;
@property (nonatomic, strong) NSNumber * ExpectedInspectionTime;
@property (nonatomic, strong) NSNumber * TotalInspectionTime;
@property (nonatomic, strong) NSString * LastRoomCleaningDate;
@property (nonatomic, strong) NSString * LastModified;
@property (nonatomic, strong) NSNumber * KindOfRoom;
@property (nonatomic, strong) NSNumber * IsMockRoom;
@property (nonatomic, strong) NSNumber * GuestProfileID;
@property (nonatomic, strong) NSString * GuestFirstName;
@property (nonatomic, strong) NSString * GuestLastName;
@property (nonatomic, strong) NSString * GuestVIPCode;
@property (nonatomic, strong) NSString * GuestArrivalTime;
@property (nonatomic, strong) NSString * GuestDepartureTime;
@property (nonatomic, strong) NSString * GuestPreference;
@property (nonatomic, strong) NSString * GuestName;
@property (nonatomic, strong) NSString * Priority;
@property (nonatomic, strong) NSString * Remark;
@property (nonatomic, strong) NSNumber * PrioritySortOrder;
@property (nonatomic, strong) NSNumber * CleaningCredit;
@property (nonatomic, strong) NSString * ChecklistRemarks;
@end
@interface Web_x0020_Service_ElementUpdatePopupMsgStatusResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementUpdatePopupMsgStatusResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * UpdatePopupMsgStatusResult;
@end
@interface Web_x0020_Service_ArrayOfPostChecklistItem : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_ElementPostLaundryItemResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementPostLaundryItemResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * PostLaundryItemResult;
@end
@interface Web_x0020_Service_ElementUpdateCleaningStatusResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementUpdateCleaningStatusResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * UpdateCleaningStatusResult;
@end
@interface Web_x0020_Service_ArrayOfRoomStatus : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_RoomStatusList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_RoomStatusList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSArray * RoomStatusList;
@end
@interface Web_x0020_Service_ArrayOfGuestInformation : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_AmenitiesCategoryList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_AmenitiesCategoryList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * amcID;
@property (nonatomic, strong) NSString * amcName;
@property (nonatomic, strong) NSString * amcLang;
@property (nonatomic, strong) NSData * amcPicture;
@property (nonatomic, strong) NSString * amcLastModified;
@end
@interface Web_x0020_Service_MinibarItmList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_MinibarItmList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSArray * MinibarItm;
@end
@interface Web_x0020_Service_ArrayOfRoomSetupList : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_RoomStpList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_RoomStpList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSArray * RoomSetupList;
@end
@interface Web_x0020_Service_ElementGetCheckListItemScoreForMaidResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetCheckListItemScoreForMaidResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ChkListScoreForMaid * GetCheckListItemScoreForMaidResult;
@end
@interface Web_x0020_Service_UpdateAdHocMessageResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_UpdateAdHocMessageResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@end
@interface Web_x0020_Service_ElementGetReceivedAdHocMessageResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetReceivedAdHocMessageResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_AdHocMsgList2 * GetReceivedAdHocMessageResult;
@end
@interface Web_x0020_Service_ElementUpdateAdditionalJobStatusResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementUpdateAdditionalJobStatusResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * UpdateAdditionalJobStatusResult;
@end
@interface Web_x0020_Service_ElementFindAttendantResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementFindAttendantResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_AttdantList * FindAttendantResult;
@end
@interface Web_x0020_Service_FloorDetails : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_FloorDetails *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSString * ID;
@property (nonatomic, strong) NSString * Name;
@property (nonatomic, strong) NSString * Lang;
@property (nonatomic, strong) NSNumber * BuildingID;
@end
@interface Web_x0020_Service_ElementGetLFItemList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetLFItemList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSString * strLastModified;
@property (nonatomic, strong) NSString * strHotel_ID;
@end
@interface Web_x0020_Service_ArrayOfInspectionStatusList : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_AdditionalJobCount : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_AdditionalJobCount *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSNumber * AdditionalJobCount;
@end
@interface Web_x0020_Service_ElementPostUpdateOtherActivityStatusRoutine : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementPostUpdateOtherActivityStatusRoutine *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSString * UserID;
@property (nonatomic, strong) NSString * ActivityID;
@property (nonatomic, strong) NSString * StatusID;
@property (nonatomic, strong) NSString * Time;
@property (nonatomic, strong) NSString * Remark;
@property (nonatomic, strong) NSString * DurationSpent;
@end
@interface Web_x0020_Service_ArrayOfAdditionalJobRoom : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_AdditionalJobRoomList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_AdditionalJobRoomList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSArray * AdditionalJobRoomList;
@end
@interface Web_x0020_Service_AdditionalJobFloor : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_AdditionalJobFloor *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * floorID;
@property (nonatomic, strong) NSString * floorName;
@property (nonatomic, strong) NSString * floorLang;
@property (nonatomic, strong) NSNumber * numberOfJob;
@end
@interface Web_x0020_Service_ElementAuthenticateUser : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementAuthenticateUser *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSString * strUsrName;
@property (nonatomic, strong) NSString * strUsrPassword;
@property (nonatomic, strong) NSString * strDeviceNo;
@property (nonatomic, strong) NSNumber * intDeviceType;
@property (nonatomic, strong) NSString * strLastModified;
@property (nonatomic, strong) NSString * intHotelID;
@property (nonatomic, strong) NSString * strUsrLoggedInLanguage;
@end
@interface Web_x0020_Service_LanguageItem : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_LanguageItem *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSString * lgCode;
@property (nonatomic, strong) NSString * lgName;
@property (nonatomic, strong) NSString * lgXmlUrl;
@property (nonatomic, strong) NSString * lgLastModified;
@property (nonatomic, strong) NSString * lgIsActive;
@property (nonatomic, strong) NSString * lgCurrencySymbol;
@property (nonatomic, strong) NSString * lgCurrencyNoDigitAfterDecimal;
@end
@interface Web_x0020_Service_MessageItemList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_MessageItemList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * msgItmID;
@property (nonatomic, strong) NSNumber * msgItmSeq;
@property (nonatomic, strong) NSNumber * msgItmCatID;
@property (nonatomic, strong) NSString * msgItmName;
@property (nonatomic, strong) NSString * msgItmDesc;
@property (nonatomic, strong) NSData * msgItmPhoto;
@property (nonatomic, strong) NSString * msgItmLastModified;
@end
@interface Web_x0020_Service_ElementGetRoomRemarksResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetRoomRemarksResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_GetRoomRemarksList * GetRoomRemarksResult;
@end
@interface Web_x0020_Service_ElementPostMessageResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementPostMessageResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_GrpAdHocList * PostMessageResult;
@end
@interface Web_x0020_Service_LdrySrvList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_LdrySrvList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSArray * LaundrySrvList;
@end
@interface Web_x0020_Service_PostPostingHistoryResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_PostPostingHistoryResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@end
@interface Web_x0020_Service_ElementGetReleaseRoomsList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetReleaseRoomsList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSNumber * intHTID;
@end
@interface Web_x0020_Service_RoomSectionList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_RoomSectionList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * rsetID;
@property (nonatomic, strong) NSString * rsetName;
@property (nonatomic, strong) NSString * rsetLang;
@property (nonatomic, strong) NSNumber * rsetRoomTypeID;
@property (nonatomic, strong) NSData * rsetPicture;
@property (nonatomic, strong) NSString * rsetLastModified;
@property (nonatomic, strong) NSNumber * sgc_CategoryID;
@end
@interface Web_x0020_Service_ElementPostAmenitiesItemsList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementPostAmenitiesItemsList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUserId;
@property (nonatomic, strong) NSArray * amenitiesItemsList;
@end
@interface Web_x0020_Service_ElementGetMessageCategoryListResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetMessageCategoryListResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_MsgCatList * GetMessageCategoryListResult;
@end
@interface Web_x0020_Service_ElementGetEngineeringItem : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetEngineeringItem *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSNumber * intHotelID;
@property (nonatomic, strong) NSString * strLastModified;
@end
@interface Web_x0020_Service_ElementGetMessageCategoryList2 : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetMessageCategoryList2 *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSNumber * intHotelID;
@property (nonatomic, strong) NSString * strLastModified;
@end
@interface Web_x0020_Service_ElementGetOtherActivityAssignmentRoutineResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetOtherActivityAssignmentRoutineResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_OtherActivityDetailsList * GetOtherActivityAssignmentRoutineResult;
@end
@interface Web_x0020_Service_ElementPosteCnJob : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementPosteCnJob *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSString * strSender;
@property (nonatomic, strong) NSString * strAction;
@property (nonatomic, strong) NSString * strUsername;
@property (nonatomic, strong) NSString * strPassword;
@property (nonatomic, strong) NSString * strPropertyID;
@property (nonatomic, strong) NSString * strServiceType;
@property (nonatomic, strong) NSString * strRoomNo;
@property (nonatomic, strong) NSString * strItemCode;
@property (nonatomic, strong) NSString * strLocationCode;
@property (nonatomic, strong) NSString * strRemark;
@property (nonatomic, strong) NSData * binImage;
@property (nonatomic, strong) NSString * strImageExtension;
@end
@interface Web_x0020_Service_ElementGetAdditionalJobDetail : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetAdditionalJobDetail *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * ondayAdditionalJobId;
@end
@interface Web_x0020_Service_ElementGetSupervisorFindFloorList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetSupervisorFindFloorList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intSupervisorID;
@property (nonatomic, strong) NSNumber * intBuildingID;
@property (nonatomic, strong) NSNumber * intHotelID;
@property (nonatomic, strong) NSNumber * intStatusID;
@property (nonatomic, strong) NSNumber * intFilterType;
@end
@interface Web_x0020_Service_ElementPostPostingHistoryRoutineResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementPostPostingHistoryRoutineResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_PostPostingHistoryResponse * PostPostingHistoryRoutineResult;
@end
@interface Web_x0020_Service_ElementPostLaundryItem : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementPostLaundryItem *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSNumber * intLaundryOrderID;
@property (nonatomic, strong) NSNumber * intLaundryItemID;
@property (nonatomic, strong) NSNumber * intLaundryCategoryID;
@property (nonatomic, strong) NSNumber * intLaundryServiceID;
@property (nonatomic, strong) NSNumber * intQuantity;
@property (nonatomic, strong) NSString * dblTotalPrice;
@property (nonatomic, strong) NSString * strTransactionTime;
@end
@interface Web_x0020_Service_MessageTemplateList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_MessageTemplateList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * msgteID;
@property (nonatomic, strong) NSString * msgteName;
@property (nonatomic, strong) NSString * msgteContent;
@property (nonatomic, strong) NSString * msgtcLastModified;
@end
@interface Web_x0020_Service_EngCase : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_EngCase *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSNumber * ecID;
@end
@interface Web_x0020_Service_OtherActivityStatus : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_OtherActivityStatus *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * StatusID;
@property (nonatomic, strong) NSString * StatusName;
@property (nonatomic, strong) NSString * StatusLang;
@property (nonatomic, strong) NSData * StatusImage;
@property (nonatomic, strong) NSString * StatusLastModified;
@end
@interface Web_x0020_Service_ElementPostWSLog : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementPostWSLog *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSString * strMessage;
@property (nonatomic, strong) NSNumber * intMessageType;
@end
@interface Web_x0020_Service_ArrayOfZoneList : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_ZnLst : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ZnLst *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSArray * ZnLst;
@end
@interface Web_x0020_Service_ArrayOfMessageTemplateList : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_MsgTempList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_MsgTempList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSArray * MessageTemplateList;
@end
@interface Web_x0020_Service_ElementGetPostingHistoryRoutine : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetPostingHistoryRoutine *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSString * UserID;
@property (nonatomic, strong) NSString * RoomNo;
@property (nonatomic, strong) NSString * HTID;
@property (nonatomic, strong) NSString * ModuleID;
@property (nonatomic, strong) NSString * DateFilter;
@end
@interface Web_x0020_Service_Building : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_Building *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSString * bdID;
@property (nonatomic, strong) NSString * bdName;
@property (nonatomic, strong) NSString * bdLang;
@property (nonatomic, strong) NSString * bdHotelID;
@property (nonatomic, strong) NSString * bdLastModifed;
@end
@interface Web_x0020_Service_ElementGetLaundrySpecialInstructionList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetLaundrySpecialInstructionList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSString * strLastModified;
@end
@interface Web_x0020_Service_ElementGetOtherActivitiesLocationRoutine : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetOtherActivitiesLocationRoutine *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSString * UserID;
@property (nonatomic, strong) NSString * PropertyID;
@property (nonatomic, strong) NSString * LastModified;
@end
@interface Web_x0020_Service_ArrayOfLaundryItemList : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_ArrayOfJobTask : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_JobTaskList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_JobTaskList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSArray * JobTaskList;
@end
@interface Web_x0020_Service_MinibarCatList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_MinibarCatList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSArray * MinibarCat;
@end
@interface Web_x0020_Service_ProfileNote : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ProfileNote *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSString * NoteDescription;
@end
@interface Web_x0020_Service_ArrayOfInt : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_ArrayOfPostingHistoryDetails : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_GetPostingHistoryResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_GetPostingHistoryResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSArray * PostingHistoryListing;
@end
@interface Web_x0020_Service_SupervisorCheckList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_SupervisorCheckList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * cltID;
@property (nonatomic, strong) NSString * cltName;
@property (nonatomic, strong) NSString * cltLang;
@property (nonatomic, strong) NSNumber * cltChecklistType;
@property (nonatomic, strong) NSNumber * cltTotalPointPossible;
@property (nonatomic, strong) NSNumber * cltPassingScore;
@property (nonatomic, strong) NSString * cltLastModified;
@end
@interface Web_x0020_Service_ElementGetMessageItemList2 : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetMessageItemList2 *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSString * strLastModified;
@end
@interface Web_x0020_Service_ArrayOfEngineeringCategoryList : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_GetDiscrepantRoomStatusList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_GetDiscrepantRoomStatusList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSNumber * intDAID;
@property (nonatomic, strong) NSNumber * intCurrentRoomStatus;
@property (nonatomic, strong) NSNumber * intNewRoomStatus;
@end
@interface Web_x0020_Service_ElementPostUpdateDeviceTokenRoutine : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementPostUpdateDeviceTokenRoutine *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_PostUpdateDeviceTokenRequest * PostUpdateDeviceTokenRequest;
@end
@interface Web_x0020_Service_ElementPostPostingHistoryRoutine : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementPostPostingHistoryRoutine *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSArray * PostingHistoryListing;
@end
@interface Web_x0020_Service_ElementPostLostFoundResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementPostLostFoundResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_LostFnd * PostLostFoundResult;
@end
@interface Web_x0020_Service_ElementGetLinenItemList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetLinenItemList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSNumber * intHotelID;
@property (nonatomic, strong) NSString * intCategoryID;
@property (nonatomic, strong) NSString * strLastModified;
@end
@interface Web_x0020_Service_ElementUpdateRoomStatus : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementUpdateRoomStatus *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUserID;
@property (nonatomic, strong) NSString * strRoomNo;
@property (nonatomic, strong) NSNumber * intStatusID;
@property (nonatomic, strong) NSNumber * intCleaningStatusID;
@end
@interface Web_x0020_Service_MsgItmList2 : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_MsgItmList2 *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSArray * MessageItmList;
@end
@interface Web_x0020_Service_ElementGetLinenCategoryList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetLinenCategoryList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSNumber * intHotelID;
@property (nonatomic, strong) NSString * strLastModified;
@end
@interface Web_x0020_Service_ElementGetNewMessageResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetNewMessageResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_AdHocMsgList * GetNewMessageResult;
@end
@interface Web_x0020_Service_ElementPostUpdateDeviceTokenRoutineResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementPostUpdateDeviceTokenRoutineResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_PostUpdateDeviceTokenResponse * PostUpdateDeviceTokenRoutineResult;
@end
@interface Web_x0020_Service_InspectionStatusList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_InspectionStatusList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * inspID;
@property (nonatomic, strong) NSString * inspName;
@property (nonatomic, strong) NSString * inspLang;
@property (nonatomic, strong) NSData * inspPicture;
@property (nonatomic, strong) NSString * inspLastModified;
@end
@interface Web_x0020_Service_LocationList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_LocationList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * lcID;
@property (nonatomic, strong) NSString * lcCode;
@property (nonatomic, strong) NSString * lcDesc;
@property (nonatomic, strong) NSString * lcLang;
@property (nonatomic, strong) NSString * lcRoomNo;
@property (nonatomic, strong) NSString * lcType;
@property (nonatomic, strong) NSString * lcLastModified;
@end
@interface Web_x0020_Service_PostAdditionalJobStatusUpdateResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_PostAdditionalJobStatusUpdateResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@end
@interface Web_x0020_Service_ElementGetSecretsRoutine : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetSecretsRoutine *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_GetSecretsRequest * GetSecretsRequest;
@end
@interface Web_x0020_Service_ElementGetMessageCategoryList2Response : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetMessageCategoryList2Response *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_MsgCatList2 * GetMessageCategoryList2Result;
@end
@interface Web_x0020_Service_ElementUpdateCleaningStatus : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementUpdateCleaningStatus *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUserID;
@property (nonatomic, strong) NSNumber * intRoomAssignmentID;
@property (nonatomic, strong) NSNumber * intStatusID;
@end
@interface Web_x0020_Service_ElementPostLostAndFoundRoutineResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementPostLostAndFoundRoutineResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_PostLostAndFoundResponse * PostLostAndFoundRoutineResult;
@end
@interface Web_x0020_Service_ElementGetInspectionStatus : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetInspectionStatus *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSString * strLastModified;
@end
@interface Web_x0020_Service_ArrayOfProfileNoteType : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_ProfileNoteTypeList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ProfileNoteTypeList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSArray * ProfileNoteTypeList;
@end
@interface Web_x0020_Service_ElementGetSentAdHocMessage : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetSentAdHocMessage *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSString * intGrpMessageID;
@property (nonatomic, strong) NSNumber * intHotelID;
@property (nonatomic, strong) NSString * strLastModified;
@end
@interface Web_x0020_Service_LostFnd : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_LostFnd *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSNumber * lfID;
@end
@interface Web_x0020_Service_AdHocMessageList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_AdHocMessageList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * amsgID;
@property (nonatomic, strong) NSNumber * amsgGrpID;
@property (nonatomic, strong) NSString * amsgSendFrom;
@property (nonatomic, strong) NSString * amsgSendFromLang;
@property (nonatomic, strong) NSString * amsgTopic;
@property (nonatomic, strong) NSString * amsgContent;
@property (nonatomic, strong) NSString * amsgLastModified;
@property (nonatomic, strong) NSNumber * amsgPhotoAttached;
@end
@interface Web_x0020_Service_FloorList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_FloorList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSArray * FloorList;
@end
@interface Web_x0020_Service_ElementGetFindRoomAssignmentList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetFindRoomAssignmentList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intSupervisorID;
@property (nonatomic, strong) NSNumber * intRoomStatusID;
@property (nonatomic, strong) NSString * strLastModified;
@end
@interface Web_x0020_Service_ElementGetRoomSectionList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetRoomSectionList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intHotelID;
@property (nonatomic, strong) NSNumber * intRoomType;
@property (nonatomic, strong) NSNumber * intBuildingID;
@property (nonatomic, strong) NSString * strLastModified;
@end
@interface Web_x0020_Service_InspStatus : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_InspStatus *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSArray * InspStatusLst;
@end
@interface Web_x0020_Service_ElementPostLinenItemsResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementPostLinenItemsResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_PostLinenItemsList * PostLinenItemsResult;
@end
@interface Web_x0020_Service_ElementUpdateAdHocMessageStatusResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementUpdateAdHocMessageStatusResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * UpdateAdHocMessageStatusResult;
@end
@interface Web_x0020_Service_ElementUpdatePopupMsgStatus : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementUpdatePopupMsgStatus *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSString * intListPopupMsgID;
@property (nonatomic, strong) NSNumber * intStatus;
@end
@interface Web_x0020_Service_ElementGetMinibarItemList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetMinibarItemList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSNumber * intHotelID;
@property (nonatomic, strong) NSString * intCategoryID;
@property (nonatomic, strong) NSString * strLastModified;
@end
@interface Web_x0020_Service_ElementPostLaundryOrder : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementPostLaundryOrder *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSNumber * intRoomAssignID;
@property (nonatomic, strong) NSNumber * intLaundryServiceID;
@property (nonatomic, strong) NSNumber * intTotalQuantity;
@property (nonatomic, strong) NSNumber * dblServiceCharge;
@property (nonatomic, strong) NSNumber * dblSubTotal;
@property (nonatomic, strong) NSString * strTransactionTime;
@property (nonatomic, strong) NSNumber * intHotelID;
@property (nonatomic, strong) NSString * strLaundryRmk;
@end
@interface Web_x0020_Service_ElementGetRoomTypeByRoomNoRoutineResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetRoomTypeByRoomNoRoutineResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_RoomTypeList * GetRoomTypeByRoomNoRoutineResult;
@end
@interface Web_x0020_Service_ArrayOfBase64Binary : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_ElementPostLaundrySpecialInstructionResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementPostLaundrySpecialInstructionResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * PostLaundrySpecialInstructionResult;
@end
@interface Web_x0020_Service_LocList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_LocList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSArray * LocationList;
@end
@interface Web_x0020_Service_ElementGetChecklistCategoryListResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetChecklistCategoryListResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ChkListCategoryLst * GetChecklistCategoryListResult;
@end
@interface Web_x0020_Service_RoomType : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_RoomType *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * rtID;
@property (nonatomic, strong) NSString * rtName;
@property (nonatomic, strong) NSString * rtLang;
@property (nonatomic, strong) NSString * rtLastModifed;
@end
@interface Web_x0020_Service_AmenitiesItem : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_AmenitiesItem *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intDutyAssignmentId;
@property (nonatomic, strong) Web_x0020_Service_UniqueRoom * urRoom;
@property (nonatomic, strong) NSNumber * intAmenitiesItemId;
@property (nonatomic, strong) NSNumber * intNewQuantity;
@property (nonatomic, strong) NSString * strTransactionTime;
@end
@interface Web_x0020_Service_ElementGetGuestInformationResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetGuestInformationResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_GuestInformationResponse * GetGuestInformationResult;
@end
@interface Web_x0020_Service_ElementGetNewMessage : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetNewMessage *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSString * intGrpMessageID;
@property (nonatomic, strong) NSNumber * intHotelID;
@property (nonatomic, strong) NSString * strLastModified;
@end
@interface Web_x0020_Service_ReleaseRoomList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ReleaseRoomList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSArray * ReleaseRoomsList;
@end
@interface Web_x0020_Service_ElementAuthenticateUserResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementAuthenticateUserResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_EHSKPUser * AuthenticateUserResult;
@end
@interface Web_x0020_Service_ElementGetSentAdHocMessageResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetSentAdHocMessageResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_AdHocMsgList2 * GetSentAdHocMessageResult;
@end
@interface Web_x0020_Service_PostLostAndFoundResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_PostLostAndFoundResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@end
@interface Web_x0020_Service_ArrayOfUserDetailsReponseDetails : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_GetUserDetailsResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_GetUserDetailsResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSArray * UserDetailsReponseListing;
@end
@interface Web_x0020_Service_LostAndFoundItemTypeList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_LostAndFoundItemTypeList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * lfitID;
@property (nonatomic, strong) NSString * lfitName;
@property (nonatomic, strong) NSString * lfitLang;
@property (nonatomic, strong) NSString * lfcLastModified;
@property (nonatomic, strong) NSData * lfcPicture;
@property (nonatomic, strong) NSNumber * lfitCategoryID;
@end
@interface Web_x0020_Service_ArrayOfLaundryItmPriceList : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_LdryItmPriceList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_LdryItmPriceList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSArray * LdryItmPriceList;
@end
@interface Web_x0020_Service_RoomSetupList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_RoomSetupList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * rsgID;
@property (nonatomic, strong) NSNumber * rsgRoomSectionID;
@property (nonatomic, strong) NSString * rsgRoomSectionName;
@property (nonatomic, strong) NSString * rsgRoomSectionLang;
@property (nonatomic, strong) NSString * rsgDesc;
@property (nonatomic, strong) NSString * rsgLang;
@property (nonatomic, strong) NSString * rsgLastModified;
@property (nonatomic, strong) NSData * rsgPicture;
@property (nonatomic, strong) NSData * rsgImageUrl;
@property (nonatomic, strong) NSString * sgc_Name;
@property (nonatomic, strong) NSString * sgc_Lang;
@property (nonatomic, strong) NSArray * rsgImages;
@end
@interface Web_x0020_Service_RoomDetailsRequestDetails : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_RoomDetailsRequestDetails *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSString * RoomNo;
@property (nonatomic, strong) NSString * HotelID;
@end
@interface Web_x0020_Service_PostingLinenItemsList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_PostingLinenItemsList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSArray * LinenItems;
@end
@interface Web_x0020_Service_ElementGetBuildingListResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetBuildingListResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_BuildingList * GetBuildingListResult;
@end
@interface Web_x0020_Service_ElementRemoveLaundryItemResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementRemoveLaundryItemResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * RemoveLaundryItemResult;
@end
@interface Web_x0020_Service_ElementFindRoomByRoomNoResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementFindRoomByRoomNoResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_RoomAssignmentList * FindRoomByRoomNoResult;
@end
@interface Web_x0020_Service_ArrayOfMessageUpdateDetails : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_ElementPostUpdateAdHocMessageRoutine : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementPostUpdateAdHocMessageRoutine *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSArray * MessageUpdateListing;
@end
@interface Web_x0020_Service_PostPhysicalCheck : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_PostPhysicalCheck *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatuses;
@property (nonatomic, strong) NSArray * FloorInfo;
@end
@interface Web_x0020_Service_ElementUpdateRoomDetailsResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementUpdateRoomDetailsResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * UpdateRoomDetailsResult;
@end
@interface Web_x0020_Service_ElementGetGuestInfoByRoomIDResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetGuestInfoByRoomIDResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_GuestOfRoomInfo * GetGuestInfoByRoomIDResult;
@end
@interface Web_x0020_Service_ElementPostMinibarItemResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementPostMinibarItemResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_PostMinibarList * PostMinibarItemResult;
@end
@interface Web_x0020_Service_ElementGetRoomSectionListResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetRoomSectionListResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_RmSecList * GetRoomSectionListResult;
@end
@interface Web_x0020_Service_Guest : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_Guest *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSString * raID;
@property (nonatomic, strong) NSString * gpTitle;
@property (nonatomic, strong) NSString * gpName;
@property (nonatomic, strong) NSString * gpHousekeepingName;
@property (nonatomic, strong) NSString * gpLangPref;
@property (nonatomic, strong) NSString * gpCheckInDt;
@property (nonatomic, strong) NSString * gpCheckOutDt;
@property (nonatomic, strong) NSString * gpPreferenceDesc;
@property (nonatomic, strong) NSString * gpVIP;
@property (nonatomic, strong) NSString * gpLastModified;
@property (nonatomic, strong) NSData * gpPhoto;
@property (nonatomic, strong) NSString * gpSpecialService;
@end
@interface Web_x0020_Service_ElementRemoveLaundryItem : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementRemoveLaundryItem *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSNumber * intDutyAssignID;
@property (nonatomic, strong) NSNumber * intLdryLstID;
@end
@interface Web_x0020_Service_ElementGetOtherActivitiesStatusRoutineResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetOtherActivitiesStatusRoutineResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_OtherActivityStatusList * GetOtherActivitiesStatusRoutineResult;
@end
@interface Web_x0020_Service_ArrayOfAdHocMessageList : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_AdHocMsgList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_AdHocMsgList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSArray * AdHocMessageList;
@end
@interface Web_x0020_Service_PostMinibarItemsList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_PostMinibarItemsList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSArray * MinibarItems;
@end
@interface Web_x0020_Service_ElementUpdateAdditionalJobRemark : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementUpdateAdditionalJobRemark *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSNumber * intOnDayJobID;
@property (nonatomic, strong) NSString * sRemark;
@end
@interface Web_x0020_Service_ZoneList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ZoneList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * znID;
@property (nonatomic, strong) NSString * znName;
@property (nonatomic, strong) NSString * znLastModified;
@end
@interface Web_x0020_Service_ElementGetAmenitiesLocationListResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetAmenitiesLocationListResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_AmenitiesLocList * GetAmenitiesLocationListResult;
@end
@interface Web_x0020_Service_ElementGetLaundryCategoryList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetLaundryCategoryList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSNumber * intHotelID;
@property (nonatomic, strong) NSString * strLastModified;
@end
@interface Web_x0020_Service_ElementPostMinibarItemsResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementPostMinibarItemsResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_PostMinibarItemsList * PostMinibarItemsResult;
@end
@interface Web_x0020_Service_ArrayOfSupervisorCheckList : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_SprChkList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_SprChkList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSArray * SprChkList;
@end
@interface Web_x0020_Service_ArrayOfUnassignRoomList : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_RoomDetail : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_RoomDetail *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) Web_x0020_Service_RoomAssign * RoomDetail;
@end
@interface Web_x0020_Service_ArrayOfRoomDetailsResponseDetails : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_ElementGetLaundryServiceList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetLaundryServiceList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSNumber * intHotelID;
@property (nonatomic, strong) NSString * strLastModified;
@end
@interface Web_x0020_Service_ElementGetAccessRightsResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetAccessRightsResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_AccessRightsList * GetAccessRightsResult;
@end
@interface Web_x0020_Service_ElementGetLanguageListResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetLanguageListResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_LanguageList * GetLanguageListResult;
@end
@interface Web_x0020_Service_OtherActivityDetailsList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_OtherActivityDetailsList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * Response;
@property (nonatomic, strong) NSArray * OtherActvityDetailsListing;
@end
@interface Web_x0020_Service_ElementCompleteAdditionalJobResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementCompleteAdditionalJobResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * CompleteAdditionalJobResult;
@end
@interface Web_x0020_Service_ElementGetReleaseRoomsListResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetReleaseRoomsListResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ReleaseRoomList * GetReleaseRoomsListResult;
@end
@interface Web_x0020_Service_AdditionalJob : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_AdditionalJob *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * ondayAdditionalJobId;
@property (nonatomic, strong) NSNumber * additionalJobId;
@property (nonatomic, strong) NSString * additionalJobName;
@property (nonatomic, strong) NSString * additionalJobNamend;
@property (nonatomic, strong) NSString * additionalJobNamerd;
@property (nonatomic, strong) NSNumber * additionalJobStatus;
@property (nonatomic, strong) NSString * additionalJobRemark;
@property (nonatomic, strong) NSString * additionalJobAssignedDate;
@end
@interface Web_x0020_Service_ElementGetNoOfAdHocMessageRoutineResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetNoOfAdHocMessageRoutineResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_AdHocMessageCounter * GetNoOfAdHocMessageRoutineResult;
@end
@interface Web_x0020_Service_JobTask : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_JobTask *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * taskID;
@property (nonatomic, strong) NSString * taskName;
@property (nonatomic, strong) NSString * taskLang;
@property (nonatomic, strong) NSString * taskLang2;
@end
@interface Web_x0020_Service_ElementGetLaundryServiceListResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetLaundryServiceListResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_LdrySrvList * GetLaundryServiceListResult;
@end
@interface Web_x0020_Service_ArrayOfPopupMsgItem : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_PopupMsgList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_PopupMsgList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSArray * PopupMsgItems;
@end
@interface Web_x0020_Service_ElementPostingLinenItemsResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementPostingLinenItemsResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_PostingLinenItemsList * PostingLinenItemsResult;
@end
@interface Web_x0020_Service_ElementGetReceivedAdHocMessage : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetReceivedAdHocMessage *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSString * intGrpMessageID;
@property (nonatomic, strong) NSNumber * intHotelID;
@property (nonatomic, strong) NSString * strLastModified;
@end
@interface Web_x0020_Service_CheckListCategory : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_CheckListCategory *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * clcID;
@property (nonatomic, strong) NSString * clcName;
@property (nonatomic, strong) NSString * clcLang;
@property (nonatomic, strong) NSString * clcLastModified;
@end
@interface Web_x0020_Service_ElementUpdateRoomCleaningStatusResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementUpdateRoomCleaningStatusResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * UpdateRoomCleaningStatusResult;
@end
@interface Web_x0020_Service_ElementGetFindRoomDetailsListResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetFindRoomDetailsListResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_GetFindRoomDetailsResponse * GetFindRoomDetailsListResult;
@end
@interface Web_x0020_Service_ElementPostECAttachPhoto : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementPostECAttachPhoto *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSNumber * intRoomAssignID;
@property (nonatomic, strong) NSData * binPhoto;
@end
@interface Web_x0020_Service_ElementPostMinibarOrderResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementPostMinibarOrderResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * PostMinibarOrderResult;
@end
@interface Web_x0020_Service_ElementGetCommonConfigurationsResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetCommonConfigurationsResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_CommonConfigurationsList * GetCommonConfigurationsResult;
@end
@interface Web_x0020_Service_ElementGetAdditionalJobOfRoomResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetAdditionalJobOfRoomResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_AdditionalJobRoomDetail * GetAdditionalJobOfRoomResult;
@end
@interface Web_x0020_Service_ElementGetAdditionalJobFloorListResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetAdditionalJobFloorListResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_AdditionalJobFloorList * GetAdditionalJobFloorListResult;
@end
@interface Web_x0020_Service_ElementGetRoomTypeInventoryRoutineResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetRoomTypeInventoryRoutineResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_RoomTypeInventoryResponse * GetRoomTypeInventoryRoutineResult;
@end
@interface Web_x0020_Service_LinenItemList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_LinenItemList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * lniID;
@property (nonatomic, strong) NSString * lniName;
@property (nonatomic, strong) NSString * lniLang;
@property (nonatomic, strong) NSNumber * lniCategoryID;
@property (nonatomic, strong) NSData * lniPicture;
@property (nonatomic, strong) NSString * lniLastModified;
@end
@interface Web_x0020_Service_EngCatList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_EngCatList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSArray * EngineeringCatList;
@end
@interface Web_x0020_Service_ElementPostingPanicAlert : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementPostingPanicAlert *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSNumber * intHotelID;
@end
@interface Web_x0020_Service_ArrayOfCheckListScoreForMaid : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_ElementGetUserDetailsRoutineResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetUserDetailsRoutineResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_GetUserDetailsResponse * GetUserDetailsRoutineResult;
@end
@interface Web_x0020_Service_ElementGetAdditionalJobFloorList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetAdditionalJobFloorList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intHotelID;
@property (nonatomic, strong) NSNumber * intUserId;
@property (nonatomic, strong) NSString * taskIDs;
@end
@interface Web_x0020_Service_ElementGetHotelInfoResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetHotelInfoResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_HotelInfo * GetHotelInfoResult;
@end
@interface Web_x0020_Service_ElementGetLaundryItemList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetLaundryItemList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSString * strLastModified;
@end
@interface Web_x0020_Service_LdryInstList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_LdryInstList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSArray * LdryInstructionList;
@end
@interface Web_x0020_Service_GuestRoomInformation : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_GuestRoomInformation *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSString * strHousekeeperName;
@property (nonatomic, strong) NSNumber * intNoOfCurrentGuest;
@property (nonatomic, strong) NSNumber * intNoOfArrivalGuest;
@end
@interface Web_x0020_Service_ElementFindRoomByRoomNo : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementFindRoomByRoomNo *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intStaffId;
@property (nonatomic, strong) NSString * strRoomNo;
@end
@interface Web_x0020_Service_ElementPostMessage : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementPostMessage *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSString * strSendToID;
@property (nonatomic, strong) NSString * strTopic;
@property (nonatomic, strong) NSString * strContent;
@property (nonatomic, strong) NSNumber * intHotelId;
@property (nonatomic, strong) NSString * strTransactionTime;
@end
@interface Web_x0020_Service_ElementGetChecklistItemList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetChecklistItemList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSNumber * intHotelID;
@property (nonatomic, strong) NSNumber * intCheckListID;
@property (nonatomic, strong) NSString * strLastModified;
@end
@interface Web_x0020_Service_ArrayOfOtherActivityStatus : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_ElementGetFloorList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetFloorList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intBuildingID;
@property (nonatomic, strong) NSNumber * intHotelID;
@property (nonatomic, strong) NSString * strLastModified;
@end
@interface Web_x0020_Service_ElementAddReassignRoomResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementAddReassignRoomResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * AddReassignRoomResult;
@end
@interface Web_x0020_Service_ElementUpdateGuestInfoResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementUpdateGuestInfoResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * UpdateGuestInfoResult;
@end
@interface Web_x0020_Service_PostMinibarList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_PostMinibarList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSNumber * mbID;
@end
@interface Web_x0020_Service_ElementGetSupervisorFiltersRoutineResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetSupervisorFiltersRoutineResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_GetSupervisorFiltersResponse * GetSupervisorFiltersRoutineResult;
@end
@interface Web_x0020_Service_ElementUpdateAdditionalJobStatus : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementUpdateAdditionalJobStatus *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * ondayAdditionalJobId;
@property (nonatomic, strong) NSNumber * ondayAdditionalJobStatus;
@property (nonatomic, strong) NSString * completedDateTime;
@property (nonatomic, strong) NSString * strUserID;
@end
@interface Web_x0020_Service_ElementGetAllChecklistResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetAllChecklistResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_SprChkList * GetAllChecklistResult;
@end
@interface Web_x0020_Service_ElementGetAdditionalJobTask : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetAdditionalJobTask *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intHotelID;
@end
@interface Web_x0020_Service_ElementGetSupervisorFindRoomAssignmentLists : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetSupervisorFindRoomAssignmentLists *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intSupervisorID;
@property (nonatomic, strong) NSNumber * intFloorID;
@property (nonatomic, strong) NSNumber * intStatusID;
@property (nonatomic, strong) NSNumber * intFilterType;
@end
@interface Web_x0020_Service_AdHocPhoto : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_AdHocPhoto *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * amaID;
@property (nonatomic, strong) NSData * amaPhoto;
@property (nonatomic, strong) NSNumber * amaGrpID;
@end
@interface Web_x0020_Service_ElementGetInspectionStatusResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetInspectionStatusResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_InspStatus * GetInspectionStatusResult;
@end
@interface Web_x0020_Service_OtherActivityDetails : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_OtherActivityDetails *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * OnDayID;
@property (nonatomic, strong) NSNumber * OnDayActivityID;
@property (nonatomic, strong) NSNumber * OnDayActivitiesID;
@property (nonatomic, strong) NSString * OnDayActivitiesName;
@property (nonatomic, strong) NSString * OnDayActivitiesNameLang;
@property (nonatomic, strong) NSString * OnDayActivitiesCode;
@property (nonatomic, strong) NSString * OnDayActivitiesDesc;
@property (nonatomic, strong) NSString * OnDayActivitiesDescLang;
@property (nonatomic, strong) NSNumber * OnDayLocationID;
@property (nonatomic, strong) NSNumber * OnDayStatusID;
@property (nonatomic, strong) NSString * OnDayRemark;
@property (nonatomic, strong) NSNumber * OnDayDuration;
@property (nonatomic, strong) NSNumber * OnDayReminder;
@property (nonatomic, strong) NSNumber * OnDayRemind;
@property (nonatomic, strong) NSString * OnDayAssignDate;
@property (nonatomic, strong) NSString * OnDayStartTime;
@property (nonatomic, strong) NSString * OnDayEndTime;
@property (nonatomic, strong) NSString * OnDayStaffStartTime;
@property (nonatomic, strong) NSString * OnDayStaffEndTime;
@property (nonatomic, strong) NSString * OnDayStaffStopTime;
@property (nonatomic, strong) NSString * OnDayDeclineServiceTime;
@property (nonatomic, strong) NSNumber * OnDayStaffID;
@property (nonatomic, strong) NSNumber * OnDayIsSingle;
@property (nonatomic, strong) NSNumber * OnDayDeleted;
@property (nonatomic, strong) NSNumber * OnDayDurationSpent;
@end
@interface Web_x0020_Service_ArrayOfSupervisorFiltersDetails : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_ImageAttachmentDetails : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ImageAttachmentDetails *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSString * Content;
@end
@interface Web_x0020_Service_ArrayOfAdditionalJobStatusDetails : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_GetAdditionalJobStatusesResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_GetAdditionalJobStatusesResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSArray * AdditionalJobStatusListing;
@end
@interface Web_x0020_Service_WSSettings : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_WSSettings *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSString * ccModule;
@property (nonatomic, strong) NSString * ccValue;
@end
@interface Web_x0020_Service_MsgCatList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_MsgCatList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSArray * MessageCatList;
@end
@interface Web_x0020_Service_ElementPostMessageAttachPhotoResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementPostMessageAttachPhotoResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * PostMessageAttachPhotoResult;
@end
@interface Web_x0020_Service_ElementGetNumberOfGuestResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetNumberOfGuestResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_NumberOfGuestInfo * GetNumberOfGuestResult;
@end
@interface Web_x0020_Service_FindGuestStayHistoryResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_FindGuestStayHistoryResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatuses;
@property (nonatomic, strong) NSArray * FindGuestInformationList;
@end
@interface Web_x0020_Service_EHSKPUser : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_EHSKPUser *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) Web_x0020_Service_UserDetail * UserDetail;
@end
@interface Web_x0020_Service_LostAndFoundColorList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_LostAndFoundColorList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * lfcID;
@property (nonatomic, strong) NSString * lfcName;
@property (nonatomic, strong) NSString * lfcLang;
@property (nonatomic, strong) NSString * lfcHTMLColorCode;
@property (nonatomic, strong) NSString * lfcLastModified;
@end
@interface Web_x0020_Service_AmenitiesLocationList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_AmenitiesLocationList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * amlID;
@property (nonatomic, strong) NSString * amlLocationName;
@property (nonatomic, strong) NSString * amlLocationLang;
@property (nonatomic, strong) NSString * amlLocationLastModified;
@end
@interface Web_x0020_Service_PostAmenitiesItems : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_PostAmenitiesItems *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSArray * amenitiesItemsList;
@end
@interface Web_x0020_Service_ElementPostChecklistItems : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementPostChecklistItems *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUserID;
@property (nonatomic, strong) NSNumber * intRoomAssignID;
@property (nonatomic, strong) NSNumber * intChecklistID;
@property (nonatomic, strong) NSArray * PostChkLstItems;
@end
@interface Web_x0020_Service_ChkListItm : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ChkListItm *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSArray * ChkLstItm;
@end
@interface Web_x0020_Service_ChkListScoreForMaid : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ChkListScoreForMaid *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSArray * ChkLstScoreForMaid;
@end
@interface Web_x0020_Service_PostLinenItemsList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_PostLinenItemsList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSArray * LinenItems;
@end
@interface Web_x0020_Service_PostItem : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_PostItem *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intItemID;
@property (nonatomic, strong) NSNumber * intNewQuantity;
@property (nonatomic, strong) NSNumber * intUsedQuantity;
@property (nonatomic, strong) NSString * strTransactionTime;
@end
@interface Web_x0020_Service_ElementValidateRoomNoResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementValidateRoomNoResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ValidateRoomNoResult;
@end
@interface Web_x0020_Service_ElementGetStillLoggedInResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetStillLoggedInResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * GetStillLoggedInResult;
@end
@interface Web_x0020_Service_ElementGetUserList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetUserList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSNumber * intHotelID;
@property (nonatomic, strong) NSString * strLastModified;
@end
@interface Web_x0020_Service_ElementPosteCnAhMsg : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementPosteCnAhMsg *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSString * strSender;
@property (nonatomic, strong) NSString * strUsername;
@property (nonatomic, strong) NSString * strPassword;
@property (nonatomic, strong) NSString * strPropertyID;
@property (nonatomic, strong) NSString * strRunnerID;
@property (nonatomic, strong) NSString * strRunnerName;
@property (nonatomic, strong) NSString * strGroupID;
@property (nonatomic, strong) NSString * strGroupName;
@property (nonatomic, strong) NSString * strDeviceType;
@property (nonatomic, strong) NSString * strDeviceNo;
@property (nonatomic, strong) NSString * strMessage;
@property (nonatomic, strong) NSString * strRoomNo;
@property (nonatomic, strong) NSString * strLocationCode;
@end
@interface Web_x0020_Service_UserDetailsReponseDetails : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_UserDetailsReponseDetails *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSString * UserID;
@property (nonatomic, strong) NSString * UserName;
@property (nonatomic, strong) NSString * UserFullName;
@property (nonatomic, strong) NSString * UserHotelID;
@end
@interface Web_x0020_Service_GuestOfRoomInfo : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_GuestOfRoomInfo *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) Web_x0020_Service_GuestOfRoom * GuestInfo;
@end
@interface Web_x0020_Service_AdditionalJobDetail : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_AdditionalJobDetail *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSNumber * expectedCleaningTime;
@property (nonatomic, strong) NSArray * JobTaskList;
@property (nonatomic, strong) NSString * jobRemark;
@end
@interface Web_x0020_Service_ProfileNoteType : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ProfileNoteType *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * typeID;
@property (nonatomic, strong) NSString * typeCode;
@property (nonatomic, strong) NSString * typeDescription;
@property (nonatomic, strong) NSArray * ProfileNoteList;
@end
@interface Web_x0020_Service_HotelInfo : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_HotelInfo *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) Web_x0020_Service_Hotel * Hotel;
@end
@interface Web_x0020_Service_AccessRightsList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_AccessRightsList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSArray * AccessRightsList;
@end
@interface Web_x0020_Service_ElementGetRoomTypeList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetRoomTypeList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intHotelID;
@property (nonatomic, strong) NSString * strLastModified;
@end
@interface Web_x0020_Service_ElementGetAdditionalJobStatusesRoutine : NSObject
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetAdditionalJobStatusesRoutine *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

@end
@interface Web_x0020_Service_ElementGetAccessRights : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetAccessRights *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@end
@interface Web_x0020_Service_ElementAddReassignRoom : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementAddReassignRoom *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSString * strRoomNo;
@property (nonatomic, strong) NSString * strAssignDateTime;
@property (nonatomic, strong) NSNumber * intExpectedCleaningTime;
@property (nonatomic, strong) NSNumber * intRoomStatus;
@property (nonatomic, strong) NSNumber * intCleaningStatus;
@property (nonatomic, strong) NSNumber * intHousekeeperID;
@property (nonatomic, strong) NSNumber * intRoomTypeID;
@property (nonatomic, strong) NSNumber * intFloorID;
@property (nonatomic, strong) NSNumber * intGuestProfileID;
@property (nonatomic, strong) NSNumber * intHotelID;
@property (nonatomic, strong) NSNumber * intDutyAssignmentID;
@property (nonatomic, strong) NSString * strRemark;
@property (nonatomic, strong) NSString * CleaningCredit;
@end
@interface Web_x0020_Service_PostChecklistItemsList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_PostChecklistItemsList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSArray * ChecklistItems;
@end
@interface Web_x0020_Service_SupervisorFiltersDetails : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_SupervisorFiltersDetails *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSString * Type;
@property (nonatomic, strong) NSString * ID;
@property (nonatomic, strong) NSString * Name;
@property (nonatomic, strong) NSString * Lang;
@property (nonatomic, strong) NSString * RoomNo;
@property (nonatomic, strong) NSString * HotelID;
@end
@interface Web_x0020_Service_ElementPostChecklistItemsResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementPostChecklistItemsResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_PostChecklistItemsList * PostChecklistItemsResult;
@end
@interface Web_x0020_Service_ArrayOfOtherActivityLocation : NSObject
+ (NSArray *)deserializeNode:(xmlNodePtr)cur;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(NSArray *)value;
@end
@interface Web_x0020_Service_OtherActivityLocationList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_OtherActivityLocationList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * Response;
@property (nonatomic, strong) NSArray * ActivityLocationList;
@end
@interface Web_x0020_Service_ElementPostLinenItemResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementPostLinenItemResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_PostLinenList * PostLinenItemResult;
@end
@interface Web_x0020_Service_ElementPostLaundryOrderResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementPostLaundryOrderResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * PostLaundryOrderResult;
@end
@interface Web_x0020_Service_ElementGetGuestInfo : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetGuestInfo *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSNumber * intRoomAssignID;
@property (nonatomic, strong) NSString * strLastModified;
@end
@interface Web_x0020_Service_ElementGetLinenItemListResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetLinenItemListResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_LinenItmList * GetLinenItemListResult;
@end
@interface Web_x0020_Service_ElementPostChecklistItemResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementPostChecklistItemResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * PostChecklistItemResult;
@end
@interface Web_x0020_Service_ElementGetGuestInfoByRoomID : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetGuestInfoByRoomID *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSNumber * intRoomID;
@property (nonatomic, strong) NSString * strLastModified;
@end
@interface Web_x0020_Service_ElementGetMessageAttachPhotoResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetMessageAttachPhotoResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_AdHocMsgPhoto * GetMessageAttachPhotoResult;
@end
@interface Web_x0020_Service_ElementUpdateRoomRemarks : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementUpdateRoomRemarks *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSString * strRoomNo;
@property (nonatomic, strong) NSNumber * intHTID;
@property (nonatomic, strong) NSNumber * intRemarksType;
@property (nonatomic, strong) NSString * strRemarks;
@end
@interface Web_x0020_Service_ElementGetMessageTemplateListResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetMessageTemplateListResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_MsgTempList * GetMessageTemplateListResult;
@end
@interface Web_x0020_Service_ElementPosteCnAhMsgResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementPosteCnAhMsgResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_PosteCnAhMsgList * PosteCnAhMsgResult;
@end
@interface Web_x0020_Service_LostNFoundColorList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_LostNFoundColorList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSArray * LostNFoundColorList;
@end
@interface Web_x0020_Service_ElementGetAdditionalJobRoomByFloor : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetAdditionalJobRoomByFloor *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intHotelID;
@property (nonatomic, strong) NSNumber * intUserID;
@property (nonatomic, strong) NSNumber * intFloorID;
@property (nonatomic, strong) NSString * taskIds;
@end
@interface Web_x0020_Service_AdditionalJobStatusDetails : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_AdditionalJobStatusDetails *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSString * ID;
@property (nonatomic, strong) NSString * Name;
@property (nonatomic, strong) NSString * Lang;
@property (nonatomic, strong) NSString * Image;
@end
@interface Web_x0020_Service_GetSecretsResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_GetSecretsResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSString * DesExtendedSecretKey;
@property (nonatomic, strong) NSString * DesExtendedInitializationVector;
@end
@interface Web_x0020_Service_ElementUpdateRoomStatusResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementUpdateRoomStatusResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * UpdateRoomStatusResult;
@end
@interface Web_x0020_Service_ElementGetMessageAttachPhoto : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetMessageAttachPhoto *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSNumber * intGrpMsgID;
@end
@interface Web_x0020_Service_ElementGetNumberOfGuest : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetNumberOfGuest *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSNumber * intHotelID;
@property (nonatomic, strong) NSString * sRoomNo;
@end
@interface Web_x0020_Service_ElementGetAdditionalJobCount : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetAdditionalJobCount *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intHotelID;
@property (nonatomic, strong) NSNumber * intUserID;
@end
@interface Web_x0020_Service_ElementGetAdditionalJobCountResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetAdditionalJobCountResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_AdditionalJobCount * GetAdditionalJobCountResult;
@end
@interface Web_x0020_Service_ElementGetAmenitiesCategoryList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetAmenitiesCategoryList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSNumber * intHotelID;
@property (nonatomic, strong) NSString * strLastModified;
@end
@interface Web_x0020_Service_ElementGetReceivedAdHocMessageRoutine : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetReceivedAdHocMessageRoutine *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSString * UserID;
@property (nonatomic, strong) NSString * GroupMessageID;
@property (nonatomic, strong) NSString * PropertyID;
@property (nonatomic, strong) NSString * LastModifiedDateTime;
@end
@interface Web_x0020_Service_OtherActivityStatusList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_OtherActivityStatusList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * Response;
@property (nonatomic, strong) NSArray * ActivityStatusList;
@end
@interface Web_x0020_Service_AmenitiesLocList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_AmenitiesLocList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSArray * AmenitiesLocLst;
@end
@interface Web_x0020_Service_ResponseStatus : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ResponseStatus *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * respCode;
@property (nonatomic, strong) NSString * respMsg;
@end
@interface Web_x0020_Service_ElementGetRoomRemarks : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetRoomRemarks *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSString * strRoomNo;
@property (nonatomic, strong) NSNumber * intHTID;
@property (nonatomic, strong) NSNumber * intRemarksType;
@end
@interface Web_x0020_Service_ElementPostUpdateAdHocMessageRoutineResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementPostUpdateAdHocMessageRoutineResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_UpdateAdHocMessageResponse * PostUpdateAdHocMessageRoutineResult;
@end
@interface Web_x0020_Service_ElementGetPopupMsgListResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetPopupMsgListResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_PopupMsgList * GetPopupMsgListResult;
@end
@interface Web_x0020_Service_ElementGetSupervisorFiltersRoutine : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetSupervisorFiltersRoutine *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSString * UserID;
@property (nonatomic, strong) NSString * BuildingID;
@property (nonatomic, strong) NSString * HTID;
@end
@interface Web_x0020_Service_AttdantList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_AttdantList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSArray * AttendantList;
@end
@interface Web_x0020_Service_ElementPostLogoutResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementPostLogoutResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * PostLogoutResult;
@end
@interface Web_x0020_Service_GetFindRoomDetailsRequest : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_GetFindRoomDetailsRequest *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * UserID;
@property (nonatomic, strong) NSString * FloorID;
@property (nonatomic, strong) NSNumber * BuildingID;
@property (nonatomic, strong) NSNumber * HTID;
@property (nonatomic, strong) NSNumber * FilterType;
@property (nonatomic, strong) NSString * GeneralFilter;
@end
@interface Web_x0020_Service_ZoneRoomList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ZoneRoomList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * znrRoomID;
@property (nonatomic, strong) NSNumber * znrZoneID;
@property (nonatomic, strong) NSNumber * znrFloorID;
@property (nonatomic, strong) NSString * znrLastModified;
@end
@interface Web_x0020_Service_LaundryList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_LaundryList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * ldryLstID;
@property (nonatomic, strong) NSString * ldryLstName;
@property (nonatomic, strong) NSString * ldryLstLang;
@property (nonatomic, strong) NSString * ldryLstLastModified;
@property (nonatomic, strong) NSData * ldryLstPicture;
@end
@interface Web_x0020_Service_ElementGetFindFloorDetailsList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetFindFloorDetailsList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_GetFindFloorDetailsRequest * GetFindFloorDetailsRequest;
@end
@interface Web_x0020_Service_CleaningStatusList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_CleaningStatusList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSArray * CleaningStatusList;
@end
@interface Web_x0020_Service_ElementGetVersion : NSObject
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetVersion *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

@end
@interface Web_x0020_Service_RoomStatus : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_RoomStatus *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSString * rsID;
@property (nonatomic, strong) NSString * rsCode;
@property (nonatomic, strong) NSString * rsName;
@property (nonatomic, strong) NSString * rsLang;
@property (nonatomic, strong) NSData * rsIconImage;
@property (nonatomic, strong) NSString * rsLastModifed;
@property (nonatomic, strong) NSString * rsPhysicalCheck;
@end
@interface Web_x0020_Service_ElementValidateRoomNo : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementValidateRoomNo *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSString * strRoomNo;
@property (nonatomic, strong) NSString * strHotel_ID;
@end
@interface Web_x0020_Service_ElementGetAdditionalJobDetailResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetAdditionalJobDetailResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_AdditionalJobDetail * GetAdditionalJobDetailResult;
@end
@interface Web_x0020_Service_PostingHistoryDetails : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_PostingHistoryDetails *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSString * UserID;
@property (nonatomic, strong) NSString * RoomNo;
@property (nonatomic, strong) NSString * HotelID;
@property (nonatomic, strong) NSString * ModuleID;
@property (nonatomic, strong) NSString * CategoryDescription;
@property (nonatomic, strong) NSString * ItemDescription;
@property (nonatomic, strong) NSString * NewQuantity;
@property (nonatomic, strong) NSString * UsedQuantity;
@property (nonatomic, strong) NSString * TransactionDateTime;
@property (nonatomic, strong) NSString * UserName;
@end
@interface Web_x0020_Service_ElementGetChecklistItemListResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetChecklistItemListResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ChkListItm * GetChecklistItemListResult;
@end
@interface Web_x0020_Service_AdditionalJobRoom : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_AdditionalJobRoom *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * roomId;
@property (nonatomic, strong) NSString * roomNo;
@property (nonatomic, strong) NSNumber * isDueOutRoom;
@property (nonatomic, strong) NSNumber * roomStatusID;
@property (nonatomic, strong) NSString * remark;
@property (nonatomic, strong) NSArray * pendingAdditionalJobList;
@property (nonatomic, strong) NSNumber * totalPendingAdditionalJobList;
@property (nonatomic, strong) NSNumber * roomTypeID;
@end
@interface Web_x0020_Service_ElementGetMessageItemListResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetMessageItemListResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_MsgItmList * GetMessageItemListResult;
@end
@interface Web_x0020_Service_LdryItmList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_LdryItmList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSArray * LaundryItmList;
@end
@interface Web_x0020_Service_UnAssgnLst : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_UnAssgnLst *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSArray * UnAssgnLst;
@end
@interface Web_x0020_Service_GetSecretsRequest : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_GetSecretsRequest *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSString * UserName;
@property (nonatomic, strong) NSString * Password;
@end
@interface Web_x0020_Service_ElementGetLFColorListResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetLFColorListResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_LostNFoundColorList * GetLFColorListResult;
@end
@interface Web_x0020_Service_ElementGetRoomAssignmentList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetRoomAssignmentList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSString * strLastModified;
@property (nonatomic, strong) NSString * intInspectionStatus;
@property (nonatomic, strong) NSString * intRoomStatus;
@end
@interface Web_x0020_Service_GuestInfo : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_GuestInfo *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) Web_x0020_Service_Guest * GuestInfo;
@end
@interface Web_x0020_Service_GetSupervisorFiltersResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_GetSupervisorFiltersResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSArray * SupervisorFiltersListing;
@end
@interface Web_x0020_Service_ElementUpdateAdHocMessageStatus : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementUpdateAdHocMessageStatus *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSNumber * intMsgID;
@property (nonatomic, strong) NSNumber * intStatus;
@end
@interface Web_x0020_Service_ElementPostEngineeringCaseResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementPostEngineeringCaseResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_EngCase * PostEngineeringCaseResult;
@end
@interface Web_x0020_Service_ElementGetRoomSetGuideList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetRoomSetGuideList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intHotelID;
@property (nonatomic, strong) NSNumber * intCategoryID;
@property (nonatomic, strong) NSString * strLastModified;
@end
@interface Web_x0020_Service_ElementGetDiscrepantRoomStatus : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetDiscrepantRoomStatus *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSNumber * intRoomAssignID;
@property (nonatomic, strong) NSNumber * intRoomStatusID;
@end
@interface Web_x0020_Service_ElementPostWSLogResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementPostWSLogResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * PostWSLogResult;
@end
@interface Web_x0020_Service_PostOtherActivityStatusUpdateResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_PostOtherActivityStatusUpdateResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@end
@interface Web_x0020_Service_LinenCatList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_LinenCatList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSArray * LinenCat;
@end
@interface Web_x0020_Service_ReleaseRoomDetails : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ReleaseRoomDetails *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSString * strRoomNo;
@property (nonatomic, strong) NSNumber * intHTID;
@property (nonatomic, strong) NSNumber * intRoomStatus;
@property (nonatomic, strong) NSString * strRemarks;
@property (nonatomic, strong) NSString * strReason;
@end
@interface Web_x0020_Service_ElementGetGuestInfoResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetGuestInfoResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_GuestInfo * GetGuestInfoResult;
@end
@interface Web_x0020_Service_ElementGetVersionResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetVersionResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_Version * GetVersionResult;
@end
@interface Web_x0020_Service_ElementGetRoomTypeByRoomNoRoutine : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetRoomTypeByRoomNoRoutine *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSString * UserID;
@property (nonatomic, strong) NSString * RoomNo;
@property (nonatomic, strong) NSString * PropertyID;
@end
@interface Web_x0020_Service_GuestInformationResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_GuestInformationResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * mResponseStatus;
@property (nonatomic, strong) Web_x0020_Service_GuestRoomInformation * mGuestRoomInformation;
@property (nonatomic, strong) NSArray * mCurrentGuestList;
@property (nonatomic, strong) NSArray * mArrivalGuestList;
@end
@interface Web_x0020_Service_GetRoomDetailsResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_GetRoomDetailsResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSArray * RoomDetailsReponseListing;
@end
@interface Web_x0020_Service_ElementGetProfileNote : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetProfileNote *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSString * sRoomNo;
@property (nonatomic, strong) NSNumber * intUserID;
@property (nonatomic, strong) NSNumber * intHotelID;
@end
@interface Web_x0020_Service_RoomTypeInventoryDetails : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_RoomTypeInventoryDetails *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSString * ServiceItem;
@property (nonatomic, strong) NSNumber * InventoryID;
@property (nonatomic, strong) NSNumber * CategoryID;
@property (nonatomic, strong) NSNumber * RoomTypeID;
@property (nonatomic, strong) NSNumber * BuildingID;
@property (nonatomic, strong) NSNumber * SectionID;
@property (nonatomic, strong) NSNumber * DefaultQuantity;
@end
@interface Web_x0020_Service_CommonConfigurationsList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_CommonConfigurationsList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * ResponseStatus;
@property (nonatomic, strong) NSArray * CommonConfigurationsList;
@end
@interface Web_x0020_Service_ElementGetLaundrySpecialInstructionListResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetLaundrySpecialInstructionListResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_LdryInstList * GetLaundrySpecialInstructionListResult;
@end
@interface Web_x0020_Service_ElementGetAmenitiesItemListResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetAmenitiesItemListResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_AmenitiesItmList * GetAmenitiesItemListResult;
@end
@interface Web_x0020_Service_ElementPostChecklistItem : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementPostChecklistItem *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSNumber * intRoomAssignID;
@property (nonatomic, strong) NSNumber * intChecklistID;
@property (nonatomic, strong) NSNumber * intChecklistItemID;
@property (nonatomic, strong) NSNumber * intScoreValue;
@end
@interface Web_x0020_Service_ElementGetUnassignRoomList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetUnassignRoomList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSNumber * intHotelID;
@property (nonatomic, strong) NSString * strLastModified;
@end
@interface Web_x0020_Service_BlockRoomDetails : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_BlockRoomDetails *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSString * strRoomNo;
@property (nonatomic, strong) NSNumber * intHTID;
@property (nonatomic, strong) NSNumber * intRoomStatus;
@property (nonatomic, strong) NSString * strOOSRemarks;
@property (nonatomic, strong) NSString * strOOSReason;
@property (nonatomic, strong) NSString * strOOSDuration;
@end
@interface Web_x0020_Service_ElementPostLFAttachPhotoResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementPostLFAttachPhotoResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_ResponseStatus * PostLFAttachPhotoResult;
@end
@interface Web_x0020_Service_ElementStartAdditionalJob : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementStartAdditionalJob *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * ondayAdditionalJobId;
@property (nonatomic, strong) NSString * startedDateTime;
@end
@interface Web_x0020_Service_ElementGetLocationList : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetLocationList *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intHotelID;
@property (nonatomic, strong) NSString * strLastModified;
@end
@interface Web_x0020_Service_ElementPostLinenItem : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementPostLinenItem *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSNumber * intUsrID;
@property (nonatomic, strong) NSNumber * intRoomAssignID;
@property (nonatomic, strong) NSNumber * intLinenItemID;
@property (nonatomic, strong) NSNumber * intNewQuantity;
@property (nonatomic, strong) NSNumber * intUsedQuantity;
@property (nonatomic, strong) NSString * strTransactionTime;
@end
@interface Web_x0020_Service_ElementGetRoomAssignmentListResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetRoomAssignmentListResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_RoomAssignmentList * GetRoomAssignmentListResult;
@end
@interface Web_x0020_Service_ElementGetRoomSetGuideByRoomTypeResponse : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetRoomSetGuideByRoomTypeResponse *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) Web_x0020_Service_RoomStpList * GetRoomSetGuideByRoomTypeResult;
@end
@interface Web_x0020_Service_ElementGetOtherActivitiesStatusRoutine : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_ElementGetOtherActivitiesStatusRoutine *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSString * UserID;
@property (nonatomic, strong) NSString * LastModified;
@end
@interface Web_x0020_Service_Floor : NSObject
- (void)addElementsToNode:(xmlNodePtr)node;
+ (void)serializeToChildOf:(xmlNodePtr)node withName:(const char *)childName value:(Web_x0020_Service_Floor *)value;
+ (instancetype)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSString * flID;
@property (nonatomic, strong) NSString * flName;
@property (nonatomic, strong) NSString * flLang;
@property (nonatomic, strong) NSString * flBuildingID;
@property (nonatomic, strong) NSString * flLastModifed;
@end
/* Cookies handling provided by http://en.wikibooks.org/wiki/Programming:WebObjects/Web_Services/Web_Service_Provider */

#import <libxml/parser.h>
#import "xsd.h"
#import "Web_x0020_Service.h"

@class Web_x0020_Service_Web_x0020_ServiceSoapBinding;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding;

@interface Web_x0020_Service : NSObject

+ (Web_x0020_Service_Web_x0020_ServiceSoapBinding *)Web_x0020_ServiceSoapBinding;
+ (Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)Web_x0020_ServiceSoap12Binding;

@end

@class Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse;
@class Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_AddReassignRoom;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_AssignRoom;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_AuthenticateUser;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_ChangeRoomAssignmentSequence;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_CompleteAdditionalJob;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_FindAttendant;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_FindGuestStayedHistoryRoutine;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_FindRoomByRoomNo;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetAccessRights;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetAdditionalJobByRoomNo;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetAdditionalJobCount;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetAdditionalJobDetail;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetAdditionalJobFloorList;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetAdditionalJobOfRoom;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetAdditionalJobRoomByFloor;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetAdditionalJobStatusesRoutine;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetAdditionalJobTask;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetAllChecklist;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetAmenitiesCategoryList;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetAmenitiesItemList;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetAmenitiesLocationList;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetBuildingList;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetCheckListItemScoreForMaid;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetChecklistCategoryList;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetChecklistItemList;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetChecklistRoomType;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetCleaningStatusList;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetCommonConfigurations;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetDiscrepantRoomStatus;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetEngineeringCategoryList;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetEngineeringItem;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetFindFloorDetailsList;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetFindRoomAssignmentList;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetFindRoomAssignmentLists;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetFindRoomDetailsList;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetFloorList;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetGuestInfo;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetGuestInfoByRoomID;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetGuestInformation;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetHotelInfo;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetInspectionStatus;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetLFCategoryList;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetLFColorList;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetLFItemList;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetLanguageList;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetLaundryCategoryList;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetLaundryItemList;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetLaundryItemPriceList;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetLaundryServiceList;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetLaundrySpecialInstructionList;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetLinenCategoryList;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetLinenItemList;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetLocationList;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetMessageAttachPhoto;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetMessageCategoryList;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetMessageCategoryList2;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetMessageItemList;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetMessageItemList2;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetMessageTemplateList;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetMinibarCategoryList;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetMinibarItemList;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetNewMessage;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetNoOfAdHocMessage;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetNoOfAdHocMessageRoutine;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetNumberOfGuest;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetOOSBlockRoomsList;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetOtherActivitiesLocationRoutine;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetOtherActivitiesStatusRoutine;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetOtherActivityAssignmentRoutine;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetPanicAlertConfig;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetPopupMsgList;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetPostingHistoryRoutine;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetPrevRoomAssignmentList;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetProfileNote;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetReceivedAdHocMessage;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetReceivedAdHocMessageRoutine;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetReleaseRoomsList;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetRoomAssignmentList;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetRoomDetail;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetRoomDetailsRoutine;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetRoomRemarks;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetRoomSectionList;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetRoomSetGuideByRoomType;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetRoomSetGuideList;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetRoomStatusList;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetRoomTypeByRoomNoRoutine;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetRoomTypeInventoryRoutine;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetRoomTypeList;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetSecretsRoutine;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetSendAdHocMessageRoutine;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetSentAdHocMessage;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetStillLoggedIn;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetSupervisorFiltersRoutine;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetSupervisorFindFloorList;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetSupervisorFindRoomAssignmentLists;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetUnassignRoomList;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetUserDetailsRoutine;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetUserList;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetVersion;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetZoneList;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetZoneRoomList;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_LogInvalidStartRoom;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostAdditionalJobStatusUpdateRoutine;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostAmenitiesItem;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostAmenitiesItemsList;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostChecklistItem;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostChecklistItems;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostECAttachPhoto;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostEngineeringCase;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostLFAttachPhoto;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostLaundryItem;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostLaundryOrder;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostLaundrySpecialInstruction;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostLinenItem;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostLinenItems;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostLogout;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostLostAndFoundRoutine;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostLostFound;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostMessage;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostMessageAttachPhoto;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostMinibarItem;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostMinibarItems;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostMinibarOrder;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostPostingHistoryRoutine;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostUnassignRoomRoutine;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostUpdateAdHocMessageRoutine;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostUpdateDeviceTokenRoutine;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostUpdateOtherActivityStatusRoutine;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostUpdateRoomDetailsRoutine;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostWSLog;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_PosteCnAhMsg;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_PosteCnJob;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostingLinenItems;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostingPanicAlert;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_RemoveLaundryItem;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_StartAdditionalJob;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_StopAdditionalJob;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_UpdateAdHocMessageStatus;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_UpdateAdditionalJobRemark;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_UpdateAdditionalJobStatus;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_UpdateCleaningStatus;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_UpdateGuestInfo;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_UpdateInspection;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_UpdatePopupMsgStatus;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_UpdateRoomAssignment;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_UpdateRoomCleaningStatus;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_UpdateRoomDetails;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_UpdateRoomRemarks;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_UpdateRoomStatus;
@class Web_x0020_Service_Web_x0020_ServiceSoapBinding_ValidateRoomNo;

typedef void (^Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)(NSArray *headers, NSArray *bodyParts);
typedef void (^Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)(NSError *error);

@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding : NSObject
@property (nonatomic, copy) NSURL *address;
@property (nonatomic) BOOL logXMLInOut;
@property (nonatomic) BOOL ignoreEmptyResponse;
@property (nonatomic) NSTimeInterval timeout;
@property (nonatomic, strong) NSMutableArray *cookies;
@property (nonatomic, strong) NSMutableDictionary *customHeaders;
@property (nonatomic, strong) id <SSLCredentialsManaging> sslManager;
@property (nonatomic, strong) SOAPSigner *soapSigner;


+ (NSTimeInterval) defaultTimeout;

- (id)initWithAddress:(NSString *)anAddress;
- (void)sendHTTPCallUsingBody:(NSString *)body soapAction:(NSString *)soapAction forOperation:(Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation *)operation;
- (void)addCookie:(NSHTTPCookie *)toAdd;
- (NSString *)MIMEType;

- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)AddReassignRoom:(Web_x0020_Service_ElementAddReassignRoom *)aAddReassignRoom;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_AddReassignRoom*)AddReassignRoom:(Web_x0020_Service_ElementAddReassignRoom *)aAddReassignRoom success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)AssignRoom:(Web_x0020_Service_ElementAssignRoom *)aAssignRoom;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_AssignRoom*)AssignRoom:(Web_x0020_Service_ElementAssignRoom *)aAssignRoom success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)AuthenticateUser:(Web_x0020_Service_ElementAuthenticateUser *)aAuthenticateUser;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_AuthenticateUser*)AuthenticateUser:(Web_x0020_Service_ElementAuthenticateUser *)aAuthenticateUser success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)ChangeRoomAssignmentSequence:(Web_x0020_Service_ElementChangeRoomAssignmentSequence *)aChangeRoomAssignmentSequence;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_ChangeRoomAssignmentSequence*)ChangeRoomAssignmentSequence:(Web_x0020_Service_ElementChangeRoomAssignmentSequence *)aChangeRoomAssignmentSequence success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)CompleteAdditionalJob:(Web_x0020_Service_ElementCompleteAdditionalJob *)aCompleteAdditionalJob;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_CompleteAdditionalJob*)CompleteAdditionalJob:(Web_x0020_Service_ElementCompleteAdditionalJob *)aCompleteAdditionalJob success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)FindAttendant:(Web_x0020_Service_ElementFindAttendant *)aFindAttendant;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_FindAttendant*)FindAttendant:(Web_x0020_Service_ElementFindAttendant *)aFindAttendant success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)FindGuestStayedHistoryRoutine:(Web_x0020_Service_ElementFindGuestStayedHistoryRoutine *)aFindGuestStayedHistoryRoutine;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_FindGuestStayedHistoryRoutine*)FindGuestStayedHistoryRoutine:(Web_x0020_Service_ElementFindGuestStayedHistoryRoutine *)aFindGuestStayedHistoryRoutine success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)FindRoomByRoomNo:(Web_x0020_Service_ElementFindRoomByRoomNo *)aFindRoomByRoomNo;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_FindRoomByRoomNo*)FindRoomByRoomNo:(Web_x0020_Service_ElementFindRoomByRoomNo *)aFindRoomByRoomNo success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetAccessRights:(Web_x0020_Service_ElementGetAccessRights *)aGetAccessRights;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetAccessRights*)GetAccessRights:(Web_x0020_Service_ElementGetAccessRights *)aGetAccessRights success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetAdditionalJobByRoomNo:(Web_x0020_Service_ElementGetAdditionalJobByRoomNo *)aGetAdditionalJobByRoomNo;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetAdditionalJobByRoomNo*)GetAdditionalJobByRoomNo:(Web_x0020_Service_ElementGetAdditionalJobByRoomNo *)aGetAdditionalJobByRoomNo success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetAdditionalJobCount:(Web_x0020_Service_ElementGetAdditionalJobCount *)aGetAdditionalJobCount;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetAdditionalJobCount*)GetAdditionalJobCount:(Web_x0020_Service_ElementGetAdditionalJobCount *)aGetAdditionalJobCount success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetAdditionalJobDetail:(Web_x0020_Service_ElementGetAdditionalJobDetail *)aGetAdditionalJobDetail;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetAdditionalJobDetail*)GetAdditionalJobDetail:(Web_x0020_Service_ElementGetAdditionalJobDetail *)aGetAdditionalJobDetail success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetAdditionalJobFloorList:(Web_x0020_Service_ElementGetAdditionalJobFloorList *)aGetAdditionalJobFloorList;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetAdditionalJobFloorList*)GetAdditionalJobFloorList:(Web_x0020_Service_ElementGetAdditionalJobFloorList *)aGetAdditionalJobFloorList success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetAdditionalJobOfRoom:(Web_x0020_Service_ElementGetAdditionalJobOfRoom *)aGetAdditionalJobOfRoom;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetAdditionalJobOfRoom*)GetAdditionalJobOfRoom:(Web_x0020_Service_ElementGetAdditionalJobOfRoom *)aGetAdditionalJobOfRoom success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetAdditionalJobRoomByFloor:(Web_x0020_Service_ElementGetAdditionalJobRoomByFloor *)aGetAdditionalJobRoomByFloor;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetAdditionalJobRoomByFloor*)GetAdditionalJobRoomByFloor:(Web_x0020_Service_ElementGetAdditionalJobRoomByFloor *)aGetAdditionalJobRoomByFloor success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetAdditionalJobStatusesRoutine:(Web_x0020_Service_ElementGetAdditionalJobStatusesRoutine *)aGetAdditionalJobStatusesRoutine;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetAdditionalJobStatusesRoutine*)GetAdditionalJobStatusesRoutine:(Web_x0020_Service_ElementGetAdditionalJobStatusesRoutine *)aGetAdditionalJobStatusesRoutine success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetAdditionalJobTask:(Web_x0020_Service_ElementGetAdditionalJobTask *)aGetAdditionalJobTask;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetAdditionalJobTask*)GetAdditionalJobTask:(Web_x0020_Service_ElementGetAdditionalJobTask *)aGetAdditionalJobTask success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetAllChecklist:(Web_x0020_Service_ElementGetAllChecklist *)aGetAllChecklist;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetAllChecklist*)GetAllChecklist:(Web_x0020_Service_ElementGetAllChecklist *)aGetAllChecklist success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetAmenitiesCategoryList:(Web_x0020_Service_ElementGetAmenitiesCategoryList *)aGetAmenitiesCategoryList;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetAmenitiesCategoryList*)GetAmenitiesCategoryList:(Web_x0020_Service_ElementGetAmenitiesCategoryList *)aGetAmenitiesCategoryList success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetAmenitiesItemList:(Web_x0020_Service_ElementGetAmenitiesItemList *)aGetAmenitiesItemList;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetAmenitiesItemList*)GetAmenitiesItemList:(Web_x0020_Service_ElementGetAmenitiesItemList *)aGetAmenitiesItemList success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetAmenitiesLocationList:(Web_x0020_Service_ElementGetAmenitiesLocationList *)aGetAmenitiesLocationList;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetAmenitiesLocationList*)GetAmenitiesLocationList:(Web_x0020_Service_ElementGetAmenitiesLocationList *)aGetAmenitiesLocationList success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetBuildingList:(Web_x0020_Service_ElementGetBuildingList *)aGetBuildingList;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetBuildingList*)GetBuildingList:(Web_x0020_Service_ElementGetBuildingList *)aGetBuildingList success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetCheckListItemScoreForMaid:(Web_x0020_Service_ElementGetCheckListItemScoreForMaid *)aGetCheckListItemScoreForMaid;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetCheckListItemScoreForMaid*)GetCheckListItemScoreForMaid:(Web_x0020_Service_ElementGetCheckListItemScoreForMaid *)aGetCheckListItemScoreForMaid success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetChecklistCategoryList:(Web_x0020_Service_ElementGetChecklistCategoryList *)aGetChecklistCategoryList;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetChecklistCategoryList*)GetChecklistCategoryList:(Web_x0020_Service_ElementGetChecklistCategoryList *)aGetChecklistCategoryList success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetChecklistItemList:(Web_x0020_Service_ElementGetChecklistItemList *)aGetChecklistItemList;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetChecklistItemList*)GetChecklistItemList:(Web_x0020_Service_ElementGetChecklistItemList *)aGetChecklistItemList success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetChecklistRoomType:(Web_x0020_Service_ElementGetChecklistRoomType *)aGetChecklistRoomType;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetChecklistRoomType*)GetChecklistRoomType:(Web_x0020_Service_ElementGetChecklistRoomType *)aGetChecklistRoomType success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetCleaningStatusList:(Web_x0020_Service_ElementGetCleaningStatusList *)aGetCleaningStatusList;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetCleaningStatusList*)GetCleaningStatusList:(Web_x0020_Service_ElementGetCleaningStatusList *)aGetCleaningStatusList success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetCommonConfigurations:(Web_x0020_Service_ElementGetCommonConfigurations *)aGetCommonConfigurations;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetCommonConfigurations*)GetCommonConfigurations:(Web_x0020_Service_ElementGetCommonConfigurations *)aGetCommonConfigurations success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetDiscrepantRoomStatus:(Web_x0020_Service_ElementGetDiscrepantRoomStatus *)aGetDiscrepantRoomStatus;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetDiscrepantRoomStatus*)GetDiscrepantRoomStatus:(Web_x0020_Service_ElementGetDiscrepantRoomStatus *)aGetDiscrepantRoomStatus success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetEngineeringCategoryList:(Web_x0020_Service_ElementGetEngineeringCategoryList *)aGetEngineeringCategoryList;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetEngineeringCategoryList*)GetEngineeringCategoryList:(Web_x0020_Service_ElementGetEngineeringCategoryList *)aGetEngineeringCategoryList success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetEngineeringItem:(Web_x0020_Service_ElementGetEngineeringItem *)aGetEngineeringItem;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetEngineeringItem*)GetEngineeringItem:(Web_x0020_Service_ElementGetEngineeringItem *)aGetEngineeringItem success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetFindFloorDetailsList:(Web_x0020_Service_ElementGetFindFloorDetailsList *)aGetFindFloorDetailsList;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetFindFloorDetailsList*)GetFindFloorDetailsList:(Web_x0020_Service_ElementGetFindFloorDetailsList *)aGetFindFloorDetailsList success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetFindRoomAssignmentList:(Web_x0020_Service_ElementGetFindRoomAssignmentList *)aGetFindRoomAssignmentList;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetFindRoomAssignmentList*)GetFindRoomAssignmentList:(Web_x0020_Service_ElementGetFindRoomAssignmentList *)aGetFindRoomAssignmentList success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetFindRoomAssignmentLists:(Web_x0020_Service_ElementGetFindRoomAssignmentLists *)aGetFindRoomAssignmentLists;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetFindRoomAssignmentLists*)GetFindRoomAssignmentLists:(Web_x0020_Service_ElementGetFindRoomAssignmentLists *)aGetFindRoomAssignmentLists success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetFindRoomDetailsList:(Web_x0020_Service_ElementGetFindRoomDetailsList *)aGetFindRoomDetailsList;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetFindRoomDetailsList*)GetFindRoomDetailsList:(Web_x0020_Service_ElementGetFindRoomDetailsList *)aGetFindRoomDetailsList success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetFloorList:(Web_x0020_Service_ElementGetFloorList *)aGetFloorList;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetFloorList*)GetFloorList:(Web_x0020_Service_ElementGetFloorList *)aGetFloorList success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetGuestInfo:(Web_x0020_Service_ElementGetGuestInfo *)aGetGuestInfo;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetGuestInfo*)GetGuestInfo:(Web_x0020_Service_ElementGetGuestInfo *)aGetGuestInfo success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetGuestInfoByRoomID:(Web_x0020_Service_ElementGetGuestInfoByRoomID *)aGetGuestInfoByRoomID;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetGuestInfoByRoomID*)GetGuestInfoByRoomID:(Web_x0020_Service_ElementGetGuestInfoByRoomID *)aGetGuestInfoByRoomID success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetGuestInformation:(Web_x0020_Service_ElementGetGuestInformation *)aGetGuestInformation;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetGuestInformation*)GetGuestInformation:(Web_x0020_Service_ElementGetGuestInformation *)aGetGuestInformation success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetHotelInfo:(Web_x0020_Service_ElementGetHotelInfo *)aGetHotelInfo;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetHotelInfo*)GetHotelInfo:(Web_x0020_Service_ElementGetHotelInfo *)aGetHotelInfo success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetInspectionStatus:(Web_x0020_Service_ElementGetInspectionStatus *)aGetInspectionStatus;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetInspectionStatus*)GetInspectionStatus:(Web_x0020_Service_ElementGetInspectionStatus *)aGetInspectionStatus success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetLFCategoryList:(Web_x0020_Service_ElementGetLFCategoryList *)aGetLFCategoryList;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetLFCategoryList*)GetLFCategoryList:(Web_x0020_Service_ElementGetLFCategoryList *)aGetLFCategoryList success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetLFColorList:(Web_x0020_Service_ElementGetLFColorList *)aGetLFColorList;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetLFColorList*)GetLFColorList:(Web_x0020_Service_ElementGetLFColorList *)aGetLFColorList success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetLFItemList:(Web_x0020_Service_ElementGetLFItemList *)aGetLFItemList;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetLFItemList*)GetLFItemList:(Web_x0020_Service_ElementGetLFItemList *)aGetLFItemList success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetLanguageList:(Web_x0020_Service_ElementGetLanguageList *)aGetLanguageList;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetLanguageList*)GetLanguageList:(Web_x0020_Service_ElementGetLanguageList *)aGetLanguageList success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetLaundryCategoryList:(Web_x0020_Service_ElementGetLaundryCategoryList *)aGetLaundryCategoryList;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetLaundryCategoryList*)GetLaundryCategoryList:(Web_x0020_Service_ElementGetLaundryCategoryList *)aGetLaundryCategoryList success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetLaundryItemList:(Web_x0020_Service_ElementGetLaundryItemList *)aGetLaundryItemList;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetLaundryItemList*)GetLaundryItemList:(Web_x0020_Service_ElementGetLaundryItemList *)aGetLaundryItemList success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetLaundryItemPriceList:(Web_x0020_Service_ElementGetLaundryItemPriceList *)aGetLaundryItemPriceList;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetLaundryItemPriceList*)GetLaundryItemPriceList:(Web_x0020_Service_ElementGetLaundryItemPriceList *)aGetLaundryItemPriceList success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetLaundryServiceList:(Web_x0020_Service_ElementGetLaundryServiceList *)aGetLaundryServiceList;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetLaundryServiceList*)GetLaundryServiceList:(Web_x0020_Service_ElementGetLaundryServiceList *)aGetLaundryServiceList success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetLaundrySpecialInstructionList:(Web_x0020_Service_ElementGetLaundrySpecialInstructionList *)aGetLaundrySpecialInstructionList;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetLaundrySpecialInstructionList*)GetLaundrySpecialInstructionList:(Web_x0020_Service_ElementGetLaundrySpecialInstructionList *)aGetLaundrySpecialInstructionList success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetLinenCategoryList:(Web_x0020_Service_ElementGetLinenCategoryList *)aGetLinenCategoryList;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetLinenCategoryList*)GetLinenCategoryList:(Web_x0020_Service_ElementGetLinenCategoryList *)aGetLinenCategoryList success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetLinenItemList:(Web_x0020_Service_ElementGetLinenItemList *)aGetLinenItemList;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetLinenItemList*)GetLinenItemList:(Web_x0020_Service_ElementGetLinenItemList *)aGetLinenItemList success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetLocationList:(Web_x0020_Service_ElementGetLocationList *)aGetLocationList;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetLocationList*)GetLocationList:(Web_x0020_Service_ElementGetLocationList *)aGetLocationList success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetMessageAttachPhoto:(Web_x0020_Service_ElementGetMessageAttachPhoto *)aGetMessageAttachPhoto;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetMessageAttachPhoto*)GetMessageAttachPhoto:(Web_x0020_Service_ElementGetMessageAttachPhoto *)aGetMessageAttachPhoto success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetMessageCategoryList:(Web_x0020_Service_ElementGetMessageCategoryList *)aGetMessageCategoryList;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetMessageCategoryList*)GetMessageCategoryList:(Web_x0020_Service_ElementGetMessageCategoryList *)aGetMessageCategoryList success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetMessageCategoryList2:(Web_x0020_Service_ElementGetMessageCategoryList2 *)aGetMessageCategoryList2;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetMessageCategoryList2*)GetMessageCategoryList2:(Web_x0020_Service_ElementGetMessageCategoryList2 *)aGetMessageCategoryList2 success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetMessageItemList:(Web_x0020_Service_ElementGetMessageItemList *)aGetMessageItemList;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetMessageItemList*)GetMessageItemList:(Web_x0020_Service_ElementGetMessageItemList *)aGetMessageItemList success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetMessageItemList2:(Web_x0020_Service_ElementGetMessageItemList2 *)aGetMessageItemList2;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetMessageItemList2*)GetMessageItemList2:(Web_x0020_Service_ElementGetMessageItemList2 *)aGetMessageItemList2 success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetMessageTemplateList:(Web_x0020_Service_ElementGetMessageTemplateList *)aGetMessageTemplateList;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetMessageTemplateList*)GetMessageTemplateList:(Web_x0020_Service_ElementGetMessageTemplateList *)aGetMessageTemplateList success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetMinibarCategoryList:(Web_x0020_Service_ElementGetMinibarCategoryList *)aGetMinibarCategoryList;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetMinibarCategoryList*)GetMinibarCategoryList:(Web_x0020_Service_ElementGetMinibarCategoryList *)aGetMinibarCategoryList success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetMinibarItemList:(Web_x0020_Service_ElementGetMinibarItemList *)aGetMinibarItemList;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetMinibarItemList*)GetMinibarItemList:(Web_x0020_Service_ElementGetMinibarItemList *)aGetMinibarItemList success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetNewMessage:(Web_x0020_Service_ElementGetNewMessage *)aGetNewMessage;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetNewMessage*)GetNewMessage:(Web_x0020_Service_ElementGetNewMessage *)aGetNewMessage success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetNoOfAdHocMessage:(Web_x0020_Service_ElementGetNoOfAdHocMessage *)aGetNoOfAdHocMessage;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetNoOfAdHocMessage*)GetNoOfAdHocMessage:(Web_x0020_Service_ElementGetNoOfAdHocMessage *)aGetNoOfAdHocMessage success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetNoOfAdHocMessageRoutine:(Web_x0020_Service_ElementGetNoOfAdHocMessageRoutine *)aGetNoOfAdHocMessageRoutine;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetNoOfAdHocMessageRoutine*)GetNoOfAdHocMessageRoutine:(Web_x0020_Service_ElementGetNoOfAdHocMessageRoutine *)aGetNoOfAdHocMessageRoutine success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetNumberOfGuest:(Web_x0020_Service_ElementGetNumberOfGuest *)aGetNumberOfGuest;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetNumberOfGuest*)GetNumberOfGuest:(Web_x0020_Service_ElementGetNumberOfGuest *)aGetNumberOfGuest success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetOOSBlockRoomsList:(Web_x0020_Service_ElementGetOOSBlockRoomsList *)aGetOOSBlockRoomsList;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetOOSBlockRoomsList*)GetOOSBlockRoomsList:(Web_x0020_Service_ElementGetOOSBlockRoomsList *)aGetOOSBlockRoomsList success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetOtherActivitiesLocationRoutine:(Web_x0020_Service_ElementGetOtherActivitiesLocationRoutine *)aGetOtherActivitiesLocationRoutine;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetOtherActivitiesLocationRoutine*)GetOtherActivitiesLocationRoutine:(Web_x0020_Service_ElementGetOtherActivitiesLocationRoutine *)aGetOtherActivitiesLocationRoutine success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetOtherActivitiesStatusRoutine:(Web_x0020_Service_ElementGetOtherActivitiesStatusRoutine *)aGetOtherActivitiesStatusRoutine;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetOtherActivitiesStatusRoutine*)GetOtherActivitiesStatusRoutine:(Web_x0020_Service_ElementGetOtherActivitiesStatusRoutine *)aGetOtherActivitiesStatusRoutine success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetOtherActivityAssignmentRoutine:(Web_x0020_Service_ElementGetOtherActivityAssignmentRoutine *)aGetOtherActivityAssignmentRoutine;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetOtherActivityAssignmentRoutine*)GetOtherActivityAssignmentRoutine:(Web_x0020_Service_ElementGetOtherActivityAssignmentRoutine *)aGetOtherActivityAssignmentRoutine success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetPanicAlertConfig:(Web_x0020_Service_ElementGetPanicAlertConfig *)aGetPanicAlertConfig;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetPanicAlertConfig*)GetPanicAlertConfig:(Web_x0020_Service_ElementGetPanicAlertConfig *)aGetPanicAlertConfig success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetPopupMsgList:(Web_x0020_Service_ElementGetPopupMsgList *)aGetPopupMsgList;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetPopupMsgList*)GetPopupMsgList:(Web_x0020_Service_ElementGetPopupMsgList *)aGetPopupMsgList success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetPostingHistoryRoutine:(Web_x0020_Service_ElementGetPostingHistoryRoutine *)aGetPostingHistoryRoutine;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetPostingHistoryRoutine*)GetPostingHistoryRoutine:(Web_x0020_Service_ElementGetPostingHistoryRoutine *)aGetPostingHistoryRoutine success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetPrevRoomAssignmentList:(Web_x0020_Service_ElementGetPrevRoomAssignmentList *)aGetPrevRoomAssignmentList;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetPrevRoomAssignmentList*)GetPrevRoomAssignmentList:(Web_x0020_Service_ElementGetPrevRoomAssignmentList *)aGetPrevRoomAssignmentList success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetProfileNote:(Web_x0020_Service_ElementGetProfileNote *)aGetProfileNote;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetProfileNote*)GetProfileNote:(Web_x0020_Service_ElementGetProfileNote *)aGetProfileNote success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetReceivedAdHocMessage:(Web_x0020_Service_ElementGetReceivedAdHocMessage *)aGetReceivedAdHocMessage;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetReceivedAdHocMessage*)GetReceivedAdHocMessage:(Web_x0020_Service_ElementGetReceivedAdHocMessage *)aGetReceivedAdHocMessage success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetReceivedAdHocMessageRoutine:(Web_x0020_Service_ElementGetReceivedAdHocMessageRoutine *)aGetReceivedAdHocMessageRoutine;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetReceivedAdHocMessageRoutine*)GetReceivedAdHocMessageRoutine:(Web_x0020_Service_ElementGetReceivedAdHocMessageRoutine *)aGetReceivedAdHocMessageRoutine success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetReleaseRoomsList:(Web_x0020_Service_ElementGetReleaseRoomsList *)aGetReleaseRoomsList;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetReleaseRoomsList*)GetReleaseRoomsList:(Web_x0020_Service_ElementGetReleaseRoomsList *)aGetReleaseRoomsList success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetRoomAssignmentList:(Web_x0020_Service_ElementGetRoomAssignmentList *)aGetRoomAssignmentList;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetRoomAssignmentList*)GetRoomAssignmentList:(Web_x0020_Service_ElementGetRoomAssignmentList *)aGetRoomAssignmentList success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetRoomDetail:(Web_x0020_Service_ElementGetRoomDetail *)aGetRoomDetail;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetRoomDetail*)GetRoomDetail:(Web_x0020_Service_ElementGetRoomDetail *)aGetRoomDetail success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetRoomDetailsRoutine:(Web_x0020_Service_ElementGetRoomDetailsRoutine *)aGetRoomDetailsRoutine;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetRoomDetailsRoutine*)GetRoomDetailsRoutine:(Web_x0020_Service_ElementGetRoomDetailsRoutine *)aGetRoomDetailsRoutine success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetRoomRemarks:(Web_x0020_Service_ElementGetRoomRemarks *)aGetRoomRemarks;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetRoomRemarks*)GetRoomRemarks:(Web_x0020_Service_ElementGetRoomRemarks *)aGetRoomRemarks success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetRoomSectionList:(Web_x0020_Service_ElementGetRoomSectionList *)aGetRoomSectionList;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetRoomSectionList*)GetRoomSectionList:(Web_x0020_Service_ElementGetRoomSectionList *)aGetRoomSectionList success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetRoomSetGuideByRoomType:(Web_x0020_Service_ElementGetRoomSetGuideByRoomType *)aGetRoomSetGuideByRoomType;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetRoomSetGuideByRoomType*)GetRoomSetGuideByRoomType:(Web_x0020_Service_ElementGetRoomSetGuideByRoomType *)aGetRoomSetGuideByRoomType success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetRoomSetGuideList:(Web_x0020_Service_ElementGetRoomSetGuideList *)aGetRoomSetGuideList;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetRoomSetGuideList*)GetRoomSetGuideList:(Web_x0020_Service_ElementGetRoomSetGuideList *)aGetRoomSetGuideList success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetRoomStatusList:(Web_x0020_Service_ElementGetRoomStatusList *)aGetRoomStatusList;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetRoomStatusList*)GetRoomStatusList:(Web_x0020_Service_ElementGetRoomStatusList *)aGetRoomStatusList success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetRoomTypeByRoomNoRoutine:(Web_x0020_Service_ElementGetRoomTypeByRoomNoRoutine *)aGetRoomTypeByRoomNoRoutine;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetRoomTypeByRoomNoRoutine*)GetRoomTypeByRoomNoRoutine:(Web_x0020_Service_ElementGetRoomTypeByRoomNoRoutine *)aGetRoomTypeByRoomNoRoutine success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetRoomTypeInventoryRoutine:(Web_x0020_Service_ElementGetRoomTypeInventoryRoutine *)aGetRoomTypeInventoryRoutine;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetRoomTypeInventoryRoutine*)GetRoomTypeInventoryRoutine:(Web_x0020_Service_ElementGetRoomTypeInventoryRoutine *)aGetRoomTypeInventoryRoutine success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetRoomTypeList:(Web_x0020_Service_ElementGetRoomTypeList *)aGetRoomTypeList;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetRoomTypeList*)GetRoomTypeList:(Web_x0020_Service_ElementGetRoomTypeList *)aGetRoomTypeList success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetSecretsRoutine:(Web_x0020_Service_ElementGetSecretsRoutine *)aGetSecretsRoutine;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetSecretsRoutine*)GetSecretsRoutine:(Web_x0020_Service_ElementGetSecretsRoutine *)aGetSecretsRoutine success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetSendAdHocMessageRoutine:(Web_x0020_Service_ElementGetSendAdHocMessageRoutine *)aGetSendAdHocMessageRoutine;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetSendAdHocMessageRoutine*)GetSendAdHocMessageRoutine:(Web_x0020_Service_ElementGetSendAdHocMessageRoutine *)aGetSendAdHocMessageRoutine success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetSentAdHocMessage:(Web_x0020_Service_ElementGetSentAdHocMessage *)aGetSentAdHocMessage;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetSentAdHocMessage*)GetSentAdHocMessage:(Web_x0020_Service_ElementGetSentAdHocMessage *)aGetSentAdHocMessage success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetStillLoggedIn:(Web_x0020_Service_ElementGetStillLoggedIn *)aGetStillLoggedIn;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetStillLoggedIn*)GetStillLoggedIn:(Web_x0020_Service_ElementGetStillLoggedIn *)aGetStillLoggedIn success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetSupervisorFiltersRoutine:(Web_x0020_Service_ElementGetSupervisorFiltersRoutine *)aGetSupervisorFiltersRoutine;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetSupervisorFiltersRoutine*)GetSupervisorFiltersRoutine:(Web_x0020_Service_ElementGetSupervisorFiltersRoutine *)aGetSupervisorFiltersRoutine success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetSupervisorFindFloorList:(Web_x0020_Service_ElementGetSupervisorFindFloorList *)aGetSupervisorFindFloorList;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetSupervisorFindFloorList*)GetSupervisorFindFloorList:(Web_x0020_Service_ElementGetSupervisorFindFloorList *)aGetSupervisorFindFloorList success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetSupervisorFindRoomAssignmentLists:(Web_x0020_Service_ElementGetSupervisorFindRoomAssignmentLists *)aGetSupervisorFindRoomAssignmentLists;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetSupervisorFindRoomAssignmentLists*)GetSupervisorFindRoomAssignmentLists:(Web_x0020_Service_ElementGetSupervisorFindRoomAssignmentLists *)aGetSupervisorFindRoomAssignmentLists success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetUnassignRoomList:(Web_x0020_Service_ElementGetUnassignRoomList *)aGetUnassignRoomList;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetUnassignRoomList*)GetUnassignRoomList:(Web_x0020_Service_ElementGetUnassignRoomList *)aGetUnassignRoomList success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetUserDetailsRoutine:(Web_x0020_Service_ElementGetUserDetailsRoutine *)aGetUserDetailsRoutine;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetUserDetailsRoutine*)GetUserDetailsRoutine:(Web_x0020_Service_ElementGetUserDetailsRoutine *)aGetUserDetailsRoutine success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetUserList:(Web_x0020_Service_ElementGetUserList *)aGetUserList;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetUserList*)GetUserList:(Web_x0020_Service_ElementGetUserList *)aGetUserList success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetVersion:(Web_x0020_Service_ElementGetVersion *)aGetVersion;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetVersion*)GetVersion:(Web_x0020_Service_ElementGetVersion *)aGetVersion success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetZoneList:(Web_x0020_Service_ElementGetZoneList *)aGetZoneList;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetZoneList*)GetZoneList:(Web_x0020_Service_ElementGetZoneList *)aGetZoneList success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)GetZoneRoomList:(Web_x0020_Service_ElementGetZoneRoomList *)aGetZoneRoomList;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetZoneRoomList*)GetZoneRoomList:(Web_x0020_Service_ElementGetZoneRoomList *)aGetZoneRoomList success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)LogInvalidStartRoom:(Web_x0020_Service_ElementLogInvalidStartRoom *)aLogInvalidStartRoom;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_LogInvalidStartRoom*)LogInvalidStartRoom:(Web_x0020_Service_ElementLogInvalidStartRoom *)aLogInvalidStartRoom success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)PostAdditionalJobStatusUpdateRoutine:(Web_x0020_Service_ElementPostAdditionalJobStatusUpdateRoutine *)aPostAdditionalJobStatusUpdateRoutine;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostAdditionalJobStatusUpdateRoutine*)PostAdditionalJobStatusUpdateRoutine:(Web_x0020_Service_ElementPostAdditionalJobStatusUpdateRoutine *)aPostAdditionalJobStatusUpdateRoutine success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)PostAmenitiesItem:(Web_x0020_Service_ElementPostAmenitiesItem *)aPostAmenitiesItem;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostAmenitiesItem*)PostAmenitiesItem:(Web_x0020_Service_ElementPostAmenitiesItem *)aPostAmenitiesItem success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)PostAmenitiesItemsList:(Web_x0020_Service_ElementPostAmenitiesItemsList *)aPostAmenitiesItemsList;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostAmenitiesItemsList*)PostAmenitiesItemsList:(Web_x0020_Service_ElementPostAmenitiesItemsList *)aPostAmenitiesItemsList success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)PostChecklistItem:(Web_x0020_Service_ElementPostChecklistItem *)aPostChecklistItem;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostChecklistItem*)PostChecklistItem:(Web_x0020_Service_ElementPostChecklistItem *)aPostChecklistItem success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)PostChecklistItems:(Web_x0020_Service_ElementPostChecklistItems *)aPostChecklistItems;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostChecklistItems*)PostChecklistItems:(Web_x0020_Service_ElementPostChecklistItems *)aPostChecklistItems success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)PostECAttachPhoto:(Web_x0020_Service_ElementPostECAttachPhoto *)aPostECAttachPhoto;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostECAttachPhoto*)PostECAttachPhoto:(Web_x0020_Service_ElementPostECAttachPhoto *)aPostECAttachPhoto success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)PostEngineeringCase:(Web_x0020_Service_ElementPostEngineeringCase *)aPostEngineeringCase;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostEngineeringCase*)PostEngineeringCase:(Web_x0020_Service_ElementPostEngineeringCase *)aPostEngineeringCase success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)PostLFAttachPhoto:(Web_x0020_Service_ElementPostLFAttachPhoto *)aPostLFAttachPhoto;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostLFAttachPhoto*)PostLFAttachPhoto:(Web_x0020_Service_ElementPostLFAttachPhoto *)aPostLFAttachPhoto success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)PostLaundryItem:(Web_x0020_Service_ElementPostLaundryItem *)aPostLaundryItem;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostLaundryItem*)PostLaundryItem:(Web_x0020_Service_ElementPostLaundryItem *)aPostLaundryItem success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)PostLaundryOrder:(Web_x0020_Service_ElementPostLaundryOrder *)aPostLaundryOrder;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostLaundryOrder*)PostLaundryOrder:(Web_x0020_Service_ElementPostLaundryOrder *)aPostLaundryOrder success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)PostLaundrySpecialInstruction:(Web_x0020_Service_ElementPostLaundrySpecialInstruction *)aPostLaundrySpecialInstruction;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostLaundrySpecialInstruction*)PostLaundrySpecialInstruction:(Web_x0020_Service_ElementPostLaundrySpecialInstruction *)aPostLaundrySpecialInstruction success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)PostLinenItem:(Web_x0020_Service_ElementPostLinenItem *)aPostLinenItem;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostLinenItem*)PostLinenItem:(Web_x0020_Service_ElementPostLinenItem *)aPostLinenItem success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)PostLinenItems:(Web_x0020_Service_ElementPostLinenItems *)aPostLinenItems;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostLinenItems*)PostLinenItems:(Web_x0020_Service_ElementPostLinenItems *)aPostLinenItems success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)PostLogout:(Web_x0020_Service_ElementPostLogout *)aPostLogout;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostLogout*)PostLogout:(Web_x0020_Service_ElementPostLogout *)aPostLogout success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)PostLostAndFoundRoutine:(Web_x0020_Service_ElementPostLostAndFoundRoutine *)aPostLostAndFoundRoutine;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostLostAndFoundRoutine*)PostLostAndFoundRoutine:(Web_x0020_Service_ElementPostLostAndFoundRoutine *)aPostLostAndFoundRoutine success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)PostLostFound:(Web_x0020_Service_ElementPostLostFound *)aPostLostFound;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostLostFound*)PostLostFound:(Web_x0020_Service_ElementPostLostFound *)aPostLostFound success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)PostMessage:(Web_x0020_Service_ElementPostMessage *)aPostMessage;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostMessage*)PostMessage:(Web_x0020_Service_ElementPostMessage *)aPostMessage success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)PostMessageAttachPhoto:(Web_x0020_Service_ElementPostMessageAttachPhoto *)aPostMessageAttachPhoto;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostMessageAttachPhoto*)PostMessageAttachPhoto:(Web_x0020_Service_ElementPostMessageAttachPhoto *)aPostMessageAttachPhoto success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)PostMinibarItem:(Web_x0020_Service_ElementPostMinibarItem *)aPostMinibarItem;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostMinibarItem*)PostMinibarItem:(Web_x0020_Service_ElementPostMinibarItem *)aPostMinibarItem success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)PostMinibarItems:(Web_x0020_Service_ElementPostMinibarItems *)aPostMinibarItems;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostMinibarItems*)PostMinibarItems:(Web_x0020_Service_ElementPostMinibarItems *)aPostMinibarItems success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)PostMinibarOrder:(Web_x0020_Service_ElementPostMinibarOrder *)aPostMinibarOrder;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostMinibarOrder*)PostMinibarOrder:(Web_x0020_Service_ElementPostMinibarOrder *)aPostMinibarOrder success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)PostPostingHistoryRoutine:(Web_x0020_Service_ElementPostPostingHistoryRoutine *)aPostPostingHistoryRoutine;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostPostingHistoryRoutine*)PostPostingHistoryRoutine:(Web_x0020_Service_ElementPostPostingHistoryRoutine *)aPostPostingHistoryRoutine success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)PostUnassignRoomRoutine:(Web_x0020_Service_ElementPostUnassignRoomRoutine *)aPostUnassignRoomRoutine;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostUnassignRoomRoutine*)PostUnassignRoomRoutine:(Web_x0020_Service_ElementPostUnassignRoomRoutine *)aPostUnassignRoomRoutine success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)PostUpdateAdHocMessageRoutine:(Web_x0020_Service_ElementPostUpdateAdHocMessageRoutine *)aPostUpdateAdHocMessageRoutine;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostUpdateAdHocMessageRoutine*)PostUpdateAdHocMessageRoutine:(Web_x0020_Service_ElementPostUpdateAdHocMessageRoutine *)aPostUpdateAdHocMessageRoutine success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)PostUpdateDeviceTokenRoutine:(Web_x0020_Service_ElementPostUpdateDeviceTokenRoutine *)aPostUpdateDeviceTokenRoutine;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostUpdateDeviceTokenRoutine*)PostUpdateDeviceTokenRoutine:(Web_x0020_Service_ElementPostUpdateDeviceTokenRoutine *)aPostUpdateDeviceTokenRoutine success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)PostUpdateOtherActivityStatusRoutine:(Web_x0020_Service_ElementPostUpdateOtherActivityStatusRoutine *)aPostUpdateOtherActivityStatusRoutine;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostUpdateOtherActivityStatusRoutine*)PostUpdateOtherActivityStatusRoutine:(Web_x0020_Service_ElementPostUpdateOtherActivityStatusRoutine *)aPostUpdateOtherActivityStatusRoutine success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)PostUpdateRoomDetailsRoutine:(Web_x0020_Service_ElementPostUpdateRoomDetailsRoutine *)aPostUpdateRoomDetailsRoutine;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostUpdateRoomDetailsRoutine*)PostUpdateRoomDetailsRoutine:(Web_x0020_Service_ElementPostUpdateRoomDetailsRoutine *)aPostUpdateRoomDetailsRoutine success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)PostWSLog:(Web_x0020_Service_ElementPostWSLog *)aPostWSLog;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostWSLog*)PostWSLog:(Web_x0020_Service_ElementPostWSLog *)aPostWSLog success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)PosteCnAhMsg:(Web_x0020_Service_ElementPosteCnAhMsg *)aPosteCnAhMsg;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_PosteCnAhMsg*)PosteCnAhMsg:(Web_x0020_Service_ElementPosteCnAhMsg *)aPosteCnAhMsg success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)PosteCnJob:(Web_x0020_Service_ElementPosteCnJob *)aPosteCnJob;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_PosteCnJob*)PosteCnJob:(Web_x0020_Service_ElementPosteCnJob *)aPosteCnJob success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)PostingLinenItems:(Web_x0020_Service_ElementPostingLinenItems *)aPostingLinenItems;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostingLinenItems*)PostingLinenItems:(Web_x0020_Service_ElementPostingLinenItems *)aPostingLinenItems success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)PostingPanicAlert:(Web_x0020_Service_ElementPostingPanicAlert *)aPostingPanicAlert;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostingPanicAlert*)PostingPanicAlert:(Web_x0020_Service_ElementPostingPanicAlert *)aPostingPanicAlert success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)RemoveLaundryItem:(Web_x0020_Service_ElementRemoveLaundryItem *)aRemoveLaundryItem;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_RemoveLaundryItem*)RemoveLaundryItem:(Web_x0020_Service_ElementRemoveLaundryItem *)aRemoveLaundryItem success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)StartAdditionalJob:(Web_x0020_Service_ElementStartAdditionalJob *)aStartAdditionalJob;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_StartAdditionalJob*)StartAdditionalJob:(Web_x0020_Service_ElementStartAdditionalJob *)aStartAdditionalJob success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)StopAdditionalJob:(Web_x0020_Service_ElementStopAdditionalJob *)aStopAdditionalJob;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_StopAdditionalJob*)StopAdditionalJob:(Web_x0020_Service_ElementStopAdditionalJob *)aStopAdditionalJob success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)UpdateAdHocMessageStatus:(Web_x0020_Service_ElementUpdateAdHocMessageStatus *)aUpdateAdHocMessageStatus;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_UpdateAdHocMessageStatus*)UpdateAdHocMessageStatus:(Web_x0020_Service_ElementUpdateAdHocMessageStatus *)aUpdateAdHocMessageStatus success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)UpdateAdditionalJobRemark:(Web_x0020_Service_ElementUpdateAdditionalJobRemark *)aUpdateAdditionalJobRemark;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_UpdateAdditionalJobRemark*)UpdateAdditionalJobRemark:(Web_x0020_Service_ElementUpdateAdditionalJobRemark *)aUpdateAdditionalJobRemark success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)UpdateAdditionalJobStatus:(Web_x0020_Service_ElementUpdateAdditionalJobStatus *)aUpdateAdditionalJobStatus;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_UpdateAdditionalJobStatus*)UpdateAdditionalJobStatus:(Web_x0020_Service_ElementUpdateAdditionalJobStatus *)aUpdateAdditionalJobStatus success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)UpdateCleaningStatus:(Web_x0020_Service_ElementUpdateCleaningStatus *)aUpdateCleaningStatus;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_UpdateCleaningStatus*)UpdateCleaningStatus:(Web_x0020_Service_ElementUpdateCleaningStatus *)aUpdateCleaningStatus success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)UpdateGuestInfo:(Web_x0020_Service_ElementUpdateGuestInfo *)aUpdateGuestInfo;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_UpdateGuestInfo*)UpdateGuestInfo:(Web_x0020_Service_ElementUpdateGuestInfo *)aUpdateGuestInfo success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)UpdateInspection:(Web_x0020_Service_ElementUpdateInspection *)aUpdateInspection;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_UpdateInspection*)UpdateInspection:(Web_x0020_Service_ElementUpdateInspection *)aUpdateInspection success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)UpdatePopupMsgStatus:(Web_x0020_Service_ElementUpdatePopupMsgStatus *)aUpdatePopupMsgStatus;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_UpdatePopupMsgStatus*)UpdatePopupMsgStatus:(Web_x0020_Service_ElementUpdatePopupMsgStatus *)aUpdatePopupMsgStatus success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)UpdateRoomAssignment:(Web_x0020_Service_ElementUpdateRoomAssignment *)aUpdateRoomAssignment;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_UpdateRoomAssignment*)UpdateRoomAssignment:(Web_x0020_Service_ElementUpdateRoomAssignment *)aUpdateRoomAssignment success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)UpdateRoomCleaningStatus:(Web_x0020_Service_ElementUpdateRoomCleaningStatus *)aUpdateRoomCleaningStatus;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_UpdateRoomCleaningStatus*)UpdateRoomCleaningStatus:(Web_x0020_Service_ElementUpdateRoomCleaningStatus *)aUpdateRoomCleaningStatus success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)UpdateRoomDetails:(Web_x0020_Service_ElementUpdateRoomDetails *)aUpdateRoomDetails;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_UpdateRoomDetails*)UpdateRoomDetails:(Web_x0020_Service_ElementUpdateRoomDetails *)aUpdateRoomDetails success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)UpdateRoomRemarks:(Web_x0020_Service_ElementUpdateRoomRemarks *)aUpdateRoomRemarks;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_UpdateRoomRemarks*)UpdateRoomRemarks:(Web_x0020_Service_ElementUpdateRoomRemarks *)aUpdateRoomRemarks success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)UpdateRoomStatus:(Web_x0020_Service_ElementUpdateRoomStatus *)aUpdateRoomStatus;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_UpdateRoomStatus*)UpdateRoomStatus:(Web_x0020_Service_ElementUpdateRoomStatus *)aUpdateRoomStatus success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *)ValidateRoomNo:(Web_x0020_Service_ElementValidateRoomNo *)aValidateRoomNo;
- (Web_x0020_Service_Web_x0020_ServiceSoapBinding_ValidateRoomNo*)ValidateRoomNo:(Web_x0020_Service_ElementValidateRoomNo *)aValidateRoomNo success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;
@end

@interface Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation : NSOperation <NSURLConnectionDelegate>
@property(nonatomic, strong) Web_x0020_Service_Web_x0020_ServiceSoapBinding *binding;
@property(nonatomic, strong, readonly) Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse *response;
@property(nonatomic, strong) NSMutableData *responseData;
@property(nonatomic, strong) NSURLConnection *urlConnection;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error;

/**
 * Cancels connection. Response has error with code kCFURLErrorCancelled in domain kCFErrorDomainCFNetwork.
 */
- (void)cancel;

@end

@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_AddReassignRoom : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementAddReassignRoom * AddReassignRoom;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	AddReassignRoom:(Web_x0020_Service_ElementAddReassignRoom *)aAddReassignRoom
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_AssignRoom : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementAssignRoom * AssignRoom;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	AssignRoom:(Web_x0020_Service_ElementAssignRoom *)aAssignRoom
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_AuthenticateUser : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementAuthenticateUser * AuthenticateUser;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	AuthenticateUser:(Web_x0020_Service_ElementAuthenticateUser *)aAuthenticateUser
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_ChangeRoomAssignmentSequence : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementChangeRoomAssignmentSequence * ChangeRoomAssignmentSequence;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	ChangeRoomAssignmentSequence:(Web_x0020_Service_ElementChangeRoomAssignmentSequence *)aChangeRoomAssignmentSequence
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_CompleteAdditionalJob : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementCompleteAdditionalJob * CompleteAdditionalJob;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	CompleteAdditionalJob:(Web_x0020_Service_ElementCompleteAdditionalJob *)aCompleteAdditionalJob
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_FindAttendant : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementFindAttendant * FindAttendant;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	FindAttendant:(Web_x0020_Service_ElementFindAttendant *)aFindAttendant
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_FindGuestStayedHistoryRoutine : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementFindGuestStayedHistoryRoutine * FindGuestStayedHistoryRoutine;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	FindGuestStayedHistoryRoutine:(Web_x0020_Service_ElementFindGuestStayedHistoryRoutine *)aFindGuestStayedHistoryRoutine
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_FindRoomByRoomNo : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementFindRoomByRoomNo * FindRoomByRoomNo;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	FindRoomByRoomNo:(Web_x0020_Service_ElementFindRoomByRoomNo *)aFindRoomByRoomNo
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetAccessRights : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetAccessRights * GetAccessRights;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetAccessRights:(Web_x0020_Service_ElementGetAccessRights *)aGetAccessRights
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetAdditionalJobByRoomNo : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetAdditionalJobByRoomNo * GetAdditionalJobByRoomNo;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetAdditionalJobByRoomNo:(Web_x0020_Service_ElementGetAdditionalJobByRoomNo *)aGetAdditionalJobByRoomNo
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetAdditionalJobCount : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetAdditionalJobCount * GetAdditionalJobCount;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetAdditionalJobCount:(Web_x0020_Service_ElementGetAdditionalJobCount *)aGetAdditionalJobCount
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetAdditionalJobDetail : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetAdditionalJobDetail * GetAdditionalJobDetail;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetAdditionalJobDetail:(Web_x0020_Service_ElementGetAdditionalJobDetail *)aGetAdditionalJobDetail
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetAdditionalJobFloorList : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetAdditionalJobFloorList * GetAdditionalJobFloorList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetAdditionalJobFloorList:(Web_x0020_Service_ElementGetAdditionalJobFloorList *)aGetAdditionalJobFloorList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetAdditionalJobOfRoom : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetAdditionalJobOfRoom * GetAdditionalJobOfRoom;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetAdditionalJobOfRoom:(Web_x0020_Service_ElementGetAdditionalJobOfRoom *)aGetAdditionalJobOfRoom
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetAdditionalJobRoomByFloor : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetAdditionalJobRoomByFloor * GetAdditionalJobRoomByFloor;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetAdditionalJobRoomByFloor:(Web_x0020_Service_ElementGetAdditionalJobRoomByFloor *)aGetAdditionalJobRoomByFloor
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetAdditionalJobStatusesRoutine : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetAdditionalJobStatusesRoutine * GetAdditionalJobStatusesRoutine;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetAdditionalJobStatusesRoutine:(Web_x0020_Service_ElementGetAdditionalJobStatusesRoutine *)aGetAdditionalJobStatusesRoutine
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetAdditionalJobTask : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetAdditionalJobTask * GetAdditionalJobTask;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetAdditionalJobTask:(Web_x0020_Service_ElementGetAdditionalJobTask *)aGetAdditionalJobTask
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetAllChecklist : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetAllChecklist * GetAllChecklist;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetAllChecklist:(Web_x0020_Service_ElementGetAllChecklist *)aGetAllChecklist
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetAmenitiesCategoryList : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetAmenitiesCategoryList * GetAmenitiesCategoryList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetAmenitiesCategoryList:(Web_x0020_Service_ElementGetAmenitiesCategoryList *)aGetAmenitiesCategoryList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetAmenitiesItemList : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetAmenitiesItemList * GetAmenitiesItemList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetAmenitiesItemList:(Web_x0020_Service_ElementGetAmenitiesItemList *)aGetAmenitiesItemList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetAmenitiesLocationList : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetAmenitiesLocationList * GetAmenitiesLocationList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetAmenitiesLocationList:(Web_x0020_Service_ElementGetAmenitiesLocationList *)aGetAmenitiesLocationList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetBuildingList : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetBuildingList * GetBuildingList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetBuildingList:(Web_x0020_Service_ElementGetBuildingList *)aGetBuildingList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetCheckListItemScoreForMaid : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetCheckListItemScoreForMaid * GetCheckListItemScoreForMaid;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetCheckListItemScoreForMaid:(Web_x0020_Service_ElementGetCheckListItemScoreForMaid *)aGetCheckListItemScoreForMaid
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetChecklistCategoryList : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetChecklistCategoryList * GetChecklistCategoryList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetChecklistCategoryList:(Web_x0020_Service_ElementGetChecklistCategoryList *)aGetChecklistCategoryList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetChecklistItemList : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetChecklistItemList * GetChecklistItemList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetChecklistItemList:(Web_x0020_Service_ElementGetChecklistItemList *)aGetChecklistItemList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetChecklistRoomType : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetChecklistRoomType * GetChecklistRoomType;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetChecklistRoomType:(Web_x0020_Service_ElementGetChecklistRoomType *)aGetChecklistRoomType
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetCleaningStatusList : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetCleaningStatusList * GetCleaningStatusList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetCleaningStatusList:(Web_x0020_Service_ElementGetCleaningStatusList *)aGetCleaningStatusList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetCommonConfigurations : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetCommonConfigurations * GetCommonConfigurations;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetCommonConfigurations:(Web_x0020_Service_ElementGetCommonConfigurations *)aGetCommonConfigurations
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetDiscrepantRoomStatus : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetDiscrepantRoomStatus * GetDiscrepantRoomStatus;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetDiscrepantRoomStatus:(Web_x0020_Service_ElementGetDiscrepantRoomStatus *)aGetDiscrepantRoomStatus
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetEngineeringCategoryList : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetEngineeringCategoryList * GetEngineeringCategoryList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetEngineeringCategoryList:(Web_x0020_Service_ElementGetEngineeringCategoryList *)aGetEngineeringCategoryList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetEngineeringItem : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetEngineeringItem * GetEngineeringItem;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetEngineeringItem:(Web_x0020_Service_ElementGetEngineeringItem *)aGetEngineeringItem
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetFindFloorDetailsList : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetFindFloorDetailsList * GetFindFloorDetailsList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetFindFloorDetailsList:(Web_x0020_Service_ElementGetFindFloorDetailsList *)aGetFindFloorDetailsList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetFindRoomAssignmentList : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetFindRoomAssignmentList * GetFindRoomAssignmentList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetFindRoomAssignmentList:(Web_x0020_Service_ElementGetFindRoomAssignmentList *)aGetFindRoomAssignmentList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetFindRoomAssignmentLists : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetFindRoomAssignmentLists * GetFindRoomAssignmentLists;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetFindRoomAssignmentLists:(Web_x0020_Service_ElementGetFindRoomAssignmentLists *)aGetFindRoomAssignmentLists
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetFindRoomDetailsList : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetFindRoomDetailsList * GetFindRoomDetailsList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetFindRoomDetailsList:(Web_x0020_Service_ElementGetFindRoomDetailsList *)aGetFindRoomDetailsList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetFloorList : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetFloorList * GetFloorList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetFloorList:(Web_x0020_Service_ElementGetFloorList *)aGetFloorList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetGuestInfo : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetGuestInfo * GetGuestInfo;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetGuestInfo:(Web_x0020_Service_ElementGetGuestInfo *)aGetGuestInfo
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetGuestInfoByRoomID : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetGuestInfoByRoomID * GetGuestInfoByRoomID;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetGuestInfoByRoomID:(Web_x0020_Service_ElementGetGuestInfoByRoomID *)aGetGuestInfoByRoomID
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetGuestInformation : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetGuestInformation * GetGuestInformation;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetGuestInformation:(Web_x0020_Service_ElementGetGuestInformation *)aGetGuestInformation
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetHotelInfo : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetHotelInfo * GetHotelInfo;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetHotelInfo:(Web_x0020_Service_ElementGetHotelInfo *)aGetHotelInfo
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetInspectionStatus : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetInspectionStatus * GetInspectionStatus;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetInspectionStatus:(Web_x0020_Service_ElementGetInspectionStatus *)aGetInspectionStatus
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetLFCategoryList : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetLFCategoryList * GetLFCategoryList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetLFCategoryList:(Web_x0020_Service_ElementGetLFCategoryList *)aGetLFCategoryList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetLFColorList : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetLFColorList * GetLFColorList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetLFColorList:(Web_x0020_Service_ElementGetLFColorList *)aGetLFColorList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetLFItemList : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetLFItemList * GetLFItemList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetLFItemList:(Web_x0020_Service_ElementGetLFItemList *)aGetLFItemList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetLanguageList : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetLanguageList * GetLanguageList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetLanguageList:(Web_x0020_Service_ElementGetLanguageList *)aGetLanguageList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetLaundryCategoryList : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetLaundryCategoryList * GetLaundryCategoryList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetLaundryCategoryList:(Web_x0020_Service_ElementGetLaundryCategoryList *)aGetLaundryCategoryList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetLaundryItemList : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetLaundryItemList * GetLaundryItemList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetLaundryItemList:(Web_x0020_Service_ElementGetLaundryItemList *)aGetLaundryItemList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetLaundryItemPriceList : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetLaundryItemPriceList * GetLaundryItemPriceList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetLaundryItemPriceList:(Web_x0020_Service_ElementGetLaundryItemPriceList *)aGetLaundryItemPriceList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetLaundryServiceList : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetLaundryServiceList * GetLaundryServiceList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetLaundryServiceList:(Web_x0020_Service_ElementGetLaundryServiceList *)aGetLaundryServiceList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetLaundrySpecialInstructionList : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetLaundrySpecialInstructionList * GetLaundrySpecialInstructionList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetLaundrySpecialInstructionList:(Web_x0020_Service_ElementGetLaundrySpecialInstructionList *)aGetLaundrySpecialInstructionList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetLinenCategoryList : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetLinenCategoryList * GetLinenCategoryList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetLinenCategoryList:(Web_x0020_Service_ElementGetLinenCategoryList *)aGetLinenCategoryList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetLinenItemList : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetLinenItemList * GetLinenItemList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetLinenItemList:(Web_x0020_Service_ElementGetLinenItemList *)aGetLinenItemList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetLocationList : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetLocationList * GetLocationList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetLocationList:(Web_x0020_Service_ElementGetLocationList *)aGetLocationList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetMessageAttachPhoto : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetMessageAttachPhoto * GetMessageAttachPhoto;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetMessageAttachPhoto:(Web_x0020_Service_ElementGetMessageAttachPhoto *)aGetMessageAttachPhoto
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetMessageCategoryList : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetMessageCategoryList * GetMessageCategoryList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetMessageCategoryList:(Web_x0020_Service_ElementGetMessageCategoryList *)aGetMessageCategoryList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetMessageCategoryList2 : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetMessageCategoryList2 * GetMessageCategoryList2;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetMessageCategoryList2:(Web_x0020_Service_ElementGetMessageCategoryList2 *)aGetMessageCategoryList2
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetMessageItemList : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetMessageItemList * GetMessageItemList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetMessageItemList:(Web_x0020_Service_ElementGetMessageItemList *)aGetMessageItemList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetMessageItemList2 : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetMessageItemList2 * GetMessageItemList2;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetMessageItemList2:(Web_x0020_Service_ElementGetMessageItemList2 *)aGetMessageItemList2
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetMessageTemplateList : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetMessageTemplateList * GetMessageTemplateList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetMessageTemplateList:(Web_x0020_Service_ElementGetMessageTemplateList *)aGetMessageTemplateList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetMinibarCategoryList : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetMinibarCategoryList * GetMinibarCategoryList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetMinibarCategoryList:(Web_x0020_Service_ElementGetMinibarCategoryList *)aGetMinibarCategoryList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetMinibarItemList : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetMinibarItemList * GetMinibarItemList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetMinibarItemList:(Web_x0020_Service_ElementGetMinibarItemList *)aGetMinibarItemList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetNewMessage : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetNewMessage * GetNewMessage;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetNewMessage:(Web_x0020_Service_ElementGetNewMessage *)aGetNewMessage
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetNoOfAdHocMessage : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetNoOfAdHocMessage * GetNoOfAdHocMessage;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetNoOfAdHocMessage:(Web_x0020_Service_ElementGetNoOfAdHocMessage *)aGetNoOfAdHocMessage
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetNoOfAdHocMessageRoutine : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetNoOfAdHocMessageRoutine * GetNoOfAdHocMessageRoutine;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetNoOfAdHocMessageRoutine:(Web_x0020_Service_ElementGetNoOfAdHocMessageRoutine *)aGetNoOfAdHocMessageRoutine
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetNumberOfGuest : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetNumberOfGuest * GetNumberOfGuest;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetNumberOfGuest:(Web_x0020_Service_ElementGetNumberOfGuest *)aGetNumberOfGuest
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetOOSBlockRoomsList : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetOOSBlockRoomsList * GetOOSBlockRoomsList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetOOSBlockRoomsList:(Web_x0020_Service_ElementGetOOSBlockRoomsList *)aGetOOSBlockRoomsList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetOtherActivitiesLocationRoutine : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetOtherActivitiesLocationRoutine * GetOtherActivitiesLocationRoutine;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetOtherActivitiesLocationRoutine:(Web_x0020_Service_ElementGetOtherActivitiesLocationRoutine *)aGetOtherActivitiesLocationRoutine
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetOtherActivitiesStatusRoutine : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetOtherActivitiesStatusRoutine * GetOtherActivitiesStatusRoutine;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetOtherActivitiesStatusRoutine:(Web_x0020_Service_ElementGetOtherActivitiesStatusRoutine *)aGetOtherActivitiesStatusRoutine
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetOtherActivityAssignmentRoutine : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetOtherActivityAssignmentRoutine * GetOtherActivityAssignmentRoutine;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetOtherActivityAssignmentRoutine:(Web_x0020_Service_ElementGetOtherActivityAssignmentRoutine *)aGetOtherActivityAssignmentRoutine
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetPanicAlertConfig : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetPanicAlertConfig * GetPanicAlertConfig;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetPanicAlertConfig:(Web_x0020_Service_ElementGetPanicAlertConfig *)aGetPanicAlertConfig
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetPopupMsgList : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetPopupMsgList * GetPopupMsgList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetPopupMsgList:(Web_x0020_Service_ElementGetPopupMsgList *)aGetPopupMsgList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetPostingHistoryRoutine : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetPostingHistoryRoutine * GetPostingHistoryRoutine;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetPostingHistoryRoutine:(Web_x0020_Service_ElementGetPostingHistoryRoutine *)aGetPostingHistoryRoutine
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetPrevRoomAssignmentList : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetPrevRoomAssignmentList * GetPrevRoomAssignmentList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetPrevRoomAssignmentList:(Web_x0020_Service_ElementGetPrevRoomAssignmentList *)aGetPrevRoomAssignmentList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetProfileNote : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetProfileNote * GetProfileNote;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetProfileNote:(Web_x0020_Service_ElementGetProfileNote *)aGetProfileNote
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetReceivedAdHocMessage : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetReceivedAdHocMessage * GetReceivedAdHocMessage;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetReceivedAdHocMessage:(Web_x0020_Service_ElementGetReceivedAdHocMessage *)aGetReceivedAdHocMessage
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetReceivedAdHocMessageRoutine : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetReceivedAdHocMessageRoutine * GetReceivedAdHocMessageRoutine;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetReceivedAdHocMessageRoutine:(Web_x0020_Service_ElementGetReceivedAdHocMessageRoutine *)aGetReceivedAdHocMessageRoutine
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetReleaseRoomsList : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetReleaseRoomsList * GetReleaseRoomsList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetReleaseRoomsList:(Web_x0020_Service_ElementGetReleaseRoomsList *)aGetReleaseRoomsList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetRoomAssignmentList : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetRoomAssignmentList * GetRoomAssignmentList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetRoomAssignmentList:(Web_x0020_Service_ElementGetRoomAssignmentList *)aGetRoomAssignmentList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetRoomDetail : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetRoomDetail * GetRoomDetail;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetRoomDetail:(Web_x0020_Service_ElementGetRoomDetail *)aGetRoomDetail
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetRoomDetailsRoutine : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetRoomDetailsRoutine * GetRoomDetailsRoutine;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetRoomDetailsRoutine:(Web_x0020_Service_ElementGetRoomDetailsRoutine *)aGetRoomDetailsRoutine
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetRoomRemarks : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetRoomRemarks * GetRoomRemarks;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetRoomRemarks:(Web_x0020_Service_ElementGetRoomRemarks *)aGetRoomRemarks
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetRoomSectionList : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetRoomSectionList * GetRoomSectionList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetRoomSectionList:(Web_x0020_Service_ElementGetRoomSectionList *)aGetRoomSectionList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetRoomSetGuideByRoomType : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetRoomSetGuideByRoomType * GetRoomSetGuideByRoomType;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetRoomSetGuideByRoomType:(Web_x0020_Service_ElementGetRoomSetGuideByRoomType *)aGetRoomSetGuideByRoomType
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetRoomSetGuideList : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetRoomSetGuideList * GetRoomSetGuideList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetRoomSetGuideList:(Web_x0020_Service_ElementGetRoomSetGuideList *)aGetRoomSetGuideList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetRoomStatusList : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetRoomStatusList * GetRoomStatusList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetRoomStatusList:(Web_x0020_Service_ElementGetRoomStatusList *)aGetRoomStatusList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetRoomTypeByRoomNoRoutine : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetRoomTypeByRoomNoRoutine * GetRoomTypeByRoomNoRoutine;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetRoomTypeByRoomNoRoutine:(Web_x0020_Service_ElementGetRoomTypeByRoomNoRoutine *)aGetRoomTypeByRoomNoRoutine
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetRoomTypeInventoryRoutine : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetRoomTypeInventoryRoutine * GetRoomTypeInventoryRoutine;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetRoomTypeInventoryRoutine:(Web_x0020_Service_ElementGetRoomTypeInventoryRoutine *)aGetRoomTypeInventoryRoutine
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetRoomTypeList : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetRoomTypeList * GetRoomTypeList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetRoomTypeList:(Web_x0020_Service_ElementGetRoomTypeList *)aGetRoomTypeList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetSecretsRoutine : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetSecretsRoutine * GetSecretsRoutine;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetSecretsRoutine:(Web_x0020_Service_ElementGetSecretsRoutine *)aGetSecretsRoutine
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetSendAdHocMessageRoutine : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetSendAdHocMessageRoutine * GetSendAdHocMessageRoutine;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetSendAdHocMessageRoutine:(Web_x0020_Service_ElementGetSendAdHocMessageRoutine *)aGetSendAdHocMessageRoutine
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetSentAdHocMessage : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetSentAdHocMessage * GetSentAdHocMessage;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetSentAdHocMessage:(Web_x0020_Service_ElementGetSentAdHocMessage *)aGetSentAdHocMessage
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetStillLoggedIn : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetStillLoggedIn * GetStillLoggedIn;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetStillLoggedIn:(Web_x0020_Service_ElementGetStillLoggedIn *)aGetStillLoggedIn
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetSupervisorFiltersRoutine : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetSupervisorFiltersRoutine * GetSupervisorFiltersRoutine;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetSupervisorFiltersRoutine:(Web_x0020_Service_ElementGetSupervisorFiltersRoutine *)aGetSupervisorFiltersRoutine
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetSupervisorFindFloorList : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetSupervisorFindFloorList * GetSupervisorFindFloorList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetSupervisorFindFloorList:(Web_x0020_Service_ElementGetSupervisorFindFloorList *)aGetSupervisorFindFloorList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetSupervisorFindRoomAssignmentLists : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetSupervisorFindRoomAssignmentLists * GetSupervisorFindRoomAssignmentLists;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetSupervisorFindRoomAssignmentLists:(Web_x0020_Service_ElementGetSupervisorFindRoomAssignmentLists *)aGetSupervisorFindRoomAssignmentLists
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetUnassignRoomList : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetUnassignRoomList * GetUnassignRoomList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetUnassignRoomList:(Web_x0020_Service_ElementGetUnassignRoomList *)aGetUnassignRoomList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetUserDetailsRoutine : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetUserDetailsRoutine * GetUserDetailsRoutine;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetUserDetailsRoutine:(Web_x0020_Service_ElementGetUserDetailsRoutine *)aGetUserDetailsRoutine
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetUserList : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetUserList * GetUserList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetUserList:(Web_x0020_Service_ElementGetUserList *)aGetUserList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetVersion : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetVersion * GetVersion;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetVersion:(Web_x0020_Service_ElementGetVersion *)aGetVersion
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetZoneList : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetZoneList * GetZoneList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetZoneList:(Web_x0020_Service_ElementGetZoneList *)aGetZoneList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_GetZoneRoomList : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetZoneRoomList * GetZoneRoomList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	GetZoneRoomList:(Web_x0020_Service_ElementGetZoneRoomList *)aGetZoneRoomList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_LogInvalidStartRoom : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementLogInvalidStartRoom * LogInvalidStartRoom;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	LogInvalidStartRoom:(Web_x0020_Service_ElementLogInvalidStartRoom *)aLogInvalidStartRoom
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostAdditionalJobStatusUpdateRoutine : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementPostAdditionalJobStatusUpdateRoutine * PostAdditionalJobStatusUpdateRoutine;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	PostAdditionalJobStatusUpdateRoutine:(Web_x0020_Service_ElementPostAdditionalJobStatusUpdateRoutine *)aPostAdditionalJobStatusUpdateRoutine
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostAmenitiesItem : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementPostAmenitiesItem * PostAmenitiesItem;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	PostAmenitiesItem:(Web_x0020_Service_ElementPostAmenitiesItem *)aPostAmenitiesItem
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostAmenitiesItemsList : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementPostAmenitiesItemsList * PostAmenitiesItemsList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	PostAmenitiesItemsList:(Web_x0020_Service_ElementPostAmenitiesItemsList *)aPostAmenitiesItemsList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostChecklistItem : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementPostChecklistItem * PostChecklistItem;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	PostChecklistItem:(Web_x0020_Service_ElementPostChecklistItem *)aPostChecklistItem
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostChecklistItems : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementPostChecklistItems * PostChecklistItems;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	PostChecklistItems:(Web_x0020_Service_ElementPostChecklistItems *)aPostChecklistItems
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostECAttachPhoto : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementPostECAttachPhoto * PostECAttachPhoto;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	PostECAttachPhoto:(Web_x0020_Service_ElementPostECAttachPhoto *)aPostECAttachPhoto
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostEngineeringCase : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementPostEngineeringCase * PostEngineeringCase;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	PostEngineeringCase:(Web_x0020_Service_ElementPostEngineeringCase *)aPostEngineeringCase
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostLFAttachPhoto : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementPostLFAttachPhoto * PostLFAttachPhoto;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	PostLFAttachPhoto:(Web_x0020_Service_ElementPostLFAttachPhoto *)aPostLFAttachPhoto
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostLaundryItem : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementPostLaundryItem * PostLaundryItem;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	PostLaundryItem:(Web_x0020_Service_ElementPostLaundryItem *)aPostLaundryItem
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostLaundryOrder : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementPostLaundryOrder * PostLaundryOrder;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	PostLaundryOrder:(Web_x0020_Service_ElementPostLaundryOrder *)aPostLaundryOrder
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostLaundrySpecialInstruction : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementPostLaundrySpecialInstruction * PostLaundrySpecialInstruction;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	PostLaundrySpecialInstruction:(Web_x0020_Service_ElementPostLaundrySpecialInstruction *)aPostLaundrySpecialInstruction
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostLinenItem : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementPostLinenItem * PostLinenItem;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	PostLinenItem:(Web_x0020_Service_ElementPostLinenItem *)aPostLinenItem
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostLinenItems : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementPostLinenItems * PostLinenItems;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	PostLinenItems:(Web_x0020_Service_ElementPostLinenItems *)aPostLinenItems
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostLogout : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementPostLogout * PostLogout;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	PostLogout:(Web_x0020_Service_ElementPostLogout *)aPostLogout
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostLostAndFoundRoutine : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementPostLostAndFoundRoutine * PostLostAndFoundRoutine;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	PostLostAndFoundRoutine:(Web_x0020_Service_ElementPostLostAndFoundRoutine *)aPostLostAndFoundRoutine
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostLostFound : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementPostLostFound * PostLostFound;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	PostLostFound:(Web_x0020_Service_ElementPostLostFound *)aPostLostFound
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostMessage : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementPostMessage * PostMessage;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	PostMessage:(Web_x0020_Service_ElementPostMessage *)aPostMessage
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostMessageAttachPhoto : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementPostMessageAttachPhoto * PostMessageAttachPhoto;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	PostMessageAttachPhoto:(Web_x0020_Service_ElementPostMessageAttachPhoto *)aPostMessageAttachPhoto
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostMinibarItem : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementPostMinibarItem * PostMinibarItem;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	PostMinibarItem:(Web_x0020_Service_ElementPostMinibarItem *)aPostMinibarItem
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostMinibarItems : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementPostMinibarItems * PostMinibarItems;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	PostMinibarItems:(Web_x0020_Service_ElementPostMinibarItems *)aPostMinibarItems
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostMinibarOrder : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementPostMinibarOrder * PostMinibarOrder;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	PostMinibarOrder:(Web_x0020_Service_ElementPostMinibarOrder *)aPostMinibarOrder
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostPostingHistoryRoutine : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementPostPostingHistoryRoutine * PostPostingHistoryRoutine;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	PostPostingHistoryRoutine:(Web_x0020_Service_ElementPostPostingHistoryRoutine *)aPostPostingHistoryRoutine
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostUnassignRoomRoutine : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementPostUnassignRoomRoutine * PostUnassignRoomRoutine;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	PostUnassignRoomRoutine:(Web_x0020_Service_ElementPostUnassignRoomRoutine *)aPostUnassignRoomRoutine
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostUpdateAdHocMessageRoutine : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementPostUpdateAdHocMessageRoutine * PostUpdateAdHocMessageRoutine;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	PostUpdateAdHocMessageRoutine:(Web_x0020_Service_ElementPostUpdateAdHocMessageRoutine *)aPostUpdateAdHocMessageRoutine
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostUpdateDeviceTokenRoutine : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementPostUpdateDeviceTokenRoutine * PostUpdateDeviceTokenRoutine;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	PostUpdateDeviceTokenRoutine:(Web_x0020_Service_ElementPostUpdateDeviceTokenRoutine *)aPostUpdateDeviceTokenRoutine
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostUpdateOtherActivityStatusRoutine : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementPostUpdateOtherActivityStatusRoutine * PostUpdateOtherActivityStatusRoutine;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	PostUpdateOtherActivityStatusRoutine:(Web_x0020_Service_ElementPostUpdateOtherActivityStatusRoutine *)aPostUpdateOtherActivityStatusRoutine
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostUpdateRoomDetailsRoutine : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementPostUpdateRoomDetailsRoutine * PostUpdateRoomDetailsRoutine;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	PostUpdateRoomDetailsRoutine:(Web_x0020_Service_ElementPostUpdateRoomDetailsRoutine *)aPostUpdateRoomDetailsRoutine
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostWSLog : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementPostWSLog * PostWSLog;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	PostWSLog:(Web_x0020_Service_ElementPostWSLog *)aPostWSLog
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_PosteCnAhMsg : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementPosteCnAhMsg * PosteCnAhMsg;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	PosteCnAhMsg:(Web_x0020_Service_ElementPosteCnAhMsg *)aPosteCnAhMsg
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_PosteCnJob : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementPosteCnJob * PosteCnJob;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	PosteCnJob:(Web_x0020_Service_ElementPosteCnJob *)aPosteCnJob
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostingLinenItems : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementPostingLinenItems * PostingLinenItems;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	PostingLinenItems:(Web_x0020_Service_ElementPostingLinenItems *)aPostingLinenItems
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_PostingPanicAlert : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementPostingPanicAlert * PostingPanicAlert;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	PostingPanicAlert:(Web_x0020_Service_ElementPostingPanicAlert *)aPostingPanicAlert
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_RemoveLaundryItem : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementRemoveLaundryItem * RemoveLaundryItem;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	RemoveLaundryItem:(Web_x0020_Service_ElementRemoveLaundryItem *)aRemoveLaundryItem
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_StartAdditionalJob : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementStartAdditionalJob * StartAdditionalJob;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	StartAdditionalJob:(Web_x0020_Service_ElementStartAdditionalJob *)aStartAdditionalJob
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_StopAdditionalJob : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementStopAdditionalJob * StopAdditionalJob;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	StopAdditionalJob:(Web_x0020_Service_ElementStopAdditionalJob *)aStopAdditionalJob
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_UpdateAdHocMessageStatus : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementUpdateAdHocMessageStatus * UpdateAdHocMessageStatus;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	UpdateAdHocMessageStatus:(Web_x0020_Service_ElementUpdateAdHocMessageStatus *)aUpdateAdHocMessageStatus
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_UpdateAdditionalJobRemark : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementUpdateAdditionalJobRemark * UpdateAdditionalJobRemark;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	UpdateAdditionalJobRemark:(Web_x0020_Service_ElementUpdateAdditionalJobRemark *)aUpdateAdditionalJobRemark
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_UpdateAdditionalJobStatus : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementUpdateAdditionalJobStatus * UpdateAdditionalJobStatus;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	UpdateAdditionalJobStatus:(Web_x0020_Service_ElementUpdateAdditionalJobStatus *)aUpdateAdditionalJobStatus
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_UpdateCleaningStatus : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementUpdateCleaningStatus * UpdateCleaningStatus;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	UpdateCleaningStatus:(Web_x0020_Service_ElementUpdateCleaningStatus *)aUpdateCleaningStatus
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_UpdateGuestInfo : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementUpdateGuestInfo * UpdateGuestInfo;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	UpdateGuestInfo:(Web_x0020_Service_ElementUpdateGuestInfo *)aUpdateGuestInfo
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_UpdateInspection : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementUpdateInspection * UpdateInspection;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	UpdateInspection:(Web_x0020_Service_ElementUpdateInspection *)aUpdateInspection
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_UpdatePopupMsgStatus : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementUpdatePopupMsgStatus * UpdatePopupMsgStatus;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	UpdatePopupMsgStatus:(Web_x0020_Service_ElementUpdatePopupMsgStatus *)aUpdatePopupMsgStatus
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_UpdateRoomAssignment : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementUpdateRoomAssignment * UpdateRoomAssignment;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	UpdateRoomAssignment:(Web_x0020_Service_ElementUpdateRoomAssignment *)aUpdateRoomAssignment
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_UpdateRoomCleaningStatus : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementUpdateRoomCleaningStatus * UpdateRoomCleaningStatus;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	UpdateRoomCleaningStatus:(Web_x0020_Service_ElementUpdateRoomCleaningStatus *)aUpdateRoomCleaningStatus
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_UpdateRoomDetails : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementUpdateRoomDetails * UpdateRoomDetails;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	UpdateRoomDetails:(Web_x0020_Service_ElementUpdateRoomDetails *)aUpdateRoomDetails
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_UpdateRoomRemarks : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementUpdateRoomRemarks * UpdateRoomRemarks;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	UpdateRoomRemarks:(Web_x0020_Service_ElementUpdateRoomRemarks *)aUpdateRoomRemarks
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_UpdateRoomStatus : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementUpdateRoomStatus * UpdateRoomStatus;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	UpdateRoomStatus:(Web_x0020_Service_ElementUpdateRoomStatus *)aUpdateRoomStatus
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_ValidateRoomNo : Web_x0020_Service_Web_x0020_ServiceSoapBindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementValidateRoomNo * ValidateRoomNo;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoapBinding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoapBindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoapBindingErrorBlock)error
	ValidateRoomNo:(Web_x0020_Service_ElementValidateRoomNo *)aValidateRoomNo
;
@end

@interface Web_x0020_Service_Web_x0020_ServiceSoapBinding_envelope : NSObject
+ (NSString *)serializedFormUsingDelegate:(id)delegate;
@end

@interface Web_x0020_Service_Web_x0020_ServiceSoapBindingResponse : NSObject
@property(nonatomic, strong) NSArray *headers;
@property(nonatomic, strong) NSArray *bodyParts;
@property(nonatomic, strong) NSError *error;
@end

@class Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse;
@class Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_AddReassignRoom;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_AssignRoom;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_AuthenticateUser;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_ChangeRoomAssignmentSequence;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_CompleteAdditionalJob;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_FindAttendant;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_FindGuestStayedHistoryRoutine;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_FindRoomByRoomNo;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetAccessRights;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetAdditionalJobByRoomNo;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetAdditionalJobCount;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetAdditionalJobDetail;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetAdditionalJobFloorList;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetAdditionalJobOfRoom;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetAdditionalJobRoomByFloor;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetAdditionalJobStatusesRoutine;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetAdditionalJobTask;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetAllChecklist;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetAmenitiesCategoryList;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetAmenitiesItemList;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetAmenitiesLocationList;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetBuildingList;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetCheckListItemScoreForMaid;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetChecklistCategoryList;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetChecklistItemList;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetChecklistRoomType;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetCleaningStatusList;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetCommonConfigurations;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetDiscrepantRoomStatus;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetEngineeringCategoryList;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetEngineeringItem;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetFindFloorDetailsList;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetFindRoomAssignmentList;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetFindRoomAssignmentLists;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetFindRoomDetailsList;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetFloorList;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetGuestInfo;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetGuestInfoByRoomID;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetGuestInformation;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetHotelInfo;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetInspectionStatus;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetLFCategoryList;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetLFColorList;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetLFItemList;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetLanguageList;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetLaundryCategoryList;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetLaundryItemList;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetLaundryItemPriceList;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetLaundryServiceList;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetLaundrySpecialInstructionList;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetLinenCategoryList;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetLinenItemList;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetLocationList;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetMessageAttachPhoto;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetMessageCategoryList;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetMessageCategoryList2;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetMessageItemList;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetMessageItemList2;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetMessageTemplateList;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetMinibarCategoryList;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetMinibarItemList;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetNewMessage;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetNoOfAdHocMessage;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetNoOfAdHocMessageRoutine;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetNumberOfGuest;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetOOSBlockRoomsList;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetOtherActivitiesLocationRoutine;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetOtherActivitiesStatusRoutine;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetOtherActivityAssignmentRoutine;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetPanicAlertConfig;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetPopupMsgList;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetPostingHistoryRoutine;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetPrevRoomAssignmentList;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetProfileNote;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetReceivedAdHocMessage;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetReceivedAdHocMessageRoutine;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetReleaseRoomsList;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetRoomAssignmentList;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetRoomDetail;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetRoomDetailsRoutine;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetRoomRemarks;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetRoomSectionList;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetRoomSetGuideByRoomType;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetRoomSetGuideList;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetRoomStatusList;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetRoomTypeByRoomNoRoutine;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetRoomTypeInventoryRoutine;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetRoomTypeList;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetSecretsRoutine;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetSendAdHocMessageRoutine;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetSentAdHocMessage;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetStillLoggedIn;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetSupervisorFiltersRoutine;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetSupervisorFindFloorList;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetSupervisorFindRoomAssignmentLists;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetUnassignRoomList;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetUserDetailsRoutine;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetUserList;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetVersion;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetZoneList;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetZoneRoomList;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_LogInvalidStartRoom;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostAdditionalJobStatusUpdateRoutine;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostAmenitiesItem;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostAmenitiesItemsList;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostChecklistItem;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostChecklistItems;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostECAttachPhoto;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostEngineeringCase;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostLFAttachPhoto;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostLaundryItem;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostLaundryOrder;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostLaundrySpecialInstruction;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostLinenItem;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostLinenItems;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostLogout;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostLostAndFoundRoutine;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostLostFound;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostMessage;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostMessageAttachPhoto;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostMinibarItem;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostMinibarItems;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostMinibarOrder;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostPostingHistoryRoutine;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostUnassignRoomRoutine;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostUpdateAdHocMessageRoutine;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostUpdateDeviceTokenRoutine;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostUpdateOtherActivityStatusRoutine;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostUpdateRoomDetailsRoutine;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostWSLog;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PosteCnAhMsg;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PosteCnJob;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostingLinenItems;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostingPanicAlert;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_RemoveLaundryItem;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_StartAdditionalJob;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_StopAdditionalJob;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_UpdateAdHocMessageStatus;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_UpdateAdditionalJobRemark;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_UpdateAdditionalJobStatus;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_UpdateCleaningStatus;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_UpdateGuestInfo;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_UpdateInspection;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_UpdatePopupMsgStatus;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_UpdateRoomAssignment;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_UpdateRoomCleaningStatus;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_UpdateRoomDetails;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_UpdateRoomRemarks;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_UpdateRoomStatus;
@class Web_x0020_Service_Web_x0020_ServiceSoap12Binding_ValidateRoomNo;

typedef void (^Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)(NSArray *headers, NSArray *bodyParts);
typedef void (^Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)(NSError *error);

@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding : NSObject
@property (nonatomic, copy) NSURL *address;
@property (nonatomic) BOOL logXMLInOut;
@property (nonatomic) BOOL ignoreEmptyResponse;
@property (nonatomic) NSTimeInterval timeout;
@property (nonatomic, strong) NSMutableArray *cookies;
@property (nonatomic, strong) NSMutableDictionary *customHeaders;
@property (nonatomic, strong) id <SSLCredentialsManaging> sslManager;
@property (nonatomic, strong) SOAPSigner *soapSigner;


+ (NSTimeInterval) defaultTimeout;

- (id)initWithAddress:(NSString *)anAddress;
- (void)sendHTTPCallUsingBody:(NSString *)body soapAction:(NSString *)soapAction forOperation:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation *)operation;
- (void)addCookie:(NSHTTPCookie *)toAdd;
- (NSString *)MIMEType;

- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)AddReassignRoom:(Web_x0020_Service_ElementAddReassignRoom *)aAddReassignRoom;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_AddReassignRoom*)AddReassignRoom:(Web_x0020_Service_ElementAddReassignRoom *)aAddReassignRoom success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)AssignRoom:(Web_x0020_Service_ElementAssignRoom *)aAssignRoom;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_AssignRoom*)AssignRoom:(Web_x0020_Service_ElementAssignRoom *)aAssignRoom success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)AuthenticateUser:(Web_x0020_Service_ElementAuthenticateUser *)aAuthenticateUser;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_AuthenticateUser*)AuthenticateUser:(Web_x0020_Service_ElementAuthenticateUser *)aAuthenticateUser success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)ChangeRoomAssignmentSequence:(Web_x0020_Service_ElementChangeRoomAssignmentSequence *)aChangeRoomAssignmentSequence;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_ChangeRoomAssignmentSequence*)ChangeRoomAssignmentSequence:(Web_x0020_Service_ElementChangeRoomAssignmentSequence *)aChangeRoomAssignmentSequence success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)CompleteAdditionalJob:(Web_x0020_Service_ElementCompleteAdditionalJob *)aCompleteAdditionalJob;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_CompleteAdditionalJob*)CompleteAdditionalJob:(Web_x0020_Service_ElementCompleteAdditionalJob *)aCompleteAdditionalJob success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)FindAttendant:(Web_x0020_Service_ElementFindAttendant *)aFindAttendant;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_FindAttendant*)FindAttendant:(Web_x0020_Service_ElementFindAttendant *)aFindAttendant success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)FindGuestStayedHistoryRoutine:(Web_x0020_Service_ElementFindGuestStayedHistoryRoutine *)aFindGuestStayedHistoryRoutine;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_FindGuestStayedHistoryRoutine*)FindGuestStayedHistoryRoutine:(Web_x0020_Service_ElementFindGuestStayedHistoryRoutine *)aFindGuestStayedHistoryRoutine success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)FindRoomByRoomNo:(Web_x0020_Service_ElementFindRoomByRoomNo *)aFindRoomByRoomNo;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_FindRoomByRoomNo*)FindRoomByRoomNo:(Web_x0020_Service_ElementFindRoomByRoomNo *)aFindRoomByRoomNo success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetAccessRights:(Web_x0020_Service_ElementGetAccessRights *)aGetAccessRights;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetAccessRights*)GetAccessRights:(Web_x0020_Service_ElementGetAccessRights *)aGetAccessRights success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetAdditionalJobByRoomNo:(Web_x0020_Service_ElementGetAdditionalJobByRoomNo *)aGetAdditionalJobByRoomNo;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetAdditionalJobByRoomNo*)GetAdditionalJobByRoomNo:(Web_x0020_Service_ElementGetAdditionalJobByRoomNo *)aGetAdditionalJobByRoomNo success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetAdditionalJobCount:(Web_x0020_Service_ElementGetAdditionalJobCount *)aGetAdditionalJobCount;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetAdditionalJobCount*)GetAdditionalJobCount:(Web_x0020_Service_ElementGetAdditionalJobCount *)aGetAdditionalJobCount success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetAdditionalJobDetail:(Web_x0020_Service_ElementGetAdditionalJobDetail *)aGetAdditionalJobDetail;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetAdditionalJobDetail*)GetAdditionalJobDetail:(Web_x0020_Service_ElementGetAdditionalJobDetail *)aGetAdditionalJobDetail success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetAdditionalJobFloorList:(Web_x0020_Service_ElementGetAdditionalJobFloorList *)aGetAdditionalJobFloorList;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetAdditionalJobFloorList*)GetAdditionalJobFloorList:(Web_x0020_Service_ElementGetAdditionalJobFloorList *)aGetAdditionalJobFloorList success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetAdditionalJobOfRoom:(Web_x0020_Service_ElementGetAdditionalJobOfRoom *)aGetAdditionalJobOfRoom;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetAdditionalJobOfRoom*)GetAdditionalJobOfRoom:(Web_x0020_Service_ElementGetAdditionalJobOfRoom *)aGetAdditionalJobOfRoom success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetAdditionalJobRoomByFloor:(Web_x0020_Service_ElementGetAdditionalJobRoomByFloor *)aGetAdditionalJobRoomByFloor;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetAdditionalJobRoomByFloor*)GetAdditionalJobRoomByFloor:(Web_x0020_Service_ElementGetAdditionalJobRoomByFloor *)aGetAdditionalJobRoomByFloor success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetAdditionalJobStatusesRoutine:(Web_x0020_Service_ElementGetAdditionalJobStatusesRoutine *)aGetAdditionalJobStatusesRoutine;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetAdditionalJobStatusesRoutine*)GetAdditionalJobStatusesRoutine:(Web_x0020_Service_ElementGetAdditionalJobStatusesRoutine *)aGetAdditionalJobStatusesRoutine success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetAdditionalJobTask:(Web_x0020_Service_ElementGetAdditionalJobTask *)aGetAdditionalJobTask;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetAdditionalJobTask*)GetAdditionalJobTask:(Web_x0020_Service_ElementGetAdditionalJobTask *)aGetAdditionalJobTask success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetAllChecklist:(Web_x0020_Service_ElementGetAllChecklist *)aGetAllChecklist;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetAllChecklist*)GetAllChecklist:(Web_x0020_Service_ElementGetAllChecklist *)aGetAllChecklist success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetAmenitiesCategoryList:(Web_x0020_Service_ElementGetAmenitiesCategoryList *)aGetAmenitiesCategoryList;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetAmenitiesCategoryList*)GetAmenitiesCategoryList:(Web_x0020_Service_ElementGetAmenitiesCategoryList *)aGetAmenitiesCategoryList success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetAmenitiesItemList:(Web_x0020_Service_ElementGetAmenitiesItemList *)aGetAmenitiesItemList;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetAmenitiesItemList*)GetAmenitiesItemList:(Web_x0020_Service_ElementGetAmenitiesItemList *)aGetAmenitiesItemList success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetAmenitiesLocationList:(Web_x0020_Service_ElementGetAmenitiesLocationList *)aGetAmenitiesLocationList;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetAmenitiesLocationList*)GetAmenitiesLocationList:(Web_x0020_Service_ElementGetAmenitiesLocationList *)aGetAmenitiesLocationList success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetBuildingList:(Web_x0020_Service_ElementGetBuildingList *)aGetBuildingList;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetBuildingList*)GetBuildingList:(Web_x0020_Service_ElementGetBuildingList *)aGetBuildingList success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetCheckListItemScoreForMaid:(Web_x0020_Service_ElementGetCheckListItemScoreForMaid *)aGetCheckListItemScoreForMaid;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetCheckListItemScoreForMaid*)GetCheckListItemScoreForMaid:(Web_x0020_Service_ElementGetCheckListItemScoreForMaid *)aGetCheckListItemScoreForMaid success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetChecklistCategoryList:(Web_x0020_Service_ElementGetChecklistCategoryList *)aGetChecklistCategoryList;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetChecklistCategoryList*)GetChecklistCategoryList:(Web_x0020_Service_ElementGetChecklistCategoryList *)aGetChecklistCategoryList success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetChecklistItemList:(Web_x0020_Service_ElementGetChecklistItemList *)aGetChecklistItemList;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetChecklistItemList*)GetChecklistItemList:(Web_x0020_Service_ElementGetChecklistItemList *)aGetChecklistItemList success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetChecklistRoomType:(Web_x0020_Service_ElementGetChecklistRoomType *)aGetChecklistRoomType;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetChecklistRoomType*)GetChecklistRoomType:(Web_x0020_Service_ElementGetChecklistRoomType *)aGetChecklistRoomType success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetCleaningStatusList:(Web_x0020_Service_ElementGetCleaningStatusList *)aGetCleaningStatusList;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetCleaningStatusList*)GetCleaningStatusList:(Web_x0020_Service_ElementGetCleaningStatusList *)aGetCleaningStatusList success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetCommonConfigurations:(Web_x0020_Service_ElementGetCommonConfigurations *)aGetCommonConfigurations;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetCommonConfigurations*)GetCommonConfigurations:(Web_x0020_Service_ElementGetCommonConfigurations *)aGetCommonConfigurations success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetDiscrepantRoomStatus:(Web_x0020_Service_ElementGetDiscrepantRoomStatus *)aGetDiscrepantRoomStatus;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetDiscrepantRoomStatus*)GetDiscrepantRoomStatus:(Web_x0020_Service_ElementGetDiscrepantRoomStatus *)aGetDiscrepantRoomStatus success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetEngineeringCategoryList:(Web_x0020_Service_ElementGetEngineeringCategoryList *)aGetEngineeringCategoryList;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetEngineeringCategoryList*)GetEngineeringCategoryList:(Web_x0020_Service_ElementGetEngineeringCategoryList *)aGetEngineeringCategoryList success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetEngineeringItem:(Web_x0020_Service_ElementGetEngineeringItem *)aGetEngineeringItem;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetEngineeringItem*)GetEngineeringItem:(Web_x0020_Service_ElementGetEngineeringItem *)aGetEngineeringItem success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetFindFloorDetailsList:(Web_x0020_Service_ElementGetFindFloorDetailsList *)aGetFindFloorDetailsList;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetFindFloorDetailsList*)GetFindFloorDetailsList:(Web_x0020_Service_ElementGetFindFloorDetailsList *)aGetFindFloorDetailsList success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetFindRoomAssignmentList:(Web_x0020_Service_ElementGetFindRoomAssignmentList *)aGetFindRoomAssignmentList;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetFindRoomAssignmentList*)GetFindRoomAssignmentList:(Web_x0020_Service_ElementGetFindRoomAssignmentList *)aGetFindRoomAssignmentList success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetFindRoomAssignmentLists:(Web_x0020_Service_ElementGetFindRoomAssignmentLists *)aGetFindRoomAssignmentLists;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetFindRoomAssignmentLists*)GetFindRoomAssignmentLists:(Web_x0020_Service_ElementGetFindRoomAssignmentLists *)aGetFindRoomAssignmentLists success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetFindRoomDetailsList:(Web_x0020_Service_ElementGetFindRoomDetailsList *)aGetFindRoomDetailsList;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetFindRoomDetailsList*)GetFindRoomDetailsList:(Web_x0020_Service_ElementGetFindRoomDetailsList *)aGetFindRoomDetailsList success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetFloorList:(Web_x0020_Service_ElementGetFloorList *)aGetFloorList;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetFloorList*)GetFloorList:(Web_x0020_Service_ElementGetFloorList *)aGetFloorList success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetGuestInfo:(Web_x0020_Service_ElementGetGuestInfo *)aGetGuestInfo;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetGuestInfo*)GetGuestInfo:(Web_x0020_Service_ElementGetGuestInfo *)aGetGuestInfo success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetGuestInfoByRoomID:(Web_x0020_Service_ElementGetGuestInfoByRoomID *)aGetGuestInfoByRoomID;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetGuestInfoByRoomID*)GetGuestInfoByRoomID:(Web_x0020_Service_ElementGetGuestInfoByRoomID *)aGetGuestInfoByRoomID success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetGuestInformation:(Web_x0020_Service_ElementGetGuestInformation *)aGetGuestInformation;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetGuestInformation*)GetGuestInformation:(Web_x0020_Service_ElementGetGuestInformation *)aGetGuestInformation success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetHotelInfo:(Web_x0020_Service_ElementGetHotelInfo *)aGetHotelInfo;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetHotelInfo*)GetHotelInfo:(Web_x0020_Service_ElementGetHotelInfo *)aGetHotelInfo success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetInspectionStatus:(Web_x0020_Service_ElementGetInspectionStatus *)aGetInspectionStatus;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetInspectionStatus*)GetInspectionStatus:(Web_x0020_Service_ElementGetInspectionStatus *)aGetInspectionStatus success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetLFCategoryList:(Web_x0020_Service_ElementGetLFCategoryList *)aGetLFCategoryList;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetLFCategoryList*)GetLFCategoryList:(Web_x0020_Service_ElementGetLFCategoryList *)aGetLFCategoryList success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetLFColorList:(Web_x0020_Service_ElementGetLFColorList *)aGetLFColorList;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetLFColorList*)GetLFColorList:(Web_x0020_Service_ElementGetLFColorList *)aGetLFColorList success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetLFItemList:(Web_x0020_Service_ElementGetLFItemList *)aGetLFItemList;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetLFItemList*)GetLFItemList:(Web_x0020_Service_ElementGetLFItemList *)aGetLFItemList success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetLanguageList:(Web_x0020_Service_ElementGetLanguageList *)aGetLanguageList;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetLanguageList*)GetLanguageList:(Web_x0020_Service_ElementGetLanguageList *)aGetLanguageList success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetLaundryCategoryList:(Web_x0020_Service_ElementGetLaundryCategoryList *)aGetLaundryCategoryList;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetLaundryCategoryList*)GetLaundryCategoryList:(Web_x0020_Service_ElementGetLaundryCategoryList *)aGetLaundryCategoryList success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetLaundryItemList:(Web_x0020_Service_ElementGetLaundryItemList *)aGetLaundryItemList;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetLaundryItemList*)GetLaundryItemList:(Web_x0020_Service_ElementGetLaundryItemList *)aGetLaundryItemList success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetLaundryItemPriceList:(Web_x0020_Service_ElementGetLaundryItemPriceList *)aGetLaundryItemPriceList;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetLaundryItemPriceList*)GetLaundryItemPriceList:(Web_x0020_Service_ElementGetLaundryItemPriceList *)aGetLaundryItemPriceList success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetLaundryServiceList:(Web_x0020_Service_ElementGetLaundryServiceList *)aGetLaundryServiceList;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetLaundryServiceList*)GetLaundryServiceList:(Web_x0020_Service_ElementGetLaundryServiceList *)aGetLaundryServiceList success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetLaundrySpecialInstructionList:(Web_x0020_Service_ElementGetLaundrySpecialInstructionList *)aGetLaundrySpecialInstructionList;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetLaundrySpecialInstructionList*)GetLaundrySpecialInstructionList:(Web_x0020_Service_ElementGetLaundrySpecialInstructionList *)aGetLaundrySpecialInstructionList success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetLinenCategoryList:(Web_x0020_Service_ElementGetLinenCategoryList *)aGetLinenCategoryList;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetLinenCategoryList*)GetLinenCategoryList:(Web_x0020_Service_ElementGetLinenCategoryList *)aGetLinenCategoryList success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetLinenItemList:(Web_x0020_Service_ElementGetLinenItemList *)aGetLinenItemList;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetLinenItemList*)GetLinenItemList:(Web_x0020_Service_ElementGetLinenItemList *)aGetLinenItemList success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetLocationList:(Web_x0020_Service_ElementGetLocationList *)aGetLocationList;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetLocationList*)GetLocationList:(Web_x0020_Service_ElementGetLocationList *)aGetLocationList success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetMessageAttachPhoto:(Web_x0020_Service_ElementGetMessageAttachPhoto *)aGetMessageAttachPhoto;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetMessageAttachPhoto*)GetMessageAttachPhoto:(Web_x0020_Service_ElementGetMessageAttachPhoto *)aGetMessageAttachPhoto success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetMessageCategoryList:(Web_x0020_Service_ElementGetMessageCategoryList *)aGetMessageCategoryList;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetMessageCategoryList*)GetMessageCategoryList:(Web_x0020_Service_ElementGetMessageCategoryList *)aGetMessageCategoryList success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetMessageCategoryList2:(Web_x0020_Service_ElementGetMessageCategoryList2 *)aGetMessageCategoryList2;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetMessageCategoryList2*)GetMessageCategoryList2:(Web_x0020_Service_ElementGetMessageCategoryList2 *)aGetMessageCategoryList2 success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetMessageItemList:(Web_x0020_Service_ElementGetMessageItemList *)aGetMessageItemList;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetMessageItemList*)GetMessageItemList:(Web_x0020_Service_ElementGetMessageItemList *)aGetMessageItemList success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetMessageItemList2:(Web_x0020_Service_ElementGetMessageItemList2 *)aGetMessageItemList2;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetMessageItemList2*)GetMessageItemList2:(Web_x0020_Service_ElementGetMessageItemList2 *)aGetMessageItemList2 success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetMessageTemplateList:(Web_x0020_Service_ElementGetMessageTemplateList *)aGetMessageTemplateList;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetMessageTemplateList*)GetMessageTemplateList:(Web_x0020_Service_ElementGetMessageTemplateList *)aGetMessageTemplateList success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetMinibarCategoryList:(Web_x0020_Service_ElementGetMinibarCategoryList *)aGetMinibarCategoryList;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetMinibarCategoryList*)GetMinibarCategoryList:(Web_x0020_Service_ElementGetMinibarCategoryList *)aGetMinibarCategoryList success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetMinibarItemList:(Web_x0020_Service_ElementGetMinibarItemList *)aGetMinibarItemList;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetMinibarItemList*)GetMinibarItemList:(Web_x0020_Service_ElementGetMinibarItemList *)aGetMinibarItemList success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetNewMessage:(Web_x0020_Service_ElementGetNewMessage *)aGetNewMessage;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetNewMessage*)GetNewMessage:(Web_x0020_Service_ElementGetNewMessage *)aGetNewMessage success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetNoOfAdHocMessage:(Web_x0020_Service_ElementGetNoOfAdHocMessage *)aGetNoOfAdHocMessage;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetNoOfAdHocMessage*)GetNoOfAdHocMessage:(Web_x0020_Service_ElementGetNoOfAdHocMessage *)aGetNoOfAdHocMessage success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetNoOfAdHocMessageRoutine:(Web_x0020_Service_ElementGetNoOfAdHocMessageRoutine *)aGetNoOfAdHocMessageRoutine;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetNoOfAdHocMessageRoutine*)GetNoOfAdHocMessageRoutine:(Web_x0020_Service_ElementGetNoOfAdHocMessageRoutine *)aGetNoOfAdHocMessageRoutine success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetNumberOfGuest:(Web_x0020_Service_ElementGetNumberOfGuest *)aGetNumberOfGuest;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetNumberOfGuest*)GetNumberOfGuest:(Web_x0020_Service_ElementGetNumberOfGuest *)aGetNumberOfGuest success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetOOSBlockRoomsList:(Web_x0020_Service_ElementGetOOSBlockRoomsList *)aGetOOSBlockRoomsList;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetOOSBlockRoomsList*)GetOOSBlockRoomsList:(Web_x0020_Service_ElementGetOOSBlockRoomsList *)aGetOOSBlockRoomsList success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetOtherActivitiesLocationRoutine:(Web_x0020_Service_ElementGetOtherActivitiesLocationRoutine *)aGetOtherActivitiesLocationRoutine;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetOtherActivitiesLocationRoutine*)GetOtherActivitiesLocationRoutine:(Web_x0020_Service_ElementGetOtherActivitiesLocationRoutine *)aGetOtherActivitiesLocationRoutine success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetOtherActivitiesStatusRoutine:(Web_x0020_Service_ElementGetOtherActivitiesStatusRoutine *)aGetOtherActivitiesStatusRoutine;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetOtherActivitiesStatusRoutine*)GetOtherActivitiesStatusRoutine:(Web_x0020_Service_ElementGetOtherActivitiesStatusRoutine *)aGetOtherActivitiesStatusRoutine success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetOtherActivityAssignmentRoutine:(Web_x0020_Service_ElementGetOtherActivityAssignmentRoutine *)aGetOtherActivityAssignmentRoutine;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetOtherActivityAssignmentRoutine*)GetOtherActivityAssignmentRoutine:(Web_x0020_Service_ElementGetOtherActivityAssignmentRoutine *)aGetOtherActivityAssignmentRoutine success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetPanicAlertConfig:(Web_x0020_Service_ElementGetPanicAlertConfig *)aGetPanicAlertConfig;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetPanicAlertConfig*)GetPanicAlertConfig:(Web_x0020_Service_ElementGetPanicAlertConfig *)aGetPanicAlertConfig success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetPopupMsgList:(Web_x0020_Service_ElementGetPopupMsgList *)aGetPopupMsgList;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetPopupMsgList*)GetPopupMsgList:(Web_x0020_Service_ElementGetPopupMsgList *)aGetPopupMsgList success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetPostingHistoryRoutine:(Web_x0020_Service_ElementGetPostingHistoryRoutine *)aGetPostingHistoryRoutine;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetPostingHistoryRoutine*)GetPostingHistoryRoutine:(Web_x0020_Service_ElementGetPostingHistoryRoutine *)aGetPostingHistoryRoutine success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetPrevRoomAssignmentList:(Web_x0020_Service_ElementGetPrevRoomAssignmentList *)aGetPrevRoomAssignmentList;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetPrevRoomAssignmentList*)GetPrevRoomAssignmentList:(Web_x0020_Service_ElementGetPrevRoomAssignmentList *)aGetPrevRoomAssignmentList success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetProfileNote:(Web_x0020_Service_ElementGetProfileNote *)aGetProfileNote;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetProfileNote*)GetProfileNote:(Web_x0020_Service_ElementGetProfileNote *)aGetProfileNote success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetReceivedAdHocMessage:(Web_x0020_Service_ElementGetReceivedAdHocMessage *)aGetReceivedAdHocMessage;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetReceivedAdHocMessage*)GetReceivedAdHocMessage:(Web_x0020_Service_ElementGetReceivedAdHocMessage *)aGetReceivedAdHocMessage success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetReceivedAdHocMessageRoutine:(Web_x0020_Service_ElementGetReceivedAdHocMessageRoutine *)aGetReceivedAdHocMessageRoutine;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetReceivedAdHocMessageRoutine*)GetReceivedAdHocMessageRoutine:(Web_x0020_Service_ElementGetReceivedAdHocMessageRoutine *)aGetReceivedAdHocMessageRoutine success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetReleaseRoomsList:(Web_x0020_Service_ElementGetReleaseRoomsList *)aGetReleaseRoomsList;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetReleaseRoomsList*)GetReleaseRoomsList:(Web_x0020_Service_ElementGetReleaseRoomsList *)aGetReleaseRoomsList success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetRoomAssignmentList:(Web_x0020_Service_ElementGetRoomAssignmentList *)aGetRoomAssignmentList;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetRoomAssignmentList*)GetRoomAssignmentList:(Web_x0020_Service_ElementGetRoomAssignmentList *)aGetRoomAssignmentList success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetRoomDetail:(Web_x0020_Service_ElementGetRoomDetail *)aGetRoomDetail;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetRoomDetail*)GetRoomDetail:(Web_x0020_Service_ElementGetRoomDetail *)aGetRoomDetail success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetRoomDetailsRoutine:(Web_x0020_Service_ElementGetRoomDetailsRoutine *)aGetRoomDetailsRoutine;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetRoomDetailsRoutine*)GetRoomDetailsRoutine:(Web_x0020_Service_ElementGetRoomDetailsRoutine *)aGetRoomDetailsRoutine success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetRoomRemarks:(Web_x0020_Service_ElementGetRoomRemarks *)aGetRoomRemarks;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetRoomRemarks*)GetRoomRemarks:(Web_x0020_Service_ElementGetRoomRemarks *)aGetRoomRemarks success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetRoomSectionList:(Web_x0020_Service_ElementGetRoomSectionList *)aGetRoomSectionList;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetRoomSectionList*)GetRoomSectionList:(Web_x0020_Service_ElementGetRoomSectionList *)aGetRoomSectionList success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetRoomSetGuideByRoomType:(Web_x0020_Service_ElementGetRoomSetGuideByRoomType *)aGetRoomSetGuideByRoomType;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetRoomSetGuideByRoomType*)GetRoomSetGuideByRoomType:(Web_x0020_Service_ElementGetRoomSetGuideByRoomType *)aGetRoomSetGuideByRoomType success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetRoomSetGuideList:(Web_x0020_Service_ElementGetRoomSetGuideList *)aGetRoomSetGuideList;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetRoomSetGuideList*)GetRoomSetGuideList:(Web_x0020_Service_ElementGetRoomSetGuideList *)aGetRoomSetGuideList success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetRoomStatusList:(Web_x0020_Service_ElementGetRoomStatusList *)aGetRoomStatusList;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetRoomStatusList*)GetRoomStatusList:(Web_x0020_Service_ElementGetRoomStatusList *)aGetRoomStatusList success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetRoomTypeByRoomNoRoutine:(Web_x0020_Service_ElementGetRoomTypeByRoomNoRoutine *)aGetRoomTypeByRoomNoRoutine;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetRoomTypeByRoomNoRoutine*)GetRoomTypeByRoomNoRoutine:(Web_x0020_Service_ElementGetRoomTypeByRoomNoRoutine *)aGetRoomTypeByRoomNoRoutine success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetRoomTypeInventoryRoutine:(Web_x0020_Service_ElementGetRoomTypeInventoryRoutine *)aGetRoomTypeInventoryRoutine;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetRoomTypeInventoryRoutine*)GetRoomTypeInventoryRoutine:(Web_x0020_Service_ElementGetRoomTypeInventoryRoutine *)aGetRoomTypeInventoryRoutine success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetRoomTypeList:(Web_x0020_Service_ElementGetRoomTypeList *)aGetRoomTypeList;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetRoomTypeList*)GetRoomTypeList:(Web_x0020_Service_ElementGetRoomTypeList *)aGetRoomTypeList success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetSecretsRoutine:(Web_x0020_Service_ElementGetSecretsRoutine *)aGetSecretsRoutine;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetSecretsRoutine*)GetSecretsRoutine:(Web_x0020_Service_ElementGetSecretsRoutine *)aGetSecretsRoutine success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetSendAdHocMessageRoutine:(Web_x0020_Service_ElementGetSendAdHocMessageRoutine *)aGetSendAdHocMessageRoutine;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetSendAdHocMessageRoutine*)GetSendAdHocMessageRoutine:(Web_x0020_Service_ElementGetSendAdHocMessageRoutine *)aGetSendAdHocMessageRoutine success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetSentAdHocMessage:(Web_x0020_Service_ElementGetSentAdHocMessage *)aGetSentAdHocMessage;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetSentAdHocMessage*)GetSentAdHocMessage:(Web_x0020_Service_ElementGetSentAdHocMessage *)aGetSentAdHocMessage success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetStillLoggedIn:(Web_x0020_Service_ElementGetStillLoggedIn *)aGetStillLoggedIn;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetStillLoggedIn*)GetStillLoggedIn:(Web_x0020_Service_ElementGetStillLoggedIn *)aGetStillLoggedIn success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetSupervisorFiltersRoutine:(Web_x0020_Service_ElementGetSupervisorFiltersRoutine *)aGetSupervisorFiltersRoutine;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetSupervisorFiltersRoutine*)GetSupervisorFiltersRoutine:(Web_x0020_Service_ElementGetSupervisorFiltersRoutine *)aGetSupervisorFiltersRoutine success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetSupervisorFindFloorList:(Web_x0020_Service_ElementGetSupervisorFindFloorList *)aGetSupervisorFindFloorList;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetSupervisorFindFloorList*)GetSupervisorFindFloorList:(Web_x0020_Service_ElementGetSupervisorFindFloorList *)aGetSupervisorFindFloorList success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetSupervisorFindRoomAssignmentLists:(Web_x0020_Service_ElementGetSupervisorFindRoomAssignmentLists *)aGetSupervisorFindRoomAssignmentLists;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetSupervisorFindRoomAssignmentLists*)GetSupervisorFindRoomAssignmentLists:(Web_x0020_Service_ElementGetSupervisorFindRoomAssignmentLists *)aGetSupervisorFindRoomAssignmentLists success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetUnassignRoomList:(Web_x0020_Service_ElementGetUnassignRoomList *)aGetUnassignRoomList;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetUnassignRoomList*)GetUnassignRoomList:(Web_x0020_Service_ElementGetUnassignRoomList *)aGetUnassignRoomList success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetUserDetailsRoutine:(Web_x0020_Service_ElementGetUserDetailsRoutine *)aGetUserDetailsRoutine;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetUserDetailsRoutine*)GetUserDetailsRoutine:(Web_x0020_Service_ElementGetUserDetailsRoutine *)aGetUserDetailsRoutine success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetUserList:(Web_x0020_Service_ElementGetUserList *)aGetUserList;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetUserList*)GetUserList:(Web_x0020_Service_ElementGetUserList *)aGetUserList success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetVersion:(Web_x0020_Service_ElementGetVersion *)aGetVersion;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetVersion*)GetVersion:(Web_x0020_Service_ElementGetVersion *)aGetVersion success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetZoneList:(Web_x0020_Service_ElementGetZoneList *)aGetZoneList;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetZoneList*)GetZoneList:(Web_x0020_Service_ElementGetZoneList *)aGetZoneList success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)GetZoneRoomList:(Web_x0020_Service_ElementGetZoneRoomList *)aGetZoneRoomList;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetZoneRoomList*)GetZoneRoomList:(Web_x0020_Service_ElementGetZoneRoomList *)aGetZoneRoomList success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)LogInvalidStartRoom:(Web_x0020_Service_ElementLogInvalidStartRoom *)aLogInvalidStartRoom;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_LogInvalidStartRoom*)LogInvalidStartRoom:(Web_x0020_Service_ElementLogInvalidStartRoom *)aLogInvalidStartRoom success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)PostAdditionalJobStatusUpdateRoutine:(Web_x0020_Service_ElementPostAdditionalJobStatusUpdateRoutine *)aPostAdditionalJobStatusUpdateRoutine;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostAdditionalJobStatusUpdateRoutine*)PostAdditionalJobStatusUpdateRoutine:(Web_x0020_Service_ElementPostAdditionalJobStatusUpdateRoutine *)aPostAdditionalJobStatusUpdateRoutine success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)PostAmenitiesItem:(Web_x0020_Service_ElementPostAmenitiesItem *)aPostAmenitiesItem;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostAmenitiesItem*)PostAmenitiesItem:(Web_x0020_Service_ElementPostAmenitiesItem *)aPostAmenitiesItem success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)PostAmenitiesItemsList:(Web_x0020_Service_ElementPostAmenitiesItemsList *)aPostAmenitiesItemsList;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostAmenitiesItemsList*)PostAmenitiesItemsList:(Web_x0020_Service_ElementPostAmenitiesItemsList *)aPostAmenitiesItemsList success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)PostChecklistItem:(Web_x0020_Service_ElementPostChecklistItem *)aPostChecklistItem;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostChecklistItem*)PostChecklistItem:(Web_x0020_Service_ElementPostChecklistItem *)aPostChecklistItem success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)PostChecklistItems:(Web_x0020_Service_ElementPostChecklistItems *)aPostChecklistItems;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostChecklistItems*)PostChecklistItems:(Web_x0020_Service_ElementPostChecklistItems *)aPostChecklistItems success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)PostECAttachPhoto:(Web_x0020_Service_ElementPostECAttachPhoto *)aPostECAttachPhoto;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostECAttachPhoto*)PostECAttachPhoto:(Web_x0020_Service_ElementPostECAttachPhoto *)aPostECAttachPhoto success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)PostEngineeringCase:(Web_x0020_Service_ElementPostEngineeringCase *)aPostEngineeringCase;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostEngineeringCase*)PostEngineeringCase:(Web_x0020_Service_ElementPostEngineeringCase *)aPostEngineeringCase success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)PostLFAttachPhoto:(Web_x0020_Service_ElementPostLFAttachPhoto *)aPostLFAttachPhoto;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostLFAttachPhoto*)PostLFAttachPhoto:(Web_x0020_Service_ElementPostLFAttachPhoto *)aPostLFAttachPhoto success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)PostLaundryItem:(Web_x0020_Service_ElementPostLaundryItem *)aPostLaundryItem;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostLaundryItem*)PostLaundryItem:(Web_x0020_Service_ElementPostLaundryItem *)aPostLaundryItem success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)PostLaundryOrder:(Web_x0020_Service_ElementPostLaundryOrder *)aPostLaundryOrder;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostLaundryOrder*)PostLaundryOrder:(Web_x0020_Service_ElementPostLaundryOrder *)aPostLaundryOrder success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)PostLaundrySpecialInstruction:(Web_x0020_Service_ElementPostLaundrySpecialInstruction *)aPostLaundrySpecialInstruction;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostLaundrySpecialInstruction*)PostLaundrySpecialInstruction:(Web_x0020_Service_ElementPostLaundrySpecialInstruction *)aPostLaundrySpecialInstruction success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)PostLinenItem:(Web_x0020_Service_ElementPostLinenItem *)aPostLinenItem;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostLinenItem*)PostLinenItem:(Web_x0020_Service_ElementPostLinenItem *)aPostLinenItem success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)PostLinenItems:(Web_x0020_Service_ElementPostLinenItems *)aPostLinenItems;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostLinenItems*)PostLinenItems:(Web_x0020_Service_ElementPostLinenItems *)aPostLinenItems success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)PostLogout:(Web_x0020_Service_ElementPostLogout *)aPostLogout;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostLogout*)PostLogout:(Web_x0020_Service_ElementPostLogout *)aPostLogout success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)PostLostAndFoundRoutine:(Web_x0020_Service_ElementPostLostAndFoundRoutine *)aPostLostAndFoundRoutine;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostLostAndFoundRoutine*)PostLostAndFoundRoutine:(Web_x0020_Service_ElementPostLostAndFoundRoutine *)aPostLostAndFoundRoutine success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)PostLostFound:(Web_x0020_Service_ElementPostLostFound *)aPostLostFound;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostLostFound*)PostLostFound:(Web_x0020_Service_ElementPostLostFound *)aPostLostFound success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)PostMessage:(Web_x0020_Service_ElementPostMessage *)aPostMessage;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostMessage*)PostMessage:(Web_x0020_Service_ElementPostMessage *)aPostMessage success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)PostMessageAttachPhoto:(Web_x0020_Service_ElementPostMessageAttachPhoto *)aPostMessageAttachPhoto;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostMessageAttachPhoto*)PostMessageAttachPhoto:(Web_x0020_Service_ElementPostMessageAttachPhoto *)aPostMessageAttachPhoto success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)PostMinibarItem:(Web_x0020_Service_ElementPostMinibarItem *)aPostMinibarItem;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostMinibarItem*)PostMinibarItem:(Web_x0020_Service_ElementPostMinibarItem *)aPostMinibarItem success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)PostMinibarItems:(Web_x0020_Service_ElementPostMinibarItems *)aPostMinibarItems;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostMinibarItems*)PostMinibarItems:(Web_x0020_Service_ElementPostMinibarItems *)aPostMinibarItems success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)PostMinibarOrder:(Web_x0020_Service_ElementPostMinibarOrder *)aPostMinibarOrder;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostMinibarOrder*)PostMinibarOrder:(Web_x0020_Service_ElementPostMinibarOrder *)aPostMinibarOrder success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)PostPostingHistoryRoutine:(Web_x0020_Service_ElementPostPostingHistoryRoutine *)aPostPostingHistoryRoutine;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostPostingHistoryRoutine*)PostPostingHistoryRoutine:(Web_x0020_Service_ElementPostPostingHistoryRoutine *)aPostPostingHistoryRoutine success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)PostUnassignRoomRoutine:(Web_x0020_Service_ElementPostUnassignRoomRoutine *)aPostUnassignRoomRoutine;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostUnassignRoomRoutine*)PostUnassignRoomRoutine:(Web_x0020_Service_ElementPostUnassignRoomRoutine *)aPostUnassignRoomRoutine success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)PostUpdateAdHocMessageRoutine:(Web_x0020_Service_ElementPostUpdateAdHocMessageRoutine *)aPostUpdateAdHocMessageRoutine;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostUpdateAdHocMessageRoutine*)PostUpdateAdHocMessageRoutine:(Web_x0020_Service_ElementPostUpdateAdHocMessageRoutine *)aPostUpdateAdHocMessageRoutine success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)PostUpdateDeviceTokenRoutine:(Web_x0020_Service_ElementPostUpdateDeviceTokenRoutine *)aPostUpdateDeviceTokenRoutine;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostUpdateDeviceTokenRoutine*)PostUpdateDeviceTokenRoutine:(Web_x0020_Service_ElementPostUpdateDeviceTokenRoutine *)aPostUpdateDeviceTokenRoutine success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)PostUpdateOtherActivityStatusRoutine:(Web_x0020_Service_ElementPostUpdateOtherActivityStatusRoutine *)aPostUpdateOtherActivityStatusRoutine;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostUpdateOtherActivityStatusRoutine*)PostUpdateOtherActivityStatusRoutine:(Web_x0020_Service_ElementPostUpdateOtherActivityStatusRoutine *)aPostUpdateOtherActivityStatusRoutine success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)PostUpdateRoomDetailsRoutine:(Web_x0020_Service_ElementPostUpdateRoomDetailsRoutine *)aPostUpdateRoomDetailsRoutine;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostUpdateRoomDetailsRoutine*)PostUpdateRoomDetailsRoutine:(Web_x0020_Service_ElementPostUpdateRoomDetailsRoutine *)aPostUpdateRoomDetailsRoutine success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)PostWSLog:(Web_x0020_Service_ElementPostWSLog *)aPostWSLog;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostWSLog*)PostWSLog:(Web_x0020_Service_ElementPostWSLog *)aPostWSLog success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)PosteCnAhMsg:(Web_x0020_Service_ElementPosteCnAhMsg *)aPosteCnAhMsg;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PosteCnAhMsg*)PosteCnAhMsg:(Web_x0020_Service_ElementPosteCnAhMsg *)aPosteCnAhMsg success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)PosteCnJob:(Web_x0020_Service_ElementPosteCnJob *)aPosteCnJob;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PosteCnJob*)PosteCnJob:(Web_x0020_Service_ElementPosteCnJob *)aPosteCnJob success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)PostingLinenItems:(Web_x0020_Service_ElementPostingLinenItems *)aPostingLinenItems;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostingLinenItems*)PostingLinenItems:(Web_x0020_Service_ElementPostingLinenItems *)aPostingLinenItems success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)PostingPanicAlert:(Web_x0020_Service_ElementPostingPanicAlert *)aPostingPanicAlert;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostingPanicAlert*)PostingPanicAlert:(Web_x0020_Service_ElementPostingPanicAlert *)aPostingPanicAlert success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)RemoveLaundryItem:(Web_x0020_Service_ElementRemoveLaundryItem *)aRemoveLaundryItem;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_RemoveLaundryItem*)RemoveLaundryItem:(Web_x0020_Service_ElementRemoveLaundryItem *)aRemoveLaundryItem success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)StartAdditionalJob:(Web_x0020_Service_ElementStartAdditionalJob *)aStartAdditionalJob;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_StartAdditionalJob*)StartAdditionalJob:(Web_x0020_Service_ElementStartAdditionalJob *)aStartAdditionalJob success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)StopAdditionalJob:(Web_x0020_Service_ElementStopAdditionalJob *)aStopAdditionalJob;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_StopAdditionalJob*)StopAdditionalJob:(Web_x0020_Service_ElementStopAdditionalJob *)aStopAdditionalJob success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)UpdateAdHocMessageStatus:(Web_x0020_Service_ElementUpdateAdHocMessageStatus *)aUpdateAdHocMessageStatus;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_UpdateAdHocMessageStatus*)UpdateAdHocMessageStatus:(Web_x0020_Service_ElementUpdateAdHocMessageStatus *)aUpdateAdHocMessageStatus success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)UpdateAdditionalJobRemark:(Web_x0020_Service_ElementUpdateAdditionalJobRemark *)aUpdateAdditionalJobRemark;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_UpdateAdditionalJobRemark*)UpdateAdditionalJobRemark:(Web_x0020_Service_ElementUpdateAdditionalJobRemark *)aUpdateAdditionalJobRemark success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)UpdateAdditionalJobStatus:(Web_x0020_Service_ElementUpdateAdditionalJobStatus *)aUpdateAdditionalJobStatus;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_UpdateAdditionalJobStatus*)UpdateAdditionalJobStatus:(Web_x0020_Service_ElementUpdateAdditionalJobStatus *)aUpdateAdditionalJobStatus success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)UpdateCleaningStatus:(Web_x0020_Service_ElementUpdateCleaningStatus *)aUpdateCleaningStatus;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_UpdateCleaningStatus*)UpdateCleaningStatus:(Web_x0020_Service_ElementUpdateCleaningStatus *)aUpdateCleaningStatus success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)UpdateGuestInfo:(Web_x0020_Service_ElementUpdateGuestInfo *)aUpdateGuestInfo;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_UpdateGuestInfo*)UpdateGuestInfo:(Web_x0020_Service_ElementUpdateGuestInfo *)aUpdateGuestInfo success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)UpdateInspection:(Web_x0020_Service_ElementUpdateInspection *)aUpdateInspection;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_UpdateInspection*)UpdateInspection:(Web_x0020_Service_ElementUpdateInspection *)aUpdateInspection success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)UpdatePopupMsgStatus:(Web_x0020_Service_ElementUpdatePopupMsgStatus *)aUpdatePopupMsgStatus;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_UpdatePopupMsgStatus*)UpdatePopupMsgStatus:(Web_x0020_Service_ElementUpdatePopupMsgStatus *)aUpdatePopupMsgStatus success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)UpdateRoomAssignment:(Web_x0020_Service_ElementUpdateRoomAssignment *)aUpdateRoomAssignment;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_UpdateRoomAssignment*)UpdateRoomAssignment:(Web_x0020_Service_ElementUpdateRoomAssignment *)aUpdateRoomAssignment success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)UpdateRoomCleaningStatus:(Web_x0020_Service_ElementUpdateRoomCleaningStatus *)aUpdateRoomCleaningStatus;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_UpdateRoomCleaningStatus*)UpdateRoomCleaningStatus:(Web_x0020_Service_ElementUpdateRoomCleaningStatus *)aUpdateRoomCleaningStatus success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)UpdateRoomDetails:(Web_x0020_Service_ElementUpdateRoomDetails *)aUpdateRoomDetails;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_UpdateRoomDetails*)UpdateRoomDetails:(Web_x0020_Service_ElementUpdateRoomDetails *)aUpdateRoomDetails success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)UpdateRoomRemarks:(Web_x0020_Service_ElementUpdateRoomRemarks *)aUpdateRoomRemarks;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_UpdateRoomRemarks*)UpdateRoomRemarks:(Web_x0020_Service_ElementUpdateRoomRemarks *)aUpdateRoomRemarks success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)UpdateRoomStatus:(Web_x0020_Service_ElementUpdateRoomStatus *)aUpdateRoomStatus;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_UpdateRoomStatus*)UpdateRoomStatus:(Web_x0020_Service_ElementUpdateRoomStatus *)aUpdateRoomStatus success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
- (Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *)ValidateRoomNo:(Web_x0020_Service_ElementValidateRoomNo *)aValidateRoomNo;
- (Web_x0020_Service_Web_x0020_ServiceSoap12Binding_ValidateRoomNo*)ValidateRoomNo:(Web_x0020_Service_ElementValidateRoomNo *)aValidateRoomNo success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;
@end

@interface Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation : NSOperation <NSURLConnectionDelegate>
@property(nonatomic, strong) Web_x0020_Service_Web_x0020_ServiceSoap12Binding *binding;
@property(nonatomic, strong, readonly) Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse *response;
@property(nonatomic, strong) NSMutableData *responseData;
@property(nonatomic, strong) NSURLConnection *urlConnection;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error;

/**
 * Cancels connection. Response has error with code kCFURLErrorCancelled in domain kCFErrorDomainCFNetwork.
 */
- (void)cancel;

@end

@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_AddReassignRoom : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementAddReassignRoom * AddReassignRoom;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	AddReassignRoom:(Web_x0020_Service_ElementAddReassignRoom *)aAddReassignRoom
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_AssignRoom : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementAssignRoom * AssignRoom;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	AssignRoom:(Web_x0020_Service_ElementAssignRoom *)aAssignRoom
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_AuthenticateUser : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementAuthenticateUser * AuthenticateUser;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	AuthenticateUser:(Web_x0020_Service_ElementAuthenticateUser *)aAuthenticateUser
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_ChangeRoomAssignmentSequence : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementChangeRoomAssignmentSequence * ChangeRoomAssignmentSequence;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	ChangeRoomAssignmentSequence:(Web_x0020_Service_ElementChangeRoomAssignmentSequence *)aChangeRoomAssignmentSequence
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_CompleteAdditionalJob : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementCompleteAdditionalJob * CompleteAdditionalJob;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	CompleteAdditionalJob:(Web_x0020_Service_ElementCompleteAdditionalJob *)aCompleteAdditionalJob
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_FindAttendant : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementFindAttendant * FindAttendant;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	FindAttendant:(Web_x0020_Service_ElementFindAttendant *)aFindAttendant
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_FindGuestStayedHistoryRoutine : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementFindGuestStayedHistoryRoutine * FindGuestStayedHistoryRoutine;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	FindGuestStayedHistoryRoutine:(Web_x0020_Service_ElementFindGuestStayedHistoryRoutine *)aFindGuestStayedHistoryRoutine
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_FindRoomByRoomNo : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementFindRoomByRoomNo * FindRoomByRoomNo;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	FindRoomByRoomNo:(Web_x0020_Service_ElementFindRoomByRoomNo *)aFindRoomByRoomNo
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetAccessRights : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetAccessRights * GetAccessRights;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetAccessRights:(Web_x0020_Service_ElementGetAccessRights *)aGetAccessRights
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetAdditionalJobByRoomNo : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetAdditionalJobByRoomNo * GetAdditionalJobByRoomNo;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetAdditionalJobByRoomNo:(Web_x0020_Service_ElementGetAdditionalJobByRoomNo *)aGetAdditionalJobByRoomNo
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetAdditionalJobCount : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetAdditionalJobCount * GetAdditionalJobCount;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetAdditionalJobCount:(Web_x0020_Service_ElementGetAdditionalJobCount *)aGetAdditionalJobCount
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetAdditionalJobDetail : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetAdditionalJobDetail * GetAdditionalJobDetail;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetAdditionalJobDetail:(Web_x0020_Service_ElementGetAdditionalJobDetail *)aGetAdditionalJobDetail
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetAdditionalJobFloorList : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetAdditionalJobFloorList * GetAdditionalJobFloorList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetAdditionalJobFloorList:(Web_x0020_Service_ElementGetAdditionalJobFloorList *)aGetAdditionalJobFloorList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetAdditionalJobOfRoom : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetAdditionalJobOfRoom * GetAdditionalJobOfRoom;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetAdditionalJobOfRoom:(Web_x0020_Service_ElementGetAdditionalJobOfRoom *)aGetAdditionalJobOfRoom
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetAdditionalJobRoomByFloor : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetAdditionalJobRoomByFloor * GetAdditionalJobRoomByFloor;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetAdditionalJobRoomByFloor:(Web_x0020_Service_ElementGetAdditionalJobRoomByFloor *)aGetAdditionalJobRoomByFloor
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetAdditionalJobStatusesRoutine : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetAdditionalJobStatusesRoutine * GetAdditionalJobStatusesRoutine;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetAdditionalJobStatusesRoutine:(Web_x0020_Service_ElementGetAdditionalJobStatusesRoutine *)aGetAdditionalJobStatusesRoutine
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetAdditionalJobTask : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetAdditionalJobTask * GetAdditionalJobTask;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetAdditionalJobTask:(Web_x0020_Service_ElementGetAdditionalJobTask *)aGetAdditionalJobTask
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetAllChecklist : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetAllChecklist * GetAllChecklist;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetAllChecklist:(Web_x0020_Service_ElementGetAllChecklist *)aGetAllChecklist
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetAmenitiesCategoryList : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetAmenitiesCategoryList * GetAmenitiesCategoryList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetAmenitiesCategoryList:(Web_x0020_Service_ElementGetAmenitiesCategoryList *)aGetAmenitiesCategoryList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetAmenitiesItemList : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetAmenitiesItemList * GetAmenitiesItemList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetAmenitiesItemList:(Web_x0020_Service_ElementGetAmenitiesItemList *)aGetAmenitiesItemList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetAmenitiesLocationList : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetAmenitiesLocationList * GetAmenitiesLocationList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetAmenitiesLocationList:(Web_x0020_Service_ElementGetAmenitiesLocationList *)aGetAmenitiesLocationList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetBuildingList : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetBuildingList * GetBuildingList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetBuildingList:(Web_x0020_Service_ElementGetBuildingList *)aGetBuildingList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetCheckListItemScoreForMaid : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetCheckListItemScoreForMaid * GetCheckListItemScoreForMaid;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetCheckListItemScoreForMaid:(Web_x0020_Service_ElementGetCheckListItemScoreForMaid *)aGetCheckListItemScoreForMaid
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetChecklistCategoryList : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetChecklistCategoryList * GetChecklistCategoryList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetChecklistCategoryList:(Web_x0020_Service_ElementGetChecklistCategoryList *)aGetChecklistCategoryList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetChecklistItemList : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetChecklistItemList * GetChecklistItemList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetChecklistItemList:(Web_x0020_Service_ElementGetChecklistItemList *)aGetChecklistItemList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetChecklistRoomType : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetChecklistRoomType * GetChecklistRoomType;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetChecklistRoomType:(Web_x0020_Service_ElementGetChecklistRoomType *)aGetChecklistRoomType
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetCleaningStatusList : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetCleaningStatusList * GetCleaningStatusList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetCleaningStatusList:(Web_x0020_Service_ElementGetCleaningStatusList *)aGetCleaningStatusList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetCommonConfigurations : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetCommonConfigurations * GetCommonConfigurations;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetCommonConfigurations:(Web_x0020_Service_ElementGetCommonConfigurations *)aGetCommonConfigurations
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetDiscrepantRoomStatus : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetDiscrepantRoomStatus * GetDiscrepantRoomStatus;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetDiscrepantRoomStatus:(Web_x0020_Service_ElementGetDiscrepantRoomStatus *)aGetDiscrepantRoomStatus
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetEngineeringCategoryList : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetEngineeringCategoryList * GetEngineeringCategoryList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetEngineeringCategoryList:(Web_x0020_Service_ElementGetEngineeringCategoryList *)aGetEngineeringCategoryList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetEngineeringItem : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetEngineeringItem * GetEngineeringItem;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetEngineeringItem:(Web_x0020_Service_ElementGetEngineeringItem *)aGetEngineeringItem
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetFindFloorDetailsList : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetFindFloorDetailsList * GetFindFloorDetailsList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetFindFloorDetailsList:(Web_x0020_Service_ElementGetFindFloorDetailsList *)aGetFindFloorDetailsList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetFindRoomAssignmentList : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetFindRoomAssignmentList * GetFindRoomAssignmentList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetFindRoomAssignmentList:(Web_x0020_Service_ElementGetFindRoomAssignmentList *)aGetFindRoomAssignmentList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetFindRoomAssignmentLists : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetFindRoomAssignmentLists * GetFindRoomAssignmentLists;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetFindRoomAssignmentLists:(Web_x0020_Service_ElementGetFindRoomAssignmentLists *)aGetFindRoomAssignmentLists
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetFindRoomDetailsList : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetFindRoomDetailsList * GetFindRoomDetailsList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetFindRoomDetailsList:(Web_x0020_Service_ElementGetFindRoomDetailsList *)aGetFindRoomDetailsList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetFloorList : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetFloorList * GetFloorList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetFloorList:(Web_x0020_Service_ElementGetFloorList *)aGetFloorList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetGuestInfo : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetGuestInfo * GetGuestInfo;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetGuestInfo:(Web_x0020_Service_ElementGetGuestInfo *)aGetGuestInfo
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetGuestInfoByRoomID : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetGuestInfoByRoomID * GetGuestInfoByRoomID;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetGuestInfoByRoomID:(Web_x0020_Service_ElementGetGuestInfoByRoomID *)aGetGuestInfoByRoomID
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetGuestInformation : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetGuestInformation * GetGuestInformation;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetGuestInformation:(Web_x0020_Service_ElementGetGuestInformation *)aGetGuestInformation
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetHotelInfo : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetHotelInfo * GetHotelInfo;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetHotelInfo:(Web_x0020_Service_ElementGetHotelInfo *)aGetHotelInfo
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetInspectionStatus : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetInspectionStatus * GetInspectionStatus;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetInspectionStatus:(Web_x0020_Service_ElementGetInspectionStatus *)aGetInspectionStatus
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetLFCategoryList : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetLFCategoryList * GetLFCategoryList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetLFCategoryList:(Web_x0020_Service_ElementGetLFCategoryList *)aGetLFCategoryList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetLFColorList : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetLFColorList * GetLFColorList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetLFColorList:(Web_x0020_Service_ElementGetLFColorList *)aGetLFColorList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetLFItemList : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetLFItemList * GetLFItemList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetLFItemList:(Web_x0020_Service_ElementGetLFItemList *)aGetLFItemList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetLanguageList : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetLanguageList * GetLanguageList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetLanguageList:(Web_x0020_Service_ElementGetLanguageList *)aGetLanguageList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetLaundryCategoryList : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetLaundryCategoryList * GetLaundryCategoryList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetLaundryCategoryList:(Web_x0020_Service_ElementGetLaundryCategoryList *)aGetLaundryCategoryList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetLaundryItemList : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetLaundryItemList * GetLaundryItemList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetLaundryItemList:(Web_x0020_Service_ElementGetLaundryItemList *)aGetLaundryItemList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetLaundryItemPriceList : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetLaundryItemPriceList * GetLaundryItemPriceList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetLaundryItemPriceList:(Web_x0020_Service_ElementGetLaundryItemPriceList *)aGetLaundryItemPriceList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetLaundryServiceList : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetLaundryServiceList * GetLaundryServiceList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetLaundryServiceList:(Web_x0020_Service_ElementGetLaundryServiceList *)aGetLaundryServiceList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetLaundrySpecialInstructionList : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetLaundrySpecialInstructionList * GetLaundrySpecialInstructionList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetLaundrySpecialInstructionList:(Web_x0020_Service_ElementGetLaundrySpecialInstructionList *)aGetLaundrySpecialInstructionList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetLinenCategoryList : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetLinenCategoryList * GetLinenCategoryList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetLinenCategoryList:(Web_x0020_Service_ElementGetLinenCategoryList *)aGetLinenCategoryList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetLinenItemList : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetLinenItemList * GetLinenItemList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetLinenItemList:(Web_x0020_Service_ElementGetLinenItemList *)aGetLinenItemList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetLocationList : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetLocationList * GetLocationList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetLocationList:(Web_x0020_Service_ElementGetLocationList *)aGetLocationList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetMessageAttachPhoto : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetMessageAttachPhoto * GetMessageAttachPhoto;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetMessageAttachPhoto:(Web_x0020_Service_ElementGetMessageAttachPhoto *)aGetMessageAttachPhoto
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetMessageCategoryList : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetMessageCategoryList * GetMessageCategoryList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetMessageCategoryList:(Web_x0020_Service_ElementGetMessageCategoryList *)aGetMessageCategoryList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetMessageCategoryList2 : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetMessageCategoryList2 * GetMessageCategoryList2;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetMessageCategoryList2:(Web_x0020_Service_ElementGetMessageCategoryList2 *)aGetMessageCategoryList2
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetMessageItemList : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetMessageItemList * GetMessageItemList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetMessageItemList:(Web_x0020_Service_ElementGetMessageItemList *)aGetMessageItemList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetMessageItemList2 : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetMessageItemList2 * GetMessageItemList2;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetMessageItemList2:(Web_x0020_Service_ElementGetMessageItemList2 *)aGetMessageItemList2
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetMessageTemplateList : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetMessageTemplateList * GetMessageTemplateList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetMessageTemplateList:(Web_x0020_Service_ElementGetMessageTemplateList *)aGetMessageTemplateList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetMinibarCategoryList : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetMinibarCategoryList * GetMinibarCategoryList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetMinibarCategoryList:(Web_x0020_Service_ElementGetMinibarCategoryList *)aGetMinibarCategoryList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetMinibarItemList : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetMinibarItemList * GetMinibarItemList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetMinibarItemList:(Web_x0020_Service_ElementGetMinibarItemList *)aGetMinibarItemList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetNewMessage : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetNewMessage * GetNewMessage;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetNewMessage:(Web_x0020_Service_ElementGetNewMessage *)aGetNewMessage
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetNoOfAdHocMessage : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetNoOfAdHocMessage * GetNoOfAdHocMessage;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetNoOfAdHocMessage:(Web_x0020_Service_ElementGetNoOfAdHocMessage *)aGetNoOfAdHocMessage
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetNoOfAdHocMessageRoutine : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetNoOfAdHocMessageRoutine * GetNoOfAdHocMessageRoutine;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetNoOfAdHocMessageRoutine:(Web_x0020_Service_ElementGetNoOfAdHocMessageRoutine *)aGetNoOfAdHocMessageRoutine
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetNumberOfGuest : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetNumberOfGuest * GetNumberOfGuest;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetNumberOfGuest:(Web_x0020_Service_ElementGetNumberOfGuest *)aGetNumberOfGuest
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetOOSBlockRoomsList : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetOOSBlockRoomsList * GetOOSBlockRoomsList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetOOSBlockRoomsList:(Web_x0020_Service_ElementGetOOSBlockRoomsList *)aGetOOSBlockRoomsList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetOtherActivitiesLocationRoutine : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetOtherActivitiesLocationRoutine * GetOtherActivitiesLocationRoutine;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetOtherActivitiesLocationRoutine:(Web_x0020_Service_ElementGetOtherActivitiesLocationRoutine *)aGetOtherActivitiesLocationRoutine
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetOtherActivitiesStatusRoutine : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetOtherActivitiesStatusRoutine * GetOtherActivitiesStatusRoutine;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetOtherActivitiesStatusRoutine:(Web_x0020_Service_ElementGetOtherActivitiesStatusRoutine *)aGetOtherActivitiesStatusRoutine
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetOtherActivityAssignmentRoutine : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetOtherActivityAssignmentRoutine * GetOtherActivityAssignmentRoutine;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetOtherActivityAssignmentRoutine:(Web_x0020_Service_ElementGetOtherActivityAssignmentRoutine *)aGetOtherActivityAssignmentRoutine
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetPanicAlertConfig : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetPanicAlertConfig * GetPanicAlertConfig;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetPanicAlertConfig:(Web_x0020_Service_ElementGetPanicAlertConfig *)aGetPanicAlertConfig
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetPopupMsgList : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetPopupMsgList * GetPopupMsgList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetPopupMsgList:(Web_x0020_Service_ElementGetPopupMsgList *)aGetPopupMsgList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetPostingHistoryRoutine : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetPostingHistoryRoutine * GetPostingHistoryRoutine;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetPostingHistoryRoutine:(Web_x0020_Service_ElementGetPostingHistoryRoutine *)aGetPostingHistoryRoutine
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetPrevRoomAssignmentList : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetPrevRoomAssignmentList * GetPrevRoomAssignmentList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetPrevRoomAssignmentList:(Web_x0020_Service_ElementGetPrevRoomAssignmentList *)aGetPrevRoomAssignmentList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetProfileNote : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetProfileNote * GetProfileNote;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetProfileNote:(Web_x0020_Service_ElementGetProfileNote *)aGetProfileNote
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetReceivedAdHocMessage : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetReceivedAdHocMessage * GetReceivedAdHocMessage;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetReceivedAdHocMessage:(Web_x0020_Service_ElementGetReceivedAdHocMessage *)aGetReceivedAdHocMessage
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetReceivedAdHocMessageRoutine : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetReceivedAdHocMessageRoutine * GetReceivedAdHocMessageRoutine;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetReceivedAdHocMessageRoutine:(Web_x0020_Service_ElementGetReceivedAdHocMessageRoutine *)aGetReceivedAdHocMessageRoutine
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetReleaseRoomsList : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetReleaseRoomsList * GetReleaseRoomsList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetReleaseRoomsList:(Web_x0020_Service_ElementGetReleaseRoomsList *)aGetReleaseRoomsList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetRoomAssignmentList : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetRoomAssignmentList * GetRoomAssignmentList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetRoomAssignmentList:(Web_x0020_Service_ElementGetRoomAssignmentList *)aGetRoomAssignmentList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetRoomDetail : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetRoomDetail * GetRoomDetail;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetRoomDetail:(Web_x0020_Service_ElementGetRoomDetail *)aGetRoomDetail
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetRoomDetailsRoutine : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetRoomDetailsRoutine * GetRoomDetailsRoutine;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetRoomDetailsRoutine:(Web_x0020_Service_ElementGetRoomDetailsRoutine *)aGetRoomDetailsRoutine
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetRoomRemarks : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetRoomRemarks * GetRoomRemarks;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetRoomRemarks:(Web_x0020_Service_ElementGetRoomRemarks *)aGetRoomRemarks
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetRoomSectionList : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetRoomSectionList * GetRoomSectionList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetRoomSectionList:(Web_x0020_Service_ElementGetRoomSectionList *)aGetRoomSectionList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetRoomSetGuideByRoomType : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetRoomSetGuideByRoomType * GetRoomSetGuideByRoomType;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetRoomSetGuideByRoomType:(Web_x0020_Service_ElementGetRoomSetGuideByRoomType *)aGetRoomSetGuideByRoomType
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetRoomSetGuideList : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetRoomSetGuideList * GetRoomSetGuideList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetRoomSetGuideList:(Web_x0020_Service_ElementGetRoomSetGuideList *)aGetRoomSetGuideList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetRoomStatusList : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetRoomStatusList * GetRoomStatusList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetRoomStatusList:(Web_x0020_Service_ElementGetRoomStatusList *)aGetRoomStatusList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetRoomTypeByRoomNoRoutine : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetRoomTypeByRoomNoRoutine * GetRoomTypeByRoomNoRoutine;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetRoomTypeByRoomNoRoutine:(Web_x0020_Service_ElementGetRoomTypeByRoomNoRoutine *)aGetRoomTypeByRoomNoRoutine
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetRoomTypeInventoryRoutine : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetRoomTypeInventoryRoutine * GetRoomTypeInventoryRoutine;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetRoomTypeInventoryRoutine:(Web_x0020_Service_ElementGetRoomTypeInventoryRoutine *)aGetRoomTypeInventoryRoutine
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetRoomTypeList : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetRoomTypeList * GetRoomTypeList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetRoomTypeList:(Web_x0020_Service_ElementGetRoomTypeList *)aGetRoomTypeList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetSecretsRoutine : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetSecretsRoutine * GetSecretsRoutine;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetSecretsRoutine:(Web_x0020_Service_ElementGetSecretsRoutine *)aGetSecretsRoutine
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetSendAdHocMessageRoutine : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetSendAdHocMessageRoutine * GetSendAdHocMessageRoutine;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetSendAdHocMessageRoutine:(Web_x0020_Service_ElementGetSendAdHocMessageRoutine *)aGetSendAdHocMessageRoutine
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetSentAdHocMessage : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetSentAdHocMessage * GetSentAdHocMessage;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetSentAdHocMessage:(Web_x0020_Service_ElementGetSentAdHocMessage *)aGetSentAdHocMessage
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetStillLoggedIn : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetStillLoggedIn * GetStillLoggedIn;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetStillLoggedIn:(Web_x0020_Service_ElementGetStillLoggedIn *)aGetStillLoggedIn
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetSupervisorFiltersRoutine : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetSupervisorFiltersRoutine * GetSupervisorFiltersRoutine;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetSupervisorFiltersRoutine:(Web_x0020_Service_ElementGetSupervisorFiltersRoutine *)aGetSupervisorFiltersRoutine
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetSupervisorFindFloorList : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetSupervisorFindFloorList * GetSupervisorFindFloorList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetSupervisorFindFloorList:(Web_x0020_Service_ElementGetSupervisorFindFloorList *)aGetSupervisorFindFloorList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetSupervisorFindRoomAssignmentLists : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetSupervisorFindRoomAssignmentLists * GetSupervisorFindRoomAssignmentLists;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetSupervisorFindRoomAssignmentLists:(Web_x0020_Service_ElementGetSupervisorFindRoomAssignmentLists *)aGetSupervisorFindRoomAssignmentLists
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetUnassignRoomList : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetUnassignRoomList * GetUnassignRoomList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetUnassignRoomList:(Web_x0020_Service_ElementGetUnassignRoomList *)aGetUnassignRoomList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetUserDetailsRoutine : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetUserDetailsRoutine * GetUserDetailsRoutine;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetUserDetailsRoutine:(Web_x0020_Service_ElementGetUserDetailsRoutine *)aGetUserDetailsRoutine
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetUserList : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetUserList * GetUserList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetUserList:(Web_x0020_Service_ElementGetUserList *)aGetUserList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetVersion : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetVersion * GetVersion;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetVersion:(Web_x0020_Service_ElementGetVersion *)aGetVersion
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetZoneList : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetZoneList * GetZoneList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetZoneList:(Web_x0020_Service_ElementGetZoneList *)aGetZoneList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_GetZoneRoomList : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementGetZoneRoomList * GetZoneRoomList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	GetZoneRoomList:(Web_x0020_Service_ElementGetZoneRoomList *)aGetZoneRoomList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_LogInvalidStartRoom : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementLogInvalidStartRoom * LogInvalidStartRoom;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	LogInvalidStartRoom:(Web_x0020_Service_ElementLogInvalidStartRoom *)aLogInvalidStartRoom
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostAdditionalJobStatusUpdateRoutine : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementPostAdditionalJobStatusUpdateRoutine * PostAdditionalJobStatusUpdateRoutine;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	PostAdditionalJobStatusUpdateRoutine:(Web_x0020_Service_ElementPostAdditionalJobStatusUpdateRoutine *)aPostAdditionalJobStatusUpdateRoutine
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostAmenitiesItem : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementPostAmenitiesItem * PostAmenitiesItem;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	PostAmenitiesItem:(Web_x0020_Service_ElementPostAmenitiesItem *)aPostAmenitiesItem
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostAmenitiesItemsList : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementPostAmenitiesItemsList * PostAmenitiesItemsList;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	PostAmenitiesItemsList:(Web_x0020_Service_ElementPostAmenitiesItemsList *)aPostAmenitiesItemsList
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostChecklistItem : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementPostChecklistItem * PostChecklistItem;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	PostChecklistItem:(Web_x0020_Service_ElementPostChecklistItem *)aPostChecklistItem
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostChecklistItems : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementPostChecklistItems * PostChecklistItems;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	PostChecklistItems:(Web_x0020_Service_ElementPostChecklistItems *)aPostChecklistItems
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostECAttachPhoto : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementPostECAttachPhoto * PostECAttachPhoto;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	PostECAttachPhoto:(Web_x0020_Service_ElementPostECAttachPhoto *)aPostECAttachPhoto
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostEngineeringCase : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementPostEngineeringCase * PostEngineeringCase;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	PostEngineeringCase:(Web_x0020_Service_ElementPostEngineeringCase *)aPostEngineeringCase
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostLFAttachPhoto : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementPostLFAttachPhoto * PostLFAttachPhoto;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	PostLFAttachPhoto:(Web_x0020_Service_ElementPostLFAttachPhoto *)aPostLFAttachPhoto
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostLaundryItem : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementPostLaundryItem * PostLaundryItem;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	PostLaundryItem:(Web_x0020_Service_ElementPostLaundryItem *)aPostLaundryItem
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostLaundryOrder : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementPostLaundryOrder * PostLaundryOrder;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	PostLaundryOrder:(Web_x0020_Service_ElementPostLaundryOrder *)aPostLaundryOrder
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostLaundrySpecialInstruction : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementPostLaundrySpecialInstruction * PostLaundrySpecialInstruction;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	PostLaundrySpecialInstruction:(Web_x0020_Service_ElementPostLaundrySpecialInstruction *)aPostLaundrySpecialInstruction
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostLinenItem : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementPostLinenItem * PostLinenItem;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	PostLinenItem:(Web_x0020_Service_ElementPostLinenItem *)aPostLinenItem
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostLinenItems : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementPostLinenItems * PostLinenItems;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	PostLinenItems:(Web_x0020_Service_ElementPostLinenItems *)aPostLinenItems
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostLogout : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementPostLogout * PostLogout;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	PostLogout:(Web_x0020_Service_ElementPostLogout *)aPostLogout
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostLostAndFoundRoutine : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementPostLostAndFoundRoutine * PostLostAndFoundRoutine;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	PostLostAndFoundRoutine:(Web_x0020_Service_ElementPostLostAndFoundRoutine *)aPostLostAndFoundRoutine
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostLostFound : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementPostLostFound * PostLostFound;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	PostLostFound:(Web_x0020_Service_ElementPostLostFound *)aPostLostFound
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostMessage : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementPostMessage * PostMessage;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	PostMessage:(Web_x0020_Service_ElementPostMessage *)aPostMessage
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostMessageAttachPhoto : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementPostMessageAttachPhoto * PostMessageAttachPhoto;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	PostMessageAttachPhoto:(Web_x0020_Service_ElementPostMessageAttachPhoto *)aPostMessageAttachPhoto
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostMinibarItem : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementPostMinibarItem * PostMinibarItem;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	PostMinibarItem:(Web_x0020_Service_ElementPostMinibarItem *)aPostMinibarItem
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostMinibarItems : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementPostMinibarItems * PostMinibarItems;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	PostMinibarItems:(Web_x0020_Service_ElementPostMinibarItems *)aPostMinibarItems
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostMinibarOrder : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementPostMinibarOrder * PostMinibarOrder;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	PostMinibarOrder:(Web_x0020_Service_ElementPostMinibarOrder *)aPostMinibarOrder
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostPostingHistoryRoutine : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementPostPostingHistoryRoutine * PostPostingHistoryRoutine;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	PostPostingHistoryRoutine:(Web_x0020_Service_ElementPostPostingHistoryRoutine *)aPostPostingHistoryRoutine
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostUnassignRoomRoutine : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementPostUnassignRoomRoutine * PostUnassignRoomRoutine;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	PostUnassignRoomRoutine:(Web_x0020_Service_ElementPostUnassignRoomRoutine *)aPostUnassignRoomRoutine
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostUpdateAdHocMessageRoutine : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementPostUpdateAdHocMessageRoutine * PostUpdateAdHocMessageRoutine;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	PostUpdateAdHocMessageRoutine:(Web_x0020_Service_ElementPostUpdateAdHocMessageRoutine *)aPostUpdateAdHocMessageRoutine
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostUpdateDeviceTokenRoutine : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementPostUpdateDeviceTokenRoutine * PostUpdateDeviceTokenRoutine;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	PostUpdateDeviceTokenRoutine:(Web_x0020_Service_ElementPostUpdateDeviceTokenRoutine *)aPostUpdateDeviceTokenRoutine
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostUpdateOtherActivityStatusRoutine : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementPostUpdateOtherActivityStatusRoutine * PostUpdateOtherActivityStatusRoutine;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	PostUpdateOtherActivityStatusRoutine:(Web_x0020_Service_ElementPostUpdateOtherActivityStatusRoutine *)aPostUpdateOtherActivityStatusRoutine
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostUpdateRoomDetailsRoutine : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementPostUpdateRoomDetailsRoutine * PostUpdateRoomDetailsRoutine;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	PostUpdateRoomDetailsRoutine:(Web_x0020_Service_ElementPostUpdateRoomDetailsRoutine *)aPostUpdateRoomDetailsRoutine
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostWSLog : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementPostWSLog * PostWSLog;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	PostWSLog:(Web_x0020_Service_ElementPostWSLog *)aPostWSLog
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PosteCnAhMsg : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementPosteCnAhMsg * PosteCnAhMsg;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	PosteCnAhMsg:(Web_x0020_Service_ElementPosteCnAhMsg *)aPosteCnAhMsg
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PosteCnJob : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementPosteCnJob * PosteCnJob;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	PosteCnJob:(Web_x0020_Service_ElementPosteCnJob *)aPosteCnJob
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostingLinenItems : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementPostingLinenItems * PostingLinenItems;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	PostingLinenItems:(Web_x0020_Service_ElementPostingLinenItems *)aPostingLinenItems
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_PostingPanicAlert : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementPostingPanicAlert * PostingPanicAlert;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	PostingPanicAlert:(Web_x0020_Service_ElementPostingPanicAlert *)aPostingPanicAlert
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_RemoveLaundryItem : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementRemoveLaundryItem * RemoveLaundryItem;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	RemoveLaundryItem:(Web_x0020_Service_ElementRemoveLaundryItem *)aRemoveLaundryItem
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_StartAdditionalJob : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementStartAdditionalJob * StartAdditionalJob;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	StartAdditionalJob:(Web_x0020_Service_ElementStartAdditionalJob *)aStartAdditionalJob
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_StopAdditionalJob : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementStopAdditionalJob * StopAdditionalJob;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	StopAdditionalJob:(Web_x0020_Service_ElementStopAdditionalJob *)aStopAdditionalJob
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_UpdateAdHocMessageStatus : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementUpdateAdHocMessageStatus * UpdateAdHocMessageStatus;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	UpdateAdHocMessageStatus:(Web_x0020_Service_ElementUpdateAdHocMessageStatus *)aUpdateAdHocMessageStatus
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_UpdateAdditionalJobRemark : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementUpdateAdditionalJobRemark * UpdateAdditionalJobRemark;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	UpdateAdditionalJobRemark:(Web_x0020_Service_ElementUpdateAdditionalJobRemark *)aUpdateAdditionalJobRemark
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_UpdateAdditionalJobStatus : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementUpdateAdditionalJobStatus * UpdateAdditionalJobStatus;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	UpdateAdditionalJobStatus:(Web_x0020_Service_ElementUpdateAdditionalJobStatus *)aUpdateAdditionalJobStatus
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_UpdateCleaningStatus : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementUpdateCleaningStatus * UpdateCleaningStatus;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	UpdateCleaningStatus:(Web_x0020_Service_ElementUpdateCleaningStatus *)aUpdateCleaningStatus
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_UpdateGuestInfo : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementUpdateGuestInfo * UpdateGuestInfo;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	UpdateGuestInfo:(Web_x0020_Service_ElementUpdateGuestInfo *)aUpdateGuestInfo
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_UpdateInspection : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementUpdateInspection * UpdateInspection;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	UpdateInspection:(Web_x0020_Service_ElementUpdateInspection *)aUpdateInspection
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_UpdatePopupMsgStatus : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementUpdatePopupMsgStatus * UpdatePopupMsgStatus;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	UpdatePopupMsgStatus:(Web_x0020_Service_ElementUpdatePopupMsgStatus *)aUpdatePopupMsgStatus
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_UpdateRoomAssignment : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementUpdateRoomAssignment * UpdateRoomAssignment;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	UpdateRoomAssignment:(Web_x0020_Service_ElementUpdateRoomAssignment *)aUpdateRoomAssignment
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_UpdateRoomCleaningStatus : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementUpdateRoomCleaningStatus * UpdateRoomCleaningStatus;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	UpdateRoomCleaningStatus:(Web_x0020_Service_ElementUpdateRoomCleaningStatus *)aUpdateRoomCleaningStatus
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_UpdateRoomDetails : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementUpdateRoomDetails * UpdateRoomDetails;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	UpdateRoomDetails:(Web_x0020_Service_ElementUpdateRoomDetails *)aUpdateRoomDetails
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_UpdateRoomRemarks : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementUpdateRoomRemarks * UpdateRoomRemarks;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	UpdateRoomRemarks:(Web_x0020_Service_ElementUpdateRoomRemarks *)aUpdateRoomRemarks
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_UpdateRoomStatus : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementUpdateRoomStatus * UpdateRoomStatus;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	UpdateRoomStatus:(Web_x0020_Service_ElementUpdateRoomStatus *)aUpdateRoomStatus
;
@end
@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_ValidateRoomNo : Web_x0020_Service_Web_x0020_ServiceSoap12BindingOperation
@property(nonatomic, strong) Web_x0020_Service_ElementValidateRoomNo * ValidateRoomNo;

- (id)initWithBinding:(Web_x0020_Service_Web_x0020_ServiceSoap12Binding *)aBinding success:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingSuccessBlock)success error:(Web_x0020_Service_Web_x0020_ServiceSoap12BindingErrorBlock)error
	ValidateRoomNo:(Web_x0020_Service_ElementValidateRoomNo *)aValidateRoomNo
;
@end

@interface Web_x0020_Service_Web_x0020_ServiceSoap12Binding_envelope : NSObject
+ (NSString *)serializedFormUsingDelegate:(id)delegate;
@end

@interface Web_x0020_Service_Web_x0020_ServiceSoap12BindingResponse : NSObject
@property(nonatomic, strong) NSArray *headers;
@property(nonatomic, strong) NSArray *bodyParts;
@property(nonatomic, strong) NSError *error;
@end
