//
//  CheckMemory.h
//  me360
//
//  Created by Son Nguyen on 9/15/10.
//  Copyright 2010 TMS. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface CheckMemory : NSObject {

}
+ (CheckMemory*) sharedCheckMemory;

-(NSString*) getMemoryStatus;
- (void) print_free_memory;
-(NSString*) getFreeMemory;
@end
