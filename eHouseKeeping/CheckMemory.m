//
//  CheckMemory.m
//  me360
//
//  Created by Son Nguyen on 9/15/10.
//  Copyright 2010 TMS. All rights reserved.
//

#import "CheckMemory.h"
#import <mach/mach.h>
#import <mach/mach_host.h>
#import "LogFileManager.h"

@implementation CheckMemory

static  CheckMemory* sharedCheckMemoryInstance = nil;

+ (CheckMemory*) sharedCheckMemory{
	if (sharedCheckMemoryInstance == nil) {
        sharedCheckMemoryInstance = [[super allocWithZone:NULL] init];
    }
    return sharedCheckMemoryInstance;
}
+ (id)allocWithZone:(NSZone *)zone
{
    return [[self sharedCheckMemory] retain];
}

- (id)copyWithZone:(NSZone *)zone
{
    return self;
}

- (id)retain
{
    return self;
}

- (NSUInteger)retainCount
{
    return NSUIntegerMax;  //denotes an object that cannot be released
}

- (void)release
{
    //do nothing
}

- (id)autorelease
{
    return self;
}


-(void) print_free_memory {
	
    mach_port_t host_port;
    mach_msg_type_number_t host_size;
    vm_size_t pagesize;
    
    host_port = mach_host_self();
    host_size = sizeof(vm_statistics_data_t) / sizeof(integer_t);
    host_page_size(host_port, &pagesize);        
	
    vm_statistics_data_t vm_stat;
	
    if (host_statistics(host_port, HOST_VM_INFO, (host_info_t)&vm_stat, &host_size) != KERN_SUCCESS)
    {
        if([LogFileManager isLogConsole])
        {
            NSLog(@"Failed to fetch vm statistics");
        }
    }
	
    // Stats in bytes
    //natural_t mem_used = (vm_stat.active_count +
//                          vm_stat.inactive_count +
//                          vm_stat.wire_count) * pagesize;
    //natural_t mem_free = vm_stat.free_count * pagesize;
    //natural_t mem_total = mem_used + mem_free;
    //NSLog(@"used: %u free: %u total: %u", mem_used, mem_free, mem_total);
	//NSString *temp = [NSString stringWithFormat:@"used: %u free: %u total: %u", mem_used, mem_free, mem_total];
	//UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Memory info" message:temp delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
	//[alert show];	
	//[alert release];
	
}

-(NSString*) getMemoryStatus {
	
    mach_port_t host_port;
    mach_msg_type_number_t host_size;
    vm_size_t pagesize;
    
    host_port = mach_host_self();
    host_size = sizeof(vm_statistics_data_t) / sizeof(integer_t);
    host_page_size(host_port, &pagesize);        
	
    vm_statistics_data_t vm_stat;
	
    if (host_statistics(host_port, HOST_VM_INFO, (host_info_t)&vm_stat, &host_size) != KERN_SUCCESS)
    {
        if([LogFileManager isLogConsole])
        {
            NSLog(@"Failed to fetch vm statistics");
        }
    }
	
    // Stats in bytes
    natural_t mem_used = (vm_stat.active_count +
                          vm_stat.inactive_count +
                          vm_stat.wire_count) * pagesize;
    natural_t mem_free = vm_stat.free_count * pagesize;
    natural_t mem_total = mem_used + mem_free;
    //NSLog(@"used: %u free: %u total: %u", mem_used, mem_free, mem_total);
	NSString *temp = [NSString stringWithFormat:@"used: %u free: %u total: %u", mem_used, mem_free, mem_total];
	return temp;
	//UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Memory info" message:temp delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
	//[alert show];	
	//[alert release];
	
}

-(NSString*) getFreeMemory {
	
    mach_port_t host_port;
    mach_msg_type_number_t host_size;
    vm_size_t pagesize;
    
    host_port = mach_host_self();
    host_size = sizeof(vm_statistics_data_t) / sizeof(integer_t);
    host_page_size(host_port, &pagesize);        
	
    vm_statistics_data_t vm_stat;
	
    if (host_statistics(host_port, HOST_VM_INFO, (host_info_t)&vm_stat, &host_size) != KERN_SUCCESS)
    {
        if([LogFileManager isLogConsole])
        {
            NSLog(@"Failed to fetch vm statistics");
        }
    }
	
    natural_t mem_free = vm_stat.free_count * pagesize;
    //NSLog(@"used: %u free: %u total: %u", mem_used, mem_free, mem_total);
	NSString *temp = [NSString stringWithFormat:@"%u", mem_free];
	return temp;
	//UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Memory info" message:temp delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
	//[alert show];	
	//[alert release];
	 
}




@end
