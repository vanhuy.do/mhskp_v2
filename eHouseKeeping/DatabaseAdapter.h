//
//  DatabaseAdapter.h
//  eHouseKeeping
//
//  Created by KhanhNguyen on 6/2/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface DatabaseAdapter : NSObject {
    sqlite3_stmt *sqlStament;
    sqlite3 *database;
//    NSInteger returnCode;
}
@property (nonatomic) sqlite3 *database;
@property (nonatomic) sqlite3_stmt *sqlStament;
//@property (nonatomic) NSInteger returnCode;
-(DatabaseAdapter*)openDatabase;
-(void)close;
-(void)resetSqlCommand;

//
- (void)setCacheSize:(NSUInteger)pages;
- (void)executeUpdateSQL:(NSString *) updateSQL;

@end
