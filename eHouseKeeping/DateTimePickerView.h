//
//  DateTimePickerView.h
//  eHouseKeeping
//
//  Created by chinh bui on 6/21/11.
//  Copyright 2011 TMA. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface DateTimePickerView : UIViewController {
    UIDatePicker *datePicker;
}
@property (nonatomic, retain) IBOutlet UIDatePicker *datePicker;
- (IBAction)Done:(id)sender;
@end
