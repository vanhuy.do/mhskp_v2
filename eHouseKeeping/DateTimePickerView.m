//
//  DateTimePickerView.m
//  eHouseKeeping
//
//  Created by chinh bui on 6/21/11.
//  Copyright 2011 TMA. All rights reserved.
//

#import "DateTimePickerView.h"
#import "ehkConvert.h"
#import "ehkDefines.h"
@implementation DateTimePickerView

#define background          @"bg.png"

@synthesize datePicker;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [datePicker release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //set background:
    UIImage *imgbg=[UIImage imageNamed:background];
    UIColor *bgColor=[[UIColor alloc] initWithPatternImage:imgbg];
    self.view.backgroundColor=bgColor;
    
    imgbg=nil;
    [bgColor release];
    [imgbg release];
    
}
- (IBAction)Done:(id)sender {
     NSDateFormatter* formatter = [[[NSDateFormatter alloc] init] autorelease];
    [formatter setDateFormat:@"yyyyMMdd:hhmm"];
    NSString *dateTimeData=[formatter stringFromDate:datePicker.date];
   
                             
    [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@",removePickerViewGuestInfoNotification]   object:dateTimeData];
    
}



- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
