//
//  DateTimeUtility.h
//  eHouseKeeping
//
//  Created by chinh bui on 6/22/11.
//  Copyright 2011 TMA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ehkConvert.h"

typedef enum : NSUInteger {
    DateComparison_GreaterThanCurrent,
    DateComparison_LessThanCurrent, //In the past
    DateComparison_EqualCurrent //In the furture
} DateTimeComparison;

@interface DateTimeUtility : NSObject {
    
}
+(NSString*)getDateTimeNow;
+(float)HoursByTime:(NSString*)startTime endTime:(NSString*)endTime;
+(NSString*)getHH_MMFromSecond:(int)second;
+(DateTimeComparison)compareDatetimeValue:(NSString*)datetimeToCompare;
+(NSString*)getMM_SSFromSecond:(int)numberOfSeconds;
@end
