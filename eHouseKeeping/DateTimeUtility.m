//
//  DateTimeUtility.m
//  eHouseKeeping
//
//  Created by chinh bui on 6/22/11.
//  Copyright 2011 TMA. All rights reserved.
//

#import "DateTimeUtility.h"


@implementation DateTimeUtility
/**
 * getDateTimeNow
 * Date :
 *
 * @return date time now with format : yyyyMMdd:HHmm.
 
 * @author Chinh.Bui <>
 * @since 2011
 */
+(NSString*)getDateTimeNow
{
    NSDate* now = [NSDate date];
    NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
    
    [dateFormatter setDateFormat:[ehkConvert getyyyyMMddhhmm]]; //yyyyMMdd:HHmmss
    return [dateFormatter stringFromDate:now];
}

/**
 * SecondsByTime
 * Date :
 *
 * @return return seconds from time start and time end.
 *
 *@param : startTime : time start with format: yyyyMMdd:hhmm.
 *
 *
 *@param : endTime : time end.
 *
 * @author Chinh.Bui <>
 * @since 2011
 */
+(float)HoursByTime:(NSString*)startTime endTime:(NSString*)endTime
{
    float result=0;
    NSDateFormatter *df = [[[NSDateFormatter alloc] init] autorelease];
    [df setDateFormat:[ehkConvert getyyyy_MM_dd_HH_mm_ss]];
    //[df setDateFormat:@"HH:mm:ss"];
    NSDate *date1 = [df dateFromString:startTime];
    NSDate *date2 = [df dateFromString:endTime];
    
    NSTimeInterval interval = [date2 timeIntervalSinceDate:date1];
    
    result = interval;
    //    NSLog(@"result = %f", result);
    return result;
}

+(NSString*)getHH_MMFromSecond:(int)second {
    int hour = second / 3600;
    int min = (int)((second - hour * 3600) / 60.0);
    NSString* hourStr;
    if (hour < 10) {
        hourStr = [NSString stringWithFormat:@"%.2d",hour];
    } else
        hourStr = [NSString stringWithFormat:@"%d",hour];
    NSString* minStr;
    if (min < 10) {
        minStr = [NSString stringWithFormat:@"%.2d",min];
    } else
        minStr = [NSString stringWithFormat:@"%d",min];
    NSString* result = [NSString stringWithFormat:@"%@:%@", hourStr, minStr];
    return result;
}

+(NSString *)getMM_SSFromSecond:(int)numberOfSeconds
{
    numberOfSeconds = numberOfSeconds * 60;
    int seconds = (numberOfSeconds % 3600) / 60;
    int minutes = numberOfSeconds / 3600;
    return [NSString stringWithFormat:@"%02d:%02d", minutes, seconds];
}

/*!
 Compare date string with current date
 datetimeToCompare should be format "yyyy-MM-dd HH:mm:ss"
 */
+(DateTimeComparison)compareDatetimeValue:(NSString*)datetimeToCompare {
    NSString *lastCleaningTime = datetimeToCompare;
    if (lastCleaningTime != nil) {
        NSDateFormatter *dateTimeFormat = [[NSDateFormatter alloc] init];
        [dateTimeFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate* cleaningDate = [dateTimeFormat dateFromString:lastCleaningTime];
        
        //Remove time from cleaning date
        unsigned int flags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit;
        NSCalendar* calendar = [NSCalendar currentCalendar];
        NSDateComponents* componentRemoved = [calendar components:flags fromDate:cleaningDate];
        NSDate* cleaningDateRemoveTime = [calendar dateFromComponents:componentRemoved];
        
        //Remove time from current date
        componentRemoved = [calendar components:flags fromDate:[NSDate date]];
        NSDate* currentDateRemoveTime = [calendar dateFromComponents:componentRemoved];
        
        [dateTimeFormat setDateFormat:@"dd / MM / yyyy"];
        lastCleaningTime = [dateTimeFormat stringFromDate:cleaningDate];
        
        NSCalendar *calender = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        NSDateComponents *components = [calender components:NSDayCalendarUnit fromDate:cleaningDateRemoveTime toDate:currentDateRemoveTime options:0];
        
        if (components.day == 0) {
            //lastCleaningTime = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_today];
            return DateComparison_EqualCurrent;
        }
        else if (components.day == 1) {
            //lastCleaningTime = [[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_yesterday];
            return DateComparison_LessThanCurrent; //In the past
        } else {
            return DateComparison_LessThanCurrent; //In the past
        }
    }
    return 0;
}

@end









