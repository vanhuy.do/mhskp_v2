//
//  DayPickerView.h
//  mHouseKeeping
//
//  Created by Khanh Phan on 11/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DayPickerView;

@protocol DayPickerViewDelegate <NSObject>

@optional

-(void)dayPickerViewController:(DayPickerView *)controller didSelectDayLogAt:(NSInteger)index;

@end

@interface DayPickerView : UIViewController<UIPickerViewDelegate,UIPickerViewDataSource>
{
    NSArray *dayLogList;
    IBOutlet UIPickerView *pickerView;
    BOOL isAlreadyLayoutSubviews;
    IBOutlet UIBarButtonItem *buttonDone;
    IBOutlet UIBarButtonItem *buttonCancel;
}

@property(nonatomic) int selectedRow;
@property (nonatomic, assign) id<DayPickerViewDelegate> delegate;

-(IBAction) cancelBtnClicked:(id)sender;
-(IBAction) doneBtnClicked:(id)sender;
-(IBAction) outsideClicked:(id)sender;
-(NSString *) getDataOfSelectedRow;

@end
