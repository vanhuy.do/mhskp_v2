//
//  DayPickerView.m
//  mHouseKeeping
//
//  Created by Khanh Phan on 11/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DayPickerView.h"
#import "DeviceManager.h"

@interface DayPickerView ()

@end

@implementation DayPickerView

@synthesize selectedRow;
@synthesize delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    NSString *strdaysago = [L_days_ago currentKeyToLanguage];
    NSString *str2daysago = [NSString stringWithFormat:@"2 %@", strdaysago];
    NSString *str3daysago = [NSString stringWithFormat:@"3 %@", strdaysago];
    NSString *str4daysago = [NSString stringWithFormat:@"4 %@", strdaysago];
    NSString *str5daysago = [NSString stringWithFormat:@"5 %@", strdaysago];
    NSString *str6daysago = [NSString stringWithFormat:@"6 %@", strdaysago];
    dayLogList = [[NSArray alloc] initWithObjects:[L_today currentKeyToLanguage], [L_yesterday currentKeyToLanguage], str2daysago, str3daysago, str4daysago, str5daysago, str6daysago, nil];
    //dayLogList = [[NSArray alloc] initWithObjects:[L_today currentKeyToLanguage], [L_yesterday currentKeyToLanguage], @"2 days ago", @"3 days ago", @"4 days ago", @"5 days ago", @"6 days ago",nil];
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    isAlreadyLayoutSubviews = NO;
    [self setCaptionsView];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

//Call after set frames serveral subviews of this view
-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0") && !isAlreadyLayoutSubviews){
        isAlreadyLayoutSubviews = YES;
        CGRect framePickerView = pickerView.frame;
        UIToolbar *blurView = [[UIToolbar alloc] initWithFrame:framePickerView];
        blurView.translucent = YES;
        [self.view insertSubview:blurView belowSubview:pickerView];
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return [dayLogList count];
}
-(NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return [dayLogList objectAtIndex:row];
}
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    self.selectedRow = (int)row;
}

-(IBAction)cancelBtnClicked:(id)sender
{
    [self.view removeFromSuperview];
}

-(IBAction)doneBtnClicked:(id)sender
{
    [delegate dayPickerViewController:self didSelectDayLogAt:selectedRow];
    [self.view removeFromSuperview];
}

-(IBAction)outsideClicked:(id)sender
{
    [self.view removeFromSuperview];
}

-(NSString *) getDataOfSelectedRow
{
    return [dayLogList objectAtIndex:selectedRow];
}

-(void)setCaptionsView {
    [buttonDone setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_DONE]];
    [buttonCancel setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_CANCEL]];
}
@end
