//
//  LanguageAdapter.h
//  eHouseKeeping
//
//  Created by TMS2 on 6/29/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DbDefinesV2.h"
#import "DatabaseAdapter.h"

@class LanguageModel;
@interface LanguageAdapter : DatabaseAdapter {
    
}
-(NSString*) insertLanguageModel:(LanguageModel *)languageModel;
-(int) updatedLanguageModel:(LanguageModel *)languageModel;
-(LanguageModel *) loadLanguageModel:(LanguageModel *)languageModel;
-(NSMutableArray *)loadAllLanguageModel;
@end
