//
//  LanguageAdapter.m
//  eHouseKeeping
//
//  Created by TMS2 on 6/29/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "LanguageAdapter.h"
#import "LanguageModel.h"

@implementation LanguageAdapter

-(NSString*) insertLanguageModel:(LanguageModel *)languageModel
{
    if(self.sqlStament == nil) {
        NSString *sqlString = [[NSString alloc] initWithFormat:@"INSERT INTO %@ (%@, %@, %@, %@, %@, %@, %@) %@",LANGUAGE_REFERENCE,langRe_id,langRe_name, langRe_pack, langRe_active,   langRe_lastmodified, langRe_currencysymbol, langRe_currencynodigitafterdecimal,@"VALUES(?,?,?,?,?,?,?)"];
        const char *sql=[sqlString UTF8String];
        [sqlString release];
		if(sqlite3_prepare_v2(database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
        {
            //NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(database));
        }
    }
    sqlite3_bind_text(self.sqlStament, 1, [languageModel.lang_id UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 2, [languageModel.lang_name UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 3, [languageModel.lang_pack UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 4, [languageModel.lang_active UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 5, [languageModel.lang_lastmodified UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 6, [languageModel.lang_currencysymbol UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(self.sqlStament, 7, [languageModel.lang_currencynodigitafterdecimal UTF8String], -1, SQLITE_TRANSIENT);
    
    //        sqlite3_reset(self.sqlStament);
    NSString *returnInt = nil;
    if(SQLITE_DONE != sqlite3_step(self.sqlStament)){
        //NSAssert1(0, @"Error while inserting data. '%s'", sqlite3_errmsg(database));
        return returnInt;
    }
    else
    {
        sqlite3_last_insert_rowid(self.database);
        sqlite3_reset(self.sqlStament);
        //returnInt = languageModel.lang_id;
        return languageModel.lang_id;
    }

}

-(int) updatedLanguageModel:(LanguageModel *)languageModel
{
    if(self.sqlStament == nil) {
        NSString *sqlString = [[NSString alloc] initWithFormat:@"UPDATE %@ SET %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ? WHERE %@ = %@",LANGUAGE_REFERENCE, langRe_id, langRe_name, langRe_pack, langRe_active,  langRe_id, langRe_lastmodified, langRe_currencysymbol, langRe_currencynodigitafterdecimal, languageModel.lang_id];
        const char *sql = [sqlString UTF8String];
        [sqlString release];
		if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) != SQLITE_OK)
        {
        }
        
        sqlite3_bind_text(self.sqlStament, 1, [languageModel.lang_id UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(self.sqlStament, 2, [languageModel.lang_name UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(self.sqlStament, 3, [languageModel.lang_pack UTF8String], -1,SQLITE_TRANSIENT);
        sqlite3_bind_text(self.sqlStament, 4, [languageModel.lang_active UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(self.sqlStament, 5, [languageModel.lang_lastmodified UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(self.sqlStament, 6, [languageModel.lang_currencysymbol UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(self.sqlStament, 7, [languageModel.lang_currencynodigitafterdecimal UTF8String], -1, SQLITE_TRANSIENT);
        
    }
    NSInteger returnInt = 0;
    if(SQLITE_DONE != sqlite3_step(self.sqlStament))
    {
        sqlite3_reset(self.sqlStament);
         
        return (int)returnInt;
    }
    else { 
        return 1;
    }
}

-(LanguageModel *) loadLanguageModel:(LanguageModel *)languageModel
{
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ = ?",langRe_id, langRe_name, langRe_pack, langRe_active, langRe_lastmodified, langRe_currencysymbol, langRe_currencynodigitafterdecimal, LANGUAGE_REFERENCE, langRe_id];
    const char *sql =[sqlString UTF8String];

    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {
        
        sqlite3_bind_text(self.sqlStament, 1, [languageModel.lang_id UTF8String], -1, SQLITE_TRANSIENT);
        while(sqlite3_step(sqlStament) == SQLITE_ROW)
        {
            languageModel.lang_name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
            if (sqlite3_column_text(sqlStament, 2)) {
                languageModel.lang_pack = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
            }
            if (sqlite3_column_text(sqlStament, 3)){
                languageModel.lang_active = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
            }
            if (sqlite3_column_text(self.sqlStament, 4)) {
                languageModel.lang_lastmodified = [NSString stringWithUTF8String:(char *)sqlite3_column_text(self.sqlStament, 4)];
            }
            if (sqlite3_column_text(self.sqlStament, 5)) {
                languageModel.lang_currencysymbol = [NSString stringWithUTF8String:(char *) sqlite3_column_text(self.sqlStament, 5)];
            }
            if (sqlite3_column_text(self.sqlStament, 6)) {
                languageModel.lang_currencynodigitafterdecimal = [NSString stringWithUTF8String:(char *)sqlite3_column_text(self.sqlStament, 6)];
            }
            
        }
    }
    [sqlString release]; 
    //sqlite3_reset(self.sqlStament);
    return languageModel;

}

-(NSMutableArray *)loadAllLanguageModel;
{
    NSString *sqlString = [[NSString alloc] initWithFormat:@"SELECT * FROM %@ WHERE %@ = %d",LANGUAGE_REFERENCE, langRe_active, 1];
//    NSLog(@"sql = %@", sqlString);
    const char *sql =[sqlString UTF8String];
    [sqlString release];
    NSMutableArray *result = [NSMutableArray array];
//    NSLog(@"ma loi = %d", sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL));
    if(sqlite3_prepare_v2(self.database, sql, -1, &sqlStament, NULL) == SQLITE_OK) {          
        while(sqlite3_step(sqlStament) == SQLITE_ROW)
        {
            LanguageModel* languageModel = [[LanguageModel alloc] init];
            
            languageModel.lang_id   = [NSString stringWithUTF8String:(char*) sqlite3_column_text(sqlStament, 0)];
            languageModel.lang_name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 1)];
            if (sqlite3_column_text(sqlStament, 2)) {
                languageModel.lang_pack = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 2)];
            }
            if (sqlite3_column_text(sqlStament, 3)){
                languageModel.lang_active = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 3)];
            }
            if (sqlite3_column_text(sqlStament, 4)) {
                languageModel.lang_lastmodified = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStament, 4)];
            }
            if (sqlite3_column_text(self.sqlStament, 5)) {
                languageModel.lang_currencysymbol = [NSString stringWithUTF8String:(char *) sqlite3_column_text(self.sqlStament, 5)];
            }
            if (sqlite3_column_text(self.sqlStament, 6)) {
                languageModel.lang_currencynodigitafterdecimal = [NSString stringWithUTF8String:(char *)sqlite3_column_text(self.sqlStament, 6)];
            }
            [result addObject:languageModel];            
            [languageModel release];
        }
    }
     
    //sqlite3_reset(self.sqlStament);
//     NSLog(@"count = %d", [result count]);
    return result;
    
}

@end
