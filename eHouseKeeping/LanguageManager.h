//
//  LanguageManager.h
//  eHouseKeeping
//
//  Created by TMS2 on 6/30/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ehkDefines.h"
#import "LanguageAdapter.h"

@interface LanguageManager : NSObject <NSXMLParserDelegate>{
    NSMutableDictionary *dic_language;
//    LanguageAdapter* languageAdapter;
@private NSString *currentElementName;
@private NSMutableDictionary *tempDictionary;
@private BOOL returnNow;
}

//@property (nonatomic, retain) LanguageAdapter *languageAdapter;
@property (nonatomic, retain) NSMutableDictionary *dic_language;
@property (nonatomic, retain) NSString *currentElementName;
@property (nonatomic, retain) NSMutableDictionary *tempDictionary;

//+(NSMutableArray *)updateLanguageList;
//+ (LanguageManager*) sharedLanguageManager;
//
//-(NSString *)getCurrentLanguage;
//-(void)setCurrentLanguage:(NSString *) lang;
//
//-(NSMutableDictionary *)getDictionaryLanguage:(NSString *)langCode;
//-(void)setDictionaryLanguage:(NSMutableDictionary *)dic;
//-(NSMutableDictionary *) parseXMLToDictionary:(NSString *)langCode;
//-(NSMutableDictionary *)getCurrentDictionaryLanguageByCurrentUserLang:(NSString *)langCode;
//-(NSMutableDictionary *)getCurrentDictionaryLanguage;
//
//// Language Model
//-(NSString*) insertLanguageModel:(LanguageModel*)languageModels;
//-(int) updateLanguageModel:(LanguageModel*)languageModels;
//-(void) loadLanguageModel:(LanguageModel*)languageModels;
//-(NSMutableArray*)loadAllLanguageModel;

//-(NSString *) getStringLanguageByStringId:(id) stringId;
//
////functions get string of language
//-(NSString *) getUserID;
//-(NSString *) getTitleLogin;
//-(NSString *) getTitlePassword;
//-(NSString *) getCount;
//-(NSString *) getLaundry;
//-(NSString *) getGuide;
//-(NSString *) getROOMASSIGNMENT;
//-(NSString *) getLOCATION;
//-(NSString *) getTOWER;
//-(NSString *) getFLOORNO;
//-(NSString *) getPROPERTY;
//-(NSString *) getBUILDING;
//-(NSString *) getCompletedRooms;
//-(NSString *) getHOME;
//-(NSString *) getRoom;
//-(NSString*)getRoomDetailTitle;
//-(NSString *) getGuest;
//-(NSString *) getRMStatus;
//-(NSString *) getVIP;
//-(NSString *) getDuration;
//-(NSString *) getCleaningStatus;
//-(NSString *) getHousekeeperInfo;
//-(NSString *) getHousekeeper;
//-(NSString *) getTotalsRoomsCleaned;
//-(NSString *) getTotalTimeUsed;
//-(NSString *) getAverageCleaningTime;
//-(NSString *) getRoomDetail;
//-(NSString *) getGuestInfo;
//-(NSString *) getExpectedCleaningTime;
//-(NSString *) getGuestPreference;
//-(NSString *) getAdditionalJob;
//-(NSString *) getLOSTANDFOUND;
//-(NSString *) getENGINEERING;
//-(NSString *) getGUIDELINE;
//-(NSString *) getCOUNT;
//-(NSString *) getLAUNDRY;
//-(NSString *) getGUIDELINES;
//-(NSString *) getMESSAGELIST;
//-(NSString *) getMESSAGEDETAIL;
//-(NSString *) getNEWMESSAGE;
//-(NSString *) getMessageTopic;
//-(NSString *) getMessageTemplate;
//-(NSString *) getMessage;
//-(NSString *) getUser;
//-(NSString *) getVIPStatus;
//-(NSString *) getLanguagePreference;
//-(NSString *) getCheckIn;
//-(NSString *) getCheckOut;
//-(NSString *) getSelectAction;
//-(NSString *) getLoadingData;
//-(NSString *) getPleasewaitdot;
//-(NSString *) getCheckListRoom;
//-(NSString *) getBed;
//-(NSString *) getBathRoom;
//-(NSString *) getWritingDesk;
//-(NSString *) getLivingRoom;
//-(NSString *) getMirror;
//-(NSString *) getPass;
//-(NSString *) getFail;
//-(NSString *) getRating;
//-(NSString *) getComment;
//-(NSString *) getLanguage;
//-(NSString *) getDone;
//-(NSString *) getCancel;
//-(NSString *) getOK;
//-(NSString *) getYes;
//-(NSString *) getNo;
//-(NSString *) get7pt;
//-(NSString *) getMiniBar;
//-(NSString *) getCHARGEABLE;
//-(NSString *) getNONCHARGEABLE;
//-(NSString *) getBeverage;
//-(NSString *) getSnack;
//-(NSString *) getWater;
//-(NSString *) getItems;
//-(NSString *) getQuantity;
//-(NSString *) getPrice;
//-(NSString *) getEngineer;
////-(NSString *) getMoveUp;
////-(NSString *) getMoveDown;
////-(NSString *) getViewDetails;
//-(NSString *) getEngineeringCase;
//-(NSString *) getLocation;
//-(NSString *) getTypeofItem;
//-(NSString *) getTop10Items;
//-(NSString *) getRemark;
//-(NSString *) getATTACHPHOTO;
//-(NSString *) getDISPATCH;
//-(NSString *) getSelectLocation;
//-(NSString *) getSelectPhoto;
//-(NSString *) getLostandFountCase;
//-(NSString *) getCategories;
//-(NSString *) getColour;
//-(NSString *) getOnlyavailableforSupervisor;
//-(NSString *) getIncorrectUsernameorPassworddot;
//-(NSString *) getLoadingRoomData;
//-(NSString *) getAreYouSure;
//-(NSString *) getSigningIndot;
//-(NSString *) getSavesuccessfully;
//-(NSString *) getSyncompleted;
//-(NSString *) getSyncingdot;
//-(NSString *) getUserlocked;
//-(NSString *) getPleaseinputvaliddate;
//-(NSString *) getName;
//-(NSString *) getQuantity;
//-(NSString *) getPrice;
//-(NSString *) getNew;
//-(NSString *) getDeleteAll;
//-(NSString *) getAddNew;
//-(NSString *) getCollected;
//-(NSString *) getTotalCharge;
//-(NSString *) getFrom;
//-(NSString *) getAmenities;
//-(NSString *) getLinen;
//-(NSString *) getDelete;
//-(NSString *) getTakePhoto;
//-(NSString *) getEdit;
//-(NSString *) getSend;
//-(NSString *) getNonetworkconnection;
//-(NSString *) getNOROOMASSIGNED;
//-(NSString *) getNOMESSAGE;
//-(NSString *) getSavingData;
//-(NSString *) getSendingData;
//-(NSString *) getSendingfailedpleasetryagain;
//-(NSString *) getSendingsuccessfully;
//-(NSString *) getRefreshingdatahascompleted;
//-(NSString *) getCHECKLIST;
//-(NSString *) getMESSAGE;
//-(NSString *) getSAVE;
//-(NSString *) getSYNCNOW;
//-(NSString *) getSETTING;
//-(NSString *) getLOGOUT;
////-(NSString *) getLanguagepackloaded;
////-(NSString *) getDataerror;
////-(NSString *) getNodataloaded;
////-(NSString *) getOthererror;
////-(NSString *) getDND;
////-(NSString *) getSERVICELATER;
////-(NSString *) getDENIEDSERVICE;
////-(NSString *) getSTART;
////-(NSString *) getFINISH;
////-(NSString *) getPAUSE;
//-(NSString *) getROOMCOMPLETED;
//-(NSString *) getBack;
//-(NSString *) getAction;
//-(NSString *) getSearch;
//-(NSString *) getError;
//-(NSString *) getSyncincomplete;
//-(NSString *) getNewmessage;
//-(NSString *) getListuser;
//-(NSString *) getCantsavewhileediting;
//-(NSString *) getYoucanonlyattach3;
//-(NSString *) getNomoreitem;
//-(NSString *) getAdditemsuccessful;
//-(NSString *) getUpdateitemsuccessful;
//-(NSString *) getfrom5to60;
//-(NSString *) getPleasechoose;
//
//-(NSString *) getMsgNotSupervisor;
//-(NSString *) getMsgIncorrectLogin;
//-(NSString *) getMsgWaitingRoomDetail;
//-(NSString *) getMsgWaitingRoomDetailContent;
//-(NSString *) getMsgLogout;
//-(NSString *) getMsgAuthenticating;
//-(NSString *) getMsgAuthenticatingContent;
//-(NSString *) getMsgSaveMessage;
//-(NSString *) getMsgSyncMessage;
//-(NSString *) getMsgSyncRunningMessage;
//-(NSString *) getMsgAlertSaveAsign;
//-(NSString *) getRaSavingData;
//-(NSString *) getSendingData;
//-(NSString *) getSendingDataFail;
//-(NSString *) getSendingDataSuccesfully;
//-(NSString *) getMsgRefreshingData;
//-(NSString *) getError;
//-(NSString *) getOK;
//-(NSString *) getCancel;
//-(NSString *) getYes;
//-(NSString *) getNo;
//-(NSString *) getLogout;
//-(NSString *) getBtnSyncNow;
//-(NSString *) getMsgSyncIncomplete;
//-(NSString *) getMsgCanNotSaveWhenEdit;
//-(NSString *) getRaLoadingData;
//-(NSString *) getNoWifi;
//-(NSString *) getMsgCanOnlyChoose3Picture;
//-(NSString *) getMsgDoNotHaveMoreItemToAdd;
//-(NSString *) getMsgAddItemSuccessfully;
//-(NSString *) getMsgUpdateItemSuccessfully;
//-(NSString *) getDone;
//-(NSString *) getComment;
//-(NSString *) getMsgSyncPeriodSetting;
//-(NSString *) getMsgPleaseChoose;
//-(NSString *) getLocations;
//-(NSString *) getTypeOfItem;
//-(NSString *) getTop10Item;
//-(NSString *) getQuantity;
//-(NSString *) getColour;
//-(NSString *) getCategories;
//-(NSString *) getBack;
//-(NSString *) getNoRoomAssigned;
//-(NSString *) getDialogTotalCharge;
//-(NSString *) getLanguage;
//-(NSString *) getBtnSetting;
//-(NSString *) getEdit;
//-(NSString*) getCounts;
//-(NSString *) getLaundryRegular;
//-(NSString *) getLaundryExpress;
//-(NSString *) getRegular;
//-(NSString *) getExpress;
//-(NSString *) getMale;
//-(NSString *) getFemale;
//-(NSString *) getInstruction;
//-(NSString *) getGrandTotal;
//-(NSString *) getAddToCart;
//-(NSString *) getUsed;
//-(NSString *) getCart;
//-(NSString *) getSubtotal;
//-(NSString *) getPointsReceived;
//-(NSString *) getPointsPossible;
//-(NSString *) getPointsTotal;
//-(NSString *) getRoomStandards;
//-(NSString *) getChecklistStatus;
//-(NSString *) getOverral;
//-(NSString *)getUnassignedRoom;
//-(NSString *)getAssignedRoom;
//
//#pragma mark - Find Function
//-(NSString *) getAttendant;
//-(NSString *) getRoomTitle;
//-(NSString *) getNoResultsFound;
//-(NSString *) getFind;
//-(NSString *) getBy;
//-(NSString *) getAll;
//-(NSString *) getByRoomStatus;
//-(NSString *) getCurrent;
//-(NSString *) getTime;
//-(NSString *) getALL;
//-(NSString *) getNoRoom;
//-(NSString *) getFailInspection;
//-(NSString *) getCompletedRoom;
//-(NSString *) getPassInspection;
//-(NSString *) getPendingRoom;
//-(NSString *) getAheadSchedule;
//-(NSString *) getRoomPending;
//-(NSString *) getRoomComplete;
//
//#pragma mark - Message Alert Box
//-(NSString *) getMsgAlertSaveCheckList;
//-(NSString *) getMsgAlertSaveCheckListDetail;
//-(NSString *) getMsgAlertErrorCheckListDetail;
//-(NSString *) getMsgAlertSaveCounts;
//-(NSString *) getMsgAlertDiscardCounts;
//
//
//-(NSString *) getMsgAlertSaveRoomDetail;
@end