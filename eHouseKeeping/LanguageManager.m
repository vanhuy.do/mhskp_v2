//
//  LanguageManager.m
//  eHouseKeeping
//
//  Created by TMS2 on 6/30/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "LanguageManager.h"
#import "LanguageModel.h"
#import "ServiceDefines.h"
#import "LogFileManager.h"
#import "NSFileManager+DoNotBackup.h"

@interface LanguageManager (PrivateMethods)

    +(void) updateFileLangXMLWithURL:(NSString *)url;
    
@end

@implementation LanguageManager
@synthesize dic_language, currentElementName, tempDictionary;
//@synthesize languageAdapter;

static LanguageManager* sharedLanguageManagerInstance = nil;
static NSString *currentLang = nil;

+ (LanguageManager*) sharedLanguageManager
{
	if (sharedLanguageManagerInstance == nil) {
        currentLang = [NSString string];
        sharedLanguageManagerInstance = [[super alloc] init];
    }
    if (sharedLanguageManagerInstance.retainCount > 0) {
        if([LogFileManager isLogConsole])
        {
            NSLog(@"sharedLanguageManagerInstance retain count is OK +++++++");
        }
    }
    return sharedLanguageManagerInstance;	
}

//+ (id)allocWithZone:(NSZone *)zone
//{
//    return [[self sharedLanguageManager] retain];
//}

//- (id)copyWithZone:(NSZone *)zone
//{
//    return self;
//}

//- (id)retain
//{
//    return self;
//}

- (NSUInteger)retainCount
{
    return NSUIntegerMax;  //denotes an object that cannot be released
}

//- (void)release
//{
    //do nothing
//}

//- (id)autorelease
//{
//    return self;
//}

-(id)init {
    return [super init];
}

static NSMutableData *fileData = nil;
static NSString *fileName = nil;

// Get language list from server and update into database
//+(NSMutableArray *)updateLanguageList{
//    NSMutableArray *arrresult = [NSMutableArray array];
//    
//    
//    eHousekeepingServiceSoapBinding *binding = [eHousekeepingServiceSvc eHousekeepingServiceSoapBinding];
//    //binding.logXMLInOut = YES;
//    eHousekeepingServiceSvc_GetLanguageList *request = [[eHousekeepingServiceSvc_GetLanguageList alloc] init];
//    eHousekeepingServiceSoapBindingResponse *response = [binding GetLanguageListUsingParameters:request];
//    [request release];
//    NSArray *responseHeaders = response.headers;
//    
//    // Check header of response
//    for (id header in responseHeaders) {
//        // do something with header
//    }
//    
//    // Check body of response
//    NSArray *responseBodyParts = response.bodyParts;
//    for (id bodyPart in responseBodyParts) {
//        if ([bodyPart isKindOfClass:[SOAPFault class]]) {
//            NSLog(@"error: %@", ((SOAPFault *)bodyPart).simpleFaultString);
//            continue;
//        }
//        
//        // Get Language list
//        if ([bodyPart isKindOfClass:[eHousekeepingServiceSvc_GetLanguageListResponse class]]) {
//            eHousekeepingServiceSvc_GetLanguageListResponse *body = (eHousekeepingServiceSvc_GetLanguageListResponse *)bodyPart;
//            eHousekeepingServiceSvc_ArrayOfLanguageItem *languageList = [body.GetLanguageListResult LanguageList];
//            for (eHousekeepingServiceSvc_LanguageItem *languageItem in languageList.LanguageItem)
//            {                
//                LanguageModel *languageModel = [[LanguageModel alloc] init];  
//                languageModel.lang_id = languageItem.lgCode;
//                [[LanguageManagerV2 sharedLanguageManager] loadLanguageModel:languageModel];
//                
//                if ([languageModel.lang_lastmodified isEqualToString:languageItem.lgLastModified]) {
//                    
//                } else {
//                    if ([languageModel.lang_lastmodified length] > 0) {
//                        LanguageModel *lModel = [[LanguageModel alloc] init];
//                        lModel.lang_id = languageItem.lgCode;
//                        lModel.lang_name = languageItem.lgName;
//                        lModel.lang_lastmodified = languageItem.lgLastModified;
//                        lModel.lang_active = languageItem.lgIsActive;
//                        lModel.lang_pack = languageItem.lgXmlUrl;
//                        lModel.lang_currencysymbol = languageItem.lgCurrencySymbol;
//                        lModel.lang_currencynodigitafterdecimal = languageItem.lgCurrencyNoDigitAfterDecimal;
//                        [[LanguageManagerV2 sharedLanguageManager] updateLanguageModel:lModel];
//
//                        [lModel release];
//                    } else {
//                        LanguageModel *lModel = [[LanguageModel alloc] init];
//                        lModel.lang_id = languageItem.lgCode;
//                        lModel.lang_name = languageItem.lgName;
//                        lModel.lang_lastmodified = languageItem.lgLastModified;
//                        lModel.lang_active = languageItem.lgIsActive;
//                        lModel.lang_pack = languageItem.lgXmlUrl;
//                        lModel.lang_currencysymbol = languageItem.lgCurrencySymbol;
//                        lModel.lang_currencynodigitafterdecimal = languageItem.lgCurrencyNoDigitAfterDecimal;
//                        [[LanguageManagerV2 sharedLanguageManager] insertLanguageModel:lModel];
//                        
//                        [lModel release];
//                    }
//                    
//                    [arrresult addObject:languageItem.lgXmlUrl];
//                    //update file language xml
////                    [self updateFileLangXMLWithURL:languageItem.lgXmlUrl];
//                }
//                [languageModel release];
//            }
//            
//        }
//    }
//     
//    return arrresult;
//    
//}

-(void)dealloc{
//    [languageAdapter release];
//    [tempDictionary release];
//    if (currentElementName.retainCount > 0) {
//        [currentElementName release];
//        currentElementName = nil;
//    }
    
//    if (dic_language.retainCount > 0) {
//        [dic_language release];
//        dic_language = nil;
//    }
    
    [super dealloc];
}
    
+(void)updateFileLangXMLWithURL:(NSString *)url {
    fileData = [NSMutableData data];
    
    NSArray *arr = [url componentsSeparatedByString:@"/"];
    fileName = [arr objectAtIndex:[arr count]-1];
    
    NSString *file = [NSString stringWithFormat:@"%@", url];
    NSURL *fileURL = [NSURL URLWithString:[file stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLRequest *req = [NSURLRequest requestWithURL:fileURL];
    NSURLConnection *conn = [NSURLConnection connectionWithRequest:req delegate:self];
    [conn start];
}

-(NSMutableDictionary *)getCurrentDictionaryLanguage {
    if (sharedLanguageManagerInstance.dic_language != nil) {
        if ([sharedLanguageManagerInstance.dic_language retainCount] > 0) {
            if([LogFileManager isLogConsole])
            {
                NSLog(@"dictionary language is OK ==========");
            }
        }
        return sharedLanguageManagerInstance.dic_language;
    }
    if ([[sharedLanguageManagerInstance getCurrentLanguage] length] > 0) {
        return [sharedLanguageManagerInstance getDictionaryLanguage:[sharedLanguageManagerInstance getCurrentLanguage]];
    }
    return [self getDictionaryLanguage:(NSString *)ENGLISH_LANGUAGE];
}

-(NSMutableDictionary *)getCurrentDictionaryLanguageByCurrentUserLang:(NSString *)langCode {
    if ([[sharedLanguageManagerInstance.dic_language valueForKey:[NSString stringWithFormat:@"%@", L_TYPE_LANGUAGE]] isEqualToString:langCode]) {
        return sharedLanguageManagerInstance.dic_language;
    } else {
        return [self getDictionaryLanguage:langCode];
    }
}

-(NSMutableDictionary *)getDictionaryLanguage:(NSString *)langCode {
//    NSLog(@"language dictionary = %@", langCode);
    NSMutableDictionary *xdic;// = [NSMutableDictionary dictionary];
    if ([[NSUserDefaults standardUserDefaults] objectForKey:langCode]) {
        xdic = [[NSUserDefaults standardUserDefaults] objectForKey:langCode];
    } else {
        //parse langCode
        xdic = [self parseXMLToDictionary:langCode];
    }
    
    if (xdic.retainCount > 0) {
        if([LogFileManager isLogConsole])
        {
            NSLog(@"xdic retain Count is OK ===========");
        }
    }
    
    return xdic;
}

//parse langCode to Dictionary
-(void)setDictionaryLanguage:(NSMutableDictionary *)dic {
    sharedLanguageManagerInstance.dic_language = dic;
}

-(void) exitParseRunLoop {
    returnNow = YES;
}

-(NSMutableDictionary *) parseXMLToDictionary:(NSString *)langCode {
    self.tempDictionary = [NSMutableDictionary dictionary];
    returnNow = NO;
    [self.tempDictionary setValue:langCode forKey:[NSString stringWithFormat:@"%@", L_TYPE_LANGUAGE]];
    
    //load lang model from database to get file xml
    LanguageModel *lModel = [[LanguageModel alloc] init];
    lModel.lang_id = langCode;
    [self loadLanguageModel:lModel];
    //[lModel load];
    NSString *fileName = nil;
    if (lModel.lang_pack == nil) {
        //default is english language pack
        fileName = (NSString *)L_ENG_LANGUAGE_NAME;
    } else {
        NSArray *arr = [lModel.lang_pack componentsSeparatedByString:@"/"];
        fileName = [arr objectAtIndex:[arr count] - 1];
    }
    
    //NSArray *dirArray = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,    NSUserDomainMask, YES);
    NSArray *dirArray = NSSearchPathForDirectoriesInDomains(DataDirectory, NSUserDomainMask, YES);
//    NSLog(@"%@", [dirArray objectAtIndex:0]);
    
    NSString *path = [NSString stringWithFormat:@"%@/%@", [dirArray objectAtIndex:0], fileName];
    
    BOOL fileExist = [[NSFileManager defaultManager] fileExistsAtPath:path];
    
    if (fileExist == NO) {
        path = [NSString stringWithFormat:@"%@/%@", [dirArray objectAtIndex:0], L_ENG_LANGUAGE_NAME];
    }
    
    NSData *dataFile = [NSData dataWithContentsOfFile:path];
    NSXMLParser *parser = [[NSXMLParser alloc] initWithData:dataFile];
    [parser setDelegate: sharedLanguageManagerInstance];
    [parser parse];
    
    [lModel release];
    [parser release];

    [self performSelector:@selector(exitParseRunLoop) withObject:nil afterDelay:10];
    
    while (returnNow == NO) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:10]];
    }
    
    return self.tempDictionary;
}

#pragma mark - NSXMLParser Delegate Methods
-(void)parserDidStartDocument:(NSXMLParser *)parser {
    
}

-(void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    if ([elementName isEqualToString:@"string"]) {
        self.currentElementName = [attributeDict valueForKey:@"name"];
    } else {
        self.currentElementName = nil;
    }
}

-(void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    if(self.currentElementName != nil || [self.currentElementName length] > 0) {
        NSString *hadString = [tempDictionary valueForKey:currentElementName];
        if (hadString != nil) {
            [tempDictionary setValue:[NSString stringWithFormat:@"%@%@", hadString, string] forKey:currentElementName];
        } else
            [self.tempDictionary setValue:string forKey:self.currentElementName];
    }
    
//    if (self.currentElementName != nil || [self.currentElementName length] > 0) {
//        [self.tempDictionary setValue:string forKey:self.currentElementName];
//    }
}

-(void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    self.currentElementName = nil;
}

-(void)parserDidEndDocument:(NSXMLParser *)parser {
    [[NSUserDefaults standardUserDefaults] setObject:self.tempDictionary forKey:[self.tempDictionary valueForKey:[NSString stringWithFormat:@"%@", L_TYPE_LANGUAGE]]];
    returnNow = YES;
}

#pragma mark - Current Language Methods
-(NSString *)getCurrentLanguage {
    if (currentLang == nil) {
        return [NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE];
    }
    return currentLang;
}

-(void)setCurrentLanguage:(NSString *)lang {
    if ([currentLang isEqualToString:lang] == NO) {
        currentLang = lang;
        sharedLanguageManagerInstance.dic_language = [sharedLanguageManagerInstance parseXMLToDictionary:currentLang];
    }
}


// Language model
-(NSString*) insertLanguageModel:(LanguageModel*)languageModels
{
    /*
    if (languageAdapter == nil) {
        languageAdapter = [[LanguageAdapter alloc] init];
    }
    [languageAdapter openDatabase];
    [languageAdapter resetSqlCommand];
    NSString* result = [languageAdapter insertLanguageModel:languageModels];
    [languageAdapter close];
    return result;
     */
    
    LanguageAdapter *languageAdapter1 = [[LanguageAdapter alloc] init];
    [languageAdapter1 openDatabase];
    [languageAdapter1 resetSqlCommand];
    NSString* result = [languageAdapter1 insertLanguageModel:languageModels];
    [languageAdapter1 close];
    [languageAdapter1 release];
    return result;
    
}

-(int) updateLanguageModel:(LanguageModel*)languageModels {
    /*
    if (languageAdapter == nil) {
        languageAdapter = [[LanguageAdapter alloc] init];
    }
    [languageAdapter openDatabase];
    [languageAdapter resetSqlCommand];
    int result = [languageAdapter updatedLanguageModel:languageModels];
    [languageAdapter close];
    return result;
     */
    
    LanguageAdapter *languageAdapter1 = [[LanguageAdapter alloc] init];
    [languageAdapter1 openDatabase];
    [languageAdapter1 resetSqlCommand];
    int result = [languageAdapter1 updatedLanguageModel:languageModels];
    [languageAdapter1 close];
    [languageAdapter1 release];
    return result;
}

-(void) loadLanguageModel:(LanguageModel*)languageModels
{
    /*
    if (languageAdapter == nil) {
        languageAdapter = [[LanguageAdapter alloc] init];
    }
    [languageAdapter openDatabase];
    [languageAdapter resetSqlCommand];
    [languageAdapter loadLanguageModel:languageModels];
    [languageAdapter close];
     */
    
    LanguageAdapter *languageAdapter1 = [[LanguageAdapter alloc] init];
    [languageAdapter1 openDatabase];
    [languageAdapter1 resetSqlCommand];
    [languageAdapter1 loadLanguageModel:languageModels];
    [languageAdapter1 close];
    [languageAdapter1 release];
}


-(NSMutableArray*)loadAllLanguageModel
{
    /*
    if (languageAdapter == nil) {
        languageAdapter = [[LanguageAdapter alloc] init];
    }
    [languageAdapter openDatabase];
    [languageAdapter resetSqlCommand];
    NSMutableArray* array = [languageAdapter loadAllLanguageModel];
    [languageAdapter close];
    return array;
     */
    LanguageAdapter *lAdapter = [[LanguageAdapter alloc] init];
    [lAdapter openDatabase];
    [lAdapter resetSqlCommand];
    NSMutableArray* array = [lAdapter loadAllLanguageModel];
    [lAdapter close];
    [lAdapter release];
    return array;
}

#pragma mark - === Function to get language string base on current language with string id
-(NSString *)getStringLanguageByStringId:(id)stringId {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    
    NSString *s = [dic valueForKey:stringId];
    
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:stringId];
    }
    
    return s ? s : @"";
}

#pragma mark - get string from current language
//functions get string of language
-(NSString *) getMsgNotSupervisor{
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_NOTSUPERVISOR]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_NOTSUPERVISOR]];
    }
    return s;
}
-(NSString *) getMsgIncorrectLogin{
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_INCORRECT_LOGIN]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_INCORRECT_LOGIN]];
    }
    return s;
}
-(NSString *) getMsgWaitingRoomDetail{
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_WAITING_ROOM_DETAIL]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_WAITING_ROOM_DETAIL]];
    }
    return s;
}
-(NSString *) getMsgWaitingRoomDetailContent{
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_WAITING_ROOM_DETAIL_CONTENT]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_WAITING_ROOM_DETAIL_CONTENT]];
    }
    return s;
}
-(NSString *) getMsgLogout{
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_LOGOUT]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_LOGOUT]];
    }
    return s;
}
-(NSString *) getMsgAuthenticating{
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_AUTHENTICATING]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_AUTHENTICATING]];
    }
    return s;
}
-(NSString *) getMsgAuthenticatingContent{
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_AUTHENTICATING_CONTENT]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_AUTHENTICATING_CONTENT]];
    }
    return s;
}

//-(NSString *) getMessageResetDb{
//    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
//    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_RESET_DATABASE]];
//    if (s == nil) {
//        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
//        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_RESET_DATABASE]];
//    }
//    return s;
//}

-(NSString *) getMsgSaveMessage{
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_SAVE_MESSAGE]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_SAVE_MESSAGE]];
    }
    return s;
}
-(NSString *) getMsgSyncMessage{
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_SYNC_MESSAGE]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_SYNC_MESSAGE]];
    }
    return s;
}
-(NSString *) getMsgSyncRunningMessage{
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_SYNC_RUNNING_MESSAGE]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_SYNC_RUNNING_MESSAGE]];
    }
    return s;
}
-(NSString *) getRaSavingData{
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_RA_SAVING_DATA]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_RA_SAVING_DATA]];
    }
    return s;
}
-(NSString *) getSendingData{
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_SENDING_DATA]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_SENDING_DATA]];
    }
    return s;
}
-(NSString *) getSendingDataFail{
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_SENDING_DATA_FAIL]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_SENDING_DATA_FAIL]];
    }
    return s;
}
-(NSString *) getSendingDataSuccesfully{
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_SENDING_DATA_SUCCESFULLY]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_SENDING_DATA_SUCCESFULLY]];
    }
    return s;
}
-(NSString *) getMsgRefreshingData{
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_REFRESHING_DATA]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_REFRESHING_DATA]];
    }
    return s;
}
-(NSString *) getError{
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_ERROR]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_ERROR]];
    }
    return s;
}
-(NSString *) getOK {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_OK]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_OK]];
    }
    return s;
}
-(NSString *) getCancel {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_CANCEL]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_CANCEL]];
    }
    return s;
}
-(NSString *) getYes {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_YES]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_YES]];
    }
    return s;
}
-(NSString *) getNo {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_NO]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_NO]];
    }
    return s;
}
-(NSString *) getLogout {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_BTN_LOGOUT]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_BTN_LOGOUT]];
    }
    return s;
}
-(NSString *) getBtnSyncNow {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_BTN_SYNC_NOW]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_BTN_SYNC_NOW]];
    }
    return s;
}
-(NSString *) getMsgSyncIncomplete {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_SYNC_INCOMPLETE]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_SYNC_INCOMPLETE]];
    }
    return s;
}
-(NSString *) getMsgCanNotSaveWhenEdit {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_CAN_NOT_SAVE_WHEN_EDIT]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_CAN_NOT_SAVE_WHEN_EDIT]];
    }
    return s;
}
-(NSString *) getRaLoadingData {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_RA_LOADING_DATA]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_RA_LOADING_DATA]];
    }
    return s;
}
-(NSString *) getNoWifi {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_NO_WIFI]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_NO_WIFI]];
    }
    return s;
}
-(NSString *) getMsgCanOnlyChoose3Picture {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_CAN_ONLY_CHOOSE_3_PICTURE]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_CAN_ONLY_CHOOSE_3_PICTURE]];
    }
    return s;
}
-(NSString *) getMsgDoNotHaveMoreItemToAdd {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_DO_NOT_HAVE_MORE_ITEM_TO_ADD]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_DO_NOT_HAVE_MORE_ITEM_TO_ADD]];
    }
    return s;
}
-(NSString *) getMsgAddItemSuccessfully {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_ADD_ITEM_SUCCESSFULLY]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_ADD_ITEM_SUCCESSFULLY]];
    }
    return s;
}
-(NSString *) getMsgUpdateItemSuccessfully {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_UPDATE_ITEM_SUCCESSFULLY]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_UPDATE_ITEM_SUCCESSFULLY]];
    }
    return s;
}
-(NSString *) getDone {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_DONE]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_DONE]];
    }
    return s;
}

-(NSString *)getComment {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_COMMENT]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_COMMENT]];
    }
    return s;
}

-(NSString *) getMsgSyncPeriodSetting {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_SYNC_PERIOD_SETTING]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_SYNC_PERIOD_SETTING]];
    }
    return s;
}
-(NSString *) getMsgPleaseChoose {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_PLEASE_CHOOSE]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_PLEASE_CHOOSE]];
    }
    return s;
}
-(NSString *) getLocations {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_EC_LOCATIONS]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_EC_LOCATIONS]];
    }
    return s;
}
-(NSString *) getTypeOfItem {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_EC_TYPE_OF_ITEM]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_EC_TYPE_OF_ITEM]];
    }
    return s;
}
-(NSString *) getTop10Item {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_EC_TOP_10_ITEM]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_EC_TOP_10_ITEM]];
    }
    return s;
}
-(NSString *) getQuantity {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_QUANTITY]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_QUANTITY]];
    }
    return s;
}
-(NSString *) getColour {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_LF_COLOUR]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_LF_COLOUR]];
    }
    return s;
}
-(NSString *) getCategories {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_LF_CATEGORIES]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_LF_CATEGORIES]];
    }
    return s;
}
-(NSString *)getTitleLogin {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_TITLE_LOGIN]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_TITLE_LOGIN]];
    }
    return s;
}
-(NSString *)getTitlePassword {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_TITLE_PASSWORD]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_TITLE_PASSWORD]];
    }
    return s;
}
-(NSString *)getBack {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_BACK]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_BACK]];
    }
    return s;
}
-(NSString *) getNoRoomAssigned {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_NO_ROOMASSIGNED]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_NO_ROOMASSIGNED]];
    }
    return s;
}
-(NSString *)getDialogTotalCharge {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_DIALOG_TOTAL_CHARGE]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_DIALOG_TOTAL_CHARGE]];
    }
    return s;
}
-(NSString *)getLanguage {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_LANGUAGE]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_LANGUAGE]];
    }
    return s;
}
-(NSString *) getBtnSetting {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_BTN_SETTING]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_BTN_SETTING]];
    }
    return s;
}
-(NSString *) getEdit {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_EDIT]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_EDIT]];
    }
    return s;
}
-(NSString *) getCount {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_COUNT]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_COUNT]];
    }
    return s;
}
-(NSString *) getLaundry {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_LAUNDRY]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_LAUNDRY]];
    }
    return s;
}
-(NSString *) getGuide {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_GUIDE]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_GUIDE]];
    }
    return s;
}
-(NSString *) getROOMASSIGNMENT {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_ASSIGNMENT]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_ASSIGNMENT]];
    }
    return s;
}
-(NSString *) getLOCATION {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_LOCATION]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_LOCATION]];
    }
    return s;
}
-(NSString *) getTOWER {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_TOWER]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_TOWER]];
    }
    return s;
}
-(NSString *) getFLOORNO {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_FLOOR_NO]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_FLOOR_NO]];
    }
    return s;
}
-(NSString *) getPROPERTY {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_PROPERTY]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_PROPERTY]];
    }
    return s;
}
-(NSString *) getBUILDING {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_BUILDING]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_BUILDING]];
    }
    return s;
}
-(NSString *) getCompletedRooms {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_BTN_VIEW_ASSIGNMENT]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_BTN_VIEW_ASSIGNMENT]];
    }
    return s;
}
-(NSString *) getHOME {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_BTN_HOME]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_BTN_HOME]];
    }
    return s;
}
-(NSString *) getRoom {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_ROOM_NO]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_ROOM_NO]];
    }
    return s;
}

-(NSString*)getRoomDetailTitle{
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_ROOM_NO_DETAIL]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_ROOM_NO_DETAIL]];
    }
    return s;
}
-(NSString *) getGuest {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_GUEST_NAME]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_GUEST_NAME]];
    }
    return s;
}
-(NSString *) getRMStatus {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_RM_STATUS]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_RM_STATUS]];
    }
    return s;
}
-(NSString *) getVIP {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_VIP]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_VIP]];
    }
    return s;
}
-(NSString *) getDuration {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_DURATION_USED]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_DURATION_USED]];
    }
    return s;
}
-(NSString *) getCleaningStatus {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_CLEANING_STATUS]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_CLEANING_STATUS]];
    }
    return s;
}
-(NSString *) getHousekeeperInfo {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_HOUSEKEEPER_INFO]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_HOUSEKEEPER_INFO]];
    }
    return s;
}
-(NSString *) getHousekeeper {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_HOUSEKEEPER_NAME]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_HOUSEKEEPER_NAME]];
    }
    return s;
}
-(NSString *) getTotalsRoomsCleaned {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_TOTAL_ROOM_CLEANED]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_TOTAL_ROOM_CLEANED]];
    }
    return s;
}
-(NSString *) getTotalTimeUsed {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_TOTAL_TIME_USED]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_TOTAL_TIME_USED]];
    }
    return s;
}
-(NSString *) getAverageCleaningTime {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_AVERAGE_TIME_USED]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_AVERAGE_TIME_USED]];
    }
    return s;
}
-(NSString *) getRoomDetail {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_ROOM_DETAIL]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_ROOM_DETAIL]];
    }
    return s;
}
-(NSString *) getGuestInfo {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_GUEST_INFO]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_GUEST_INFO]];
    }
    return s;
}
-(NSString *) getExpectedCleaningTime {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_EXPECTED_CLEANING_TIME]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_EXPECTED_CLEANING_TIME]];
    }
    return s;
}
-(NSString *) getGuestPreference {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_GUEST_REFERENCE]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_GUEST_REFERENCE]];
    }
    return s;
}
-(NSString *) getAdditionalJob {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_ADDITIONAL_JOB]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_ADDITIONAL_JOB]];
    }
    return s;
}
-(NSString *) getLOSTANDFOUND {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_LOST_FOUND]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_LOST_FOUND]];
    }
    return s;
}
-(NSString *) getENGINEERING {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_ENGINEERING_CASE]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_ENGINEERING_CASE]];
    }
    return s;
}
-(NSString *) getGUIDELINE {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_GUIDE_LINE]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_GUIDE_LINE]];
    }
    return s;
}
-(NSString *) getCOUNT {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_COUNT_UPPERCASE]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_COUNT_UPPERCASE]];
    }
    return s;
}
-(NSString *) getLAUNDRY {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_LAUNDRY_UPPERCASE]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_LAUNDRY_UPPERCASE]];
    }
    return s;
}
-(NSString *) getGUIDELINES {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_GUIDE_UPPERCASE]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_GUIDE_UPPERCASE]];
    }
    return s;
}
-(NSString *) getMESSAGELIST {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MESSAGE_LIST]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MESSAGE_LIST]];
    }
    return s;
}
-(NSString *) getMESSAGEDETAIL {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MESSAGE_DETAIL]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MESSAGE_DETAIL]];
    }
    return s;
}
-(NSString *) getNEWMESSAGE {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_NEW_MESSAGE]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_NEW_MESSAGE]];
    }
    return s;
}
-(NSString *) getMessageTopic {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MESSAGE_TOPIC]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MESSAGE_TOPIC]];
    }
    return s;
}
-(NSString *) getMessageTemplate {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MESSAGE_TEMPLATE]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MESSAGE_TEMPLATE]];
    }
    return s;
}
-(NSString *) getMessage {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MESSAGE]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MESSAGE]];
    }
    return s;
}
-(NSString *) getUser {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_USER]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_USER]];
    }
    return s;
}
-(NSString *) getVIPStatus {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_VIP_STATUS]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_VIP_STATUS]];
    }
    return s;
}
-(NSString *) getLanguagePreference {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_LANGUAGE_PREF]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_LANGUAGE_PREF]];
    }
    return s;
}
-(NSString *) getCheckIn {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_CHECK_IN]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_CHECK_IN]];
    }
    return s;
}
-(NSString *) getCheckOut {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_CHECK_OUT]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_CHECK_OUT]];
    }
    return s;
}
-(NSString *) getSelectAction {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_RA_SELECT_ACTION]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_RA_SELECT_ACTION]];
    }
    return s;
}
-(NSString *) getLoadingData {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_RA_LOADING_DATA]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_RA_LOADING_DATA]];
    }
    return s;
}
-(NSString *) getPleasewaitdot {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_RA_LOADING_DATA_CONTENT]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_RA_LOADING_DATA_CONTENT]];
    }
    return s;
}
-(NSString *) getCheckListRoom {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_CHECKLIST]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_CHECKLIST]];
    }
    return s;
}
-(NSString *) getEngineer{
    return @"Engineering";
}
-(NSString *) getBed {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_BED]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_BED]];
    }
    return s;
}
-(NSString *) getBathRoom {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_BATHROOM]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_BATHROOM]];
    }
    return s;
}
-(NSString *) getWritingDesk {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_WRITING_DESK]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_WRITING_DESK]];
    }
    return s;
}
-(NSString *) getLivingRoom {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_LIVING_ROOM]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_LIVING_ROOM]];
    }
    return s;
}
-(NSString *) getMirror {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MIRROR]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MIRROR]];
    }
    return s;
}
-(NSString *) getPass {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_PASS]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_PASS]];
    }
    return s;
}
-(NSString *) getFail {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_FAIL]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_FAIL]];
    }
    return s;
}
-(NSString *) getRating {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_RATING]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_RATING]];
    }
    return s;
}

-(NSString *) get7pt {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_TEXT_SIZE]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_TEXT_SIZE]];
    }
    return s;
}
-(NSString *) getMiniBar {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MINI_BAR]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MINI_BAR]];
    }
    return s;
}
-(NSString *) getCHARGEABLE {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_CHARGEABLE]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_CHARGEABLE]];
    }
    return s;
}
-(NSString *) getNONCHARGEABLE {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_NON_CHARGEABLE]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_NON_CHARGEABLE]];
    }
    return s;
}
-(NSString *) getBeverage {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_BEVERAGE]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_BEVERAGE]];
    }
    return s;
}
-(NSString *) getSnack {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_SNACK]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_SNACK]];
    }
    return s;
}
-(NSString *) getWater {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_WATER]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_WATER]];
    }
    return s;
}
-(NSString *) getItems {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_ITEM]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_ITEM]];
    }
    return s;
}
-(NSString *) getPrice {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_PRICE]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_PRICE]];
    }
    return s;
}
//-(NSString *) getMoveUp 
//-(NSString *) getMoveDown;
//-(NSString *) getViewDetails;
-(NSString *) getEngineeringCase {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_ENGINEERING_CASE_TITLE]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_ENGINEERING_CASE_TITLE]];
    }
    return s;
}
-(NSString *) getLocation {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_EC_LOCATIONS]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_EC_LOCATIONS]];
    }
    return s;
}
-(NSString *) getTypeofItem {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_EC_TYPE_OF_ITEM]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_EC_TYPE_OF_ITEM]];
    }
    return s;
}
-(NSString *) getTop10Items {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_EC_TOP_10_ITEM]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_EC_TOP_10_ITEM]];
    }
    return s;
}
-(NSString *) getRemark {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_EC_REMARK]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_EC_REMARK]];
    }
    return s;
}
-(NSString *) getATTACHPHOTO {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_EC_BTN_ATTACHPHOTO]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_EC_BTN_ATTACHPHOTO]];
    }
    return s;
}
-(NSString *) getDISPATCH {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_EC_BTN_DISPATCH]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_EC_BTN_DISPATCH]];
    }
    return s;
}
-(NSString *) getSelectLocation {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_EC_LOCATION_TITLE]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_EC_LOCATION_TITLE]];
    }
    return s;
}
-(NSString *) getSelectPhoto {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_EC_PHOTO_TITLE]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_EC_PHOTO_TITLE]];
    }
    return s;
}
-(NSString *) getLostandFountCase {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_LOSTANDFOUND_CASE_TITLE]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_LOSTANDFOUND_CASE_TITLE]];
    }
    return s;
}
-(NSString *) getOnlyavailableforSupervisor {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_NOTSUPERVISOR]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_NOTSUPERVISOR]];
    }
    return s;
}
-(NSString *) getIncorrectUsernameorPassworddot {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_INCORRECT_LOGIN]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_INCORRECT_LOGIN]];
    }
    return s;
}
-(NSString *) getLoadingRoomData {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_WAITING_ROOM_DETAIL]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_WAITING_ROOM_DETAIL]];
    }
    return s;
}
-(NSString *) getAreYouSure {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_LOGOUT]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_LOGOUT]];
    }
    return s;
}
-(NSString *) getSigningIndot {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_AUTHENTICATING]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_AUTHENTICATING]];
    }
    return s;
}
-(NSString *) getSavesuccessfully {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_SAVE_MESSAGE]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_SAVE_MESSAGE]];
    }
    return s;
}
-(NSString *) getSyncompleted {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_SYNC_MESSAGE]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_SYNC_MESSAGE]];
    }
    return s;
}
-(NSString *) getSyncingdot {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_SYNC_RUNNING_MESSAGE]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_SYNC_RUNNING_MESSAGE]];
    }
    return s;
}
-(NSString *) getUserlocked {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_USER_LOCKED]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_USER_LOCKED]];
    }
    return s;
}
-(NSString *) getPleaseinputvaliddate {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_INVALIDTIME]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_INVALIDTIME]];
    }
    return s;
}
-(NSString *) getName {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_DIALOG_NAME]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_DIALOG_NAME]];
    }
    return s;
}

-(NSString *) getNew {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_DIALOG_NEW]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_DIALOG_NEW]];
    }
    return s;
}
-(NSString *) getDeleteAll {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_DIALOG_DELETE_ALL]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_DIALOG_DELETE_ALL]];
    }
    return s;
}
-(NSString *) getAddNew {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_DIALOG_ADD_NEW]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_DIALOG_ADD_NEW]];
    }
    return s;
}
-(NSString *) getCollected {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_DIALOG_COLLECTED]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_DIALOG_COLLECTED]];
    }
    return s;
}
-(NSString *) getTotalCharge {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_DIALOG_TOTAL_CHARGE]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_DIALOG_TOTAL_CHARGE]];
    }
    return s;
}
-(NSString *) getFrom {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_FROM]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_FROM]];
    }
    return s;
}
-(NSString *) getAmenities {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_AMENITIES]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_AMENITIES]];
    }
    return s;
}
-(NSString *) getLinen {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_LINEN]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_LINEN]];
    }
    return s;
}
-(NSString *) getDelete {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_DELETE]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_DELETE]];
    }
    return s;
}
-(NSString *) getTakePhoto {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_TAKE_PHOTO]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_TAKE_PHOTO]];
    }
    return s;
}
-(NSString *) getSend {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_SEND]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_SEND]];
    }
    return s;
}
-(NSString *) getNonetworkconnection {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_NO_WIFI]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_NO_WIFI]];
    }
    return s;
}
-(NSString *) getNOROOMASSIGNED {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_NO_ROOMASSIGNED]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_NO_ROOMASSIGNED]];
    }
    return s;
}
-(NSString *) getNOMESSAGE {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_NO_MESSAGE]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_NO_MESSAGE]];
    }
    return s;
}
-(NSString *) getSavingData {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_RA_SAVING_DATA]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_RA_SAVING_DATA]];
    }
    return s;
}
-(NSString *) getSendingfailedpleasetryagain {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_SENDING_DATA_FAIL]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_SENDING_DATA_FAIL]];
    }
    return s;
}
-(NSString *) getSendingsuccessfully {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_SENDING_DATA_SUCCESFULLY]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_SENDING_DATA_SUCCESFULLY]];
    }
    return s;
}
-(NSString *) getRefreshingdatahascompleted {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_REFRESHING_DATA]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_REFRESHING_DATA]];
    }
    return s;
}
-(NSString *) getCHECKLIST {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_BTN_CHECK_LIST]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_BTN_CHECK_LIST]];
    }
    return s;
}
-(NSString *) getMESSAGE {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_BTN_MESSAGE]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_BTN_MESSAGE]];
    }
    return s;
}
-(NSString *) getSAVE {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_BTN_SAVE]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_BTN_SAVE]];
    }
    return s;
}
-(NSString *) getSYNCNOW {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_BTN_SYNC_NOW]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_BTN_SYNC_NOW]];
    }
    return s;
}
-(NSString *) getSETTING {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_BTN_SETTING]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_BTN_SETTING]];
    }
    return s;
}
-(NSString *) getLOGOUT {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_BTN_LOGOUT]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_BTN_LOGOUT]];
    }
    return s;
}
//-(NSString *) getLanguagepackloaded 
//-(NSString *) getDataerror 
//-(NSString *) getNodataloaded 
//-(NSString *) getOthererror 
//-(NSString *) getDND 
//-(NSString *) getSERVICELATER 
//-(NSString *) getDENIEDSERVICE 
//-(NSString *) getSTART 
//-(NSString *) getFINISH 
//-(NSString *) getPAUSE 
-(NSString *) getROOMCOMPLETED {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_ASSIGNMENT_COMPLETED]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_ASSIGNMENT_COMPLETED]];
    }
    return s;
}

-(NSString *) getAction {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_BTN_ACTION]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_BTN_ACTION]];
    }
    return s;
}
-(NSString *) getSearch {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_SEARCH]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_SEARCH]];
    }
    return s;
}

-(NSString *) getSyncincomplete {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_SYNC_INCOMPLETE]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_SYNC_INCOMPLETE]];
    }
    return s;
}
-(NSString *) getNewmessage {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_NEW_MESSAGE]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_NEW_MESSAGE]];
    }
    return s;
}
-(NSString *) getListuser {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_LIST_USER]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_LIST_USER]];
    }
    return s;
}
-(NSString *) getCantsavewhileediting {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_CAN_NOT_SAVE_WHEN_EDIT]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_CAN_NOT_SAVE_WHEN_EDIT]];
    }
    return s;
}
-(NSString *) getYoucanonlyattach3 {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_CAN_ONLY_CHOOSE_3_PICTURE]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_CAN_ONLY_CHOOSE_3_PICTURE]];
    }
    return s;
}
-(NSString *) getNomoreitem {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_DO_NOT_HAVE_MORE_ITEM_TO_ADD]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_DO_NOT_HAVE_MORE_ITEM_TO_ADD]];
    }
    return s;
}
-(NSString *) getAdditemsuccessful {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_ADD_ITEM_SUCCESSFULLY]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_ADD_ITEM_SUCCESSFULLY]];
    }
    return s;
}
-(NSString *) getUpdateitemsuccessful {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_UPDATE_ITEM_SUCCESSFULLY]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_UPDATE_ITEM_SUCCESSFULLY]];
    }
    return s;
}
-(NSString *) getfrom5to60 {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_SYNC_PERIOD_SETTING]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_SYNC_PERIOD_SETTING]];
    }
    return s;
}
-(NSString *) getPleasechoose {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_PLEASE_CHOOSE]];
    if (s == nil) {
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MSG_PLEASE_CHOOSE]];
    }
    return s;
}

-(NSString *)getUserID {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@",L_TITLE_USER_ID]];
    if(s == nil){
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@",
         ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_TITLE_USER_ID]];
    }
    return s;
}
-(NSString *)getCounts{
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@",L_COUNTS]];
    if(s == nil){
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@",
                                                                                  ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_COUNTS]];
    }
    return s;

}

-(NSString *) getLaundryRegular {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_LAUNDRY_REGULAR]];
    if(s == nil){
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@",
                                                                                  ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_LAUNDRY_REGULAR]];
    }
    return s;
}

-(NSString *) getLaundryExpress {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_LAUNDRY_EXPRESS]];
    if(s == nil){
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@",
                                                                                  ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_LAUNDRY_EXPRESS]];
    }
    return s;
}
-(NSString *) getRegular {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_REGULAR]];
    if(s == nil){
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@",
                                                                                  ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_REGULAR]];
    }
    return s;
}

-(NSString *) getExpress {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_EXPRESS]];
    if(s == nil){
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@",
                                                                                  ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_EXPRESS]];
    }
    return s;
}
-(NSString *) getMale {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@",L_MALE]];
    if(s == nil){
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@",
                                                                                  ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_MALE]];
    }
    return s;
}

-(NSString *) getFemale {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_FEMALE]];
    if(s == nil){
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@",
                                                                                  ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_FEMALE]];
    }
    return s;
}
-(NSString *) getInstruction {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_INSTRUCTION]];
    if(s == nil){
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@",
                                                                                  ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_INSTRUCTION]];
    }
    return s;
}

-(NSString *)getGrandTotal {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_GRAND_TOTAL]];
    if(s == nil){
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@",
                                                                                  ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_GRAND_TOTAL]];
    }
    return s;
}

-(NSString *)getAddToCart {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_ADD_TO_CART]];
    if(s == nil){
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@",
                                                                                  ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_ADD_TO_CART]];
    }
    return s;
}

-(NSString *)getUsed {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_USED]];
    if(s == nil){
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@",
                                                                                  ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_USED]];
    }
    return s;
}

-(NSString *)getCart {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@",L_CART]];
    if(s == nil){
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@",
                                                                                  ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_CART]];
    }
    return s;
}

-(NSString *)getSubtotal {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_SUBTOTAL]];
    if(s == nil){
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@",
                                                                                  ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_SUBTOTAL]];
    }
    return s;
}

-(NSString *)getPointsReceived{
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_POINTS_RECEIVED]];
    if(s == nil){
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@",
                                                                                  ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_POINTS_RECEIVED]];
    }
    return s;

}

-(NSString *)getPointsPossible{
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_POINTS_POSSIBLE]];
    if(s == nil){
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@",
                                                                                  ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_POINTS_POSSIBLE]];
    }
    return s;

}

-(NSString *)getPointsTotal{
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_POINTS_TOTAL]];
    if(s == nil){
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@",
                                                                                  ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_POINTS_TOTAL]];
    }
    return s;

}
   
-(NSString *)getRoomStandards{
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_ROOM_STANDARDS]];
    if(s == nil){
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@",
                                                                                  ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_ROOM_STANDARDS]];
    }
    return s;

}

-(NSString *)getChecklistStatus{
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_CHECKLIST_STATUS]];
    if(s == nil){
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@",
                                                                                  ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_CHECKLIST_STATUS]];
    }
    return s;

}

-(NSString *)getOverral{
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_OVERRAL]];
    if(s == nil){
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@",
                                                                                  ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_OVERRAL]];
    }
    return s;

}

-(NSString *)getUnassignedRoom{

    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_UNASSIGNED_ROOM]];
    if(s == nil){
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@",
                                                                                  ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_UNASSIGNED_ROOM]];
    }
    return s;
}

-(NSString *)getAssignedRoom{
    
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_ASSIGN_ROOM]];
    if(s == nil){
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@",
                                                                                  ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_ASSIGN_ROOM]];
    }
    return s;
}

#pragma mark - Find Function
-(NSString *) getAttendant {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_ATTENDANT]];
    
    if(s == nil){
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_ATTENDANT]];
    }
    
    return s;
}

-(NSString *) getRoomTitle {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_ROOM_TITLE]];
    
    if(s == nil){
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_ROOM_TITLE]];
    }
    
    return s;
}


-(NSString *) getNoResultsFound {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_NO_RESULT_FOUND]];
    
    if(s == nil){
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_NO_RESULT_FOUND]];
    }
    
    return s;
}

-(NSString *) getFind {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_FIND]];
    
    if(s == nil){
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_FIND]];
    }
    
    return s;
}

-(NSString *) getBy {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_BY]];
    
    if(s == nil){
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_BY]];
    }
    
    return s;
}

-(NSString *) getAll {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_ALL]];
    
    if(s == nil){
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_ALL]];
    }
    
    return s;
}

-(NSString *) getByRoomStatus {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_BY_ROOM_STATUS]];
    
    if(s == nil){
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_BY_ROOM_STATUS]];
    }
    
    return s;
}

-(NSString *) getCurrent {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_CURRENT]];
    
    if(s == nil){
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_CURRENT]];
    }
    
    return s;
}

-(NSString *) getTime {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_TIME]];
    
    if(s == nil){
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_TIME]];
    }
    
    return s;
}

-(NSString *) getALL {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_ALL_TITLE]];
    
    if(s == nil){
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_ALL_TITLE]];
    }
    
    return s;
}

-(NSString *) getNoRoom {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_NO_ROOM]];
    
    if(s == nil){
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_NO_ROOM]];
    }
    
    return s;
}

-(NSString *) getFailInspection {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_FAIL_INSPECTION]];
    
    if(s == nil){
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_FAIL_INSPECTION]];
    }
    
    return s;
}

-(NSString *) getCompletedRoom {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_COMPLETED_ROOM]];
    
    if(s == nil){
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_COMPLETED_ROOM]];
    }
    
    return s;
}

-(NSString *) getPassInspection {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_PASS_INSPECTION]];
    
    if(s == nil){
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_PASS_INSPECTION]];
    }
    
    return s;
}

-(NSString *) getPendingRoom {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_PENDING_ROOM]];
    
    if(s == nil){
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_PENDING_ROOM]];
    }
    
    return s;
}

-(NSString *) getAheadSchedule {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_AHEAD_SCHEDULE]];
    
    if(s == nil){
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_AHEAD_SCHEDULE]];
    }
    
    return s;
}

-(NSString *) getRoomPending {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_ROOMS_PENDING]];
    
    if(s == nil){
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_ROOMS_PENDING]];
    }
    
    return s;
}

-(NSString *) getRoomComplete {
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_ROOMS_COMPLETED]];
    
    if(s == nil){
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_ROOMS_COMPLETED]];
    }
    
    return s;
}


-(NSString *) getMsgAlertSaveRoomDetail
{
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_ALERT_SAVEROOMDETAIL]];
    
    if(s == nil){
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_ALERT_SAVEROOMDETAIL]];
    }
    
    return s;    
}


-(NSString *) getMsgAlertSaveCheckList
{
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_ALERT_SAVECHECKLIST]];
    
    if(s == nil){
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_ALERT_SAVECHECKLIST]];
    }
    
    return s;

}

-(NSString *) getMsgAlertSaveCheckListDetail
{
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_ALERT_SAVECHECKLISTDETAIL]];
    
    if(s == nil){
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_ALERT_SAVECHECKLISTDETAIL]];
    }
    
    return s;

}

-(NSString *) getMsgAlertErrorCheckListDetail
{
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_ALERT_ERRORCHECKLISTDETAIL]];
    
    if(s == nil){
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_ALERT_ERRORCHECKLISTDETAIL]];
    }
    
    return s;

}

-(NSString *) getMsgAlertSaveCounts
{
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_ALERT_SAVECOUNTS]];
    
    if(s == nil){
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_ALERT_SAVECOUNTS]];
    }
    
    return s;

}
-(NSString *) getMsgAlertSaveAsign
{
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_ALERT_SAVECOUNTS]];
    
    if(s == nil){
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_ALERT_SAVECOUNTS]];
    }
    
    return s;
    
}

-(NSString *) getMsgAlertDiscardCounts
{
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_ALERT_DISCARDSAVECOUNTS]];
    
    if(s == nil){
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", L_ALERT_DISCARDSAVECOUNTS]];
    }
    
    return s;

}
#pragma Mark-Laundry View
-(NSString *) getAlertTitle
{
    NSDictionary *dic = [sharedLanguageManagerInstance getCurrentDictionaryLanguage];
    
    NSString *s = [dic valueForKey:[NSString stringWithFormat:@"%@", LF_MSG_ALERT]];
    
    if(s == nil){
        NSDictionary *dic = [sharedLanguageManagerInstance getDictionaryLanguage:[NSString stringWithFormat:@"%@", ENGLISH_LANGUAGE]];
        s = [dic valueForKey:[NSString stringWithFormat:@"%@", LF_MSG_ALERT]];
    }
    
    return s;
    
}
@end
