//
//  LanguageModel.h
//  eHouseKeeping
//
//  Created by TMS2 on 6/29/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LanguageAdapter.h"

@class LanguageAdapter;
@interface LanguageModel : NSObject {
    NSString    *lang_id;
    NSString    *lang_name;
    NSString    *lang_pack;
    NSString    *lang_active;
    NSString    *lang_lastmodified;
    NSString    *lang_currencysymbol;
    NSString    *lang_currencynodigitafterdecimal;
    //LanguageAdapter *adapter;
}

@property (nonatomic, retain) NSString  *lang_id;
@property (nonatomic, retain) NSString  *lang_name;
@property (nonatomic, retain) NSString  *lang_pack;
@property (nonatomic, retain) NSString  *lang_active;
@property (nonatomic, retain) NSString  *lang_lastmodified;
@property (nonatomic, retain) NSString  *lang_currencysymbol;
@property (nonatomic, retain) NSString  *lang_currencynodigitafterdecimal;


-(id)initWithLangId:(NSString*)langId langName:(NSString*)langName langPack:(NSString*)langPack langActive:(NSString*)langActive;


@end
