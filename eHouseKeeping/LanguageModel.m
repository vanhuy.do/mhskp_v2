//
//  LanguageModel.m
//  eHouseKeeping
//
//  Created by TMS2 on 6/29/11.
//  Copyright __MyCompanyName__. All rights reserved.
//

#import "LanguageModel.h"


@implementation LanguageModel
@synthesize lang_id;
@synthesize lang_name;
@synthesize lang_pack;
@synthesize lang_active;
@synthesize lang_lastmodified;
@synthesize lang_currencysymbol;
@synthesize lang_currencynodigitafterdecimal;

/*
-(id)init{
    //adapter = [[LanguageAdapter alloc] init];
    //[adapter openDatabase];
    [super init];
    return self;
}
*/
-(id)initWithLangId:(NSString*)langId langName:(NSString *)langName langPack:(NSString *)langPack langActive:(NSString *)langActive
{
    [super init];
    self.lang_id = langId;
    self.lang_name  = langName;
    self.lang_pack = langPack;
    self.lang_active = langActive;
    //adapter = [[LanguageAdapter alloc] init];
    //[adapter openDatabase];
    
    return self;
    
}



-(void)dealloc
{
    [lang_id release];
    [lang_name release];
    [lang_pack release];
    [lang_active release];
    [lang_lastmodified release];
    [lang_currencynodigitafterdecimal release];
    [lang_currencysymbol release];
    //[adapter release];
    [super dealloc];
}
@end
