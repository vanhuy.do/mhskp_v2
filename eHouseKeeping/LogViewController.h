//
//  LogViewController.h
//  mHouseKeeping
//
//  Created by Khanh Phan on 11/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LogViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>
{
    IBOutlet UILabel *label;
    IBOutlet UITableView *logTable;
}

@property(nonatomic, strong) NSArray *logData;
@property(nonatomic, strong) NSString *labelStr;

@end
