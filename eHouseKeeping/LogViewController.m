//
//  LogViewController.m
//  mHouseKeeping
//
//  Created by Khanh Phan on 11/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LogViewController.h"
#import "iToast.h"
#import "MyNavigationBarV2.h"
#import "DeviceManager.h"

@interface LogViewController ()

@end

@implementation LogViewController

@synthesize logData;
@synthesize labelStr;

-(void)btnBackPressed
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Resize content views of current VC to fit with Navigation bar and tabbar
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    // Do any additional setup after loading the view from its nib.
    [logTable setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg.png"]]];
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //back button navigation
    self.navigationItem.leftBarButtonItem = [MyNavigationBarV2 createBackButtonWithTarget:self selector:@selector(btnBackPressed) forControlEvents:UIControlEventTouchUpInside];
    
    [label setText:labelStr];
    CGRect frame = [logTable frame];
    CGFloat hieght = 60*[logData count]>350?350:60*[logData count];
    if([DeviceManager getDeviceScreenKind] == DeviceScreenKindRetina4_0){
        hieght += 81;
    }
    frame.size.height = hieght;
    [logTable setFrame:frame];
    [logTable reloadData];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    label = nil;
    logTable = nil;
    logData = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(CGFloat) minHeightForText:(NSString *) context
{
    return [context sizeWithFont:[UIFont systemFontOfSize:13] constrainedToSize:CGSizeMake(318,999999) lineBreakMode:NSLineBreakByWordWrapping].height;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self minHeightForText:[logData objectAtIndex:indexPath.row]] + 2;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [logData count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    const CGFloat LABEL_HEIGHT = [self minHeightForText:[logData objectAtIndex:indexPath.row]];
    const NSInteger LABEL_TAG = 1004;
    
    UILabel *lbl;
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        lbl = [[UILabel alloc]initWithFrame:CGRectMake(1,1,318,LABEL_HEIGHT)];
        lbl.lineBreakMode = NSLineBreakByWordWrapping;
        lbl.font = [UIFont systemFontOfSize:13];
        [cell.contentView addSubview:lbl];
		lbl.tag = LABEL_TAG;
    }
    else {
        lbl = (UILabel *)[cell viewWithTag:LABEL_TAG];
        lbl.frame = CGRectMake(1, 1, 318, LABEL_HEIGHT);
    }
    int line = LABEL_HEIGHT/13;
    [lbl setNumberOfLines:line];
    lbl.text = [logData objectAtIndex:indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath 
{
    NSString *logCellValue = [logData objectAtIndex:indexPath.row];
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    [pasteboard setString:logCellValue];
    
    [[[[iToast makeText:@"Copy to clipboard sucess"]
       setGravity:iToastGravityBottom] setDuration:iToastDurationShort] show];
}

@end
