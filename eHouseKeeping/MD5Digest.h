//
//  MD5Digest.h
//  eHouseKeeping
//
//  Created by Chinh X.Bui on 8/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface MD5Digest : NSObject {
    
}
+ (NSString*)md5HexDigest:(NSString*)input;
@end

