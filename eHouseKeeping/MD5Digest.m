//
//  MD5Digest.m
//  eHouseKeeping
//
//  Created by Chinh X.Bui on 8/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "MD5Digest.h"
#include <CommonCrypto/CommonDigest.h>

@implementation MD5Digest

+ (NSString*)md5HexDigest:(NSString*)input {
    const char* str = [input UTF8String];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5(str, strlen(str), result);
    
    NSMutableString *ret = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH*2];
    for(int i = 0; i<CC_MD5_DIGEST_LENGTH; i++) {
        [ret appendFormat:@"%02x",result[i]];
    }
    return ret;
}

@end

