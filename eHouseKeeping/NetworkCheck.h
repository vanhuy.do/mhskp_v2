//
//  NetworkCheck.h
//  me360
//
//  Created by Thuong Tran on 9/10/10.
//  Copyright 2010 TMS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sys/socket.h>
#import <netinet/in.h>
#import <arpa/inet.h>
#import <netdb.h>
#include <SystemConfiguration/SCNetworkReachability.h>
//#import "AFNetworkReachabilityManager.h"

@interface NetworkCheck : NSObject {
//    AFNetworkReachabilityStatus currentStatus;
}

+ (NetworkCheck*) sharedNetworkCheck;
-(BOOL) isConnectNetworkSuccessful;
-(BOOL) isConnectNetworkSuccessfulNoPopUp;
-(BOOL) hasLocation;
- (BOOL)checkInternetConnection;
@end
