//
//  NetworkCheck.m
//  me360
//
//  Created by Thuong Tran on 9/10/10.
//  Copyright 2010 TMS. All rights reserved.
//

#import "NetworkCheck.h"
#import "Reachability1.h"

@implementation NetworkCheck

static NetworkCheck* sharedNetworkCheckInstance = nil;

+ (NetworkCheck*) sharedNetworkCheck;
{
	if (sharedNetworkCheckInstance == nil) {
        sharedNetworkCheckInstance = [[super allocWithZone:NULL] init];
    }
    return sharedNetworkCheckInstance;	
}

+ (id)allocWithZone:(NSZone *)zone
{
    return [[self sharedNetworkCheck] retain];
}

- (id)copyWithZone:(NSZone *)zone
{
    return self;
}

- (id)retain
{
    return self;
}
//- (void)initNotify{
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleNetworkChange:) name:kReachabilityChangedNotification object:nil];
//    
//    reachability = [Reachability reachabilityForInternetConnection];
//    [reachability startNotifier];
//    
//    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
//    
//    if(remoteHostStatus == NotReachable) {
//        NSLog(@"no");
//    }
//    else if (remoteHostStatus == ReachableViaWiFi) {
//        NSLog(@"wifi");
//    }
//    else if (remoteHostStatus == ReachableViaWWAN) {
//        NSLog(@"cell");
//    }
//    
//}
//- (void) handleNetworkChange:(NSNotification *)notice
//{
//    reachability = [Reachability reachabilityForInternetConnection];
//    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
//    
//    if(remoteHostStatus == NotReachable) {
//        NSLog(@"no");
//    }
//    else if (remoteHostStatus == ReachableViaWiFi) {
//        NSLog(@"wifi");
//    }
//    else if (remoteHostStatus == ReachableViaWWAN) {
//        NSLog(@"cell");
//    }
//}
- (NSUInteger)retainCount
{
    return NSUIntegerMax;  //denotes an object that cannot be released
}

- (void)release
{
    //do nothing
}

- (id)autorelease
{
    return self;
}

// Check connect to network
// Return true if have, otherwise false
- (BOOL) connectedToNetwork
{
	// Check bug 
	//[[BugLogging sharedBugLogging] saveLog:[NSString stringWithFormat:@"%@ %@",NSStringFromClass([self class]), NSStringFromSelector(_cmd)]];
	// Create zero addy
	struct sockaddr_in zeroAddress;
	bzero(&zeroAddress, sizeof(zeroAddress));
	zeroAddress.sin_len = sizeof(zeroAddress);
	zeroAddress.sin_family = AF_INET;
	
	// Recover reachability flags
	SCNetworkReachabilityRef defaultRouteReachability = SCNetworkReachabilityCreateWithAddress(NULL, (struct sockaddr *)&zeroAddress);
	SCNetworkReachabilityFlags flags;
	
	BOOL didRetrieveFlags = SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags);
	CFRelease(defaultRouteReachability);
	
	if (!didRetrieveFlags)
	{
		printf("Error. Could not recover network reachability flags\n");
		return NO;
	}
	
	BOOL isReachable = ((flags & kSCNetworkFlagsReachable) != 0);
	BOOL needsConnection = ((flags & kSCNetworkFlagsConnectionRequired) != 0);
	return (isReachable && !needsConnection) ? YES : NO;
}


-(BOOL) isConnectNetworkSuccessful
{
	if (! [self connectedToNetwork]) {
		UIAlertView * alert = [[UIAlertView alloc] initWithTitle:[L_internet_exception currentKeyToLanguage] message:nil delegate:self cancelButtonTitle:[L_OK currentKeyToLanguage] otherButtonTitles:nil];
		[alert show];
		[alert release];
		return FALSE;
	}
	return TRUE;
}
- (BOOL)checkInternetConnection{
    
    BOOL result = NO;
    BOOL enable3G =  ([[NSUserDefaults standardUserDefaults] integerForKey:ENABLE_3G]  == 1);
    BOOL enableWIFI = [[NSUserDefaults standardUserDefaults] boolForKey:ENABLE_WIFI];
    Reachability1 *reachability = [Reachability1 reachabilityForInternetConnection];
    [reachability startNotifier];
    
    NetworkStatus status = [reachability currentReachabilityStatus];

    if (status == ReachableViaWiFi && enableWIFI)
    {
        //WiFi
//        result = enable3G == 1 ? NO : YES;
        result = YES;
    }
    else if (status == ReachableViaWWAN && enable3G)
    {
        //3G
//        result = enable3G == 1 ? YES : NO;

        result = YES;
    }
    return result;

}
-(BOOL) isConnectNetworkSuccessfulNoPopUp
{
    if(!isDemoMode)
    {
        if ([self connectedToNetwork] && [self checkInternetConnection]) {
            return TRUE;
        }
        else{
            return FALSE;
        }
    } else {
        return FALSE;
    }
}

-(BOOL)hasLocation{
    return NO;
}


@end
