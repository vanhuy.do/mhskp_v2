//
//  eHouseKeepingAppDelegate.h
//  eHouseKeeping
//
//  Created by tms on 5/6/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Crittercism/Crittercism.h>
//#import "Crittercism.h"
#import "CustomTabBar.h"
#import "NetworkCheck.h"
#import "GCDAsyncSocket.h"

#if DEFAULT_ENABLE_PTT
#import <PTTLibrary/HomeViewController.h>
#endif
@class Reachability;
@interface eHouseKeepingAppDelegate : NSObject <UIApplicationDelegate, UITabBarControllerDelegate, UINavigationControllerDelegate,CrittercismDelegate> {
    
    Reachability* hostReach;
    Reachability* internetReach;
    Reachability* wifiReach;
    
    GCDAsyncSocket *asyncSocket;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;

@property (nonatomic, retain) IBOutlet UITabBarController *tabBarController;
@property (nonatomic, retain) IBOutlet CustomTabBar *bottomTabbarMenu;
#if DEFAULT_ENABLE_PTT
@property (nonatomic, retain) HomeViewController *pttController;
#endif
- (void)createEditableCopyOfDatabaseIfNeeded ;
- (void)copyLanguagePackToEditableIfNeeded;
- (void)resetLocalDatabase;
- (void)resetDemoDatabase;
- (void)resetAllLanguages;
- (void)sendEmail:(int)day;
@end
