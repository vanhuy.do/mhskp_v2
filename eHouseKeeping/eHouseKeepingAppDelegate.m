//
//  eHouseKeepingAppDelegate.m
//  eHouseKeeping
//
//  Created by tms on 5/6/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "eHouseKeepingAppDelegate.h"
#import "DbDefinesV2.h"
#import "sqlite3.h"
#import "CheckMemory.h"
#import "ehkDefines.h"
#import "Reachability.h"
#import "SettingAdapterV2.h"
#import "SettingModelV2.h"
#import "RoomAssignmentInfoViewController.h"
#import <Crittercism/Crittercism.h>
#import "LogFileManager.h"
#import "CustomTabBar.h"
#import "AdditionalJobDetailsScreenV2.h"
#import "DeviceManager.h"
#import "PanicManager.h"
#import "NSFileManager+DoNotBackup.h"
#import "CleaningTimeManager.h"
#import "OtherActivityViewController.h"
#import <CoreTelephony/CTCallCenter.h>
#import <CoreTelephony/CTCall.h>
#import "MLog.h"

#if DEFAULT_ENABLE_PTT
#import <PTTLibrary/LanguageManager.h>
#import <PTTLibrary/UIAlertView+Blocks.h>
#endif
#pragma mark - PTT REMOVE END
#define tabBarImage @"tabBar.png"
#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

@implementation eHouseKeepingAppDelegate

@synthesize window=_window;

@synthesize tabBarController=_tabBarController;
@synthesize bottomTabbarMenu;

#pragma mark - network status notification - Tan Nguyen implement

- (void) updateReachabilityFlag: (Reachability*) curReach
{
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    [[NSUserDefaults standardUserDefaults] setInteger:(NSInteger)netStatus forKey:@"networkStatus"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
	Reachability* curReach = [note object];
	NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
	[self updateReachabilityFlag: curReach];
//    [[NSNotificationCenter defaultCenter] postNotificationName: kChangedWifiIconNotification object: note];
}

#pragma mark - catch app crash

void exceptionHandler(NSException *exception)
{
    NSString *localDate = [NSDateFormatter localizedStringFromDate:[NSDate date] dateStyle:NSDateFormatterMediumStyle timeStyle:NSDateFormatterMediumStyle];
    NSArray *backtrace = [exception callStackSymbols];
    NSString *version = [[UIDevice currentDevice] systemVersion];
    NSString *message = [NSString stringWithFormat:@"Date:%@, Device: %@, Backtrace:\n%@", localDate, version, backtrace];
    LogObject *logObj = [[LogObject alloc] init];
    logObj.dateTime = [NSDate date];
    NSString *logCrashFormat = @"<AppCrash>\
                                <OSVersion = '%@' />\
                                <StackTrace>\
                                %@\
                                </StackTrace>\
                                </AppCrash>";
    logObj.log = [NSString stringWithFormat:logCrashFormat, version, backtrace];
    LogFileManager *logManager = [[LogFileManager alloc] init];
    [logManager save:logObj];
    
//    [[NSUserDefaults standardUserDefaults] setValue:message forKey:STACK_TRACE];
    if([LogFileManager isLogConsole])
    {
        FCSLog(@"Stack trace, %@", message);
    }
}

//detect crash on relaunch app
- (void)crittercismDidCrashOnLastLoad
{
//    NSString *stackTrace = [[NSUserDefaults standardUserDefaults] objectForKey:STACK_TRACE];
//    
//    //release value stack trace
//    [[NSUserDefaults standardUserDefaults] setValue:nil forKey:STACK_TRACE];
}

#pragma mark - app delegate
- (void)initLogFile{
    [MLog setLogOn:YES];
    NSDateFormatter *dateFmt = [[NSDateFormatter alloc] init];
    [dateFmt setDateFormat:@"dd"];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *fileName =[NSString stringWithFormat:@"WS_%@.log", [dateFmt stringFromDate:[NSDate date]]];
    NSString *logFilePath = [documentsDirectory stringByAppendingPathComponent:fileName];
    freopen([logFilePath cStringUsingEncoding:NSASCIIStringEncoding],"a+",stderr);
    [dateFmt release];
    
}
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    [self.window.rootViewController dismissViewControllerAnimated:YES completion:nil];
}
- (void)sendEmail:(int)day{
    // Check that a mail account is available
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *emailController = [[MFMailComposeViewController alloc] init];
        emailController.mailComposeDelegate = self;
        
        // Build the subject
        NSDate *today = [NSDate date];
        NSDateFormatter *dateFmt = [[NSDateFormatter alloc] init];
        [dateFmt setDateFormat:@"dd-MM-yyyy HH:mm:ss"];
        NSString *strSubject = [NSString stringWithFormat:@"%@ [%@]",
                                [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleName"],
                                [dateFmt stringFromDate:today]];
        [dateFmt release];
        
        // Build the body
        NSString *strBody = [NSString stringWithFormat:
                             @"%@:\n%@\n\n"
                             @"%@:\n%@\n\n"
                             "%@:\n%@\n\n"
                             "%@:\n%@\n\n"
                             "%@:\n%d\n\n",
                             @"App Version: ", [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"],
                             @"Build Version: ", [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"],
                             @"OS Version: ", [[UIDevice currentDevice] systemVersion],
                             @"Resolution: ", [NSString stringWithFormat:@"%d x %d", (int)[[UIScreen mainScreen] bounds].size.width, (int)[[UIScreen mainScreen] bounds].size.height],
                             @"UserID: ", [UserManagerV2 sharedUserManager].currentUser.userId];
        
        // Build the recipient
        NSArray *arrRecipient = @[EMAIL_SUPPORT];
        FCSLog(@"%@", arrRecipient);
        
        // Get the attachment path
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSArray *directoryContent = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsDirectory error:NULL];
        FCSLog(@"%@", directoryContent);
        
        [emailController setSubject:strSubject];
        [emailController setMessageBody:strBody isHTML:NO];
        [emailController setToRecipients:arrRecipient];
        NSString *zipFilePath = [self getLinkLogFile:day];
        if (zipFilePath && zipFilePath.length > 0) {
            int fileDescription = open([zipFilePath fileSystemRepresentation], O_RDONLY);
            if (fileDescription > 0) {
                NSFileHandle *fileHandle = [[[NSFileHandle alloc] initWithFileDescriptor:fileDescription] autorelease];
                NSData *recData = [fileHandle readDataToEndOfFile];
                NSString *mime = [self giveMimeForPath:zipFilePath];
                [emailController addAttachmentData:recData mimeType:mime fileName:@"AppLog.log"];
            }
            
            [self.window.rootViewController presentViewController:emailController animated:YES completion:nil];
            
            [emailController release];
        }else{
            UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:nil message:@"Please select another day" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alertView show];
            [alertView release];
        }
        
    }
    else
    {
        UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:nil message:@"Please setup Email" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alertView show];
        [alertView release];
    }
}
-(NSString *) giveMimeForPath:(NSString *)filePath
{
    NSURL* fileUrl = [NSURL fileURLWithPath:filePath];
    NSURLRequest* fileUrlRequest = [[NSURLRequest alloc] initWithURL:fileUrl cachePolicy:NSURLCacheStorageNotAllowed timeoutInterval:.1];
    NSURLResponse* response = nil;
    
    [NSURLConnection sendSynchronousRequest:fileUrlRequest returningResponse:&response  error:nil];
    NSString* mimeType = [response MIMEType];
    FCSLog(@"MIME: %@", mimeType);
    [fileUrlRequest release];
    
    return mimeType;
}
- (NSString*)getLinkLogFile:(int)day{
    NSString *retVal = @"";
    // Get the attachment path
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSArray *directoryContent = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsDirectory error:NULL];
    NSString *fileName = [NSString stringWithFormat:@"WS_%d", day];
    // Add files to zip
    for (NSString *strLog in directoryContent)
    {
        if ([strLog rangeOfString:fileName options:NSCaseInsensitiveSearch].location != NSNotFound)
        {
            NSString *fileToCompress = [documentsDirectory stringByAppendingPathComponent:strLog];
            retVal = fileToCompress;
        }
    }
    return retVal;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [self initLogFile];
    if ([[NSUserDefaults standardUserDefaults] objectForKey:ENABLE_3G] == nil) {
        [[NSUserDefaults standardUserDefaults] setInteger:DEFAULT_ENABLE_3G forKey:ENABLE_3G];
    }
    if ([[NSUserDefaults standardUserDefaults] objectForKey:ENABLE_WIFI] == nil) {
        [[NSUserDefaults standardUserDefaults] setBool:[NSNumber numberWithBool:DEFAULT_ENABLE_WIFI] forKey:ENABLE_WIFI];
    }
    [NetworkCheck sharedNetworkCheck];
    
    //crash report
//    [Crittercism enableWithAppID: @"516b6f2f558d6a5e97000005"];
    
    //Register catch exception handler (Catch app crash)
    NSSetUncaughtExceptionHandler (&exceptionHandler);
    
    [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
    [application registerForRemoteNotifications];
    
    FCSLog(@"identifierForVendor = %@",[DeviceManager identifierForVendor]);
    
    [LogFileManager setLogConsole:NO];
#ifdef DEBUG
    NSString* libraryPath = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    FCSLog(@"Library Path: %@", libraryPath);
    [LogFileManager setLogConsole:YES];
#endif
    
    //Hao Tran[20130520/Check change build] - Check to clear all old data in previous build
    NSString *currentVersion = [self getCurrentVersion];
    NSString *previousVersion = [self getPreviousVersion];
    if([currentVersion caseInsensitiveCompare:previousVersion] != NSOrderedSame)
    {
        [self ResetDataBaseAndLanguages];
        [[NSUserDefaults standardUserDefaults] setValue:currentVersion  forKey:APP_VERSION];
        [[NSUserDefaults standardUserDefaults] setValue:nil forKey:KEY_WEBSERVICE_URL];
    }
    else
    {
        //copy database to writable location
        [self createEditableCopyOfDatabaseIfNeeded];
        //copy eng language pack to writable location if needed
        [self copyLanguagePackToEditableIfNeeded];
    }
    //Hao Tran[20130520/Check change build] - END
    
//    [application setStatusBarStyle:UIStatusBarStyleBlackOpaque];

    //Fixed App Crash
    //De-active Demo Mode for first time load app
    //Demo Mode will automatic load when login with demo user
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:ENABLE_DEMO_MODE];
    
    SettingAdapterV2 *stAdapter=[[SettingAdapterV2 alloc] init];
    [stAdapter openDatabase];
    [stAdapter resetSqlCommand];
    SettingModelV2 *saveSettingModel = [[SettingModelV2 alloc] init];
    [stAdapter loadSettingModel:saveSettingModel];
    
    // CFG [20160927/CRF-00001432] - Set default values for background mode.
    [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithInt:0] forKey:KEY_BACKGROUND_MODE];
    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:KEY_HEARTBEAT_SERVER];
    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:KEY_HEARTBEAT_PORT];
    // CFG [20160927/CRF-00001432] - End.
    
    if (saveSettingModel.row_Id)
    {
        [[NSUserDefaults standardUserDefaults] setObject:saveSettingModel.settingsWSURL forKey:KEY_WEBSERVICE_URL];
        [[NSUserDefaults standardUserDefaults] synchronize];
        if (saveSettingModel.settingEnableLog) {
            [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithInt:1] forKey:ENABLE_LOG];
        }
        else {
            [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithInt:0] forKey:ENABLE_LOG];
        }
        
        // CFG [20160927/CRF-00001432] - Set default values for background mode.
        if (saveSettingModel.settingsBackgroundMode) {
            [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithInt:1] forKey:KEY_BACKGROUND_MODE];
        }
        else {
            [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithInt:0] forKey:KEY_BACKGROUND_MODE];
        }
        [[NSUserDefaults standardUserDefaults] setObject:saveSettingModel.settingsHeartbeatServer forKey:KEY_HEARTBEAT_SERVER];
        [[NSUserDefaults standardUserDefaults] setObject:saveSettingModel.settingsHeartbeatPort forKey:KEY_HEARTBEAT_PORT];
        // CFG [20160927/CRF-00001432] - End.
    }
    else {
        [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithInt:1] forKey:ENABLE_LOG];
    }
    
//    [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithInt:1] forKey:IS_FOLLOW_ROOM_ASSIGN_ORDER];
    if ([[NSUserDefaults standardUserDefaults] valueForKey:LAST_LOG_FILE_POSITION] == nil) {
        NSDate *now = [NSDate date];
        [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithInt:0] forKey:LAST_LOG_FILE_POSITION];
        [[NSUserDefaults standardUserDefaults] setValue:now forKey:LAST_LOGGING_DATE];
    }
    
    //Hao Tran - Remove tabbar for PRD 2.2 Release 2
    /*
    CGRect frame = CGRectMake(0.0, 0.0, self.tabBarController.view.bounds.size.width, HEIGHT_TAB_BAR);
    UIImageView *img = [[UIImageView alloc] initWithImage:[UIImage imageNamed:tabBarImage]];
    img.frame = frame;
    [[self.tabBarController tabBar] insertSubview:img atIndex:0];
    [img release];
    */
    
    //[[self.tabBarController tabBar] removeFromSuperview];
    //Hao Tran - End Remove Tabbar for PRD 2.2 Release 2
    
    // Override point for customization after application launch.
    // Add the tab bar controller's current view as a subview of the window
    
    /*********** Tan Nguyen add for handle network connective ***********/
    // Observe the kNetworkReachabilityChangedNotification. When that notification is posted, the
    // method "reachabilityChanged" will be called. 
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(reachabilityChanged:) name: kReachabilityChangedNotification object: nil];
    
    //Change the host name here to change the server your monitoring
	hostReach = [[Reachability reachabilityWithHostName: @"www.apple.com"] retain];
	[hostReach startNotifier];
	[self updateReachabilityFlag: hostReach];
	
    internetReach = [[Reachability reachabilityForInternetConnection] retain];
	[internetReach startNotifier];
	[self updateReachabilityFlag: internetReach];
    
    wifiReach = [[Reachability reachabilityForLocalWiFi] retain];
	[wifiReach startNotifier];
	[self updateReachabilityFlag: wifiReach];
    
    /**************** Tan Nguyen End Add *********************/
    [self.tabBarController.tabBar setHidden:YES];
    self.window.rootViewController = self.tabBarController;
    [self.window makeKeyAndVisible];
    if SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")
    // Works on >= version 8.0
        [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeBadge|UIUserNotificationTypeSound|UIUserNotificationTypeAlert categories:nil]];
    else
    // Works on < version 8.0
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeAlert
                                                                           |UIRemoteNotificationTypeBadge
                                                                           |UIRemoteNotificationTypeSound)];
//    if([self IsIOS8AndAbove])
//    {
//        
//    }
//    else
//    {
//       
//    }
    
    // CFG [20160926/CRF-00001432] - Enable pooling from the server to stay alive.
    if ([[NSUserDefaults standardUserDefaults] integerForKey:KEY_BACKGROUND_MODE]) {
        dispatch_queue_t mainQueue = dispatch_get_main_queue();
        asyncSocket = [[GCDAsyncSocket alloc] initWithDelegate:self delegateQueue:mainQueue];
        
        NSError *error = nil;
        
        NSString *sHeartbeatServer = [[NSUserDefaults standardUserDefaults] stringForKey:KEY_HEARTBEAT_SERVER];
        int nHeartbeatPort = (int) [[NSUserDefaults standardUserDefaults] stringForKey:KEY_HEARTBEAT_PORT].integerValue;
        
        if (![asyncSocket connectToHost:sHeartbeatServer onPort:nHeartbeatPort error:&error]) {
            FCSLog(@"eHouseKeepingAppDelegate.m>application:didFinishLaunchingWithOptions>Error connecting: %@", error);
        }
    }
    // CFG [20160926/CRF-00001432] - End.
    
    return YES;
}
- (BOOL)IsIOS8AndAbove
{
    NSString *version = [[UIDevice currentDevice] systemVersion];
    return [version floatValue] >= 8.0;
}
- (void)applicationWillResignActive:(UIApplication *)application
{
    //[application setStatusBarStyle:UIStatusBarStyleBlackOpaque];
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    if([LogFileManager isLogConsole])
    {
        FCSLog(@"applicationDidEnterBackground");
    }
    if([PanicManager isRunningPanic]){
        [[PanicManager sharedPanicManager] stopAlarmSound];
    }
    [[NSUserDefaults standardUserDefaults] setValue:[NSDate date] forKey:TIME_GO_BG];
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
     */
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    if([LogFileManager isLogConsole]) {
        FCSLog(@"applicationWillEnterForeground");
    }
    
    //Delete old data of additional job
    if(!isDemoMode)
        [[AdditionalJobManagerV2 sharedAddionalJobManager] deleteAllAddJobDataBeforeToday];
    if([PanicManager isRunningPanic]){
        [[PanicManager sharedPanicManager] playAlarmSound];
    }
    NSDate *timeBG = [[NSUserDefaults standardUserDefaults] valueForKey:TIME_GO_BG];
    NSDate *now = [NSDate date];
    NSTimeInterval dif = [now timeIntervalSinceDate:timeBG];

    for (UINavigationController *nav in [self.tabBarController childViewControllers]) {
        BOOL flag = NO;
        for (UIViewController *viewController in [nav childViewControllers]) {
            if ([[viewController class] isSubclassOfClass:[HomeViewV2 class]]) {
                flag = YES;
                HomeViewV2 *homeView = (HomeViewV2*) viewController;
                RoomAssignmentInfoViewController *roomAssignmentInfoViewController = nil;
                
                AdditionalJobDetailsScreenV2 *addJobDetailView = nil;
                OtherActivityViewController *otherActivityDetailView = nil;
                for (UIViewController *subViewController in [homeView.navigationController childViewControllers]) {
                    if([LogFileManager isLogConsole]) {
                        FCSLog(@"%@",subViewController);
                    }
                    
                    if ([[subViewController class] isSubclassOfClass:[RoomAssignmentInfoViewController class]]) {
                        roomAssignmentInfoViewController = (RoomAssignmentInfoViewController*)subViewController;
                        if([now compare:timeBG] == NSOrderedDescending)
                        {
                            [roomAssignmentInfoViewController countDownWithTimeDif:(int)dif];
                        }
                    }
                    
                    if ([[subViewController class] isSubclassOfClass:[AdditionalJobDetailsScreenV2 class]]) {
                        addJobDetailView = (AdditionalJobDetailsScreenV2*)subViewController;
                        if([now compare:timeBG] == NSOrderedDescending)
                        {
                            [addJobDetailView countDownWithTimeDif:(int)dif];
                        }
                    }
                    
                    if ([[subViewController class] isSubclassOfClass:[OtherActivityViewController class]]) {
                        
                        otherActivityDetailView = (OtherActivityViewController*)subViewController;
                        if ([now compare:timeBG] == NSOrderedDescending) {
                            [otherActivityDetailView countDownWithTimeDif:(int)dif];
                        }
                    }
                }
                
                break;
            }
        }
        if (flag) {
            break;
        }
    }
       /*
     Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
     */
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    //Clear badge number for push notification
    application.applicationIconBadgeNumber = 0;
    
    if([LogFileManager isLogConsole]) {
        FCSLog(@"applicationDidBecomeActive");
    }
    
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    //Remove remote notification when app terminated
    [[UIApplication sharedApplication] unregisterForRemoteNotifications];
    if([LogFileManager isLogConsole]) {
        FCSLog(@"applicationWillTerminate");
    }
    /*
     Called when the application is about to terminate.
     Save data if appropriate.
     See also applicationDidEnterBackground:.
     */
    if (((CleaningTimeManager*)[CleaningTimeManager sharedManager]).isCleaning) {
        [[CleaningTimeManager sharedManager] saveRoomRecordDetail];
    }
    // DungPhan - 20151023: save progress of Other Activity to DB when app terminated
    // need to handle for Room Assignment and Addtional Job also in future
    for (UINavigationController *nav in [self.tabBarController childViewControllers]) {
        BOOL flag = NO;
        for (UIViewController *viewController in [nav childViewControllers]) {
            if ([[viewController class] isSubclassOfClass:[HomeViewV2 class]]) {
                flag = YES;
                HomeViewV2 *homeView = (HomeViewV2*) viewController;
                
                OtherActivityViewController *otherActivityDetailView = nil;
                
                for (UIViewController *subViewController in [homeView.navigationController childViewControllers]) {
                    if ([LogFileManager isLogConsole]) {
                        FCSLog(@"%@",subViewController);
                    }
                    if ([[subViewController class] isSubclassOfClass:[OtherActivityViewController class]]) {
                        
                        otherActivityDetailView = (OtherActivityViewController*)subViewController;
                        [otherActivityDetailView saveProgress];
                    }
                }
                break;
            }
        }
        if (flag) {
            break;
        }
    }
}

- (void)dealloc
{
    sqlite3_shutdown();
    [super dealloc];
}

#pragma mark -
#pragma mark tabBarControllerDelegate Methods
// Optional UITabBarControllerDelegate method.
- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
    
}



// Optional UITabBarControllerDelegate method.
- (void)tabBarController:(UITabBarController *)tabBarController didEndCustomizingViewControllers:(NSArray *)viewControllers changed:(BOOL)changed
{
    
}


#pragma mark - Version Functions

//Hao Tran[20130520/Version functions] - Create function for checking app version
-(NSString*)getCurrentVersion
{
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
//    NSString *appDisplayName = [infoDictionary objectForKey:@"CFBundleDisplayName"];
    NSString *majorVersion = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
    NSString *minorVersion = [infoDictionary objectForKey:@"CFBundleVersion"];
    return [NSString stringWithFormat:@"Version %@ (%@)", majorVersion, minorVersion];
}

-(NSString*)getPreviousVersion
{
    NSString *previousVersion = [[NSUserDefaults standardUserDefaults] stringForKey:APP_VERSION];
    return previousVersion;
}

//Clear all user data
-(void)ResetDataBaseAndLanguages
{
    [self resetLocalDatabase];
    [self resetDemoDatabase];
    [self resetAllLanguages];
}
//Hao Tran[20130520/Version functions] - END

#pragma mark - Copy Database to writable location
- (void)createEditableCopyOfDatabaseIfNeeded
{
    NSString *databaseName = [[[NSString alloc] initWithFormat:@"%@", DATABASE_NAME] autorelease];
    [self createDataBaseName:databaseName isOverride:NO];
    
    databaseName = [[[NSString alloc] initWithFormat:@"%@", DATABASE_NAME_DEMO] autorelease];
    [self createDataBaseName:databaseName isOverride:NO];
}

//Create database name with override permission
-(void) createDataBaseName:(NSString*) databaseName isOverride:(BOOL) isOverride
{
    BOOL success = NO;
    success = [self isExistingDatabase: databaseName];
    
    if(success && isOverride) {
        return;
    }
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    //NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSArray *paths = NSSearchPathForDirectoriesInDomains(DataDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:databaseName];
    
    // The writable database does not exist, so copy the default to the appropriate location.
    NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:databaseName];
    success = [fileManager copyItemAtPath:defaultDBPath toPath:writableDBPath error:&error];
    
    //Skip Backup Data for AppStore Deployment
    NSURL *skipBackupDatabase = [NSURL fileURLWithPath:writableDBPath];
    [[NSFileManager defaultManager] addSkipBackupAttributeToItemAtURL:skipBackupDatabase];
}

//HaoTran[20130509/Add Func Reset Database] - Add functions to reset database when user change url WS in setting
//Reset mHouseKeeping.db
-(void)resetLocalDatabase
{
    //To reload default setting
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:DEFAULT_SETTING];
 
    //Update current language file
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:IS_LOAD_LANGUAGE];
    
    NSString *databaseName = @"";
    
    databaseName = [[[NSString alloc] initWithFormat:@"%@", DATABASE_NAME] autorelease];
    BOOL isExisting = [self isExistingDatabase: databaseName];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    //NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSArray *paths = NSSearchPathForDirectoriesInDomains(DataDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:databaseName];
    NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:databaseName];
    
    BOOL sucess = YES;
    //check to remove old database
    if (isExisting) {
        sucess = [fileManager removeItemAtPath:writableDBPath error:&error];
    }
    //create new DB
    [fileManager copyItemAtPath:defaultDBPath toPath:writableDBPath error:&error];
    
    //Skip Backup Data for AppStore Deployment
    NSURL *skipBackupDatabase = [NSURL fileURLWithPath:writableDBPath];
    [[NSFileManager defaultManager] addSkipBackupAttributeToItemAtURL:skipBackupDatabase];
    
}

-(void)resetDemoDatabase
{
    NSString *databaseName = @"";

    databaseName = [[[NSString alloc] initWithFormat:@"%@",DATABASE_NAME_DEMO] autorelease];
    BOOL isExisting = [self isExistingDatabase: databaseName];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    //NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSArray *paths = NSSearchPathForDirectoriesInDomains(DataDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:databaseName];
    NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:databaseName];
    
    BOOL sucess = YES;
    //check to remove old database
    if (isExisting) {
        sucess = [fileManager removeItemAtPath:writableDBPath error:&error];
    }
    //create new DB
    [fileManager copyItemAtPath:defaultDBPath toPath:writableDBPath error:&error];
    
    //Skip Backup Data for AppStore Deployment
    NSURL *skipBackupDatabase = [NSURL fileURLWithPath:writableDBPath];
    [[NSFileManager defaultManager] addSkipBackupAttributeToItemAtURL:skipBackupDatabase];
}

//Clear all languages and replace new ones in current build
-(void)resetAllLanguages
{
    BOOL success;
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    //NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSArray *paths = NSSearchPathForDirectoriesInDomains(DataDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *languageName = [[NSString alloc] initWithFormat:@"%@", L_DEFAULT_LANGUAGE_FILE];
    NSString *languageDefault = [documentsDirectory stringByAppendingPathComponent:languageName];
    success = [fileManager fileExistsAtPath:languageDefault];
    //[languageName release];
    
    if (success) {
        //Hao Tran[20130518/Fix miss new data] - Fix miss data if any update local language
        //remove old file language
        [fileManager removeItemAtPath:languageDefault error:nil];
        //return;
        //Hao Tran[20130518/Fix miss new data] - END
    }
    
    NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:languageName];
    success = [fileManager copyItemAtPath:defaultDBPath toPath:languageDefault error:&error];
    
    [languageName release];
    languageName = [[NSString alloc] initWithFormat:@"%@", L_ENG_LANGUAGE_NAME];
    NSString *languageEngPath = [documentsDirectory stringByAppendingPathComponent:languageName];
    success = [fileManager fileExistsAtPath:languageEngPath];
    
    if (success) {
        //Hao Tran[20130518/Fix miss new data] - Fix miss data if any update local language
        //remove old file language
        [fileManager removeItemAtPath:languageEngPath error:nil];
        //return;
        //Hao Tran[20130518/Fix miss new data] - END
    }
    
    // The writable database does not exist, so copy the default to the appropriate location.
    defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:languageName];
    success = [fileManager copyItemAtPath:defaultDBPath toPath:languageEngPath error:&error];
    if (!success) {
        //NSAssert1(0, @"Failed to create writable database file with message '%@'.", [error localizedDescription]);
    }
    
    //Skip Backup Data for AppStore Deployment
    NSURL *skipBackupLanguage = [NSURL fileURLWithPath:languageEngPath];
    [[NSFileManager defaultManager] addSkipBackupAttributeToItemAtURL:skipBackupLanguage];
    
    [languageName release];
    //copy chinese language
    languageName = [[NSString alloc] initWithFormat:@"%@", L_SIMPLIFIED_LANGUAGE_NAME];
    languageEngPath = [documentsDirectory stringByAppendingPathComponent:languageName];
    success = [fileManager fileExistsAtPath:languageEngPath];
    
    if (success) {
        //Hao Tran[20130518/Fix miss new data] - Fix miss data if any update local language
        //remove old file language
        [fileManager removeItemAtPath:languageEngPath error:nil];
        //return;
        //Hao Tran[20130518/Fix miss new data] - END
    }
    
    // The writable database does not exist, so copy the default to the appropriate location.
    defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:languageName];
    [fileManager copyItemAtPath:defaultDBPath toPath:languageEngPath error:&error];
    
    //Skip Backup Data for AppStore Deployment
    skipBackupLanguage = [NSURL fileURLWithPath:languageEngPath];
    [[NSFileManager defaultManager] addSkipBackupAttributeToItemAtURL:skipBackupLanguage];
    
    [languageName release];
}

//Check whether or not database mHouseKeeping.db is existing
//return True if existing
//return False if not
-(BOOL)isExistingDatabase: (NSString*)databaseName
{
    // First, test for existence.
    BOOL isExisting = NO;
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    //NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSArray *paths = NSSearchPathForDirectoriesInDomains(DataDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:databaseName];
    isExisting = [fileManager fileExistsAtPath:writableDBPath];
    
    return isExisting;
}
//HaoTran[20130509/Add Reset Database] - END

-(void)copyLanguagePackToEditableIfNeeded {
    
    BOOL success;
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    //NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSArray *paths = NSSearchPathForDirectoriesInDomains(DataDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *languageName = [NSString stringWithFormat:@"%@", L_DEFAULT_LANGUAGE_FILE];
    NSString *languageDefault = [documentsDirectory stringByAppendingPathComponent:languageName];
    success = [fileManager fileExistsAtPath:languageDefault];
    
    if (success) {
        return;
    }
    
    NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:languageName];
    success = [fileManager copyItemAtPath:defaultDBPath toPath:languageDefault error:&error];
    
    languageName = [NSString stringWithFormat:@"%@", L_ENG_LANGUAGE_NAME];
    NSString *languageEngPath = [documentsDirectory stringByAppendingPathComponent:languageName];
    success = [fileManager fileExistsAtPath:languageEngPath];
    
    if (success) {
        return;
    }
    
    // The writable database does not exist, so copy the default to the appropriate location.
    defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:languageName];
    success = [fileManager copyItemAtPath:defaultDBPath toPath:languageEngPath error:&error];
    
    //copy chinese language
    languageName = [NSString stringWithFormat:@"%@", L_SIMPLIFIED_LANGUAGE_NAME];
    languageEngPath = [documentsDirectory stringByAppendingPathComponent:languageName];
    success = [fileManager fileExistsAtPath:languageEngPath];
    
    if (success) {
        return;
    }
    
    // The writable database does not exist, so copy the default to the appropriate location.
    defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:languageName];
    [fileManager copyItemAtPath:defaultDBPath toPath:languageEngPath error:&error];
}

#pragma mark - Push Notification Delegate
- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    NSLog(@"My token is: %@", deviceToken);
    NSString * deviceTokenString = [[[[deviceToken description] stringByReplacingOccurrencesOfString: @"<" withString: @""] stringByReplacingOccurrencesOfString: @">" withString: @""]   stringByReplacingOccurrencesOfString: @" " withString: @""];
    NSLog(@"the generated device token string is : %@",deviceTokenString);
    
    [[NSUserDefaults standardUserDefaults] setObject: deviceTokenString forKey:@"deviceToken"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    NSLog(@"Failed to get token, error: %@", error);
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    if (application.applicationState == UIApplicationStateActive) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[L_alert currentKeyToLanguage] message:userInfo[@"aps"][@"alert"] delegate:nil cancelButtonTitle:L_OK otherButtonTitles:nil];
        
        [alertView show];
    }
    else if (application.applicationState == UIApplicationStateBackground || application.applicationState == UIApplicationStateInactive) {
        // Do something else rather than showing an alert view, because it won't be displayed.
    }
}

/*
-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    
}
*/

#pragma mark - === UINavigationController Delegate Methods ===
#pragma mark
-(void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    
    //Catch lastview appeared for debug mode
    [LogFileManager logDebugMode:@"Last View Appeared: %@", NSStringFromClass(viewController.class)];
    
    //update number of records must post for button sync
    SyncManagerV2 *syncmanager = [[SyncManagerV2 alloc] init];
    [syncmanager updateMustPostRecords];
    [syncmanager release];
}

#pragma mark - PTT Integrate
- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
#if DEFAULT_ENABLE_PTT
    UIApplicationState state = [application applicationState];
    FCSLog(@"%@", notification.userInfo);
    if (state == UIApplicationStateActive) {
        //        [self playSystemSound];

        PTTLanguageManager *langObject = [PTTLanguageManager sharedLanguageManager];
        //        ;
        [UIAlertView showWithTitle:@""
                           message:notification.alertBody
                 cancelButtonTitle:[langObject getTranslationForKey:@"Accept"]
                 otherButtonTitles:@[[langObject getTranslationForKey:@"Reject"]]
                          tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                              //                              [audioPlayer pause];
                              if (buttonIndex == [alertView cancelButtonIndex]) {
                                  [GlobalObject instance].callToUserID = notification.userInfo[@"callerId"];
                                  NotifPost(@"STOP_CALL_SOUND");
                                  
                                  NotifPost2Obj(MQTT_MAKE_CALL, notification.userInfo);
                                  
                              } else {
                                  
                              } }];
        

        return;
        
    }
    
    // Request to reload table view data
    [GlobalObject instance].callToUserID = notification.userInfo[@"callerId"];
    NotifPost(@"STOP_CALL_SOUND");
    NotifPost2Obj(MQTT_MAKE_CALL, notification.userInfo);
#endif
    // Set icon badge number to zero
    application.applicationIconBadgeNumber = 0;
}



//- (void)callDetect{
//    CTCallCenter *callCenter = [[[CTCallCenter alloc] init] autorelease];
//    [callCenter setCallEventHandler:^(CTCall *call)
//     {
//         dispatch_async(dispatch_get_main_queue(), ^{
//             NotifPost(@"SHUTDOWN");
////             FCSLog(@"Event handler called");
//             if ([call.callState isEqualToString: CTCallStateConnected])
//             {
//                 
////                 FCSLog(@"Connected");
//             }
//             else if ([call.callState isEqualToString: CTCallStateDialing])
//             {
////                 FCSLog(@"Dialing");
//             }
//             else if ([call.callState isEqualToString: CTCallStateDisconnected])
//             {
//                 NotifPost(@"RESTART");
////                 FCSLog(@"Disconnected");
//                 
//             } else if ([call.callState isEqualToString: CTCallStateIncoming])
//             {
////                 FCSLog(@"Incomming");
//             }
//         });
//         
//     }];
//}

// CFG [20160926/CRF-00001432] - Enable pooling from the server to stay alive.
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Socket Delegate
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)socket:(GCDAsyncSocket *)sock didConnectToHost:(NSString *)host port:(UInt16)port {
    FCSLog(@"eHouseKeepingAppDelegate.m>socket>%p, didConnectToHost:%@, port:%hu", sock, host, port);
    
    [self readWithTag:1];
}

- (void)socket:(GCDAsyncSocket *)sock didWriteDataWithTag:(long)tag
{
    FCSLog(@"eHouseKeepingAppDelegate.m>socket>:%p didWriteDataWithTag:%ld", sock, tag);
}

- (void)socket:(GCDAsyncSocket *)sock didReadData:(NSData *)data withTag:(long)tag
{
    FCSLog(@"eHouseKeepingAppDelegate.m>socket>:%p didReadData:withTag:%ld", sock, tag);
    
    [self readWithTag:2];
}

- (void)socketDidDisconnect:(GCDAsyncSocket *)sock withError:(NSError *)err {
    FCSLog(@"eHouseKeepingAppDelegate.m>socketDidDisconnect>%p, withError: %@", sock, err);
    
    UIBackgroundTaskIdentifier counterTask = [[UIApplication sharedApplication]
                                              beginBackgroundTaskWithExpirationHandler:^{
                                                  [[UIApplication sharedApplication] endBackgroundTask:counterTask];
                                              }];
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW,5* NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        if (!asyncSocket.isConnected)
        {
            FCSLog(@"The socket reconnect");
            NSError *error = nil;
            
            NSString *sHeartbeatServer = [[NSUserDefaults standardUserDefaults] stringForKey:KEY_HEARTBEAT_SERVER];
            int nHeartbeatPort = (int) [[NSUserDefaults standardUserDefaults] stringForKey:KEY_HEARTBEAT_PORT].integerValue;
            
            [asyncSocket connectToHost:sHeartbeatServer onPort:nHeartbeatPort error:&error];
            [[UIApplication sharedApplication] endBackgroundTask:counterTask];
            
        }
    });
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Socket Related Methods
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)readWithTag:(long)tag {
    // reads response line-by-line
    [asyncSocket readDataToData:[GCDAsyncSocket CRLFData] withTimeout:-1 tag:tag];
}

// CFG [20160926/CRF-00001432] - End.

@end
