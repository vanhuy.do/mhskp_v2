//
//  ehkConvert.h
//  eHouseKeeping
//
//  Created by Khanh Nguyen on 14/06/2011.
//  Copyright 2011 TMA. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface ehkConvert : NSObject {
    
}

+(NSString*)commonDateRemoveTime:(NSString*)datetime;

//Convert date to string with format
+(NSString*)dateStringByDate:(NSDate*)date format:(NSString*)format;

//Subtract days from current date
+(NSString*)dateStringAgoWithPeriodDays:(int)beforeDays;
+(NSDate*)convertStringToDate:(NSString*)dateString formatInputString:(NSString*)formatString;
+(NSString *)GetDateTimeNowWithFormat:(NSString *)format;
+(NSString *)DateToStringWithString: (NSString *)stringDate fromFormat:(NSString *)fromFormat toFormat:(NSString *)toFormat;
+(NSString *)getyyyyMMddhhmm;
+(NSString *)getddMMMyyyyHHmm;
+(NSString *)getyyyy_MM_dd_HH_mm_ss;
+(NSString *)charToString:(char*)input;
+(UIImage*)imageWithImage:(UIImage*)sourceImage scaledToSizeWithSameAspectRatio:(CGSize)targetSize;
+(NSString *)getTodayDateString;

//Checking date time with current time is in the past or not
+(BOOL)isDateTimeInPast:(NSDate*)dateTimeChecked;

@end
