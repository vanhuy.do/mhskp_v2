//
//  ehkConvert.m
//  eHouseKeeping
//
//  Created by Khanh Nguyen on 14/06/2011.
//  Copyright 2011 TMA. All rights reserved.
//

#import "ehkConvert.h"
#define radians( degrees ) ( degrees * M_PI / 180 )

@implementation ehkConvert

static NSString *yyyyMMddhhmm = @"yyyyMMdd:HHmmss";
static NSString *ddMMMyyyyHHmm = @"ddMMMyyyy',' HH:mm";
static NSString *yyyy_MM_dd_HH_mm_ss=@"yyyy-MM-dd HH:mm:ss";  //@ format of this : yyyy-MM-dd HH-mm-ss

//Convert date to string with format
+(NSString*)commonDateRemoveTime:(NSString*)datetime
{
    NSDateFormatter *dateFormat = [[[NSDateFormatter alloc] init] autorelease];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"]; //Common date time
    NSDate *date = [dateFormat dateFromString:datetime];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    //NSString *result = [NSString stringWithFormat:@"%@ 00:00:00",[dateFormat stringFromDate:date]];
    NSString *result = [dateFormat stringFromDate:date];
    return result;
}

//Convert date to string with format
+(NSString*)dateStringByDate:(NSDate*)date format:(NSString*)format
{
    NSDateFormatter *dateFormat = [[[NSDateFormatter alloc] init] autorelease];
    [dateFormat setDateFormat:format];
    NSString *value = [dateFormat stringFromDate:date];
    return value;
}

//Subtract days from current date
+(NSString*)dateStringAgoWithPeriodDays:(int)beforeDays
{
    NSDate *now = [NSDate date];
    NSDate *daysAgo = [now dateByAddingTimeInterval: -beforeDays * 24 * 60 * 60];
    NSDateFormatter *dateFormat = [[[NSDateFormatter alloc] init] autorelease];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *value = [dateFormat stringFromDate:daysAgo];
    return value;
}

+(NSDate*)convertStringToDate:(NSString*)dateString formatInputString:(NSString*)formatString
{
    NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
    [dateFormatter setDateFormat:formatString];
    NSDate *date = [dateFormatter dateFromString:dateString];
    return date;
}

+(NSString *)GetDateTimeNowWithFormat:(NSString *)format {
    if (format == nil) {
        format = [self getyyyy_MM_dd_HH_mm_ss];
    }
    NSDate* now = [NSDate date];
    NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
    
    [dateFormatter setDateFormat:format];
    return [dateFormatter stringFromDate:now];
}

+(NSString *)getTodayDateString
{
    NSDateFormatter *dateFormat = [[[NSDateFormatter alloc] init] autorelease];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSDate *now = [NSDate date];
    NSString *strNow = [dateFormat stringFromDate:now];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [dateFormat setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    now = [dateFormat dateFromString:[NSString stringWithFormat:@"%@ 00:00:00",strNow]];
    return [dateFormat stringFromDate:now];
}

+(NSString *)DateToStringWithString:(NSString *)stringDate fromFormat:(NSString *)fromFormat toFormat:(NSString *)toFormat{
    
    if(stringDate.length <= 0){
        return @"";
    }
    NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
    //[dateFormatter setDateFormat:@"yyyyMMdd:HHmm"];
    [dateFormatter setDateFormat:fromFormat];
    NSDate *date = [dateFormatter dateFromString:stringDate];
    //[dateFormatter setDateFormat:@"ddMMMyyyy',' HH:mm"];
    [dateFormatter setDateFormat:toFormat];
    return [dateFormatter stringFromDate:date];
}

+(NSString *)getyyyyMMddhhmm{
    return yyyyMMddhhmm;
}

+(NSString *)getddMMMyyyyHHmm{
    return ddMMMyyyyHHmm;
}

+(NSString *)getyyyy_MM_dd_HH_mm_ss{
    return yyyy_MM_dd_HH_mm_ss;
}

/*
 @return string from array char.
 */
+(NSString *)charToString:(char*)input
{
    return  [[[NSString alloc] initWithBytes:input length:strlen(input) encoding:NSASCIIStringEncoding] autorelease];
}

// Resize a image to a smaller size
+ (UIImage*)imageWithImage:(UIImage*)sourceImage scaledToSizeWithSameAspectRatio:(CGSize)targetSize
{  
    CGSize imageSize = sourceImage.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    CGFloat targetWidth = targetSize.width;
    CGFloat targetHeight = targetSize.height;
    CGFloat scaleFactor = 0.0;
    CGFloat scaledWidth = targetWidth;
    CGFloat scaledHeight = targetHeight;
    CGPoint thumbnailPoint = CGPointMake(0.0,0.0);
    
    if (CGSizeEqualToSize(imageSize, targetSize) == NO) {
        CGFloat widthFactor = targetWidth / width;
        CGFloat heightFactor = targetHeight / height;
        
        if (widthFactor > heightFactor) {
            scaleFactor = widthFactor; // scale to fit height
        }
        else {
            scaleFactor = heightFactor; // scale to fit width
        }
        
        scaledWidth  = width * scaleFactor;
        scaledHeight = height * scaleFactor;
        
        // center the image
        if (widthFactor > heightFactor) {
            thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5; 
        }
        else if (widthFactor < heightFactor) {
            thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
        }
    }     
    
    CGImageRef imageRef = [sourceImage CGImage];
    CGBitmapInfo bitmapInfo = CGImageGetBitmapInfo(imageRef);
    CGColorSpaceRef colorSpaceInfo = CGImageGetColorSpace(imageRef);
    
    if (bitmapInfo == kCGImageAlphaNone) {
        bitmapInfo = kCGImageAlphaNoneSkipLast;
    }
    
    CGContextRef bitmap;
    
    if (sourceImage.imageOrientation == UIImageOrientationUp || sourceImage.imageOrientation == UIImageOrientationDown) {
        bitmap = CGBitmapContextCreate(NULL, targetWidth, targetHeight, CGImageGetBitsPerComponent(imageRef), CGImageGetBytesPerRow(imageRef), colorSpaceInfo, bitmapInfo);
        
    } else {
        bitmap = CGBitmapContextCreate(NULL, targetHeight, targetWidth, CGImageGetBitsPerComponent(imageRef), CGImageGetBytesPerRow(imageRef), colorSpaceInfo, bitmapInfo);
        
    }   
    
    // In the right or left cases, we need to switch scaledWidth and scaledHeight,
    // and also the thumbnail point
    if (sourceImage.imageOrientation == UIImageOrientationLeft) {
        thumbnailPoint = CGPointMake(thumbnailPoint.y, thumbnailPoint.x);
        CGFloat oldScaledWidth = scaledWidth;
        scaledWidth = scaledHeight;
        scaledHeight = oldScaledWidth;
        
        CGContextRotateCTM (bitmap, radians(90));
        CGContextTranslateCTM (bitmap, 0, -targetHeight);
        
    } else if (sourceImage.imageOrientation == UIImageOrientationRight) {
        thumbnailPoint = CGPointMake(thumbnailPoint.y, thumbnailPoint.x);
        CGFloat oldScaledWidth = scaledWidth;
        scaledWidth = scaledHeight;
        scaledHeight = oldScaledWidth;
        
        CGContextRotateCTM (bitmap, radians(-90));
        CGContextTranslateCTM (bitmap, -targetWidth, 0);
        
    } else if (sourceImage.imageOrientation == UIImageOrientationUp) {
        // NOTHING
    } else if (sourceImage.imageOrientation == UIImageOrientationDown) {
        CGContextTranslateCTM (bitmap, targetWidth, targetHeight);
        CGContextRotateCTM (bitmap, radians(-180.));
    }
    
    CGContextDrawImage(bitmap, CGRectMake(thumbnailPoint.x, thumbnailPoint.y, scaledWidth, scaledHeight), imageRef);
    CGImageRef ref = CGBitmapContextCreateImage(bitmap);
    UIImage* newImage = [UIImage imageWithCGImage:ref];
    
    CGContextRelease(bitmap);
    CGImageRelease(ref);
    
    return newImage; 
}

//Checking date time with current time is in the past or not
+(BOOL)isDateTimeInPast:(NSDate*)dateTimeChecked
{
    NSDate *currentTime = [NSDate date];
    if([currentTime compare:dateTimeChecked] == NSOrderedDescending || [currentTime compare:dateTimeChecked] == NSOrderedSame) {
        return YES;
    }
    
    return NO;
    
}

@end
