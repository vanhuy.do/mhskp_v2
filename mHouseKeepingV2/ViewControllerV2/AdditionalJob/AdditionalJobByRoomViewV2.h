//
//  AdditionalJobByRoomViewV2.h
//  mHouseKeeping

#import <UIKit/UIKit.h>
#import "AdditionalJobManagerV2.h"
#import "RoomManagerV2.h"

@interface AdditionalJobByRoomViewV2 : UIViewController 
    <UITableViewDelegate, UITableViewDataSource> {
    
        AdditionalJobManagerV2 *addJobManager;
        RoomManagerV2 *roomManager;
        NSMutableDictionary *listRoomDisplay;
        IBOutlet UIView * tableTitleView;
        UILabel *lblNoResult;
}

@property (nonatomic, strong) IBOutlet UILabel *lblRoomNo;
@property (nonatomic, strong) IBOutlet UILabel *lblRoomStatus;
@property (nonatomic, strong) IBOutlet UILabel *lblNumberJob;

@property (nonatomic, strong) IBOutlet UITableView *tbvContent;
@property (nonatomic, strong) NSMutableArray *listAddJobRooms;
@property (nonatomic, strong) NSMutableArray *listSearchAddJobItems;

@property (nonatomic) NSInteger floorId;
@property (nonatomic) NSInteger statusId;

-(void) loadData;
-(void) setCaptionsView;

@end
