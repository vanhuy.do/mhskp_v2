//
//  AdditionalJobByRoomViewV2.m
//  mHouseKeeping
//

#import "AdditionalJobByRoomViewV2.h"
#import "AdditionalJobRoomCellV2.h"
#import "RoomManagerV2.h"
#import "RoomModelV2.h"
#import "FloorModelV2.h"
#import "AddJobDetailModelV2.h"
#import "AdditionalJobManagerV2.h"
#import "MyNavigationBarV2.h"
#import "LogFileManager.h"
#import "UnassignManagerV2.h"
#import "AdditionalJobListScreenV2.h"
#import "DeviceManager.h"
#import "RoomTypeModel.h"

#define markImage           @"!.png"
#define backgroundImage     @"bg.png"

#define heightTbvContent    60.0f
#define tagNoResult         12345
#define heightTbvNoResult   240.0f

#define font                @"Arial-BoldMT"

#define sizeNoResult        22.0f
#define sizeNormal          17.0f
#define sizeSmall           10.0f

#define colorAlpha          1.0
#define colorWhite          255/255.0
#define colorBlueR          105/255.0
#define colorBlueG          218/255.0
#define colorBlueB          222/255.0
#define colorCellDueOut   [UIColor colorWithRed:216/255.0 green:255/255.0 blue:215/255.0 alpha:1.0f]
#define tagRoomNo                   1
#define tagRMStatus                 2
#define tagAddJobNames              3

@interface  RoomCellDisplay: NSObject
@property (nonatomic, retain) NSString *room_categories;
@property (nonatomic, retain) NSString *room_status;
@property (nonatomic, retain) NSString *room_type_name;
@end

@implementation RoomCellDisplay
@synthesize room_categories,room_status;
@end

@interface AdditionalJobByRoomViewV2(PrivateMethods)

-(void) btnDetailArrowPressed:(NSInteger)index;

-(void) loadTopbarView;
-(void) showTopbarView;
-(void) hideTopbarView;
-(void) setHiddenTopbarView;
-(void) adjustShowForViews;
-(void) adjustRemoveForViews;
-(void) addButtonHandleShowHideTopbar;
-(void) topbarTapped:(UIButton *) sender;
-(TopbarViewV2 *) getTopBarView;
-(void) btnBackPressed;
-(void) addBackButton;
- (void)addAccessoryButton:(UITableViewCell *)cell  andIndexPath:(NSIndexPath *)indexPath;

@end
@implementation AdditionalJobByRoomViewV2

@synthesize lblRoomNo, lblRoomStatus, lblNumberJob, tbvContent, floorId, statusId, listAddJobRooms, listSearchAddJobItems;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle
-(void)viewWillAppear:(BOOL)animated {
    [self setCaptionsView];
    
    // Load Data
    [self performSelector:@selector(loadData) withObject:nil afterDelay:0.0f];
    [super viewWillAppear:animated];
    
    //set selection find in bottom bar
    [[HomeViewV2 shareHomeView] setSelectedButton:tagOfAdditionalJob];
}


- (void)viewDidLoad {
    
    [super viewDidLoad];
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        //Clear redundant view for header, footter
        CGRect frameHeaderFooter = tbvContent.tableHeaderView.frame;
        frameHeaderFooter.size.height = 0.1f;
        UIView *headerView = [[UIView alloc] initWithFrame:frameHeaderFooter];
        [tbvContent setTableHeaderView:headerView];
        
        frameHeaderFooter = tbvContent.tableFooterView.frame;
        frameHeaderFooter.size.height = 0.1f;
        UIView *footerView = [[UIView alloc] initWithFrame:frameHeaderFooter];
        [tbvContent setTableFooterView:footerView];
    }
    
    addJobManager = [AdditionalJobManagerV2 sharedAddionalJobManager];
    roomManager = [[RoomManagerV2 alloc] init];
    listRoomDisplay = [[NSMutableDictionary alloc] init];

    [tbvContent setBackgroundView:nil];
    [tbvContent setBackgroundColor:[UIColor clearColor]];
    [tbvContent setOpaque:YES];

    [self performSelector:@selector(loadTopbarView)];
    
    [self addBackButton];
    
    // Load Data
    //[self performSelector:@selector(loadData) withObject:nil afterDelay:0.0f];
}

- (void)viewDidUnload {
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - UITableView Delegate Methods
-(CGFloat)tableView:(UITableView *)tableView 
    heightForRowAtIndexPath:(NSIndexPath *)indexPath {
        return heightTbvContent;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView 
    numberOfRowsInSection:(NSInteger)section {

    return [listAddJobRooms count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentify = @"AdditionalJobRoomCellV2";
    
    AdditionalJobRoomCellV2 *cell = (AdditionalJobRoomCellV2 *)[tableView dequeueReusableCellWithIdentifier:cellIdentify];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    if (cell == nil) {
        NSArray *arrayCell = [[NSBundle mainBundle] loadNibNamed:@"AdditionalJobRoomCellV2" owner:self options:nil];
        cell = (AdditionalJobRoomCellV2 *)[arrayCell objectAtIndex:0];
        
        [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
        
        UIColor *badgeFrameColor = nil;
        BOOL isShining = NO;
        if(SYSTEM_VERSION_LESS_THAN(@"7.0")){
            badgeFrameColor = [UIColor whiteColor];
            isShining = YES;
        } else {
            badgeFrameColor = [UIColor clearColor];
        }
        
        CustomBadge *customBadge = [CustomBadge customBadgeWithString:nil
                                                      withStringColor:[UIColor whiteColor]
                                                       withInsetColor:[UIColor redColor]
                                                       withBadgeFrame:YES
                                                  withBadgeFrameColor:badgeFrameColor
                                                            withScale:1.0
                                                          withShining:isShining];
        customBadge.tag = 982;
        [customBadge setFrame:CGRectMake(230 ,10, 27, 25)];
        [cell.contentView addSubview:customBadge];
        [customBadge setHidden:YES];
        
        //Rectangle for badge
        //customBadge1.badgeCornerRoundness = 0.1f;
    }
    
    AddJobRoomModelV2 *addJobRoom = [self.listAddJobRooms objectAtIndex:indexPath.row];
    RoomCellDisplay *cellDisplay = [listRoomDisplay objectForKey:addJobRoom.addjob_room_number];
    [cell.lblRoomNo setText: addJobRoom.addjob_room_number];
    [cell.lblRoomStatus setText:[cellDisplay room_status]];
    [cell.lblAddJobNames setText:[cellDisplay room_categories]];
    [cell.lblRoomType setText:[cellDisplay room_type_name]];
    
    int countAddJob = (int)addJobRoom.addjob_room_number_pending_job;
    
    CustomBadge *customBadge = (CustomBadge*)[cell.contentView viewWithTag:982];
    CGRect frameBadge = customBadge.frame;
    if(countAddJob <= 0)
    {
        [customBadge setHidden:YES];
    }
    else
    {
        [customBadge setHidden:NO];
        if(countAddJob > 0 && countAddJob <= 99) {
            frameBadge.size.width = 27;
            [customBadge setFrame:frameBadge];
        }else if(countAddJob >= 100 && countAddJob <= 999) {
            frameBadge.size.width = 30;
            [customBadge setFrame:frameBadge];
        } else if(countAddJob > 999) {
            frameBadge.size.width = 33;
            [customBadge setFrame:frameBadge];
        }
        
        if(countAddJob > 999) {
            [customBadge setBadgeText:@"999+"];
        } else {
            [customBadge setBadgeText:[NSString stringWithFormat:@"%d",countAddJob]];
        }
    }
    
    [customBadge setNeedsDisplay];
    
    if(addJobRoom.addjob_room_is_due_out > 0){
        [cell setBackgroundColor:colorCellDueOut];
    } else {
        [cell setBackgroundColor:[UIColor whiteColor]];
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
    
}


- (void)addAccessoryButton:(UITableViewCell *)cell  andIndexPath:(NSIndexPath *)indexPath{
    /*****************************************/
    UIButton *accessoryButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 24, 24)];
    [accessoryButton setBackgroundColor:[UIColor clearColor]];
    //    [accessoryButton setImage:[UIImage imageNamed:@"gray_arrow_36x36.png"] forState:UIControlStateNormal];
    [accessoryButton setImage:[UIImage imageNamed:@"gray_arrow_36x36.png"] forState:UIControlStateNormal];
    [accessoryButton setTag:indexPath.row];
    
    [accessoryButton addTarget:self action:@selector(btnDetailPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell setAccessoryView:accessoryButton];
    //cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
    /*****************************************/
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    AddJobRoomModelV2 *addJobRoomSelected = [self.listAddJobRooms objectAtIndex:indexPath.row];
    AdditionalJobListScreenV2 *addJobListScreen = [[AdditionalJobListScreenV2 alloc] initWithNibName:@"AdditionalJobListScreenV2" bundle:nil];
    addJobListScreen.roomSelected = addJobRoomSelected;
    [self.navigationController pushViewController:addJobListScreen animated:YES];
    
}

-(void) hiddenHUDAfterSaved:(MBProgressHUD *)HUD{
[HUD hide:YES];
[HUD removeFromSuperview];

}

-(void) loadDisplayRoomCell
{
    for (AddJobRoomModelV2 *curAddJob in listAddJobRooms) {
        
        int searchId = 0;
        NSMutableArray *listNumberItems = [NSMutableArray array];
        NSString *categoriesString = nil;
        if([listSearchAddJobItems count] > 0){
            for (AddJobItemModelV2 *curItem in listSearchAddJobItems) {
                [listNumberItems addObject:[NSNumber numberWithInteger:curItem.addjob_item_id]];
            }
            AddJobSearchModelV2 *searchSelected = [addJobManager loadAddJobSearchByUserId:[UserManagerV2 sharedUserManager].currentUser.userId tasksFilter:listNumberItems];
            
            if(searchSelected != nil){
                searchId = (int)searchSelected.addjob_search_id;
            }
        }
        
        if(isDemoMode){
            categoriesString = [addJobManager loadAddJobCategoriesStringByUserId:[UserManagerV2 sharedUserManager].currentUser.userId roomId:(int)curAddJob.addjob_room_id roomAssignId:(int)curAddJob.addjob_room_assign_id];
        }
        else{
            if(searchId > 0){
                categoriesString = [addJobManager loadAddJobCategoriesStringByUserId:[UserManagerV2 sharedUserManager].currentUser.userId roomId:(int)curAddJob.addjob_room_id searchId:searchId];
            } else {
                categoriesString = [addJobManager loadAddJobCategoriesStringByUserId:[UserManagerV2 sharedUserManager].currentUser.userId roomId:(int)curAddJob.addjob_room_id roomAssignId:(int)curAddJob.addjob_room_assign_id];
            }
        }
        
        RoomCellDisplay *cellDisplay = [[RoomCellDisplay alloc] init];
        [cellDisplay setRoom_categories:categoriesString];
        
        NSString *roomStatusName;
        NSString *roomTypeNameValue = @"";
        
        RoomTypeModel *roomTypeModel = [[RoomTypeModel alloc] init];
        roomTypeModel.amsId = curAddJob.addjob_room_type_id;
        [roomManager loadRoomTypeModelByPrimaryKey:roomTypeModel];
        
        RoomStatusModelV2 *roomStatusModel = [[RoomStatusModelV2 alloc] init];
        roomStatusModel.rstat_Id = (int)curAddJob.addjob_room_status_id;
        [roomManager loadRoomStatusData:roomStatusModel];
        
        if([[LanguageManagerV2 sharedLanguageManager] currentLanguageIsEqualTo:@"%@", ENGLISH_LANGUAGE]){
            roomStatusName = roomStatusModel.rstat_Name;
            roomTypeNameValue = roomTypeModel.amsName;
        } else {
            roomStatusName = roomStatusModel.rstat_Lang;
            roomTypeNameValue = roomTypeModel.amsNameLang;
        }
        
        [cellDisplay setRoom_status:roomStatusName];
        [cellDisplay setRoom_type_name:roomTypeNameValue];
        [listRoomDisplay setObject:cellDisplay forKey:curAddJob.addjob_room_number];
    }
}

#pragma mark - Load Data
-(void)loadData {
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getLoadingData]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES];
    [[HomeViewV2 shareHomeView] waitingLoadingData];
    
    if(listAddJobRooms == nil) {
        listAddJobRooms = [NSMutableArray array];
    }
    
    [listAddJobRooms removeAllObjects];
    
    int searchIdSelected = 0;
    //In search mode
    if ([listSearchAddJobItems count] > 0){
        NSMutableArray *listSearchNumbers = [NSMutableArray array];
        for (AddJobItemModelV2 *curItem in listSearchAddJobItems) {
            [listSearchNumbers addObject:[NSNumber numberWithInteger:curItem.addjob_item_id]];
        }
        if(!isDemoMode){
            [addJobManager loadWSAddJobRoomListByUserId:[UserManagerV2 sharedUserManager].currentUser.userId hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId floorId:(int)floorId tasksFilter:listSearchNumbers];
        }
        AddJobSearchModelV2 *searchModel = [addJobManager loadAddJobSearchByUserId:[UserManagerV2 sharedUserManager].currentUser.userId tasksFilter:listSearchNumbers];
        searchIdSelected = (int)searchModel.addjob_search_id;
        
    } else if (!isDemoMode){
        [addJobManager loadWSAddJobRoomListByUserId:[UserManagerV2 sharedUserManager].currentUser.userId hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId floorId:(int)floorId tasksFilter:nil];
    }
    
    listAddJobRooms = [addJobManager loadAddJobRoomsByUserId:[UserManagerV2 sharedUserManager].currentUser.userId floorId:(int)floorId searchId:searchIdSelected];
    if (listAddJobRooms.count == 0) {
//        lblNoResult  =  [[UILabel alloc]init];
//        lblNoResult.frame     =  CGRectMake(15, 150, 290, 80);
//        [lblNoResult setText:[[LanguageManagerV2 sharedLanguageManager] getNoResultsFound]];
//        lblNoResult.textAlignment = NSTextAlignmentCenter;
//        lblNoResult.layer.cornerRadius = 10;
//        [lblNoResult setFont:[UIFont fontWithName:@"Arial-BoldMT" size:22]];
//        lblNoResult.numberOfLines = 2;
//        [lblNoResult setTextColor:[UIColor grayColor]];
//        //lblAlert.font = [UIFont boldSystemFontOfSize:20.0f];
        [tbvContent setSeparatorStyle:UITableViewCellSeparatorStyleNone];
//        [tbvContent addSubview:lblNoResult];
//    }
//    else{
//        if(lblNoResult){
//            [lblNoResult removeFromSuperview];
//        }
    }
    [self loadDisplayRoomCell];
    
    [tbvContent reloadData];
    
    [self performSelector:@selector(hiddenHUDAfterSaved:) withObject:HUD afterDelay:0.0f];
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
}

#pragma mark  - Set Captions View
-(void)setCaptionsView {
    [self setTitle:[NSString stringWithFormat:@"%@ - %@ %@", [[LanguageManagerV2 sharedLanguageManager] getFind], [[LanguageManagerV2 sharedLanguageManager] getBy], [[LanguageManagerV2 sharedLanguageManager] getRoomTitle]]];

    [lblRoomNo setText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_room_number]];
    [lblRoomNo setFont:[UIFont fontWithName:font size:sizeNormal]];
    
    [lblRoomStatus setText:[[LanguageManagerV2 sharedLanguageManager] getRMStatus]];
    [lblRoomStatus setFont:[UIFont fontWithName:font size:sizeNormal]];
    
    [lblNumberJob setText:[[LanguageManagerV2 sharedLanguageManager] getNoOfJobs]];
    [lblNumberJob setFont:[UIFont fontWithName:font size:sizeNormal]];
}

#pragma mark - === Handle Topbar Methods ===
#pragma mark
-(void)loadTopbarView {
    TopbarViewV2 *topbar = [[TopbarViewV2 alloc] initViewWithSuperModel:[[CommonVariable sharedCommonVariable] superRoomModel]];
    [topbar setTag:tagTopbarView];
    [self.view addSubview:topbar];
    [topbar setHidden:YES];
    
    [topbar setAlpha:0.0];
    [self addButtonHandleShowHideTopbar];
}

-(void)showTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationWillStartSelector:@selector(setHiddenTopbarView)];
    
    f = topbar.frame;
    f.origin.y = 0;
    [topbar setFrame:f];
    [topbar setAlpha:1.0];
    
    [self adjustShowForViews];
    
    [UIView commitAnimations];
}

-(void)hideTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDidStopSelector:@selector(setHiddenTopbarView)];
    [UIView setAnimationDelegate:self];
    
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    [topbar setAlpha:0.0];
    
    [self adjustRemoveForViews];
    
    [UIView commitAnimations];
}

-(void)setHiddenTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    [topbar setHidden:!topbar.hidden];
}

-(TopbarViewV2 *)getTopBarView {
    return (TopbarViewV2 *)[self.view viewWithTag:tagTopbarView];
}

-(void)addButtonHandleShowHideTopbar {
    UIView *vBarButton = [[UIView alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
    UIButton *titleBarButtonFirst = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 200, 30)];
    
    titleBarButtonFirst.titleLabel.backgroundColor = [UIColor clearColor];
    [titleBarButtonFirst.titleLabel setFont: FONT_BUTTON_TOPBAR];
    titleBarButtonFirst.titleLabel.textAlignment = NSTextAlignmentCenter;
    titleBarButtonFirst.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
    
    [titleBarButtonFirst.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
    [titleBarButtonFirst.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
    [titleBarButtonFirst addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    int searchId = 0;
    if([listSearchAddJobItems count] > 0){
        NSMutableArray *listTasks = [NSMutableArray array];
        for (AddJobItemModelV2 *curItem in listSearchAddJobItems) {
            [listTasks addObject:[NSNumber numberWithInteger:curItem.addjob_item_id]];
        }
        AddJobSearchModelV2 *searchSelected = [addJobManager loadAddJobSearchByUserId:[UserManagerV2 sharedUserManager].currentUser.userId tasksFilter:listTasks];
        if(searchSelected){
            searchId = (int)searchSelected.addjob_search_id;
        }
    }
    
    AddJobFloorModelV2 *floorSelected = [addJobManager loadAddJobFloorByUserId:[UserManagerV2 sharedUserManager].currentUser.userId floorId:(int)floorId searchId:searchId];
    if(floorSelected != nil){
        if([[LanguageManagerV2 sharedLanguageManager] currentLanguageIsEqualTo:@"%@", ENGLISH_LANGUAGE ]){
            [titleBarButtonFirst setTitle:floorSelected.addjob_floor_name forState:UIControlStateNormal];
        } else {
            [titleBarButtonFirst setTitle:floorSelected.addjob_floor_name_lang forState:UIControlStateNormal];
        }
    }
    
    [vBarButton addSubview:titleBarButtonFirst];
    
    UIButton *titleBarButtonSecond = [[UIButton alloc] initWithFrame:CGRectMake(0, 22, 200, 22)];
    [titleBarButtonSecond setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [titleBarButtonSecond.titleLabel setFont:FONT_BUTTON_TOPBAR_SECOND];
    [titleBarButtonSecond setTitle:[[LanguageManagerV2 sharedLanguageManager] getAdditionalJobTitle] forState:UIControlStateNormal];
    [titleBarButtonSecond addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    [vBarButton addSubview:titleBarButtonSecond];
    
    self.navigationItem.titleView = vBarButton;
    //Move to center vertical
    CGRect titleViewBounds = self.navigationItem.titleView.bounds;
    self.navigationItem.titleView.bounds = CGRectMake(titleViewBounds.origin.x + 10, Y_TOPBAR_TITLE, titleViewBounds.size.width, titleViewBounds.size.height);
}

-(void)topbarTapped:(UIButton *)sender {
    TopbarViewV2 *topbar = [self getTopBarView];
    [sender setEnabled:NO];
    if (topbar.isHidden) {
        [self showTopbarView];
    } else {
        [self hideTopbarView];
    }
    [self performSelector:@selector(topbarButtonEnable:) withObject:sender afterDelay:0.3];
}

-(void) topbarButtonEnable:(UIButton *) topbarBtn {
    [topbarBtn setEnabled:YES];
}

-(void)adjustShowForViews {
    //adjust all views when show 
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    CGRect ftbv = tableTitleView.frame;
    [tableTitleView setFrame:CGRectMake(0, f.size.height, 320, ftbv.size.height)];
    
    ftbv = tbvContent.frame;
    [tbvContent setFrame:CGRectMake(0, f.size.height + tableTitleView.frame.size.height, 320, ftbv.size.height - f.size.height)];

}

-(void)adjustRemoveForViews { 
    //adjust all views when hide
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    [tableTitleView setFrame:CGRectMake(0, 0, 320, tableTitleView.frame.size.height)];
    
    CGRect ftbv = tbvContent.frame;
    [tbvContent setFrame:CGRectMake(0, tableTitleView.frame.size.height, 320, ftbv.size.height + f.size.height)];

}

#pragma mark - === Handle Back Button ===
#pragma mark

-(void)backBarPressed {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)addBackButton {
    self.navigationItem.leftBarButtonItem = [MyNavigationBarV2 createBackButtonWithTarget:self selector:@selector(backBarPressed) forControlEvents:UIControlEventTouchUpInside];
}

@end
