//
//  AdditionalJobRoomCellV2.h
//  mHouseKeeping


#import <UIKit/UIKit.h>

@interface AdditionalJobDetailCell : UITableViewCell {
    
}

@property (nonatomic, strong) IBOutlet UILabel *lblAddJobTitle;
@property (nonatomic, strong) IBOutlet UIImageView *imgAddJobStatus;
@property (nonatomic, strong) IBOutlet UILabel *lblTime;

@end
