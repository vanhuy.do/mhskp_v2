//
//  AdditionalJobByRoomViewV2.h
//  mHouseKeeping

#import <UIKit/UIKit.h>
#import "AdditionalJobManagerV2.h"
#import "RoomManagerV2.h"
#import "RoomModelV2.h"
#import "RoomAssignmentModelV2.h"
#import "GuestInfoViewV2.h"
#import "GuestInfoModelV2.h"
//MARK: re-use defined of "RoomAssignmentInfoViewController"
#import "RoomAssignmentInfoViewController.h"
#import "ehkDefinesV2.h"

@interface AdditionalJobDetailsScreenV2 : UIViewController 
    <UITableViewDelegate, UITableViewDataSource, UIAlertViewDelegate, RemarkViewV2Delegate, CleaningStatusPopupDelegate, CleaningStatusConfirmPopupDelegate> {
    
        //View
        IBOutlet UIScrollView *scrContent;
        IBOutlet UIScrollView *scrGuests;
        IBOutlet UIView *viewDetail;
        IBOutlet UIButton *btnGuestInfo;
        IBOutlet UIButton *btnRoomStatus;
        IBOutlet UIButton *btnRoomAssignment;
        IBOutlet UIImageView *imgRushQueueRoom;
        IBOutlet UIImageView *imgRoomStatus;
        IBOutlet UILabel *lblGuestInfo;
        IBOutlet UIView *viewAction;
        IBOutlet UIButton *btnStartStop;
        IBOutlet UIButton *btnComplete;
        IBOutlet UIButton *btnTimer;
        IBOutlet UILabel *lblRoomStatus;
        IBOutlet UIButton *btnCleaningStatus;
        
        UIButton *titleBarButtonFirst;
        UIButton *titleBarButtonSecond;
        CleaningStatusPopupView *cleaningStatusPopup;
        CleaningStatusConfirmPopupView *cleaningStatusConfirmPopup;
        RemarkViewV2 *remarkView;
        //Variables for cleaning, inspecting
        enum TIMER_STATUS timerStatus;
        NSTimer *timer;
        NSInteger timeLeft;
        NSInteger duration;
        //GuestInfoViewV2 *guestInfo;
        //GuestInfoViewV2 *guestInfoArrival;
        NSMutableArray *listGuestModels;
        NSMutableArray *listGuestViews;
        UIImageView *guestIndicatorLeft;
        UIImageView *guestIndicatorRight;
        //AddJobGuestInfoModelV2 *guestInfoModel;
        //AddJobGuestInfoModelV2 *guestInfoArrivalModel;
        
        AdditionalJobManagerV2 *addJobManager;
        NSString *addJobCategorySelected;
        bool isEnglishLang;
        bool isStartOnLoad;
        bool isAlreadyLayoutSubviewsFirstLoad;
        bool isAlreadyGetGuest;
}

@property (nonatomic, retain) IBOutlet UITableView *tbvContent;
@property (nonatomic, retain) NSMutableArray *listTaskDetails;
@property (nonatomic, assign) enum IS_PUSHED_FROM isPushFrom;
@property (nonatomic, retain) AddJobDetailModelV2 *detailSelected;
@property (nonatomic, retain) AddJobRoomModelV2 *roomSelected;

-(void) loadData;
-(void) setCaptionsView;
-(IBAction)btnCompleteClicked:(id)sender;
-(IBAction)btnStartStopClicked:(id)sender;
-(IBAction)btnGuestInfoClicked:(id)sender;
-(IBAction)btnRoomAssignmentInfo:(id)sender;

//Update timer in AppDelegate become foreground
-(void) countDownWithTimeDif:(NSInteger) timeDif;

@end
