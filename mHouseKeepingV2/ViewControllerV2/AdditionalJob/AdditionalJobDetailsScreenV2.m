//
//  AdditionalJobByRoomViewV2.m
//  mHouseKeeping
//

#import "AdditionalJobDetailsScreenV2.h"
#import "AdditionalJobRoomCellV2.h"
#import "RoomManagerV2.h"
#import "RoomModelV2.h"
#import "FloorModelV2.h"
#import "AddJobDetailModelV2.h"
#import "AdditionalJobManagerV2.h"
#import "MyNavigationBarV2.h"
#import "LogFileManager.h"
#import "UnassignManagerV2.h"
#import "DeviceManager.h"
#import "ActionPopupView.h"
#import "LanguageManagerV2.h"
#import "AdditionalJobListByFloorViewV2.h"
#import "ehkDefinesV2.h"
#import "UIImage+Tint.h"
#import "AddJobFloorModelV2.h"
#import "CustomAlertViewV2.h"

#define heightTaskDetailsCell    45.0f
#define heightAddJobCell    45.0f
#define heightHeaderTable   20.0f
#define heightFooterTable   4.0f
#define tagNoResult         12345
#define numberTableSection  2

#define heightTbvNoResult   240.0f
#define sizeNoResult        22.0f
#define sizeNormal          17.0f
#define sizeSmall           10.0f

#define colorAlpha          1.0
#define colorWhite          255/255.0
#define colorBlueR          105/255.0
#define colorBlueG          218/255.0
#define colorBlueB          222/255.0

#define tagConfirmDone      4812
#define tagConfirmStop      4813

@interface AdditionalJobDetailsScreenV2(PrivateMethods)

-(void) btnDetailArrowPressed:(NSInteger)index;

-(void) loadTopbarView;
-(void) showTopbarView;
-(void) hideTopbarView;
-(void) setHiddenTopbarView;
-(void) adjustShowForViews;
-(void) adjustRemoveForViews;
-(void) addButtonHandleShowHideTopbar;
-(void) topbarTapped:(UIButton *) sender;
-(TopbarViewV2 *) getTopBarView;
-(void) btnBackPressed;
-(void) addBackButton;
- (void)addAccessoryButton:(UITableViewCell *)cell  andIndexPath:(NSIndexPath *)indexPath;

@end

@implementation AdditionalJobDetailsScreenV2
@synthesize tbvContent;
@synthesize detailSelected,roomSelected;
@synthesize listTaskDetails;
@synthesize isPushFrom;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle
-(void)viewWillAppear:(BOOL)animated {

    [super viewWillAppear:animated];
    [self setCaptionsView];
    isEnglishLang = [[LanguageManagerV2 sharedLanguageManager] currentLanguageIsEqualTo:@"%@", ENGLISH_LANGUAGE];
    
    if(isPushFrom == IS_PUSHED_FROM_ADD_JOB_LIST)
    {
        //set selection find in bottom bar
        [[HomeViewV2 shareHomeView] setSelectedButton:tagOfAdditionalJob];
    }
    else
    {
        [[HomeViewV2 shareHomeView] setSelectedButton:tagOfHomeButton];
    }

}

-(void)viewDidAppear:(BOOL)animated
{
    TopbarViewV2 *topbar = [self getTopBarView];
    BOOL isTopbarShowing = (topbar.isHidden != YES || topbar.alpha > 0.0f);
    
    [self modifyAllViews:isTopbarShowing];
    [self populateData];
    
    [super viewDidAppear:animated];
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    //Fix bug content in table is lost apart on first load
    if(!isAlreadyLayoutSubviewsFirstLoad){
        isAlreadyLayoutSubviewsFirstLoad = YES;
        TopbarViewV2 *topbar = [self getTopBarView];
        BOOL isTopbarShowing = (topbar.isHidden != YES || topbar.alpha > 0.0f);
        
        [self modifyAllViews:isTopbarShowing];
    }
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    //Fix bug content in table is lost apart on first load
    isAlreadyLayoutSubviewsFirstLoad = NO;
    isAlreadyGetGuest = NO;
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    if ([[UserManagerV2 sharedUserManager].currentUserAccessRight isUseAdditionalJobStatusTable]) {
        [btnCleaningStatus setHidden:NO];
    } else {
        [btnCleaningStatus setHidden:YES];
        CGRect frame1 = btnComplete.frame;
        frame1.origin = CGPointMake(btnCleaningStatus.frame.origin.x, btnCleaningStatus.frame.origin.y);
        [btnComplete setFrame:frame1];
        CGRect frame2 = btnTimer.frame;
        frame2.origin = CGPointMake(frame2.origin.x, frame2.origin.y - frame1.size.height);
        [btnTimer setFrame:frame2];
    }
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        [self loadFlatResource];
    }
    isEnglishLang = [[LanguageManagerV2 sharedLanguageManager] currentLanguageIsEqualTo:@"%@", ENGLISH_LANGUAGE];
    addJobManager = [AdditionalJobManagerV2 sharedAddionalJobManager];
    listTaskDetails = [NSMutableArray array];
    
    [tbvContent setBackgroundView:nil];
    [tbvContent setBackgroundColor:[UIColor clearColor]];
    [tbvContent setOpaque:YES];

    [self performSelector:@selector(loadTopbarView)];
    
    [self addBackButton];
    
    //Will auto start after load data done
    isStartOnLoad = YES;
    
    // Load Data
    remarkView = [[RemarkViewV2 alloc] initWithNibName:@"RemarkViewV2" bundle:nil];
    [self loadData];
}



- (void)viewDidUnload {
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Service Later Pop Up

-(void) btnCloseClicked
{
    
}

-(void) btnYesClicked
{
    [cleaningStatusConfirmPopup removeFromSuperview];
    switch (cleaningStatusConfirmPopup.confirmPopupType) {
        case CLEANING_STATUS_CONFIRM_POPUP_SERVICE_LATER:
        {
            NSDateFormatter *_timeFomarter = [[NSDateFormatter alloc] init];
            [_timeFomarter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSString *serviceLaterTime = [_timeFomarter stringFromDate:cleaningStatusConfirmPopup.selectedDate];
            [self updateCurrentAddJobByAddJobStatus:AddJobStatus_ServiceLater serviceLaterTime:serviceLaterTime];
        }
            break;
            
        default:
        {
            
        }
    }
}
#pragma mark - Clenaning Status Pop up delegate

-(void) btnCancelClicked
{
    
}

-(void) btnDNDClicked
{
    [cleaningStatusPopup removeFromSuperview];
    [self updateCurrentAddJobByAddJobStatus:AddJobStatus_DND serviceLaterTime:@""];
}
-(void) btnDoubleLockClicked
{
    [cleaningStatusPopup removeFromSuperview];
    [self updateCurrentAddJobByAddJobStatus:AddJobStatus_DoubleLock serviceLaterTime:@""];
}

-(void) btnServiceLaterClicked
{
    [cleaningStatusPopup removeFromSuperview];
    if (cleaningStatusConfirmPopup == nil) {
        cleaningStatusConfirmPopup = [[CleaningStatusConfirmPopupView alloc] initWithDelegate:self andOption:CLEANING_STATUS_CONFIRM_POPUP_SERVICE_LATER];
    }
    
    [cleaningStatusPopup removeFromSuperview];
    [cleaningStatusConfirmPopup setConfirmPopupType:CLEANING_STATUS_CONFIRM_POPUP_SERVICE_LATER];
    [cleaningStatusConfirmPopup setRoomNo:detailSelected.addjob_detail_room_number];
    
    //[self.tabBarController.view addSubview:cleaningStatusConfirmPopup];
    [self.tabBarController addSubview:cleaningStatusConfirmPopup];
}

-(void) btnDeclinedServiceClicked
{
    [cleaningStatusPopup removeFromSuperview];
    [self updateCurrentAddJobByAddJobStatus:AddJobStatus_DeclinedService serviceLaterTime:@""];
}

-(void) btnPendingClicked
{
    
}

-(void) btnCompleteClicked
{
    
}

#pragma mark - Alert View Delegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == tagConfirmDone)
    {
        //YES
        if(buttonIndex == 0)
        {
            [self completeCurrentAddJob];
        }
    }
    else if(alertView.tag == tagConfirmStop)
    {
        //YES
        if(buttonIndex == 0)
        {
            [self StopCurrentAddJob];
        }
    }
}


#pragma mark - UITableView Delegate Methods

-(int)sectionAddJob
{
    return 0;
}

-(int)sectionTaskDetails
{
    if([listTaskDetails count] <= 0){
        return (int)NSNotFound;
    }
    return 1;
}

-(int)sectionRemark
{
    if([listTaskDetails count] > 0) {
        return 2;
    } else {
        return 1;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(section == [self sectionTaskDetails]) {
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceScreenSizeStandardWidth3_5, 45)];
        NSString *fontType = @"Helvetica-Bold";
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15, -12, DeviceScreenSizeStandardWidth3_5 - 30, 45)];
        [label setTextColor:[UIColor colorWithRed:6.0f/255.0f green:62.0f/255.0f blue:127.0f/255.0f alpha:1]];
        [label setFont:[UIFont fontWithName:fontType size:13]];
        [label setBackgroundColor:[UIColor clearColor]];
        [label setText:[L_task_details currentKeyToLanguage]];
        [headerView addSubview:label];
        return headerView;
    } else  if(section == [self sectionRemark]) {
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceScreenSizeStandardWidth3_5, 45)];
        NSString *fontType = @"Helvetica-Bold";
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15, -12, DeviceScreenSizeStandardWidth3_5 - 30, 45)];
        [label setTextColor:[UIColor colorWithRed:6.0f/255.0f green:62.0f/255.0f blue:127.0f/255.0f alpha:1]];
        [label setFont:[UIFont fontWithName:fontType size:13]];
        [label setBackgroundColor:[UIColor clearColor]];
        [label setText:[L_remark currentKeyToLanguage]];
        [headerView addSubview:label];
        return headerView;
    } else {
        //section Add Job Detail
        return nil;
    }
}

-(CGFloat)tableView:(UITableView*)tableView heightForFooterInSection:(NSInteger)section
{
    return 4.0f;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section == [self sectionAddJob])
    {
        return 5;
    } else {
        //section task details
        return 20;
    }
}

-(CGFloat)tableView:(UITableView *)tableView
    heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return heightAddJobCell;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if([listTaskDetails count] <= 0){
        return 2;
    }
    return 3;
}

-(NSInteger)tableView:(UITableView *)tableView 
    numberOfRowsInSection:(NSInteger)section {
    if(section == [self sectionAddJob]){
        return 1;
    } else if(section == [self sectionRemark]) {
        return 1;
    }else { //Section Task Details
        return [listTaskDetails count];
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentify = @"cellIdentify";
    UITableViewCell *cell = nil;
    
    cell = [tableView dequeueReusableCellWithIdentifier:cellIdentify];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellSelectionStyleNone reuseIdentifier:cellIdentify];
        [cell.textLabel setAdjustsFontSizeToFitWidth:NO];
        [cell.textLabel setLineBreakMode:NSLineBreakByTruncatingTail];
        [cell.textLabel setTextColor:[UIColor colorWithRed:6.0/255.0f green:62.0/255.0f blue:127.0/255.0f alpha:1.0f]];
        [cell setUserInteractionEnabled:NO];
        [cell.textLabel setBackgroundColor:[UIColor clearColor]];
    }
    
    //create cell
    if(indexPath.section == [self sectionAddJob])
    {
        [cell.textLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20]];
        [cell.textLabel setTextAlignment:NSTextAlignmentLeft];
    }
    else if(indexPath.section == [self sectionRemark])
    {
        [cell.textLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20]];
        [cell.textLabel setAdjustsFontSizeToFitWidth:NO];
        [cell.textLabel setNumberOfLines:2];
        [cell.textLabel setLineBreakMode:NSLineBreakByTruncatingTail];
        [cell.textLabel setBackgroundColor:[UIColor clearColor]];
        [cell.textLabel setTextColor:[UIColor grayColor]];
        [cell setUserInteractionEnabled:YES];
    }
    else //Task Details
    {
        [cell.textLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:18]];
        [cell.textLabel setNumberOfLines:2];
    }
    
    if(indexPath.section == [self sectionAddJob])
    {
        [cell.textLabel setText:addJobCategorySelected];
    }
    else if(indexPath.section == [self sectionRemark])
    {
        [cell.textLabel setText:detailSelected.addjob_detail_remark];
    }
    else if(indexPath.section == [self sectionTaskDetails])
    {
        AddJobItemModelV2 *curItem = [listTaskDetails objectAtIndex:indexPath.row];
        if(isEnglishLang){
            [cell.textLabel setText:curItem.addjob_item_name];
        } else {
            [cell.textLabel setText:curItem.addjob_item_name_lang];
        }

        [cell setUserInteractionEnabled:YES];
    }
    
    return cell;
    
}


- (void)addAccessoryButton:(UITableViewCell *)cell  andIndexPath:(NSIndexPath *)indexPath{
    /*****************************************/
    UIButton *accessoryButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 24, 24)];
    [accessoryButton setBackgroundColor:[UIColor clearColor]];
    //    [accessoryButton setImage:[UIImage imageNamed:@"gray_arrow_36x36.png"] forState:UIControlStateNormal];
    [accessoryButton setImage:[UIImage imageNamed:@"gray_arrow_36x36.png"] forState:UIControlStateNormal];
    [accessoryButton setTag:indexPath.row];
    
    [accessoryButton addTarget:self action:@selector(btnDetailPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell setAccessoryView:accessoryButton];
    //cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
    /*****************************************/
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    if(indexPath.section == [self sectionRemark])
    {
        UIView *view = remarkView.view;
        [view setFrame:self.view.bounds];
        [remarkView setDelegate:self];
        [remarkView setTextinRemark:detailSelected.addjob_detail_remark != nil ? detailSelected.addjob_detail_remark : @""];
        remarkView.isViewOnly = NO;
        remarkView.isAddSubview = YES;
        [remarkView viewWillAppear:NO];
        [self.tabBarController.view addSubview:view];
    }else if(indexPath.section == [self sectionTaskDetails]){
        UIView *view = remarkView.view;
        [view setFrame:self.view.bounds];
        AddJobItemModelV2 *curItem = [listTaskDetails objectAtIndex:indexPath.row];
        [remarkView setDelegate:nil];
        [remarkView setTextinRemark:isEnglishLang ? curItem.addjob_item_name : curItem.addjob_item_name_lang];
        remarkView.isViewOnly = YES;
        remarkView.isAddSubview = YES;
        [remarkView viewWillAppear:NO];
        [self.tabBarController.view addSubview:view];
    }
}

-(int)totalHeightOfTableContent
{
    int total = heightAddJobCell;
    total += heightTaskDetailsCell * ([listTaskDetails count] > 0 ? [listTaskDetails count] : 1);
    total += heightFooterTable * numberTableSection;
    total += heightHeaderTable * numberTableSection;
    total += 25;
    
    return total;
}

-(void) hiddenHUDAfterSaved:(MBProgressHUD *)HUD{
    [HUD hide:YES];
    [HUD removeFromSuperview];
    
}

#pragma mark - Delegate Remark View
-(void)remarkViewV2DoneWithText:(NSString *)text andController:(UIViewController *)controllerView{
    detailSelected.addjob_detail_remark = text;
    detailSelected.addjob_detail_post_status = POST_STATUS_SAVED_UNPOSTED;
    [addJobManager updateAddJobDetail:detailSelected];
    [tbvContent reloadData];
}

//implement this for remove remark field with button cancel
-(void)remarkViewV2didDismissWithButtonIndex:(NSInteger)buttonIndez
{
    
}

-(void) remarkViewV2DoneWithText:(NSString *) text
{
    detailSelected.addjob_detail_remark = text;
    detailSelected.addjob_detail_post_status = POST_STATUS_SAVED_UNPOSTED;
    [addJobManager updateAddJobDetail:detailSelected];
    
    //Update for Remark consolidation
    if([[UserManagerV2 sharedUserManager].currentUserAccessRight isRemarkConsolidation]) {
        [[RoomManagerV2 sharedRoomManager] updateRoomRemarksByUserId:[UserManagerV2 sharedUserManager].currentUser.userId hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId remarkType:RemarkType_ConsolidationRemark roomNumber:detailSelected.addjob_detail_room_number remarkValue:text];
    }
    
    [tbvContent reloadData];
}

#pragma mark - Load Data

-(void)loadGuestViews
{
    if(listGuestModels.count > 0) {
        
        listGuestViews = [NSMutableArray array];
        
        //Scroll View
        CGRect guestScrollViewFr = scrGuests.frame;
        CGFloat maxHeightContent = guestScrollViewFr.size.height;
        
        for (int i = 0; i < listGuestModels.count; i ++) {
            GuestInfoModelV2 *curGuest = [listGuestModels objectAtIndex:i];
            
            GuestInfoViewV2 *guestInfoView = [[GuestInfoViewV2 alloc] initWithNibName:@"GuestInfoViewV2" bundle:nil];
            guestInfoView.parentVC = self;
            [guestInfoView setDataModel:curGuest];
            guestInfoView.roomNumber = detailSelected.addjob_detail_room_number;
            //[guestInfoView.view setHidden:YES];
            [listGuestViews addObject:guestInfoView];
            [scrGuests addSubview:guestInfoView.view];
            
            //Set position for Guest View
            CGRect guestInfoViewFr = guestInfoView.view.frame;
            guestInfoViewFr.origin.x = guestScrollViewFr.size.width * i;
            guestInfoViewFr.origin.y = 0;
            [guestInfoView.view setFrame:guestInfoViewFr];
            [guestInfoView reloadView]; //Load Guest Profile Note
            
            //Find max height contain for scroll view
            if(guestInfoViewFr.size.height > maxHeightContent) {
                maxHeightContent = guestInfoViewFr.size.height;
            }
        }
        
        //Change content size for Scrollview
        [scrGuests setContentSize:CGSizeMake(scrGuests.frame.size.width * listGuestModels.count, maxHeightContent)];
        
        //Set up indicators highlight
        if(listGuestModels.count > 1) {
            guestIndicatorLeft = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 5, self.view.frame.size.height)];
            [guestIndicatorLeft setBackgroundColor:[UIColor clearColor]];
            [guestIndicatorLeft setHidden:YES];
            [guestIndicatorLeft setImage:[UIImage imageNamed:imgMenuLeft]];
            
            guestIndicatorRight = [[UIImageView alloc] initWithFrame:CGRectMake(scrContent.frame.size.width - 5, 0, 5, self.view.frame.size.height)];
            [guestIndicatorRight setImage:[UIImage imageNamed:imgMenuRight]];
            [guestIndicatorRight setBackgroundColor:[UIColor clearColor]];
            [guestIndicatorRight setHidden:YES];
            
            [self.view addSubview:guestIndicatorLeft];
            [self.view addSubview:guestIndicatorRight];
        }
    }
}

-(void) loadGuestData
{
    NSMutableArray *listCurrentGuests = [[RoomManagerV2 sharedRoomManager] loadGuestsByUserId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId roomNumber:roomSelected.addjob_room_number guestType:GuestTypeCurrent];
    NSMutableArray *listArrivalGuests = [[RoomManagerV2 sharedRoomManager] loadGuestsByUserId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId roomNumber:roomSelected.addjob_room_number guestType:GuestTypeArrival];
    
    listGuestModels = [NSMutableArray array];
    [listGuestModels addObjectsFromArray:listCurrentGuests];
    [listGuestModels addObjectsFromArray:listArrivalGuests];
}

-(void)loadData {
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getLoadingData]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES];
    [[HomeViewV2 shareHomeView] waitingLoadingData];
    
    //Load data here
    [listTaskDetails removeAllObjects];
    
    int userId = [UserManagerV2 sharedUserManager].currentUser.userId;
    [addJobManager loadWSAddJobDetailByUserId:userId ondayAddJobId:(int)detailSelected.addjob_detail_onday_addjob_id];
    
    //AddJobCategory
    AddJobCategoryModelV2 *addJobCategory = [addJobManager loadAddJobCategoryByUserId:userId categoryId:(int)detailSelected.addjob_detail_category_id];
    
    if(isEnglishLang){
        addJobCategorySelected = addJobCategory.addjob_category_name;
    } else {
        addJobCategorySelected = addJobCategory.addjob_category_name_lang;
    }
    
    //Reload local database Additional Job Detail
    AddJobDetailModelV2 *newDetail = [addJobManager loadAddJobDetailByUserId:userId ondayAddJobId:(int)detailSelected.addjob_detail_onday_addjob_id];
    if(newDetail){
        detailSelected = newDetail;
    }
    
    //For showing room remark at the same as Room Detail
    if([[UserManagerV2 sharedUserManager].currentUserAccessRight isRemarkConsolidation]) {
        NSString *remarkWS = [[RoomManagerV2 sharedRoomManager] getRoomRemarksByUserId:[UserManagerV2 sharedUserManager].currentUser.userId hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId remarkType:RemarkType_ConsolidationRemark roomNumber:roomSelected.addjob_room_number];
        [[UserManagerV2 sharedUserManager] setCommontUserConfig:[NSString stringWithFormat:@"%@%@", REMARK_CONSOLIDATE, roomSelected.addjob_room_number] value:remarkWS.length > 0 ? remarkWS : @""];
        detailSelected.addjob_detail_remark = remarkWS;
    }
    //Load Additional Job Task
    
    if(isDemoMode){
        NSMutableArray *listtemp = [addJobManager loadAllAddJobItemsByUserId:userId ondayAddJobId:(int)detailSelected.addjob_detail_onday_addjob_id];
        listTaskDetails = [[NSMutableArray alloc] init];
        NSMutableSet *listUnique = [[NSMutableSet alloc] init];
        for(AddJobItemModelV2 *itemModel in listtemp){
            if(![listUnique containsObject:[NSString stringWithFormat:@"%d", (int)itemModel.addjob_item_id]]){
                [listTaskDetails addObject:itemModel];
            }
            [listUnique addObject:[NSString stringWithFormat:@"%d", (int)itemModel.addjob_item_id]];
        }
    }
    
    else{
        listTaskDetails = [addJobManager loadAllAddJobItemsByUserId:userId ondayAddJobId:(int)detailSelected.addjob_detail_onday_addjob_id];
    }
    
    timerStatus = TIMER_STATUS_NOT_START;
    timeLeft = detailSelected.addjob_detail_expected_cleaning_time * 60 - detailSelected.addjob_detail_total_time ;
    [btnTimer setTitle:[self countTimeSpendForTask:timeLeft option:YES] forState:UIControlStateNormal];
    
    [HUD hide:YES];
    [HUD removeFromSuperview];
    
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
    [tbvContent reloadData];
    
    //Start onload
    if(isStartOnLoad){
        isStartOnLoad = NO;
        [self StartCurrentAddJob];
    }
}

-(void)populateSupervisorData
{
    if(timerStatus == TIMER_STATUS_NOT_START) {
        [self setEnableBtnComplete:NO];
        [btnStartStop setBackgroundImage:[UIImage imageBeforeiOS7:imgStartBtn equaliOS7:imgStartBtnFlat] forState:UIControlStateNormal];
        [btnStartStop setBackgroundImage:[UIImage imageBeforeiOS7:imgStartBtnClicked equaliOS7:imgStartBtnClickedFlat] forState:UIControlEventTouchDown];
    } else if(timerStatus == TIMER_STATUS_PAUSE){
        [self setEnableBtnComplete:NO];
        [btnStartStop setBackgroundImage:[UIImage imageBeforeiOS7:imgStartBtn equaliOS7:imgStartBtnFlat] forState:UIControlStateNormal];
        [btnStartStop setBackgroundImage:[UIImage imageBeforeiOS7:imgStartBtn equaliOS7:imgStartBtnFlat] forState:UIControlEventTouchDown];
    } else if (timerStatus == TIMER_STATUS_START){
        [self setEnableBtnComplete:YES];
        [btnStartStop setBackgroundImage:[UIImage imageBeforeiOS7:imgStopBtn equaliOS7:imgStopBtnFlat] forState:UIControlStateNormal];
        [btnStartStop setBackgroundImage:[UIImage imageBeforeiOS7:imgStopBtnClicked equaliOS7:imgStopBtnClickedFlat] forState:UIControlEventTouchDown];
    } else { //Timer finish
        
    }
}

-(void)populateMaidData
{
    if(timerStatus == TIMER_STATUS_NOT_START) {
        [self setEnableBtnComplete:NO];
        [btnStartStop setBackgroundImage:[UIImage imageBeforeiOS7:imgStartBtn equaliOS7:imgStartBtnFlat] forState:UIControlStateNormal];
        [btnStartStop setBackgroundImage:[UIImage imageBeforeiOS7:imgStartBtnClicked equaliOS7:imgStartBtnClickedFlat] forState:UIControlEventTouchDown];
    } else if(timerStatus == TIMER_STATUS_PAUSE){
        [self setEnableBtnComplete:NO];
        [btnStartStop setBackgroundImage:[UIImage imageBeforeiOS7:imgStartBtn equaliOS7:imgStartBtnFlat] forState:UIControlStateNormal];
        [btnStartStop setBackgroundImage:[UIImage imageBeforeiOS7:imgStartBtn equaliOS7:imgStartBtnFlat] forState:UIControlEventTouchDown];
    } else if (timerStatus == TIMER_STATUS_START){
        [self setEnableBtnComplete:YES];
        [btnStartStop setBackgroundImage:[UIImage imageBeforeiOS7:imgStopBtn equaliOS7:imgStopBtnFlat] forState:UIControlStateNormal];
        [btnStartStop setBackgroundImage:[UIImage imageBeforeiOS7:imgStopBtnClicked equaliOS7:imgStopBtnClickedFlat] forState:UIControlEventTouchDown];
    } else { //Timer finish
        
    }
}

//For binding data
-(void)populateData
{
    //MARK: Hao Tran - kind of room (Queue room, rush room) is not apply in current
    /*
    NSInteger kindOfRoom = [(NSString *)[roomAssignment objectForKey:kRaKindOfRoom] integerValue];
    if (kindOfRoom == ENUM_KIND_OF_ROOM_RUSH) {
        [imgRushQueueRoom setHidden:NO];
        [imgRushQueueRoom setImage:[UIImage imageNamed:@"icon_rush room.png"]];
    }
    else if (kindOfRoom == ENUM_KIND_OF_ROOM_QUEUE) {
        [imgRushQueueRoom setHidden:NO];
        [imgRushQueueRoom setImage:[UIImage imageNamed:@"icon_queue room.png"]];
    }
    else if (kindOfRoom == ENUM_KIND_OF_ROOM_NORMAL) {
        [imgRushQueueRoom setHidden:YES];
    }
    else {
        
    }
     */
    
    RoomStatusModelV2 *roomStatus = [[RoomStatusModelV2 alloc] init];
    roomStatus.rstat_Id = (int)roomSelected.addjob_room_status_id;
    roomStatus = [[RoomManagerV2 sharedRoomManager] loadRoomStatusData:roomStatus];
    
    if(roomStatus.rstat_Image){
        NSData *imageData = roomStatus.rstat_Image;
        UIImage *imageRoomStatus = [UIImage imageWithData:imageData];
        [imgRoomStatus setImage:imageRoomStatus];
    }
    
    NSString *roomStatusCode = nil;
    if(isEnglishLang){
        roomStatusCode = roomStatus.rstat_Name;
    } else {
        roomStatusCode = roomStatus.rstat_Lang;
    }
    
    if(roomStatusCode.length <= 0){ //No have room status code
        roomStatusCode = [RoomManagerV2 getRoomStatusCodeByEnum:(int)roomSelected.addjob_room_status_id];
    }
    
    [lblRoomStatus setText:roomStatusCode];
    [lblRoomStatus setTextColor:[self colorForLabel:(int)roomSelected.addjob_room_status_id]];
    
    /*
    if (roomSelected.addjob_room_status_id == ENUM_ROOM_STATUS_VD
        || roomSelected.addjob_room_status_id == ENUM_ROOM_STATUS_VC
        || roomSelected.addjob_room_status_id == ENUM_ROOM_STATUS_VCB
        || roomSelected.addjob_room_status_id == ENUM_ROOM_STATUS_VI
        || roomSelected.addjob_room_status_id == ENUM_ROOM_STATUS_VPU) {
        [btnGuestInfo setUserInteractionEnabled:NO];
        [btnGuestInfo setBackgroundImage:[UIImage imageNamed:imgGuestInfoInactive] forState:UIControlStateNormal];
    }
    else if (roomSelected.addjob_room_status_id == ENUM_ROOM_STATUS_OC
             || roomSelected.addjob_room_status_id == ENUM_ROOM_STATUS_OD
             || roomSelected.addjob_room_status_id == ENUM_ROOM_STATUS_OI
             || roomSelected.addjob_room_status_id == ENUM_ROOM_STATUS_OOO
             || roomSelected.addjob_room_status_id == ENUM_ROOM_STATUS_OOS
             || roomSelected.addjob_room_status_id == ENUM_ROOM_STATUS_OPU
             || roomSelected.addjob_room_status_id == ENUM_ROOM_STATUS_OC_SO) {
        [btnGuestInfo setUserInteractionEnabled:YES];
        [btnGuestInfo setBackgroundImage:[UIImage imageNamed:imgGuestInfoActive] forState:UIControlStateNormal];
        
    }*/
    int result = [[RoomManagerV2 sharedRoomManager] updateGuestInfoByUserId:[UserManagerV2 sharedUserManager].currentUser.userId hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId roomNumber:roomSelected.addjob_room_number];
    if(result == RESPONSE_STATUS_OK){
        isAlreadyGetGuest = YES;
        [self loadGuestData];
    }
    if([[UserManagerV2 sharedUserManager].currentUserAccessRight isEnableGuestInfo] && listGuestModels.count > 0){
        [btnGuestInfo setUserInteractionEnabled:YES];
        [btnGuestInfo setBackgroundImage:[UIImage imageBeforeiOS7:imgGuestInfoActive equaliOS7:imgGuestInfoActiveFlat] forState:UIControlStateNormal];
    } else {
        [btnGuestInfo setUserInteractionEnabled:NO];
        [btnGuestInfo setBackgroundImage:[UIImage imageBeforeiOS7:imgGuestInfoInactive equaliOS7:imgGuestInfoInactiveFlat] forState:UIControlStateNormal];
    }
    
    if (timeLeft < 0) {
        [btnTimer setBackgroundImage:[UIImage imageBeforeiOS7:imgTimerOver equaliOS7:imgTimerOverFlat] forState:UIControlStateNormal];
    }
    else {
        if (detailSelected.addjob_detail_status == AddJobStatus_Pause) {
            [btnTimer setBackgroundImage:[UIImage imageBeforeiOS7:imgTimerPause equaliOS7:imgTimerPauseFlat] forState:UIControlStateNormal];
        }
        else if (detailSelected.addjob_detail_status == AddJobStatus_Started){
            [btnTimer setBackgroundImage:[UIImage imageBeforeiOS7:imgTimerPlay equaliOS7:imgTimerPlayFlat] forState:UIControlStateNormal];
        }
        else {
            [btnTimer setBackgroundImage:[UIImage imageBeforeiOS7:imgTimerNotStart equaliOS7:imgTimerNotStartFlat] forState:UIControlStateNormal];
        }
    }
    
    if ([UserManagerV2 isSupervisor])
    {
        [self populateSupervisorData];
    }
    else
    {
        [self populateMaidData];
    }
}
#pragma mark - View Interaction

-(IBAction) btnCleaningStatusClicked:(id)sender
{
    if (cleaningStatusPopup == nil) {        
        cleaningStatusPopup = [[CleaningStatusPopupView alloc] initWithDelegate:self andOption:(isPushFrom == IS_PUSHED_FROM_FIND_BY_RA || isPushFrom == IS_PUSHED_FROM_FIND_BY_ROOM)];
    }
    
    [cleaningStatusPopup setIsPushedFromAddJobDetail: YES];
    [cleaningStatusPopup setRoomNo:detailSelected.addjob_detail_room_number];
    [self.tabBarController addSubview:cleaningStatusPopup];
}

-(IBAction)btnStartStopClicked:(id)sender
{
    //Current Timer Status
    switch (timerStatus) {
        case TIMER_STATUS_PAUSE:
        {
            [self StartCurrentAddJob];
            
        }
            break;
            
        case TIMER_STATUS_START:
        {
            UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_are_you_sure_to_stop_this_additional_job] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles:[[LanguageManagerV2 sharedLanguageManager] getNo], nil];
            alert.delegate = self;
            [alert setTag:tagConfirmStop];
            [alert show];
        }
            break;
            
        case TIMER_STATUS_NOT_START:
        {
            [self StartCurrentAddJob];
        }
            
            break;
            
        default:
            break;
    }
    
}

-(IBAction)btnCompleteClicked:(id)sender
{
    UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_complete_this_additional_job] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getYes] otherButtonTitles:[[LanguageManagerV2 sharedLanguageManager] getNo], nil];
    alert.delegate = self;
    [alert setTag:tagConfirmDone];
    [alert show];
}

-(IBAction)btnRoomAssignmentInfo:(id)sender
{
    if (listGuestModels.count > 0) {
        
        [viewDetail setHidden:NO];
        [tbvContent setHidden:NO];
        [scrContent setHidden:NO];
        [scrGuests setHidden:YES];
        [guestIndicatorLeft setHidden:YES];
        [guestIndicatorRight setHidden:YES];
        TopbarViewV2 *topbar = [self getTopBarView];
        BOOL isTopbarShowing = (topbar.isHidden != YES || topbar.alpha > 0.0f);
        [self modifyAllViews:isTopbarShowing];
    }
}

-(IBAction)btnGuestInfoClicked:(id)sender
{
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getLoadingData]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES];
    [[HomeViewV2 shareHomeView] waitingLoadingData];
    
    if(isAlreadyGetGuest  && [[UserManagerV2 sharedUserManager].currentUserAccessRight isEnableGuestInfo]){
        //bool isSuccess = [addJobManager loadWSAddJobGuestInfoByUserId:[UserManagerV2 sharedUserManager].currentUser.userId roomId:roomSelected.addjob_room_id];
//        int result = [[RoomManagerV2 sharedRoomManager] updateGuestInfoByUserId:[UserManagerV2 sharedUserManager].currentUser.userId hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId roomNumber:roomSelected.addjob_room_number];
//        if(result == RESPONSE_STATUS_OK){
//            isAlreadyGetGuest = YES;
//            [self loadGuestData];
            [self loadGuestViews];
//        }
    }
    
    if(listGuestModels.count <= 0) { //Don't have Guest Info
        [self performSelector:@selector(hiddenHUDAfterSaved:) withObject:HUD afterDelay:0.0f];
        [[HomeViewV2 shareHomeView] endWaitingLoadingData];
        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:@"" message:[L_guest_do_not_exist currentKeyToLanguage] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getOK] otherButtonTitles: nil];
        [alert show];
        return; //break
    }
    
    [viewDetail setHidden:YES];
    [tbvContent setHidden:YES];
    [scrContent setHidden:YES];
    [scrGuests setHidden:NO];
    
    //Check and show indicators
    [self scrollViewDidScroll:scrGuests];
    
    [self performSelector:@selector(hiddenHUDAfterSaved:) withObject:HUD afterDelay:0.0f];
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
    
}

#pragma mark -

-(void)completeCurrentAddJob
{
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getLoadingData]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES];
    [[HomeViewV2 shareHomeView] waitingLoadingData];
    
    //Complete Addition Job For Maid
    //if(![UserManagerV2 isSupervisor])
    //{
    
    NSString *strCurrentTime = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
    detailSelected.addjob_detail_stop_time = strCurrentTime;
    if ([[UserManagerV2 sharedUserManager].currentUserAccessRight isUseAdditionalJobStatusTable]) {
        detailSelected.addjob_detail_status = AddJobStatus_Complete;
    } else {
        detailSelected.addjob_detail_status = 1;
    }
    detailSelected.addjob_detail_last_modified = strCurrentTime;
    
    //Post status is not apply for addjob detail
    detailSelected.addjob_detail_post_status = POST_STATUS_SAVED_UNPOSTED; //This line is not effect to project
    
    //Total Time is sum of duration
    int totalTime = (int)(detailSelected.addjob_detail_total_time + duration);
    detailSelected.addjob_detail_total_time = totalTime;
    [addJobManager updateAddJobDetail:detailSelected];
    [self changeTimerStatus:TIMER_STATUS_FINISH];
    
    //PART 1: POST REMARK
    //Post addtional job remark
    bool isPostRemarkSucess = NO;
    if(isDemoMode){
        isPostRemarkSucess = YES;
    }
    else{
        isPostRemarkSucess = [addJobManager postAddJobRemarkWithUserId:[UserManagerV2 sharedUserManager].currentUser.userId ondayAddJobId:(int)detailSelected.addjob_detail_onday_addjob_id remark:detailSelected.addjob_detail_remark];
    }
    if(isPostRemarkSucess) {
        detailSelected.addjob_detail_post_status = POST_STATUS_POSTED;
        [addJobManager updateAddJobDetail:detailSelected];
    }
    
    //PART 2: POST COMPLETE
    //Post complete add job
    bool isPostSucess = NO;
    if(isDemoMode){
        isPostSucess = YES;
    }
    else{
        isPostSucess = [addJobManager postAddJobCompleteWithUserId:[UserManagerV2 sharedUserManager].currentUser.userId ondayAddJobId:(int)detailSelected.addjob_detail_onday_addjob_id time:strCurrentTime];
    }
    
    //Manage post data for Additional Job Detail
    AddJobDetailRecordModelV2 *detailRecord = [[AddJobDetailRecordModelV2 alloc] init];
    detailRecord.addjob_detail_record_last_modified = strCurrentTime;
    detailRecord.addjob_detail_record_onday_addjob_id = detailSelected.addjob_detail_onday_addjob_id;
    if ([[UserManagerV2 sharedUserManager].currentUserAccessRight isUseAdditionalJobStatusTable]) {
        detailRecord.addjob_detail_record_operation = AddJobStatus_Complete;
    }   else {
        detailRecord.addjob_detail_record_operation = 1;
    }
    detailRecord.addjob_detail_record_post_status = POST_STATUS_SAVED_UNPOSTED;
    detailRecord.addjob_detail_record_time = strCurrentTime;
    detailRecord.addjob_detail_record_user_id = [UserManagerV2 sharedUserManager].currentUser.userId;
    
    //records
    if(isPostSucess){
        detailRecord.addjob_detail_record_post_status = POST_STATUS_POSTED;
        
        [addJobManager deleteAddJobSearchRoomCategoryByUserId:[UserManagerV2 sharedUserManager].currentUser.userId roomId:(int)detailSelected.addjob_detail_room_id categoryId:(int)detailSelected.addjob_detail_category_id];
        NSArray *listVC = self.navigationController.viewControllers;
        for (UIViewController *curVC in listVC) {
            if([curVC isKindOfClass:[AdditionalJobListByFloorViewV2 class]]){
                AdditionalJobListByFloorViewV2 *addJobFloorVC = (AdditionalJobListByFloorViewV2*)curVC;
                addJobFloorVC.isReloadDataOnShow = YES;//Reload data for complete pending additional Job
            }
        }
    }
    
    [addJobManager insertAddJobDetailRecord:detailRecord];
    roomSelected.addjob_room_number_pending_job -= 1;
    if(roomSelected.addjob_room_number_pending_job < 0){
        roomSelected.addjob_room_number_pending_job = 0;
    }
    
    //Because of Room Assignment don't use AddJobRoomModel to store local database
    if(isPushFrom != IS_PUSHED_FROM_ROOM_ASSIGNMENT){
        if(!isDemoMode){
            [addJobManager updateAddJobRoom:roomSelected];
        }
    }
    
    if(isDemoMode){
        AddJobRoomModelV2 *addJobRoomModel = [addJobManager loadAddJobRoomByUserId:(int)roomSelected.addjob_room_user_id roomId:(int)roomSelected.addjob_room_id roomAssignId:(int)roomSelected.addjob_room_assign_id searchId:0];
        addJobRoomModel.addjob_room_number_pending_job -= 1;
        [addJobManager updateAddJobRoom:addJobRoomModel];
        [addJobManager updateAddJobRoom:roomSelected];
        
        if(roomSelected.addjob_room_number_pending_job == 0){
            [addJobManager deleteAddJobRoomByUserId:(int)roomSelected.addjob_room_user_id roomAssignId:(int)roomSelected.addjob_room_assign_id searchId:(int)roomSelected.addjob_room_search_id];
        }
        if(addJobRoomModel.addjob_room_number_pending_job == 0){
            [addJobManager deleteAddJobRoomByUserId:(int)roomSelected.addjob_room_user_id roomAssignId:(int)roomSelected.addjob_room_assign_id searchId:0];
        }
        
        AddJobFloorModelV2 *floorModel = [addJobManager loadAddJobFloorByUserId:(int)roomSelected.addjob_room_user_id floorId:(int)roomSelected.addjob_room_floor_id searchId:0];
        floorModel.addjob_floor_number_of_job -= 1;
        AddJobFloorModelV2 *floorModelSearch = [addJobManager loadAddJobFloorByUserId:(int)roomSelected.addjob_room_user_id floorId:(int)roomSelected.addjob_room_floor_id searchId:(int)roomSelected.addjob_room_search_id];
        floorModelSearch.addjob_floor_number_of_job -= 1;
        [addJobManager updateAddJobFloor:floorModel];
        [addJobManager updateAddJobFloor:floorModelSearch];
        
        if(floorModelSearch.addjob_floor_number_of_job == 0){
            [addJobManager deleteAddJobFloorsByAddJobFloorModel:floorModelSearch];
            //[addJobManager deleteAddJobSearchBySearchId:roomSelected.addjob_room_search_id];
        }
        if(floorModel.addjob_floor_number_of_job == 0){
            [addJobManager deleteAddJobFloorsByAddJobFloorModel:floorModel];
        }
    }
    
    [HUD hide:YES];
    [HUD removeFromSuperview];
    
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
    
    [self backBarPressed];
    
    //}
}

-(void)updateCurrentAddJobByAddJobStatus:(int)addJobStatus serviceLaterTime:(NSString*)serviceLaterTime
{
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getLoadingData]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES];
    [[HomeViewV2 shareHomeView] waitingLoadingData];
    
    //Complete Addition Job For Maid
    //if(![UserManagerV2 isSupervisor])
    //{
    
    if (addJobStatus == AddJobStatus_ServiceLater) {
        detailSelected.addjob_detail_assigned_date = serviceLaterTime;
    }
    
    NSString *strCurrentTime = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
    detailSelected.addjob_detail_stop_time = strCurrentTime;
    detailSelected.addjob_detail_status = addJobStatus;
    detailSelected.addjob_detail_last_modified = strCurrentTime;
    
    //Post status is not apply for addjob detail
    detailSelected.addjob_detail_post_status = POST_STATUS_SAVED_UNPOSTED; //This line is not effect to project
    
    //Total Time is sum of duration
    int totalTime = (int)(detailSelected.addjob_detail_total_time + duration);
    detailSelected.addjob_detail_total_time = totalTime;
    [addJobManager updateAddJobDetail:detailSelected];
    [self changeTimerStatus:TIMER_STATUS_FINISH];
    
    //PART 1: POST REMARK
    //Post addtional job remark
    bool isPostRemarkSucess = NO;
    if(isDemoMode){
        isPostRemarkSucess = YES;
    }
    else{
        isPostRemarkSucess = [addJobManager postAddJobRemarkWithUserId:[UserManagerV2 sharedUserManager].currentUser.userId ondayAddJobId:(int)detailSelected.addjob_detail_onday_addjob_id remark:detailSelected.addjob_detail_remark];
    }
    
    if(isPostRemarkSucess) {
        detailSelected.addjob_detail_post_status = POST_STATUS_POSTED;
        [addJobManager updateAddJobDetail:detailSelected];
    }
    
    //PART 2: POST COMPLETE
    //Post complete add job
    bool isPostSucess = NO;
    if(isDemoMode){
        isPostSucess = YES;
    }
    else{
        
        if (addJobStatus == AddJobStatus_ServiceLater) {
            isPostSucess = [addJobManager postAddJobStatusByUserId:[UserManagerV2 sharedUserManager].currentUser.userId ondayAddJobId:(int)detailSelected.addjob_detail_onday_addjob_id addJobStatus:addJobStatus serviceLaterDateTime:serviceLaterTime];
        } else {
            isPostSucess = [addJobManager postAddJobStatusByUserId:[UserManagerV2 sharedUserManager].currentUser.userId ondayAddJobId:(int)detailSelected.addjob_detail_onday_addjob_id addJobStatus:addJobStatus serviceLaterDateTime:@""];
        }
        //isPostSucess = [addJobManager postAddJobCompleteWithUserId:[UserManagerV2 sharedUserManager].currentUser.userId ondayAddJobId:detailSelected.addjob_detail_onday_addjob_id time:strCurrentTime];
    }
    
    //Manage post data for Additional Job Detail
    AddJobDetailRecordModelV2 *detailRecord = [[AddJobDetailRecordModelV2 alloc] init];
    detailRecord.addjob_detail_record_last_modified = strCurrentTime;
    detailRecord.addjob_detail_record_onday_addjob_id = detailSelected.addjob_detail_onday_addjob_id;
    detailRecord.addjob_detail_record_operation = addJobStatus;
    detailRecord.addjob_detail_record_post_status = POST_STATUS_SAVED_UNPOSTED;
    
    if (addJobStatus == AddJobStatus_ServiceLater) {
        detailRecord.addjob_detail_record_time = serviceLaterTime; //User record time as service later time
    } else {
        detailRecord.addjob_detail_record_time = strCurrentTime;
    }
    detailRecord.addjob_detail_record_user_id = [UserManagerV2 sharedUserManager].currentUser.userId;
    
    //records
    if(isPostSucess){
        detailRecord.addjob_detail_record_post_status = POST_STATUS_POSTED;
        
        [addJobManager deleteAddJobSearchRoomCategoryByUserId:[UserManagerV2 sharedUserManager].currentUser.userId roomId:(int)detailSelected.addjob_detail_room_id categoryId:(int)detailSelected.addjob_detail_category_id];
        NSArray *listVC = self.navigationController.viewControllers;
        for (UIViewController *curVC in listVC) {
            if([curVC isKindOfClass:[AdditionalJobListByFloorViewV2 class]]){
                AdditionalJobListByFloorViewV2 *addJobFloorVC = (AdditionalJobListByFloorViewV2*)curVC;
                addJobFloorVC.isReloadDataOnShow = YES;//Reload data for complete pending additional Job
            }
        }
    }
    
    [addJobManager insertAddJobDetailRecord:detailRecord];
    roomSelected.addjob_room_number_pending_job -= 1;
    if(roomSelected.addjob_room_number_pending_job < 0){
        roomSelected.addjob_room_number_pending_job = 0;
    }
    
    //Because of Room Assignment don't use AddJobRoomModel to store local database
    if(isPushFrom != IS_PUSHED_FROM_ROOM_ASSIGNMENT){
        if(!isDemoMode){
            [addJobManager updateAddJobRoom:roomSelected];
        }
    }
    
    [HUD hide:YES];
    [HUD removeFromSuperview];
    
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
    
    [self backBarPressed];
    
    //}
}

-(void)StopCurrentAddJob
{
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getLoadingData]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES];
    [[HomeViewV2 shareHomeView] waitingLoadingData];
    
    //Complete Addition Job For Maid
    //if(![UserManagerV2 isSupervisor])
    //{
    
    NSString *strCurrentTime = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
    detailSelected.addjob_detail_stop_time = strCurrentTime;
    if ([[UserManagerV2 sharedUserManager].currentUserAccessRight isUseAdditionalJobStatusTable]) {
        detailSelected.addjob_detail_status = AddJobStatus_Pause;
    } else {
        detailSelected.addjob_detail_status = 0;
    }
    detailSelected.addjob_detail_last_modified = strCurrentTime;

    //Post status is not apply for addjob detail
    detailSelected.addjob_detail_post_status = POST_STATUS_SAVED_UNPOSTED; //This line is not effect to project
    
    //Total Time is sum of duration
    int totalTime = (int)(detailSelected.addjob_detail_total_time + duration);
    detailSelected.addjob_detail_total_time = totalTime;
    
    [addJobManager updateAddJobDetail:detailSelected];
    [self changeTimerStatus:TIMER_STATUS_PAUSE];
    
    //Post stop add job
    bool isPostSucess = NO;
    if(isDemoMode){
        isPostSucess = YES;
    }
    else{
        isPostSucess = [addJobManager postAddJobStopWithUserId:[UserManagerV2 sharedUserManager].currentUser.userId ondayAddJobId:(int)detailSelected.addjob_detail_onday_addjob_id time:strCurrentTime];
    }
    
    
    //Post addtional job remark
    bool isPostRemarkSucess = NO;
    if(isDemoMode){
        isPostRemarkSucess = YES;
    }
    else{
        isPostRemarkSucess = [addJobManager postAddJobRemarkWithUserId:[UserManagerV2 sharedUserManager].currentUser.userId ondayAddJobId:(int)detailSelected.addjob_detail_onday_addjob_id remark:detailSelected.addjob_detail_remark];
    }
    
    if(isPostRemarkSucess) {
        detailSelected.addjob_detail_post_status = POST_STATUS_POSTED;
        [addJobManager updateAddJobDetail:detailSelected];
    }
    
    //Manage post data for Additional Job Detail
    AddJobDetailRecordModelV2 *detailRecord = [[AddJobDetailRecordModelV2 alloc] init];
    detailRecord.addjob_detail_record_last_modified = strCurrentTime;
    detailRecord.addjob_detail_record_onday_addjob_id = detailSelected.addjob_detail_onday_addjob_id;
    if ([[UserManagerV2 sharedUserManager].currentUserAccessRight isUseAdditionalJobStatusTable]) {
        detailRecord.addjob_detail_record_operation = AddJobStatus_Pause;
    } else {
        detailRecord.addjob_detail_record_operation = 0;
    }
    detailRecord.addjob_detail_record_post_status = POST_STATUS_SAVED_UNPOSTED;
    detailRecord.addjob_detail_record_time = strCurrentTime;
    detailRecord.addjob_detail_record_user_id = [UserManagerV2 sharedUserManager].currentUser.userId;
    //records
    if(isPostSucess){
        detailRecord.addjob_detail_record_post_status = POST_STATUS_POSTED;
    }
    [addJobManager insertAddJobDetailRecord:detailRecord];
    
    [HUD hide:YES];
    [HUD removeFromSuperview];
    
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
    
    [self backBarPressed];
    
    //}
}

-(void)StartCurrentAddJob
{
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getLoadingData]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES];
    [[HomeViewV2 shareHomeView] waitingLoadingData];
    
    //Complete Addition Job For Maid
    //if(![UserManagerV2 isSupervisor])
    //{
    
    if (timeLeft < 0) {
        [btnTimer setBackgroundImage:[UIImage imageBeforeiOS7:imgTimerOver equaliOS7:imgTimerOverFlat] forState:UIControlStateNormal];
    }
    else {
        [btnTimer setBackgroundImage:[UIImage imageBeforeiOS7:imgTimerPlay equaliOS7:imgTimerPlayFlat] forState:UIControlStateNormal];
    }
    
    [btnStartStop setBackgroundImage:[UIImage imageBeforeiOS7:imgStopBtn equaliOS7:imgStopBtnFlat] forState:UIControlStateNormal];
    [btnStartStop setBackgroundImage:[UIImage imageBeforeiOS7:imgStopBtnClicked equaliOS7:imgStopBtnClickedFlat] forState:UIControlEventTouchDown];
    
    timeLeft = detailSelected.addjob_detail_expected_cleaning_time * 60 - detailSelected.addjob_detail_total_time;
    
    NSString *strCurrentTime = [ehkConvert GetDateTimeNowWithFormat:@"yyyy-MM-dd HH:mm:ss"];
    detailSelected.addjob_detail_start_time = strCurrentTime;
    if ([[UserManagerV2 sharedUserManager].currentUserAccessRight isUseAdditionalJobStatusTable]) {
        detailSelected.addjob_detail_status = AddJobStatus_Started;
    } else {
        detailSelected.addjob_detail_status = 0;
    }
    detailSelected.addjob_detail_last_modified = strCurrentTime;
    
    //Post status is not apply for addjob detail
    detailSelected.addjob_detail_post_status = POST_STATUS_SAVED_UNPOSTED; //This line is not effect to project
    
    //Total Time is sum of duration
    int totalTime = (int)(detailSelected.addjob_detail_total_time + duration);
    detailSelected.addjob_detail_total_time = totalTime;
    
    [addJobManager updateAddJobDetail:detailSelected];
    
    //Post start add job
    bool isPostSucess = NO;
    if(isDemoMode){
        isPostSucess = YES;
    }
    else{
        isPostSucess = [addJobManager postAddJobStartWithUserId:[UserManagerV2 sharedUserManager].currentUser.userId ondayAddJobId:(int)detailSelected.addjob_detail_onday_addjob_id time:strCurrentTime];
    }
    
    //Manage post data for Additional Job Detail
    AddJobDetailRecordModelV2 *detailRecord = [[AddJobDetailRecordModelV2 alloc] init];
    detailRecord.addjob_detail_record_last_modified = strCurrentTime;
    detailRecord.addjob_detail_record_onday_addjob_id = detailSelected.addjob_detail_onday_addjob_id;
    if ([[UserManagerV2 sharedUserManager].currentUserAccessRight isUseAdditionalJobStatusTable]) {
        detailRecord.addjob_detail_record_operation = AddJobStatus_Started;
    } else {
        detailRecord.addjob_detail_record_operation = 0;
    }
    detailRecord.addjob_detail_record_post_status = POST_STATUS_SAVED_UNPOSTED;
    detailRecord.addjob_detail_record_time = strCurrentTime;
    detailRecord.addjob_detail_record_user_id = [UserManagerV2 sharedUserManager].currentUser.userId;
    //records 
    if(isPostSucess){
        detailSelected.addjob_detail_post_status = POST_STATUS_POSTED;
        [addJobManager updateAddJobDetail:detailSelected];
        detailRecord.addjob_detail_record_post_status = POST_STATUS_POSTED;
    }
    [addJobManager insertAddJobDetailRecord:detailRecord];
    
    [self changeTimerStatus:TIMER_STATUS_START];
    [self setEnableBtnComplete:YES];
    
    [HUD hide:YES];
    [HUD removeFromSuperview];
    
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
    
    //}
}



-(void) setEnableBtnComplete:(BOOL) isEnable
{
    [btnComplete setUserInteractionEnabled:isEnable];
    [btnComplete setBackgroundImage:[UIImage imageBeforeiOS7:(isEnable?imgCompleteBtn:imgCompleteBtnDisable) equaliOS7:(isEnable?imgCompleteBtnFlat:imgCompleteBtnDisableFlat)] forState:UIControlStateNormal];
    [btnComplete setBackgroundImage:[UIImage imageBeforeiOS7:(isEnable?imgCompleteBtnClicked:imgCompleteBtnDisable) equaliOS7:(isEnable?imgCompleteBtnClickedFlat:imgCompleteBtnDisableFlat)] forState:UIControlEventTouchDown];
}

-(void) setEnableBtnStartStop:(BOOL) isEnable
{
    [btnStartStop setUserInteractionEnabled:isEnable];
    if (timerStatus == TIMER_STATUS_START) {
        [btnStartStop setBackgroundImage:[UIImage imageBeforeiOS7:(isEnable?imgStartBtn:imgStartBtnDisable) equaliOS7:(isEnable?imgStartBtnFlat:imgStartBtnDisableFlat)] forState:UIControlStateNormal];
        [btnStartStop setBackgroundImage:[UIImage imageBeforeiOS7:(isEnable?imgStartBtnClicked:imgStartBtnDisable) equaliOS7:(isEnable?imgStartBtnClickedFlat:imgStartBtnDisableFlat)] forState:UIControlEventTouchDown];
    }
    else {
        [btnStartStop setBackgroundImage:[UIImage imageBeforeiOS7:(isEnable?imgStopBtn:imgStartBtnDisable) equaliOS7:(isEnable?imgStopBtnFlat:imgStartBtnDisableFlat)] forState:UIControlStateNormal];
        [btnStartStop setBackgroundImage:[UIImage imageBeforeiOS7:(isEnable?imgStopBtnClicked:imgStartBtnDisable) equaliOS7:(isEnable?imgStopBtnClickedFlat:imgStartBtnDisableFlat)] forState:UIControlEventTouchDown];
    }
}

-(UIColor *) colorForLabel:(enum ENUM_ROOM_STATUS) roomStatus{
    switch (roomStatus) {
        case ENUM_ROOM_STATUS_VD:
        case ENUM_ROOM_STATUS_OD:
            return [UIColor redColor];
            
        case ENUM_ROOM_STATUS_VPU:
        case ENUM_ROOM_STATUS_OPU:
            return [UIColor yellowColor];
            
        case ENUM_ROOM_STATUS_OOO:
        case ENUM_ROOM_STATUS_OOS:
            return [UIColor grayColor];
            
        case ENUM_ROOM_STATUS_VC:
        case ENUM_ROOM_STATUS_OC:
        case ENUM_ROOM_STATUS_OC_SO:
            return [UIColor colorWithRed:0.0f green:1.0f blue:1.0f alpha:1.0f];
            
        case ENUM_ROOM_STATUS_VI:
        case ENUM_ROOM_STATUS_OI:
            return [UIColor greenColor];
            
        default:
            return [UIColor blackColor];
    }
}
#pragma mark - Timer method

-(void) runTimer
{
    if (timerStatus == TIMER_STATUS_START) {
        [CommonVariable sharedCommonVariable].roomIsCompleted = 0;
        if (timer == nil) {
            timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(countDown) userInfo:nil repeats:YES];
        }
    }
    else {
        [CommonVariable sharedCommonVariable].roomIsCompleted = 1;
        [timer invalidate];
        timer = nil;
    }
}

-(void) changeTimerStatus:(enum TIMER_STATUS) status
{
    timerStatus = status;
    [self runTimer];
}

-(void) countDownWithTimeDif:(NSInteger) timeDif
{
    if(timerStatus == TIMER_STATUS_NOT_START || timerStatus == TIMER_STATUS_PAUSE){
        return;
    }
    
    timeLeft -= timeDif;
    duration += timeDif;
    [btnTimer setTitle:[self countTimeSpendForTask:timeLeft option:YES] forState:UIControlStateNormal];
    if (timeLeft < 0) {
        [btnTimer setBackgroundImage:[UIImage imageBeforeiOS7:imgTimerOver equaliOS7:imgTimerOverFlat] forState:UIControlStateNormal];
    }
}

-(void) countDown
{
    [self countDownWithTimeDif:1];
}

- (NSString *)countTimeSpendForTask:(NSInteger)expectedTimeForTask  option:(BOOL) isAttachedSecond{
    NSString *secondsString;
    NSString *minutesString;
    NSString *hoursString;
    NSInteger secondsInt;
    NSInteger minutesInt;
    NSInteger hoursInt;
    NSString *timeDisplay;
    NSString *minus = @"-";
    if (expectedTimeForTask < 0) {
        hoursInt = -expectedTimeForTask/3600;
        minutesInt =-((expectedTimeForTask + (hoursInt*3600)) / 60);
        secondsInt =-(expectedTimeForTask % 60);
        minus = @"-";
    }
    else {
        hoursInt = expectedTimeForTask/3600;
        minutesInt = (expectedTimeForTask - (hoursInt*3600)) / 60;
        secondsInt = expectedTimeForTask % 60;
        minus = @"";
    }
    
    hoursString = [NSString stringWithFormat:@"%d",(int)hoursInt];
    minutesString = [NSString stringWithFormat:@"%d",(int)minutesInt];
    secondsString = [NSString stringWithFormat:@"%d",(int)secondsInt];
    
    if([hoursString length]==1){
        hoursString = [NSString stringWithFormat:@"0%@",hoursString];
    }
    
    if([minutesString length]==1){
        minutesString = [NSString stringWithFormat:@"0%@",minutesString];
    }
    
    if([secondsString length]==1){
        secondsString = [NSString stringWithFormat:@"0%@",secondsString];
    }
    
    if (isAttachedSecond) {
        timeDisplay = [NSString stringWithFormat:@"%@%@:%@:%@",minus,hoursString,minutesString,secondsString];
    }
    else {
        timeDisplay = [NSString stringWithFormat:@"%@%@:%@",minus,hoursString,minutesString];
    }
    
    return timeDisplay;
}

#pragma mark  - Set Captions View
-(void)setCaptionsView {
    [self setTitle:[NSString stringWithFormat:@"%@ - %@ %@", [[LanguageManagerV2 sharedLanguageManager] getFind], [[LanguageManagerV2 sharedLanguageManager] getBy], [[LanguageManagerV2 sharedLanguageManager] getRoomTitle]]];
    
}

#pragma mark - === Handle Topbar Methods ===
#pragma mark

-(void)modifyAllViews:(BOOL)isTopBarShowing
{
    TopbarViewV2 *topbar = [self getTopBarView];
    int heightTopbar = 0;
    if(isTopBarShowing) {
        heightTopbar = topbar.frame.size.height;
    }
    
    //ViewAction
    CGRect fViewAction = viewAction.frame;
    fViewAction.origin.y = heightTopbar;
    [viewAction setFrame:fViewAction];
    
    //more height to fit content table
    CGRect frTbvContent = self.tbvContent.frame;
    frTbvContent.size.height = self.tbvContent.contentSize.height + topbar.frame.size.height; //[self totalHeightOfTableContent];
    [tbvContent setFrame:frTbvContent];
    
    //View Detail
    CGRect fViewDetail = viewDetail.frame;
    fViewDetail.origin.y = self.tbvContent.contentSize.height + 5;
    [viewDetail setFrame:fViewDetail];
    
    if (scrGuests.isHidden) {
        //View Detail is hidden in NIB, so we set it to show
        [viewDetail setHidden:NO];
    }
    
    //ScrollView Content
    CGRect fScrContent = scrContent.frame;
    if([DeviceManager getScreenDeviceHeight] == DeviceScreenSizeStandardHeight4_0) {
        fScrContent.size.height = 416 - heightTopbar;
    } else {
        fScrContent.size.height = 328 - heightTopbar;
    }
    fScrContent.origin.y = fViewAction.origin.y + fViewAction.size.height;
    [scrContent setContentSize:CGSizeMake(fScrContent.size.width, fViewDetail.origin.y + fViewDetail.size.height + 5)];
    [scrContent setFrame:fScrContent];
    [scrContent setScrollEnabled:YES];
}

-(void)loadTopbarView {
    TopbarViewV2 *topbar = [[TopbarViewV2 alloc] initViewWithSuperModel:[[CommonVariable sharedCommonVariable] superRoomModel]];
    [topbar setTag:tagTopbarView];
    [self.view addSubview:topbar];
    [topbar setHidden:YES];
    [topbar setAlpha:0.0];
    [self addButtonHandleShowHideTopbar];
}

-(void)showTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationWillStartSelector:@selector(setHiddenTopbarView)];
    
    f = topbar.frame;
    f.origin.y = 0;
    [topbar setFrame:f];
    [topbar setAlpha:1.0];
    
    [self adjustShowForViews];
    
    [UIView commitAnimations];
    
}

-(void)hideTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDidStopSelector:@selector(setHiddenTopbarView)];
    [UIView setAnimationDelegate:self];
    
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    [topbar setAlpha:0.0];
    
    [self adjustRemoveForViews];
    
    [UIView commitAnimations];
}

-(void)setHiddenTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    [topbar setHidden:!topbar.hidden];
}

-(TopbarViewV2 *)getTopBarView {
    return (TopbarViewV2 *)[self.view viewWithTag:tagTopbarView];
}

-(void)addButtonHandleShowHideTopbar {
    UIView *vBarButton = [[UIView alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
    titleBarButtonFirst = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 200, 30)];
    
    titleBarButtonFirst.titleLabel.backgroundColor = [UIColor clearColor];
    [titleBarButtonFirst.titleLabel setFont: FONT_BUTTON_TOPBAR];
    titleBarButtonFirst.titleLabel.textAlignment = NSTextAlignmentCenter;
    titleBarButtonFirst.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
    
    [titleBarButtonFirst.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
    [titleBarButtonFirst.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
    [titleBarButtonFirst addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    [titleBarButtonFirst setTitle:detailSelected.addjob_detail_room_number forState:UIControlStateNormal];
    
    [vBarButton addSubview:titleBarButtonFirst];
    titleBarButtonSecond = [[UIButton alloc] initWithFrame:CGRectMake(0, 22, 200, 22)];
    [titleBarButtonSecond setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [titleBarButtonSecond.titleLabel setFont:FONT_BUTTON_TOPBAR_SECOND];
    [titleBarButtonSecond setTitle:[[LanguageManagerV2 sharedLanguageManager] getAdditionalJobTitle] forState:UIControlStateNormal];
    [titleBarButtonSecond addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    [vBarButton addSubview:titleBarButtonSecond];
    
    self.navigationItem.titleView = vBarButton;
    
    //Move title to center vertical
    CGRect titleViewBounds = self.navigationItem.titleView.bounds;
    int moveLeftTitle = 0;
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        moveLeftTitle = 10;
    }
    self.navigationItem.titleView.bounds = CGRectMake(titleViewBounds.origin.x + moveLeftTitle, Y_TOPBAR_TITLE, titleViewBounds.size.width, titleViewBounds.size.height);
}

-(void)topbarTapped:(UIButton *)sender {
    TopbarViewV2 *topbar = [self getTopBarView];
    [titleBarButtonFirst setEnabled:NO];
    [titleBarButtonSecond setEnabled:NO];
    
    if (topbar.isHidden) {
        [self showTopbarView];
    } else {
        [self hideTopbarView];
    }
    
    [self performSelector:@selector(topbarButtonEnable:) withObject:sender afterDelay:0.6f];
}

-(void) topbarButtonEnable:(UIButton *) topbarBtn {
    //[topbarBtn setEnabled:YES];
    [titleBarButtonSecond setEnabled:YES];
    [titleBarButtonFirst setEnabled:YES];
}

-(void)adjustShowForViews {
    TopbarViewV2 *topBarView = [self getTopBarView];
    CGRect fTopbarView = topBarView.frame;
    
    CGRect fViewAction = viewAction.frame;
    fViewAction.origin.y += fTopbarView.size.height;
    [viewAction setFrame:fViewAction];
    
    CGRect fTableContent = tbvContent.frame;
    fTableContent.size.height = self.tbvContent.contentSize.height + fTopbarView.size.height;
    [tbvContent setFrame:fTableContent];
    
    CGRect fViewDetail = viewDetail.frame;
    fViewDetail.origin.y = self.tbvContent.contentSize.height + 5; // fTopbarView.size.height;
    [viewDetail setFrame:fViewDetail];
    
    CGRect fScrViewContent = scrContent.frame;
    fScrViewContent.origin.y += fTopbarView.size.height;
    fScrViewContent.size.height -= fTopbarView.size.height;
    [scrContent setFrame:fScrViewContent];
    
//    CGRect fGuestInfoView = scrGuests.frame;
//    fGuestInfoView.size.height -= fTopbarView.size.height;
//    [scrGuests setFrame:fGuestInfoView];
    
    CGRect fScrGuest = scrGuests.frame;
    fScrGuest.origin.y += fTopbarView.size.height;
    fScrGuest.size.height -= fTopbarView.size.height;
    [scrGuests setFrame:fScrGuest];
    
}

-(void)adjustRemoveForViews {
    TopbarViewV2 *topBarView = [self getTopBarView];
    CGRect fTopbarView = topBarView.frame;
    
    CGRect fViewAction = viewAction.frame;
    fViewAction.origin.y -= fTopbarView.size.height;
    [viewAction setFrame:fViewAction];
    
    CGRect fTableContent = tbvContent.frame;
    fTableContent.size.height = self.tbvContent.contentSize.height;
    [tbvContent setFrame:fTableContent];
    
    CGRect fViewDetail = viewDetail.frame;
    fViewDetail.origin.y = self.tbvContent.contentSize.height + 5;
    [viewDetail setFrame:fViewDetail];
    
    CGRect fScrViewContent = scrContent.frame;
    fScrViewContent.origin.y -= fTopbarView.size.height;
    fScrViewContent.size.height += fTopbarView.size.height;
    [scrContent setFrame:fScrViewContent];
    
//    CGRect fGuestInfoView = scrGuests.frame;
//    fGuestInfoView.size.height += fTopbarView.size.height;
//    [scrGuests setFrame:fGuestInfoView];
    
    CGRect fScrGuest = scrGuests.frame;
    fScrGuest.origin.y -= fTopbarView.size.height;
    fScrGuest.size.height += fTopbarView.size.height;
    [scrGuests setFrame:fScrGuest];
    
}

#pragma mark - === Handle Back Button ===
#pragma mark

-(UIButton*)createButtonAction
{
//    [btnAction setUserInteractionEnabled:isEnable];
//    @"btn_bg-cleaning status & action.png"
//    [btnAction setBackgroundImage:[UIImage imageNamed:(isEnable?imgCleaningStatusBtnAndActionBtn:imgCleaningStatusBtnAndActionBtnDisable)]
//                         forState:UIControlStateNormal];
//    [btnAction setBackgroundImage:[UIImage imageNamed:(isEnable?imgCleaningStatusBtnAndActionBtnClicked:imgCleaningStatusBtnAndActionBtnDisable)]
//                         forState:UIControlEventTouchDown];
    return nil;
}

-(void)backBarPressed {
    if(timerStatus == TIMER_STATUS_START)
    {
//        NSString *message = @"Please complete the room before leaving";
        NSString *message = [L_please_complete_the_additional_job_before_leaving currentKeyToLanguage];
        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], nil] title:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_lostandfound_msg_alert] message:message delegate:nil cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_OK] otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void)addBackButton {    
    //back button navigation
    self.navigationItem.leftBarButtonItem = [MyNavigationBarV2 createBackButtonWithTarget:self selector:@selector(backBarPressed) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - Scroll View Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if(scrollView == scrGuests) {
        if(listGuestModels.count > 1) {
            // Switch the indicator when more than 50% of the previous/next page is visible
            CGFloat pageWidth = scrollView.frame.size.width;
            int page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
            if(page == 0) { //First Page
                [guestIndicatorLeft setHidden:YES];
                [guestIndicatorRight setHidden:NO];
            } else if(page == listGuestModels.count - 1) { //Last Page
                [guestIndicatorLeft setHidden:NO];
                [guestIndicatorRight setHidden:YES];
            } else {
                [guestIndicatorLeft setHidden:NO];
                [guestIndicatorRight setHidden:NO];
            }
        }
    }
}

#pragma mark - iOS 7 Resources
-(void)loadFlatResource
{
    [btnRoomAssignment setBackgroundImage:[UIImage imageNamed:imgGuestInfoActiveFlat] forState:UIControlStateNormal];
    [btnCleaningStatus setBackgroundImage:[UIImage imageBeforeiOS7:imgCleaningStatusBtnAndActionBtn equaliOS7:imgCleaningStatusBtnAndActionBtnFlat]
                                 forState:UIControlStateNormal];
    [btnCleaningStatus setTitle:[L_job_status currentKeyToLanguage] forState:UIControlStateNormal];
    [lblGuestInfo setText:[L_GUEST_INFO currentKeyToLanguage]];
}

@end
