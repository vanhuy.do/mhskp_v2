//
//  AdditionalJobListByFloorViewV2.h
//  mHouseKeeping
//
//  Created by Quang Nguyen on Mar 18, 2013.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AdditionalJobSearchViewV2.h"
#import "AdditionalJobManagerV2.h"
#import "SyncManagerV2.h"
#import <ZXingWidgetController.h>

#define markImage           @"!.png"
#define backgroundImage     @"bg.png"

#define tagNoResult         12345
#define heightTbvContent    55.0f

#define font                @"Arial-BoldMT"

#define sizeNoResult        22.0f
#define sizeNormal          17.0f
#define sizeSmall           10.0f

#define colorAlpha          1.0
#define colorWhite          255/255.0
#define colorBlueR          105/255.0
#define colorBlueG          218/255.0
#define colorBlueB          222/255.0

#define roomStatusType      1
#define cleaningStatusType  2
#define kindOfRoomType      3
#define inspectedCase       -2

@interface AdditionalJobListByFloorViewV2 : UIViewController<AdditionalJobSearchViewDelegate, ZXingDelegate, UISearchBarDelegate>
{
    AdditionalJobManagerV2 *addJobManager;
    SyncManagerV2 *syncManager;
    
    AccessRight *QRCodeScanner;
    UIButton *qrScanButton;
    
    NSMutableArray *listItemsSearch;
    BOOL hasCancelSearch;
    UILabel *lblNoResult;
}

@property (nonatomic, strong) IBOutlet UITableView *tbvContent;
@property (strong) NSMutableArray *listDisplayData;
@property (nonatomic, strong) IBOutlet UISearchBar *searchBar;
@property (nonatomic, strong) IBOutlet UIView *searchView;
@property (nonatomic) bool isReloadDataOnShow;

-(void) loadData;

@end
